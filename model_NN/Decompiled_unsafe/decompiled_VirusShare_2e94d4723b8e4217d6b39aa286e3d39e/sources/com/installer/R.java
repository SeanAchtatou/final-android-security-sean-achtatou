package com.installer;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int icon = 2130837504;
        public static final int opera = 2130837505;
    }

    public static final class id {
        public static final int button1 = 2131034117;
        public static final int button2 = 2131034118;
        public static final int linearLayout2 = 2131034112;
        public static final int linearLayout3 = 2131034116;
        public static final int linearLayout4 = 2131034114;
        public static final int scrollView1 = 2131034113;
        public static final int textView1 = 2131034115;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class string {
        public static final int app_name = 2130968576;
    }
}
