package com.soft.android.appinstaller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/* compiled from: MemberActivity */
class RemoteCache {
    private static RemoteCache instance = null;
    private HashMap<String, String> cache = new HashMap<>();
    private String currentLink;
    private List<String> data = new ArrayList();
    private Stack<String> history = new Stack<>();
    private List<String> links = new ArrayList();
    private List<String> types = new ArrayList();

    private RemoteCache() {
    }

    public static RemoteCache getInstance() {
        if (instance == null) {
            instance = new RemoteCache();
        }
        return instance;
    }

    public List<String> getData() {
        return this.data;
    }

    public List<String> getLinks() {
        return this.links;
    }

    public List<String> getTypes() {
        return this.types;
    }

    public void setLink(String newLink) {
        this.currentLink = newLink;
    }

    public String getLink() {
        return this.currentLink;
    }

    public Stack<String> getHistory() {
        return this.history;
    }

    public HashMap<String, String> getCache() {
        return this.cache;
    }
}
