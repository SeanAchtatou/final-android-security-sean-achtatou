package com.google.mygson.internal.bind;

import com.google.mygson.Gson;
import com.google.mygson.TypeAdapter;
import com.google.mygson.TypeAdapterFactory;
import com.google.mygson.internal.C$Gson$Types;
import com.google.mygson.internal.ConstructorConstructor;
import com.google.mygson.internal.ObjectConstructor;
import com.google.mygson.reflect.TypeToken;
import com.google.mygson.stream.JsonReader;
import com.google.mygson.stream.JsonToken;
import com.google.mygson.stream.JsonWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;

public final class CollectionTypeAdapterFactory implements TypeAdapterFactory {
    private final ConstructorConstructor constructorConstructor;

    public CollectionTypeAdapterFactory(ConstructorConstructor constructorConstructor2) {
        this.constructorConstructor = constructorConstructor2;
    }

    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
        Type type = typeToken.getType();
        Class<? super T> rawType = typeToken.getRawType();
        if (!Collection.class.isAssignableFrom(rawType)) {
            return null;
        }
        Type elementType = C$Gson$Types.getCollectionElementType(type, rawType);
        return new Adapter(gson, elementType, gson.getAdapter(TypeToken.get(elementType)), this.constructorConstructor.getConstructor(typeToken));
    }

    private final class Adapter<E> extends TypeAdapter<Collection<E>> {
        private final ObjectConstructor<? extends Collection<E>> constructor;
        private final TypeAdapter<E> elementTypeAdapter;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.mygson.internal.bind.CollectionTypeAdapterFactory.Adapter.write(com.google.mygson.stream.JsonWriter, java.util.Collection):void
         arg types: [com.google.mygson.stream.JsonWriter, java.lang.Object]
         candidates:
          com.google.mygson.internal.bind.CollectionTypeAdapterFactory.Adapter.write(com.google.mygson.stream.JsonWriter, java.lang.Object):void
          com.google.mygson.TypeAdapter.write(com.google.mygson.stream.JsonWriter, java.lang.Object):void
          com.google.mygson.internal.bind.CollectionTypeAdapterFactory.Adapter.write(com.google.mygson.stream.JsonWriter, java.util.Collection):void */
        public /* bridge */ /* synthetic */ void write(JsonWriter x0, Object x1) throws IOException {
            write(x0, (Collection) ((Collection) x1));
        }

        public Adapter(Gson context, Type elementType, TypeAdapter<E> elementTypeAdapter2, ObjectConstructor<? extends Collection<E>> constructor2) {
            this.elementTypeAdapter = new TypeAdapterRuntimeTypeWrapper(context, elementTypeAdapter2, elementType);
            this.constructor = constructor2;
        }

        public Collection<E> read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            Collection<E> collection = (Collection) this.constructor.construct();
            in.beginArray();
            while (in.hasNext()) {
                collection.add(this.elementTypeAdapter.read(in));
            }
            in.endArray();
            return collection;
        }

        public void write(JsonWriter out, Collection<E> collection) throws IOException {
            if (collection == null) {
                out.nullValue();
                return;
            }
            out.beginArray();
            for (E element : collection) {
                this.elementTypeAdapter.write(out, element);
            }
            out.endArray();
        }
    }
}
