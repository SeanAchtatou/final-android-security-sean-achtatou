package com.apperhand.device.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.apperhand.common.dto.EULAAcceptDetails;

public class a {
    Context a;

    /* renamed from: com.apperhand.device.android.a$a  reason: collision with other inner class name */
    private enum C0001a {
        SHOW,
        EMPTY,
        TERMINATE
    }

    public a(Context context) {
        this.a = context;
    }

    private C0001a b() {
        String string = this.a.getSharedPreferences("com.apperhand.global", 0).getString("NewEulaTemplate", null);
        return (string == null || !string.toLowerCase().startsWith("empty_terminate")) ? (string == null || !string.toLowerCase().startsWith("empty")) ? C0001a.SHOW : C0001a.EMPTY : C0001a.TERMINATE;
    }

    private boolean c() {
        return this.a.getSharedPreferences("com.apperhand.global", 0).getBoolean("ACCEPTED_EULA", false);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putBoolean("ACCEPTED_EULA", z);
        edit.commit();
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        if (c()) {
            return true;
        }
        C0001a b = b();
        if (b == C0001a.TERMINATE) {
            return false;
        }
        if (b == C0001a.EMPTY) {
            SharedPreferences sharedPreferences = this.a.getSharedPreferences("com.apperhand.global", 0);
            String string = sharedPreferences.getString("NewEulaChain", null);
            String string2 = sharedPreferences.getString("NewStep", null);
            long j = sharedPreferences.getLong("EulaCounter", 0);
            long j2 = sharedPreferences.getLong("EulaGlobalCounter", 0);
            Bundle bundle = new Bundle();
            EULAAcceptDetails eULAAcceptDetails = new EULAAcceptDetails();
            eULAAcceptDetails.setButton(null);
            eULAAcceptDetails.setTemplate("empty");
            eULAAcceptDetails.setAccepted(false);
            if (string == null) {
                string = "default";
            }
            eULAAcceptDetails.setChain(string);
            eULAAcceptDetails.setStep(string2);
            long j3 = j2 + 1;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putLong("EulaGlobalCounter", j3);
            edit.commit();
            eULAAcceptDetails.setCounter(j);
            eULAAcceptDetails.setGlobalCounter(j3);
            bundle.putSerializable("eulaAcceptDetails", eULAAcceptDetails);
            AndroidSDKProvider.a(this.a, 3, bundle);
            return false;
        }
        Intent intent = new Intent(this.a, EULAActivity.class);
        intent.setFlags(335544320);
        this.a.startActivity(intent);
        return false;
    }
}
