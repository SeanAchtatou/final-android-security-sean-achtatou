package com.apperhand.device.android.a;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import com.apperhand.common.dto.EULADetails;
import com.apperhand.device.a.a.b;
import java.io.FileOutputStream;

public class d implements b {
    private Context a;

    public d(Context context) {
        this.a = context;
    }

    public void a(String str, String str2) {
        Intent intent = new Intent("com.apperhand.action.show.eula");
        intent.setPackage(this.a.getPackageName());
        if (!(str == null || str2 == null)) {
            intent.putExtra("bodyHTML", str);
            intent.putExtra("footerHTML", str2);
        }
        this.a.sendBroadcast(intent);
    }

    public boolean a(EULADetails eULADetails) {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putString("NewEulaTemplate", eULADetails.getTemplate());
        edit.putString("NewEulaChain", eULADetails.getChain());
        edit.putString("NewStep", eULADetails.getStep());
        edit.commit();
        return true;
    }

    public boolean a(byte[] bArr, String str) {
        if (bArr == null) {
            return false;
        }
        try {
            FileOutputStream openFileOutput = this.a.openFileOutput(str, 0);
            openFileOutput.write(bArr);
            openFileOutput.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
