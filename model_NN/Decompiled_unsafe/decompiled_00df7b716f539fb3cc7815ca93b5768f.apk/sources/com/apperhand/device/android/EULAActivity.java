package com.apperhand.device.android;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.apperhand.common.dto.EULAAcceptDetails;
import com.apperhand.device.android.c.f;
import com.apperhand.device.android.c.g;

public class EULAActivity extends Activity {
    /* access modifiers changed from: private */
    public final Handler a = new Handler();
    private EulaJsInterface b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public View d = null;
    /* access modifiers changed from: private */
    public WebView e = null;
    private WebView f = null;
    /* access modifiers changed from: private */
    public View g = null;
    /* access modifiers changed from: private */
    public BroadcastReceiver h = null;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public int j = 0;
    /* access modifiers changed from: private */
    public boolean k = false;
    private String l;
    /* access modifiers changed from: private */
    public String m;
    /* access modifiers changed from: private */
    public String n;
    private String o;
    /* access modifiers changed from: private */
    public long p = 0;
    private long q = 0;
    /* access modifiers changed from: private */
    public AsyncTask<Void, Void, Void> r = null;
    /* access modifiers changed from: private */
    public int s;

    final class EulaJsInterface {
        boolean processed;

        EulaJsInterface() {
        }

        @JavascriptInterface
        public void press(final String str, final String str2, final boolean z) {
            if (!this.processed) {
                this.processed = true;
                EULAActivity.this.a.post(new Runnable() {
                    public void run() {
                        EULAActivity.this.a(str, str2, z);
                    }
                });
            }
        }
    }

    final class a extends AsyncTask<Void, Void, Void> {
        private boolean b;

        a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, long):long
         arg types: [com.apperhand.device.android.EULAActivity, int]
         candidates:
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, int):int
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, android.content.BroadcastReceiver):android.content.BroadcastReceiver
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, android.os.AsyncTask):android.os.AsyncTask
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, java.lang.String):java.lang.String
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, boolean):void
          com.apperhand.device.android.EULAActivity.a(java.lang.String, java.lang.String):void
          com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, long):long */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Void doInBackground(Void... voidArr) {
            int unused = EULAActivity.this.s = EULAActivity.this.getPackageManager().checkPermission("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS", EULAActivity.this.getPackageName());
            this.b = EULAActivity.this.a();
            if (this.b) {
                return null;
            }
            long unused2 = EULAActivity.this.p = 0L;
            BroadcastReceiver unused3 = EULAActivity.this.h = new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    EULAActivity.this.a.removeCallbacksAndMessages(null);
                    String stringExtra = intent.getStringExtra("bodyHTML");
                    String stringExtra2 = intent.getStringExtra("footerHTML");
                    if (stringExtra == null || stringExtra == null) {
                        EULAActivity.this.a(EULAActivity.this.c);
                    } else if (EULAActivity.this.c) {
                        SharedPreferences sharedPreferences = EULAActivity.this.getSharedPreferences("com.apperhand.global", 0);
                        String unused = EULAActivity.this.m = sharedPreferences.getString("NewEulaChain", null);
                        String unused2 = EULAActivity.this.n = sharedPreferences.getString("NewStep", null);
                        EULAActivity.this.a(stringExtra, stringExtra2);
                    }
                }
            };
            return null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.apperhand.device.android.EULAActivity.b(com.apperhand.device.android.EULAActivity, boolean):boolean
         arg types: [com.apperhand.device.android.EULAActivity, int]
         candidates:
          com.apperhand.device.android.EULAActivity.b(com.apperhand.device.android.EULAActivity, java.lang.String):java.lang.String
          com.apperhand.device.android.EULAActivity.b(com.apperhand.device.android.EULAActivity, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Void voidR) {
            super.onPostExecute(voidR);
            AsyncTask unused = EULAActivity.this.r = (AsyncTask) null;
            if (!this.b) {
                EULAActivity.this.registerReceiver(EULAActivity.this.h, new IntentFilter("com.apperhand.action.show.eula"));
                boolean unused2 = EULAActivity.this.i = true;
                AndroidSDKProvider.a(EULAActivity.this, 2, null);
                EULAActivity.this.a.postDelayed(new Runnable() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, boolean):void
                     arg types: [com.apperhand.device.android.EULAActivity, int]
                     candidates:
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, int):int
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, long):long
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, android.content.BroadcastReceiver):android.content.BroadcastReceiver
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, android.os.AsyncTask):android.os.AsyncTask
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, java.lang.String):java.lang.String
                      com.apperhand.device.android.EULAActivity.a(java.lang.String, java.lang.String):void
                      com.apperhand.device.android.EULAActivity.a(com.apperhand.device.android.EULAActivity, boolean):void */
                    public void run() {
                        boolean unused = EULAActivity.this.c = false;
                        EULAActivity.this.a(true);
                    }
                }, 3500);
                return;
            }
            BroadcastReceiver unused3 = EULAActivity.this.h = (BroadcastReceiver) null;
            EULAActivity.this.a("new_eula_body.html", "new_eula_footer.html");
        }
    }

    final class b extends AsyncTask<Void, Void, Boolean> {
        String a;
        String b;
        boolean c;

        private b(boolean z) {
            this.a = null;
            this.b = null;
            this.c = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Void... voidArr) {
            boolean z;
            boolean z2;
            boolean z3 = false;
            if (!EULAActivity.this.getFileStreamPath("offline_startapp_eula_body.html").exists() || !EULAActivity.this.getFileStreamPath("offline_startapp_eula_footer.html").exists()) {
                String path = EULAActivity.this.getFilesDir().getPath();
                String str = path + "/" + "offline_startapp_eula_footer.html";
                String str2 = path + "/" + "offline_startapp_eula_body.html";
                String str3 = path + "/" + "eula.zip";
                try {
                    if (g.a(EULAActivity.this.getPackageManager().getApplicationInfo(EULAActivity.this.getPackageName(), 0).sourceDir, "eula.zip", str3) || g.a(EULAActivity.this.getAssets(), "eula.zip", str3)) {
                        z2 = g.a(str3, "offline_startapp_eula_footer.html", str);
                        try {
                            z = g.a(str3, "offline_startapp_eula_body.html", str2);
                            try {
                                this.b = "offline_startapp_eula_body.html";
                                this.a = "offline_startapp_eula_footer.html";
                                EULAActivity.this.deleteFile("eula.zip");
                            } catch (PackageManager.NameNotFoundException e) {
                            }
                        } catch (PackageManager.NameNotFoundException e2) {
                            z = false;
                        }
                    } else {
                        z = false;
                        z2 = false;
                    }
                } catch (PackageManager.NameNotFoundException e3) {
                    z = false;
                    z2 = false;
                }
                if (z2 && z) {
                    z3 = true;
                }
                return Boolean.valueOf(z3);
            }
            this.b = "offline_startapp_eula_body.html";
            this.a = "offline_startapp_eula_footer.html";
            return Boolean.TRUE;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (!this.c) {
                return;
            }
            if (bool.booleanValue()) {
                EULAActivity.this.a(this.b, this.a);
            } else {
                EULAActivity.this.finish();
            }
        }
    }

    final class c extends WebViewClient {
        c() {
        }

        public void onPageFinished(WebView webView, String str) {
            EULAActivity.this.runOnUiThread(new Runnable() {
                public void run() {
                    EULAActivity.e(EULAActivity.this);
                    if (EULAActivity.this.j == 2) {
                        EULAActivity.this.g.setVisibility(8);
                        EULAActivity.this.d.setVisibility(0);
                    }
                }
            });
            if (webView == EULAActivity.this.e && EULAActivity.this.s == 0) {
                EULAActivity.this.e.loadUrl("javascript:showBookmark()");
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            boolean unused = EULAActivity.this.k = true;
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return str.indexOf("eula_more") <= -1 && !str.startsWith("javascript:showBookmark");
        }
    }

    private void a(WebView webView) {
        webView.setWebViewClient(new c());
        webView.getSettings().setJavaScriptEnabled(true);
        webView.clearCache(true);
        webView.setWebChromeClient(new WebChromeClient());
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        this.j = 0;
        this.e.loadUrl("file:///" + this.l + "/" + str);
        this.f.loadUrl("file:///" + this.l + "/" + str2);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, boolean z) {
        Bundle bundle = new Bundle();
        EULAAcceptDetails eULAAcceptDetails = new EULAAcceptDetails();
        eULAAcceptDetails.setButton(str2);
        eULAAcceptDetails.setTemplate(str);
        eULAAcceptDetails.setAccepted(z);
        eULAAcceptDetails.setChain(this.m == null ? "default" : this.m);
        eULAAcceptDetails.setStep(this.n);
        b();
        eULAAcceptDetails.setCounter(this.p);
        eULAAcceptDetails.setGlobalCounter(this.q);
        bundle.putSerializable("eulaAcceptDetails", eULAAcceptDetails);
        AndroidSDKProvider.a(getApplicationContext(), 3, bundle);
        if (z) {
            new a(this).a(true);
            AndroidSDKProvider.a(getApplicationContext(), 1, null);
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.m = "default";
        this.n = null;
        new b(z).execute((Object[]) null);
    }

    /* access modifiers changed from: private */
    public boolean a() {
        boolean z = false;
        synchronized (com.apperhand.device.a.c.c.f) {
            SharedPreferences sharedPreferences = getSharedPreferences("com.apperhand.global", 0);
            this.m = sharedPreferences.getString("NewEulaChain", null);
            this.n = sharedPreferences.getString("NewStep", null);
            this.o = sharedPreferences.getString("NewEulaTemplate", null);
            this.p = sharedPreferences.getLong("EulaCounter", 0);
            this.q = sharedPreferences.getLong("EulaGlobalCounter", 0);
            if (this.n != null && this.m != null && this.o != null) {
                if (getFileStreamPath("new_eula_body.html").exists() && getFileStreamPath("new_eula_footer.html").exists()) {
                    z = true;
                }
            }
        }
        return z;
    }

    private void b() {
        this.p++;
        this.q++;
        SharedPreferences.Editor edit = getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putLong("EulaCounter", this.p);
        edit.putLong("EulaGlobalCounter", this.q);
        edit.commit();
    }

    private void c() {
        int i2 = 8;
        int i3 = getResources().getConfiguration().orientation;
        int orientation = getWindowManager().getDefaultDisplay().getOrientation();
        int i4 = 9;
        if (Build.VERSION.SDK_INT <= 8) {
            i4 = 1;
            i2 = 0;
        }
        if (orientation == 0 || orientation == 1) {
            if (i3 == 1) {
                setRequestedOrientation(1);
            } else if (i3 == 2) {
                setRequestedOrientation(0);
            }
        } else if (orientation != 2 && orientation != 3) {
        } else {
            if (i3 == 1) {
                setRequestedOrientation(i4);
            } else if (i3 == 2) {
                setRequestedOrientation(i2);
            }
        }
    }

    static /* synthetic */ int e(EULAActivity eULAActivity) {
        int i2 = eULAActivity.j;
        eULAActivity.j = i2 + 1;
        return i2;
    }

    public void onBackPressed() {
        if (this.k) {
            super.onBackPressed();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        this.b = new EulaJsInterface();
        this.l = getFilesDir().getAbsolutePath();
        this.k = false;
        float f2 = getResources().getDisplayMetrics().density;
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        frameLayout.setPadding(0, 0, 0, 0);
        frameLayout.setBackgroundColor(0);
        frameLayout.setPadding((int) ((20.0f * f2) + 0.5f), 0, (int) ((f2 * 20.0f) + 0.5f), 0);
        this.e = new WebView(this);
        this.f = new WebView(this);
        this.d = f.a(this, "License Agreement", this.e, this.f);
        a(this.e);
        a(this.f);
        this.f.setWebChromeClient(new WebChromeClient());
        this.f.addJavascriptInterface(this.b, "startapp");
        this.g = f.a(this);
        frameLayout.addView(this.g);
        frameLayout.addView(this.d);
        this.d.setVisibility(8);
        this.g.setVisibility(0);
        this.r = new a();
        this.r.execute(new Void[0]);
        c();
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.h != null && this.i) {
            unregisterReceiver(this.h);
            this.h = null;
        }
        if (this.r != null) {
            this.r.cancel(true);
            this.r = null;
        }
        this.a.removeCallbacksAndMessages(null);
        super.onDestroy();
    }
}
