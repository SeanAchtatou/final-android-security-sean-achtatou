package com.apperhand.device.android;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.webkit.WebView;
import com.apperhand.common.dto.ApplicationDetails;
import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;
import com.apperhand.device.a.e.b;
import com.apperhand.device.a.e.c;
import com.apperhand.device.android.a.d;
import com.apperhand.device.android.a.e;
import com.apperhand.device.android.a.f;
import com.apperhand.device.android.a.g;
import com.apperhand.device.android.a.h;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;

public class AndroidSDKProvider extends IntentService implements a {
    private static boolean a = false;
    private String b;
    private int c = 0;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f;
    private c g;
    private com.apperhand.device.a.d.a h;
    private com.apperhand.device.android.a.a i;
    private com.apperhand.device.android.a.c j;
    private g k;
    private h l;
    private e m;
    private d n;
    private f o;

    public enum SearchCategory {
        WEB,
        IMAGES,
        VIDEO
    }

    public AndroidSDKProvider() {
        super("AndroidSDKProvider");
    }

    private static void a(Context context) {
        String str;
        boolean z;
        boolean z2;
        int i2;
        boolean z3 = false;
        if (a) {
            HashSet<String> hashSet = new HashSet<>();
            hashSet.add("android.permission.INTERNET");
            hashSet.add("android.permission.ACCESS_WIFI_STATE");
            hashSet.add("android.permission.READ_PHONE_STATE");
            hashSet.add("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS");
            hashSet.add("com.android.browser.permission.READ_HISTORY_BOOKMARKS");
            hashSet.add("com.android.launcher.permission.INSTALL_SHORTCUT");
            hashSet.add("com.android.launcher.permission.UNINSTALL_SHORTCUT");
            hashSet.add("com.android.launcher.permission.READ_SETTINGS");
            hashSet.add("com.htc.launcher.permission.READ_SETTINGS");
            hashSet.add("com.motorola.launcher.permission.READ_SETTINGS");
            hashSet.add("com.motorola.dlauncher.permission.READ_SETTINGS");
            hashSet.add("com.fede.launcher.permission.READ_SETTINGS");
            hashSet.add("com.lge.launcher.permission.READ_SETTINGS");
            hashSet.add("org.adw.launcher.permission.READ_SETTINGS");
            hashSet.add("com.android.launcher.permission.WRITE_SETTINGS");
            hashSet.add("com.htc.launcher.permission.WRITE_SETTINGS");
            hashSet.add("com.motorola.launcher.permission.WRITE_SETTINGS");
            hashSet.add("com.motorola.dlauncher.permission.WRITE_SETTINGS");
            hashSet.add("com.fede.launcher.permission.WRITE_SETTINGS");
            hashSet.add("com.lge.launcher.permission.WRITE_SETTINGS");
            hashSet.add("org.adw.launcher.permission.WRITE_SETTINGS");
            hashSet.add("com.motorola.launcher.permission.INSTALL_SHORTCUT");
            hashSet.add("com.motorola.dlauncher.permission.INSTALL_SHORTCUT");
            hashSet.add("com.lge.launcher.permission.INSTALL_SHORTCUT");
            HashSet<String> hashSet2 = new HashSet<>();
            hashSet2.add("com.android.browser.permission.WRITE_HISTORY_BOOKMARKS");
            hashSet2.add("com.android.browser.permission.READ_HISTORY_BOOKMARKS");
            StringBuilder sb = new StringBuilder();
            PackageInfo packageInfo = null;
            try {
                packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4101);
            } catch (Exception e2) {
            }
            if (packageInfo != null) {
                ActivityInfo[] activityInfoArr = packageInfo.activities;
                ServiceInfo[] serviceInfoArr = packageInfo.services;
                String[] strArr = packageInfo.requestedPermissions;
                if (activityInfoArr != null) {
                    boolean z4 = true;
                    for (ActivityInfo activityInfo : activityInfoArr) {
                        if ("com.apperhand.device.android.EULAActivity".equals(activityInfo.name)) {
                            z4 = false;
                        }
                    }
                    z = z4;
                } else {
                    z = true;
                }
                if (serviceInfoArr != null) {
                    boolean z5 = true;
                    for (ServiceInfo serviceInfo : serviceInfoArr) {
                        if ("com.apperhand.device.android.AndroidSDKProvider".equals(serviceInfo.name)) {
                            z5 = false;
                        }
                    }
                    z2 = z5;
                } else {
                    z2 = true;
                }
                if (strArr != null) {
                    for (String str2 : strArr) {
                        if (hashSet.contains(str2)) {
                            hashSet.remove(str2);
                        }
                        if (hashSet2.contains(str2)) {
                            hashSet2.remove(str2);
                        }
                    }
                }
                boolean z6 = !hashSet.isEmpty();
                boolean z7 = !hashSet2.isEmpty();
                StringBuilder sb2 = new StringBuilder();
                StringBuilder sb3 = new StringBuilder();
                com.apperhand.device.android.c.g.a(context, sb2, sb3);
                if (z) {
                    sb.append(1).append(". Decleration of Activity 'com.apperhand.device.android.EULAActivity' is missing\n");
                    i2 = 1;
                } else {
                    i2 = 0;
                }
                if (z2) {
                    i2++;
                    sb.append(i2).append(". Decleration of Service 'com.apperhand.device.android.AndroidSDKProvider' is missing\n");
                }
                if (sb2.length() == 0 || sb3.length() == 0) {
                    i2++;
                    sb.append(i2).append(". Developer/App-ID is missing\n");
                }
                if (z6) {
                    i2++;
                    sb.append(i2).append(". The following permissions are missing:\n");
                    for (String append : hashSet) {
                        sb.append(append).append(10);
                    }
                }
                int i3 = i2;
                if (sb.length() == 0) {
                    sb.append("Integration was completed successfully\n");
                    z3 = true;
                }
                if (z7) {
                    int i4 = i3 + 1;
                    sb.append("Warning - The following permissions are missing:\n");
                    for (String append2 : hashSet2) {
                        sb.append(append2).append(10);
                    }
                }
            } else {
                sb.append("Can't retrieve packageInfo");
            }
            if (z3) {
                Log.i("apperhand", sb.toString());
                str = "StartApp - Success";
            } else {
                Log.e("apperhand", sb.toString());
                str = "StartApp - Failed Integration";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(sb);
            builder.setCancelable(true);
            builder.setTitle(str);
            builder.create().show();
        }
    }

    static void a(Context context, int i2, Bundle bundle) {
        int i3;
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        com.apperhand.device.android.c.g.a(context, sb, sb2);
        if (sb.length() != 0 && sb2.length() != 0) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.apperhand.global", 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            try {
                i3 = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e2) {
                i3 = 0;
            }
            int i4 = sharedPreferences.getInt("VERSION_CODE", 0);
            if (sharedPreferences.getBoolean("TERMINATE", false)) {
                if (i3 > i4) {
                    edit.putBoolean("TERMINATE", false);
                } else {
                    return;
                }
            }
            edit.putInt("VERSION_CODE", i3);
            edit.commit();
            Intent intent = new Intent(context, AndroidSDKProvider.class);
            if (bundle != null) {
                intent.putExtras(bundle);
            }
            String str = new String(b.a(com.apperhand.device.a.e.a.a("CRoQAlVGS1keGVoEHgRLEBoOGRdLEUE+agQtJzsiJj8tABJOHhYdGwYHQQU=", 0), null));
            intent.putExtra("APPLICATION_ID", sb2.toString());
            intent.putExtra("DEVELOPER_ID", sb.toString());
            intent.putExtra("M_SERVER_URL", str);
            intent.putExtra("FIRST_RUN", Boolean.TRUE);
            intent.putExtra("USER_AGENT", new WebView(context).getSettings().getUserAgentString());
            intent.putExtra("SERVICE_MODE", i2);
            context.startService(intent);
        }
    }

    public static void a(Context context, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("externalData", str);
        a(context, 5, bundle);
    }

    private boolean a(com.apperhand.device.a.b bVar, Command.Commands commands, SharedPreferences sharedPreferences, boolean z) {
        String str;
        Log.v("apperhand", "handleAlarm start [" + commands + "]");
        if (commands == Command.Commands.INFO) {
            str = "NEXT_INFO_RUN";
        } else if (commands != Command.Commands.COMMANDS_DETAILS) {
            return false;
        } else {
            str = "NEXT_DETAILS_RUN";
        }
        long currentTimeMillis = System.currentTimeMillis();
        long j2 = sharedPreferences.getLong(str, -1);
        if (z || j2 != -1) {
            if (j2 == -1) {
                j2 = currentTimeMillis;
            } else {
                bVar.a(false);
            }
            Log.v("apperhand", "handleAlarm currentTime=[" + currentTimeMillis + "], nextRun=[" + j2 + "]");
            if (currentTimeMillis < j2) {
                Log.v("apperhand", "handleAlarm end go schedule");
                bVar.a(j2, commands);
                return false;
            }
            Log.v("apperhand", "handleAlarm end return true");
            return true;
        }
        Log.v("apperhand", "handleAlarm end (!forceRun && nextRun == -1)");
        return false;
    }

    public static void initSDK(Context context) {
        initSDK(context, true);
    }

    public static void initSDK(Context context, boolean z) {
        if (Build.VERSION.SDK_INT >= 7) {
            if ((context.getApplicationInfo().flags & 2) != 0 && a) {
                a(context);
            } else if (!z) {
                a(context, 1, null);
            } else if (new a(context).a()) {
                a(context, 1, null);
                Log.v("apperhand", "Eula was accepted");
            }
        }
    }

    public static String searchURL(String str) {
        return searchURL(str, SearchCategory.WEB);
    }

    public static String searchURL(String str, SearchCategory searchCategory) {
        String replace;
        String a2 = com.apperhand.device.android.c.a.a().a("SEARCH_URL", "http://www.searchmobileonline.com/{$CATEGORY$}?sourceid=7&q={$QUERY$}");
        switch (searchCategory) {
            case WEB:
                replace = a2.replace("{$CATEGORY$}", "");
                break;
            case IMAGES:
                replace = a2.replace("{$CATEGORY$}", "simages");
                break;
            case VIDEO:
                replace = a2.replace("{$CATEGORY$}", "svideos");
                break;
            default:
                replace = a2.replace("{$CATEGORY$}", "");
                break;
        }
        return str != null ? replace.replace("{$QUERY$}", URLEncoder.encode(str)) : replace.replace("{$QUERY$}", "");
    }

    public static void setTestMode(boolean z) {
        a = z;
    }

    public c a() {
        return this.g;
    }

    public com.apperhand.device.a.d.a b() {
        return this.h;
    }

    public com.apperhand.device.a.a.a c() {
        return this.j;
    }

    public com.apperhand.device.a.a.f d() {
        return this.k;
    }

    public com.apperhand.device.a.a.g e() {
        return this.l;
    }

    public com.apperhand.device.a.a.c f() {
        return this.m;
    }

    public com.apperhand.device.a.a.b g() {
        return this.n;
    }

    public com.apperhand.device.a.a.d h() {
        return this.o;
    }

    public com.apperhand.device.a.e.d i() {
        return com.apperhand.device.android.c.a.a();
    }

    public ApplicationDetails j() {
        ApplicationDetails applicationDetails = new ApplicationDetails();
        applicationDetails.setApplicationId(this.d);
        applicationDetails.setDeveloperId(this.e);
        applicationDetails.setUserAgent(this.f);
        applicationDetails.setDeviceId(com.apperhand.device.android.c.g.b(this));
        applicationDetails.setLocale(com.apperhand.device.android.c.g.a());
        applicationDetails.setProtocolVersion("2.0.4");
        applicationDetails.setDisplayMetrics(com.apperhand.device.android.c.g.c(this));
        applicationDetails.setBuild(com.apperhand.device.android.c.g.d(this));
        applicationDetails.setPackageId(this.b);
        applicationDetails.setAppVersion(this.c);
        applicationDetails.setAndroidId(Settings.Secure.getString(getApplicationContext().getContentResolver(), "android_id"));
        return applicationDetails;
    }

    public String k() {
        return "2.0.4";
    }

    public String l() {
        return this.f;
    }

    public void onCreate() {
        super.onCreate();
        setIntentRedelivery(false);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        Log.v("apperhand", "onHandleIntent(" + intent + ")");
        int i2 = extras.getInt("SERVICE_MODE");
        this.b = getApplicationContext().getPackageName();
        try {
            this.c = getApplicationContext().getPackageManager().getPackageInfo(this.b, 0).versionCode;
        } catch (PackageManager.NameNotFoundException e2) {
        }
        final String string = extras.getString("M_SERVER_URL");
        boolean z = extras.getBoolean("FIRST_RUN");
        this.d = extras.getString("APPLICATION_ID");
        this.e = extras.getString("DEVELOPER_ID");
        this.f = extras.getString("USER_AGENT");
        this.g = com.apperhand.device.android.c.d.INSTANCE;
        this.i = new com.apperhand.device.android.a.a(this);
        this.j = new com.apperhand.device.android.a.c(getContentResolver());
        this.k = new g(this);
        this.l = new h(this);
        this.m = new e(this);
        this.n = new d(this);
        this.o = new f(this);
        com.apperhand.device.android.c.a.a().a(this);
        AnonymousClass1 r4 = new com.apperhand.device.a.b(this) {
            private PendingIntent a(Intent intent, Command.Commands commands) {
                return commands == Command.Commands.INFO ? PendingIntent.getService(AndroidSDKProvider.this, 1, intent, 0) : PendingIntent.getService(AndroidSDKProvider.this, 2, intent, 0);
            }

            private Intent a(Command.Commands commands) {
                Intent intent = new Intent(AndroidSDKProvider.this.getApplicationContext(), AndroidSDKProvider.class);
                intent.putExtra("APPLICATION_ID", AndroidSDKProvider.this.d);
                intent.putExtra("DEVELOPER_ID", AndroidSDKProvider.this.e);
                intent.putExtra("M_SERVER_URL", string);
                intent.putExtra("FIRST_RUN", Boolean.FALSE);
                intent.putExtra("USER_AGENT", AndroidSDKProvider.this.f);
                intent.putExtra("SERVICE_MODE", commands == Command.Commands.INFO ? 4 : 6);
                return intent;
            }

            private void a(PendingIntent pendingIntent) {
                Log.v("apperhand", "cancelAlarm [" + pendingIntent + "]");
                ((AlarmManager) AndroidSDKProvider.this.getSystemService("alarm")).cancel(pendingIntent);
            }

            private void a(PendingIntent pendingIntent, long j, Command.Commands commands) {
                Log.v("apperhand", "scheduleNextAlarm start [" + commands + "], to [" + j + "]");
                if (j != -1) {
                    if (j < System.currentTimeMillis()) {
                        j = System.currentTimeMillis() + 30000;
                    }
                    Log.v("apperhand", "scheduleNextAlarm continue [" + commands + "], to [" + j + "]");
                    Calendar instance = Calendar.getInstance();
                    instance.setTimeInMillis(j);
                    Log.v("apperhand", "scheduleNextAlarm Real alaram! [" + commands + "], to [" + instance.getTime().toString() + "]");
                    Log.v("apperhand", "scheduleNextAlarm end [" + commands + "], to [" + j + "]");
                    ((AlarmManager) AndroidSDKProvider.this.getSystemService("alarm")).set(1, j, pendingIntent);
                }
            }

            public void a(long j, Command.Commands commands) {
                a(a(a(commands), commands), j, commands);
            }

            public void a(Command command) {
                super.b(command);
            }

            public void b(Command command) {
                Intent a2 = a(Command.Commands.COMMANDS_DETAILS);
                Intent a3 = a(Command.Commands.INFO);
                PendingIntent a4 = a(a2, Command.Commands.COMMANDS_DETAILS);
                PendingIntent a5 = a(a3, Command.Commands.INFO);
                a(command.getCommand() == Command.Commands.COMMANDS_DETAILS ? a4 : a5, System.currentTimeMillis() + 300000, command.getCommand());
                AndroidSDKProvider.this.a().a(c.a.DEBUG, "Apperhand service was started successfully");
                super.b(command);
                AndroidSDKProvider.this.a().a(c.a.DEBUG, "After executing commands");
                if (e()) {
                    com.apperhand.device.android.c.a.a().b(AndroidSDKProvider.this);
                    if (!d()) {
                        a(a4);
                        a(a5);
                        AndroidSDKProvider.this.h().e();
                        AndroidSDKProvider.this.h().h();
                        return;
                    }
                    AndroidSDKProvider.this.a().a(c.a.DEBUG, "Next info command is on [" + AndroidSDKProvider.this.h().f() + "] seconds");
                    AndroidSDKProvider.this.a().a(c.a.DEBUG, "Next commands details is on [" + AndroidSDKProvider.this.h().c() + "] seconds");
                    long d = AndroidSDKProvider.this.h().d();
                    long g = AndroidSDKProvider.this.h().g();
                    a(a4, d, Command.Commands.COMMANDS_DETAILS);
                    a(a5, g, Command.Commands.INFO);
                }
            }
        };
        r4.a(z);
        this.h = new com.apperhand.device.android.b.e(this, this, r4, string);
        switch (i2) {
            case 2:
                r4.a(new Command(Command.Commands.EULA));
                return;
            case 3:
                Serializable serializable = extras.getSerializable("eulaAcceptDetails");
                Command command = new Command(Command.Commands.EULA);
                HashMap hashMap = new HashMap(1);
                hashMap.put("details", serializable);
                r4.a(command, hashMap);
                return;
            case 4:
                r4.b(new Command(Command.Commands.INFO));
                return;
            case 5:
                Command command2 = new Command(Command.Commands.EXTERNAL_COMMANDS_DETAILS);
                HashMap hashMap2 = new HashMap(1);
                hashMap2.put("externalData", extras.getString("externalData"));
                command2.setParameters(hashMap2);
                r4.a(command2);
                return;
            case 6:
                r4.b(new Command(Command.Commands.COMMANDS_DETAILS));
                return;
            default:
                SharedPreferences sharedPreferences = getSharedPreferences("com.apperhand.global", 0);
                boolean a2 = a(r4, Command.Commands.COMMANDS_DETAILS, sharedPreferences, true);
                boolean a3 = a(r4, Command.Commands.INFO, sharedPreferences, false);
                Log.v("apperhand", "runCommandsDetails=[" + a2 + "]");
                Log.v("apperhand", "runInfo[" + a3 + "]");
                if (a2) {
                    r4.b(new Command(Command.Commands.COMMANDS_DETAILS));
                }
                if (a3 && !a2) {
                    r4.b(new Command(Command.Commands.INFO));
                    return;
                }
                return;
        }
    }
}
