package com.apperhand.device.a.c;

import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import com.apperhand.device.a.e.c;

public class f {
    public static a a(b bVar, Command command, a aVar) {
        switch (command.getCommand()) {
            case INFO:
                return new e(bVar, aVar, command.getId(), command);
            case EULA:
                return new c(bVar, aVar, command.getId(), command);
            case COMMANDS_DETAILS:
                return new b(bVar, aVar, command.getId(), command);
            case EXTERNAL_COMMANDS_DETAILS:
                return new d(bVar, aVar, command.getId(), command);
            default:
                aVar.a().a(c.a.DEBUG, String.format("Uknown command [command = %s] !!!", command));
                return null;
        }
    }
}
