package com.apperhand.device.a.d;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.BaseResponse;
import java.util.Map;

public interface a {
    <T extends BaseResponse> T a(Object obj, Command.Commands commands, Map<String, String> map, Class<T> cls);
}
