package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.HomepageDetailsResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.c;
import com.apperhand.device.a.b;
import java.util.HashMap;
import java.util.Map;

public class d extends b {
    private c f;

    public d(b bVar, a aVar, String str, Command.Commands commands) {
        super(bVar, aVar, str, commands);
        this.f = aVar.f();
    }

    public CommandStatus a(Map<String, Object> map) {
        boolean booleanValue = map != null ? ((Boolean) map.get("output_flag")) != null ? ((Boolean) map.get("output_flag")).booleanValue() : false : false;
        return a(Command.Commands.HOMEPAGE, booleanValue ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, booleanValue ? "Sababa" : "Didn't attemp to change the homepage", null);
    }

    public Map<String, Object> a(BaseResponse baseResponse) {
        HomepageDetailsResponse homepageDetailsResponse = (HomepageDetailsResponse) baseResponse;
        if (homepageDetailsResponse == null) {
            return null;
        }
        boolean a = this.f.a(homepageDetailsResponse.getHomepage().getNewHomepage());
        HashMap hashMap = new HashMap(1);
        hashMap.put("output_flag", Boolean.valueOf(a));
        return hashMap;
    }
}
