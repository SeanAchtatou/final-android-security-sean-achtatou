package com.apperhand.common.dto;

public class BaseDetails extends BaseDTO {
    private static final long serialVersionUID = -9076021298176698137L;
    private Status status;

    public BaseDetails() {
        this.status = null;
    }

    public BaseDetails(Status status2) {
        this.status = status2;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public String toString() {
        return "BaseDetails [status=" + this.status + "]";
    }
}
