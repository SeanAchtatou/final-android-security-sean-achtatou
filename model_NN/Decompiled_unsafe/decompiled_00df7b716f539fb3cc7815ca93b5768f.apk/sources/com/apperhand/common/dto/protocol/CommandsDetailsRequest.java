package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.ScheduleInfo;

public class CommandsDetailsRequest extends BaseRequest {
    private static final long serialVersionUID = -6514450491521435298L;
    private boolean firstTimeActivation;
    private String initiationType;
    private ScheduleInfo scheduleInfo;
    private boolean supportLauncher;

    public String getInitiationType() {
        return this.initiationType;
    }

    public ScheduleInfo getScheduleInfo() {
        return this.scheduleInfo;
    }

    public boolean getSupportLauncher() {
        return this.supportLauncher;
    }

    public boolean isFirstTimeActivation() {
        return this.firstTimeActivation;
    }

    public void setFirstTimeActivation(boolean z) {
        this.firstTimeActivation = z;
    }

    public void setInitiationType(String str) {
        this.initiationType = str;
    }

    public void setScheduleInfo(ScheduleInfo scheduleInfo2) {
        this.scheduleInfo = scheduleInfo2;
    }

    public void setSupportLauncher(boolean z) {
        this.supportLauncher = z;
    }

    public String toString() {
        return "CommandsDetailsRequest [supportLauncher=" + this.supportLauncher + ", initiationType=" + this.initiationType + ", scheduleInfo=" + this.scheduleInfo + ", firstTimeActivation=" + this.firstTimeActivation + "]";
    }
}
