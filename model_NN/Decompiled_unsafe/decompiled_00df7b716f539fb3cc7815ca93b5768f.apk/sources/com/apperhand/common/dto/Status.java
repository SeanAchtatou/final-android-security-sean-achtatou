package com.apperhand.common.dto;

public enum Status {
    ADD,
    DELETE,
    EXISTS,
    UPDATE,
    ADD_OR_UPDATE
}
