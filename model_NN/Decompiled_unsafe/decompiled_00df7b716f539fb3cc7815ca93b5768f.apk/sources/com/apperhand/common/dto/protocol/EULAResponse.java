package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.EULADetails;

public class EULAResponse extends BaseResponse {
    private static final long serialVersionUID = 4691412857273337470L;
    private EULADetails details;

    public EULADetails getDetails() {
        return this.details;
    }

    public void setDetails(EULADetails eULADetails) {
        this.details = eULADetails;
    }

    public String toString() {
        return "EULAResponse [details=" + this.details + "]";
    }
}
