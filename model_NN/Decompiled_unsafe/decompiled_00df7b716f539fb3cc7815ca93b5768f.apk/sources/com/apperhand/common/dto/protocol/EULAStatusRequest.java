package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.EULAAcceptDetails;

public class EULAStatusRequest extends BaseRequest {
    private static final long serialVersionUID = -2317367096091465630L;
    private EULAAcceptDetails details;

    public EULAAcceptDetails getDetails() {
        return this.details;
    }

    public void setDetails(EULAAcceptDetails eULAAcceptDetails) {
        this.details = eULAAcceptDetails;
    }

    public String toString() {
        return "EULAStatusRequest [details=" + this.details + ", toString()=" + super.toString() + "]";
    }
}
