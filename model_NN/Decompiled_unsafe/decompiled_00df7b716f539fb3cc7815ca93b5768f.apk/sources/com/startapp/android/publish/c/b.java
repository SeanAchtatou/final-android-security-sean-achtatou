package com.startapp.android.publish.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class b {
    private static final Map<String, Bitmap> a = new HashMap();
    private static boolean b = true;

    /* JADX WARNING: Removed duplicated region for block: B:18:0x005e A[SYNTHETIC, Splitter:B:18:0x005e] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0066 A[SYNTHETIC, Splitter:B:23:0x0066] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(android.content.Context r5, java.lang.String r6) {
        /*
            r1 = 0
            java.util.Map<java.lang.String, android.graphics.Bitmap> r0 = com.startapp.android.publish.c.b.a
            java.lang.Object r0 = r0.get(r6)
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            if (r0 == 0) goto L_0x000c
        L_0x000b:
            return r0
        L_0x000c:
            a(r5)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            r0.<init>()     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.io.File r3 = r5.getFilesDir()     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.String r3 = "/"
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x005a, all -> 0x0063 }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            android.content.res.Resources r3 = r5.getResources()     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            android.content.res.Resources r4 = r5.getResources()     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            if (r4 == 0) goto L_0x0057
            android.util.DisplayMetrics r3 = r3.getDisplayMetrics()     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            int r3 = r3.densityDpi     // Catch:{ Exception -> 0x0071, all -> 0x006e }
        L_0x0047:
            r0.setDensity(r3)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            java.util.Map<java.lang.String, android.graphics.Bitmap> r3 = com.startapp.android.publish.c.b.a     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            r3.put(r6, r0)     // Catch:{ Exception -> 0x0071, all -> 0x006e }
            if (r2 == 0) goto L_0x000b
            r2.close()     // Catch:{ IOException -> 0x0055 }
            goto L_0x000b
        L_0x0055:
            r1 = move-exception
            goto L_0x000b
        L_0x0057:
            r3 = 160(0xa0, float:2.24E-43)
            goto L_0x0047
        L_0x005a:
            r0 = move-exception
            r0 = r1
        L_0x005c:
            if (r0 == 0) goto L_0x0061
            r0.close()     // Catch:{ IOException -> 0x006a }
        L_0x0061:
            r0 = r1
            goto L_0x000b
        L_0x0063:
            r0 = move-exception
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            r1.close()     // Catch:{ IOException -> 0x006c }
        L_0x0069:
            throw r0
        L_0x006a:
            r0 = move-exception
            goto L_0x0061
        L_0x006c:
            r1 = move-exception
            goto L_0x0069
        L_0x006e:
            r0 = move-exception
            r1 = r2
            goto L_0x0064
        L_0x0071:
            r0 = move-exception
            r0 = r2
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.c.b.a(android.content.Context, java.lang.String):android.graphics.Bitmap");
    }

    private static void a(Context context) {
        String str;
        if (b) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("com.startapp.android.publish", 0);
            b = sharedPreferences.getBoolean("copyDrawables", true);
            if (b) {
                switch (context.getResources().getDisplayMetrics().densityDpi) {
                    case 120:
                        str = "drawable-ldpi.zip";
                        break;
                    case 160:
                        str = "drawable-mdpi.zip";
                        break;
                    case 240:
                        str = "drawable-hdpi.zip";
                        break;
                    case 320:
                        str = "drawable-xhdpi.zip";
                        break;
                    case 480:
                        str = "drawable-xxhdpi.zip";
                        break;
                    default:
                        str = "drawable-hdpi.zip";
                        break;
                }
                try {
                    String str2 = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
                    a(str2, str, context.getFilesDir().getPath() + "/" + str);
                    b(context, context.getFilesDir().getPath() + "/" + str);
                    a(str2, "drawable.zip", context.getFilesDir().getPath() + "/" + "drawable.zip");
                    b(context, context.getFilesDir().getPath() + "/" + "drawable.zip");
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putBoolean("copyDrawables", false);
                    edit.commit();
                } catch (Exception e) {
                }
            }
        }
    }

    private static boolean a(String str, String str2, String str3) {
        FileOutputStream fileOutputStream;
        ZipEntry zipEntry;
        InputStream inputStream = null;
        try {
            ZipFile zipFile = new ZipFile(str);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (true) {
                if (!entries.hasMoreElements()) {
                    zipEntry = null;
                    break;
                }
                zipEntry = (ZipEntry) entries.nextElement();
                if (!zipEntry.isDirectory() && zipEntry.getName().equals(str2)) {
                    break;
                }
            }
            if (zipEntry == null) {
                return false;
            }
            InputStream inputStream2 = zipFile.getInputStream(zipEntry);
            try {
                fileOutputStream = new FileOutputStream(str3);
            } catch (IOException e) {
                fileOutputStream = null;
                inputStream = inputStream2;
                try {
                    inputStream.close();
                    fileOutputStream.close();
                } catch (IOException e2) {
                }
                return false;
            }
            try {
                byte[] bArr = new byte[256];
                while (true) {
                    int read = inputStream2.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.flush();
                try {
                    inputStream2.close();
                    fileOutputStream.close();
                } catch (IOException e3) {
                }
                return true;
            } catch (IOException e4) {
                inputStream = inputStream2;
                inputStream.close();
                fileOutputStream.close();
                return false;
            }
        } catch (IOException e5) {
            fileOutputStream = null;
            inputStream.close();
            fileOutputStream.close();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0093, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0094, code lost:
        r7 = r1;
        r1 = null;
        r0 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00a0, code lost:
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f A[ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), PHI: r0 
      PHI: (r0v9 java.io.FileOutputStream) = (r0v0 java.io.FileOutputStream), (r0v0 java.io.FileOutputStream), (r0v18 java.io.FileOutputStream), (r0v18 java.io.FileOutputStream), (r0v18 java.io.FileOutputStream) binds: [B:3:0x000f, B:4:?, B:10:0x0043, B:20:0x0058, B:21:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x000f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void b(android.content.Context r8, java.lang.String r9) {
        /*
            r0 = 0
            r1 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r1]
            java.util.zip.ZipInputStream r2 = new java.util.zip.ZipInputStream     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x006f, all -> 0x007b }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x006f, all -> 0x007b }
            r1.<init>(r9)     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x006f, all -> 0x007b }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00a7, IOException -> 0x006f, all -> 0x007b }
            java.util.zip.ZipEntry r1 = r2.getNextEntry()     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x009f, all -> 0x0093 }
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0016:
            if (r0 == 0) goto L_0x0066
            java.lang.String r4 = r0.getName()     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.io.FileOutputStream r0 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.io.File r6 = r8.getFilesDir()     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.String r6 = r6.getPath()     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.String r6 = "/"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.StringBuilder r4 = r5.append(r4)     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            java.lang.String r4 = r4.toString()     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
            r0.<init>(r4)     // Catch:{ FileNotFoundException -> 0x00aa, IOException -> 0x00a5, all -> 0x009d }
        L_0x0040:
            r1 = 0
            r4 = 1024(0x400, float:1.435E-42)
            int r1 = r2.read(r3, r1, r4)     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x00a2, all -> 0x0098 }
            r4 = -1
            if (r1 <= r4) goto L_0x0058
            r4 = 0
            r0.write(r3, r4, r1)     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x00a2, all -> 0x0098 }
            goto L_0x0040
        L_0x004f:
            r1 = move-exception
            r1 = r2
        L_0x0051:
            r0.close()     // Catch:{ IOException -> 0x0089 }
        L_0x0054:
            r1.close()     // Catch:{ IOException -> 0x008b }
        L_0x0057:
            return
        L_0x0058:
            r0.close()     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x00a2, all -> 0x0098 }
            r2.closeEntry()     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x00a2, all -> 0x0098 }
            java.util.zip.ZipEntry r1 = r2.getNextEntry()     // Catch:{ FileNotFoundException -> 0x004f, IOException -> 0x00a2, all -> 0x0098 }
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0016
        L_0x0066:
            r1.close()     // Catch:{ IOException -> 0x0087 }
        L_0x0069:
            r2.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0057
        L_0x006d:
            r0 = move-exception
            goto L_0x0057
        L_0x006f:
            r1 = move-exception
            r1 = r0
            r2 = r0
        L_0x0072:
            r1.close()     // Catch:{ IOException -> 0x008d }
        L_0x0075:
            r2.close()     // Catch:{ IOException -> 0x0079 }
            goto L_0x0057
        L_0x0079:
            r0 = move-exception
            goto L_0x0057
        L_0x007b:
            r1 = move-exception
            r2 = r0
            r7 = r0
            r0 = r1
            r1 = r7
        L_0x0080:
            r1.close()     // Catch:{ IOException -> 0x008f }
        L_0x0083:
            r2.close()     // Catch:{ IOException -> 0x0091 }
        L_0x0086:
            throw r0
        L_0x0087:
            r0 = move-exception
            goto L_0x0069
        L_0x0089:
            r0 = move-exception
            goto L_0x0054
        L_0x008b:
            r0 = move-exception
            goto L_0x0057
        L_0x008d:
            r0 = move-exception
            goto L_0x0075
        L_0x008f:
            r1 = move-exception
            goto L_0x0083
        L_0x0091:
            r1 = move-exception
            goto L_0x0086
        L_0x0093:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0080
        L_0x0098:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
            goto L_0x0080
        L_0x009d:
            r0 = move-exception
            goto L_0x0080
        L_0x009f:
            r1 = move-exception
            r1 = r0
            goto L_0x0072
        L_0x00a2:
            r1 = move-exception
            r1 = r0
            goto L_0x0072
        L_0x00a5:
            r0 = move-exception
            goto L_0x0072
        L_0x00a7:
            r1 = move-exception
            r1 = r0
            goto L_0x0051
        L_0x00aa:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.c.b.b(android.content.Context, java.lang.String):void");
    }
}
