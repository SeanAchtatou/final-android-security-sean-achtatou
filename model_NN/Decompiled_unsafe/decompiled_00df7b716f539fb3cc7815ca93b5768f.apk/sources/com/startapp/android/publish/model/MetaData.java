package com.startapp.android.publish.model;

import android.content.Context;
import android.content.SharedPreferences;
import com.startapp.android.publish.c.h;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum MetaData {
    INSTANCE;
    
    public static final int DEFAULT_BG_BOTTOM = -14606047;
    public static final int DEFAULT_BG_TOP = -14606047;
    public static final int DEFAULT_ITEM_BOTTOM = -8750199;
    public static final Integer DEFAULT_ITEM_DESC_TEXT_COLOR = -1;
    public static final Set<String> DEFAULT_ITEM_DESC_TEXT_DECORATION = new HashSet();
    public static final Integer DEFAULT_ITEM_DESC_TEXT_SIZE = 14;
    public static final Integer DEFAULT_ITEM_TITLE_TEXT_COLOR = -1;
    public static final Set<String> DEFAULT_ITEM_TITLE_TEXT_DECORATION = new HashSet(Arrays.asList(TEXT_DECORATION_BOLD));
    public static final Integer DEFAULT_ITEM_TITLE_TEXT_SIZE = 18;
    public static final int DEFAULT_ITEM_TOP = -14014151;
    public static final int DEFAULT_MAX_ADS = 10;
    public static final Integer DEFAULT_POWERED_BY_BG = Integer.valueOf((int) DEFAULT_TITLE_BG);
    public static final Integer DEFAULT_POWERED_BY_TEXT_COLOR = -1;
    public static final int DEFAULT_PROBABILITY_3D = 80;
    public static final int DEFAULT_TITLE_BG = -14803426;
    public static final String DEFAULT_TITLE_CONTENT = "Free Apps of the day";
    public static final Integer DEFAULT_TITLE_LINE_COLOR = -16777216;
    public static final Integer DEFAULT_TITLE_TEXT_COLOR = -1;
    public static final Set<String> DEFAULT_TITLE_TEXT_DECORATION = new HashSet(Arrays.asList(TEXT_DECORATION_BOLD));
    public static final Integer DEFAULT_TITLE_TEXT_SIZE = 18;
    public static final String KEY_BG_BOTTOM = "backgroundGradientBottom";
    public static final String KEY_BG_TOP = "backgroundGradientTop";
    public static final String KEY_ITEM_BOTTOM = "itemGradientBottom";
    public static final String KEY_ITEM_DESC_TEXT_COLOR = "itemDescriptionTextColor";
    public static final String KEY_ITEM_DESC_TEXT_DECORATION = "itemDescriptionTextDecoration";
    public static final String KEY_ITEM_DESC_TEXT_SIZE = "itemDescriptionTextSize";
    public static final String KEY_ITEM_TITLE_TEXT_COLOR = "itemTitleTextColor";
    public static final String KEY_ITEM_TITLE_TEXT_DECORATION = "itemTitleTextDecoration";
    public static final String KEY_ITEM_TITLE_TEXT_SIZE = "itemTitleTextSize";
    public static final String KEY_ITEM_TOP = "itemGradientTop";
    public static final String KEY_MAX_ADS = "maxAds";
    public static final String KEY_POWERED_BY_BG = "poweredByBackgroundColor";
    public static final String KEY_POWERED_BY_TEXT_COLOR = "poweredByTextColor";
    public static final String KEY_PROBABILITY_3D = "probability3D";
    public static final String KEY_TITLE_BG = "titleBackgroundColor";
    public static final String KEY_TITLE_CONTENT = "titleContent";
    public static final String KEY_TITLE_LINE_COLOR = "titleLineColor";
    public static final String KEY_TITLE_TEXT_COLOR = "titleTextColor";
    public static final String KEY_TITLE_TEXT_DECORATION = "titleTextDecoration";
    public static final String KEY_TITLE_TEXT_SIZE = "titleTextSize";
    public static final String TEXT_DECORATION_BOLD = "BOLD";
    public static final String TEXT_DECORATION_ITALIC = "ITALIC";
    public static final String TEXT_DECORATION_UNDERLINE = "UNDERLINE";
    private Integer backgroundGradientBottom = null;
    private Integer backgroundGradientTop = null;
    private boolean init = false;
    private Integer itemDescriptionTextColor = null;
    private Set<String> itemDescriptionTextDecoration = null;
    private Integer itemDescriptionTextSize = null;
    private Integer itemGradientBottom = null;
    private Integer itemGradientTop = null;
    private Integer itemTitleTextColor = null;
    private Set<String> itemTitleTextDecoration = null;
    private Integer itemTitleTextSize = null;
    private Integer maxAds = null;
    private Integer poweredByBackgroundColor = null;
    private Integer poweredByTextColor = null;
    private Integer probability3D = null;
    private Integer titleBackgroundColor = null;
    private String titleContent = null;
    private Integer titleLineColor = null;
    private Integer titleTextColor = null;
    private Set<String> titleTextDecoration = null;
    private Integer titleTextSize = null;

    public int getBackgroundGradientBottom(Context context) {
        if (this.backgroundGradientBottom == null) {
            this.backgroundGradientBottom = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_BG_BOTTOM, -14606047));
        }
        return this.backgroundGradientBottom.intValue();
    }

    public int getBackgroundGradientTop(Context context) {
        if (this.backgroundGradientTop == null) {
            this.backgroundGradientTop = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_BG_TOP, -14606047));
        }
        return this.backgroundGradientTop.intValue();
    }

    public Integer getItemDescriptionTextColor(Context context) {
        if (this.itemDescriptionTextColor == null) {
            this.itemDescriptionTextColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_DESC_TEXT_COLOR, DEFAULT_ITEM_DESC_TEXT_COLOR.intValue()));
        }
        return this.itemDescriptionTextColor;
    }

    public Set<String> getItemDescriptionTextDecoration(Context context) {
        if (this.itemDescriptionTextDecoration == null) {
            this.itemDescriptionTextDecoration = h.a(context.getSharedPreferences("com.startapp.android.publish", 0).getString(KEY_ITEM_DESC_TEXT_DECORATION, h.a(DEFAULT_ITEM_DESC_TEXT_DECORATION)));
        }
        return this.itemDescriptionTextDecoration;
    }

    public Integer getItemDescriptionTextSize(Context context) {
        if (this.itemDescriptionTextSize == null) {
            this.itemDescriptionTextSize = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_DESC_TEXT_SIZE, DEFAULT_ITEM_DESC_TEXT_SIZE.intValue()));
        }
        return this.itemDescriptionTextSize;
    }

    public int getItemGradientBottom(Context context) {
        if (this.itemGradientBottom == null) {
            this.itemGradientBottom = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_BOTTOM, DEFAULT_ITEM_BOTTOM));
        }
        return this.itemGradientBottom.intValue();
    }

    public int getItemGradientTop(Context context) {
        if (this.itemGradientTop == null) {
            this.itemGradientTop = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_TOP, DEFAULT_ITEM_TOP));
        }
        return this.itemGradientTop.intValue();
    }

    public Integer getItemTitleTextColor(Context context) {
        if (this.itemTitleTextColor == null) {
            this.itemTitleTextColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_TITLE_TEXT_COLOR, DEFAULT_ITEM_TITLE_TEXT_COLOR.intValue()));
        }
        return this.itemTitleTextColor;
    }

    public Set<String> getItemTitleTextDecoration(Context context) {
        if (this.itemTitleTextDecoration == null) {
            this.itemTitleTextDecoration = h.a(context.getSharedPreferences("com.startapp.android.publish", 0).getString(KEY_ITEM_TITLE_TEXT_DECORATION, h.a(DEFAULT_ITEM_TITLE_TEXT_DECORATION)));
        }
        return this.itemTitleTextDecoration;
    }

    public Integer getItemTitleTextSize(Context context) {
        if (this.itemTitleTextSize == null) {
            this.itemTitleTextSize = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_ITEM_TITLE_TEXT_SIZE, DEFAULT_ITEM_TITLE_TEXT_SIZE.intValue()));
        }
        return this.itemTitleTextSize;
    }

    public int getMaxAds(Context context) {
        if (this.maxAds == null) {
            this.maxAds = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_MAX_ADS, 10));
        }
        return this.maxAds.intValue();
    }

    public Integer getPoweredByBackgroundColor(Context context) {
        if (this.poweredByBackgroundColor == null) {
            this.poweredByBackgroundColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_POWERED_BY_BG, DEFAULT_POWERED_BY_BG.intValue()));
        }
        return this.poweredByBackgroundColor;
    }

    public Integer getPoweredByTextColor(Context context) {
        if (this.poweredByTextColor == null) {
            this.poweredByTextColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_POWERED_BY_TEXT_COLOR, DEFAULT_POWERED_BY_TEXT_COLOR.intValue()));
        }
        return this.poweredByTextColor;
    }

    public int getProbability3D(Context context) {
        if (this.probability3D == null) {
            this.probability3D = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_PROBABILITY_3D, 80));
        }
        return this.probability3D.intValue();
    }

    public Integer getTitleBackgroundColor(Context context) {
        if (this.titleBackgroundColor == null) {
            this.titleBackgroundColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_TITLE_BG, DEFAULT_TITLE_BG));
        }
        return this.titleBackgroundColor;
    }

    public String getTitleContent(Context context) {
        if (this.titleContent == null) {
            this.titleContent = context.getSharedPreferences("com.startapp.android.publish", 0).getString(KEY_TITLE_CONTENT, DEFAULT_TITLE_CONTENT);
        }
        return this.titleContent;
    }

    public Integer getTitleLineColor(Context context) {
        if (this.titleLineColor == null) {
            this.titleLineColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_TITLE_LINE_COLOR, DEFAULT_TITLE_LINE_COLOR.intValue()));
        }
        return this.titleLineColor;
    }

    public Integer getTitleTextColor(Context context) {
        if (this.titleTextColor == null) {
            this.titleTextColor = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_TITLE_TEXT_COLOR, DEFAULT_TITLE_TEXT_COLOR.intValue()));
        }
        return this.titleTextColor;
    }

    public Set<String> getTitleTextDecoration(Context context) {
        if (this.titleTextDecoration == null) {
            this.titleTextDecoration = h.a(context.getSharedPreferences("com.startapp.android.publish", 0).getString(KEY_TITLE_TEXT_DECORATION, h.a(DEFAULT_TITLE_TEXT_DECORATION)));
        }
        return this.titleTextDecoration;
    }

    public Integer getTitleTextSize(Context context) {
        if (this.titleTextSize == null) {
            this.titleTextSize = Integer.valueOf(context.getSharedPreferences("com.startapp.android.publish", 0).getInt(KEY_TITLE_TEXT_SIZE, DEFAULT_TITLE_TEXT_SIZE.intValue()));
        }
        return this.titleTextSize;
    }

    public synchronized void init(Context context, MetaDataResponse metaDataResponse) {
        if (!isInit()) {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.startapp.android.publish", 0).edit();
            edit.putInt(KEY_PROBABILITY_3D, metaDataResponse.getProbability3D().intValue());
            edit.putInt(KEY_BG_TOP, metaDataResponse.getBackgroundGradientTop().intValue());
            edit.putInt(KEY_BG_BOTTOM, metaDataResponse.getBackgroundGradientBottom().intValue());
            edit.putInt(KEY_MAX_ADS, metaDataResponse.getMaxAds().intValue());
            edit.putInt(KEY_TITLE_BG, metaDataResponse.getTitleBackgroundColor().intValue());
            edit.putString(KEY_TITLE_CONTENT, metaDataResponse.getTitleContent());
            edit.putInt(KEY_TITLE_TEXT_COLOR, metaDataResponse.getTitleTextColor().intValue());
            edit.putInt(KEY_TITLE_TEXT_SIZE, metaDataResponse.getTitleTextSize().intValue());
            edit.putString(KEY_TITLE_TEXT_DECORATION, h.a(metaDataResponse.getTitleTextDecoration()));
            edit.putInt(KEY_TITLE_LINE_COLOR, metaDataResponse.getTitleLineColor().intValue());
            edit.putInt(KEY_ITEM_TOP, metaDataResponse.getItemGradientTop().intValue());
            edit.putInt(KEY_ITEM_BOTTOM, metaDataResponse.getItemGradientBottom().intValue());
            edit.putInt(KEY_ITEM_TITLE_TEXT_COLOR, metaDataResponse.getItemTitleTextColor().intValue());
            edit.putInt(KEY_ITEM_TITLE_TEXT_SIZE, metaDataResponse.getItemTitleTextSize().intValue());
            edit.putString(KEY_ITEM_TITLE_TEXT_DECORATION, h.a(metaDataResponse.getItemTitleTextDecoration()));
            edit.putInt(KEY_ITEM_DESC_TEXT_COLOR, metaDataResponse.getItemDescriptionTextColor().intValue());
            edit.putInt(KEY_ITEM_DESC_TEXT_SIZE, metaDataResponse.getItemDescriptionTextSize().intValue());
            edit.putString(KEY_ITEM_DESC_TEXT_DECORATION, h.a(metaDataResponse.getItemDescriptionTextDecoration()));
            edit.putInt(KEY_POWERED_BY_BG, metaDataResponse.getPoweredByBackgroundColor().intValue());
            edit.putInt(KEY_POWERED_BY_TEXT_COLOR, metaDataResponse.getPoweredByTextColor().intValue());
            edit.commit();
            this.init = true;
        }
    }

    public synchronized boolean isInit() {
        return this.init;
    }
}
