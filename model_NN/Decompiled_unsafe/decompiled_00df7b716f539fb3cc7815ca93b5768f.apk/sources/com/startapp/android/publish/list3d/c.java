package com.startapp.android.publish.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.startapp.android.publish.c.f;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.Stack;

public class c {
    HashSet<String> a = new HashSet<>();
    Hashtable<String, Bitmap> b = new Hashtable<>();
    Set<String> c = new HashSet();
    i d;
    int e = 0;
    Stack<b> f = new Stack<>();

    private class a extends AsyncTask<Object, Void, Bitmap> {
        String a;
        int b;

        private a() {
            this.b = -1;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Bitmap doInBackground(Object... objArr) {
            this.b = ((Integer) objArr[0]).intValue();
            this.a = (String) objArr[1];
            return a.a((String) objArr[2]);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            c cVar = c.this;
            cVar.e--;
            if (bitmap != null) {
                c.this.b.put(this.a, bitmap);
                if (c.this.d != null) {
                    c.this.d.a(this.b);
                }
                c.this.b();
            }
        }
    }

    class b {
        String a;
        String b;

        public b(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    public Bitmap a(int i, String str, String str2) {
        Bitmap bitmap = this.b.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        if (!this.c.contains(str)) {
            this.c.add(str);
            if (this.e >= 15) {
                this.f.push(new b(str, str2));
            } else {
                this.e++;
                new a().execute(Integer.valueOf(i), str, str2);
            }
        }
        return null;
    }

    public void a() {
        this.c.clear();
        this.e = 0;
        this.f.clear();
    }

    public void a(final Context context, String str, final String str2) {
        if (!this.a.contains(str)) {
            this.a.add(str);
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    try {
                        com.startapp.android.publish.b.b.a(context, str2, null);
                    } catch (f e) {
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
    }

    public void a(i iVar) {
        this.d = iVar;
        a();
    }

    public void b() {
        if (!this.f.isEmpty()) {
            b pop = this.f.pop();
            new a().execute(pop.a, pop.b);
        }
    }
}
