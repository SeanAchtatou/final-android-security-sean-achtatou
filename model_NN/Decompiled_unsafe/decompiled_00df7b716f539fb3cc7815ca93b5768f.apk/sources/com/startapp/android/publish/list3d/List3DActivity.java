package com.startapp.android.publish.list3d;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.android.publish.c.b;
import com.startapp.android.publish.c.d;
import com.startapp.android.publish.c.g;
import com.startapp.android.publish.model.MetaData;
import java.util.List;

public class List3DActivity extends Activity implements i {
    private e a;
    /* access modifiers changed from: private */
    public ProgressDialog b = null;
    /* access modifiers changed from: private */
    public WebView c = null;

    private class a extends WebViewClient {
        private a() {
        }

        public void onPageFinished(WebView webView, String str) {
            d.a(2, "MyWebViewClient::onPageFinished - [" + str + "]");
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            d.a(2, "MyWebViewClient::onPageStarted - [" + str + "]");
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            d.a(2, "MyWebViewClient::onReceivedError - [" + str + "], [" + str2 + "]");
            if (List3DActivity.this.b != null) {
                List3DActivity.this.b.dismiss();
            }
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            d.a(2, "MyWebViewClient::shouldOverrideUrlLoading - [" + str + "]");
            String lowerCase = str.toLowerCase();
            if (!(lowerCase.startsWith("market") || lowerCase.startsWith("http://play.google.com") || lowerCase.startsWith("https://play.google.com"))) {
                return false;
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(344457216);
            List3DActivity.this.startActivity(intent);
            if (List3DActivity.this.b != null) {
                List3DActivity.this.b.dismiss();
            }
            List3DActivity.this.finish();
            return true;
        }
    }

    public void a(int i) {
        View childAt = this.a.getChildAt(i - this.a.getFirstItemPosition());
        if (childAt != null) {
            g gVar = (g) childAt.getTag();
            if (h.INSTANCE != null && h.INSTANCE.b() != null && i < h.INSTANCE.b().size()) {
                f fVar = h.INSTANCE.b().get(i);
                gVar.b().setImageBitmap(h.INSTANCE.a(i, fVar.a(), fVar.f()));
                gVar.b().requestLayout();
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        int backgroundGradientTop = MetaData.INSTANCE.getBackgroundGradientTop(this);
        int backgroundGradientBottom = MetaData.INSTANCE.getBackgroundGradientBottom(this);
        this.a = new e(this, null);
        this.a.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{backgroundGradientTop, backgroundGradientBottom}));
        final List<f> b2 = h.INSTANCE.b();
        if (b2 == null) {
            finish();
            return;
        }
        d dVar = new d(this, b2);
        h.INSTANCE.a(this);
        this.a.setAdapter(dVar);
        this.a.setDynamics(new j(0.9f, 0.6f));
        this.a.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                String b2 = ((f) b2.get(i)).b();
                boolean h = ((f) b2.get(i)).h();
                if (TextUtils.isEmpty(b2)) {
                    return;
                }
                if (!h) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b2));
                    intent.addFlags(335544320);
                    List3DActivity.this.startActivity(intent);
                    List3DActivity.this.finish();
                    return;
                }
                WebView unused = List3DActivity.this.c = new WebView(List3DActivity.this.getApplicationContext());
                List3DActivity.this.c.getSettings().setJavaScriptEnabled(true);
                List3DActivity.this.c.setWebChromeClient(new WebChromeClient());
                List3DActivity.this.c.setWebViewClient(new a());
                List3DActivity.this.c.loadUrl(b2);
                ProgressDialog unused2 = List3DActivity.this.b = ProgressDialog.show(List3DActivity.this, null, "Loading....", false, false, new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        List3DActivity.this.c.stopLoading();
                    }
                });
            }
        });
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        relativeLayout.setBackgroundColor(MetaData.INSTANCE.getTitleBackgroundColor(this).intValue());
        linearLayout.addView(relativeLayout);
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(13);
        textView.setLayoutParams(layoutParams2);
        textView.setPadding(0, g.a(this, 2), 0, g.a(this, 5));
        textView.setTextColor(MetaData.INSTANCE.getTitleTextColor(this).intValue());
        textView.setTextSize((float) MetaData.INSTANCE.getTitleTextSize(this).intValue());
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setText(MetaData.INSTANCE.getTitleContent(this));
        textView.setShadowLayer(2.5f, -2.0f, 2.0f, -11513776);
        g.a(textView, MetaData.INSTANCE.getTitleTextDecoration(this));
        relativeLayout.addView(textView);
        ImageButton imageButton = new ImageButton(this, null, 16973839);
        imageButton.setImageBitmap(Bitmap.createScaledBitmap(b.a(this, "close_button.png"), g.a(this, 36), g.a(this, 36), true));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(11);
        layoutParams3.addRule(15);
        imageButton.setLayoutParams(layoutParams3);
        imageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                List3DActivity.this.finish();
            }
        });
        relativeLayout.addView(imageButton);
        View view = new View(this);
        view.setLayoutParams(new LinearLayout.LayoutParams(-1, g.a(this, 2)));
        view.setBackgroundColor(MetaData.INSTANCE.getTitleLineColor(this).intValue());
        linearLayout.addView(view);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, 0);
        layoutParams4.weight = 1.0f;
        this.a.setLayoutParams(layoutParams4);
        linearLayout.addView(this.a);
        LinearLayout linearLayout2 = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams5.gravity = 80;
        linearLayout2.setLayoutParams(layoutParams5);
        linearLayout2.setBackgroundColor(MetaData.INSTANCE.getPoweredByBackgroundColor(this).intValue());
        linearLayout2.setGravity(17);
        linearLayout.addView(linearLayout2);
        TextView textView2 = new TextView(this);
        textView2.setTextColor(MetaData.INSTANCE.getPoweredByTextColor(this).intValue());
        textView2.setPadding(0, g.a(this, 2), 0, g.a(this, 5));
        textView2.setText("Powered By ");
        textView2.setTextSize(16.0f);
        linearLayout2.addView(textView2);
        ImageView imageView = new ImageView(this);
        imageView.setImageBitmap(b.a(this, "logo.png"));
        linearLayout2.addView(imageView);
        setContentView(linearLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.b != null) {
            this.b.dismiss();
        }
        if (this.c != null) {
            this.c.stopLoading();
        }
        super.onDestroy();
    }
}
