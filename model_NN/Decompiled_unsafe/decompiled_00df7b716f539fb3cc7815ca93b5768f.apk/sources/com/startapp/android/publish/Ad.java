package com.startapp.android.publish;

import android.content.Context;
import android.os.Handler;
import com.startapp.android.publish.c.h;
import com.startapp.android.publish.model.AdPreferences;

public abstract class Ad {
    protected Context context;
    protected String errorMessage = null;
    protected Object extraData = null;
    private AdState state = AdState.UN_INITIALIZED;

    public enum AdState {
        UN_INITIALIZED,
        PROCESSING,
        READY
    }

    public Ad(Context context2) {
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void fillMissingAdPreferences(AdPreferences adPreferences) {
        String a = h.a(this.context, "com.startapp.android.DEV_ID");
        String a2 = h.a(this.context, "com.startapp.android.APP_ID");
        if (adPreferences.getPublisherId() == null) {
            adPreferences.setPublisherId(a);
        }
        if (adPreferences.getProductId() == null) {
            adPreferences.setProductId(a2);
        }
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public Object getExtraData() {
        return this.extraData;
    }

    public AdState getState() {
        return this.state;
    }

    public boolean isReady() {
        return this.state == AdState.READY;
    }

    public boolean load() {
        return load(new AdPreferences(), null);
    }

    public boolean load(AdEventListener adEventListener) {
        return load(new AdPreferences(), adEventListener);
    }

    public boolean load(AdPreferences adPreferences) {
        return load(adPreferences, null);
    }

    public boolean load(AdPreferences adPreferences, final AdEventListener adEventListener) {
        boolean z;
        fillMissingAdPreferences(adPreferences);
        String str = "";
        if (adPreferences.getPublisherId() == null || "".equals(adPreferences.getPublisherId()) || adPreferences.getProductId() == null || "".equals(adPreferences.getProductId())) {
            str = "publisher ID and/or product ID were not set.";
            z = true;
        } else {
            z = false;
        }
        if (this.state != AdState.UN_INITIALIZED) {
            str = "load() was already called.";
            z = true;
        }
        if (!h.a(this.context)) {
            str = "network not available.";
            z = true;
        }
        if (z) {
            setErrorMessage("Ad wasn't loaded: " + str);
            if (adEventListener == null) {
                return false;
            }
            new Handler().post(new Runnable() {
                public void run() {
                    adEventListener.onFailedToReceiveAd(Ad.this);
                }
            });
            return false;
        }
        setState(AdState.PROCESSING);
        return true;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setExtraData(Object obj) {
        this.extraData = obj;
    }

    public void setState(AdState adState) {
        this.state = adState;
    }
}
