package com.startapp.android.publish.c;

public class f extends Exception {
    private boolean a;

    public f() {
        this.a = false;
    }

    public f(String str, Throwable th) {
        this(str, th, false);
    }

    public f(String str, Throwable th, boolean z) {
        super(str, th);
        this.a = false;
        this.a = z;
    }

    public boolean a() {
        return this.a;
    }
}
