package com.startapp.android.publish.model;

import java.util.List;

public interface NameValueSerializer {
    List<NameValueObject> getNameValueMap();
}
