package com.startapp.android.publish.b;

import android.content.Context;
import com.startapp.android.publish.c.a;
import com.startapp.android.publish.c.e;
import com.startapp.android.publish.c.f;
import com.startapp.android.publish.model.BaseRequest;
import com.startapp.android.publish.model.BaseResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class b {
    public static <T extends BaseResponse> T a(Context context, String str, BaseRequest baseRequest, Map<String, String> map, Class<T> cls) {
        StringBuilder sb = new StringBuilder();
        a(context, str, baseRequest, map, sb);
        return (BaseResponse) a.INSTANCE.a(sb.toString(), cls);
    }

    public static String a(Context context, String str, BaseRequest baseRequest, Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        a(context, str, baseRequest, map, sb);
        return sb.toString();
    }

    private static Map<String, String> a(Context context, Map<String, String> map) {
        if (map == null) {
            map = new HashMap<>();
        }
        String a = a.a(context);
        try {
            a = URLEncoder.encode(a, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        map.put("device-id", a);
        map.put("Accept-Language", Locale.getDefault().toString());
        return map;
    }

    private static void a(Context context, String str, BaseRequest baseRequest, Map<String, String> map, StringBuilder sb) {
        if (baseRequest != null) {
            str = str + baseRequest.getRequestString();
        }
        Map<String, String> a = a(context, map);
        boolean z = false;
        int i = 1;
        while (!z) {
            try {
                e.a(str, a, sb);
                z = true;
            } catch (f e) {
                if (!e.a() || i >= 3) {
                    throw e;
                }
                i++;
            }
        }
    }

    public static boolean a(Context context, String str, Map<String, String> map) {
        a(context, str, (BaseRequest) null, map, (StringBuilder) null);
        return true;
    }
}
