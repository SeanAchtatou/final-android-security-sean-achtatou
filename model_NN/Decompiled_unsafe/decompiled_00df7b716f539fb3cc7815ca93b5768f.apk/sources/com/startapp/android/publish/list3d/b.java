package com.startapp.android.publish.list3d;

public abstract class b {
    protected float a;
    protected float b;
    protected float c = Float.MAX_VALUE;
    protected float d = -3.4028235E38f;
    protected long e = 0;

    public float a() {
        return this.a;
    }

    public void a(float f) {
        this.c = f;
    }

    public void a(float f, float f2, long j) {
        this.b = f2;
        this.a = f;
        this.e = j;
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    public void a(long j) {
        int i = 50;
        int i2 = (int) (j - this.e);
        if (i2 <= 50) {
            i = i2;
        }
        a(i);
        this.e = j;
    }

    public boolean a(float f, float f2) {
        return ((Math.abs(this.b) > f ? 1 : (Math.abs(this.b) == f ? 0 : -1)) < 0) && (((this.a - f2) > this.c ? 1 : ((this.a - f2) == this.c ? 0 : -1)) < 0 && ((this.a + f2) > this.d ? 1 : ((this.a + f2) == this.d ? 0 : -1)) > 0);
    }

    public float b() {
        return this.b;
    }

    public void b(float f) {
        this.d = f;
    }

    /* access modifiers changed from: protected */
    public float c() {
        if (this.a > this.c) {
            return this.c - this.a;
        }
        if (this.a < this.d) {
            return this.d - this.a;
        }
        return 0.0f;
    }
}
