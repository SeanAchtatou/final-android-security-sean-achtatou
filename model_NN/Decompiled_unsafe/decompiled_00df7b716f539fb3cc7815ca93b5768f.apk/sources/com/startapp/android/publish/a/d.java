package com.startapp.android.publish.a;

import android.content.Context;
import android.os.AsyncTask;
import com.startapp.android.publish.b.b;
import com.startapp.android.publish.c.f;
import com.startapp.android.publish.model.AdPreferences;
import com.startapp.android.publish.model.MetaData;
import com.startapp.android.publish.model.MetaDataRequest;
import com.startapp.android.publish.model.MetaDataResponse;
import java.util.Map;

public class d extends AsyncTask<Void, Void, Boolean> {
    private final Context a;
    private final AdPreferences b;
    private final e c;
    private MetaDataResponse d = null;

    public d(Context context, AdPreferences adPreferences, e eVar) {
        this.a = context;
        this.b = adPreferences;
        this.c = eVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        MetaDataRequest metaDataRequest = new MetaDataRequest();
        metaDataRequest.fillApplicationDetails(this.a, this.b);
        try {
            this.d = (MetaDataResponse) b.a(this.a, "http://www.startappexchange.com/1.2/getadsmetadata", metaDataRequest, (Map<String, String>) null, MetaDataResponse.class);
            return Boolean.TRUE;
        } catch (f e) {
            com.startapp.android.publish.c.d.a(6, "Unable to handle GetHtmlAdService command!!!!", e);
            return Boolean.FALSE;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        if (bool.booleanValue()) {
            MetaData.INSTANCE.init(this.a, this.d);
        }
        if (this.c != null) {
            this.c.a();
        }
    }
}
