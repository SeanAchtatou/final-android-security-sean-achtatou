package com.startapp.android.publish.model;

import java.util.HashSet;
import java.util.Set;

public class AdPreferences {
    private int age = 0;
    private Set<String> categories = null;
    private Set<String> categoriesExclude = null;
    private String gender = null;
    private String keywords = null;
    private String productId;
    private String publisherId;
    private boolean testMode = false;

    public enum Placement {
        INAPP_EXIT,
        INAPP_3DAPPWALL
    }

    public AdPreferences() {
    }

    public AdPreferences(AdPreferences adPreferences) {
        this.publisherId = adPreferences.publisherId;
        this.productId = adPreferences.productId;
        this.testMode = adPreferences.testMode;
        this.gender = adPreferences.gender;
        this.age = adPreferences.age;
        this.keywords = adPreferences.keywords;
        this.categories = adPreferences.categories;
        this.categoriesExclude = adPreferences.categoriesExclude;
    }

    @Deprecated
    public AdPreferences(String str, String str2) {
        this.publisherId = str;
        this.productId = str2;
    }

    public void addCategory(String str) {
        if (this.categories == null) {
            this.categories = new HashSet();
        }
        this.categories.add(str);
    }

    public void addCategoryExclude(String str) {
        if (this.categoriesExclude == null) {
            this.categoriesExclude = new HashSet();
        }
        this.categoriesExclude.add(str);
    }

    public int getAge() {
        return this.age;
    }

    public Set<String> getCategories() {
        return this.categories;
    }

    public Set<String> getCategoriesExclude() {
        return this.categoriesExclude;
    }

    public String getGender() {
        return this.gender;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    @Deprecated
    public void setProductId(String str) {
        this.productId = str;
    }

    @Deprecated
    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public void setTestMode(boolean z) {
        this.testMode = z;
    }

    public String toString() {
        return "AdPreferences [publisherId=" + this.publisherId + ", productId=" + this.productId + ", testMode=" + this.testMode + ", gender=" + this.gender + ", age=" + this.age + ", keywords=" + this.keywords + ", categories=" + this.categories + ", categoriesExclude=" + this.categoriesExclude + "]";
    }
}
