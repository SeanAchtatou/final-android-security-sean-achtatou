package com.startapp.android.publish.c;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import com.startapp.android.publish.model.NameValueObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class h {
    public static String a(Context context, String str) {
        Bundle bundle;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (Exception e) {
            bundle = null;
        }
        if (bundle == null) {
            return null;
        }
        Object obj = bundle.get(str);
        String obj2 = obj != null ? obj.toString() : null;
        if (obj2 == null || obj2.trim().length() == 0) {
            return null;
        }
        return obj2;
    }

    public static String a(String str, String str2, String str3) {
        int indexOf;
        int indexOf2;
        if (str == null || str2 == null || str3 == null || (indexOf = str.indexOf(str2)) == -1 || (indexOf2 = str.indexOf(str3, str2.length() + indexOf)) == -1) {
            return null;
        }
        return str.substring(str2.length() + indexOf, indexOf2);
    }

    public static String a(Set<String> set) {
        StringBuilder sb = new StringBuilder(set.size());
        for (String append : set) {
            sb.append(append).append(",");
        }
        return sb.toString();
    }

    public static Set<String> a(String str) {
        return new HashSet(Arrays.asList(str.split(",")));
    }

    public static void a(List<NameValueObject> list, String str, String str2, boolean z) {
        if (z && str2 == null) {
            throw new f("Required key: [" + str + "] is missing", null);
        } else if (str2 != null) {
            try {
                NameValueObject nameValueObject = new NameValueObject();
                nameValueObject.setName(str);
                nameValueObject.setValue(URLEncoder.encode(str2, "UTF-8"));
                list.add(nameValueObject);
            } catch (UnsupportedEncodingException e) {
                if (z) {
                    throw new f("failed encoding value: [" + str2 + "]", e);
                }
            }
        }
    }

    public static void a(List<NameValueObject> list, String str, Set<String> set, boolean z) {
        if (z && set == null) {
            throw new f("Required key: [" + str + "] is missing", null);
        } else if (set != null) {
            NameValueObject nameValueObject = new NameValueObject();
            nameValueObject.setName(str);
            HashSet hashSet = new HashSet();
            for (String encode : set) {
                try {
                    hashSet.add(URLEncoder.encode(encode, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                }
            }
            if (!z || hashSet.size() != 0) {
                nameValueObject.setValueSet(hashSet);
                list.add(nameValueObject);
                return;
            }
            throw new f("failed encoding value: [" + set + "]", null);
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
