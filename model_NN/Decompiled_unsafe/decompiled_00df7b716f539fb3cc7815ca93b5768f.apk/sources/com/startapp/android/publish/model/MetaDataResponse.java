package com.startapp.android.publish.model;

import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MetaDataResponse extends BaseResponse {
    private Integer backgroundGradientBottom = null;
    private Integer backgroundGradientTop = null;
    private Integer itemDescriptionTextColor = null;
    private Set<String> itemDescriptionTextDecoration = null;
    private Integer itemDescriptionTextSize = null;
    private Integer itemGradientBottom = null;
    private Integer itemGradientTop = null;
    private Integer itemTitleTextColor = null;
    private Set<String> itemTitleTextDecoration = null;
    private Integer itemTitleTextSize = null;
    private Integer maxAds = null;
    private Integer poweredByBackgroundColor = null;
    private Integer poweredByTextColor = null;
    private Integer probability3D = null;
    private Integer titleBackgroundColor = null;
    private String titleContent = null;
    private Integer titleLineColor = null;
    private Integer titleTextColor = null;
    private Set<String> titleTextDecoration = null;
    private Integer titleTextSize = null;

    private Set<String> jsonArrayToSet(JSONArray jSONArray, Set<String> set) {
        if (jSONArray != null) {
            set = new HashSet<>();
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    set.add(jSONArray.getString(i).intern());
                } catch (JSONException e) {
                }
            }
        }
        return set;
    }

    public void fromJson(JSONObject jSONObject) {
        super.fromJson(jSONObject);
        this.probability3D = Integer.valueOf(jSONObject.optInt(MetaData.KEY_PROBABILITY_3D, 80));
        this.backgroundGradientTop = Integer.valueOf(jSONObject.optInt(MetaData.KEY_BG_TOP, -14606047));
        this.backgroundGradientBottom = Integer.valueOf(jSONObject.optInt(MetaData.KEY_BG_BOTTOM, -14606047));
        this.maxAds = Integer.valueOf(jSONObject.optInt(MetaData.KEY_MAX_ADS, 10));
        this.titleBackgroundColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_TITLE_BG, MetaData.DEFAULT_TITLE_BG));
        this.titleContent = jSONObject.optString(MetaData.KEY_TITLE_CONTENT, MetaData.DEFAULT_TITLE_CONTENT);
        this.titleTextColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_TITLE_TEXT_COLOR, MetaData.DEFAULT_TITLE_TEXT_COLOR.intValue()));
        this.titleTextSize = Integer.valueOf(jSONObject.optInt(MetaData.KEY_TITLE_TEXT_SIZE, MetaData.DEFAULT_TITLE_TEXT_SIZE.intValue()));
        this.titleTextDecoration = jsonArrayToSet(jSONObject.optJSONArray(MetaData.KEY_TITLE_TEXT_DECORATION), MetaData.DEFAULT_TITLE_TEXT_DECORATION);
        this.titleLineColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_TITLE_LINE_COLOR, MetaData.DEFAULT_TITLE_LINE_COLOR.intValue()));
        this.itemGradientTop = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_TOP, MetaData.DEFAULT_ITEM_TOP));
        this.itemGradientBottom = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_BOTTOM, MetaData.DEFAULT_ITEM_BOTTOM));
        this.itemTitleTextColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_TITLE_TEXT_COLOR, MetaData.DEFAULT_ITEM_TITLE_TEXT_COLOR.intValue()));
        this.itemTitleTextSize = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_TITLE_TEXT_SIZE, MetaData.DEFAULT_ITEM_TITLE_TEXT_SIZE.intValue()));
        this.itemTitleTextDecoration = jsonArrayToSet(jSONObject.optJSONArray(MetaData.KEY_ITEM_TITLE_TEXT_DECORATION), MetaData.DEFAULT_ITEM_TITLE_TEXT_DECORATION);
        this.itemDescriptionTextColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_DESC_TEXT_COLOR, MetaData.DEFAULT_ITEM_DESC_TEXT_COLOR.intValue()));
        this.itemDescriptionTextSize = Integer.valueOf(jSONObject.optInt(MetaData.KEY_ITEM_DESC_TEXT_SIZE, MetaData.DEFAULT_ITEM_DESC_TEXT_SIZE.intValue()));
        this.itemDescriptionTextDecoration = jsonArrayToSet(jSONObject.optJSONArray(MetaData.KEY_ITEM_DESC_TEXT_DECORATION), MetaData.DEFAULT_ITEM_DESC_TEXT_DECORATION);
        this.poweredByBackgroundColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_POWERED_BY_BG, MetaData.DEFAULT_POWERED_BY_BG.intValue()));
        this.poweredByTextColor = Integer.valueOf(jSONObject.optInt(MetaData.KEY_POWERED_BY_TEXT_COLOR, MetaData.DEFAULT_POWERED_BY_TEXT_COLOR.intValue()));
    }

    public Integer getBackgroundGradientBottom() {
        return this.backgroundGradientBottom;
    }

    public Integer getBackgroundGradientTop() {
        return this.backgroundGradientTop;
    }

    public Integer getItemDescriptionTextColor() {
        return this.itemDescriptionTextColor;
    }

    public Set<String> getItemDescriptionTextDecoration() {
        return this.itemDescriptionTextDecoration;
    }

    public Integer getItemDescriptionTextSize() {
        return this.itemDescriptionTextSize;
    }

    public Integer getItemGradientBottom() {
        return this.itemGradientBottom;
    }

    public Integer getItemGradientTop() {
        return this.itemGradientTop;
    }

    public Integer getItemTitleTextColor() {
        return this.itemTitleTextColor;
    }

    public Set<String> getItemTitleTextDecoration() {
        return this.itemTitleTextDecoration;
    }

    public Integer getItemTitleTextSize() {
        return this.itemTitleTextSize;
    }

    public Integer getMaxAds() {
        return this.maxAds;
    }

    public Integer getPoweredByBackgroundColor() {
        return this.poweredByBackgroundColor;
    }

    public Integer getPoweredByTextColor() {
        return this.poweredByTextColor;
    }

    public Integer getProbability3D() {
        return this.probability3D;
    }

    public Integer getTitleBackgroundColor() {
        return this.titleBackgroundColor;
    }

    public String getTitleContent() {
        return this.titleContent;
    }

    public Integer getTitleLineColor() {
        return this.titleLineColor;
    }

    public Integer getTitleTextColor() {
        return this.titleTextColor;
    }

    public Set<String> getTitleTextDecoration() {
        return this.titleTextDecoration;
    }

    public Integer getTitleTextSize() {
        return this.titleTextSize;
    }

    public void setBackgroundGradientBottom(Integer num) {
        this.backgroundGradientBottom = num;
    }

    public void setBackgroundGradientTop(Integer num) {
        this.backgroundGradientTop = num;
    }

    public void setItemDescriptionTextColor(Integer num) {
        this.itemDescriptionTextColor = num;
    }

    public void setItemDescriptionTextDecoration(Set<String> set) {
        this.itemDescriptionTextDecoration = set;
    }

    public void setItemDescriptionTextSize(Integer num) {
        this.itemDescriptionTextSize = num;
    }

    public void setItemGradientBottom(Integer num) {
        this.itemGradientBottom = num;
    }

    public void setItemGradientTop(Integer num) {
        this.itemGradientTop = num;
    }

    public void setItemTitleTextColor(Integer num) {
        this.itemTitleTextColor = num;
    }

    public void setItemTitleTextDecoration(Set<String> set) {
        this.itemTitleTextDecoration = set;
    }

    public void setItemTitleTextSize(Integer num) {
        this.itemTitleTextSize = num;
    }

    public void setMaxAds(Integer num) {
        this.maxAds = num;
    }

    public void setPoweredByBackgroundColor(Integer num) {
        this.poweredByBackgroundColor = num;
    }

    public void setPoweredByTextColor(Integer num) {
        this.poweredByTextColor = num;
    }

    public void setProbability3D(Integer num) {
        this.probability3D = num;
    }

    public void setTitleBackgroundColor(Integer num) {
        this.titleBackgroundColor = num;
    }

    public void setTitleContent(String str) {
        this.titleContent = str;
    }

    public void setTitleLineColor(Integer num) {
        this.titleLineColor = num;
    }

    public void setTitleTextColor(Integer num) {
        this.titleTextColor = num;
    }

    public void setTitleTextDecoration(Set<String> set) {
        this.titleTextDecoration = set;
    }

    public void setTitleTextSize(Integer num) {
        this.titleTextSize = num;
    }
}
