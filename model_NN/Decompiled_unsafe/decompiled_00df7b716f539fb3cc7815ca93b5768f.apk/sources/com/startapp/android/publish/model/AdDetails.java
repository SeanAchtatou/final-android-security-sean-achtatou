package com.startapp.android.publish.model;

import org.json.JSONObject;

public class AdDetails implements JsonDeserializer {
    private String adId;
    private String clickUrl;
    private String description;
    private String imageUrl;
    private float rating;
    private boolean smartRedirect;
    private String title;
    private String trackingUrl;

    public void fromJson(JSONObject jSONObject) {
        this.adId = jSONObject.optString("adId", null);
        this.clickUrl = jSONObject.optString("clickUrl", null);
        this.trackingUrl = jSONObject.optString("trackingUrl", null);
        this.title = jSONObject.optString("title", null);
        this.description = jSONObject.optString("description", null);
        this.imageUrl = jSONObject.optString("imageUrl", null);
        this.rating = Float.valueOf(jSONObject.optString("rating", "5")).floatValue();
        this.smartRedirect = jSONObject.optBoolean("smartRedirect", false);
    }

    public String getAdId() {
        return this.adId;
    }

    public String getClickUrl() {
        return this.clickUrl;
    }

    public String getDescription() {
        return this.description;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public float getRating() {
        return this.rating;
    }

    public String getTitle() {
        return this.title;
    }

    public String getTrackingUrl() {
        return this.trackingUrl;
    }

    public boolean isSmartRedirect() {
        return this.smartRedirect;
    }

    public String toString() {
        return "AdDetails [adId=" + this.adId + ", clickUrl=" + this.clickUrl + ", trackingUrl=" + this.trackingUrl + ", title=" + this.title + ", description=" + this.description + ", imageUrl=" + this.imageUrl + ", smartRedirect=" + this.smartRedirect + "]";
    }
}
