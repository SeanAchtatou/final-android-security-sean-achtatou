package com.startapp.android.publish;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import com.startapp.android.publish.a.b;
import com.startapp.android.publish.a.d;
import com.startapp.android.publish.a.e;
import com.startapp.android.publish.c.h;
import com.startapp.android.publish.list3d.List3DActivity;
import com.startapp.android.publish.model.AdDetails;
import com.startapp.android.publish.model.AdPreferences;
import com.startapp.android.publish.model.MetaData;
import java.util.List;

public class a extends Ad {
    private static String b = null;
    private List<AdDetails> a = null;

    public a(Context context) {
        super(context);
        if (b == null) {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void a(AdPreferences adPreferences, AdEventListener adEventListener) {
        new b(this.context, this, adPreferences, adEventListener).execute(new Void[0]);
    }

    private void c() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        ResolveInfo resolveActivity = this.context.getPackageManager().resolveActivity(intent, 0);
        if (resolveActivity != null && resolveActivity.activityInfo != null) {
            b = resolveActivity.activityInfo.packageName;
            if (b != null) {
                b = b.toLowerCase();
            }
        }
    }

    public List<AdDetails> a() {
        return this.a;
    }

    public void a(List<AdDetails> list) {
        this.a = list;
    }

    public boolean b() {
        if (!h.a(this.context) || !isReady()) {
            return false;
        }
        Intent intent = new Intent(this.context, List3DActivity.class);
        intent.addFlags(1149763584);
        this.context.startActivity(intent);
        return true;
    }

    public boolean load(final AdPreferences adPreferences, final AdEventListener adEventListener) {
        if (!super.load(adPreferences, adEventListener)) {
            return false;
        }
        if (!MetaData.INSTANCE.isInit()) {
            new d(this.context, adPreferences, new e() {
                public void a() {
                    a.this.a(adPreferences, adEventListener);
                }
            }).execute(null);
            return true;
        }
        a(adPreferences, adEventListener);
        return true;
    }
}
