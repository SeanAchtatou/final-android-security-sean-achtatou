package com.startapp.android.publish.list3d;

import android.graphics.drawable.Drawable;
import com.startapp.android.publish.model.AdDetails;

public class f {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private float g;
    private boolean h;
    private Drawable i = null;

    public f(AdDetails adDetails) {
        this.a = adDetails.getAdId();
        this.b = adDetails.getClickUrl();
        this.c = adDetails.getTrackingUrl();
        this.d = adDetails.getTitle();
        this.e = adDetails.getDescription();
        this.f = adDetails.getImageUrl();
        this.g = adDetails.getRating();
        this.h = adDetails.isSmartRedirect();
        this.i = null;
    }

    public String a() {
        return this.a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }

    public String f() {
        return this.f;
    }

    public float g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }
}
