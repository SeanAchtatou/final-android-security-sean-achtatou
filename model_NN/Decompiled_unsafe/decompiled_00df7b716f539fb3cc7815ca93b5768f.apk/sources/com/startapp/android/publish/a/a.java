package com.startapp.android.publish.a;

import android.content.Context;
import android.os.AsyncTask;
import com.startapp.android.publish.Ad;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.model.AdPreferences;

public abstract class a extends AsyncTask<Void, Void, Boolean> {
    protected final Context a;
    protected final Ad b;
    protected final AdPreferences c;
    protected final AdEventListener d;
    protected String e = null;

    public a(Context context, Ad ad, AdPreferences adPreferences, AdEventListener adEventListener) {
        this.a = context;
        this.b = ad;
        this.c = adPreferences;
        this.d = adEventListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        return Boolean.valueOf(a(a()));
    }

    /* access modifiers changed from: protected */
    public abstract Object a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        this.b.setState(bool.booleanValue() ? Ad.AdState.READY : Ad.AdState.UN_INITIALIZED);
        if (!bool.booleanValue()) {
            this.b.setErrorMessage(this.e);
            if (this.d != null) {
                this.d.onFailedToReceiveAd(this.b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);
}
