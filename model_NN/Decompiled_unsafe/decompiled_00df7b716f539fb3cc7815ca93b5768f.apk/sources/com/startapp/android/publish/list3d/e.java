package com.startapp.android.publish.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Adapter;
import android.widget.AdapterView;
import java.util.LinkedList;

public class e extends AdapterView<Adapter> {
    private Adapter a;
    /* access modifiers changed from: private */
    public int b = 0;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private int h;
    private int i;
    private int j;
    private VelocityTracker k;
    /* access modifiers changed from: private */
    public b l;
    private Runnable m;
    private final LinkedList<View> n = new LinkedList<>();
    private Runnable o;
    private Rect p;
    private Camera q;
    private Matrix r;
    private Paint s;
    private int t = Integer.MIN_VALUE;

    public e(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: private */
    public int a(int i2, int i3) {
        if (this.p == null) {
            this.p = new Rect();
        }
        for (int i4 = 0; i4 < getChildCount(); i4++) {
            getChildAt(i4).getHitRect(this.p);
            if (this.p.contains(i2, i3)) {
                return i4;
            }
        }
        return -1;
    }

    private int a(View view) {
        return (int) ((((float) view.getMeasuredHeight()) * 0.35000002f) / 2.0f);
    }

    private LightingColorFilter a(float f2) {
        int i2 = 255;
        double cos = Math.cos((3.141592653589793d * ((double) f2)) / 180.0d);
        int i3 = ((int) (200.0d * cos)) + 55;
        int pow = (int) (Math.pow(cos, 200.0d) * 70.0d);
        if (i3 > 255) {
            i3 = 255;
        }
        if (pow <= 255) {
            i2 = pow;
        }
        return new LightingColorFilter(Color.rgb(i3, i3, i3), Color.rgb(i2, i2, i2));
    }

    private void a() {
        int i2 = this.h % 90;
        int height = i2 < 45 ? ((-(this.h - i2)) * getHeight()) / 270 : ((-((this.h + 90) - i2)) * getHeight()) / 270;
        if (this.t == Integer.MIN_VALUE && this.j == this.a.getCount() - 1 && c(getChildAt(getChildCount() - 1)) < getHeight()) {
            this.t = height;
        }
        if (height > 0) {
            height = 0;
        } else if (height < this.t) {
            height = this.t;
        }
        this.l.a((float) height);
        this.l.b((float) height);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.f = this.e + i2;
        this.h = (-(this.f * 270)) / getHeight();
        a();
        requestLayout();
    }

    private void a(Canvas canvas, Bitmap bitmap, int i2, int i3, int i4, int i5, float f2, float f3) {
        if (this.q == null) {
            this.q = new Camera();
        }
        this.q.save();
        this.q.translate(0.0f, 0.0f, (float) i5);
        this.q.rotateX(f3);
        this.q.translate(0.0f, 0.0f, (float) (-i5));
        if (this.r == null) {
            this.r = new Matrix();
        }
        this.q.getMatrix(this.r);
        this.q.restore();
        this.r.preTranslate((float) (-i4), (float) (-i5));
        this.r.postScale(f2, f2);
        this.r.postTranslate((float) (i3 + i4), (float) (i2 + i5));
        if (this.s == null) {
            this.s = new Paint();
            this.s.setAntiAlias(true);
            this.s.setFilterBitmap(true);
        }
        this.s.setColorFilter(a(f3));
        canvas.drawBitmap(bitmap, this.r, this.s);
    }

    private void a(MotionEvent motionEvent) {
        removeCallbacks(this.m);
        this.c = (int) motionEvent.getX();
        this.d = (int) motionEvent.getY();
        this.e = b(getChildAt(0)) - this.g;
        b();
        this.k = VelocityTracker.obtain();
        this.k.addMovement(motionEvent);
        this.b = 1;
    }

    private void a(View view, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        ViewGroup.LayoutParams layoutParams2 = layoutParams == null ? new ViewGroup.LayoutParams(-2, -2) : layoutParams;
        int i3 = i2 == 1 ? 0 : -1;
        view.setDrawingCacheEnabled(true);
        addViewInLayout(view, i3, layoutParams2, true);
        view.measure(((int) (((float) getWidth()) * 0.85f)) | 1073741824, 0);
    }

    /* access modifiers changed from: private */
    public int b(View view) {
        return view.getTop() - a(view);
    }

    private void b() {
        if (this.o == null) {
            this.o = new Runnable() {
                public void run() {
                    int a2;
                    if (e.this.b == 1 && (a2 = e.this.a(e.this.c, e.this.d)) != -1) {
                        e.this.b(a2);
                    }
                }
            };
        }
        postDelayed(this.o, (long) ViewConfiguration.getLongPressTimeout());
    }

    private void b(float f2) {
        if (this.k != null) {
            this.k.recycle();
            this.k = null;
            removeCallbacks(this.o);
            if (this.m == null) {
                this.m = new Runnable() {
                    public void run() {
                        if (e.this.l != null) {
                            View childAt = e.this.getChildAt(0);
                            if (childAt != null) {
                                int unused = e.this.e = e.this.b(childAt) - e.this.g;
                                e.this.l.a(AnimationUtils.currentAnimationTimeMillis());
                                e.this.a(((int) e.this.l.a()) - e.this.e);
                            }
                            if (!e.this.l.a(0.5f, 0.4f)) {
                                e.this.postDelayed(this, 16);
                            }
                        }
                    }
                };
            }
            if (this.l != null) {
                this.l.a((float) this.f, f2, AnimationUtils.currentAnimationTimeMillis());
                post(this.m);
            }
            this.b = 0;
        }
    }

    /* access modifiers changed from: private */
    public void b(int i2) {
        View childAt = getChildAt(i2);
        int i3 = this.i + i2;
        long itemId = this.a.getItemId(i3);
        AdapterView.OnItemLongClickListener onItemLongClickListener = getOnItemLongClickListener();
        if (onItemLongClickListener != null) {
            onItemLongClickListener.onItemLongClick(this, childAt, i3, itemId);
        }
    }

    private void b(int i2, int i3) {
        int a2 = a(i2, i3);
        if (a2 != -1) {
            View childAt = getChildAt(a2);
            int i4 = a2 + this.i;
            performItemClick(childAt, i4, this.a.getItemId(i4));
        }
    }

    private boolean b(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (x >= this.c - 10 && x <= this.c + 10 && y >= this.d - 10 && y <= this.d + 10) {
            return false;
        }
        removeCallbacks(this.o);
        this.b = 2;
        return true;
    }

    private int c(View view) {
        return view.getBottom() + a(view);
    }

    private void c() {
        int i2 = this.g + this.f;
        float width = 0.0f * ((float) getWidth());
        float height = 1.0f / (((float) getHeight()) * 0.9f);
        for (int i3 = 0; i3 < getChildCount(); i3++) {
            View childAt = getChildAt(i3);
            int measuredWidth = childAt.getMeasuredWidth();
            int measuredHeight = childAt.getMeasuredHeight();
            int sin = ((int) (((double) width) * Math.sin(6.283185307179586d * ((double) height) * ((double) i2)))) + ((getWidth() - measuredWidth) / 2);
            int a2 = a(childAt);
            int i4 = i2 + a2;
            childAt.layout(sin, i4, measuredWidth + sin, i4 + measuredHeight);
            i2 += (a2 * 2) + measuredHeight;
        }
    }

    private void c(int i2) {
        int childCount = getChildCount();
        if (this.j != this.a.getCount() - 1 && childCount > 1) {
            View childAt = getChildAt(0);
            while (childAt != null && c(childAt) + i2 < 0) {
                removeViewInLayout(childAt);
                int i3 = childCount - 1;
                this.n.addLast(childAt);
                this.i++;
                this.g += d(childAt);
                if (i3 > 1) {
                    childAt = getChildAt(0);
                    childCount = i3;
                } else {
                    childAt = null;
                    childCount = i3;
                }
            }
        }
        if (this.i != 0 && childCount > 1) {
            int i4 = childCount;
            View childAt2 = getChildAt(childCount - 1);
            while (childAt2 != null && b(childAt2) + i2 > getHeight()) {
                removeViewInLayout(childAt2);
                i4--;
                this.n.addLast(childAt2);
                this.j--;
                childAt2 = i4 > 1 ? getChildAt(i4 - 1) : null;
            }
        }
    }

    private void c(int i2, int i3) {
        while (i2 + i3 < getHeight() && this.j < this.a.getCount() - 1) {
            this.j++;
            View view = this.a.getView(this.j, getCachedView(), this);
            a(view, 0);
            i2 += d(view);
        }
    }

    private int d(View view) {
        return view.getMeasuredHeight() + (a(view) * 2);
    }

    private void d(int i2) {
        c(c(getChildAt(getChildCount() - 1)), i2);
        d(b(getChildAt(0)), i2);
    }

    private void d(int i2, int i3) {
        while (i2 + i3 > 0 && this.i > 0) {
            this.i--;
            View view = this.a.getView(this.i, getCachedView(), this);
            a(view, 1);
            int d2 = d(view);
            i2 -= d2;
            this.g -= d2;
        }
    }

    private View getCachedView() {
        if (this.n.size() != 0) {
            return this.n.removeFirst();
        }
        return null;
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return super.dispatchKeyShortcutEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        Bitmap drawingCache = view.getDrawingCache();
        if (drawingCache == null) {
            return super.drawChild(canvas, view, j2);
        }
        int top = view.getTop();
        int left = view.getLeft();
        int width = view.getWidth() / 2;
        int height = view.getHeight() / 2;
        float height2 = (float) (getHeight() / 2);
        float f2 = (((float) (top + height)) - height2) / height2;
        float cos = (float) (1.0d - (0.15000000596046448d * (1.0d - Math.cos((double) f2))));
        float f3 = (((float) this.h) - (f2 * 20.0f)) % 90.0f;
        float f4 = f3 < 0.0f ? f3 + 90.0f : f3;
        if (f4 < 45.0f) {
            a(canvas, drawingCache, top, left, width, height, cos, f4 - 90.0f);
            a(canvas, drawingCache, top, left, width, height, cos, f4);
        } else {
            a(canvas, drawingCache, top, left, width, height, cos, f4);
            a(canvas, drawingCache, top, left, width, height, cos, f4 - 90.0f);
        }
        return false;
    }

    public Adapter getAdapter() {
        return this.a;
    }

    public int getFirstItemPosition() {
        return this.i;
    }

    public int getLastItemPosition() {
        return this.j;
    }

    public View getSelectedView() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
        if (this.a != null) {
            if (getChildCount() == 0) {
                this.j = -1;
                c(this.f, 0);
            } else {
                int b2 = (this.f + this.g) - b(getChildAt(0));
                c(b2);
                d(b2);
            }
            c();
            invalidate();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f2 = 0.0f;
        if (getChildCount() == 0) {
            return false;
        }
        switch (motionEvent.getAction()) {
            case 0:
                a(motionEvent);
                break;
            case 1:
                if (this.b == 1) {
                    b((int) motionEvent.getX(), (int) motionEvent.getY());
                } else if (this.b == 2) {
                    this.k.addMovement(motionEvent);
                    this.k.computeCurrentVelocity(1000);
                    f2 = this.k.getYVelocity();
                }
                b(f2);
                break;
            case 2:
                if (this.b == 1) {
                    b(motionEvent);
                }
                if (this.b == 2) {
                    this.k.addMovement(motionEvent);
                    a(((int) motionEvent.getY()) - this.d);
                    break;
                }
                break;
            default:
                b(0.0f);
                break;
        }
        return true;
    }

    public void setAdapter(Adapter adapter) {
        this.a = adapter;
        removeAllViewsInLayout();
        requestLayout();
    }

    public void setDynamics(b bVar) {
        if (this.l != null) {
            bVar.a(this.l.a(), this.l.b(), AnimationUtils.currentAnimationTimeMillis());
        }
        this.l = bVar;
    }

    public void setSelection(int i2) {
        throw new UnsupportedOperationException("Not supported");
    }
}
