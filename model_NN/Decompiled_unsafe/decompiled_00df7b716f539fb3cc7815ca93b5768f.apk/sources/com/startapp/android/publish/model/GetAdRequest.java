package com.startapp.android.publish.model;

import com.startapp.android.publish.c.h;
import com.startapp.android.publish.model.AdPreferences;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GetAdRequest extends BaseRequest {
    private int adsNumber = 1;
    private int age;
    private Set<String> categories = null;
    private Set<String> categoriesExclude = null;
    private String gender;
    private String keywords;
    private double latitude = 0.0d;
    private double longitude = 0.0d;
    private AdPreferences.Placement placement;
    private String template;
    private boolean testMode;

    public void addCategory(String str) {
        if (this.categories == null) {
            this.categories = new HashSet();
        }
        this.categories.add(str);
    }

    public void addCategoryExclude(String str) {
        if (this.categoriesExclude == null) {
            this.categoriesExclude = new HashSet();
        }
        this.categoriesExclude.add(str);
    }

    public void fillAdPreferences(AdPreferences adPreferences, AdPreferences.Placement placement2) {
        this.placement = placement2;
        this.age = adPreferences.getAge();
        this.gender = adPreferences.getGender();
        this.keywords = adPreferences.getKeywords();
        this.testMode = adPreferences.isTestMode();
        this.categories = adPreferences.getCategories();
        this.categoriesExclude = adPreferences.getCategoriesExclude();
    }

    public int getAdsNumber() {
        return this.adsNumber;
    }

    public int getAge() {
        return this.age;
    }

    public Set<String> getCategories() {
        return this.categories;
    }

    public Set<String> getCategoriesExclude() {
        return this.categoriesExclude;
    }

    public String getGender() {
        return this.gender;
    }

    public String getKeywords() {
        return this.keywords;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, boolean):void
     arg types: [java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, int]
     candidates:
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.util.Set<java.lang.String>, boolean):void
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.util.Set<java.lang.String>, boolean):void
     arg types: [java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.util.Set<java.lang.String>, int]
     candidates:
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, boolean):void
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.util.Set<java.lang.String>, boolean):void */
    public List<NameValueObject> getNameValueMap() {
        List<NameValueObject> nameValueMap = super.getNameValueMap();
        if (nameValueMap == null) {
            nameValueMap = new ArrayList<>();
        }
        h.a(nameValueMap, "placement", this.placement.name(), true);
        h.a(nameValueMap, "testMode", Boolean.toString(this.testMode), false);
        h.a(nameValueMap, "longitude", Double.toString(this.longitude), false);
        h.a(nameValueMap, "latitude", Double.toString(this.latitude), false);
        h.a(nameValueMap, "gender", this.gender, false);
        h.a(nameValueMap, "age", Integer.toString(this.age), false);
        h.a(nameValueMap, "keywords", this.keywords, false);
        h.a(nameValueMap, "template", this.template, false);
        h.a(nameValueMap, "adsNumber", Integer.toString(this.adsNumber), false);
        h.a(nameValueMap, "category", this.categories, false);
        h.a(nameValueMap, "categoryExclude", this.categoriesExclude, false);
        return nameValueMap;
    }

    public AdPreferences.Placement getPlacement() {
        return this.placement;
    }

    public String getTemplate() {
        return this.template;
    }

    public boolean isTestMode() {
        return this.testMode;
    }

    public void setAdsNumber(int i) {
        this.adsNumber = i;
    }

    public void setAge(int i) {
        this.age = i;
    }

    public void setCategories(Set<String> set) {
        this.categories = set;
    }

    public void setCategoriesExclude(Set<String> set) {
        this.categoriesExclude = set;
    }

    public void setGender(String str) {
        this.gender = str;
    }

    public void setKeywords(String str) {
        this.keywords = str;
    }

    public void setLatitude(double d) {
        this.latitude = d;
    }

    public void setLongitude(double d) {
        this.longitude = d;
    }

    public void setPlacement(AdPreferences.Placement placement2) {
        this.placement = placement2;
    }

    public void setTemplate(String str) {
        this.template = str;
    }

    public void setTestMode(boolean z) {
        this.testMode = z;
    }

    public String toString() {
        return "GetAdRequest [placement=" + this.placement + ", testMode=" + this.testMode + ", longitude=" + this.longitude + ", latitude=" + this.latitude + ", gender=" + this.gender + ", age=" + this.age + ", keywords=" + this.keywords + ", template=" + this.template + ", adsNumber=" + this.adsNumber + ", categories=" + this.categories + ", categoriesExclude=" + this.categoriesExclude + "]";
    }
}
