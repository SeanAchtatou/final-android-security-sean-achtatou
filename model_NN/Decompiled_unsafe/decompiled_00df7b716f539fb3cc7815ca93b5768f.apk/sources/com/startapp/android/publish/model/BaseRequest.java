package com.startapp.android.publish.model;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.startapp.android.publish.c.a;
import com.startapp.android.publish.c.h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public abstract class BaseRequest extends BaseDTO implements NameValueSerializer {
    private static final String OS = "android";
    private String deviceVersion;
    private int height;
    private String isp;
    private String manufacturer;
    private String model;
    private String os = OS;
    private String packageId;
    private Map<String, String> parameters = new HashMap();
    private String productId;
    private String publisherId;
    private String sdkVersion = "1.4.1";
    private String subProductId;
    private String subPublisherId;
    private String userId;
    private int width;

    public void fillApplicationDetails(Context context, AdPreferences adPreferences) {
        setPublisherId(adPreferences.getPublisherId());
        setProductId(adPreferences.getProductId());
        setUserId(a.a(context));
        setPackageId(context.getPackageName());
        setManufacturer(Build.MANUFACTURER);
        setModel(Build.MODEL);
        setDeviceVersion(Integer.toString(Build.VERSION.SDK_INT));
        setWidth(context.getResources().getDisplayMetrics().widthPixels);
        setHeight(context.getResources().getDisplayMetrics().heightPixels);
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            setIsp(telephonyManager.getSimOperator());
        } else {
            setIsp(null);
        }
    }

    public String getDeviceVersion() {
        return this.deviceVersion;
    }

    public int getHeight() {
        return this.height;
    }

    public String getIsp() {
        return this.isp;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public String getModel() {
        return this.model;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, boolean):void
     arg types: [java.util.ArrayList, java.lang.String, java.lang.String, int]
     candidates:
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.util.Set<java.lang.String>, boolean):void
      com.startapp.android.publish.c.h.a(java.util.List<com.startapp.android.publish.model.NameValueObject>, java.lang.String, java.lang.String, boolean):void */
    public List<NameValueObject> getNameValueMap() {
        ArrayList arrayList = new ArrayList();
        h.a((List<NameValueObject>) arrayList, "publisherId", this.publisherId, true);
        h.a((List<NameValueObject>) arrayList, "productId", this.productId, true);
        h.a((List<NameValueObject>) arrayList, "os", this.os, true);
        h.a((List<NameValueObject>) arrayList, "sdkVersion", this.sdkVersion, false);
        h.a((List<NameValueObject>) arrayList, "packageId", this.packageId, false);
        h.a((List<NameValueObject>) arrayList, "userId", this.userId, false);
        h.a((List<NameValueObject>) arrayList, "model", this.model, false);
        h.a((List<NameValueObject>) arrayList, "manufacturer", this.manufacturer, false);
        h.a((List<NameValueObject>) arrayList, "manufacturer", this.manufacturer, false);
        h.a((List<NameValueObject>) arrayList, "isp", this.isp, false);
        h.a((List<NameValueObject>) arrayList, "subPublisherId", this.subPublisherId, false);
        h.a((List<NameValueObject>) arrayList, "subProductId", this.subProductId, false);
        h.a((List<NameValueObject>) arrayList, "width", String.valueOf(this.width), false);
        h.a((List<NameValueObject>) arrayList, "height", String.valueOf(this.height), false);
        return arrayList;
    }

    public String getOs() {
        return this.os;
    }

    public String getPackageId() {
        return this.packageId;
    }

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public String getProductId() {
        return this.productId;
    }

    public String getPublisherId() {
        return this.publisherId;
    }

    public String getRequestString() {
        Set<String> valueSet;
        StringBuilder sb = new StringBuilder();
        List<NameValueObject> nameValueMap = getNameValueMap();
        if (nameValueMap == null) {
            return sb.toString();
        }
        sb.append('?');
        for (NameValueObject next : nameValueMap) {
            if (next.getValue() != null) {
                sb.append(next.getName()).append('=').append(next.getValue()).append('&');
            } else if (!(next.getValueSet() == null || (valueSet = next.getValueSet()) == null)) {
                for (String append : valueSet) {
                    sb.append(next.getName()).append('=').append(append).append('&');
                }
            }
        }
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString().replace("+", "%20");
    }

    public String getSdkVersion() {
        return this.sdkVersion;
    }

    public String getSubProductId() {
        return this.subProductId;
    }

    public String getSubPublisherId() {
        return this.subPublisherId;
    }

    public String getUserId() {
        return this.userId;
    }

    public int getWidth() {
        return this.width;
    }

    public void setDeviceVersion(String str) {
        this.deviceVersion = str;
    }

    public void setHeight(int i) {
        this.height = i;
    }

    public void setIsp(String str) {
        this.isp = str;
    }

    public void setManufacturer(String str) {
        this.manufacturer = str;
    }

    public void setModel(String str) {
        this.model = str;
    }

    public void setPackageId(String str) {
        this.packageId = str;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public void setProductId(String str) {
        this.productId = str;
    }

    public void setPublisherId(String str) {
        this.publisherId = str;
    }

    public void setSdkVersion(String str) {
        this.sdkVersion = str;
    }

    public void setSubProductId(String str) {
        this.subProductId = str;
    }

    public void setSubPublisherId(String str) {
        this.subPublisherId = str;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setWidth(int i) {
        this.width = i;
    }

    public String toString() {
        return "BaseRequest [parameters=" + this.parameters + "]";
    }
}
