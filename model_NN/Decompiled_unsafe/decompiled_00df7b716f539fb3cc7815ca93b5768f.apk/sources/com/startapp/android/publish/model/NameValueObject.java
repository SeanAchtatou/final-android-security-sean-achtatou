package com.startapp.android.publish.model;

import java.util.Set;

public class NameValueObject {
    private String name;
    private String value;
    private Set<String> valueSet;

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return this.value;
    }

    public Set<String> getValueSet() {
        return this.valueSet;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setValue(String str) {
        this.value = str;
    }

    public void setValueSet(Set<String> set) {
        this.valueSet = set;
    }

    public String toString() {
        return "NameValueObject [name=" + this.name + ", value=" + this.value + ", valueSet=" + this.valueSet + "]";
    }
}
