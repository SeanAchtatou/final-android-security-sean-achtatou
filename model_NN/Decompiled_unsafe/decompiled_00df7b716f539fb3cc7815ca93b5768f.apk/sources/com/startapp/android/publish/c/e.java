package com.startapp.android.publish.c;

import android.os.Build;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class e {
    static {
        if (Build.VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    private static HttpURLConnection a(String str, byte[] bArr, Map<String, String> map) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.addRequestProperty("Cache-Control", "no-cache");
        if (bArr != null) {
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
        } else {
            httpURLConnection.setRequestMethod("GET");
        }
        httpURLConnection.setRequestProperty("Accept", "application/json;text/html;text/plain");
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(10000);
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x006b A[SYNTHETIC, Splitter:B:18:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r12, java.util.Map<java.lang.String, java.lang.String> r13, java.lang.StringBuilder r14) {
        /*
            r0 = 1
            r11 = -1
            r1 = 0
            r3 = 0
            r2 = 0
            java.net.HttpURLConnection r2 = a(r12, r2, r13)     // Catch:{ Exception -> 0x00d1, all -> 0x00ce }
            int r4 = r2.getResponseCode()     // Catch:{ Exception -> 0x004b }
            r5 = 200(0xc8, float:2.8E-43)
            if (r4 == r5) goto L_0x008c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x004b }
            r5.<init>()     // Catch:{ Exception -> 0x004b }
            java.lang.String r6 = "Error sendGetWithResponse code = ["
            java.lang.StringBuilder r6 = r5.append(r6)     // Catch:{ Exception -> 0x004b }
            java.lang.StringBuilder r6 = r6.append(r4)     // Catch:{ Exception -> 0x004b }
            r7 = 93
            r6.append(r7)     // Catch:{ Exception -> 0x004b }
            java.io.InputStream r1 = r2.getErrorStream()     // Catch:{ Exception -> 0x004b }
            if (r1 == 0) goto L_0x007b
            java.io.StringWriter r6 = new java.io.StringWriter     // Catch:{ Exception -> 0x004b }
            r6.<init>()     // Catch:{ Exception -> 0x004b }
            r7 = 1024(0x400, float:1.435E-42)
            char[] r7 = new char[r7]     // Catch:{ Exception -> 0x004b }
            java.io.BufferedReader r8 = new java.io.BufferedReader     // Catch:{ Exception -> 0x004b }
            java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x004b }
            java.lang.String r10 = "UTF-8"
            r9.<init>(r1, r10)     // Catch:{ Exception -> 0x004b }
            r8.<init>(r9)     // Catch:{ Exception -> 0x004b }
        L_0x0040:
            int r9 = r8.read(r7)     // Catch:{ Exception -> 0x004b }
            if (r9 == r11) goto L_0x0074
            r10 = 0
            r6.write(r7, r10, r9)     // Catch:{ Exception -> 0x004b }
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
        L_0x004c:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r3.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r4 = "Error execute Exception "
            java.lang.StringBuilder r4 = r3.append(r4)     // Catch:{ all -> 0x0068 }
            java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0068 }
            r4.append(r5)     // Catch:{ all -> 0x0068 }
            com.startapp.android.publish.c.f r4 = new com.startapp.android.publish.c.f     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0068 }
            r4.<init>(r3, r0)     // Catch:{ all -> 0x0068 }
            throw r4     // Catch:{ all -> 0x0068 }
        L_0x0068:
            r0 = move-exception
        L_0x0069:
            if (r1 == 0) goto L_0x006e
            r1.close()     // Catch:{ IOException -> 0x00cc }
        L_0x006e:
            if (r2 == 0) goto L_0x0073
            r2.disconnect()
        L_0x0073:
            throw r0
        L_0x0074:
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x004b }
            r5.append(r6)     // Catch:{ Exception -> 0x004b }
        L_0x007b:
            r6 = 502(0x1f6, float:7.03E-43)
            if (r4 != r6) goto L_0x008a
        L_0x007f:
            com.startapp.android.publish.c.f r3 = new com.startapp.android.publish.c.f     // Catch:{ Exception -> 0x004b }
            java.lang.String r4 = r5.toString()     // Catch:{ Exception -> 0x004b }
            r5 = 0
            r3.<init>(r4, r5, r0)     // Catch:{ Exception -> 0x004b }
            throw r3     // Catch:{ Exception -> 0x004b }
        L_0x008a:
            r0 = r3
            goto L_0x007f
        L_0x008c:
            java.io.InputStream r1 = r2.getInputStream()     // Catch:{ Exception -> 0x004b }
            if (r14 == 0) goto L_0x00bf
            if (r1 == 0) goto L_0x00bf
            java.io.StringWriter r3 = new java.io.StringWriter     // Catch:{ Exception -> 0x004b }
            r3.<init>()     // Catch:{ Exception -> 0x004b }
            r4 = 1024(0x400, float:1.435E-42)
            char[] r4 = new char[r4]     // Catch:{ Exception -> 0x004b }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x004b }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x004b }
            java.lang.String r7 = "UTF-8"
            r6.<init>(r1, r7)     // Catch:{ Exception -> 0x004b }
            r5.<init>(r6)     // Catch:{ Exception -> 0x004b }
        L_0x00a9:
            int r6 = r5.read(r4)     // Catch:{ Exception -> 0x004b }
            if (r6 == r11) goto L_0x00b4
            r7 = 0
            r3.write(r4, r7, r6)     // Catch:{ Exception -> 0x004b }
            goto L_0x00a9
        L_0x00b4:
            r4 = 0
            r14.setLength(r4)     // Catch:{ Exception -> 0x004b }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x004b }
            r14.append(r3)     // Catch:{ Exception -> 0x004b }
        L_0x00bf:
            if (r1 == 0) goto L_0x00c4
            r1.close()     // Catch:{ IOException -> 0x00ca }
        L_0x00c4:
            if (r2 == 0) goto L_0x00c9
            r2.disconnect()
        L_0x00c9:
            return r0
        L_0x00ca:
            r1 = move-exception
            goto L_0x00c4
        L_0x00cc:
            r1 = move-exception
            goto L_0x006e
        L_0x00ce:
            r0 = move-exception
            r2 = r1
            goto L_0x0069
        L_0x00d1:
            r0 = move-exception
            r2 = r1
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.c.e.a(java.lang.String, java.util.Map, java.lang.StringBuilder):boolean");
    }
}
