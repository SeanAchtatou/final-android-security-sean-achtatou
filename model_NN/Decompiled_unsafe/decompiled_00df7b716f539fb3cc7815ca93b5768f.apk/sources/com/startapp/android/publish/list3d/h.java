package com.startapp.android.publish.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import com.startapp.android.publish.model.AdDetails;
import java.util.ArrayList;
import java.util.List;

public enum h {
    INSTANCE;
    
    private c b;
    private List<f> c;

    public Bitmap a(int i, String str, String str2) {
        return this.b.a(i, str, str2);
    }

    public void a() {
        this.b = new c();
        this.c = new ArrayList();
    }

    public void a(Context context, String str, String str2) {
        this.b.a(context, str, str2);
    }

    public void a(i iVar) {
        this.b.a(iVar);
    }

    public void a(AdDetails adDetails) {
        f fVar = new f(adDetails);
        this.c.add(fVar);
        this.b.a(this.c.size() - 1, fVar.a(), fVar.f());
    }

    public List<f> b() {
        return this.c;
    }
}
