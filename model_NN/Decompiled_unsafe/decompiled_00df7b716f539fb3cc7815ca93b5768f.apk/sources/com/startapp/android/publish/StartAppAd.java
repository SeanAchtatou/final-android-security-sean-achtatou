package com.startapp.android.publish;

import android.content.Context;
import com.startapp.android.publish.a.d;
import com.startapp.android.publish.a.e;
import com.startapp.android.publish.c.h;
import com.startapp.android.publish.model.AdPreferences;
import com.startapp.android.publish.model.MetaData;
import java.util.Random;

public class StartAppAd extends Ad {
    private a appWallAd = null;
    private HtmlAd htmlAd = null;

    private class a implements AdEventListener {
        private boolean b = false;
        private AdEventListener c = null;
        private boolean d = false;

        public a(AdEventListener adEventListener) {
            this.c = adEventListener;
        }

        public void onFailedToReceiveAd(Ad ad) {
            if (this.d) {
                this.c.onFailedToReceiveAd(StartAppAd.this);
            }
            this.d = true;
        }

        public void onReceiveAd(Ad ad) {
            if (!this.b) {
                this.b = true;
                this.c.onReceiveAd(StartAppAd.this);
            }
        }
    }

    public StartAppAd(Context context) {
        super(context);
    }

    /* access modifiers changed from: private */
    public void loadAds(AdPreferences adPreferences, AdEventListener adEventListener) {
        if (new Random().nextInt(100) < MetaData.INSTANCE.getProbability3D(this.context)) {
            a aVar = null;
            if (adEventListener != null) {
                aVar = new a(adEventListener);
            }
            this.appWallAd = new a(this.context);
            this.appWallAd.load(adPreferences, aVar);
            this.htmlAd = new HtmlAd(this.context);
            this.htmlAd.load(adPreferences, aVar);
            return;
        }
        this.htmlAd = new HtmlAd(this.context);
        this.htmlAd.load(adPreferences, adEventListener);
    }

    public boolean doHome() {
        if (this.htmlAd != null) {
            return this.htmlAd.doHome();
        }
        return false;
    }

    public boolean load(final AdPreferences adPreferences, final AdEventListener adEventListener) {
        fillMissingAdPreferences(adPreferences);
        this.htmlAd = null;
        this.appWallAd = null;
        if (!h.a(this.context)) {
            return false;
        }
        if (!MetaData.INSTANCE.isInit()) {
            new d(this.context, adPreferences, new e() {
                public void a() {
                    StartAppAd.this.loadAds(adPreferences, adEventListener);
                }
            }).execute(null);
            return true;
        }
        loadAds(adPreferences, adEventListener);
        return true;
    }

    public boolean show() {
        if (this.appWallAd != null && this.appWallAd.b()) {
            return true;
        }
        if (this.htmlAd != null) {
            return this.htmlAd.show();
        }
        return false;
    }
}
