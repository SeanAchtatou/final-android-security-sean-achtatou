package com.startapp.android.publish.list3d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.android.publish.model.MetaData;

public class g {
    private RelativeLayout a;
    private ImageView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private RatingBar f;

    public g(Context context) {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
        this.a = new RelativeLayout(context);
        this.a.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{MetaData.INSTANCE.getItemGradientTop(context), MetaData.INSTANCE.getItemGradientBottom(context)}));
        this.a.setLayoutParams(layoutParams);
        int a2 = com.startapp.android.publish.c.g.a(context, 3);
        int a3 = com.startapp.android.publish.c.g.a(context, 4);
        int a4 = com.startapp.android.publish.c.g.a(context, 5);
        int a5 = com.startapp.android.publish.c.g.a(context, 6);
        int a6 = com.startapp.android.publish.c.g.a(context, 10);
        int a7 = com.startapp.android.publish.c.g.a(context, 84);
        this.a.setPadding(a6, a2, a6, a2);
        this.a.setTag(this);
        this.b = new ImageView(context);
        this.b.setId(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(a7, a7);
        layoutParams2.addRule(15);
        this.b.setLayoutParams(layoutParams2);
        this.b.setPadding(0, 0, a5, 0);
        this.c = new TextView(context);
        this.c.setId(2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(1, 1);
        layoutParams3.addRule(6, 1);
        this.c.setLayoutParams(layoutParams3);
        this.c.setPadding(0, 0, 0, a4);
        this.c.setTextColor(MetaData.INSTANCE.getItemTitleTextColor(context).intValue());
        this.c.setTextSize((float) MetaData.INSTANCE.getItemTitleTextSize(context).intValue());
        this.c.setSingleLine(true);
        this.c.setEllipsize(TextUtils.TruncateAt.END);
        com.startapp.android.publish.c.g.a(this.c, MetaData.INSTANCE.getItemTitleTextDecoration(context));
        this.d = new TextView(context);
        this.d.setId(3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(1, 1);
        layoutParams4.addRule(3, 2);
        layoutParams4.setMargins(0, 0, 0, a4);
        this.d.setLayoutParams(layoutParams4);
        this.d.setTextColor(MetaData.INSTANCE.getItemDescriptionTextColor(context).intValue());
        this.d.setTextSize((float) MetaData.INSTANCE.getItemDescriptionTextSize(context).intValue());
        this.d.setSingleLine(true);
        this.d.setEllipsize(TextUtils.TruncateAt.END);
        com.startapp.android.publish.c.g.a(this.d, MetaData.INSTANCE.getItemDescriptionTextDecoration(context));
        this.f = new RatingBar(context, null, 16842877);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(1, 1);
        layoutParams5.addRule(8, 1);
        layoutParams5.setMargins(0, 0, 0, -a4);
        this.f.setLayoutParams(layoutParams5);
        this.f.setPadding(0, 0, 0, a3);
        this.f.setId(5);
        this.e = new TextView(context);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.addRule(11);
        layoutParams6.addRule(8, 1);
        this.e.setLayoutParams(layoutParams6);
        this.e.setText("Download");
        this.e.setTextColor(-1);
        this.e.setTextSize(12.0f);
        this.e.setTypeface(null, 1);
        this.e.setPadding(a6, a5, a6, a5);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, null, null)) {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
             arg types: [int, int, float, int, int, int, android.graphics.Shader$TileMode]
             candidates:
              ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
              ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
              ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
              ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
            /* access modifiers changed from: protected */
            public void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setShader(new LinearGradient(0.0f, 0.0f, shape.getWidth(), 0.0f, -15626493, -15485690, Shader.TileMode.REPEAT));
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        this.a.addView(this.b);
        this.a.addView(this.c);
        this.a.addView(this.d);
        this.a.addView(this.f);
        this.a.addView(this.e);
    }

    public RelativeLayout a() {
        return this.a;
    }

    public ImageView b() {
        return this.b;
    }

    public TextView c() {
        return this.c;
    }

    public TextView d() {
        return this.d;
    }

    public RatingBar e() {
        return this.f;
    }
}
