package com.startapp.android.publish.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import java.util.List;

public class d extends ArrayAdapter<f> {
    public d(Context context, List<f> list) {
        super(context, 0, list);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        g gVar;
        if (view == null) {
            g gVar2 = new g(getContext());
            view = gVar2.a();
            gVar = gVar2;
        } else {
            gVar = (g) view.getTag();
        }
        f fVar = (f) getItem(i);
        gVar.c().setText(fVar.d());
        gVar.d().setText(fVar.e());
        Bitmap a = h.INSTANCE.a(i, fVar.a(), fVar.f());
        if (a == null) {
            gVar.b().setImageResource(17301651);
        } else {
            gVar.b().setImageBitmap(a);
        }
        gVar.e().setRating(fVar.g());
        h.INSTANCE.a(getContext(), fVar.a(), fVar.c());
        return view;
    }
}
