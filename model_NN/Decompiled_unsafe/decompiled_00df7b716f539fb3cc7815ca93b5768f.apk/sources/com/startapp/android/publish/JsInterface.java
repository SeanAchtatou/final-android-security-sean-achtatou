package com.startapp.android.publish;

import android.webkit.JavascriptInterface;

public class JsInterface {
    private Runnable callback = null;
    private boolean processed = false;

    public JsInterface(Runnable runnable) {
        this.callback = runnable;
    }

    @JavascriptInterface
    public void closeAd() {
        if (!this.processed) {
            this.processed = true;
            this.callback.run();
        }
    }
}
