package com.startapp.android.publish.list3d;

class j extends b {
    private float f;
    private float g;

    public j(float f2, float f3) {
        this.f = f2;
        this.g = f3;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.b += c() * this.g;
        this.a += (this.b * ((float) i)) / 1000.0f;
        this.b *= this.f;
    }
}
