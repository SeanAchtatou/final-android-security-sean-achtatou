package com.startapp.android.publish.a;

import android.content.Context;
import com.startapp.android.publish.AdEventListener;
import com.startapp.android.publish.a;
import com.startapp.android.publish.c.d;
import com.startapp.android.publish.c.f;
import com.startapp.android.publish.list3d.h;
import com.startapp.android.publish.model.AdDetails;
import com.startapp.android.publish.model.AdPreferences;
import com.startapp.android.publish.model.BaseResponse;
import com.startapp.android.publish.model.GetAdRequest;
import com.startapp.android.publish.model.GetAdResponse;
import com.startapp.android.publish.model.MetaData;
import java.util.List;
import java.util.Map;

public class b extends a {
    public b(Context context, a aVar, AdPreferences adPreferences, AdEventListener adEventListener) {
        super(context, aVar, adPreferences, adEventListener);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        super.onPostExecute(bool);
        if (bool.booleanValue()) {
            List<AdDetails> a = ((a) this.b).a();
            h.INSTANCE.a();
            if (a != null) {
                for (AdDetails a2 : a) {
                    h.INSTANCE.a(a2);
                }
            }
            if (this.d != null) {
                this.d.onReceiveAd(this.b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        GetAdResponse getAdResponse = (GetAdResponse) obj;
        if (obj == null) {
            this.e = "Empty Response";
            d.a(6, "Error Empty Response");
            return false;
        } else if (!getAdResponse.isValidResponse()) {
            this.e = getAdResponse.getErrorMessage();
            d.a(6, "Error msg = [" + this.e + "]");
            return false;
        } else {
            ((a) this.b).a(getAdResponse.getAdsDetails());
            boolean z = getAdResponse.getAdsDetails() != null && getAdResponse.getAdsDetails().size() > 0;
            if (z) {
                return z;
            }
            this.e = "Empty Response";
            return z;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public BaseResponse a() {
        GetAdRequest getAdRequest = new GetAdRequest();
        getAdRequest.fillApplicationDetails(this.a, this.c);
        getAdRequest.fillAdPreferences(this.c, AdPreferences.Placement.INAPP_3DAPPWALL);
        getAdRequest.setAdsNumber(MetaData.INSTANCE.getMaxAds(this.a));
        try {
            return (GetAdResponse) com.startapp.android.publish.b.b.a(this.a, "http://www.startappexchange.com/1.2/getads", getAdRequest, (Map<String, String>) null, GetAdResponse.class);
        } catch (f e) {
            d.a(6, "Unable to handle GetAdsSetService command!!!!", e);
            this.e = e.getMessage();
            return null;
        }
    }
}
