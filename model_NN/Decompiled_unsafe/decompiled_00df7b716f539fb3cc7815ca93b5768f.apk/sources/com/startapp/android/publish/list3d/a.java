package com.startapp.android.publish.list3d;

import java.io.FilterInputStream;
import java.io.InputStream;

public class a {

    /* renamed from: com.startapp.android.publish.list3d.a$a  reason: collision with other inner class name */
    static class C0004a extends FilterInputStream {
        public C0004a(InputStream inputStream) {
            super(inputStream);
        }

        public long skip(long j) {
            long j2 = 0;
            while (j2 < j) {
                long skip = this.in.skip(j - j2);
                if (skip == 0) {
                    if (read() < 0) {
                        break;
                    }
                    skip = 1;
                }
                j2 = skip + j2;
            }
            return j2;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        r1.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003e, code lost:
        r5 = r1;
        r1 = r0;
        r0 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0048, code lost:
        r5 = r0;
        r0 = r1;
        r1 = r5;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x003d A[ExcHandler: all (r1v5 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:3:0x000c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(java.lang.String r6) {
        /*
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            r0.<init>(r6)     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x002e, all -> 0x0036 }
            r0.connect()     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            java.io.BufferedInputStream r3 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            com.startapp.android.publish.list3d.a$a r4 = new com.startapp.android.publish.list3d.a$a     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r4)     // Catch:{ Exception -> 0x0042, all -> 0x003d }
            r3.close()     // Catch:{ Exception -> 0x0047, all -> 0x003d }
            r2.close()     // Catch:{ Exception -> 0x0047, all -> 0x003d }
            if (r0 == 0) goto L_0x004c
            r0.disconnect()
            r0 = r1
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            r0 = r1
        L_0x0030:
            if (r1 == 0) goto L_0x002d
            r1.disconnect()
            goto L_0x002d
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.disconnect()
        L_0x003c:
            throw r0
        L_0x003d:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0037
        L_0x0042:
            r2 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
            goto L_0x0030
        L_0x0047:
            r2 = move-exception
            r5 = r0
            r0 = r1
            r1 = r5
            goto L_0x0030
        L_0x004c:
            r0 = r1
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.list3d.a.a(java.lang.String):android.graphics.Bitmap");
    }
}
