package com.startapp.android.publish.c;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.shapes.Shape;

public class c extends Shape {
    private RectF a = new RectF();

    /* renamed from: a */
    public c clone() {
        c cVar = (c) super.clone();
        cVar.a = new RectF(this.a);
        return cVar;
    }

    public void draw(Canvas canvas, Paint paint) {
        canvas.drawRect(this.a, paint);
        Paint paint2 = new Paint(1);
        paint2.setColor(-1);
        paint2.setStrokeWidth(4.0f);
        paint2.setStyle(Paint.Style.STROKE);
        canvas.drawRect(this.a, paint2);
    }

    /* access modifiers changed from: protected */
    public void onResize(float f, float f2) {
        this.a.set(0.0f, 0.0f, f, f2);
    }
}
