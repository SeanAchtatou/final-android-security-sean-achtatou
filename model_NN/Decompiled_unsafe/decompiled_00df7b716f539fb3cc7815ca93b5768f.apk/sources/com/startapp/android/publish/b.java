package com.startapp.android.publish;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.startapp.android.publish.c.d;
import com.startapp.android.publish.c.f;
import com.startapp.android.publish.c.g;

public class b extends FrameLayout {
    public static boolean a = false;
    private static a d = new a();
    protected int b = -1;
    protected WindowManager c;
    private String e = null;
    /* access modifiers changed from: private */
    public String f = null;
    /* access modifiers changed from: private */
    public boolean g = true;
    /* access modifiers changed from: private */
    public View h = null;
    /* access modifiers changed from: private */
    public long i = System.currentTimeMillis();
    /* access modifiers changed from: private */
    public Handler j = new Handler();

    public static class a extends BroadcastReceiver {
        private b a;

        public void a(b bVar) {
            this.a = bVar;
        }

        public void onReceive(Context context, Intent intent) {
            d.a(2, "DismissButtonBroadcastReceiver::onReceive - action = [" + intent.getAction() + "]");
            if (this.a != null) {
                this.a.a();
            }
        }
    }

    /* renamed from: com.startapp.android.publish.b$b  reason: collision with other inner class name */
    private class C0003b extends WebViewClient {
        private C0003b() {
        }

        public void onPageFinished(WebView webView, String str) {
            d.a(2, "MyWebViewClient::onPageFinished - [" + str + "]");
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            d.a(2, "MyWebViewClient::onPageStarted - [" + str + "]");
            long unused = b.this.i = System.currentTimeMillis();
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            d.a(2, "MyWebViewClient::onReceivedError - [" + str + "], [" + str2 + "]");
            if (b.this.h.getVisibility() == 0) {
                b.this.h.setVisibility(8);
            }
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            b.this.d();
            if (b.this.h.getVisibility() != 0) {
                b.this.h.setVisibility(0);
            }
            d.a(2, "MyWebViewClient::shouldOverrideUrlLoading - [" + str + "]");
            String lowerCase = str.toLowerCase();
            if (!(lowerCase.startsWith("market") || lowerCase.startsWith("http://play.google.com") || lowerCase.startsWith("https://play.google.com")) && b.this.g) {
                return false;
            }
            long currentTimeMillis = System.currentTimeMillis() - b.this.i;
            d.a(2, "MyWebViewClient::shouldOverrideUrlLoading - timeDiff=[" + currentTimeMillis + "]");
            long j = 0;
            if (currentTimeMillis < 5000) {
                j = 5000 - currentTimeMillis;
                if (b.this.h.getVisibility() != 0) {
                    b.this.h.setVisibility(0);
                }
            }
            d.a(2, "MyWebViewClient::shouldOverrideUrlLoading - delay=[" + j + "]");
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(344457216);
            b.this.getContext().startActivity(intent);
            d.a(2, "MyWebViewClient::shouldOverrideUrlLoading:run - time of activity =[" + System.currentTimeMillis() + "]");
            b.this.j.postDelayed(new Runnable() {
                public void run() {
                    b.this.a(1000);
                }
            }, j);
            return true;
        }
    }

    public b(Context context) {
        super(context);
        d.a(this);
        setBackgroundColor(0);
        this.c = (WindowManager) getContext().getSystemService("window");
    }

    private void c() {
        removeAllViews();
        WebView webView = new WebView(getContext());
        webView.setBackgroundColor(-1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setWebViewClient(new C0003b());
        webView.addJavascriptInterface(new JsInterface(new Runnable() {
            public void run() {
                b.this.a();
            }
        }), "startappwall");
        webView.loadUrl(this.e);
        webView.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                b.this.a();
                return true;
            }
        });
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        layoutParams.topMargin = getStatusBarHeight();
        webView.setLayoutParams(layoutParams);
        addView(webView);
        this.h = g.a(getContext());
        addView(this.h);
        this.h.setVisibility(8);
    }

    /* access modifiers changed from: private */
    public void d() {
        getContext().sendBroadcast(new Intent("com.startapp.android.publish.DISMISS_ACTIVITY"));
        a = true;
    }

    public void a() {
        d();
        d.a(2, "MyWebViewClient::hide - curreent time [" + System.currentTimeMillis() + "]");
        try {
            this.c.removeView(this);
            getContext().unregisterReceiver(d);
        } catch (RuntimeException e2) {
        }
    }

    public void a(int i2) {
        d.a(2, "MyWebViewClient::delay hide - curreent time [" + i2 + "]");
        postDelayed(new Runnable() {
            public void run() {
                b.this.a();
            }
        }, (long) i2);
    }

    public void b() {
        a = false;
        c();
        this.c.addView(this, getWmParams());
        getContext().registerReceiver(d, new IntentFilter("com.startapp.android.publish.DISMISS_OVERLAY"));
        getContext().registerReceiver(d, new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
        if (this.f != null) {
            new AsyncTask<Void, Void, Void>() {
                /* access modifiers changed from: protected */
                /* renamed from: a */
                public Void doInBackground(Void... voidArr) {
                    try {
                        com.startapp.android.publish.b.b.a(b.this.getContext(), b.this.f, null);
                    } catch (f e) {
                    }
                    return null;
                }
            }.execute(new Void[0]);
        }
        this.i = System.currentTimeMillis();
        d.a(2, "OverlayHtml::show - startShowTime=[" + this.i + "]");
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return super.dispatchKeyShortcutEvent(keyEvent);
    }

    public int getStatusBarHeight() {
        int identifier = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (identifier > 0) {
            return getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    public WindowManager.LayoutParams getWmParams() {
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.height = -1;
        layoutParams.width = -1;
        layoutParams.alpha = 1.0f;
        layoutParams.format = 1;
        layoutParams.type = 2003;
        layoutParams.flags = 327968;
        return layoutParams;
    }

    public void setSmartRedirect(boolean z) {
        this.g = z;
    }

    public void setTrackingUrl(String str) {
        this.f = str;
    }

    public void setUrl(String str) {
        this.e = str;
    }
}
