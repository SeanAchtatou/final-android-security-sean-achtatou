package com.startapp.android.publish.b;

public enum a {
    INSTANCE;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public <T extends com.startapp.android.publish.model.JsonDeserializer> T a(java.lang.String r5, java.lang.Class<T> r6) {
        /*
            r4 = this;
            r1 = 0
            if (r5 == 0) goto L_0x0009
            int r0 = r5.length()
            if (r0 != 0) goto L_0x000b
        L_0x0009:
            r0 = r1
        L_0x000a:
            return r0
        L_0x000b:
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0046 }
            r2.<init>(r5)     // Catch:{ JSONException -> 0x0046 }
            java.lang.Object r0 = r6.newInstance()     // Catch:{ InstantiationException -> 0x006e, IllegalAccessException -> 0x0071 }
            com.startapp.android.publish.model.JsonDeserializer r0 = (com.startapp.android.publish.model.JsonDeserializer) r0     // Catch:{ InstantiationException -> 0x006e, IllegalAccessException -> 0x0071 }
        L_0x0016:
            if (r2 == 0) goto L_0x000a
            if (r0 == 0) goto L_0x000a
            r0.fromJson(r2)     // Catch:{ Throwable -> 0x001e }
            goto L_0x000a
        L_0x001e:
            r0 = move-exception
            com.startapp.android.publish.c.f r1 = new com.startapp.android.publish.c.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not read String=["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = "] "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x0046:
            r0 = move-exception
            com.startapp.android.publish.c.f r1 = new com.startapp.android.publish.c.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Could not read String=["
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r5)
            java.lang.String r3 = "] "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = r0.getMessage()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r0)
            throw r1
        L_0x006e:
            r0 = move-exception
            r0 = r1
            goto L_0x0016
        L_0x0071:
            r0 = move-exception
            r0 = r1
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.android.publish.b.a.a(java.lang.String, java.lang.Class):com.startapp.android.publish.model.JsonDeserializer");
    }
}
