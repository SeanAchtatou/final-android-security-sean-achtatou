package com.tapfortap;

import com.tapfortap.AppWall;

public class DefaultAppWallListener implements AppWall.AppWallListener {
    static final AppWall.AppWallListener DEFAULT_LISTENER = new DefaultAppWallListener();
    private static final String TAG = DefaultAppWallListener.class.getName();

    public void appWallOnDismiss(AppWall appWall) {
        TapForTapLog.d(TAG, appWall.hashCode() + ": appWallOnDismiss.");
    }

    public void appWallOnFail(AppWall appWall, String str, Throwable th) {
        TapForTapLog.d(TAG, appWall.hashCode() + ": appWallOnFail because: " + str, th);
    }

    public void appWallOnReceive(AppWall appWall) {
        TapForTapLog.d(TAG, appWall.hashCode() + ": appWallOnReceive.");
    }

    public void appWallOnShow(AppWall appWall) {
        TapForTapLog.d(TAG, appWall.hashCode() + ": appWallOnShow.");
    }

    public void appWallOnTap(AppWall appWall) {
        TapForTapLog.d(TAG, appWall.hashCode() + ": appWallOnTap.");
    }
}
