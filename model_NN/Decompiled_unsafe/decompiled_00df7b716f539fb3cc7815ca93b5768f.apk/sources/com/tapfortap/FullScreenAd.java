package com.tapfortap;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.tapfortap.AdRequest;
import com.tapfortap.TapForTap;

class FullScreenAd {
    public static final String AD_NOT_DISMISSED_WARNING = "You have called show before the previous ad was dismissed. This will cause a new ad to be shown as soon as it soon it is downloaded and the previous ad is dismissed. Use the InterstitialAd.ResponseListener to make sure you are showing ads when you intend to.";
    public static final String AD_NOT_LOADED_WARNING = "You have called show before a new ad is ready. This will cause an ad to show as soon as it is downloaded. This may cause ads to show at an undesirable time if the user has poor network connectivity. Check isReadyToShow or use the InterstitialAd.ResponseListener to make sure you are showing ads when you intend to.";
    static final String FULL_SCREEN_AD = "com.tapfortap.full_screen_ad";
    static final String FULL_SCREEN_AD_EVENT_FILTER = "com.tapfortap.full_screen_ad_event";
    static final String FULL_SCREEN_AD_TYPE = "com.tapfortap.full_screen_ad_type";
    static final String IMPRESSION_LOGGED = "com.tapfortap.full_screen_impression_logged";
    /* access modifiers changed from: private */
    public static final String TAG = FullScreenAd.class.getName();
    /* access modifiers changed from: private */
    public Ad ad;
    private final Context context;
    private final IntentFilter filter;
    private final TapForTap.InitializationListener initializeResponseListener;
    private boolean loadNext = false;
    /* access modifiers changed from: private */
    public boolean loading = false;
    /* access modifiers changed from: private */
    public Ad nextAd;
    private final String path;
    /* access modifiers changed from: private */
    public boolean readyToShow = false;
    /* access modifiers changed from: private */
    public final BroadcastReceiver receiver;
    /* access modifiers changed from: private */
    public final ResponseListener responseListener;
    /* access modifiers changed from: private */
    public boolean showWhenReady = false;
    /* access modifiers changed from: private */
    public boolean showing = false;
    private final String type;

    private class FullScreenAdReceiver extends BroadcastReceiver {
        private FullScreenAdReceiver() {
        }

        /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0056=Splitter:B:18:0x0056, B:26:0x007e=Splitter:B:26:0x007e} */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(android.content.Context r5, android.content.Intent r6) {
            /*
                r4 = this;
                android.os.Bundle r0 = r6.getExtras()
                java.lang.String r1 = "impression_id"
                java.lang.String r1 = r0.getString(r1)
                com.tapfortap.FullScreenAd r2 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                com.tapfortap.Ad r2 = r2.ad     // Catch:{ JSONException -> 0x003c }
                java.lang.String r2 = r2.getFirstImpressionId()     // Catch:{ JSONException -> 0x003c }
                boolean r1 = r1.equals(r2)     // Catch:{ JSONException -> 0x003c }
                if (r1 == 0) goto L_0x002d
                java.lang.String r1 = "event"
                java.lang.String r1 = r0.getString(r1)     // Catch:{ JSONException -> 0x003c }
                java.lang.String r2 = "show"
                boolean r2 = r1.equals(r2)     // Catch:{ JSONException -> 0x003c }
                if (r2 == 0) goto L_0x002e
                com.tapfortap.FullScreenAd r0 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                r0.postOnShow()     // Catch:{ JSONException -> 0x003c }
            L_0x002d:
                return
            L_0x002e:
                java.lang.String r2 = "tap"
                boolean r2 = r1.equals(r2)     // Catch:{ JSONException -> 0x003c }
                if (r2 == 0) goto L_0x0045
                com.tapfortap.FullScreenAd r0 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                r0.postOnTap()     // Catch:{ JSONException -> 0x003c }
                goto L_0x002d
            L_0x003c:
                r0 = move-exception
                com.tapfortap.FullScreenAd r1 = com.tapfortap.FullScreenAd.this
                java.lang.String r2 = "Failed to parse json."
                r1.postOnFail(r2, r0)
                goto L_0x002d
            L_0x0045:
                java.lang.String r2 = "dismiss"
                boolean r2 = r1.equals(r2)     // Catch:{ JSONException -> 0x003c }
                if (r2 == 0) goto L_0x006d
                com.tapfortap.FullScreenAd r0 = com.tapfortap.FullScreenAd.this     // Catch:{ IllegalArgumentException -> 0x0062 }
                android.content.BroadcastReceiver r0 = r0.receiver     // Catch:{ IllegalArgumentException -> 0x0062 }
                r5.unregisterReceiver(r0)     // Catch:{ IllegalArgumentException -> 0x0062 }
            L_0x0056:
                com.tapfortap.FullScreenAd r0 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                r1 = 0
                boolean unused = r0.showing = r1     // Catch:{ JSONException -> 0x003c }
                com.tapfortap.FullScreenAd r0 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                r0.postOnDismiss()     // Catch:{ JSONException -> 0x003c }
                goto L_0x002d
            L_0x0062:
                r0 = move-exception
                java.lang.String r0 = com.tapfortap.FullScreenAd.TAG     // Catch:{ JSONException -> 0x003c }
                java.lang.String r1 = "Failed to un-register receiver for some reason..."
                com.tapfortap.TapForTapLog.d(r0, r1)     // Catch:{ JSONException -> 0x003c }
                goto L_0x0056
            L_0x006d:
                java.lang.String r2 = "error"
                boolean r1 = r1.equals(r2)     // Catch:{ JSONException -> 0x003c }
                if (r1 == 0) goto L_0x002d
                com.tapfortap.FullScreenAd r1 = com.tapfortap.FullScreenAd.this     // Catch:{ IllegalArgumentException -> 0x0097 }
                android.content.BroadcastReceiver r1 = r1.receiver     // Catch:{ IllegalArgumentException -> 0x0097 }
                r5.unregisterReceiver(r1)     // Catch:{ IllegalArgumentException -> 0x0097 }
            L_0x007e:
                com.tapfortap.FullScreenAd r1 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                r2 = 0
                boolean unused = r1.showing = r2     // Catch:{ JSONException -> 0x003c }
                java.lang.String r1 = "message"
                java.lang.String r0 = r0.getString(r1)     // Catch:{ JSONException -> 0x003c }
                com.tapfortap.FullScreenAd r1 = com.tapfortap.FullScreenAd.this     // Catch:{ JSONException -> 0x003c }
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ JSONException -> 0x003c }
                java.lang.String r3 = "Failed to render html."
                r2.<init>(r3)     // Catch:{ JSONException -> 0x003c }
                r1.postOnFail(r0, r2)     // Catch:{ JSONException -> 0x003c }
                goto L_0x002d
            L_0x0097:
                r1 = move-exception
                java.lang.String r1 = com.tapfortap.FullScreenAd.TAG     // Catch:{ JSONException -> 0x003c }
                java.lang.String r2 = "Failed to un-register receiver for some reason..."
                com.tapfortap.TapForTapLog.d(r1, r2)     // Catch:{ JSONException -> 0x003c }
                goto L_0x007e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tapfortap.FullScreenAd.FullScreenAdReceiver.onReceive(android.content.Context, android.content.Intent):void");
        }
    }

    enum FullScreenAdType {
        APP_WALL("app-wall"),
        INTERSTITIAL("interstitial");
        
        private final String type;

        private FullScreenAdType(String str) {
            this.type = str;
        }

        /* access modifiers changed from: package-private */
        public String getType() {
            return this.type;
        }
    }

    interface ResponseListener {
        void fullScreenOnDismiss();

        void fullScreenOnFail(String str, Throwable th);

        void fullScreenOnReceive();

        void fullScreenOnShow();

        void fullScreenOnTap();
    }

    private FullScreenAd(Context context2, String str, String str2, ResponseListener responseListener2) {
        this.responseListener = responseListener2;
        this.context = context2.getApplicationContext();
        this.path = str;
        this.type = str2;
        this.initializeResponseListener = getInitializeResponseListener();
        this.receiver = new FullScreenAdReceiver();
        this.filter = new IntentFilter();
        this.filter.addAction(FULL_SCREEN_AD_EVENT_FILTER);
    }

    static FullScreenAd create(Context context2, String str, String str2, ResponseListener responseListener2) {
        if (!TapForTap.androidManifestIsMissingFullScreenActivity(context2)) {
            return new FullScreenAd(context2, str, str2, responseListener2);
        }
        throw new RuntimeException("missing com.tapfortap.FullScreenAdActivity");
    }

    private TapForTap.InitializationListener getInitializeResponseListener() {
        return new TapForTap.InitializationListener() {
            public void onFail(String str, Throwable th) {
                FullScreenAd.this.postOnFail("Failed to initialize: " + str, th);
            }

            public void onSuccess(boolean z) {
                if (FullScreenAd.this.readyToShow) {
                    FullScreenAd.this.postOnReceive();
                } else {
                    FullScreenAd.this.getFullScreenAd();
                }
            }
        };
    }

    /* access modifiers changed from: private */
    public void postOnDismiss() {
        Utils.postToMainLooper(new Runnable() {
            public void run() {
                FullScreenAd.this.responseListener.fullScreenOnDismiss();
            }
        });
    }

    /* access modifiers changed from: private */
    public void postOnFail(final String str, final Throwable th) {
        Utils.postToMainLooper(new Runnable() {
            public void run() {
                FullScreenAd.this.responseListener.fullScreenOnFail(str, th);
            }
        });
    }

    /* access modifiers changed from: private */
    public void postOnReceive() {
        Utils.postToMainLooper(new Runnable() {
            public void run() {
                FullScreenAd.this.responseListener.fullScreenOnReceive();
            }
        });
    }

    /* access modifiers changed from: private */
    public void postOnShow() {
        Utils.postToMainLooper(new Runnable() {
            public void run() {
                FullScreenAd.this.responseListener.fullScreenOnShow();
            }
        });
    }

    /* access modifiers changed from: private */
    public void postOnTap() {
        Utils.postToMainLooper(new Runnable() {
            public void run() {
                FullScreenAd.this.responseListener.fullScreenOnTap();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void getFullScreenAd() {
        TapForTapLog.d(TAG, "Getting a full screen ad.");
        if (this.loading) {
            TapForTapLog.d(TAG, "Already loading a full screen ad.");
            return;
        }
        this.loading = true;
        AdRequest.start(this.path, new AdRequest.ResponseListener() {
            public void onFailure(String str, Throwable th) {
                TapForTapLog.e(FullScreenAd.TAG, "Failed to get a full screen ad: " + str);
                boolean unused = FullScreenAd.this.loading = false;
                boolean unused2 = FullScreenAd.this.readyToShow = false;
                FullScreenAd.this.postOnFail(str, th);
            }

            public void onSuccess(Ad ad) {
                Ad unused = FullScreenAd.this.nextAd = ad;
                boolean unused2 = FullScreenAd.this.loading = false;
                boolean unused3 = FullScreenAd.this.readyToShow = true;
                FullScreenAd.this.postOnReceive();
                if (FullScreenAd.this.showWhenReady) {
                    boolean unused4 = FullScreenAd.this.showWhenReady = false;
                    FullScreenAd.this.showAd();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public boolean isReadyToShow() {
        return this.readyToShow;
    }

    /* access modifiers changed from: package-private */
    public void load() {
        TapForTap.initialize(this.context, this.initializeResponseListener);
    }

    /* access modifiers changed from: package-private */
    public void show() {
        this.loadNext = false;
        showAd();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: package-private */
    public void showAd() {
        TapForTapLog.d(TAG, "Starting to show a full screen ad.");
        if (!this.readyToShow || this.showing) {
            TapForTapLog.d(TAG, "No full screen ad ready to show, loading a new one.");
            if (this.showing) {
                TapForTapLog.w(TAG, AD_NOT_DISMISSED_WARNING);
            } else {
                TapForTapLog.w(TAG, AD_NOT_LOADED_WARNING);
            }
            this.showWhenReady = true;
            load();
            return;
        }
        TapForTapLog.d(TAG, "Showing a full screen ad.");
        this.showing = true;
        this.readyToShow = false;
        this.ad = this.nextAd;
        if (this.loadNext) {
            load();
        }
        this.context.registerReceiver(this.receiver, this.filter);
        Intent intent = new Intent(this.context, FullScreenAdActivity.class);
        intent.putExtra(FULL_SCREEN_AD_TYPE, this.type);
        intent.putExtra(FULL_SCREEN_AD, this.ad.getAdAsString());
        intent.putExtra(IMPRESSION_LOGGED, false);
        intent.addFlags(65536);
        intent.addFlags(268435456);
        this.context.startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    public void showAndLoadNext() {
        this.loadNext = true;
        showAd();
    }
}
