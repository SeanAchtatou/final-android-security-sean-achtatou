package com.tapfortap;

import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

class AdRequest extends ApiRequest {
    private static final String TAG = AdRequest.class.getName();
    private final ResponseListener responseListener;

    interface ResponseListener {
        void onFailure(String str, Throwable th);

        void onSuccess(Ad ad);
    }

    private AdRequest(String str, ResponseListener responseListener2) {
        super(str);
        this.responseListener = responseListener2;
    }

    static void start(String str, ResponseListener responseListener2) {
        TapForTapLog.d(TAG, "Starting a new AdRequest for path " + str);
        submit(new AdRequest(str, responseListener2));
    }

    /* access modifiers changed from: package-private */
    public void onFailure(String str, Throwable th) {
        this.responseListener.onFailure(str, th);
    }

    /* access modifiers changed from: package-private */
    public void onSuccess(JSONObject jSONObject) throws JSONException {
        Ad ad = new Ad(jSONObject);
        int i = 0;
        while (i < ad.length()) {
            try {
                String str = DiskCache.get(ad.getImageUrl(i));
                if (str != null) {
                    ad.putFileUrl(i, "file://" + str);
                }
                i++;
            } catch (IOException e) {
                this.responseListener.onFailure("Could not get image.", new Exception());
                return;
            }
        }
        this.responseListener.onSuccess(ad);
    }
}
