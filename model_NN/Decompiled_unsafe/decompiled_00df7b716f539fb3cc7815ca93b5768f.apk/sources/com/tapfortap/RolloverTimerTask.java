package com.tapfortap;

import java.util.Timer;
import java.util.TimerTask;

class RolloverTimerTask {
    /* access modifiers changed from: private */
    public final Runnable runnable;
    private final Timer timer = new Timer();
    private TimerTask timerTask;

    RolloverTimerTask(Runnable runnable2) {
        this.runnable = runnable2;
    }

    private TimerTask getTimerTask() {
        return new TimerTask() {
            public void run() {
                RolloverTimerTask.this.runnable.run();
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void stopTimerTask() {
        if (this.timerTask != null) {
            this.timerTask.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public void tryIn(long j) {
        stopTimerTask();
        this.timerTask = getTimerTask();
        this.timer.schedule(this.timerTask, j);
    }
}
