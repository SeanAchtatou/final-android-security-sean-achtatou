package com.tapfortap;

import org.json.JSONException;
import org.json.JSONObject;

class InfoHelpers {

    static class MutableVariable {
        private boolean isNew = false;
        private final String name;
        private String value;

        MutableVariable(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public void addTo(JSONObject jSONObject) throws JSONException {
            if (!this.isNew) {
                jSONObject.put(this.name, this.value);
                this.isNew = false;
            }
        }

        /* access modifiers changed from: package-private */
        public void setValue(String str) {
            this.value = str;
            this.isNew = true;
        }
    }

    static abstract class OneShotVariable {
        private boolean gotten = false;
        private final String name;

        OneShotVariable(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public void addTo(JSONObject jSONObject) throws JSONException {
            if (!this.gotten) {
                jSONObject.put(this.name, get());
                this.gotten = true;
            }
        }

        /* access modifiers changed from: package-private */
        public abstract String get();
    }

    InfoHelpers() {
    }
}
