package com.tapfortap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class Ad implements Serializable {
    private final JSONObject ads;
    private final long refresh;

    Ad(JSONObject jSONObject) throws JSONException {
        if (jSONObject.has("refresh")) {
            this.refresh = jSONObject.getLong("refresh");
        } else {
            this.refresh = 60;
        }
        this.ads = jSONObject;
    }

    /* access modifiers changed from: package-private */
    public JSONObject getAd(int i) throws JSONException {
        return this.ads.getJSONArray("ads").getJSONObject(i);
    }

    /* access modifiers changed from: package-private */
    public String getAdAsString() {
        return this.ads.toString();
    }

    /* access modifiers changed from: package-private */
    public String getAdsAsString() throws JSONException {
        return this.ads.getJSONArray("ads").toString();
    }

    /* access modifiers changed from: package-private */
    public String getFileUrl(int i) throws JSONException {
        return getAd(i).getString("file_url");
    }

    /* access modifiers changed from: package-private */
    public String getFirstFileUrl() throws JSONException {
        return getFileUrl(0);
    }

    /* access modifiers changed from: package-private */
    public String getFirstImpressionId() throws JSONException {
        return getImpressionId(0);
    }

    /* access modifiers changed from: package-private */
    public String getFirstUrl() throws JSONException {
        return getUrl(0);
    }

    /* access modifiers changed from: package-private */
    public String getImageUrl(int i) throws JSONException {
        return getAd(i).getString("image_url");
    }

    /* access modifiers changed from: package-private */
    public String getImpressionId(int i) throws JSONException {
        return getAd(i).getString("impression_id");
    }

    /* access modifiers changed from: package-private */
    public List<String> getImpressionIds() throws JSONException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < this.ads.getJSONArray("ads").length(); i++) {
            arrayList.add(this.ads.getJSONArray("ads").getJSONObject(i).getString("impression_id"));
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    public String getOrientation() {
        try {
            return getAd(0).getString("orientation");
        } catch (JSONException e) {
            return "portrait";
        }
    }

    /* access modifiers changed from: package-private */
    public long getRefreshRate() {
        return this.refresh;
    }

    /* access modifiers changed from: package-private */
    public String getUrl(int i) throws JSONException {
        return getAd(i).getString("url");
    }

    /* access modifiers changed from: package-private */
    public int length() throws JSONException {
        return this.ads.getJSONArray("ads").length();
    }

    /* access modifiers changed from: package-private */
    public void putFileUrl(int i, String str) throws JSONException {
        getAd(i).put("file_url", str);
    }
}
