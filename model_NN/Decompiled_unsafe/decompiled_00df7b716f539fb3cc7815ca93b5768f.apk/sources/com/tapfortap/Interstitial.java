package com.tapfortap;

import android.app.Activity;
import android.content.Context;
import com.tapfortap.FullScreenAd;

public final class Interstitial {
    private static final String INTERSTITIAL_PATH = "ad/interstitial";
    private static final String TAG = Interstitial.class.getName();
    private FullScreenAd fullScreenAd;

    public interface InterstitialListener {
        void interstitialOnDismiss(Interstitial interstitial);

        void interstitialOnFail(Interstitial interstitial, String str, Throwable th);

        void interstitialOnReceive(Interstitial interstitial);

        void interstitialOnShow(Interstitial interstitial);

        void interstitialOnTap(Interstitial interstitial);
    }

    private Interstitial() {
    }

    public static Interstitial create(Context context) {
        return create(context, DefaultInterstitialListener.DEFAULT_LISTENER);
    }

    public static Interstitial create(Context context, final InterstitialListener interstitialListener) {
        if (interstitialListener instanceof Activity) {
            TapForTapLog.e(TAG, "Tap for Tap suggests not using an Activity as a response listener as it may cause the Activity to leak. Please use an anonymous inner class or a regular class. Visit tapfortap.com/doc/android for more details.");
        }
        final Interstitial interstitial = new Interstitial();
        interstitial.fullScreenAd = FullScreenAd.create(context, INTERSTITIAL_PATH, FullScreenAd.FullScreenAdType.INTERSTITIAL.getType(), new FullScreenAd.ResponseListener() {
            public void fullScreenOnDismiss() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        interstitialListener.interstitialOnDismiss(interstitial);
                    }
                });
            }

            public void fullScreenOnFail(final String str, final Throwable th) {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        interstitialListener.interstitialOnFail(interstitial, str, th);
                    }
                });
            }

            public void fullScreenOnReceive() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        interstitialListener.interstitialOnReceive(interstitial);
                    }
                });
            }

            public void fullScreenOnShow() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        interstitialListener.interstitialOnShow(interstitial);
                    }
                });
            }

            public void fullScreenOnTap() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        interstitialListener.interstitialOnTap(interstitial);
                    }
                });
            }
        });
        interstitial.load();
        return interstitial;
    }

    public boolean isReadyToShow() {
        return this.fullScreenAd.isReadyToShow();
    }

    public void load() {
        this.fullScreenAd.load();
    }

    public void show() {
        this.fullScreenAd.show();
    }

    public void showAndLoad() {
        this.fullScreenAd.showAndLoadNext();
    }
}
