package com.tapfortap;

import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class EventLoggerRequest extends ApiRequest {
    private static final String EVENT_PATH = "events";
    private static final String TAG = EventLoggerRequest.class.getName();
    private final Map<String, String> events;
    private final ResponseListener responseListener;

    interface ResponseListener {
        void onFailure(String str, Throwable th);

        void onSuccess();
    }

    private EventLoggerRequest(Map<String, String> map, ResponseListener responseListener2) {
        super(EVENT_PATH);
        this.events = map;
        this.responseListener = responseListener2;
    }

    static void startRequest(Map<String, String> map, ResponseListener responseListener2) {
        submit(new EventLoggerRequest(map, responseListener2));
    }

    /* access modifiers changed from: protected */
    public JSONObject addTo(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        for (String put : this.events.values()) {
            jSONArray.put(put);
        }
        jSONObject.put(EVENT_PATH, jSONArray);
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void onFailure(String str, Throwable th) {
        this.responseListener.onFailure(str, th);
    }

    /* access modifiers changed from: package-private */
    public void onSuccess(JSONObject jSONObject) {
        this.responseListener.onSuccess();
    }
}
