package com.tapfortap;

import com.tapfortap.Interstitial;

public class DefaultInterstitialListener implements Interstitial.InterstitialListener {
    static final Interstitial.InterstitialListener DEFAULT_LISTENER = new DefaultInterstitialListener();
    private static final String TAG = DefaultInterstitialListener.class.getName();

    public void interstitialOnDismiss(Interstitial interstitial) {
        TapForTapLog.d(TAG, interstitial.hashCode() + ": interstitialOnDismiss.");
    }

    public void interstitialOnFail(Interstitial interstitial, String str, Throwable th) {
        TapForTapLog.d(TAG, interstitial.hashCode() + ": interstitialOnFail because: " + str, th);
    }

    public void interstitialOnReceive(Interstitial interstitial) {
        TapForTapLog.d(TAG, interstitial.hashCode() + ": interstitialOnReceive.");
    }

    public void interstitialOnShow(Interstitial interstitial) {
        TapForTapLog.d(TAG, interstitial.hashCode() + ": interstitialOnShow.");
    }

    public void interstitialOnTap(Interstitial interstitial) {
        TapForTapLog.d(TAG, interstitial.hashCode() + ": interstitialOnTap.");
    }
}
