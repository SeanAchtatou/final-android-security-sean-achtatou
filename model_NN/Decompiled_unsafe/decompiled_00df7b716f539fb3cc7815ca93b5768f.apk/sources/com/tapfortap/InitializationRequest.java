package com.tapfortap;

import com.tapfortap.TapForTap;
import org.json.JSONException;
import org.json.JSONObject;

class InitializationRequest extends ApiRequest {
    private static final String INITIALIZE_PATH = "check-in";
    private static final String TAG = InitializationRequest.class.getName();
    private final TapForTap.InitializationListener responseListener;

    private InitializationRequest(TapForTap.InitializationListener initializationListener) {
        super(INITIALIZE_PATH);
        this.responseListener = initializationListener;
    }

    static void start(TapForTap.InitializationListener initializationListener) {
        submit(new InitializationRequest(initializationListener));
    }

    /* access modifiers changed from: package-private */
    public void onFailure(String str, Throwable th) {
        this.responseListener.onFail(str, th);
    }

    /* access modifiers changed from: package-private */
    public void onSuccess(JSONObject jSONObject) {
        boolean z;
        try {
            z = jSONObject.getJSONObject("extra").getBoolean("created");
        } catch (JSONException e) {
            z = false;
        }
        try {
            if (jSONObject.getJSONObject("extra").getBoolean("paused")) {
                TapForTap.disableTapForTap();
            }
        } catch (JSONException e2) {
        }
        this.responseListener.onSuccess(z);
    }
}
