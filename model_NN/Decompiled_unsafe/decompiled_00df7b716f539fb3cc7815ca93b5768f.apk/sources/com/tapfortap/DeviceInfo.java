package com.tapfortap;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.tapfortap.InfoHelpers;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

class DeviceInfo {
    private static Context context;
    private static final InfoHelpers.OneShotVariable country = new Country();
    private static String klass = null;
    private static String resolution = null;
    private static String screenScale = null;
    private static final InfoHelpers.OneShotVariable timezone = new TimeZone();

    private static class Country extends InfoHelpers.OneShotVariable {
        Country() {
            super("country");
        }

        /* access modifiers changed from: protected */
        public String get() {
            return Locale.getDefault().getCountry();
        }
    }

    private static class TimeZone extends InfoHelpers.OneShotVariable {
        TimeZone() {
            super("timezone");
        }

        /* access modifiers changed from: protected */
        public String get() {
            return String.valueOf(java.util.TimeZone.getDefault().getRawOffset() / 60000);
        }
    }

    DeviceInfo() {
    }

    public static void addTo(JSONObject jSONObject) throws JSONException {
        jSONObject.put("class", getKlass());
        country.addTo(jSONObject);
        jSONObject.put("language", Locale.getDefault().getLanguage());
        jSONObject.put("make", Build.MANUFACTURER);
        jSONObject.put("model", Build.MODEL);
        jSONObject.put("resolution", getResolution());
        jSONObject.put("screen_scale", getScreenScale());
        jSONObject.put("system", "Android");
        timezone.addTo(jSONObject);
        jSONObject.put("version", Build.VERSION.RELEASE);
    }

    private static String getKlass() {
        boolean z = true;
        if (klass == null) {
            int i = context.getResources().getConfiguration().screenLayout;
            boolean z2 = (i & 15) == 3 || (i & 15) == 4;
            if (Build.VERSION.SDK_INT < 11) {
                z = false;
            }
            if (!z2 || !z) {
                klass = "phone";
            } else {
                klass = "tablet";
            }
        }
        return klass;
    }

    static String getOrientation() {
        switch (context.getResources().getConfiguration().orientation) {
            case 1:
                return "portrait";
            case 2:
                return "landscape";
            default:
                return "portrait";
        }
    }

    private static String getResolution() {
        if (resolution == null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            resolution = displayMetrics.widthPixels + "x" + displayMetrics.heightPixels;
        }
        return resolution;
    }

    private static String getScreenScale() {
        if (screenScale == null) {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            screenScale = String.format("%1.2f", Float.valueOf(displayMetrics.density));
        }
        return screenScale;
    }

    static void initialize(Context context2) {
        context = context2.getApplicationContext();
    }
}
