package com.tapfortap;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.tapfortap.InfoHelpers;
import org.json.JSONException;
import org.json.JSONObject;

class IdInfo {
    private static String androidId;
    private static final AppVersion appVersion = new AppVersion();
    /* access modifiers changed from: private */
    public static Context context;
    private static String macAddress;
    private static String packageName;
    private static String telephonyId;

    static class AppVersion extends InfoHelpers.OneShotVariable {
        AppVersion() {
            super("app_version");
        }

        /* access modifiers changed from: package-private */
        public String get() {
            try {
                String str = IdInfo.context.getPackageManager().getPackageInfo(IdInfo.context.getPackageName(), 0).versionName;
                return str != null ? str : "0";
            } catch (PackageManager.NameNotFoundException e) {
                return "0";
            }
        }
    }

    IdInfo() {
    }

    static void addTo(JSONObject jSONObject) throws JSONException {
        jSONObject.put("device_id", telephonyId);
        jSONObject.put("android_id", androidId);
        jSONObject.put("mac_address", macAddress);
        appVersion.addTo(jSONObject);
    }

    static String getGetPackageName() {
        return packageName;
    }

    static void initialize(Context context2) {
        context = context2.getApplicationContext();
        telephonyId = ((TelephonyManager) context2.getSystemService("phone")).getDeviceId();
        androidId = Settings.Secure.getString(context2.getContentResolver(), "android_id");
        macAddress = ((WifiManager) context2.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        packageName = context2.getPackageName();
    }
}
