package com.tapfortap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

class URIUtils {
    private static String API_VERSION = "v2";
    private static String BASE_URL = (PROTOCOL + DOMAIN + ":" + PORT + "/" + API_VERSION + "/");
    private static String DOMAIN = "api.tapfortap.com";
    private static String PORT = "80";
    private static final String PROTOCOL = "http://";
    private static final String TAG = URIUtils.class.getName();
    private static boolean isDevelopment = false;

    URIUtils() {
    }

    static void configureForDevelopment() {
        TapForTapLog.v(TAG, "Configure for development");
        DOMAIN = "dev.tapfortap.com";
        PORT = "4000";
        BASE_URL = PROTOCOL + DOMAIN + ":" + PORT + "/" + API_VERSION + "/";
        isDevelopment = true;
        TapForTapLog.v(TAG, "Development Parameters = \n" + printParams());
    }

    static void configureForProduction() {
        TapForTapLog.v(TAG, "Configure for production");
        DOMAIN = "api.tapfortap.com";
        PORT = "80";
        BASE_URL = PROTOCOL + DOMAIN + ":" + PORT + "/" + API_VERSION + "/";
        isDevelopment = false;
        TapForTapLog.v(TAG, "Production Parameters = \n" + printParams());
    }

    static Map<String, String> decodeUrl(String str) {
        HashMap hashMap = new HashMap();
        for (String split : TextUtils.split(str, "&")) {
            String[] split2 = TextUtils.split(split, "=");
            try {
                String decode = split2.length > 0 ? URLDecoder.decode(split2[0], "UTF-8") : "";
                String decode2 = split2.length > 1 ? URLDecoder.decode(split2[1], "UTF-8") : "";
                if (decode.length() > 0) {
                    hashMap.put(decode, decode2);
                }
            } catch (Exception e) {
                TapForTapLog.e(TAG, "Failed to decode key/value.", e);
            }
        }
        return hashMap;
    }

    static String getUriForPath(String str) {
        return BASE_URL + str;
    }

    static boolean loadUrl(Context context, String str) {
        if (str.startsWith("market://") || str.contains("play.google.com")) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } else if (!str.startsWith(PROTOCOL) && !str.startsWith("https://")) {
            return false;
        } else {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        }
    }

    private static String printParams() {
        return (((("" + "Mode = " + (isDevelopment ? "development" : "production") + "\n") + "Protocol = http://\n") + "Domain = " + DOMAIN + "\n") + "Port = " + PORT + "\n") + "API Version = " + API_VERSION + "\n";
    }
}
