package com.tapfortap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class FormatsInfo {
    private static final String TAG = FormatsInfo.class.getName();
    private static JSONArray adUnits;
    private static JSONArray creativeTypes;

    FormatsInfo() {
    }

    static void addTo(JSONObject jSONObject) {
        if (adUnits == null || creativeTypes == null) {
            try {
                adUnits = new JSONArray("[banner, interstitial, app-wall]");
                creativeTypes = new JSONArray("[image, gif]");
            } catch (JSONException e) {
                adUnits = null;
                creativeTypes = null;
                TapForTapLog.e(TAG, "Failed to create adUnits and creativeTypes.", e);
            }
        }
        if (adUnits != null && creativeTypes != null) {
            try {
                jSONObject.put("ad_units", adUnits);
                jSONObject.put("creative_types", creativeTypes);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }
}
