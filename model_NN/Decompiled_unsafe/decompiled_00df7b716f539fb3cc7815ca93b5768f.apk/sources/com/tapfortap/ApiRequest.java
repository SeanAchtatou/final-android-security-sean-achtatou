package com.tapfortap;

import android.os.AsyncTask;
import android.os.Build;
import org.json.JSONException;
import org.json.JSONObject;

abstract class ApiRequest {
    private final String path;

    ApiRequest(String str) {
        this.path = URIUtils.getUriForPath(str);
    }

    static void submit(ApiRequest apiRequest) {
        Utils.postToMainLooper(new Runnable(apiRequest) {
            final /* synthetic */ ApiRequest val$request;

            {
                this.val$request = r1;
            }

            public void run() {
                if (Build.VERSION.SDK_INT >= 14) {
                    new ApiRequestTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, this.val$request);
                    return;
                }
                new ApiRequestTask().execute(this.val$request);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public JSONObject addTo(JSONObject jSONObject) throws JSONException {
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public String getPath() {
        return this.path;
    }

    /* access modifiers changed from: package-private */
    public abstract void onFailure(String str, Throwable th);

    /* access modifiers changed from: package-private */
    public abstract void onSuccess(JSONObject jSONObject) throws JSONException;
}
