package com.tapfortap;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

public final class TapForTap {
    private static final String API_KEY_META_DATA = "com.tapfortap.API_KEY";
    private static final InitializationListener DEFAULT_RESPONSE_LISTENER = new InitializationListener() {
        public void onFail(String str, Throwable th) {
            TapForTapLog.d(TapForTap.TAG, "onFail: " + str, th);
        }

        public void onSuccess(boolean z) {
            TapForTapLog.d(TapForTap.TAG, "onSuccess");
        }
    };
    private static final String DEVELOPMENT = "development";
    static final String PLATFORM_ID = "android";
    public static String PLUGIN = PLATFORM_ID;
    public static String PLUGIN_VERSION = SDK_VERSION;
    private static final String PRODUCTION = "production";
    static final String SDK_VERSION = "3.0.8";
    private static final String SDK_VERSION_PREFERENCE = "com.tapfortap.SDK_VERSION";
    /* access modifiers changed from: private */
    public static final String TAG = TapForTap.class.getName();
    private static final String TAP_FOR_TAP_PREFERENCES = "com.tapfortap.PREFERENCES";
    private static final String TEST_MODE_META_DATA = "com.tapfortap.TEST_MODE";
    private static String apiKey = null;
    private static boolean disabled = false;
    private static String environment;
    /* access modifiers changed from: private */
    public static final Object initializationLock = new Object();
    /* access modifiers changed from: private */
    public static boolean isInitialized = false;
    /* access modifiers changed from: private */
    public static boolean isInitializing = false;
    /* access modifiers changed from: private */
    public static final ArrayList<InitializationListener> responseListeners = new ArrayList<>();
    private static Boolean testMode = false;

    public enum Gender {
        NONE("none", -1),
        MALE("male", 0),
        FEMALE("female", 1);
        
        private final String name;
        private final Integer value;

        private Gender(String str, Integer num) {
            this.name = str;
            this.value = num;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }

    public interface InitializationListener {
        void onFail(String str, Throwable th);

        void onSuccess(boolean z);
    }

    static boolean androidManifestIsMissingFullScreenActivity(Context context) {
        try {
            context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), FullScreenAdActivity.class.getName()), 0);
            return false;
        } catch (PackageManager.NameNotFoundException e) {
            TapForTapLog.e(TAG, "Tap for Tap could not find the activity \"com.tapfortap.FullScreenAdActivity\" in the AndroidManifest.xml.");
            return true;
        }
    }

    private static boolean androidManifestIsNotConfiguredProperly(Context context) {
        boolean z = false;
        if (Utils.doNotHavePermission(context, "android.permission.INTERNET")) {
            TapForTapLog.e(TAG, "Tap for Tap requires the INTERNET permission. Please add it to the AndroidManifest.xml");
            z = true;
        }
        if (Utils.doNotHavePermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
            TapForTapLog.e(TAG, "Tap for Tap requires the ACCESS_NETWORK_STATE permission. Please add it to the AndroidManifest.xml");
            z = true;
        }
        if (Utils.doNotHavePermission(context, "android.permission.ACCESS_WIFI_STATE")) {
            TapForTapLog.e(TAG, "Tap for Tap requires the ACCESS_WIFI_STATE permission. Please add it to the AndroidManifest.xml");
            z = true;
        }
        if (Utils.doNotHavePermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            TapForTapLog.e(TAG, "Tap for Tap requires the WRITE_EXTERNAL_STORAGE permission. Please add it to the AndroidManifest.xml");
            z = true;
        }
        if (!Utils.doNotHavePermission(context, "android.permission.READ_PHONE_STATE")) {
            return z;
        }
        TapForTapLog.e(TAG, "Tap for Tap requires the READ_PHONE_STATE permission. Please add it to the AndroidManifest.xml");
        return true;
    }

    public static void disableTapForTap() {
        disabled = true;
    }

    public static void disableTestMode() {
        testMode = false;
    }

    public static void enableTapForTap() {
        disabled = false;
    }

    public static void enableTestMode() {
        testMode = true;
    }

    public static String getApiKey() {
        return apiKey;
    }

    private static String getApiKeyFromMetaData(Context context) throws PackageManager.NameNotFoundException {
        return getMetaData(context).getString(API_KEY_META_DATA);
    }

    private static void getEnvironmentFromFile() {
        try {
            if (DEVELOPMENT.equals(new BufferedReader(new InputStreamReader(new FileInputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "tapfortap_environment")))).readLine())) {
                setEnvironment(DEVELOPMENT);
            }
        } catch (FileNotFoundException | IOException e) {
        }
    }

    private static Bundle getMetaData(Context context) throws PackageManager.NameNotFoundException {
        ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        if (applicationInfo.metaData != null) {
            return applicationInfo.metaData;
        }
        throw new PackageManager.NameNotFoundException("No meta data found. Cannot configure the TapForTap API key. Please add your API key to the meta-data or call TapForTap.initialize.");
    }

    private static String getSdkVersion(Context context) {
        return context.getApplicationContext().getSharedPreferences(TAP_FOR_TAP_PREFERENCES, 0).getString(SDK_VERSION_PREFERENCE, "0.0.0");
    }

    private static void getTestModeFromMetaData(Context context) {
        try {
            Bundle metaData = getMetaData(context);
            if (metaData.containsKey(TEST_MODE_META_DATA)) {
                testMode = Boolean.valueOf(metaData.getBoolean(TEST_MODE_META_DATA));
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
    }

    public static String getVersion() {
        return SDK_VERSION;
    }

    static void initialize(Context context, InitializationListener initializationListener) {
        if (apiKey == null && !isDisabled().booleanValue()) {
            try {
                apiKey = getApiKeyFromMetaData(context);
                getTestModeFromMetaData(context);
            } catch (PackageManager.NameNotFoundException e) {
                initializationListener.onFail(e.getMessage(), e);
                return;
            }
        }
        initialize(context, apiKey, initializationListener);
    }

    public static void initialize(Context context, String str) {
        initialize(context, str, DEFAULT_RESPONSE_LISTENER);
    }

    public static void initialize(final Context context, String str, InitializationListener initializationListener) {
        if (androidManifestIsNotConfiguredProperly(context)) {
            TapForTapLog.e(TAG, "The AndroidManifest.xml is not configured properly. Please add the missing elements in order to continue");
            disabled = true;
        } else if (str == null || str.equals("YOUR API KEY")) {
            TapForTapLog.e(TAG, "Please enter a valid API key");
            disabled = true;
        } else {
            synchronized (initializationLock) {
                if (isInitialized) {
                    initializationListener.onSuccess(false);
                } else if (isInitializing) {
                    responseListeners.add(initializationListener);
                } else {
                    isInitializing = true;
                    responseListeners.add(initializationListener);
                    IdInfo.initialize(context);
                    DeviceInfo.initialize(context);
                    DiskCache.initialize(Utils.getCacheDir(context), Utils.cacheSize());
                    File file = new File(context.getCacheDir(), "tft-assets");
                    file.mkdirs();
                    AssetManager.initialize(file);
                    invalidateAssets(context);
                    apiKey = str;
                    InitializationRequest.start(new InitializationListener() {
                        public void onFail(String str, Throwable th) {
                            synchronized (TapForTap.initializationLock) {
                                Iterator it = TapForTap.responseListeners.iterator();
                                while (it.hasNext()) {
                                    ((InitializationListener) it.next()).onFail(str, th);
                                }
                                TapForTap.responseListeners.clear();
                                boolean unused = TapForTap.isInitializing = false;
                                boolean unused2 = TapForTap.isInitialized = false;
                            }
                        }

                        public void onSuccess(boolean z) {
                            synchronized (TapForTap.initializationLock) {
                                boolean unused = TapForTap.isInitializing = false;
                                boolean unused2 = TapForTap.isInitialized = true;
                            }
                            Iterator it = TapForTap.responseListeners.iterator();
                            while (it.hasNext()) {
                                ((InitializationListener) it.next()).onSuccess(z);
                            }
                            TapForTap.responseListeners.clear();
                            if (!z) {
                                return;
                            }
                            if (context instanceof Activity) {
                                Utils.postToMainLooper(new Runnable() {
                                    public void run() {
                                        new AlertDialog.Builder(context).setTitle("New App").setMessage("Alright, we created your app! You can manage it at tapfortap.com/manage.").setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                            }
                                        }).show();
                                    }
                                });
                            } else {
                                Log.d("Tap for Tap", "Alright, we created your app! You can manage it at tapfortap.com/manage.");
                            }
                        }
                    });
                }
            }
        }
    }

    private static void invalidateAssets(Context context) {
        if (!getSdkVersion(context).equals(SDK_VERSION)) {
            setSdkVersion(context, SDK_VERSION);
            try {
                AssetManager.writeVersion("0");
            } catch (IOException e) {
                setSdkVersion(context, "0.0.0");
            }
        }
    }

    static Boolean isDisabled() {
        return Boolean.valueOf(disabled);
    }

    public static boolean isEnabled() {
        return !disabled;
    }

    public static boolean isInTestMode() {
        return testMode.booleanValue();
    }

    public static void setEnvironment(String str) {
        if (str.equals(DEVELOPMENT)) {
            environment = DEVELOPMENT;
            URIUtils.configureForDevelopment();
            isInitialized = false;
            return;
        }
        environment = PRODUCTION;
        URIUtils.configureForProduction();
        isInitialized = false;
    }

    public static void setGender(Gender gender) {
        UserInfo.setGender(gender);
    }

    public static void setLocation(Location location) {
        UserInfo.setLocation(location);
    }

    private static void setSdkVersion(Context context, String str) {
        context.getApplicationContext().getSharedPreferences(TAP_FOR_TAP_PREFERENCES, 0).edit().putString(SDK_VERSION_PREFERENCE, str).commit();
    }

    public static void setUserAccountId(String str) {
        UserInfo.setUserAccountId(str);
    }

    public static void setYearOfBirth(int i) {
        UserInfo.setYearOfBirth(i);
    }
}
