package com.tapfortap;

import org.json.JSONException;
import org.json.JSONObject;

class TapRequest extends ApiRequest {
    private static final String CLICK_PATH = "tap";
    private static final String TAG = TapRequest.class.getName();
    private final String coordinates;

    private TapRequest(String str, String str2) {
        super("tap/" + str);
        this.coordinates = str2;
    }

    static void start(String str, float f, float f2) {
        submit(new TapRequest(str, f + "," + f2));
    }

    /* access modifiers changed from: protected */
    public JSONObject addTo(JSONObject jSONObject) throws JSONException {
        jSONObject.put("coordinates", this.coordinates);
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void onFailure(String str, Throwable th) {
        TapForTapLog.d(TAG, "Failed to log the tap: " + str);
    }

    /* access modifiers changed from: package-private */
    public void onSuccess(JSONObject jSONObject) {
        TapForTapLog.d(TAG, "Successfully logged the tap.");
    }
}
