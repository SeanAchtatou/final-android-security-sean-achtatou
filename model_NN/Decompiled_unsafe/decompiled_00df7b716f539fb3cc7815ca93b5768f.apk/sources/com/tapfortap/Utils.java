package com.tapfortap;

import android.content.Context;
import android.graphics.Point;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.Display;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

class Utils {
    private static final String TAG = Utils.class.getName();
    private static boolean usingInternal = true;

    Utils() {
    }

    static long cacheSize() {
        return usingInternal ? 1048576 : 10485760;
    }

    private static boolean canUseExternalStorage() {
        return "mounted".equals(Environment.getExternalStorageState());
    }

    static long copyStream(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[8192];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                j += (long) read;
            } else {
                try {
                    break;
                } catch (IOException e) {
                    TapForTapLog.v(TAG, "Failed to close a stream.");
                }
            }
        }
        inputStream.close();
        outputStream.close();
        return j;
    }

    static boolean doNotHavePermission(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) != 0;
    }

    static File getCacheDir(Context context) {
        File cacheDir = context.getCacheDir();
        if (canUseExternalStorage()) {
            usingInternal = false;
            cacheDir = context.getExternalCacheDir();
        }
        return new File(cacheDir, "tft-cache");
    }

    static Point getDisplaySize(Display display) {
        Point point = new Point();
        try {
            display.getSize(point);
        } catch (NoSuchMethodError e) {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

    static boolean postToMainLooper(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper == null) {
            return false;
        }
        new Handler(mainLooper).post(runnable);
        return true;
    }
}
