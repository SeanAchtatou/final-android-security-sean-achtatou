package com.tapfortap;

import com.tapfortap.Banner;

public class DefaultBannerListener implements Banner.BannerListener {
    static final Banner.BannerListener DEFAULT_LISTENER = new DefaultBannerListener();
    private static final String TAG = DefaultBannerListener.class.getName();

    public void bannerOnFail(Banner banner, String str, Throwable th) {
        TapForTapLog.e(TAG, banner.hashCode() + " : bannerOnFail because: " + str, th);
    }

    public void bannerOnReceive(Banner banner) {
        TapForTapLog.v(TAG, banner.hashCode() + " : bannerOnReceive");
    }

    public void bannerOnTap(Banner banner) {
        TapForTapLog.v(TAG, "bannerOnTap");
    }
}
