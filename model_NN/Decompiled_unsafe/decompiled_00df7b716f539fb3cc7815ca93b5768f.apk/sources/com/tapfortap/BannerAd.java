package com.tapfortap;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tapfortap.Banner;
import java.lang.ref.WeakReference;
import org.json.JSONException;

class BannerAd extends WebView {
    private static final String TAG = BannerAd.class.getName();
    private Ad ad;
    /* access modifiers changed from: private */
    public WeakReference<Banner.BannerListener> responseListener;

    BannerAd(Context context, WebViewClient webViewClient, Banner.BannerListener bannerListener) {
        super(context);
        getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        getSettings().setLoadWithOverviewMode(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        setWebViewClient(webViewClient);
        setBackgroundColor(17170445);
        this.responseListener = new WeakReference<>(bannerListener);
    }

    /* access modifiers changed from: package-private */
    public void onTap(float f, float f2) {
        if (this.ad != null) {
            try {
                TapRequest.start(this.ad.getFirstImpressionId(), f, f2);
                postOnTap();
                if (!URIUtils.loadUrl(getContext(), this.ad.getFirstUrl())) {
                    getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.ad.getFirstUrl())));
                }
            } catch (JSONException e) {
                JSONException jSONException = e;
                jSONException.printStackTrace();
                Banner.BannerListener bannerListener = this.responseListener.get();
                if (bannerListener != null) {
                    bannerListener.bannerOnFail((Banner) getParent(), "Failed to parse json.", jSONException);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void postOnTap() {
        post(new Runnable() {
            public void run() {
                Banner.BannerListener bannerListener = (Banner.BannerListener) BannerAd.this.responseListener.get();
                if (bannerListener != null) {
                    bannerListener.bannerOnTap((Banner) BannerAd.this.getParent());
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setAd(Ad ad2) {
        try {
            this.ad = ad2;
            if (ad2 != null) {
                final String str = "<head><style type='text/css'>body{margin:auto auto;text-align:center;} </style></head><body><img src=\"" + ad2.getFirstFileUrl() + "\"/></body>";
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        BannerAd.this.loadDataWithBaseURL("fake://not/needed", str, "text/html", "utf-8", "");
                    }
                });
            }
        } catch (JSONException e) {
            JSONException jSONException = e;
            this.ad = null;
            Banner.BannerListener bannerListener = this.responseListener.get();
            if (bannerListener != null) {
                bannerListener.bannerOnFail((Banner) getParent(), "Failed to get URL from json.", jSONException);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void setResponseListener(Banner.BannerListener bannerListener) {
        this.responseListener = new WeakReference<>(bannerListener);
    }
}
