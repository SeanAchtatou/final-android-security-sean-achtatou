package com.tapfortap;

import android.location.Location;
import com.tapfortap.InfoHelpers;
import com.tapfortap.TapForTap;
import org.json.JSONException;
import org.json.JSONObject;

class UserInfo {
    private static final InfoHelpers.MutableVariable gender = new InfoHelpers.MutableVariable("gender");
    private static final InfoHelpers.MutableVariable location = new InfoHelpers.MutableVariable("location");
    private static final InfoHelpers.MutableVariable userAccountId = new InfoHelpers.MutableVariable("user_account_id");
    private static final InfoHelpers.MutableVariable yearOfBirth = new InfoHelpers.MutableVariable("year_of_birth");

    UserInfo() {
    }

    static void addTo(JSONObject jSONObject) throws JSONException {
        yearOfBirth.addTo(jSONObject);
        gender.addTo(jSONObject);
        location.addTo(jSONObject);
        userAccountId.addTo(jSONObject);
    }

    public static void setGender(TapForTap.Gender gender2) {
        gender.setValue(gender2.getName());
    }

    public static void setLocation(Location location2) {
        location.setValue(location2.getLatitude() + "x" + location2.getLongitude());
    }

    public static void setUserAccountId(String str) {
        userAccountId.setValue(str);
    }

    public static void setYearOfBirth(int i) {
        yearOfBirth.setValue(String.valueOf(i));
    }
}
