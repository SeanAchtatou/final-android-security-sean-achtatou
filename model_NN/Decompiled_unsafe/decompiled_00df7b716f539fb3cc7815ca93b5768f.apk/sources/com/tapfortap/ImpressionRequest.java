package com.tapfortap;

import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ImpressionRequest extends ApiRequest {
    private static final String IMPRESSION_PATH = "impressions";
    private static final String TAG = ImpressionRequest.class.getName();
    private final List<String> impressionIds;

    private ImpressionRequest(List<String> list) {
        super(IMPRESSION_PATH);
        this.impressionIds = list;
    }

    static void start(List<String> list) {
        submit(new ImpressionRequest(list));
    }

    /* access modifiers changed from: package-private */
    public JSONObject addTo(JSONObject jSONObject) throws JSONException {
        jSONObject.put("impression_ids", new JSONArray((Collection) this.impressionIds));
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void onFailure(String str, Throwable th) {
        TapForTapLog.d(TAG, "Failed to log impressions: " + str);
    }

    /* access modifiers changed from: package-private */
    public void onSuccess(JSONObject jSONObject) {
        TapForTapLog.d(TAG, "Successfully logged impressions.");
    }
}
