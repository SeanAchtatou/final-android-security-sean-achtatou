package com.tapfortap;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;

class DiskCache {
    private static final String TAG = DiskCache.class.getName();
    private static long cacheSize;
    private static boolean hasCacheDir = true;
    private static File rootDirectory;
    private static long size = 0;

    DiskCache() {
    }

    private static String fetch(String str) throws IOException {
        TapForTapLog.d(TAG, "Fetching ad from url: " + str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setDoInput(true);
        httpURLConnection.connect();
        put(str, httpURLConnection.getInputStream());
        return getFileForUrl(str).getAbsolutePath();
    }

    static String get(String str) throws IOException {
        if (!hasCacheDir) {
            return null;
        }
        File fileForUrl = getFileForUrl(str);
        if (!fileForUrl.exists()) {
            return fetch(str);
        }
        fileForUrl.setLastModified(System.currentTimeMillis());
        return fileForUrl.getAbsolutePath();
    }

    private static File getFileForUrl(String str) {
        return new File(rootDirectory, str.replaceAll("[\\\\/:\"*?<>|]+", ""));
    }

    static void initialize(File file, long j) {
        if (file == null) {
            hasCacheDir = false;
            return;
        }
        rootDirectory = file;
        cacheSize = j;
        if (!file.exists() && !file.mkdirs()) {
            TapForTapLog.e(TAG, "Unable to create cache dir " + file.getAbsolutePath());
        }
        File[] listFiles = file.listFiles();
        int length = listFiles.length;
        for (int i = 0; i < length; i++) {
            size += listFiles[i].length();
        }
        prune(0);
    }

    private static void prune(long j) {
        if (size + j >= cacheSize) {
            File[] listFiles = rootDirectory.listFiles();
            Arrays.sort(listFiles, new Comparator<File>() {
                public int compare(File file, File file2) {
                    double lastModified = (double) (file.lastModified() - file2.lastModified());
                    if (lastModified < 0.0d) {
                        return -1;
                    }
                    return lastModified > 0.0d ? 1 : 0;
                }
            });
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                File file = listFiles[i];
                if (size - file.length() > cacheSize) {
                    long length2 = file.length();
                    if (file.delete()) {
                        size -= length2;
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    private static void put(String str, InputStream inputStream) throws IOException {
        prune(Utils.copyStream(new BufferedInputStream(inputStream, 8192), new BufferedOutputStream(new FileOutputStream(getFileForUrl(str)))));
    }
}
