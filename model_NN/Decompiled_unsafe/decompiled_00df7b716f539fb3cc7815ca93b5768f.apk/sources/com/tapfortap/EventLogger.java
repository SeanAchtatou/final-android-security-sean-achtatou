package com.tapfortap;

import android.content.Context;
import android.content.SharedPreferences;
import com.tapfortap.EventLoggerRequest;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

final class EventLogger {
    private static final int BUFFER_SIZE = 10;
    private static final String SHARED_PREFERENCES_NAME = "com.tapfortap.action_logger";
    /* access modifiers changed from: private */
    public static final String TAG = EventLogger.class.getName();
    /* access modifiers changed from: private */
    public static SharedPreferences sharedPreferences;
    /* access modifiers changed from: private */
    public static final Object sharedPreferencesLock = new Object();

    public enum Category {
        IN_APP_PURCHASE("In app purchase");
        
        private final String name;

        private Category(String str) {
            this.name = str;
        }

        /* access modifiers changed from: package-private */
        public String getName() {
            return this.name;
        }
    }

    EventLogger() {
    }

    static void initialize(Context context) {
        sharedPreferences = context.getApplicationContext().getSharedPreferences(SHARED_PREFERENCES_NAME, 0);
        sendLoggedActions(true);
    }

    public static void logEvent(Category category, String str) {
        logEvent(category.getName(), str);
    }

    public static void logEvent(String str, String str2) {
        synchronized (sharedPreferencesLock) {
            JSONObject jSONObject = new JSONObject();
            String valueOf = String.valueOf(System.currentTimeMillis());
            try {
                jSONObject.put("timestamp", valueOf);
                jSONObject.put("name", str);
                jSONObject.put("value", str2);
                sharedPreferences.edit().putString(valueOf, jSONObject.toString()).commit();
            } catch (JSONException e) {
                TapForTapLog.d(TAG, "Failed to addTo action.", e);
                return;
            }
        }
        sendLoggedActions(false);
    }

    private static void sendLoggedActions(boolean z) {
        final HashMap hashMap = new HashMap();
        synchronized (sharedPreferencesLock) {
            Map<String, ?> all = sharedPreferences.getAll();
            if (z || all.size() >= 10) {
                SharedPreferences.Editor edit = sharedPreferences.edit();
                for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
                    hashMap.put(next.getKey(), (String) next.getValue());
                    edit.remove((String) next.getKey());
                }
                edit.commit();
                EventLoggerRequest.startRequest(hashMap, new EventLoggerRequest.ResponseListener() {
                    public void onFailure(String str, Throwable th) {
                        synchronized (EventLogger.sharedPreferencesLock) {
                            SharedPreferences.Editor edit = EventLogger.sharedPreferences.edit();
                            for (Map.Entry entry : hashMap.entrySet()) {
                                edit.putString((String) entry.getKey(), (String) entry.getValue());
                            }
                            edit.commit();
                        }
                    }

                    public void onSuccess() {
                        TapForTapLog.v(EventLogger.TAG, "onSuccess");
                    }
                });
            }
        }
    }
}
