package com.tapfortap;

import android.content.Context;
import android.os.PowerManager;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ViewFlipper;
import com.tapfortap.AdRequest;
import com.tapfortap.TapForTap;
import org.json.JSONException;

public final class Banner extends ViewFlipper {
    private static final String BANNER_PATH = "ad/banner";
    /* access modifiers changed from: private */
    public static final String TAG = Banner.class.getName();
    private boolean autoRollover = true;
    /* access modifiers changed from: private */
    public BannerAd banner1;
    /* access modifiers changed from: private */
    public BannerAd banner2;
    /* access modifiers changed from: private */
    public BannerListener bannerListener;
    private boolean forceLoad = false;
    private GestureDetector gestureDetector;
    private TapForTap.InitializationListener initializeResponseListener;
    private PowerManager powerManager;
    private RolloverTimerTask rolloverTask;
    /* access modifiers changed from: private */
    public boolean show1 = true;
    private boolean stopped = false;

    public interface BannerListener {
        void bannerOnFail(Banner banner, String str, Throwable th);

        void bannerOnReceive(Banner banner);

        void bannerOnTap(Banner banner);
    }

    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private GestureListener() {
        }

        public boolean onSingleTapUp(MotionEvent motionEvent) {
            ((BannerAd) Banner.this.getCurrentView()).onTap(motionEvent.getX(), motionEvent.getY());
            return true;
        }
    }

    private class WebBannerAdClient extends WebViewClient {
        private WebBannerAdClient() {
        }

        public void onPageFinished(WebView webView, String str) {
            Banner.this.showNext();
            Banner.this.postOnReceive();
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            TapForTapLog.e(Banner.TAG, str + ":" + str2);
            Banner.this.postOnFailureToReceive(str + ":" + str2, null);
        }
    }

    public Banner(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, new BannerListener() {
            public void bannerOnFail(Banner banner, String str, Throwable th) {
            }

            public void bannerOnReceive(Banner banner) {
            }

            public void bannerOnTap(Banner banner) {
            }
        });
        startShowingAds();
    }

    private Banner(Context context, BannerListener bannerListener2) {
        super(context);
        init(context, bannerListener2);
    }

    public static Banner create(Context context) {
        return create(context, DefaultBannerListener.DEFAULT_LISTENER);
    }

    public static Banner create(Context context, BannerListener bannerListener2) {
        Banner banner = new Banner(context, bannerListener2);
        if (!banner.isInEditMode()) {
            banner.startShowingAds();
        }
        return banner;
    }

    private BannerAd createBannerAd(Context context, BannerListener bannerListener2) {
        BannerAd bannerAd = new BannerAd(context, new WebBannerAdClient(), bannerListener2);
        bannerAd.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        return bannerAd;
    }

    /* access modifiers changed from: private */
    public void getBannerAd() {
        if (!this.powerManager.isScreenOn()) {
            tryIn(5000);
        } else if (isShown() || this.forceLoad) {
            AdRequest.start(BANNER_PATH, new AdRequest.ResponseListener() {
                public void onFailure(String str, Throwable th) {
                    Banner.this.postOnFailureToReceive(str, th);
                }

                public void onSuccess(Ad ad) {
                    if (Banner.this.show1) {
                        Banner.this.banner1.setAd(ad);
                        Banner.this.banner2.setAd(null);
                    } else {
                        Banner.this.banner1.setAd(null);
                        Banner.this.banner2.setAd(ad);
                    }
                    try {
                        ImpressionRequest.start(ad.getImpressionIds());
                    } catch (JSONException e) {
                        TapForTapLog.e(Banner.TAG, "Failed to log impression.", e);
                    }
                    boolean unused = Banner.this.show1 = !Banner.this.show1;
                    boolean unused2 = Banner.this.tryIn(ad.getRefreshRate() * 1000);
                }
            });
        } else {
            tryIn(1000);
        }
    }

    private TranslateAnimation getDefaultInAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 1.0f, 2, 0.0f);
        translateAnimation.setDuration(750);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setFillAfter(true);
        return translateAnimation;
    }

    private TranslateAnimation getDefaultOutAnimation() {
        TranslateAnimation translateAnimation = new TranslateAnimation(2, 0.0f, 2, 0.0f, 2, 0.0f, 2, -1.0f);
        translateAnimation.setDuration(750);
        translateAnimation.setFillEnabled(true);
        translateAnimation.setFillAfter(true);
        return translateAnimation;
    }

    private TapForTap.InitializationListener getInitializeResponseListener() {
        return new TapForTap.InitializationListener() {
            public void onFail(String str, Throwable th) {
                Banner.this.postOnFailureToReceive("Failed to initialize", th);
            }

            public void onSuccess(boolean z) {
                Banner.this.getBannerAd();
            }
        };
    }

    private void init(Context context, BannerListener bannerListener2) {
        if (isInEditMode()) {
            this.initializeResponseListener = null;
            this.bannerListener = null;
            this.banner1 = null;
            this.banner2 = null;
            this.rolloverTask = null;
            this.powerManager = null;
            return;
        }
        this.bannerListener = bannerListener2;
        this.gestureDetector = new GestureDetector(getContext().getApplicationContext(), new GestureListener());
        setInAnimation(getDefaultInAnimation());
        setOutAnimation(getDefaultOutAnimation());
        this.banner1 = createBannerAd(context, bannerListener2);
        this.banner2 = createBannerAd(context, bannerListener2);
        addView(this.banner2);
        addView(this.banner1);
        this.initializeResponseListener = getInitializeResponseListener();
        this.rolloverTask = new RolloverTimerTask(new Runnable() {
            public void run() {
                Banner.this.getBannerAd();
            }
        });
        this.powerManager = (PowerManager) getContext().getSystemService("power");
    }

    /* access modifiers changed from: private */
    public boolean tryIn(long j) {
        if (!this.autoRollover || this.stopped) {
            return false;
        }
        this.rolloverTask.tryIn(j);
        return true;
    }

    public void disableAutoRollover() {
        this.autoRollover = false;
    }

    public void disableForceLoad() {
        this.forceLoad = false;
    }

    public void enableAutoRollover() {
        this.autoRollover = true;
    }

    public void enableForceLoad() {
        this.forceLoad = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        try {
            super.onDetachedFromWindow();
        } catch (IllegalArgumentException e) {
            stopFlipping();
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.gestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        float f = getResources().getDisplayMetrics().density;
        if (((double) size) / (320.0d * ((double) f)) <= ((double) size2) / (50.0d * ((double) f))) {
            size2 = (int) Math.ceil(0.15625d * ((double) size));
        } else {
            size = (int) Math.ceil((double) (size2 * 6));
        }
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            getChildAt(i3).measure(i, i2);
        }
        setMeasuredDimension(size, size2);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        this.gestureDetector.onTouchEvent(motionEvent);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void postOnFailureToReceive(final String str, final Throwable th) {
        post(new Runnable() {
            public void run() {
                Banner.this.bannerListener.bannerOnFail(Banner.this, str, th);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void postOnReceive() {
        post(new Runnable() {
            public void run() {
                Banner.this.bannerListener.bannerOnReceive(Banner.this);
            }
        });
    }

    public void setListener(BannerListener bannerListener2) {
        if (bannerListener2 == null) {
            this.bannerListener = DefaultBannerListener.DEFAULT_LISTENER;
            this.banner1.setResponseListener(DefaultBannerListener.DEFAULT_LISTENER);
            this.banner2.setResponseListener(DefaultBannerListener.DEFAULT_LISTENER);
            return;
        }
        this.bannerListener = bannerListener2;
        this.banner1.setResponseListener(bannerListener2);
        this.banner2.setResponseListener(bannerListener2);
    }

    public void startShowingAds() {
        if (!isInEditMode()) {
            stopShowingAds();
            this.stopped = false;
            TapForTap.initialize(getContext(), this.initializeResponseListener);
        }
    }

    public void stopShowingAds() {
        if (!isInEditMode()) {
            this.stopped = true;
            this.rolloverTask.stopTimerTask();
        }
    }
}
