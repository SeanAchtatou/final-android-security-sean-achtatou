package com.tapfortap;

import android.os.AsyncTask;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.json.JSONException;
import org.json.JSONObject;

class ApiRequestTask extends AsyncTask<ApiRequest, Void, Void> {
    private static final String TAG = ApiRequestTask.class.getName();

    ApiRequestTask() {
    }

    private void addHeaders(HttpPost httpPost) {
        if (!httpPost.containsHeader("Accept-Encoding")) {
            httpPost.addHeader("Accept-Encoding", "gzip");
        }
        if (!httpPost.containsHeader("Accept")) {
            httpPost.addHeader("Accept", "application/json");
        }
        if (!httpPost.containsHeader("Content-type")) {
            httpPost.addHeader("Content-type", "application/json");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private JSONObject createJSONPayload() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        if (TapForTap.isInTestMode()) {
            jSONObject.put("test", true);
        }
        jSONObject.put("format", "json");
        jSONObject.put("platform_id", "android");
        jSONObject.put("api_key", TapForTap.getApiKey());
        jSONObject.put("sdk_version", "3.0.8");
        jSONObject.put("plugin", TapForTap.PLUGIN);
        jSONObject.put("plugin_version", TapForTap.PLUGIN_VERSION);
        jSONObject.put("bundle_id", IdInfo.getGetPackageName());
        try {
            jSONObject.put("asset_version", AssetManager.getVersion());
        } catch (IOException e) {
            jSONObject.put("asset_version", "");
        }
        jSONObject.put("orientation", DeviceInfo.getOrientation());
        FormatsInfo.addTo(jSONObject);
        JSONObject jSONObject2 = new JSONObject();
        IdInfo.addTo(jSONObject2);
        DeviceInfo.addTo(jSONObject2);
        UserInfo.addTo(jSONObject2);
        jSONObject.put("device", jSONObject2);
        return jSONObject;
    }

    private InputStream getInputStream(HttpResponse httpResponse) throws IOException {
        InputStream content = httpResponse.getEntity().getContent();
        Header firstHeader = httpResponse.getFirstHeader("Content-Encoding");
        return (firstHeader == null || !firstHeader.getValue().equalsIgnoreCase("gzip")) ? content : new GZIPInputStream(content);
    }

    private HttpPost getPostRequest(ApiRequest apiRequest) throws JSONException, UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(apiRequest.getPath());
        addHeaders(httpPost);
        JSONObject createJSONPayload = createJSONPayload();
        apiRequest.addTo(createJSONPayload);
        StringEntity stringEntity = new StringEntity(createJSONPayload.toString());
        stringEntity.setContentType(new BasicHeader("Content-Type", "application/json"));
        httpPost.setEntity(stringEntity);
        return httpPost;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00d8 A[SYNTHETIC, Splitter:B:46:0x00d8] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void doInBackground(com.tapfortap.ApiRequest... r7) {
        /*
            r6 = this;
            r2 = 0
            r0 = 0
            r3 = r7[r0]
            java.lang.Boolean r0 = com.tapfortap.TapForTap.isDisabled()
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0021
            java.lang.String r0 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r1 = "Tap for Tap is disabled."
            com.tapfortap.TapForTapLog.e(r0, r1)
            java.lang.String r0 = "Tap for Tap is disabled."
            java.lang.Exception r1 = new java.lang.Exception
            java.lang.String r4 = "Tap for Tap is disabled."
            r1.<init>(r4)
            r3.onFailure(r0, r1)
        L_0x0021:
            org.apache.http.client.methods.HttpPost r0 = r6.getPostRequest(r3)     // Catch:{ Exception -> 0x0069 }
            org.apache.http.impl.client.DefaultHttpClient r1 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0077 }
            r1.<init>()     // Catch:{ Exception -> 0x0077 }
            org.apache.http.HttpResponse r0 = r1.execute(r0)     // Catch:{ Exception -> 0x0077 }
            java.io.InputStream r1 = r6.getInputStream(r0)     // Catch:{ Exception -> 0x00b7, all -> 0x00d4 }
            java.util.Scanner r0 = new java.util.Scanner     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r4 = "UTF-8"
            r0.<init>(r1, r4)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r4 = "\\A"
            java.util.Scanner r0 = r0.useDelimiter(r4)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r0 = r0.next()     // Catch:{ Exception -> 0x00e7 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x00e7 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r0 = "status"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r5 = "fail"
            boolean r0 = r0.equals(r5)     // Catch:{ Exception -> 0x00e7 }
            if (r0 == 0) goto L_0x008e
            java.lang.String r0 = com.tapfortap.ApiRequestTask.TAG     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r4 = "Failed to request ad."
            com.tapfortap.TapForTapLog.e(r0, r4)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r0 = "Failed to request ad."
            r4 = 0
            r3.onFailure(r0, r4)     // Catch:{ Exception -> 0x00e7 }
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x0085 }
        L_0x0068:
            return r2
        L_0x0069:
            r0 = move-exception
            java.lang.String r1 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r4 = "Failed to create post request."
            com.tapfortap.TapForTapLog.e(r1, r4)
            java.lang.String r1 = "Failed to create post request."
            r3.onFailure(r1, r0)
            goto L_0x0068
        L_0x0077:
            r0 = move-exception
            java.lang.String r1 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r4 = "Failed to execute post request."
            com.tapfortap.TapForTapLog.e(r1, r4)
            java.lang.String r1 = "Failed to execute post request."
            r3.onFailure(r1, r0)
            goto L_0x0068
        L_0x0085:
            r0 = move-exception
            java.lang.String r1 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r3 = "Failed to close input stream."
            com.tapfortap.TapForTapLog.v(r1, r3, r0)
            goto L_0x0068
        L_0x008e:
            java.lang.String r0 = "assets"
            boolean r0 = r4.has(r0)     // Catch:{ Exception -> 0x00e7 }
            if (r0 == 0) goto L_0x00a5
            java.lang.String r0 = "asset_version"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x00e7 }
            java.lang.String r5 = "assets"
            org.json.JSONObject r5 = r4.getJSONObject(r5)     // Catch:{ Exception -> 0x00e7 }
            com.tapfortap.AssetManager.writeAssets(r0, r5)     // Catch:{ Exception -> 0x00e7 }
        L_0x00a5:
            r3.onSuccess(r4)     // Catch:{ Exception -> 0x00e7 }
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x00ae }
            goto L_0x0068
        L_0x00ae:
            r0 = move-exception
            java.lang.String r1 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r3 = "Failed to close input stream."
            com.tapfortap.TapForTapLog.v(r1, r3, r0)
            goto L_0x0068
        L_0x00b7:
            r0 = move-exception
            r1 = r2
        L_0x00b9:
            java.lang.String r4 = com.tapfortap.ApiRequestTask.TAG     // Catch:{ all -> 0x00e5 }
            java.lang.String r5 = "Failed to read response."
            com.tapfortap.TapForTapLog.e(r4, r5, r0)     // Catch:{ all -> 0x00e5 }
            java.lang.String r4 = "Failed to read response."
            r3.onFailure(r4, r0)     // Catch:{ all -> 0x00e5 }
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ IOException -> 0x00cb }
            goto L_0x0068
        L_0x00cb:
            r0 = move-exception
            java.lang.String r1 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r3 = "Failed to close input stream."
            com.tapfortap.TapForTapLog.v(r1, r3, r0)
            goto L_0x0068
        L_0x00d4:
            r0 = move-exception
            r1 = r2
        L_0x00d6:
            if (r1 == 0) goto L_0x00db
            r1.close()     // Catch:{ IOException -> 0x00dc }
        L_0x00db:
            throw r0
        L_0x00dc:
            r1 = move-exception
            java.lang.String r2 = com.tapfortap.ApiRequestTask.TAG
            java.lang.String r3 = "Failed to close input stream."
            com.tapfortap.TapForTapLog.v(r2, r3, r1)
            goto L_0x00db
        L_0x00e5:
            r0 = move-exception
            goto L_0x00d6
        L_0x00e7:
            r0 = move-exception
            goto L_0x00b9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapfortap.ApiRequestTask.doInBackground(com.tapfortap.ApiRequest[]):java.lang.Void");
    }
}
