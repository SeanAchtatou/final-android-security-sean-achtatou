package com.tapfortap;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

class TapForTapLog {
    private static final String LOG_LEVEL_META_DATA = "LOG_LEVEL";
    private static LogLevel logLevel = LogLevel.ERROR;

    enum LogLevel {
        OFF(-1),
        VERBOSE(0),
        DEBUG(1),
        INFO(2),
        WARNING(3),
        ERROR(4);
        
        private final int level;

        private LogLevel(int i) {
            this.level = i;
        }
    }

    private static class NoLogLevelFile extends Exception {
        private NoLogLevelFile() {
        }
    }

    TapForTapLog() {
    }

    static void d(String str, String str2) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.DEBUG)) {
            Log.d(str, str2);
        }
    }

    static void d(String str, String str2, Throwable th) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.DEBUG)) {
            Log.d(str, str2, th);
        }
    }

    static void e(String str, String str2) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.ERROR)) {
            Log.e(str, str2);
        }
    }

    static void e(String str, String str2, Throwable th) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.ERROR)) {
            Log.e(str, str2, th);
        }
    }

    private static LogLevel getLogLevelFromFile() throws NoLogLevelFile {
        try {
            return stringToLogLevel(new BufferedReader(new InputStreamReader(new FileInputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "tapfortap_log_level")))).readLine());
        } catch (FileNotFoundException e) {
            throw new NoLogLevelFile();
        } catch (IOException e2) {
            throw new NoLogLevelFile();
        }
    }

    private static LogLevel getLogLevelFromMetaData(Context context) throws PackageManager.NameNotFoundException {
        return stringToLogLevel(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(LOG_LEVEL_META_DATA));
    }

    static void i(String str, String str2) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.INFO)) {
            Log.i(str, str2);
        }
    }

    static void i(String str, String str2, Throwable th) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.INFO)) {
            Log.i(str, str2, th);
        }
    }

    static void initialize(Context context) {
        try {
            logLevel = getLogLevelFromMetaData(context);
        } catch (PackageManager.NameNotFoundException e) {
            try {
                logLevel = getLogLevelFromFile();
            } catch (NoLogLevelFile e2) {
                logLevel = LogLevel.WARNING;
            }
        }
    }

    static void initialize(LogLevel logLevel2) {
        logLevel = logLevel2;
    }

    static void setLogLevel(LogLevel logLevel2) {
        logLevel = logLevel2;
    }

    private static LogLevel stringToLogLevel(String str) {
        if ("VERBOSE".equals(str)) {
            return LogLevel.VERBOSE;
        }
        if ("DEBUG".equals(str)) {
            return LogLevel.DEBUG;
        }
        if ("INFO".equals(str)) {
            return LogLevel.INFO;
        }
        if ("WARNING".equals(str)) {
            return LogLevel.WARNING;
        }
        if ("ERROR".equals(str)) {
            return LogLevel.ERROR;
        }
        if ("OFF".equals(str)) {
            return LogLevel.OFF;
        }
        return null;
    }

    static void v(String str, String str2) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.VERBOSE)) {
            Log.v(str, str2);
        }
    }

    static void v(String str, String str2, Throwable th) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.VERBOSE)) {
            Log.v(str, str2, th);
        }
    }

    static void w(String str, String str2) {
        if (LogLevel.access$100(logLevel) >= LogLevel.access$100(LogLevel.WARNING)) {
            Log.w(str, str2);
        }
    }

    static void w(String str, String str2, Throwable th) {
        if (LogLevel.access$100(logLevel) <= LogLevel.access$100(LogLevel.WARNING)) {
            Log.w(str, str2, th);
        }
    }
}
