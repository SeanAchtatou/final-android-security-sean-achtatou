package com.tapfortap;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import java.io.IOException;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class FullScreenAdActivity extends Activity {
    /* access modifiers changed from: private */
    public static final String TAG = FullScreenAdActivity.class.getName();
    private Ad ad;
    private boolean impressionLogged;
    /* access modifiers changed from: private */
    public String type;
    /* access modifiers changed from: private */
    public WebView webView;

    private class TapForTapWebViewClient extends WebViewClient {
        private TapForTapWebViewClient() {
        }

        public void onPageFinished(WebView webView, String str) {
            FullScreenAdActivity.this.webView.setBackgroundColor(17170445);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            FullScreenAdActivity.this.broadcastError(str + ": " + str2);
            FullScreenAdActivity.this.finish();
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            String str2;
            TapForTapLog.d(FullScreenAdActivity.TAG, str);
            if (URIUtils.loadUrl(FullScreenAdActivity.this, str)) {
                return true;
            }
            if (!str.startsWith("tapfortap://")) {
                return false;
            }
            String replace = str.replace("tapfortap://", "");
            if (replace.contains("?")) {
                str2 = replace.substring(0, replace.indexOf("?"));
                replace = replace.substring(replace.indexOf("?") + 1);
            } else {
                str2 = replace;
            }
            Map<String, String> decodeUrl = URIUtils.decodeUrl(replace);
            if (str2.equals("getData")) {
                FullScreenAdActivity.this.getData();
            } else if (str2.equals("logImpression")) {
                FullScreenAdActivity.this.logImpression();
            } else if (str2.equals("tap")) {
                FullScreenAdActivity.this.tap(decodeUrl);
                return true;
            } else if (str2.equals("back")) {
                FullScreenAdActivity.this.onBackPressed();
            } else if (str2.equals("dismiss")) {
                FullScreenAdActivity.this.finish();
                FullScreenAdActivity.this.sendEventBroadcast("dismiss", "");
            } else if (str2.equals("error")) {
                FullScreenAdActivity.this.finish();
                FullScreenAdActivity.this.broadcastError(decodeUrl.get("message"));
            }
            return true;
        }
    }

    private void back() {
        if (Build.VERSION.SDK_INT < 11) {
            this.webView.setVisibility(8);
        }
        dimBackground(0.0f);
        this.webView.loadUrl("javascript:dismiss()");
    }

    /* access modifiers changed from: private */
    public void broadcastError(String str) {
        sendEventBroadcast("error", str);
    }

    private void dimBackground(float f) {
        getWindow().setLayout(-1, -1);
        getWindow().getAttributes().dimAmount = f;
        getWindow().addFlags(2);
    }

    /* access modifiers changed from: private */
    public void getData() {
        String str = null;
        try {
            str = "{ dimensions : { width : " + this.webView.getWidth() + "," + "height : " + this.webView.getHeight() + "," + "density : " + getResources().getDisplayMetrics().density + "}, " + "ads : " + this.ad.getAdsAsString() + "}";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.webView.loadUrl("javascript:setData(" + str + ")");
    }

    /* access modifiers changed from: private */
    public void logImpression() {
        if (!this.impressionLogged) {
            try {
                ImpressionRequest.start(this.ad.getImpressionIds());
                this.impressionLogged = true;
                sendEventBroadcast("show", "");
            } catch (JSONException e) {
                broadcastError("Failed to log impression.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendEventBroadcast(String str, String str2) {
        Intent intent = new Intent("com.tapfortap.full_screen_ad_event");
        try {
            intent.putExtra("impression_id", this.ad.getFirstImpressionId());
        } catch (JSONException e) {
            intent.putExtra("impression_id", "");
        }
        intent.putExtra("event", str);
        intent.putExtra("message", str2);
        sendBroadcast(intent);
    }

    private void setOrientation() {
        int i = 0;
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int rotation = defaultDisplay.getRotation();
        Point displaySize = Utils.getDisplaySize(defaultDisplay);
        if (rotation == 0 || rotation == 2) {
            if (displaySize.x <= displaySize.y) {
                i = rotation == 0 ? 1 : 9;
            } else if (rotation != 0) {
                i = 8;
            }
        } else if (displaySize.x <= displaySize.y) {
            i = rotation == 1 ? 9 : 1;
        } else if (rotation != 1) {
            i = 8;
        }
        setRequestedOrientation(i);
    }

    /* access modifiers changed from: private */
    public void tap(Map<String, String> map) {
        TapRequest.start(map.get("tap_id"), toFloat(map.get("x_coordinate")), toFloat(map.get("y_coordinate")));
        sendEventBroadcast("tap", "");
        String str = map.get("url");
        if (!URIUtils.loadUrl(this, str)) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        }
    }

    private float toFloat(String str) {
        try {
            return Float.valueOf(str).floatValue();
        } catch (Exception e) {
            return 0.0f;
        }
    }

    public void onBackPressed() {
        back();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setOrientation();
        Bundle extras = getIntent().getExtras();
        try {
            this.ad = new Ad(new JSONObject(extras.getString("com.tapfortap.full_screen_ad")));
        } catch (JSONException e) {
            broadcastError("Failed to parse ad.");
            finish();
        }
        this.type = extras.getString("com.tapfortap.full_screen_ad_type");
        this.impressionLogged = extras.getBoolean("com.tapfortap.full_screen_impression_logged");
        requestWindowFeature(1);
        this.webView = new WebView(this);
        this.webView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.webView.setWebViewClient(new TapForTapWebViewClient());
        this.webView.setWebChromeClient(new WebChromeClient() {
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                TapForTapLog.e(FullScreenAdActivity.TAG + "-" + FullScreenAdActivity.this.type + ".js ", String.format("%s @ %d: %s", consoleMessage.message(), Integer.valueOf(consoleMessage.lineNumber()), consoleMessage.sourceId()));
                if (consoleMessage.messageLevel().equals(ConsoleMessage.MessageLevel.ERROR)) {
                    try {
                        AssetManager.writeVersion("0");
                    } catch (IOException e) {
                    }
                    FullScreenAdActivity.this.finish();
                    FullScreenAdActivity.this.broadcastError("Javascript error");
                }
                return true;
            }

            public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
                TapForTapLog.e(FullScreenAdActivity.TAG, "JS alert from " + str + ": " + str2);
                return true;
            }
        });
        this.webView.setHorizontalScrollBarEnabled(false);
        this.webView.setVerticalScrollBarEnabled(false);
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setLoadsImagesAutomatically(true);
        this.webView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        this.webView.setBackgroundColor(17170445);
        if (Build.VERSION.SDK_INT >= 11) {
            this.webView.setLayerType(1, null);
        }
        FrameLayout frameLayout = new FrameLayout(this);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        frameLayout.addView(this.webView);
        dimBackground(0.8f);
        setContentView(frameLayout);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.webView.loadUrl("about:blank");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        try {
            this.webView.loadUrl("file://" + AssetManager.getAssetFor(this.type));
        } catch (IOException e) {
            broadcastError("Failed to load asset.");
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("com.tapfortap.full_screen_ad_type", this.type);
        bundle.putString("com.tapfortap.full_screen_ad", this.ad.getAdAsString());
        bundle.putBoolean("com.tapfortap.full_screen_impression_logged", this.impressionLogged);
        super.onSaveInstanceState(bundle);
    }
}
