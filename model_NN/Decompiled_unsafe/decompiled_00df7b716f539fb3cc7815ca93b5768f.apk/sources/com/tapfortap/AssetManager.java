package com.tapfortap;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

class AssetManager {
    private static final String TAG = AssetManager.class.getName();
    private static File cacheDir;

    AssetManager() {
    }

    static String getAssetFor(String str) throws IOException {
        return getFile(str).getAbsolutePath();
    }

    private static File getFile(String str) throws IOException {
        File file = new File(cacheDir, "tft-" + str);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    static String getVersion() throws IOException {
        return read("version");
    }

    static void initialize(File file) {
        cacheDir = file;
    }

    private static String read(String str) throws IOException {
        return readFile(getFile(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0034  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String readFile(java.io.File r6) throws java.io.IOException {
        /*
            r2 = 0
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ all -> 0x0047 }
            java.lang.String r0 = r6.getAbsolutePath()     // Catch:{ all -> 0x0047 }
            r3.<init>(r0)     // Catch:{ all -> 0x0047 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ all -> 0x004a }
            r0 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r3, r0)     // Catch:{ all -> 0x004a }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x002b }
            r0.<init>()     // Catch:{ all -> 0x002b }
        L_0x0016:
            java.lang.String r2 = r1.readLine()     // Catch:{ all -> 0x002b }
            if (r2 == 0) goto L_0x0038
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x002b }
            byte[] r2 = r2.getBytes()     // Catch:{ all -> 0x002b }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r2, r5)     // Catch:{ all -> 0x002b }
            r0.append(r4)     // Catch:{ all -> 0x002b }
            goto L_0x0016
        L_0x002b:
            r0 = move-exception
            r2 = r3
        L_0x002d:
            if (r2 == 0) goto L_0x0032
            r2.close()
        L_0x0032:
            if (r1 == 0) goto L_0x0037
            r1.close()
        L_0x0037:
            throw r0
        L_0x0038:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x002b }
            if (r3 == 0) goto L_0x0041
            r3.close()
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()
        L_0x0046:
            return r0
        L_0x0047:
            r0 = move-exception
            r1 = r2
            goto L_0x002d
        L_0x004a:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tapfortap.AssetManager.readFile(java.io.File):java.lang.String");
    }

    private static void write(String str, String str2) throws IOException {
        FileWriter fileWriter = new FileWriter(getFile(str));
        fileWriter.write(str2);
        fileWriter.close();
    }

    static void writeAssets(String str, JSONObject jSONObject) throws JSONException, IOException {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            write(next, jSONObject.getString(next));
        }
        writeVersion(str);
    }

    static void writeVersion(String str) throws IOException {
        write("version", str);
    }
}
