package com.tapfortap;

import android.app.Activity;
import android.content.Context;
import com.tapfortap.FullScreenAd;

public final class AppWall {
    private static final String APP_WALL_PATH = "ad/app-wall";
    private static final String TAG = AppWall.class.getName();
    private FullScreenAd fullScreenAd;

    public interface AppWallListener {
        void appWallOnDismiss(AppWall appWall);

        void appWallOnFail(AppWall appWall, String str, Throwable th);

        void appWallOnReceive(AppWall appWall);

        void appWallOnShow(AppWall appWall);

        void appWallOnTap(AppWall appWall);
    }

    private AppWall() {
    }

    public static AppWall create(Context context) {
        return create(context, DefaultAppWallListener.DEFAULT_LISTENER);
    }

    public static AppWall create(Context context, final AppWallListener appWallListener) {
        if (appWallListener instanceof Activity) {
            TapForTapLog.w(TAG, "Tap for Tap suggests not using an Activity as a response listener as it may cause the Activity to leak. Please use an anonymous inner class or a regular class. Visit tapfortap.com/doc/android for more details.");
        }
        final AppWall appWall = new AppWall();
        appWall.fullScreenAd = FullScreenAd.create(context, APP_WALL_PATH, FullScreenAd.FullScreenAdType.APP_WALL.getType(), new FullScreenAd.ResponseListener() {
            public void fullScreenOnDismiss() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        appWallListener.appWallOnDismiss(appWall);
                    }
                });
            }

            public void fullScreenOnFail(final String str, final Throwable th) {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        appWallListener.appWallOnFail(appWall, str, th);
                    }
                });
            }

            public void fullScreenOnReceive() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        appWallListener.appWallOnReceive(appWall);
                    }
                });
            }

            public void fullScreenOnShow() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        appWallListener.appWallOnShow(appWall);
                    }
                });
            }

            public void fullScreenOnTap() {
                Utils.postToMainLooper(new Runnable() {
                    public void run() {
                        appWallListener.appWallOnTap(appWall);
                    }
                });
            }
        });
        appWall.load();
        return appWall;
    }

    public boolean isReadyToShow() {
        return this.fullScreenAd.isReadyToShow();
    }

    public void load() {
        this.fullScreenAd.load();
    }

    public void show() {
        this.fullScreenAd.show();
    }

    public void showAndLoad() {
        this.fullScreenAd.showAndLoadNext();
    }
}
