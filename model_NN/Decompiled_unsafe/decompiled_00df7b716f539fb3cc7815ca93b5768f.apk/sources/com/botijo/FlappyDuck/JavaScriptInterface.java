package com.botijo.FlappyDuck;

import android.app.Activity;
import android.content.SharedPreferences;
import java.util.Map;

public class JavaScriptInterface {
    private Activity activity;
    SharedPreferences p = null;

    public JavaScriptInterface(Activity activiy, SharedPreferences p2) {
        this.activity = activiy;
        this.p = p2;
    }

    public void exit() {
        ((FlappyDuck) this.activity).myFinish();
    }

    public String getCookie(String param) {
        return this.p.getString(param, "");
    }

    public void setCookie(String param, String value) {
        SharedPreferences.Editor editor = this.p.edit();
        editor.putString(param, value);
        editor.commit();
    }

    public String listCookies() {
        String lista = "";
        boolean primero = true;
        for (Map.Entry<String, ?> entry : this.p.getAll().entrySet()) {
            if (primero) {
                lista = String.valueOf((String) entry.getKey()) + "=" + entry.getValue().toString();
                primero = false;
            } else {
                lista = String.valueOf(lista) + "|" + ((String) entry.getKey()) + "=" + entry.getValue().toString();
            }
        }
        return lista;
    }
}
