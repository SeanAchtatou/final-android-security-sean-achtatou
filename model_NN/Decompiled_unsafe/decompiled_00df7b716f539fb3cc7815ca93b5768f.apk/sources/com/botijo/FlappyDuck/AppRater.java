package com.botijo.FlappyDuck;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AppRater {
    public static String APP_PNAME = "YOUR-PACKAGE-NAME";
    public static String APP_TITLE = "YOUR-APP-NAME";
    public static int DAYS_UNTIL_PROMPT = 0;
    public static int LAUNCHES_UNTIL_PROMPT = 0;

    public static void init(String titulo, String packageName, int days, int uses) {
        APP_TITLE = titulo;
        APP_PNAME = packageName;
        DAYS_UNTIL_PROMPT = days;
        LAUNCHES_UNTIL_PROMPT = uses;
    }

    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (!prefs.getBoolean("dontshowagain", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            long launch_count = prefs.getLong("launch_count", 0) + 1;
            editor.putLong("launch_count", launch_count);
            Long date_firstLaunch = Long.valueOf(prefs.getLong("date_firstlaunch", 0));
            if (date_firstLaunch.longValue() == 0) {
                date_firstLaunch = Long.valueOf(System.currentTimeMillis());
                editor.putLong("date_firstlaunch", date_firstLaunch.longValue());
            }
            if (launch_count >= ((long) LAUNCHES_UNTIL_PROMPT) && System.currentTimeMillis() >= date_firstLaunch.longValue() + ((long) (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000))) {
                showRateDialog(mContext, editor);
            }
            editor.commit();
        }
    }

    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {
        if (!mContext.getSharedPreferences("apprater", 0).getBoolean("dontshowagain", false)) {
            final Dialog dialog = new Dialog(mContext);
            dialog.setTitle("Rate " + APP_TITLE);
            LinearLayout ll = new LinearLayout(mContext);
            ll.setOrientation(1);
            TextView tv = new TextView(mContext);
            tv.setText("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!");
            tv.setWidth(240);
            tv.setPadding(4, 0, 4, 10);
            ll.addView(tv);
            Button b1 = new Button(mContext);
            b1.setText("Rate " + APP_TITLE);
            b1.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mContext.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + AppRater.APP_PNAME)));
                    dialog.dismiss();
                }
            });
            ll.addView(b1);
            Button b2 = new Button(mContext);
            b2.setText("Remind me later");
            b2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            ll.addView(b2);
            Button b3 = new Button(mContext);
            b3.setText("No, thanks");
            b3.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (editor != null) {
                        editor.putBoolean("dontshowagain", true);
                        editor.commit();
                    }
                    dialog.dismiss();
                }
            });
            ll.addView(b3);
            dialog.setContentView(ll);
            dialog.show();
        }
    }
}
