package com.botijo.FlappyDuck;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.tapfortap.Interstitial;
import com.tapfortap.TapForTap;
import java.util.Locale;
import java.util.Random;

public class FlappyDuck extends Activity {
    protected static final String TAG = "Tapfortap";
    public static boolean establet = false;
    String country = "";
    Interstitial interstitial = null;
    int intertime1 = 40;
    int intertime2 = 160;
    JavaScriptInterface jsInterface = null;
    SharedPreferences preferences = null;
    String title = "Flappy Duck";
    String titleEN = "Flappy Duck";
    String titleSP = "Patito contra Zombies";
    String url = "http://mphasis.net/flapy/";
    String urlEN = "http://mphasis.net/flapy/index.html?sss";
    String urlSP = "http://mphasis.net/flapy/index.html?asd";
    WebView webview;

    public boolean onCreateOptionsMenu(Menu menu) {
        if (this.country != "SP") {
            menu.add(0, 1, 1, "Home");
            menu.add(0, 4, 1, "Rate this app");
            menu.add(0, 5, 1, "Exit");
        } else {
            menu.add(0, 1, 1, "Inicio");
            menu.add(0, 4, 1, "Puntuar");
            menu.add(0, 5, 1, "Salir");
        }
        return true;
    }

    public void myFinish() {
        showOfferwall();
        finish();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 1:
                this.webview.loadUrl(this.url);
                return true;
            case 2:
                this.webview.loadUrl(String.valueOf(this.url) + "/menu");
                return true;
            case 3:
                this.webview.loadUrl(String.valueOf(this.url) + "/highlights");
                return true;
            case 4:
                AppRater.showRateDialog(this, null);
                return true;
            case 5:
                myFinish();
                return true;
            default:
                return false;
        }
    }

    private void showOfferwall() {
        this.interstitial.show();
    }

    public void onBackPressed() {
        if (this.webview.canGoBack()) {
            this.webview.goBack();
            return;
        }
        String titleDlg = "Close App";
        String msg = "Are you sure you want to close this app?(After closing you will see a sponsored apps list)";
        String y = "Yes";
        if (this.country == "SP") {
            msg = "Está seguro que desea salir?(Después aparecerá un listado de aplicaciones patrocinadas)";
            y = "Sí";
            titleDlg = "Cerrar App";
        }
        new AlertDialog.Builder(this).setIcon(17301543).setTitle(titleDlg).setMessage(msg).setPositiveButton(y, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                FlappyDuck.this.myFinish();
            }
        }).setNegativeButton("No", (DialogInterface.OnClickListener) null).show();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.url = this.urlEN;
        this.title = this.titleEN;
        if (this.urlSP != "") {
            this.country = Locale.getDefault().getDisplayCountry();
            if (this.country.indexOf("Espa") >= 0) {
                this.country = "SP";
                this.url = this.urlSP;
                this.title = this.titleSP;
            }
        }
        TapForTap.initialize(this, "b62cea278d3710ebd53dce64467db25c");
        this.interstitial = Interstitial.create(this, new Interstitial.InterstitialListener() {
            public void interstitialOnReceive(Interstitial interstitial) {
                Log.d(FlappyDuck.TAG, "InterstitialOnReceive");
            }

            public void interstitialOnFail(Interstitial interstitial, String reason, Throwable throwable) {
                Log.d(FlappyDuck.TAG, "InterstitialOnFail");
                interstitial.load();
            }

            public void interstitialOnShow(Interstitial interstitial) {
                Log.d(FlappyDuck.TAG, "InterstitialOnShow");
            }

            public void interstitialOnTap(Interstitial interstitial) {
                Log.d(FlappyDuck.TAG, "InterstitialOnTap");
            }

            public void interstitialOnDismiss(Interstitial interstitial) {
                Log.d(FlappyDuck.TAG, "InterstitialOnDismiss");
            }
        });
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        getWindow().requestFeature(2);
        setContentView(R.layout.main);
        Display display = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        if (width >= 799) {
            establet = true;
        }
        if (establet) {
            setRequestedOrientation(0);
        }
        this.webview = (WebView) findViewById(R.id.webview);
        this.webview.getSettings().setJavaScriptEnabled(true);
        this.webview.setScrollBarStyle(33554432);
        this.webview.getSettings().setJavaScriptEnabled(true);
        WebSettings settings = this.webview.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setCacheMode(2);
        settings.setDatabasePath(getApplicationContext().getDir("database", 0).getPath());
        settings.setDomStorageEnabled(true);
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading. Please wait...", true);
        this.webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                this.setProgress(progress * 100);
                if (progress >= 60) {
                    dialog.hide();
                }
            }

            public void onExceededDatabaseQuota(String url, String databaseIdentifier, long currentQuota, long estimatedSize, long totalUsedQuota, WebStorage.QuotaUpdater quotaUpdater) {
                quotaUpdater.updateQuota(1048576);
            }
        });
        this.webview.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                dialog.show();
            }

            public void onPageFinished(WebView view, String url) {
                dialog.hide();
                view.loadUrl("javascript:fijar2('#banner')");
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                if (errorCode == -9) {
                    FlappyDuck.this.webview.loadUrl(FlappyDuck.this.url);
                }
                Toast.makeText(this, "Oh no! " + description, 0).show();
                FlappyDuck.this.webview.loadUrl("file:///android_asset/error.html");
            }
        });
        this.webview.loadUrl(this.url);
        Context context = getApplicationContext();
        CharSequence text = "You can enjoy this app in your pc desktop or laptop at " + this.url;
        if (this.country != "SP") {
            Toast.makeText(context, text, 4).show();
        }
        AppRater.APP_PNAME = getApplicationContext().getPackageName();
        AppRater.APP_TITLE = this.title;
        AppRater.app_launched(this);
        this.preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        this.jsInterface = new JavaScriptInterface(this, this.preferences);
        this.webview.addJavascriptInterface(this.jsInterface, "JSInterface");
        final Handler h = new Handler();
        h.postDelayed(new Runnable() {
            private long time = 0;

            public void run() {
                h.postDelayed(this, (long) ((new Random().nextInt(FlappyDuck.this.intertime2 - FlappyDuck.this.intertime1) + FlappyDuck.this.intertime1) * 1000));
            }
        }, (long) ((new Random().nextInt(85) + 35) * 1000));
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int nextInt = new Random().nextInt(6601) + 65;
    }
}
