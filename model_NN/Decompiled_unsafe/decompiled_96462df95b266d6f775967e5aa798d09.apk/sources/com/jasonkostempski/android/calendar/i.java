package com.jasonkostempski.android.calendar;

import android.view.View;

final class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ CalendarView f308a;

    i(CalendarView calendarView) {
        this.f308a = calendarView;
    }

    public final void onClick(View view) {
        int[] iArr = (int[]) view.getTag();
        this.f308a.m.b(iArr[0], iArr[1]);
        CalendarView.i(this.f308a);
        this.f308a.a(1);
    }
}
