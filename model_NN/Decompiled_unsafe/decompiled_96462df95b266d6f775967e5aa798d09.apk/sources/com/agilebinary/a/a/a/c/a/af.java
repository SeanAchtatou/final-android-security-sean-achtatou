package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.h;
import java.io.Serializable;
import java.util.Date;

public final class af extends d implements h, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f25a;
    private int[] b;
    private boolean c;

    public af(String str, String str2) {
        super(str, str2);
    }

    public final void a(int[] iArr) {
        this.b = iArr;
    }

    public final void a_(String str) {
        this.f25a = str;
    }

    public final boolean b(Date date) {
        return this.c || super.b(date);
    }

    public final Object clone() {
        af afVar = (af) super.clone();
        afVar.b = (int[]) this.b.clone();
        return afVar;
    }

    public final int[] g() {
        return this.b;
    }

    public final void i() {
        this.c = true;
    }
}
