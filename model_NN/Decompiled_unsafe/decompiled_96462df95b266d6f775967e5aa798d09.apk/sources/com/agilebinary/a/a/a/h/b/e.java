package com.agilebinary.a.a.a.h.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.h.h;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public final class e implements a, i {

    /* renamed from: a  reason: collision with root package name */
    private static final e f102a = new e();
    private final b b = null;

    public static e b() {
        return f102a;
    }

    public final Socket a() {
        return new Socket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.a.e.e eVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            inetSocketAddress = new InetSocketAddress(inetAddress, i2 < 0 ? 0 : i2);
        }
        return a(socket, new InetSocketAddress(this.b != null ? this.b.a() : InetAddress.getByName(str), i), inetSocketAddress, eVar);
    }

    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, com.agilebinary.a.a.a.e.e eVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            Socket socket2 = socket == null ? new Socket() : socket;
            if (inetSocketAddress2 != null) {
                socket2.setReuseAddress(c.b(eVar));
                socket2.bind(inetSocketAddress2);
            }
            try {
                socket2.connect(inetSocketAddress, c.c(eVar));
                return socket2;
            } catch (SocketTimeoutException e) {
                throw new h("Connect to " + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!socket.isClosed()) {
            return false;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }

    public final Socket f_() {
        return new Socket();
    }
}
