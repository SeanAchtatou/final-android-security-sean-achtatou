package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.f;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.commons.logging.Log;

public final class b implements m {

    /* renamed from: a  reason: collision with root package name */
    private final Log f43a = a.a(getClass());
    private h b;

    public b(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.b = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private static void a(Socket socket, e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        socket.setTcpNoDelay(eVar.a("http.tcp.nodelay", true));
        socket.setSoTimeout(c.a(eVar));
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        int a2 = eVar.a("http.socket.linger", -1);
        if (a2 >= 0) {
            socket.setSoLinger(a2 > 0, a2);
        }
    }

    public final g a() {
        return new h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      com.agilebinary.a.a.a.h.b.i.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, com.agilebinary.a.a.a.e.e):java.net.Socket
      com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public final void a(g gVar, com.agilebinary.a.a.a.b bVar, e eVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!gVar.l()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            com.agilebinary.a.a.a.h.b.g a2 = this.b.a(bVar.c());
            if (!(a2.b() instanceof f)) {
                throw new IllegalArgumentException("Target scheme (" + a2.c() + ") must have layered socket factory.");
            }
            f fVar = (f) a2.b();
            try {
                Socket a3 = fVar.a(gVar.i_(), bVar.a(), bVar.b(), true);
                a(a3, eVar);
                gVar.a(a3, bVar, fVar.a(a3), eVar);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.a.h.f(bVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:40:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00da A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.g r12, com.agilebinary.a.a.a.b r13, java.net.InetAddress r14, com.agilebinary.a.a.a.e.e r15) {
        /*
            r11 = this;
            if (r12 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000a:
            if (r13 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Target host may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0014:
            if (r15 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Parameters may not be null"
            r0.<init>(r1)
            throw r0
        L_0x001e:
            boolean r0 = r12.l()
            if (r0 == 0) goto L_0x002c
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Connection must not be open"
            r0.<init>(r1)
            throw r0
        L_0x002c:
            com.agilebinary.a.a.a.h.b.h r0 = r11.b
            java.lang.String r1 = r13.c()
            com.agilebinary.a.a.a.h.b.g r0 = r0.a(r1)
            com.agilebinary.a.a.a.h.b.i r1 = r0.b()
            java.lang.String r2 = r13.a()
            java.net.InetAddress[] r2 = java.net.InetAddress.getAllByName(r2)
            int r3 = r13.b()
            int r0 = r0.a(r3)
            r3 = 0
        L_0x004b:
            int r4 = r2.length
            if (r3 >= r4) goto L_0x009e
            r4 = r2[r3]
            int r5 = r2.length
            r6 = 1
            int r5 = r5 - r6
            if (r3 != r5) goto L_0x009f
            r5 = 1
        L_0x0056:
            java.net.Socket r6 = r1.f_()
            r12.a(r6, r13)
            java.net.InetSocketAddress r7 = new java.net.InetSocketAddress
            r7.<init>(r4, r0)
            r4 = 0
            if (r14 == 0) goto L_0x006b
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress
            r8 = 0
            r4.<init>(r14, r8)
        L_0x006b:
            org.apache.commons.logging.Log r8 = r11.f43a
            boolean r8 = r8.isDebugEnabled()
            if (r8 == 0) goto L_0x008b
            org.apache.commons.logging.Log r8 = r11.f43a
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Connecting to "
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r7)
            java.lang.String r9 = r9.toString()
            r8.debug(r9)
        L_0x008b:
            java.net.Socket r4 = r1.a(r6, r7, r4, r15)     // Catch:{ ConnectException -> 0x00a1, h -> 0x00aa }
            if (r6 == r4) goto L_0x00de
            r12.a(r4, r13)     // Catch:{ ConnectException -> 0x00a1, h -> 0x00aa }
        L_0x0094:
            a(r4, r15)     // Catch:{ ConnectException -> 0x00a1, h -> 0x00aa }
            boolean r4 = r1.a(r4)     // Catch:{ ConnectException -> 0x00a1, h -> 0x00aa }
            r12.a(r4, r15)     // Catch:{ ConnectException -> 0x00a1, h -> 0x00aa }
        L_0x009e:
            return
        L_0x009f:
            r5 = 0
            goto L_0x0056
        L_0x00a1:
            r4 = move-exception
            if (r5 == 0) goto L_0x00ae
            com.agilebinary.a.a.a.h.f r0 = new com.agilebinary.a.a.a.h.f
            r0.<init>(r13, r4)
            throw r0
        L_0x00aa:
            r4 = move-exception
            if (r5 == 0) goto L_0x00ae
            throw r4
        L_0x00ae:
            org.apache.commons.logging.Log r4 = r11.f43a
            boolean r4 = r4.isDebugEnabled()
            if (r4 == 0) goto L_0x00da
            org.apache.commons.logging.Log r4 = r11.f43a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "Connect to "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.StringBuilder r5 = r5.append(r7)
            java.lang.String r6 = " timed out. "
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r4.debug(r5)
        L_0x00da:
            int r3 = r3 + 1
            goto L_0x004b
        L_0x00de:
            r4 = r6
            goto L_0x0094
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.b.a(com.agilebinary.a.a.a.h.g, com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.e.e):void");
    }
}
