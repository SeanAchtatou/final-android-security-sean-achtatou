package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.b.t;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class f implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final t f61a;
    private final a b;
    private final int c;
    private final int d;
    private final List e;
    private int f;
    private x g;

    public f(a aVar, e eVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = aVar;
            this.c = eVar.a("http.connection.max-header-count", -1);
            this.d = eVar.a("http.connection.max-line-length", -1);
            this.f61a = s.f21a;
            this.e = new ArrayList();
            this.f = 0;
        }
    }

    public static com.agilebinary.a.a.a.t[] a(a aVar, int i, int i2, t tVar, List list) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Line parser may not be null");
        } else if (list == null) {
            throw new IllegalArgumentException("Header line list may not be null");
        } else {
            c cVar = null;
            c cVar2 = null;
            while (true) {
                if (cVar2 == null) {
                    cVar2 = new c(64);
                } else {
                    cVar2.a();
                }
                if (aVar.a(cVar2) == -1 || cVar2.c() <= 0) {
                    com.agilebinary.a.a.a.t[] tVarArr = new com.agilebinary.a.a.a.t[list.size()];
                    int i3 = 0;
                } else {
                    if ((cVar2.a(0) == ' ' || cVar2.a(0) == 9) && cVar != null) {
                        int i4 = 0;
                        while (i4 < cVar2.c() && ((r3 = cVar2.a(i4)) == ' ' || r3 == 9)) {
                            i4++;
                        }
                        if (i2 <= 0 || ((cVar.c() + 1) + cVar2.c()) - i4 <= i2) {
                            cVar.a(' ');
                            cVar.a(cVar2, i4, cVar2.c() - i4);
                        } else {
                            throw new IOException("Maximum line length limit exceeded");
                        }
                    } else {
                        list.add(cVar2);
                        cVar = cVar2;
                        cVar2 = null;
                    }
                    if (i > 0 && list.size() >= i) {
                        throw new IOException("Maximum header count exceeded");
                    }
                }
            }
            com.agilebinary.a.a.a.t[] tVarArr2 = new com.agilebinary.a.a.a.t[list.size()];
            int i32 = 0;
            while (i32 < list.size()) {
                try {
                    tVarArr2[i32] = tVar.a((c) list.get(i32));
                    i32++;
                } catch (u e2) {
                    throw new l(e2.getMessage());
                }
            }
            return tVarArr2;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final x a() {
        switch (this.f) {
            case 0:
                try {
                    this.g = a(this.b);
                    this.f = 1;
                    break;
                } catch (u e2) {
                    throw new l(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException("Inconsistent parser state");
        }
        this.g.a(a(this.b, this.c, this.d, this.f61a, this.e));
        x xVar = this.g;
        this.g = null;
        this.e.clear();
        this.f = 0;
        return xVar;
    }

    /* access modifiers changed from: protected */
    public abstract x a(a aVar);
}
