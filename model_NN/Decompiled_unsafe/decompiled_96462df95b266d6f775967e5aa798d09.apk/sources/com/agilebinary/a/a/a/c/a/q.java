package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.i;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class q extends ah {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f31a;

    public q() {
        this(null);
    }

    public q(String[] strArr) {
        if (strArr != null) {
            this.f31a = (String[]) strArr.clone();
        } else {
            this.f31a = new String[]{"EEE, dd-MMM-yy HH:mm:ss z"};
        }
        a("path", new m());
        a("domain", new ab());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.f31a));
    }

    public final int a() {
        return 0;
    }

    public final List a(t tVar, f fVar) {
        i iVar;
        c cVar;
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!tVar.a().equalsIgnoreCase("Set-Cookie")) {
            throw new e("Unrecognized cookie header '" + tVar.toString() + "'");
        } else {
            if (tVar instanceof r) {
                cVar = ((r) tVar).e();
                iVar = new i(((r) tVar).d(), cVar.c());
            } else {
                String b = tVar.b();
                if (b == null) {
                    throw new e("Header value is null");
                }
                c cVar2 = new c(b.length());
                cVar2.a(b);
                c cVar3 = cVar2;
                iVar = new i(0, cVar2.c());
                cVar = cVar3;
            }
            return a(new m[]{aa.a(cVar, iVar)}, fVar);
        }
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            c cVar = new c(list.size() * 20);
            cVar.a("Cookie");
            cVar.a(": ");
            for (int i = 0; i < list.size(); i++) {
                com.agilebinary.a.a.a.k.c cVar2 = (com.agilebinary.a.a.a.k.c) list.get(i);
                if (i > 0) {
                    cVar.a("; ");
                }
                cVar.a(cVar2.a());
                String b = cVar2.b();
                if (b != null) {
                    cVar.a("=");
                    cVar.a(b);
                }
            }
            ArrayList arrayList = new ArrayList(1);
            arrayList.add(new com.agilebinary.a.a.a.b.e(cVar));
            return arrayList;
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return "netscape";
    }
}
