package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.j;

public final class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private final ab[] f91a;
    private final ad[] b;

    public f(ab[] abVarArr, ad[] adVarArr) {
        if (abVarArr != null) {
            int length = abVarArr.length;
            this.f91a = new ab[length];
            for (int i = 0; i < length; i++) {
                this.f91a[i] = abVarArr[i];
            }
        } else {
            this.f91a = new ab[0];
        }
        if (adVarArr != null) {
            int length2 = adVarArr.length;
            this.b = new ad[length2];
            for (int i2 = 0; i2 < length2; i2++) {
                this.b[i2] = adVarArr[i2];
            }
            return;
        }
        this.b = new ad[0];
    }

    public final void a(com.agilebinary.a.a.a.f fVar, k kVar) {
        for (ab a2 : this.f91a) {
            a2.a(fVar, kVar);
        }
    }

    public final void a(j jVar, k kVar) {
        for (ad a2 : this.b) {
            a2.a(jVar, kVar);
        }
    }
}
