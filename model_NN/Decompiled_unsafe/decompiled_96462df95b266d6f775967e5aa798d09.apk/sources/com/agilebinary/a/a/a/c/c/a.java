package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.r;
import com.agilebinary.a.a.a.j.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.w;
import com.agilebinary.a.a.a.x;

public abstract class a implements c {

    /* renamed from: a  reason: collision with root package name */
    protected final e f56a;
    protected final com.agilebinary.a.a.a.i.c b;
    protected final com.agilebinary.a.a.a.b.c c;

    public a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.f56a = eVar;
        this.b = new com.agilebinary.a.a.a.i.c(128);
        this.c = r.f20a;
    }

    /* access modifiers changed from: protected */
    public abstract void a(x xVar);

    public final void b(x xVar) {
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        a(xVar);
        w f = xVar.f();
        while (f.hasNext()) {
            this.f56a.a(this.c.a(this.b, (t) f.next()));
        }
        this.b.a();
        this.f56a.a(this.b);
    }
}
