package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.h;
import com.agilebinary.a.a.a.k.i;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.k.m;

public final class n implements k {
    public final void a(b bVar, String str) {
        int i;
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for version attribute");
        } else {
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                i = -1;
            }
            if (i < 0) {
                throw new e("Invalid cookie version.");
            }
            bVar.a(i);
        }
    }

    public final void a(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if ((cVar instanceof h) && (cVar instanceof m) && !((m) cVar).e("version")) {
            throw new i("Violates RFC 2965. Version attribute is required.");
        }
    }

    public final boolean b(c cVar, f fVar) {
        return true;
    }
}
