package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;

public final class f implements j {

    /* renamed from: a  reason: collision with root package name */
    public static final f f11a = new f();
    private static final char[] b = {';', ','};

    public static o a(c cVar, i iVar, char[] cArr) {
        boolean z;
        int i;
        String str;
        boolean z2;
        int i2;
        int i3;
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int b3 = iVar.b();
            int a2 = iVar.a();
            while (true) {
                if (b2 < a2) {
                    char a3 = cVar.a(b2);
                    if (a3 == '=') {
                        break;
                    } else if (a(a3, cArr)) {
                        z = true;
                        break;
                    } else {
                        b2++;
                    }
                } else {
                    break;
                }
            }
            z = false;
            if (b2 == a2) {
                z = true;
                int i4 = b2;
                str = cVar.b(b3, a2);
                i = i4;
            } else {
                String b4 = cVar.b(b3, b2);
                i = b2 + 1;
                str = b4;
            }
            if (z) {
                iVar.a(i);
                return a(str, (String) null);
            }
            boolean z3 = false;
            boolean z4 = false;
            int i5 = i;
            while (true) {
                if (i5 < a2) {
                    char a4 = cVar.a(i5);
                    if (a4 == '\"' && !z3) {
                        z4 = !z4;
                    }
                    if (!z4 && !z3 && a(a4, cArr)) {
                        z2 = true;
                        break;
                    }
                    z3 = !z3 && z4 && a4 == '\\';
                    i5++;
                } else {
                    z2 = z;
                    break;
                }
            }
            while (i < i5 && d.a(cVar.a(i))) {
                i++;
            }
            int i6 = i5;
            while (i6 > i && d.a(cVar.a(i6 - 1))) {
                i6--;
            }
            if (i6 - i >= 2 && cVar.a(i) == '\"' && cVar.a(i6 - 1) == '\"') {
                int i7 = i6 - 1;
                i2 = i + 1;
                i3 = i7;
            } else {
                int i8 = i6;
                i2 = i;
                i3 = i8;
            }
            String a5 = cVar.a(i2, i3);
            iVar.a(z2 ? i5 + 1 : i5);
            return a(str, a5);
        }
    }

    private static o a(String str, String str2) {
        return new o(str, str2);
    }

    private static boolean a(char c, char[] cArr) {
        if (cArr != null) {
            for (char c2 : cArr) {
                if (c == c2) {
                    return true;
                }
            }
        }
        return false;
    }

    public static final m[] a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Value to parse may not be null");
        }
        f fVar = f11a;
        c cVar = new c(str.length());
        cVar.a(str);
        return fVar.a(cVar, new i(0, str.length()));
    }

    private o[] c(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            while (b2 < a2 && d.a(cVar.a(b2))) {
                b2++;
            }
            iVar.a(b2);
            if (iVar.c()) {
                return new o[0];
            }
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(d(cVar, iVar));
                if (cVar.a(iVar.b() - 1) == ',') {
                    break;
                }
            }
            return (o[]) arrayList.toArray(new o[arrayList.size()]);
        }
    }

    private static o d(c cVar, i iVar) {
        return a(cVar, iVar, b);
    }

    public final m[] a(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                m b2 = b(cVar, iVar);
                if (b2.a().length() != 0 || b2.b() != null) {
                    arrayList.add(b2);
                }
            }
            return (m[]) arrayList.toArray(new m[arrayList.size()]);
        }
    }

    public final m b(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            o d = d(cVar, iVar);
            o[] oVarArr = null;
            if (!iVar.c() && cVar.a(iVar.b() - 1) != ',') {
                oVarArr = c(cVar, iVar);
            }
            return new a(d.a(), d.b(), oVarArr);
        }
    }
}
