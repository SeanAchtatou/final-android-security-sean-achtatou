package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.k.e;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.k;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public final class ae extends ag {
    public ae() {
        this(null, false);
    }

    public ae(String[] strArr, boolean z) {
        super(strArr, z);
        a("domain", new y());
        a("port", new g());
        a("commenturl", new z());
        a("discard", new b());
        a("version", new n());
    }

    private static f b(f fVar) {
        String a2 = fVar.a();
        boolean z = true;
        int i = 0;
        while (true) {
            if (i >= a2.length()) {
                break;
            }
            char charAt = a2.charAt(i);
            if (charAt == '.' || charAt == ':') {
                z = false;
            } else {
                i++;
            }
        }
        return z ? new f(a2 + ".local", fVar.c(), fVar.b(), fVar.d()) : fVar;
    }

    private List b(m[] mVarArr, f fVar) {
        ArrayList arrayList = new ArrayList(mVarArr.length);
        for (m mVar : mVarArr) {
            String a2 = mVar.a();
            String b = mVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new e("Cookie name may not be empty");
            }
            af afVar = new af(a2, b);
            afVar.c(a(fVar));
            afVar.b(fVar.a());
            afVar.a(new int[]{fVar.c()});
            o[] c = mVar.c();
            HashMap hashMap = new HashMap(c.length);
            for (int length = c.length - 1; length >= 0; length--) {
                o oVar = c[length];
                hashMap.put(oVar.a().toLowerCase(Locale.ENGLISH), oVar);
            }
            for (Map.Entry value : hashMap.entrySet()) {
                o oVar2 = (o) value.getValue();
                String lowerCase = oVar2.a().toLowerCase(Locale.ENGLISH);
                afVar.a(lowerCase, oVar2.b());
                k a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(afVar, oVar2.b());
                }
            }
            arrayList.add(afVar);
        }
        return arrayList;
    }

    public final int a() {
        return 1;
    }

    public final List a(t tVar, f fVar) {
        if (tVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!tVar.a().equalsIgnoreCase("Set-Cookie2")) {
            throw new e("Unrecognized cookie header '" + tVar.toString() + "'");
        } else {
            return b(tVar.c(), b(fVar));
        }
    }

    /* access modifiers changed from: protected */
    public final List a(m[] mVarArr, f fVar) {
        return b(mVarArr, b(fVar));
    }

    /* access modifiers changed from: protected */
    public final void a(c cVar, com.agilebinary.a.a.a.k.c cVar2, int i) {
        String d;
        int[] g;
        super.a(cVar, cVar2, i);
        if ((cVar2 instanceof com.agilebinary.a.a.a.k.m) && (d = ((com.agilebinary.a.a.a.k.m) cVar2).d("port")) != null) {
            cVar.a("; $Port");
            cVar.a("=\"");
            if (d.trim().length() > 0 && (g = cVar2.g()) != null) {
                int length = g.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 > 0) {
                        cVar.a(",");
                    }
                    cVar.a(Integer.toString(g[i2]));
                }
            }
            cVar.a("\"");
        }
    }

    public final void a(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            super.a(cVar, b(fVar));
        }
    }

    public final t b() {
        c cVar = new c(40);
        cVar.a("Cookie2");
        cVar.a(": ");
        cVar.a("$Version=");
        cVar.a(Integer.toString(1));
        return new com.agilebinary.a.a.a.b.e(cVar);
    }

    public final boolean b(com.agilebinary.a.a.a.k.c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar != null) {
            return super.b(cVar, b(fVar));
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    public final String toString() {
        return "rfc2965";
    }
}
