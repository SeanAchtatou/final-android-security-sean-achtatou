package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.e;
import com.agilebinary.a.a.a.j.f;

public final class j implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f49a;
    private final q b;
    private final String c;

    public j(e eVar, q qVar, String str) {
        this.f49a = eVar;
        this.b = qVar;
        this.c = str != null ? str : "ASCII";
    }

    public final void a(int i) {
        this.f49a.a(i);
        if (this.b.a()) {
            this.b.a(new byte[]{(byte) i});
        }
    }

    public final void a(c cVar) {
        this.f49a.a(cVar);
        if (this.b.a()) {
            this.b.a((new String(cVar.b(), 0, cVar.c()) + "\r\n").getBytes(this.c));
        }
    }

    public final void a(String str) {
        this.f49a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    public final void a(byte[] bArr, int i, int i2) {
        this.f49a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public final void b() {
        this.f49a.b();
    }

    public final f c() {
        return this.f49a.c();
    }
}
