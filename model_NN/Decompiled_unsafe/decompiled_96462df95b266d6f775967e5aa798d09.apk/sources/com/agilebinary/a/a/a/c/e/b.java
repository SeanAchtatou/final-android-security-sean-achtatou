package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.aa;
import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.x;

public final class b implements a {
    public final long a(x xVar) {
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        t c = xVar.c("Transfer-Encoding");
        t c2 = xVar.c("Content-Length");
        if (c != null) {
            String b = c.b();
            if ("chunked".equalsIgnoreCase(b)) {
                if (!xVar.c().a(aa.c)) {
                    return -2;
                }
                throw new l("Chunked transfer encoding not allowed for " + xVar.c());
            } else if ("identity".equalsIgnoreCase(b)) {
                return -1;
            } else {
                throw new l("Unsupported transfer encoding: " + b);
            }
        } else if (c2 == null) {
            return -1;
        } else {
            String b2 = c2.b();
            try {
                return Long.parseLong(b2);
            } catch (NumberFormatException e) {
                throw new l("Invalid content length: " + b2);
            }
        }
    }
}
