package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.a.i;
import com.agilebinary.a.a.a.d.b;
import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.r;
import com.agilebinary.a.a.b.a.a;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class t implements b {
    private static final List b = Collections.unmodifiableList(Arrays.asList("negotiate", "NTLM", "Digest", "Basic"));

    /* renamed from: a  reason: collision with root package name */
    private final Log f79a = a.a(getClass());

    protected static Map a(com.agilebinary.a.a.a.t[] tVarArr) {
        c cVar;
        int i;
        HashMap hashMap = new HashMap(tVarArr.length);
        for (com.agilebinary.a.a.a.t tVar : tVarArr) {
            if (tVar instanceof r) {
                cVar = ((r) tVar).e();
                i = ((r) tVar).d();
            } else {
                String b2 = tVar.b();
                if (b2 == null) {
                    throw new i("Header value is null");
                }
                cVar = new c(b2.length());
                cVar.a(b2);
                i = 0;
            }
            while (i < cVar.c() && d.a(cVar.a(i))) {
                i++;
            }
            int i2 = i;
            while (i2 < cVar.c() && !d.a(cVar.a(i2))) {
                i2++;
            }
            hashMap.put(cVar.a(i, i2).toLowerCase(Locale.ENGLISH), tVar);
        }
        return hashMap;
    }

    public final h a(Map map, j jVar, k kVar) {
        h hVar;
        com.agilebinary.a.a.a.a.a aVar = (com.agilebinary.a.a.a.a.a) kVar.a("http.authscheme-registry");
        if (aVar == null) {
            throw new IllegalStateException("AuthScheme registry not set in HTTP context");
        }
        List a2 = a(jVar, kVar);
        if (a2 == null) {
            a2 = b;
        }
        if (this.f79a.isDebugEnabled()) {
            this.f79a.debug("Authentication schemes in the order of preference: " + a2);
        }
        Iterator it = a2.iterator();
        while (true) {
            if (!it.hasNext()) {
                hVar = null;
                break;
            }
            String str = (String) it.next();
            if (((com.agilebinary.a.a.a.t) map.get(str.toLowerCase(Locale.ENGLISH))) != null) {
                if (this.f79a.isDebugEnabled()) {
                    this.f79a.debug(str + " authentication scheme selected");
                }
                try {
                    jVar.g();
                    hVar = aVar.a(str);
                    break;
                } catch (IllegalStateException e) {
                    if (this.f79a.isWarnEnabled()) {
                        this.f79a.warn("Authentication scheme " + str + " not supported");
                    }
                }
            } else if (this.f79a.isDebugEnabled()) {
                this.f79a.debug("Challenge for " + str + " authentication scheme not available");
            }
        }
        if (hVar != null) {
            return hVar;
        }
        throw new com.agilebinary.a.a.a.a.b("Unable to respond to any of these challenges: " + map);
    }

    /* access modifiers changed from: protected */
    public List a(j jVar, k kVar) {
        return b;
    }
}
