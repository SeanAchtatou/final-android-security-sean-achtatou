package com.agilebinary.a.a.a.c.d;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class r {

    /* renamed from: a  reason: collision with root package name */
    private final Set f77a = new HashSet();
    private final List b = new ArrayList();

    public final boolean a(URI uri) {
        return this.f77a.contains(uri);
    }

    public final void b(URI uri) {
        this.f77a.add(uri);
        this.b.add(uri);
    }
}
