package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.c.c.f;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.n;
import com.agilebinary.a.a.a.x;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class p extends f {
    private final Log b = a.a(getClass());
    private final i c;
    private final c d;
    private final int e;

    public p(com.agilebinary.a.a.a.j.a aVar, i iVar, e eVar) {
        super(aVar, eVar);
        if (iVar == null) {
            throw new IllegalArgumentException("Response factory may not be null");
        }
        this.c = iVar;
        this.d = new c(128);
        this.e = eVar.a("http.connection.max-status-line-garbage", Integer.MAX_VALUE);
    }

    /* access modifiers changed from: protected */
    public final x a(com.agilebinary.a.a.a.j.a aVar) {
        int i = 0;
        while (true) {
            this.d.a();
            int a2 = aVar.a(this.d);
            if (a2 == -1 && i == 0) {
                throw new n("The target server failed to respond");
            }
            com.agilebinary.a.a.a.b.i iVar = new com.agilebinary.a.a.a.b.i(0, this.d.c());
            if (this.f61a.a(this.d, iVar)) {
                return this.c.a(this.f61a.b(this.d, iVar));
            } else if (a2 != -1 && i < this.e) {
                if (this.b.isDebugEnabled()) {
                    this.b.debug("Garbage in response: " + this.d.toString());
                }
                i++;
            }
        }
        throw new l("The server failed to respond with a valid HTTP response");
    }
}
