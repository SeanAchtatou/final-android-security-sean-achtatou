package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;
import com.biige.client.android.R;

public final class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private double f126a;
    private double b;

    public b(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f126a = cVar.e();
        this.b = cVar.e();
    }

    public final String b(Context context) {
        return context.getString(R.string.label_event_location_type_gps);
    }

    public final String c(Context context) {
        if (d()) {
            return "";
        }
        return context.getString(R.string.label_event_location_wrapNotCurrent, com.agilebinary.mobilemonitor.client.android.c.c.a().d(c()));
    }

    public final byte k() {
        return 5;
    }
}
