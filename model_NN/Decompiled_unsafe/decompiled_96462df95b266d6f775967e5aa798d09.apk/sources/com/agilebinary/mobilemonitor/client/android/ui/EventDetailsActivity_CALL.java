package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.u;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class EventDetailsActivity_CALL extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f195a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;
    private TextView i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_call, viewGroup, true);
        this.f195a = (TextView) findViewById(R.id.eventdetails_call_direction);
        this.i = (TextView) findViewById(R.id.eventdetails_call_fromto);
        this.e = (TextView) findViewById(R.id.eventdetails_call_remoteparty);
        this.h = (TextView) findViewById(R.id.eventdetails_call_duration);
        this.b = (TextView) findViewById(R.id.eventdetails_call_time_initiated);
        this.c = (TextView) findViewById(R.id.eventdetails_call_time_connected);
        this.d = (TextView) findViewById(R.id.eventdetails_call_time_terminated);
        this.f = (TextView) findViewById(R.id.eventdetails_sms_speed);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        String string;
        String str;
        int i2;
        boolean z;
        super.a(oVar);
        u uVar = (u) oVar;
        String a2 = a.a(uVar.e(), uVar.f());
        long a3 = uVar.a();
        long b2 = uVar.b();
        long c2 = uVar.c();
        int c3 = (int) ((uVar.c() - uVar.b()) / 1000);
        switch (uVar.d()) {
            case -1:
                String string2 = getString(R.string.label_event_call_unknown);
                string = getString(R.string.label_event_to);
                str = string2;
                i2 = 0;
                z = false;
                break;
            case 0:
            default:
                string = "";
                z = false;
                i2 = c3;
                str = "";
                break;
            case 1:
                String string3 = getString(R.string.label_event_call_incoming);
                string = getString(R.string.label_event_from);
                i2 = c3;
                str = string3;
                z = true;
                break;
            case 2:
                string = getString(R.string.label_event_to);
                z = true;
                i2 = c3;
                str = c3 > 0 ? getString(R.string.label_event_call_outgoing) : getString(R.string.label_event_call_outgoing_missed);
                break;
            case 3:
                String string4 = getString(R.string.label_event_call_incoming_missed);
                string = getString(R.string.label_event_from);
                str = string4;
                i2 = 0;
                z = false;
                break;
        }
        this.f195a.setText(str);
        this.i.setText(string);
        this.e.setText(a2);
        this.h.setText(i2 == 0 ? "" : getString(R.string.label_event_duration_fmt, new Object[]{Integer.valueOf(i2 / 60), Integer.valueOf(i2 % 60)}));
        this.b.setText(c.a().c(a3));
        this.c.setText(c.a().c(b2));
        this.d.setText(c.a().c(c2));
        this.f.setText(a.a(this, uVar));
        if (!z || !a.a(uVar)) {
            this.f.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.f.setTextColor(getResources().getColor(R.color.warning));
        }
    }
}
