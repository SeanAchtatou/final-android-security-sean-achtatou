package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public abstract class q extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f136a;
    private long b;
    private byte c;
    private String d;

    public q(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f136a = eVar.a(cVar.d());
        this.b = eVar.a(cVar.d());
        this.c = cVar.b();
        this.d = cVar.g();
    }

    public final long v() {
        return this.f136a;
    }

    public final long w() {
        return this.b;
    }

    public final byte x() {
        return this.c;
    }
}
