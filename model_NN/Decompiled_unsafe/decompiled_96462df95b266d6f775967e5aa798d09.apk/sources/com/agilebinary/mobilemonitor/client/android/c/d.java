package com.agilebinary.mobilemonitor.client.android.c;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f182a = System.getProperty("line.separator");
    private static char[] b = new char[64];
    private static byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        b[i] = '+';
        b[i + 1] = '/';
        for (int i2 = 0; i2 < c.length; i2++) {
            c[i2] = -1;
        }
        for (int i3 = 0; i3 < 64; i3++) {
            c[b[i3]] = (byte) i3;
        }
    }

    private d() {
    }

    public static byte[] a(char[] cArr, int i) {
        int i2;
        char c2;
        int i3;
        char c3;
        int i4 = 0;
        if (i % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        int i5 = i;
        while (i5 > 0 && cArr[(i5 + 0) - 1] == '=') {
            i5--;
        }
        int i6 = (i5 * 3) / 4;
        byte[] bArr = new byte[i6];
        int i7 = i5 + 0;
        int i8 = 0;
        while (i8 < i7) {
            int i9 = i8 + 1;
            char c4 = cArr[i8];
            int i10 = i9 + 1;
            char c5 = cArr[i9];
            if (i10 < i7) {
                i2 = i10 + 1;
                c2 = cArr[i10];
            } else {
                i2 = i10;
                c2 = 'A';
            }
            if (i2 < i7) {
                i3 = i2 + 1;
                c3 = cArr[i2];
            } else {
                i3 = i2;
                c3 = 'A';
            }
            if (c4 > 127 || c5 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = c[c4];
            byte b3 = c[c5];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i11 = (b2 << 2) | (b3 >>> 4);
            int i12 = ((b3 & 15) << 4) | (b4 >>> 2);
            byte b6 = ((b4 & 3) << 6) | b5;
            int i13 = i4 + 1;
            bArr[i4] = (byte) i11;
            if (i13 < i6) {
                i4 = i13 + 1;
                bArr[i13] = (byte) i12;
            } else {
                i4 = i13;
            }
            if (i4 < i6) {
                bArr[i4] = (byte) b6;
                i4++;
            }
            i8 = i3;
        }
        return bArr;
    }
}
