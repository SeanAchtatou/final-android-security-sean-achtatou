package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.biige.client.android.R;

public class AboutActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f189a;
    private TextView b;
    private TextView c;

    /* access modifiers changed from: protected */
    public final int a() {
        return R.layout.about;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.c = (TextView) findViewById(R.id.about_version);
        TextView textView = this.c;
        t.a();
        textView.setText("1.22");
        this.f189a = (TextView) findViewById(R.id.about_url);
        this.f189a.setMovementMethod(LinkMovementMethod.getInstance());
        t.a();
        this.f189a.setText(Html.fromHtml(getString(R.string.label_about_url, new Object[]{"<a href='http://www.biige.com'>www.biige.com</a>"})));
        this.b = (TextView) findViewById(R.id.about_copyright);
        TextView textView2 = this.b;
        t.a();
        textView2.setText(getString(R.string.label_about_copyright, new Object[]{"2011 biige.com"}));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
