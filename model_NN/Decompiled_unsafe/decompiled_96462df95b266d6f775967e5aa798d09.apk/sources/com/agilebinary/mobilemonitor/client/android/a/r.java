package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.android.c.f;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import javax.net.ssl.X509TrustManager;

public final class r implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    private PublicKey f162a;
    private boolean b;
    private HashSet c = new HashSet();
    private /* synthetic */ b d;

    r(b bVar) {
        this.d = bVar;
        if ("30820122300d06092a864886f70d01010105000382010f003082010a0282010100a1291765f28b08d1c71280fa8022e5780c7eafc284d56962157e25b833b6606ad92215dac503d25c10a099542f44553305ecbdcbf02bf08ffaa3cfce8a290c386d10a38adde76e617de50e7e7d0cac22ba7229cb28c3584958fe332836024ed25d7ef1b916f6fb3991d3c68469f316af5ba553632e92fde2a849a0c5aa07b14c4722d3ee802667c5e02e2de1f523dd94dae17022bfa47c031bd299aefe9065991d316c1ac566150bbcbcb9c30602b2f3eed6eaab9c41d74cdfc397d9d1d0ab4c334002548c4b421be864643e3c0cfd145ad514dfc3748ff498444b99ca5fa4060bfd31cd40760746cba73d7cf5a7019920d050e67af1354827ec5b93cc8c422b0203010001".trim().length() > 0) {
            try {
                this.f162a = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(f.a("30820122300d06092a864886f70d01010105000382010f003082010a0282010100a1291765f28b08d1c71280fa8022e5780c7eafc284d56962157e25b833b6606ad92215dac503d25c10a099542f44553305ecbdcbf02bf08ffaa3cfce8a290c386d10a38adde76e617de50e7e7d0cac22ba7229cb28c3584958fe332836024ed25d7ef1b916f6fb3991d3c68469f316af5ba553632e92fde2a849a0c5aa07b14c4722d3ee802667c5e02e2de1f523dd94dae17022bfa47c031bd299aefe9065991d316c1ac566150bbcbcb9c30602b2f3eed6eaab9c41d74cdfc397d9d1d0ab4c334002548c4b421be864643e3c0cfd145ad514dfc3748ff498444b99ca5fa4060bfd31cd40760746cba73d7cf5a7019920d050e67af1354827ec5b93cc8c422b0203010001".toCharArray())));
            } catch (Exception e) {
                this.b = true;
            }
        }
    }

    public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        if (x509CertificateArr.length == 0) {
            throw new CertificateException("no certificate in chain");
        } else if (this.b) {
            throw new CertificateException("certificate invalid because couldn't decode configured public key");
        } else if (this.f162a != null) {
            X509Certificate x509Certificate = x509CertificateArr[0];
            if (!this.c.contains(x509Certificate)) {
                try {
                    x509Certificate.verify(this.f162a);
                    this.c.add(x509Certificate);
                } catch (InvalidKeyException e) {
                    a.e(e);
                    throw new CertificateException(e);
                } catch (NoSuchAlgorithmException e2) {
                    a.e(e2);
                    throw new CertificateException(e2);
                } catch (NoSuchProviderException e3) {
                    a.e(e3);
                    throw new CertificateException(e3);
                } catch (SignatureException e4) {
                    a.e(e4);
                    throw new CertificateException(e4);
                }
            }
        }
    }

    public final X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
