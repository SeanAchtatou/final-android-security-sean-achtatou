package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.ui.BaseActivity;
import com.agilebinary.mobilemonitor.client.android.ui.LoginActivity;

public final class i extends AsyncTask implements g {

    /* renamed from: a  reason: collision with root package name */
    public static i f216a;
    private static String b = b.a();
    private BaseActivity c;
    private MyApplication d;
    private com.agilebinary.a.a.a.d.c.b e;

    public i(BaseActivity baseActivity) {
        this.c = baseActivity;
        this.d = baseActivity.g;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public c doInBackground(String... strArr) {
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, b);
        newWakeLock.acquire();
        try {
            c cVar = new c(this.d.a(strArr[0], strArr[1], this));
            try {
                newWakeLock.release();
            } catch (Exception e2) {
            }
            f216a = null;
            return cVar;
        } catch (q e3) {
            c cVar2 = new c(e3);
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            f216a = null;
            return cVar2;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
            f216a = null;
            throw th;
        }
    }

    public final void a() {
        cancel(false);
        if (this.e != null) {
            try {
                this.e.d();
            } catch (Exception e2) {
            }
        }
    }

    public final void a(com.agilebinary.a.a.a.d.c.b bVar) {
        this.e = bVar;
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        LoginActivity.a(this.c, null);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        c cVar = (c) obj;
        super.onPostExecute(cVar);
        LoginActivity.a(this.c, cVar);
    }
}
