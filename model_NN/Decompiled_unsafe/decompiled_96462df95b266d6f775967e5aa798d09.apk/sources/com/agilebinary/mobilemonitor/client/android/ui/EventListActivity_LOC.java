package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import com.agilebinary.mobilemonitor.client.android.i;
import com.biige.client.android.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventListActivity_LOC extends EventListActivity_base {
    protected AlertDialog h;
    protected int i = 2;
    private Button k;
    private Button l;
    private i m;
    private Set n = new HashSet();

    static /* synthetic */ void a(EventListActivity_LOC eventListActivity_LOC) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(eventListActivity_LOC.n);
        Collections.sort(arrayList, new ae(eventListActivity_LOC));
        eventListActivity_LOC.a(arrayList);
    }

    private void a(List list) {
        Intent intent = new Intent(this, MapActivity_GOOGLE.class);
        intent.putExtra("EXTRA_EVENT_IDS", (Serializable) list);
        startActivity(intent);
    }

    public final void a(long j, boolean z) {
        if (z) {
            this.n.add(Long.valueOf(j));
        } else {
            this.n.remove(Long.valueOf(j));
        }
        this.k.setEnabled(this.n.size() > 0);
    }

    /* access modifiers changed from: protected */
    public final void a(ax axVar) {
        super.a(axVar);
        this.l.setEnabled(axVar != ax.PROGRESS);
        this.k.setEnabled(axVar != ax.PROGRESS);
    }

    public final void a(boolean z) {
        super.a(z);
        if (this.m != null) {
            this.m.c();
        }
    }

    /* access modifiers changed from: protected */
    public final ba a_() {
        return new ad(this);
    }

    public final void b(long j) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j));
        a(arrayList);
    }

    /* access modifiers changed from: protected */
    public final void b_() {
        super.b_();
        l();
    }

    public final boolean c(long j) {
        return this.n.contains(Long.valueOf(j));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public final void l() {
        int i2;
        Double d;
        Long l2;
        int columnIndex = this.f.getColumnIndex("lat");
        this.n.clear();
        if (this.f.moveToFirst()) {
            switch (this.i) {
                case 0:
                    this.n.clear();
                    break;
                case 1:
                    do {
                        if (!this.f.isNull(columnIndex)) {
                            this.n.add(Long.valueOf(this.f.getLong(0)));
                        }
                    } while (this.f.moveToNext());
                    break;
                case 2:
                    Double d2 = null;
                    Long l3 = null;
                    Long l4 = null;
                    do {
                        long j = this.f.getLong(0);
                        if (!this.f.isNull(columnIndex)) {
                            if (l4 == null || Math.abs(l4.longValue() - j) <= 5000) {
                                d = d2;
                                l2 = l3;
                            } else {
                                if (l3 != null) {
                                    this.n.add(l3);
                                }
                                l2 = null;
                                d = null;
                            }
                            Cursor cursor = this.f;
                            double d3 = 1.0d;
                            int i3 = cursor.getInt(cursor.getColumnIndex("contenttype"));
                            if (i3 == 5 || i3 == 6 || i3 == 4) {
                                d3 = cursor.getInt(cursor.getColumnIndex("powersave")) == 1 ? 2.0d : cursor.getInt(cursor.getColumnIndex("valloc")) == 1 ? 1.0d : 9.9999999E7d;
                            } else if (i3 == 7 || i3 == 8 || i3 == 12) {
                                d3 = 1.5d;
                            }
                            Double valueOf = Double.valueOf(cursor.getDouble(cursor.getColumnIndex("accuracy")));
                            double doubleValue = valueOf == null ? Double.MAX_VALUE : d3 * valueOf.doubleValue();
                            if (l2 == null || doubleValue < d.doubleValue()) {
                                d = Double.valueOf(doubleValue);
                                l2 = Long.valueOf(j);
                            }
                            l3 = l2;
                            d2 = d;
                            l4 = Long.valueOf(j);
                        }
                    } while (this.f.moveToNext());
                    if (l3 != null) {
                        this.n.add(l3);
                        break;
                    }
                    break;
                case 3:
                    int columnIndex2 = this.f.getColumnIndex("contenttype");
                    do {
                        if (!this.f.isNull(columnIndex) && ((i2 = this.f.getInt(columnIndex2)) == 5 || i2 == 6 || i2 == 4)) {
                            this.n.add(Long.valueOf(this.f.getLong(0)));
                        }
                    } while (this.f.moveToNext());
                    break;
            }
            this.c.notifyDataSetInvalidated();
        }
        this.k.setEnabled(this.n.size() > 0);
    }

    public final boolean m() {
        return this.f201a.isEnabled();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.i = bundle.getInt("EXTRA_FILTER_ID");
        }
        this.b.addView(getLayoutInflater().inflate((int) R.layout.eventlist_footer_loc, (ViewGroup) null), 1);
        this.k = (Button) findViewById(R.id.eventlist_loc_showmap);
        this.k.setOnClickListener(new af(this));
        this.l = (Button) findViewById(R.id.eventlist_loc_filter);
        this.l.setOnClickListener(new ab(this));
        CharSequence[] charSequenceArr = {getString(R.string.label_map_filter_clear), getString(R.string.label_map_filter_all), getString(R.string.label_map_filter_smart), getString(R.string.label_map_filter_gpsnet)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.label_map_filter_prompt);
        builder.setSingleChoiceItems(charSequenceArr, this.i, new ac(this));
        this.h = builder.create();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("EXTRA_FILTER_ID", this.i);
    }
}
