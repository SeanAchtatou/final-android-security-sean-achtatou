package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import java.io.Serializable;

public abstract class o implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f135a;
    private long b;
    private String c;
    private String d;
    private String e;

    protected o(String str, long j, long j2, c cVar) {
        this.c = str;
        this.f135a = j;
        this.b = j2;
        this.d = cVar.g();
        this.e = cVar.g();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.f135a == ((o) obj).f135a;
    }

    public int hashCode() {
        return ((int) (this.f135a ^ (this.f135a >>> 32))) + 31;
    }

    public abstract byte k();

    public final String r() {
        return this.d;
    }

    public final String s() {
        return this.e;
    }

    public final long t() {
        return this.b;
    }

    public final long u() {
        return this.f135a;
    }
}
