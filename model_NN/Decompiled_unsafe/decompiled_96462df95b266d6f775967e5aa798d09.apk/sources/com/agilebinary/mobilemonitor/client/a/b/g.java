package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.e;

public final class g extends i {

    /* renamed from: a  reason: collision with root package name */
    private long f131a;
    private String b;
    private String c;
    private boolean d;
    private int e;

    public g(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f131a = eVar.a(cVar.d());
        this.b = cVar.g();
        this.c = cVar.g();
        this.d = cVar.a();
        this.e = cVar.c();
    }

    public final long a() {
        return this.f131a;
    }

    public final String b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public final boolean d() {
        return this.d;
    }

    public final int e() {
        return this.e;
    }

    public final String f() {
        return this.b;
    }

    public final String g() {
        return this.c;
    }

    public final byte k() {
        return 14;
    }
}
