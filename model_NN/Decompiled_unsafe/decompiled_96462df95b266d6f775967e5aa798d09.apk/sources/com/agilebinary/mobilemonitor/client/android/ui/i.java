package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.biige.client.android.R;

public final class i extends k implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private TextView c;
    private TextView d;
    private CheckBox e;
    private TextView f;
    private EventListActivity_LOC g;
    private long h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.i, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public i(EventListActivity_base eventListActivity_base, EventListActivity_LOC eventListActivity_LOC) {
        super(eventListActivity_base);
        this.g = eventListActivity_LOC;
        ((LayoutInflater) eventListActivity_base.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_loc, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = (TextView) findViewById(R.id.eventlist_rowview_loc_line1);
        this.d = (TextView) findViewById(R.id.eventlist_rowview_loc_line2);
        this.f = (TextView) findViewById(R.id.eventlist_rowview_loc_accuracy);
        this.e = (CheckBox) findViewById(R.id.eventlist_rowview_loc_checkbox);
        this.e.setOnCheckedChangeListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.b.setOnClickListener(this);
        this.f274a.setOnClickListener(this);
    }

    public final void a(Cursor cursor) {
        super.a(cursor);
        this.h = cursor.getLong(0);
        this.c.setText(cursor.getString(cursor.getColumnIndex("line1")));
        this.d.setText(cursor.getString(cursor.getColumnIndex("line2")));
        Double valueOf = Double.valueOf(cursor.getDouble(cursor.getColumnIndex("accuracy")));
        if (valueOf == null || valueOf.doubleValue() <= 0.0d) {
            this.f.setText("");
        } else {
            this.f.setText(getContext().getString(R.string.label_event_location_accuracy, valueOf));
        }
        this.e.setChecked(this.g.c(this.h));
        this.e.setVisibility(cursor.isNull(cursor.getColumnIndex("lat")) ? 4 : 0);
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.g.a(this.h, z);
    }

    public final void onClick(View view) {
        if (this.e.getVisibility() == 0 && this.g.m()) {
            this.g.b(this.h);
        }
    }
}
