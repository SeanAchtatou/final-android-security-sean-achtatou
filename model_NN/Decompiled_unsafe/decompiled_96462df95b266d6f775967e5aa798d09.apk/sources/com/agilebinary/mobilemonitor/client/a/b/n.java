package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.a.a.b;
import org.osmdroid.util.GeoPoint;

public abstract class n extends s implements d {

    /* renamed from: a  reason: collision with root package name */
    protected String f134a;
    protected b b;

    public n(String str, long j, long j2, c cVar, e eVar) {
        super(str, j, j2, cVar, eVar);
        this.f134a = cVar.g();
    }

    public CharSequence a(Context context) {
        return "";
    }

    public Double f() {
        if (this.b == null) {
            return null;
        }
        return Double.valueOf(this.b.c());
    }

    public final double g() {
        return f().doubleValue();
    }

    public final GeoPoint h() {
        if (this.b == null) {
            return null;
        }
        return new GeoPoint(this.b.a(), this.b.b());
    }

    public final com.google.android.maps.GeoPoint i() {
        GeoPoint h = h();
        if (h == null) {
            return null;
        }
        return new com.google.android.maps.GeoPoint(h.a(), h.b());
    }

    public boolean j() {
        return this.b != null;
    }

    public final b l() {
        return this.b;
    }
}
