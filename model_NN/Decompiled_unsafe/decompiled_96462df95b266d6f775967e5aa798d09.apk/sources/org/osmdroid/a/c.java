package org.osmdroid.a;

import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.a.a;
import org.osmdroid.a.b.v;

public class c extends i {
    private static final org.a.c f = a.a(c.class);
    protected final List c;
    private final ConcurrentHashMap e;

    protected c() {
        this(new v[0]);
    }

    private c(v[] vVarArr) {
        super((byte) 0);
        this.e = new ConcurrentHashMap();
        this.c = new ArrayList();
        Collections.addAll(this.c, vVarArr);
    }

    private boolean a(v vVar) {
        boolean contains;
        synchronized (this.c) {
            contains = this.c.contains(vVar);
        }
        return contains;
    }

    private v b(d dVar) {
        v c2;
        do {
            c2 = dVar.c();
            if (c2 == null || a(c2) || d()) {
                return c2;
            }
        } while (c2.e());
        return c2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004f, code lost:
        r0 = b(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0053, code lost:
        if (r0 == null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0055, code lost:
        r0.a(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005d, code lost:
        a(r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable a(org.osmdroid.a.f r6) {
        /*
            r5 = this;
            r4 = 0
            org.osmdroid.a.h r0 = r5.d
            boolean r0 = r0.b(r6)
            if (r0 == 0) goto L_0x0010
            org.osmdroid.a.h r0 = r5.d
            android.graphics.drawable.Drawable r0 = r0.a(r6)
        L_0x000f:
            return r0
        L_0x0010:
            java.util.concurrent.ConcurrentHashMap r0 = r5.e
            monitor-enter(r0)
            java.util.concurrent.ConcurrentHashMap r1 = r5.e     // Catch:{ all -> 0x0043 }
            boolean r1 = r1.containsValue(r6)     // Catch:{ all -> 0x0043 }
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            if (r1 != 0) goto L_0x0058
            java.util.List r1 = r5.c
            monitor-enter(r1)
            java.util.List r0 = r5.c     // Catch:{ all -> 0x0046 }
            int r0 = r0.size()     // Catch:{ all -> 0x0046 }
            org.osmdroid.a.b.v[] r0 = new org.osmdroid.a.b.v[r0]     // Catch:{ all -> 0x0046 }
            org.osmdroid.a.d r2 = new org.osmdroid.a.d     // Catch:{ all -> 0x0046 }
            java.util.List r3 = r5.c     // Catch:{ all -> 0x0046 }
            java.lang.Object[] r0 = r3.toArray(r0)     // Catch:{ all -> 0x0046 }
            org.osmdroid.a.b.v[] r0 = (org.osmdroid.a.b.v[]) r0     // Catch:{ all -> 0x0046 }
            r2.<init>(r6, r0, r5)     // Catch:{ all -> 0x0046 }
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            java.util.concurrent.ConcurrentHashMap r0 = r5.e
            monitor-enter(r0)
            java.util.concurrent.ConcurrentHashMap r1 = r5.e     // Catch:{ all -> 0x005a }
            boolean r1 = r1.containsValue(r6)     // Catch:{ all -> 0x005a }
            if (r1 == 0) goto L_0x0049
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            r0 = r4
            goto L_0x000f
        L_0x0043:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0043 }
            throw r1
        L_0x0046:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0046 }
            throw r0
        L_0x0049:
            java.util.concurrent.ConcurrentHashMap r1 = r5.e     // Catch:{ all -> 0x005a }
            r1.put(r2, r6)     // Catch:{ all -> 0x005a }
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            org.osmdroid.a.b.v r0 = r5.b(r2)
            if (r0 == 0) goto L_0x005d
            r0.a(r2)
        L_0x0058:
            r0 = r4
            goto L_0x000f
        L_0x005a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x005a }
            throw r1
        L_0x005d:
            r5.a(r2)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.c.a(org.osmdroid.a.f):android.graphics.drawable.Drawable");
    }

    public final void a() {
        synchronized (this.c) {
            for (v b : this.c) {
                b.b();
            }
        }
    }

    public final void a(d dVar) {
        v b = b(dVar);
        if (b != null) {
            b.a(dVar);
            return;
        }
        synchronized (this.e) {
            this.e.remove(dVar);
        }
        super.a(dVar);
    }

    public final void a(d dVar, Drawable drawable) {
        synchronized (this.e) {
            this.e.remove(dVar);
        }
        super.a(dVar, drawable);
    }

    public final int b() {
        int i = 23;
        synchronized (this.c) {
            for (v vVar : this.c) {
                if (vVar.h() < i) {
                    i = vVar.h();
                }
            }
        }
        return i;
    }

    public final int c() {
        int i = 0;
        synchronized (this.c) {
            for (v vVar : this.c) {
                if (vVar.i() > i) {
                    i = vVar.i();
                }
            }
        }
        return i;
    }
}
