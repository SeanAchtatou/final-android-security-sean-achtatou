package org.osmdroid.a.b;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.a.c;
import org.osmdroid.a.d.a;

public class k implements c, a {
    private static final c c = org.a.a.a(k.class);
    /* access modifiers changed from: private */
    public static long d;

    public k() {
        b bVar = new b(this);
        bVar.setPriority(1);
        bVar.start();
    }

    private static boolean a(File file) {
        if (file.mkdirs()) {
            return true;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        return file.exists();
    }

    /* access modifiers changed from: private */
    public void b() {
        synchronized (b) {
            if (d > 524288000) {
                c.b("Trimming tile cache from " + d + " to " + 524288000L);
                File[] fileArr = (File[]) c(b).toArray(new File[0]);
                Arrays.sort(fileArr, new a(this));
                for (File file : fileArr) {
                    if (d <= 524288000) {
                        break;
                    }
                    long length = file.length();
                    if (file.delete()) {
                        d -= length;
                    }
                }
                c.b("Finished trimming tile cache");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    d += file2.length();
                }
                if (file2.isDirectory()) {
                    b(file2);
                }
            }
        }
    }

    private List c(File file) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    arrayList.add(file2);
                }
                if (file2.isDirectory()) {
                    arrayList.addAll(c(file2));
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.osmdroid.a.a.f r7, org.osmdroid.a.f r8, java.io.InputStream r9) {
        /*
            r6 = this;
            r5 = 0
            java.io.File r0 = new java.io.File
            java.io.File r1 = org.osmdroid.a.b.k.b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r7.b(r8)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = ".tile"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r1, r2)
            java.io.File r1 = r0.getParentFile()
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x0031
            boolean r1 = a(r1)
            if (r1 != 0) goto L_0x0031
            r0 = r5
        L_0x0030:
            return r0
        L_0x0031:
            r1 = 0
            java.io.BufferedOutputStream r2 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x005a, all -> 0x0063 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005a, all -> 0x0063 }
            java.lang.String r0 = r0.getPath()     // Catch:{ IOException -> 0x005a, all -> 0x0063 }
            r3.<init>(r0)     // Catch:{ IOException -> 0x005a, all -> 0x0063 }
            r0 = 8192(0x2000, float:1.14794E-41)
            r2.<init>(r3, r0)     // Catch:{ IOException -> 0x005a, all -> 0x0063 }
            long r0 = org.osmdroid.a.c.c.a(r9, r2)     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            long r3 = org.osmdroid.a.b.k.d     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            long r0 = r0 + r3
            org.osmdroid.a.b.k.d = r0     // Catch:{ IOException -> 0x006d, all -> 0x006a }
            r3 = 629145600(0x25800000, double:3.10839227E-315)
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0055
            r6.b()     // Catch:{ IOException -> 0x006d, all -> 0x006a }
        L_0x0055:
            org.osmdroid.a.c.c.a(r2)
            r0 = 1
            goto L_0x0030
        L_0x005a:
            r0 = move-exception
            r0 = r1
        L_0x005c:
            if (r0 == 0) goto L_0x0061
            org.osmdroid.a.c.c.a(r0)
        L_0x0061:
            r0 = r5
            goto L_0x0030
        L_0x0063:
            r0 = move-exception
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            org.osmdroid.a.c.c.a(r1)
        L_0x0069:
            throw r0
        L_0x006a:
            r0 = move-exception
            r1 = r2
            goto L_0x0064
        L_0x006d:
            r0 = move-exception
            r0 = r2
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.k.a(org.osmdroid.a.a.f, org.osmdroid.a.f, java.io.InputStream):boolean");
    }
}
