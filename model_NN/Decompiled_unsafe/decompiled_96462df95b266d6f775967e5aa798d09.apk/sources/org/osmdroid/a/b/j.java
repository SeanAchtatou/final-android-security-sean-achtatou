package org.osmdroid.a.b;

final class j extends i {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f331a;

    /* synthetic */ j(q qVar) {
        this(qVar, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private j(q qVar, byte b) {
        super(qVar);
        this.f331a = qVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable a(org.osmdroid.a.d r7) {
        /*
            r6 = this;
            r4 = 0
            org.osmdroid.a.b.q r0 = r6.f331a
            org.osmdroid.a.a.f r0 = r0.c
            if (r0 != 0) goto L_0x0009
            r0 = r4
        L_0x0008:
            return r0
        L_0x0009:
            org.osmdroid.a.f r0 = r7.a()
            org.osmdroid.a.b.q r1 = r6.f331a
            boolean r1 = r1.a()
            if (r1 != 0) goto L_0x0017
            r0 = r4
            goto L_0x0008
        L_0x0017:
            org.osmdroid.a.b.q r1 = r6.f331a     // Catch:{ Throwable -> 0x0035, all -> 0x0046 }
            java.io.InputStream r0 = r1.a(r0)     // Catch:{ Throwable -> 0x0035, all -> 0x0046 }
            if (r0 == 0) goto L_0x002e
            org.osmdroid.a.b.q r1 = r6.f331a     // Catch:{ Throwable -> 0x0055, all -> 0x004e }
            org.osmdroid.a.a.f r1 = r1.c     // Catch:{ Throwable -> 0x0055, all -> 0x004e }
            android.graphics.drawable.Drawable r1 = r1.a(r0)     // Catch:{ Throwable -> 0x0055, all -> 0x004e }
            if (r0 == 0) goto L_0x002c
            org.osmdroid.a.c.c.a(r0)
        L_0x002c:
            r0 = r1
            goto L_0x0008
        L_0x002e:
            if (r0 == 0) goto L_0x0033
            org.osmdroid.a.c.c.a(r0)
        L_0x0033:
            r0 = r4
            goto L_0x0008
        L_0x0035:
            r0 = move-exception
            r1 = r4
        L_0x0037:
            org.a.c r2 = org.osmdroid.a.b.q.e     // Catch:{ all -> 0x0053 }
            java.lang.String r3 = "Error loading tile"
            r2.c(r3, r0)     // Catch:{ all -> 0x0053 }
            if (r1 == 0) goto L_0x0033
            org.osmdroid.a.c.c.a(r1)
            goto L_0x0033
        L_0x0046:
            r0 = move-exception
            r1 = r4
        L_0x0048:
            if (r1 == 0) goto L_0x004d
            org.osmdroid.a.c.c.a(r1)
        L_0x004d:
            throw r0
        L_0x004e:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0048
        L_0x0053:
            r0 = move-exception
            goto L_0x0048
        L_0x0055:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.j.a(org.osmdroid.a.d):android.graphics.drawable.Drawable");
    }
}
