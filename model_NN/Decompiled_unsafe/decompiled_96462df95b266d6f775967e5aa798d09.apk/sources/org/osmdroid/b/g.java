package org.osmdroid.b;

import android.view.GestureDetector;
import android.view.MotionEvent;

final class g implements GestureDetector.OnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ f f358a;

    /* synthetic */ g(f fVar) {
        this(fVar, (byte) 0);
    }

    private g(f fVar, byte b) {
        this.f358a = fVar;
    }

    public final boolean onDown(MotionEvent motionEvent) {
        this.f358a.n.a(this.f358a.o);
        return true;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int j = this.f358a.j();
        this.f358a.i.fling(this.f358a.getScrollX(), this.f358a.getScrollY(), (int) (-f), (int) (-f2), -j, j, -j, j);
        return true;
    }

    public final void onLongPress(MotionEvent motionEvent) {
        this.f358a.a(motionEvent);
    }

    public final boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.f358a.scrollBy((int) f, (int) f2);
        return true;
    }

    public final void onShowPress(MotionEvent motionEvent) {
    }

    public final boolean onSingleTapUp(MotionEvent motionEvent) {
        return this.f358a.b(motionEvent);
    }
}
