package org.osmdroid.b;

import android.view.animation.Animation;

final class d implements Animation.AnimationListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f355a;
    /* access modifiers changed from: private */
    public boolean b;
    private /* synthetic */ f c;

    /* synthetic */ d(f fVar) {
        this(fVar, (byte) 0);
    }

    private d(f fVar, byte b2) {
        this.c = fVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.b = false;
        this.c.b(this.f355a);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
        this.b = true;
    }
}
