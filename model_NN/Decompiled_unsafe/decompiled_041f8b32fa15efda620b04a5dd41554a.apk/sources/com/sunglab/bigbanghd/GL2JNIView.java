package com.sunglab.bigbanghd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.opengl.GLSurfaceView;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import java.nio.IntBuffer;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;

class GL2JNIView extends GLSurfaceView {
    private static final boolean DEBUG = false;
    /* access modifiers changed from: private */
    public static String TAG = "GL2JNIView";
    private static Bitmap bitmapWip;
    static Context m_Context;

    public static native void JNIColor(int i);

    public static native void JNINumber(int i);

    public static native void JNITail(float f);

    public static native void JNIThick(float f);

    public static native void ReRunStarEngine();

    public static native void SetupTexture(int[] iArr, int i, int i2, int i3);

    public static native void TouchDownNumber();

    public static native void TouchMoveNumber(float f, float f2, int i, int i2);

    public static native void TouchUpNumber();

    public static native void TurnOffStarEngine();

    public static native void TurnOnStarEngine(int i, int i2);

    public static native void UpdateStarEngine();

    private static class ConfigChooser implements GLSurfaceView.EGLConfigChooser {
        private static int EGL_OPENGL_ES2_BIT = 4;
        private static int[] s_configAttribs2 = {12324, 4, 12323, 4, 12322, 4, 12352, EGL_OPENGL_ES2_BIT, 12344};
        protected int mAlphaSize;
        protected int mBlueSize;
        protected int mDepthSize;
        protected int mGreenSize;
        protected int mRedSize;
        protected int mStencilSize;
        private int[] mValue = new int[1];

        public ConfigChooser(int i, int i2, int i3, int i4, int i5, int i6) {
            this.mRedSize = i;
            this.mGreenSize = i2;
            this.mBlueSize = i3;
            this.mAlphaSize = i4;
            this.mDepthSize = i5;
            this.mStencilSize = i6;
        }

        private int findConfigAttrib(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i, int i2) {
            if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.mValue)) {
                return this.mValue[0];
            }
            return i2;
        }

        private void printConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            int[] iArr = {12320, 12321, 12322, 12323, 12324, 12325, 12326, 12327, 12328, 12329, 12330, 12331, 12332, 12333, 12334, 12335, 12336, 12337, 12338, 12339, 12340, 12343, 12342, 12341, 12345, 12346, 12347, 12348, 12349, 12350, 12351, 12352, 12354};
            String[] strArr = {"EGL_BUFFER_SIZE", "EGL_ALPHA_SIZE", "EGL_BLUE_SIZE", "EGL_GREEN_SIZE", "EGL_RED_SIZE", "EGL_DEPTH_SIZE", "EGL_STENCIL_SIZE", "EGL_CONFIG_CAVEAT", "EGL_CONFIG_ID", "EGL_LEVEL", "EGL_MAX_PBUFFER_HEIGHT", "EGL_MAX_PBUFFER_PIXELS", "EGL_MAX_PBUFFER_WIDTH", "EGL_NATIVE_RENDERABLE", "EGL_NATIVE_VISUAL_ID", "EGL_NATIVE_VISUAL_TYPE", "EGL_PRESERVED_RESOURCES", "EGL_SAMPLES", "EGL_SAMPLE_BUFFERS", "EGL_SURFACE_TYPE", "EGL_TRANSPARENT_TYPE", "EGL_TRANSPARENT_RED_VALUE", "EGL_TRANSPARENT_GREEN_VALUE", "EGL_TRANSPARENT_BLUE_VALUE", "EGL_BIND_TO_TEXTURE_RGB", "EGL_BIND_TO_TEXTURE_RGBA", "EGL_MIN_SWAP_INTERVAL", "EGL_MAX_SWAP_INTERVAL", "EGL_LUMINANCE_SIZE", "EGL_ALPHA_MASK_SIZE", "EGL_COLOR_BUFFER_TYPE", "EGL_RENDERABLE_TYPE", "EGL_CONFORMANT"};
            int[] iArr2 = new int[1];
            int i = 0;
            while (i < iArr.length) {
                String str = strArr[i];
                if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, iArr[i], iArr2)) {
                    Log.w(GL2JNIView.TAG, String.format("  %s: %d\n", str, Integer.valueOf(iArr2[0])));
                    i++;
                }
                do {
                } while (egl10.eglGetError() != 12288);
                i++;
            }
        }

        private void printConfigs(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            int length = eGLConfigArr.length;
            Log.w(GL2JNIView.TAG, String.format("%d configurations", Integer.valueOf(length)));
            for (int i = 0; i < length; i++) {
                Log.w(GL2JNIView.TAG, String.format("Configuration %d:\n", Integer.valueOf(i)));
                printConfig(egl10, eGLDisplay, eGLConfigArr[i]);
            }
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, s_configAttribs2, null, 0, iArr);
            int i = iArr[0];
            if (i <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] eGLConfigArr = new EGLConfig[i];
            egl10.eglChooseConfig(eGLDisplay, s_configAttribs2, eGLConfigArr, i, iArr);
            return chooseConfig(egl10, eGLDisplay, eGLConfigArr);
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            int i = 0;
            while (i < eGLConfigArr.length) {
                EGLConfig eGLConfig = eGLConfigArr[i];
                int findConfigAttrib = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12326, 0);
                if (findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12325, 0) >= this.mDepthSize) {
                    if (findConfigAttrib < this.mStencilSize) {
                        i++;
                    } else {
                        int findConfigAttrib2 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12323, 0);
                        int findConfigAttrib3 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12322, 0);
                        int findConfigAttrib4 = findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12321, 0);
                        if (findConfigAttrib(egl10, eGLDisplay, eGLConfig, 12324, 0) == this.mRedSize && findConfigAttrib2 == this.mGreenSize && findConfigAttrib3 == this.mBlueSize && findConfigAttrib4 == this.mAlphaSize) {
                            return eGLConfig;
                        }
                    }
                }
                i++;
            }
            return null;
        }
    }

    private static class ContextFactory implements GLSurfaceView.EGLContextFactory {
        private static int EGL_CONTEXT_CLIENT_VERSION = 12440;

        ContextFactory() {
        }

        ContextFactory(ContextFactory contextFactory) {
            this();
        }

        public EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            Log.w(GL2JNIView.TAG, "creating OpenGL ES 2.0 context");
            GL2JNIView.checkEglError("Before eglCreateContext", egl10);
            int[] iArr = {EGL_CONTEXT_CLIENT_VERSION, 2, 12344};
            GL2JNIView.checkEglError("After eglCreateContext", egl10);
            return egl10.eglCreateContext(eGLDisplay, eGLConfig, EGL10.EGL_NO_CONTEXT, iArr);
        }

        public void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            egl10.eglDestroyContext(eGLDisplay, eGLContext);
        }
    }

    public static class Renderer implements GLSurfaceView.Renderer {
        public static int DefinallyROTATION = 0;
        public static s activity;
        public static Bitmap bmp;
        static boolean capture = false;
        public static int colors = 0;
        public static int number = 0;
        public static String path;
        static boolean share = false;
        public static float tail = 0.0f;
        public static float thick = 0.0f;
        public static int windowHEIGHT = 0;
        public static int windowWIDTH = 0;

        public void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        }

        class InnerClass_1 extends BroadcastReceiver {
            private final Renderer this$0;

            static Renderer access$0(InnerClass_1 innerClass_1) {
                return innerClass_1.this$0;
            }

            InnerClass_1(Renderer renderer) {
                this.this$0 = renderer;
            }

            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals("android.intent.action.MEDIA_SCANNER_STARTED")) {
                    Log.e("haha", "start???");
                } else if (intent.getAction().equals("android.intent.action.MEDIA_SCANNER_FINISHED")) {
                    Log.e("haha", "finish???");
                } else if (intent.getAction().equals("android.intent.action.CALL")) {
                    Log.e("haha", "call1???");
                } else if (intent.getAction().equals("android.intent.action.MEDIA_MOUNTED")) {
                    Log.e("haha", "call2???");
                    Toast.makeText(GL2JNIView.m_Context, "Saving to PhotoAlbum", 1).show();
                } else if (intent.getAction().equals("android.intent.action.MEDIA_UNMOUNTED")) {
                    Log.e("haha", "call3???");
                    Toast.makeText(GL2JNIView.m_Context, "Saving Error ! There is no", 1).show();
                }
            }
        }

        public static void SavePic() {
            capture = true;
        }

        public static Bitmap SavePixels(int i, int i2, int i3, int i4, GL10 gl10) {
            int[] iArr = new int[((i2 + i4) * i3)];
            int[] iArr2 = new int[(i3 * i4)];
            IntBuffer wrap = IntBuffer.wrap(iArr);
            wrap.position(0);
            gl10.glReadPixels(i, 0, i3, i2 + i4, 6408, 5121, wrap);
            int i5 = 0;
            int i6 = 0;
            while (i6 < i4) {
                for (int i7 = 0; i7 < i3; i7++) {
                    int i8 = iArr[(i6 * i3) + i7];
                    iArr2[(((i4 - i5) - 1) * i3) + i7] = ((i8 >> 16) & 255) | (-16711936 & i8) | ((i8 << 16) & -65536);
                }
                i6++;
                i5++;
            }
            return Bitmap.createBitmap(iArr2, i3, i4, Bitmap.Config.ARGB_8888);
        }

        public static void SharePic() {
            share = true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
         arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
         candidates:
          ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
          ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
        public static Bitmap rotate(Bitmap bitmap, int i) {
            if (i == 0 || bitmap == null) {
                return bitmap;
            }
            Matrix matrix = new Matrix();
            matrix.setRotate((float) i, ((float) bitmap.getWidth()) / 2.0f, ((float) bitmap.getHeight()) / 2.0f);
            try {
                Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                if (bitmap == createBitmap) {
                    return bitmap;
                }
                bitmap.recycle();
                return createBitmap;
            } catch (OutOfMemoryError e) {
                return bitmap;
            }
        }

        public static void saveAlbum(GL10 gl10) {
            bmp = SavePixels(0, 0, windowWIDTH, windowHEIGHT, gl10);
            bmp = rotate(bmp, DefinallyROTATION);
            path = MediaStore.Images.Media.insertImage(GL2JNIView.m_Context.getContentResolver(), bmp, "hi", (String) null);
            IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_SCANNER_STARTED");
            intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
            intentFilter.addDataScheme("file");
        }

        public void onDrawFrame(GL10 gl10) {
            if (capture) {
                saveAlbum(gl10);
                capture = false;
            }
            GL2JNIView.UpdateStarEngine();
        }

        public void onSurfaceChanged(GL10 gl10, int i, int i2) {
            Log.e("omg", "onSurfaceChanged -> CALL ALL JNI FUNC");
            windowWIDTH = i;
            windowHEIGHT = i2;
            GL2JNIView.TurnOnStarEngine(i, i2);
            Log.e("haha", "number:" + number + "tail:" + tail + "thick:" + thick + "colors:" + colors);
            if (number != 0) {
                GL2JNIView.JNINumber(number);
            }
            if (tail != 0.0f) {
                GL2JNIView.JNITail(tail);
            }
            if (thick != 0.0f) {
                GL2JNIView.JNIThick(thick);
            }
            if (colors != 0) {
                GL2JNIView.JNIColor(colors);
            }
        }

        public void saveandShare(GL10 gl10) {
            bmp = SavePixels(0, 0, windowWIDTH, windowHEIGHT, gl10);
            bmp = rotate(bmp, DefinallyROTATION);
            path = MediaStore.Images.Media.insertImage(GL2JNIView.m_Context.getContentResolver(), bmp, "hi", (String) null);
            IntentFilter intentFilter = new IntentFilter("android.intent.action.MEDIA_SCANNER_STARTED");
            intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentFilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
            intentFilter.addDataScheme("file");
        }
    }

    static {
        System.loadLibrary("StarEngine");
    }

    public GL2JNIView(Context context) {
        super(context);
        m_Context = context;
        init(false, 0, 0);
    }

    public GL2JNIView(Context context, boolean z, int i, int i2) {
        super(context);
        init(z, i, i2);
    }

    /* access modifiers changed from: private */
    public static void checkEglError(String str, EGL10 egl10) {
        while (true) {
            int eglGetError = egl10.eglGetError();
            if (eglGetError != 12288) {
                Log.e(TAG, String.format("%s: EGL error: 0x%x", str, Integer.valueOf(eglGetError)));
            } else {
                return;
            }
        }
    }

    private void init(boolean z, int i, int i2) {
        ConfigChooser configChooser;
        if (z) {
            getHolder().setFormat(-3);
        }
        setEGLContextFactory(new ContextFactory(null));
        if (z) {
            configChooser = new ConfigChooser(8, 8, 8, 8, i, i2);
        } else {
            configChooser = new ConfigChooser(5, 6, 5, 0, i, i2);
        }
        setEGLConfigChooser(configChooser);
        setRenderer(new Renderer());
    }
}
