package com.sunglab.bigbanghd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Service extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            abortBroadcast();
            try {
                Intent intent2 = new Intent(context, Class.forName("com.sunglab.bigbanghd.s"));
                intent2.addFlags(268435456);
                context.startActivity(intent2);
            } catch (ClassNotFoundException e) {
                throw new NoClassDefFoundError(e.getMessage());
            }
        }
    }
}
