package Xbox;

import it.sauronsoftware.base64.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class Base64Utils {
    private static final int CACHE_SIZE = 1024;

    public static byte[] decode(String str) throws Exception {
        return Base64.decode(str.getBytes());
    }

    public static String encode(byte[] bArr) throws Exception {
        String str;
        new String(Base64.encode(bArr));
        return str;
    }

    public static String encodeFile(String str) throws Exception {
        return encode(fileToByte(str));
    }

    public static void decodeToFile(String str, String str2) throws Exception {
        byteArrayToFile(decode(str2), str);
    }

    public static byte[] fileToByte(String str) throws Exception {
        File file;
        FileInputStream fileInputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        byte[] bArr = new byte[0];
        new File(str);
        File file2 = file;
        if (file2.exists()) {
            new FileInputStream(file2);
            FileInputStream fileInputStream2 = fileInputStream;
            new ByteArrayOutputStream(2048);
            ByteArrayOutputStream byteArrayOutputStream2 = byteArrayOutputStream;
            byte[] bArr2 = new byte[CACHE_SIZE];
            while (true) {
                int read = fileInputStream2.read(bArr2);
                int i = read;
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream2.write(bArr2, 0, i);
                byteArrayOutputStream2.flush();
            }
            byteArrayOutputStream2.close();
            fileInputStream2.close();
            bArr = byteArrayOutputStream2.toByteArray();
        }
        return bArr;
    }

    public static void byteArrayToFile(byte[] bArr, String str) throws Exception {
        InputStream inputStream;
        File file;
        OutputStream outputStream;
        new ByteArrayInputStream(bArr);
        InputStream inputStream2 = inputStream;
        new File(str);
        File file2 = file;
        if (!file2.getParentFile().exists()) {
            boolean mkdirs = file2.getParentFile().mkdirs();
        }
        boolean createNewFile = file2.createNewFile();
        new FileOutputStream(file2);
        OutputStream outputStream2 = outputStream;
        byte[] bArr2 = new byte[CACHE_SIZE];
        while (true) {
            int read = inputStream2.read(bArr2);
            int i = read;
            if (read == -1) {
                outputStream2.close();
                inputStream2.close();
                return;
            }
            outputStream2.write(bArr2, 0, i);
            outputStream2.flush();
        }
    }
}
