package Xbox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

public class StreamGobbler extends Thread {
    InputStream is;
    OutputStream os;
    String type;

    private void close() {
    }

    StreamGobbler(InputStream inputStream, String str) {
        this(inputStream, str, null);
    }

    StreamGobbler(InputStream inputStream, String str, OutputStream outputStream) {
        this.is = inputStream;
        this.type = str;
        this.os = outputStream;
    }

    public void run() {
        InputStreamReader inputStreamReader;
        BufferedReader bufferedReader;
        StringBuffer stringBuffer;
        StringBuffer stringBuffer2;
        PrintWriter printWriter;
        InputStreamReader inputStreamReader2 = null;
        BufferedReader bufferedReader2 = null;
        PrintWriter printWriter2 = null;
        try {
            if (this.os != null) {
                new PrintWriter(this.os);
                printWriter2 = printWriter;
            }
            new InputStreamReader(this.is);
            inputStreamReader2 = inputStreamReader;
            new BufferedReader(inputStreamReader2);
            bufferedReader2 = bufferedReader;
            String str = null;
            while (true) {
                String readLine = bufferedReader2.readLine();
                String str2 = readLine;
                if (readLine == null) {
                    break;
                }
                if (printWriter2 != null) {
                    printWriter2.println(str2);
                }
                PrintStream printStream = System.out;
                new StringBuffer();
                new StringBuffer();
                printStream.println(stringBuffer.append(stringBuffer2.append(this.type).append(">").toString()).append(str2).toString());
            }
            if (printWriter2 != null) {
                printWriter2.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Throwable th) {
            Throwable th2 = th;
            try {
                printWriter2.close();
                bufferedReader2.close();
                inputStreamReader2.close();
            } catch (Exception e2) {
            }
            throw th2;
        }
        try {
            printWriter2.close();
            bufferedReader2.close();
            inputStreamReader2.close();
        } catch (Exception e3) {
        }
    }
}
