package Xbox;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;

public class AESUtils {
    private static final String ALGORITHM = "AES";
    private static final int CACHE_SIZE = 1024;
    private static final int KEY_SIZE = 128;

    public static String getSecretKey() throws Exception {
        return getSecretKey(null);
    }

    public static String getSecretKey(String str) throws Exception {
        SecureRandom secureRandom;
        SecureRandom secureRandom2;
        SecureRandom secureRandom3;
        String str2 = str;
        KeyGenerator instance = KeyGenerator.getInstance(ALGORITHM);
        if (str2 == null || "".equals(str2)) {
            new SecureRandom();
            secureRandom2 = secureRandom;
        } else {
            new SecureRandom(str2.getBytes());
            secureRandom2 = secureRandom3;
        }
        instance.init((int) KEY_SIZE, secureRandom2);
        return Base64Utils.encode(instance.generateKey().getEncoded());
    }

    public static byte[] encrypt(byte[] bArr, String str) throws Exception {
        Key key;
        new SecretKeySpec(toKey(Base64Utils.decode(str)).getEncoded(), ALGORITHM);
        Cipher instance = Cipher.getInstance(ALGORITHM);
        instance.init(1, key);
        return instance.doFinal(bArr);
    }

    public static void encryptFile(String str, String str2, String str3) throws Exception {
        File file;
        File file2;
        InputStream inputStream;
        OutputStream outputStream;
        Key key;
        CipherInputStream cipherInputStream;
        String str4 = str;
        new File(str2);
        File file3 = file;
        new File(str3);
        File file4 = file2;
        if (file3.exists() && file3.isFile()) {
            if (!file4.getParentFile().exists()) {
                boolean mkdirs = file4.getParentFile().mkdirs();
            }
            boolean createNewFile = file4.createNewFile();
            new FileInputStream(file3);
            InputStream inputStream2 = inputStream;
            new FileOutputStream(file4);
            OutputStream outputStream2 = outputStream;
            new SecretKeySpec(toKey(Base64Utils.decode(str4)).getEncoded(), ALGORITHM);
            Cipher instance = Cipher.getInstance(ALGORITHM);
            instance.init(1, key);
            new CipherInputStream(inputStream2, instance);
            CipherInputStream cipherInputStream2 = cipherInputStream;
            byte[] bArr = new byte[CACHE_SIZE];
            while (true) {
                int read = cipherInputStream2.read(bArr);
                int i = read;
                if (read == -1) {
                    outputStream2.close();
                    cipherInputStream2.close();
                    inputStream2.close();
                    return;
                }
                outputStream2.write(bArr, 0, i);
                outputStream2.flush();
            }
        }
    }

    public static byte[] decrypt(byte[] bArr, String str) throws Exception {
        Key key;
        new SecretKeySpec(toKey(Base64Utils.decode(str)).getEncoded(), ALGORITHM);
        Cipher instance = Cipher.getInstance(ALGORITHM);
        instance.init(2, key);
        return instance.doFinal(bArr);
    }

    public static void decryptFile(String str, String str2, String str3) throws Exception {
        File file;
        File file2;
        FileInputStream fileInputStream;
        FileOutputStream fileOutputStream;
        Key key;
        CipherOutputStream cipherOutputStream;
        String str4 = str;
        new File(str2);
        File file3 = file;
        new File(str3);
        File file4 = file2;
        if (file3.exists() && file3.isFile()) {
            if (!file4.getParentFile().exists()) {
                boolean mkdirs = file4.getParentFile().mkdirs();
            }
            boolean createNewFile = file4.createNewFile();
            new FileInputStream(file3);
            FileInputStream fileInputStream2 = fileInputStream;
            new FileOutputStream(file4);
            FileOutputStream fileOutputStream2 = fileOutputStream;
            new SecretKeySpec(toKey(Base64Utils.decode(str4)).getEncoded(), ALGORITHM);
            Cipher instance = Cipher.getInstance(ALGORITHM);
            instance.init(2, key);
            new CipherOutputStream(fileOutputStream2, instance);
            CipherOutputStream cipherOutputStream2 = cipherOutputStream;
            byte[] bArr = new byte[CACHE_SIZE];
            while (true) {
                int read = fileInputStream2.read(bArr);
                int i = read;
                if (read == -1) {
                    cipherOutputStream2.close();
                    fileOutputStream2.close();
                    fileInputStream2.close();
                    return;
                }
                cipherOutputStream2.write(bArr, 0, i);
                cipherOutputStream2.flush();
            }
        }
    }

    private static Key toKey(byte[] bArr) throws Exception {
        Key key;
        new SecretKeySpec(bArr, ALGORITHM);
        return key;
    }
}
