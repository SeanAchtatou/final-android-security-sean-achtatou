package it.sauronsoftware.base64;

import java.io.IOException;
import java.io.InputStream;

public class Base64InputStream extends InputStream {
    private int[] buffer;
    private int bufferCounter = 0;
    private boolean eof = false;
    private InputStream inputStream;

    public Base64InputStream(InputStream inputStream2) {
        this.inputStream = inputStream2;
    }

    private void acquire() throws IOException {
        Throwable th;
        int i;
        Throwable th2;
        Throwable th3;
        Throwable th4;
        char[] cArr = new char[4];
        int i2 = 0;
        do {
            int read = this.inputStream.read();
            if (read != -1) {
                char c = (char) read;
                if (Shared.chars.indexOf(c) != -1 || c == Shared.pad) {
                    int i3 = i2;
                    i2++;
                    cArr[i3] = c;
                } else if (!(c == 13 || c == 10)) {
                    Throwable th5 = th4;
                    new IOException("Bad base64 stream");
                    throw th5;
                }
            } else if (i2 != 0) {
                Throwable th6 = th;
                new IOException("Bad base64 stream");
                throw th6;
            } else {
                this.buffer = new int[0];
                this.eof = true;
                return;
            }
        } while (i2 < 4);
        boolean z = false;
        for (int i4 = 0; i4 < 4; i4++) {
            if (cArr[i4] != Shared.pad) {
                if (z) {
                    Throwable th7 = th3;
                    new IOException("Bad base64 stream");
                    throw th7;
                }
            } else if (!z) {
                z = true;
            }
        }
        if (cArr[3] != Shared.pad) {
            i = 3;
        } else if (this.inputStream.read() != -1) {
            Throwable th8 = th2;
            new IOException("Bad base64 stream");
            throw th8;
        } else {
            this.eof = true;
            i = cArr[2] == Shared.pad ? 1 : 2;
        }
        int i5 = 0;
        for (int i6 = 0; i6 < 4; i6++) {
            if (cArr[i6] != Shared.pad) {
                i5 |= Shared.chars.indexOf(cArr[i6]) << (6 * (3 - i6));
            }
        }
        this.buffer = new int[i];
        for (int i7 = 0; i7 < i; i7++) {
            this.buffer[i7] = (i5 >>> (8 * (2 - i7))) & 255;
        }
    }

    public void close() throws IOException {
        this.inputStream.close();
    }

    public int read() throws IOException {
        if (this.buffer == null || this.bufferCounter == this.buffer.length) {
            if (this.eof) {
                return -1;
            }
            acquire();
            if (this.buffer.length == 0) {
                this.buffer = null;
                return -1;
            }
            this.bufferCounter = 0;
        }
        int[] iArr = this.buffer;
        int i = this.bufferCounter;
        this.bufferCounter = i + 1;
        return iArr[i];
    }
}
