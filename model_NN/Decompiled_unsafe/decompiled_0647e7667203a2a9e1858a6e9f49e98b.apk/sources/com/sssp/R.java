package com.sssp;

public final class R {

    public static final class anim {
        public static final int show1 = 2130968576;
        public static final int show2 = 2130968577;
        public static final int show3 = 2130968578;
        public static final int show4 = 2130968579;
        public static final int tvanim = 2130968580;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int down = 2131165184;
        public static final int stroke = 2131165186;
        public static final int up = 2131165185;
    }

    public static final class drawable {
        public static final int authorization_manager_image = 2130837504;
        public static final int btn_green_normal = 2130837505;
        public static final int buttonyun = 2130837506;
        public static final int ed = 2130837507;
        public static final int ic_launcher = 2130837508;
        public static final int ic_mess_receiver_btn_pressed = 2130837509;
        public static final int icon = 2130837510;
        public static final int image_1 = 2130837511;
        public static final int tv = 2130837512;
    }

    public static final class id {
        public static final int bt = 2131361794;
        public static final int ed = 2131361793;
        public static final int tv = 2131361792;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int newone = 2130903041;
    }

    public static final class raw {
        public static final int bahk = 2131099648;
        public static final int gdmm = 2131099649;
        public static final int pin = 2131099650;
    }

    public static final class string {
        public static final int app_name = 2131230721;
        public static final int hello = 2131230720;
    }

    public static final class style {
        public static final int AppTheme = 2131296256;
    }

    public static final class xml {
        public static final int my_admin = 2131034112;
    }
}
