package Cute;

import java.io.InputStream;
import java.io.OutputStream;

public class StreamGobbler extends Thread {
    InputStream is;
    OutputStream os;
    String type;

    StreamGobbler(InputStream inputStream, String str) {
        this(inputStream, str, null);
    }

    StreamGobbler(InputStream inputStream, String str, OutputStream outputStream) {
        this.is = inputStream;
        this.type = str;
        this.os = outputStream;
    }

    private void close() {
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r9 = this;
            r1 = 0
            r0 = r1
            java.io.InputStreamReader r0 = (java.io.InputStreamReader) r0
            r2 = r1
            java.io.BufferedReader r2 = (java.io.BufferedReader) r2
            java.io.PrintWriter r1 = (java.io.PrintWriter) r1
            java.io.OutputStream r3 = r9.os     // Catch:{ IOException -> 0x0096, all -> 0x0074 }
            if (r3 == 0) goto L_0x00a5
            java.io.PrintWriter r3 = new java.io.PrintWriter     // Catch:{ IOException -> 0x0096, all -> 0x0074 }
            java.io.OutputStream r4 = r9.os     // Catch:{ IOException -> 0x0096, all -> 0x0074 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0096, all -> 0x0074 }
        L_0x0014:
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x009b, all -> 0x0087 }
            java.io.InputStream r1 = r9.is     // Catch:{ IOException -> 0x009b, all -> 0x0087 }
            r4.<init>(r1)     // Catch:{ IOException -> 0x009b, all -> 0x0087 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ IOException -> 0x00a1, all -> 0x008c }
            r1.<init>(r4)     // Catch:{ IOException -> 0x00a1, all -> 0x008c }
            r0 = 0
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0069, all -> 0x008f }
        L_0x0023:
            java.lang.String r0 = r1.readLine()     // Catch:{ IOException -> 0x0069, all -> 0x008f }
            if (r0 != 0) goto L_0x0038
            if (r3 == 0) goto L_0x002e
            r3.flush()     // Catch:{ IOException -> 0x0069, all -> 0x008f }
        L_0x002e:
            r3.close()     // Catch:{ Exception -> 0x0083 }
            r1.close()     // Catch:{ Exception -> 0x0083 }
            r4.close()     // Catch:{ Exception -> 0x0083 }
        L_0x0037:
            return
        L_0x0038:
            if (r3 == 0) goto L_0x003d
            r3.println(r0)     // Catch:{  }
        L_0x003d:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{  }
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{  }
            r5.<init>()     // Catch:{  }
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{  }
            r6.<init>()     // Catch:{  }
            java.lang.String r7 = r9.type     // Catch:{  }
            java.lang.StringBuffer r6 = r6.append(r7)     // Catch:{  }
            java.lang.String r7 = ">"
            java.lang.StringBuffer r6 = r6.append(r7)     // Catch:{  }
            java.lang.String r6 = r6.toString()     // Catch:{  }
            java.lang.StringBuffer r5 = r5.append(r6)     // Catch:{  }
            java.lang.StringBuffer r0 = r5.append(r0)     // Catch:{  }
            java.lang.String r0 = r0.toString()     // Catch:{  }
            r2.println(r0)     // Catch:{  }
            goto L_0x0023
        L_0x0069:
            r0 = move-exception
            r2 = r1
            r1 = r3
            r3 = r4
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x0091 }
            r4 = r3
            r3 = r1
            r1 = r2
            goto L_0x002e
        L_0x0074:
            r3 = move-exception
            r4 = r0
            r0 = r3
            r3 = r1
            r1 = r2
        L_0x0079:
            r3.close()     // Catch:{ Exception -> 0x0085 }
            r1.close()     // Catch:{ Exception -> 0x0085 }
            r4.close()     // Catch:{ Exception -> 0x0085 }
        L_0x0082:
            throw r0
        L_0x0083:
            r0 = move-exception
            goto L_0x0037
        L_0x0085:
            r1 = move-exception
            goto L_0x0082
        L_0x0087:
            r1 = move-exception
            r4 = r0
            r0 = r1
            r1 = r2
            goto L_0x0079
        L_0x008c:
            r0 = move-exception
            r1 = r2
            goto L_0x0079
        L_0x008f:
            r0 = move-exception
            goto L_0x0079
        L_0x0091:
            r0 = move-exception
            r4 = r3
            r3 = r1
            r1 = r2
            goto L_0x0079
        L_0x0096:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x006d
        L_0x009b:
            r1 = move-exception
            r8 = r1
            r1 = r3
            r3 = r0
            r0 = r8
            goto L_0x006d
        L_0x00a1:
            r0 = move-exception
            r1 = r3
            r3 = r4
            goto L_0x006d
        L_0x00a5:
            r3 = r1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: Cute.StreamGobbler.run():void");
    }
}
