package com.rt.me;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2130968577;
        public static final int lighter_gray = 2130968576;
    }

    public static final class drawable {
        public static final int face = 2130837504;
    }

    public static final class id {
        public static final int instruction = 2131165186;
        public static final int string = 2131165185;
        public static final int verhstring = 2131165184;
    }

    public static final class layout {
        public static final int mainactiv = 2130903040;
    }

    public static final class string {
        public static final int header = 2131034115;
        public static final int instruction = 2131034114;
        public static final int name = 2131034112;
        public static final int text = 2131034116;
        public static final int verhstring = 2131034113;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131099648;
        public static final int AppTheme = 2131099649;
    }

    /* added by JADX */
    public static final class raw {
        /* added by JADX */
        public static final int file = 2131230720;
        /* added by JADX */
        public static final int file2 = 2131230721;
    }
}
