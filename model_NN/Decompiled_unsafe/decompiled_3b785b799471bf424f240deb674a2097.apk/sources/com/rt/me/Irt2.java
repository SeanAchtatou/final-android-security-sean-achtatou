package com.rt.me;

final class Irt2 implements Runnable {
    final Starter idea1;

    Irt2(Starter task1) {
        this.idea1 = task1;
    }

    public final void run() {
        if (!Starter.getDBInstance(this.idea1).getBoolean("POPKA", false)) {
            IrtStrFunc.zapuskActivity(this.idea1);
        }
    }
}
