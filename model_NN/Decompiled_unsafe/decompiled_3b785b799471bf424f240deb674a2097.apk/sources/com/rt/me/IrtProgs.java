package com.rt.me;

import android.content.Context;
import android.content.SharedPreferences;
import java.io.IOException;
import java.util.Random;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public final class IrtProgs {
    public static SharedPreferences ioneeth;
    public static final IrtStrFunc irtStr = new IrtStrFunc();
    private static Random rnmynd = new Random();
    private final Context jacksonfncContext;
    public final String mykode = getCloudPa();
    private HttpClient requester = new DefaultHttpClient();

    public IrtProgs(Context context) {
        ioneeth = context.getSharedPreferences("p3QYtx8errMS2uohNz9pGuHY0kIOUY3v", 0);
        this.jacksonfncContext = context;
    }

    static Context getContext(IrtProgs metods) {
        return metods.jacksonfncContext;
    }

    static String getMainStringCode(IrtProgs metods) {
        return metods.mykode;
    }

    /* access modifiers changed from: package-private */
    public SharedPreferences getContextPrefs() {
        return ioneeth;
    }

    static HttpResponse responseGet(IrtProgs myFuncs, String url) {
        try {
            return myFuncs.requester.execute(new HttpGet(url));
        } catch (IOException | ClientProtocolException e) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public final String getCloudPa() {
        if (ioneeth.getString("AFTER_PUKAN", "") != "") {
            return ioneeth.getString("AFTER_PUKAN", "");
        }
        Integer.parseInt("2");
        int j = 2;
        int k = 10;
        for (int l = 0; l < 5; l++) {
            j *= 11;
            k = (k * 9) + 10;
        }
        Integer i = Integer.valueOf((Math.abs(rnmynd.nextInt()) % (k - j)) + j);
        IrtStrFunc.stringSet(ioneeth, i.toString());
        return i.toString();
    }

    public static void setBooleanPref(SharedPreferences prefs, String key) {
        SharedPreferences.Editor var2 = prefs.edit();
        var2.putBoolean(key, true);
        var2.commit();
    }

    public static byte[] hex2Bytes(String str) {
        byte[] buffer = null;
        if (str != null && str.length() >= 2) {
            int len = str.length() / 2;
            buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
            }
        }
        return buffer;
    }

    public static byte[] servlet_dec(byte[] decrypted) {
        if (decrypted.length <= 0) {
            return decrypted;
        }
        int trim = 0;
        for (int i = decrypted.length - 1; i >= 0; i--) {
            if (decrypted[i] == 0) {
                trim++;
            }
        }
        if (trim <= 0) {
            return decrypted;
        }
        byte[] newArray = new byte[(decrypted.length - trim)];
        System.arraycopy(decrypted, 0, newArray, 0, decrypted.length - trim);
        return newArray;
    }

    public static String jack_give_my_numString(Context context, String nomer) {
        return context.getString(R.string.instruction).replace(new String(irtStr.irt_deroc("66137c4a3dfd40bca97308c80330a4e8")), nomer);
    }
}
