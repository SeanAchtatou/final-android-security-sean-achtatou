package com;

import java.io.PrintStream;

public class EncryptString {
    public static String applyCaesar(String str, int i) {
        char[] cArr = (char[]) String.class.getMethod("toCharArray", new Class[0]).invoke(str, new Object[0]);
        int i2 = 0;
        while (true) {
            if (i2 >= ((Integer) String.class.getMethod("length", new Class[0]).invoke(str, new Object[0])).intValue()) {
                return new String(cArr);
            }
            char c = cArr[i2];
            if (c >= ' ' && c <= 127) {
                int i3 = ((c - ' ') + i) % 96;
                if (i3 < 0) {
                    i3 += 96;
                }
                cArr[i2] = (char) (i3 + 32);
            }
            i2++;
        }
    }

    public static String applyCaesar(String str) {
        return applyCaesar(str, -1);
    }

    public static void main(String[] strArr) {
        Object[] objArr = {"hello world!"};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr);
        String applyCaesar = applyCaesar("hello world!", 1);
        Object[] objArr2 = {applyCaesar};
        PrintStream.class.getMethod("println", String.class).invoke(System.out, objArr2);
        PrintStream printStream = System.out;
        Object[] objArr3 = {applyCaesar(applyCaesar)};
        PrintStream.class.getMethod("println", String.class).invoke(printStream, objArr3);
    }
}
