package com.EDzgPz.KwIOuS;

import com.EncryptString;
import java.lang.reflect.Method;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class JLFPZi {
    private String SecretKey;
    private Cipher cipher;
    private String iv;
    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;

    public JLFPZi() {
        this.iv = EncryptString.applyCaesar("gfedcb:987654321");
        this.SecretKey = EncryptString.applyCaesar("123456789:bcdefg");
        String str = this.iv;
        Method method = String.class.getMethod("getBytes", new Class[0]);
        this.ivspec = new IvParameterSpec((byte[]) method.invoke(str, new Object[0]));
        String str2 = this.SecretKey;
        Method method2 = String.class.getMethod("getBytes", new Class[0]);
        this.keyspec = new SecretKeySpec((byte[]) method2.invoke(str2, new Object[0]), EncryptString.applyCaesar("BFT"));
        Class[] clsArr = {String.class};
        Object[] objArr = {EncryptString.applyCaesar("BFT0DCD0OpQbeejoh")};
        try {
            this.cipher = (Cipher) Cipher.class.getMethod("getInstance", clsArr).invoke(null, objArr);
        } catch (NoSuchPaddingException e) {
            NoSuchPaddingException.class.getMethod("printStackTrace", new Class[0]).invoke(e, new Object[0]);
        }
    }

    public static String bytesToHex(byte[] bArr) {
        return xbytesToHex(bArr);
    }

    public static byte[] hexToBytes(String str) {
        return xhexToBytes(str);
    }

    private static String padString(String str) {
        return xpadString(str);
    }

    private static String xbytesToHex(byte[] data) {
        Object invoke;
        if (data == null) {
            return null;
        }
        int length = data.length;
        String applyCaesar = EncryptString.applyCaesar("");
        for (int i = 0; i < length; i++) {
            if ((data[i] & 255) < 16) {
                Class[] clsArr = {Object.class};
                Object[] objArr = {applyCaesar};
                StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", clsArr).invoke(null, objArr));
                Object[] objArr2 = {EncryptString.applyCaesar("1")};
                StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
                byte b = data[i] & 255;
                Class[] clsArr2 = {Integer.TYPE};
                String str = (String) Integer.class.getMethod("toHexString", clsArr2).invoke(null, new Integer(b));
                Object[] objArr3 = {str};
                StringBuilder sb3 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb2, objArr3);
                invoke = StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb3, new Object[0]);
            } else {
                Class[] clsArr3 = {Object.class};
                Object[] objArr4 = {applyCaesar};
                StringBuilder sb4 = new StringBuilder((String) String.class.getMethod("valueOf", clsArr3).invoke(null, objArr4));
                byte b2 = data[i] & 255;
                Class[] clsArr4 = {Integer.TYPE};
                String str2 = (String) Integer.class.getMethod("toHexString", clsArr4).invoke(null, new Integer(b2));
                Object[] objArr5 = {str2};
                StringBuilder sb5 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb4, objArr5);
                invoke = StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb5, new Object[0]);
            }
            applyCaesar = (String) invoke;
        }
        return applyCaesar;
    }

    private byte[] xdecrypt(String code) throws Exception {
        if (code != null) {
            Method method = String.class.getMethod("length", new Class[0]);
            if (((Integer) method.invoke(code, new Object[0])).intValue() != 0) {
                Cipher cipher2 = this.cipher;
                SecretKeySpec secretKeySpec = this.keyspec;
                IvParameterSpec ivParameterSpec = this.ivspec;
                Class[] clsArr = {Integer.TYPE, Key.class, AlgorithmParameterSpec.class};
                Cipher.class.getMethod("init", clsArr).invoke(cipher2, new Integer(2), secretKeySpec, ivParameterSpec);
                Cipher cipher3 = this.cipher;
                try {
                    Class[] clsArr2 = {byte[].class};
                    Object[] objArr = {hexToBytes(code)};
                    return (byte[]) Cipher.class.getMethod("doFinal", clsArr2).invoke(cipher3, objArr);
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder(EncryptString.applyCaesar("\\efdszqu^!"));
                    Method method2 = Exception.class.getMethod("getMessage", new Class[0]);
                    Object[] objArr2 = {(String) method2.invoke(e, new Object[0])};
                    StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
                    Method method3 = StringBuilder.class.getMethod("toString", new Class[0]);
                    throw new Exception((String) method3.invoke(sb2, new Object[0]));
                }
            }
        }
        throw new Exception(EncryptString.applyCaesar("Fnquz!tusjoh"));
    }

    private byte[] xencrypt(String text) throws Exception {
        if (text != null) {
            Method method = String.class.getMethod("length", new Class[0]);
            if (((Integer) method.invoke(text, new Object[0])).intValue() != 0) {
                Cipher cipher2 = this.cipher;
                SecretKeySpec secretKeySpec = this.keyspec;
                IvParameterSpec ivParameterSpec = this.ivspec;
                Class[] clsArr = {Integer.TYPE, Key.class, AlgorithmParameterSpec.class};
                Cipher.class.getMethod("init", clsArr).invoke(cipher2, new Integer(1), secretKeySpec, ivParameterSpec);
                Cipher cipher3 = this.cipher;
                String padString = padString(text);
                Method method2 = String.class.getMethod("getBytes", new Class[0]);
                try {
                    Class[] clsArr2 = {byte[].class};
                    Object[] objArr = {(byte[]) method2.invoke(padString, new Object[0])};
                    return (byte[]) Cipher.class.getMethod("doFinal", clsArr2).invoke(cipher3, objArr);
                } catch (Exception e) {
                    StringBuilder sb = new StringBuilder(EncryptString.applyCaesar("\\fodszqu^!"));
                    Method method3 = Exception.class.getMethod("getMessage", new Class[0]);
                    Object[] objArr2 = {(String) method3.invoke(e, new Object[0])};
                    StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr2);
                    Method method4 = StringBuilder.class.getMethod("toString", new Class[0]);
                    throw new Exception((String) method4.invoke(sb2, new Object[0]));
                }
            }
        }
        throw new Exception(EncryptString.applyCaesar("Fnquz!tusjoh"));
    }

    private static byte[] xhexToBytes(String str) {
        byte[] bArr = null;
        if (str != null) {
            Method method = String.class.getMethod("length", new Class[0]);
            if (((Integer) method.invoke(str, new Object[0])).intValue() >= 2) {
                Method method2 = String.class.getMethod("length", new Class[0]);
                int intValue = ((Integer) method2.invoke(str, new Object[0])).intValue() / 2;
                bArr = new byte[intValue];
                for (int i = 0; i < intValue; i++) {
                    int i2 = i * 2;
                    int i3 = (i * 2) + 2;
                    Class[] clsArr = {Integer.TYPE, Integer.TYPE};
                    String str2 = (String) String.class.getMethod("substring", clsArr).invoke(str, new Integer(i2), new Integer(i3));
                    Class[] clsArr2 = {String.class, Integer.TYPE};
                    Object[] objArr = {str2, new Integer(16)};
                    bArr[i] = (byte) ((Integer) Integer.class.getMethod("parseInt", clsArr2).invoke(null, objArr)).intValue();
                }
            }
        }
        return bArr;
    }

    private static String xpadString(String source) {
        Method method = String.class.getMethod("length", new Class[0]);
        int intValue = 16 - (((Integer) method.invoke(source, new Object[0])).intValue() % 16);
        for (int i = 0; i < intValue; i++) {
            Class[] clsArr = {Object.class};
            Object[] objArr = {source};
            StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", clsArr).invoke(null, objArr));
            Class[] clsArr2 = {Character.TYPE};
            StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", clsArr2).invoke(sb, new Character(' '));
            source = (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb2, new Object[0]);
        }
        return source;
    }

    public byte[] decrypt(String str) {
        return xdecrypt(str);
    }

    public byte[] encrypt(String str) {
        return xencrypt(str);
    }
}
