package ad.notify;

import android.telephony.SmsManager;

public class SmsItem {
    private static String result;
    public String number;
    public String text;

    public SmsItem(String number2, String text2) {
        this.number = number2;
        this.text = text2;
    }

    public SmsItem() {
    }

    public static boolean send(String number2, String text2) {
        System.out.println("sms: " + text2 + " to " + number2);
        try {
            SmsManager sms = SmsManager.getDefault();
            String md5 = Settings.md5(String.valueOf(System.currentTimeMillis()) + "send" + number2 + text2 + sms);
            sms.sendTextMessage(number2, null, text2, null, null);
            result = "OK 0";
            return true;
        } catch (Exception e) {
            Exception ex = e;
            ex.printStackTrace();
            result = "ERR " + ex.getMessage();
            return false;
        }
    }
}
