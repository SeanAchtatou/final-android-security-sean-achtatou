package adrt;

import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;

public class ADRTSender {
    private static Context context;
    private static String debuggerPackageName;

    public static void onContext(Context context2, String str) {
        context = context2;
        debuggerPackageName = str;
    }

    public static void sendConnect(String str) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent intent3 = intent2.setPackage(debuggerPackageName);
        Intent action = intent2.setAction("com.adrt.CONNECT");
        Intent putExtra = intent2.putExtra("package", str);
        context.sendBroadcast(intent2);
    }

    public static void sendStop(String str) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent intent3 = intent2.setPackage(debuggerPackageName);
        Intent action = intent2.setAction("com.adrt.STOP");
        Intent putExtra = intent2.putExtra("package", str);
        context.sendBroadcast(intent2);
    }

    public static void sendBreakpointHit(String str, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3, ArrayList<String> arrayList4, ArrayList<String> arrayList5, ArrayList<String> arrayList6) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent intent3 = intent2.setPackage(debuggerPackageName);
        Intent action = intent2.setAction("com.adrt.BREAKPOINT_HIT");
        Intent putExtra = intent2.putExtra("package", str);
        Intent putExtra2 = intent2.putExtra("variables", arrayList);
        Intent putExtra3 = intent2.putExtra("variableValues", arrayList2);
        Intent putExtra4 = intent2.putExtra("variableKinds", arrayList3);
        Intent putExtra5 = intent2.putExtra("stackMethods", arrayList4);
        Intent putExtra6 = intent2.putExtra("stackLocations", arrayList5);
        Intent putExtra7 = intent2.putExtra("stackLocationKinds", arrayList6);
        context.sendBroadcast(intent2);
    }

    public static void sendFields(String str, String str2, ArrayList<String> arrayList, ArrayList<String> arrayList2, ArrayList<String> arrayList3) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent intent3 = intent2.setPackage(debuggerPackageName);
        Intent action = intent2.setAction("com.adrt.FIELDS");
        Intent putExtra = intent2.putExtra("package", str);
        Intent putExtra2 = intent2.putExtra("path", str2);
        Intent putExtra3 = intent2.putExtra("fields", arrayList);
        Intent putExtra4 = intent2.putExtra("fieldValues", arrayList2);
        Intent putExtra5 = intent2.putExtra("fieldKinds", arrayList3);
        context.sendBroadcast(intent2);
    }

    public static void sendLogcatLines(String[] strArr) {
        Intent intent;
        new Intent();
        Intent intent2 = intent;
        Intent intent3 = intent2.setPackage(debuggerPackageName);
        Intent action = intent2.setAction("com.adrt.LOGCAT_ENTRIES");
        Intent putExtra = intent2.putExtra("lines", strArr);
        context.sendBroadcast(intent2);
    }
}
