package com.android.tencent.zdevs.bah;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.internal.view.SupportMenu;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.widget.ProgressBar;

public class QQqun571012706 extends ProgressBar {
    Paint mPaint;
    String text;

    public QQqun571012706(Context context) {
        super(context);
        initText();
    }

    public QQqun571012706(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initText();
    }

    public QQqun571012706(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initText();
    }

    @Override
    public synchronized void setProgress(int i) {
        int i2 = i;
        synchronized (this) {
            setText(i2);
            super.setProgress(i2);
        }
    }

    /* access modifiers changed from: protected */
    @Override
    public synchronized void onDraw(Canvas canvas) {
        Rect rect;
        Canvas canvas2 = canvas;
        synchronized (this) {
            super.onDraw(canvas2);
            new Rect();
            Rect rect2 = rect;
            this.mPaint.getTextBounds(this.text, 0, this.text.length(), rect2);
            canvas2.drawText(this.text, (float) ((getWidth() / 2) - rect2.centerX()), (float) ((getHeight() / 2) - rect2.centerY()), this.mPaint);
        }
    }

    private void initText() {
        Paint paint;
        new Paint();
        this.mPaint = paint;
        this.mPaint.setColor((int) SupportMenu.CATEGORY_MASK);
        if (getResources().getDisplayMetrics().widthPixels > 480) {
            this.mPaint.setTextSize((float) 36);
        } else {
            this.mPaint.setTextSize((float) 18);
        }
    }

    private void setText() {
        setText(getProgress());
    }

    private void setText(int i) {
        StringBuffer stringBuffer;
        new StringBuffer();
        this.text = stringBuffer.append(String.valueOf((i * 100) / getMax())).append("%").toString();
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displayMetrics;
        new DisplayMetrics();
        DisplayMetrics displayMetrics2 = displayMetrics;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics2);
        return displayMetrics2.widthPixels;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displayMetrics;
        new DisplayMetrics();
        DisplayMetrics displayMetrics2 = displayMetrics;
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics2);
        return displayMetrics2.heightPixels;
    }
}
