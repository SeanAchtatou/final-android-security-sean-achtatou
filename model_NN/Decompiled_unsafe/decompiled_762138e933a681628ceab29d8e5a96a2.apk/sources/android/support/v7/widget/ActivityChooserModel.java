package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

class ActivityChooserModel extends DataSetObservable {
    private static final String ATTRIBUTE_ACTIVITY = "activity";
    private static final String ATTRIBUTE_TIME = "time";
    private static final String ATTRIBUTE_WEIGHT = "weight";
    private static final boolean DEBUG = false;
    private static final int DEFAULT_ACTIVITY_INFLATION = 5;
    private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0f;
    public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
    public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
    private static final String HISTORY_FILE_EXTENSION = ".xml";
    private static final int INVALID_INDEX = -1;
    /* access modifiers changed from: private */
    public static final String LOG_TAG = ActivityChooserModel.class.getSimpleName();
    private static final String TAG_HISTORICAL_RECORD = "historical-record";
    private static final String TAG_HISTORICAL_RECORDS = "historical-records";
    private static final Map<String, ActivityChooserModel> sDataModelRegistry;
    private static final Object sRegistryLock;
    private final List<ActivityResolveInfo> mActivities;
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter;
    private boolean mCanReadHistoricalData = true;
    /* access modifiers changed from: private */
    public final Context mContext;
    private final List<HistoricalRecord> mHistoricalRecords;
    private boolean mHistoricalRecordsChanged = true;
    /* access modifiers changed from: private */
    public final String mHistoryFileName;
    private int mHistoryMaxSize = 50;
    private final Object mInstanceLock;
    private Intent mIntent;
    private boolean mReadShareHistoryCalled = false;
    private boolean mReloadActivities = false;

    public interface ActivityChooserModelClient {
        void setActivityChooserModel(ActivityChooserModel activityChooserModel);
    }

    public interface ActivitySorter {
        void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2);
    }

    public interface OnChooseActivityListener {
        boolean onChooseActivity(ActivityChooserModel activityChooserModel, Intent intent);
    }

    static /* synthetic */ boolean access$502(ActivityChooserModel activityChooserModel, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        activityChooserModel.mCanReadHistoricalData = z3;
        return z2;
    }

    static {
        Object obj;
        Map<String, ActivityChooserModel> map;
        new Object();
        sRegistryLock = obj;
        new HashMap();
        sDataModelRegistry = map;
    }

    /* JADX INFO: finally extract failed */
    public static ActivityChooserModel get(Context context, String str) {
        ActivityChooserModel activityChooserModel;
        Context context2 = context;
        String str2 = str;
        Object obj = sRegistryLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ActivityChooserModel activityChooserModel2 = sDataModelRegistry.get(str2);
                if (activityChooserModel2 == null) {
                    new ActivityChooserModel(context2, str2);
                    activityChooserModel2 = activityChooserModel;
                    ActivityChooserModel put = sDataModelRegistry.put(str2, activityChooserModel2);
                }
                ActivityChooserModel activityChooserModel3 = activityChooserModel2;
                return activityChooserModel3;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    private ActivityChooserModel(Context context, String str) {
        Object obj;
        List<ActivityResolveInfo> list;
        List<HistoricalRecord> list2;
        ActivitySorter activitySorter;
        StringBuilder sb;
        String str2 = str;
        new Object();
        this.mInstanceLock = obj;
        new ArrayList();
        this.mActivities = list;
        new ArrayList();
        this.mHistoricalRecords = list2;
        new DefaultSorter();
        this.mActivitySorter = activitySorter;
        this.mContext = context.getApplicationContext();
        if (TextUtils.isEmpty(str2) || str2.endsWith(HISTORY_FILE_EXTENSION)) {
            this.mHistoryFileName = str2;
            return;
        }
        new StringBuilder();
        this.mHistoryFileName = sb.append(str2).append(HISTORY_FILE_EXTENSION).toString();
    }

    /* JADX INFO: finally extract failed */
    public void setIntent(Intent intent) {
        Intent intent2 = intent;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (this.mIntent == intent2) {
                    return;
                }
                this.mIntent = intent2;
                this.mReloadActivities = true;
                ensureConsistentState();
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public Intent getIntent() {
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                Intent intent = this.mIntent;
                return intent;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public int getActivityCount() {
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                int size = this.mActivities.size();
                return size;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public ResolveInfo getActivity(int i) {
        int i2 = i;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                ResolveInfo resolveInfo = this.mActivities.get(i2).resolveInfo;
                return resolveInfo;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public int getActivityIndex(ResolveInfo resolveInfo) {
        ResolveInfo resolveInfo2 = resolveInfo;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                List<ActivityResolveInfo> list = this.mActivities;
                int size = list.size();
                for (int i = 0; i < size; i++) {
                    if (list.get(i).resolveInfo == resolveInfo2) {
                        int i2 = i;
                        return i2;
                    }
                }
                return -1;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public Intent chooseActivity(int i) {
        ComponentName componentName;
        Intent intent;
        HistoricalRecord historicalRecord;
        Intent intent2;
        int i2 = i;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (this.mIntent == null) {
                    return null;
                }
                ensureConsistentState();
                ActivityResolveInfo activityResolveInfo = this.mActivities.get(i2);
                new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
                ComponentName componentName2 = componentName;
                new Intent(this.mIntent);
                Intent intent3 = intent;
                Intent component = intent3.setComponent(componentName2);
                if (this.mActivityChoserModelPolicy != null) {
                    new Intent(intent3);
                    if (this.mActivityChoserModelPolicy.onChooseActivity(this, intent2)) {
                        return null;
                    }
                }
                new HistoricalRecord(componentName2, System.currentTimeMillis(), (float) DEFAULT_HISTORICAL_RECORD_WEIGHT);
                boolean addHisoricalRecord = addHisoricalRecord(historicalRecord);
                Intent intent4 = intent3;
                return intent4;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public void setOnChooseActivityListener(OnChooseActivityListener onChooseActivityListener) {
        OnChooseActivityListener onChooseActivityListener2 = onChooseActivityListener;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                this.mActivityChoserModelPolicy = onChooseActivityListener2;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public ResolveInfo getDefaultActivity() {
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                if (!this.mActivities.isEmpty()) {
                    ResolveInfo resolveInfo = this.mActivities.get(0).resolveInfo;
                    return resolveInfo;
                }
                return null;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void setDefaultActivity(int i) {
        float f;
        ComponentName componentName;
        HistoricalRecord historicalRecord;
        int i2 = i;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                ActivityResolveInfo activityResolveInfo = this.mActivities.get(i2);
                ActivityResolveInfo activityResolveInfo2 = this.mActivities.get(0);
                if (activityResolveInfo2 != null) {
                    f = (activityResolveInfo2.weight - activityResolveInfo.weight) + 5.0f;
                } else {
                    f = 1.0f;
                }
                new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
                new HistoricalRecord(componentName, System.currentTimeMillis(), f);
                boolean addHisoricalRecord = addHisoricalRecord(historicalRecord);
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    private void persistHistoricalDataIfNeeded() {
        Object obj;
        Object obj2;
        Throwable th;
        if (!this.mReadShareHistoryCalled) {
            Throwable th2 = th;
            new IllegalStateException("No preceding call to #readHistoricalData");
            throw th2;
        } else if (this.mHistoricalRecordsChanged) {
            this.mHistoricalRecordsChanged = false;
            if (!TextUtils.isEmpty(this.mHistoryFileName)) {
                Object obj3 = obj;
                new PersistHistoryAsyncTask();
                Object[] objArr = new Object[2];
                new ArrayList(this.mHistoricalRecords);
                objArr[0] = obj2;
                Object[] objArr2 = objArr;
                objArr2[1] = this.mHistoryFileName;
                AsyncTask executeParallel = AsyncTaskCompat.executeParallel(obj3, objArr2);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void setActivitySorter(ActivitySorter activitySorter) {
        ActivitySorter activitySorter2 = activitySorter;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (this.mActivitySorter == activitySorter2) {
                    return;
                }
                this.mActivitySorter = activitySorter2;
                if (sortActivitiesIfNeeded()) {
                    notifyChanged();
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void setHistoryMaxSize(int i) {
        int i2 = i;
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (this.mHistoryMaxSize == i2) {
                    return;
                }
                this.mHistoryMaxSize = i2;
                pruneExcessiveHistoricalRecordsIfNeeded();
                if (sortActivitiesIfNeeded()) {
                    notifyChanged();
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public int getHistoryMaxSize() {
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                int i = this.mHistoryMaxSize;
                return i;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public int getHistorySize() {
        Object obj = this.mInstanceLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                ensureConsistentState();
                int size = this.mHistoricalRecords.size();
                return size;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    private void ensureConsistentState() {
        pruneExcessiveHistoricalRecordsIfNeeded();
        if (loadActivitiesIfNeeded() || readHistoricalDataIfNeeded()) {
            boolean sortActivitiesIfNeeded = sortActivitiesIfNeeded();
            notifyChanged();
        }
    }

    private boolean sortActivitiesIfNeeded() {
        if (this.mActivitySorter == null || this.mIntent == null || this.mActivities.isEmpty() || this.mHistoricalRecords.isEmpty()) {
            return false;
        }
        this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList(this.mHistoricalRecords));
        return true;
    }

    private boolean loadActivitiesIfNeeded() {
        Object obj;
        if (!this.mReloadActivities || this.mIntent == null) {
            return false;
        }
        this.mReloadActivities = false;
        this.mActivities.clear();
        List<ResolveInfo> queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0);
        int size = queryIntentActivities.size();
        for (int i = 0; i < size; i++) {
            new ActivityResolveInfo(queryIntentActivities.get(i));
            boolean add = this.mActivities.add(obj);
        }
        return true;
    }

    private boolean readHistoricalDataIfNeeded() {
        if (!this.mCanReadHistoricalData || !this.mHistoricalRecordsChanged || TextUtils.isEmpty(this.mHistoryFileName)) {
            return false;
        }
        this.mCanReadHistoricalData = false;
        this.mReadShareHistoryCalled = true;
        readHistoricalDataImpl();
        return true;
    }

    private boolean addHisoricalRecord(HistoricalRecord historicalRecord) {
        boolean add = this.mHistoricalRecords.add(historicalRecord);
        if (add) {
            this.mHistoricalRecordsChanged = true;
            pruneExcessiveHistoricalRecordsIfNeeded();
            persistHistoricalDataIfNeeded();
            boolean sortActivitiesIfNeeded = sortActivitiesIfNeeded();
            notifyChanged();
        }
        return add;
    }

    private void pruneExcessiveHistoricalRecordsIfNeeded() {
        int size = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
        if (size > 0) {
            this.mHistoricalRecordsChanged = true;
            for (int i = 0; i < size; i++) {
                HistoricalRecord remove = this.mHistoricalRecords.remove(0);
            }
        }
    }

    public static final class HistoricalRecord {
        public final ComponentName activity;
        public final long time;
        public final float weight;

        public HistoricalRecord(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public HistoricalRecord(ComponentName componentName, long j, float f) {
            this.activity = componentName;
            this.time = j;
            this.weight = f;
        }

        public int hashCode() {
            return (31 * ((31 * ((31 * 1) + (this.activity == null ? 0 : this.activity.hashCode()))) + ((int) (this.time ^ (this.time >>> 32))))) + Float.floatToIntBits(this.weight);
        }

        /* JADX WARN: Type inference failed for: r8v0, types: [java.lang.Object] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r8) {
            /*
                r7 = this;
                r0 = r7
                r1 = r8
                r3 = r0
                r4 = r1
                if (r3 != r4) goto L_0x0009
                r3 = 1
                r0 = r3
            L_0x0008:
                return r0
            L_0x0009:
                r3 = r1
                if (r3 != 0) goto L_0x000f
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x000f:
                r3 = r0
                java.lang.Class r3 = r3.getClass()
                r4 = r1
                java.lang.Class r4 = r4.getClass()
                if (r3 == r4) goto L_0x001e
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x001e:
                r3 = r1
                android.support.v7.widget.ActivityChooserModel$HistoricalRecord r3 = (android.support.v7.widget.ActivityChooserModel.HistoricalRecord) r3
                r2 = r3
                r3 = r0
                android.content.ComponentName r3 = r3.activity
                if (r3 != 0) goto L_0x002f
                r3 = r2
                android.content.ComponentName r3 = r3.activity
                if (r3 == 0) goto L_0x003e
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x002f:
                r3 = r0
                android.content.ComponentName r3 = r3.activity
                r4 = r2
                android.content.ComponentName r4 = r4.activity
                boolean r3 = r3.equals(r4)
                if (r3 != 0) goto L_0x003e
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x003e:
                r3 = r0
                long r3 = r3.time
                r5 = r2
                long r5 = r5.time
                int r3 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r3 == 0) goto L_0x004b
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x004b:
                r3 = r0
                float r3 = r3.weight
                int r3 = java.lang.Float.floatToIntBits(r3)
                r4 = r2
                float r4 = r4.weight
                int r4 = java.lang.Float.floatToIntBits(r4)
                if (r3 == r4) goto L_0x005e
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x005e:
                r3 = 1
                r0 = r3
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActivityChooserModel.HistoricalRecord.equals(java.lang.Object):boolean");
        }

        public String toString() {
            StringBuilder sb;
            Object obj;
            new StringBuilder();
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("[");
            StringBuilder append2 = sb2.append("; activity:").append(this.activity);
            StringBuilder append3 = sb2.append("; time:").append(this.time);
            new BigDecimal((double) this.weight);
            StringBuilder append4 = sb2.append("; weight:").append(obj);
            StringBuilder append5 = sb2.append("]");
            return sb2.toString();
        }
    }

    public final class ActivityResolveInfo implements Comparable<ActivityResolveInfo> {
        public final ResolveInfo resolveInfo;
        public float weight;

        public ActivityResolveInfo(ResolveInfo resolveInfo2) {
            this.resolveInfo = resolveInfo2;
        }

        public int hashCode() {
            return 31 + Float.floatToIntBits(this.weight);
        }

        /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.Object] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r6) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r3 = r0
                r4 = r1
                if (r3 != r4) goto L_0x0009
                r3 = 1
                r0 = r3
            L_0x0008:
                return r0
            L_0x0009:
                r3 = r1
                if (r3 != 0) goto L_0x000f
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x000f:
                r3 = r0
                java.lang.Class r3 = r3.getClass()
                r4 = r1
                java.lang.Class r4 = r4.getClass()
                if (r3 == r4) goto L_0x001e
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x001e:
                r3 = r1
                android.support.v7.widget.ActivityChooserModel$ActivityResolveInfo r3 = (android.support.v7.widget.ActivityChooserModel.ActivityResolveInfo) r3
                r2 = r3
                r3 = r0
                float r3 = r3.weight
                int r3 = java.lang.Float.floatToIntBits(r3)
                r4 = r2
                float r4 = r4.weight
                int r4 = java.lang.Float.floatToIntBits(r4)
                if (r3 == r4) goto L_0x0035
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x0035:
                r3 = 1
                r0 = r3
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActivityChooserModel.ActivityResolveInfo.equals(java.lang.Object):boolean");
        }

        public int compareTo(ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }

        public String toString() {
            StringBuilder sb;
            Object obj;
            new StringBuilder();
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("[");
            StringBuilder append2 = sb2.append("resolveInfo:").append(this.resolveInfo.toString());
            new BigDecimal((double) this.weight);
            StringBuilder append3 = sb2.append("; weight:").append(obj);
            StringBuilder append4 = sb2.append("]");
            return sb2.toString();
        }
    }

    private final class DefaultSorter implements ActivitySorter {
        private static final float WEIGHT_DECAY_COEFFICIENT = 0.95f;
        private final Map<ComponentName, ActivityResolveInfo> mPackageNameToActivityMap;

        private DefaultSorter() {
            Map<ComponentName, ActivityResolveInfo> map;
            new HashMap();
            this.mPackageNameToActivityMap = map;
        }

        public void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2) {
            Object obj;
            List<ActivityResolveInfo> list3 = list;
            List<HistoricalRecord> list4 = list2;
            Map<ComponentName, ActivityResolveInfo> map = this.mPackageNameToActivityMap;
            map.clear();
            int size = list3.size();
            for (int i = 0; i < size; i++) {
                ActivityResolveInfo activityResolveInfo = list3.get(i);
                activityResolveInfo.weight = 0.0f;
                new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
                ActivityResolveInfo put = map.put(obj, activityResolveInfo);
            }
            float f = 1.0f;
            for (int size2 = list4.size() - 1; size2 >= 0; size2--) {
                HistoricalRecord historicalRecord = list4.get(size2);
                ActivityResolveInfo activityResolveInfo2 = map.get(historicalRecord.activity);
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight += historicalRecord.weight * f;
                    f *= WEIGHT_DECAY_COEFFICIENT;
                }
            }
            Collections.sort(list3);
        }
    }

    private void readHistoricalDataImpl() {
        StringBuilder sb;
        StringBuilder sb2;
        Throwable th;
        Object obj;
        Throwable th2;
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = this.mContext.openFileInput(this.mHistoryFileName);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(fileInputStream, "UTF-8");
                int i = 0;
                while (i != 1 && i != 2) {
                    i = newPullParser.next();
                }
                if (!TAG_HISTORICAL_RECORDS.equals(newPullParser.getName())) {
                    Throwable th3 = th2;
                    new XmlPullParserException("Share records file does not start with historical-records tag.");
                    throw th3;
                }
                List<HistoricalRecord> list = this.mHistoricalRecords;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next == 1) {
                        if (fileInputStream != null) {
                            try {
                                fileInputStream.close();
                                return;
                            } catch (IOException e) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!(next == 3 || next == 4)) {
                        if (!TAG_HISTORICAL_RECORD.equals(newPullParser.getName())) {
                            Throwable th4 = th;
                            new XmlPullParserException("Share records file not well-formed.");
                            throw th4;
                        }
                        new HistoricalRecord(newPullParser.getAttributeValue(null, ATTRIBUTE_ACTIVITY), Long.parseLong(newPullParser.getAttributeValue(null, ATTRIBUTE_TIME)), Float.parseFloat(newPullParser.getAttributeValue(null, ATTRIBUTE_WEIGHT)));
                        boolean add = list.add(obj);
                    }
                }
            } catch (XmlPullParserException e2) {
                XmlPullParserException xmlPullParserException = e2;
                String str = LOG_TAG;
                new StringBuilder();
                int e3 = Log.e(str, sb2.append("Error reading historical recrod file: ").append(this.mHistoryFileName).toString(), xmlPullParserException);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (IOException e5) {
                IOException iOException = e5;
                String str2 = LOG_TAG;
                new StringBuilder();
                int e6 = Log.e(str2, sb.append("Error reading historical recrod file: ").append(this.mHistoryFileName).toString(), iOException);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e7) {
                    }
                }
            } catch (Throwable th5) {
                Throwable th6 = th5;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (IOException e8) {
                    }
                }
                throw th6;
            }
        } catch (FileNotFoundException e9) {
            FileNotFoundException fileNotFoundException = e9;
        }
    }

    private final class PersistHistoryAsyncTask extends AsyncTask<Object, Void, Void> {
        private PersistHistoryAsyncTask() {
        }

        public Void doInBackground(Object... objArr) {
            StringBuilder sb;
            StringBuilder sb2;
            StringBuilder sb3;
            StringBuilder sb4;
            Object[] objArr2 = objArr;
            List list = (List) objArr2[0];
            String str = (String) objArr2[1];
            try {
                FileOutputStream openFileOutput = ActivityChooserModel.this.mContext.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", true);
                    XmlSerializer startTag = newSerializer.startTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORDS);
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        HistoricalRecord historicalRecord = (HistoricalRecord) list.remove(0);
                        XmlSerializer startTag2 = newSerializer.startTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORD);
                        XmlSerializer attribute = newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_ACTIVITY, historicalRecord.activity.flattenToString());
                        XmlSerializer attribute2 = newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_TIME, String.valueOf(historicalRecord.time));
                        XmlSerializer attribute3 = newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_WEIGHT, String.valueOf(historicalRecord.weight));
                        XmlSerializer endTag = newSerializer.endTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORD);
                    }
                    XmlSerializer endTag2 = newSerializer.endTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORDS);
                    newSerializer.endDocument();
                    boolean access$502 = ActivityChooserModel.access$502(ActivityChooserModel.this, true);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IllegalArgumentException e2) {
                    IllegalArgumentException illegalArgumentException = e2;
                    String access$300 = ActivityChooserModel.LOG_TAG;
                    new StringBuilder();
                    int e3 = Log.e(access$300, sb4.append("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName).toString(), illegalArgumentException);
                    boolean access$5022 = ActivityChooserModel.access$502(ActivityChooserModel.this, true);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e4) {
                        }
                    }
                } catch (IllegalStateException e5) {
                    IllegalStateException illegalStateException = e5;
                    String access$3002 = ActivityChooserModel.LOG_TAG;
                    new StringBuilder();
                    int e6 = Log.e(access$3002, sb3.append("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName).toString(), illegalStateException);
                    boolean access$5023 = ActivityChooserModel.access$502(ActivityChooserModel.this, true);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e7) {
                        }
                    }
                } catch (IOException e8) {
                    IOException iOException = e8;
                    String access$3003 = ActivityChooserModel.LOG_TAG;
                    new StringBuilder();
                    int e9 = Log.e(access$3003, sb2.append("Error writing historical recrod file: ").append(ActivityChooserModel.this.mHistoryFileName).toString(), iOException);
                    boolean access$5024 = ActivityChooserModel.access$502(ActivityChooserModel.this, true);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e10) {
                        }
                    }
                } catch (Throwable th) {
                    Throwable th2 = th;
                    boolean access$5025 = ActivityChooserModel.access$502(ActivityChooserModel.this, true);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e11) {
                        }
                    }
                    throw th2;
                }
                return null;
            } catch (FileNotFoundException e12) {
                FileNotFoundException fileNotFoundException = e12;
                String access$3004 = ActivityChooserModel.LOG_TAG;
                new StringBuilder();
                int e13 = Log.e(access$3004, sb.append("Error writing historical recrod file: ").append(str).toString(), fileNotFoundException);
                return null;
            }
        }
    }
}
