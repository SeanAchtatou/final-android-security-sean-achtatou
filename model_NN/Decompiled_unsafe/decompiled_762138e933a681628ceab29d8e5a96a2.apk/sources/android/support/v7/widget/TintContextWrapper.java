package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

class TintContextWrapper extends ContextWrapper {
    private Resources mResources;

    public static Context wrap(Context context) {
        Context context2;
        Context context3 = context;
        if (!(context3 instanceof TintContextWrapper)) {
            new TintContextWrapper(context3);
            context3 = context2;
        }
        return context3;
    }

    private TintContextWrapper(Context context) {
        super(context);
    }

    public Resources getResources() {
        Resources resources;
        if (this.mResources == null) {
            new TintResources(super.getResources(), TintManager.get(this));
            this.mResources = resources;
        }
        return this.mResources;
    }

    static class TintResources extends ResourcesWrapper {
        private final TintManager mTintManager;

        public TintResources(Resources resources, TintManager tintManager) {
            super(resources);
            this.mTintManager = tintManager;
        }

        public Drawable getDrawable(int i) throws Resources.NotFoundException {
            int i2 = i;
            Drawable drawable = super.getDrawable(i2);
            if (drawable != null) {
                boolean tintDrawableUsingColorFilter = this.mTintManager.tintDrawableUsingColorFilter(i2, drawable);
            }
            return drawable;
        }
    }
}
