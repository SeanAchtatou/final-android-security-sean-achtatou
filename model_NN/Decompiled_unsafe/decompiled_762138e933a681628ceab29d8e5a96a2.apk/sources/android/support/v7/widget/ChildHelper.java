package android.support.v7.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.List;

class ChildHelper {
    private static final boolean DEBUG = false;
    private static final String TAG = "ChildrenHelper";
    final Bucket mBucket;
    final Callback mCallback;
    final List<View> mHiddenViews;

    interface Callback {
        void addView(View view, int i);

        void attachViewToParent(View view, int i, ViewGroup.LayoutParams layoutParams);

        void detachViewFromParent(int i);

        View getChildAt(int i);

        int getChildCount();

        RecyclerView.ViewHolder getChildViewHolder(View view);

        int indexOfChild(View view);

        void onEnteredHiddenState(View view);

        void onLeftHiddenState(View view);

        void removeAllViews();

        void removeViewAt(int i);
    }

    ChildHelper(Callback callback) {
        Bucket bucket;
        List<View> list;
        this.mCallback = callback;
        new Bucket();
        this.mBucket = bucket;
        new ArrayList();
        this.mHiddenViews = list;
    }

    private void hideViewInternal(View view) {
        View view2 = view;
        boolean add = this.mHiddenViews.add(view2);
        this.mCallback.onEnteredHiddenState(view2);
    }

    private boolean unhideViewInternal(View view) {
        View view2 = view;
        if (!this.mHiddenViews.remove(view2)) {
            return false;
        }
        this.mCallback.onLeftHiddenState(view2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void addView(View view, boolean z) {
        addView(view, -1, z);
    }

    /* access modifiers changed from: package-private */
    public void addView(View view, int i, boolean z) {
        int offset;
        View view2 = view;
        int i2 = i;
        boolean z2 = z;
        if (i2 < 0) {
            offset = this.mCallback.getChildCount();
        } else {
            offset = getOffset(i2);
        }
        this.mBucket.insert(offset, z2);
        if (z2) {
            hideViewInternal(view2);
        }
        this.mCallback.addView(view2, offset);
    }

    private int getOffset(int i) {
        int i2 = i;
        if (i2 < 0) {
            return -1;
        }
        int childCount = this.mCallback.getChildCount();
        int i3 = i2;
        while (true) {
            int i4 = i3;
            if (i4 >= childCount) {
                return -1;
            }
            int countOnesBefore = i2 - (i4 - this.mBucket.countOnesBefore(i4));
            if (countOnesBefore == 0) {
                while (this.mBucket.get(i4)) {
                    i4++;
                }
                return i4;
            }
            i3 = i4 + countOnesBefore;
        }
    }

    /* access modifiers changed from: package-private */
    public void removeView(View view) {
        View view2 = view;
        int indexOfChild = this.mCallback.indexOfChild(view2);
        if (indexOfChild >= 0) {
            if (this.mBucket.remove(indexOfChild)) {
                boolean unhideViewInternal = unhideViewInternal(view2);
            }
            this.mCallback.removeViewAt(indexOfChild);
        }
    }

    /* access modifiers changed from: package-private */
    public void removeViewAt(int i) {
        int offset = getOffset(i);
        View childAt = this.mCallback.getChildAt(offset);
        if (childAt != null) {
            if (this.mBucket.remove(offset)) {
                boolean unhideViewInternal = unhideViewInternal(childAt);
            }
            this.mCallback.removeViewAt(offset);
        }
    }

    /* access modifiers changed from: package-private */
    public View getChildAt(int i) {
        return this.mCallback.getChildAt(getOffset(i));
    }

    /* access modifiers changed from: package-private */
    public void removeAllViewsUnfiltered() {
        this.mBucket.reset();
        for (int size = this.mHiddenViews.size() - 1; size >= 0; size--) {
            this.mCallback.onLeftHiddenState(this.mHiddenViews.get(size));
            View remove = this.mHiddenViews.remove(size);
        }
        this.mCallback.removeAllViews();
    }

    /* access modifiers changed from: package-private */
    public View findHiddenNonRemovedView(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int size = this.mHiddenViews.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view = this.mHiddenViews.get(i5);
            RecyclerView.ViewHolder childViewHolder = this.mCallback.getChildViewHolder(view);
            if (childViewHolder.getLayoutPosition() == i3 && !childViewHolder.isInvalid() && !childViewHolder.isRemoved() && (i4 == -1 || childViewHolder.getItemViewType() == i4)) {
                return view;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void attachViewToParent(View view, int i, ViewGroup.LayoutParams layoutParams, boolean z) {
        int offset;
        View view2 = view;
        int i2 = i;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        boolean z2 = z;
        if (i2 < 0) {
            offset = this.mCallback.getChildCount();
        } else {
            offset = getOffset(i2);
        }
        this.mBucket.insert(offset, z2);
        if (z2) {
            hideViewInternal(view2);
        }
        this.mCallback.attachViewToParent(view2, offset, layoutParams2);
    }

    /* access modifiers changed from: package-private */
    public int getChildCount() {
        return this.mCallback.getChildCount() - this.mHiddenViews.size();
    }

    /* access modifiers changed from: package-private */
    public int getUnfilteredChildCount() {
        return this.mCallback.getChildCount();
    }

    /* access modifiers changed from: package-private */
    public View getUnfilteredChildAt(int i) {
        return this.mCallback.getChildAt(i);
    }

    /* access modifiers changed from: package-private */
    public void detachViewFromParent(int i) {
        int offset = getOffset(i);
        boolean remove = this.mBucket.remove(offset);
        this.mCallback.detachViewFromParent(offset);
    }

    /* access modifiers changed from: package-private */
    public int indexOfChild(View view) {
        int indexOfChild = this.mCallback.indexOfChild(view);
        if (indexOfChild == -1) {
            return -1;
        }
        if (this.mBucket.get(indexOfChild)) {
            return -1;
        }
        return indexOfChild - this.mBucket.countOnesBefore(indexOfChild);
    }

    /* access modifiers changed from: package-private */
    public boolean isHidden(View view) {
        return this.mHiddenViews.contains(view);
    }

    /* access modifiers changed from: package-private */
    public void hide(View view) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        int indexOfChild = this.mCallback.indexOfChild(view2);
        if (indexOfChild < 0) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("view is not a child, cannot hide ").append(view2).toString());
            throw th2;
        }
        this.mBucket.set(indexOfChild);
        hideViewInternal(view2);
    }

    /* access modifiers changed from: package-private */
    public void unhide(View view) {
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        View view2 = view;
        int indexOfChild = this.mCallback.indexOfChild(view2);
        if (indexOfChild < 0) {
            Throwable th3 = th2;
            new StringBuilder();
            new IllegalArgumentException(sb2.append("view is not a child, cannot hide ").append(view2).toString());
            throw th3;
        } else if (!this.mBucket.get(indexOfChild)) {
            Throwable th4 = th;
            new StringBuilder();
            new RuntimeException(sb.append("trying to unhide a view that was not hidden").append(view2).toString());
            throw th4;
        } else {
            this.mBucket.clear(indexOfChild);
            boolean unhideViewInternal = unhideViewInternal(view2);
        }
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder();
        return sb.append(this.mBucket.toString()).append(", hidden list:").append(this.mHiddenViews.size()).toString();
    }

    /* access modifiers changed from: package-private */
    public boolean removeViewIfHidden(View view) {
        View view2 = view;
        int indexOfChild = this.mCallback.indexOfChild(view2);
        if (indexOfChild == -1) {
            if (unhideViewInternal(view2)) {
            }
            return true;
        } else if (!this.mBucket.get(indexOfChild)) {
            return false;
        } else {
            boolean remove = this.mBucket.remove(indexOfChild);
            if (!unhideViewInternal(view2)) {
            }
            this.mCallback.removeViewAt(indexOfChild);
            return true;
        }
    }

    static class Bucket {
        static final int BITS_PER_WORD = 64;
        static final long LAST_BIT = Long.MIN_VALUE;
        long mData = 0;
        Bucket next;

        Bucket() {
        }

        /* access modifiers changed from: package-private */
        public void set(int i) {
            int i2 = i;
            if (i2 >= 64) {
                ensureNext();
                this.next.set(i2 - 64);
                return;
            }
            this.mData = this.mData | (1 << i2);
        }

        private void ensureNext() {
            Bucket bucket;
            if (this.next == null) {
                new Bucket();
                this.next = bucket;
            }
        }

        /* access modifiers changed from: package-private */
        public void clear(int i) {
            int i2 = i;
            if (i2 < 64) {
                this.mData = this.mData & ((1 << i2) ^ -1);
            } else if (this.next != null) {
                this.next.clear(i2 - 64);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean get(int i) {
            int i2 = i;
            if (i2 >= 64) {
                ensureNext();
                return this.next.get(i2 - 64);
            }
            return (this.mData & (1 << i2)) != 0;
        }

        /* access modifiers changed from: package-private */
        public void reset() {
            this.mData = 0;
            if (this.next != null) {
                this.next.reset();
            }
        }

        /* access modifiers changed from: package-private */
        public void insert(int i, boolean z) {
            int i2 = i;
            boolean z2 = z;
            if (i2 >= 64) {
                ensureNext();
                this.next.insert(i2 - 64, z2);
                return;
            }
            boolean z3 = (this.mData & LAST_BIT) != 0;
            long j = (1 << i2) - 1;
            this.mData = (this.mData & j) | ((this.mData & (j ^ -1)) << 1);
            if (z2) {
                set(i2);
            } else {
                clear(i2);
            }
            if (z3 || this.next != null) {
                ensureNext();
                this.next.insert(0, z3);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean remove(int i) {
            int i2 = i;
            if (i2 >= 64) {
                ensureNext();
                return this.next.remove(i2 - 64);
            }
            long j = 1 << i2;
            boolean z = (this.mData & j) != 0;
            this.mData = this.mData & (j ^ -1);
            long j2 = j - 1;
            this.mData = (this.mData & j2) | Long.rotateRight(this.mData & (j2 ^ -1), 1);
            if (this.next != null) {
                if (this.next.get(0)) {
                    set(63);
                }
                boolean remove = this.next.remove(0);
            }
            return z;
        }

        /* access modifiers changed from: package-private */
        public int countOnesBefore(int i) {
            int i2 = i;
            if (this.next == null) {
                if (i2 >= 64) {
                    return Long.bitCount(this.mData);
                }
                return Long.bitCount(this.mData & ((1 << i2) - 1));
            } else if (i2 < 64) {
                return Long.bitCount(this.mData & ((1 << i2) - 1));
            } else {
                return this.next.countOnesBefore(i2 - 64) + Long.bitCount(this.mData);
            }
        }

        public String toString() {
            StringBuilder sb;
            String sb2;
            if (this.next == null) {
                sb2 = Long.toBinaryString(this.mData);
            } else {
                new StringBuilder();
                sb2 = sb.append(this.next.toString()).append("xx").append(Long.toBinaryString(this.mData)).toString();
            }
            return sb2;
        }
    }
}
