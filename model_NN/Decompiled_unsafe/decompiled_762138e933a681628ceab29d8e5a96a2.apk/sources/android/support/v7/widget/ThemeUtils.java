package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.graphics.ColorUtils;
import android.util.AttributeSet;
import android.util.TypedValue;

class ThemeUtils {
    static final int[] ACTIVATED_STATE_SET = {16843518};
    static final int[] CHECKED_STATE_SET = {16842912};
    static final int[] DISABLED_STATE_SET = {-16842910};
    static final int[] EMPTY_STATE_SET = new int[0];
    static final int[] FOCUSED_STATE_SET = {16842908};
    static final int[] NOT_PRESSED_OR_FOCUSED_STATE_SET = {-16842919, -16842908};
    static final int[] PRESSED_STATE_SET = {16842919};
    static final int[] SELECTED_STATE_SET = {16842913};
    private static final int[] TEMP_ARRAY = new int[1];
    private static final ThreadLocal<TypedValue> TL_TYPED_VALUE;

    ThemeUtils() {
    }

    static {
        ThreadLocal<TypedValue> threadLocal;
        new ThreadLocal<>();
        TL_TYPED_VALUE = threadLocal;
    }

    public static ColorStateList createDisabledStateList(int i, int i2) {
        ColorStateList colorStateList;
        int[][] iArr = new int[2][];
        int[] iArr2 = new int[2];
        iArr[0] = DISABLED_STATE_SET;
        iArr2[0] = i2;
        int i3 = 0 + 1;
        iArr[i3] = EMPTY_STATE_SET;
        iArr2[i3] = i;
        int i4 = i3 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    /* JADX INFO: finally extract failed */
    public static int getThemeAttrColor(Context context, int i) {
        TEMP_ARRAY[0] = i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, TEMP_ARRAY);
        try {
            int color = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
            return color;
        } catch (Throwable th) {
            Throwable th2 = th;
            obtainStyledAttributes.recycle();
            throw th2;
        }
    }

    /* JADX INFO: finally extract failed */
    public static ColorStateList getThemeAttrColorStateList(Context context, int i) {
        TEMP_ARRAY[0] = i;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes((AttributeSet) null, TEMP_ARRAY);
        try {
            ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(0);
            obtainStyledAttributes.recycle();
            return colorStateList;
        } catch (Throwable th) {
            Throwable th2 = th;
            obtainStyledAttributes.recycle();
            throw th2;
        }
    }

    public static int getDisabledThemeAttrColor(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        ColorStateList themeAttrColorStateList = getThemeAttrColorStateList(context2, i2);
        if (themeAttrColorStateList != null && themeAttrColorStateList.isStateful()) {
            return themeAttrColorStateList.getColorForState(DISABLED_STATE_SET, themeAttrColorStateList.getDefaultColor());
        }
        TypedValue typedValue = getTypedValue();
        boolean resolveAttribute = context2.getTheme().resolveAttribute(16842803, typedValue, true);
        return getThemeAttrColor(context2, i2, typedValue.getFloat());
    }

    private static TypedValue getTypedValue() {
        TypedValue typedValue;
        TypedValue typedValue2 = TL_TYPED_VALUE.get();
        if (typedValue2 == null) {
            new TypedValue();
            typedValue2 = typedValue;
            TL_TYPED_VALUE.set(typedValue2);
        }
        return typedValue2;
    }

    static int getThemeAttrColor(Context context, int i, float f) {
        int themeAttrColor = getThemeAttrColor(context, i);
        return ColorUtils.setAlphaComponent(themeAttrColor, Math.round(((float) Color.alpha(themeAttrColor)) * f));
    }
}
