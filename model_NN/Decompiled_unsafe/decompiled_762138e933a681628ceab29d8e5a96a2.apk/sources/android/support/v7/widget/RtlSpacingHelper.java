package android.support.v7.widget;

class RtlSpacingHelper {
    public static final int UNDEFINED = Integer.MIN_VALUE;
    private int mEnd = Integer.MIN_VALUE;
    private int mExplicitLeft = 0;
    private int mExplicitRight = 0;
    private boolean mIsRelative = false;
    private boolean mIsRtl = false;
    private int mLeft = 0;
    private int mRight = 0;
    private int mStart = Integer.MIN_VALUE;

    RtlSpacingHelper() {
    }

    public int getLeft() {
        return this.mLeft;
    }

    public int getRight() {
        return this.mRight;
    }

    public int getStart() {
        return this.mIsRtl ? this.mRight : this.mLeft;
    }

    public int getEnd() {
        return this.mIsRtl ? this.mLeft : this.mRight;
    }

    public void setRelative(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        this.mStart = i3;
        this.mEnd = i4;
        this.mIsRelative = true;
        if (this.mIsRtl) {
            if (i4 != Integer.MIN_VALUE) {
                this.mLeft = i4;
            }
            if (i3 != Integer.MIN_VALUE) {
                this.mRight = i3;
                return;
            }
            return;
        }
        if (i3 != Integer.MIN_VALUE) {
            this.mLeft = i3;
        }
        if (i4 != Integer.MIN_VALUE) {
            this.mRight = i4;
        }
    }

    public void setAbsolute(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        this.mIsRelative = false;
        if (i3 != Integer.MIN_VALUE) {
            int i5 = i3;
            this.mExplicitLeft = i5;
            this.mLeft = i5;
        }
        if (i4 != Integer.MIN_VALUE) {
            int i6 = i4;
            this.mExplicitRight = i6;
            this.mRight = i6;
        }
    }

    public void setDirection(boolean z) {
        boolean z2 = z;
        if (z2 != this.mIsRtl) {
            this.mIsRtl = z2;
            if (!this.mIsRelative) {
                this.mLeft = this.mExplicitLeft;
                this.mRight = this.mExplicitRight;
            } else if (z2) {
                this.mLeft = this.mEnd != Integer.MIN_VALUE ? this.mEnd : this.mExplicitLeft;
                this.mRight = this.mStart != Integer.MIN_VALUE ? this.mStart : this.mExplicitRight;
            } else {
                this.mLeft = this.mStart != Integer.MIN_VALUE ? this.mStart : this.mExplicitLeft;
                this.mRight = this.mEnd != Integer.MIN_VALUE ? this.mEnd : this.mExplicitRight;
            }
        }
    }
}
