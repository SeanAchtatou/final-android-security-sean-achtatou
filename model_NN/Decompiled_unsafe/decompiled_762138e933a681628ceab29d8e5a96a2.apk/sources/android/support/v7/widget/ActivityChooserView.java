package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.widget.ActivityChooserModel;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

public class ActivityChooserView extends ViewGroup implements ActivityChooserModel.ActivityChooserModelClient {
    private static final String LOG_TAG = "ActivityChooserView";
    private final LinearLayoutCompat mActivityChooserContent;
    private final Drawable mActivityChooserContentBackground;
    /* access modifiers changed from: private */
    public final ActivityChooserViewAdapter mAdapter;
    private final Callbacks mCallbacks;
    private int mDefaultActionButtonContentDescription;
    /* access modifiers changed from: private */
    public final FrameLayout mDefaultActivityButton;
    private final ImageView mDefaultActivityButtonImage;
    /* access modifiers changed from: private */
    public final FrameLayout mExpandActivityOverflowButton;
    private final ImageView mExpandActivityOverflowButtonImage;
    /* access modifiers changed from: private */
    public int mInitialActivityCount;
    private boolean mIsAttachedToWindow;
    /* access modifiers changed from: private */
    public boolean mIsSelectingDefaultActivity;
    private final int mListPopupMaxWidth;
    private ListPopupWindow mListPopupWindow;
    /* access modifiers changed from: private */
    public final DataSetObserver mModelDataSetOberver;
    /* access modifiers changed from: private */
    public PopupWindow.OnDismissListener mOnDismissListener;
    private final ViewTreeObserver.OnGlobalLayoutListener mOnGlobalLayoutListener;
    ActionProvider mProvider;

    static /* synthetic */ boolean access$602(ActivityChooserView activityChooserView, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        activityChooserView.mIsSelectingDefaultActivity = z3;
        return z2;
    }

    public ActivityChooserView(Context context) {
        this(context, null);
    }

    public ActivityChooserView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.ActivityChooserView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActivityChooserView(android.content.Context r16, android.util.AttributeSet r17, int r18) {
        /*
            r15 = this;
            r0 = r15
            r1 = r16
            r2 = r17
            r3 = r18
            r9 = r0
            r10 = r1
            r11 = r2
            r12 = r3
            r9.<init>(r10, r11, r12)
            r9 = r0
            android.support.v7.widget.ActivityChooserView$1 r10 = new android.support.v7.widget.ActivityChooserView$1
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r11.<init>()
            r9.mModelDataSetOberver = r10
            r9 = r0
            android.support.v7.widget.ActivityChooserView$2 r10 = new android.support.v7.widget.ActivityChooserView$2
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r11.<init>()
            r9.mOnGlobalLayoutListener = r10
            r9 = r0
            r10 = 4
            r9.mInitialActivityCount = r10
            r9 = r1
            r10 = r2
            int[] r11 = android.support.v7.appcompat.R.styleable.ActivityChooserView
            r12 = r3
            r13 = 0
            android.content.res.TypedArray r9 = r9.obtainStyledAttributes(r10, r11, r12, r13)
            r4 = r9
            r9 = r0
            r10 = r4
            int r11 = android.support.v7.appcompat.R.styleable.ActivityChooserView_initialActivityCount
            r12 = 4
            int r10 = r10.getInt(r11, r12)
            r9.mInitialActivityCount = r10
            r9 = r4
            int r10 = android.support.v7.appcompat.R.styleable.ActivityChooserView_expandActivityOverflowButtonDrawable
            android.graphics.drawable.Drawable r9 = r9.getDrawable(r10)
            r5 = r9
            r9 = r4
            r9.recycle()
            r9 = r0
            android.content.Context r9 = r9.getContext()
            android.view.LayoutInflater r9 = android.view.LayoutInflater.from(r9)
            r6 = r9
            r9 = r6
            int r10 = android.support.v7.appcompat.R.layout.abc_activity_chooser_view
            r11 = r0
            r12 = 1
            android.view.View r9 = r9.inflate(r10, r11, r12)
            r9 = r0
            android.support.v7.widget.ActivityChooserView$Callbacks r10 = new android.support.v7.widget.ActivityChooserView$Callbacks
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r13 = 0
            r11.<init>()
            r9.mCallbacks = r10
            r9 = r0
            r10 = r0
            int r11 = android.support.v7.appcompat.R.id.activity_chooser_view_content
            android.view.View r10 = r10.findViewById(r11)
            android.support.v7.widget.LinearLayoutCompat r10 = (android.support.v7.widget.LinearLayoutCompat) r10
            r9.mActivityChooserContent = r10
            r9 = r0
            r10 = r0
            android.support.v7.widget.LinearLayoutCompat r10 = r10.mActivityChooserContent
            android.graphics.drawable.Drawable r10 = r10.getBackground()
            r9.mActivityChooserContentBackground = r10
            r9 = r0
            r10 = r0
            int r11 = android.support.v7.appcompat.R.id.default_activity_button
            android.view.View r10 = r10.findViewById(r11)
            android.widget.FrameLayout r10 = (android.widget.FrameLayout) r10
            r9.mDefaultActivityButton = r10
            r9 = r0
            android.widget.FrameLayout r9 = r9.mDefaultActivityButton
            r10 = r0
            android.support.v7.widget.ActivityChooserView$Callbacks r10 = r10.mCallbacks
            r9.setOnClickListener(r10)
            r9 = r0
            android.widget.FrameLayout r9 = r9.mDefaultActivityButton
            r10 = r0
            android.support.v7.widget.ActivityChooserView$Callbacks r10 = r10.mCallbacks
            r9.setOnLongClickListener(r10)
            r9 = r0
            r10 = r0
            android.widget.FrameLayout r10 = r10.mDefaultActivityButton
            int r11 = android.support.v7.appcompat.R.id.image
            android.view.View r10 = r10.findViewById(r11)
            android.widget.ImageView r10 = (android.widget.ImageView) r10
            r9.mDefaultActivityButtonImage = r10
            r9 = r0
            int r10 = android.support.v7.appcompat.R.id.expand_activities_button
            android.view.View r9 = r9.findViewById(r10)
            android.widget.FrameLayout r9 = (android.widget.FrameLayout) r9
            r7 = r9
            r9 = r7
            r10 = r0
            android.support.v7.widget.ActivityChooserView$Callbacks r10 = r10.mCallbacks
            r9.setOnClickListener(r10)
            r9 = r7
            android.support.v7.widget.ActivityChooserView$3 r10 = new android.support.v7.widget.ActivityChooserView$3
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r13 = r7
            r11.<init>(r13)
            r9.setOnTouchListener(r10)
            r9 = r0
            r10 = r7
            r9.mExpandActivityOverflowButton = r10
            r9 = r0
            r10 = r7
            int r11 = android.support.v7.appcompat.R.id.image
            android.view.View r10 = r10.findViewById(r11)
            android.widget.ImageView r10 = (android.widget.ImageView) r10
            r9.mExpandActivityOverflowButtonImage = r10
            r9 = r0
            android.widget.ImageView r9 = r9.mExpandActivityOverflowButtonImage
            r10 = r5
            r9.setImageDrawable(r10)
            r9 = r0
            android.support.v7.widget.ActivityChooserView$ActivityChooserViewAdapter r10 = new android.support.v7.widget.ActivityChooserView$ActivityChooserViewAdapter
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r13 = 0
            r11.<init>()
            r9.mAdapter = r10
            r9 = r0
            android.support.v7.widget.ActivityChooserView$ActivityChooserViewAdapter r9 = r9.mAdapter
            android.support.v7.widget.ActivityChooserView$4 r10 = new android.support.v7.widget.ActivityChooserView$4
            r14 = r10
            r10 = r14
            r11 = r14
            r12 = r0
            r11.<init>()
            r9.registerDataSetObserver(r10)
            r9 = r1
            android.content.res.Resources r9 = r9.getResources()
            r8 = r9
            r9 = r0
            r10 = r8
            android.util.DisplayMetrics r10 = r10.getDisplayMetrics()
            int r10 = r10.widthPixels
            r11 = 2
            int r10 = r10 / 2
            r11 = r8
            int r12 = android.support.v7.appcompat.R.dimen.abc_config_prefDialogWidth
            int r11 = r11.getDimensionPixelSize(r12)
            int r10 = java.lang.Math.max(r10, r11)
            r9.mListPopupMaxWidth = r10
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActivityChooserView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setActivityChooserModel(ActivityChooserModel activityChooserModel) {
        this.mAdapter.setDataModel(activityChooserModel);
        if (isShowingPopup()) {
            boolean dismissPopup = dismissPopup();
            boolean showPopup = showPopup();
        }
    }

    public void setExpandActivityOverflowButtonDrawable(Drawable drawable) {
        this.mExpandActivityOverflowButtonImage.setImageDrawable(drawable);
    }

    public void setExpandActivityOverflowButtonContentDescription(int i) {
        this.mExpandActivityOverflowButtonImage.setContentDescription(getContext().getString(i));
    }

    public void setProvider(ActionProvider actionProvider) {
        this.mProvider = actionProvider;
    }

    public boolean showPopup() {
        if (isShowingPopup() || !this.mIsAttachedToWindow) {
            return false;
        }
        this.mIsSelectingDefaultActivity = false;
        showPopupUnchecked(this.mInitialActivityCount);
        return true;
    }

    /* access modifiers changed from: private */
    public void showPopupUnchecked(int i) {
        Throwable th;
        int i2 = i;
        if (this.mAdapter.getDataModel() == null) {
            Throwable th2 = th;
            new IllegalStateException("No data model. Did you call #setDataModel?");
            throw th2;
        }
        getViewTreeObserver().addOnGlobalLayoutListener(this.mOnGlobalLayoutListener);
        boolean z = this.mDefaultActivityButton.getVisibility() == 0;
        int activityCount = this.mAdapter.getActivityCount();
        int i3 = z ? 1 : 0;
        if (i2 == Integer.MAX_VALUE || activityCount <= i2 + i3) {
            this.mAdapter.setShowFooterView(false);
            this.mAdapter.setMaxActivityCount(i2);
        } else {
            this.mAdapter.setShowFooterView(true);
            this.mAdapter.setMaxActivityCount(i2 - 1);
        }
        ListPopupWindow listPopupWindow = getListPopupWindow();
        if (!listPopupWindow.isShowing()) {
            if (this.mIsSelectingDefaultActivity || !z) {
                this.mAdapter.setShowDefaultActivity(true, z);
            } else {
                this.mAdapter.setShowDefaultActivity(false, false);
            }
            listPopupWindow.setContentWidth(Math.min(this.mAdapter.measureContentWidth(), this.mListPopupMaxWidth));
            listPopupWindow.show();
            if (this.mProvider != null) {
                this.mProvider.subUiVisibilityChanged(true);
            }
            listPopupWindow.getListView().setContentDescription(getContext().getString(R.string.abc_activitychooserview_choose_application));
        }
    }

    public boolean dismissPopup() {
        if (isShowingPopup()) {
            getListPopupWindow().dismiss();
            ViewTreeObserver viewTreeObserver = getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeGlobalOnLayoutListener(this.mOnGlobalLayoutListener);
            }
        }
        return true;
    }

    public boolean isShowingPopup() {
        return getListPopupWindow().isShowing();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ActivityChooserModel dataModel = this.mAdapter.getDataModel();
        if (dataModel != null) {
            dataModel.registerObserver(this.mModelDataSetOberver);
        }
        this.mIsAttachedToWindow = true;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ActivityChooserModel dataModel = this.mAdapter.getDataModel();
        if (dataModel != null) {
            dataModel.unregisterObserver(this.mModelDataSetOberver);
        }
        ViewTreeObserver viewTreeObserver = getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.removeGlobalOnLayoutListener(this.mOnGlobalLayoutListener);
        }
        if (isShowingPopup()) {
            boolean dismissPopup = dismissPopup();
        }
        this.mIsAttachedToWindow = false;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        LinearLayoutCompat linearLayoutCompat = this.mActivityChooserContent;
        if (this.mDefaultActivityButton.getVisibility() != 0) {
            i4 = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i4), 1073741824);
        }
        measureChild(linearLayoutCompat, i3, i4);
        setMeasuredDimension(linearLayoutCompat.getMeasuredWidth(), linearLayoutCompat.getMeasuredHeight());
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.mActivityChooserContent.layout(0, 0, i3 - i, i4 - i2);
        if (!isShowingPopup()) {
            boolean dismissPopup = dismissPopup();
        }
    }

    public ActivityChooserModel getDataModel() {
        return this.mAdapter.getDataModel();
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
    }

    public void setInitialActivityCount(int i) {
        this.mInitialActivityCount = i;
    }

    public void setDefaultActionButtonContentDescription(int i) {
        this.mDefaultActionButtonContentDescription = i;
    }

    /* access modifiers changed from: private */
    public ListPopupWindow getListPopupWindow() {
        ListPopupWindow listPopupWindow;
        if (this.mListPopupWindow == null) {
            new ListPopupWindow(getContext());
            this.mListPopupWindow = listPopupWindow;
            this.mListPopupWindow.setAdapter(this.mAdapter);
            this.mListPopupWindow.setAnchorView(this);
            this.mListPopupWindow.setModal(true);
            this.mListPopupWindow.setOnItemClickListener(this.mCallbacks);
            this.mListPopupWindow.setOnDismissListener(this.mCallbacks);
        }
        return this.mListPopupWindow;
    }

    /* access modifiers changed from: private */
    public void updateAppearance() {
        if (this.mAdapter.getCount() > 0) {
            this.mExpandActivityOverflowButton.setEnabled(true);
        } else {
            this.mExpandActivityOverflowButton.setEnabled(false);
        }
        int activityCount = this.mAdapter.getActivityCount();
        int historySize = this.mAdapter.getHistorySize();
        if (activityCount == 1 || (activityCount > 1 && historySize > 0)) {
            this.mDefaultActivityButton.setVisibility(0);
            ResolveInfo defaultActivity = this.mAdapter.getDefaultActivity();
            PackageManager packageManager = getContext().getPackageManager();
            this.mDefaultActivityButtonImage.setImageDrawable(defaultActivity.loadIcon(packageManager));
            if (this.mDefaultActionButtonContentDescription != 0) {
                this.mDefaultActivityButton.setContentDescription(getContext().getString(this.mDefaultActionButtonContentDescription, defaultActivity.loadLabel(packageManager)));
            }
        } else {
            this.mDefaultActivityButton.setVisibility(8);
        }
        if (this.mDefaultActivityButton.getVisibility() == 0) {
            this.mActivityChooserContent.setBackgroundDrawable(this.mActivityChooserContentBackground);
        } else {
            this.mActivityChooserContent.setBackgroundDrawable(null);
        }
    }

    private class Callbacks implements AdapterView.OnItemClickListener, View.OnClickListener, View.OnLongClickListener, PopupWindow.OnDismissListener {
        private Callbacks() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            int i2;
            Throwable th;
            int i3 = i;
            switch (((ActivityChooserViewAdapter) adapterView.getAdapter()).getItemViewType(i3)) {
                case 0:
                    boolean dismissPopup = ActivityChooserView.this.dismissPopup();
                    if (!ActivityChooserView.this.mIsSelectingDefaultActivity) {
                        if (ActivityChooserView.this.mAdapter.getShowDefaultActivity()) {
                            i2 = i3;
                        } else {
                            i2 = i3 + 1;
                        }
                        Intent chooseActivity = ActivityChooserView.this.mAdapter.getDataModel().chooseActivity(i2);
                        if (chooseActivity != null) {
                            Intent addFlags = chooseActivity.addFlags(524288);
                            ActivityChooserView.this.getContext().startActivity(chooseActivity);
                            return;
                        }
                        return;
                    } else if (i3 > 0) {
                        ActivityChooserView.this.mAdapter.getDataModel().setDefaultActivity(i3);
                        return;
                    } else {
                        return;
                    }
                case 1:
                    ActivityChooserView.this.showPopupUnchecked(ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                    return;
                default:
                    Throwable th2 = th;
                    new IllegalArgumentException();
                    throw th2;
            }
        }

        public void onClick(View view) {
            Throwable th;
            View view2 = view;
            if (view2 == ActivityChooserView.this.mDefaultActivityButton) {
                boolean dismissPopup = ActivityChooserView.this.dismissPopup();
                Intent chooseActivity = ActivityChooserView.this.mAdapter.getDataModel().chooseActivity(ActivityChooserView.this.mAdapter.getDataModel().getActivityIndex(ActivityChooserView.this.mAdapter.getDefaultActivity()));
                if (chooseActivity != null) {
                    Intent addFlags = chooseActivity.addFlags(524288);
                    ActivityChooserView.this.getContext().startActivity(chooseActivity);
                }
            } else if (view2 == ActivityChooserView.this.mExpandActivityOverflowButton) {
                boolean access$602 = ActivityChooserView.access$602(ActivityChooserView.this, false);
                ActivityChooserView.this.showPopupUnchecked(ActivityChooserView.this.mInitialActivityCount);
            } else {
                Throwable th2 = th;
                new IllegalArgumentException();
                throw th2;
            }
        }

        public boolean onLongClick(View view) {
            Throwable th;
            if (view == ActivityChooserView.this.mDefaultActivityButton) {
                if (ActivityChooserView.this.mAdapter.getCount() > 0) {
                    boolean access$602 = ActivityChooserView.access$602(ActivityChooserView.this, true);
                    ActivityChooserView.this.showPopupUnchecked(ActivityChooserView.this.mInitialActivityCount);
                }
                return true;
            }
            Throwable th2 = th;
            new IllegalArgumentException();
            throw th2;
        }

        public void onDismiss() {
            notifyOnDismissListener();
            if (ActivityChooserView.this.mProvider != null) {
                ActivityChooserView.this.mProvider.subUiVisibilityChanged(false);
            }
        }

        private void notifyOnDismissListener() {
            if (ActivityChooserView.this.mOnDismissListener != null) {
                ActivityChooserView.this.mOnDismissListener.onDismiss();
            }
        }
    }

    private class ActivityChooserViewAdapter extends BaseAdapter {
        private static final int ITEM_VIEW_TYPE_ACTIVITY = 0;
        private static final int ITEM_VIEW_TYPE_COUNT = 3;
        private static final int ITEM_VIEW_TYPE_FOOTER = 1;
        public static final int MAX_ACTIVITY_COUNT_DEFAULT = 4;
        public static final int MAX_ACTIVITY_COUNT_UNLIMITED = Integer.MAX_VALUE;
        private ActivityChooserModel mDataModel;
        private boolean mHighlightDefaultActivity;
        private int mMaxActivityCount;
        private boolean mShowDefaultActivity;
        private boolean mShowFooterView;

        private ActivityChooserViewAdapter() {
            this.mMaxActivityCount = 4;
        }

        public void setDataModel(ActivityChooserModel activityChooserModel) {
            ActivityChooserModel activityChooserModel2 = activityChooserModel;
            ActivityChooserModel dataModel = ActivityChooserView.this.mAdapter.getDataModel();
            if (dataModel != null && ActivityChooserView.this.isShown()) {
                dataModel.unregisterObserver(ActivityChooserView.this.mModelDataSetOberver);
            }
            this.mDataModel = activityChooserModel2;
            if (activityChooserModel2 != null && ActivityChooserView.this.isShown()) {
                activityChooserModel2.registerObserver(ActivityChooserView.this.mModelDataSetOberver);
            }
            notifyDataSetChanged();
        }

        public int getItemViewType(int i) {
            int i2 = i;
            if (!this.mShowFooterView || i2 != getCount() - 1) {
                return 0;
            }
            return 1;
        }

        public int getViewTypeCount() {
            return 3;
        }

        public int getCount() {
            int activityCount = this.mDataModel.getActivityCount();
            if (!this.mShowDefaultActivity && this.mDataModel.getDefaultActivity() != null) {
                activityCount--;
            }
            int min = Math.min(activityCount, this.mMaxActivityCount);
            if (this.mShowFooterView) {
                min++;
            }
            return min;
        }

        public Object getItem(int i) {
            Throwable th;
            int i2 = i;
            switch (getItemViewType(i2)) {
                case 0:
                    if (!this.mShowDefaultActivity && this.mDataModel.getDefaultActivity() != null) {
                        i2++;
                    }
                    return this.mDataModel.getActivity(i2);
                case 1:
                    return null;
                default:
                    Throwable th2 = th;
                    new IllegalArgumentException();
                    throw th2;
            }
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            Throwable th;
            int i2 = i;
            View view2 = view;
            ViewGroup viewGroup2 = viewGroup;
            switch (getItemViewType(i2)) {
                case 0:
                    if (view2 == null || view2.getId() != R.id.list_item) {
                        view2 = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, viewGroup2, false);
                    }
                    PackageManager packageManager = ActivityChooserView.this.getContext().getPackageManager();
                    ImageView imageView = (ImageView) view2.findViewById(R.id.icon);
                    ResolveInfo resolveInfo = (ResolveInfo) getItem(i2);
                    imageView.setImageDrawable(resolveInfo.loadIcon(packageManager));
                    ((TextView) view2.findViewById(R.id.title)).setText(resolveInfo.loadLabel(packageManager));
                    if (!this.mShowDefaultActivity || i2 != 0 || !this.mHighlightDefaultActivity) {
                        ViewCompat.setActivated(view2, false);
                    } else {
                        ViewCompat.setActivated(view2, true);
                    }
                    return view2;
                case 1:
                    if (view2 == null || view2.getId() != 1) {
                        view2 = LayoutInflater.from(ActivityChooserView.this.getContext()).inflate(R.layout.abc_activity_chooser_view_list_item, viewGroup2, false);
                        view2.setId(1);
                        ((TextView) view2.findViewById(R.id.title)).setText(ActivityChooserView.this.getContext().getString(R.string.abc_activity_chooser_view_see_all));
                    }
                    return view2;
                default:
                    Throwable th2 = th;
                    new IllegalArgumentException();
                    throw th2;
            }
        }

        public int measureContentWidth() {
            int i = this.mMaxActivityCount;
            this.mMaxActivityCount = MAX_ACTIVITY_COUNT_UNLIMITED;
            int i2 = 0;
            View view = null;
            int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
            int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(0, 0);
            int count = getCount();
            for (int i3 = 0; i3 < count; i3++) {
                view = getView(i3, view, null);
                view.measure(makeMeasureSpec, makeMeasureSpec2);
                i2 = Math.max(i2, view.getMeasuredWidth());
            }
            this.mMaxActivityCount = i;
            return i2;
        }

        public void setMaxActivityCount(int i) {
            int i2 = i;
            if (this.mMaxActivityCount != i2) {
                this.mMaxActivityCount = i2;
                notifyDataSetChanged();
            }
        }

        public ResolveInfo getDefaultActivity() {
            return this.mDataModel.getDefaultActivity();
        }

        public void setShowFooterView(boolean z) {
            boolean z2 = z;
            if (this.mShowFooterView != z2) {
                this.mShowFooterView = z2;
                notifyDataSetChanged();
            }
        }

        public int getActivityCount() {
            return this.mDataModel.getActivityCount();
        }

        public int getHistorySize() {
            return this.mDataModel.getHistorySize();
        }

        public ActivityChooserModel getDataModel() {
            return this.mDataModel;
        }

        public void setShowDefaultActivity(boolean z, boolean z2) {
            boolean z3 = z;
            boolean z4 = z2;
            if (this.mShowDefaultActivity != z3 || this.mHighlightDefaultActivity != z4) {
                this.mShowDefaultActivity = z3;
                this.mHighlightDefaultActivity = z4;
                notifyDataSetChanged();
            }
        }

        public boolean getShowDefaultActivity() {
            return this.mShowDefaultActivity;
        }
    }

    public static class InnerLayout extends LinearLayoutCompat {
        private static final int[] TINT_ATTRS = {16842964};

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public InnerLayout(android.content.Context r8, android.util.AttributeSet r9) {
            /*
                r7 = this;
                r0 = r7
                r1 = r8
                r2 = r9
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r1
                r5 = r2
                int[] r6 = android.support.v7.widget.ActivityChooserView.InnerLayout.TINT_ATTRS
                android.support.v7.widget.TintTypedArray r4 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r4, r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                r6 = 0
                android.graphics.drawable.Drawable r5 = r5.getDrawable(r6)
                r4.setBackgroundDrawable(r5)
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActivityChooserView.InnerLayout.<init>(android.content.Context, android.util.AttributeSet):void");
        }
    }
}
