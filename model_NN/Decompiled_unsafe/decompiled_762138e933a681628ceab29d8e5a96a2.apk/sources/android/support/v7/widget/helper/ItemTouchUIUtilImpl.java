package android.support.v7.widget.helper;

import android.graphics.Canvas;
import android.support.v4.view.ViewCompat;
import android.support.v7.recyclerview.R;
import android.support.v7.widget.RecyclerView;
import android.view.View;

class ItemTouchUIUtilImpl {

    static class Lollipop extends Honeycomb {
        Lollipop() {
        }

        public void onDraw(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            Canvas canvas2 = canvas;
            RecyclerView recyclerView2 = recyclerView;
            View view2 = view;
            float f3 = f;
            float f4 = f2;
            int i2 = i;
            boolean z2 = z;
            if (z2 && view2.getTag(R.id.item_touch_helper_previous_elevation) == null) {
                Float valueOf = Float.valueOf(ViewCompat.getElevation(view2));
                ViewCompat.setElevation(view2, 1.0f + findMaxElevation(recyclerView2, view2));
                view2.setTag(R.id.item_touch_helper_previous_elevation, valueOf);
            }
            super.onDraw(canvas2, recyclerView2, view2, f3, f4, i2, z2);
        }

        private float findMaxElevation(RecyclerView recyclerView, View view) {
            RecyclerView recyclerView2 = recyclerView;
            View view2 = view;
            int childCount = recyclerView2.getChildCount();
            float f = 0.0f;
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView2.getChildAt(i);
                if (childAt != view2) {
                    float elevation = ViewCompat.getElevation(childAt);
                    if (elevation > f) {
                        f = elevation;
                    }
                }
            }
            return f;
        }

        public void clearView(View view) {
            View view2 = view;
            Object tag = view2.getTag(R.id.item_touch_helper_previous_elevation);
            if (tag != null && (tag instanceof Float)) {
                ViewCompat.setElevation(view2, ((Float) tag).floatValue());
            }
            view2.setTag(R.id.item_touch_helper_previous_elevation, null);
            super.clearView(view2);
        }
    }

    ItemTouchUIUtilImpl() {
    }

    static class Honeycomb implements ItemTouchUIUtil {
        Honeycomb() {
        }

        public void clearView(View view) {
            View view2 = view;
            ViewCompat.setTranslationX(view2, 0.0f);
            ViewCompat.setTranslationY(view2, 0.0f);
        }

        public void onSelected(View view) {
        }

        public void onDraw(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            View view2 = view;
            ViewCompat.setTranslationX(view2, f);
            ViewCompat.setTranslationY(view2, f2);
        }

        public void onDrawOver(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
        }
    }

    static class Gingerbread implements ItemTouchUIUtil {
        Gingerbread() {
        }

        private void draw(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2) {
            Canvas canvas2 = canvas;
            int save = canvas2.save();
            canvas2.translate(f, f2);
            boolean drawChild = recyclerView.drawChild(canvas2, view, 0);
            canvas2.restore();
        }

        public void clearView(View view) {
            view.setVisibility(0);
        }

        public void onSelected(View view) {
            view.setVisibility(4);
        }

        public void onDraw(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            Canvas canvas2 = canvas;
            RecyclerView recyclerView2 = recyclerView;
            View view2 = view;
            float f3 = f;
            float f4 = f2;
            if (i != 2) {
                draw(canvas2, recyclerView2, view2, f3, f4);
            }
        }

        public void onDrawOver(Canvas canvas, RecyclerView recyclerView, View view, float f, float f2, int i, boolean z) {
            Canvas canvas2 = canvas;
            RecyclerView recyclerView2 = recyclerView;
            View view2 = view;
            float f3 = f;
            float f4 = f2;
            if (i == 2) {
                draw(canvas2, recyclerView2, view2, f3, f4);
            }
        }
    }
}
