package android.support.v7.widget.helper;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.animation.AnimatorCompatHelper;
import android.support.v4.animation.AnimatorListenerCompat;
import android.support.v4.animation.AnimatorUpdateListenerCompat;
import android.support.v4.animation.ValueAnimatorCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.recyclerview.R;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchUIUtilImpl;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.List;

public class ItemTouchHelper extends RecyclerView.ItemDecoration implements RecyclerView.OnChildAttachStateChangeListener {
    private static final int ACTION_MODE_DRAG_MASK = 16711680;
    private static final int ACTION_MODE_IDLE_MASK = 255;
    private static final int ACTION_MODE_SWIPE_MASK = 65280;
    public static final int ACTION_STATE_DRAG = 2;
    public static final int ACTION_STATE_IDLE = 0;
    public static final int ACTION_STATE_SWIPE = 1;
    private static final int ACTIVE_POINTER_ID_NONE = -1;
    public static final int ANIMATION_TYPE_DRAG = 8;
    public static final int ANIMATION_TYPE_SWIPE_CANCEL = 4;
    public static final int ANIMATION_TYPE_SWIPE_SUCCESS = 2;
    private static final boolean DEBUG = false;
    private static final int DIRECTION_FLAG_COUNT = 8;
    public static final int DOWN = 2;
    public static final int END = 32;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int START = 16;
    private static final String TAG = "ItemTouchHelper";
    public static final int UP = 1;
    int mActionState = 0;
    int mActivePointerId = -1;
    Callback mCallback;
    private RecyclerView.ChildDrawingOrderCallback mChildDrawingOrderCallback;
    private List<Integer> mDistances;
    private long mDragScrollStartTimeInMs;
    float mDx;
    float mDy;
    /* access modifiers changed from: private */
    public GestureDetectorCompat mGestureDetector;
    float mInitialTouchX;
    float mInitialTouchY;
    private final RecyclerView.OnItemTouchListener mOnItemTouchListener;
    /* access modifiers changed from: private */
    public View mOverdrawChild;
    /* access modifiers changed from: private */
    public int mOverdrawChildPosition;
    final List<View> mPendingCleanup;
    List<RecoverAnimation> mRecoverAnimations;
    /* access modifiers changed from: private */
    public RecyclerView mRecyclerView;
    /* access modifiers changed from: private */
    public final Runnable mScrollRunnable;
    RecyclerView.ViewHolder mSelected = null;
    int mSelectedFlags;
    float mSelectedStartX;
    float mSelectedStartY;
    private int mSlop;
    private List<RecyclerView.ViewHolder> mSwapTargets;
    private final float[] mTmpPosition = new float[2];
    private Rect mTmpRect;
    /* access modifiers changed from: private */
    public VelocityTracker mVelocityTracker;

    public interface ViewDropHandler {
        void prepareForDrop(View view, View view2, int i, int i2);
    }

    static /* synthetic */ int access$2302(ItemTouchHelper itemTouchHelper, int i) {
        int i2 = i;
        int i3 = i2;
        itemTouchHelper.mOverdrawChildPosition = i3;
        return i2;
    }

    public ItemTouchHelper(Callback callback) {
        List<View> list;
        List<RecoverAnimation> list2;
        Runnable runnable;
        RecyclerView.OnItemTouchListener onItemTouchListener;
        new ArrayList();
        this.mPendingCleanup = list;
        new ArrayList();
        this.mRecoverAnimations = list2;
        new Runnable() {
            public void run() {
                if (ItemTouchHelper.this.mSelected != null && ItemTouchHelper.this.scrollIfNecessary()) {
                    if (ItemTouchHelper.this.mSelected != null) {
                        ItemTouchHelper.this.moveIfNecessary(ItemTouchHelper.this.mSelected);
                    }
                    boolean removeCallbacks = ItemTouchHelper.this.mRecyclerView.removeCallbacks(ItemTouchHelper.this.mScrollRunnable);
                    ViewCompat.postOnAnimation(ItemTouchHelper.this.mRecyclerView, this);
                }
            }
        };
        this.mScrollRunnable = runnable;
        this.mChildDrawingOrderCallback = null;
        this.mOverdrawChild = null;
        this.mOverdrawChildPosition = -1;
        new RecyclerView.OnItemTouchListener() {
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                int findPointerIndex;
                boolean z;
                RecoverAnimation access$600;
                MotionEvent motionEvent2 = motionEvent;
                boolean onTouchEvent = ItemTouchHelper.this.mGestureDetector.onTouchEvent(motionEvent2);
                int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
                if (actionMasked == 0) {
                    ItemTouchHelper.this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                    ItemTouchHelper.this.mInitialTouchX = motionEvent2.getX();
                    ItemTouchHelper.this.mInitialTouchY = motionEvent2.getY();
                    ItemTouchHelper.this.obtainVelocityTracker();
                    if (ItemTouchHelper.this.mSelected == null && (access$600 = ItemTouchHelper.this.findAnimation(motionEvent2)) != null) {
                        ItemTouchHelper.this.mInitialTouchX -= access$600.mX;
                        ItemTouchHelper.this.mInitialTouchY -= access$600.mY;
                        int access$700 = ItemTouchHelper.this.endRecoverAnimation(access$600.mViewHolder, true);
                        if (ItemTouchHelper.this.mPendingCleanup.remove(access$600.mViewHolder.itemView)) {
                            ItemTouchHelper.this.mCallback.clearView(ItemTouchHelper.this.mRecyclerView, access$600.mViewHolder);
                        }
                        ItemTouchHelper.this.select(access$600.mViewHolder, access$600.mActionState);
                        ItemTouchHelper.this.updateDxDy(motionEvent2, ItemTouchHelper.this.mSelectedFlags, 0);
                    }
                } else if (actionMasked == 3 || actionMasked == 1) {
                    ItemTouchHelper.this.mActivePointerId = -1;
                    ItemTouchHelper.this.select(null, 0);
                } else if (ItemTouchHelper.this.mActivePointerId != -1 && (findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, ItemTouchHelper.this.mActivePointerId)) >= 0) {
                    boolean access$1000 = ItemTouchHelper.this.checkSelectForSwipe(actionMasked, motionEvent2, findPointerIndex);
                }
                if (ItemTouchHelper.this.mVelocityTracker != null) {
                    ItemTouchHelper.this.mVelocityTracker.addMovement(motionEvent2);
                }
                if (ItemTouchHelper.this.mSelected != null) {
                    z = true;
                } else {
                    z = false;
                }
                return z;
            }

            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                MotionEvent motionEvent2 = motionEvent;
                boolean onTouchEvent = ItemTouchHelper.this.mGestureDetector.onTouchEvent(motionEvent2);
                if (ItemTouchHelper.this.mVelocityTracker != null) {
                    ItemTouchHelper.this.mVelocityTracker.addMovement(motionEvent2);
                }
                if (ItemTouchHelper.this.mActivePointerId != -1) {
                    int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, ItemTouchHelper.this.mActivePointerId);
                    if (findPointerIndex >= 0) {
                        boolean access$1000 = ItemTouchHelper.this.checkSelectForSwipe(actionMasked, motionEvent2, findPointerIndex);
                    }
                    RecyclerView.ViewHolder viewHolder = ItemTouchHelper.this.mSelected;
                    if (viewHolder != null) {
                        switch (actionMasked) {
                            case 1:
                            case 3:
                                if (ItemTouchHelper.this.mVelocityTracker != null) {
                                    ItemTouchHelper.this.mVelocityTracker.computeCurrentVelocity(1000, (float) ItemTouchHelper.this.mRecyclerView.getMaxFlingVelocity());
                                }
                                ItemTouchHelper.this.select(null, 0);
                                ItemTouchHelper.this.mActivePointerId = -1;
                                return;
                            case 2:
                                if (findPointerIndex >= 0) {
                                    ItemTouchHelper.this.updateDxDy(motionEvent2, ItemTouchHelper.this.mSelectedFlags, findPointerIndex);
                                    ItemTouchHelper.this.moveIfNecessary(viewHolder);
                                    boolean removeCallbacks = ItemTouchHelper.this.mRecyclerView.removeCallbacks(ItemTouchHelper.this.mScrollRunnable);
                                    ItemTouchHelper.this.mScrollRunnable.run();
                                    ItemTouchHelper.this.mRecyclerView.invalidate();
                                    return;
                                }
                                return;
                            case 4:
                            case 5:
                            default:
                                return;
                            case 6:
                                int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
                                if (MotionEventCompat.getPointerId(motionEvent2, actionIndex) == ItemTouchHelper.this.mActivePointerId) {
                                    if (ItemTouchHelper.this.mVelocityTracker != null) {
                                        ItemTouchHelper.this.mVelocityTracker.computeCurrentVelocity(1000, (float) ItemTouchHelper.this.mRecyclerView.getMaxFlingVelocity());
                                    }
                                    ItemTouchHelper.this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, actionIndex == 0 ? 1 : 0);
                                    ItemTouchHelper.this.updateDxDy(motionEvent2, ItemTouchHelper.this.mSelectedFlags, actionIndex);
                                    return;
                                }
                                return;
                        }
                    }
                }
            }

            public void onRequestDisallowInterceptTouchEvent(boolean z) {
                if (z) {
                    ItemTouchHelper.this.select(null, 0);
                }
            }
        };
        this.mOnItemTouchListener = onItemTouchListener;
        this.mCallback = callback;
    }

    private static boolean hitTest(View view, float f, float f2, float f3, float f4) {
        View view2 = view;
        float f5 = f;
        float f6 = f2;
        float f7 = f3;
        float f8 = f4;
        return f5 >= f7 && f5 <= f7 + ((float) view2.getWidth()) && f6 >= f8 && f6 <= f8 + ((float) view2.getHeight());
    }

    public void attachToRecyclerView(RecyclerView recyclerView) {
        RecyclerView recyclerView2 = recyclerView;
        if (this.mRecyclerView != recyclerView2) {
            if (this.mRecyclerView != null) {
                destroyCallbacks();
            }
            this.mRecyclerView = recyclerView2;
            if (this.mRecyclerView != null) {
                setupCallbacks();
            }
        }
    }

    private void setupCallbacks() {
        this.mSlop = ViewConfiguration.get(this.mRecyclerView.getContext()).getScaledTouchSlop();
        this.mRecyclerView.addItemDecoration(this);
        this.mRecyclerView.addOnItemTouchListener(this.mOnItemTouchListener);
        this.mRecyclerView.addOnChildAttachStateChangeListener(this);
        initGestureDetector();
    }

    private void destroyCallbacks() {
        this.mRecyclerView.removeItemDecoration(this);
        this.mRecyclerView.removeOnItemTouchListener(this.mOnItemTouchListener);
        this.mRecyclerView.removeOnChildAttachStateChangeListener(this);
        for (int size = this.mRecoverAnimations.size() - 1; size >= 0; size--) {
            this.mCallback.clearView(this.mRecyclerView, this.mRecoverAnimations.get(0).mViewHolder);
        }
        this.mRecoverAnimations.clear();
        this.mOverdrawChild = null;
        this.mOverdrawChildPosition = -1;
        releaseVelocityTracker();
    }

    private void initGestureDetector() {
        GestureDetectorCompat gestureDetectorCompat;
        GestureDetector.OnGestureListener onGestureListener;
        if (this.mGestureDetector == null) {
            new ItemTouchHelperGestureListener();
            new GestureDetectorCompat(this.mRecyclerView.getContext(), onGestureListener);
            this.mGestureDetector = gestureDetectorCompat;
        }
    }

    private void getSelectedDxDy(float[] fArr) {
        float[] fArr2 = fArr;
        if ((this.mSelectedFlags & 12) != 0) {
            fArr2[0] = (this.mSelectedStartX + this.mDx) - ((float) this.mSelected.itemView.getLeft());
        } else {
            fArr2[0] = ViewCompat.getTranslationX(this.mSelected.itemView);
        }
        if ((this.mSelectedFlags & 3) != 0) {
            fArr2[1] = (this.mSelectedStartY + this.mDy) - ((float) this.mSelected.itemView.getTop());
        } else {
            fArr2[1] = ViewCompat.getTranslationY(this.mSelected.itemView);
        }
    }

    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Canvas canvas2 = canvas;
        RecyclerView recyclerView2 = recyclerView;
        float f = 0.0f;
        float f2 = 0.0f;
        if (this.mSelected != null) {
            getSelectedDxDy(this.mTmpPosition);
            f = this.mTmpPosition[0];
            f2 = this.mTmpPosition[1];
        }
        this.mCallback.onDrawOver(canvas2, recyclerView2, this.mSelected, this.mRecoverAnimations, this.mActionState, f, f2);
    }

    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Canvas canvas2 = canvas;
        RecyclerView recyclerView2 = recyclerView;
        this.mOverdrawChildPosition = -1;
        float f = 0.0f;
        float f2 = 0.0f;
        if (this.mSelected != null) {
            getSelectedDxDy(this.mTmpPosition);
            f = this.mTmpPosition[0];
            f2 = this.mTmpPosition[1];
        }
        this.mCallback.onDraw(canvas2, recyclerView2, this.mSelected, this.mRecoverAnimations, this.mActionState, f, f2);
    }

    /* access modifiers changed from: private */
    public void select(RecyclerView.ViewHolder viewHolder, int i) {
        int swipeIfNecessary;
        float f;
        float signum;
        int i2;
        RecoverAnimation recoverAnimation;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i3 = i;
        if (viewHolder2 != this.mSelected || i3 != this.mActionState) {
            this.mDragScrollStartTimeInMs = Long.MIN_VALUE;
            int i4 = this.mActionState;
            int endRecoverAnimation = endRecoverAnimation(viewHolder2, true);
            this.mActionState = i3;
            if (i3 == 2) {
                this.mOverdrawChild = viewHolder2.itemView;
                addChildDrawingOrderCallback();
            }
            int i5 = (1 << (8 + (8 * i3))) - 1;
            boolean z = false;
            if (this.mSelected != null) {
                RecyclerView.ViewHolder viewHolder3 = this.mSelected;
                if (viewHolder3.itemView.getParent() != null) {
                    if (i4 == 2) {
                        swipeIfNecessary = 0;
                    } else {
                        swipeIfNecessary = swipeIfNecessary(viewHolder3);
                    }
                    int i6 = swipeIfNecessary;
                    releaseVelocityTracker();
                    switch (i6) {
                        case 1:
                        case 2:
                            f = 0.0f;
                            signum = Math.signum(this.mDy) * ((float) this.mRecyclerView.getHeight());
                            break;
                        case 4:
                        case 8:
                        case 16:
                        case 32:
                            signum = 0.0f;
                            f = Math.signum(this.mDx) * ((float) this.mRecyclerView.getWidth());
                            break;
                        default:
                            f = 0.0f;
                            signum = 0.0f;
                            break;
                    }
                    if (i4 == 2) {
                        i2 = 8;
                    } else if (i6 > 0) {
                        i2 = 2;
                    } else {
                        i2 = 4;
                    }
                    getSelectedDxDy(this.mTmpPosition);
                    float f2 = this.mTmpPosition[0];
                    float f3 = this.mTmpPosition[1];
                    final int i7 = i6;
                    final RecyclerView.ViewHolder viewHolder4 = viewHolder3;
                    new RecoverAnimation(this, viewHolder3, i2, i4, f2, f3, f, signum) {
                        final /* synthetic */ ItemTouchHelper this$0;

                        {
                            ItemTouchHelper itemTouchHelper = r21;
                            this.this$0 = itemTouchHelper;
                        }

                        public void onAnimationEnd(ValueAnimatorCompat valueAnimatorCompat) {
                            super.onAnimationEnd(valueAnimatorCompat);
                            if (!this.mOverridden) {
                                if (i7 <= 0) {
                                    this.this$0.mCallback.clearView(this.this$0.mRecyclerView, viewHolder4);
                                } else {
                                    boolean add = this.this$0.mPendingCleanup.add(viewHolder4.itemView);
                                    this.mIsPendingCleanup = true;
                                    if (i7 > 0) {
                                        this.this$0.postDispatchSwipe(this, i7);
                                    }
                                }
                                if (this.this$0.mOverdrawChild == viewHolder4.itemView) {
                                    this.this$0.removeChildDrawingOrderCallbackIfNecessary(viewHolder4.itemView);
                                }
                            }
                        }
                    };
                    RecoverAnimation recoverAnimation2 = recoverAnimation;
                    recoverAnimation2.setDuration(this.mCallback.getAnimationDuration(this.mRecyclerView, i2, f - f2, signum - f3));
                    boolean add = this.mRecoverAnimations.add(recoverAnimation2);
                    recoverAnimation2.start();
                    z = true;
                } else {
                    removeChildDrawingOrderCallbackIfNecessary(viewHolder3.itemView);
                    this.mCallback.clearView(this.mRecyclerView, viewHolder3);
                }
                this.mSelected = null;
            }
            if (viewHolder2 != null) {
                this.mSelectedFlags = (this.mCallback.getAbsoluteMovementFlags(this.mRecyclerView, viewHolder2) & i5) >> (this.mActionState * 8);
                this.mSelectedStartX = (float) viewHolder2.itemView.getLeft();
                this.mSelectedStartY = (float) viewHolder2.itemView.getTop();
                this.mSelected = viewHolder2;
                if (i3 == 2) {
                    boolean performHapticFeedback = this.mSelected.itemView.performHapticFeedback(0);
                }
            }
            ViewParent parent = this.mRecyclerView.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(this.mSelected != null);
            }
            if (!z) {
                this.mRecyclerView.getLayoutManager().requestSimpleAnimationsInNextLayout();
            }
            this.mCallback.onSelectedChanged(this.mSelected, this.mActionState);
            this.mRecyclerView.invalidate();
        }
    }

    /* access modifiers changed from: private */
    public void postDispatchSwipe(RecoverAnimation recoverAnimation, int i) {
        Runnable runnable;
        final RecoverAnimation recoverAnimation2 = recoverAnimation;
        final int i2 = i;
        new Runnable() {
            public void run() {
                if (ItemTouchHelper.this.mRecyclerView != null && ItemTouchHelper.this.mRecyclerView.isAttachedToWindow() && !recoverAnimation2.mOverridden && recoverAnimation2.mViewHolder.getAdapterPosition() != -1) {
                    RecyclerView.ItemAnimator itemAnimator = ItemTouchHelper.this.mRecyclerView.getItemAnimator();
                    if ((itemAnimator == null || !itemAnimator.isRunning(null)) && !ItemTouchHelper.this.hasRunningRecoverAnim()) {
                        ItemTouchHelper.this.mCallback.onSwiped(recoverAnimation2.mViewHolder, i2);
                    } else {
                        boolean post = ItemTouchHelper.this.mRecyclerView.post(this);
                    }
                }
            }
        };
        boolean post = this.mRecyclerView.post(runnable);
    }

    /* access modifiers changed from: private */
    public boolean hasRunningRecoverAnim() {
        int size = this.mRecoverAnimations.size();
        for (int i = 0; i < size; i++) {
            if (!this.mRecoverAnimations.get(i).mEnded) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean scrollIfNecessary() {
        long j;
        int height;
        int width;
        Rect rect;
        if (this.mSelected == null) {
            this.mDragScrollStartTimeInMs = Long.MIN_VALUE;
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis();
        if (this.mDragScrollStartTimeInMs == Long.MIN_VALUE) {
            j = 0;
        } else {
            j = currentTimeMillis - this.mDragScrollStartTimeInMs;
        }
        long j2 = j;
        RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
        if (this.mTmpRect == null) {
            new Rect();
            this.mTmpRect = rect;
        }
        int i = 0;
        int i2 = 0;
        layoutManager.calculateItemDecorationsForChild(this.mSelected.itemView, this.mTmpRect);
        if (layoutManager.canScrollHorizontally()) {
            int i3 = (int) (this.mSelectedStartX + this.mDx);
            int paddingLeft = (i3 - this.mTmpRect.left) - this.mRecyclerView.getPaddingLeft();
            if (this.mDx < 0.0f && paddingLeft < 0) {
                i = paddingLeft;
            } else if (this.mDx > 0.0f && (width = ((i3 + this.mSelected.itemView.getWidth()) + this.mTmpRect.right) - (this.mRecyclerView.getWidth() - this.mRecyclerView.getPaddingRight())) > 0) {
                i = width;
            }
        }
        if (layoutManager.canScrollVertically()) {
            int i4 = (int) (this.mSelectedStartY + this.mDy);
            int paddingTop = (i4 - this.mTmpRect.top) - this.mRecyclerView.getPaddingTop();
            if (this.mDy < 0.0f && paddingTop < 0) {
                i2 = paddingTop;
            } else if (this.mDy > 0.0f && (height = ((i4 + this.mSelected.itemView.getHeight()) + this.mTmpRect.bottom) - (this.mRecyclerView.getHeight() - this.mRecyclerView.getPaddingBottom())) > 0) {
                i2 = height;
            }
        }
        if (i != 0) {
            i = this.mCallback.interpolateOutOfBoundsScroll(this.mRecyclerView, this.mSelected.itemView.getWidth(), i, this.mRecyclerView.getWidth(), j2);
        }
        if (i2 != 0) {
            i2 = this.mCallback.interpolateOutOfBoundsScroll(this.mRecyclerView, this.mSelected.itemView.getHeight(), i2, this.mRecyclerView.getHeight(), j2);
        }
        if (i == 0 && i2 == 0) {
            this.mDragScrollStartTimeInMs = Long.MIN_VALUE;
            return false;
        }
        if (this.mDragScrollStartTimeInMs == Long.MIN_VALUE) {
            this.mDragScrollStartTimeInMs = currentTimeMillis;
        }
        this.mRecyclerView.scrollBy(i, i2);
        return true;
    }

    private List<RecyclerView.ViewHolder> findSwapTargets(RecyclerView.ViewHolder viewHolder) {
        List<RecyclerView.ViewHolder> list;
        List<Integer> list2;
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (this.mSwapTargets == null) {
            new ArrayList();
            this.mSwapTargets = list;
            new ArrayList();
            this.mDistances = list2;
        } else {
            this.mSwapTargets.clear();
            this.mDistances.clear();
        }
        int boundingBoxMargin = this.mCallback.getBoundingBoxMargin();
        int round = Math.round(this.mSelectedStartX + this.mDx) - boundingBoxMargin;
        int round2 = Math.round(this.mSelectedStartY + this.mDy) - boundingBoxMargin;
        int width = round + viewHolder2.itemView.getWidth() + (2 * boundingBoxMargin);
        int height = round2 + viewHolder2.itemView.getHeight() + (2 * boundingBoxMargin);
        int i = (round + width) / 2;
        int i2 = (round2 + height) / 2;
        RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
        int childCount = layoutManager.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = layoutManager.getChildAt(i3);
            if (childAt != viewHolder2.itemView && childAt.getBottom() >= round2 && childAt.getTop() <= height && childAt.getRight() >= round && childAt.getLeft() <= width) {
                RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(childAt);
                if (this.mCallback.canDropOver(this.mRecyclerView, this.mSelected, childViewHolder)) {
                    int abs = Math.abs(i - ((childAt.getLeft() + childAt.getRight()) / 2));
                    int abs2 = Math.abs(i2 - ((childAt.getTop() + childAt.getBottom()) / 2));
                    int i4 = (abs * abs) + (abs2 * abs2);
                    int i5 = 0;
                    int size = this.mSwapTargets.size();
                    int i6 = 0;
                    while (i6 < size && i4 > this.mDistances.get(i6).intValue()) {
                        i5++;
                        i6++;
                    }
                    this.mSwapTargets.add(i5, childViewHolder);
                    this.mDistances.add(i5, Integer.valueOf(i4));
                }
            }
        }
        return this.mSwapTargets;
    }

    /* access modifiers changed from: private */
    public void moveIfNecessary(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (!this.mRecyclerView.isLayoutRequested() && this.mActionState == 2) {
            float moveThreshold = this.mCallback.getMoveThreshold(viewHolder2);
            int i = (int) (this.mSelectedStartX + this.mDx);
            int i2 = (int) (this.mSelectedStartY + this.mDy);
            if (((float) Math.abs(i2 - viewHolder2.itemView.getTop())) >= ((float) viewHolder2.itemView.getHeight()) * moveThreshold || ((float) Math.abs(i - viewHolder2.itemView.getLeft())) >= ((float) viewHolder2.itemView.getWidth()) * moveThreshold) {
                List<RecyclerView.ViewHolder> findSwapTargets = findSwapTargets(viewHolder2);
                if (findSwapTargets.size() != 0) {
                    RecyclerView.ViewHolder chooseDropTarget = this.mCallback.chooseDropTarget(viewHolder2, findSwapTargets, i, i2);
                    if (chooseDropTarget == null) {
                        this.mSwapTargets.clear();
                        this.mDistances.clear();
                        return;
                    }
                    int adapterPosition = chooseDropTarget.getAdapterPosition();
                    int adapterPosition2 = viewHolder2.getAdapterPosition();
                    if (this.mCallback.onMove(this.mRecyclerView, viewHolder2, chooseDropTarget)) {
                        this.mCallback.onMoved(this.mRecyclerView, viewHolder2, adapterPosition2, chooseDropTarget, adapterPosition, i, i2);
                    }
                }
            }
        }
    }

    public void onChildViewAttachedToWindow(View view) {
    }

    public void onChildViewDetachedFromWindow(View view) {
        View view2 = view;
        removeChildDrawingOrderCallbackIfNecessary(view2);
        RecyclerView.ViewHolder childViewHolder = this.mRecyclerView.getChildViewHolder(view2);
        if (childViewHolder != null) {
            if (this.mSelected == null || childViewHolder != this.mSelected) {
                int endRecoverAnimation = endRecoverAnimation(childViewHolder, false);
                if (this.mPendingCleanup.remove(childViewHolder.itemView)) {
                    this.mCallback.clearView(this.mRecyclerView, childViewHolder);
                    return;
                }
                return;
            }
            select(null, 0);
        }
    }

    /* access modifiers changed from: private */
    public int endRecoverAnimation(RecyclerView.ViewHolder viewHolder, boolean z) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        boolean z2 = z;
        for (int size = this.mRecoverAnimations.size() - 1; size >= 0; size--) {
            RecoverAnimation recoverAnimation = this.mRecoverAnimations.get(size);
            if (recoverAnimation.mViewHolder == viewHolder2) {
                recoverAnimation.mOverridden |= z2;
                if (!recoverAnimation.mEnded) {
                    recoverAnimation.cancel();
                }
                RecoverAnimation remove = this.mRecoverAnimations.remove(size);
                return recoverAnimation.mAnimationType;
            }
        }
        return 0;
    }

    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        rect.setEmpty();
    }

    /* access modifiers changed from: private */
    public void obtainVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
        }
        this.mVelocityTracker = VelocityTracker.obtain();
    }

    private void releaseVelocityTracker() {
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    private RecyclerView.ViewHolder findSwipedView(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        RecyclerView.LayoutManager layoutManager = this.mRecyclerView.getLayoutManager();
        if (this.mActivePointerId == -1) {
            return null;
        }
        int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
        float x = MotionEventCompat.getX(motionEvent2, findPointerIndex) - this.mInitialTouchX;
        float y = MotionEventCompat.getY(motionEvent2, findPointerIndex) - this.mInitialTouchY;
        float abs = Math.abs(x);
        float abs2 = Math.abs(y);
        if (abs < ((float) this.mSlop) && abs2 < ((float) this.mSlop)) {
            return null;
        }
        if (abs > abs2 && layoutManager.canScrollHorizontally()) {
            return null;
        }
        if (abs2 > abs && layoutManager.canScrollVertically()) {
            return null;
        }
        View findChildView = findChildView(motionEvent2);
        if (findChildView == null) {
            return null;
        }
        return this.mRecyclerView.getChildViewHolder(findChildView);
    }

    /* access modifiers changed from: private */
    public boolean checkSelectForSwipe(int i, MotionEvent motionEvent, int i2) {
        int i3 = i;
        MotionEvent motionEvent2 = motionEvent;
        int i4 = i2;
        if (this.mSelected != null || i3 != 2 || this.mActionState == 2 || !this.mCallback.isItemViewSwipeEnabled()) {
            return false;
        }
        if (this.mRecyclerView.getScrollState() == 1) {
            return false;
        }
        RecyclerView.ViewHolder findSwipedView = findSwipedView(motionEvent2);
        if (findSwipedView == null) {
            return false;
        }
        int absoluteMovementFlags = (this.mCallback.getAbsoluteMovementFlags(this.mRecyclerView, findSwipedView) & 65280) >> 8;
        if (absoluteMovementFlags == 0) {
            return false;
        }
        float x = MotionEventCompat.getX(motionEvent2, i4);
        float y = MotionEventCompat.getY(motionEvent2, i4);
        float f = x - this.mInitialTouchX;
        float f2 = y - this.mInitialTouchY;
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        if (abs < ((float) this.mSlop) && abs2 < ((float) this.mSlop)) {
            return false;
        }
        if (abs > abs2) {
            if (f < 0.0f && (absoluteMovementFlags & 4) == 0) {
                return false;
            }
            if (f > 0.0f && (absoluteMovementFlags & 8) == 0) {
                return false;
            }
        } else if (f2 < 0.0f && (absoluteMovementFlags & 1) == 0) {
            return false;
        } else {
            if (f2 > 0.0f && (absoluteMovementFlags & 2) == 0) {
                return false;
            }
        }
        this.mDy = 0.0f;
        this.mDx = 0.0f;
        this.mActivePointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
        select(findSwipedView, 1);
        return true;
    }

    /* access modifiers changed from: private */
    public View findChildView(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        float x = motionEvent2.getX();
        float y = motionEvent2.getY();
        if (this.mSelected != null) {
            View view = this.mSelected.itemView;
            if (hitTest(view, x, y, this.mSelectedStartX + this.mDx, this.mSelectedStartY + this.mDy)) {
                return view;
            }
        }
        for (int size = this.mRecoverAnimations.size() - 1; size >= 0; size--) {
            RecoverAnimation recoverAnimation = this.mRecoverAnimations.get(size);
            View view2 = recoverAnimation.mViewHolder.itemView;
            if (hitTest(view2, x, y, recoverAnimation.mX, recoverAnimation.mY)) {
                return view2;
            }
        }
        return this.mRecyclerView.findChildViewUnder(x, y);
    }

    public void startDrag(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (!this.mCallback.hasDragFlag(this.mRecyclerView, viewHolder2)) {
            int e = Log.e(TAG, "Start drag has been called but swiping is not enabled");
        } else if (viewHolder2.itemView.getParent() != this.mRecyclerView) {
            int e2 = Log.e(TAG, "Start drag has been called with a view holder which is not a child of the RecyclerView which is controlled by this ItemTouchHelper.");
        } else {
            obtainVelocityTracker();
            this.mDy = 0.0f;
            this.mDx = 0.0f;
            select(viewHolder2, 2);
        }
    }

    public void startSwipe(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (!this.mCallback.hasSwipeFlag(this.mRecyclerView, viewHolder2)) {
            int e = Log.e(TAG, "Start swipe has been called but dragging is not enabled");
        } else if (viewHolder2.itemView.getParent() != this.mRecyclerView) {
            int e2 = Log.e(TAG, "Start swipe has been called with a view holder which is not a child of the RecyclerView controlled by this ItemTouchHelper.");
        } else {
            obtainVelocityTracker();
            this.mDy = 0.0f;
            this.mDx = 0.0f;
            select(viewHolder2, 1);
        }
    }

    /* access modifiers changed from: private */
    public RecoverAnimation findAnimation(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        if (this.mRecoverAnimations.isEmpty()) {
            return null;
        }
        View findChildView = findChildView(motionEvent2);
        for (int size = this.mRecoverAnimations.size() - 1; size >= 0; size--) {
            RecoverAnimation recoverAnimation = this.mRecoverAnimations.get(size);
            if (recoverAnimation.mViewHolder.itemView == findChildView) {
                return recoverAnimation;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* access modifiers changed from: private */
    public void updateDxDy(MotionEvent motionEvent, int i, int i2) {
        MotionEvent motionEvent2 = motionEvent;
        int i3 = i;
        int i4 = i2;
        this.mDx = MotionEventCompat.getX(motionEvent2, i4) - this.mInitialTouchX;
        this.mDy = MotionEventCompat.getY(motionEvent2, i4) - this.mInitialTouchY;
        if ((i3 & 4) == 0) {
            this.mDx = Math.max(0.0f, this.mDx);
        }
        if ((i3 & 8) == 0) {
            this.mDx = Math.min(0.0f, this.mDx);
        }
        if ((i3 & 1) == 0) {
            this.mDy = Math.max(0.0f, this.mDy);
        }
        if ((i3 & 2) == 0) {
            this.mDy = Math.min(0.0f, this.mDy);
        }
    }

    private int swipeIfNecessary(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        if (this.mActionState == 2) {
            return 0;
        }
        int movementFlags = this.mCallback.getMovementFlags(this.mRecyclerView, viewHolder2);
        int convertToAbsoluteDirection = (this.mCallback.convertToAbsoluteDirection(movementFlags, ViewCompat.getLayoutDirection(this.mRecyclerView)) & 65280) >> 8;
        if (convertToAbsoluteDirection == 0) {
            return 0;
        }
        int i = (movementFlags & 65280) >> 8;
        if (Math.abs(this.mDx) > Math.abs(this.mDy)) {
            int checkHorizontalSwipe = checkHorizontalSwipe(viewHolder2, convertToAbsoluteDirection);
            int i2 = checkHorizontalSwipe;
            if (checkHorizontalSwipe <= 0) {
                int checkVerticalSwipe = checkVerticalSwipe(viewHolder2, convertToAbsoluteDirection);
                int i3 = checkVerticalSwipe;
                if (checkVerticalSwipe > 0) {
                    return i3;
                }
            } else if ((i & i2) == 0) {
                return Callback.convertToRelativeDirection(i2, ViewCompat.getLayoutDirection(this.mRecyclerView));
            } else {
                return i2;
            }
        } else {
            int checkVerticalSwipe2 = checkVerticalSwipe(viewHolder2, convertToAbsoluteDirection);
            int i4 = checkVerticalSwipe2;
            if (checkVerticalSwipe2 > 0) {
                return i4;
            }
            int checkHorizontalSwipe2 = checkHorizontalSwipe(viewHolder2, convertToAbsoluteDirection);
            int i5 = checkHorizontalSwipe2;
            if (checkHorizontalSwipe2 > 0) {
                if ((i & i5) == 0) {
                    return Callback.convertToRelativeDirection(i5, ViewCompat.getLayoutDirection(this.mRecyclerView));
                }
                return i5;
            }
        }
        return 0;
    }

    private int checkHorizontalSwipe(RecyclerView.ViewHolder viewHolder, int i) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i2 = i;
        if ((i2 & 12) != 0) {
            int i3 = this.mDx > 0.0f ? 8 : 4;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                float xVelocity = VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mActivePointerId);
                int i4 = xVelocity > 0.0f ? 8 : 4;
                if ((i4 & i2) != 0 && i3 == i4 && Math.abs(xVelocity) >= ((float) this.mRecyclerView.getMinFlingVelocity())) {
                    return i4;
                }
            }
            float width = ((float) this.mRecyclerView.getWidth()) * this.mCallback.getSwipeThreshold(viewHolder2);
            if ((i2 & i3) != 0 && Math.abs(this.mDx) > width) {
                return i3;
            }
        }
        return 0;
    }

    private int checkVerticalSwipe(RecyclerView.ViewHolder viewHolder, int i) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int i2 = i;
        if ((i2 & 3) != 0) {
            int i3 = this.mDy > 0.0f ? 2 : 1;
            if (this.mVelocityTracker != null && this.mActivePointerId > -1) {
                float yVelocity = VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mActivePointerId);
                int i4 = yVelocity > 0.0f ? 2 : 1;
                if ((i4 & i2) != 0 && i4 == i3 && Math.abs(yVelocity) >= ((float) this.mRecyclerView.getMinFlingVelocity())) {
                    return i4;
                }
            }
            float height = ((float) this.mRecyclerView.getHeight()) * this.mCallback.getSwipeThreshold(viewHolder2);
            if ((i2 & i3) != 0 && Math.abs(this.mDy) > height) {
                return i3;
            }
        }
        return 0;
    }

    private void addChildDrawingOrderCallback() {
        RecyclerView.ChildDrawingOrderCallback childDrawingOrderCallback;
        if (Build.VERSION.SDK_INT < 21) {
            if (this.mChildDrawingOrderCallback == null) {
                new RecyclerView.ChildDrawingOrderCallback() {
                    public int onGetChildDrawingOrder(int i, int i2) {
                        int i3 = i;
                        int i4 = i2;
                        if (ItemTouchHelper.this.mOverdrawChild == null) {
                            return i4;
                        }
                        int access$2300 = ItemTouchHelper.this.mOverdrawChildPosition;
                        if (access$2300 == -1) {
                            access$2300 = ItemTouchHelper.this.mRecyclerView.indexOfChild(ItemTouchHelper.this.mOverdrawChild);
                            int access$2302 = ItemTouchHelper.access$2302(ItemTouchHelper.this, access$2300);
                        }
                        if (i4 == i3 - 1) {
                            return access$2300;
                        }
                        return i4 < access$2300 ? i4 : i4 + 1;
                    }
                };
                this.mChildDrawingOrderCallback = childDrawingOrderCallback;
            }
            this.mRecyclerView.setChildDrawingOrderCallback(this.mChildDrawingOrderCallback);
        }
    }

    /* access modifiers changed from: private */
    public void removeChildDrawingOrderCallbackIfNecessary(View view) {
        if (view == this.mOverdrawChild) {
            this.mOverdrawChild = null;
            if (this.mChildDrawingOrderCallback != null) {
                this.mRecyclerView.setChildDrawingOrderCallback(null);
            }
        }
    }

    public static abstract class Callback {
        private static final int ABS_HORIZONTAL_DIR_FLAGS = 789516;
        public static final int DEFAULT_DRAG_ANIMATION_DURATION = 200;
        public static final int DEFAULT_SWIPE_ANIMATION_DURATION = 250;
        private static final long DRAG_SCROLL_ACCELERATION_LIMIT_TIME_MS = 2000;
        static final int RELATIVE_DIR_FLAGS = 3158064;
        private static final Interpolator sDragScrollInterpolator;
        private static final Interpolator sDragViewScrollCapInterpolator;
        private static final ItemTouchUIUtil sUICallback;
        private int mCachedMaxScrollSpeed = -1;

        public abstract int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder);

        public abstract boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2);

        public abstract void onSwiped(RecyclerView.ViewHolder viewHolder, int i);

        static {
            Interpolator interpolator;
            Interpolator interpolator2;
            ItemTouchUIUtil itemTouchUIUtil;
            ItemTouchUIUtil itemTouchUIUtil2;
            ItemTouchUIUtil itemTouchUIUtil3;
            new Interpolator() {
                public float getInterpolation(float f) {
                    float f2 = f;
                    return f2 * f2 * f2 * f2 * f2;
                }
            };
            sDragScrollInterpolator = interpolator;
            new Interpolator() {
                public float getInterpolation(float f) {
                    float f2 = f - 1.0f;
                    return (f2 * f2 * f2 * f2 * f2) + 1.0f;
                }
            };
            sDragViewScrollCapInterpolator = interpolator2;
            if (Build.VERSION.SDK_INT >= 21) {
                new ItemTouchUIUtilImpl.Lollipop();
                sUICallback = itemTouchUIUtil3;
            } else if (Build.VERSION.SDK_INT >= 11) {
                new ItemTouchUIUtilImpl.Honeycomb();
                sUICallback = itemTouchUIUtil2;
            } else {
                new ItemTouchUIUtilImpl.Gingerbread();
                sUICallback = itemTouchUIUtil;
            }
        }

        public static ItemTouchUIUtil getDefaultUIUtil() {
            return sUICallback;
        }

        public static int convertToRelativeDirection(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            int i5 = i3 & ABS_HORIZONTAL_DIR_FLAGS;
            if (i5 == 0) {
                return i3;
            }
            int i6 = i3 & (i5 ^ -1);
            if (i4 == 0) {
                return i6 | (i5 << 2);
            }
            return i6 | ((i5 << 1) & -789517) | (((i5 << 1) & ABS_HORIZONTAL_DIR_FLAGS) << 2);
        }

        public static int makeMovementFlags(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            return makeFlag(0, i4 | i3) | makeFlag(1, i4) | makeFlag(2, i3);
        }

        public static int makeFlag(int i, int i2) {
            return i2 << (i * 8);
        }

        public int convertToAbsoluteDirection(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            int i5 = i3 & RELATIVE_DIR_FLAGS;
            if (i5 == 0) {
                return i3;
            }
            int i6 = i3 & (i5 ^ -1);
            if (i4 == 0) {
                return i6 | (i5 >> 2);
            }
            return i6 | ((i5 >> 1) & -3158065) | (((i5 >> 1) & RELATIVE_DIR_FLAGS) >> 2);
        }

        /* access modifiers changed from: package-private */
        public final int getAbsoluteMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            RecyclerView recyclerView2 = recyclerView;
            return convertToAbsoluteDirection(getMovementFlags(recyclerView2, viewHolder), ViewCompat.getLayoutDirection(recyclerView2));
        }

        /* access modifiers changed from: private */
        public boolean hasDragFlag(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return (getAbsoluteMovementFlags(recyclerView, viewHolder) & ItemTouchHelper.ACTION_MODE_DRAG_MASK) != 0;
        }

        /* access modifiers changed from: private */
        public boolean hasSwipeFlag(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return (getAbsoluteMovementFlags(recyclerView, viewHolder) & 65280) != 0;
        }

        public boolean canDropOver(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            return true;
        }

        public boolean isLongPressDragEnabled() {
            return true;
        }

        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        public int getBoundingBoxMargin() {
            return 0;
        }

        public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        public float getMoveThreshold(RecyclerView.ViewHolder viewHolder) {
            return 0.5f;
        }

        public RecyclerView.ViewHolder chooseDropTarget(RecyclerView.ViewHolder viewHolder, List<RecyclerView.ViewHolder> list, int i, int i2) {
            int bottom;
            int abs;
            int top;
            int abs2;
            int left;
            int abs3;
            int right;
            int abs4;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            List<RecyclerView.ViewHolder> list2 = list;
            int i3 = i;
            int i4 = i2;
            int width = i3 + viewHolder2.itemView.getWidth();
            int height = i4 + viewHolder2.itemView.getHeight();
            RecyclerView.ViewHolder viewHolder3 = null;
            int i5 = -1;
            int left2 = i3 - viewHolder2.itemView.getLeft();
            int top2 = i4 - viewHolder2.itemView.getTop();
            int size = list2.size();
            for (int i6 = 0; i6 < size; i6++) {
                RecyclerView.ViewHolder viewHolder4 = list2.get(i6);
                if (left2 > 0 && (right = viewHolder4.itemView.getRight() - width) < 0 && viewHolder4.itemView.getRight() > viewHolder2.itemView.getRight() && (abs4 = Math.abs(right)) > i5) {
                    i5 = abs4;
                    viewHolder3 = viewHolder4;
                }
                if (left2 < 0 && (left = viewHolder4.itemView.getLeft() - i3) > 0 && viewHolder4.itemView.getLeft() < viewHolder2.itemView.getLeft() && (abs3 = Math.abs(left)) > i5) {
                    i5 = abs3;
                    viewHolder3 = viewHolder4;
                }
                if (top2 < 0 && (top = viewHolder4.itemView.getTop() - i4) > 0 && viewHolder4.itemView.getTop() < viewHolder2.itemView.getTop() && (abs2 = Math.abs(top)) > i5) {
                    i5 = abs2;
                    viewHolder3 = viewHolder4;
                }
                if (top2 > 0 && (bottom = viewHolder4.itemView.getBottom() - height) < 0 && viewHolder4.itemView.getBottom() > viewHolder2.itemView.getBottom() && (abs = Math.abs(bottom)) > i5) {
                    i5 = abs;
                    viewHolder3 = viewHolder4;
                }
            }
            return viewHolder3;
        }

        public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int i) {
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            if (viewHolder2 != null) {
                sUICallback.onSelected(viewHolder2.itemView);
            }
        }

        private int getMaxDragScroll(RecyclerView recyclerView) {
            RecyclerView recyclerView2 = recyclerView;
            if (this.mCachedMaxScrollSpeed == -1) {
                this.mCachedMaxScrollSpeed = recyclerView2.getResources().getDimensionPixelSize(R.dimen.item_touch_helper_max_drag_scroll_per_frame);
            }
            return this.mCachedMaxScrollSpeed;
        }

        public void onMoved(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int i, RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4) {
            RecyclerView recyclerView2 = recyclerView;
            RecyclerView.ViewHolder viewHolder3 = viewHolder;
            RecyclerView.ViewHolder viewHolder4 = viewHolder2;
            int i5 = i2;
            int i6 = i3;
            int i7 = i4;
            RecyclerView.LayoutManager layoutManager = recyclerView2.getLayoutManager();
            if (layoutManager instanceof ViewDropHandler) {
                ((ViewDropHandler) layoutManager).prepareForDrop(viewHolder3.itemView, viewHolder4.itemView, i6, i7);
                return;
            }
            if (layoutManager.canScrollHorizontally()) {
                if (layoutManager.getDecoratedLeft(viewHolder4.itemView) <= recyclerView2.getPaddingLeft()) {
                    recyclerView2.scrollToPosition(i5);
                }
                if (layoutManager.getDecoratedRight(viewHolder4.itemView) >= recyclerView2.getWidth() - recyclerView2.getPaddingRight()) {
                    recyclerView2.scrollToPosition(i5);
                }
            }
            if (layoutManager.canScrollVertically()) {
                if (layoutManager.getDecoratedTop(viewHolder4.itemView) <= recyclerView2.getPaddingTop()) {
                    recyclerView2.scrollToPosition(i5);
                }
                if (layoutManager.getDecoratedBottom(viewHolder4.itemView) >= recyclerView2.getHeight() - recyclerView2.getPaddingBottom()) {
                    recyclerView2.scrollToPosition(i5);
                }
            }
        }

        /* access modifiers changed from: private */
        public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<RecoverAnimation> list, int i, float f, float f2) {
            Canvas canvas2 = canvas;
            RecyclerView recyclerView2 = recyclerView;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            List<RecoverAnimation> list2 = list;
            int i2 = i;
            float f3 = f;
            float f4 = f2;
            int size = list2.size();
            for (int i3 = 0; i3 < size; i3++) {
                RecoverAnimation recoverAnimation = list2.get(i3);
                recoverAnimation.update();
                int save = canvas2.save();
                onChildDraw(canvas2, recyclerView2, recoverAnimation.mViewHolder, recoverAnimation.mX, recoverAnimation.mY, recoverAnimation.mActionState, false);
                canvas2.restoreToCount(save);
            }
            if (viewHolder2 != null) {
                int save2 = canvas2.save();
                onChildDraw(canvas2, recyclerView2, viewHolder2, f3, f4, i2, true);
                canvas2.restoreToCount(save2);
            }
        }

        /* access modifiers changed from: private */
        public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, List<RecoverAnimation> list, int i, float f, float f2) {
            Canvas canvas2 = canvas;
            RecyclerView recyclerView2 = recyclerView;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            List<RecoverAnimation> list2 = list;
            int i2 = i;
            float f3 = f;
            float f4 = f2;
            int size = list2.size();
            for (int i3 = 0; i3 < size; i3++) {
                RecoverAnimation recoverAnimation = list2.get(i3);
                int save = canvas2.save();
                onChildDrawOver(canvas2, recyclerView2, recoverAnimation.mViewHolder, recoverAnimation.mX, recoverAnimation.mY, recoverAnimation.mActionState, false);
                canvas2.restoreToCount(save);
            }
            if (viewHolder2 != null) {
                int save2 = canvas2.save();
                onChildDrawOver(canvas2, recyclerView2, viewHolder2, f3, f4, i2, true);
                canvas2.restoreToCount(save2);
            }
            boolean z = false;
            for (int i4 = size - 1; i4 >= 0; i4--) {
                RecoverAnimation recoverAnimation2 = list2.get(i4);
                if (recoverAnimation2.mEnded && !recoverAnimation2.mIsPendingCleanup) {
                    RecoverAnimation remove = list2.remove(i4);
                } else if (!recoverAnimation2.mEnded) {
                    z = true;
                }
            }
            if (z) {
                recyclerView2.invalidate();
            }
        }

        public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            sUICallback.clearView(viewHolder.itemView);
        }

        public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            sUICallback.onDraw(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        public void onChildDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
            sUICallback.onDrawOver(canvas, recyclerView, viewHolder.itemView, f, f2, i, z);
        }

        public long getAnimationDuration(RecyclerView recyclerView, int i, float f, float f2) {
            int i2 = i;
            RecyclerView.ItemAnimator itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator == null) {
                return i2 == 8 ? 200 : 250;
            }
            return i2 == 8 ? itemAnimator.getMoveDuration() : itemAnimator.getRemoveDuration();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public int interpolateOutOfBoundsScroll(RecyclerView recyclerView, int i, int i2, int i3, long j) {
            float f;
            int i4 = i2;
            long j2 = j;
            int signum = (int) (((float) (((int) Math.signum((float) i4)) * getMaxDragScroll(recyclerView))) * sDragViewScrollCapInterpolator.getInterpolation(Math.min(1.0f, (1.0f * ((float) Math.abs(i4))) / ((float) i))));
            if (j2 > DRAG_SCROLL_ACCELERATION_LIMIT_TIME_MS) {
                f = 1.0f;
            } else {
                f = ((float) j2) / 2000.0f;
            }
            int interpolation = (int) (((float) signum) * sDragScrollInterpolator.getInterpolation(f));
            if (interpolation != 0) {
                return interpolation;
            }
            return i4 > 0 ? 1 : -1;
        }
    }

    public static abstract class SimpleCallback extends Callback {
        private int mDefaultDragDirs;
        private int mDefaultSwipeDirs;

        public SimpleCallback(int i, int i2) {
            this.mDefaultSwipeDirs = i2;
            this.mDefaultDragDirs = i;
        }

        public void setDefaultSwipeDirs(int i) {
            this.mDefaultSwipeDirs = i;
        }

        public void setDefaultDragDirs(int i) {
            this.mDefaultDragDirs = i;
        }

        public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.mDefaultSwipeDirs;
        }

        public int getDragDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            return this.mDefaultDragDirs;
        }

        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
            RecyclerView recyclerView2 = recyclerView;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            return makeMovementFlags(getDragDirs(recyclerView2, viewHolder2), getSwipeDirs(recyclerView2, viewHolder2));
        }
    }

    private class ItemTouchHelperGestureListener extends GestureDetector.SimpleOnGestureListener {
        private ItemTouchHelperGestureListener() {
        }

        public boolean onDown(MotionEvent motionEvent) {
            return true;
        }

        public void onLongPress(MotionEvent motionEvent) {
            RecyclerView.ViewHolder childViewHolder;
            MotionEvent motionEvent2 = motionEvent;
            View access$2400 = ItemTouchHelper.this.findChildView(motionEvent2);
            if (access$2400 != null && (childViewHolder = ItemTouchHelper.this.mRecyclerView.getChildViewHolder(access$2400)) != null && ItemTouchHelper.this.mCallback.hasDragFlag(ItemTouchHelper.this.mRecyclerView, childViewHolder) && MotionEventCompat.getPointerId(motionEvent2, 0) == ItemTouchHelper.this.mActivePointerId) {
                int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, ItemTouchHelper.this.mActivePointerId);
                float x = MotionEventCompat.getX(motionEvent2, findPointerIndex);
                float y = MotionEventCompat.getY(motionEvent2, findPointerIndex);
                ItemTouchHelper.this.mInitialTouchX = x;
                ItemTouchHelper.this.mInitialTouchY = y;
                ItemTouchHelper itemTouchHelper = ItemTouchHelper.this;
                ItemTouchHelper.this.mDy = 0.0f;
                itemTouchHelper.mDx = 0.0f;
                if (ItemTouchHelper.this.mCallback.isLongPressDragEnabled()) {
                    ItemTouchHelper.this.select(childViewHolder, 2);
                }
            }
        }
    }

    private class RecoverAnimation implements AnimatorListenerCompat {
        final int mActionState;
        /* access modifiers changed from: private */
        public final int mAnimationType;
        /* access modifiers changed from: private */
        public boolean mEnded = false;
        private float mFraction;
        public boolean mIsPendingCleanup;
        boolean mOverridden = false;
        final float mStartDx;
        final float mStartDy;
        final float mTargetX;
        final float mTargetY;
        private final ValueAnimatorCompat mValueAnimator;
        final RecyclerView.ViewHolder mViewHolder;
        float mX;
        float mY;
        final /* synthetic */ ItemTouchHelper this$0;

        public RecoverAnimation(ItemTouchHelper itemTouchHelper, RecyclerView.ViewHolder viewHolder, int i, int i2, float f, float f2, float f3, float f4) {
            AnimatorUpdateListenerCompat animatorUpdateListenerCompat;
            ItemTouchHelper itemTouchHelper2 = itemTouchHelper;
            RecyclerView.ViewHolder viewHolder2 = viewHolder;
            this.this$0 = itemTouchHelper2;
            this.mActionState = i2;
            this.mAnimationType = i;
            this.mViewHolder = viewHolder2;
            this.mStartDx = f;
            this.mStartDy = f2;
            this.mTargetX = f3;
            this.mTargetY = f4;
            this.mValueAnimator = AnimatorCompatHelper.emptyValueAnimator();
            final ItemTouchHelper itemTouchHelper3 = itemTouchHelper2;
            new AnimatorUpdateListenerCompat() {
                public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                    RecoverAnimation.this.setFraction(valueAnimatorCompat.getAnimatedFraction());
                }
            };
            this.mValueAnimator.addUpdateListener(animatorUpdateListenerCompat);
            this.mValueAnimator.setTarget(viewHolder2.itemView);
            this.mValueAnimator.addListener(this);
            setFraction(0.0f);
        }

        public void setDuration(long j) {
            this.mValueAnimator.setDuration(j);
        }

        public void start() {
            this.mViewHolder.setIsRecyclable(false);
            this.mValueAnimator.start();
        }

        public void cancel() {
            this.mValueAnimator.cancel();
        }

        public void setFraction(float f) {
            this.mFraction = f;
        }

        public void update() {
            if (this.mStartDx == this.mTargetX) {
                this.mX = ViewCompat.getTranslationX(this.mViewHolder.itemView);
            } else {
                this.mX = this.mStartDx + (this.mFraction * (this.mTargetX - this.mStartDx));
            }
            if (this.mStartDy == this.mTargetY) {
                this.mY = ViewCompat.getTranslationY(this.mViewHolder.itemView);
            } else {
                this.mY = this.mStartDy + (this.mFraction * (this.mTargetY - this.mStartDy));
            }
        }

        public void onAnimationStart(ValueAnimatorCompat valueAnimatorCompat) {
        }

        public void onAnimationEnd(ValueAnimatorCompat valueAnimatorCompat) {
            if (!this.mEnded) {
                this.mViewHolder.setIsRecyclable(true);
            }
            this.mEnded = true;
        }

        public void onAnimationCancel(ValueAnimatorCompat valueAnimatorCompat) {
            setFraction(1.0f);
        }

        public void onAnimationRepeat(ValueAnimatorCompat valueAnimatorCompat) {
        }
    }
}
