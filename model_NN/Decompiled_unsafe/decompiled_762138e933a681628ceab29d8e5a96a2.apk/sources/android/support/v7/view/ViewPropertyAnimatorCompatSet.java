package android.support.v7.view;

import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.view.View;
import android.view.animation.Interpolator;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewPropertyAnimatorCompatSet {
    /* access modifiers changed from: private */
    public final ArrayList<ViewPropertyAnimatorCompat> mAnimators;
    private long mDuration = -1;
    private Interpolator mInterpolator;
    private boolean mIsStarted;
    /* access modifiers changed from: private */
    public ViewPropertyAnimatorListener mListener;
    private final ViewPropertyAnimatorListenerAdapter mProxyListener;

    public ViewPropertyAnimatorCompatSet() {
        ViewPropertyAnimatorListenerAdapter viewPropertyAnimatorListenerAdapter;
        ArrayList<ViewPropertyAnimatorCompat> arrayList;
        new ViewPropertyAnimatorListenerAdapter() {
            private int mProxyEndCount = 0;
            private boolean mProxyStarted = false;

            public void onAnimationStart(View view) {
                if (!this.mProxyStarted) {
                    this.mProxyStarted = true;
                    if (ViewPropertyAnimatorCompatSet.this.mListener != null) {
                        ViewPropertyAnimatorCompatSet.this.mListener.onAnimationStart(null);
                    }
                }
            }

            /* access modifiers changed from: package-private */
            public void onEnd() {
                this.mProxyEndCount = 0;
                this.mProxyStarted = false;
                ViewPropertyAnimatorCompatSet.this.onAnimationsEnded();
            }

            public void onAnimationEnd(View view) {
                int i = this.mProxyEndCount + 1;
                int i2 = i;
                this.mProxyEndCount = i;
                if (i2 == ViewPropertyAnimatorCompatSet.this.mAnimators.size()) {
                    if (ViewPropertyAnimatorCompatSet.this.mListener != null) {
                        ViewPropertyAnimatorCompatSet.this.mListener.onAnimationEnd(null);
                    }
                    onEnd();
                }
            }
        };
        this.mProxyListener = viewPropertyAnimatorListenerAdapter;
        new ArrayList<>();
        this.mAnimators = arrayList;
    }

    public ViewPropertyAnimatorCompatSet play(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = viewPropertyAnimatorCompat;
        if (!this.mIsStarted) {
            boolean add = this.mAnimators.add(viewPropertyAnimatorCompat2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet playSequentially(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat3 = viewPropertyAnimatorCompat;
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat4 = viewPropertyAnimatorCompat2;
        boolean add = this.mAnimators.add(viewPropertyAnimatorCompat3);
        ViewPropertyAnimatorCompat startDelay = viewPropertyAnimatorCompat4.setStartDelay(viewPropertyAnimatorCompat3.getDuration());
        boolean add2 = this.mAnimators.add(viewPropertyAnimatorCompat4);
        return this;
    }

    public void start() {
        if (!this.mIsStarted) {
            Iterator<ViewPropertyAnimatorCompat> it = this.mAnimators.iterator();
            while (it.hasNext()) {
                ViewPropertyAnimatorCompat next = it.next();
                if (this.mDuration >= 0) {
                    ViewPropertyAnimatorCompat duration = next.setDuration(this.mDuration);
                }
                if (this.mInterpolator != null) {
                    ViewPropertyAnimatorCompat interpolator = next.setInterpolator(this.mInterpolator);
                }
                if (this.mListener != null) {
                    ViewPropertyAnimatorCompat listener = next.setListener(this.mProxyListener);
                }
                next.start();
            }
            this.mIsStarted = true;
        }
    }

    /* access modifiers changed from: private */
    public void onAnimationsEnded() {
        this.mIsStarted = false;
    }

    public void cancel() {
        if (this.mIsStarted) {
            Iterator<ViewPropertyAnimatorCompat> it = this.mAnimators.iterator();
            while (it.hasNext()) {
                it.next().cancel();
            }
            this.mIsStarted = false;
        }
    }

    public ViewPropertyAnimatorCompatSet setDuration(long j) {
        long j2 = j;
        if (!this.mIsStarted) {
            this.mDuration = j2;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet setInterpolator(Interpolator interpolator) {
        Interpolator interpolator2 = interpolator;
        if (!this.mIsStarted) {
            this.mInterpolator = interpolator2;
        }
        return this;
    }

    public ViewPropertyAnimatorCompatSet setListener(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2 = viewPropertyAnimatorListener;
        if (!this.mIsStarted) {
            this.mListener = viewPropertyAnimatorListener2;
        }
        return this;
    }
}
