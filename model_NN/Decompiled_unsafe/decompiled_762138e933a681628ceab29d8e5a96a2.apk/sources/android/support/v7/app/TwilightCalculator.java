package android.support.v7.app;

class TwilightCalculator {
    private static final float ALTIDUTE_CORRECTION_CIVIL_TWILIGHT = -0.10471976f;
    private static final float C1 = 0.0334196f;
    private static final float C2 = 3.49066E-4f;
    private static final float C3 = 5.236E-6f;
    public static final int DAY = 0;
    private static final float DEGREES_TO_RADIANS = 0.017453292f;
    private static final float J0 = 9.0E-4f;
    public static final int NIGHT = 1;
    private static final float OBLIQUITY = 0.4092797f;
    private static final long UTC_2000 = 946728000000L;
    private static TwilightCalculator sInstance;
    public int state;
    public long sunrise;
    public long sunset;

    TwilightCalculator() {
    }

    static TwilightCalculator getInstance() {
        TwilightCalculator twilightCalculator;
        if (sInstance == null) {
            new TwilightCalculator();
            sInstance = twilightCalculator;
        }
        return sInstance;
    }

    public void calculateTwilight(long j, double d, double d2) {
        long j2 = j;
        float f = ((float) (j2 - UTC_2000)) / 8.64E7f;
        float f2 = 6.24006f + (f * 0.01720197f);
        double sin = ((double) f2) + (0.03341960161924362d * Math.sin((double) f2)) + (3.4906598739326E-4d * Math.sin((double) (2.0f * f2))) + (5.236000106378924E-6d * Math.sin((double) (3.0f * f2))) + 1.796593063d + 3.141592653589793d;
        double d3 = (-d2) / 360.0d;
        double round = ((double) (((float) Math.round(((double) (f - J0)) - d3)) + J0)) + d3 + (0.0053d * Math.sin((double) f2)) + (-0.0069d * Math.sin(2.0d * sin));
        double asin = Math.asin(Math.sin(sin) * Math.sin(0.4092797040939331d));
        double d4 = d * 0.01745329238474369d;
        double sin2 = (Math.sin(-0.10471975803375244d) - (Math.sin(d4) * Math.sin(asin))) / (Math.cos(d4) * Math.cos(asin));
        if (sin2 >= 1.0d) {
            this.state = 1;
            this.sunset = -1;
            this.sunrise = -1;
        } else if (sin2 <= -1.0d) {
            this.state = 0;
            this.sunset = -1;
            this.sunrise = -1;
        } else {
            float acos = (float) (Math.acos(sin2) / 6.283185307179586d);
            this.sunset = Math.round((round + ((double) acos)) * 8.64E7d) + UTC_2000;
            this.sunrise = Math.round((round - ((double) acos)) * 8.64E7d) + UTC_2000;
            if (this.sunrise >= j2 || this.sunset <= j2) {
                this.state = 1;
            } else {
                this.state = 0;
            }
        }
    }
}
