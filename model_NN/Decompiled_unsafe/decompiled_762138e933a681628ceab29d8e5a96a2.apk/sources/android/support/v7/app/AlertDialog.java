package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AlertController;
import android.support.v7.appcompat.R;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AlertDialog extends AppCompatDialog implements DialogInterface {
    static final int LAYOUT_HINT_NONE = 0;
    static final int LAYOUT_HINT_SIDE = 1;
    /* access modifiers changed from: private */
    public AlertController mAlert;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.app.AlertDialog.<init>(android.content.Context, boolean, android.content.DialogInterface$OnCancelListener):void
      android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected AlertDialog(android.content.Context r7) {
        /*
            r6 = this;
            r0 = r6
            r1 = r7
            r2 = r0
            r3 = r1
            r4 = r1
            r5 = 0
            int r4 = resolveDialogTheme(r4, r5)
            r5 = 1
            r2.<init>(r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertDialog.<init>(android.content.Context):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.app.AlertDialog.<init>(android.content.Context, boolean, android.content.DialogInterface$OnCancelListener):void
      android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void */
    protected AlertDialog(Context context, int i) {
        this(context, i, true);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    AlertDialog(android.content.Context r12, int r13, boolean r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r0
            r5 = r1
            r6 = r1
            r7 = r2
            int r6 = resolveDialogTheme(r6, r7)
            r4.<init>(r5, r6)
            r4 = r0
            android.support.v7.app.AlertController r5 = new android.support.v7.app.AlertController
            r10 = r5
            r5 = r10
            r6 = r10
            r7 = r0
            android.content.Context r7 = r7.getContext()
            r8 = r0
            r9 = r0
            android.view.Window r9 = r9.getWindow()
            r6.<init>(r7, r8, r9)
            r4.mAlert = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected AlertDialog(android.content.Context r12, boolean r13, android.content.DialogInterface.OnCancelListener r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r0
            r5 = r1
            r6 = r1
            r7 = 0
            int r6 = resolveDialogTheme(r6, r7)
            r4.<init>(r5, r6)
            r4 = r0
            r5 = r2
            r4.setCancelable(r5)
            r4 = r0
            r5 = r3
            r4.setOnCancelListener(r5)
            r4 = r0
            android.support.v7.app.AlertController r5 = new android.support.v7.app.AlertController
            r10 = r5
            r5 = r10
            r6 = r10
            r7 = r1
            r8 = r0
            r9 = r0
            android.view.Window r9 = r9.getWindow()
            r6.<init>(r7, r8, r9)
            r4.mAlert = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertDialog.<init>(android.content.Context, boolean, android.content.DialogInterface$OnCancelListener):void");
    }

    static int resolveDialogTheme(Context context, int i) {
        TypedValue typedValue;
        Context context2 = context;
        int i2 = i;
        if (i2 >= 16777216) {
            return i2;
        }
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        boolean resolveAttribute = context2.getTheme().resolveAttribute(R.attr.alertDialogTheme, typedValue2, true);
        return typedValue2.resourceId;
    }

    public Button getButton(int i) {
        return this.mAlert.getButton(i);
    }

    public ListView getListView() {
        return this.mAlert.getListView();
    }

    public void setTitle(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        super.setTitle(charSequence2);
        this.mAlert.setTitle(charSequence2);
    }

    public void setCustomTitle(View view) {
        this.mAlert.setCustomTitle(view);
    }

    public void setMessage(CharSequence charSequence) {
        this.mAlert.setMessage(charSequence);
    }

    public void setView(View view) {
        this.mAlert.setView(view);
    }

    public void setView(View view, int i, int i2, int i3, int i4) {
        this.mAlert.setView(view, i, i2, i3, i4);
    }

    /* access modifiers changed from: package-private */
    public void setButtonPanelLayoutHint(int i) {
        this.mAlert.setButtonPanelLayoutHint(i);
    }

    public void setButton(int i, CharSequence charSequence, Message message) {
        this.mAlert.setButton(i, charSequence, null, message);
    }

    public void setButton(int i, CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        this.mAlert.setButton(i, charSequence, onClickListener, null);
    }

    public void setIcon(int i) {
        this.mAlert.setIcon(i);
    }

    public void setIcon(Drawable drawable) {
        this.mAlert.setIcon(drawable);
    }

    public void setIconAttribute(int i) {
        TypedValue typedValue;
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        boolean resolveAttribute = getContext().getTheme().resolveAttribute(i, typedValue2, true);
        this.mAlert.setIcon(typedValue2.resourceId);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.mAlert.installContent();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (this.mAlert.onKeyDown(i2, keyEvent2)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent2);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (this.mAlert.onKeyUp(i2, keyEvent2)) {
            return true;
        }
        return super.onKeyUp(i2, keyEvent2);
    }

    public static class Builder {
        private final AlertController.AlertParams P;
        private int mTheme;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Builder(android.content.Context r7) {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r0
                r3 = r1
                r4 = r1
                r5 = 0
                int r4 = android.support.v7.app.AlertDialog.resolveDialogTheme(r4, r5)
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.app.AlertDialog.Builder.<init>(android.content.Context):void");
        }

        public Builder(Context context, int i) {
            AlertController.AlertParams alertParams;
            Context context2;
            Context context3 = context;
            int i2 = i;
            new ContextThemeWrapper(context3, AlertDialog.resolveDialogTheme(context3, i2));
            new AlertController.AlertParams(context2);
            this.P = alertParams;
            this.mTheme = i2;
        }

        public Context getContext() {
            return this.P.mContext;
        }

        public Builder setTitle(int i) {
            this.P.mTitle = this.P.mContext.getText(i);
            return this;
        }

        public Builder setTitle(CharSequence charSequence) {
            this.P.mTitle = charSequence;
            return this;
        }

        public Builder setCustomTitle(View view) {
            this.P.mCustomTitleView = view;
            return this;
        }

        public Builder setMessage(int i) {
            this.P.mMessage = this.P.mContext.getText(i);
            return this;
        }

        public Builder setMessage(CharSequence charSequence) {
            this.P.mMessage = charSequence;
            return this;
        }

        public Builder setIcon(int i) {
            this.P.mIconId = i;
            return this;
        }

        public Builder setIcon(Drawable drawable) {
            this.P.mIcon = drawable;
            return this;
        }

        public Builder setIconAttribute(int i) {
            TypedValue typedValue;
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            boolean resolveAttribute = this.P.mContext.getTheme().resolveAttribute(i, typedValue2, true);
            this.P.mIconId = typedValue2.resourceId;
            return this;
        }

        public Builder setPositiveButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mPositiveButtonText = this.P.mContext.getText(i);
            this.P.mPositiveButtonListener = onClickListener;
            return this;
        }

        public Builder setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.P.mPositiveButtonText = charSequence;
            this.P.mPositiveButtonListener = onClickListener;
            return this;
        }

        public Builder setNegativeButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mNegativeButtonText = this.P.mContext.getText(i);
            this.P.mNegativeButtonListener = onClickListener;
            return this;
        }

        public Builder setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.P.mNegativeButtonText = charSequence;
            this.P.mNegativeButtonListener = onClickListener;
            return this;
        }

        public Builder setNeutralButton(int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mNeutralButtonText = this.P.mContext.getText(i);
            this.P.mNeutralButtonListener = onClickListener;
            return this;
        }

        public Builder setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
            this.P.mNeutralButtonText = charSequence;
            this.P.mNeutralButtonListener = onClickListener;
            return this;
        }

        public Builder setCancelable(boolean z) {
            this.P.mCancelable = z;
            return this;
        }

        public Builder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
            this.P.mOnCancelListener = onCancelListener;
            return this;
        }

        public Builder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.P.mOnDismissListener = onDismissListener;
            return this;
        }

        public Builder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
            this.P.mOnKeyListener = onKeyListener;
            return this;
        }

        public Builder setItems(int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mItems = this.P.mContext.getResources().getTextArray(i);
            this.P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setItems(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
            this.P.mItems = charSequenceArr;
            this.P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setAdapter(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.P.mAdapter = listAdapter;
            this.P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setCursor(Cursor cursor, DialogInterface.OnClickListener onClickListener, String str) {
            this.P.mCursor = cursor;
            this.P.mLabelColumn = str;
            this.P.mOnClickListener = onClickListener;
            return this;
        }

        public Builder setMultiChoiceItems(int i, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.P.mItems = this.P.mContext.getResources().getTextArray(i);
            this.P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.P.mCheckedItems = zArr;
            this.P.mIsMultiChoice = true;
            return this;
        }

        public Builder setMultiChoiceItems(CharSequence[] charSequenceArr, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.P.mItems = charSequenceArr;
            this.P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.P.mCheckedItems = zArr;
            this.P.mIsMultiChoice = true;
            return this;
        }

        public Builder setMultiChoiceItems(Cursor cursor, String str, String str2, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
            this.P.mCursor = cursor;
            this.P.mOnCheckboxClickListener = onMultiChoiceClickListener;
            this.P.mIsCheckedColumn = str;
            this.P.mLabelColumn = str2;
            this.P.mIsMultiChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(int i, int i2, DialogInterface.OnClickListener onClickListener) {
            this.P.mItems = this.P.mContext.getResources().getTextArray(i);
            this.P.mOnClickListener = onClickListener;
            this.P.mCheckedItem = i2;
            this.P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(Cursor cursor, int i, String str, DialogInterface.OnClickListener onClickListener) {
            this.P.mCursor = cursor;
            this.P.mOnClickListener = onClickListener;
            this.P.mCheckedItem = i;
            this.P.mLabelColumn = str;
            this.P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(CharSequence[] charSequenceArr, int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mItems = charSequenceArr;
            this.P.mOnClickListener = onClickListener;
            this.P.mCheckedItem = i;
            this.P.mIsSingleChoice = true;
            return this;
        }

        public Builder setSingleChoiceItems(ListAdapter listAdapter, int i, DialogInterface.OnClickListener onClickListener) {
            this.P.mAdapter = listAdapter;
            this.P.mOnClickListener = onClickListener;
            this.P.mCheckedItem = i;
            this.P.mIsSingleChoice = true;
            return this;
        }

        public Builder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
            this.P.mOnItemSelectedListener = onItemSelectedListener;
            return this;
        }

        public Builder setView(int i) {
            this.P.mView = null;
            this.P.mViewLayoutResId = i;
            this.P.mViewSpacingSpecified = false;
            return this;
        }

        public Builder setView(View view) {
            this.P.mView = view;
            this.P.mViewLayoutResId = 0;
            this.P.mViewSpacingSpecified = false;
            return this;
        }

        public Builder setView(View view, int i, int i2, int i3, int i4) {
            this.P.mView = view;
            this.P.mViewLayoutResId = 0;
            this.P.mViewSpacingSpecified = true;
            this.P.mViewSpacingLeft = i;
            this.P.mViewSpacingTop = i2;
            this.P.mViewSpacingRight = i3;
            this.P.mViewSpacingBottom = i4;
            return this;
        }

        public Builder setInverseBackgroundForced(boolean z) {
            this.P.mForceInverseBackground = z;
            return this;
        }

        public Builder setRecycleOnMeasureEnabled(boolean z) {
            this.P.mRecycleOnMeasure = z;
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void
         arg types: [android.content.Context, int, int]
         candidates:
          android.support.v7.app.AlertDialog.<init>(android.content.Context, boolean, android.content.DialogInterface$OnCancelListener):void
          android.support.v7.app.AlertDialog.<init>(android.content.Context, int, boolean):void */
        public AlertDialog create() {
            AlertDialog alertDialog;
            new AlertDialog(this.P.mContext, this.mTheme, false);
            AlertDialog alertDialog2 = alertDialog;
            this.P.apply(alertDialog2.mAlert);
            alertDialog2.setCancelable(this.P.mCancelable);
            if (this.P.mCancelable) {
                alertDialog2.setCanceledOnTouchOutside(true);
            }
            alertDialog2.setOnCancelListener(this.P.mOnCancelListener);
            alertDialog2.setOnDismissListener(this.P.mOnDismissListener);
            if (this.P.mOnKeyListener != null) {
                alertDialog2.setOnKeyListener(this.P.mOnKeyListener);
            }
            return alertDialog2;
        }

        public AlertDialog show() {
            AlertDialog create = create();
            create.show();
            return create;
        }
    }
}
