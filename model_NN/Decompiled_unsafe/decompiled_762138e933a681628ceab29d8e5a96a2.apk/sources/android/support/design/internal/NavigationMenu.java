package android.support.design.internal;

import android.content.Context;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.SubMenuBuilder;
import android.view.SubMenu;

public class NavigationMenu extends MenuBuilder {
    public NavigationMenu(Context context) {
        super(context);
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        SubMenuBuilder subMenuBuilder;
        MenuItemImpl menuItemImpl = (MenuItemImpl) addInternal(i, i2, i3, charSequence);
        new NavigationSubMenu(getContext(), this, menuItemImpl);
        SubMenuBuilder subMenuBuilder2 = subMenuBuilder;
        menuItemImpl.setSubMenu(subMenuBuilder2);
        return subMenuBuilder2;
    }
}
