package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.design.R;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButtonImpl;
import android.support.design.widget.Snackbar;
import android.support.design.widget.ValueAnimatorCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import java.util.List;

@CoordinatorLayout.DefaultBehavior(Behavior.class)
public class FloatingActionButton extends ImageButton {
    private static final String LOG_TAG = "FloatingActionButton";
    private static final int SIZE_MINI = 1;
    private static final int SIZE_NORMAL = 0;
    private ColorStateList mBackgroundTint;
    private PorterDuff.Mode mBackgroundTintMode;
    private int mBorderWidth;
    /* access modifiers changed from: private */
    public int mContentPadding;
    private final FloatingActionButtonImpl mImpl;
    private int mRippleColor;
    /* access modifiers changed from: private */
    public final Rect mShadowPadding;
    private int mSize;

    public static abstract class OnVisibilityChangedListener {
        public void onShown(FloatingActionButton floatingActionButton) {
        }

        public void onHidden(FloatingActionButton floatingActionButton) {
        }
    }

    public FloatingActionButton(Context context) {
        this(context, null);
    }

    public FloatingActionButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FloatingActionButton(android.content.Context r17, android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r10 = r0
            r11 = r1
            r12 = r2
            r13 = r3
            r10.<init>(r11, r12, r13)
            r10 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r10)
            r10 = r0
            android.graphics.Rect r11 = new android.graphics.Rect
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mShadowPadding = r11
            r10 = r1
            r11 = r2
            int[] r12 = android.support.design.R.styleable.FloatingActionButton
            r13 = r3
            int r14 = android.support.design.R.style.Widget_Design_FloatingActionButton
            android.content.res.TypedArray r10 = r10.obtainStyledAttributes(r11, r12, r13, r14)
            r4 = r10
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.FloatingActionButton_backgroundTint
            android.content.res.ColorStateList r11 = r11.getColorStateList(r12)
            r10.mBackgroundTint = r11
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.FloatingActionButton_backgroundTintMode
            r13 = -1
            int r11 = r11.getInt(r12, r13)
            r12 = 0
            android.graphics.PorterDuff$Mode r11 = parseTintMode(r11, r12)
            r10.mBackgroundTintMode = r11
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.FloatingActionButton_rippleColor
            r13 = 0
            int r11 = r11.getColor(r12, r13)
            r10.mRippleColor = r11
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.FloatingActionButton_fabSize
            r13 = 0
            int r11 = r11.getInt(r12, r13)
            r10.mSize = r11
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.FloatingActionButton_borderWidth
            r13 = 0
            int r11 = r11.getDimensionPixelSize(r12, r13)
            r10.mBorderWidth = r11
            r10 = r4
            int r11 = android.support.design.R.styleable.FloatingActionButton_elevation
            r12 = 0
            float r10 = r10.getDimension(r11, r12)
            r5 = r10
            r10 = r4
            int r11 = android.support.design.R.styleable.FloatingActionButton_pressedTranslationZ
            r12 = 0
            float r10 = r10.getDimension(r11, r12)
            r6 = r10
            r10 = r4
            r10.recycle()
            android.support.design.widget.FloatingActionButton$1 r10 = new android.support.design.widget.FloatingActionButton$1
            r15 = r10
            r10 = r15
            r11 = r15
            r12 = r0
            r11.<init>()
            r7 = r10
            int r10 = android.os.Build.VERSION.SDK_INT
            r8 = r10
            r10 = r8
            r11 = 21
            if (r10 < r11) goto L_0x00d5
            r10 = r0
            android.support.design.widget.FloatingActionButtonLollipop r11 = new android.support.design.widget.FloatingActionButtonLollipop
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r14 = r7
            r12.<init>(r13, r14)
            r10.mImpl = r11
        L_0x009a:
            r10 = r0
            android.content.res.Resources r10 = r10.getResources()
            int r11 = android.support.design.R.dimen.design_fab_content_size
            float r10 = r10.getDimension(r11)
            int r10 = (int) r10
            r9 = r10
            r10 = r0
            r11 = r0
            int r11 = r11.getSizeDimension()
            r12 = r9
            int r11 = r11 - r12
            r12 = 2
            int r11 = r11 / 2
            r10.mContentPadding = r11
            r10 = r0
            android.support.design.widget.FloatingActionButtonImpl r10 = r10.mImpl
            r11 = r0
            android.content.res.ColorStateList r11 = r11.mBackgroundTint
            r12 = r0
            android.graphics.PorterDuff$Mode r12 = r12.mBackgroundTintMode
            r13 = r0
            int r13 = r13.mRippleColor
            r14 = r0
            int r14 = r14.mBorderWidth
            r10.setBackgroundDrawable(r11, r12, r13, r14)
            r10 = r0
            android.support.design.widget.FloatingActionButtonImpl r10 = r10.mImpl
            r11 = r5
            r10.setElevation(r11)
            r10 = r0
            android.support.design.widget.FloatingActionButtonImpl r10 = r10.mImpl
            r11 = r6
            r10.setPressedTranslationZ(r11)
            return
        L_0x00d5:
            r10 = r8
            r11 = 12
            if (r10 < r11) goto L_0x00e8
            r10 = r0
            android.support.design.widget.FloatingActionButtonHoneycombMr1 r11 = new android.support.design.widget.FloatingActionButtonHoneycombMr1
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r14 = r7
            r12.<init>(r13, r14)
            r10.mImpl = r11
            goto L_0x009a
        L_0x00e8:
            r10 = r0
            android.support.design.widget.FloatingActionButtonEclairMr1 r11 = new android.support.design.widget.FloatingActionButtonEclairMr1
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r14 = r7
            r12.<init>(r13, r14)
            r10.mImpl = r11
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.FloatingActionButton.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int sizeDimension = getSizeDimension();
        int min = Math.min(resolveAdjustedSize(sizeDimension, i), resolveAdjustedSize(sizeDimension, i2));
        setMeasuredDimension(min + this.mShadowPadding.left + this.mShadowPadding.right, min + this.mShadowPadding.top + this.mShadowPadding.bottom);
    }

    public void setRippleColor(@ColorInt int i) {
        int i2 = i;
        if (this.mRippleColor != i2) {
            this.mRippleColor = i2;
            this.mImpl.setRippleColor(i2);
        }
    }

    @Nullable
    public ColorStateList getBackgroundTintList() {
        return this.mBackgroundTint;
    }

    public void setBackgroundTintList(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mBackgroundTint != colorStateList2) {
            this.mBackgroundTint = colorStateList2;
            this.mImpl.setBackgroundTintList(colorStateList2);
        }
    }

    @Nullable
    public PorterDuff.Mode getBackgroundTintMode() {
        return this.mBackgroundTintMode;
    }

    public void setBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        if (this.mBackgroundTintMode != mode2) {
            this.mBackgroundTintMode = mode2;
            this.mImpl.setBackgroundTintMode(mode2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        int i = Log.i(LOG_TAG, "Setting a custom background is not supported.");
    }

    public void setBackgroundResource(int i) {
        int i2 = Log.i(LOG_TAG, "Setting a custom background is not supported.");
    }

    public void setBackgroundColor(int i) {
        int i2 = Log.i(LOG_TAG, "Setting a custom background is not supported.");
    }

    public void show() {
        this.mImpl.show(null);
    }

    public void show(@Nullable OnVisibilityChangedListener onVisibilityChangedListener) {
        this.mImpl.show(wrapOnVisibilityChangedListener(onVisibilityChangedListener));
    }

    public void hide() {
        this.mImpl.hide(null);
    }

    public void hide(@Nullable OnVisibilityChangedListener onVisibilityChangedListener) {
        this.mImpl.hide(wrapOnVisibilityChangedListener(onVisibilityChangedListener));
    }

    @Nullable
    private FloatingActionButtonImpl.InternalVisibilityChangedListener wrapOnVisibilityChangedListener(@Nullable OnVisibilityChangedListener onVisibilityChangedListener) {
        FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener;
        OnVisibilityChangedListener onVisibilityChangedListener2 = onVisibilityChangedListener;
        if (onVisibilityChangedListener2 == null) {
            return null;
        }
        final OnVisibilityChangedListener onVisibilityChangedListener3 = onVisibilityChangedListener2;
        new FloatingActionButtonImpl.InternalVisibilityChangedListener() {
            public void onShown() {
                onVisibilityChangedListener3.onShown(FloatingActionButton.this);
            }

            public void onHidden() {
                onVisibilityChangedListener3.onHidden(FloatingActionButton.this);
            }
        };
        return internalVisibilityChangedListener;
    }

    /* access modifiers changed from: package-private */
    public final int getSizeDimension() {
        switch (this.mSize) {
            case 1:
                return getResources().getDimensionPixelSize(R.dimen.design_fab_size_mini);
            default:
                return getResources().getDimensionPixelSize(R.dimen.design_fab_size_normal);
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mImpl.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mImpl.onDetachedFromWindow();
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        this.mImpl.onDrawableStateChanged(getDrawableState());
    }

    @TargetApi(11)
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.mImpl.jumpDrawableToCurrentState();
    }

    private static int resolveAdjustedSize(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int i5 = i3;
        int mode = View.MeasureSpec.getMode(i4);
        int size = View.MeasureSpec.getSize(i4);
        switch (mode) {
            case Integer.MIN_VALUE:
                i5 = Math.min(i3, size);
                break;
            case 0:
                i5 = i3;
                break;
            case 1073741824:
                i5 = size;
                break;
        }
        return i5;
    }

    static PorterDuff.Mode parseTintMode(int i, PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        switch (i) {
            case 3:
                return PorterDuff.Mode.SRC_OVER;
            case 5:
                return PorterDuff.Mode.SRC_IN;
            case 9:
                return PorterDuff.Mode.SRC_ATOP;
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            default:
                return mode2;
        }
    }

    public static class Behavior extends CoordinatorLayout.Behavior<FloatingActionButton> {
        private static final boolean SNACKBAR_BEHAVIOR_ENABLED = (Build.VERSION.SDK_INT >= 11);
        private float mFabTranslationY;
        private ValueAnimatorCompat mFabTranslationYAnimator;
        private Rect mTmpRect;

        public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            return SNACKBAR_BEHAVIOR_ENABLED && (view instanceof Snackbar.SnackbarLayout);
        }

        public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            View view2 = view;
            if (view2 instanceof Snackbar.SnackbarLayout) {
                updateFabTranslationForSnackbar(coordinatorLayout2, floatingActionButton2, view2);
            } else if (view2 instanceof AppBarLayout) {
                boolean updateFabVisibility = updateFabVisibility(coordinatorLayout2, (AppBarLayout) view2, floatingActionButton2);
            }
            return false;
        }

        private boolean updateFabVisibility(CoordinatorLayout coordinatorLayout, AppBarLayout appBarLayout, FloatingActionButton floatingActionButton) {
            Rect rect;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            AppBarLayout appBarLayout2 = appBarLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            if (((CoordinatorLayout.LayoutParams) floatingActionButton2.getLayoutParams()).getAnchorId() != appBarLayout2.getId()) {
                return false;
            }
            if (this.mTmpRect == null) {
                new Rect();
                this.mTmpRect = rect;
            }
            Rect rect2 = this.mTmpRect;
            ViewGroupUtils.getDescendantRect(coordinatorLayout2, appBarLayout2, rect2);
            if (rect2.bottom <= appBarLayout2.getMinimumHeightForVisibleOverlappingContent()) {
                floatingActionButton2.hide();
            } else {
                floatingActionButton2.show();
            }
            return true;
        }

        private void updateFabTranslationForSnackbar(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, View view) {
            ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            if (floatingActionButton2.getVisibility() == 0) {
                float fabTranslationYForSnackbar = getFabTranslationYForSnackbar(coordinatorLayout2, floatingActionButton2);
                if (this.mFabTranslationY != fabTranslationYForSnackbar) {
                    float translationY = ViewCompat.getTranslationY(floatingActionButton2);
                    if (this.mFabTranslationYAnimator != null && this.mFabTranslationYAnimator.isRunning()) {
                        this.mFabTranslationYAnimator.cancel();
                    }
                    if (Math.abs(translationY - fabTranslationYForSnackbar) > ((float) floatingActionButton2.getHeight()) * 0.667f) {
                        if (this.mFabTranslationYAnimator == null) {
                            this.mFabTranslationYAnimator = ViewUtils.createAnimator();
                            this.mFabTranslationYAnimator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
                            final FloatingActionButton floatingActionButton3 = floatingActionButton2;
                            new ValueAnimatorCompat.AnimatorUpdateListener() {
                                public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                                    ViewCompat.setTranslationY(floatingActionButton3, valueAnimatorCompat.getAnimatedFloatValue());
                                }
                            };
                            this.mFabTranslationYAnimator.setUpdateListener(animatorUpdateListener);
                        }
                        this.mFabTranslationYAnimator.setFloatValues(translationY, fabTranslationYForSnackbar);
                        this.mFabTranslationYAnimator.start();
                    } else {
                        ViewCompat.setTranslationY(floatingActionButton2, fabTranslationYForSnackbar);
                    }
                    this.mFabTranslationY = fabTranslationYForSnackbar;
                }
            }
        }

        private float getFabTranslationYForSnackbar(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            float f = 0.0f;
            List<View> dependencies = coordinatorLayout2.getDependencies(floatingActionButton2);
            int size = dependencies.size();
            for (int i = 0; i < size; i++) {
                View view = dependencies.get(i);
                if ((view instanceof Snackbar.SnackbarLayout) && coordinatorLayout2.doViewsOverlap(floatingActionButton2, view)) {
                    f = Math.min(f, ViewCompat.getTranslationY(view) - ((float) view.getHeight()));
                }
            }
            return f;
        }

        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton, int i) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            int i2 = i;
            List<View> dependencies = coordinatorLayout2.getDependencies(floatingActionButton2);
            int size = dependencies.size();
            for (int i3 = 0; i3 < size; i3++) {
                View view = dependencies.get(i3);
                if ((view instanceof AppBarLayout) && updateFabVisibility(coordinatorLayout2, (AppBarLayout) view, floatingActionButton2)) {
                    break;
                }
            }
            coordinatorLayout2.onLayoutChild(floatingActionButton2, i2);
            offsetIfNeeded(coordinatorLayout2, floatingActionButton2);
            return true;
        }

        private void offsetIfNeeded(CoordinatorLayout coordinatorLayout, FloatingActionButton floatingActionButton) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            FloatingActionButton floatingActionButton2 = floatingActionButton;
            Rect access$000 = floatingActionButton2.mShadowPadding;
            if (access$000 != null && access$000.centerX() > 0 && access$000.centerY() > 0) {
                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) floatingActionButton2.getLayoutParams();
                int i = 0;
                int i2 = 0;
                if (floatingActionButton2.getRight() >= coordinatorLayout2.getWidth() - layoutParams.rightMargin) {
                    i2 = access$000.right;
                } else if (floatingActionButton2.getLeft() <= layoutParams.leftMargin) {
                    i2 = -access$000.left;
                }
                if (floatingActionButton2.getBottom() >= coordinatorLayout2.getBottom() - layoutParams.bottomMargin) {
                    i = access$000.bottom;
                } else if (floatingActionButton2.getTop() <= layoutParams.topMargin) {
                    i = -access$000.top;
                }
                floatingActionButton2.offsetTopAndBottom(i);
                floatingActionButton2.offsetLeftAndRight(i2);
            }
        }
    }
}
