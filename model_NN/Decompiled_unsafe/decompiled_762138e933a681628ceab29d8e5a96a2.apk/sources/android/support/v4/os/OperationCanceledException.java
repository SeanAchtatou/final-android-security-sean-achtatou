package android.support.v4.os;

public class OperationCanceledException extends RuntimeException {
    public OperationCanceledException() {
        this(null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OperationCanceledException(java.lang.String r5) {
        /*
            r4 = this;
            r0 = r4
            r1 = r5
            r2 = r0
            r3 = r1
            if (r3 == 0) goto L_0x000b
            r3 = r1
        L_0x0007:
            r2.<init>(r3)
            return
        L_0x000b:
            java.lang.String r3 = "The operation has been canceled."
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.os.OperationCanceledException.<init>(java.lang.String):void");
    }
}
