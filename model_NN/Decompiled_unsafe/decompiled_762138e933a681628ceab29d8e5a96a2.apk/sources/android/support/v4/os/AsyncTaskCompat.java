package android.support.v4.os;

import android.os.AsyncTask;
import android.os.Build;

public class AsyncTaskCompat {
    public static <Params, Progress, Result> AsyncTask<Params, Progress, Result> executeParallel(AsyncTask<Params, Progress, Result> asyncTask, Params... paramsArr) {
        Throwable th;
        AsyncTask<Params, Progress, Result> asyncTask2 = asyncTask;
        Params[] paramsArr2 = paramsArr;
        if (asyncTask2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("task can not be null");
            throw th2;
        }
        if (Build.VERSION.SDK_INT >= 11) {
            AsyncTaskCompatHoneycomb.executeParallel(asyncTask2, paramsArr2);
        } else {
            AsyncTask<Params, Progress, Result> execute = asyncTask2.execute(paramsArr2);
        }
        return asyncTask2;
    }
}
