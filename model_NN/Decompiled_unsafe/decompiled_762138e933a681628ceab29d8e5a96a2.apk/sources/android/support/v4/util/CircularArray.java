package android.support.v4.util;

public final class CircularArray<E> {
    private int mCapacityBitmask;
    private E[] mElements;
    private int mHead;
    private int mTail;

    private void doubleCapacity() {
        Throwable th;
        int length = this.mElements.length;
        int i = length - this.mHead;
        int i2 = length << 1;
        if (i2 < 0) {
            Throwable th2 = th;
            new RuntimeException("Max array capacity exceeded");
            throw th2;
        }
        E[] eArr = new Object[i2];
        System.arraycopy(this.mElements, this.mHead, eArr, 0, i);
        System.arraycopy(this.mElements, 0, eArr, i, this.mHead);
        this.mElements = (Object[]) eArr;
        this.mHead = 0;
        this.mTail = length;
        this.mCapacityBitmask = i2 - 1;
    }

    public CircularArray() {
        this(8);
    }

    public CircularArray(int i) {
        Throwable th;
        int i2 = i;
        if (i2 <= 0) {
            Throwable th2 = th;
            new IllegalArgumentException("capacity must be positive");
            throw th2;
        }
        int highestOneBit = Integer.bitCount(i2) != 1 ? 1 << (Integer.highestOneBit(i2) + 1) : i2;
        this.mCapacityBitmask = highestOneBit - 1;
        this.mElements = (Object[]) new Object[highestOneBit];
    }

    public void addFirst(E e) {
        this.mHead = (this.mHead - 1) & this.mCapacityBitmask;
        this.mElements[this.mHead] = e;
        if (this.mHead == this.mTail) {
            doubleCapacity();
        }
    }

    public void addLast(E e) {
        this.mElements[this.mTail] = e;
        this.mTail = (this.mTail + 1) & this.mCapacityBitmask;
        if (this.mTail == this.mHead) {
            doubleCapacity();
        }
    }

    public E popFirst() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        E e = this.mElements[this.mHead];
        this.mElements[this.mHead] = null;
        this.mHead = (this.mHead + 1) & this.mCapacityBitmask;
        return e;
    }

    public E popLast() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        int i = (this.mTail - 1) & this.mCapacityBitmask;
        E e = this.mElements[i];
        this.mElements[i] = null;
        this.mTail = i;
        return e;
    }

    public void clear() {
        removeFromStart(size());
    }

    public void removeFromStart(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            int length = this.mElements.length;
            if (i2 < length - this.mHead) {
                length = this.mHead + i2;
            }
            for (int i3 = this.mHead; i3 < length; i3++) {
                this.mElements[i3] = null;
            }
            int i4 = length - this.mHead;
            int i5 = i2 - i4;
            this.mHead = (this.mHead + i4) & this.mCapacityBitmask;
            if (i5 > 0) {
                for (int i6 = 0; i6 < i5; i6++) {
                    this.mElements[i6] = null;
                }
                this.mHead = i5;
            }
        }
    }

    public void removeFromEnd(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            int i3 = 0;
            if (i2 < this.mTail) {
                i3 = this.mTail - i2;
            }
            for (int i4 = i3; i4 < this.mTail; i4++) {
                this.mElements[i4] = null;
            }
            int i5 = this.mTail - i3;
            int i6 = i2 - i5;
            this.mTail = this.mTail - i5;
            if (i6 > 0) {
                this.mTail = this.mElements.length;
                int i7 = this.mTail - i6;
                for (int i8 = i7; i8 < this.mTail; i8++) {
                    this.mElements[i8] = null;
                }
                this.mTail = i7;
            }
        }
    }

    public E getFirst() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[this.mHead];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public E getLast() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[(this.mTail - 1) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public E get(int i) {
        Throwable th;
        int i2 = i;
        if (i2 >= 0 && i2 < size()) {
            return this.mElements[(this.mHead + i2) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int size() {
        return (this.mTail - this.mHead) & this.mCapacityBitmask;
    }

    public boolean isEmpty() {
        return this.mHead == this.mTail;
    }
}
