package android.support.v4.graphics.drawable;

import android.graphics.drawable.Drawable;

class DrawableCompatHoneycomb {
    DrawableCompatHoneycomb() {
    }

    public static void jumpToCurrentState(Drawable drawable) {
        drawable.jumpToCurrentState();
    }

    public static Drawable wrapForTinting(Drawable drawable) {
        Drawable drawable2;
        Drawable drawable3 = drawable;
        if (drawable3 instanceof DrawableWrapperHoneycomb) {
            return drawable3;
        }
        new DrawableWrapperHoneycomb(drawable3);
        return drawable2;
    }
}
