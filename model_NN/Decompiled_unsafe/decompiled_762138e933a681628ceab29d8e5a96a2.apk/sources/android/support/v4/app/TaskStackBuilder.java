package android.support.v4.app;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class TaskStackBuilder implements Iterable<Intent> {
    private static final TaskStackBuilderImpl IMPL;
    private static final String TAG = "TaskStackBuilder";
    private final ArrayList<Intent> mIntents;
    private final Context mSourceContext;

    public interface SupportParentable {
        Intent getSupportParentActivityIntent();
    }

    interface TaskStackBuilderImpl {
        PendingIntent getPendingIntent(Context context, Intent[] intentArr, int i, int i2, Bundle bundle);
    }

    static class TaskStackBuilderImplBase implements TaskStackBuilderImpl {
        TaskStackBuilderImplBase() {
        }

        public PendingIntent getPendingIntent(Context context, Intent[] intentArr, int i, int i2, Bundle bundle) {
            Intent intent;
            Intent[] intentArr2 = intentArr;
            new Intent(intentArr2[intentArr2.length - 1]);
            Intent intent2 = intent;
            Intent addFlags = intent2.addFlags(268435456);
            return PendingIntent.getActivity(context, i, intent2, i2);
        }
    }

    static class TaskStackBuilderImplHoneycomb implements TaskStackBuilderImpl {
        TaskStackBuilderImplHoneycomb() {
        }

        public PendingIntent getPendingIntent(Context context, Intent[] intentArr, int i, int i2, Bundle bundle) {
            Intent intent;
            Intent[] intentArr2 = intentArr;
            new Intent(intentArr2[0]);
            intentArr2[0] = intent.addFlags(268484608);
            return TaskStackBuilderHoneycomb.getActivitiesPendingIntent(context, i, intentArr2, i2);
        }
    }

    static class TaskStackBuilderImplJellybean implements TaskStackBuilderImpl {
        TaskStackBuilderImplJellybean() {
        }

        public PendingIntent getPendingIntent(Context context, Intent[] intentArr, int i, int i2, Bundle bundle) {
            Intent intent;
            Intent[] intentArr2 = intentArr;
            new Intent(intentArr2[0]);
            intentArr2[0] = intent.addFlags(268484608);
            return TaskStackBuilderJellybean.getActivitiesPendingIntent(context, i, intentArr2, i2, bundle);
        }
    }

    static {
        TaskStackBuilderImpl taskStackBuilderImpl;
        TaskStackBuilderImpl taskStackBuilderImpl2;
        if (Build.VERSION.SDK_INT >= 11) {
            new TaskStackBuilderImplHoneycomb();
            IMPL = taskStackBuilderImpl2;
            return;
        }
        new TaskStackBuilderImplBase();
        IMPL = taskStackBuilderImpl;
    }

    private TaskStackBuilder(Context context) {
        ArrayList<Intent> arrayList;
        new ArrayList<>();
        this.mIntents = arrayList;
        this.mSourceContext = context;
    }

    public static TaskStackBuilder create(Context context) {
        TaskStackBuilder taskStackBuilder;
        new TaskStackBuilder(context);
        return taskStackBuilder;
    }

    public static TaskStackBuilder from(Context context) {
        return create(context);
    }

    public TaskStackBuilder addNextIntent(Intent intent) {
        boolean add = this.mIntents.add(intent);
        return this;
    }

    public TaskStackBuilder addNextIntentWithParentStack(Intent intent) {
        Intent intent2 = intent;
        ComponentName component = intent2.getComponent();
        if (component == null) {
            component = intent2.resolveActivity(this.mSourceContext.getPackageManager());
        }
        if (component != null) {
            TaskStackBuilder addParentStack = addParentStack(component);
        }
        TaskStackBuilder addNextIntent = addNextIntent(intent2);
        return this;
    }

    public TaskStackBuilder addParentStack(Activity activity) {
        Activity activity2 = activity;
        Intent intent = null;
        if (activity2 instanceof SupportParentable) {
            intent = ((SupportParentable) activity2).getSupportParentActivityIntent();
        }
        if (intent == null) {
            intent = NavUtils.getParentActivityIntent(activity2);
        }
        if (intent != null) {
            ComponentName component = intent.getComponent();
            if (component == null) {
                component = intent.resolveActivity(this.mSourceContext.getPackageManager());
            }
            TaskStackBuilder addParentStack = addParentStack(component);
            TaskStackBuilder addNextIntent = addNextIntent(intent);
        }
        return this;
    }

    public TaskStackBuilder addParentStack(Class<?> cls) {
        ComponentName componentName;
        new ComponentName(this.mSourceContext, cls);
        return addParentStack(componentName);
    }

    public TaskStackBuilder addParentStack(ComponentName componentName) {
        Throwable th;
        ComponentName componentName2 = componentName;
        int size = this.mIntents.size();
        try {
            Intent parentActivityIntent = NavUtils.getParentActivityIntent(this.mSourceContext, componentName2);
            while (true) {
                Intent intent = parentActivityIntent;
                if (intent == null) {
                    return this;
                }
                this.mIntents.add(size, intent);
                parentActivityIntent = NavUtils.getParentActivityIntent(this.mSourceContext, intent.getComponent());
            }
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            int e2 = Log.e(TAG, "Bad ComponentName while traversing activity parent metadata");
            Throwable th2 = th;
            new IllegalArgumentException(nameNotFoundException);
            throw th2;
        }
    }

    public int getIntentCount() {
        return this.mIntents.size();
    }

    public Intent getIntent(int i) {
        return editIntentAt(i);
    }

    public Intent editIntentAt(int i) {
        return this.mIntents.get(i);
    }

    public Iterator<Intent> iterator() {
        return this.mIntents.iterator();
    }

    public void startActivities() {
        startActivities(null);
    }

    public void startActivities(Bundle bundle) {
        Intent intent;
        Intent intent2;
        Throwable th;
        Bundle bundle2 = bundle;
        if (this.mIntents.isEmpty()) {
            Throwable th2 = th;
            new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
            throw th2;
        }
        Intent[] intentArr = (Intent[]) this.mIntents.toArray(new Intent[this.mIntents.size()]);
        new Intent(intentArr[0]);
        intentArr[0] = intent.addFlags(268484608);
        if (!ContextCompat.startActivities(this.mSourceContext, intentArr, bundle2)) {
            new Intent(intentArr[intentArr.length - 1]);
            Intent intent3 = intent2;
            Intent addFlags = intent3.addFlags(268435456);
            this.mSourceContext.startActivity(intent3);
        }
    }

    public PendingIntent getPendingIntent(int i, int i2) {
        return getPendingIntent(i, i2, null);
    }

    public PendingIntent getPendingIntent(int i, int i2, Bundle bundle) {
        Intent intent;
        Throwable th;
        int i3 = i;
        int i4 = i2;
        Bundle bundle2 = bundle;
        if (this.mIntents.isEmpty()) {
            Throwable th2 = th;
            new IllegalStateException("No intents added to TaskStackBuilder; cannot getPendingIntent");
            throw th2;
        }
        Intent[] intentArr = (Intent[]) this.mIntents.toArray(new Intent[this.mIntents.size()]);
        new Intent(intentArr[0]);
        intentArr[0] = intent.addFlags(268484608);
        return IMPL.getPendingIntent(this.mSourceContext, intentArr, i3, i4, bundle2);
    }

    public Intent[] getIntents() {
        Intent intent;
        Intent intent2;
        Intent[] intentArr = new Intent[this.mIntents.size()];
        if (intentArr.length == 0) {
            return intentArr;
        }
        new Intent(this.mIntents.get(0));
        intentArr[0] = intent.addFlags(268484608);
        for (int i = 1; i < intentArr.length; i++) {
            new Intent(this.mIntents.get(i));
            intentArr[i] = intent2;
        }
        return intentArr;
    }
}
