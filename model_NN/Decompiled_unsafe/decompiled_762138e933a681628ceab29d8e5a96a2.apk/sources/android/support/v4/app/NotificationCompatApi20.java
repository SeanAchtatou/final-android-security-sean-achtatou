package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompatBase;
import android.support.v4.app.RemoteInputCompatBase;
import android.widget.RemoteViews;
import java.util.ArrayList;

class NotificationCompatApi20 {
    NotificationCompatApi20() {
    }

    public static class Builder implements NotificationBuilderWithBuilderAccessor, NotificationBuilderWithActions {
        private Notification.Builder b;
        private Bundle mExtras;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, ArrayList<String> arrayList, Bundle bundle, String str, boolean z5, String str2) {
            Notification.Builder builder;
            boolean z6;
            boolean z7;
            boolean z8;
            boolean z9;
            Bundle bundle2;
            Notification notification2 = notification;
            CharSequence charSequence5 = charSequence;
            CharSequence charSequence6 = charSequence2;
            CharSequence charSequence7 = charSequence3;
            int i5 = i;
            PendingIntent pendingIntent3 = pendingIntent;
            PendingIntent pendingIntent4 = pendingIntent2;
            Bitmap bitmap2 = bitmap;
            int i6 = i2;
            int i7 = i3;
            boolean z10 = z;
            boolean z11 = z3;
            int i8 = i4;
            CharSequence charSequence8 = charSequence4;
            boolean z12 = z4;
            ArrayList<String> arrayList2 = arrayList;
            Bundle bundle3 = bundle;
            String str3 = str;
            boolean z13 = z5;
            String str4 = str2;
            new Notification.Builder(context);
            Notification.Builder lights = builder.setWhen(notification2.when).setShowWhen(z2).setSmallIcon(notification2.icon, notification2.iconLevel).setContent(notification2.contentView).setTicker(notification2.tickerText, remoteViews).setSound(notification2.sound, notification2.audioStreamType).setVibrate(notification2.vibrate).setLights(notification2.ledARGB, notification2.ledOnMS, notification2.ledOffMS);
            if ((notification2.flags & 2) != 0) {
                z6 = true;
            } else {
                z6 = false;
            }
            Notification.Builder ongoing = lights.setOngoing(z6);
            if ((notification2.flags & 8) != 0) {
                z7 = true;
            } else {
                z7 = false;
            }
            Notification.Builder onlyAlertOnce = ongoing.setOnlyAlertOnce(z7);
            if ((notification2.flags & 16) != 0) {
                z8 = true;
            } else {
                z8 = false;
            }
            Notification.Builder deleteIntent = onlyAlertOnce.setAutoCancel(z8).setDefaults(notification2.defaults).setContentTitle(charSequence5).setContentText(charSequence6).setSubText(charSequence8).setContentInfo(charSequence7).setContentIntent(pendingIntent3).setDeleteIntent(notification2.deleteIntent);
            PendingIntent pendingIntent5 = pendingIntent4;
            if ((notification2.flags & 128) != 0) {
                z9 = true;
            } else {
                z9 = false;
            }
            this.b = deleteIntent.setFullScreenIntent(pendingIntent5, z9).setLargeIcon(bitmap2).setNumber(i5).setUsesChronometer(z11).setPriority(i8).setProgress(i6, i7, z10).setLocalOnly(z12).setGroup(str3).setGroupSummary(z13).setSortKey(str4);
            new Bundle();
            this.mExtras = bundle2;
            if (bundle3 != null) {
                this.mExtras.putAll(bundle3);
            }
            if (arrayList2 != null && !arrayList2.isEmpty()) {
                this.mExtras.putStringArray(NotificationCompat.EXTRA_PEOPLE, (String[]) arrayList2.toArray(new String[arrayList2.size()]));
            }
        }

        public void addAction(NotificationCompatBase.Action action) {
            NotificationCompatApi20.addAction(this.b, action);
        }

        public Notification.Builder getBuilder() {
            return this.b;
        }

        public Notification build() {
            Notification.Builder extras = this.b.setExtras(this.mExtras);
            return this.b.build();
        }
    }

    public static void addAction(Notification.Builder builder, NotificationCompatBase.Action action) {
        Notification.Action.Builder builder2;
        Notification.Builder builder3 = builder;
        NotificationCompatBase.Action action2 = action;
        new Notification.Action.Builder(action2.getIcon(), action2.getTitle(), action2.getActionIntent());
        Notification.Action.Builder builder4 = builder2;
        if (action2.getRemoteInputs() != null) {
            RemoteInput[] fromCompat = RemoteInputCompatApi20.fromCompat(action2.getRemoteInputs());
            int length = fromCompat.length;
            for (int i = 0; i < length; i++) {
                Notification.Action.Builder addRemoteInput = builder4.addRemoteInput(fromCompat[i]);
            }
        }
        if (action2.getExtras() != null) {
            Notification.Action.Builder addExtras = builder4.addExtras(action2.getExtras());
        }
        Notification.Builder addAction = builder3.addAction(builder4.build());
    }

    public static NotificationCompatBase.Action getAction(Notification notification, int i, NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        return getActionCompatFromAction(notification.actions[i], factory, factory2);
    }

    private static NotificationCompatBase.Action getActionCompatFromAction(Notification.Action action, NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        Notification.Action action2 = action;
        return factory.build(action2.icon, action2.title, action2.actionIntent, action2.getExtras(), RemoteInputCompatApi20.toCompat(action2.getRemoteInputs(), factory2));
    }

    private static Notification.Action getActionFromActionCompat(NotificationCompatBase.Action action) {
        Notification.Action.Builder builder;
        NotificationCompatBase.Action action2 = action;
        new Notification.Action.Builder(action2.getIcon(), action2.getTitle(), action2.getActionIntent());
        Notification.Action.Builder addExtras = builder.addExtras(action2.getExtras());
        RemoteInputCompatBase.RemoteInput[] remoteInputs = action2.getRemoteInputs();
        if (remoteInputs != null) {
            RemoteInput[] fromCompat = RemoteInputCompatApi20.fromCompat(remoteInputs);
            int length = fromCompat.length;
            for (int i = 0; i < length; i++) {
                Notification.Action.Builder addRemoteInput = addExtras.addRemoteInput(fromCompat[i]);
            }
        }
        return addExtras.build();
    }

    public static NotificationCompatBase.Action[] getActionsFromParcelableArrayList(ArrayList<Parcelable> arrayList, NotificationCompatBase.Action.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        ArrayList<Parcelable> arrayList2 = arrayList;
        NotificationCompatBase.Action.Factory factory3 = factory;
        RemoteInputCompatBase.RemoteInput.Factory factory4 = factory2;
        if (arrayList2 == null) {
            return null;
        }
        NotificationCompatBase.Action[] newArray = factory3.newArray(arrayList2.size());
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = getActionCompatFromAction((Notification.Action) arrayList2.get(i), factory3, factory4);
        }
        return newArray;
    }

    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.ArrayList<android.os.Parcelable> getParcelableArrayListForActions(android.support.v4.app.NotificationCompatBase.Action[] r10) {
        /*
            r0 = r10
            r6 = r0
            if (r6 != 0) goto L_0x0007
            r6 = 0
            r0 = r6
        L_0x0006:
            return r0
        L_0x0007:
            java.util.ArrayList r6 = new java.util.ArrayList
            r9 = r6
            r6 = r9
            r7 = r9
            r8 = r0
            int r8 = r8.length
            r7.<init>(r8)
            r1 = r6
            r6 = r0
            r2 = r6
            r6 = r2
            int r6 = r6.length
            r3 = r6
            r6 = 0
            r4 = r6
        L_0x0019:
            r6 = r4
            r7 = r3
            if (r6 >= r7) goto L_0x002f
            r6 = r2
            r7 = r4
            r6 = r6[r7]
            r5 = r6
            r6 = r1
            r7 = r5
            android.app.Notification$Action r7 = getActionFromActionCompat(r7)
            boolean r6 = r6.add(r7)
            int r4 = r4 + 1
            goto L_0x0019
        L_0x002f:
            r6 = r1
            r0 = r6
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.NotificationCompatApi20.getParcelableArrayListForActions(android.support.v4.app.NotificationCompatBase$Action[]):java.util.ArrayList");
    }

    public static boolean getLocalOnly(Notification notification) {
        return (notification.flags & 256) != 0;
    }

    public static String getGroup(Notification notification) {
        return notification.getGroup();
    }

    public static boolean isGroupSummary(Notification notification) {
        return (notification.flags & 512) != 0;
    }

    public static String getSortKey(Notification notification) {
        return notification.getSortKey();
    }
}
