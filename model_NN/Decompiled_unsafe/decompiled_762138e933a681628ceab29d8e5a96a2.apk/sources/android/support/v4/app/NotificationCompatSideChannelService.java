package android.support.v4.app;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.INotificationSideChannel;

public abstract class NotificationCompatSideChannelService extends Service {
    public abstract void cancel(String str, int i, String str2);

    public abstract void cancelAll(String str);

    public abstract void notify(String str, int i, String str2, Notification notification);

    public IBinder onBind(Intent intent) {
        IBinder iBinder;
        if (!intent.getAction().equals(NotificationManagerCompat.ACTION_BIND_SIDE_CHANNEL)) {
            return null;
        }
        if (Build.VERSION.SDK_INT > 19) {
            return null;
        }
        new NotificationSideChannelStub();
        return iBinder;
    }

    private class NotificationSideChannelStub extends INotificationSideChannel.Stub {
        private NotificationSideChannelStub() {
        }

        public void notify(String str, int i, String str2, Notification notification) throws RemoteException {
            String str3 = str;
            int i2 = i;
            String str4 = str2;
            Notification notification2 = notification;
            NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), str3);
            long clearCallingIdentity = clearCallingIdentity();
            try {
                NotificationCompatSideChannelService.this.notify(str3, i2, str4, notification2);
                restoreCallingIdentity(clearCallingIdentity);
            } catch (Throwable th) {
                Throwable th2 = th;
                restoreCallingIdentity(clearCallingIdentity);
                throw th2;
            }
        }

        public void cancel(String str, int i, String str2) throws RemoteException {
            String str3 = str;
            int i2 = i;
            String str4 = str2;
            NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), str3);
            long clearCallingIdentity = clearCallingIdentity();
            try {
                NotificationCompatSideChannelService.this.cancel(str3, i2, str4);
                restoreCallingIdentity(clearCallingIdentity);
            } catch (Throwable th) {
                Throwable th2 = th;
                restoreCallingIdentity(clearCallingIdentity);
                throw th2;
            }
        }

        public void cancelAll(String str) {
            String str2 = str;
            NotificationCompatSideChannelService.this.checkPermission(getCallingUid(), str2);
            long clearCallingIdentity = clearCallingIdentity();
            try {
                NotificationCompatSideChannelService.this.cancelAll(str2);
                restoreCallingIdentity(clearCallingIdentity);
            } catch (Throwable th) {
                Throwable th2 = th;
                restoreCallingIdentity(clearCallingIdentity);
                throw th2;
            }
        }
    }

    /* access modifiers changed from: private */
    public void checkPermission(int i, String str) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        String str2 = str;
        String[] packagesForUid = getPackageManager().getPackagesForUid(i2);
        int length = packagesForUid.length;
        int i3 = 0;
        while (i3 < length) {
            if (!packagesForUid[i3].equals(str2)) {
                i3++;
            } else {
                return;
            }
        }
        Throwable th2 = th;
        new StringBuilder();
        new SecurityException(sb.append("NotificationSideChannelService: Uid ").append(i2).append(" is not authorized for package ").append(str2).toString());
        throw th2;
    }
}
