package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.BackStackRecord;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayList;

/* compiled from: BackStackRecord */
final class BackStackState implements Parcelable {
    public static final Parcelable.Creator<BackStackState> CREATOR;
    final int mBreadCrumbShortTitleRes;
    final CharSequence mBreadCrumbShortTitleText;
    final int mBreadCrumbTitleRes;
    final CharSequence mBreadCrumbTitleText;
    final int mIndex;
    final String mName;
    final int[] mOps;
    final ArrayList<String> mSharedElementSourceNames;
    final ArrayList<String> mSharedElementTargetNames;
    final int mTransition;
    final int mTransitionStyle;

    public BackStackState(BackStackRecord backStackRecord) {
        Throwable th;
        BackStackRecord backStackRecord2 = backStackRecord;
        int i = 0;
        BackStackRecord.Op op = backStackRecord2.mHead;
        while (true) {
            BackStackRecord.Op op2 = op;
            if (op2 == null) {
                break;
            }
            if (op2.removed != null) {
                i += op2.removed.size();
            }
            op = op2.next;
        }
        this.mOps = new int[((backStackRecord2.mNumOp * 7) + i)];
        if (!backStackRecord2.mAddToBackStack) {
            Throwable th2 = th;
            new IllegalStateException("Not on back stack");
            throw th2;
        }
        int i2 = 0;
        for (BackStackRecord.Op op3 = backStackRecord2.mHead; op3 != null; op3 = op3.next) {
            int i3 = i2;
            int i4 = i2 + 1;
            this.mOps[i3] = op3.cmd;
            int i5 = i4;
            int i6 = i4 + 1;
            this.mOps[i5] = op3.fragment != null ? op3.fragment.mIndex : -1;
            int i7 = i6;
            int i8 = i6 + 1;
            this.mOps[i7] = op3.enterAnim;
            int i9 = i8;
            int i10 = i8 + 1;
            this.mOps[i9] = op3.exitAnim;
            int i11 = i10;
            int i12 = i10 + 1;
            this.mOps[i11] = op3.popEnterAnim;
            int i13 = i12;
            int i14 = i12 + 1;
            this.mOps[i13] = op3.popExitAnim;
            if (op3.removed != null) {
                int size = op3.removed.size();
                int i15 = i14;
                i2 = i14 + 1;
                this.mOps[i15] = size;
                for (int i16 = 0; i16 < size; i16++) {
                    int i17 = i2;
                    i2++;
                    this.mOps[i17] = op3.removed.get(i16).mIndex;
                }
            } else {
                int i18 = i14;
                i2 = i14 + 1;
                this.mOps[i18] = 0;
            }
        }
        this.mTransition = backStackRecord2.mTransition;
        this.mTransitionStyle = backStackRecord2.mTransitionStyle;
        this.mName = backStackRecord2.mName;
        this.mIndex = backStackRecord2.mIndex;
        this.mBreadCrumbTitleRes = backStackRecord2.mBreadCrumbTitleRes;
        this.mBreadCrumbTitleText = backStackRecord2.mBreadCrumbTitleText;
        this.mBreadCrumbShortTitleRes = backStackRecord2.mBreadCrumbShortTitleRes;
        this.mBreadCrumbShortTitleText = backStackRecord2.mBreadCrumbShortTitleText;
        this.mSharedElementSourceNames = backStackRecord2.mSharedElementSourceNames;
        this.mSharedElementTargetNames = backStackRecord2.mSharedElementTargetNames;
    }

    public BackStackState(Parcel parcel) {
        Parcel parcel2 = parcel;
        this.mOps = parcel2.createIntArray();
        this.mTransition = parcel2.readInt();
        this.mTransitionStyle = parcel2.readInt();
        this.mName = parcel2.readString();
        this.mIndex = parcel2.readInt();
        this.mBreadCrumbTitleRes = parcel2.readInt();
        this.mBreadCrumbTitleText = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
        this.mBreadCrumbShortTitleRes = parcel2.readInt();
        this.mBreadCrumbShortTitleText = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
        this.mSharedElementSourceNames = parcel2.createStringArrayList();
        this.mSharedElementTargetNames = parcel2.createStringArrayList();
    }

    public BackStackRecord instantiate(FragmentManagerImpl fragmentManagerImpl) {
        BackStackRecord backStackRecord;
        BackStackRecord.Op op;
        ArrayList<Fragment> arrayList;
        StringBuilder sb;
        StringBuilder sb2;
        FragmentManagerImpl fragmentManagerImpl2 = fragmentManagerImpl;
        new BackStackRecord(fragmentManagerImpl2);
        BackStackRecord backStackRecord2 = backStackRecord;
        int i = 0;
        int i2 = 0;
        while (i < this.mOps.length) {
            new BackStackRecord.Op();
            BackStackRecord.Op op2 = op;
            int i3 = i;
            int i4 = i + 1;
            op2.cmd = this.mOps[i3];
            if (FragmentManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v("FragmentManager", sb2.append("Instantiate ").append(backStackRecord2).append(" op #").append(i2).append(" base fragment #").append(this.mOps[i4]).toString());
            }
            int i5 = i4;
            int i6 = i4 + 1;
            int i7 = this.mOps[i5];
            if (i7 >= 0) {
                op2.fragment = fragmentManagerImpl2.mActive.get(i7);
            } else {
                op2.fragment = null;
            }
            int i8 = i6;
            int i9 = i6 + 1;
            op2.enterAnim = this.mOps[i8];
            int i10 = i9;
            int i11 = i9 + 1;
            op2.exitAnim = this.mOps[i10];
            int i12 = i11;
            int i13 = i11 + 1;
            op2.popEnterAnim = this.mOps[i12];
            int i14 = i13;
            int i15 = i13 + 1;
            op2.popExitAnim = this.mOps[i14];
            int i16 = i15;
            i = i15 + 1;
            int i17 = this.mOps[i16];
            if (i17 > 0) {
                new ArrayList<>(i17);
                op2.removed = arrayList;
                for (int i18 = 0; i18 < i17; i18++) {
                    if (FragmentManagerImpl.DEBUG) {
                        new StringBuilder();
                        int v2 = Log.v("FragmentManager", sb.append("Instantiate ").append(backStackRecord2).append(" set remove fragment #").append(this.mOps[i]).toString());
                    }
                    int i19 = i;
                    i++;
                    boolean add = op2.removed.add(fragmentManagerImpl2.mActive.get(this.mOps[i19]));
                }
            }
            backStackRecord2.addOp(op2);
            i2++;
        }
        backStackRecord2.mTransition = this.mTransition;
        backStackRecord2.mTransitionStyle = this.mTransitionStyle;
        backStackRecord2.mName = this.mName;
        backStackRecord2.mIndex = this.mIndex;
        backStackRecord2.mAddToBackStack = true;
        backStackRecord2.mBreadCrumbTitleRes = this.mBreadCrumbTitleRes;
        backStackRecord2.mBreadCrumbTitleText = this.mBreadCrumbTitleText;
        backStackRecord2.mBreadCrumbShortTitleRes = this.mBreadCrumbShortTitleRes;
        backStackRecord2.mBreadCrumbShortTitleText = this.mBreadCrumbShortTitleText;
        backStackRecord2.mSharedElementSourceNames = this.mSharedElementSourceNames;
        backStackRecord2.mSharedElementTargetNames = this.mSharedElementTargetNames;
        backStackRecord2.bumpBackStackNesting(1);
        return backStackRecord2;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Parcel parcel2 = parcel;
        parcel2.writeIntArray(this.mOps);
        parcel2.writeInt(this.mTransition);
        parcel2.writeInt(this.mTransitionStyle);
        parcel2.writeString(this.mName);
        parcel2.writeInt(this.mIndex);
        parcel2.writeInt(this.mBreadCrumbTitleRes);
        TextUtils.writeToParcel(this.mBreadCrumbTitleText, parcel2, 0);
        parcel2.writeInt(this.mBreadCrumbShortTitleRes);
        TextUtils.writeToParcel(this.mBreadCrumbShortTitleText, parcel2, 0);
        parcel2.writeStringList(this.mSharedElementSourceNames);
        parcel2.writeStringList(this.mSharedElementTargetNames);
    }

    static {
        Parcelable.Creator<BackStackState> creator;
        new Parcelable.Creator<BackStackState>() {
            public BackStackState createFromParcel(Parcel parcel) {
                BackStackState backStackState;
                new BackStackState(parcel);
                return backStackState;
            }

            public BackStackState[] newArray(int i) {
                return new BackStackState[i];
            }
        };
        CREATOR = creator;
    }
}
