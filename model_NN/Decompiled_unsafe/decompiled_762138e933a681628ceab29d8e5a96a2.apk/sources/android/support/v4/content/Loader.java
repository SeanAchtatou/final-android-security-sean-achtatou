package android.support.v4.content;

import android.content.Context;
import android.database.ContentObserver;
import android.support.v4.util.DebugUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class Loader<D> {
    boolean mAbandoned = false;
    boolean mContentChanged = false;
    Context mContext;
    int mId;
    OnLoadCompleteListener<D> mListener;
    OnLoadCanceledListener<D> mOnLoadCanceledListener;
    boolean mProcessingChange = false;
    boolean mReset = true;
    boolean mStarted = false;

    public interface OnLoadCanceledListener<D> {
        void onLoadCanceled(Loader<D> loader);
    }

    public interface OnLoadCompleteListener<D> {
        void onLoadComplete(Loader<D> loader, D d);
    }

    public final class ForceLoadContentObserver extends ContentObserver {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ForceLoadContentObserver() {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r0
                r3 = r1
                android.support.v4.content.Loader.this = r3
                r2 = r0
                android.os.Handler r3 = new android.os.Handler
                r5 = r3
                r3 = r5
                r4 = r5
                r4.<init>()
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.content.Loader.ForceLoadContentObserver.<init>(android.support.v4.content.Loader):void");
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            Loader.this.onContentChanged();
        }
    }

    public Loader(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public void deliverResult(D d) {
        D d2 = d;
        if (this.mListener != null) {
            this.mListener.onLoadComplete(this, d2);
        }
    }

    public void deliverCancellation() {
        if (this.mOnLoadCanceledListener != null) {
            this.mOnLoadCanceledListener.onLoadCanceled(this);
        }
    }

    public Context getContext() {
        return this.mContext;
    }

    public int getId() {
        return this.mId;
    }

    public void registerListener(int i, OnLoadCompleteListener<D> onLoadCompleteListener) {
        Throwable th;
        int i2 = i;
        OnLoadCompleteListener<D> onLoadCompleteListener2 = onLoadCompleteListener;
        if (this.mListener != null) {
            Throwable th2 = th;
            new IllegalStateException("There is already a listener registered");
            throw th2;
        }
        this.mListener = onLoadCompleteListener2;
        this.mId = i2;
    }

    public void unregisterListener(OnLoadCompleteListener<D> onLoadCompleteListener) {
        Throwable th;
        Throwable th2;
        OnLoadCompleteListener<D> onLoadCompleteListener2 = onLoadCompleteListener;
        if (this.mListener == null) {
            Throwable th3 = th2;
            new IllegalStateException("No listener register");
            throw th3;
        } else if (this.mListener != onLoadCompleteListener2) {
            Throwable th4 = th;
            new IllegalArgumentException("Attempting to unregister the wrong listener");
            throw th4;
        } else {
            this.mListener = null;
        }
    }

    public void registerOnLoadCanceledListener(OnLoadCanceledListener<D> onLoadCanceledListener) {
        Throwable th;
        OnLoadCanceledListener<D> onLoadCanceledListener2 = onLoadCanceledListener;
        if (this.mOnLoadCanceledListener != null) {
            Throwable th2 = th;
            new IllegalStateException("There is already a listener registered");
            throw th2;
        }
        this.mOnLoadCanceledListener = onLoadCanceledListener2;
    }

    public void unregisterOnLoadCanceledListener(OnLoadCanceledListener<D> onLoadCanceledListener) {
        Throwable th;
        Throwable th2;
        OnLoadCanceledListener<D> onLoadCanceledListener2 = onLoadCanceledListener;
        if (this.mOnLoadCanceledListener == null) {
            Throwable th3 = th2;
            new IllegalStateException("No listener register");
            throw th3;
        } else if (this.mOnLoadCanceledListener != onLoadCanceledListener2) {
            Throwable th4 = th;
            new IllegalArgumentException("Attempting to unregister the wrong listener");
            throw th4;
        } else {
            this.mOnLoadCanceledListener = null;
        }
    }

    public boolean isStarted() {
        return this.mStarted;
    }

    public boolean isAbandoned() {
        return this.mAbandoned;
    }

    public boolean isReset() {
        return this.mReset;
    }

    public final void startLoading() {
        this.mStarted = true;
        this.mReset = false;
        this.mAbandoned = false;
        onStartLoading();
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
    }

    public boolean cancelLoad() {
        return onCancelLoad();
    }

    /* access modifiers changed from: protected */
    public boolean onCancelLoad() {
        return false;
    }

    public void forceLoad() {
        onForceLoad();
    }

    /* access modifiers changed from: protected */
    public void onForceLoad() {
    }

    public void stopLoading() {
        this.mStarted = false;
        onStopLoading();
    }

    /* access modifiers changed from: protected */
    public void onStopLoading() {
    }

    public void abandon() {
        this.mAbandoned = true;
        onAbandon();
    }

    /* access modifiers changed from: protected */
    public void onAbandon() {
    }

    public void reset() {
        onReset();
        this.mReset = true;
        this.mStarted = false;
        this.mAbandoned = false;
        this.mContentChanged = false;
        this.mProcessingChange = false;
    }

    /* access modifiers changed from: protected */
    public void onReset() {
    }

    public boolean takeContentChanged() {
        boolean z = this.mContentChanged;
        this.mContentChanged = false;
        this.mProcessingChange = this.mProcessingChange | z;
        return z;
    }

    public void commitContentChanged() {
        this.mProcessingChange = false;
    }

    public void rollbackContentChanged() {
        if (this.mProcessingChange) {
            this.mContentChanged = true;
        }
    }

    public void onContentChanged() {
        if (this.mStarted) {
            forceLoad();
        } else {
            this.mContentChanged = true;
        }
    }

    public String dataToString(D d) {
        StringBuilder sb;
        new StringBuilder(64);
        StringBuilder sb2 = sb;
        DebugUtils.buildShortClassTag(d, sb2);
        StringBuilder append = sb2.append("}");
        return sb2.toString();
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder(64);
        StringBuilder sb2 = sb;
        DebugUtils.buildShortClassTag(this, sb2);
        StringBuilder append = sb2.append(" id=");
        StringBuilder append2 = sb2.append(this.mId);
        StringBuilder append3 = sb2.append("}");
        return sb2.toString();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str;
        PrintWriter printWriter2 = printWriter;
        printWriter2.print(str2);
        printWriter2.print("mId=");
        printWriter2.print(this.mId);
        printWriter2.print(" mListener=");
        printWriter2.println(this.mListener);
        if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
            printWriter2.print(str2);
            printWriter2.print("mStarted=");
            printWriter2.print(this.mStarted);
            printWriter2.print(" mContentChanged=");
            printWriter2.print(this.mContentChanged);
            printWriter2.print(" mProcessingChange=");
            printWriter2.println(this.mProcessingChange);
        }
        if (this.mAbandoned || this.mReset) {
            printWriter2.print(str2);
            printWriter2.print("mAbandoned=");
            printWriter2.print(this.mAbandoned);
            printWriter2.print(" mReset=");
            printWriter2.println(this.mReset);
        }
    }
}
