package android.support.v4.content;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.os.CancellationSignal;
import android.support.v4.os.OperationCanceledException;

public class ContentResolverCompat {
    private static final ContentResolverCompatImpl IMPL;

    interface ContentResolverCompatImpl {
        Cursor query(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal);
    }

    static class ContentResolverCompatImplBase implements ContentResolverCompatImpl {
        ContentResolverCompatImplBase() {
        }

        public Cursor query(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal) {
            ContentResolver contentResolver2 = contentResolver;
            Uri uri2 = uri;
            String[] strArr3 = strArr;
            String str3 = str;
            String[] strArr4 = strArr2;
            String str4 = str2;
            CancellationSignal cancellationSignal2 = cancellationSignal;
            if (cancellationSignal2 != null) {
                cancellationSignal2.throwIfCanceled();
            }
            return contentResolver2.query(uri2, strArr3, str3, strArr4, str4);
        }
    }

    static class ContentResolverCompatImplJB extends ContentResolverCompatImplBase {
        ContentResolverCompatImplJB() {
        }

        public Cursor query(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal) {
            Object obj;
            Throwable th;
            CancellationSignal cancellationSignal2 = cancellationSignal;
            ContentResolver contentResolver2 = contentResolver;
            Uri uri2 = uri;
            String[] strArr3 = strArr;
            String str3 = str;
            String[] strArr4 = strArr2;
            String str4 = str2;
            if (cancellationSignal2 != null) {
                try {
                    obj = cancellationSignal2.getCancellationSignalObject();
                } catch (Exception e) {
                    Exception exc = e;
                    if (ContentResolverCompatJellybean.isFrameworkOperationCanceledException(exc)) {
                        Throwable th2 = th;
                        new OperationCanceledException();
                        throw th2;
                    }
                    throw exc;
                }
            } else {
                obj = null;
            }
            return ContentResolverCompatJellybean.query(contentResolver2, uri2, strArr3, str3, strArr4, str4, obj);
        }
    }

    static {
        ContentResolverCompatImpl contentResolverCompatImpl;
        ContentResolverCompatImpl contentResolverCompatImpl2;
        if (Build.VERSION.SDK_INT >= 16) {
            new ContentResolverCompatImplJB();
            IMPL = contentResolverCompatImpl2;
            return;
        }
        new ContentResolverCompatImplBase();
        IMPL = contentResolverCompatImpl;
    }

    private ContentResolverCompat() {
    }

    public static Cursor query(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, CancellationSignal cancellationSignal) {
        return IMPL.query(contentResolver, uri, strArr, str, strArr2, str2, cancellationSignal);
    }
}
