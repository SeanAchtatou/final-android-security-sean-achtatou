package android.support.v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaDescriptionCompatApi21;
import android.support.v4.media.MediaDescriptionCompatApi23;
import android.text.TextUtils;

public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator<MediaDescriptionCompat> CREATOR;
    private final CharSequence mDescription;
    private Object mDescriptionObj;
    private final Bundle mExtras;
    private final Bitmap mIcon;
    private final Uri mIconUri;
    private final String mMediaId;
    private final Uri mMediaUri;
    private final CharSequence mSubtitle;
    private final CharSequence mTitle;

    private MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.mMediaId = str;
        this.mTitle = charSequence;
        this.mSubtitle = charSequence2;
        this.mDescription = charSequence3;
        this.mIcon = bitmap;
        this.mIconUri = uri;
        this.mExtras = bundle;
        this.mMediaUri = uri2;
    }

    private MediaDescriptionCompat(Parcel parcel) {
        Parcel parcel2 = parcel;
        this.mMediaId = parcel2.readString();
        this.mTitle = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
        this.mSubtitle = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
        this.mDescription = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
        this.mIcon = (Bitmap) parcel2.readParcelable(null);
        this.mIconUri = (Uri) parcel2.readParcelable(null);
        this.mExtras = parcel2.readBundle();
        this.mMediaUri = (Uri) parcel2.readParcelable(null);
    }

    @Nullable
    public String getMediaId() {
        return this.mMediaId;
    }

    @Nullable
    public CharSequence getTitle() {
        return this.mTitle;
    }

    @Nullable
    public CharSequence getSubtitle() {
        return this.mSubtitle;
    }

    @Nullable
    public CharSequence getDescription() {
        return this.mDescription;
    }

    @Nullable
    public Bitmap getIconBitmap() {
        return this.mIcon;
    }

    @Nullable
    public Uri getIconUri() {
        return this.mIconUri;
    }

    @Nullable
    public Bundle getExtras() {
        return this.mExtras;
    }

    @Nullable
    public Uri getMediaUri() {
        return this.mMediaUri;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        Parcel parcel2 = parcel;
        int i2 = i;
        if (Build.VERSION.SDK_INT < 21) {
            parcel2.writeString(this.mMediaId);
            TextUtils.writeToParcel(this.mTitle, parcel2, i2);
            TextUtils.writeToParcel(this.mSubtitle, parcel2, i2);
            TextUtils.writeToParcel(this.mDescription, parcel2, i2);
            parcel2.writeParcelable(this.mIcon, i2);
            parcel2.writeParcelable(this.mIconUri, i2);
            parcel2.writeBundle(this.mExtras);
            return;
        }
        MediaDescriptionCompatApi21.writeToParcel(getMediaDescription(), parcel2, i2);
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder();
        return sb.append((Object) this.mTitle).append(", ").append((Object) this.mSubtitle).append(", ").append((Object) this.mDescription).toString();
    }

    public Object getMediaDescription() {
        if (this.mDescriptionObj != null || Build.VERSION.SDK_INT < 21) {
            return this.mDescriptionObj;
        }
        Object newInstance = MediaDescriptionCompatApi21.Builder.newInstance();
        MediaDescriptionCompatApi21.Builder.setMediaId(newInstance, this.mMediaId);
        MediaDescriptionCompatApi21.Builder.setTitle(newInstance, this.mTitle);
        MediaDescriptionCompatApi21.Builder.setSubtitle(newInstance, this.mSubtitle);
        MediaDescriptionCompatApi21.Builder.setDescription(newInstance, this.mDescription);
        MediaDescriptionCompatApi21.Builder.setIconBitmap(newInstance, this.mIcon);
        MediaDescriptionCompatApi21.Builder.setIconUri(newInstance, this.mIconUri);
        MediaDescriptionCompatApi21.Builder.setExtras(newInstance, this.mExtras);
        if (Build.VERSION.SDK_INT >= 23) {
            MediaDescriptionCompatApi23.Builder.setMediaUri(newInstance, this.mMediaUri);
        }
        this.mDescriptionObj = MediaDescriptionCompatApi21.Builder.build(newInstance);
        return this.mDescriptionObj;
    }

    public static MediaDescriptionCompat fromMediaDescription(Object obj) {
        Builder builder;
        Object obj2 = obj;
        if (obj2 == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        new Builder();
        Builder builder2 = builder;
        Builder mediaId = builder2.setMediaId(MediaDescriptionCompatApi21.getMediaId(obj2));
        Builder title = builder2.setTitle(MediaDescriptionCompatApi21.getTitle(obj2));
        Builder subtitle = builder2.setSubtitle(MediaDescriptionCompatApi21.getSubtitle(obj2));
        Builder description = builder2.setDescription(MediaDescriptionCompatApi21.getDescription(obj2));
        Builder iconBitmap = builder2.setIconBitmap(MediaDescriptionCompatApi21.getIconBitmap(obj2));
        Builder iconUri = builder2.setIconUri(MediaDescriptionCompatApi21.getIconUri(obj2));
        Builder extras = builder2.setExtras(MediaDescriptionCompatApi21.getExtras(obj2));
        if (Build.VERSION.SDK_INT >= 23) {
            Builder mediaUri = builder2.setMediaUri(MediaDescriptionCompatApi23.getMediaUri(obj2));
        }
        MediaDescriptionCompat build = builder2.build();
        build.mDescriptionObj = obj2;
        return build;
    }

    static {
        Parcelable.Creator<MediaDescriptionCompat> creator;
        new Parcelable.Creator<MediaDescriptionCompat>() {
            public MediaDescriptionCompat createFromParcel(Parcel parcel) {
                MediaDescriptionCompat mediaDescriptionCompat;
                Parcel parcel2 = parcel;
                if (Build.VERSION.SDK_INT >= 21) {
                    return MediaDescriptionCompat.fromMediaDescription(MediaDescriptionCompatApi21.fromParcel(parcel2));
                }
                new MediaDescriptionCompat(parcel2);
                return mediaDescriptionCompat;
            }

            public MediaDescriptionCompat[] newArray(int i) {
                return new MediaDescriptionCompat[i];
            }
        };
        CREATOR = creator;
    }

    public static final class Builder {
        private CharSequence mDescription;
        private Bundle mExtras;
        private Bitmap mIcon;
        private Uri mIconUri;
        private String mMediaId;
        private Uri mMediaUri;
        private CharSequence mSubtitle;
        private CharSequence mTitle;

        public Builder setMediaId(@Nullable String str) {
            this.mMediaId = str;
            return this;
        }

        public Builder setTitle(@Nullable CharSequence charSequence) {
            this.mTitle = charSequence;
            return this;
        }

        public Builder setSubtitle(@Nullable CharSequence charSequence) {
            this.mSubtitle = charSequence;
            return this;
        }

        public Builder setDescription(@Nullable CharSequence charSequence) {
            this.mDescription = charSequence;
            return this;
        }

        public Builder setIconBitmap(@Nullable Bitmap bitmap) {
            this.mIcon = bitmap;
            return this;
        }

        public Builder setIconUri(@Nullable Uri uri) {
            this.mIconUri = uri;
            return this;
        }

        public Builder setExtras(@Nullable Bundle bundle) {
            this.mExtras = bundle;
            return this;
        }

        public Builder setMediaUri(@Nullable Uri uri) {
            this.mMediaUri = uri;
            return this;
        }

        public MediaDescriptionCompat build() {
            MediaDescriptionCompat mediaDescriptionCompat;
            new MediaDescriptionCompat(this.mMediaId, this.mTitle, this.mSubtitle, this.mDescription, this.mIcon, this.mIconUri, this.mExtras, this.mMediaUri);
            return mediaDescriptionCompat;
        }
    }
}
