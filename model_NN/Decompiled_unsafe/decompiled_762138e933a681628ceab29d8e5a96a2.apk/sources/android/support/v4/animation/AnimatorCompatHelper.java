package android.support.v4.animation;

import android.os.Build;
import android.view.View;

public abstract class AnimatorCompatHelper {
    static AnimatorProvider IMPL;

    static {
        AnimatorProvider animatorProvider;
        AnimatorProvider animatorProvider2;
        if (Build.VERSION.SDK_INT >= 12) {
            new HoneycombMr1AnimatorCompatProvider();
            IMPL = animatorProvider2;
            return;
        }
        new DonutAnimatorCompatProvider();
        IMPL = animatorProvider;
    }

    public static ValueAnimatorCompat emptyValueAnimator() {
        return IMPL.emptyValueAnimator();
    }

    AnimatorCompatHelper() {
    }

    public static void clearInterpolator(View view) {
        IMPL.clearInterpolator(view);
    }
}
