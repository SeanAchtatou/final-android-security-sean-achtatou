package android.support.v4.view;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public class GestureDetectorCompat {
    private final GestureDetectorCompatImpl mImpl;

    interface GestureDetectorCompatImpl {
        boolean isLongpressEnabled();

        boolean onTouchEvent(MotionEvent motionEvent);

        void setIsLongpressEnabled(boolean z);

        void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener);
    }

    static class GestureDetectorCompatImplBase implements GestureDetectorCompatImpl {
        private static final int DOUBLE_TAP_TIMEOUT = ViewConfiguration.getDoubleTapTimeout();
        private static final int LONGPRESS_TIMEOUT = ViewConfiguration.getLongPressTimeout();
        private static final int LONG_PRESS = 2;
        private static final int SHOW_PRESS = 1;
        private static final int TAP = 3;
        private static final int TAP_TIMEOUT = ViewConfiguration.getTapTimeout();
        private boolean mAlwaysInBiggerTapRegion;
        private boolean mAlwaysInTapRegion;
        /* access modifiers changed from: private */
        public MotionEvent mCurrentDownEvent;
        private boolean mDeferConfirmSingleTap;
        /* access modifiers changed from: private */
        public GestureDetector.OnDoubleTapListener mDoubleTapListener;
        private int mDoubleTapSlopSquare;
        private float mDownFocusX;
        private float mDownFocusY;
        private final Handler mHandler;
        private boolean mInLongPress;
        private boolean mIsDoubleTapping;
        private boolean mIsLongpressEnabled;
        private float mLastFocusX;
        private float mLastFocusY;
        /* access modifiers changed from: private */
        public final GestureDetector.OnGestureListener mListener;
        private int mMaximumFlingVelocity;
        private int mMinimumFlingVelocity;
        private MotionEvent mPreviousUpEvent;
        /* access modifiers changed from: private */
        public boolean mStillDown;
        private int mTouchSlopSquare;
        private VelocityTracker mVelocityTracker;

        static /* synthetic */ boolean access$502(GestureDetectorCompatImplBase gestureDetectorCompatImplBase, boolean z) {
            boolean z2 = z;
            boolean z3 = z2;
            gestureDetectorCompatImplBase.mDeferConfirmSingleTap = z3;
            return z2;
        }

        private class GestureHandler extends Handler {
            GestureHandler() {
            }

            GestureHandler(Handler handler) {
                super(handler.getLooper());
            }

            public void handleMessage(Message message) {
                Throwable th;
                StringBuilder sb;
                Message message2 = message;
                switch (message2.what) {
                    case 1:
                        GestureDetectorCompatImplBase.this.mListener.onShowPress(GestureDetectorCompatImplBase.this.mCurrentDownEvent);
                        return;
                    case 2:
                        GestureDetectorCompatImplBase.this.dispatchLongPress();
                        return;
                    case 3:
                        if (GestureDetectorCompatImplBase.this.mDoubleTapListener == null) {
                            return;
                        }
                        if (!GestureDetectorCompatImplBase.this.mStillDown) {
                            boolean onSingleTapConfirmed = GestureDetectorCompatImplBase.this.mDoubleTapListener.onSingleTapConfirmed(GestureDetectorCompatImplBase.this.mCurrentDownEvent);
                            return;
                        } else {
                            boolean access$502 = GestureDetectorCompatImplBase.access$502(GestureDetectorCompatImplBase.this, true);
                            return;
                        }
                    default:
                        Throwable th2 = th;
                        new StringBuilder();
                        new RuntimeException(sb.append("Unknown message ").append(message2).toString());
                        throw th2;
                }
            }
        }

        public GestureDetectorCompatImplBase(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            Handler handler2;
            Handler handler3;
            Context context2 = context;
            GestureDetector.OnGestureListener onGestureListener2 = onGestureListener;
            Handler handler4 = handler;
            if (handler4 != null) {
                new GestureHandler(handler4);
                this.mHandler = handler3;
            } else {
                new GestureHandler();
                this.mHandler = handler2;
            }
            this.mListener = onGestureListener2;
            if (onGestureListener2 instanceof GestureDetector.OnDoubleTapListener) {
                setOnDoubleTapListener((GestureDetector.OnDoubleTapListener) onGestureListener2);
            }
            init(context2);
        }

        private void init(Context context) {
            Throwable th;
            Throwable th2;
            Context context2 = context;
            if (context2 == null) {
                Throwable th3 = th2;
                new IllegalArgumentException("Context must not be null");
                throw th3;
            } else if (this.mListener == null) {
                Throwable th4 = th;
                new IllegalArgumentException("OnGestureListener must not be null");
                throw th4;
            } else {
                this.mIsLongpressEnabled = true;
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context2);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.mMinimumFlingVelocity = viewConfiguration.getScaledMinimumFlingVelocity();
                this.mMaximumFlingVelocity = viewConfiguration.getScaledMaximumFlingVelocity();
                this.mTouchSlopSquare = scaledTouchSlop * scaledTouchSlop;
                this.mDoubleTapSlopSquare = scaledDoubleTapSlop * scaledDoubleTapSlop;
            }
        }

        public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.mDoubleTapListener = onDoubleTapListener;
        }

        public void setIsLongpressEnabled(boolean z) {
            this.mIsLongpressEnabled = z;
        }

        public boolean isLongpressEnabled() {
            return this.mIsLongpressEnabled;
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            MotionEvent motionEvent2 = motionEvent;
            int action = motionEvent2.getAction();
            if (this.mVelocityTracker == null) {
                this.mVelocityTracker = VelocityTracker.obtain();
            }
            this.mVelocityTracker.addMovement(motionEvent2);
            boolean z = (action & 255) == 6;
            int actionIndex = z ? MotionEventCompat.getActionIndex(motionEvent2) : -1;
            float f = 0.0f;
            float f2 = 0.0f;
            int pointerCount = MotionEventCompat.getPointerCount(motionEvent2);
            for (int i = 0; i < pointerCount; i++) {
                if (actionIndex != i) {
                    f += MotionEventCompat.getX(motionEvent2, i);
                    f2 += MotionEventCompat.getY(motionEvent2, i);
                }
            }
            int i2 = z ? pointerCount - 1 : pointerCount;
            float f3 = f / ((float) i2);
            float f4 = f2 / ((float) i2);
            boolean z2 = false;
            switch (action & 255) {
                case 0:
                    if (this.mDoubleTapListener != null) {
                        boolean hasMessages = this.mHandler.hasMessages(3);
                        if (hasMessages) {
                            this.mHandler.removeMessages(3);
                        }
                        if (this.mCurrentDownEvent == null || this.mPreviousUpEvent == null || !hasMessages || !isConsideredDoubleTap(this.mCurrentDownEvent, this.mPreviousUpEvent, motionEvent2)) {
                            boolean sendEmptyMessageDelayed = this.mHandler.sendEmptyMessageDelayed(3, (long) DOUBLE_TAP_TIMEOUT);
                        } else {
                            this.mIsDoubleTapping = true;
                            z2 = false | this.mDoubleTapListener.onDoubleTap(this.mCurrentDownEvent) | this.mDoubleTapListener.onDoubleTapEvent(motionEvent2);
                        }
                    }
                    float f5 = f3;
                    this.mLastFocusX = f5;
                    this.mDownFocusX = f5;
                    float f6 = f4;
                    this.mLastFocusY = f6;
                    this.mDownFocusY = f6;
                    if (this.mCurrentDownEvent != null) {
                        this.mCurrentDownEvent.recycle();
                    }
                    this.mCurrentDownEvent = MotionEvent.obtain(motionEvent2);
                    this.mAlwaysInTapRegion = true;
                    this.mAlwaysInBiggerTapRegion = true;
                    this.mStillDown = true;
                    this.mInLongPress = false;
                    this.mDeferConfirmSingleTap = false;
                    if (this.mIsLongpressEnabled) {
                        this.mHandler.removeMessages(2);
                        boolean sendEmptyMessageAtTime = this.mHandler.sendEmptyMessageAtTime(2, this.mCurrentDownEvent.getDownTime() + ((long) TAP_TIMEOUT) + ((long) LONGPRESS_TIMEOUT));
                    }
                    boolean sendEmptyMessageAtTime2 = this.mHandler.sendEmptyMessageAtTime(1, this.mCurrentDownEvent.getDownTime() + ((long) TAP_TIMEOUT));
                    z2 |= this.mListener.onDown(motionEvent2);
                    break;
                case 1:
                    this.mStillDown = false;
                    MotionEvent obtain = MotionEvent.obtain(motionEvent2);
                    if (this.mIsDoubleTapping) {
                        z2 = false | this.mDoubleTapListener.onDoubleTapEvent(motionEvent2);
                    } else if (this.mInLongPress) {
                        this.mHandler.removeMessages(3);
                        this.mInLongPress = false;
                    } else if (this.mAlwaysInTapRegion) {
                        z2 = this.mListener.onSingleTapUp(motionEvent2);
                        if (this.mDeferConfirmSingleTap && this.mDoubleTapListener != null) {
                            boolean onSingleTapConfirmed = this.mDoubleTapListener.onSingleTapConfirmed(motionEvent2);
                        }
                    } else {
                        VelocityTracker velocityTracker = this.mVelocityTracker;
                        int pointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                        velocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumFlingVelocity);
                        float yVelocity = VelocityTrackerCompat.getYVelocity(velocityTracker, pointerId);
                        float xVelocity = VelocityTrackerCompat.getXVelocity(velocityTracker, pointerId);
                        if (Math.abs(yVelocity) > ((float) this.mMinimumFlingVelocity) || Math.abs(xVelocity) > ((float) this.mMinimumFlingVelocity)) {
                            z2 = this.mListener.onFling(this.mCurrentDownEvent, motionEvent2, xVelocity, yVelocity);
                        }
                    }
                    if (this.mPreviousUpEvent != null) {
                        this.mPreviousUpEvent.recycle();
                    }
                    this.mPreviousUpEvent = obtain;
                    if (this.mVelocityTracker != null) {
                        this.mVelocityTracker.recycle();
                        this.mVelocityTracker = null;
                    }
                    this.mIsDoubleTapping = false;
                    this.mDeferConfirmSingleTap = false;
                    this.mHandler.removeMessages(1);
                    this.mHandler.removeMessages(2);
                    break;
                case 2:
                    if (!this.mInLongPress) {
                        float f7 = this.mLastFocusX - f3;
                        float f8 = this.mLastFocusY - f4;
                        if (!this.mIsDoubleTapping) {
                            if (!this.mAlwaysInTapRegion) {
                                if (Math.abs(f7) >= 1.0f || Math.abs(f8) >= 1.0f) {
                                    z2 = this.mListener.onScroll(this.mCurrentDownEvent, motionEvent2, f7, f8);
                                    this.mLastFocusX = f3;
                                    this.mLastFocusY = f4;
                                    break;
                                }
                            } else {
                                int i3 = (int) (f3 - this.mDownFocusX);
                                int i4 = (int) (f4 - this.mDownFocusY);
                                int i5 = (i3 * i3) + (i4 * i4);
                                if (i5 > this.mTouchSlopSquare) {
                                    z2 = this.mListener.onScroll(this.mCurrentDownEvent, motionEvent2, f7, f8);
                                    this.mLastFocusX = f3;
                                    this.mLastFocusY = f4;
                                    this.mAlwaysInTapRegion = false;
                                    this.mHandler.removeMessages(3);
                                    this.mHandler.removeMessages(1);
                                    this.mHandler.removeMessages(2);
                                }
                                if (i5 > this.mTouchSlopSquare) {
                                    this.mAlwaysInBiggerTapRegion = false;
                                    break;
                                }
                            }
                        } else {
                            z2 = false | this.mDoubleTapListener.onDoubleTapEvent(motionEvent2);
                            break;
                        }
                    }
                    break;
                case 3:
                    cancel();
                    break;
                case 5:
                    float f9 = f3;
                    this.mLastFocusX = f9;
                    this.mDownFocusX = f9;
                    float f10 = f4;
                    this.mLastFocusY = f10;
                    this.mDownFocusY = f10;
                    cancelTaps();
                    break;
                case 6:
                    float f11 = f3;
                    this.mLastFocusX = f11;
                    this.mDownFocusX = f11;
                    float f12 = f4;
                    this.mLastFocusY = f12;
                    this.mDownFocusY = f12;
                    this.mVelocityTracker.computeCurrentVelocity(1000, (float) this.mMaximumFlingVelocity);
                    int actionIndex2 = MotionEventCompat.getActionIndex(motionEvent2);
                    int pointerId2 = MotionEventCompat.getPointerId(motionEvent2, actionIndex2);
                    float xVelocity2 = VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, pointerId2);
                    float yVelocity2 = VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, pointerId2);
                    int i6 = 0;
                    while (true) {
                        if (i6 >= pointerCount) {
                            break;
                        } else {
                            if (i6 != actionIndex2) {
                                int pointerId3 = MotionEventCompat.getPointerId(motionEvent2, i6);
                                if ((xVelocity2 * VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, pointerId3)) + (yVelocity2 * VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, pointerId3)) < 0.0f) {
                                    this.mVelocityTracker.clear();
                                    break;
                                }
                            }
                            i6++;
                        }
                    }
            }
            return z2;
        }

        private void cancel() {
            this.mHandler.removeMessages(1);
            this.mHandler.removeMessages(2);
            this.mHandler.removeMessages(3);
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
            this.mIsDoubleTapping = false;
            this.mStillDown = false;
            this.mAlwaysInTapRegion = false;
            this.mAlwaysInBiggerTapRegion = false;
            this.mDeferConfirmSingleTap = false;
            if (this.mInLongPress) {
                this.mInLongPress = false;
            }
        }

        private void cancelTaps() {
            this.mHandler.removeMessages(1);
            this.mHandler.removeMessages(2);
            this.mHandler.removeMessages(3);
            this.mIsDoubleTapping = false;
            this.mAlwaysInTapRegion = false;
            this.mAlwaysInBiggerTapRegion = false;
            this.mDeferConfirmSingleTap = false;
            if (this.mInLongPress) {
                this.mInLongPress = false;
            }
        }

        private boolean isConsideredDoubleTap(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            MotionEvent motionEvent4 = motionEvent;
            MotionEvent motionEvent5 = motionEvent2;
            MotionEvent motionEvent6 = motionEvent3;
            if (!this.mAlwaysInBiggerTapRegion) {
                return false;
            }
            if (motionEvent6.getEventTime() - motionEvent5.getEventTime() > ((long) DOUBLE_TAP_TIMEOUT)) {
                return false;
            }
            int x = ((int) motionEvent4.getX()) - ((int) motionEvent6.getX());
            int y = ((int) motionEvent4.getY()) - ((int) motionEvent6.getY());
            return (x * x) + (y * y) < this.mDoubleTapSlopSquare;
        }

        /* access modifiers changed from: private */
        public void dispatchLongPress() {
            this.mHandler.removeMessages(3);
            this.mDeferConfirmSingleTap = false;
            this.mInLongPress = true;
            this.mListener.onLongPress(this.mCurrentDownEvent);
        }
    }

    static class GestureDetectorCompatImplJellybeanMr2 implements GestureDetectorCompatImpl {
        private final GestureDetector mDetector;

        public GestureDetectorCompatImplJellybeanMr2(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            GestureDetector gestureDetector;
            new GestureDetector(context, onGestureListener, handler);
            this.mDetector = gestureDetector;
        }

        public boolean isLongpressEnabled() {
            return this.mDetector.isLongpressEnabled();
        }

        public boolean onTouchEvent(MotionEvent motionEvent) {
            return this.mDetector.onTouchEvent(motionEvent);
        }

        public void setIsLongpressEnabled(boolean z) {
            this.mDetector.setIsLongpressEnabled(z);
        }

        public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.mDetector.setOnDoubleTapListener(onDoubleTapListener);
        }
    }

    public GestureDetectorCompat(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, null);
    }

    public GestureDetectorCompat(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        GestureDetectorCompatImpl gestureDetectorCompatImpl;
        GestureDetectorCompatImpl gestureDetectorCompatImpl2;
        Context context2 = context;
        GestureDetector.OnGestureListener onGestureListener2 = onGestureListener;
        Handler handler2 = handler;
        if (Build.VERSION.SDK_INT > 17) {
            new GestureDetectorCompatImplJellybeanMr2(context2, onGestureListener2, handler2);
            this.mImpl = gestureDetectorCompatImpl2;
            return;
        }
        new GestureDetectorCompatImplBase(context2, onGestureListener2, handler2);
        this.mImpl = gestureDetectorCompatImpl;
    }

    public boolean isLongpressEnabled() {
        return this.mImpl.isLongpressEnabled();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        return this.mImpl.onTouchEvent(motionEvent);
    }

    public void setIsLongpressEnabled(boolean z) {
        this.mImpl.setIsLongpressEnabled(z);
    }

    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
        this.mImpl.setOnDoubleTapListener(onDoubleTapListener);
    }
}
