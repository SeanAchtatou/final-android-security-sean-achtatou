package Cute;

import it.sauronsoftware.base64.Base64;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class Base64Utils {
    private static final int CACHE_SIZE = 1024;

    public static void byteArrayToFile(byte[] bArr, String str) throws Exception {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        File file = new File(str);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        byte[] bArr2 = new byte[CACHE_SIZE];
        while (true) {
            int read = byteArrayInputStream.read(bArr2);
            if (read == -1) {
                fileOutputStream.close();
                byteArrayInputStream.close();
                return;
            }
            fileOutputStream.write(bArr2, 0, read);
            fileOutputStream.flush();
        }
    }

    public static byte[] decode(String str) throws Exception {
        return Base64.decode(str.getBytes());
    }

    public static void decodeToFile(String str, String str2) throws Exception {
        byteArrayToFile(decode(str2), str);
    }

    public static String encode(byte[] bArr) throws Exception {
        return new String(Base64.encode(bArr));
    }

    public static String encodeFile(String str) throws Exception {
        return encode(fileToByte(str));
    }

    public static byte[] fileToByte(String str) throws Exception {
        byte[] bArr = new byte[0];
        File file = new File(str);
        if (!file.exists()) {
            return bArr;
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2048);
        byte[] bArr2 = new byte[CACHE_SIZE];
        while (true) {
            int read = fileInputStream.read(bArr2);
            if (read == -1) {
                byteArrayOutputStream.close();
                fileInputStream.close();
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }
}
