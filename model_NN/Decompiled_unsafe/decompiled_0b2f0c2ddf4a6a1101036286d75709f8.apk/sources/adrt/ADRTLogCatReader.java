package adrt;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class ADRTLogCatReader implements Runnable {
    private static Context context;

    public static void onContext(Context context2, String str) {
        Thread thread;
        Runnable runnable;
        Context context3 = context2;
        String str2 = str;
        if (context == null) {
            context = context3.getApplicationContext();
            if (0 != (context3.getApplicationInfo().flags & 2)) {
                try {
                    PackageInfo packageInfo = context3.getPackageManager().getPackageInfo(str2, 128);
                    ADRTSender.onContext(context, str2);
                    new ADRTLogCatReader();
                    new Thread(runnable, "LogCat");
                    thread.start();
                } catch (PackageManager.NameNotFoundException e) {
                }
            }
        }
    }

    public void run() {
        BufferedReader bufferedReader;
        Reader reader;
        try {
            new InputStreamReader(Runtime.getRuntime().exec("logcat -v threadtime").getInputStream());
            new BufferedReader(reader, 20);
            BufferedReader bufferedReader2 = bufferedReader;
            Object obj = "";
            while (true) {
                String readLine = bufferedReader2.readLine();
                String str = readLine;
                if (readLine != null) {
                    ADRTSender.sendLogcatLines(new String[]{str});
                } else {
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
