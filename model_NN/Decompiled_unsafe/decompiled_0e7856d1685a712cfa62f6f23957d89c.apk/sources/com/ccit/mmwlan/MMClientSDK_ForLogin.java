package com.ccit.mmwlan;

import android.content.Context;
import com.ccit.mmwlan.a.e;
import com.ccit.mmwlan.a.f;
import com.ccit.mmwlan.a.g;
import com.ccit.mmwlan.a.h;
import com.ccit.mmwlan.b.a;
import com.ccit.mmwlan.b.b;
import com.ccit.mmwlan.exception.ClientSDKException;
import com.ccit.mmwlan.vo.DeviceInfo;
import java.util.ArrayList;
import org.apache.http.HttpHost;

public final class MMClientSDK_ForLogin {
    private static final int INT_RESULT_0 = 0;
    private static final int INT_RESULT_1 = 1;
    private static ClientSDK clientSDK;
    private static Context context = null;
    private static a getDevInfo = new a();

    static {
        clientSDK = null;
        clientSDK = new ClientSDK();
    }

    public static String SIDSign(int i, int i2, String str, String str2, String str3) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str2 == null || str2.equals("") || str3 == null || str3.equals(""))) {
            "SIDSign() result -> " + ((String) null);
            return null;
        } else if (i2 != 2 || (str != null && !str.equals(""))) {
            try {
                DeviceInfo a = a.a(context, i);
                if (i == 2) {
                    a.getStrMac();
                } else {
                    a.getStrImsi();
                }
                try {
                    String SignNativeForLogin = clientSDK.SignNativeForLogin(str2, str3, i2, str, a);
                    if (SignNativeForLogin == null || SignNativeForLogin.equals("")) {
                        "SIDSign() result -> " + ((String) null) + "; 签名失败!";
                        return null;
                    }
                    String str4 = "#V1.0#" + SignNativeForLogin;
                    "SIDSign() result -> " + str4;
                    return str4;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    "SIDSign() ClientSDKException -> " + e.toString();
                    return null;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                "SIDSign() ClientSDKException -> " + e2.toString();
                return null;
            }
        } else {
            "SIDSign() result -> " + ((String) null);
            return null;
        }
    }

    public static int applySecCert(int i, int i2, String str, String str2, String str3, String str4, String str5, HttpHost httpHost, HttpHost httpHost2, int i3) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str5 == null || str5.equals("") || str4 == null || str4.equals("") || httpHost.getHostName() == null || httpHost.getHostName().equals(""))) {
            new StringBuilder("applySecCert() result -> 5").toString();
            return 5;
        } else if (i2 != 2 || (str != null && !str.equals("") && str2 != null && !str2.equals(""))) {
            try {
                DeviceInfo a = a.a(context, i);
                try {
                    String digestNative = clientSDK.getDigestNative("md5", i == 2 ? a.getStrMac() : a.getStrImsi());
                    try {
                        String pubKeyForLogin = clientSDK.getPubKeyForLogin(i2, str, a);
                        if (pubKeyForLogin == null || pubKeyForLogin.equals("")) {
                            if (clientSDK.genPKIKeyNativeForLogin(i2, str, a) != 0) {
                                new StringBuilder("applySecCert() result -> 1").toString();
                                return 1;
                            }
                            pubKeyForLogin = clientSDK.getPubKeyForLogin(i2, str, a);
                        }
                        String[] applySecCertFromMoserver = applySecCertFromMoserver(str4, digestNative, pubKeyForLogin, i2, str, str2, str5, httpHost, httpHost2);
                        if (applySecCertFromMoserver[0] == null || applySecCertFromMoserver[0].equals("")) {
                            new StringBuilder("applySecCert() result -> 1").toString();
                            return 1;
                        } else if (applySecCertFromMoserver[0].equals("105")) {
                            int parseInt = Integer.parseInt(applySecCertFromMoserver[0]);
                            "applySecCert() result -> " + parseInt;
                            return parseInt;
                        } else if (applySecCertFromMoserver[0].equals("107")) {
                            int parseInt2 = Integer.parseInt(applySecCertFromMoserver[0]);
                            "applySecCert() result -> " + parseInt2;
                            return parseInt2;
                        } else if (applySecCertFromMoserver[0].equals("108")) {
                            int parseInt3 = Integer.parseInt(applySecCertFromMoserver[0]);
                            "applySecCert() result -> " + parseInt3;
                            return parseInt3;
                        } else if (applySecCertFromMoserver[0].equals("122")) {
                            int parseInt4 = Integer.parseInt(applySecCertFromMoserver[0]);
                            "applySecCert() result -> " + parseInt4;
                            return parseInt4;
                        } else if (applySecCertFromMoserver[0].equals("500")) {
                            int parseInt5 = Integer.parseInt(applySecCertFromMoserver[0]);
                            "applySecCert() result -> " + parseInt5;
                            return parseInt5;
                        } else {
                            try {
                                int i4 = clientSDK.saveSecCertNativeForLogin(applySecCertFromMoserver[0], applySecCertFromMoserver[1], i2, str, a, str4) == 0 ? 0 : 1;
                                "applySecCert() result -> " + i4;
                                return i4;
                            } catch (ClientSDKException e) {
                                e.printStackTrace();
                                "saveSecCertNative() ClientSDKException -> " + e.toString();
                                return 1;
                            }
                        }
                    } catch (ClientSDKException e2) {
                        e2.printStackTrace();
                        "genPKIKeyNative() ClientSDKException -> " + e2.toString();
                        return 1;
                    }
                } catch (ClientSDKException e3) {
                    e3.printStackTrace();
                    "applySecCert() ClientSDKException -> " + e3.toString();
                    return 1;
                }
            } catch (ClientSDKException e4) {
                e4.printStackTrace();
                "applySecCert() ClientSDKException -> " + e4.toString();
                return 2;
            }
        } else {
            new StringBuilder("applySecCert() result -> 5").toString();
            return 5;
        }
    }

    private static String[] applySecCertFromMoserver(String str, String str2, String str3, int i, String str4, String str5, String str6, HttpHost httpHost, HttpHost httpHost2) {
        String a;
        int i2;
        String[] strArr = new String[2];
        String str7 = "http://" + httpHost.getHostName() + ":" + httpHost.getPort() + "/" + new b().a("applySecCertUrl");
        "applySecCertFromMoserver() applyCertUrl -> " + str7;
        g gVar = new g();
        new h();
        f fVar = new f();
        e eVar = new e();
        if (i == 1) {
            a = eVar.a(str, str6, str3, str2, new StringBuilder(String.valueOf(i)).toString());
            i2 = 65568;
        } else {
            a = eVar.a(str, str6, str3, str2, new StringBuilder(String.valueOf(i)).toString(), str4, str5);
            i2 = 65584;
        }
        "applySecCertFromMoserver() requestXml -> " + a;
        try {
            try {
                ArrayList a2 = fVar.a(h.a(e.a(str7, gVar.a(a, i2), httpHost2)));
                if (a2.get(0) == null || ((com.ccit.mmwlan.vo.a) a2.get(0)).equals("")) {
                    return strArr;
                }
                String a3 = ((com.ccit.mmwlan.vo.a) a2.get(0)).a();
                if (a3 == null || !a3.equals(com.a.a.h.b.SMS_RESULT_SEND_FAIL)) {
                    String c = ((com.ccit.mmwlan.vo.a) a2.get(0)).c();
                    strArr[0] = c;
                    "applySecCertFromMoserver() cert -> " + c;
                    String e = ((com.ccit.mmwlan.vo.a) a2.get(0)).e();
                    strArr[1] = e;
                    "applySecCertFromMoserver() encData -> " + e;
                    return strArr;
                }
                "applySecCertFromMoserver()  Errormsg -> " + ((com.ccit.mmwlan.vo.a) a2.get(0)).b();
                return strArr;
            } catch (Exception e2) {
                "applySecCertFromMoserver() Exception -> " + e2.toString();
                e2.printStackTrace();
                String exc = e2.toString();
                if (exc == null || !exc.contains("returnCode")) {
                    return strArr;
                }
                strArr[0] = exc.split("=")[1].trim();
                return strArr;
            }
        } catch (Exception e3) {
            "applySecCertFromMoserver() Exception -> 与mo.server通信异常\n" + e3.toString();
            e3.printStackTrace();
            return strArr;
        }
    }

    public static int checkSecCert(int i, int i2, String str) {
        int i3 = 1;
        if ((i != 0 && i != 1 && i != 2) || (i2 != 1 && i2 != 2)) {
            new StringBuilder("checkSecCert() result -> 5").toString();
            return 5;
        } else if (i2 != 2 || (str != null && !str.equals(""))) {
            try {
                try {
                    if (clientSDK.checkSecCertNativeForLogin(i2, str, a.a(context, i)) == 0) {
                        i3 = 0;
                    }
                    "checkSecCert() result -> " + i3;
                    return i3;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    "checkSecCertNative() ClientSDKException -> " + e.toString();
                    return 6;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                "checkSecCert() ClientSDKException -> " + e2.toString();
                return 2;
            }
        } else {
            new StringBuilder("checkSecCert() result -> 5").toString();
            return 5;
        }
    }

    public static String encWithCert(String str) {
        String str2 = null;
        if (str == null || str.equals("")) {
            "encWithCert() result -> " + ((String) null);
        } else {
            try {
                str2 = clientSDK.encWithCertNativeForLogin(str, new b().a("uicCert"));
                "encWithCert() result -> " + str2;
            } catch (ClientSDKException e) {
                e.printStackTrace();
                "encWithCert() ClientSDKException -> " + e.toString();
            }
        }
        return str2;
    }

    public static String encWithPubKey(int i, int i2, String str, String str2) {
        if (i != 0 && i != 1 && i != 2) {
            return null;
        }
        if (i2 != 1 && i2 != 2) {
            return null;
        }
        if (i2 == 2 && (str == null || str.equals(""))) {
            "encWithPubKey() error -> parameters error!" + ((String) null);
            return null;
        } else if (str2 == null || str2.equals("")) {
            "encWithPubKey error --> the fourth parameter error!" + ((String) null);
            return null;
        } else {
            try {
                try {
                    return clientSDK.AsymmetricEncryptionForMMLogin(i2, str, a.a(context, i), str2);
                } catch (ClientSDKException e) {
                    "encWithPubKey call AsymmetricEncryptionForMMLogin error --> !" + ((String) null);
                    return null;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                "checkSecCert() ClientSDKException -> " + e2.toString();
                return null;
            }
        }
    }

    public static String genPKIKey(int i, int i2, String str) {
        String str2 = null;
        if ((i == 0 || i == 1 || i == 2) && (i2 == 1 || i2 == 2)) {
            if (i2 != 2 || (str != null && !str.equals(""))) {
                try {
                    DeviceInfo a = a.a(context, i);
                    try {
                        if (clientSDK.genPKIKeyNativeForLogin(i2, str, a) == 0) {
                            try {
                                str2 = clientSDK.getPubKeyForLogin(i2, str, a);
                                "genPKIKey() pubKey -> " + str2;
                            } catch (ClientSDKException e) {
                                e.printStackTrace();
                                "genPKIKeyNative() ClientSDKException -> " + e.toString();
                            }
                        }
                    } catch (ClientSDKException e2) {
                        e2.printStackTrace();
                        "genPKIKeyNative() ClientSDKException -> " + e2.toString();
                    }
                } catch (ClientSDKException e3) {
                    e3.printStackTrace();
                    "checkSecCert() ClientSDKException -> " + e3.toString();
                }
            } else {
                "genPKIKey() error -> parameters error!" + ((String) null);
            }
        }
        return str2;
    }

    public static String genSID() {
        ClientSDKException e;
        String str;
        try {
            str = clientSDK.genSIDNative();
            try {
                "genSID() strResult -> " + str;
            } catch (ClientSDKException e2) {
                e = e2;
                "genSID() 生成SID失败 -> " + e;
                return str;
            }
        } catch (ClientSDKException e3) {
            ClientSDKException clientSDKException = e3;
            str = null;
            e = clientSDKException;
        }
        return str;
    }

    public static String getDigest(String str, String str2) {
        String str3 = null;
        if (str2 == null || str2.equals("")) {
            "getMd5Digest() result -> " + ((String) null);
        } else {
            try {
                str3 = clientSDK.getDigestNative(str, str2);
                "getMd5Digest() result -> " + str3;
            } catch (ClientSDKException e) {
                e.printStackTrace();
                "getDigestNative() ClientSDKException -> " + e.toString();
            }
        }
        return str3;
    }

    public static String getVersion() {
        return "1.1.6";
    }

    public static int initMmClientSdk(Context context2, int i) {
        context = context2;
        try {
            try {
                return clientSDK.transmitInfoNative(a.a(context, i)) == 0 ? 0 : 1;
            } catch (ClientSDKException e) {
                e.printStackTrace();
                "initMmClientSdk() ClientSDKException -> " + e.toString();
                return 1;
            }
        } catch (ClientSDKException e2) {
            e2.printStackTrace();
            "initMmClientSdk() ClientSDKException -> " + e2.toString();
            return 2;
        }
    }

    public static int saveCert(int i, int i2, String str, String str2, String str3, String str4) {
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str3 == null || str3.equals("") || str2 == null || str2.equals("") || str4 == null || str4.equals(""))) {
            new StringBuilder("saveCert() parameter invalid -> 1").toString();
            return 1;
        } else if (i2 != 2 || (str != null && !str.equals(""))) {
            try {
                try {
                    int i3 = clientSDK.saveSecCertNativeForLogin(str3, str4, i2, str, a.a(context, i), str2) == 0 ? 0 : 1;
                    "saveCert() result -> " + i3;
                    return i3;
                } catch (ClientSDKException e) {
                    e.printStackTrace();
                    "saveCert(...) call saveSecCertNativeForLogin() throw ClientSDKException -> " + e.toString();
                    return 1;
                }
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                "saveCert(...) call getDeviceInfo(...) ClientSDKException -> " + e2.toString();
                return 1;
            }
        } else {
            new StringBuilder("saveCert() the third parameter invalid -> 1").toString();
            return 1;
        }
    }

    public static int updateRandNum(int i, int i2, String str, String str2, String str3) {
        int i3;
        if ((i != 0 && i != 1 && i != 2) || ((i2 != 1 && i2 != 2) || str2 == null || str2.equals("") || str3 == null || str3.equals(""))) {
            new StringBuilder("updateRandNum() parameter invalid -> 1").toString();
            return 1;
        } else if (i2 != 2 || (str != null && !str.equals(""))) {
            try {
                try {
                    i3 = clientSDK.UpdateRandNumForLogin(str3, i2, str, a.a(context, i), str2);
                    "updateRandNum() call UpdateRandNumForLogin(...) iResult -> " + i3;
                    if (i3 != 0) {
                        return 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    i3 = 1;
                }
                return i3;
            } catch (ClientSDKException e2) {
                e2.printStackTrace();
                "updateRandNum() call getDeviceInfo(...) ClientSDKException -> " + e2.toString();
                return 1;
            }
        } else {
            new StringBuilder("updateRandNum() the third parameter error -> 1").toString();
            return 1;
        }
    }
}
