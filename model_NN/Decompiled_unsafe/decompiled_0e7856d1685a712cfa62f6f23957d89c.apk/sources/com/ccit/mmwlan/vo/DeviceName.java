package com.ccit.mmwlan.vo;

public class DeviceName {
    private String deviceName;
    private String errormsg;
    private String result;

    public String getDeviceName() {
        return this.deviceName;
    }

    public String getErrormsg() {
        return this.errormsg;
    }

    public String getResult() {
        return this.result;
    }

    public void setDeviceName(String str) {
        this.deviceName = str;
    }

    public void setErrormsg(String str) {
        this.errormsg = str;
    }

    public void setResult(String str) {
        this.result = str;
    }
}
