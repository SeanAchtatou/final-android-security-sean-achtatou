package com.ccit.mmwlan.vo;

public class DeviceInfo {
    private String filePath = "";
    private String strImei = "";
    private String strImsi = "";
    private String strMac = "";

    public String getFilePath() {
        return this.filePath;
    }

    public String getStrImei() {
        return this.strImei;
    }

    public String getStrImsi() {
        return this.strImsi;
    }

    public String getStrMac() {
        return this.strMac;
    }

    public void setFilePath(String str) {
        this.filePath = str;
    }

    public void setStrImei(String str) {
        this.strImei = str;
    }

    public void setStrImsi(String str) {
        this.strImsi = str;
    }

    public void setStrMac(String str) {
        this.strMac = str;
    }

    public String toString() {
        return "DeviceInfo ( " + super.toString() + "    strImei = " + this.strImei + "    strImsi = " + this.strImsi + "    strMac = " + this.strMac + "    filePath = " + this.filePath + "     )";
    }
}
