package com.ccit.mmwlan.exception;

public final class ClientSDKException extends Exception {
    private static final long serialVersionUID = 6277968152045563133L;

    public ClientSDKException() {
    }

    public ClientSDKException(String str) {
        super(str);
    }
}
