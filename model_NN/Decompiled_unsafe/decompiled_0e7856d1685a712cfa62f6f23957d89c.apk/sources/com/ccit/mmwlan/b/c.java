package com.ccit.mmwlan.b;

import java.lang.reflect.Method;

public final class c {
    private static Object a(Class cls, String str, Class[] clsArr, Object[] objArr) {
        try {
            Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(null, objArr);
        } catch (NoSuchMethodException e) {
            e.getMessage();
            return null;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    public static Object a(Object obj, String str, Class[] clsArr, Object[] objArr) {
        try {
            return obj.getClass().getMethod(str, clsArr).invoke(obj, objArr);
        } catch (NoSuchMethodException e) {
            e.getMessage();
            return null;
        } catch (Exception e2) {
            e2.getMessage();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ccit.mmwlan.b.c.a(java.lang.Class, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.String, java.lang.Class[], java.lang.Object[]]
     candidates:
      com.ccit.mmwlan.b.c.a(java.lang.Object, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
      com.ccit.mmwlan.b.c.a(java.lang.String, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
      com.ccit.mmwlan.b.c.a(java.lang.Class, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object */
    public static Object a(String str, String str2, Class[] clsArr, Object[] objArr) {
        try {
            return a((Class) Class.forName(str), str2, clsArr, objArr);
        } catch (ClassNotFoundException e) {
            e.getMessage();
            return null;
        }
    }
}
