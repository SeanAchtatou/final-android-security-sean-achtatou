package com.ccit.mmwlan.phone;

import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public final class d {
    private a a = null;
    private ArrayList b = null;

    public final ArrayList a(String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            this.a = new a();
            xMLReader.setContentHandler(this.a);
            xMLReader.parse(new InputSource(new StringReader(str)));
            this.b = this.a.a();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.b;
    }
}
