package com.ccit.mmwlan.phone;

public class IPDress_ForPhone {
    private String strApplySecCertIP;
    private String strSMSNumber;

    public String getStrApplySecCertIP() {
        return this.strApplySecCertIP;
    }

    public String getStrSMSNumber() {
        return this.strSMSNumber;
    }

    public void setStrApplySecCertIP(String str) {
        this.strApplySecCertIP = str;
    }

    public void setStrSMSNumber(String str) {
        this.strSMSNumber = str;
    }
}
