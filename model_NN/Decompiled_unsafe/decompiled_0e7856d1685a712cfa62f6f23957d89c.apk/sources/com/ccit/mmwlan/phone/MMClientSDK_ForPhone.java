package com.ccit.mmwlan.phone;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import com.a.a.h.b;
import com.ccit.mmwlan.ClientSDK;
import com.ccit.mmwlan.vo.DeviceInfo;
import com.ccit.mmwlan.vo.SignView;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpHost;

public final class MMClientSDK_ForPhone {
    private static final int INT_RESULT_0 = 0;
    private static final int INT_RESULT_1 = 1;
    private static final int INT_RESULT_2 = 2;
    private static final int INT_RESULT_3 = 3;
    private static final int INT_RESULT_4 = 4;
    private static final int INT_RESULT_5 = 5;
    private static final int INT_RESULT_6 = 6;
    private static final int INT_RESULT_7 = 7;
    private static final String MMCLIENT_SDK = "MMClientSDK_ForPhone";
    private static String SMSNumber = null;
    private static ClientSDK clientSDK;
    private static Context context = null;
    private static String strApplyCertForPhone = null;

    static {
        clientSDK = null;
        clientSDK = new ClientSDK();
    }

    public static int DestorySecCert(String str) {
        Exception e;
        int i;
        try {
            i = clientSDK.DestorySecCertForBilling(str);
            try {
                "DestroySecCert() iRet -> " + i;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return i;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            i = -1;
            e = exc;
        }
        return i;
    }

    public static String RSAEncryptWithPubKey(String str) {
        try {
            String AsymmetricEncryptionForBilling = clientSDK.AsymmetricEncryptionForBilling(str);
            if (AsymmetricEncryptionForBilling == null) {
                AsymmetricEncryptionForBilling = String.valueOf(1);
            }
            "RSAEncryptWithPubKey() strRet -> " + AsymmetricEncryptionForBilling;
            return AsymmetricEncryptionForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(1);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static SignView SIDSign(String str, HttpHost httpHost) {
        SignView signView = new SignView();
        if (str == null) {
            signView.setResult(6);
        } else {
            int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
            "SIDSign() iCertState -> " + checkSecCertNativeForBilling;
            if (checkSecCertNativeForBilling == 0) {
                String genSID = genSID();
                if (b.SMS_RESULT_SEND_CANCEL.equals(genSID)) {
                    signView.setResult(2);
                } else {
                    String sIDSignValue = getSIDSignValue(genSID, str);
                    if (sIDSignValue == null) {
                        signView.setResult(7);
                    } else {
                        signView.setResult(0);
                        signView.setUserSignature(sIDSignValue);
                    }
                }
            } else if (1 == checkSecCertNativeForBilling) {
                int sendMessageAndApplyCert = sendMessageAndApplyCert(str, httpHost);
                if (sendMessageAndApplyCert != 0) {
                    signView.setResult(sendMessageAndApplyCert);
                } else {
                    String genSID2 = genSID();
                    if (b.SMS_RESULT_SEND_CANCEL.equals(genSID2)) {
                        signView.setResult(2);
                    } else {
                        String sIDSignValue2 = getSIDSignValue(genSID2, str);
                        if (sIDSignValue2 == null) {
                            signView.setResult(7);
                        } else {
                            signView.setResult(0);
                            signView.setUserSignature(sIDSignValue2);
                        }
                    }
                }
            } else if (2 == checkSecCertNativeForBilling) {
                String genSID3 = genSID();
                if (b.SMS_RESULT_SEND_CANCEL.equals(genSID3)) {
                    signView.setResult(2);
                } else {
                    try {
                        int applySecCertMethod = applySecCertMethod(genSID3, str, httpHost);
                        "SIDSign() iResult -> " + applySecCertMethod;
                        if (applySecCertMethod == 0) {
                            String genSID4 = genSID();
                            if (b.SMS_RESULT_SEND_CANCEL.equals(genSID4)) {
                                signView.setResult(2);
                            } else {
                                String sIDSignValue3 = getSIDSignValue(genSID4, str);
                                if (sIDSignValue3 == null) {
                                    signView.setResult(7);
                                } else {
                                    signView.setResult(applySecCertMethod);
                                    signView.setUserSignature(sIDSignValue3);
                                }
                            }
                        } else {
                            signView.setResult(applySecCertMethod);
                        }
                    } catch (Exception e) {
                        signView.setResult(6);
                        e.printStackTrace();
                    }
                }
            } else {
                int sendMessageAndApplyCert2 = sendMessageAndApplyCert(str, httpHost);
                if (sendMessageAndApplyCert2 != 0) {
                    signView.setResult(sendMessageAndApplyCert2);
                } else {
                    String genSID5 = genSID();
                    if (b.SMS_RESULT_SEND_CANCEL.equals(genSID5)) {
                        signView.setResult(2);
                    } else {
                        String sIDSignValue4 = getSIDSignValue(genSID5, str);
                        if (sIDSignValue4 == null) {
                            signView.setResult(7);
                        } else {
                            signView.setResult(0);
                            signView.setUserSignature(sIDSignValue4);
                        }
                    }
                }
            }
        }
        return signView;
    }

    private static String applyCertPrivate(c cVar, d dVar, String str, String str2, HttpHost httpHost, String str3) {
        int updateRandNum;
        byte[] a = c.a(str, str2.getBytes("utf-8"), httpHost, str3);
        "applyCertPrivate() -> " + new String(a);
        ArrayList a2 = dVar.a(new String(a));
        String a3 = ((b) a2.get(0)).a();
        if (!b.SMS_RESULT_SEND_SUCCESS.equals(a3)) {
            return a3;
        }
        String b = ((b) a2.get(0)).b();
        String d = ((b) a2.get(0)).d();
        if (d != null && (updateRandNum = updateRandNum(d)) != 0) {
            return String.valueOf(updateRandNum);
        }
        int saveSecCertNativeForBilling = clientSDK.saveSecCertNativeForBilling(b, null);
        "applyCertPrivate() iResult -> " + saveSecCertNativeForBilling;
        return String.valueOf(saveSecCertNativeForBilling);
    }

    public static int applySecCert(String str, HttpHost httpHost) {
        int i;
        if (str == null) {
            return 6;
        }
        String genSID = genSID();
        if (b.SMS_RESULT_SEND_CANCEL.equals(genSID)) {
            return 2;
        }
        try {
            i = applySecCertMethod(genSID, str, httpHost);
            "applySecCert() iResult -> " + i;
        } catch (Exception e) {
            e.printStackTrace();
            i = 6;
        }
        return i;
    }

    private static int applySecCertMethod(String str, String str2, HttpHost httpHost) {
        String str3;
        int updateRandNum;
        String str4 = "http://" + strApplyCertForPhone + "/mmwlan/applySecCertForAPPThird";
        "applySecCertMethod() strUrl -> " + str4;
        int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
        "applySecCertMethod() iCertState -> " + checkSecCertNativeForBilling;
        if (checkSecCertNativeForBilling == 2) {
            str3 = getGenPubkey();
            if (b.SMS_RESULT_SEND_NOTIFIED.equals(str3)) {
                return 4;
            }
        } else if (3 == genPKIKey()) {
            return 3;
        } else {
            str3 = getGenPubkey();
            if (b.SMS_RESULT_SEND_NOTIFIED.equals(str3)) {
                return 4;
            }
        }
        String str5 = str3;
        String imsiOfMD5Value = imsiOfMD5Value();
        if ("5".equals(imsiOfMD5Value)) {
            return 5;
        }
        c cVar = new c();
        String a = cVar.a(str, str5, imsiOfMD5Value, str2);
        "applySecCertMethod() requestXML -> " + a;
        d dVar = new d();
        byte[] a2 = c.a(str4, a.getBytes("utf-8"), httpHost, b.SMS_RESULT_RECV_SUCCESS);
        "applySecCertMethod() byResponse -> " + new String(a2);
        ArrayList a3 = dVar.a(new String(a2));
        String a4 = ((b) a3.get(0)).a();
        "applySecCertMethod() strApplyCertResult -> " + a4;
        if (b.SMS_RESULT_SEND_SUCCESS.equals(a4)) {
            String b = ((b) a3.get(0)).b();
            "applySecCertMethod() strDynPdworld -> " + ((b) a3.get(0)).c();
            String d = ((b) a3.get(0)).d();
            if (d != null && (updateRandNum = updateRandNum(d)) != 0) {
                return updateRandNum;
            }
            int saveSecCertNativeForBilling = clientSDK.saveSecCertNativeForBilling(b, null);
            "applySecCertMethod() iResult -> " + saveSecCertNativeForBilling;
            return saveSecCertNativeForBilling;
        } else if (!"7".equals(a4)) {
            return Integer.parseInt(a4);
        } else {
            for (int i = 0; i < 10; i++) {
                String applyCertPrivate = applyCertPrivate(cVar, dVar, str4, a, httpHost, b.SMS_RESULT_SEND_CANCEL);
                if (!"7".equals(applyCertPrivate)) {
                    return Integer.parseInt(applyCertPrivate);
                }
            }
            return 1;
        }
    }

    public static int checkSecCert() {
        try {
            int checkSecCertNativeForBilling = clientSDK.checkSecCertNativeForBilling();
            "checkSecCert()  iResult -> " + checkSecCertNativeForBilling;
            return checkSecCertNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    private static int genPKIKey() {
        try {
            int genPKIKeyNativeForBilling = clientSDK.genPKIKeyNativeForBilling();
            "genPKIKey() iResult -> " + genPKIKeyNativeForBilling;
            return genPKIKeyNativeForBilling;
        } catch (Exception e) {
            e.printStackTrace();
            return 3;
        }
    }

    private static String genSID() {
        try {
            String genSIDNative = clientSDK.genSIDNative();
            if (genSIDNative == null) {
                genSIDNative = String.valueOf(2);
            }
            "genSID() strResult -> " + genSIDNative;
            return genSIDNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getDeviceID() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (deviceId == null) {
            sb.append(((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress().toString());
            "getDeviceID_PAD() MacAddress -> " + sb.toString();
            return sb.toString();
        }
        sb.append(deviceId);
        "getDeviceID_PAD() strIMEI -> " + sb.toString();
        return sb.toString();
    }

    private static String getGenPubkey() {
        try {
            String pubKeyForBilling = clientSDK.getPubKeyForBilling();
            if (pubKeyForBilling == null) {
                return String.valueOf(4);
            }
            "getGenPubkey()  strResult -> " + pubKeyForBilling;
            return pubKeyForBilling;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(4);
            exc.printStackTrace();
            return valueOf;
        }
    }

    public static String getIMSI() {
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (subscriberId == null) {
            sb.append(String.valueOf(1));
            "getIMSI() errorValue -> " + sb.toString();
            return sb.toString();
        }
        sb.append(subscriberId);
        "getIMSI() IMSI -> " + sb.toString();
        return sb.toString();
    }

    private static String getSIDSignValue(String str, String str2) {
        Exception e;
        String str3;
        try {
            str3 = clientSDK.SIDSignNativeForBilling(str, str2, null);
            try {
                "getSIDSignValue() -> " + str3;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return str3;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            str3 = null;
            e = exc;
        }
        return str3;
    }

    public static String getVersion() {
        return "1.1.6";
    }

    private static String imsiOfMD5Value() {
        StringBuilder sb = new StringBuilder();
        String subscriberId = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        if (subscriberId == null) {
            sb.setLength(0);
            sb.append(String.valueOf(5).toString());
            return sb.toString().trim();
        }
        try {
            String digestNative = clientSDK.getDigestNative("md5", subscriberId);
            if (digestNative == null) {
                sb.setLength(0);
                sb.append(String.valueOf(5).toString());
                return sb.toString().trim();
            }
            "imsiOfMD5Value() strMD5Result -> " + digestNative;
            sb.setLength(0);
            sb.append(digestNative.toString());
            return sb.toString().trim();
        } catch (Exception e) {
            sb.setLength(0);
            sb.append(String.valueOf(5).toString());
            e.printStackTrace();
        }
    }

    private static int initialImsiAndImeiValue() {
        DeviceInfo deviceInfo = new DeviceInfo();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String subscriberId = telephonyManager.getSubscriberId();
        if (subscriberId == null) {
            return 1;
        }
        deviceInfo.setStrImsi(subscriberId);
        "strIMSI -> " + subscriberId;
        String deviceId = telephonyManager.getDeviceId();
        if (deviceId == null) {
            deviceId = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress().toString();
            "initialImsiAndImeiValue() strIMEI MacAddress -> " + deviceId;
        }
        deviceInfo.setStrImei(deviceId);
        "strIMEI -> " + deviceId;
        String str = context.getFilesDir().getPath().toString();
        deviceInfo.setFilePath(str);
        "FilePath -> " + str;
        try {
            int transmitInfoNative = clientSDK.transmitInfoNative(deviceInfo);
            "initialImsiAndImeiValue() iResult -> " + transmitInfoNative;
            return transmitInfoNative != 0 ? 4 : 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int initialMMSDK(Context context2, IPDress_ForPhone iPDress_ForPhone) {
        context = context2;
        if (iPDress_ForPhone == null || iPDress_ForPhone.getStrApplySecCertIP() == null || iPDress_ForPhone.getStrSMSNumber() == null) {
            return 5;
        }
        strApplyCertForPhone = iPDress_ForPhone.getStrApplySecCertIP();
        SMSNumber = iPDress_ForPhone.getStrSMSNumber();
        "initialMMSDK() strIPDress -> " + strApplyCertForPhone + "  :  " + SMSNumber;
        int initialImsiAndImeiValue = initialImsiAndImeiValue();
        "initialMMSDK() iResult -> " + initialImsiAndImeiValue;
        return initialImsiAndImeiValue;
    }

    public static String md5Algorithm(String str) {
        if (str == null) {
            return String.valueOf(1);
        }
        try {
            String digestNative = clientSDK.getDigestNative("md5", str);
            return digestNative == null ? String.valueOf(2) : digestNative;
        } catch (Exception e) {
            Exception exc = e;
            String valueOf = String.valueOf(2);
            exc.printStackTrace();
            return valueOf;
        }
    }

    private static int sendMessageAndApplyCert(String str, HttpHost httpHost) {
        String genSID = genSID();
        b.SMS_RESULT_SEND_CANCEL.equals(genSID);
        String imsiOfMD5Value = imsiOfMD5Value();
        "5".equals(imsiOfMD5Value);
        StringBuilder sb = new StringBuilder();
        sb.setLength(0);
        sb.append("MM#WLAN#").append(imsiOfMD5Value.toString().trim()).append("#").append(genSID.toString().trim()).append("#").append(str.toString());
        "sendMessageAndApplyCert() sendMessage -> " + sb.toString();
        SmsManager smsManager = SmsManager.getDefault();
        if (sb.toString().trim().length() > 70) {
            Iterator<String> it = smsManager.divideMessage(sb.toString().trim()).iterator();
            while (it.hasNext()) {
                smsManager.sendTextMessage(SMSNumber, null, it.next(), null, null);
            }
        } else {
            smsManager.sendTextMessage(SMSNumber, null, sb.toString().trim(), null, null);
        }
        try {
            int applySecCertMethod = applySecCertMethod(genSID, str, httpHost);
            "sendMessageAndApplyCert() -> " + applySecCertMethod;
            return applySecCertMethod;
        } catch (Exception e) {
            e.printStackTrace();
            return 6;
        }
    }

    public static int updateRandNum(String str) {
        int i;
        if (str == null) {
            return 1;
        }
        try {
            i = clientSDK.UpdateRandNumForBilling(str);
            if (i != 0) {
                return 1;
            }
            "updateRandNum()  iResult -> " + i;
            return i;
        } catch (Exception e) {
            e.printStackTrace();
            i = 1;
        }
    }
}
