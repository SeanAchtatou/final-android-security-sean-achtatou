package com.ccit.mmwlan.a;

import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import mm.purchasesdk.PurchaseCode;

public final class g {
    private static String a = "Default";
    private static int b = -16909061;
    private static int c = PurchaseCode.NOT_CMCC_ERR;
    private static int d = 0;
    private short e = 0;
    private ArrayList f = new ArrayList();
    private ArrayList g = new ArrayList();

    private static byte[] a(int i) {
        byte[] bArr = new byte[4];
        bArr[3] = (byte) (i & PurchaseCode.AUTH_INVALID_APP);
        bArr[2] = (byte) ((65280 & i) >> 8);
        bArr[1] = (byte) ((16711680 & i) >> 16);
        bArr[0] = (byte) ((int) ((-16777216 & ((long) i)) >> 24));
        return bArr;
    }

    private static byte[] a(short s) {
        byte[] bArr = new byte[2];
        bArr[1] = (byte) (s & 255);
        bArr[0] = (byte) ((65280 & s) >> 8);
        return bArr;
    }

    public final byte[] a(String str, int i) {
        byte[] byteArray;
        if (str != null) {
            try {
                byte[] bytes = str.getBytes(e.f);
                if (bytes != null) {
                    this.f.add(String.valueOf(2));
                    this.g.add(bytes);
                    this.e = (short) (this.e + 1);
                }
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
        }
        if (this.e == 0) {
            byteArray = null;
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byteArrayOutputStream.write(a(this.e));
            for (short s = 0; s < this.e; s++) {
                byteArrayOutputStream.write(a((short) (s + 1)));
                short parseShort = Short.parseShort((String) this.f.get(s));
                byteArrayOutputStream.write(a(parseShort));
                byte[] bArr = (byte[]) this.g.get(s);
                byteArrayOutputStream.write(a(bArr.length));
                byteArrayOutputStream.write(bArr);
                if (parseShort == 2) {
                    String str2 = a;
                    "request message body :" + new String(bArr, e.f);
                }
            }
            byteArray = byteArrayOutputStream.toByteArray();
        }
        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        byteArrayOutputStream2.write(a(b));
        byteArrayOutputStream2.write(a(byteArray.length + 20));
        byteArrayOutputStream2.write(a(i));
        byteArrayOutputStream2.write(a(c));
        byteArrayOutputStream2.write(a(0));
        byteArrayOutputStream2.write(byteArray);
        return byteArrayOutputStream2.toByteArray();
    }
}
