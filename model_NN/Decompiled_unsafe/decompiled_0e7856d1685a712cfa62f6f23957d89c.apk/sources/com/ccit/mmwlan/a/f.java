package com.ccit.mmwlan.a;

import java.io.StringReader;
import java.util.ArrayList;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public final class f {
    private a a = null;
    private ArrayList b = null;
    private c c = null;
    private ArrayList d = null;
    private b e = null;
    private ArrayList f = null;

    public final ArrayList a(String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            this.a = new a();
            xMLReader.setContentHandler(this.a);
            xMLReader.parse(new InputSource(new StringReader(str)));
            this.b = this.a.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.b;
    }

    public final ArrayList b(String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            this.c = new c();
            xMLReader.setContentHandler(this.c);
            xMLReader.parse(new InputSource(new StringReader(str)));
            this.d = this.c.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.d;
    }

    public final ArrayList c(String str) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            this.e = new b();
            xMLReader.setContentHandler(this.e);
            xMLReader.parse(new InputSource(new StringReader(str)));
            this.f = this.e.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return this.f;
    }
}
