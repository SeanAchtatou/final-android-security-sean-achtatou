package com.ccit.mmwlan.a;

import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public final class d {
    private StringBuilder a;

    public static byte[] a(String str, byte[] bArr) {
        int length = bArr.length;
        URL url = new URL(str);
        "doPost() url -> " + url.toString();
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("Connection", "close");
        httpURLConnection.setRequestProperty("Charset", e.f);
        httpURLConnection.setRequestProperty("Content-Type", "text/xml");
        httpURLConnection.setRequestProperty("Content-length", String.valueOf(length));
        httpURLConnection.setConnectTimeout(8000);
        httpURLConnection.setReadTimeout(8000);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        OutputStream outputStream = httpURLConnection.getOutputStream();
        outputStream.write(bArr);
        outputStream.flush();
        outputStream.close();
        DataInputStream dataInputStream = new DataInputStream(httpURLConnection.getInputStream());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[6000];
        while (true) {
            int read = dataInputStream.read(bArr2);
            if (read < 0) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    public final String a(String str) {
        this.a = null;
        this.a = new StringBuilder();
        this.a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.a.append("<request>");
        this.a.append("<deviceID>").append(str).append("</deviceID>");
        this.a.append("</request>");
        return this.a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        this.a = null;
        this.a = new StringBuilder();
        this.a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.a.append("<request>");
        this.a.append("<appid>").append(str).append("</appid>");
        this.a.append("<DeviceId>").append(str2).append("</DeviceId>");
        this.a.append("<sid>").append(str3).append("</sid>");
        this.a.append("<pubkey>").append(str4).append("</pubkey>");
        if (str5 == null) {
            this.a.append("<deviceAuthorizationCode></deviceAuthorizationCode>");
        } else {
            this.a.append("<deviceAuthorizationCode>").append(str5).append("</deviceAuthorizationCode>");
        }
        this.a.append("</request>");
        return this.a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.a = null;
        this.a = new StringBuilder();
        this.a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.a.append("<request>");
        this.a.append("<appid>").append(str).append("</appid>");
        this.a.append("<mobilePhone>").append(str2).append("</mobilePhone>");
        this.a.append("<deviceID>").append(str3).append("</deviceID>");
        this.a.append("<mac></mac>");
        this.a.append("<deviceName>").append(str5).append("</deviceName>");
        this.a.append("<MODEL></MODEL>");
        this.a.append("<VERSION></VERSION>");
        this.a.append("</request>");
        return this.a.toString();
    }
}
