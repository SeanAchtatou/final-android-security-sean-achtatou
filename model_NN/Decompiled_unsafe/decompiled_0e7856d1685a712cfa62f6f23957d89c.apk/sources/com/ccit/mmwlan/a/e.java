package com.ccit.mmwlan.a;

import com.a.a.e.a;
import com.ccit.mmwlan.b.b;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import org.apache.http.HttpHost;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class e {
    private static b b;
    private static String c;
    private static String d = b.a("readTimeout");
    private static final int e = Integer.parseInt(c);
    private static final int f = Integer.parseInt(d);
    private StringBuilder a;

    static {
        b bVar = new b();
        b = bVar;
        c = bVar.a("connectTimeout");
    }

    public static byte[] a(String str, byte[] bArr, HttpHost httpHost) {
        "doPostByHttpClient() url -> " + str;
        "doPostByHttpClient() request -> " + new String(bArr, com.umeng.common.b.e.f);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpParams params = defaultHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, e);
        HttpConnectionParams.setSoTimeout(params, f);
        if (!(httpHost == null || httpHost.getHostName() == null || httpHost.getHostName().equals(""))) {
            defaultHttpClient.getParams().setParameter("http.route.default-proxy", httpHost);
            "doPostByHttpClient()  used wapHost -> " + httpHost.getHostName() + ":" + httpHost.getPort();
        }
        HttpPost httpPost = new HttpPost(str);
        httpPost.setHeader("Charset", com.umeng.common.b.e.f);
        httpPost.setHeader("Content-Type", "text/xml");
        httpPost.setEntity(new ByteArrayEntity(bArr));
        DataInputStream dataInputStream = new DataInputStream(defaultHttpClient.execute(httpPost).getEntity().getContent());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr2 = new byte[a.GAME_D_PRESSED];
        while (true) {
            int read = dataInputStream.read(bArr2);
            if (read < 0) {
                defaultHttpClient.getConnectionManager().shutdown();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                "doPost() response -> " + new String(byteArray, com.umeng.common.b.e.f);
                return byteArray;
            }
            byteArrayOutputStream.write(bArr2, 0, read);
            byteArrayOutputStream.flush();
        }
    }

    public final String a(String str, String str2, String str3, String str4, String str5) {
        this.a = new StringBuilder();
        this.a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.a.append("<request>");
        this.a.append("<sid>").append(str2).append("</sid>");
        this.a.append("<pubkey>").append(str3).append("</pubkey>");
        this.a.append("<imsi>").append(str4).append("</imsi>");
        this.a.append("<id_mode>").append(str5).append("</id_mode>");
        this.a.append("<appid>").append(str).append("</appid>");
        this.a.append("</request>");
        return this.a.toString();
    }

    public final String a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        this.a = new StringBuilder();
        this.a.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        this.a.append("<request>");
        this.a.append("<sid>").append(str2).append("</sid>");
        this.a.append("<pubkey>").append(str3).append("</pubkey>");
        this.a.append("<deviceid>").append(str4).append("</deviceid>");
        this.a.append("<loginType>").append(str5).append("</loginType>");
        this.a.append("<userName>").append(str6).append("</userName>");
        this.a.append("<passCode>").append(str7).append("</passCode>");
        this.a.append("<appid>").append(str).append("</appid>");
        this.a.append("</request>");
        return this.a.toString();
    }
}
