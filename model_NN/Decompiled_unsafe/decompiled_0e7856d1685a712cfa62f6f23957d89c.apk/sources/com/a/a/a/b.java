package com.a.a.a;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.ArrayList;
import java.util.UUID;
import java.util.Vector;
import org.meteoroid.core.k;

public final class b {
    public static final int CACHED = 0;
    public static final int GIAC = 10390323;
    public static final int LIAC = 10390272;
    public static final int NOT_DISCOVERABLE = 0;
    public static final int PREKNOWN = 1;
    public static final b fQ = new b();
    protected static c fY;
    public UUID fR;
    protected final BroadcastReceiver fS = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.device.action.FOUND".equals(intent.getAction())) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                new h(bluetoothDevice);
                new a(bluetoothDevice.getBluetoothClass());
                "name = " + bluetoothDevice.getName();
                "add = " + bluetoothDevice.getAddress();
                if (bluetoothDevice.getBluetoothClass().getMajorDeviceClass() == 512 && bluetoothDevice.getBluetoothClass().getDeviceClass() == 524) {
                    b.this.fZ.add(bluetoothDevice);
                }
                c cVar = b.this.fU;
            }
        }
    };
    protected final BroadcastReceiver fT = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(intent.getAction())) {
                c cVar = b.this.fU;
                k.getActivity().unregisterReceiver(b.this.fS);
                k.getActivity().unregisterReceiver(b.this.fT);
            }
        }
    };
    c fU;
    boolean fV = false;
    ArrayList<a> fW = new ArrayList<>();
    public Thread fX = new Thread() {
        public final void run() {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < b.this.fW.size()) {
                    try {
                        BluetoothDevice bluetoothDevice = b.this.fW.get(i2).gb.gh;
                        "server name = " + bluetoothDevice.getName();
                        new g[1][0] = new g(bluetoothDevice, i.fR.toString());
                        c cVar = b.fY;
                        int i3 = b.this.fW.get(i2).index;
                    } catch (Exception e) {
                    }
                    c cVar2 = b.fY;
                    int i4 = b.this.fW.get(i2).index;
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    };
    public Vector<BluetoothDevice> fZ = new Vector<>();
    int index = 0;

    protected class a {
        h gb;
        int index;
    }
}
