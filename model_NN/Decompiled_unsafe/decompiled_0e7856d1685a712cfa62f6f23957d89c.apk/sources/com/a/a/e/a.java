package com.a.a.e;

import com.a.a.d.b;
import org.meteoroid.core.c;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class a extends b {
    public static final int DOWN_PRESSED = 64;
    public static final int FIRE_PRESSED = 256;
    public static final int GAME_A_PRESSED = 512;
    public static final int GAME_B_PRESSED = 1024;
    public static final int GAME_C_PRESSED = 2048;
    public static final int GAME_D_PRESSED = 4096;
    public static final int LEFT_PRESSED = 4;
    public static final int RIGHT_PRESSED = 32;
    public static final int UP_PRESSED = 2;
    private int gT;

    private static final boolean q(int i) {
        return i == 6 || i == 1 || i == 2 || i == 5 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12;
    }

    public final void n(int i, int i2) {
        com.a.a.i.a aVar = c.mf;
        int G = MIDPDevice.G(i2);
        if (i == 0 && q(G)) {
            this.gT = (1 << G) | this.gT;
        } else if (i == 1 && q(G)) {
            this.gT = (1 << G) ^ this.gT;
        }
    }
}
