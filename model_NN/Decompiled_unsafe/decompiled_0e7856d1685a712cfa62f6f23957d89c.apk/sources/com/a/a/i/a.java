package com.a.a.i;

public interface a {
    public static final int MSG_DEVICE_KEY_EVENT = 44285;
    public static final int MSG_DEVICE_REQUEST_REFRESH = 44287;
    public static final int MSG_DEVICE_TOUCH_EVENT = 44286;

    int getHeight();

    int getWidth();

    void onCreate();

    void onDestroy();
}
