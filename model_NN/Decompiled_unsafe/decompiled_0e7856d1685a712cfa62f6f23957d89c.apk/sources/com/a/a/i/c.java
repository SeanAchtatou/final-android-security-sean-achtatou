package com.a.a.i;

import android.util.AttributeSet;
import org.meteoroid.core.e;
import org.meteoroid.core.f;

public interface c {
    public static final String LOG_TAG = "VirtualDevice";

    public interface a extends e.a, f.b {
        void a(AttributeSet attributeSet, String str);

        void a(c cVar);

        boolean dA();

        boolean isTouchable();

        void setVisible(boolean z);
    }

    void aF(String str);

    int getOrientation();

    void onCreate();

    void onDestroy();
}
