package com.a.a.j;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import com.androidbox.hywxfktj4.R;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import org.meteoroid.core.e;
import org.meteoroid.core.k;

public final class c {
    public static final String LOG_TAG = "ResourceUtils";
    private static String rj;
    private static final HashMap<String, Bitmap> rk = new HashMap<>();

    public static void aJ(String str) {
        if (str != null) {
            rj = str + File.separator;
        } else {
            rj = null;
        }
    }

    public static final String aK(String str) {
        return "abresource" + File.separator + str;
    }

    private static int aL(String str) {
        try {
            return b(R.drawable.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get resource id:" + str);
        }
    }

    public static int aM(String str) {
        try {
            return b(R.raw.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get raw id:" + str);
        }
    }

    public static Rect aN(String str) {
        if (str == null) {
            Log.w(LOG_TAG, "Rect string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length != 4) {
            throw new IllegalArgumentException("Invalid rect string:" + str);
        }
        int parseInt = Integer.parseInt(split[0].trim());
        int parseInt2 = Integer.parseInt(split[1].trim());
        return new Rect(parseInt, parseInt2, Integer.parseInt(split[2].trim()) + parseInt, Integer.parseInt(split[3].trim()) + parseInt2);
    }

    public static Bitmap aO(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        } else if (str.trim().length() == 0) {
            return null;
        } else {
            if (rk.containsKey(str)) {
                "Wow, " + str + " hit the bitmap cache.";
                return rk.get(str);
            }
            if (rj != null) {
                Bitmap c = e.c(k.az(rj + str + ".png"));
                rk.put(str, c);
                return c;
            }
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(k.getActivity().getResources(), aL(str.trim()));
                rk.put(str, decodeResource);
                return decodeResource;
            } catch (Exception e) {
                throw new IOException("Fail to load bitmap " + str + ":" + e.toString());
            }
        }
    }

    public static Bitmap[] aP(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length <= 0) {
            return null;
        }
        Bitmap[] bitmapArr = new Bitmap[split.length];
        for (int i = 0; i < split.length; i++) {
            bitmapArr[i] = aO(split[i].trim());
        }
        return bitmapArr;
    }

    public static final String aQ(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? "" : str.substring(0, lastIndexOf + 1);
    }

    public static final String aR(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    private static int b(Class<?> cls, String str) {
        return cls.getField(str).getInt(null);
    }

    public static void dU() {
        rk.clear();
    }
}
