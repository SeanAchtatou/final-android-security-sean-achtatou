package com.a.a.j;

import android.util.Log;
import java.util.HashMap;

public final class b {
    private HashMap<String, String> ri = new HashMap<>();

    public b(String str) {
        if (str != null && str.length() > 0) {
            String[] split = str.split(",");
            for (int i = 0; i < split.length; i++) {
                int indexOf = split[i].indexOf(":");
                if (indexOf != -1) {
                    String trim = split[i].substring(0, indexOf).trim();
                    String substring = split[i].substring(indexOf + 1);
                    this.ri.put(trim, substring);
                    trim + ":" + substring;
                } else {
                    Log.w("Configuration", "Unkown args for configuration." + split[i]);
                }
            }
        }
    }

    public final String aI(String str) {
        return this.ri.get(str);
    }
}
