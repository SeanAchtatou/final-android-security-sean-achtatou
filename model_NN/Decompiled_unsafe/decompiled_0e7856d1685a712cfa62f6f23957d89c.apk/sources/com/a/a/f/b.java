package com.a.a.f;

import android.util.Log;
import com.a.a.e.a;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import javax.microedition.media.control.ToneControl;
import javax.microedition.rms.RecordListener;

public final class b {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    private int gK;
    private HashMap<Integer, byte[]> gY = new HashMap<>();
    private int gZ;
    private long ha;
    private HashSet<RecordListener> hb = new HashSet<>();
    private boolean hc;
    private String name;
    private int version;

    private b(String str) {
        this.name = str;
    }

    public static b a(String str, boolean z) {
        "openRecordStore:" + str;
        b bVar = new b(str);
        if (org.meteoroid.core.b.av(PREFIX + str)) {
            str + " exist.";
            try {
                DataInputStream dataInputStream = new DataInputStream(org.meteoroid.core.b.ax(PREFIX + str));
                bVar.b(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new c();
            }
        } else if (!z) {
            str + " not exist and not necessary to create.";
            throw new e();
        } else {
            str + " not exist and necessary to create.";
            try {
                bVar.bb();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new c();
            }
        }
        bVar.hc = true;
        return bVar;
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
        this.version++;
        dataOutputStream2.writeInt(this.version);
        dataOutputStream2.writeLong(System.currentTimeMillis());
        dataOutputStream2.writeInt(this.gZ);
        dataOutputStream2.writeInt(this.gY.size());
        for (Map.Entry next : this.gY.entrySet()) {
            dataOutputStream2.writeInt(((Integer) next.getKey()).intValue());
            byte[] bArr = (byte[]) next.getValue();
            dataOutputStream2.writeInt(bArr.length);
            dataOutputStream2.write(bArr);
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String b = b(byteArray, "MD5");
        "Generate sign:" + b;
        dataOutputStream.write(byteArray);
        dataOutputStream.writeUTF(b);
        dataOutputStream.flush();
    }

    private void aZ() {
        this.gK = 0;
        for (byte[] length : this.gY.values()) {
            this.gK = length.length + this.gK;
        }
        "calcTheSizeOfRecords:" + this.gK;
    }

    private static String b(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str.toUpperCase());
            instance.reset();
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                    stringBuffer.append(com.a.a.h.b.SMS_RESULT_SEND_SUCCESS).append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                }
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private synchronized void b(DataInputStream dataInputStream) {
        synchronized (this) {
            DataInputStream dataInputStream2 = new DataInputStream(dataInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[a.GAME_B_PRESSED];
            while (true) {
                int read = dataInputStream2.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            DataInputStream dataInputStream3 = new DataInputStream(new ByteArrayInputStream(byteArray));
            this.version = dataInputStream3.readInt();
            this.ha = dataInputStream3.readLong();
            this.gZ = dataInputStream3.readInt();
            int readInt = dataInputStream3.readInt();
            for (int i = 0; i < readInt; i++) {
                int readInt2 = dataInputStream3.readInt();
                byte[] bArr2 = new byte[dataInputStream3.readInt()];
                dataInputStream3.read(bArr2);
                this.gY.put(Integer.valueOf(readInt2), bArr2);
            }
            try {
                String readUTF = dataInputStream3.readUTF();
                dataInputStream3.close();
                byte[] bArr3 = new byte[((byteArray.length - readUTF.getBytes().length) - 2)];
                System.arraycopy(byteArray, 0, bArr3, 0, bArr3.length);
                if (!readUTF.equals(b(bArr3, "MD5"))) {
                    Log.w(LOG_TAG, "Verify sign fail:" + readUTF);
                    throw new IllegalArgumentException("Invalid sign. The file has been modified.");
                }
            } catch (IOException e) {
            } catch (Exception e2) {
                throw new IOException(e2.getMessage());
            }
            aZ();
        }
    }

    private synchronized void ba() {
        if (org.meteoroid.core.b.av(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(org.meteoroid.core.b.ax(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                "Current version:" + this.version + " newest version:" + readInt;
                dataInputStream.close();
                if (readInt > this.version) {
                    b(new DataInputStream(org.meteoroid.core.b.ax(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    private synchronized void bb() {
        DataOutputStream dataOutputStream = new DataOutputStream(org.meteoroid.core.b.aw(PREFIX + this.name));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        aZ();
    }

    private boolean isOpen() {
        if (this.hc) {
            return this.hc;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new f();
    }

    public final void a(int i, byte[] bArr, int i2, int i3) {
        isOpen();
        "setRecord:" + 1 + " data:" + bArr.length;
        if (this.gY.containsKey(1)) {
            this.gY.remove(1);
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, 0, bArr2, 0, i3);
            this.gY.put(1, bArr2);
            try {
                bb();
            } catch (Exception e) {
            }
            Iterator<RecordListener> it = this.hb.iterator();
            while (it.hasNext()) {
                it.next();
            }
            return;
        }
        throw new a();
    }

    public final void bc() {
        isOpen();
        ba();
        "closeRecordStore" + this.name + " at version:" + this.version;
        this.hb.clear();
        this.hb = null;
        this.gY.clear();
        this.hc = false;
        this.gY = null;
        this.name = null;
        System.gc();
    }

    public final int d(byte[] bArr, int i, int i2) {
        isOpen();
        this.gZ++;
        "addRecord:" + this.gZ + " offset:0" + " numBytes:" + i2;
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, 0, bArr2, 0, i2);
        }
        this.gY.put(Integer.valueOf(this.gZ), bArr2);
        try {
            bb();
            Iterator<RecordListener> it = this.hb.iterator();
            while (it.hasNext()) {
                it.next();
                int i3 = this.gZ;
            }
            return this.gZ;
        } catch (Exception e) {
            throw new c();
        }
    }

    public final byte[] t(int i) {
        isOpen();
        ba();
        if (this.gY.containsKey(1)) {
            "getRecord" + 1 + " length:" + this.gY.get(1).length;
            return this.gY.get(1);
        }
        throw new a("recordId=" + 1);
    }
}
