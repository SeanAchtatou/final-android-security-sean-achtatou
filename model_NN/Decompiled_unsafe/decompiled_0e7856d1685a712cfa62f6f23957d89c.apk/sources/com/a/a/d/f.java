package com.a.a.d;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.c;
import org.meteoroid.core.i;
import org.meteoroid.core.l;

public abstract class f implements l.a {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    public boolean gA = false;
    private ArrayList<c> gB = new ArrayList<>();
    private d gC = null;
    private String gD;
    private boolean gE;
    protected e gz = null;
    private int height = -1;
    private int width = -1;

    f() {
    }

    private void aL() {
        this.width = c.mf.getWidth();
        this.height = c.mf.getHeight();
    }

    /* access modifiers changed from: protected */
    public void G() {
    }

    /* access modifiers changed from: protected */
    public void H() {
    }

    public abstract int aB();

    public void aE() {
        Iterator<c> it = this.gB.iterator();
        while (it.hasNext()) {
            i.a(it.next());
        }
        if (!this.gE) {
            try {
                H();
                getClass().getSimpleName() + " shownotify called.";
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.gE = true;
        }
    }

    public final ArrayList<c> aJ() {
        return this.gB;
    }

    public final d aK() {
        return this.gC;
    }

    public void aM() {
        Iterator<c> it = this.gB.iterator();
        while (it.hasNext()) {
            i.b(it.next());
        }
        if (this.gE) {
            try {
                G();
                getClass().getSimpleName() + " hidenotify called.";
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.gE = false;
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
    }

    /* access modifiers changed from: protected */
    public void d(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void e(int i) {
    }

    public final int getHeight() {
        if (this.height == -1) {
            aL();
        }
        return this.height;
    }

    public final String getTitle() {
        return this.gD;
    }

    public final int getWidth() {
        if (this.width == -1) {
            aL();
        }
        return this.width;
    }

    public final boolean isShown() {
        return this.gE;
    }

    public boolean onBack() {
        return false;
    }
}
