package com.a.a.d;

import android.view.View;
import org.meteoroid.core.e;

public abstract class b extends f {
    public static final int DOWN = 6;
    public static final int FIRE = 8;
    public static final int GAME_A = 9;
    public static final int GAME_B = 10;
    public static final int GAME_C = 11;
    public static final int GAME_D = 12;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_POUND = 35;
    public static final int KEY_STAR = 42;
    public static final int LEFT = 2;
    public static final int RIGHT = 5;
    public static final int UP = 1;

    /* access modifiers changed from: protected */
    public abstract void a(h hVar);

    public final int aB() {
        return 0;
    }

    public final void aC() {
        if (this.gz != null) {
            this.gz.b(this);
        }
    }

    public final void aD() {
        if (this.gz != null) {
            e eVar = this.gz;
            e.aD();
        }
    }

    public final void aE() {
        super.aE();
        aC();
    }

    public final View getView() {
        return e.mh;
    }

    public final boolean onBack() {
        return false;
    }
}
