package com.a.a.d;

import java.util.Hashtable;
import org.meteoroid.plugin.device.MIDPDevice;

public final class g {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final g gF = new g(0, 0, 0);
    private static g[] gG = {gF, gF};
    private static final Hashtable<Integer, g> gH = new Hashtable<>();
    private int gI;
    private int gJ;
    private int gK;

    private g(int i, int i2, int i3) {
        this.gI = i;
        this.gJ = i2;
        this.gK = i3;
    }

    public static g aN() {
        return gF;
    }

    public static g b(int i, int i2, int i3) {
        Integer num = new Integer(8);
        g gVar = gH.get(num);
        if (gVar != null) {
            return gVar;
        }
        g gVar2 = new g(0, 0, 8);
        gH.put(num, gVar2);
        return gVar2;
    }

    public final int J(String str) {
        return MIDPDevice.d.a(this, str);
    }

    public final int aO() {
        return this.gI;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.plugin.device.MIDPDevice.d.a(com.a.a.d.g, char):int
     arg types: [com.a.a.d.g, int]
     candidates:
      org.meteoroid.plugin.device.MIDPDevice.d.a(com.a.a.d.g, java.lang.String):int
      org.meteoroid.plugin.device.MIDPDevice.d.a(com.a.a.d.g, char):int */
    public final int d(char c) {
        return MIDPDevice.d.a(this, 20013);
    }

    public final int getHeight() {
        return MIDPDevice.d.c(this);
    }

    public final int getSize() {
        return this.gK;
    }

    public final int getStyle() {
        return this.gJ;
    }

    public final int hashCode() {
        return this.gJ + this.gK + this.gI;
    }
}
