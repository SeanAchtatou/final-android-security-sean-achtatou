package com.a.a.d;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import java.io.IOException;
import javax.microedition.enhance.MIDPHelper;
import org.meteoroid.core.e;
import org.meteoroid.plugin.device.MIDPDevice;

public class i {
    private static final Matrix gR = new Matrix();
    public Bitmap gO;
    public boolean gP;
    private h gQ;
    public int height;
    public int width;

    private i(Bitmap bitmap) {
        this(bitmap, false);
    }

    private i(Bitmap bitmap, boolean z) {
        this.gO = bitmap;
        if (bitmap != null) {
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
        }
        this.gP = z;
    }

    public static i K(String str) {
        Bitmap c = e.c(MIDPHelper.H(str));
        if (c == null) {
            throw new IOException();
        }
        i iVar = new i(c);
        iVar.gP = false;
        return iVar;
    }

    public static i m(int i, int i2) {
        "width:" + 640 + " height:360";
        return new i(e.a(640, 360, false, -16777216), true);
    }

    public final h aU() {
        if (this.gP) {
            if (this.gQ == null) {
                this.gQ = new MIDPDevice.e(this.gO);
                getClass().getName();
                "Create a MutableImage Graphics." + this.width + " " + this.height;
            }
            this.gQ.aT();
            return this.gQ;
        }
        throw new IllegalStateException("Image is immutable");
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }

    public final boolean isMutable() {
        return this.gP;
    }
}
