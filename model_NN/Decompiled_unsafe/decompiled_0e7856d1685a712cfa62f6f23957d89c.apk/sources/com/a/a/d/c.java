package com.a.a.d;

import android.view.View;
import android.widget.Button;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.k;
import org.meteoroid.plugin.device.MIDPDevice;

public final class c implements View.OnClickListener, i.c {
    public static final int BACK = 2;
    public static final int CANCEL = 3;
    public static final int EXIT = 7;
    public static final int HELP = 5;
    public static final int ITEM = 8;
    public static final int OK = 4;
    public static final int SCREEN = 1;
    public static final int STOP = 6;
    private int gq = 4;
    private Button gr = new Button(k.getActivity());
    private String label;
    private int priority = 0;

    public c(String str, int i, int i2) {
        this.label = str;
        this.gr.setText(str);
        this.gr.setOnClickListener(this);
    }

    public final String aF() {
        return this.label;
    }

    public final Button aG() {
        return this.gr;
    }

    public final void aH() {
        h.b(h.a(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }

    public final int getId() {
        return this.priority;
    }

    public final void onClick(View view) {
        h.b(h.a(MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }
}
