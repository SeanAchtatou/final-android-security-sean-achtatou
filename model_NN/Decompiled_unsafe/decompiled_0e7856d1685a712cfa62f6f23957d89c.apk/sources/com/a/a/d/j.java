package com.a.a.d;

import android.view.ViewGroup;
import java.util.Iterator;
import org.meteoroid.core.k;

public abstract class j extends f {
    public final void aE() {
        super.aE();
        k.getHandler().post(new Runnable() {
            public final void run() {
                Iterator<c> it = j.this.aJ().iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    "Add command " + next.aF();
                    ((ViewGroup) j.this.getView()).addView(next.aG());
                }
            }
        });
    }

    public final void aM() {
        super.aM();
        k.getHandler().post(new Runnable() {
            public final void run() {
                Iterator<c> it = j.this.aJ().iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    "Remove command " + next.aF();
                    ((ViewGroup) j.this.getView()).removeView(next.aG());
                }
            }
        });
    }

    public final boolean onBack() {
        k.getActivity().openOptionsMenu();
        return true;
    }
}
