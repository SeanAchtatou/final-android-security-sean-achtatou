package com.a.a.d;

import android.os.Message;
import android.util.Log;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.a;
import org.meteoroid.core.c;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public final class e implements h.a {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static f gs = null;
    public static final e gt = new e();
    private static MIDlet gu;
    private static volatile int gy;
    private Message gv = h.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, h.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean gw;
    private volatile boolean gx = true;

    private e() {
        h.a(this);
    }

    public static e a(MIDlet mIDlet) {
        if (mIDlet != null) {
            gu = mIDlet;
        }
        return gt;
    }

    private final void a(b bVar) {
        this.gx = false;
        try {
            bVar.a(((MIDPDevice) c.mf).aU());
        } catch (Exception e) {
            Log.w(LOG_TAG, "Exception in paint!" + e);
            e.printStackTrace();
        }
        ((MIDPDevice) c.mf).dB();
        this.gx = true;
    }

    public static void a(c cVar) {
        if (gs != null) {
            gs.aK();
            f fVar = gs;
        }
    }

    public static void aD() {
        Thread.yield();
    }

    private static boolean aI() {
        return gs != null && (gs.aB() == 0 || gs.aB() == 1);
    }

    public static void l(int i, int i2) {
        if (gs != null) {
            f fVar = gs;
        }
    }

    public final void a(f fVar) {
        if (fVar != gs && fVar != null) {
            if (fVar.aB() != 5) {
                if (gs != null && gs.aB() == 5) {
                    l.dq();
                }
                if (gs != null && gs.isShown()) {
                    gs.gz = null;
                }
                if (gs == null || !((gs.aB() == 0 || gs.aB() == 1) && (fVar.aB() == 0 || fVar.aB() == 1))) {
                    l.a(fVar);
                    return;
                }
                gs.aM();
                gs.gz = gt;
                l.b(fVar);
                gs = fVar;
                fVar.aE();
                h.b((int) l.MSG_VIEW_CHANGED, fVar);
                return;
            }
            a aVar = (a) fVar;
            int timeout = aVar.getTimeout();
            l.a(aVar.getTitle(), null, aVar.getView(), false, null);
            if (timeout != -2) {
                if (timeout <= 0) {
                    timeout = a.aA();
                }
                l.c((long) timeout);
            }
        }
    }

    public final boolean a(Message message) {
        if ((message.what == 44287 || message.what == 40965) && aI()) {
            if (this.gx) {
                a((b) gs);
            } else {
                if (gy > 0) {
                    gy--;
                }
                "Still not ready for repaint. Try later for " + gy;
            }
        } else if (message.what == 23041) {
            if (message.obj != null && (message.obj instanceof f)) {
                f fVar = (f) message.obj;
                gs = fVar;
                fVar.gz = this;
                this.gw = true;
                if (gs.aB() == 0) {
                    b((b) gs);
                }
            }
        } else if (message.what == 44035) {
            if (message.obj != null) {
                ((Runnable) message.obj).run();
            }
            return true;
        }
        return false;
    }

    public final synchronized void b(int i, int i2) {
        if (this.gw) {
            this.gw = false;
        }
        if (gs != null && a.cI()) {
            try {
                gs.b(i, i2);
            } catch (Exception e) {
            }
        }
    }

    public final void b(b bVar) {
        if (aI() && bVar == gs) {
            if (this.gx) {
                gy = 0;
                a(bVar);
            } else if (gy <= 1) {
                h.C(com.a.a.i.a.MSG_DEVICE_REQUEST_REFRESH);
                gy++;
            }
        }
    }

    public final void d(int i) {
        if (this.gw) {
            this.gw = false;
        }
        if (gs != null && a.cI()) {
            if (!gs.aJ().isEmpty()) {
                com.a.a.i.a aVar = c.mf;
                if (i == MIDPDevice.H(1)) {
                    a(gs.aJ().get(0));
                } else if (gs.aJ().size() > 1) {
                    com.a.a.i.a aVar2 = c.mf;
                    if (i == MIDPDevice.H(2)) {
                        a(gs.aJ().get(1));
                    }
                }
            }
            if (gs.aB() == 1) {
                ((com.a.a.e.a) gs).n(0, i);
            }
            gs.d(i);
        }
    }

    public final synchronized void d(int i, int i2) {
        if (!this.gw) {
            if (gs != null && a.cI()) {
                try {
                    gs.d(i, i2);
                } catch (Exception e) {
                }
            }
        }
    }

    public final void e(int i) {
        if (!this.gw && gs != null && a.cI()) {
            if (gs.aB() == 1) {
                ((com.a.a.e.a) gs).n(1, i);
            }
            gs.e(i);
        }
    }

    public final synchronized void k(int i, int i2) {
        if (!this.gw) {
            if (gs != null && a.cI()) {
                try {
                    gs.c(i);
                } catch (Exception e) {
                }
            }
        }
    }

    public final void p(int i) {
        if (!this.gw && gs != null && a.cI()) {
            f fVar = gs;
        }
    }
}
