package com.a.a.h;

import android.content.Intent;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import org.json.JSONObject;

public interface f {
    public static final int PAYMENT_TYPE_ALIPAYWS = 5;
    public static final int PAYMENT_TYPE_CHARGES = 6;
    public static final int PAYMENT_TYPE_LTHJ = 1;
    public static final int PAYMENT_TYPE_NOT_SET = 0;
    public static final int PAYMENT_TYPE_PROXY = 7;
    public static final int PAYMENT_TYPE_UPPAY = 2;
    public static final int PAYMENT_TYPE_YEEPAYCP = 4;
    public static final int PAYMENT_TYPE_YEEPAYHC = 3;

    public interface a {
        void requestBillingComplete(JSONObject jSONObject);

        void requestBillingError(Exception exc);
    }

    boolean forceNetwork();

    int getPaymentType();

    boolean onBack();

    void onPause();

    void onResult(Intent intent);

    void onResume();

    void pay(d dVar, e eVar, f.a aVar);
}
