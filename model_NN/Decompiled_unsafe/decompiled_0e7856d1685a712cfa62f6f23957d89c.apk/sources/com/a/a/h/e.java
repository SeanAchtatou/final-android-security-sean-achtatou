package com.a.a.h;

import android.content.Context;
import java.util.HashMap;
import me.gall.tinybee.TinybeeLogger;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.Purchase;

public final class e extends b implements OnPurchaseListener {
    private static boolean hasInited = false;
    private Context context;
    private f.a listener;
    private d paymentContext;
    private me.gall.totalpay.android.e paymentDetail;

    public final boolean forceNetwork() {
        return false;
    }

    public final int getPaymentType() {
        return 6;
    }

    public final void onAfterApply() {
    }

    public final void onAfterDownload() {
    }

    public final boolean onBack() {
        return false;
    }

    public final void onBeforeApply() {
    }

    public final void onBeforeDownload() {
    }

    public final void onBillingFinish(int i, HashMap hashMap) {
        String str;
        String str2;
        String str3;
        h.getLogName();
        "billing finish, status code = " + i;
        if (i == 102 || i == 104) {
            String str4 = "订购结果：订购成功。";
            if (hashMap != null) {
                String str5 = (String) hashMap.get(OnPurchaseListener.LEFTDAY);
                if (!(str5 == null || str5.trim().length() == 0)) {
                    str4 = String.valueOf(str4) + ",剩余时间 :" + str5;
                }
                String str6 = (String) hashMap.get(OnPurchaseListener.ORDERID);
                if (str6 == null || str6.trim().length() == 0) {
                    str3 = str4;
                    str = null;
                } else {
                    str3 = String.valueOf(str4) + ",OrderID:" + str6;
                    str = str6;
                }
                String str7 = (String) hashMap.get(OnPurchaseListener.PAYCODE);
                if (!(str7 == null || str7.trim().length() == 0)) {
                    str3 = String.valueOf(str3) + ",Paycode:" + str7;
                }
                String str8 = (String) hashMap.get(OnPurchaseListener.TRADEID);
                if (!(str8 == null || str8.trim().length() == 0)) {
                    str3 = String.valueOf(str3) + ",tradeID:" + str8;
                }
                String str9 = (String) hashMap.get(OnPurchaseListener.ORDERTYPE);
                if (!(str9 == null || str9.trim().length() == 0)) {
                    String.valueOf(str3) + ",ORDERTYPE:" + str9;
                }
            } else {
                str = null;
            }
            if (!(this.listener == null || this.paymentDetail == null)) {
                this.listener.onSuccess(this.paymentDetail);
            }
            str2 = b.SMS_RESULT_RECV_SUCCESS;
        } else {
            str = Purchase.getReason(i);
            String str10 = "订购结果：" + str;
            if (this.listener != null) {
                this.listener.onFail(str10, this.paymentDetail);
            }
            str2 = b.SMS_RESULT_SEND_FAIL;
        }
        processPaymentTransaction(str2, this.context, this.paymentDetail, this.paymentContext.getBilling(), str);
        h.getLogName();
    }

    public final void onInitFinish(int i) {
        "初始化结果：" + Purchase.getReason(i);
        h.getLogName();
        hasInited = true;
        this.paymentContext.sendUIMessage(2, null);
        try {
            Purchase.bo().a(this.context, this.paymentDetail.getParam("PAYCODE"), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void onQueryFinish(int i, HashMap hashMap) {
        h.getLogName();
        "license finish, status code = " + i;
        String str = "查询成功,该商品已购买";
        if (i != 101) {
            "查询结果：" + Purchase.getReason(i);
        } else {
            String str2 = (String) hashMap.get(OnPurchaseListener.LEFTDAY);
            if (!(str2 == null || str2.trim().length() == 0)) {
                str = String.valueOf(str) + ",剩余时间 ： " + str2;
            }
            String str3 = (String) hashMap.get(OnPurchaseListener.ORDERID);
            if (!(str3 == null || str3.trim().length() == 0)) {
                String.valueOf(str) + ",OrderID ： " + str3;
            }
        }
        h.getLogName();
    }

    public final void pay(d dVar, me.gall.totalpay.android.e eVar, f.a aVar) {
        this.paymentContext = dVar;
        this.context = dVar.getContext();
        this.paymentDetail = eVar;
        this.listener = aVar;
        Purchase bo = Purchase.bo();
        if (!hasInited) {
            dVar.sendUIMessage(1, "正在初始化，请稍候");
            try {
                Purchase.j(eVar.getParam(TinybeeLogger.EventTask.APPID), eVar.getParam("APPKEY"));
                bo.a(dVar.getContext(), this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                bo.a(dVar.getContext(), eVar.getParam("PAYCODE"), this);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
