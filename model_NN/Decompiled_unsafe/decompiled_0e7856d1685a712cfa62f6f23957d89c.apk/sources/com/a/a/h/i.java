package com.a.a.h;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import me.gall.totalpay.android.a;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;
import org.json.JSONException;
import org.json.JSONObject;

public final class i extends c {
    private static final int HIDE_PROCESSING_DIALOG = 2;
    private static final int SHOW_ERROR_TOAST = 7;
    private static final int SHOW_PROCESSING_DIALOG = 1;
    private static final int SHOW_YEEPAYFAIL = 5;
    private static final int SHOW_YEEPAYINFO = 3;
    private static final int SHOW_YEEPAYMAIN = 4;
    private static final int SHOW_YEEPAYSUCCESS = 6;
    private static final String TP_YEEPAY_USERS = "TP_YEEPAY_USERS";
    /* access modifiers changed from: private */
    public static final String[] YEEPAY_CARD_AMT_TYPES = {"50", "100"};
    private static final String[] YEEPAY_CARD_TYPES = {"请选择充值卡类型", "移动卡", "联通卡", "电信卡"};
    /* access modifiers changed from: private */
    public static final String[] YEEPAY_CARD_TYPES_CODE;
    /* access modifiers changed from: private */
    public String MerKey;
    /* access modifiers changed from: private */
    public JSONObject billing;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public Handler handler = new Handler() {
        public final void handleMessage(Message message) {
            i.this.state = message.what;
            switch (message.what) {
                case 1:
                    if (i.this.progressDialog == null) {
                        i.this.progressDialog = new ProgressDialog(i.this.context);
                        i.this.progressDialog.setMessage("请稍候");
                        i.this.progressDialog.setCancelable(false);
                    }
                    if (message.obj != null) {
                        i.this.progressDialog.setMessage((String) message.obj);
                    }
                    i.this.progressDialog.show();
                    h.getLogName();
                    return;
                case 2:
                    if (i.this.progressDialog != null && i.this.progressDialog.isShowing()) {
                        i.this.progressDialog.dismiss();
                        h.getLogName();
                        return;
                    }
                    return;
                case 3:
                    i.this.showInfoView();
                    return;
                case 4:
                    i.this.showMainView();
                    return;
                case 5:
                    i.this.showFailView((String) message.obj);
                    return;
                case 6:
                    i.this.showSuccessView((String) message.obj);
                    return;
                case 7:
                    Toast.makeText(i.this.context, (String) message.obj, 0);
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public f.a listener;
    /* access modifiers changed from: private */
    public String p1_MerId;
    private String p2_Order;
    /* access modifiers changed from: private */
    public String p3_Amt;
    /* access modifiers changed from: private */
    public String p4_verifyAmt;
    /* access modifiers changed from: private */
    public String p5_Pid;
    /* access modifiers changed from: private */
    public String p6_Pcat;
    /* access modifiers changed from: private */
    public String p7_Pdesc;
    /* access modifiers changed from: private */
    public String p8_Url;
    /* access modifiers changed from: private */
    public String pa7_cardAmt;
    /* access modifiers changed from: private */
    public String pa8_cardNo;
    /* access modifiers changed from: private */
    public String pa9_cardPwd;
    /* access modifiers changed from: private */
    public e paymentDetail;
    /* access modifiers changed from: private */
    public String pd_FrpId;
    /* access modifiers changed from: private */
    public ProgressDialog progressDialog;
    /* access modifiers changed from: private */
    public String pz1_userRegTime;
    /* access modifiers changed from: private */
    public String pz_userId;
    /* access modifiers changed from: private */
    public int state;

    static {
        String[] strArr = new String[4];
        strArr[1] = "SZX";
        strArr[2] = "UNICOM";
        strArr[3] = "TELECOM";
        YEEPAY_CARD_TYPES_CODE = strArr;
    }

    /* access modifiers changed from: private */
    public String constructBillingParams() {
        return "?p1_MerId=" + this.p1_MerId + "&p2_Order=" + this.p2_Order + "&p3_Amt=" + this.p3_Amt + "&p4_verifyAmt=" + this.p4_verifyAmt + "&p5_Pid=" + this.p5_Pid + "&p6_Pcat=" + this.p6_Pcat + "&p7_Pdesc=" + this.p7_Pdesc + "&p8_Url=" + this.p8_Url + "&pa_MP=&pa7_cardAmt=" + this.pa7_cardAmt + "&pa8_cardNo=" + this.pa8_cardNo + "&pa9_cardPwd=" + this.pa9_cardPwd + "&pd_FrpId=" + this.pd_FrpId + "&pz_userId=" + this.pz_userId + "&pz1_userRegTime=" + this.pz1_userRegTime + "&MerKey=" + this.MerKey;
    }

    /* access modifiers changed from: private */
    public void dismissPayingProgress() {
        this.handler.sendEmptyMessage(2);
    }

    /* access modifiers changed from: private */
    public String getBillingAddress() {
        return String.valueOf(f.isTestMode(this.context) ? "http://test.gall.me" : "http://totalpay.savenumber.com") + "/cardpro/req.jsp";
    }

    /* access modifiers changed from: private */
    public String getCallbackURL() {
        return String.valueOf(f.isTestMode(this.context) ? "http://test.gall.me" : "http://totalpay.savenumber.com") + "/totalpayserver/escrow/" + getPaymentType() + "/order/result";
    }

    /* access modifiers changed from: private */
    public String getMissingErrorHint() {
        return "支付未完成，您的订单号码为：<b>" + this.p2_Order + "</b>，请牢记订单号码，致电客服解决问题，客服电话：010-82097003。";
    }

    /* access modifiers changed from: private */
    public String getUserReg(Context context2, String str) {
        return context2.getApplicationContext().getSharedPreferences(TP_YEEPAY_USERS, 0).getString(str, null);
    }

    /* access modifiers changed from: private */
    public boolean localCheckParams() {
        if (this.pd_FrpId == null) {
            Toast.makeText(this.context, "充值卡类型未选择", 0).show();
            return false;
        } else if (this.pa7_cardAmt.length() > 0) {
            try {
                int parseInt = Integer.parseInt(this.pa7_cardAmt);
                if (parseInt < 0 || parseInt > 1000) {
                    throw new NumberFormatException();
                } else if (this.pa8_cardNo.length() <= 0) {
                    Toast.makeText(this.context, "卡号未填写", 0).show();
                    return false;
                } else if (this.pa8_cardNo.length() > 20) {
                    Toast.makeText(this.context, "卡号非法", 0).show();
                    return false;
                } else if (this.pa9_cardPwd.length() <= 0) {
                    Toast.makeText(this.context, "卡密未填写", 0).show();
                    return false;
                } else if (this.pa9_cardPwd.length() <= 19) {
                    return true;
                } else {
                    Toast.makeText(this.context, "卡密非法", 0).show();
                    return false;
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Toast.makeText(this.context, "非法的充值卡面额" + this.pa7_cardAmt, 0).show();
                return false;
            }
        } else {
            Toast.makeText(this.context, "面额未填写", 0).show();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public URL parseUrl(String str) {
        URL url = new URL(str);
        return new URI(url.getProtocol(), url.getAuthority(), url.getPath(), url.getQuery(), url.getRef()).toURL();
    }

    /* access modifiers changed from: private */
    public void requestBillingResult(final int i) {
        h.executeTask(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0122  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0127  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0165  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x01c5  */
            /* JADX WARNING: Removed duplicated region for block: B:38:0x01ca  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x0208  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r15 = this;
                    r12 = 30000(0x7530, double:1.4822E-319)
                    r11 = 6
                    r10 = 2
                    r9 = 5
                    r8 = 1
                    r2 = 0
                    java.lang.String r4 = "网络错误"
                    java.lang.String r3 = "0"
                    r1 = 0
                    com.a.a.h.i r0 = com.a.a.h.i.this     // Catch:{ Exception -> 0x02c5 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02c5 }
                    com.a.a.h.i r6 = com.a.a.h.i.this     // Catch:{ Exception -> 0x02c5 }
                    android.content.Context r6 = r6.context     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r6 = me.gall.totalpay.android.a.getBillingHost(r6)     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x02c5 }
                    r5.<init>(r6)     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r6 = "tpbilling/"
                    java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02c5 }
                    com.a.a.h.i r6 = com.a.a.h.i.this     // Catch:{ Exception -> 0x02c5 }
                    org.json.JSONObject r6 = r6.billing     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r7 = "id"
                    int r6 = r6.getInt(r7)     // Catch:{ Exception -> 0x02c5 }
                    java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x02c5 }
                    java.net.URL r0 = r0.parseUrl(r5)     // Catch:{ Exception -> 0x02c5 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x02c5 }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r6 = "GET:"
                    r5.<init>(r6)     // Catch:{ Exception -> 0x02c5 }
                    java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ Exception -> 0x02c5 }
                    r5.toString()     // Catch:{ Exception -> 0x02c5 }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x02c5 }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x02c5 }
                    java.lang.String r1 = "GET"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r0.connect()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r6 = "RESPONSE:"
                    r5.<init>(r6)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r5.toString()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r5 = 200(0xc8, float:2.8E-43)
                    if (r1 != r5) goto L_0x00fc
                    java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r1 = me.gall.totalpay.android.h.consumeStream(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r6 = "RESPONSE:"
                    r5.<init>(r6)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r5 = r5.append(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r5.toString()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r5.<init>(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r1 = "serState"
                    java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    int r2 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    if (r2 != r8) goto L_0x00f2
                    java.lang.String r1 = "balanceamt"
                    java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r3 = r4
                L_0x00ad:
                    if (r0 == 0) goto L_0x00b2
                    r0.disconnect()
                L_0x00b2:
                    if (r2 != r8) goto L_0x0262
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    android.os.Handler r2 = r2.handler
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    java.lang.String r4 = "支付成功，本次支付金额为"
                    r3.<init>(r4)
                    com.a.a.h.i r4 = com.a.a.h.i.this
                    java.lang.String r4 = r4.p3_Amt
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r4 = "元，卡内余额"
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.StringBuilder r1 = r3.append(r1)
                    java.lang.String r3 = "元<br /><b>余额有效期为1个月<b/>，在有效期内您可以多次使用直至余额为0，谢谢"
                    java.lang.StringBuilder r1 = r1.append(r3)
                    java.lang.String r1 = r1.toString()
                    android.os.Message r1 = r2.obtainMessage(r11, r1)
                    r0.sendMessage(r1)
                L_0x00f1:
                    return
                L_0x00f2:
                    java.lang.String r1 = "failRemark"
                    java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r14 = r3
                    r3 = r1
                    r1 = r14
                    goto L_0x00ad
                L_0x00fc:
                    java.lang.Exception r5 = new java.lang.Exception     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r7 = "ResponseCode:"
                    r6.<init>(r7)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    r5.<init>(r1)     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                    throw r5     // Catch:{ Exception -> 0x0111, all -> 0x02bf }
                L_0x0111:
                    r1 = move-exception
                    r14 = r1
                    r1 = r0
                    r0 = r14
                L_0x0115:
                    java.lang.String r5 = me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x01c2 }
                    java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x01c2 }
                    android.util.Log.w(r5, r0)     // Catch:{ all -> 0x01c2 }
                    if (r1 == 0) goto L_0x0125
                    r1.disconnect()
                L_0x0125:
                    if (r2 != r8) goto L_0x0165
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    java.lang.String r4 = "支付成功，本次支付金额为"
                    r2.<init>(r4)
                    com.a.a.h.i r4 = com.a.a.h.i.this
                    java.lang.String r4 = r4.p3_Amt
                    java.lang.StringBuilder r2 = r2.append(r4)
                    java.lang.String r4 = "元，卡内余额"
                    java.lang.StringBuilder r2 = r2.append(r4)
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r3 = "元<br /><b>余额有效期为1个月<b/>，在有效期内您可以多次使用直至余额为0，谢谢"
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.os.Message r1 = r1.obtainMessage(r11, r2)
                    r0.sendMessage(r1)
                    goto L_0x00f1
                L_0x0165:
                    if (r2 <= r8) goto L_0x0190
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    java.lang.String r3 = "很遗憾，本次支付失败，您可以返回重新进行支付。<br />失败原因："
                    r2.<init>(r3)
                    java.lang.StringBuilder r2 = r2.append(r4)
                    java.lang.String r2 = r2.toString()
                    android.os.Message r1 = r1.obtainMessage(r9, r2)
                    r0.sendMessage(r1)
                    goto L_0x00f1
                L_0x0190:
                    int r0 = r2
                    if (r0 <= r10) goto L_0x01b4
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    java.lang.String r2 = r2.getMissingErrorHint()
                    android.os.Message r1 = r1.obtainMessage(r9, r2)
                    r0.sendMessage(r1)
                    goto L_0x00f1
                L_0x01b4:
                    android.os.SystemClock.sleep(r12)
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    int r1 = r2
                    int r1 = r1 + 1
                    r0.requestBillingResult(r1)
                    goto L_0x00f1
                L_0x01c2:
                    r0 = move-exception
                L_0x01c3:
                    if (r1 == 0) goto L_0x01c8
                    r1.disconnect()
                L_0x01c8:
                    if (r2 != r8) goto L_0x0208
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    r1.dismissPayingProgress()
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    android.os.Handler r2 = r2.handler
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    java.lang.String r5 = "支付成功，本次支付金额为"
                    r4.<init>(r5)
                    com.a.a.h.i r5 = com.a.a.h.i.this
                    java.lang.String r5 = r5.p3_Amt
                    java.lang.StringBuilder r4 = r4.append(r5)
                    java.lang.String r5 = "元，卡内余额"
                    java.lang.StringBuilder r4 = r4.append(r5)
                    java.lang.StringBuilder r3 = r4.append(r3)
                    java.lang.String r4 = "元<br /><b>余额有效期为1个月<b/>，在有效期内您可以多次使用直至余额为0，谢谢"
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    android.os.Message r2 = r2.obtainMessage(r11, r3)
                    r1.sendMessage(r2)
                L_0x0207:
                    throw r0
                L_0x0208:
                    if (r2 <= r8) goto L_0x0232
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    r1.dismissPayingProgress()
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    android.os.Handler r2 = r2.handler
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    java.lang.String r5 = "很遗憾，本次支付失败，您可以返回重新进行支付。<br />失败原因："
                    r3.<init>(r5)
                    java.lang.StringBuilder r3 = r3.append(r4)
                    java.lang.String r3 = r3.toString()
                    android.os.Message r2 = r2.obtainMessage(r9, r3)
                    r1.sendMessage(r2)
                    goto L_0x0207
                L_0x0232:
                    int r1 = r2
                    if (r1 <= r10) goto L_0x0255
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    r1.dismissPayingProgress()
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    android.os.Handler r2 = r2.handler
                    com.a.a.h.i r3 = com.a.a.h.i.this
                    java.lang.String r3 = r3.getMissingErrorHint()
                    android.os.Message r2 = r2.obtainMessage(r9, r3)
                    r1.sendMessage(r2)
                    goto L_0x0207
                L_0x0255:
                    android.os.SystemClock.sleep(r12)
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    int r2 = r2
                    int r2 = r2 + 1
                    r1.requestBillingResult(r2)
                    goto L_0x0207
                L_0x0262:
                    if (r2 <= r8) goto L_0x028d
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    java.lang.String r4 = "很遗憾，本次支付失败，您可以返回重新进行支付。<br />失败原因："
                    r2.<init>(r4)
                    java.lang.StringBuilder r2 = r2.append(r3)
                    java.lang.String r2 = r2.toString()
                    android.os.Message r1 = r1.obtainMessage(r9, r2)
                    r0.sendMessage(r1)
                    goto L_0x00f1
                L_0x028d:
                    int r0 = r2
                    if (r0 <= r10) goto L_0x02b1
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    r0.dismissPayingProgress()
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    android.os.Handler r0 = r0.handler
                    com.a.a.h.i r1 = com.a.a.h.i.this
                    android.os.Handler r1 = r1.handler
                    com.a.a.h.i r2 = com.a.a.h.i.this
                    java.lang.String r2 = r2.getMissingErrorHint()
                    android.os.Message r1 = r1.obtainMessage(r9, r2)
                    r0.sendMessage(r1)
                    goto L_0x00f1
                L_0x02b1:
                    android.os.SystemClock.sleep(r12)
                    com.a.a.h.i r0 = com.a.a.h.i.this
                    int r1 = r2
                    int r1 = r1 + 1
                    r0.requestBillingResult(r1)
                    goto L_0x00f1
                L_0x02bf:
                    r1 = move-exception
                    r14 = r1
                    r1 = r0
                    r0 = r14
                    goto L_0x01c3
                L_0x02c5:
                    r0 = move-exception
                    goto L_0x0115
                */
                throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.i.AnonymousClass2.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public void saveUserReg(Context context2, String str, String str2) {
        context2.getApplicationContext().getSharedPreferences(TP_YEEPAY_USERS, 0).edit().putString(str, str2).commit();
    }

    /* access modifiers changed from: private */
    public void showFailView(String str) {
        dismissPayingProgress();
        Activity activity = (Activity) this.context;
        try {
            ViewGroup viewGroup = (ViewGroup) h.inflateView(activity, "tp_yeepaycp_fail");
            activity.setContentView(viewGroup);
            ((TextView) h.findViewByName(activity, viewGroup, "tp_yeepaycp_fail_reason")).setText(Html.fromHtml(str));
            ((Button) h.findViewByName(activity, viewGroup, "tp_yeepaycp_back_button")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (i.this.listener != null) {
                        i.this.listener.onFail("Billing fail", i.this.paymentDetail);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void showInfoView() {
        Activity activity = (Activity) this.context;
        try {
            ViewGroup viewGroup = (ViewGroup) h.inflateView(activity, "tp_yeepaycp_info");
            activity.setContentView(viewGroup);
            ((Button) h.findViewByName(activity, viewGroup, "tp_yeepaycp_back_button")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    i.this.handler.sendEmptyMessage(4);
                }
            });
            ((TextView) h.findViewByName(activity, viewGroup, "tp_yeepaycp_info_text")).setText(Html.fromHtml(this.context.getString(h.getStringIdentifier(this.context, "tp_yeepaycp_info_detail"))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void showMainView() {
        try {
            final Activity activity = (Activity) this.context;
            final ViewGroup viewGroup = (ViewGroup) h.inflateView(activity, "tp_yeepaycp_main");
            Spinner spinner = (Spinner) h.findViewByName(activity, viewGroup, "tp_yeepaycp_cardtype");
            ArrayAdapter arrayAdapter = new ArrayAdapter(activity, h.getLayoutIdentifier(this.context, "tp_layout_spinner"), YEEPAY_CARD_TYPES);
            arrayAdapter.setDropDownViewResource(17367049);
            spinner.setAdapter((SpinnerAdapter) arrayAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public final void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    i.this.pd_FrpId = i.YEEPAY_CARD_TYPES_CODE[i];
                }

                public final void onNothingSelected(AdapterView<?> adapterView) {
                    i.this.pd_FrpId = null;
                }
            });
            Spinner spinner2 = (Spinner) h.findViewByName(activity, viewGroup, "tp_yeepaycp_cardamt");
            ArrayAdapter arrayAdapter2 = new ArrayAdapter(activity, h.getLayoutIdentifier(this.context, "tp_layout_spinner"), YEEPAY_CARD_AMT_TYPES);
            arrayAdapter2.setDropDownViewResource(17367049);
            spinner2.setAdapter((SpinnerAdapter) arrayAdapter2);
            spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public final void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    i.this.pa7_cardAmt = i.YEEPAY_CARD_AMT_TYPES[i];
                }

                public final void onNothingSelected(AdapterView<?> adapterView) {
                    i.this.pa7_cardAmt = null;
                }
            });
            activity.setContentView(viewGroup);
            final Button button = (Button) h.findViewByName(activity, viewGroup, "tp_yeepaycp_confirm_button");
            button.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    int i = 10;
                    i.this.p1_MerId = i.this.paymentDetail.getParam("MerId");
                    i.this.MerKey = i.this.paymentDetail.getParam("MerKey");
                    int price = i.this.paymentDetail.getPrice();
                    if (price >= 10) {
                        i = price;
                    }
                    i.this.p3_Amt = String.valueOf(((float) i) / 100.0f);
                    i.this.p4_verifyAmt = "true";
                    try {
                        i.this.p5_Pid = a.getBillingProductName(i.this.billing);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    i.this.p6_Pcat = "手机游戏";
                    i.this.p7_Pdesc = String.valueOf(Build.MANUFACTURER) + " " + Build.MODEL;
                    i.this.p8_Url = i.this.getCallbackURL();
                    i.this.pa8_cardNo = ((EditText) h.findViewByName(activity, viewGroup, "tp_yeepaycp_cardno")).getText().toString();
                    i.this.pa9_cardPwd = ((EditText) h.findViewByName(activity, viewGroup, "tp_yeepaycp_cardpwd")).getText().toString();
                    i.this.pz_userId = h.getDeviceUniqueID(i.this.context);
                    i.this.pz1_userRegTime = i.this.getUserReg(activity, i.this.pz_userId);
                    if (i.this.pz1_userRegTime == null) {
                        i.this.pz1_userRegTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        i.this.saveUserReg(i.this.context, i.this.pz_userId, i.this.pz1_userRegTime);
                    }
                    if (i.this.localCheckParams()) {
                        button.setEnabled(false);
                        InputMethodManager inputMethodManager = (InputMethodManager) i.this.context.getSystemService("input_method");
                        if (activity.getCurrentFocus() != null) {
                            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 2);
                        }
                        i.this.showRunningProgress(null);
                        a.getInstance(i.this.context).addBillingProperty("appid", i.this.p1_MerId);
                        a.getInstance(i.this.context).requestBillingOrderId(i.this.paymentDetail, i.this, i.this.billing);
                    }
                }
            });
            ((TextView) h.findViewByName(activity, viewGroup, "tp_yeepaycp_info")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    i.this.handler.sendEmptyMessage(3);
                }
            });
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showRunningProgress() {
        showRunningProgress(null);
    }

    /* access modifiers changed from: private */
    public void showRunningProgress(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(1, str));
    }

    /* access modifiers changed from: private */
    public void showSuccessView(String str) {
        dismissPayingProgress();
        Activity activity = (Activity) this.context;
        try {
            ViewGroup viewGroup = (ViewGroup) h.inflateView(activity, "tp_yeepaycp_success");
            activity.setContentView(viewGroup);
            ((TextView) h.findViewByName(activity, viewGroup, "tp_yeepaycp_success_info")).setText(Html.fromHtml(str));
            ((Button) h.findViewByName(activity, viewGroup, "tp_yeepaycp_back_button")).setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    if (i.this.listener != null) {
                        i.this.listener.onSuccess(i.this.paymentDetail);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final boolean forceNetwork() {
        return true;
    }

    public final int getPaymentType() {
        return 4;
    }

    public final boolean onBack() {
        return this.state == 4;
    }

    public final void onResult(Intent intent) {
    }

    public final void pay(d dVar, e eVar, f.a aVar) {
        this.context = dVar.getContext();
        this.billing = dVar.getBilling();
        this.listener = aVar;
        this.paymentDetail = eVar;
        this.handler.sendEmptyMessage(4);
    }

    public final void requestBillingComplete(JSONObject jSONObject) {
        this.p2_Order = jSONObject.getString("orderid");
        h.executeTask(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:18:0x00cb  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x0102  */
            /* JADX WARNING: Removed duplicated region for block: B:64:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r7 = this;
                    r5 = -1
                    r1 = 0
                    com.a.a.h.i r0 = com.a.a.h.i.this     // Catch:{ Exception -> 0x019a }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019a }
                    com.a.a.h.i r3 = com.a.a.h.i.this     // Catch:{ Exception -> 0x019a }
                    java.lang.String r3 = r3.getBillingAddress()     // Catch:{ Exception -> 0x019a }
                    java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x019a }
                    r2.<init>(r3)     // Catch:{ Exception -> 0x019a }
                    com.a.a.h.i r3 = com.a.a.h.i.this     // Catch:{ Exception -> 0x019a }
                    java.lang.String r3 = r3.constructBillingParams()     // Catch:{ Exception -> 0x019a }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x019a }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x019a }
                    java.net.URL r0 = r0.parseUrl(r2)     // Catch:{ Exception -> 0x019a }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x019a }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019a }
                    java.lang.String r3 = "Create:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x019a }
                    java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x019a }
                    r2.toString()     // Catch:{ Exception -> 0x019a }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x019a }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x019a }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r1 = "GET"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r0.connect()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r3 = "RESPONSE:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r2.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r2 = 200(0xc8, float:2.8E-43)
                    if (r1 != r2) goto L_0x0182
                    java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r4 = me.gall.totalpay.android.h.consumeStream(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "RESPONSE:"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r1.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r3 = "订单的提交状态[r1_Code："
                    int r2 = r4.indexOf(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    if (r2 != r5) goto L_0x0106
                    java.lang.String r3 = "errorCode:"
                    int r2 = r4.indexOf(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    if (r2 != r5) goto L_0x00cf
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "非法订单"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0093:
                    r1 = move-exception
                    r6 = r1
                    r1 = r0
                    r0 = r6
                L_0x0097:
                    com.a.a.h.i r2 = com.a.a.h.i.this     // Catch:{ all -> 0x0197 }
                    android.os.Handler r2 = r2.handler     // Catch:{ all -> 0x0197 }
                    com.a.a.h.i r3 = com.a.a.h.i.this     // Catch:{ all -> 0x0197 }
                    android.os.Handler r3 = r3.handler     // Catch:{ all -> 0x0197 }
                    r4 = 5
                    java.lang.String r5 = r0.getMessage()     // Catch:{ all -> 0x0197 }
                    android.os.Message r3 = r3.obtainMessage(r4, r5)     // Catch:{ all -> 0x0197 }
                    r2.sendMessage(r3)     // Catch:{ all -> 0x0197 }
                    java.lang.String r2 = me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x0197 }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0197 }
                    java.lang.String r4 = "Validate Billing Error"
                    r3.<init>(r4)     // Catch:{ all -> 0x0197 }
                    java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0197 }
                    java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ all -> 0x0197 }
                    java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0197 }
                    android.util.Log.w(r2, r0)     // Catch:{ all -> 0x0197 }
                    if (r1 == 0) goto L_0x00ce
                    r1.disconnect()
                L_0x00ce:
                    return
                L_0x00cf:
                    java.lang.String r1 = ";"
                    int r1 = r4.indexOf(r1, r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x00d5:
                    if (r1 == r5) goto L_0x016d
                    int r3 = r3.length()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    int r2 = r2 + r3
                    java.lang.String r1 = r4.substring(r2, r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    switch(r1) {
                        case -1: goto L_0x010d;
                        case 1: goto L_0x0115;
                        case 2: goto L_0x012d;
                        case 5: goto L_0x0135;
                        case 11: goto L_0x013d;
                        case 66: goto L_0x0145;
                        case 95: goto L_0x014d;
                        case 112: goto L_0x0155;
                        case 8001: goto L_0x015d;
                        case 8002: goto L_0x0165;
                        default: goto L_0x00e7;
                    }     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x00e7:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r3 = "未知错误"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x00fc:
                    r1 = move-exception
                    r6 = r1
                    r1 = r0
                    r0 = r6
                L_0x0100:
                    if (r1 == 0) goto L_0x0105
                    r1.disconnect()
                L_0x0105:
                    throw r0
                L_0x0106:
                    java.lang.String r1 = "]"
                    int r1 = r4.indexOf(r1, r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    goto L_0x00d5
                L_0x010d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "签名较验失败或未知错误"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0115:
                    com.a.a.h.i r1 = com.a.a.h.i.this     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "正在获取支付结果，该过程可能需要2分钟，请耐心等待"
                    r1.showRunningProgress(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r1 = 12000(0x2ee0, double:5.929E-320)
                    android.os.SystemClock.sleep(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    com.a.a.h.i r1 = com.a.a.h.i.this     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r2 = 0
                    r1.requestBillingResult(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    if (r0 == 0) goto L_0x00ce
                    r0.disconnect()
                    goto L_0x00ce
                L_0x012d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "卡密成功处理过或者提交卡号过于频繁"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0135:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "卡数量过多，目前最多支持10张卡"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x013d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "订单号重复"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0145:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "支付金额有误"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x014d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "支付方式未开通"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0155:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "业务状态不可用，未开通此类卡业务"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x015d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "卡面额组填写错误"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0165:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = "卡号密码为空或者数量不相等（使用组合支付时）"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x016d:
                    java.lang.Exception r1 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r3 = "未知错误"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r1     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0182:
                    java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r4 = "响应码错误:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    r2.<init>(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                    throw r2     // Catch:{ Exception -> 0x0093, all -> 0x00fc }
                L_0x0197:
                    r0 = move-exception
                    goto L_0x0100
                L_0x019a:
                    r0 = move-exception
                    goto L_0x0097
                */
                throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.i.AnonymousClass10.run():void");
            }
        });
    }

    public final void requestBillingError(Exception exc) {
        Log.w(h.getLogName(), "Fail to create billing." + exc);
        this.listener.onFail("Create Billing Error", this.paymentDetail);
    }
}
