package com.a.a.h;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.alipay.android.BaseHelper;
import com.alipay.android.MobileSecurePayHelper;
import com.alipay.android.MobileSecurePayer;
import com.alipay.android.ResultChecker;
import com.alipay.android.Rsa;
import java.net.URLEncoder;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;
import org.json.JSONException;
import org.json.JSONObject;

public final class a extends c {
    private static final int SHOW_PAYING_MSG = 65317;
    private static final int SHOW_PLUGIN_NOT_INSTALLED_TOAST = 65318;
    /* access modifiers changed from: private */
    public JSONObject billing;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public f.a listener;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.alipay.android.BaseHelper.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, boolean):android.app.ProgressDialog
         arg types: [android.content.Context, ?[OBJECT, ARRAY], java.lang.String, int]
         candidates:
          com.alipay.android.BaseHelper.a(android.app.Activity, java.lang.String, java.lang.String, int):void
          com.alipay.android.BaseHelper.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, boolean):android.app.ProgressDialog */
        public final void handleMessage(Message message) {
            try {
                if (message.what == a.SHOW_PLUGIN_NOT_INSTALLED_TOAST) {
                    Toast.makeText(a.this.context, "安装支付宝快捷支付插件失败", 1).show();
                } else if (message.what == a.SHOW_PAYING_MSG) {
                    a.this.closeProgress();
                    a.this.mProgress = BaseHelper.a(a.this.context, (CharSequence) null, (CharSequence) "正在支付", false);
                } else if (message.what == 1) {
                    String str = (String) message.obj;
                    Log.e(h.getLogName(), str);
                    a.this.closeProgress();
                    try {
                        String substring = str.substring("resultStatus={".length() + str.indexOf("resultStatus="), str.indexOf("};memo="));
                        if (new ResultChecker(str).A(a.this.paymentDetail.getParam("rsa_public")) == 1) {
                            BaseHelper.a((Activity) a.this.context, "提示", a.this.context.getString(h.getStringIdentifier(a.this.context, "check_sign_failed")), 17301543);
                        } else if (!substring.equals("9000")) {
                            h.getLogName();
                            a.this.listener.onFail(substring, a.this.paymentDetail);
                        } else if (a.this.listener != null) {
                            a.this.listener.onSuccess(a.this.paymentDetail);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        h.getLogName();
                        h.getLogName();
                        a.this.listener.onFail(e.getMessage(), a.this.paymentDetail);
                    }
                }
                super.handleMessage(message);
            } catch (Exception e2) {
                e2.printStackTrace();
                h.getLogName();
                a.this.listener.onFail(e2.getMessage(), a.this.paymentDetail);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog mProgress = null;
    private MobileSecurePayHelper mspHelper;
    private String outTradeNo;
    /* access modifiers changed from: private */
    public e paymentDetail;
    /* access modifiers changed from: private */
    public boolean startInstallPlugin;

    /* renamed from: com.a.a.h.a$a  reason: collision with other inner class name */
    public interface C0000a {
        void cancel();

        void installed();

        void startInstall();
    }

    private String getCallbackURL() {
        return String.valueOf(f.isTestMode(this.context) ? "http://test.gall.me" : "http://totalpay.savenumber.com") + "/totalpayserver/escrow/5/order/result";
    }

    /* access modifiers changed from: package-private */
    public final void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final boolean forceNetwork() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public final String getCharset() {
        return "charset=\"utf-8\"";
    }

    /* access modifiers changed from: package-private */
    public final String getOrderInfo() {
        JSONException jSONException;
        String str;
        String str2;
        try {
            try {
                str2 = String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("partner=\"" + this.paymentDetail.getParam("partner") + "\"") + "&") + "seller=\"" + this.paymentDetail.getParam("seller") + "\"") + "&") + "out_trade_no=\"" + this.outTradeNo + "\"") + "&") + "subject=\"" + me.gall.totalpay.android.a.getBillingProductName(this.billing) + "\"") + "&") + "body=\"" + me.gall.totalpay.android.a.getBillingProductName(this.billing) + "\"") + "&") + "total_fee=\"" + (((float) this.paymentDetail.getPrice()) / 100.0f) + "\"") + "&";
                return String.valueOf(str2) + "notify_url=\"" + getCallbackURL() + "\"";
            } catch (JSONException e) {
                JSONException jSONException2 = e;
                str = str2;
                jSONException = jSONException2;
            }
        } catch (JSONException e2) {
            JSONException jSONException3 = e2;
            str = null;
            jSONException = jSONException3;
        }
        jSONException.printStackTrace();
        return str;
    }

    public final int getPaymentType() {
        return 5;
    }

    /* access modifiers changed from: package-private */
    public final String getSignType() {
        return "sign_type=\"RSA\"";
    }

    public final boolean onBack() {
        return false;
    }

    public final void onResult(Intent intent) {
    }

    public final void onResume() {
        if (this.startInstallPlugin) {
            this.startInstallPlugin = false;
            if (this.mspHelper == null) {
                return;
            }
            if (this.mspHelper.v()) {
                me.gall.totalpay.android.a.getInstance(this.context).requestBillingOrderId(this.paymentDetail, this, this.billing);
                return;
            }
            this.mHandler.sendEmptyMessage(SHOW_PLUGIN_NOT_INSTALLED_TOAST);
            this.listener.onCancel();
        }
    }

    public final void pay(d dVar, final e eVar, final f.a aVar) {
        this.context = dVar.getContext();
        this.listener = aVar;
        this.paymentDetail = eVar;
        this.billing = dVar.getBilling();
        me.gall.totalpay.android.a.getInstance(this.context).addBillingProperty("appid", this.context.getPackageName());
        this.mspHelper = new MobileSecurePayHelper(this.context);
        this.mspHelper.a(new C0000a() {
            public final void cancel() {
                a.this.mHandler.sendEmptyMessage(a.SHOW_PLUGIN_NOT_INSTALLED_TOAST);
                aVar.onCancel();
            }

            public final void installed() {
                me.gall.totalpay.android.a.getInstance(a.this.context).requestBillingOrderId(eVar, a.this, a.this.billing);
            }

            public final void startInstall() {
                a.this.startInstallPlugin = true;
            }
        });
    }

    public final void requestBillingComplete(JSONObject jSONObject) {
        this.outTradeNo = jSONObject.getString("orderid");
        try {
            String orderInfo = getOrderInfo();
            if (new MobileSecurePayer().a(String.valueOf(orderInfo) + "&sign=\"" + URLEncoder.encode(sign(getSignType(), orderInfo)) + "\"&" + getSignType(), this.mHandler, 1, (Activity) this.context)) {
                this.mHandler.sendEmptyMessage(SHOW_PAYING_MSG);
                return;
            }
            Log.w(h.getLogName(), "Fail to start billing.");
            this.listener.onFail("Start Billing Error", this.paymentDetail);
        } catch (Exception e) {
            e.printStackTrace();
            Log.w(h.getLogName(), "Fail to call plugin.");
            this.listener.onFail(e.getMessage(), this.paymentDetail);
        }
    }

    public final void requestBillingError(Exception exc) {
        Log.w(h.getLogName(), "Fail to create billing." + exc);
        this.listener.onFail(exc.getMessage(), this.paymentDetail);
    }

    /* access modifiers changed from: package-private */
    public final String sign(String str, String str2) {
        return Rsa.sign(str2, this.paymentDetail.getParam("rsa_private"));
    }
}
