package com.a.a.h;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import java.util.Map;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class b implements f {
    public static final String SMS_RESULT_RECV_SUCCESS = "3";
    public static final String SMS_RESULT_SEND_CANCEL = "2";
    public static final String SMS_RESULT_SEND_FAIL = "1";
    public static final String SMS_RESULT_SEND_NOTIFIED = "4";
    public static final String SMS_RESULT_SEND_SUCCESS = "0";
    public static final String TOTALPAY_RESULTS = "TP_RESULTS";
    public static final String TOTALPAY_RESULTS_EXTRA = "TOTALPAY_RESULTS_EXTRA";
    public static final String TP_PAYMENT_RECORD = "TP_PAYMENT_RECORD";

    private static SharedPreferences getExtraPaymentResults(Context context) {
        return context.getApplicationContext().getSharedPreferences(TOTALPAY_RESULTS_EXTRA, 0);
    }

    public static Map<String, String> getExtraRemainPaymentResults(Context context) {
        return getExtraPaymentResults(context).getAll();
    }

    private static SharedPreferences getPaymentRecords(Context context) {
        return context.getApplicationContext().getSharedPreferences(TP_PAYMENT_RECORD, 0);
    }

    public static SharedPreferences getPaymentResults(Context context) {
        return context.getApplicationContext().getSharedPreferences(TOTALPAY_RESULTS, 0);
    }

    public static Map<String, String> getRemainPaymentResults(Context context) {
        return getPaymentResults(context).getAll();
    }

    public static boolean hasPaymentSent(Context context, long j) {
        return !getPaymentResults(context).contains(String.valueOf(j));
    }

    public static boolean isPaymentConsumed(Context context, e eVar) {
        return !eVar.isRepeatable() && isPaymentRecordedById(context, eVar.getId());
    }

    public static boolean isPaymentRecordedById(Context context, String str) {
        return getPaymentRecords(context).getBoolean(str, false);
    }

    public static void processPaymentTransaction(String str, Context context, e eVar, JSONObject jSONObject, String str2) {
        if (eVar.getMode() == 0) {
            if (!eVar.isRepeatable()) {
                recordIdOfPayment(context, eVar.getId());
                h.getLogName();
            }
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("cid", f.getChannelId(context));
                jSONObject2.put("tpFid", eVar.getId());
                jSONObject2.put("tpCarrier", String.valueOf(eVar.getCarrier()));
                jSONObject2.put("tpType", eVar.getType());
                jSONObject2.put("tpPrice", String.valueOf(eVar.getPrice()));
                if (str2 == null) {
                    str2 = eVar.getRemark();
                }
                jSONObject2.put("tpRemark", str2);
                jSONObject2.put("uid", h.getDeviceUniqueID(context));
                jSONObject2.put("result", str);
                String param = eVar.getParam("TP_keyword");
                if (param != null && param.length() > 0) {
                    jSONObject2.put("keywords", param);
                }
                jSONObject2.put("orderid", jSONObject.get("orderid"));
                uploadChargesPaymentResult(context, System.currentTimeMillis(), jSONObject2.toString());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Log.w(h.getLogName(), "Not a charges transaction.");
        }
    }

    public static void recordIdOfPayment(Context context, String str) {
        getPaymentRecords(context).edit().putBoolean(str, true).commit();
    }

    public static void saveExtraPaymentResult(Context context, long j, String str) {
        getExtraPaymentResults(context).edit().putString(String.valueOf(j), str).commit();
    }

    public static void savePaymentResult(Context context, long j, String str) {
        if (str != null) {
            try {
                SharedPreferences.Editor edit = getPaymentResults(context).edit();
                edit.putString(String.valueOf(j), str);
                edit.commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void uploadChargesPaymentResult(final Context context, final long j, final String str) {
        try {
            if (new JSONObject(str).getString("result").equals(SMS_RESULT_SEND_NOTIFIED)) {
                h.getLogName();
                "Skipped because has been notified once." + str;
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        h.getLogName();
        "Save first." + j;
        savePaymentResult(context, j, str);
        h.executeTask(new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00e3  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r6 = this;
                    r1 = 0
                    android.content.Context r0 = r2     // Catch:{ Exception -> 0x0011 }
                    boolean r0 = me.gall.totalpay.android.h.isConnectedOrConnecting(r0)     // Catch:{ Exception -> 0x0011 }
                    if (r0 != 0) goto L_0x002f
                    java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r2 = "Network disable now."
                    r0.<init>(r2)     // Catch:{ Exception -> 0x0011 }
                    throw r0     // Catch:{ Exception -> 0x0011 }
                L_0x0011:
                    r0 = move-exception
                L_0x0012:
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x00e0 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e0 }
                    java.lang.String r3 = "Network problem:"
                    r2.<init>(r3)     // Catch:{ all -> 0x00e0 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00e0 }
                    java.lang.String r2 = ". Will try it next launch."
                    java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00e0 }
                    r0.toString()     // Catch:{ all -> 0x00e0 }
                    if (r1 == 0) goto L_0x002e
                    r1.disconnect()
                L_0x002e:
                    return
                L_0x002f:
                    java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x0011 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0011 }
                    android.content.Context r3 = r2     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r3 = me.gall.totalpay.android.a.getBillingHost(r3)     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x0011 }
                    r2.<init>(r3)     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r3 = "result"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0011 }
                    r0.<init>(r2)     // Catch:{ Exception -> 0x0011 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0011 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r3 = "NOTIFY:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x0011 }
                    java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x0011 }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0011 }
                    r2.toString()     // Catch:{ Exception -> 0x0011 }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x0011 }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x0011 }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r1 = "POST"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r1 = "Timestamp"
                    long r2 = r3     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r1 = "Content-Type"
                    java.lang.String r2 = "application/json"
                    r0.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r1 = 1
                    r0.setDoInput(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r1 = 1
                    r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r2 = r5     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r3 = "utf-8"
                    byte[] r2 = r2.getBytes(r3)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r1.write(r2)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r1.flush()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r1.close()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r0.connect()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r3 = "ResponseCode:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r2.toString()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    r2 = 201(0xc9, float:2.82E-43)
                    if (r1 != r2) goto L_0x00d0
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    android.content.Context r1 = r2     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    long r2 = r3     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r4 = r5     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    com.a.a.h.b.uploadPaymentResultSuccess(r1, r2, r4)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                L_0x00c9:
                    if (r0 == 0) goto L_0x002e
                    r0.disconnect()
                    goto L_0x002e
                L_0x00d0:
                    java.lang.String r1 = me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    java.lang.String r2 = "Upload fail. Will try it next launch."
                    android.util.Log.w(r1, r2)     // Catch:{ Exception -> 0x00da, all -> 0x00e7 }
                    goto L_0x00c9
                L_0x00da:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                    goto L_0x0012
                L_0x00e0:
                    r0 = move-exception
                L_0x00e1:
                    if (r1 == 0) goto L_0x00e6
                    r1.disconnect()
                L_0x00e6:
                    throw r0
                L_0x00e7:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                    goto L_0x00e1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.b.AnonymousClass1.run():void");
            }
        });
    }

    private static void uploadExtraChargesPaymentResult(final Context context, final long j, final String str) {
        h.executeTask(new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:26:0x00d6  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r6 = this;
                    r1 = 0
                    java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00dc }
                    android.content.Context r2 = r1     // Catch:{ Exception -> 0x00dc }
                    boolean r2 = me.gall.totalpay.android.f.isTestMode(r2)     // Catch:{ Exception -> 0x00dc }
                    java.lang.String r2 = com.a.a.h.h.getHost(r2)     // Catch:{ Exception -> 0x00dc }
                    r0.<init>(r2)     // Catch:{ Exception -> 0x00dc }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00dc }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00dc }
                    java.lang.String r3 = "POST:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x00dc }
                    java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x00dc }
                    r2.toString()     // Catch:{ Exception -> 0x00dc }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00dc }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00dc }
                    java.lang.String r1 = "POST"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r1 = "content-type"
                    java.lang.String r2 = "application/json;charset=utf-8"
                    r0.addRequestProperty(r1, r2)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r1 = 1
                    r0.setDoOutput(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r2 = "DATA:"
                    r1.<init>(r2)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r2 = r4     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r1.toString()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r2 = r4     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r3 = "UTF-8"
                    byte[] r2 = r2.getBytes(r3)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r1.write(r2)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r0.connect()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r3 = "RESPONSE:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r2.toString()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r2 = 200(0xc8, float:2.8E-43)
                    if (r1 != r2) goto L_0x008c
                    android.content.Context r1 = r1     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    long r2 = r2     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    com.a.a.h.b.uploadExtraPaymentResultSuccess(r1, r2, r4)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                L_0x0086:
                    if (r0 == 0) goto L_0x008b
                    r0.disconnect()
                L_0x008b:
                    return
                L_0x008c:
                    r2 = 500(0x1f4, float:7.0E-43)
                    if (r1 != r2) goto L_0x00bb
                    android.content.Context r1 = r1     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    long r2 = r2     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r4 = r4     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    com.a.a.h.b.uploadExtraPaymentResultSuccess(r1, r2, r4)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    goto L_0x0086
                L_0x009d:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                L_0x00a1:
                    r0.printStackTrace()     // Catch:{ all -> 0x00da }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x00da }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00da }
                    java.lang.String r3 = "Woow, send error."
                    r2.<init>(r3)     // Catch:{ all -> 0x00da }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00da }
                    r0.toString()     // Catch:{ all -> 0x00da }
                    if (r1 == 0) goto L_0x008b
                    r1.disconnect()
                    goto L_0x008b
                L_0x00bb:
                    java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r4 = "ResponseCode:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    r2.<init>(r1)     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                    throw r2     // Catch:{ Exception -> 0x009d, all -> 0x00d0 }
                L_0x00d0:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                L_0x00d4:
                    if (r1 == 0) goto L_0x00d9
                    r1.disconnect()
                L_0x00d9:
                    throw r0
                L_0x00da:
                    r0 = move-exception
                    goto L_0x00d4
                L_0x00dc:
                    r0 = move-exception
                    goto L_0x00a1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.b.AnonymousClass2.run():void");
            }
        });
    }

    public static void uploadExtraPaymentResultSuccess(Context context, long j, String str) {
        if (getExtraPaymentResults(context).contains(String.valueOf(j))) {
            h.getLogName();
            "This is an extra remain result. Time to remove it." + str;
            getExtraPaymentResults(context).edit().remove(String.valueOf(j)).commit();
        }
    }

    public static void uploadPaymentResultSuccess(Context context, long j, String str) {
        if (getPaymentResults(context).contains(String.valueOf(j))) {
            h.getLogName();
            "This is a remain result. Time to remove it." + str;
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getString("result").equals(SMS_RESULT_SEND_SUCCESS)) {
                    jSONObject.put("result", SMS_RESULT_SEND_NOTIFIED);
                    getPaymentResults(context).edit().putString(String.valueOf(j), jSONObject.toString()).commit();
                    return;
                }
                getPaymentResults(context).edit().remove(String.valueOf(j)).commit();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public static void uploadRemainPaymentResults(Context context) {
        for (Map.Entry next : getRemainPaymentResults(context).entrySet()) {
            h.getLogName();
            "Find a remain result." + ((String) next.getKey()) + ". Try to upload again." + ((String) next.getValue());
            uploadChargesPaymentResult(context, Long.parseLong((String) next.getKey()), (String) next.getValue());
        }
        for (Map.Entry next2 : getExtraRemainPaymentResults(context).entrySet()) {
            h.getLogName();
            "Find an extra remain result. Try to upload again." + ((String) next2.getValue());
            uploadExtraChargesPaymentResult(context, Long.parseLong((String) next2.getKey()), (String) next2.getValue());
        }
    }

    public void onPause() {
    }

    public void onResult(Intent intent) {
    }

    public void onResume() {
    }
}
