package com.a.a.h;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.a.a.e.a;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.util.PluginHelper;
import java.io.ByteArrayInputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.parsers.DocumentBuilderFactory;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;
import org.json.JSONObject;
import org.w3c.dom.Element;

public final class d extends c {
    private JSONObject billing;
    /* access modifiers changed from: private */
    public Context context;
    /* access modifiers changed from: private */
    public me.gall.totalpay.android.d contextWrapper;
    /* access modifiers changed from: private */
    public f.a listener;
    private String merchantId;
    /* access modifiers changed from: private */
    public String merchantOrderId;
    private String merchantOrderTime;
    private String merchantPublicCert;
    /* access modifiers changed from: private */
    public e paymentDetail;
    private String sign;

    /* access modifiers changed from: private */
    public String createLTHJIntentBundleData() {
        StringBuffer stringBuffer = new StringBuffer((int) a.GAME_C_PRESSED);
        stringBuffer.append("<upomp application=\"QueryOrder.Req\" version=\"1.0.0\">");
        stringBuffer.append("<transType>01</transType>");
        stringBuffer.append("<merchantId>" + this.merchantId + "</merchantId>");
        stringBuffer.append("<merchantOrderId>" + this.merchantOrderId + "</merchantOrderId>");
        stringBuffer.append("<merchantOrderTime>" + this.merchantOrderTime + "</merchantOrderTime>");
        stringBuffer.append("<sign>" + this.sign + "</sign>");
        stringBuffer.append("<merchantPublicCert>" + this.merchantPublicCert + "</merchantPublicCert>");
        stringBuffer.append("</upomp>");
        return stringBuffer.toString();
    }

    private Socket getHostSocket() {
        return new Socket("202.96.33.145", f.isTestMode(this.context) ? 9015 : 9005);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00f7 A[SYNTHETIC, Splitter:B:26:0x00f7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean signBilling(me.gall.totalpay.android.e r7) {
        /*
            r6 = this;
            r2 = 0
            java.net.Socket r3 = r6.getHostSocket()     // Catch:{ Exception -> 0x00e5, all -> 0x00f3 }
            me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0121 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "Sign:"
            r0.<init>(r1)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = r3.toString()     // Catch:{ Exception -> 0x0121 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0121 }
            r0.toString()     // Catch:{ Exception -> 0x0121 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x0121 }
            r0.<init>()     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "appId"
            java.lang.String r4 = "appId"
            java.lang.String r4 = r7.getParam(r4)     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "ext"
            android.content.Context r4 = r6.context     // Catch:{ Exception -> 0x0121 }
            java.lang.String r4 = me.gall.totalpay.android.h.getDeviceUniqueID(r4)     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "merchantName"
            java.lang.String r4 = "merchantName"
            java.lang.String r4 = r7.getParam(r4)     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "merchantOrderAmt"
            int r4 = r7.getPrice()     // Catch:{ Exception -> 0x0121 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "merchantOrderId"
            java.lang.String r4 = r6.merchantOrderId     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "merchantOrderDesc"
            android.content.Context r4 = r6.context     // Catch:{ Exception -> 0x0121 }
            java.lang.String r4 = me.gall.totalpay.android.h.getDeviceUniqueID(r4)     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "transTimeout"
            java.lang.String r4 = ""
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r1 = "merchantOrderTime"
            java.lang.String r4 = r6.merchantOrderTime     // Catch:{ Exception -> 0x0121 }
            r0.put(r1, r4)     // Catch:{ Exception -> 0x0121 }
            java.io.OutputStream r1 = r3.getOutputStream()     // Catch:{ Exception -> 0x0121 }
            java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x0121 }
            java.lang.String r5 = "UTF-8"
            byte[] r4 = r4.getBytes(r5)     // Catch:{ Exception -> 0x0121 }
            r1.write(r4)     // Catch:{ Exception -> 0x0121 }
            r1.flush()     // Catch:{ Exception -> 0x0121 }
            r1 = 10000(0x2710, float:1.4013E-41)
            r3.setSoTimeout(r1)     // Catch:{ Exception -> 0x0121 }
            me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x0121 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0121 }
            java.lang.String r4 = "DATA:"
            r1.<init>(r4)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0121 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0121 }
            r0.toString()     // Catch:{ Exception -> 0x0121 }
            java.io.InputStream r0 = r3.getInputStream()     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = me.gall.totalpay.android.h.consumeStream(r0)     // Catch:{ Exception -> 0x0121 }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x0121 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "merchantId"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0121 }
            r6.merchantId = r0     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "merchantOrderId"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0121 }
            r6.merchantOrderId = r0     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "merchantOrderTime"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0121 }
            r6.merchantOrderTime = r0     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "sign"
            java.lang.String r0 = r4.getString(r0)     // Catch:{ Exception -> 0x0121 }
            r6.sign = r0     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "respCode"
            java.lang.String r1 = r4.getString(r0)     // Catch:{ Exception -> 0x0121 }
            java.lang.String r0 = "respDesc"
            java.lang.String r2 = r4.getString(r0)     // Catch:{ Exception -> 0x0124 }
            if (r3 == 0) goto L_0x00d9
            r3.close()     // Catch:{ IOException -> 0x011d }
        L_0x00d9:
            if (r1 == 0) goto L_0x00fb
            java.lang.String r0 = "0000"
            boolean r0 = r1.equals(r0)
            if (r0 == 0) goto L_0x00fb
            r0 = 1
        L_0x00e4:
            return r0
        L_0x00e5:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00e8:
            r0.printStackTrace()     // Catch:{ all -> 0x011f }
            if (r3 == 0) goto L_0x00d9
            r3.close()     // Catch:{ IOException -> 0x00f1 }
            goto L_0x00d9
        L_0x00f1:
            r0 = move-exception
            goto L_0x00d9
        L_0x00f3:
            r0 = move-exception
            r3 = r2
        L_0x00f5:
            if (r3 == 0) goto L_0x00fa
            r3.close()     // Catch:{ IOException -> 0x011b }
        L_0x00fa:
            throw r0
        L_0x00fb:
            java.lang.String r0 = me.gall.totalpay.android.h.getLogName()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r3.<init>(r1)
            java.lang.String r1 = " "
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.util.Log.w(r0, r1)
            r0 = 0
            goto L_0x00e4
        L_0x011b:
            r1 = move-exception
            goto L_0x00fa
        L_0x011d:
            r0 = move-exception
            goto L_0x00d9
        L_0x011f:
            r0 = move-exception
            goto L_0x00f5
        L_0x0121:
            r0 = move-exception
            r1 = r2
            goto L_0x00e8
        L_0x0124:
            r0 = move-exception
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.h.d.signBilling(me.gall.totalpay.android.e):boolean");
    }

    public final boolean forceNetwork() {
        return true;
    }

    public final int getPaymentType() {
        return 1;
    }

    public final boolean onBack() {
        return true;
    }

    public final void onResult(Intent intent) {
        String str;
        Bundle extras = intent.getExtras();
        if (extras != null) {
            byte[] byteArray = extras.getByteArray("xml");
            try {
                String str2 = new String(byteArray, "utf-8");
                h.getLogName();
                "bundle:" + str2;
                Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(byteArray)).getDocumentElement();
                String nodeValue = documentElement.getElementsByTagName("merchantId").item(0).getFirstChild().getNodeValue();
                String nodeValue2 = documentElement.getElementsByTagName("merchantOrderId").item(0).getFirstChild().getNodeValue();
                try {
                    str = documentElement.getElementsByTagName("respCode").item(0).getFirstChild().getNodeValue();
                } catch (Exception e) {
                    e.printStackTrace();
                    str = "";
                }
                if (!nodeValue.equals(this.merchantId) || !nodeValue2.equals(this.merchantOrderId) || !str.equals("0000")) {
                    h.getLogName();
                    String.valueOf(nodeValue2) + " " + str;
                    this.listener.onFail(str, this.paymentDetail);
                    return;
                }
                h.getLogName();
                String.valueOf(nodeValue2) + " billing success by " + nodeValue;
                if (this.listener != null) {
                    this.listener.onSuccess(this.paymentDetail);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.w(h.getLogName(), "Fail to finish billing." + e2);
                this.listener.onFail(e2.getMessage(), this.paymentDetail);
            }
        } else {
            h.getLogName();
        }
    }

    public final void pay(me.gall.totalpay.android.d dVar, e eVar, f.a aVar) {
        this.context = dVar.getContext();
        this.listener = aVar;
        this.paymentDetail = eVar;
        this.billing = dVar.getBilling();
        this.contextWrapper = dVar;
        dVar.sendUIMessage(1, "正在准备银联支付数据，请稍候");
        PluginLink.setContext(this.context);
        me.gall.totalpay.android.a.getInstance(this.context).addBillingProperty("appid", eVar.getParam("appId"));
        me.gall.totalpay.android.a.getInstance(this.context).requestBillingOrderId(eVar, this, this.billing);
    }

    public final void requestBillingComplete(JSONObject jSONObject) {
        this.merchantOrderId = jSONObject.getString("orderid");
        this.merchantOrderTime = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(jSONObject.getLong("createtime")));
        if (this.merchantOrderId == null || this.merchantOrderTime == null) {
            throw new IllegalArgumentException("Not valid response.");
        }
        h.executeTask(new Runnable() {
            public final void run() {
                h.getLogName();
                "Start to sign billing." + d.this.merchantOrderId;
                boolean access$2 = d.this.signBilling(d.this.paymentDetail);
                if (d.this.contextWrapper != null) {
                    d.this.contextWrapper.sendUIMessage(2, null);
                }
                if (access$2) {
                    Bundle bundle = new Bundle();
                    bundle.putByteArray("xml", d.this.createLTHJIntentBundleData().getBytes());
                    bundle.putString("action_cmd", "cmd_pay_plugin");
                    PluginHelper.LaunchPlugin((Activity) d.this.context, bundle);
                    return;
                }
                Log.w(h.getLogName(), "Fail to sign billing." + d.this.merchantOrderId);
                d.this.listener.onFail("Sign billing Error", d.this.paymentDetail);
            }
        });
    }

    public final void requestBillingError(Exception exc) {
        if (this.contextWrapper != null) {
            this.contextWrapper.sendUIMessage(2, null);
        }
        Log.w(h.getLogName(), "Fail to create billing." + exc);
        this.listener.onFail("Create Billing Error", this.paymentDetail);
    }
}
