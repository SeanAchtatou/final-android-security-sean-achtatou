package com.a.a.h;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.telephony.SmsManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;
import me.gall.totalpay.android.d;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import me.gall.totalpay.android.h;

public class g extends b {
    public static final String SMS_DELIEVERED_ACTION = "me.gall.totalpay.sms_delievered";
    public static final String SMS_SENT_ACTION = "me.gall.totalpay.sms_sent";
    private static final String SMS_TIMEOUT_COUNT = "SMS_TIMEOUT_COUNT";
    private static final Timer timer = new Timer(SMS_TIMEOUT_COUNT);
    /* access modifiers changed from: private */
    public d paymentContextWrapper;
    private TimerTask timeoutCountTask;

    private PendingIntent createDelieveredIntent(Context context, String str, long j) {
        return PendingIntent.getBroadcast(context, 0, new Intent(SMS_DELIEVERED_ACTION + str + j), 1073741824);
    }

    private PendingIntent createSentIntent(Context context, String str, long j) {
        return PendingIntent.getBroadcast(context, 0, new Intent(SMS_SENT_ACTION + str + j), 1073741824);
    }

    private void registerBroadcastReceiver(Context context, String str, String str2, long j, f.a aVar, e eVar) {
        if (context.checkCallingOrSelfPermission("android.permission.SEND_SMS") == 0) {
            final String str3 = str;
            final long j2 = j;
            final e eVar2 = eVar;
            final f.a aVar2 = aVar;
            h.registerReceiver(context, SMS_SENT_ACTION + str + j, new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(g.SMS_SENT_ACTION + str3 + j2)) {
                        g.this.resetTimeoutCount();
                        h.unregisterReceiver(context, intent.getAction());
                        int resultCode = getResultCode();
                        if (resultCode == -1) {
                            h.getLogName();
                            g.processPaymentTransaction(b.SMS_RESULT_SEND_SUCCESS, g.this.paymentContextWrapper.getContext(), eVar2, g.this.paymentContextWrapper.getBilling(), null);
                            final f.a aVar = aVar2;
                            final e eVar = eVar2;
                            h.executeTask(new Runnable() {
                                public final void run() {
                                    if (h.getCurrentCarrier() == 3) {
                                        h.getLogName();
                                        SystemClock.sleep(20000);
                                    }
                                    g.this.notifySMSSent(aVar, eVar);
                                }
                            });
                        } else {
                            Log.w(h.getLogName(), "Sent ResultCode:" + resultCode);
                            g.this.notifySMSError(aVar2, eVar2, resultCode);
                        }
                    }
                    h.getLogName();
                    "Sent?" + intent.getAction() + " code=" + getResultCode() + " data=" + getResultData();
                }
            });
            return;
        }
        throw new SecurityException("Not have the SEND_SMS permission.");
    }

    /* access modifiers changed from: private */
    public void resetTimeoutCount() {
        if (this.timeoutCountTask != null) {
            this.timeoutCountTask.cancel();
        }
    }

    private void startTimeoutCount(final e eVar, final f.a aVar) {
        this.timeoutCountTask = new TimerTask() {
            public final void run() {
                if (aVar != null) {
                    g.this.smsNotPermitted(eVar, aVar, new TimeoutException("发送短信超时"));
                }
            }
        };
        timer.schedule(this.timeoutCountTask, 18000);
    }

    public boolean forceNetwork() {
        return false;
    }

    public int getPaymentType() {
        return 6;
    }

    public void notifySMSError(f.a aVar, e eVar, int i) {
        processPaymentTransaction(b.SMS_RESULT_SEND_FAIL, this.paymentContextWrapper.getContext(), eVar, this.paymentContextWrapper.getBilling(), String.valueOf(i));
        if (aVar != null) {
            aVar.onFail("Sent ResultCode:" + i, eVar);
        }
    }

    public void notifySMSSent(f.a aVar, e eVar) {
        if (aVar != null && eVar != null) {
            aVar.onSuccess(eVar);
        }
    }

    public boolean onBack() {
        return false;
    }

    public void pay(d dVar, e eVar, f.a aVar) {
        this.paymentContextWrapper = dVar;
        if (h.isApplicationInstalled(dVar.getContext(), "com.lbe.security")) {
            h.getLogName();
            "Oooops,lbe is installed.Fail at once." + eVar.getId();
            aVar.onFail(f.a.LBE_PROBLEM, eVar);
            return;
        }
        String param = eVar.getParam("address");
        String param2 = eVar.getParam("content");
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> divideMessage = smsManager.divideMessage(param2);
        long currentTimeMillis = System.currentTimeMillis();
        try {
            registerBroadcastReceiver(dVar.getContext(), param, param2, currentTimeMillis, aVar, eVar);
            for (String next : divideMessage) {
                PendingIntent createSentIntent = createSentIntent(dVar.getContext(), param, currentTimeMillis);
                PendingIntent createDelieveredIntent = createDelieveredIntent(dVar.getContext(), param, currentTimeMillis);
                if (!h.isDualSim() || h.getReadySim(dVar.getContext()) != 1) {
                    smsManager.sendTextMessage(param, "", next, createSentIntent, createDelieveredIntent);
                } else {
                    Class.forName("android.telephony.gemini.GeminiSmsManager").getMethod("sendTextMessageGemini", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(null, param, "", next, Integer.valueOf(h.getReadySim(dVar.getContext())), createSentIntent, createDelieveredIntent);
                }
                h.getLogName();
                "Send " + next + " to " + param;
                resetTimeoutCount();
                startTimeoutCount(eVar, aVar);
            }
        } catch (Exception e) {
            e.printStackTrace();
            smsNotPermitted(eVar, aVar, e);
        }
    }

    public void smsNotPermitted(e eVar, f.a aVar, Exception exc) {
        processPaymentTransaction(b.SMS_RESULT_SEND_FAIL, this.paymentContextWrapper.getContext(), eVar, this.paymentContextWrapper.getBilling(), exc.getMessage());
        aVar.onFail(exc.getMessage(), eVar);
    }
}
