package com.unionpay.upomp.lthj.plugin.model;

public class BankCardInfo extends Data {
    public String activityId;
    public String pan;
    public String panBank;
    public String panType;
    public String payTips;
    public String validateCode;
}
