package com.unionpay.upomp.lthj.plugin.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.widget.EditText;
import com.a.a.h.b;
import com.lthj.unipay.plugin.bt;
import com.lthj.unipay.plugin.g;
import com.lthj.unipay.plugin.i;
import java.util.Calendar;

public class YearAndMonthDialog implements DialogInterface.OnClickListener {
    String[] a = new String[12];
    String[] b = new String[11];
    private AlertDialog c;
    private int d;
    /* access modifiers changed from: private */
    public EditText e;
    private int f;
    private bt g;
    private Context h;

    public YearAndMonthDialog(Context context, EditText editText, int i, bt btVar) {
        this.g = btVar;
        this.f = i;
        this.h = context;
        this.e = editText;
        g gVar = new g(this);
        for (int i2 = 0; i2 <= 10; i2++) {
            this.d = Calendar.getInstance().get(1);
            this.b[i2] = (this.d + i2) + "年";
        }
        for (int i3 = 0; i3 < 12; i3++) {
            this.a[i3] = (i3 + 1) + "月";
        }
        if (this.f == 0) {
            this.c = new AlertDialog.Builder(context).setTitle("请输入信用卡有效期月份").setItems(this.a, this).create();
        } else if (this.f == 1) {
            this.c = new AlertDialog.Builder(context).setTitle("请输入信用卡有效期年份").setItems(this.b, this).create();
        }
        this.c.setOnDismissListener(gVar);
    }

    public int getType() {
        return this.f;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.f == 0) {
            if (i < 9) {
                stringBuffer.append(b.SMS_RESULT_SEND_SUCCESS);
            }
            stringBuffer.append(i + 1);
        } else if (this.f == 1) {
            this.d = Calendar.getInstance().get(1);
            stringBuffer.append((this.d - 2000) + i);
        }
        switch (this.f) {
            case 0:
                this.g.b(JniMethod.getJniMethod().encryptConfig(stringBuffer.toString().getBytes(), stringBuffer.toString().getBytes().length));
                i.a(this.h, this.e, 1, this.g);
                break;
            case 1:
                this.e.setClickable(true);
                this.g.a(JniMethod.getJniMethod().encryptConfig(stringBuffer.toString().getBytes(), stringBuffer.toString().getBytes().length));
                this.e.setText("**月/**年");
                stringBuffer.delete(0, stringBuffer.length());
                this.e.dispatchKeyEvent(new KeyEvent(1, 66));
                break;
        }
        dialogInterface.dismiss();
    }

    public void setInputText(EditText editText) {
        this.e = editText;
    }

    public void setType(int i) {
        this.f = i;
    }

    public void show() {
        if (this.c != null) {
            this.c.show();
        }
    }
}
