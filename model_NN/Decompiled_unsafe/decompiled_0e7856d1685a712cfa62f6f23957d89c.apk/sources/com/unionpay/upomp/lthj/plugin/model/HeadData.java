package com.unionpay.upomp.lthj.plugin.model;

import android.content.Context;
import com.lthj.unipay.plugin.cu;
import com.lthj.unipay.plugin.h;
import com.lthj.unipay.plugin.i;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;

public class HeadData {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public HeadData(String str, Context context) {
        cu a2 = new h(context).a(1);
        String str2 = "";
        if (a2 != null) {
            byte[] bytes = a2.b().getBytes();
            str2 = new String(JniMethod.getJniMethod().decryptConfig(bytes, bytes.length));
        }
        this.a = str;
        this.f = str2;
        this.c = "01-02-1.1.0";
        this.d = i.b();
        this.e = i.a();
        this.g = i.a(context);
        this.b = "1.1.0";
    }

    public static HeadData createHeadData(String str, Context context) {
        return new HeadData(str, context);
    }

    public String getApplication() {
        return this.a;
    }

    public String getPluginSerialNo() {
        return this.f;
    }

    public String getPluginVersion() {
        return this.c;
    }

    public String getTerminalModel() {
        return this.d;
    }

    public String getTerminalOs() {
        return this.e;
    }

    public String getTerminalPhysicalNo() {
        return this.g;
    }

    public String getVersion() {
        return this.b;
    }

    public void setApplication(String str) {
        this.a = str;
    }

    public void setPluginSerialNo(String str) {
        this.f = str;
    }

    public void setPluginVersion(String str) {
        this.c = str;
    }

    public void setTerminalModel(String str) {
        this.d = str;
    }

    public void setTerminalOs(String str) {
        this.e = str;
    }

    public void setTerminalPhysicalNo(String str) {
        this.g = str;
    }

    public void setVersion(String str) {
        this.b = str;
    }
}
