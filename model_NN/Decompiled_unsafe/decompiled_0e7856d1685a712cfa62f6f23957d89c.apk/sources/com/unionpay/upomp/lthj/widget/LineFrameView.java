package com.unionpay.upomp.lthj.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.a.a.g.c;
import com.lthj.unipay.plugin.at;
import com.unionpay.upomp.lthj.link.PluginLink;

public class LineFrameView extends LinearLayout {
    private static String a = "LineFrameView";
    private TextView b;
    private TextView c;
    private AttributeSet d;
    private ImageView e;
    private Context f;

    public LineFrameView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f = context;
        this.d = attributeSet;
        b();
    }

    private void b() {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this.f).inflate(PluginLink.getLayoutupomp_lthj_lineframe(), this);
        this.c = (TextView) linearLayout.findViewById(PluginLink.getIdupomp_lthj_lineframe_tv());
        this.b = (TextView) linearLayout.findViewById(PluginLink.getIdupomp_lthj_lineframe_title());
        this.e = (ImageView) linearLayout.findViewById(PluginLink.getIdupomp_lthj_lineframe_image());
        if (this.d != null) {
            int attributeResourceValue = this.d.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "src", 0);
            if (attributeResourceValue == 0) {
                at.a(a, "src is null");
            } else {
                b(attributeResourceValue);
            }
            int attributeResourceValue2 = this.d.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "hint", 0);
            if (attributeResourceValue2 == 0) {
                at.a(a, "title-id is null");
                String attributeValue = this.d.getAttributeValue("http://schemas.android.com/apk/res/android", "hint");
                if (attributeValue != null) {
                    this.b.setText(attributeValue);
                } else {
                    this.b.setVisibility(8);
                }
            } else {
                this.b.setText(attributeResourceValue2);
            }
            int attributeResourceValue3 = this.d.getAttributeResourceValue("http://schemas.android.com/apk/res/android", c.TEXT_MESSAGE, 0);
            if (attributeResourceValue3 == 0) {
                at.a(a, "text-id is null");
                String attributeValue2 = this.d.getAttributeValue("http://schemas.android.com/apk/res/android", c.TEXT_MESSAGE);
                if (attributeValue2 != null) {
                    a(attributeValue2);
                    return;
                }
                return;
            }
            a(attributeResourceValue3);
        }
    }

    public TextView a() {
        return this.c;
    }

    public void a(int i) {
        this.c.setText(i);
    }

    public void a(CharSequence charSequence) {
        this.c.setText(charSequence);
    }

    public void b(int i) {
        this.e.setImageResource(i);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }
}
