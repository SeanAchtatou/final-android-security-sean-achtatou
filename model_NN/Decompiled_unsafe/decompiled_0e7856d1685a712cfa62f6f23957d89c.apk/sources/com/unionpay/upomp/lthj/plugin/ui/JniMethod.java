package com.unionpay.upomp.lthj.plugin.ui;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.JNIBottomBackData;
import com.unionpay.upomp.lthj.plugin.model.JNICreditBackData;
import com.unionpay.upomp.lthj.plugin.model.JNIInitBottomData;
import com.unionpay.upomp.lthj.plugin.model.SmsCodeVerfiyData;

public class JniMethod {
    public static JniMethod instance;

    public static JniMethod getJniMethod() {
        if (instance == null) {
            instance = new JniMethod();
        }
        return instance;
    }

    public native byte[] decryptConfig(byte[] bArr, int i);

    public native JNICreditBackData encryptClass(JNICreditBackData jNICreditBackData, int i);

    public native byte[] encryptConfig(byte[] bArr, int i);

    public native byte[] encryptPasswdLogin(byte[] bArr, int i);

    public native byte[] encryptPinBlock(byte[] bArr, int i, byte[] bArr2, int i2);

    public native int initResource(JNIInitBottomData jNIInitBottomData, byte[] bArr, byte[] bArr2);

    public native JNIBottomBackData packAndEncrypt(Data data, int i);

    public native int releaseResource();

    public native Data unpackAndDecrypt(byte[] bArr, int i, int i2);

    public native int validateSMSCode(SmsCodeVerfiyData smsCodeVerfiyData);
}
