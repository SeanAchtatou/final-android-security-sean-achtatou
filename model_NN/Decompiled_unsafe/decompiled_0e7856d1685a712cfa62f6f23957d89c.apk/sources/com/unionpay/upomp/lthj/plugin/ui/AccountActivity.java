package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.lthj.unipay.plugin.ae;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.ar;
import com.lthj.unipay.plugin.bl;
import com.lthj.unipay.plugin.bm;
import com.lthj.unipay.plugin.bn;
import com.lthj.unipay.plugin.bq;
import com.lthj.unipay.plugin.br;
import com.lthj.unipay.plugin.bs;
import com.lthj.unipay.plugin.bu;
import com.lthj.unipay.plugin.bv;
import com.lthj.unipay.plugin.bx;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cl;
import com.lthj.unipay.plugin.i;
import com.lthj.unipay.plugin.k;
import com.lthj.unipay.plugin.m;
import com.lthj.unipay.plugin.w;
import com.lthj.unipay.plugin.x;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.widget.CustomInputView;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import java.util.Timer;
import java.util.TimerTask;

public class AccountActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    public int a = 60;
    public TimerTask aaTimerTask;
    private View.OnClickListener b = new bl(this);
    private Button c;
    private Button d;
    private Button e;
    private Button f;
    private CustomInputView g;
    private CustomInputView h;
    private CustomInputView i;
    private EditText j;
    private Button k;
    /* access modifiers changed from: private */
    public EditText l;
    /* access modifiers changed from: private */
    public Button m;
    /* access modifiers changed from: private */
    public Handler n = new bx(this);

    private void b() {
        Intent intent = new Intent(this, BankCardInfoActivity.class);
        intent.addFlags(67108864);
        a().changeSubActivity(intent);
    }

    /* access modifiers changed from: private */
    public void c() {
        a(getString(PluginLink.getStringupomp_lthj_back()), new bn(this));
        setContentView(PluginLink.getLayoutupomp_lthj_myinfo());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bind_number_view());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(ap.a().x);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view())).a(ap.a().D);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(i.f(ap.a().y.toString()));
        this.e = (Button) findViewById(PluginLink.getIdupomp_lthj_button_bankcard_manage());
        if (ap.a().A == null || ap.a().A.size() == 0) {
            lineFrameView.a(PluginLink.getStringupomp_lthj_no_bind());
            this.e.setText(getString(PluginLink.getStringupomp_lthj_bind_now()));
            this.e.setOnClickListener(new bm(this));
        } else {
            lineFrameView.a(ap.a().A.size() + "张(上限" + ((int) ap.a().a.c()) + "张)");
            this.e.setOnClickListener(this);
        }
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_button_change_pwd());
        this.c.setOnClickListener(this);
        this.d = (Button) findViewById(PluginLink.getIdupomp_lthj_button_change_mobile());
        this.d.setOnClickListener(this);
    }

    private void d() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.b);
        setContentView(PluginLink.getLayoutupomp_lthj_changepassword());
        this.g = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_old_pwd_input());
        this.h = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_new_pwd_input());
        this.i = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        x xVar = new x(9);
        this.g.b().setOnTouchListener(xVar);
        this.g.b().setOnFocusChangeListener(xVar);
        x xVar2 = new x(8);
        this.h.b().setOnTouchListener(xVar2);
        this.h.b().setOnFocusChangeListener(xVar2);
        x xVar3 = new x(7);
        this.i.b().setOnTouchListener(xVar3);
        this.i.b().setOnFocusChangeListener(xVar3);
        this.j = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(ap.a().y);
        this.f = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.f.setOnClickListener(this);
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.m.setOnClickListener(new bu(this));
    }

    private void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.b);
        setContentView(PluginLink.getLayoutupomp_lthj_changemobile());
        this.l = (EditText) findViewById(PluginLink.getIdupomp_lthj_new_mobilenum_edit());
        this.j = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(ap.a().y);
        this.k = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.k.setOnClickListener(this);
        this.m = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.m.setOnClickListener(new bs(this));
    }

    public void errorCallBack(String str) {
        i.a(this, str);
    }

    public void onClick(View view) {
        if (view == this.c) {
            d();
        } else if (view == this.d) {
            e();
        } else if (view == this.e) {
            b();
        } else if (view == this.f) {
            if (c.e(this, this.g.b()) && c.f(this, this.h.b()) && c.d(this, this.i.b()) && c.a(this)) {
                cl.b(this, this, this.j.getText().toString());
            }
        } else if (view == this.k && c.b(this, this.l) && c.a(this, this.j)) {
            cl.c(this, this, this.l.getText().toString(), this.j.getText().toString());
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        c();
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new bv(this);
            timer.schedule(this.aaTimerTask, 0, 1000);
        }
    }

    public void responseCallBack(w wVar) {
        if (wVar != null && wVar.m() != null && !isFinishing()) {
            int e2 = wVar.e();
            int parseInt = Integer.parseInt(wVar.m());
            if ("5309".equals(wVar.m())) {
                k.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new br(this));
                return;
            }
            switch (e2) {
                case 8196:
                    if (parseInt == 0) {
                        ap.a().z.setLength(0);
                        ap.a().z.append(z.a().d);
                        z.a().d.setLength(0);
                        k.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_pwd_success()), new bq(this));
                        return;
                    }
                    i.a(this, wVar.n(), parseInt);
                    d();
                    return;
                case 8197:
                    if (parseInt == 0) {
                        ap.a().y.setLength(0);
                        ap.a().y.append(((ar) wVar).a());
                        k.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_change_mobile_success()), new ae(this));
                        return;
                    }
                    i.a(this, wVar.n(), parseInt);
                    e();
                    return;
                case 8198:
                case 8199:
                default:
                    return;
                case 8200:
                    m mVar = (m) wVar;
                    if (parseInt == 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        ap.a().v.delete(0, ap.a().v.length());
                        ap.a().v.append(mVar.c());
                        processRefreshConn();
                        return;
                    }
                    Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + mVar.n() + ",错误码为：" + parseInt, 1);
                    makeText2.setGravity(17, 0, 0);
                    makeText2.show();
                    this.m.setEnabled(true);
                    this.m.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                    return;
            }
        }
    }
}
