package com.unionpay.upomp.lthj.link;

import android.content.Context;
import android.util.Log;

public class PluginLink {
    private static Context context;

    public static int getDrawableupomp_lthj_button_gray() {
        return getIdentifier(context, "drawable", "upomp_lthj_button_gray");
    }

    public static int getDrawableupomp_lthj_common_drop() {
        return getIdentifier(context, "drawable", "upomp_lthj_common_drop");
    }

    public static int getDrawableupomp_lthj_default_drop() {
        return getIdentifier(context, "drawable", "upomp_lthj_default_drop");
    }

    public static int getDrawableupomp_lthj_dialog_ok_color() {
        return getIdentifier(context, "drawable", "upomp_lthj_dialog_ok_color");
    }

    public static int getDrawableupomp_lthj_fail_icon() {
        return getIdentifier(context, "drawable", "upomp_lthj_fail_icon");
    }

    public static int getDrawableupomp_lthj_info_down_btn() {
        return getIdentifier(context, "drawable", "upomp_lthj_info_down_btn");
    }

    public static int getDrawableupomp_lthj_info_up_btn() {
        return getIdentifier(context, "drawable", "upomp_lthj_info_up_btn");
    }

    public static int getDrawableupomp_lthj_keybtn_enlarge() {
        return getIdentifier(context, "drawable", "upomp_lthj_keybtn_enlarge");
    }

    public static int getDrawableupomp_lthj_pay_icon() {
        return getIdentifier(context, "drawable", "upomp_lthj_pay_icon");
    }

    public static int getDrawableupomp_lthj_set_default_icon() {
        return getIdentifier(context, "drawable", "upomp_lthj_set_default_icon");
    }

    public static int getDrawableupomp_lthj_success_icon() {
        return getIdentifier(context, "drawable", "upomp_lthj_success_icon");
    }

    public static int getDrawableupomp_lthj_unbind_icon() {
        return getIdentifier(context, "drawable", "upomp_lthj_unbind_icon");
    }

    private static final int getIdentifier(Context context2, String str, String str2) {
        int identifier = context2.getResources().getIdentifier(str2, str, context2.getPackageName());
        if (identifier == 0) {
            Log.e("PluginLinkWrapper", "res/" + str + "/" + str2 + " not found.");
        }
        return identifier;
    }

    public static int getIdupomp_lthj_about_btn() {
        return getIdentifier(context, "id", "upomp_lthj_about_btn");
    }

    public static int getIdupomp_lthj_account_mange_btn() {
        return getIdentifier(context, "id", "upomp_lthj_account_mange_btn");
    }

    public static int getIdupomp_lthj_add_bankcard_btn() {
        return getIdentifier(context, "id", "upomp_lthj_add_bankcard_btn");
    }

    public static int getIdupomp_lthj_auto_welcome_btn() {
        return getIdentifier(context, "id", "upomp_lthj_auto_welcome_btn");
    }

    public static int getIdupomp_lthj_bankcard_info_view() {
        return getIdentifier(context, "id", "upomp_lthj_bankcard_info_view");
    }

    public static int getIdupomp_lthj_bankcard_listview() {
        return getIdentifier(context, "id", "upomp_lthj_bankcard_listview");
    }

    public static int getIdupomp_lthj_bankcard_listview_childicon() {
        return getIdentifier(context, "id", "upomp_lthj_bankcard_listview_childicon");
    }

    public static int getIdupomp_lthj_bankcard_listview_childtv() {
        return getIdentifier(context, "id", "upomp_lthj_bankcard_listview_childtv");
    }

    public static int getIdupomp_lthj_bind_number_view() {
        return getIdentifier(context, "id", "upomp_lthj_bind_number_view");
    }

    public static int getIdupomp_lthj_build_no() {
        return getIdentifier(context, "id", "upomp_lthj_build_no");
    }

    public static int getIdupomp_lthj_button_bankcard_manage() {
        return getIdentifier(context, "id", "upomp_lthj_button_bankcard_manage");
    }

    public static int getIdupomp_lthj_button_cancel() {
        return getIdentifier(context, "id", "upomp_lthj_button_cancel");
    }

    public static int getIdupomp_lthj_button_change_mobile() {
        return getIdentifier(context, "id", "upomp_lthj_button_change_mobile");
    }

    public static int getIdupomp_lthj_button_change_pwd() {
        return getIdentifier(context, "id", "upomp_lthj_button_change_pwd");
    }

    public static int getIdupomp_lthj_button_ok() {
        return getIdentifier(context, "id", "upomp_lthj_button_ok");
    }

    public static int getIdupomp_lthj_card_menu_drop() {
        return getIdentifier(context, "id", "upomp_lthj_card_menu_drop");
    }

    public static int getIdupomp_lthj_card_num_edit() {
        return getIdentifier(context, "id", "upomp_lthj_card_num_edit");
    }

    public static int getIdupomp_lthj_confirm_pwd_input() {
        return getIdentifier(context, "id", "upomp_lthj_confirm_pwd_input");
    }

    public static int getIdupomp_lthj_container() {
        return getIdentifier(context, "id", "upomp_lthj_container");
    }

    public static int getIdupomp_lthj_custominput_et() {
        return getIdentifier(context, "id", "upomp_lthj_custominput_et");
    }

    public static int getIdupomp_lthj_custominput_tv() {
        return getIdentifier(context, "id", "upomp_lthj_custominput_tv");
    }

    public static int getIdupomp_lthj_cvn2_edit() {
        return getIdentifier(context, "id", "upomp_lthj_cvn2_edit");
    }

    public static int getIdupomp_lthj_cvn2_help() {
        return getIdentifier(context, "id", "upomp_lthj_cvn2_help");
    }

    public static int getIdupomp_lthj_cvn2_layout() {
        return getIdentifier(context, "id", "upomp_lthj_cvn2_layout");
    }

    public static int getIdupomp_lthj_date_edit() {
        return getIdentifier(context, "id", "upomp_lthj_date_edit");
    }

    public static int getIdupomp_lthj_date_help() {
        return getIdentifier(context, "id", "upomp_lthj_date_help");
    }

    public static int getIdupomp_lthj_date_layout() {
        return getIdentifier(context, "id", "upomp_lthj_date_layout");
    }

    public static int getIdupomp_lthj_default_card() {
        return getIdentifier(context, "id", "upomp_lthj_default_card");
    }

    public static int getIdupomp_lthj_default_checkbox() {
        return getIdentifier(context, "id", "upomp_lthj_default_checkbox");
    }

    public static final int getIdupomp_lthj_desc_line() {
        return getIdentifier(context, "id", "upomp_lthj_desc_line");
    }

    public static int getIdupomp_lthj_dialog_message() {
        return getIdentifier(context, "id", "upomp_lthj_dialog_message");
    }

    public static int getIdupomp_lthj_dialog_progress() {
        return getIdentifier(context, "id", "upomp_lthj_dialog_progress");
    }

    public static int getIdupomp_lthj_error_desc() {
        return getIdentifier(context, "id", "upomp_lthj_error_desc");
    }

    public static int getIdupomp_lthj_forget_pwd() {
        return getIdentifier(context, "id", "upomp_lthj_forget_pwd");
    }

    public static int getIdupomp_lthj_get_imgcode() {
        return getIdentifier(context, "id", "upomp_lthj_get_imgcode");
    }

    public static int getIdupomp_lthj_get_mac_btn() {
        return getIdentifier(context, "id", "upomp_lthj_get_mac_btn");
    }

    public static int getIdupomp_lthj_help_btn() {
        return getIdentifier(context, "id", "upomp_lthj_help_btn");
    }

    public static int getIdupomp_lthj_homebackbutton() {
        return getIdentifier(context, "id", "upomp_lthj_homebackbutton");
    }

    public static final int getIdupomp_lthj_imgview_checkword() {
        return getIdentifier(context, "id", "upomp_lthj_imgview_checkword");
    }

    public static final int getIdupomp_lthj_keyboardButtonNum() {
        return getIdentifier(context, "id", "upomp_lthj_keyboardButtonNum");
    }

    public static final int getIdupomp_lthj_keyboard_button0() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button0");
    }

    public static final int getIdupomp_lthj_keyboard_button1() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button1");
    }

    public static final int getIdupomp_lthj_keyboard_button2() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button2");
    }

    public static final int getIdupomp_lthj_keyboard_button3() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button3");
    }

    public static final int getIdupomp_lthj_keyboard_button4() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button4");
    }

    public static final int getIdupomp_lthj_keyboard_button5() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button5");
    }

    public static final int getIdupomp_lthj_keyboard_button6() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button6");
    }

    public static final int getIdupomp_lthj_keyboard_button7() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button7");
    }

    public static final int getIdupomp_lthj_keyboard_button8() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button8");
    }

    public static final int getIdupomp_lthj_keyboard_button9() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button9");
    }

    public static final int getIdupomp_lthj_keyboard_buttonC() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_buttonC");
    }

    public static final int getIdupomp_lthj_keyboard_buttonLetter() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_buttonLetter");
    }

    public static final int getIdupomp_lthj_keyboard_buttonOK() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_buttonOK");
    }

    public static final int getIdupomp_lthj_keyboard_buttonSign() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_buttonSign");
    }

    public static final int getIdupomp_lthj_keyboard_button_a() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_a");
    }

    public static final int getIdupomp_lthj_keyboard_button_b() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_b");
    }

    public static final int getIdupomp_lthj_keyboard_button_c() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_c");
    }

    public static final int getIdupomp_lthj_keyboard_button_d() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_d");
    }

    public static final int getIdupomp_lthj_keyboard_button_e() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_e");
    }

    public static final int getIdupomp_lthj_keyboard_button_f() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_f");
    }

    public static final int getIdupomp_lthj_keyboard_button_g() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_g");
    }

    public static final int getIdupomp_lthj_keyboard_button_h() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_h");
    }

    public static final int getIdupomp_lthj_keyboard_button_i() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_i");
    }

    public static final int getIdupomp_lthj_keyboard_button_j() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_j");
    }

    public static final int getIdupomp_lthj_keyboard_button_k() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_k");
    }

    public static final int getIdupomp_lthj_keyboard_button_l() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_l");
    }

    public static final int getIdupomp_lthj_keyboard_button_m() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_m");
    }

    public static final int getIdupomp_lthj_keyboard_button_n() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_n");
    }

    public static final int getIdupomp_lthj_keyboard_button_o() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_o");
    }

    public static final int getIdupomp_lthj_keyboard_button_p() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_p");
    }

    public static final int getIdupomp_lthj_keyboard_button_q() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_q");
    }

    public static final int getIdupomp_lthj_keyboard_button_r() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_r");
    }

    public static final int getIdupomp_lthj_keyboard_button_s() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_s");
    }

    public static final int getIdupomp_lthj_keyboard_button_shift() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_shift");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign1() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign1");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign10() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign10");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign11() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign11");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign12() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign12");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign13() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign13");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign14() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign14");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign15() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign15");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign16() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign16");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign17() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign17");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign18() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign18");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign19() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign19");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign2() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign2");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign20() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign20");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign21() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign21");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign22() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign22");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign23() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign23");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign24() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign24");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign25() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign25");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign3() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign3");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign4() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign4");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign5() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign5");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign6() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign6");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign7() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign7");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign8() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign8");
    }

    public static final int getIdupomp_lthj_keyboard_button_sign9() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_sign9");
    }

    public static final int getIdupomp_lthj_keyboard_button_signnext() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_signnext");
    }

    public static final int getIdupomp_lthj_keyboard_button_t() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_t");
    }

    public static final int getIdupomp_lthj_keyboard_button_u() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_u");
    }

    public static final int getIdupomp_lthj_keyboard_button_v() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_v");
    }

    public static final int getIdupomp_lthj_keyboard_button_w() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_w");
    }

    public static final int getIdupomp_lthj_keyboard_button_x() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_x");
    }

    public static final int getIdupomp_lthj_keyboard_button_y() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_y");
    }

    public static final int getIdupomp_lthj_keyboard_button_z() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_button_z");
    }

    public static final int getIdupomp_lthj_keyboard_editText() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_editText");
    }

    public static final int getIdupomp_lthj_keyboard_layoutRight() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_layoutRight");
    }

    public static final int getIdupomp_lthj_keyboard_view() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_view");
    }

    public static final int getIdupomp_lthj_keyboardtitle() {
        return getIdentifier(context, "id", "upomp_lthj_keyboard_title");
    }

    public static int getIdupomp_lthj_lineframe_image() {
        return getIdentifier(context, "id", "upomp_lthj_lineframe_image");
    }

    public static int getIdupomp_lthj_lineframe_title() {
        return getIdentifier(context, "id", "upomp_lthj_lineframe_title");
    }

    public static int getIdupomp_lthj_lineframe_tv() {
        return getIdentifier(context, "id", "upomp_lthj_lineframe_tv");
    }

    public static int getIdupomp_lthj_list_openview() {
        return getIdentifier(context, "id", "upomp_lthj_list_openview");
    }

    public static int getIdupomp_lthj_merchant_tv() {
        return getIdentifier(context, "id", "upomp_lthj_merchant_tv");
    }

    public static int getIdupomp_lthj_mobile_num_edit() {
        return getIdentifier(context, "id", "upomp_lthj_mobile_num_edit");
    }

    public static int getIdupomp_lthj_mobile_number_view() {
        return getIdentifier(context, "id", "upomp_lthj_mobile_number_view");
    }

    public static int getIdupomp_lthj_mobilemac_edit() {
        return getIdentifier(context, "id", "upomp_lthj_mobilemac_edit");
    }

    public static int getIdupomp_lthj_myinfo_view() {
        return getIdentifier(context, "id", "upomp_lthj_myinfo_view");
    }

    public static int getIdupomp_lthj_new_mobilenum_edit() {
        return getIdentifier(context, "id", "upomp_lthj_new_mobilenum_edit");
    }

    public static int getIdupomp_lthj_new_pwd_input() {
        return getIdentifier(context, "id", "upomp_lthj_new_pwd_input");
    }

    public static int getIdupomp_lthj_next_btn() {
        return getIdentifier(context, "id", "upomp_lthj_next_btn");
    }

    public static int getIdupomp_lthj_no_card_layout() {
        return getIdentifier(context, "id", "upomp_lthj_no_card_layout");
    }

    public static int getIdupomp_lthj_old_pwd_input() {
        return getIdentifier(context, "id", "upomp_lthj_old_pwd_input");
    }

    public static int getIdupomp_lthj_orderamt_tv() {
        return getIdentifier(context, "id", "upomp_lthj_orderamt_tv");
    }

    public static int getIdupomp_lthj_orderno_row() {
        return getIdentifier(context, "id", "upomp_lthj_orderno_row");
    }

    public static int getIdupomp_lthj_orderno_tv() {
        return getIdentifier(context, "id", "upomp_lthj_orderno_tv");
    }

    public static int getIdupomp_lthj_ordertime_tv() {
        return getIdentifier(context, "id", "upomp_lthj_ordertime_tv");
    }

    public static int getIdupomp_lthj_password_edit() {
        return getIdentifier(context, "id", "upomp_lthj_password_edit");
    }

    public static final int getIdupomp_lthj_pay_desc() {
        return getIdentifier(context, "id", "upomp_lthj_pay_desc");
    }

    public static final int getIdupomp_lthj_pay_state() {
        return getIdentifier(context, "id", "upomp_lthj_pay_state");
    }

    public static int getIdupomp_lthj_pin_edit() {
        return getIdentifier(context, "id", "upomp_lthj_pin_edit");
    }

    public static int getIdupomp_lthj_pin_layout() {
        return getIdentifier(context, "id", "upomp_lthj_pin_layout");
    }

    public static int getIdupomp_lthj_quickpaycard_layout() {
        return getIdentifier(context, "id", "upomp_lthj_quickpaycard_layout");
    }

    public static int getIdupomp_lthj_refrush_btn() {
        return getIdentifier(context, "id", "upomp_lthj_refrush_btn");
    }

    public static int getIdupomp_lthj_reg_pro_btn() {
        return getIdentifier(context, "id", "upomp_lthj_reg_pro_btn");
    }

    public static int getIdupomp_lthj_reg_prompt() {
        return getIdentifier(context, "id", "upomp_lthj_reg_prompt");
    }

    public static int getIdupomp_lthj_safe_ask_drop() {
        return getIdentifier(context, "id", "upomp_lthj_safe_ask_drop");
    }

    public static int getIdupomp_lthj_safe_ask_view() {
        return getIdentifier(context, "id", "upomp_lthj_safe_ask_view");
    }

    public static int getIdupomp_lthj_safe_asw_view() {
        return getIdentifier(context, "id", "upomp_lthj_safe_asw_view");
    }

    public static int getIdupomp_lthj_safe_prompt_view() {
        return getIdentifier(context, "id", "upomp_lthj_safe_prompt_view");
    }

    public static int getIdupomp_lthj_savecard_listview() {
        return getIdentifier(context, "id", "upomp_lthj_savecard_listview");
    }

    public static int getIdupomp_lthj_splash_seekbar() {
        return getIdentifier(context, "id", "upomp_lthj_splash_seekbar");
    }

    public static final int getIdupomp_lthj_state_view() {
        return getIdentifier(context, "id", "upomp_lthj_state_view");
    }

    public static int getIdupomp_lthj_sup_bank_item() {
        return getIdentifier(context, "id", "upomp_lthj_sup_bank_item");
    }

    public static int getIdupomp_lthj_sup_credit_item() {
        return getIdentifier(context, "id", "upomp_lthj_sup_credit_item");
    }

    public static int getIdupomp_lthj_sup_debit_item() {
        return getIdentifier(context, "id", "upomp_lthj_sup_debit_item");
    }

    public static int getIdupomp_lthj_support_card_title1() {
        return getIdentifier(context, "id", "upomp_lthj_support_card_title1");
    }

    public static int getIdupomp_lthj_support_card_title2() {
        return getIdentifier(context, "id", "upomp_lthj_support_card_title2");
    }

    public static int getIdupomp_lthj_support_creditcard_tv() {
        return getIdentifier(context, "id", "upomp_lthj_support_creditcard_tv");
    }

    public static int getIdupomp_lthj_support_debitcard_tv() {
        return getIdentifier(context, "id", "upomp_lthj_support_debitcard_tv");
    }

    public static int getIdupomp_lthj_textview_time() {
        return getIdentifier(context, "id", "upomp_lthj_textview_time");
    }

    public static int getIdupomp_lthj_trantime_row() {
        return getIdentifier(context, "id", "upomp_lthj_trantime_row");
    }

    public static int getIdupomp_lthj_unfold_btn() {
        return getIdentifier(context, "id", "upomp_lthj_unfold_btn");
    }

    public static int getIdupomp_lthj_use_card_pay() {
        return getIdentifier(context, "id", "upomp_lthj_use_card_pay");
    }

    public static int getIdupomp_lthj_use_quick_pay() {
        return getIdentifier(context, "id", "upomp_lthj_use_quick_pay");
    }

    public static int getIdupomp_lthj_user_pro_box() {
        return getIdentifier(context, "id", "upomp_lthj_user_pro_box");
    }

    public static int getIdupomp_lthj_username_edit() {
        return getIdentifier(context, "id", "upomp_lthj_username_edit");
    }

    public static int getIdupomp_lthj_username_view() {
        return getIdentifier(context, "id", "upomp_lthj_username_view");
    }

    public static int getIdupomp_lthj_userprotocol_content() {
        return getIdentifier(context, "id", "upomp_lthj_userprotocol_content");
    }

    public static int getIdupomp_lthj_userprotocol_listview() {
        return getIdentifier(context, "id", "upomp_lthj_userprotocol_listview");
    }

    public static int getIdupomp_lthj_userprotocol_title() {
        return getIdentifier(context, "id", "upomp_lthj_userprotocol_title");
    }

    public static final int getIdupomp_lthj_validatecode_edit() {
        return getIdentifier(context, "id", "upomp_lthj_validatecode_edit");
    }

    public static final int getIdupomp_lthj_validatecode_img() {
        return getIdentifier(context, "id", "upomp_lthj_validatecode_img");
    }

    public static final int getIdupomp_lthj_validatecode_layout() {
        return getIdentifier(context, "id", "upomp_lthj_validatecode_layout");
    }

    public static final int getIdupomp_lthj_validatecode_progress() {
        return getIdentifier(context, "id", "upomp_lthj_validatecode_progress");
    }

    public static int getIdupomp_lthj_welcome_view() {
        return getIdentifier(context, "id", "upomp_lthj_welcome_view");
    }

    public static int getLayoutupomp_lthj_about() {
        return getIdentifier(context, "layout", "upomp_lthj_about");
    }

    public static int getLayoutupomp_lthj_bankcard_item() {
        return getIdentifier(context, "layout", "upomp_lthj_bankcard_item");
    }

    public static int getLayoutupomp_lthj_bankcard_itemexpad() {
        return getIdentifier(context, "layout", "upomp_lthj_bankcard_itemexpad");
    }

    public static int getLayoutupomp_lthj_bankcard_list() {
        return getIdentifier(context, "layout", "upomp_lthj_bankcard_list");
    }

    public static int getLayoutupomp_lthj_bindcard_home() {
        return getIdentifier(context, "layout", "upomp_lthj_bindcard_home");
    }

    public static int getLayoutupomp_lthj_bindcard_next() {
        return getIdentifier(context, "layout", "upomp_lthj_bindcard_next");
    }

    public static int getLayoutupomp_lthj_cardinfo_tip() {
        return getIdentifier(context, "layout", "upomp_lthj_cardinfo_tip");
    }

    public static int getLayoutupomp_lthj_changemobile() {
        return getIdentifier(context, "layout", "upomp_lthj_changemobile");
    }

    public static int getLayoutupomp_lthj_changepassword() {
        return getIdentifier(context, "layout", "upomp_lthj_changepassword");
    }

    public static int getLayoutupomp_lthj_commonpay() {
        return getIdentifier(context, "layout", "upomp_lthj_commonpay");
    }

    public static int getLayoutupomp_lthj_custominput() {
        return getIdentifier(context, "layout", "upomp_lthj_custominput");
    }

    public static int getLayoutupomp_lthj_findpwd_home() {
        return getIdentifier(context, "layout", "upomp_lthj_findpwd_home");
    }

    public static int getLayoutupomp_lthj_findpwd_next() {
        return getIdentifier(context, "layout", "upomp_lthj_findpwd_next");
    }

    public static int getLayoutupomp_lthj_homeaccountpay() {
        return getIdentifier(context, "layout", "upomp_lthj_homeaccount");
    }

    public static int getLayoutupomp_lthj_homecardpay() {
        return getIdentifier(context, "layout", "upomp_lthj_homecardpay");
    }

    public static int getLayoutupomp_lthj_index() {
        return getIdentifier(context, "layout", "upomp_lthj_index");
    }

    public static int getLayoutupomp_lthj_keyboard() {
        return getIdentifier(context, "layout", "upomp_lthj_keyboard");
    }

    public static int getLayoutupomp_lthj_keyboard_letter() {
        return getIdentifier(context, "layout", "upomp_lthj_keyboard_letter");
    }

    public static int getLayoutupomp_lthj_keyboard_num() {
        return getIdentifier(context, "layout", "upomp_lthj_keyboard_num");
    }

    public static int getLayoutupomp_lthj_keyboard_sign() {
        return getIdentifier(context, "layout", "upomp_lthj_keyboard_sign");
    }

    public static int getLayoutupomp_lthj_lineframe() {
        return getIdentifier(context, "layout", "upomp_lthj_lineframe");
    }

    public static int getLayoutupomp_lthj_myinfo() {
        return getIdentifier(context, "layout", "upomp_lthj_myinfo");
    }

    public static int getLayoutupomp_lthj_onebtn_progress() {
        return getIdentifier(context, "layout", "upomp_lthj_onebtn_progress");
    }

    public static int getLayoutupomp_lthj_quick_bind_result() {
        return getIdentifier(context, "layout", "upomp_lthj_quick_bind_result");
    }

    public static int getLayoutupomp_lthj_quick_bindcard() {
        return getIdentifier(context, "layout", "upomp_lthj_quick_bindcard");
    }

    public static int getLayoutupomp_lthj_quick_reg_confirm() {
        return getIdentifier(context, "layout", "upomp_lthj_quick_reg_confirm");
    }

    public static int getLayoutupomp_lthj_quick_reg_result() {
        return getIdentifier(context, "layout", "upomp_lthj_quick_reg_result");
    }

    public static int getLayoutupomp_lthj_quick_register() {
        return getIdentifier(context, "layout", "upomp_lthj_quick_register");
    }

    public static int getLayoutupomp_lthj_quickpay_hascard() {
        return getIdentifier(context, "layout", "upomp_lthj_quickpay_hascard");
    }

    public static int getLayoutupomp_lthj_quickpay_nocard() {
        return getIdentifier(context, "layout", "upomp_lthj_quickpay_nocard");
    }

    public static int getLayoutupomp_lthj_savecardpay() {
        return getIdentifier(context, "layout", "upomp_lthj_savecardpay");
    }

    public static int getLayoutupomp_lthj_splash() {
        return getIdentifier(context, "layout", "upomp_lthj_splash");
    }

    public static int getLayoutupomp_lthj_supportcard() {
        return getIdentifier(context, "layout", "upomp_lthj_supportcard");
    }

    public static int getLayoutupomp_lthj_supportcard_bankitem() {
        return getIdentifier(context, "layout", "upomp_lthj_supportcard_bankitem");
    }

    public static int getLayoutupomp_lthj_traderesult() {
        return getIdentifier(context, "layout", "upomp_lthj_traderesult");
    }

    public static int getLayoutupomp_lthj_twobtn_progress() {
        return getIdentifier(context, "layout", "upomp_lthj_twobtn_progress");
    }

    public static int getLayoutupomp_lthj_user_protocol() {
        return getIdentifier(context, "layout", "upomp_lthj_user_protocol");
    }

    public static int getLayoutupomp_lthj_userprotocal_item() {
        return getIdentifier(context, "layout", "upomp_lthj_userprotocal_item");
    }

    public static int getLayoutupomp_lthj_validatcodeview() {
        return getIdentifier(context, "layout", "upomp_lthj_validatcodeview");
    }

    public static int getRawupomp_lthj_config() {
        return getIdentifier(context, "raw", "upomp_lthj_config");
    }

    public static int getStringupomp_lthj_add_card() {
        return getIdentifier(context, "string", "upomp_lthj_add_card");
    }

    public static int getStringupomp_lthj_after_getmobilemacAgain() {
        return getIdentifier(context, "string", "upomp_lthj_after_getmobilemacAgain");
    }

    public static int getStringupomp_lthj_app_quitNotice_msg() {
        return getIdentifier(context, "string", "upomp_lthj_app_quitNotice_msg");
    }

    public static int getStringupomp_lthj_back() {
        return getIdentifier(context, "string", "upomp_lthj_back");
    }

    public static int getStringupomp_lthj_backpage() {
        return getIdentifier(context, "string", "upomp_lthj_backpage");
    }

    public static int getStringupomp_lthj_backtomerchant() {
        return getIdentifier(context, "string", "upomp_lthj_backtomerchant");
    }

    public static int getStringupomp_lthj_bind_fail() {
        return getIdentifier(context, "string", "upomp_lthj_bind_fail");
    }

    public static int getStringupomp_lthj_bind_now() {
        return getIdentifier(context, "string", "upomp_lthj_bind_now");
    }

    public static int getStringupomp_lthj_bind_result_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_bind_result_prompt");
    }

    public static int getStringupomp_lthj_bind_success() {
        return getIdentifier(context, "string", "upomp_lthj_bind_success");
    }

    public static int getStringupomp_lthj_cancel() {
        return getIdentifier(context, "string", "upomp_lthj_cancel");
    }

    public static int getStringupomp_lthj_change_mobile_success() {
        return getIdentifier(context, "string", "upomp_lthj_change_mobile_success");
    }

    public static int getStringupomp_lthj_change_pwd_success() {
        return getIdentifier(context, "string", "upomp_lthj_change_pwd_success");
    }

    public static int getStringupomp_lthj_check_support() {
        return getIdentifier(context, "string", "upomp_lthj_check_support");
    }

    public static int getStringupomp_lthj_choose_pay_card() {
        return getIdentifier(context, "string", "upomp_lthj_choose_pay_card");
    }

    public static int getStringupomp_lthj_choose_safeask() {
        return getIdentifier(context, "string", "upomp_lthj_choose_safeask");
    }

    public static int getStringupomp_lthj_click_get_mac() {
        return getIdentifier(context, "string", "upomp_lthj_get_mac");
    }

    public static int getStringupomp_lthj_close() {
        return getIdentifier(context, "string", "upomp_lthj_close");
    }

    public static int getStringupomp_lthj_confirm_pwd() {
        return getIdentifier(context, "string", "upomp_lthj_confirm_pwd");
    }

    public static int getStringupomp_lthj_custom_safeask() {
        return getIdentifier(context, "string", "upomp_lthj_custom_safeask");
    }

    public static int getStringupomp_lthj_cvn() {
        return getIdentifier(context, "string", "upomp_lthj_cvn");
    }

    public static final int getStringupomp_lthj_emailError_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_emailError_Format_prompt");
    }

    public static int getStringupomp_lthj_getting_mac() {
        return getIdentifier(context, "string", "upomp_lthj_getting_mac");
    }

    public static int getStringupomp_lthj_have_no_initfile() {
        return getIdentifier(context, "string", "upomp_lthj_have_no_initfile");
    }

    public static int getStringupomp_lthj_logout_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_logout_prompt");
    }

    public static int getStringupomp_lthj_merchantId_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantId_Empty_prompt");
    }

    public static int getStringupomp_lthj_merchantId_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantId_Length_prompt");
    }

    public static int getStringupomp_lthj_merchantOrderId_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantOrderId_Empty_prompt");
    }

    public static int getStringupomp_lthj_merchantOrderTime_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantOrderTime_Empty_prompt");
    }

    public static int getStringupomp_lthj_merchantOrderTime_error_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantOrderTime_error_prompt");
    }

    public static int getStringupomp_lthj_merchantXml_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantXml_Format_prompt");
    }

    public static int getStringupomp_lthj_merchantXml_HashFormat_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantXml_HashFormat_prompt");
    }

    public static int getStringupomp_lthj_merchantXml_MD5Error_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantXml_MD5Error_prompt");
    }

    public static int getStringupomp_lthj_merchantXml_ReadError_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_merchantXml_ReadError_prompt");
    }

    public static final int getStringupomp_lthj_mobileMacError_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_mobileMacError_Length_prompt");
    }

    public static int getStringupomp_lthj_multiple_user_login_tip() {
        return getIdentifier(context, "string", "upomp_lthj_multiple_user_login_tip");
    }

    public static int getStringupomp_lthj_nextpage() {
        return getIdentifier(context, "string", "upomp_lthj_nextpage");
    }

    public static int getStringupomp_lthj_no_bind() {
        return getIdentifier(context, "string", "upomp_lthj_no_bind");
    }

    public static int getStringupomp_lthj_no_save() {
        return getIdentifier(context, "string", "upomp_lthj_no_save");
    }

    public static int getStringupomp_lthj_no_support_card() {
        return getIdentifier(context, "string", "upomp_lthj_no_support_card");
    }

    public static int getStringupomp_lthj_ok() {
        return getIdentifier(context, "string", "upomp_lthj_ok");
    }

    public static int getStringupomp_lthj_password() {
        return getIdentifier(context, "string", "upomp_lthj_password");
    }

    public static int getStringupomp_lthj_pay_fail() {
        return getIdentifier(context, "string", "upomp_lthj_pay_fail");
    }

    public static int getStringupomp_lthj_pay_success() {
        return getIdentifier(context, "string", "upomp_lthj_pay_success");
    }

    public static int getStringupomp_lthj_rebind() {
        return getIdentifier(context, "string", "upomp_lthj_rebind");
    }

    public static int getStringupomp_lthj_reg_safe_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_reg_safe_prompt");
    }

    public static int getStringupomp_lthj_repay() {
        return getIdentifier(context, "string", "upomp_lthj_repay");
    }

    public static int getStringupomp_lthj_reset_pwd_success() {
        return getIdentifier(context, "string", "upomp_lthj_reset_pwd_success");
    }

    public static int getStringupomp_lthj_safe_ask_default() {
        return getIdentifier(context, "string", "upomp_lthj_safe_ask_default");
    }

    public static int getStringupomp_lthj_save_account() {
        return getIdentifier(context, "string", "upomp_lthj_save_account");
    }

    public static int getStringupomp_lthj_secureinfo() {
        return getIdentifier(context, "string", "upomp_lthj_secureinfo");
    }

    public static final int getStringupomp_lthj_sendMessageLose() {
        return getIdentifier(context, "string", "upomp_lthj_sendMessageLose");
    }

    public static final int getStringupomp_lthj_sendMessageSuccess() {
        return getIdentifier(context, "string", "upomp_lthj_sendMessageSuccess");
    }

    public static int getStringupomp_lthj_session_timeout_tip() {
        return getIdentifier(context, "string", "upomp_lthj_session_timeout_tip");
    }

    public static int getStringupomp_lthj_set_default() {
        return getIdentifier(context, "string", "upomp_lthj_set_default");
    }

    public static int getStringupomp_lthj_set_default_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_set_default_prompt");
    }

    public static int getStringupomp_lthj_unbind() {
        return getIdentifier(context, "string", "upomp_lthj_unbind");
    }

    public static int getStringupomp_lthj_unbind_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_unbind_prompt");
    }

    public static int getStringupomp_lthj_use_thecard() {
        return getIdentifier(context, "string", "upomp_lthj_use_thecard");
    }

    public static final int getStringupomp_lthj_validateBankPassWord_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateBankPassWord_Length_prompt");
    }

    public static final int getStringupomp_lthj_validateCVN2_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateCVN2_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateCVN2_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateCVN2_Format_prompt");
    }

    public static final int getStringupomp_lthj_validateCardPanPassword_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateCardPanPassword_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateCardPan_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateCardPan_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateChars_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateChars_Format_prompt");
    }

    public static final int getStringupomp_lthj_validateChars_Format_prompt2() {
        return getIdentifier(context, "string", "upomp_lthj_validateChars_Format_prompt2");
    }

    public static final int getStringupomp_lthj_validateEmail_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateEmail_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateEmail_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateEmail_Format_prompt");
    }

    public static final int getStringupomp_lthj_validateExpiryMonth_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateExpiryMonth_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateExpiryYear_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateExpiryYear_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateImageCode_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateImageCode_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateImageCode_Error_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateImageCode_Error_prompt");
    }

    public static final int getStringupomp_lthj_validateMobileMac_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateMobileMac_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateMobileMac_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateMobileMac_Length_prompt");
    }

    public static final int getStringupomp_lthj_validateMobileMac_Same_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateMobileMac_Same_prompt");
    }

    public static final int getStringupomp_lthj_validateMobileNum_FromatError_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateMobileNum_FromatError_prompt");
    }

    public static final int getStringupomp_lthj_validateMobileNum_LengthEmpty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateMobileNum_LengthEmpty_prompt");
    }

    public static final int getStringupomp_lthj_validateNewMobileNum_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateNewMobileNum_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateNewPassWord_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateNewPassWord_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateNewPassWord_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateNewPassWord_Length_prompt");
    }

    public static final int getStringupomp_lthj_validateNewPassword_Same_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateNewPassword_Same_prompt");
    }

    public static final int getStringupomp_lthj_validateNum_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateNum_Length_prompt");
    }

    public static final int getStringupomp_lthj_validateOldMobileNum_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateOldMobileNum_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateOldPassWord_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateOldPassWord_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateOldPassWord_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateOldPassWord_Length_prompt");
    }

    public static final int getStringupomp_lthj_validatePanPassword_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePanPassword_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validatePanPassword_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePanPassword_Length_prompt");
    }

    public static final int getStringupomp_lthj_validatePan_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePan_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validatePan_Format_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePan_Format_prompt");
    }

    public static final int getStringupomp_lthj_validatePassWord_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePassWord_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validatePassWord_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePassWord_Length_prompt");
    }

    public static final int getStringupomp_lthj_validatePassWord_Same_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePassWord_Same_prompt");
    }

    public static final int getStringupomp_lthj_validatePassword_complex_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validatePassword_complex_prompt");
    }

    public static final int getStringupomp_lthj_validatePassword_complex_prompt_kind() {
        return getIdentifier(context, "string", "upomp_lthj_validatePassword_complex_prompt_kind");
    }

    public static final int getStringupomp_lthj_validateSafeAnswer_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateSafeAnswer_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateSafeQuestion_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateSafeQuestion_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateUserConcert_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateUserConcert_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateUserName_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateUserName_Empty_prompt");
    }

    public static final int getStringupomp_lthj_validateUserName_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateUserName_Length_prompt");
    }

    public static final int getStringupomp_lthj_validateUser_Protocal_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateUser_Protocal_prompt");
    }

    public static final int getStringupomp_lthj_validateWelcomeWord_Empty_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_validateWelcomeWord_Empty_prompt");
    }

    public static final int getStringupomp_lthj_welcome_wordError_Length_prompt() {
        return getIdentifier(context, "string", "upomp_lthj_welcome_wordError_Length_prompt");
    }

    public static int getStyleupomp_lthj_common_dialog() {
        return getIdentifier(context, "style", "upomp_lthj_common_dialog");
    }

    public static int getStyleupomp_lthj_keyboard_dialog() {
        return getIdentifier(context, "style", "upomp_lthj_keyboard_dialog");
    }

    public static void setContext(Context context2) {
        context = context2;
    }
}
