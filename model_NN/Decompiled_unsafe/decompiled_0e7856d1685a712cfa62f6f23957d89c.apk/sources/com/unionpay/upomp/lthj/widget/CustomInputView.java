package com.unionpay.upomp.lthj.widget;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.a.a.g.c;
import com.lthj.unipay.plugin.at;
import com.unionpay.upomp.lthj.link.PluginLink;

public class CustomInputView extends LinearLayout {
    private static String a = "CustomInputView";
    private TextView b;
    private EditText c;
    private AttributeSet d;
    private Context e;

    public CustomInputView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = context;
        this.d = attributeSet;
        c();
    }

    private void c() {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(this.e).inflate(PluginLink.getLayoutupomp_lthj_custominput(), this);
        this.b = (TextView) linearLayout.findViewById(PluginLink.getIdupomp_lthj_custominput_tv());
        this.c = (EditText) linearLayout.findViewById(PluginLink.getIdupomp_lthj_custominput_et());
        int attributeResourceValue = this.d.getAttributeResourceValue("http://schemas.android.com/apk/res/android", c.TEXT_MESSAGE, 0);
        if (attributeResourceValue == 0) {
            at.a(a, "text-id is null");
            String attributeValue = this.d.getAttributeValue("http://schemas.android.com/apk/res/android", c.TEXT_MESSAGE);
            if (attributeValue != null) {
                this.b.setText(attributeValue);
            }
        } else {
            this.b.setText(attributeResourceValue);
        }
        int attributeResourceValue2 = this.d.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "hint", 0);
        if (attributeResourceValue2 == 0) {
            at.a(a, "title-id is null");
            String attributeValue2 = this.d.getAttributeValue("http://schemas.android.com/apk/res/android", "hint");
            if (attributeValue2 != null) {
                this.c.setHint(attributeValue2);
            }
        } else {
            this.c.setHint(attributeResourceValue2);
        }
        if ("phone".equals(this.d.getAttributeValue("http://schemas.android.com/apk/res/android", "inputType"))) {
            this.c.setInputType(3);
        }
    }

    public Editable a() {
        return this.c.getText();
    }

    public void a(int i) {
        this.c.setText(i);
    }

    public void a(CharSequence charSequence) {
        this.c.setText(charSequence);
    }

    public EditText b() {
        return this.c;
    }
}
