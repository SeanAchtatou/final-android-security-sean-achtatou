package com.unionpay.upomp.lthj.plugin.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.a.a.h.b;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.aq;
import com.lthj.unipay.plugin.aw;
import com.lthj.unipay.plugin.ax;
import com.lthj.unipay.plugin.ay;
import com.lthj.unipay.plugin.az;
import com.lthj.unipay.plugin.ba;
import com.lthj.unipay.plugin.bc;
import com.lthj.unipay.plugin.bd;
import com.lthj.unipay.plugin.be;
import com.lthj.unipay.plugin.bk;
import com.lthj.unipay.plugin.bt;
import com.lthj.unipay.plugin.bw;
import com.lthj.unipay.plugin.bz;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.ck;
import com.lthj.unipay.plugin.cl;
import com.lthj.unipay.plugin.cp;
import com.lthj.unipay.plugin.dd;
import com.lthj.unipay.plugin.de;
import com.lthj.unipay.plugin.df;
import com.lthj.unipay.plugin.dg;
import com.lthj.unipay.plugin.dh;
import com.lthj.unipay.plugin.di;
import com.lthj.unipay.plugin.dj;
import com.lthj.unipay.plugin.dk;
import com.lthj.unipay.plugin.dl;
import com.lthj.unipay.plugin.dm;
import com.lthj.unipay.plugin.dn;
import com.lthj.unipay.plugin.du;
import com.lthj.unipay.plugin.ea;
import com.lthj.unipay.plugin.ee;
import com.lthj.unipay.plugin.ek;
import com.lthj.unipay.plugin.f;
import com.lthj.unipay.plugin.i;
import com.lthj.unipay.plugin.j;
import com.lthj.unipay.plugin.k;
import com.lthj.unipay.plugin.m;
import com.lthj.unipay.plugin.r;
import com.lthj.unipay.plugin.w;
import com.lthj.unipay.plugin.x;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.model.SmsCodeVerfiyData;
import com.unionpay.upomp.lthj.widget.CustomInputView;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

public class PayActivity extends BaseActivity implements View.OnClickListener, UIResponseListener {
    private static String b = "PayActivity";
    private ImageButton A;
    private RelativeLayout B;
    private ValidateCodeView C;
    private View.OnClickListener D = new be(this);
    private View.OnClickListener E = new bd(this);
    /* access modifiers changed from: private */
    public Handler F = new dl(this);
    private CustomInputView G;
    private CustomInputView H;
    private Dialog I;
    private Button J;
    private Button K;
    private Button L;
    private CheckBox M;
    private Button N;
    public int a = 60;
    public TimerTask aaTimerTask;
    private String c;
    private EditText d;
    private EditText e;
    private EditText f;
    private Button g;
    private String h;
    private String i;
    /* access modifiers changed from: private */
    public String j;
    private EditText k;
    /* access modifiers changed from: private */
    public LineFrameView l;
    /* access modifiers changed from: private */
    public LineFrameView m;
    private LineFrameView n;
    /* access modifiers changed from: private */
    public Button o;
    private Button p;
    /* access modifiers changed from: private */
    public CustomInputView q;
    /* access modifiers changed from: private */
    public CustomInputView r;
    private CustomInputView s;
    private Button t;
    /* access modifiers changed from: private */
    public String[] u;
    private Button v;
    private Button w;
    private Button x;
    private bt y;
    private Button z;

    private void a(bz bzVar) {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_bind_result());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_state_view());
        LineFrameView lineFrameView2 = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_error_desc());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(bzVar.a());
        if (Integer.parseInt(bzVar.m()) == 0) {
            findViewById(PluginLink.getIdupomp_lthj_desc_line()).setVisibility(8);
            lineFrameView2.setVisibility(8);
            lineFrameView.a(PluginLink.getStringupomp_lthj_bind_success());
            lineFrameView.b(PluginLink.getDrawableupomp_lthj_success_icon());
        } else {
            lineFrameView.a(PluginLink.getStringupomp_lthj_bind_fail());
            lineFrameView2.a(bzVar.n());
            lineFrameView.b(PluginLink.getDrawableupomp_lthj_fail_icon());
        }
        lineFrameView.a().setTextColor(-65536);
        ((Button) findViewById(PluginLink.getIdupomp_lthj_button_ok())).setOnClickListener(new aw(this));
    }

    private void a(cp cpVar) {
        if (Integer.parseInt(cpVar.m()) == 0) {
            g();
            return;
        }
        setContentView(PluginLink.getLayoutupomp_lthj_quick_reg_result());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_reg_prompt())).a().setTextColor(-65536);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_error_desc())).a().setText(cpVar.n());
        ((Button) findViewById(PluginLink.getIdupomp_lthj_button_ok())).setOnClickListener(new ax(this));
    }

    private void a(du duVar) {
        a(getString(PluginLink.getStringupomp_lthj_backtomerchant()), this.E);
        setContentView(PluginLink.getLayoutupomp_lthj_traderesult());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_pay_state());
        LineFrameView lineFrameView2 = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_pay_desc());
        lineFrameView2.a().setTextColor(-65536);
        lineFrameView.a().setTextColor(-65536);
        ap.a().E = duVar.c();
        ap.a().F = duVar.a();
        ap.a().G = duVar.d();
        ap.a().H = duVar.o();
        ap.a().p = duVar.m();
        ap.a().q = duVar.n();
        int parseInt = Integer.parseInt(duVar.m());
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_merchant_tv())).setText(ap.a().f);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderamt_tv())).setText(i.d(ap.a().j));
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_orderno_tv())).setText(ap.a().h);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_ordertime_tv())).setText(i.c(ap.a().i));
        ((Button) findViewById(PluginLink.getIdupomp_lthj_unfold_btn())).setOnClickListener(new dh(this));
        if (parseInt == 0) {
            if (this.h == b.SMS_RESULT_SEND_FAIL) {
                r.a().c(false);
            } else if (this.h == b.SMS_RESULT_RECV_SUCCESS) {
                r.a().d(false);
            }
            lineFrameView.a(PluginLink.getStringupomp_lthj_pay_success());
            findViewById(PluginLink.getIdupomp_lthj_desc_line()).setVisibility(8);
            lineFrameView2.setVisibility(8);
            this.z = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
            if (this.h.equals(b.SMS_RESULT_SEND_FAIL)) {
                this.z.setOnClickListener(new dg(this));
                return;
            }
            lineFrameView2.a(duVar.n());
            this.z.setText(PluginLink.getStringupomp_lthj_backtomerchant());
            this.z.setOnClickListener(new df(this));
            return;
        }
        if (this.h == b.SMS_RESULT_SEND_FAIL) {
            r.a().c(true);
        } else if (this.h == b.SMS_RESULT_RECV_SUCCESS) {
            r.a().d(true);
        }
        lineFrameView.b(PluginLink.getDrawableupomp_lthj_fail_icon());
        lineFrameView.a(PluginLink.getStringupomp_lthj_pay_fail());
        lineFrameView2.a(duVar.n() + "(" + duVar.m() + ")");
        this.z = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.z.setText(PluginLink.getStringupomp_lthj_repay());
        this.z.setOnClickListener(new de(this));
    }

    private void b() {
        this.h = b.SMS_RESULT_SEND_FAIL;
        setContentView(PluginLink.getLayoutupomp_lthj_commonpay());
        a(getString(PluginLink.getStringupomp_lthj_back()), new bc(this));
        this.C = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        if (r.a().d()) {
            this.C.setVisibility(0);
            i.a(this.C);
        }
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view());
        this.n = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_safe_prompt_view());
        this.l.a(this.i + "-" + i.a(this.c) + "(" + ap.a().t.substring(ap.a().t.length() - 4) + ")");
        this.m.a(i.f(this.j));
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
        this.K = (Button) findViewById(PluginLink.getIdupomp_lthj_cvn2_help());
        this.L = (Button) findViewById(PluginLink.getIdupomp_lthj_date_help());
        this.L.setOnClickListener(this);
        this.K.setOnClickListener(this);
        x xVar = new x(0);
        this.f.setOnFocusChangeListener(xVar);
        this.f.setOnTouchListener(xVar);
        x xVar2 = new x(2);
        this.d.setOnFocusChangeListener(xVar2);
        this.d.setOnTouchListener(xVar2);
        this.y = new bt();
        f fVar = new f(this.y);
        this.e.setOnFocusChangeListener(fVar);
        this.e.setOnTouchListener(fVar);
        this.k = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.o.setOnClickListener(new ba(this));
        if ("00".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    private void c() {
        this.h = b.SMS_RESULT_RECV_SUCCESS;
        setContentView(PluginLink.getLayoutupomp_lthj_savecardpay());
        this.C = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
        x xVar = new x(0);
        this.f.setOnFocusChangeListener(xVar);
        this.f.setOnTouchListener(xVar);
        if (r.a().e()) {
            this.C.setVisibility(0);
            i.a(this.C);
        }
    }

    private void d() {
        this.h = b.SMS_RESULT_SEND_CANCEL;
        cl.a(this, this);
        l();
    }

    private void e() {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_register());
        ((CustomInputView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(this.j);
        this.G = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_password_edit());
        x xVar = new x(8);
        this.G.b().setOnTouchListener(xVar);
        this.G.b().setOnFocusChangeListener(xVar);
        this.M = (CheckBox) findViewById(PluginLink.getIdupomp_lthj_user_pro_box());
        ((Button) findViewById(PluginLink.getIdupomp_lthj_reg_pro_btn())).setOnClickListener(new az(this));
        this.H = (CustomInputView) findViewById(PluginLink.getIdupomp_lthj_confirm_pwd_input());
        x xVar2 = new x(7);
        this.H.b().setOnTouchListener(xVar2);
        this.H.b().setOnFocusChangeListener(xVar2);
        this.J = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.J.setOnClickListener(this);
    }

    private void f() {
        View inflate = View.inflate(this, PluginLink.getLayoutupomp_lthj_quick_reg_confirm(), null);
        this.q = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_welcome_view());
        this.N = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_auto_welcome_btn());
        this.N.setOnClickListener(new ay(this));
        this.q.a(di.b[0]);
        this.r = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_ask_view());
        this.s = (CustomInputView) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_asw_view());
        this.r.a(PluginLink.getStringupomp_lthj_safe_ask_default());
        this.s.a(this.i + i.a(this.c) + i.h(ap.a().t.toString()));
        this.p = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.p.setOnClickListener(this);
        this.t = (Button) inflate.findViewById(PluginLink.getIdupomp_lthj_safe_ask_drop());
        this.t.setOnClickListener(this);
        this.I = new Dialog(this, PluginLink.getStyleupomp_lthj_common_dialog());
        Display defaultDisplay = ((WindowManager) getSystemService("window")).getDefaultDisplay();
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.height = (int) (((double) defaultDisplay.getHeight()) * 0.5d);
        attributes.width = defaultDisplay.getWidth();
        attributes.x = 0;
        attributes.y = defaultDisplay.getHeight() - attributes.height;
        this.I.getWindow().setAttributes(attributes);
        this.I.setContentView(inflate);
        this.I.show();
    }

    private void g() {
        setContentView(PluginLink.getLayoutupomp_lthj_quick_bindcard());
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view());
        this.l.a(this.i + "-" + i.a(this.c) + "(" + ap.a().t.substring(ap.a().t.length() - 4, ap.a().t.length()) + ")");
        this.m.a(this.j);
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.f = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.v = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.v.setOnClickListener(this);
        x xVar = new x(0);
        this.f.setOnFocusChangeListener(xVar);
        this.f.setOnTouchListener(xVar);
        x xVar2 = new x(2);
        this.d.setOnFocusChangeListener(xVar2);
        this.d.setOnTouchListener(xVar2);
        this.y = new bt();
        f fVar = new f(this.y);
        this.e.setOnFocusChangeListener(fVar);
        this.e.setOnTouchListener(fVar);
        if ("00".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.c)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    private void h() {
        if (!c.a(this, this.k) || !c.a(this, this.C)) {
            return;
        }
        if (ap.a().s) {
            ee eeVar = new ee(8227);
            eeVar.a(HeadData.createHeadData("PreAuth.Req", this));
            eeVar.a(this.h);
            if (this.h.equals(b.SMS_RESULT_SEND_FAIL)) {
                eeVar.f(ap.a().t.toString());
                if ("00".equals(this.c)) {
                    if (c.k(this, this.d) && c.j(this, this.e)) {
                        eeVar.k(this.d.getText().toString());
                        StringBuffer stringBuffer = new StringBuffer();
                        stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.b(), this.y.b().length)));
                        stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.a(), this.y.a().length)));
                        eeVar.h(stringBuffer.toString());
                        stringBuffer.delete(0, stringBuffer.length());
                    } else {
                        return;
                    }
                } else if ("01".equals(this.c)) {
                    if (c.m(this, this.f)) {
                        eeVar.g(ap.a().w.toString());
                    } else {
                        return;
                    }
                }
                eeVar.c(this.j);
            } else if (this.h.equals(b.SMS_RESULT_SEND_CANCEL)) {
                eeVar.b(ap.a().x.toString());
                eeVar.a(ap.a().z);
                eeVar.f(ap.a().B.pan);
                eeVar.c(ap.a().B.mobileNumber);
            }
            eeVar.e(this.C.d());
            eeVar.d(this.k.getText().toString());
            eeVar.m(ap.a().e);
            eeVar.l(ap.a().f);
            eeVar.p(ap.a().j);
            eeVar.q(ap.a().k);
            eeVar.n(ap.a().h);
            eeVar.o(ap.a().i);
            eeVar.r(ap.a().m);
            eeVar.t(ap.a().n);
            eeVar.u(ap.a().l);
            eeVar.s(ap.a().I);
            try {
                ck.a().a(eeVar, this, this, true, false);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (c.a(this, this.k)) {
            du duVar = new du(8210);
            duVar.a(HeadData.createHeadData("CommonPay.Req", this));
            duVar.b(this.h);
            if (this.h.equals(b.SMS_RESULT_SEND_FAIL)) {
                SmsCodeVerfiyData smsCodeVerfiyData = new SmsCodeVerfiyData();
                smsCodeVerfiyData.smsCode = this.k.getText().toString();
                smsCodeVerfiyData.sessionID = aq.a;
                smsCodeVerfiyData.codeMd5 = ap.a().v.toString();
                if (c.a(this, smsCodeVerfiyData)) {
                    duVar.h(ap.a().t.toString());
                    if ("00".equals(this.c)) {
                        if (c.k(this, this.d) && c.j(this, this.e)) {
                            duVar.m(this.d.getText().toString());
                            StringBuffer stringBuffer2 = new StringBuffer();
                            stringBuffer2.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.b(), this.y.b().length)));
                            stringBuffer2.append(new String(JniMethod.getJniMethod().decryptConfig(this.y.a(), this.y.a().length)));
                            duVar.l(stringBuffer2.toString());
                            stringBuffer2.delete(0, stringBuffer2.length());
                        } else {
                            return;
                        }
                    } else if ("01".equals(this.c)) {
                        if (c.m(this, this.f)) {
                            duVar.k(ap.a().w.toString());
                        } else {
                            return;
                        }
                    }
                    duVar.e(this.j);
                } else {
                    return;
                }
            } else if (this.h.equals(b.SMS_RESULT_SEND_CANCEL)) {
                duVar.c(ap.a().x.toString());
                duVar.d(new String(ap.a().z));
                duVar.h(ap.a().B.pan);
                duVar.e(ap.a().B.mobileNumber);
            }
            if (this.C != null) {
                duVar.g(this.C.d());
            }
            duVar.a(ap.a().g);
            duVar.f(this.k.getText().toString());
            duVar.o(ap.a().e);
            duVar.n(ap.a().f);
            duVar.r(ap.a().j);
            duVar.s(ap.a().k);
            duVar.p(ap.a().h);
            duVar.q(ap.a().i);
            duVar.u(ap.a().m);
            duVar.t(ap.a().n);
            duVar.v(ap.a().l);
            duVar.w(ap.a().I);
            try {
                ck.a().a(duVar, this, this, true, false);
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        j jVar = new j(8222);
        jVar.a(HeadData.createHeadData("CheckUserExist.Req", this));
        jVar.a(this.j);
        try {
            ck.a().a(jVar, this, this, true, true);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        if (b.SMS_RESULT_SEND_CANCEL == this.h) {
            k();
            return;
        }
        a().changeSubActivity(new Intent(this, HomeActivity.class));
    }

    private void k() {
        if (ap.a().A == null || ap.a().A.size() == 0) {
            l();
            return;
        }
        a(getString(PluginLink.getStringupomp_lthj_back()), this.D);
        setContentView(PluginLink.getLayoutupomp_lthj_quickpay_hascard());
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_account_mange_btn());
        this.w.setOnClickListener(this);
        this.l = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view());
        this.m = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view());
        this.k = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.o = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(ap.a().x.toString());
        LineFrameView lineFrameView = (LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view());
        lineFrameView.a(ap.a().D);
        lineFrameView.a().setTextColor(-65536);
        ap.a().B = i.a(ap.a().A);
        if (ap.a().B == null) {
            ap.a().B = (GetBundleBankCardList) ap.a().A.get(0);
        }
        this.l.a(ap.a().B.panBank + "-" + i.b(ap.a().B.panType) + "(" + ap.a().B.pan.substring(ap.a().B.pan.length() - 4, ap.a().B.pan.length()) + ")");
        this.m.a(i.f(ap.a().B.mobileNumber));
        this.o.setOnClickListener(new dd(this));
        this.B = (RelativeLayout) findViewById(PluginLink.getIdupomp_lthj_quickpaycard_layout());
        this.B.setOnClickListener(this);
        this.g = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.g.setOnClickListener(this);
    }

    private void l() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.D);
        setContentView(PluginLink.getLayoutupomp_lthj_quickpay_nocard());
        this.x = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.x.setOnClickListener(this);
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(ap.a().x.toString());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_welcome_view())).a(ap.a().D);
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_account_mange_btn());
        this.w.setOnClickListener(this);
        this.A = (ImageButton) findViewById(PluginLink.getIdupomp_lthj_refrush_btn());
        this.A.setOnClickListener(this);
    }

    private void m() {
        int size = ap.a().A.size();
        String[] strArr = new String[size];
        for (int i2 = 0; i2 < size; i2++) {
            GetBundleBankCardList getBundleBankCardList = (GetBundleBankCardList) ap.a().A.get(i2);
            strArr[i2] = getBundleBankCardList.panBank + "-" + i.b(getBundleBankCardList.panType) + "-" + getBundleBankCardList.pan.substring(getBundleBankCardList.pan.length() - 4);
        }
        new AlertDialog.Builder(this).setTitle(PluginLink.getStringupomp_lthj_choose_pay_card()).setItems(strArr, new ek(this)).setPositiveButton(getString(PluginLink.getStringupomp_lthj_add_card()), new dm(this)).setNegativeButton(getString(PluginLink.getStringupomp_lthj_close()), new dn(this)).create().show();
    }

    /* access modifiers changed from: private */
    public void n() {
        k.a().a(this, getString(PluginLink.getStringupomp_lthj_logout_prompt()), new ea(this));
    }

    public void backToMerchant() {
        a().backToMerchant();
    }

    public void errorCallBack(String str) {
        if (this.o != null) {
            this.o.setEnabled(true);
            this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
        }
        i.a(this, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void onClick(View view) {
        int i2 = 0;
        if (view == this.g) {
            h();
        } else if (view == this.w) {
            Intent intent = new Intent(this, AccountActivity.class);
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.J) {
            if (c.d(this, this.G.b()) && c.a(this) && c.a(this, this.M)) {
                f();
            }
        } else if (view == this.p) {
            if (c.g(this, this.q.b()) && c.h(this, this.r.b()) && c.i(this, this.s.b())) {
                this.I.dismiss();
                cl.b(this, this, this.j, this.q.a().toString(), this.r.a().toString(), this.s.a().toString());
            }
        } else if (view == this.v) {
            if (c.a(this, this.k)) {
                if ("00".equals(this.c)) {
                    if (!c.k(this, this.d) || !c.j(this, this.e)) {
                        return;
                    }
                } else if ("01".equals(this.c) && !c.m(this, this.f)) {
                    return;
                }
                cl.a(this, this, this.j, this.y, this.d.getText().toString());
            }
        } else if (view == this.x) {
            Intent intent2 = new Intent(this, BankCardInfoActivity.class);
            intent2.setFlags(67108864);
            intent2.putExtra("isQuickPayBind", true);
            a().changeSubActivity(intent2);
        } else if (view == this.A) {
            cl.a(this, this);
        } else if (view == this.B) {
            m();
        } else if (view == this.t) {
            Vector h2 = ap.a().a.h();
            this.u = new String[(h2.size() + 1)];
            this.u[0] = getString(PluginLink.getStringupomp_lthj_custom_safeask());
            while (true) {
                int i3 = i2;
                if (i3 < h2.size()) {
                    this.u[i3 + 1] = (String) h2.get(i3);
                    i2 = i3 + 1;
                } else {
                    new AlertDialog.Builder(this).setItems(this.u, new bk(this)).setTitle(PluginLink.getStringupomp_lthj_choose_safeask()).show();
                    return;
                }
            }
        } else if (view == this.K || view == this.L) {
            i.c(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().getBooleanExtra("isAcount", false)) {
            d();
            return;
        }
        this.c = getIntent().getStringExtra("panType");
        this.i = getIntent().getStringExtra("panBank");
        this.j = getIntent().getStringExtra("mobilenumber");
        if ("02".equals(this.c)) {
            c();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (!(this.h != b.SMS_RESULT_SEND_CANCEL || this.l == null || this.m == null || ap.a().B == null)) {
            this.l.a(ap.a().B.panBank + "-" + i.b(ap.a().B.panType) + "-" + ap.a().B.pan.substring(ap.a().B.pan.length() - 4, ap.a().B.pan.length()));
            this.m.a(i.f(ap.a().B.mobileNumber));
        }
        super.onResume();
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new dk(this);
            timer.schedule(this.aaTimerTask, 0, 1000);
        }
    }

    public void responseCallBack(w wVar) {
        if (wVar != null && wVar.m() != null && !isFinishing()) {
            int e2 = wVar.e();
            int parseInt = Integer.parseInt(wVar.m());
            if ("5309".equals(wVar.m())) {
                k.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new dj(this));
                return;
            }
            switch (e2) {
                case 8200:
                    m mVar = (m) wVar;
                    if (parseInt == 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        String a2 = mVar.a();
                        if (this.n != null) {
                            String str = getString(PluginLink.getStringupomp_lthj_secureinfo()) + "-" + a2;
                            SpannableString spannableString = new SpannableString(str);
                            spannableString.setSpan(new ForegroundColorSpan(-65536), str.length() - 4, str.length(), 33);
                            this.n.a(spannableString);
                        }
                        ap.a().v.delete(0, ap.a().v.length());
                        ap.a().v.append(mVar.c());
                        processRefreshConn();
                        return;
                    }
                    Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + mVar.n() + ",错误码为：" + parseInt, 1);
                    makeText2.setGravity(17, 0, 0);
                    makeText2.show();
                    this.o.setEnabled(true);
                    this.o.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                    return;
                case 8207:
                    bw bwVar = (bw) wVar;
                    if (parseInt == 0) {
                        ap.a().A = i.a(bwVar);
                        if (ap.a().A == null || ap.a().A.size() == 0) {
                            l();
                            return;
                        } else {
                            k();
                            return;
                        }
                    } else {
                        i.a(this, bwVar.n(), parseInt);
                        return;
                    }
                case 8210:
                case 8227:
                    a((du) wVar);
                    return;
                case 8222:
                    j jVar = (j) wVar;
                    if (parseInt != 0) {
                        backToMerchant();
                        return;
                    } else if (b.SMS_RESULT_SEND_SUCCESS.equals(jVar.a())) {
                        e();
                        return;
                    } else {
                        backToMerchant();
                        return;
                    }
                case 8225:
                    a((cp) wVar);
                    return;
                case 8229:
                    a((bz) wVar);
                    return;
                default:
                    return;
            }
        }
    }
}
