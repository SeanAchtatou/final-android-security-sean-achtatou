package com.unionpay.upomp.lthj.plugin.model;

public class SetDefaultBandCard extends Data {
    public String bindId;
    public String isDefault;
    public String loginName;
    public String mobileNumber;
    public String pan;
    public String panType;
}
