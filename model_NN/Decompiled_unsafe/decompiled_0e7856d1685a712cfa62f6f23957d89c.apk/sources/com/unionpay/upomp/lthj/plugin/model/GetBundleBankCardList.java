package com.unionpay.upomp.lthj.plugin.model;

public class GetBundleBankCardList extends Data {
    public String bindId;
    public String isDefault;
    public String loginName;
    public String mobileNumber;
    public String pan;
    public String panBank;
    public String panBankId;
    public String panType;
}
