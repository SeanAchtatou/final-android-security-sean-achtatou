package com.unionpay.upomp.lthj.plugin.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Toast;
import com.a.a.h.b;
import com.lthj.unipay.plugin.Cdo;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.au;
import com.lthj.unipay.plugin.bt;
import com.lthj.unipay.plugin.bw;
import com.lthj.unipay.plugin.c;
import com.lthj.unipay.plugin.cb;
import com.lthj.unipay.plugin.cl;
import com.lthj.unipay.plugin.cv;
import com.lthj.unipay.plugin.cx;
import com.lthj.unipay.plugin.cy;
import com.lthj.unipay.plugin.db;
import com.lthj.unipay.plugin.dp;
import com.lthj.unipay.plugin.dq;
import com.lthj.unipay.plugin.dr;
import com.lthj.unipay.plugin.ds;
import com.lthj.unipay.plugin.dt;
import com.lthj.unipay.plugin.dv;
import com.lthj.unipay.plugin.dw;
import com.lthj.unipay.plugin.dx;
import com.lthj.unipay.plugin.eb;
import com.lthj.unipay.plugin.ed;
import com.lthj.unipay.plugin.f;
import com.lthj.unipay.plugin.i;
import com.lthj.unipay.plugin.k;
import com.lthj.unipay.plugin.m;
import com.lthj.unipay.plugin.r;
import com.lthj.unipay.plugin.w;
import com.lthj.unipay.plugin.x;
import com.lthj.unipay.plugin.z;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.widget.LineFrameView;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.Timer;
import java.util.TimerTask;

public class BankCardInfoActivity extends BaseActivity implements View.OnClickListener, ExpandableListView.OnChildClickListener, UIResponseListener {
    public static final String TAG = "BankCardInfoActivity";
    private boolean A;
    private View B;
    ExpandableListView a;
    public TimerTask aaTimerTask;
    public int b = 60;
    private Button c;
    private EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private String f;
    private String g;
    private Button h;
    private Button i;
    private Button j;
    private View.OnClickListener k = new cx(this);
    /* access modifiers changed from: private */
    public Handler l = new cv(this);
    private View.OnClickListener m = new Cdo(this);
    private View.OnClickListener n = new dp(this);
    private ValidateCodeView o;
    private EditText p;
    private EditText q;
    private EditText r;
    private EditText s;
    private bt t;
    private ImageButton u;
    private GetBundleBankCardList v;
    /* access modifiers changed from: private */
    public Button w;
    private Button x;
    private Button y;
    /* access modifiers changed from: private */
    public boolean z;

    private void b() {
        cl.a(this, this);
    }

    private void c() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.k);
        setContentView(PluginLink.getLayoutupomp_lthj_bankcard_list());
        this.a = (ExpandableListView) findViewById(PluginLink.getIdupomp_lthj_bankcard_listview());
        View findViewById = findViewById(PluginLink.getIdupomp_lthj_no_card_layout());
        this.u = (ImageButton) findViewById(PluginLink.getIdupomp_lthj_refrush_btn());
        if (ap.a().A == null || ap.a().A.size() == 0) {
            this.u.setOnClickListener(this);
            this.a.setVisibility(8);
            findViewById.setVisibility(0);
        } else {
            this.a.setVisibility(0);
            findViewById.setVisibility(8);
            this.a.setAdapter(new cb(this));
            this.a.setChoiceMode(1);
            this.a.setItemChecked(0, true);
            this.a.setCacheColorHint(0);
            this.a.setGroupIndicator(null);
            this.a.setOnChildClickListener(this);
        }
        this.h = (Button) findViewById(PluginLink.getIdupomp_lthj_add_bankcard_btn());
        this.h.setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    public void d() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.m);
        setContentView(PluginLink.getLayoutupomp_lthj_bindcard_home());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_username_view())).a(ap.a().x.toString());
        this.o = (ValidateCodeView) findViewById(PluginLink.getIdupomp_lthj_validatecode_layout());
        this.d = (EditText) findViewById(PluginLink.getIdupomp_lthj_card_num_edit());
        this.d.addTextChangedListener(new au(this.d));
        this.e = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobile_num_edit());
        this.c = (Button) findViewById(PluginLink.getIdupomp_lthj_next_btn());
        this.c.setOnClickListener(this);
        this.j = (Button) findViewById(PluginLink.getIdupomp_lthj_help_btn());
        this.j.setOnClickListener(this);
        if (r.a().c()) {
            this.o.setVisibility(0);
            i.a(this.o);
        }
        this.B = findViewById(PluginLink.getIdupomp_lthj_myinfo_view());
        this.B.setOnClickListener(this);
    }

    private void e() {
        a(getString(PluginLink.getStringupomp_lthj_back()), this.n);
        setContentView(PluginLink.getLayoutupomp_lthj_bindcard_next());
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_bankcard_info_view())).a(this.f + "-" + i.a(this.g) + "(" + z.a().a.substring(z.a().a.length() - 4, z.a().a.length()) + ")");
        ((LineFrameView) findViewById(PluginLink.getIdupomp_lthj_mobile_number_view())).a(i.f(this.e.getText().toString()));
        this.q = (EditText) findViewById(PluginLink.getIdupomp_lthj_cvn2_edit());
        this.r = (EditText) findViewById(PluginLink.getIdupomp_lthj_date_edit());
        this.s = (EditText) findViewById(PluginLink.getIdupomp_lthj_pin_edit());
        this.i = (Button) findViewById(PluginLink.getIdupomp_lthj_button_ok());
        this.i.setOnClickListener(this);
        x xVar = new x(0);
        this.s.setOnFocusChangeListener(xVar);
        this.s.setOnTouchListener(xVar);
        x xVar2 = new x(2);
        this.q.setOnFocusChangeListener(xVar2);
        this.q.setOnTouchListener(xVar2);
        this.x = (Button) findViewById(PluginLink.getIdupomp_lthj_cvn2_help());
        this.y = (Button) findViewById(PluginLink.getIdupomp_lthj_date_help());
        this.y.setOnClickListener(this);
        this.x.setOnClickListener(this);
        this.t = new bt();
        f fVar = new f(this.t);
        this.r.setOnFocusChangeListener(fVar);
        this.r.setOnTouchListener(fVar);
        this.p = (EditText) findViewById(PluginLink.getIdupomp_lthj_mobilemac_edit());
        this.w = (Button) findViewById(PluginLink.getIdupomp_lthj_get_mac_btn());
        this.w.setOnClickListener(new dq(this));
        if ("00".equals(this.g)) {
            findViewById(PluginLink.getIdupomp_lthj_pin_layout()).setVisibility(8);
        } else if ("01".equals(this.g)) {
            findViewById(PluginLink.getIdupomp_lthj_cvn2_layout()).setVisibility(8);
            findViewById(PluginLink.getIdupomp_lthj_date_layout()).setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        cl.a(this, this, this.v.bindId);
    }

    /* access modifiers changed from: private */
    public void g() {
        cl.b(this, this, this.v.bindId, this.v.isDefault);
    }

    public void errorCallBack(String str) {
        if (this.w != null) {
            this.w.setEnabled(true);
            this.w.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
        }
        k.a().a(this, "确定", str);
    }

    public boolean onChildClick(ExpandableListView expandableListView, View view, int i2, int i3, long j2) {
        if (expandableListView != this.a) {
            return false;
        }
        this.v = (GetBundleBankCardList) ap.a().A.get(i2);
        if (expandableListView.getExpandableListAdapter().getChildrenCount(i2) == 2 && i3 == 1) {
            i3++;
        }
        switch (i3) {
            case 0:
                ap.a().B = this.v;
                a().changeSubActivity(new Intent(this, PayActivity.class));
                return false;
            case 1:
                k.a().a(this, getString(PluginLink.getStringupomp_lthj_set_default_prompt()), new dr(this));
                return false;
            case 2:
                k.a().a(this, getString(PluginLink.getStringupomp_lthj_unbind_prompt()), new ds(this));
                return false;
            default:
                return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void
     arg types: [com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity, com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity, java.lang.String, java.lang.String, int]
     candidates:
      com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, com.lthj.unipay.plugin.bt, java.lang.String):void
      com.lthj.unipay.plugin.cl.a(com.unionpay.upomp.lthj.plugin.ui.UIResponseListener, android.content.Context, java.lang.String, java.lang.String, boolean):void */
    public void onClick(View view) {
        if (view == this.c) {
            if (c.l(this, this.d) && c.b(this, this.e) && c.a(this, this.o)) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.delete(0, stringBuffer.length());
                stringBuffer.append(this.d.getText().toString());
                for (int i2 = 0; i2 < stringBuffer.length(); i2++) {
                    if (stringBuffer.charAt(i2) == ' ') {
                        stringBuffer.deleteCharAt(i2);
                    }
                }
                z.a().a.setLength(0);
                z.a().a.append(stringBuffer);
                stringBuffer.delete(0, stringBuffer.length());
                cl.a((UIResponseListener) this, (Context) this, z.a().a.toString(), this.o.d(), false);
            }
        } else if (view == this.h) {
            d();
        } else if (view == this.i) {
            if (c.a(this, this.p)) {
                if ("00".equals(this.g)) {
                    if (!c.k(this, this.q) || !c.j(this, this.r)) {
                        return;
                    }
                } else if (!c.m(this, this.s)) {
                    return;
                }
                cl.a(this, this, this.e.getText().toString(), this.p.getText().toString(), this.d.getText().toString().replace(" ", ""), this.t, this.q.getText().toString(), true);
            }
        } else if (view == this.u) {
            cl.a(this, this);
        } else if (view == this.x || view == this.y) {
            i.c(this);
        } else if (view == this.j) {
            Intent intent = new Intent(this, SupportCardActivity.class);
            intent.putExtra("tranType", b.SMS_RESULT_SEND_CANCEL);
            intent.addFlags(67108864);
            a().changeSubActivity(intent);
        } else if (view == this.B) {
            a().changeSubActivity(new Intent(this, AccountActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.z = getIntent().getBooleanExtra("isQuickPayBind", false);
        this.A = getIntent().getBooleanExtra("isBind", false);
        if (this.z || this.A) {
            d();
            return;
        }
        c();
        cl.a(this, this);
    }

    public void processRefreshConn() {
        if (this.aaTimerTask == null) {
            Timer timer = new Timer();
            this.aaTimerTask = new cy(this);
            timer.schedule(this.aaTimerTask, 0, 1000);
        }
    }

    public void responseCallBack(w wVar) {
        if (wVar != null && wVar.m() != null && !isFinishing()) {
            int e2 = wVar.e();
            int parseInt = Integer.parseInt(wVar.m());
            if ("5309".equals(wVar.m())) {
                k.a().a(this, getString(PluginLink.getStringupomp_lthj_multiple_user_login_tip()), new ed(this));
                return;
            }
            switch (e2) {
                case 8200:
                    m mVar = (m) wVar;
                    if (parseInt != 0) {
                        Toast makeText = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageLose()) + mVar.n() + ",错误码为：" + parseInt, 1);
                        makeText.setGravity(17, 0, 0);
                        makeText.show();
                        this.w.setEnabled(true);
                        this.w.setText(getString(PluginLink.getStringupomp_lthj_click_get_mac()));
                        break;
                    } else {
                        Toast makeText2 = Toast.makeText(this, getString(PluginLink.getStringupomp_lthj_sendMessageSuccess()), 1);
                        makeText2.setGravity(17, 0, 0);
                        makeText2.show();
                        ap.a().v.delete(0, ap.a().v.length());
                        ap.a().v.append(mVar.c());
                        processRefreshConn();
                        break;
                    }
                case 8203:
                    if (parseInt != 0) {
                        k.a().a(this, getString(PluginLink.getStringupomp_lthj_bind_fail()) + "(" + wVar.m() + ")", getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_rebind()), new dx(this), new dw(this));
                        break;
                    } else {
                        k.a().a(this, getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_bind_success()), new dt(this));
                        break;
                    }
                case 8204:
                    b();
                    break;
                case 8206:
                    b();
                    break;
                case 8207:
                    a(getString(PluginLink.getStringupomp_lthj_back()), this.k);
                    bw bwVar = (bw) wVar;
                    if (parseInt != 0) {
                        i.a(this, bwVar.n(), parseInt);
                        break;
                    } else {
                        ap.a().A = i.a(bwVar);
                        c();
                        break;
                    }
                case 8224:
                    db dbVar = (db) wVar;
                    if (parseInt != 0) {
                        r.a().b(true);
                        if (!"5110".equals(wVar.m())) {
                            i.a(this, wVar.n(), parseInt);
                            break;
                        } else {
                            k.a().a(this, i.b(this, wVar.n(), parseInt), getString(PluginLink.getStringupomp_lthj_ok()), getString(PluginLink.getStringupomp_lthj_check_support()), new eb(this), new dv(this));
                            break;
                        }
                    } else {
                        this.f = dbVar.c();
                        this.g = dbVar.a();
                        e();
                        r.a().b(false);
                        break;
                    }
            }
            if (wVar.e() != 8200) {
                i.b(this.o);
            }
        }
    }
}
