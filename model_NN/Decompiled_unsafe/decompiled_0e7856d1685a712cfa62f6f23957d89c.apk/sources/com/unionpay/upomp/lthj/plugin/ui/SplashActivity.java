package com.unionpay.upomp.lthj.plugin.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.widget.SeekBar;
import android.widget.TextView;
import com.lthj.unipay.plugin.aa;
import com.lthj.unipay.plugin.ab;
import com.lthj.unipay.plugin.ac;
import com.lthj.unipay.plugin.ad;
import com.lthj.unipay.plugin.af;
import com.lthj.unipay.plugin.ag;
import com.lthj.unipay.plugin.ah;
import com.lthj.unipay.plugin.ap;
import com.lthj.unipay.plugin.aq;
import com.lthj.unipay.plugin.av;
import com.lthj.unipay.plugin.by;
import com.lthj.unipay.plugin.ck;
import com.lthj.unipay.plugin.cu;
import com.lthj.unipay.plugin.dc;
import com.lthj.unipay.plugin.el;
import com.lthj.unipay.plugin.h;
import com.lthj.unipay.plugin.i;
import com.lthj.unipay.plugin.w;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.model.JNIInitBottomData;
import com.unionpay.upomp.lthj.util.PluginHelper;
import java.io.File;
import java.util.Calendar;
import java.util.Timer;
import mm.purchasesdk.PurchaseCode;

public class SplashActivity extends Activity implements UIResponseListener {
    public static Activity instance;
    public int a;
    public Handler b = new af(this);
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public ah d;
    /* access modifiers changed from: private */
    public Handler e = new ac(this);
    /* access modifiers changed from: private */
    public SeekBar f;

    /* access modifiers changed from: private */
    public void a() {
        try {
            if (ap.a().a != null) {
                el elVar = new el(8198);
                elVar.a(HeadData.createHeadData("PluginInit.Req", this));
                elVar.b(ap.a().a.a());
                elVar.a(ap.a().e);
                ck.a().a(elVar, this, this, false, true);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public boolean a(byte[] bArr) {
        if (bArr == null) {
            showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
            return false;
        }
        try {
            ap.a().a(bArr);
            if (TextUtils.isEmpty(ap.a().e)) {
                showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantId_Empty_prompt()));
                return false;
            } else if (ap.a().e.length() < 15 || ap.a().e.length() > 24) {
                showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantId_Length_prompt()));
                return false;
            } else if (TextUtils.isEmpty(ap.a().h)) {
                showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantOrderId_Empty_prompt()));
                return false;
            } else if (TextUtils.isEmpty(ap.a().i)) {
                showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_Empty_prompt()));
                return false;
            } else {
                if (TextUtils.isEmpty(i.c(ap.a().i))) {
                    showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantOrderTime_error_prompt()));
                    return false;
                }
                return true;
            }
        } catch (Exception e2) {
            showInitDialog(getString(PluginLink.getStringupomp_lthj_merchantXml_Format_prompt()));
        }
    }

    public void errorCallBack(String str) {
        if (!isFinishing()) {
            new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new ab(this)).show();
        }
    }

    public void initBottomData(byte[] bArr) {
        boolean z = true;
        if (bArr == null) {
            File file = new File(this.c);
            this.d = new ah(this);
            if (!file.exists()) {
                byte[] a2 = this.d.a(PluginLink.getRawupomp_lthj_config());
                bArr = JniMethod.getJniMethod().decryptConfig(a2, a2.length);
            } else {
                byte[] a3 = this.d.a(this.c);
                bArr = JniMethod.getJniMethod().decryptConfig(a3, a3.length);
                z = false;
            }
        }
        if (bArr == null) {
            try {
                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_ReadError_prompt()));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            dc dcVar = new dc();
            ap.a().a = dcVar;
            dcVar.a(bArr);
            byte[] d2 = dcVar.d();
            byte[] e3 = dcVar.e();
            byte[] bArr2 = new byte[PurchaseCode.AUTH_NO_APP];
            bArr2[0] = 0;
            bArr2[1] = 4;
            bArr2[2] = 0;
            bArr2[3] = 0;
            System.arraycopy(d2, 0, bArr2, 4, d2.length);
            System.arraycopy(e3, 0, bArr2, PurchaseCode.AUTH_INVALID_SIGN, e3.length);
            byte[] f2 = dcVar.f();
            byte[] g = dcVar.g();
            byte[] bArr3 = new byte[PurchaseCode.AUTH_NO_APP];
            bArr3[0] = 0;
            bArr3[1] = 4;
            bArr3[2] = 0;
            bArr3[3] = 0;
            System.arraycopy(f2, 0, bArr3, 4, f2.length);
            System.arraycopy(g, 0, bArr3, PurchaseCode.AUTH_INVALID_SIGN, g.length);
            JNIInitBottomData jNIInitBottomData = new JNIInitBottomData();
            jNIInitBottomData.certVersion = dcVar.b();
            jNIInitBottomData.phoneIMEI = i.a(this);
            jNIInitBottomData.imsi = i.b(this);
            JniMethod.getJniMethod().initResource(jNIInitBottomData, bArr2, bArr3);
            if (z) {
                this.d.a(this.c, JniMethod.getJniMethod().encryptConfig(bArr, bArr.length));
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.i.a(android.content.Context, boolean):boolean
     arg types: [com.unionpay.upomp.lthj.plugin.ui.SplashActivity, int]
     candidates:
      com.lthj.unipay.plugin.i.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.i.a(java.lang.String, char):java.lang.String[]
      com.lthj.unipay.plugin.i.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(PluginLink.getLayoutupomp_lthj_splash());
        instance = this;
        getWindow().setSoftInputMode(18);
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_textview_time())).setText("©2002-" + Calendar.getInstance().get(1));
        this.f = (SeekBar) findViewById(PluginLink.getIdupomp_lthj_splash_seekbar());
        this.f.setEnabled(false);
        this.a = 0;
        this.f.setProgress(this.a);
        new Timer().schedule(new ag(this), 0, 100);
        aq.a = "";
        ap.a().d = i.a((Context) this, false);
        new Handler().postDelayed(new ad(this), 3000);
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        return 4 == i;
    }

    public void responseCallBack(w wVar) {
        if (!isFinishing() && wVar != null) {
            int parseInt = Integer.parseInt(wVar.m());
            switch (wVar.e()) {
                case 8198:
                    el elVar = (el) wVar;
                    if (parseInt != 0) {
                        showInitDialog("初始化失败，" + elVar.n() + "，错误码为" + elVar.m());
                        return;
                    } else if (!elVar.d()) {
                        byte[] a2 = i.a(elVar.a().getBytes());
                        byte[] b2 = i.b((new String(a2) + i.a(this)).getBytes());
                        byte[] a3 = i.a(elVar.c().getBytes());
                        if (a3 == null || a3.length != 16) {
                            errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_HashFormat_prompt()));
                            return;
                        }
                        for (int i = 0; i < 16; i++) {
                            if (b2[i] != a3[i]) {
                                errorCallBack(getString(PluginLink.getStringupomp_lthj_merchantXml_MD5Error_prompt()));
                                return;
                            }
                        }
                        by byVar = new by(this);
                        byVar.a(a2);
                        byVar.start();
                        return;
                    } else {
                        String k = elVar.k();
                        String str = null;
                        h hVar = new h(this);
                        cu a4 = hVar.a(1);
                        if (a4 != null) {
                            byte[] bytes = a4.b().getBytes();
                            str = new String(JniMethod.getJniMethod().decryptConfig(bytes, bytes.length));
                        }
                        if (!k.equals(str)) {
                            byte[] bytes2 = k.getBytes();
                            String str2 = new String(JniMethod.getJniMethod().encryptConfig(bytes2, bytes2.length));
                            if (a4 == null) {
                                cu cuVar = new cu();
                                cuVar.a(str2);
                                hVar.a(cuVar);
                            } else {
                                a4.a(str2);
                                hVar.b(a4);
                            }
                        }
                        ap.a().b = true;
                        av avVar = new av(8223);
                        avVar.a(HeadData.createHeadData("CheckOrder.Req", this));
                        avVar.a(ap.a().e);
                        avVar.b(ap.a().h);
                        avVar.c(ap.a().i);
                        avVar.d(ap.a().o);
                        try {
                            ck.a().a(avVar, this, this, false, true);
                            return;
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            return;
                        }
                    }
                case 8223:
                    av avVar2 = (av) wVar;
                    if (parseInt == 0) {
                        ap.a().f = avVar2.a();
                        ap.a().g = avVar2.s();
                        ap.a().j = avVar2.c();
                        ap.a().k = avVar2.d();
                        ap.a().m = avVar2.o();
                        ap.a().n = avVar2.q();
                        ap.a().l = avVar2.r();
                        ap.a().I = avVar2.p();
                        synchronized (PluginHelper.a) {
                            PluginHelper.a.notify();
                        }
                        finish();
                        return;
                    }
                    showInitDialog(avVar2.n() + "，错误码为" + avVar2.m());
                    return;
                default:
                    return;
            }
        }
    }

    public void showInitDialog(String str) {
        new AlertDialog.Builder(this).setTitle("提示").setMessage(str).setPositiveButton("确定", new aa(this)).show();
    }
}
