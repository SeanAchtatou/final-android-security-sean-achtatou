package com.unionpay.upomp.lthj.plugin.model;

public class JNICreditBackData {
    public int EncryFlag;
    public int ResCode;
    public byte[] ResultData;
    public String cvn2;
    public String pan;
    public String panDate;
}
