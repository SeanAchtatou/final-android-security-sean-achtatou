package com.unionpay.upomp.lthj.plugin.ui;

import com.lthj.unipay.plugin.w;

public interface UIResponseListener {
    void errorCallBack(String str);

    void responseCallBack(w wVar);
}
