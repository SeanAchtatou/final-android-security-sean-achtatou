package com.unionpay.upomp.lthj.plugin.ui;

import android.os.Bundle;
import android.widget.TextView;
import com.lthj.unipay.plugin.a;
import com.unionpay.upomp.lthj.link.PluginLink;

public class AboutActivity extends BaseActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a(getString(PluginLink.getStringupomp_lthj_back()), new a(this));
        setContentView(PluginLink.getLayoutupomp_lthj_about());
        ((TextView) findViewById(PluginLink.getIdupomp_lthj_build_no())).setText("20121129s");
    }
}
