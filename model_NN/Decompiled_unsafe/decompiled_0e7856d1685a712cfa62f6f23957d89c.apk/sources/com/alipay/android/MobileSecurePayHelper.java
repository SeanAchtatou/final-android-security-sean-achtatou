package com.alipay.android;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.a.a.h.a;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import me.gall.totalpay.android.h;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileSecurePayHelper {
    static final String TAG = "MobileSecurePayHelper";
    /* access modifiers changed from: private */
    public a.C0000a av;
    Context mContext = null;
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
        public final void handleMessage(Message message) {
            try {
                switch (message.what) {
                    case 2:
                        MobileSecurePayHelper.this.closeProgress();
                        MobileSecurePayHelper.this.h(MobileSecurePayHelper.this.mContext, (String) message.obj);
                        break;
                }
                super.handleMessage(message);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private ProgressDialog mProgress = null;

    public MobileSecurePayHelper(Context context) {
        this.mContext = context;
    }

    private static boolean e(Context context, String str, String str2) {
        try {
            InputStream open = context.getAssets().open(str);
            File file = new File(str2);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[com.a.a.e.a.GAME_B_PRESSED];
            while (true) {
                int read = open.read(bArr);
                if (read <= 0) {
                    fileOutputStream.close();
                    open.close();
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static PackageInfo i(Context context, String str) {
        return context.getPackageManager().getPackageArchiveInfo(str, 128);
    }

    private JSONObject y(String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("action", "update");
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("platform", "android");
            jSONObject2.put("version", str);
            jSONObject2.put("partner", "");
            jSONObject.put("data", jSONObject2);
            return z(jSONObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private JSONObject z(String str) {
        JSONObject jSONObject;
        String h;
        NetworkManager networkManager = new NetworkManager(this.mContext);
        try {
            synchronized (networkManager) {
                h = networkManager.h(str, Constant.server_url);
            }
            jSONObject = new JSONObject(h);
        } catch (Exception e) {
            e.printStackTrace();
            jSONObject = null;
        }
        if (jSONObject != null) {
            jSONObject.toString();
            BaseHelper.u();
        }
        return jSONObject;
    }

    public final String a(PackageInfo packageInfo) {
        try {
            JSONObject y = y(packageInfo.versionName);
            if (y.getString("needUpdate").equalsIgnoreCase("true")) {
                return y.getString("updateUrl");
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.alipay.android.BaseHelper.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, boolean):android.app.ProgressDialog
     arg types: [android.content.Context, ?[OBJECT, ARRAY], java.lang.String, int]
     candidates:
      com.alipay.android.BaseHelper.a(android.app.Activity, java.lang.String, java.lang.String, int):void
      com.alipay.android.BaseHelper.a(android.content.Context, java.lang.CharSequence, java.lang.CharSequence, boolean):android.app.ProgressDialog */
    public final boolean a(a.C0000a aVar) {
        this.av = aVar;
        boolean v = v();
        if (!v) {
            final String str = String.valueOf(this.mContext.getCacheDir().getAbsolutePath()) + "/temp.apk";
            e(this.mContext, "20121018133442msp.apk", str);
            this.mProgress = BaseHelper.a(this.mContext, (CharSequence) null, (CharSequence) "正在检测安全支付服务版本", false);
            new Thread(new Runnable() {
                public final void run() {
                    String a = MobileSecurePayHelper.this.a(MobileSecurePayHelper.i(MobileSecurePayHelper.this.mContext, str));
                    if (a != null) {
                        MobileSecurePayHelper.this.f(MobileSecurePayHelper.this.mContext, a, str);
                    }
                    Message message = new Message();
                    message.what = 2;
                    message.obj = str;
                    MobileSecurePayHelper.this.mHandler.sendMessage(message);
                }
            }).start();
        } else if (aVar != null) {
            aVar.installed();
        }
        return v;
    }

    /* access modifiers changed from: package-private */
    public final void closeProgress() {
        try {
            if (this.mProgress != null) {
                this.mProgress.dismiss();
                this.mProgress = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final boolean f(Context context, String str, String str2) {
        try {
            return new NetworkManager(this.mContext).i(str, str2);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public final void h(final Context context, final String str) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setIcon(h.getDrawableIdentifier(context, "alipay_info"));
            builder.setTitle(context.getResources().getString(h.getStringIdentifier(context, "confirm_install_hint")));
            builder.setMessage(context.getResources().getString(h.getStringIdentifier(context, "confirm_install")));
            builder.setPositiveButton(h.getStringIdentifier(context, "Ensure"), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    BaseHelper.f("777", str);
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.addFlags(268435456);
                    intent.setDataAndType(Uri.parse("file://" + str), "application/vnd.android.package-archive");
                    context.startActivity(intent);
                    if (MobileSecurePayHelper.this.av != null) {
                        MobileSecurePayHelper.this.av.startInstall();
                    }
                }
            });
            builder.setNegativeButton(context.getResources().getString(h.getStringIdentifier(context, "Cancel")), new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    if (MobileSecurePayHelper.this.av != null) {
                        MobileSecurePayHelper.this.av.cancel();
                    }
                }
            });
            builder.show();
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
    }

    public final boolean v() {
        List<PackageInfo> installedPackages = this.mContext.getPackageManager().getInstalledPackages(0);
        for (int i = 0; i < installedPackages.size(); i++) {
            if (installedPackages.get(i).packageName.equalsIgnoreCase("com.alipay.android.app")) {
                return true;
            }
        }
        return false;
    }
}
