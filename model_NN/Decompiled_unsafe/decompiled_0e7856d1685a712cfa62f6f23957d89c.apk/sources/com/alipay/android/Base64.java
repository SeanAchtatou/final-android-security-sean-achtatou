package com.alipay.android;

import mm.purchasesdk.PurchaseCode;

public final class Base64 {
    private static final int BASELENGTH = 128;
    private static final int EIGHTBIT = 8;
    private static final int FOURBYTE = 4;
    private static final int LOOKUPLENGTH = 64;
    private static final char PAD = '=';
    private static final int SIGN = -128;
    private static final int SIXTEENBIT = 16;
    private static final int TWENTYFOURBITGROUP = 24;
    private static final byte[] at = new byte[BASELENGTH];
    private static final char[] au = new char[64];
    private static final boolean fDebug = false;

    static {
        int i = 0;
        for (int i2 = 0; i2 < BASELENGTH; i2++) {
            at[i2] = -1;
        }
        for (int i3 = 90; i3 >= 65; i3--) {
            at[i3] = (byte) (i3 - 65);
        }
        for (int i4 = 122; i4 >= 97; i4--) {
            at[i4] = (byte) ((i4 - 97) + 26);
        }
        for (int i5 = 57; i5 >= 48; i5--) {
            at[i5] = (byte) ((i5 - 48) + 52);
        }
        at[43] = 62;
        at[47] = 63;
        for (int i6 = 0; i6 <= 25; i6++) {
            au[i6] = (char) (i6 + 65);
        }
        int i7 = 26;
        int i8 = 0;
        while (i7 <= 51) {
            au[i7] = (char) (i8 + 97);
            i7++;
            i8++;
        }
        int i9 = 52;
        while (i9 <= 61) {
            au[i9] = (char) (i + 48);
            i9++;
            i++;
        }
        au[62] = '+';
        au[63] = '/';
    }

    private static boolean b(char c) {
        return c == '=';
    }

    private static boolean c(char c) {
        return c < BASELENGTH && at[c] != -1;
    }

    public static String p(byte[] bArr) {
        int i = 0;
        if (bArr == null) {
            return null;
        }
        int length = bArr.length * 8;
        if (length == 0) {
            return "";
        }
        int i2 = length % TWENTYFOURBITGROUP;
        int i3 = length / TWENTYFOURBITGROUP;
        char[] cArr = new char[((i2 != 0 ? i3 + 1 : i3) * 4)];
        int i4 = 0;
        int i5 = 0;
        while (i4 < i3) {
            int i6 = i + 1;
            byte b = bArr[i];
            int i7 = i6 + 1;
            byte b2 = bArr[i6];
            int i8 = i7 + 1;
            byte b3 = bArr[i7];
            byte b4 = (byte) (b2 & 15);
            byte b5 = (byte) (b & 3);
            byte b6 = (b & Byte.MIN_VALUE) == 0 ? (byte) (b >> 2) : (byte) ((b >> 2) ^ 192);
            byte b7 = (b2 & Byte.MIN_VALUE) == 0 ? (byte) (b2 >> 4) : (byte) ((b2 >> 4) ^ PurchaseCode.AUTH_NOORDER);
            int i9 = (b3 & Byte.MIN_VALUE) == 0 ? b3 >> 6 : (b3 >> 6) ^ PurchaseCode.AUTH_STATICMARK_VERIFY_FAILED;
            int i10 = i5 + 1;
            cArr[i5] = au[b6];
            int i11 = i10 + 1;
            cArr[i10] = au[b7 | (b5 << 4)];
            int i12 = i11 + 1;
            cArr[i11] = au[((byte) i9) | (b4 << 2)];
            cArr[i12] = au[b3 & 63];
            i4++;
            i5 = i12 + 1;
            i = i8;
        }
        if (i2 == 8) {
            byte b8 = bArr[i];
            byte b9 = (byte) (b8 & 3);
            int i13 = i5 + 1;
            cArr[i5] = au[(b8 & Byte.MIN_VALUE) == 0 ? (byte) (b8 >> 2) : (byte) ((b8 >> 2) ^ 192)];
            int i14 = i13 + 1;
            cArr[i13] = au[b9 << 4];
            cArr[i14] = PAD;
            cArr[i14 + 1] = PAD;
        } else if (i2 == 16) {
            byte b10 = bArr[i];
            byte b11 = bArr[i + 1];
            byte b12 = (byte) (b11 & 15);
            byte b13 = (byte) (b10 & 3);
            byte b14 = (b10 & Byte.MIN_VALUE) == 0 ? (byte) (b10 >> 2) : (byte) ((b10 >> 2) ^ 192);
            byte b15 = (b11 & Byte.MIN_VALUE) == 0 ? (byte) (b11 >> 4) : (byte) ((b11 >> 4) ^ PurchaseCode.AUTH_NOORDER);
            int i15 = i5 + 1;
            cArr[i5] = au[b14];
            int i16 = i15 + 1;
            cArr[i15] = au[b15 | (b13 << 4)];
            cArr[i16] = au[b12 << 2];
            cArr[i16 + 1] = PAD;
        }
        return new String(cArr);
    }

    public static byte[] x(String str) {
        int i;
        int i2;
        if (str == null) {
            return null;
        }
        char[] charArray = str.toCharArray();
        if (charArray == null) {
            i = 0;
        } else {
            int length = charArray.length;
            int i3 = 0;
            i = 0;
            while (i3 < length) {
                char c = charArray[i3];
                if (!(c == ' ' || c == 13 || c == 10 || c == 9)) {
                    i2 = i + 1;
                    charArray[i] = charArray[i3];
                } else {
                    i2 = i;
                }
                i3++;
                i = i2;
            }
        }
        if (i % 4 != 0) {
            return null;
        }
        int i4 = i / 4;
        if (i4 == 0) {
            return new byte[0];
        }
        byte[] bArr = new byte[(i4 * 3)];
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (i7 < i4 - 1) {
            int i8 = i5 + 1;
            char c2 = charArray[i5];
            if (c(c2)) {
                int i9 = i8 + 1;
                char c3 = charArray[i8];
                if (c(c3)) {
                    int i10 = i9 + 1;
                    char c4 = charArray[i9];
                    if (c(c4)) {
                        i5 = i10 + 1;
                        char c5 = charArray[i10];
                        if (c(c5)) {
                            byte b = at[c2];
                            byte b2 = at[c3];
                            byte b3 = at[c4];
                            byte b4 = at[c5];
                            int i11 = i6 + 1;
                            bArr[i6] = (byte) ((b << 2) | (b2 >> 4));
                            int i12 = i11 + 1;
                            bArr[i11] = (byte) (((b2 & 15) << 4) | ((b3 >> 2) & 15));
                            i6 = i12 + 1;
                            bArr[i12] = (byte) ((b3 << 6) | b4);
                            i7++;
                        }
                    }
                }
            }
            return null;
        }
        int i13 = i5 + 1;
        char c6 = charArray[i5];
        if (c(c6)) {
            int i14 = i13 + 1;
            char c7 = charArray[i13];
            if (c(c7)) {
                byte b5 = at[c6];
                byte b6 = at[c7];
                int i15 = i14 + 1;
                char c8 = charArray[i14];
                char c9 = charArray[i15];
                if (c(c8) && c(c9)) {
                    byte b7 = at[c8];
                    byte b8 = at[c9];
                    int i16 = i6 + 1;
                    bArr[i6] = (byte) ((b5 << 2) | (b6 >> 4));
                    bArr[i16] = (byte) (((b6 & 15) << 4) | ((b7 >> 2) & 15));
                    bArr[i16 + 1] = (byte) (b8 | (b7 << 6));
                    return bArr;
                } else if (!b(c8) || !b(c9)) {
                    if (b(c8) || !b(c9)) {
                        return null;
                    }
                    byte b9 = at[c8];
                    if ((b9 & 3) != 0) {
                        return null;
                    }
                    byte[] bArr2 = new byte[((i7 * 3) + 2)];
                    System.arraycopy(bArr, 0, bArr2, 0, i7 * 3);
                    bArr2[i6] = (byte) ((b5 << 2) | (b6 >> 4));
                    bArr2[i6 + 1] = (byte) (((b6 & 15) << 4) | ((b9 >> 2) & 15));
                    return bArr2;
                } else if ((b6 & 15) != 0) {
                    return null;
                } else {
                    byte[] bArr3 = new byte[((i7 * 3) + 1)];
                    System.arraycopy(bArr, 0, bArr3, 0, i7 * 3);
                    bArr3[i6] = (byte) ((b5 << 2) | (b6 >> 4));
                    return bArr3;
                }
            }
        }
        return null;
    }
}
