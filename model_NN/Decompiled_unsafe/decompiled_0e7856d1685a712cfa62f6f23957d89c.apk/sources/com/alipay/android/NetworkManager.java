package com.alipay.android;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.a.a.e.a;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;

public class NetworkManager {
    static final String TAG = "NetworkManager";
    private int aI = 30000;
    private int aJ = 30000;
    Proxy aK = null;
    Context mContext;

    public NetworkManager(Context context) {
        this.mContext = context;
        HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
            public final boolean verify(String str, SSLSession sSLSession) {
                return true;
            }
        });
    }

    private void x() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.mContext.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.getType() == 0) {
            String defaultHost = android.net.Proxy.getDefaultHost();
            int defaultPort = android.net.Proxy.getDefaultPort();
            if (defaultHost != null) {
                this.aK = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
    }

    public final String h(String str, String str2) {
        HttpURLConnection httpURLConnection;
        IOException e;
        String str3;
        x();
        ArrayList arrayList = new ArrayList();
        arrayList.add(new BasicNameValuePair("requestData", str));
        try {
            UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(arrayList, "utf-8");
            URL url = new URL(str2);
            httpURLConnection = this.aK != null ? (HttpURLConnection) url.openConnection(this.aK) : (HttpURLConnection) url.openConnection();
            try {
                httpURLConnection.setConnectTimeout(this.aI);
                httpURLConnection.setReadTimeout(this.aJ);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.addRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
                httpURLConnection.connect();
                OutputStream outputStream = httpURLConnection.getOutputStream();
                urlEncodedFormEntity.writeTo(outputStream);
                outputStream.flush();
                str3 = BaseHelper.b(httpURLConnection.getInputStream());
                try {
                    "response " + str3;
                    BaseHelper.u();
                    httpURLConnection.disconnect();
                } catch (IOException e2) {
                    e = e2;
                    try {
                        e.printStackTrace();
                        httpURLConnection.disconnect();
                        return str3;
                    } catch (Throwable th) {
                        th = th;
                        httpURLConnection.disconnect();
                        throw th;
                    }
                }
            } catch (IOException e3) {
                IOException iOException = e3;
                str3 = null;
                e = iOException;
            }
        } catch (IOException e4) {
            httpURLConnection = null;
            e = e4;
            str3 = null;
            e.printStackTrace();
            httpURLConnection.disconnect();
            return str3;
        } catch (Throwable th2) {
            th = th2;
            httpURLConnection = null;
            httpURLConnection.disconnect();
            throw th;
        }
        return str3;
    }

    public final boolean i(String str, String str2) {
        x();
        try {
            URL url = new URL(str);
            HttpURLConnection httpURLConnection = this.aK != null ? (HttpURLConnection) url.openConnection(this.aK) : (HttpURLConnection) url.openConnection();
            httpURLConnection.setConnectTimeout(this.aI);
            httpURLConnection.setReadTimeout(this.aJ);
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            File file = new File(str2);
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[a.GAME_B_PRESSED];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    fileOutputStream.close();
                    inputStream.close();
                    return true;
                }
                fileOutputStream.write(bArr, 0, read);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
