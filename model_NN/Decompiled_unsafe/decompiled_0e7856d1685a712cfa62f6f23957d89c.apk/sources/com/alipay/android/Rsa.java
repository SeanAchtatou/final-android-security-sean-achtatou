package com.alipay.android;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class Rsa {
    public static final String SIGN_ALGORITHMS = "SHA1WithRSA";

    public static boolean b(String str, String str2, String str3) {
        try {
            PublicKey generatePublic = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.x(str3)));
            Signature instance = Signature.getInstance(SIGN_ALGORITHMS);
            instance.initVerify(generatePublic);
            instance.update(str.getBytes("utf-8"));
            return instance.verify(Base64.x(str2));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String sign(String str, String str2) {
        try {
            PrivateKey generatePrivate = KeyFactory.getInstance("RSA", "BC").generatePrivate(new PKCS8EncodedKeySpec(Base64.x(str2)));
            Signature instance = Signature.getInstance(SIGN_ALGORITHMS);
            instance.initSign(generatePrivate);
            instance.update(str.getBytes("utf-8"));
            return Base64.p(instance.sign());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
