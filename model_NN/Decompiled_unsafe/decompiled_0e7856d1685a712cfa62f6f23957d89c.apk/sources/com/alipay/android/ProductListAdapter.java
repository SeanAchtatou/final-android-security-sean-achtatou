package com.alipay.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.alipay.android.Products;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import me.gall.totalpay.android.h;

public class ProductListAdapter extends BaseAdapter {
    private ArrayList<Products.ProductDetail> aM;
    private Context context;

    private class a {
        TextView aN;
        TextView aO;
        TextView aP;

        private a() {
        }

        /* synthetic */ a(ProductListAdapter productListAdapter, byte b) {
            this();
        }
    }

    public int getCount() {
        return this.aM.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        a aVar;
        NullPointerException nullPointerException;
        View view2;
        View view3;
        FileNotFoundException fileNotFoundException;
        if (view == null) {
            a aVar2 = new a(this, (byte) 0);
            try {
                View inflate = LayoutInflater.from(this.context).inflate(h.getLayoutIdentifier(this.context, "product_item"), (ViewGroup) null);
                try {
                    aVar2.aN = (TextView) inflate.findViewById(h.getViewIdentifier(this.context, "subject"));
                    aVar2.aO = (TextView) inflate.findViewById(h.getViewIdentifier(this.context, "body"));
                    aVar2.aP = (TextView) inflate.findViewById(h.getViewIdentifier(this.context, "price"));
                    view2 = inflate;
                } catch (FileNotFoundException e) {
                    FileNotFoundException fileNotFoundException2 = e;
                    view2 = inflate;
                    fileNotFoundException = fileNotFoundException2;
                    fileNotFoundException.printStackTrace();
                    view2.setTag(aVar2);
                    aVar = aVar2;
                    view = view2;
                    aVar.aN.setText(this.aM.get(i).aS);
                    aVar.aO.setText(this.aM.get(i).aT);
                    aVar.aP.setText(this.aM.get(i).aU);
                    return view;
                } catch (NullPointerException e2) {
                    NullPointerException nullPointerException2 = e2;
                    view3 = inflate;
                    nullPointerException = nullPointerException2;
                    nullPointerException.printStackTrace();
                    view2.setTag(aVar2);
                    aVar = aVar2;
                    view = view2;
                    aVar.aN.setText(this.aM.get(i).aS);
                    aVar.aO.setText(this.aM.get(i).aT);
                    aVar.aP.setText(this.aM.get(i).aU);
                    return view;
                }
            } catch (FileNotFoundException e3) {
                fileNotFoundException = e3;
                view2 = view;
            } catch (NullPointerException e4) {
                nullPointerException = e4;
                view3 = view;
                nullPointerException.printStackTrace();
                view2.setTag(aVar2);
                aVar = aVar2;
                view = view2;
                aVar.aN.setText(this.aM.get(i).aS);
                aVar.aO.setText(this.aM.get(i).aT);
                aVar.aP.setText(this.aM.get(i).aU);
                return view;
            }
            view2.setTag(aVar2);
            aVar = aVar2;
            view = view2;
        } else {
            aVar = (a) view.getTag();
        }
        aVar.aN.setText(this.aM.get(i).aS);
        aVar.aO.setText(this.aM.get(i).aT);
        aVar.aP.setText(this.aM.get(i).aU);
        return view;
    }
}
