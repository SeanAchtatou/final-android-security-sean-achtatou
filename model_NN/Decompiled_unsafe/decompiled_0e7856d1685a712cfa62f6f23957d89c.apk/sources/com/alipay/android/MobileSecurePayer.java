package com.alipay.android;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import com.alipay.android.app.IAlixPay;
import com.alipay.android.app.IRemoteServiceCallback;

public class MobileSecurePayer {
    static String TAG = "MobileSecurePayer";
    boolean aA = false;
    Activity aB = null;
    /* access modifiers changed from: private */
    public ServiceConnection aC = new ServiceConnection() {
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            synchronized (MobileSecurePayer.this.ay) {
                MobileSecurePayer.this.az = IAlixPay.Stub.a(iBinder);
                MobileSecurePayer.this.ay.notify();
            }
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            MobileSecurePayer.this.az = null;
        }
    };
    /* access modifiers changed from: private */
    public IRemoteServiceCallback aD = new IRemoteServiceCallback.Stub() {
        public final void a(String str, String str2, int i, Bundle bundle) {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            if (bundle == null) {
                bundle = new Bundle();
            }
            try {
                bundle.putInt("CallingPid", i);
                intent.putExtras(bundle);
            } catch (Exception e) {
                e.printStackTrace();
            }
            intent.setClassName(str, str2);
            MobileSecurePayer.this.aB.startActivity(intent);
        }

        public final void a(boolean z, String str) {
        }

        public final boolean w() {
            return false;
        }
    };
    Integer ay = 0;
    IAlixPay az = null;

    public final boolean a(final String str, final Handler handler, int i, Activity activity) {
        if (this.aA) {
            return false;
        }
        this.aA = true;
        this.aB = activity;
        if (this.az == null) {
            this.aB.getApplicationContext().bindService(new Intent(IAlixPay.class.getName()), this.aC, 1);
        }
        new Thread(new Runnable(1) {
            /* JADX WARNING: Code restructure failed: missing block: B:13:0x0074, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x0075, code lost:
                r0.printStackTrace();
                r1 = new android.os.Message();
                r1.what = 1;
                r1.obj = r0.toString();
                r6.sendMessage(r1);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
                return;
             */
            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r3 = this;
                    com.alipay.android.MobileSecurePayer r0 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    java.lang.Integer r1 = r0.ay     // Catch:{ Exception -> 0x0074 }
                    monitor-enter(r1)     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r0 = com.alipay.android.MobileSecurePayer.this     // Catch:{ all -> 0x0071 }
                    com.alipay.android.app.IAlixPay r0 = r0.az     // Catch:{ all -> 0x0071 }
                    if (r0 != 0) goto L_0x0012
                    com.alipay.android.MobileSecurePayer r0 = com.alipay.android.MobileSecurePayer.this     // Catch:{ all -> 0x0071 }
                    java.lang.Integer r0 = r0.ay     // Catch:{ all -> 0x0071 }
                    r0.wait()     // Catch:{ all -> 0x0071 }
                L_0x0012:
                    monitor-exit(r1)     // Catch:{ all -> 0x0071 }
                    com.alipay.android.MobileSecurePayer r0 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.app.IAlixPay r0 = r0.az     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r1 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.app.IRemoteServiceCallback r1 = r1.aD     // Catch:{ Exception -> 0x0074 }
                    r0.a(r1)     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r0 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.app.IAlixPay r0 = r0.az     // Catch:{ Exception -> 0x0074 }
                    java.lang.String r1 = r5     // Catch:{ Exception -> 0x0074 }
                    java.lang.String r0 = r0.B(r1)     // Catch:{ Exception -> 0x0074 }
                    java.lang.String r1 = com.alipay.android.MobileSecurePayer.TAG     // Catch:{ Exception -> 0x0074 }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0074 }
                    java.lang.String r2 = "After Pay: "
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0074 }
                    java.lang.StringBuilder r1 = r1.append(r0)     // Catch:{ Exception -> 0x0074 }
                    r1.toString()     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.BaseHelper.u()     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r1 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    r2 = 0
                    r1.aA = r2     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r1 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.app.IAlixPay r1 = r1.az     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r2 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.app.IRemoteServiceCallback r2 = r2.aD     // Catch:{ Exception -> 0x0074 }
                    r1.b(r2)     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r1 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    android.app.Activity r1 = r1.aB     // Catch:{ Exception -> 0x0074 }
                    android.content.Context r1 = r1.getApplicationContext()     // Catch:{ Exception -> 0x0074 }
                    com.alipay.android.MobileSecurePayer r2 = com.alipay.android.MobileSecurePayer.this     // Catch:{ Exception -> 0x0074 }
                    android.content.ServiceConnection r2 = r2.aC     // Catch:{ Exception -> 0x0074 }
                    r1.unbindService(r2)     // Catch:{ Exception -> 0x0074 }
                    android.os.Message r1 = new android.os.Message     // Catch:{ Exception -> 0x0074 }
                    r1.<init>()     // Catch:{ Exception -> 0x0074 }
                    int r2 = 1     // Catch:{ Exception -> 0x0074 }
                    r1.what = r2     // Catch:{ Exception -> 0x0074 }
                    r1.obj = r0     // Catch:{ Exception -> 0x0074 }
                    android.os.Handler r0 = r6     // Catch:{ Exception -> 0x0074 }
                    r0.sendMessage(r1)     // Catch:{ Exception -> 0x0074 }
                L_0x0070:
                    return
                L_0x0071:
                    r0 = move-exception
                    monitor-exit(r1)     // Catch:{ Exception -> 0x0074 }
                    throw r0     // Catch:{ Exception -> 0x0074 }
                L_0x0074:
                    r0 = move-exception
                    r0.printStackTrace()
                    android.os.Message r1 = new android.os.Message
                    r1.<init>()
                    int r2 = 1
                    r1.what = r2
                    java.lang.String r0 = r0.toString()
                    r1.obj = r0
                    android.os.Handler r0 = r6
                    r0.sendMessage(r1)
                    goto L_0x0070
                */
                throw new UnsupportedOperationException("Method not decompiled: com.alipay.android.MobileSecurePayer.AnonymousClass3.run():void");
            }
        }).start();
        return true;
    }
}
