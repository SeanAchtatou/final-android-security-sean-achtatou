package com.alipay.android;

import org.json.JSONObject;

public class ResultChecker {
    public static final int RESULT_CHECK_SIGN_FAILED = 1;
    public static final int RESULT_CHECK_SIGN_SUCCEED = 2;
    public static final int RESULT_INVALID_PARAM = 0;
    String aV;

    public ResultChecker(String str) {
        this.aV = str;
    }

    public final int A(String str) {
        try {
            String string = BaseHelper.g(this.aV, ";").getString("result");
            String substring = string.substring(1, string.length() - 1);
            String substring2 = substring.substring(0, substring.indexOf("&sign_type="));
            JSONObject g = BaseHelper.g(substring, "&");
            return (!g.getString("sign_type").replace("\"", "").equalsIgnoreCase("RSA") || Rsa.b(substring2, g.getString("sign").replace("\"", ""), str)) ? 2 : 1;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
}
