package com.chinaMobile;

import android.content.Context;
import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.apache.http.ParseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class c implements Runnable {
    private /* synthetic */ Context a;

    c(Context context) {
        this.a = context;
    }

    public final void run() {
        long j;
        String[] B = MobileAgent.A();
        if (B != null) {
            JSONArray jSONArray = new JSONArray();
            String format = new SimpleDateFormat("yyyy").format(new Date(System.currentTimeMillis()));
            String packageName = this.a.getPackageName();
            String str = "";
            String str2 = "";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS", Locale.US);
            boolean z = false;
            for (int i = 0; i < B.length; i++) {
                if (B[i].indexOf("EXCEPTION") > 0) {
                    if (!str2.equals("") && z) {
                        JSONObject jSONObject = new JSONObject();
                        try {
                            jSONObject.put("occurtime", simpleDateFormat.parse(str).getTime());
                            jSONObject.put("errmsg", URLEncoder.encode(str2, e.f));
                            jSONArray.put(jSONObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (ParseException e2) {
                            e2.printStackTrace();
                        } catch (java.text.ParseException e3) {
                            e3.printStackTrace();
                        } catch (UnsupportedEncodingException e4) {
                            e4.printStackTrace();
                        }
                    }
                    int indexOf = B[i].indexOf("E/AndroidRuntime(");
                    str = indexOf >= 0 ? format + "-" + B[i].substring(0, indexOf) : "";
                    str2 = "";
                    z = false;
                } else if (B[i].indexOf("at") >= 0 || B[i].indexOf("java.lang") >= 0 || B[i].indexOf("cause by") >= 0) {
                    str2 = str2 + B[i].substring(B[i].indexOf("):") + 2) + "\n";
                    if (B[i].indexOf(packageName) >= 0) {
                        z = true;
                    }
                }
            }
            if (!str2.equals("") && z) {
                JSONObject jSONObject2 = new JSONObject();
                try {
                    j = simpleDateFormat.parse(str).getTime();
                } catch (java.text.ParseException e5) {
                    long currentTimeMillis = System.currentTimeMillis();
                    e5.printStackTrace();
                    j = currentTimeMillis;
                }
                try {
                    jSONObject2.put("occurtime", j);
                    try {
                        jSONObject2.put("errmsg", URLEncoder.encode(str2, e.f));
                    } catch (UnsupportedEncodingException e6) {
                        e6.printStackTrace();
                    }
                    jSONArray.put(jSONObject2);
                } catch (JSONException e7) {
                    e7.printStackTrace();
                }
            }
            if (jSONArray.length() > 0) {
                String string = MobileAgent.z(this.a).getString("sessionId", null);
                JSONObject jSONObject3 = new JSONObject();
                try {
                    jSONObject3.put("sid", string);
                    jSONObject3.put("errjsonary", jSONArray);
                    boolean unused = MobileAgent.c(this.a, jSONObject3.toString(), 4);
                } catch (JSONException e8) {
                    e8.printStackTrace();
                }
            }
        }
    }
}
