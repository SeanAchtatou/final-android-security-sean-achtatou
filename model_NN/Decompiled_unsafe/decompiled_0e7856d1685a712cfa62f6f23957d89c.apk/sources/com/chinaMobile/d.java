package com.chinaMobile;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.a.a.h.b;
import java.security.MessageDigest;
import javax.microedition.media.control.ToneControl;

public final class d {
    public static String G(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        try {
            String subscriberId = telephonyManager.getSubscriberId();
            return subscriberId != null ? subscriberId.trim() : "";
        } catch (Exception e) {
            return "";
        }
    }

    public static String a(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        } catch (Exception e) {
            return "";
        }
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            char[] charArray = str.toCharArray();
            byte[] bArr = new byte[charArray.length];
            for (int i = 0; i < charArray.length; i++) {
                bArr[i] = (byte) charArray[i];
            }
            byte[] digest = instance.digest(bArr);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                byte b2 = b & ToneControl.SILENCE;
                if (b2 < 16) {
                    stringBuffer.append(b.SMS_RESULT_SEND_SUCCESS);
                }
                stringBuffer.append(Integer.toHexString(b2));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static String b(Context context) {
        String a = a(context);
        if (a != null && a.length() > 0) {
            return String.valueOf(a(a).toCharArray(), 7, 18);
        }
        String d = d(context);
        if (d != null && d.length() != 0) {
            return String.valueOf(a(d).toCharArray(), 7, 18);
        }
        String G = G(context);
        return (G == null || G.length() == 0) ? String.valueOf(a(Build.MODEL).toCharArray(), 7, 18) : String.valueOf(a(G).toCharArray(), 7, 18);
    }

    public static String d(Context context) {
        try {
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                return (macAddress == null || macAddress.equals("")) ? "unknown" : macAddress;
            }
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?");
            return "unknown";
        } catch (Exception e) {
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e);
            return "unknown";
        }
    }

    public static String e(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            return applicationInfo != null ? applicationInfo.metaData.getString("MOBILE_APPKEY") : "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String f(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            String string = applicationInfo.metaData.getString("MOBILE_CHANNEL");
            if (string != null) {
                return string;
            }
            Log.w("MobileUtils", "Could not read MOBILE_CHANNEL meta-data from AndroidManifest.xml.");
            return "";
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String g(Context context) {
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return "unknown";
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "unknown";
            }
            if (activeNetworkInfo.getType() == 1) {
                return "wifi";
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return "unknown";
            }
            "net type:" + extraInfo;
            return extraInfo.trim();
        } catch (Exception e) {
            Log.w("MobileUtils", "Could not read ACCESSPOINT, forget to include ACCESS_NETSTATE_STATE permission?", e);
            return "unknown";
        }
    }

    public static int h(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String i(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }
}
