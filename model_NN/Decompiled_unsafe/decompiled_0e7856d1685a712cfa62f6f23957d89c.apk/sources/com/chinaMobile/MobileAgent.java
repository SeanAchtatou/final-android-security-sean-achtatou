package com.chinaMobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.a.a.h.b;
import com.umeng.common.a;
import com.umeng.common.b.e;
import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileAgent {
    public static final String SDK_VERSION = "1.0";
    static int a = -1;
    private static JSONObject aX;
    private static MMOnlineConfigureListener aY;
    private static Object aZ = new Object();
    static int b = 0;
    private static boolean d = false;
    private static boolean e = true;
    private static String g = "";
    private static long h = 0;

    private static SharedPreferences A(Context context) {
        return context.getSharedPreferences("MoblieAgent_event_" + context.getPackageName(), 0);
    }

    /* access modifiers changed from: private */
    public static String[] A() {
        Log.e("AndroidRuntime", "abcdef");
        ArrayList arrayList = new ArrayList();
        arrayList.add("logcat");
        arrayList.add("-v");
        arrayList.add("time");
        arrayList.add("-s");
        arrayList.add("AndroidRuntime:E");
        arrayList.add("logcat");
        arrayList.add("-v");
        arrayList.add("raw");
        arrayList.add("-s");
        arrayList.add("AndroidRuntime:E");
        try {
            Process exec = Runtime.getRuntime().exec((String[]) arrayList.toArray(new String[arrayList.size()]));
            InputStream inputStream = exec.getInputStream();
            DataInputStream dataInputStream = new DataInputStream(inputStream);
            byte[] bArr = new byte[10000];
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            dataInputStream.read(bArr);
            exec.destroy();
            inputStream.close();
            String str = new String(bArr);
            Runtime.getRuntime().exec("logcat -c");
            String[] split = str.split("\n");
            if (split.length > 4) {
                return split;
            }
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
        }
    }

    private static SharedPreferences B(Context context) {
        return context.getSharedPreferences("MoblieAgent_upload_" + context.getPackageName(), 0);
    }

    private static JSONObject C(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", SDK_VERSION);
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", d.b(context));
            jSONObject.put("appKey", d.e(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", d.h(context));
            jSONObject.put("versionName", d.i(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("deviceId", d.a(context));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    private static synchronized long D(Context context) {
        long j;
        synchronized (MobileAgent.class) {
            j = B(context).getLong("uploadpopindex", 0);
        }
        return j;
    }

    /* access modifiers changed from: private */
    public static String E(Context context) {
        String str;
        synchronized (aZ) {
            str = "";
            String string = B(context).getString("uploadList", "");
            if (!string.equals("")) {
                str = string.substring(0, string.indexOf("|"));
            }
        }
        return str;
    }

    private static int a(Context context, String str, JSONObject jSONObject) {
        try {
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(str + "&appkey=" + d.e(context) + "&channel=" + URLEncoder.encode(d.f(context), e.f));
            httpPost.addHeader("Accept", "text/javascript");
            httpPost.addHeader("Content-Type", "application/json;charset=utf-8");
            httpPost.setEntity(new StringEntity(jSONObject.toString(), "utf-8"));
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() != 200) {
                return 2;
            }
            new JSONObject(EntityUtils.toString(execute.getEntity())).getInt("resultcode");
            return 1;
        } catch (IOException | ClientProtocolException e2) {
            return 2;
        } catch (JSONException e3) {
            return 3;
        } catch (Exception e4) {
            e4.printStackTrace();
            return 2;
        }
    }

    protected static void a(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", SDK_VERSION);
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", d.b(context));
            jSONObject.put("deviceId", d.a(context));
            jSONObject.put("appKey", d.e(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", d.h(context));
            jSONObject.put("versionName", d.i(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("imsi", d.G(context));
            jSONObject.put("mac", d.d(context));
            jSONObject.put("deviceDetail", Build.MODEL);
            jSONObject.put("manufacturer", Build.MANUFACTURER);
            jSONObject.put("phoneOS", "android " + Build.VERSION.RELEASE);
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            jSONObject.put("screenWidth", displayMetrics.widthPixels);
            DisplayMetrics displayMetrics2 = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics2);
            jSONObject.put("screenHeight", displayMetrics2.heightPixels);
            DisplayMetrics displayMetrics3 = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics3);
            jSONObject.put("screenDensity", displayMetrics3.densityDpi);
            jSONObject.put("carrierName", URLEncoder.encode(((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName(), e.f));
            jSONObject.put("accessPoint", d.g(context));
            jSONObject.put("countryCode", Locale.getDefault().getCountry());
            jSONObject.put("languageCode", Locale.getDefault().getLanguage());
            jSONObject.put(a.d, URLEncoder.encode(d.f(context), e.f));
        } catch (JSONException e2) {
            e2.printStackTrace();
            return;
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
        if (a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postsyslog", jSONObject) == 1) {
            c(context, 1);
        }
    }

    protected static boolean a(Context context, String str) {
        int i;
        String k = k(context, str);
        if (!k.equals("")) {
            JSONObject C = C(context);
            try {
                C.put("sid", new JSONObject(k).get("sid"));
                C.put("logJsonAry", new JSONArray("[" + k + "]"));
                i = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postactlog", C);
            } catch (JSONException e2) {
                e2.printStackTrace();
                i = 3;
            }
            if (i == 1 || i == 3) {
                c(context, 3);
                f(context, str);
                "send actlog success:" + str + " size:" + C.toString().getBytes().length;
                return true;
            }
            if (i == 2) {
            }
            return false;
        }
        c(context, 3);
        f(context, str);
        return true;
    }

    protected static void b(Context context) {
        HttpPost httpPost = new HttpPost("http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:getappparameter&appkey=" + d.e(context));
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpClientParams.setRedirecting(basicHttpParams, true);
            HttpProtocolParams.setUserAgent(basicHttpParams, k.a());
            HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
            HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
            if (k.a(context).equals("cmwap")) {
                basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, (String) null));
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient.setRedirectHandler(new b());
            JSONObject jSONObject = new JSONObject(EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity()));
            SharedPreferences.Editor edit = y(context).edit();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                edit.putString(next, jSONObject.getString(next));
            }
            edit.commit();
            Looper.prepare();
            if (jSONObject.length() > 0) {
                aY.z();
            }
            Looper.loop();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        } catch (ParseException e4) {
            e4.printStackTrace();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
    }

    protected static boolean b(Context context, int i) {
        int i2;
        int i3;
        SharedPreferences y = y(context);
        if (i == 3) {
            i2 = y.getInt("actionmonth", 0);
            i3 = y.getInt("actionday", 0);
        } else if (i == 2) {
            i2 = y.getInt("eventmonth", 0);
            i3 = y.getInt("eventday", 0);
        } else {
            i2 = y.getInt("sysmonth", 0);
            i3 = y.getInt("sysday", 0);
        }
        Date date = new Date();
        return (Integer.valueOf(new SimpleDateFormat("M").format(date)).intValue() == i2 && Integer.valueOf(new SimpleDateFormat("dd").format(date)).intValue() == i3) ? false : true;
    }

    protected static boolean b(Context context, String str) {
        String k = k(context, str);
        if (!k.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(k);
                String b2 = d.b(context);
                jSONObject.put("pid", 1);
                jSONObject.put("protocolVersion", SDK_VERSION);
                jSONObject.put("sdkVersion", SDK_VERSION);
                jSONObject.put("cid", b2);
                jSONObject.put("appKey", d.e(context));
                jSONObject.put("packageName", context.getPackageName());
                jSONObject.put("versionCode", d.h(context));
                jSONObject.put("versionName", d.i(context));
                jSONObject.put("sendTime", System.currentTimeMillis());
                jSONObject.put("deviceId", d.a(context));
                int a2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posteventlog", jSONObject);
                if (a2 == 1 || a2 == 3) {
                    c(context, 3);
                    f(context, str);
                    "send eventlog success" + str + " size:" + jSONObject.toString().getBytes().length;
                    return true;
                }
                if (a2 == 2) {
                }
                return false;
            } catch (JSONException e2) {
                e2.printStackTrace();
                return true;
            }
        } else {
            c(context, 3);
            f(context, str);
            return true;
        }
    }

    static void c(Context context) {
        new Thread(new a(context)).start();
    }

    private static void c(Context context, int i) {
        Date date = new Date();
        int parseInt = Integer.parseInt(new SimpleDateFormat("dd").format(date));
        int parseInt2 = Integer.parseInt(new SimpleDateFormat("M").format(date));
        SharedPreferences.Editor edit = y(context).edit();
        if (i == 3) {
            edit.putInt("actionmonth", parseInt2);
            edit.putInt("actionday", parseInt);
        } else if (i == 2) {
            edit.putInt("eventmonth", parseInt2);
            edit.putInt("eventday", parseInt);
        } else {
            edit.putInt("sysmonth", parseInt2);
            edit.putInt("sysday", parseInt);
        }
        edit.commit();
    }

    /* access modifiers changed from: private */
    public static synchronized boolean c(Context context, String str, int i) {
        String str2;
        boolean z = false;
        synchronized (MobileAgent.class) {
            if (i == 3) {
                str2 = "act";
            } else if (i == 2) {
                str2 = "evn";
            } else if (i == 4) {
                str2 = "err";
            }
            if (!str.equals("")) {
                long D = D(context);
                long j = 1 + D;
                String str3 = str2 + D;
                try {
                    FileOutputStream openFileOutput = context.openFileOutput(str3, 1);
                    synchronized (aZ) {
                        SharedPreferences B = B(context);
                        int i2 = B.getInt("uploadcount", 0);
                        String str4 = B.getString("uploadList", "") + str3 + "|";
                        B.edit().putString("uploadList", str4).commit();
                        B.edit().putLong("uploadpopindex", j).commit();
                        if (str4.split("\\|").length > 200) {
                            String E = E(context);
                            context.deleteFile(E);
                            l(context, E);
                        } else {
                            B.edit().putInt("uploadcount", i2 + 1).commit();
                        }
                    }
                    openFileOutput.write(str.getBytes());
                    openFileOutput.close();
                } catch (FileNotFoundException e2) {
                } catch (IOException e3) {
                }
                z = true;
            }
        }
        return z;
    }

    protected static void d(Context context) {
        new Thread(new c(context)).start();
    }

    private static void f(Context context, String str) {
        context.deleteFile(str);
        l(context, str);
    }

    private static boolean f(Context context, SharedPreferences sharedPreferences) {
        String string = sharedPreferences.getString("sessionId", null);
        String string2 = sharedPreferences.getString("activities", null);
        aX = new JSONObject();
        try {
            aX.put("sid", string);
            aX.put("logs", string2);
            if (!c(context, aX.toString(), 3)) {
                return true;
            }
            sharedPreferences.edit().putString("activities", "").commit();
            return true;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    private static String g(Context context, String str, String str2) {
        long j = 0;
        long currentTimeMillis = System.currentTimeMillis();
        if (str.equals("onResume")) {
            g = context.getClass().getName();
            h = currentTimeMillis;
        } else if (str.equals("onPause") && g.equals(context.getClass().getName())) {
            j = currentTimeMillis - h;
        }
        if (str2 == null) {
            str2 = "";
        }
        return str2 + (str + "|" + context.getClass().getName() + "|" + currentTimeMillis + "|" + j + "\n");
    }

    private static String getConfigParams(Context context, String str) {
        return y(context).getString(str, b.SMS_RESULT_SEND_SUCCESS);
    }

    protected static boolean j(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(k(context, str));
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", SDK_VERSION);
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", d.b(context));
            jSONObject.put("deviceId", d.a(context));
            jSONObject.put("appKey", d.e(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", d.h(context));
            jSONObject.put("versionName", d.i(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            int a2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posterrlog", jSONObject);
            if (a2 == 1 || a2 == 3) {
                c(context, 3);
                f(context, str);
                "send errlog success" + str;
                return true;
            }
            if (a2 == 2) {
            }
            return false;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0032, code lost:
        r0 = "";
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized java.lang.String k(android.content.Context r8, java.lang.String r9) {
        /*
            java.lang.Class<com.chinaMobile.MobileAgent> r1 = com.chinaMobile.MobileAgent.class
            monitor-enter(r1)
            java.lang.String r0 = ""
            java.io.FileInputStream r2 = r8.openFileInput(r9)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r3 = 10000(0x2710, float:1.4013E-41)
            byte[] r3 = new byte[r3]     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
        L_0x000d:
            int r4 = r2.read(r3)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r5 = -1
            if (r4 == r5) goto L_0x002c
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.String r6 = new java.lang.String     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            r7 = 0
            r6.<init>(r3, r7, r4)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.StringBuilder r4 = r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            java.lang.String r0 = r4.toString()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
            goto L_0x000d
        L_0x002c:
            r2.close()     // Catch:{ FileNotFoundException -> 0x0031, IOException -> 0x003a, all -> 0x0035 }
        L_0x002f:
            monitor-exit(r1)
            return r0
        L_0x0031:
            r0 = move-exception
            java.lang.String r0 = ""
            goto L_0x002f
        L_0x0035:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0037 }
        L_0x0037:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x003a:
            r2 = move-exception
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chinaMobile.MobileAgent.k(android.content.Context, java.lang.String):java.lang.String");
    }

    private static void l(Context context, String str) {
        synchronized (aZ) {
            SharedPreferences B = B(context);
            B.edit().putString("uploadList", B.getString("uploadList", "").replace(str + "|", "")).commit();
        }
    }

    private static boolean m(Context context) {
        SharedPreferences A = A(context);
        String string = A.getString("eventlogs", "");
        if (string.equals("")) {
            return false;
        }
        String string2 = z(context).getString("sessionId", null);
        aX = new JSONObject();
        try {
            aX.put("sid", string2);
            aX.put("logJsonAry", string);
            if (c(context, aX.toString(), 2)) {
                A.edit().putString("eventlogs", "").commit();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return true;
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, str, 1);
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    private static void onEvent(Context context, String str, String str2, int i) {
        if (context != null && str != null && !str.equals("") && str2 != null && !str2.equals("")) {
            SharedPreferences A = A(context);
            String string = A.getString("eventlogs", "");
            try {
                string = string + URLEncoder.encode(str, e.f) + "|" + URLEncoder.encode(str2, e.f) + "|1" + "|" + System.currentTimeMillis() + "\n";
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            A.edit().putString("eventlogs", string).commit();
            if (string.getBytes().length > 10000) {
                m(context);
            }
        }
    }

    public static void onPause(Context context) {
        SharedPreferences z = z(context);
        String string = z.getString("activities", null);
        SharedPreferences.Editor edit = z.edit();
        edit.putLong("endTime", System.currentTimeMillis());
        edit.putString("activities", g(context, "onPause", string));
        edit.commit();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0097, code lost:
        if ((r0 != null && r0.getType() == 1) != false) goto L_0x0099;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void onResume(android.content.Context r5) {
        /*
            r1 = 1
            r2 = 0
            java.lang.String r0 = com.chinaMobile.d.e(r5)
            com.chinaMobile.d.f(r5)
            boolean r3 = com.chinaMobile.MobileAgent.e     // Catch:{ Exception -> 0x0025 }
            if (r5 != 0) goto L_0x0015
            java.lang.String r0 = "MoblieAgent"
            java.lang.String r1 = "unexpected null context"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0025 }
        L_0x0014:
            return
        L_0x0015:
            if (r0 == 0) goto L_0x001d
            int r0 = r0.length()     // Catch:{ Exception -> 0x0025 }
            if (r0 != 0) goto L_0x0031
        L_0x001d:
            java.lang.String r0 = "MoblieAgent"
            java.lang.String r1 = "unexpected empty appkey"
            android.util.Log.e(r0, r1)     // Catch:{ Exception -> 0x0025 }
            goto L_0x0014
        L_0x0025:
            r0 = move-exception
            java.lang.String r1 = "MoblieAgent"
            java.lang.String r2 = "Exception occurred in Mobclick.onResume(). "
            android.util.Log.e(r1, r2)
            r0.printStackTrace()
            goto L_0x0014
        L_0x0031:
            com.chinaMobile.h r0 = new com.chinaMobile.h     // Catch:{ Exception -> 0x0025 }
            r3 = 7
            r0.<init>(r5, r3)     // Catch:{ Exception -> 0x0025 }
            r0.start()     // Catch:{ Exception -> 0x0025 }
            boolean r3 = x(r5)     // Catch:{ Exception -> 0x0025 }
            boolean r0 = com.chinaMobile.MobileAgent.e     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x007a
            java.lang.String r0 = "updateonlyonwifi"
            java.lang.String r0 = getConfigParams(r5, r0)     // Catch:{ Exception -> 0x0025 }
            java.lang.String r4 = "1"
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x00a3
            r0 = 1
            com.chinaMobile.MobileAgent.d = r0     // Catch:{ Exception -> 0x0025 }
        L_0x0053:
            java.lang.String r0 = "updatedelay"
            java.lang.String r0 = getConfigParams(r5, r0)     // Catch:{ Exception -> 0x0025 }
            java.lang.String r4 = "0"
            boolean r4 = r0.equals(r4)     // Catch:{ Exception -> 0x0025 }
            if (r4 != 0) goto L_0x0069
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0025 }
            int r0 = r0 * 1000
            com.chinaMobile.MobileAgent.b = r0     // Catch:{ Exception -> 0x0025 }
        L_0x0069:
            java.lang.String r0 = "send_policy"
            java.lang.String r0 = getConfigParams(r5, r0)     // Catch:{ Exception -> 0x0025 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x0025 }
            com.chinaMobile.MobileAgent.a = r0     // Catch:{ Exception -> 0x0025 }
            if (r0 != 0) goto L_0x007a
            r0 = 1
            com.chinaMobile.MobileAgent.a = r0     // Catch:{ Exception -> 0x0025 }
        L_0x007a:
            boolean r0 = com.chinaMobile.MobileAgent.d     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x0099
            boolean r0 = com.chinaMobile.MobileAgent.d     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x009e
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r5.getSystemService(r0)     // Catch:{ Exception -> 0x0025 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x0025 }
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x00a7
            int r0 = r0.getType()     // Catch:{ Exception -> 0x0025 }
            if (r0 != r1) goto L_0x00a7
            r0 = r1
        L_0x0097:
            if (r0 == 0) goto L_0x009e
        L_0x0099:
            int r0 = com.chinaMobile.MobileAgent.a     // Catch:{ Exception -> 0x0025 }
            switch(r0) {
                case 1: goto L_0x00a9;
                case 2: goto L_0x00b5;
                case 3: goto L_0x00c6;
                default: goto L_0x009e;
            }     // Catch:{ Exception -> 0x0025 }
        L_0x009e:
            r0 = 0
            com.chinaMobile.MobileAgent.e = r0     // Catch:{ Exception -> 0x0025 }
            goto L_0x0014
        L_0x00a3:
            r0 = 0
            com.chinaMobile.MobileAgent.d = r0     // Catch:{ Exception -> 0x0025 }
            goto L_0x0053
        L_0x00a7:
            r0 = r2
            goto L_0x0097
        L_0x00a9:
            if (r3 == 0) goto L_0x009e
            com.chinaMobile.h r0 = new com.chinaMobile.h     // Catch:{ Exception -> 0x0025 }
            r1 = 6
            r0.<init>(r5, r1)     // Catch:{ Exception -> 0x0025 }
            r0.start()     // Catch:{ Exception -> 0x0025 }
            goto L_0x009e
        L_0x00b5:
            boolean r0 = com.chinaMobile.MobileAgent.e     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x009e
            com.chinaMobile.h r0 = new com.chinaMobile.h     // Catch:{ Exception -> 0x0025 }
            r1 = 6
            r0.<init>(r5, r1)     // Catch:{ Exception -> 0x0025 }
            r0.start()     // Catch:{ Exception -> 0x0025 }
            r0 = 0
            com.chinaMobile.MobileAgent.e = r0     // Catch:{ Exception -> 0x0025 }
            goto L_0x009e
        L_0x00c6:
            r0 = 3
            boolean r0 = b(r5, r0)     // Catch:{ Exception -> 0x0025 }
            if (r0 == 0) goto L_0x009e
            com.chinaMobile.h r0 = new com.chinaMobile.h     // Catch:{ Exception -> 0x0025 }
            r1 = 6
            r0.<init>(r5, r1)     // Catch:{ Exception -> 0x0025 }
            r0.start()     // Catch:{ Exception -> 0x0025 }
            goto L_0x009e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chinaMobile.MobileAgent.onResume(android.content.Context):void");
    }

    private static synchronized boolean x(Context context) {
        boolean z = true;
        synchronized (MobileAgent.class) {
            String e2 = d.e(context);
            SharedPreferences z2 = z(context);
            String string = z2.getString("sessionId", null);
            if (System.currentTimeMillis() - z2.getLong("endTime", -1) > 30000) {
                if (string != null) {
                    m(context);
                    f(context, z2);
                    SharedPreferences.Editor edit = z2.edit();
                    edit.clear();
                    edit.commit();
                }
                String valueOf = String.valueOf(d.a(System.currentTimeMillis() + e2 + d.a(context) + d.d(context) + new Random().nextInt(10)).toCharArray(), 8, 16);
                SharedPreferences.Editor edit2 = z2.edit();
                edit2.putString("appKey", e2);
                edit2.putString("sessionId", valueOf);
                edit2.putLong("lastResumeTime", System.currentTimeMillis());
                edit2.putString("activities", g(context, "onResume", null));
                edit2.commit();
            } else {
                String string2 = z2.getString("activities", null);
                if (string2.getBytes().length > 10000) {
                    f(context, z2);
                    string2 = "";
                }
                SharedPreferences.Editor edit3 = z2.edit();
                edit3.putString("activities", g(context, "onResume", string2));
                edit3.putLong("lastResumeTime", System.currentTimeMillis());
                edit3.commit();
                z = false;
            }
        }
        return z;
    }

    private static SharedPreferences y(Context context) {
        return context.getSharedPreferences("MoblieAgent_config_" + context.getPackageName(), 0);
    }

    /* access modifiers changed from: private */
    public static SharedPreferences z(Context context) {
        return context.getSharedPreferences("MoblieAgent_state_" + context.getPackageName(), 0);
    }
}
