package com.chinaMobile;

import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;

final class b implements RedirectHandler {
    b() {
    }

    public final URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
        String value;
        Header firstHeader = httpResponse.containsHeader("location") ? httpResponse.getFirstHeader("location") : httpResponse.containsHeader("Location") ? httpResponse.getFirstHeader("Location") : httpResponse.containsHeader("LOCATION") ? httpResponse.getFirstHeader("LOCATION") : null;
        if (firstHeader == null || (value = firstHeader.getValue()) == null) {
            return null;
        }
        return URI.create(value);
    }

    public final boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        return statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 307;
    }
}
