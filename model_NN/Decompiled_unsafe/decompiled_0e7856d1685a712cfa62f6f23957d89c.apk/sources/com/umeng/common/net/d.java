package com.umeng.common.net;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.umeng.common.Log;
import com.umeng.common.net.a;
import java.io.File;

class d extends Handler {
    final /* synthetic */ DownloadingService a;

    d(DownloadingService downloadingService) {
        this.a = downloadingService;
    }

    public void handleMessage(Message message) {
        a.C0003a aVar = (a.C0003a) message.obj;
        int i = message.arg2;
        try {
            String string = message.getData().getString("filename");
            Log.c(DownloadingService.j, "Cancel old notification....");
            Notification notification = new Notification(17301634, com.umeng.common.a.l, System.currentTimeMillis());
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.addFlags(268435456);
            intent.setDataAndType(Uri.fromFile(new File(string)), "application/vnd.android.package-archive");
            notification.setLatestEventInfo(this.a.m, aVar.b, com.umeng.common.a.l, PendingIntent.getActivity(this.a.m, 0, intent, 134217728));
            notification.flags = 16;
            this.a.k = (NotificationManager) this.a.getSystemService("notification");
            this.a.k.notify(i + 1, notification);
            Log.c(DownloadingService.j, "Show new  notification....");
            boolean a2 = DownloadingService.b(this.a.m);
            Log.c(DownloadingService.j, String.format("isAppOnForeground = %1$B", Boolean.valueOf(a2)));
            if (a2) {
                this.a.k.cancel(i + 1);
                this.a.m.startActivity(intent);
            }
            Log.a(DownloadingService.j, String.format("%1$10s downloaded. Saved to: %2$s", aVar.b, string));
        } catch (Exception e) {
            Log.b(DownloadingService.j, "can not install. " + e.getMessage());
            this.a.k.cancel(i + 1);
        }
    }
}
