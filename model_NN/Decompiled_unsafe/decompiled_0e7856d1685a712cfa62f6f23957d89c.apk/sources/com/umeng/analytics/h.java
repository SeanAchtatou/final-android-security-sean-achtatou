package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import com.a.a.e.a;
import com.umeng.common.Log;
import com.umeng.common.b;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

class h {
    h() {
    }

    static SharedPreferences a(Context context) {
        return context.getSharedPreferences("mobclick_agent_user_" + context.getPackageName(), 0);
    }

    static void a(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            String g = g(context);
            try {
                jSONObject.put("cache_version", b.d(context));
                FileOutputStream openFileOutput = context.openFileOutput(g, 0);
                openFileOutput.write(jSONObject.toString().getBytes());
                openFileOutput.flush();
                openFileOutput.close();
            } catch (FileNotFoundException e) {
                Log.b("MobclickAgent", "cache message error", e);
            } catch (IOException e2) {
                Log.b("MobclickAgent", "cache message error", e2);
            } catch (Exception e3) {
                Log.b("MobclickAgent", "message is null", e3);
            }
        }
    }

    static SharedPreferences b(Context context) {
        return context.getSharedPreferences("mobclick_agent_online_setting_" + context.getPackageName(), 0);
    }

    static void b(Context context, JSONObject jSONObject) {
        if (jSONObject != null) {
            JSONObject jSONObject2 = null;
            try {
                jSONObject2 = jSONObject.optJSONObject("body");
            } catch (Exception e) {
                e.printStackTrace();
            }
            a(context, jSONObject2);
        }
    }

    static SharedPreferences c(Context context) {
        return context.getSharedPreferences("mobclick_agent_header_" + context.getPackageName(), 0);
    }

    static SharedPreferences d(Context context) {
        return context.getSharedPreferences("mobclick_agent_update_" + context.getPackageName(), 0);
    }

    static SharedPreferences e(Context context) {
        return context.getSharedPreferences("mobclick_agent_state_" + context.getPackageName(), 0);
    }

    static String f(Context context) {
        return "mobclick_agent_header_" + context.getPackageName();
    }

    static String g(Context context) {
        return "mobclick_agent_cached_" + context.getPackageName();
    }

    static JSONObject h(Context context) {
        JSONObject jSONObject = new JSONObject();
        SharedPreferences a = a(context);
        try {
            if (a.getInt("gender", -1) != -1) {
                jSONObject.put("sex", a.getInt("gender", -1));
            }
            if (a.getInt("age", -1) != -1) {
                jSONObject.put("age", a.getInt("age", -1));
            }
            if (!"".equals(a.getString("user_id", ""))) {
                jSONObject.put("id", a.getString("user_id", ""));
            }
            if (!"".equals(a.getString("id_source", ""))) {
                jSONObject.put("url", URLEncoder.encode(a.getString("id_source", "")));
            }
            if (jSONObject.length() > 0) {
                return jSONObject;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static JSONObject i(Context context) {
        String str;
        try {
            FileInputStream openFileInput = context.openFileInput(g(context));
            String str2 = "";
            byte[] bArr = new byte[a.GAME_B_PRESSED];
            while (true) {
                str = str2;
                int read = openFileInput.read(bArr);
                if (read == -1) {
                    break;
                }
                str2 = String.valueOf(str) + new String(bArr, 0, read);
            }
            openFileInput.close();
            if (str.length() == 0) {
                return null;
            }
            try {
                return new JSONObject(str);
            } catch (JSONException e) {
                j(context);
                e.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException | IOException e2) {
            return null;
        }
    }

    static void j(Context context) {
        context.deleteFile(f(context));
        context.deleteFile(g(context));
    }
}
