package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.link.PluginLink;

final class cr extends Handler {
    final /* synthetic */ ck a;

    cr(ck ckVar) {
        this.a = ckVar;
    }

    private void a(w wVar) {
        if ("0000".equals(wVar.m())) {
            switch (wVar.e()) {
                case 8194:
                case 8195:
                case 8196:
                case 8197:
                case 8200:
                case 8201:
                case 8203:
                case 8204:
                case 8206:
                    return;
                case 8198:
                case 8199:
                case 8202:
                case 8205:
                default:
                    k.a().b();
                    return;
            }
        } else {
            switch (wVar.e()) {
                case 8210:
                case 8225:
                case 8227:
                case 8229:
                    k.a().b();
                    return;
                default:
                    return;
            }
        }
    }

    private void b(w wVar) {
        switch (wVar.e()) {
            case 8194:
            case 8221:
            case 8224:
                r.a().f(true);
                return;
            default:
                return;
        }
    }

    public final void handleMessage(Message message) {
        ca caVar;
        w wVar = (w) message.obj;
        if (!"3022".equals(this.a.d.m())) {
            a(wVar);
            this.a.a(wVar);
            if (!"0000".equals(wVar.m())) {
                b(wVar);
            }
            this.a.c.responseCallBack(wVar);
            if (this.a.e != null && (caVar = (ca) this.a.e.poll()) != null) {
                this.a.a(caVar);
            }
        } else if (this.a.a != null) {
            k.a().a(this.a.a, this.a.a.getString(PluginLink.getStringupomp_lthj_session_timeout_tip()), new aj(this));
        }
    }
}
