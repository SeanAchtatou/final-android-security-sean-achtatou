package com.lthj.unipay.plugin;

import android.util.Xml;
import com.umeng.common.b.e;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Vector;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class ap {
    private static ap J = null;
    public Vector A;
    public GetBundleBankCardList B;
    public String C;
    public String D;
    public String E;
    public String F;
    public String G;
    public String H;
    public String I;
    public dc a;
    public boolean b = false;
    public boolean c = false;
    public boolean d = false;
    public String e;
    public String f;
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public String n;
    public String o;
    public String p;
    public String q;
    public boolean r;
    public boolean s;
    public StringBuffer t = new StringBuffer();
    public StringBuffer u = new StringBuffer();
    public StringBuffer v = new StringBuffer();
    public StringBuffer w = new StringBuffer();
    public StringBuffer x = new StringBuffer();
    public StringBuffer y = new StringBuffer();
    public StringBuffer z = new StringBuffer();

    public static ap a() {
        if (J == null) {
            J = new ap();
        }
        return J;
    }

    public void a(byte[] bArr) {
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
            XmlPullParser newPullParser = Xml.newPullParser();
            newPullParser.setInput(byteArrayInputStream, e.f);
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (!name.equalsIgnoreCase("merchantId")) {
                            if (!name.equalsIgnoreCase("merchantOrderId")) {
                                if (!name.equalsIgnoreCase("merchantOrderTime")) {
                                    if (!name.equalsIgnoreCase("sign")) {
                                        break;
                                    } else {
                                        this.o = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.i = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                this.h = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.e = newPullParser.nextText();
                            break;
                        }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            throw e2;
        }
    }

    public void b() {
        J = null;
        System.gc();
    }

    public void c() {
        this.x.delete(0, this.x.length());
        this.y.delete(0, this.y.length());
        this.z.delete(0, this.z.length());
    }

    public byte[] d() {
        XmlSerializer newSerializer = Xml.newSerializer();
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            newSerializer.setOutput(byteArrayOutputStream, "utf-8");
            newSerializer.startDocument("utf-8", true);
            newSerializer.startTag(null, "upomp");
            newSerializer.attribute(null, "version", "1.0.0");
            newSerializer.startTag(null, "application");
            newSerializer.text("LanchPay.Rsp");
            newSerializer.endTag(null, "application");
            newSerializer.startTag(null, "merchantId");
            newSerializer.text(this.e);
            newSerializer.endTag(null, "merchantId");
            newSerializer.startTag(null, "merchantOrderId");
            newSerializer.text(this.h);
            newSerializer.endTag(null, "merchantOrderId");
            newSerializer.startTag(null, "merchantOrderTime");
            newSerializer.text(this.i);
            newSerializer.endTag(null, "merchantOrderTime");
            newSerializer.startTag(null, "respCode");
            if (this.p != null) {
                newSerializer.text(this.p);
            } else {
                newSerializer.text("");
            }
            newSerializer.endTag(null, "respCode");
            newSerializer.startTag(null, "respDesc");
            if (this.q != null) {
                newSerializer.text(this.q);
            } else {
                newSerializer.text("");
            }
            newSerializer.endTag(null, "respDesc");
            newSerializer.endTag(null, "upomp");
            newSerializer.endDocument();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
