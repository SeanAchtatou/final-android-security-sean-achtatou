package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.CheckUserExist;
import com.unionpay.upomp.lthj.plugin.model.Data;

public class j extends w {
    private String a;
    private String b;

    public j(int i) {
        super(i);
    }

    public String a() {
        return this.b;
    }

    public void a(Data data) {
        CheckUserExist checkUserExist = (CheckUserExist) data;
        c(checkUserExist);
        this.b = checkUserExist.isExist;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        CheckUserExist checkUserExist = new CheckUserExist();
        b(checkUserExist);
        checkUserExist.loginName = this.a;
        return checkUserExist;
    }
}
