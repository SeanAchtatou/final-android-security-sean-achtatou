package com.lthj.unipay.plugin;

public class r {
    private static r a;
    private boolean b;
    private boolean c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;

    private r() {
    }

    public static r a() {
        if (a == null) {
            a = new r();
        }
        return a;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void b(boolean z) {
        this.d = z;
    }

    public boolean b() {
        return this.c;
    }

    public void c(boolean z) {
        this.e = z;
    }

    public boolean c() {
        return this.d;
    }

    public void d(boolean z) {
        this.f = z;
    }

    public boolean d() {
        return this.e;
    }

    public void e(boolean z) {
        this.g = z;
    }

    public boolean e() {
        return this.f;
    }

    public void f(boolean z) {
        this.b = z;
    }

    public boolean f() {
        return this.g;
    }

    public boolean g() {
        return this.b;
    }

    public void h() {
        a = null;
    }
}
