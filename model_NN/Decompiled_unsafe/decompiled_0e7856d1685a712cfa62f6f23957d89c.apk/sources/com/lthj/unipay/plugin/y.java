package com.lthj.unipay.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class y extends SQLiteOpenHelper {
    private static String a = "plug_info.db";
    private static int b = 1;
    private String c;
    private String[] d;
    private String[] e;
    private SQLiteDatabase f;

    public y(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, a, cursorFactory, b);
    }

    public y(Context context, String str, String[] strArr, String[] strArr2) {
        this(context, (String) null, (SQLiteDatabase.CursorFactory) null, b);
        this.c = str;
        this.d = strArr;
        this.e = strArr2;
    }

    public Cursor a(int i) {
        this.f = getReadableDatabase();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("select * from ").append(this.c).append(" where _id = ").append(i);
        return this.f.rawQuery(stringBuffer.toString(), null);
    }

    public void a() {
        if (this.f != null) {
            this.f.close();
        }
    }

    public boolean a(int i, ContentValues contentValues) {
        this.f = getWritableDatabase();
        int update = this.f.update(this.c, contentValues, "_id = ?", new String[]{new StringBuilder().append(i).toString()});
        if (this.f != null) {
            this.f.close();
        }
        return update != 0;
    }

    public boolean a(ContentValues contentValues) {
        this.f = getWritableDatabase();
        long insert = this.f.insert(this.c, null, contentValues);
        if (this.f != null) {
            this.f.close();
        }
        return insert != -1;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        if (this.c != null && this.d != null && this.e != null && this.d.length == this.e.length) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("CREATE TABLE ").append(this.c).append(" ( _id INTEGER PRIMARY KEY ");
            for (int i = 0; i < this.d.length; i++) {
                stringBuffer.append(",").append(this.d[i]).append(" ").append(this.e[i]);
            }
            stringBuffer.append(");");
            sQLiteDatabase.execSQL(stringBuffer.toString());
        }
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        Log.w("PlugDatabaseHelper", "Upgrading database from version " + i + " to " + i2 + ", which will destroy all old data");
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + this.c);
        onCreate(sQLiteDatabase);
    }
}
