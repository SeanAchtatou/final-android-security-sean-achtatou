package com.lthj.unipay.plugin;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class au implements TextWatcher {
    private EditText a;
    private StringBuffer b;
    private int c = 0;
    private int d = 0;

    public au(EditText editText) {
        this.a = editText;
    }

    public void afterTextChanged(Editable editable) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.b = new StringBuffer();
        this.b.append(charSequence);
        this.d = this.b.length();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuffer.insert(int, float):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, boolean):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, double):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.Object):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, long):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, int):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.CharSequence):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, java.lang.String):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char[]):java.lang.StringBuffer}
      ClspMth{java.lang.StringBuffer.insert(int, char):java.lang.StringBuffer} */
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        this.b = new StringBuffer();
        this.b.append(charSequence);
        this.c = this.b.length();
        for (int i4 = 0; i4 < this.b.length(); i4++) {
            if (this.b.charAt(i4) == ' ') {
                this.b.deleteCharAt(i4);
            }
        }
        for (int i5 = 0; i5 < this.b.length(); i5++) {
            if ((i5 + 1) % 5 == 0 && ' ' != this.b.charAt(i5)) {
                this.b.insert(i5, ' ');
            }
        }
        int selectionStart = this.a.getSelectionStart();
        if (!(this.a instanceof EditText)) {
            this.a.setText(this.b.toString());
        } else if (this.c > this.d) {
            int length = this.b.toString().length() - this.c;
            this.a.setText(this.b.toString());
            if (length <= 0) {
                if (selectionStart % 5 == 1 || selectionStart % 5 == 2 || selectionStart % 5 == 3 || selectionStart % 5 == 4) {
                    if (selectionStart > this.b.toString().length()) {
                        selectionStart = this.b.toString().length();
                    }
                    this.a.setSelection(selectionStart);
                } else if (selectionStart % 5 == 0) {
                    this.a.setSelection(selectionStart + 1 > this.b.toString().length() ? this.b.toString().length() : selectionStart + 1);
                }
            } else if (length <= 0) {
            } else {
                if (selectionStart % 5 == 1 || selectionStart % 5 == 2 || selectionStart % 5 == 3) {
                    if (selectionStart > this.b.toString().length()) {
                        selectionStart = this.b.toString().length();
                    }
                    this.a.setSelection(selectionStart);
                } else if (selectionStart % 5 == 0 || selectionStart % 5 == 4) {
                    this.a.setSelection(selectionStart + 1 > this.b.toString().length() ? this.b.toString().length() : selectionStart + 1);
                }
            }
        } else if (this.c < this.d) {
            this.a.setText(this.b.toString());
            if (selectionStart > this.b.toString().length()) {
                selectionStart = this.b.toString().length();
            }
            this.a.setSelection(selectionStart);
        }
    }
}
