package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.AboutActivity;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;

public class a implements View.OnClickListener {
    final /* synthetic */ AboutActivity a;

    public a(AboutActivity aboutActivity) {
        this.a = aboutActivity;
    }

    public void onClick(View view) {
        this.a.a().changeSubActivity(new Intent(this.a, HomeActivity.class));
    }
}
