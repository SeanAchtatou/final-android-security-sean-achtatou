package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.QuickRegister;

public class cp extends w {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public cp(int i) {
        super(i);
    }

    public void a(Data data) {
        QuickRegister quickRegister = (QuickRegister) data;
        c(quickRegister);
        this.a = quickRegister.loginName;
        this.b.delete(0, this.b.length());
        this.b.append(quickRegister.password);
        this.e = quickRegister.welcome;
        this.f = quickRegister.secureQuestion;
        this.g = quickRegister.secureAnswer;
        this.h = quickRegister.merchantOrderTime;
        this.c = quickRegister.merchantId;
        this.d = quickRegister.merchantOrderId;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        QuickRegister quickRegister = new QuickRegister();
        b(quickRegister);
        quickRegister.loginName = this.a;
        quickRegister.password = this.b.toString();
        quickRegister.merchantOrderTime = this.h;
        quickRegister.welcome = this.e;
        quickRegister.secureQuestion = this.f;
        quickRegister.secureAnswer = this.g;
        quickRegister.merchantId = this.c;
        quickRegister.merchantOrderId = this.d;
        return quickRegister;
    }

    public void b(String str) {
        this.b = new StringBuffer(str);
    }

    public void c(String str) {
        this.c = str;
    }

    public void d(String str) {
        this.d = str;
    }

    public void e(String str) {
        this.e = str;
    }

    public void f(String str) {
        this.f = str;
    }

    public void g(String str) {
        this.g = str;
    }

    public void h(String str) {
        this.h = str;
    }
}
