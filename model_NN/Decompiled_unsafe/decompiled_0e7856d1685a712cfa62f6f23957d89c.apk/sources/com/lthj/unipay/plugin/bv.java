package com.lthj.unipay.plugin;

import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.AccountActivity;
import java.util.TimerTask;

public class bv extends TimerTask {
    final /* synthetic */ AccountActivity a;

    public bv(AccountActivity accountActivity) {
        this.a = accountActivity;
    }

    public void run() {
        Message obtain = Message.obtain();
        obtain.obj = new StringBuilder().append(this.a.a).toString();
        this.a.n.sendMessage(obtain);
        if (this.a.a < 0) {
            this.a.aaTimerTask.cancel();
            this.a.aaTimerTask = null;
            this.a.a = 60;
            return;
        }
        AccountActivity accountActivity = this.a;
        accountActivity.a--;
    }
}
