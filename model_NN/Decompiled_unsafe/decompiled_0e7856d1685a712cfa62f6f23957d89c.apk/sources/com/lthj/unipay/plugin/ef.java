package com.lthj.unipay.plugin;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;
import com.unionpay.upomp.lthj.util.PluginHelper;

public final class ef implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ Bundle b;

    public ef(Activity activity, Bundle bundle) {
        this.a = activity;
        this.b = bundle;
    }

    public final void run() {
        Intent intent = new Intent();
        ComponentName componentName = new ComponentName(this.a.getPackageName(), "com.unionpay.upomp.lthj.plugin.ui.SplashActivity");
        intent.putExtras(this.b);
        intent.setComponent(componentName);
        this.a.startActivity(intent);
        synchronized (PluginHelper.a) {
            try {
                PluginHelper.a.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (ap.a().b) {
            intent.setComponent(new ComponentName(this.a.getPackageName(), "com.unionpay.upomp.lthj.plugin.ui.IndexActivityGroup"));
            this.a.startActivityForResult(intent, 1001);
            if (SplashActivity.instance != null) {
                SplashActivity.instance.finish();
                return;
            }
            return;
        }
        i.c();
        return;
    }
}
