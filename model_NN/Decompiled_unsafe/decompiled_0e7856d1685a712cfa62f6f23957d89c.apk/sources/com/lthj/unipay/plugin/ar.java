package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.MobileNumUpdate;

public class ar extends w {
    private String a;
    private StringBuffer b = new StringBuffer();
    private String c;
    private String d;
    private String e;

    public ar(int i) {
        super(i);
    }

    public String a() {
        return this.d;
    }

    public void a(Data data) {
        MobileNumUpdate mobileNumUpdate = (MobileNumUpdate) data;
        c(mobileNumUpdate);
        this.a = mobileNumUpdate.loginName;
        this.c = mobileNumUpdate.mobileNumber;
        this.d = mobileNumUpdate.newMobileNumber;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        MobileNumUpdate mobileNumUpdate = new MobileNumUpdate();
        b(mobileNumUpdate);
        mobileNumUpdate.loginName = this.a;
        mobileNumUpdate.password = this.b.toString();
        mobileNumUpdate.mobileNumber = this.c;
        mobileNumUpdate.newMobileNumber = this.d;
        mobileNumUpdate.mobileMac = this.e;
        return mobileNumUpdate;
    }

    public void b(String str) {
        this.c = str;
    }

    public void c(String str) {
        this.d = str;
    }

    public void d(String str) {
        this.e = str;
    }

    public void e(String str) {
        this.b.delete(0, this.b.length());
        this.b.append(str);
    }
}
