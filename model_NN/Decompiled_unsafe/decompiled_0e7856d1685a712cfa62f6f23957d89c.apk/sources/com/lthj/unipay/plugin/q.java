package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.model.BankCardBundle;
import com.unionpay.upomp.lthj.plugin.model.Data;

public class q extends w {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private StringBuffer g = new StringBuffer();
    private StringBuffer h = new StringBuffer();
    private StringBuffer i = new StringBuffer();
    private StringBuffer j = new StringBuffer();
    private String k;

    public q(int i2) {
        super(i2);
    }

    public void a(Data data) {
        BankCardBundle bankCardBundle = (BankCardBundle) data;
        c(bankCardBundle);
        this.a = bankCardBundle.loginName;
        this.b = bankCardBundle.mobileNumber;
    }

    public void a(String str) {
        this.a = str;
    }

    public Data b() {
        BankCardBundle bankCardBundle = new BankCardBundle();
        b(bankCardBundle);
        bankCardBundle.loginName = this.a;
        bankCardBundle.mobileNumber = this.b;
        bankCardBundle.mobileMac = this.c;
        bankCardBundle.validateCode = this.d;
        bankCardBundle.panType = this.e;
        bankCardBundle.panBankId = this.f;
        bankCardBundle.pan = this.g.toString();
        bankCardBundle.pin = this.h.toString();
        bankCardBundle.isDefault = this.k;
        bankCardBundle.panDate = this.i.toString();
        bankCardBundle.cvn2 = this.j.toString();
        return bankCardBundle;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.c = str;
    }

    public void d(String str) {
        this.g.delete(0, this.g.length());
        this.g.append(str);
    }

    public void e(String str) {
        this.h.delete(0, this.h.length());
        this.h.append(str);
    }

    public void f(String str) {
        this.i.delete(0, this.i.length());
        this.i.append(str);
    }

    public void g(String str) {
        this.j.delete(0, this.j.length());
        this.j.append(str);
    }

    public void h(String str) {
        this.k = str;
    }
}
