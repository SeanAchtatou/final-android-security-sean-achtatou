package com.lthj.unipay.plugin;

public class z {
    private static z f;
    public StringBuffer a = new StringBuffer();
    public StringBuffer b = new StringBuffer();
    public StringBuffer c = new StringBuffer();
    public StringBuffer d = new StringBuffer();
    public StringBuffer e = new StringBuffer();

    private z() {
    }

    public static z a() {
        if (f == null) {
            f = new z();
        }
        return f;
    }

    public void b() {
        if (this.a != null) {
            this.a.delete(0, this.a.length());
        }
        if (this.b != null) {
            this.b.delete(0, this.b.length());
        }
        if (this.c != null) {
            this.c.delete(0, this.c.length());
        }
        if (this.d != null) {
            this.d.delete(0, this.d.length());
        }
        if (this.e != null) {
            this.e.delete(0, this.e.length());
        }
        f = null;
    }
}
