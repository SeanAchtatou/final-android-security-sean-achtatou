package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.a.a.h.b;
import com.unionpay.upomp.lthj.plugin.ui.BankCardInfoActivity;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class dv implements View.OnClickListener {
    final /* synthetic */ BankCardInfoActivity a;

    public dv(BankCardInfoActivity bankCardInfoActivity) {
        this.a = bankCardInfoActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, SupportCardActivity.class);
        intent.putExtra("tranType", b.SMS_RESULT_SEND_CANCEL);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        k.a().b();
    }
}
