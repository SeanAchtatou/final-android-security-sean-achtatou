package com.lthj.unipay.plugin;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.umeng.common.b.e;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

public class aq implements Runnable {
    public static String a;
    private Context b;
    private String c;
    private Proxy d;
    private cz e = null;
    private byte[] f;
    private Thread g;
    private InputStream h;
    private DataOutputStream i;
    private HttpURLConnection j;
    private int k;

    public aq(Context context) {
        this.b = context;
        this.c = di.d;
    }

    private boolean c() {
        return this.j != null;
    }

    public void a() {
        "url=" + this.c;
        URL url = new URL(this.c);
        if (this.d != null) {
            this.j = (HttpURLConnection) url.openConnection(this.d);
        } else {
            this.j = (HttpURLConnection) url.openConnection();
        }
        this.j = (HttpURLConnection) url.openConnection();
        this.j.setRequestMethod("POST");
        this.j.setUseCaches(false);
        this.j.setRequestProperty("Charset", e.f);
        this.j.setRequestProperty("Content-Type", "text/plain");
        this.j.setDoInput(true);
        this.j.setDoOutput(true);
        this.j.setConnectTimeout(this.k);
        this.j.setReadTimeout(this.k);
        if (a != null) {
            this.j.setRequestProperty("sessionId", a);
            String str = a;
        }
        this.i = new DataOutputStream(this.j.getOutputStream());
        a(this.f, 0, this.f.length);
        this.j.connect();
        int responseCode = this.j.getResponseCode();
        if (this.j.getResponseCode() != 200) {
            throw new Exception(responseCode == 504 ? "网络连接超时, 请检查您的网络设置, 或稍候重试!" : "网络连接失败, 请检查您的网络设置, 或稍候重试!\r\n错误号: " + Integer.toString(responseCode));
        }
    }

    public void a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.getType() == 0) {
            String defaultHost = android.net.Proxy.getDefaultHost();
            int defaultPort = android.net.Proxy.getDefaultPort();
            if (defaultHost != null) {
                this.d = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(defaultHost, defaultPort));
            }
        }
    }

    public void a(byte[] bArr, int i2, int i3) {
        try {
            this.i.write(bArr, i2, i3);
            this.i.flush();
            " " + bArr.length;
        } catch (Exception e2) {
            e2.getMessage();
        }
    }

    public void a(byte[] bArr, int i2, int i3, cz czVar, int i4) {
        this.k = i4;
        this.f = bArr;
        this.e = czVar;
        this.g = new Thread(this);
        this.g.start();
    }

    public void b() {
        try {
            if (c()) {
                try {
                    if (this.h != null) {
                        this.h.close();
                        this.h = null;
                    }
                    if (this.i != null) {
                        this.i.close();
                        this.i = null;
                    }
                    if (this.j != null) {
                        this.j.disconnect();
                        this.j = null;
                    }
                    this.f = null;
                } catch (Exception e2) {
                    this.f = null;
                    this.j = null;
                    this.i = null;
                    this.h = null;
                }
            }
        } catch (Exception e3) {
            e3.getMessage();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0041 A[Catch:{ all -> 0x00c3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004a A[SYNTHETIC, Splitter:B:21:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f A[Catch:{ IOException -> 0x00a3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ad A[SYNTHETIC, Splitter:B:51:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b2 A[Catch:{ IOException -> 0x00b9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r4 = this;
            r2 = 0
            android.content.Context r0 = r4.b
            r4.a(r0)
            r4.a()     // Catch:{ SocketTimeoutException -> 0x0056, Exception -> 0x0066 }
        L_0x0009:
            java.net.HttpURLConnection r0 = r4.j     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 != r1) goto L_0x00cf
            java.net.HttpURLConnection r0 = r4.j     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            r4.h = r0     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            java.io.InputStream r0 = r4.h     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            r3.<init>(r0)     // Catch:{ Exception -> 0x00c6, all -> 0x00a8 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00ca, all -> 0x00be }
            r0 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r3, r0)     // Catch:{ Exception -> 0x00ca, all -> 0x00be }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            r0.<init>()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
        L_0x002e:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            if (r2 == 0) goto L_0x0076
            r0.append(r2)     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            goto L_0x002e
        L_0x0038:
            r0 = move-exception
            r2 = r3
        L_0x003a:
            r0.printStackTrace()     // Catch:{ all -> 0x00c3 }
            com.lthj.unipay.plugin.cz r0 = r4.e     // Catch:{ all -> 0x00c3 }
            if (r0 == 0) goto L_0x0048
            com.lthj.unipay.plugin.cz r0 = r4.e     // Catch:{ all -> 0x00c3 }
            java.lang.String r3 = "无网络连接，请检查网络"
            r0.a(r3, r4)     // Catch:{ all -> 0x00c3 }
        L_0x0048:
            if (r1 == 0) goto L_0x004d
            r1.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x004d:
            if (r2 == 0) goto L_0x0052
            r2.close()     // Catch:{ IOException -> 0x00a3 }
        L_0x0052:
            r4.b()
        L_0x0055:
            return
        L_0x0056:
            r0 = move-exception
            r0.printStackTrace()
            com.lthj.unipay.plugin.cz r0 = r4.e
            if (r0 == 0) goto L_0x0009
            com.lthj.unipay.plugin.cz r0 = r4.e
            java.lang.String r1 = "网络超时，请稍后再试"
            r0.a(r1, r4)
            goto L_0x0055
        L_0x0066:
            r0 = move-exception
            r0.printStackTrace()
            com.lthj.unipay.plugin.cz r0 = r4.e
            if (r0 == 0) goto L_0x0009
            com.lthj.unipay.plugin.cz r0 = r4.e
            java.lang.String r1 = "无网络连接，请检查网络"
            r0.a(r1, r4)
            goto L_0x0055
        L_0x0076:
            r1.close()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            r3.close()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            com.lthj.unipay.plugin.cz r2 = r4.e     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            if (r2 == 0) goto L_0x0090
            r0.toString()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            com.lthj.unipay.plugin.cz r2 = r4.e     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            byte[] r0 = r0.getBytes()     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
            r2.a(r0, r4)     // Catch:{ Exception -> 0x0038, all -> 0x00c1 }
        L_0x0090:
            if (r1 == 0) goto L_0x0095
            r1.close()     // Catch:{ IOException -> 0x009e }
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x009e }
        L_0x009a:
            r4.b()
            goto L_0x0055
        L_0x009e:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x009a
        L_0x00a3:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0052
        L_0x00a8:
            r0 = move-exception
            r1 = r2
            r3 = r2
        L_0x00ab:
            if (r1 == 0) goto L_0x00b0
            r1.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b0:
            if (r3 == 0) goto L_0x00b5
            r3.close()     // Catch:{ IOException -> 0x00b9 }
        L_0x00b5:
            r4.b()
            throw r0
        L_0x00b9:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b5
        L_0x00be:
            r0 = move-exception
            r1 = r2
            goto L_0x00ab
        L_0x00c1:
            r0 = move-exception
            goto L_0x00ab
        L_0x00c3:
            r0 = move-exception
            r3 = r2
            goto L_0x00ab
        L_0x00c6:
            r0 = move-exception
            r1 = r2
            goto L_0x003a
        L_0x00ca:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x003a
        L_0x00cf:
            r1 = r2
            r3 = r2
            goto L_0x0090
        */
        throw new UnsupportedOperationException("Method not decompiled: com.lthj.unipay.plugin.aq.run():void");
    }
}
