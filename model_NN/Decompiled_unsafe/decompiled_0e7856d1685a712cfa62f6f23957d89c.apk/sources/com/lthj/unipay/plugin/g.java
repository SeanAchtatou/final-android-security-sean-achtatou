package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.ui.YearAndMonthDialog;

public class g implements DialogInterface.OnDismissListener {
    final /* synthetic */ YearAndMonthDialog a;

    public g(YearAndMonthDialog yearAndMonthDialog) {
        this.a = yearAndMonthDialog;
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.a.e.setClickable(true);
    }
}
