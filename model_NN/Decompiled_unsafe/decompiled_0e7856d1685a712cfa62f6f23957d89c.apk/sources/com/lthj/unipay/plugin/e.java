package com.lthj.unipay.plugin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.PanBank;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class e extends BaseAdapter {
    final /* synthetic */ SupportCardActivity a;

    public e(SupportCardActivity supportCardActivity) {
        this.a = supportCardActivity;
    }

    public int getCount() {
        if (this.a.f == null) {
            return 0;
        }
        return this.a.f.size();
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View inflate = LayoutInflater.from(this.a).inflate(PluginLink.getLayoutupomp_lthj_supportcard_bankitem(), (ViewGroup) null);
        ((CheckBox) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_debit_item())).setChecked(true);
        ((CheckBox) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_credit_item())).setVisibility(8);
        ((TextView) inflate.findViewById(PluginLink.getIdupomp_lthj_sup_bank_item())).setText(((PanBank) this.a.f.get(i)).getPanBank());
        return inflate;
    }
}
