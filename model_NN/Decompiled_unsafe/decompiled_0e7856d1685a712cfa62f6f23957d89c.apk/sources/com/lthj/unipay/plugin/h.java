package com.lthj.unipay.plugin;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.a.a.g.c;

public class h {
    public String[] a = {"pluginSerialNo", "commonPayPhone"};
    public String[] b = {c.TEXT_MESSAGE, c.TEXT_MESSAGE};
    private y c;
    private String d = "pluginfo";

    public h(Context context) {
        this.c = new y(context, this.d, this.a, this.b);
    }

    public cu a(int i) {
        cu cuVar = null;
        Cursor a2 = this.c.a(i);
        if (a2 != null) {
            if (a2.moveToNext()) {
                cuVar = new cu();
                cuVar.a(i);
                String string = a2.getString(a2.getColumnIndex(this.a[0]));
                String string2 = a2.getString(a2.getColumnIndex(this.a[1]));
                cuVar.a(string);
                cuVar.b(string2);
            }
            if (a2 != null) {
                a2.close();
                this.c.a();
            }
        }
        return cuVar;
    }

    public boolean a(cu cuVar) {
        if (cuVar == null) {
            return false;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.a[0], cuVar.b());
        contentValues.put(this.a[1], cuVar.c());
        return this.c.a(contentValues);
    }

    public boolean b(cu cuVar) {
        boolean z;
        boolean z2 = true;
        ContentValues contentValues = new ContentValues();
        if (cuVar.b() != null) {
            contentValues.put(this.a[0], cuVar.b());
            z = true;
        } else {
            z = false;
        }
        if (cuVar.c() != null) {
            contentValues.put(this.a[1], cuVar.c());
        } else {
            z2 = z;
        }
        if (z2) {
            return this.c.a(cuVar.a(), contentValues);
        }
        return false;
    }
}
