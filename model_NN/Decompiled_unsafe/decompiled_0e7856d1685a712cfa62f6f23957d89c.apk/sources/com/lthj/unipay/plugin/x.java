package com.lthj.unipay.plugin;

import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class x implements View.OnFocusChangeListener, View.OnTouchListener {
    private int a;
    private d b;

    public x(int i) {
        this(i, null);
    }

    public x(int i, d dVar) {
        this.a = i;
        this.b = dVar;
    }

    public void onFocusChange(View view, boolean z) {
        EditText editText = (EditText) view;
        if (z) {
            ((InputMethodManager) editText.getContext().getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
            if (editText.isClickable()) {
                editText.setInputType(0);
                editText.setText("");
                editText.setClickable(false);
                i.a(editText.getContext(), editText, this.a);
            }
        } else if (this.b != null) {
            at.a("PasswordKeyboardListener", "validatapwd");
            this.b.a(this.a);
        }
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view instanceof EditText) {
            EditText editText = (EditText) view;
            editText.setInputType(0);
            if (motionEvent.getAction() == 1 && editText.isClickable()) {
                editText.setText("");
                editText.setClickable(false);
                i.a(editText.getContext(), editText, this.a);
            }
        }
        return false;
    }
}
