package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.model.GetBundleBankCardList;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class ek implements DialogInterface.OnClickListener {
    final /* synthetic */ PayActivity a;

    public ek(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        ap.a().B = (GetBundleBankCardList) ap.a().A.get(i);
        this.a.l.a(ap.a().B.panBank + "-" + i.b(ap.a().B.panType) + "-" + ap.a().B.pan.substring(ap.a().B.pan.length() - 4, ap.a().B.pan.length()));
        this.a.m.a(i.f(ap.a().B.mobileNumber));
    }
}
