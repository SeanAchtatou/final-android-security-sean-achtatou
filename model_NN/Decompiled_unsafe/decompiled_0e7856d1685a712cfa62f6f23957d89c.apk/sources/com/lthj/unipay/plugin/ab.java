package com.lthj.unipay.plugin;

import android.content.DialogInterface;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;
import com.unionpay.upomp.lthj.util.PluginHelper;

public class ab implements DialogInterface.OnClickListener {
    final /* synthetic */ SplashActivity a;

    public ab(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        i.c();
        synchronized (PluginHelper.a) {
            PluginHelper.a.notify();
        }
        this.a.finish();
    }
}
