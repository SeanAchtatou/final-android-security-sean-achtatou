package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class dl extends Handler {
    final /* synthetic */ PayActivity a;

    public dl(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void handleMessage(Message message) {
        String str = (String) message.obj;
        if (this.a.o == null) {
            return;
        }
        if (Integer.parseInt(str) < 0) {
            this.a.o.setEnabled(true);
            this.a.o.setText(this.a.getString(PluginLink.getStringupomp_lthj_click_get_mac()));
            return;
        }
        this.a.o.setEnabled(false);
        this.a.o.setText(str + this.a.getString(PluginLink.getStringupomp_lthj_after_getmobilemacAgain()));
    }
}
