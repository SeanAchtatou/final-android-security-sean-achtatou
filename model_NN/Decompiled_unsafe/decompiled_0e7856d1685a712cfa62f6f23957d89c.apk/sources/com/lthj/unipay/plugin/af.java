package com.lthj.unipay.plugin;

import android.os.Handler;
import android.os.Message;
import com.unionpay.upomp.lthj.plugin.ui.SplashActivity;

public class af extends Handler {
    final /* synthetic */ SplashActivity a;

    public af(SplashActivity splashActivity) {
        this.a = splashActivity;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        if (this.a.a >= 100) {
            this.a.a = 0;
            this.a.f.setProgress(this.a.a);
            return;
        }
        this.a.a += 5;
        this.a.f.incrementProgressBy(5);
    }
}
