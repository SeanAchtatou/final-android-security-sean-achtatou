package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.a.a.h.b;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import com.unionpay.upomp.lthj.plugin.ui.SupportCardActivity;

public class cf implements View.OnClickListener {
    final /* synthetic */ HomeActivity a;

    public cf(HomeActivity homeActivity) {
        this.a = homeActivity;
    }

    public void onClick(View view) {
        Intent intent = new Intent(this.a, SupportCardActivity.class);
        intent.putExtra("tranType", b.SMS_RESULT_SEND_FAIL);
        intent.addFlags(67108864);
        this.a.a().changeSubActivity(intent);
        k.a().b();
    }
}
