package com.lthj.unipay.plugin;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import com.a.a.h.b;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.HeadData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;

public class cl {
    public static void a(UIResponseListener uIResponseListener, Context context) {
        bw bwVar = new bw(8207);
        bwVar.a(HeadData.createHeadData("GetPanBankBindList.Req", context));
        bwVar.a(ap.a().x.toString());
        try {
            ck.a().a(bwVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str) {
        ai aiVar = new ai(8204);
        aiVar.a(HeadData.createHeadData("DefaultPanSet.Req", context));
        aiVar.a(ap.a().x.toString());
        aiVar.b(ap.a().y.toString());
        aiVar.c(str);
        try {
            ck.a().a(aiVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, bt btVar, String str2) {
        bz bzVar = new bz(8229);
        bzVar.a(HeadData.createHeadData("PanBankBindExpress.Req", context));
        bzVar.a(str);
        bzVar.b(ap.a().e);
        bzVar.c(ap.a().h);
        bzVar.d(ap.a().i);
        bzVar.e(ap.a().t.toString());
        bzVar.f(ap.a().w.toString());
        ap.a().w.setLength(0);
        if (!TextUtils.isEmpty(str2)) {
            bzVar.h(str2);
        }
        if (!(btVar == null || btVar.b() == null || btVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.b(), btVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.a(), btVar.a().length)));
            bzVar.g(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        try {
            ck.a().a(bzVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        l lVar = new l(8195);
        lVar.a(HeadData.createHeadData("UserResetPwd.Req", context));
        lVar.a(str);
        lVar.c(str2);
        lVar.b(z.a().d.toString());
        z.a().e.setLength(0);
        try {
            ck.a().a(lVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, bt btVar, String str4, boolean z) {
        q qVar = new q(8203);
        qVar.a(HeadData.createHeadData("PanBankBind.Req", context));
        qVar.a(ap.a().x.toString());
        qVar.d(str3);
        qVar.e(ap.a().w.toString());
        ap.a().w.setLength(0);
        qVar.b(str);
        qVar.c(str2);
        if (str4 != null) {
            qVar.g(str4);
        }
        if (!(btVar == null || btVar.b() == null || btVar.a() == null)) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.b(), btVar.b().length)));
            stringBuffer.append(new String(JniMethod.getJniMethod().decryptConfig(btVar.a(), btVar.a().length)));
            qVar.f(stringBuffer.toString());
            stringBuffer.delete(0, stringBuffer.length());
        }
        if (z) {
            qVar.h(b.SMS_RESULT_SEND_FAIL);
        } else {
            qVar.h(b.SMS_RESULT_SEND_SUCCESS);
        }
        try {
            ck.a().a(qVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        bb bbVar = new bb(8221);
        bbVar.a(HeadData.createHeadData("GetSecureQuestion.Req", context));
        bbVar.a(str);
        bbVar.c(str2);
        bbVar.b(str3);
        bbVar.d(str4);
        try {
            ck.a().a(bbVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, Context context, String str, String str2, boolean z) {
        db dbVar = new db(8224);
        dbVar.a(HeadData.createHeadData("GetBankInfo.Req", context));
        if (z) {
            dbVar.c(ap.a().g);
        }
        dbVar.a(str);
        dbVar.b(str2);
        try {
            ck.a().a(dbVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(UIResponseListener uIResponseListener, ProgressBar progressBar) {
        if (progressBar != null) {
            progressBar.setVisibility(0);
            p pVar = new p(8201);
            pVar.a(HeadData.createHeadData("GetVerifyCode.Req", progressBar.getContext()));
            try {
                ck.a().a(pVar, uIResponseListener, progressBar.getContext(), false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(String str, UIResponseListener uIResponseListener, Context context) {
        n nVar = new n(8202);
        nVar.a(HeadData.createHeadData("GetBankList.Req", context));
        nVar.a(str);
        try {
            ck.a().a(nVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(String str, String str2, String str3, UIResponseListener uIResponseListener, Context context, Button button) {
        if (c.a(context, str)) {
            button.setText(PluginLink.getStringupomp_lthj_getting_mac());
            button.setEnabled(false);
            m mVar = new m(8200);
            mVar.b(str2);
            mVar.a(HeadData.createHeadData("GetMobileMac.Req", context));
            mVar.a(str);
            mVar.j(str3);
            try {
                ck.a().a(mVar, uIResponseListener, context, false, true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str) {
        o oVar = new o(8196);
        oVar.a(HeadData.createHeadData("UserUpdatePwd.Req", context));
        oVar.a(ap.a().x.toString());
        oVar.d(ap.a().y.toString());
        oVar.e(str);
        oVar.b(z.a().c.toString());
        oVar.c(z.a().d.toString());
        z.a().e.setLength(0);
        try {
            ck.a().a(oVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        ej ejVar = new ej(8206);
        ejVar.a(HeadData.createHeadData("PanDelete.Req", context));
        ejVar.a(ap.a().x.toString());
        ejVar.b(ap.a().y.toString());
        ejVar.d(str);
        ejVar.c(str2);
        try {
            ck.a().a(ejVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void b(UIResponseListener uIResponseListener, Context context, String str, String str2, String str3, String str4) {
        cp cpVar = new cp(8225);
        cpVar.a(HeadData.createHeadData("UserRegisterExpress.Req", context));
        cpVar.a(str);
        cpVar.e(str2);
        cpVar.b(z.a().d.toString());
        cpVar.f(str3);
        cpVar.g(str4);
        cpVar.c(ap.a().e);
        cpVar.d(ap.a().h);
        cpVar.h(ap.a().i);
        try {
            ck.a().a(cpVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str) {
        ao aoVar = new ao(8228);
        aoVar.a(HeadData.createHeadData("GetBanksService.Req", context));
        aoVar.a(str);
        try {
            ck.a().a(aoVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void c(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        ar arVar = new ar(8197);
        arVar.a(HeadData.createHeadData("UpdateMobileNumber.Req", context));
        arVar.a(ap.a().x.toString());
        arVar.b(ap.a().y.toString());
        arVar.c(str);
        arVar.d(str2);
        arVar.e(ap.a().z.toString());
        try {
            ck.a().a(arVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void d(UIResponseListener uIResponseListener, Context context, String str, String str2) {
        cm cmVar = new cm(8194);
        cmVar.a(HeadData.createHeadData("UserLogin.Req", context));
        cmVar.a(str);
        cmVar.b(ap.a().z.toString());
        cmVar.c(str2);
        try {
            ck.a().a(cmVar, uIResponseListener, context, true, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
