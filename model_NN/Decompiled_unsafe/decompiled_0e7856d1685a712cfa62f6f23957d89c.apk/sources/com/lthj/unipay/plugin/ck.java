package com.lthj.unipay.plugin;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.a.a.h.b;
import com.unionpay.upomp.lthj.plugin.model.Data;
import com.unionpay.upomp.lthj.plugin.model.JNIBottomBackData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;
import java.util.Queue;

public class ck implements cz {
    private static ck b;
    protected Context a;
    /* access modifiers changed from: private */
    public UIResponseListener c;
    /* access modifiers changed from: private */
    public w d;
    /* access modifiers changed from: private */
    public Queue e;
    /* access modifiers changed from: private */
    public aq f;
    private Handler g = new cn(this);
    private Handler h = new cr(this);

    public static ck a() {
        if (b == null) {
            b = new ck();
        }
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.lthj.unipay.plugin.i.a(android.content.Context, boolean):boolean
     arg types: [android.content.Context, int]
     candidates:
      com.lthj.unipay.plugin.i.a(android.content.Context, java.lang.String):void
      com.lthj.unipay.plugin.i.a(java.lang.String, char):java.lang.String[]
      com.lthj.unipay.plugin.i.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: private */
    public void a(w wVar) {
        if ("0000".equals(wVar.m())) {
            switch (wVar.e()) {
                case 8194:
                case 8195:
                case 8225:
                    if (this.a != null) {
                        i.a(this.a, true);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    public synchronized void a(ca caVar) {
        JNIBottomBackData jNIBottomBackData;
        this.c = caVar.b();
        this.d = caVar.a();
        if (this.d != null) {
            try {
                jNIBottomBackData = JniMethod.getJniMethod().packAndEncrypt(this.d.b(), this.d.e());
            } catch (Exception e2) {
                e2.printStackTrace();
                jNIBottomBackData = null;
            }
            if (jNIBottomBackData != null) {
                if (jNIBottomBackData.stateCode == 0) {
                    "---the pack data from bottom-=" + new String(jNIBottomBackData.sendData);
                    this.f = new aq(this.a);
                    if (this.f != null) {
                        this.f.b();
                        this.f.a(jNIBottomBackData.sendData, 0, jNIBottomBackData.sendData.length, this, 120000);
                    }
                }
            }
            this.c.errorCallBack("打包出错！");
        }
    }

    public synchronized void a(w wVar, UIResponseListener uIResponseListener, Context context, boolean z, boolean z2) {
        this.a = context;
        if (z) {
            k.a().a(context, z2, new as(this));
        }
        if (this.e != null) {
            this.e.clear();
            this.e = null;
        }
        a(new ca(wVar, uIResponseListener));
    }

    public void a(String str, aq aqVar) {
        if (this.f == aqVar) {
            try {
                Message obtain = Message.obtain();
                obtain.obj = str;
                this.g.sendMessage(obtain);
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
    }

    public synchronized void a(byte[] bArr, int i, int i2) {
        try {
            Data unpackAndDecrypt = JniMethod.getJniMethod().unpackAndDecrypt(bArr, i2, this.d.e());
            if (unpackAndDecrypt != null && unpackAndDecrypt.stateCode == 0) {
                if (this.d != null) {
                    this.d.a(unpackAndDecrypt);
                    if (!b.SMS_RESULT_SEND_SUCCESS.equals(this.d.m())) {
                        "theCmd respCode=" + this.d.m() + ",theCmd.respDesc=" + this.d.n();
                    }
                }
                Message obtain = Message.obtain();
                obtain.obj = this.d;
                this.h.sendMessage(obtain);
            } else if (unpackAndDecrypt.respCode == null && unpackAndDecrypt.respDesc == null) {
                a("命令处理失败，错误码为" + unpackAndDecrypt.stateCode, this.f);
            } else {
                a(unpackAndDecrypt.respDesc + "，错误码为" + unpackAndDecrypt.respCode, this.f);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            "--quotMediator--response=" + e2;
        }
        return;
    }

    public synchronized void a(byte[] bArr, aq aqVar) {
        if (this.f == aqVar) {
            a(bArr, 0, bArr.length);
        }
    }
}
