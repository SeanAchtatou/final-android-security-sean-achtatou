package com.lthj.unipay.plugin;

import android.view.View;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class dh implements View.OnClickListener {
    final /* synthetic */ PayActivity a;

    public dh(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(View view) {
        View findViewById = this.a.findViewById(PluginLink.getIdupomp_lthj_orderno_row());
        View findViewById2 = this.a.findViewById(PluginLink.getIdupomp_lthj_trantime_row());
        if (findViewById.getVisibility() == 8) {
            findViewById.setVisibility(0);
            findViewById2.setVisibility(0);
            view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_up_btn());
            return;
        }
        findViewById.setVisibility(8);
        findViewById2.setVisibility(8);
        view.setBackgroundResource(PluginLink.getDrawableupomp_lthj_info_down_btn());
    }
}
