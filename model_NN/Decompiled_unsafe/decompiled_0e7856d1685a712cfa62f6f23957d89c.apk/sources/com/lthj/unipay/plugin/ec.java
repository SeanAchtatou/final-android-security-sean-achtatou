package com.lthj.unipay.plugin;

public class ec {
    static final /* synthetic */ boolean a = (!ec.class.desiredAssertionStatus());

    private ec() {
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        ak akVar = new ak(i3, new byte[((i2 * 3) / 4)]);
        if (!akVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (akVar.b == akVar.a.length) {
            return akVar.a;
        } else {
            byte[] bArr2 = new byte[akVar.b];
            System.arraycopy(akVar.a, 0, bArr2, 0, akVar.b);
            return bArr2;
        }
    }
}
