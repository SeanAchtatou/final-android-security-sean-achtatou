package com.lthj.unipay.plugin;

import com.unionpay.upomp.lthj.plugin.ui.UIResponseListener;

public class ca {
    private w a;
    private UIResponseListener b;

    public ca(w wVar, UIResponseListener uIResponseListener) {
        this.a = wVar;
        this.b = uIResponseListener;
    }

    public w a() {
        return this.a;
    }

    public UIResponseListener b() {
        return this.b;
    }
}
