package com.lthj.unipay.plugin;

import android.content.Intent;
import android.view.View;
import com.unionpay.upomp.lthj.plugin.ui.HomeActivity;
import com.unionpay.upomp.lthj.plugin.ui.PayActivity;

public class ea implements View.OnClickListener {
    final /* synthetic */ PayActivity a;

    public ea(PayActivity payActivity) {
        this.a = payActivity;
    }

    public void onClick(View view) {
        this.a.a().changeSubActivity(new Intent(this.a, HomeActivity.class));
        ap.a().c();
        k.a().b();
    }
}
