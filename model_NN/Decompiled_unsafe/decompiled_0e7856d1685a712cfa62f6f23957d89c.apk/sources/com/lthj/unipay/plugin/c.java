package com.lthj.unipay.plugin;

import android.content.Context;
import android.text.TextUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import com.unionpay.upomp.lthj.link.PluginLink;
import com.unionpay.upomp.lthj.plugin.model.SmsCodeVerfiyData;
import com.unionpay.upomp.lthj.plugin.ui.JniMethod;
import com.unionpay.upomp.lthj.widget.ValidateCodeView;
import java.util.regex.Pattern;

public class c {
    public static boolean a(Context context) {
        if (new String(i.b(z.a().d.toString().getBytes())).equals(new String(i.b(z.a().e.toString().getBytes())))) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePassWord_Same_prompt()));
        return false;
    }

    public static boolean a(Context context, CheckBox checkBox) {
        if (checkBox.isChecked()) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateUser_Protocal_prompt()));
        return false;
    }

    public static boolean a(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileMac_Empty_prompt()));
            return false;
        } else if (trim.length() == 6) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileMac_Length_prompt()));
            return false;
        }
    }

    public static boolean a(Context context, EditText editText, String str) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim) || trim.length() != 11) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_LengthEmpty_prompt()));
            return false;
        }
        if (trim.contains("*")) {
            if (!Pattern.matches("^1\\d{2}\\*{5}\\d{3}$", trim)) {
                i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_FromatError_prompt()));
                return false;
            } else if (!trim.substring(0, 3).equals(str.substring(0, 3)) || !trim.substring(8, 11).equals(str.substring(str.length() - 3, str.length()))) {
                i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_FromatError_prompt()));
                return false;
            }
        } else if (!TextUtils.isDigitsOnly(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_FromatError_prompt()));
            return false;
        }
        return true;
    }

    public static boolean a(Context context, SmsCodeVerfiyData smsCodeVerfiyData) {
        if (smsCodeVerfiyData == null) {
            return false;
        }
        String str = smsCodeVerfiyData.smsCode;
        if (TextUtils.isEmpty(str)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileMac_Empty_prompt()));
            return false;
        } else if (str.length() != 6) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileMac_Length_prompt()));
            return false;
        } else if (JniMethod.getJniMethod().validateSMSCode(smsCodeVerfiyData) == 0) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileMac_Same_prompt()));
            return false;
        }
    }

    public static boolean a(Context context, ValidateCodeView validateCodeView) {
        if (validateCodeView == null || validateCodeView.getVisibility() != 0) {
            return true;
        }
        String trim = validateCodeView.d().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateImageCode_Empty_prompt()));
            return false;
        } else if (!TextUtils.isDigitsOnly(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateImageCode_Error_prompt()));
            return false;
        } else if (trim.length() == 4) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateImageCode_Error_prompt()));
            return false;
        }
    }

    public static boolean a(Context context, String str) {
        String trim = str.trim();
        if (!TextUtils.isEmpty(trim) && trim.length() == 11) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_LengthEmpty_prompt()));
        return false;
    }

    public static boolean b(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim) || trim.length() != 11) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_LengthEmpty_prompt()));
            return false;
        } else if (TextUtils.isDigitsOnly(trim)) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateMobileNum_FromatError_prompt()));
            return false;
        }
    }

    public static boolean b(Context context, EditText editText, String str) {
        String replace = editText.getText().toString().replace(" ", "");
        if (TextUtils.isEmpty(replace)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Empty_prompt()));
            return false;
        } else if (replace.length() < 16 || replace.length() > 19) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Format_prompt()));
            return false;
        } else {
            if (replace.contains("*")) {
                if (!Pattern.matches("^\\d{4}\\*{8}\\d{4,7}$", replace)) {
                    i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Format_prompt()));
                    return false;
                } else if (!replace.substring(0, 4).equals(str.substring(0, 4)) || !replace.substring(12, replace.length()).equals(str.substring(12, str.length()))) {
                    i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Format_prompt()));
                    return false;
                }
            } else if (!TextUtils.isDigitsOnly(replace)) {
                i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Format_prompt()));
                return false;
            }
            return true;
        }
    }

    public static boolean c(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateUserName_Empty_prompt()));
            return false;
        }
        int length = trim.length();
        if (length >= 6 && length <= 20) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateUserName_Length_prompt()));
        return false;
    }

    public static boolean d(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePassWord_Empty_prompt()));
            return false;
        }
        int length = trim.length();
        if (length >= 6 && length <= 12) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePassWord_Length_prompt()));
        return false;
    }

    public static boolean e(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateOldPassWord_Empty_prompt()));
            return false;
        }
        int length = trim.length();
        if (length >= 6 && length <= 12) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateOldPassWord_Length_prompt()));
        return false;
    }

    public static boolean f(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateNewPassWord_Empty_prompt()));
            return false;
        }
        int length = trim.length();
        if (length >= 6 && length <= 12) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateNewPassWord_Length_prompt()));
        return false;
    }

    public static boolean g(Context context, EditText editText) {
        if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateWelcomeWord_Empty_prompt()));
        return false;
    }

    public static boolean h(Context context, EditText editText) {
        if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateSafeQuestion_Empty_prompt()));
        return false;
    }

    public static boolean i(Context context, EditText editText) {
        if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateSafeAnswer_Empty_prompt()));
        return false;
    }

    public static boolean j(Context context, EditText editText) {
        if (!TextUtils.isEmpty(editText.getText().toString().trim())) {
            return true;
        }
        i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateExpiryMonth_Empty_prompt()));
        return false;
    }

    public static boolean k(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateCVN2_Empty_prompt()));
            return false;
        } else if (trim.length() == 3) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validateCVN2_Format_prompt()));
            return false;
        }
    }

    public static boolean l(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Empty_prompt()));
            return false;
        } else if (trim.length() >= 19 && trim.length() <= 23) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePan_Format_prompt()));
            return false;
        }
    }

    public static boolean m(Context context, EditText editText) {
        String trim = editText.getText().toString().trim();
        if (TextUtils.isEmpty(trim)) {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePanPassword_Empty_prompt()));
            return false;
        } else if (trim.length() == 6) {
            return true;
        } else {
            i.b(context, context.getString(PluginLink.getStringupomp_lthj_validatePanPassword_Length_prompt()));
            return false;
        }
    }
}
