package mm.purchasesdk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class c extends Handler {
    final /* synthetic */ Purchase iv;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    c(Purchase purchase, Looper looper) {
        super(looper);
        this.iv = purchase;
    }

    public void handleMessage(Message message) {
        int i = message.arg1;
        b bVar = (b) message.obj;
        switch (message.what) {
            case 0:
                e.a(this.iv.hL, this.iv.b, bVar, i);
                break;
            case 1:
                e.a(bVar, i);
                break;
            case 2:
                e.b(this.iv.hL, this.iv.b, bVar, i);
                break;
            case 4:
                e.b();
                break;
            case 5:
                bVar.a();
                break;
        }
        super.handleMessage(message);
    }
}
