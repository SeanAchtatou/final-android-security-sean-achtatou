package mm.purchasesdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.purchasesdk.k.e;

class g extends Handler {
    final /* synthetic */ f jl;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    g(f fVar, Looper looper) {
        super(looper);
        this.jl = fVar;
    }

    public void handleMessage(Message message) {
        b bVar = (b) message.obj;
        e.c("TaskThread", "ReqHandler Handler id:" + Thread.currentThread().getId());
        e.c("TaskThread", "ReqHandler Handler name:" + Thread.currentThread().getName());
        boolean unused = f.c = false;
        switch (message.what) {
            case 0:
                f.a(this.jl, bVar);
                break;
            case 1:
                f.b(this.jl, bVar);
                break;
            case 2:
                int i = message.arg1;
                Bundle data = message.getData();
                if (i != 0) {
                    if (i != 2) {
                        boolean unused2 = f.c = true;
                        f.a(this.jl, bVar, data);
                        break;
                    } else {
                        f.b(this.jl, bVar);
                        break;
                    }
                } else {
                    f.c(this.jl, bVar);
                    break;
                }
            case 3:
                f.d(this.jl, bVar);
                break;
            case 4:
                f.e(this.jl, bVar);
                break;
        }
        super.handleMessage(message);
    }
}
