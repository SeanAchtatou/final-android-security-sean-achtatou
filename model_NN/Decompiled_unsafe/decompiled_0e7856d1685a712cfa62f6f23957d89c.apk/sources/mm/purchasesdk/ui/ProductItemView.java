package mm.purchasesdk.ui;

import android.content.Context;
import android.widget.TextView;

public class ProductItemView extends TextView {
    public ProductItemView(Context context) {
        super(context);
    }

    public boolean isFocused() {
        return true;
    }
}
