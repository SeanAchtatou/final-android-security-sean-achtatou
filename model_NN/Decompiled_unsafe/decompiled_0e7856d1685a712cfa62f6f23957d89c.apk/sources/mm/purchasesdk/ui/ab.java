package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class ab implements View.OnClickListener {
    final /* synthetic */ aa kp;

    ab(aa aaVar) {
        this.kp = aaVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("WebViewLayout", "Activity is finished!");
            return;
        }
        u.cG().J();
        int id = view.getId();
        Message obtainMessage = this.kp.hL.obtainMessage();
        switch (id) {
            case 1:
                e.c("WebViewLayout", "onClick KAlipayBackButtonType");
                this.kp.kk.b(1);
                break;
            case 2:
                e.c("WebViewLayout", "onClick KAlipayFinishButtonType");
                this.kp.kk.b(2);
                break;
        }
        this.kp.kk.a(this.kp.al);
        this.kp.kk.a(this.kp.io);
        d.e(true);
        obtainMessage.what = 2;
        obtainMessage.arg1 = 2;
        obtainMessage.obj = this.kp.kk;
        obtainMessage.sendToTarget();
    }
}
