package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;

class n implements View.OnTouchListener {
    final /* synthetic */ l lq;

    n(l lVar) {
        this.lq = lVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        if (this.lq.lb == null) {
            d unused = this.lq.lb = new d(this.lq.la, true);
        }
        try {
            this.lq.lb.a(this.lq.iO);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.lq.la.setVisibility(8);
        switch (view.getId()) {
            case 0:
                if (this.lq.d != null) {
                    this.lq.d.clearFocus();
                }
                if (this.lq.c == null) {
                    return false;
                }
                this.lq.c.requestFocus();
                return false;
            case 1:
                if (this.lq.d != null) {
                    this.lq.d.requestFocus();
                }
                if (this.lq.c == null) {
                    return false;
                }
                this.lq.c.clearFocus();
                return false;
            case 2:
                if (this.lq.f10e != null) {
                    this.lq.f10e.requestFocus();
                }
                if (this.lq.f10e == null) {
                    return false;
                }
                this.lq.f10e.clearFocus();
                return false;
            default:
                return false;
        }
    }
}
