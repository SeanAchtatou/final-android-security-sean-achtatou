package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import mm.purchasesdk.b;
import mm.purchasesdk.b.a;
import mm.purchasesdk.k.d;

public class l extends b {
    private final String TAG = "PurchaseDialog";
    /* access modifiers changed from: private */
    public Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f8b;
    /* access modifiers changed from: private */
    public EditText c;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with other field name */
    public TextView f9c;
    /* access modifiers changed from: private */
    public EditText d;
    private Button e;
    /* access modifiers changed from: private */

    /* renamed from: e  reason: collision with other field name */
    public EditText f10e;
    /* access modifiers changed from: private */
    public Handler hL;
    /* access modifiers changed from: private */
    public b iA;
    /* access modifiers changed from: private */
    public LinearLayout iO;
    private View.OnTouchListener iQ = new n(this);
    private Boolean iq = true;
    private Boolean iw = false;
    private ScrollView ix;
    private Drawable iy;
    private boolean j = true;
    private Bitmap kU;
    /* access modifiers changed from: private */
    public ImageView kV;
    /* access modifiers changed from: private */
    public ProgressBar kW;
    private TextView kX;
    private TextView kY;
    /* access modifiers changed from: private */
    public a kZ;
    /* access modifiers changed from: private */
    public View la;
    /* access modifiers changed from: private */
    public d lb = null;
    /* access modifiers changed from: private */
    public Drawable lc = null;
    /* access modifiers changed from: private */
    public Drawable ld = null;
    private Drawable le = null;
    private Drawable lf = null;
    private Drawable lg = null;
    private Drawable lh = null;
    private Drawable li = null;
    private Bitmap lj = null;
    private View.OnFocusChangeListener lk = new m(this);
    public View.OnKeyListener ll = new o(this);
    private View.OnClickListener lm = new p(this);
    private Drawable ln;
    private View.OnClickListener lo = new q(this);
    private View.OnClickListener lp = new t(this);

    public l(Context context, Handler handler, Handler handler2, b bVar, a aVar) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        this.iA = bVar;
        this.b = handler2;
        this.hL = handler;
        if (context == null || ((Activity) context) != getOwnerActivity()) {
            d.setContext(getOwnerActivity());
        }
        b(aVar);
        this.e = new Button(context);
    }

    private static View a(z zVar) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(0);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setLayoutParams(layoutParams2);
        textView.setText(zVar.bM);
        textView.setTextColor(zVar.lZ);
        textView.setTextSize(y.lW);
        linearLayout.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setTextSize(y.lW);
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setText(zVar.mValue);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setTextColor(zVar.ma);
        linearLayout.addView(productItemView);
        return linearLayout;
    }

    private static View a(z zVar, z zVar2) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setLayoutParams(layoutParams);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout2.setOrientation(0);
        linearLayout2.setLayoutParams(layoutParams2);
        TextView textView = new TextView(d.getContext());
        textView.setText(zVar.bM);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.rightMargin = 3;
        textView.setLayoutParams(layoutParams3);
        textView.setTextColor(zVar.lZ);
        textView.setTextSize(y.lW);
        linearLayout2.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setTextSize(y.lW);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setText(zVar.mValue);
        productItemView.setTextColor(zVar.ma);
        linearLayout2.addView(productItemView);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout3.setOrientation(0);
        linearLayout3.setLayoutParams(layoutParams4);
        TextView textView2 = new TextView(d.getContext());
        textView2.setLayoutParams(layoutParams3);
        textView2.setText(zVar2.bM);
        textView2.setTextColor(zVar2.lZ);
        textView2.setTextSize(y.lW);
        linearLayout3.addView(textView2);
        ProductItemView productItemView2 = new ProductItemView(d.getContext());
        productItemView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView2.setTextSize(y.lW);
        productItemView2.setSingleLine(true);
        productItemView2.setSingleLine();
        productItemView2.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView2.setMarqueeRepeatLimit(-1);
        productItemView2.setText(zVar2.mValue);
        productItemView2.setTextColor(zVar2.ma);
        linearLayout3.addView(productItemView2);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    private static LinearLayout a(z zVar, LinearLayout.LayoutParams layoutParams) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -2, 1.0f);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(layoutParams2);
        TextView textView = new TextView(d.getContext());
        textView.setText(zVar.bM);
        textView.setLayoutParams(layoutParams);
        textView.setTextColor(zVar.lZ);
        textView.setTextSize(y.lW);
        linearLayout.addView(textView);
        ProductItemView productItemView = new ProductItemView(d.getContext());
        productItemView.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        productItemView.setTextSize(y.lW);
        productItemView.setSingleLine(true);
        productItemView.setText(zVar.mValue);
        productItemView.setSingleLine();
        productItemView.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        productItemView.setMarqueeRepeatLimit(-1);
        productItemView.setTextColor(zVar.ma);
        linearLayout.addView(productItemView);
        return linearLayout;
    }

    private ImageView c(Bitmap bitmap) {
        ImageView imageView = new ImageView(d.getContext());
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        if (bitmap != null) {
            if (this.ln == null) {
                this.ln = new BitmapDrawable(bitmap);
            }
            imageView.setBackgroundDrawable(this.ln);
        }
        return imageView;
    }

    private View cA() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setBackgroundDrawable(this.lh);
        linearLayout.setGravity(16);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setPadding(10, 5, 10, 5);
        textView.setLayoutParams(layoutParams2);
        textView.setText("支付密码:");
        textView.setTextColor(-8289919);
        textView.setTextSize(y.lW);
        linearLayout.addView(textView);
        this.d = new EditText(d.getContext());
        this.d.setId(1);
        LinearLayout.LayoutParams layoutParams3 = d.jQ < 1.0f ? new LinearLayout.LayoutParams(-1, 30) : new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.rightMargin = 4;
        this.d.setLayoutParams(layoutParams3);
        this.d.setOnFocusChangeListener(this.lk);
        this.d.setOnKeyListener(this.ll);
        this.d.setOnTouchListener(this.iQ);
        this.d.setCursorVisible(true);
        this.d.setSingleLine();
        this.d.setFocusableInTouchMode(true);
        this.d.setTransformationMethod(PasswordTransformationMethod.getInstance());
        this.d.setBackgroundDrawable(this.ld);
        this.d.setHint("请输入支付密码");
        this.d.setInputType(0);
        linearLayout.addView(this.d);
        return linearLayout;
    }

    private View cB() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 5, 10, 5);
        linearLayout.setBackgroundDrawable(this.le);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        linearLayout2.setPadding(10, 5, 10, 5);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setGravity(16);
        TextView textView = new TextView(d.getContext());
        textView.setText("手机验证码:");
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams3.rightMargin = 6;
        layoutParams3.gravity = 16;
        textView.setLayoutParams(layoutParams3);
        textView.setTextColor(-8289919);
        textView.setTextSize(y.lW);
        linearLayout2.addView(textView);
        linearLayout.addView(linearLayout2);
        this.f10e = new EditText(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = d.jQ < 1.0f ? new LinearLayout.LayoutParams(-2, 30) : new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.gravity = 16;
        this.f10e.setLayoutParams(layoutParams4);
        this.f10e.setOnKeyListener(this.ll);
        this.f10e.setOnFocusChangeListener(this.lk);
        this.f10e.setOnTouchListener(this.iQ);
        this.f10e.setCursorVisible(true);
        this.f10e.setFocusableInTouchMode(true);
        this.f10e.setBackgroundDrawable(this.ld);
        this.f10e.setId(2);
        this.f10e.setHint("验证码答案");
        this.f10e.setInputType(0);
        linearLayout2.addView(this.f10e);
        this.kY = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams5.gravity = 16;
        this.kY.setLayoutParams(layoutParams5);
        this.kY.setPadding(1, 1, 1, 1);
        this.kY.setText(Html.fromHtml("<u>获取验证码</u>"));
        this.kY.setTextSize(y.lW);
        this.kY.setTextColor(-16776961);
        this.kY.setOnClickListener(new s(this));
        linearLayout2.addView(this.kY);
        return linearLayout;
    }

    private View cC() {
        View view;
        int i;
        int i2 = 0;
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 0, 5, 0);
        linearLayout.setBackgroundDrawable(this.lf);
        Bitmap n = y.n(d.getContext(), "mmiap/image/vertical/infoline.png");
        HashMap bx = this.kZ.bv().bx();
        ArrayList a = this.kZ.bv().a();
        int size = bx.size();
        while (i2 < size) {
            z zVar = (z) bx.get((String) a.get(i2));
            if (i2 == 0) {
                int i3 = i2 + 1;
                view = a(zVar, (z) bx.get((String) a.get(i3)));
                i = i3;
            } else if (i2 == size - 1) {
                view = a(zVar);
                i = i2;
            } else if (size - i2 > 0) {
                switch ((size - i2) - 1) {
                    case 1:
                        view = a(zVar);
                        i = i2;
                        break;
                    case 2:
                        int i4 = i2 + 1;
                        view = a(zVar, (z) bx.get((String) a.get(i4)));
                        i = i4;
                        break;
                    default:
                        int i5 = i2 + 1;
                        int i6 = i5 + 1;
                        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
                        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
                        linearLayout2.setPadding(10, 5, 10, 5);
                        linearLayout2.setLayoutParams(layoutParams2);
                        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
                        linearLayout2.addView(a(zVar, layoutParams3));
                        linearLayout2.addView(a((z) bx.get((String) a.get(i5)), layoutParams3));
                        linearLayout2.addView(a((z) bx.get((String) a.get(i6)), layoutParams3));
                        view = linearLayout2;
                        i = i6;
                        break;
                }
            } else {
                view = null;
                i = i2;
            }
            linearLayout.addView(view);
            if (i != size - 1) {
                linearLayout.addView(c(n));
            }
            i2 = i + 1;
        }
        return linearLayout;
    }

    /* access modifiers changed from: private */
    public static int cD() {
        String cb = d.cb();
        if (cb != null && cb.trim().length() > 0) {
            String substring = cb.substring(0, 3);
            if (substring.equals("101")) {
                return 1;
            }
            if (substring.equals("110")) {
                return 0;
            }
            if (substring.equals("111")) {
                return 2;
            }
        }
        return -1;
    }

    private View cE() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        if (1 == cD()) {
            layoutParams.topMargin = y.lO;
            layoutParams.leftMargin = 5;
            layoutParams.rightMargin = 5;
            linearLayout.setPadding(10, 0, 10, 0);
            linearLayout.setBackgroundDrawable(this.li);
        }
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        if (this.lj == null) {
            this.lj = y.n(d.getContext(), "mmiap/image/vertical/icon_chifubao.png");
        }
        if (this.lj != null) {
            ImageView imageView = new ImageView(d.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.leftMargin = 10;
            layoutParams2.topMargin = 10;
            layoutParams2.bottomMargin = 10;
            imageView.setLayoutParams(layoutParams2);
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setImageBitmap(this.lj);
            if (2 == cD()) {
                imageView.setOnClickListener(this.lp);
            }
            linearLayout.addView(imageView);
        }
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.gravity = 16;
        layoutParams3.leftMargin = 5;
        linearLayout2.setLayoutParams(layoutParams3);
        linearLayout2.setOrientation(1);
        linearLayout2.setPadding(5, 0, 5, 0);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.leftMargin = 3;
        layoutParams4.topMargin = 5;
        textView.setLayoutParams(layoutParams4);
        textView.setText("支付宝页面支付");
        textView.setTextColor(-16753749);
        textView.setTextSize(y.lX);
        if (2 == cD()) {
            textView.setOnClickListener(this.lp);
        }
        linearLayout2.addView(textView);
        TextView textView2 = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams5.leftMargin = 3;
        textView2.setLayoutParams(layoutParams5);
        textView2.setText("支持银行卡支付，无需登录");
        textView2.setTextColor(-9671572);
        textView2.setTextSize(y.lW);
        linearLayout2.addView(textView2);
        linearLayout.addView(linearLayout2);
        return linearLayout;
    }

    private View cF() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(10, 0, 10, 0);
        linearLayout.setBackgroundDrawable(this.li);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.leftMargin = 10;
        layoutParams2.topMargin = 5;
        layoutParams2.bottomMargin = 5;
        textView.setLayoutParams(layoutParams2);
        textView.setText("您还可以选择：");
        textView.setTextColor(-9671572);
        textView.setTextSize(y.lW);
        linearLayout.addView(textView);
        linearLayout.addView(c(y.n(d.getContext(), "mmiap/image/vertical/infoline.png")));
        linearLayout.addView(cE());
        return linearLayout;
    }

    private View cx() {
        View a;
        int i = 0;
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(5, 0, 5, 0);
        linearLayout.setBackgroundDrawable(this.lf);
        Bitmap n = y.n(d.getContext(), "mmiap/image/vertical/infoline.png");
        HashMap bx = this.kZ.bv().bx();
        ArrayList a2 = this.kZ.bv().a();
        int size = bx.size();
        while (i < size) {
            z zVar = (z) bx.get((String) a2.get(i));
            if (i == 0) {
                a = a(zVar);
            } else if (i == size - 1) {
                a = a(zVar);
            } else if (i == size - 2) {
                a = a(zVar);
            } else {
                int i2 = i + 1;
                a = a(zVar, (z) bx.get((String) a2.get(i2)));
                i = i2;
            }
            linearLayout.addView(a);
            if (i != size - 1) {
                linearLayout.addView(c(n));
            }
            i++;
        }
        return linearLayout;
    }

    private View cy() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(16);
        linearLayout.setBackgroundDrawable(this.lg);
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.rightMargin = 3;
        textView.setPadding(10, 10, 10, 10);
        textView.setLayoutParams(layoutParams2);
        textView.setText("手机号码:");
        textView.setTextColor(-8289919);
        textView.setTextSize(y.lW);
        linearLayout.addView(textView);
        TextView textView2 = new TextView(d.getContext());
        textView2.setTextSize(y.lW);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        textView2.setText(this.kZ.bv().o());
        textView2.setTextColor(-16777216);
        linearLayout.addView(textView2);
        return linearLayout;
    }

    private View l(String str, String str2) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = 10;
        layoutParams.bottomMargin = 10;
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(a.d(-1, 10, 10));
        linearLayout.setPadding(6, 6, 6, 6);
        TextView textView = new TextView(d.getContext());
        if (str2 == null || str2.trim().length() == 0) {
            textView.setText(str);
        } else {
            textView.setText(Html.fromHtml("<a href=\"" + this.kZ.d() + "\">" + str + "</a> "));
            textView.setMovementMethod(LinkMovementMethod.getInstance());
        }
        textView.setLineSpacing(1.0f, 1.3f);
        textView.setTextSize(y.lW);
        textView.setTextColor(-8289919);
        linearLayout.addView(textView);
        return linearLayout;
    }

    private void n() {
        this.kX.setOnClickListener(new r(this));
    }

    public final void b(a aVar) {
        this.kZ = aVar;
        this.j = aVar.b();
        this.kU = aVar.bw();
        this.iq = aVar.bs();
    }

    public final void c(a aVar) {
        this.kZ = aVar;
        this.kU = this.kZ.bw();
        if (this.kZ == null || this.kZ.bw() == null) {
            this.kW.setVisibility(8);
            this.f9c.setVisibility(0);
            this.kV.setVisibility(8);
            return;
        }
        this.kV.setImageBitmap(this.kU);
        this.kW.setVisibility(8);
        this.f9c.setVisibility(8);
        this.kV.setVisibility(0);
    }

    public final String cz() {
        return this.c == null ? "" : this.c.getText().toString();
    }

    public void dismiss() {
        super.dismiss();
    }

    public final String getPassword() {
        return this.d == null ? "" : this.d.getText().toString();
    }

    public final String m() {
        return this.f10e != null ? this.f10e.getText().toString() : "";
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void show() {
        ScrollView scrollView;
        Bitmap n;
        Bitmap n2;
        Bitmap n3;
        Bitmap n4;
        Bitmap n5;
        Bitmap n6;
        Bitmap n7;
        Bitmap n8;
        if (this.lc == null && (n8 = y.n(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk = n8.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.lc = new NinePatchDrawable(n8, ninePatchChunk, new Rect(), null);
        }
        if (this.ld == null && (n7 = y.n(d.getContext(), "mmiap/image/vertical/editbg_a.9.png")) != null) {
            byte[] ninePatchChunk2 = n7.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.ld = new NinePatchDrawable(n7, ninePatchChunk2, new Rect(), null);
        }
        if (this.le == null && (n6 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk3 = n6.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk3);
            this.le = new NinePatchDrawable(n6, ninePatchChunk3, new Rect(), null);
        }
        if (this.lg == null && (n5 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk4 = n5.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk4);
            this.lg = new NinePatchDrawable(n5, ninePatchChunk4, new Rect(), null);
        }
        if (this.lh == null && (n4 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk5 = n4.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk5);
            this.lh = new NinePatchDrawable(n4, ninePatchChunk5, new Rect(), null);
        }
        if (this.lf == null && (n3 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk6 = n3.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk6);
            this.lf = new NinePatchDrawable(n3, ninePatchChunk6, new Rect(), null);
        }
        if (this.iy == null && (n2 = y.n(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.iy = new BitmapDrawable(n2);
        }
        if (this.li == null && (n = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk7 = n.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk7);
            this.li = new NinePatchDrawable(n, ninePatchChunk7, new Rect(), null);
        }
        this.iw = y.iw;
        if (this.iw.booleanValue()) {
            this.iO = new LinearLayout(d.getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            this.iO.setOrientation(1);
            this.iO.setLayoutParams(layoutParams);
            if (1 != cD()) {
                this.iO.addView(K(d.getContext()));
                LinearLayout linearLayout = this.iO;
                Context context = d.getContext();
                ImageView imageView = this.f8b;
                linearLayout.addView(b(context, this.lm));
            } else {
                LinearLayout linearLayout2 = this.iO;
                Context context2 = d.getContext();
                ImageView imageView2 = this.f8b;
                linearLayout2.addView(a(context2, this.lm));
            }
            this.la = cC();
            this.iO.addView(this.la);
            this.iO.addView(cy());
            if (this.j) {
                this.iO.addView(cA());
            }
            if (this.iq.booleanValue()) {
                LinearLayout linearLayout3 = this.iO;
                LinearLayout linearLayout4 = new LinearLayout(d.getContext());
                LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams2.topMargin = y.lO;
                layoutParams2.leftMargin = 5;
                layoutParams2.rightMargin = 5;
                linearLayout4.setLayoutParams(layoutParams2);
                linearLayout4.setOrientation(0);
                linearLayout4.setPadding(10, 5, 10, 5);
                linearLayout4.setBackgroundDrawable(this.le);
                TextView textView = new TextView(d.getContext());
                textView.setText("请输入答案:");
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams3.rightMargin = 10;
                layoutParams3.gravity = 16;
                textView.setPadding(10, 5, 0, 5);
                textView.setLayoutParams(layoutParams3);
                textView.setTextColor(-8289919);
                textView.setTextSize(y.lW);
                linearLayout4.addView(textView);
                this.kV = new ImageView(d.getContext());
                this.kV.setLayoutParams(layoutParams3);
                if (this.kU != null) {
                    this.kV.setImageBitmap(this.kU);
                } else {
                    Bitmap n9 = y.n(d.getContext(), "mmiap/image/vertical/yanzhengma_bg.png");
                    if (n9 != null) {
                        this.kV.setImageBitmap(n9);
                    }
                }
                linearLayout4.addView(this.kV);
                this.kW = new ProgressBar(d.getContext());
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams4.addRule(13);
                this.kW.setLayoutParams(layoutParams4);
                this.kW.setIndeterminate(true);
                this.kW.setVisibility(8);
                linearLayout4.addView(this.kW);
                this.f9c = new TextView(d.getContext());
                this.f9c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                this.f9c.setGravity(17);
                this.f9c.setVisibility(8);
                this.f9c.setText("刷新失败，请重试");
                this.f9c.setTextColor(-65536);
                linearLayout4.addView(this.f9c);
                this.kX = new TextView(d.getContext());
                LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams5.rightMargin = 10;
                layoutParams5.gravity = 16;
                this.kX.setLayoutParams(layoutParams5);
                this.kX.setPadding(1, 1, 1, 1);
                this.kX.setText(Html.fromHtml("<u>看不清</u>"));
                this.kX.setTextSize(y.lW);
                this.kX.setTextColor(-16776961);
                n();
                linearLayout4.addView(this.kX);
                TextView textView2 = new TextView(d.getContext());
                textView2.setText("请输入答案:");
                LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(0, -2, 1.0f);
                layoutParams6.leftMargin = 10;
                layoutParams6.rightMargin = 4;
                layoutParams6.gravity = 16;
                textView2.setLayoutParams(layoutParams3);
                textView2.setTextColor(-8289919);
                textView2.setTextSize(y.lW);
                linearLayout4.addView(textView2);
                this.c = new EditText(d.getContext());
                this.c.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
                this.c.setOnKeyListener(this.ll);
                this.c.setOnFocusChangeListener(this.lk);
                this.c.setOnTouchListener(this.iQ);
                this.c.setFocusableInTouchMode(true);
                this.c.setBackgroundDrawable(this.ld);
                this.c.setId(0);
                this.c.setHint("验证码答案");
                this.c.setInputType(0);
                linearLayout4.addView(this.c);
                TextView textView3 = new TextView(d.getContext());
                textView3.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
                linearLayout4.addView(textView3);
                LinearLayout linearLayout5 = new LinearLayout(d.getContext());
                LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, -2);
                linearLayout5.setPadding(10, 5, 10, 5);
                linearLayout5.setLayoutParams(layoutParams7);
                linearLayout3.addView(linearLayout4);
            }
            if (1 == cD()) {
                this.iO.addView(cE());
            }
            if (true == d.bu().booleanValue()) {
                this.iO.addView(cB());
            }
            this.iO.addView(a(this.e, this.lo, "确 认 支 付"));
            if (2 == cD()) {
                this.iO.addView(cF());
            }
            String c2 = this.kZ.c();
            if (!(c2 == null || c2.trim().length() == 0)) {
                this.iO.addView(l(c2, this.kZ.d()));
            }
            this.iO.addView(L(d.getContext()));
            this.ix = new ScrollView(d.getContext());
            this.ix.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            this.ix.setFillViewport(true);
            this.ix.setBackgroundDrawable(this.iy);
            this.ix.addView(this.iO);
            scrollView = this.ix;
        } else {
            this.iO = new LinearLayout(d.getContext());
            LinearLayout.LayoutParams layoutParams8 = new LinearLayout.LayoutParams(-1, -1);
            this.iO.setOrientation(1);
            this.iO.setLayoutParams(layoutParams8);
            if (1 != cD()) {
                this.iO.addView(K(d.getContext()));
                LinearLayout linearLayout6 = this.iO;
                Context context3 = d.getContext();
                ImageView imageView3 = this.f8b;
                linearLayout6.addView(b(context3, this.lm));
            } else {
                LinearLayout linearLayout7 = this.iO;
                Context context4 = d.getContext();
                ImageView imageView4 = this.f8b;
                linearLayout7.addView(a(context4, this.lm));
            }
            this.la = cx();
            this.iO.addView(this.la);
            this.iO.addView(cy());
            if (this.j) {
                this.iO.addView(cA());
            }
            if (this.iq.booleanValue()) {
                LinearLayout linearLayout8 = this.iO;
                LinearLayout linearLayout9 = new LinearLayout(d.getContext());
                LinearLayout.LayoutParams layoutParams9 = new LinearLayout.LayoutParams(-1, -2);
                layoutParams9.topMargin = y.lO;
                layoutParams9.leftMargin = 5;
                layoutParams9.rightMargin = 5;
                linearLayout9.setLayoutParams(layoutParams9);
                linearLayout9.setOrientation(1);
                linearLayout9.setPadding(10, 5, 10, 5);
                linearLayout9.setBackgroundDrawable(this.le);
                LinearLayout linearLayout10 = new LinearLayout(d.getContext());
                LinearLayout.LayoutParams layoutParams10 = new LinearLayout.LayoutParams(-1, -2);
                linearLayout10.setPadding(10, 5, 10, 5);
                linearLayout10.setLayoutParams(layoutParams10);
                TextView textView4 = new TextView(d.getContext());
                textView4.setText("验证码:");
                LinearLayout.LayoutParams layoutParams11 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams11.rightMargin = 6;
                layoutParams11.gravity = 16;
                textView4.setLayoutParams(layoutParams11);
                textView4.setTextColor(-8289919);
                textView4.setTextSize(y.lW);
                linearLayout10.addView(textView4);
                linearLayout9.addView(linearLayout10);
                this.kV = new ImageView(d.getContext());
                this.kV.setLayoutParams(layoutParams11);
                if (this.kU != null) {
                    this.kV.setImageBitmap(this.kU);
                } else {
                    Bitmap n10 = y.n(d.getContext(), "mmiap/image/vertical/yanzhengma_bg.png");
                    if (n10 != null) {
                        this.kV.setImageBitmap(n10);
                    }
                }
                linearLayout10.addView(this.kV);
                this.kW = new ProgressBar(d.getContext(), null, 16842873);
                RelativeLayout.LayoutParams layoutParams12 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams12.addRule(13);
                this.kW.setLayoutParams(layoutParams12);
                this.kW.setIndeterminate(true);
                this.kW.setVisibility(8);
                linearLayout10.addView(this.kW);
                this.f9c = new TextView(d.getContext());
                this.f9c.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
                this.f9c.setGravity(17);
                this.f9c.setVisibility(8);
                this.f9c.setText("刷新失败，请重试");
                this.f9c.setTextColor(-65536);
                linearLayout10.addView(this.f9c);
                this.kX = new TextView(d.getContext());
                LinearLayout.LayoutParams layoutParams13 = new LinearLayout.LayoutParams(-2, -2);
                layoutParams13.gravity = 16;
                this.kX.setLayoutParams(layoutParams13);
                this.kX.setPadding(1, 1, 1, 1);
                this.kX.setText(Html.fromHtml("<u>看不清</u>"));
                this.kX.setTextSize(y.lW);
                this.kX.setTextColor(-16776961);
                n();
                linearLayout10.addView(this.kX);
                LinearLayout linearLayout11 = new LinearLayout(d.getContext());
                LinearLayout.LayoutParams layoutParams14 = new LinearLayout.LayoutParams(-1, -2);
                linearLayout11.setPadding(10, 5, 10, 5);
                linearLayout11.setLayoutParams(layoutParams14);
                TextView textView5 = new TextView(d.getContext());
                textView5.setText("请输入答案:");
                LinearLayout.LayoutParams layoutParams15 = new LinearLayout.LayoutParams(0, -2, 1.0f);
                layoutParams15.rightMargin = 6;
                layoutParams15.gravity = 16;
                textView5.setLayoutParams(layoutParams11);
                textView5.setTextColor(-8289919);
                textView5.setTextSize(y.lW);
                linearLayout11.addView(textView5);
                this.c = new EditText(d.getContext());
                if (d.jQ < 1.0f) {
                    this.c.setLayoutParams(new LinearLayout.LayoutParams(0, 30, 1.0f));
                } else {
                    this.c.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
                }
                this.c.setOnKeyListener(this.ll);
                this.c.setOnFocusChangeListener(this.lk);
                this.c.setOnTouchListener(this.iQ);
                this.c.setCursorVisible(true);
                this.c.setFocusableInTouchMode(true);
                this.c.setBackgroundDrawable(this.ld);
                this.c.setId(0);
                this.c.setHint("验证码答案");
                this.c.setInputType(0);
                linearLayout11.addView(this.c);
                TextView textView6 = new TextView(d.getContext());
                textView6.setLayoutParams(new LinearLayout.LayoutParams(0, -2, 1.0f));
                linearLayout11.addView(textView6);
                linearLayout9.addView(linearLayout11);
                linearLayout8.addView(linearLayout9);
            }
            if (1 == cD()) {
                this.iO.addView(cE());
            }
            if (true == d.bu().booleanValue()) {
                this.iO.addView(cB());
            }
            this.iO.addView(a(this.e, this.lo, "确 认 支 付"));
            if (2 == cD()) {
                this.iO.addView(cF());
            }
            String c3 = this.kZ.c();
            if (!(c3 == null || c3.trim().length() == 0)) {
                this.iO.addView(l(c3, this.kZ.d()));
            }
            this.iO.addView(L(d.getContext()));
            LinearLayout.LayoutParams layoutParams16 = new LinearLayout.LayoutParams(-1, -1);
            this.ix = new ScrollView(d.getContext());
            this.ix.setLayoutParams(layoutParams16);
            this.ix.setFillViewport(true);
            this.ix.setBackgroundDrawable(this.iy);
            this.ix.addView(this.iO);
            scrollView = this.ix;
        }
        setContentView(scrollView);
        setCancelable(false);
        super.show();
    }
}
