package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;

class c implements View.OnTouchListener {
    final /* synthetic */ b kA;

    c(b bVar) {
        this.kA = bVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.kA.kt);
            return false;
        }
        view.setBackgroundDrawable(this.kA.ks);
        return false;
    }
}
