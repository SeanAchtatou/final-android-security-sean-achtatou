package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Handler;
import java.util.HashMap;
import mm.purchasesdk.b;
import mm.purchasesdk.b.a;
import mm.purchasesdk.k.d;

public class u {
    private static u lv = null;
    private j lr;
    private l ls;
    private v lt;
    private aa lu;

    private u() {
    }

    public static u cG() {
        if (lv == null) {
            lv = new u();
        }
        return lv;
    }

    public final void J() {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (this.lr == null || this.lr.getOwnerActivity() != d.getContext()) {
                this.lr = new j(d.getContext());
            }
            if (!this.lr.isShowing()) {
                this.lr.show();
            }
        }
    }

    public final void K() {
        if (this.lr != null && this.lr.isShowing()) {
            this.lr.dismiss();
        }
    }

    public final void L() {
        if (this.ls != null && this.ls.isShowing()) {
            this.ls.dismiss();
        }
    }

    public final void M() {
        if (this.lr != null && this.lr.isShowing()) {
            this.lr.dismiss();
        }
        if (this.ls != null && this.ls.isShowing()) {
            this.ls.dismiss();
        }
        if (this.lt != null && this.lt.isShowing()) {
            this.lt.dismiss();
        }
        if (this.lu != null && this.lu.isShowing()) {
            this.lu.dismiss();
        }
        this.lr = null;
        this.ls = null;
        this.lt = null;
        this.lu = null;
    }

    public final void a(int i, String str, b bVar, Handler handler, Handler handler2, HashMap hashMap) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (!(i == 403 || i == 404 || i == 115)) {
                this.lr.dismiss();
            }
            this.lu = new aa(d.getContext(), bVar, handler, handler2, str, i, hashMap);
            this.lu.show();
        }
    }

    public final void a(int i, b bVar, Handler handler, Handler handler2, HashMap hashMap) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (!(i == 403 || i == 404 || i == 115 || !this.lr.isShowing())) {
                this.lr.dismiss();
            }
            this.lt = new v(d.getContext(), bVar, handler, handler2, i, hashMap);
            this.lt.show();
        }
    }

    public final void a(Handler handler, Handler handler2, b bVar, a aVar) {
        if (!((Activity) d.getContext()).isFinishing()) {
            if (this.ls == null || this.ls.getContext() != d.getContext()) {
                this.ls = new l(d.getContext(), handler, handler2, bVar, aVar);
            } else {
                this.ls.b(aVar);
            }
            this.ls.show();
            K();
        }
    }

    public final void c(a aVar) {
        this.ls.c(aVar);
    }
}
