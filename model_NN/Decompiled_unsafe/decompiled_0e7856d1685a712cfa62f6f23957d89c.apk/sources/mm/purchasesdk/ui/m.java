package mm.purchasesdk.ui;

import android.view.View;

class m implements View.OnFocusChangeListener {
    final /* synthetic */ l lq;

    m(l lVar) {
        this.lq = lVar;
    }

    public void onFocusChange(View view, boolean z) {
        switch (view.getId()) {
            case 0:
                if (this.lq.d != null) {
                    this.lq.d.setBackgroundDrawable(this.lq.ld);
                }
                if (this.lq.c != null) {
                    this.lq.c.setBackgroundDrawable(this.lq.lc);
                }
                if (this.lq.f10e != null) {
                    this.lq.f10e.setBackgroundDrawable(this.lq.ld);
                    return;
                }
                return;
            case 1:
                if (this.lq.d != null) {
                    this.lq.d.setBackgroundDrawable(this.lq.lc);
                }
                if (this.lq.c != null) {
                    this.lq.c.setBackgroundDrawable(this.lq.ld);
                }
                if (this.lq.f10e != null) {
                    this.lq.f10e.setBackgroundDrawable(this.lq.ld);
                    return;
                }
                return;
            case 2:
                if (this.lq.f10e != null) {
                    this.lq.f10e.setBackgroundDrawable(this.lq.lc);
                }
                if (this.lq.c != null) {
                    this.lq.c.setBackgroundDrawable(this.lq.ld);
                }
                if (this.lq.d != null) {
                    this.lq.d.setBackgroundDrawable(this.lq.ld);
                    return;
                }
                return;
            default:
                return;
        }
    }
}
