package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Toast;
import com.a.a.h.b;
import mm.purchasesdk.fingerprint.c;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class q implements View.OnClickListener {
    final /* synthetic */ l lq;

    q(l lVar) {
        this.lq = lVar;
    }

    public void onClick(View view) {
        if (this.lq.kZ.bs().booleanValue()) {
            this.lq.c.setText(this.lq.c.getText().toString().trim());
            if (this.lq.c != null && this.lq.c.getText().toString().length() == 0) {
                Toast.makeText(d.getContext(), "请输入验证码", 0).show();
                return;
            }
        }
        if (!this.lq.kZ.b() || this.lq.d == null || !this.lq.d.isShown() || this.lq.d.getText().toString().length() != 0) {
            String str = "";
            if (c.jk.booleanValue()) {
                str = c.bK();
                int status = c.getStatus();
                if (status == -6) {
                    Toast.makeText(d.getContext(), "指纹正在获取中,请稍后再试", 0).show();
                    return;
                } else if (status != 0) {
                    this.lq.dismiss();
                    e.e("PurchaseDialog", "failed to create fingerprint,error code=" + status);
                    this.lq.iA.a(400, null);
                    return;
                }
            }
            String str2 = str;
            if (((Activity) d.getContext()).isFinishing()) {
                e.e("PurchaseDialog", "Activity is finished!");
                return;
            }
            if (l.cD() == 0) {
                e.e("PurchaseDialog", "getPayType = 0");
                d.aj(b.SMS_RESULT_SEND_FAIL);
            } else if (1 == l.cD()) {
                e.e("PurchaseDialog", "getPayType = 1");
                d.aj(b.SMS_RESULT_RECV_SUCCESS);
            } else if (2 == l.cD()) {
                e.e("PurchaseDialog", "getPayType = 2");
                d.aj(b.SMS_RESULT_SEND_FAIL);
            }
            u.cG().J();
            Bundle bundle = new Bundle();
            bundle.putString("dyMark", str2);
            bundle.putString("CheckAnswer", this.lq.cz());
            bundle.putString("CheckId", this.lq.kZ.h());
            bundle.putString("Password", this.lq.getPassword());
            bundle.putString("SessionId", this.lq.kZ.g());
            bundle.putInt("OrderCount", this.lq.kZ.a());
            bundle.putBoolean("multiSubs", this.lq.kZ.m1c());
            bundle.putBoolean("NeedPasswd", this.lq.kZ.b());
            bundle.putBoolean("NeedInput", this.lq.kZ.bs().booleanValue());
            bundle.putString("RandomPwd", this.lq.m());
            Message obtainMessage = this.lq.hL.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.obj = this.lq.iA;
            obtainMessage.arg1 = 1;
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
            return;
        }
        Toast.makeText(d.getContext(), "请输入支付密码", 0).show();
    }
}
