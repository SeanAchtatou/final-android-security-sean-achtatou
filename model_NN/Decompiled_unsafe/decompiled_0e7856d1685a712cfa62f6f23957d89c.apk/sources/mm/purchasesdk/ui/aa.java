package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import javax.microedition.media.PlayerListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class aa extends b {
    private final String TAG = "WebViewLayout";
    /* access modifiers changed from: private */
    public int al;
    private Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f6b;
    private ImageView c;
    /* access modifiers changed from: private */
    public Handler hL;
    /* access modifiers changed from: private */
    public HashMap io;
    private Drawable iy;
    /* access modifiers changed from: private */
    public WebView kh;
    /* access modifiers changed from: private */
    public ProgressBar ki;
    private String kj;
    /* access modifiers changed from: private */
    public b kk;
    private final int kl = 1;
    private final int km = 2;
    /* access modifiers changed from: private */
    public RelativeLayout kn;
    private View.OnClickListener ko = new ab(this);

    public aa(Context context, b bVar, Handler handler, Handler handler2, String str, int i, HashMap hashMap) {
        super(context, 16973829);
        Bitmap n;
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.hL = handler;
        this.b = handler2;
        this.kk = bVar;
        try {
            str = URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.kj = str;
        e.c("WebViewLayout", "murl =" + this.kj);
        this.al = i;
        this.io = hashMap;
        if (this.iy == null && (n = y.n(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.iy = new BitmapDrawable(n);
        }
    }

    private View J(Context context) {
        this.kn = new RelativeLayout(context);
        this.kn.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10);
        ImageView imageView = this.f6b;
        ImageView imageView2 = this.c;
        View.OnClickListener onClickListener = this.ko;
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        relativeLayout.setPadding(0, y.lT, 0, y.lU);
        relativeLayout.setLayoutParams(layoutParams2);
        relativeLayout.setBackgroundDrawable(this.kr);
        ImageView imageView3 = new ImageView(context);
        imageView3.setTag(0);
        imageView3.setId(1);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(15);
        imageView3.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView3.setBackgroundColor(0);
        imageView3.setImageBitmap(this.kv);
        imageView3.setPadding(5, 0, 0, 0);
        imageView3.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView3, layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = d.jQ < 1.0f ? new RelativeLayout.LayoutParams((int) PurchaseCode.NONE_NETWORK, 40) : new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(13);
        ImageView imageView4 = new ImageView(context);
        imageView4.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView4.setImageBitmap(y.o(d.getContext(), "mmiap/image/vertical/logo1.png"));
        relativeLayout.addView(imageView4, layoutParams4);
        ImageView imageView5 = new ImageView(context);
        imageView5.setTag(1);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(15, -1);
        layoutParams5.addRule(11, -1);
        imageView5.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView5.setId(2);
        imageView5.setBackgroundColor(0);
        imageView5.setImageBitmap(this.kx);
        imageView5.setPadding(0, 0, 5, 0);
        imageView5.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView5, layoutParams5);
        relativeLayout.setId(10001);
        this.kn.addView(relativeLayout, layoutParams);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams6.addRule(3, 10001);
        this.kh = new WebView(context);
        this.kh.setBackgroundColor(-1);
        this.kh.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.kh.getSettings().setJavaScriptEnabled(true);
        this.kh.requestFocus();
        this.kn.addView(this.kh, layoutParams6);
        this.ki = new ProgressBar(context);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams7.addRule(13);
        this.kn.addView(this.ki, layoutParams7);
        this.kh.setWebViewClient(new ac(this, layoutParams7));
        this.kh.setWebChromeClient(new ad(this));
        try {
            this.kh.loadUrl(this.kj);
        } catch (Exception e) {
            e.d("webview", PlayerListener.ERROR + e);
        }
        return this.kn;
    }

    public void dismiss() {
        u.cG().L();
        super.dismiss();
    }

    public void show() {
        setContentView(J(d.getContext()));
        setCancelable(false);
        super.show();
    }
}
