package mm.purchasesdk.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class v extends b {
    private final String TAG = "ResultDialog";
    /* access modifiers changed from: private */
    public int al;
    private int am;
    /* access modifiers changed from: private */
    public Handler b;

    /* renamed from: b  reason: collision with other field name */
    private ImageView f11b;
    private String bK = "";
    private String bL = "";
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public Handler hL;
    /* access modifiers changed from: private */
    public HashMap io;
    private ScrollView ix;
    private Drawable iy;
    /* access modifiers changed from: private */
    public b kk;
    private View.OnClickListener lA = new w(this);
    private Drawable le;
    private View.OnClickListener lm = new x(this);
    private TextView lw;
    private String lx = "";
    private Boolean ly = true;
    private Bitmap lz;

    public v(Context context, b bVar, Handler handler, Handler handler2, int i, HashMap hashMap) {
        super(context, 16973829);
        Bitmap n;
        Bitmap n2;
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.al = i;
        this.hL = handler;
        this.b = handler2;
        this.kk = bVar;
        this.io = hashMap;
        this.f = new Button(d.getContext());
        if (i == 102 || i == 104) {
            this.lx = "支 付 成 功";
            this.bL = "确 定";
            this.lz = y.n(d.getContext(), "mmiap/image/vertical/icon_success.png");
            this.am = -11037110;
            this.bK = PurchaseCode.getReason(i);
            this.ly = true;
        } else {
            this.lx = "支 付 失 败";
            this.ly = false;
            this.am = -4703429;
            if (this.io == null) {
                e.e("ResultDialog", "mReturnObject null order fail =" + i);
                this.lz = y.n(d.getContext(), "mmiap/image/vertical/icon_info.png");
            } else {
                e.e("ResultDialog", "mReturnObject order fail =" + i);
                this.lz = y.n(d.getContext(), "mmiap/image/vertical/icon_false.png");
            }
            this.bK = PurchaseCode.getReason(i);
            if (i == 403 || i == 404 || i == 115) {
                this.bL = "重 新 购 买";
            } else {
                this.bL = "确 定";
            }
        }
        if (this.le == null && (n2 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk = n2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.le = new NinePatchDrawable(n2, ninePatchChunk, new Rect(), null);
        }
        if (this.iy == null && (n = y.n(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.iy = new BitmapDrawable(n);
        }
    }

    public void dismiss() {
        u.cG().L();
        super.dismiss();
    }

    public void show() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.gravity = 1;
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(K(d.getContext()));
        Context context = d.getContext();
        ImageView imageView = this.f11b;
        linearLayout.addView(b(context, this.lm));
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams((int) (((double) d.jR) * 0.95d), y.lG);
        layoutParams2.gravity = 1;
        layoutParams2.topMargin = y.lO;
        linearLayout2.setOrientation(1);
        linearLayout2.setLayoutParams(layoutParams2);
        linearLayout2.setBackgroundDrawable(this.le);
        int i = d.jQ < 1.0f ? (int) (((double) y.lG) * 0.65d) : (int) (((double) y.lG) * 0.75d);
        LinearLayout linearLayout3 = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, i);
        linearLayout3.setOrientation(0);
        linearLayout3.setLayoutParams(layoutParams3);
        linearLayout3.setGravity(17);
        ImageView imageView2 = new ImageView(d.getContext());
        imageView2.setImageBitmap(this.lz);
        linearLayout3.addView(imageView2);
        TextView textView = new TextView(d.getContext());
        textView.setTextSize(30.0f);
        textView.setTextColor(this.am);
        textView.setGravity(17);
        textView.setText(this.lx);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.leftMargin = 10;
        layoutParams4.gravity = 16;
        textView.setLayoutParams(layoutParams4);
        linearLayout3.addView(textView);
        linearLayout2.addView(linearLayout3);
        TextView textView2 = new TextView(d.getContext());
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, 2));
        textView2.setBackgroundDrawable(new BitmapDrawable(y.n(d.getContext(), "mmiap/image/vertical/line.png")));
        linearLayout2.addView(textView2);
        this.lw = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(-2, (y.lG - i) - 2);
        layoutParams5.gravity = 17;
        this.lw.setGravity(17);
        this.lw.setLayoutParams(layoutParams5);
        this.lw.setTextColor(-8289919);
        this.lw.setText(this.bK);
        this.lw.setTextSize(15.0f);
        linearLayout2.addView(this.lw);
        linearLayout.addView(linearLayout2);
        linearLayout.addView(a(this.f, this.lA, this.bL));
        if (this.ly.booleanValue()) {
            LinearLayout linearLayout4 = new LinearLayout(d.getContext());
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams6.leftMargin = 10;
            layoutParams6.rightMargin = 10;
            layoutParams6.topMargin = 10;
            layoutParams6.bottomMargin = 10;
            linearLayout4.setLayoutParams(layoutParams6);
            linearLayout4.setBackgroundDrawable(a.d(-1, 10, 10));
            linearLayout4.setPadding(6, 6, 6, 6);
            TextView textView3 = new TextView(d.getContext());
            textView3.setTextColor(-8289919);
            textView3.setText("欢迎您使用中国移动手机话费支付功能，支付成功后系统会向您发送提示短信，请注意查收！");
            textView3.setTextSize(y.lW);
            textView3.setLineSpacing(1.0f, 1.3f);
            linearLayout4.addView(textView3);
            linearLayout.addView(linearLayout4);
        }
        linearLayout.addView(L(d.getContext()));
        this.ix = new ScrollView(d.getContext());
        LinearLayout.LayoutParams layoutParams7 = new LinearLayout.LayoutParams(-1, -1);
        this.ix = new ScrollView(d.getContext());
        this.ix.setLayoutParams(layoutParams7);
        this.ix.setFillViewport(true);
        this.ix.setBackgroundDrawable(this.iy);
        this.ix.addView(linearLayout);
        setContentView(this.ix);
        setCancelable(false);
        super.show();
    }
}
