package mm.purchasesdk.ui;

import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

class o implements View.OnKeyListener {
    final /* synthetic */ l lq;

    o(l lVar) {
        this.lq = lVar;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i != 28) {
            return false;
        }
        ((EditText) view).setText("");
        return false;
    }
}
