package mm.purchasesdk.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.a.a.h.b;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

public class d {
    private View.OnClickListener b = new f(this);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with other field name */
    public View f7b;
    private HashMap c = null;
    private Boolean kB = true;
    /* access modifiers changed from: private */
    public PopupWindow kC;
    /* access modifiers changed from: private */
    public View kD;
    private Drawable kE;
    /* access modifiers changed from: private */
    public Drawable kF;
    /* access modifiers changed from: private */
    public Drawable kG;
    /* access modifiers changed from: private */
    public Drawable kH;
    /* access modifiers changed from: private */
    public Drawable kI;
    /* access modifiers changed from: private */
    public Drawable kJ;
    /* access modifiers changed from: private */
    public Drawable kK;
    private View.OnTouchListener kL = new e(this);
    private View.OnLongClickListener kM = new h(this);

    public d(View view, Boolean bool) {
        this.kB = bool;
        this.f7b = view;
        try {
            if (this.kE == null) {
                Bitmap n = y.n(mm.purchasesdk.k.d.getContext(), "mmiap/image/vertical/keyboard_bg.png");
                if (n != null) {
                    this.kE = new BitmapDrawable(n);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_bg.pngread failed!");
                }
            }
            if (this.kF == null) {
                Bitmap ar = ar("mmiap/image/vertical/keyboard_button.png");
                if (ar != null) {
                    this.kF = new BitmapDrawable(ar);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_button.pngread failed!");
                }
            }
            if (this.kG == null) {
                Bitmap ar2 = ar("mmiap/image/vertical/keyboard_button_delete.png");
                if (ar2 != null) {
                    this.kG = new BitmapDrawable(ar2);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_button_delete.pngread failed!");
                }
            }
            if (this.kH == null) {
                Bitmap ar3 = ar("mmiap/image/vertical/keyboard_button_hide.png");
                if (ar3 != null) {
                    this.kH = new BitmapDrawable(ar3);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_button_hide.pngread failed!");
                }
            }
            if (this.kI == null) {
                Bitmap ar4 = ar("mmiap/image/vertical/keyboard_button_press.png");
                if (ar4 != null) {
                    this.kI = new BitmapDrawable(ar4);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_button_press.pngread failed!");
                }
            }
            if (this.kK == null) {
                Bitmap ar5 = ar("mmiap/image/vertical/keyboard_button_hide_press.png");
                if (ar5 != null) {
                    this.kK = new BitmapDrawable(ar5);
                } else {
                    throw new Exception("error!mmiap/image/vertical/keyboard_button_hide_press.pngread failed!");
                }
            }
            if (this.kJ == null) {
                Bitmap ar6 = ar("mmiap/image/vertical/keyboard_button_delete_press.png");
                if (ar6 != null) {
                    this.kJ = new BitmapDrawable(ar6);
                    return;
                }
                throw new Exception("error!mmiap/image/vertical/keyboard_button_delete_press.pngread failed!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View M(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.kE);
        linearLayout.setPadding(5, 5, 5, 5);
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 10; i++) {
            Button button = new Button(context);
            button.setPadding(1, 1, 1, 1);
            button.setBackgroundDrawable(this.kF);
            button.setText(String.valueOf(i));
            button.setTextSize(25.0f);
            button.setOnClickListener(this.b);
            button.setOnTouchListener(this.kL);
            button.setTag(0);
            linkedList.add(button);
        }
        Random random = new Random();
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout2.setBackgroundColor(0);
        for (int i2 = 0; i2 < 3; i2++) {
            Button button2 = (Button) linkedList.remove(random.nextInt(10 - i2));
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams2.rightMargin = 5;
            layoutParams2.bottomMargin = 5;
            button2.setLayoutParams(layoutParams2);
            linearLayout2.addView(button2);
        }
        Button button3 = new Button(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams3.bottomMargin = 5;
        button3.setLayoutParams(layoutParams3);
        button3.setBackgroundDrawable(this.kG);
        button3.setTag(1);
        button3.setOnTouchListener(this.kL);
        button3.setOnLongClickListener(this.kM);
        button3.setOnClickListener(this.b);
        linearLayout2.addView(button3);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout3.setBackgroundColor(0);
        int i3 = 3;
        while (true) {
            int i4 = i3;
            if (i4 >= 7) {
                break;
            }
            Button button4 = (Button) linkedList.remove(random.nextInt(10 - i4));
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            if (i4 != 6) {
                layoutParams4.rightMargin = 5;
            }
            layoutParams4.bottomMargin = 5;
            button4.setLayoutParams(layoutParams4);
            linearLayout3.addView(button4);
            i3 = i4 + 1;
        }
        linearLayout.addView(linearLayout3);
        LinearLayout linearLayout4 = new LinearLayout(context);
        linearLayout4.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout4.setBackgroundColor(0);
        int i5 = 7;
        while (true) {
            int i6 = i5;
            if (i6 < 10) {
                Button button5 = (Button) linkedList.remove(random.nextInt(10 - i6));
                LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -1, 1.0f);
                layoutParams5.rightMargin = 5;
                layoutParams5.bottomMargin = 5;
                button5.setLayoutParams(layoutParams5);
                linearLayout4.addView(button5);
                i5 = i6 + 1;
            } else {
                Button button6 = new Button(context);
                LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(0, -1, 1.0f);
                layoutParams6.bottomMargin = 5;
                button6.setLayoutParams(layoutParams6);
                button6.setBackgroundDrawable(this.kH);
                button6.setTag(2);
                button6.setOnTouchListener(this.kL);
                button6.setOnClickListener(this.b);
                linearLayout4.addView(button6);
                linearLayout.addView(linearLayout4);
                return linearLayout;
            }
        }
    }

    private View N(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setBackgroundDrawable(this.kE);
        linearLayout.setPadding(5, 5, 5, 5);
        Bitmap ar = ar("mmiap/image/vertical/keyboard_button.png");
        LinkedList linkedList = new LinkedList();
        for (int i = 0; i < 10; i++) {
            Button button = new Button(context);
            button.setPadding(1, 1, 1, 1);
            if (ar != null) {
                button.setBackgroundDrawable(new BitmapDrawable(ar));
            }
            button.setText(String.valueOf(i));
            button.setTextSize(25.0f);
            button.setOnClickListener(this.b);
            button.setOnTouchListener(this.kL);
            button.setTag(0);
            linkedList.add(button);
        }
        Random random = new Random();
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout2.setBackgroundColor(0);
        for (int i2 = 0; i2 < 5; i2++) {
            Button button2 = (Button) linkedList.remove(random.nextInt(10 - i2));
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams2.rightMargin = 5;
            layoutParams2.bottomMargin = 5;
            button2.setLayoutParams(layoutParams2);
            linearLayout2.addView(button2);
        }
        Button button3 = new Button(context);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams3.bottomMargin = 5;
        button3.setLayoutParams(layoutParams3);
        button3.setBackgroundDrawable(this.kG);
        button3.setTag(1);
        button3.setOnLongClickListener(this.kM);
        button3.setOnTouchListener(this.kL);
        button3.setOnClickListener(this.b);
        linearLayout2.addView(button3);
        linearLayout.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setLayoutParams(new LinearLayout.LayoutParams(-1, 0, 1.0f));
        linearLayout3.setBackgroundColor(0);
        for (int i3 = 5; i3 < 10; i3++) {
            Button button4 = (Button) linkedList.remove(random.nextInt(10 - i3));
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -1, 1.0f);
            layoutParams4.rightMargin = 5;
            layoutParams4.bottomMargin = 5;
            button4.setLayoutParams(layoutParams4);
            linearLayout3.addView(button4);
        }
        Button button5 = new Button(context);
        LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, -1, 1.0f);
        layoutParams5.bottomMargin = 5;
        button5.setLayoutParams(layoutParams5);
        button5.setBackgroundDrawable(this.kH);
        button5.setTag(2);
        button5.setOnTouchListener(this.kL);
        button5.setOnClickListener(this.b);
        linearLayout3.addView(button5);
        linearLayout.addView(linearLayout3);
        return linearLayout;
    }

    static /* synthetic */ int a(d dVar, String str) {
        if (dVar.c == null) {
            dVar.c = new HashMap();
            dVar.c.put(b.SMS_RESULT_SEND_SUCCESS, 7);
            dVar.c.put(b.SMS_RESULT_SEND_FAIL, 8);
            dVar.c.put(b.SMS_RESULT_SEND_CANCEL, 9);
            dVar.c.put(b.SMS_RESULT_RECV_SUCCESS, 10);
            dVar.c.put(b.SMS_RESULT_SEND_NOTIFIED, 11);
            dVar.c.put("5", 12);
            dVar.c.put("6", 13);
            dVar.c.put("7", 14);
            dVar.c.put("8", 15);
            dVar.c.put("9", 16);
            dVar.c.put("back", 4);
            dVar.c.put("clear", 28);
            dVar.c.put("delete", 67);
        }
        if (dVar.c.get(str) != null) {
            return ((Integer) dVar.c.get(str)).intValue();
        }
        return 0;
    }

    private static Bitmap ar(String str) {
        Bitmap n = y.n(mm.purchasesdk.k.d.getContext(), str);
        if (n != null) {
            return n;
        }
        throw new Exception("error," + str + " is not exist!");
    }

    public final void a(View view) {
        this.kD = view;
        this.kB = Boolean.valueOf(!y.iw.booleanValue());
        if (this.kC == null) {
            this.kC = new PopupWindow(this.kB.booleanValue() ? M(mm.purchasesdk.k.d.getContext()) : N(mm.purchasesdk.k.d.getContext()), -1, y.lQ);
        }
        if (!this.kC.isShowing()) {
            this.kC.showAtLocation(this.kD, 80, 0, 0);
        }
    }
}
