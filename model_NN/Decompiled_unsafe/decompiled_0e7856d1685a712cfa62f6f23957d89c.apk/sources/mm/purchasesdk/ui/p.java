package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class p implements View.OnClickListener {
    final /* synthetic */ l lq;

    p(l lVar) {
        this.lq = lVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("PurchaseDialog", "Activity is finished!");
            return;
        }
        this.lq.dismiss();
        this.lq.iA.a((int) PurchaseCode.BILL_CANCEL_FAIL);
        this.lq.iA.a((HashMap) null);
        Message obtainMessage = this.lq.b.obtainMessage();
        obtainMessage.what = 5;
        obtainMessage.obj = this.lq.iA;
        obtainMessage.sendToTarget();
    }
}
