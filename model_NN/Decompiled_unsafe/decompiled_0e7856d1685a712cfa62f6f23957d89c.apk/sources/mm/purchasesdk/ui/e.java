package mm.purchasesdk.ui;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

class e implements View.OnTouchListener {
    final /* synthetic */ d kN;

    e(d dVar) {
        this.kN = dVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        Integer num = (Integer) view.getTag();
        if (motionEvent.getAction() == 0) {
            if (num.intValue() == 0) {
                view.setBackgroundDrawable(this.kN.kI);
                ((Button) view).setTextColor(-1);
            } else if (num.intValue() == 1) {
                view.setBackgroundDrawable(this.kN.kJ);
            } else {
                view.setBackgroundDrawable(this.kN.kK);
            }
        } else if (motionEvent.getAction() == 1) {
            if (num.intValue() == 0) {
                view.setBackgroundDrawable(this.kN.kF);
                ((Button) view).setTextColor(-16777216);
            } else if (num.intValue() == 1) {
                view.setBackgroundDrawable(this.kN.kG);
            } else {
                view.setBackgroundDrawable(this.kN.kH);
            }
        }
        return false;
    }
}
