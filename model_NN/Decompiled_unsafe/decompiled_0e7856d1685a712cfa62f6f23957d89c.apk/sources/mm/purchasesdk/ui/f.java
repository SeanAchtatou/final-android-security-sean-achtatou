package mm.purchasesdk.ui;

import android.view.View;
import android.widget.Button;

class f implements View.OnClickListener {
    final /* synthetic */ d kN;

    f(d dVar) {
        this.kN = dVar;
    }

    public void onClick(View view) {
        Integer num = (Integer) view.getTag();
        if (num.intValue() == 0 || num.intValue() == 1) {
            new Thread(new g(this, d.a(this.kN, num.intValue() == 0 ? String.valueOf(((Button) view).getText()) : "delete"))).start();
        } else if (num.intValue() == 2) {
            this.kN.kC.dismiss();
            if (this.kN.f7b != null) {
                this.kN.f7b.setVisibility(0);
            }
            this.kN.kD.scrollTo(0, 0);
        }
    }
}
