package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class x implements View.OnClickListener {
    final /* synthetic */ v lB;

    x(v vVar) {
        this.lB = vVar;
    }

    public void onClick(View view) {
        if (((Activity) d.getContext()).isFinishing()) {
            e.e("ResultDialog", "Activity is finished!");
            return;
        }
        this.lB.dismiss();
        u.cG().K();
        this.lB.kk.a(this.lB.al);
        this.lB.kk.a(this.lB.io);
        Message obtainMessage = this.lB.b.obtainMessage();
        obtainMessage.what = 5;
        obtainMessage.obj = this.lB.kk;
        obtainMessage.sendToTarget();
    }
}
