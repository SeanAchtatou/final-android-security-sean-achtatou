package mm.purchasesdk.ui;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class w implements View.OnClickListener {
    final /* synthetic */ v lB;

    w(v vVar) {
        this.lB = vVar;
    }

    public void onClick(View view) {
        if (this.lB.f.getText().equals("重 新 购 买")) {
            if (((Activity) d.getContext()).isFinishing()) {
                e.e("ResultDialog", "Activity is finished!");
                return;
            }
            this.lB.dismiss();
            Message obtainMessage = this.lB.hL.obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = this.lB.kk;
            obtainMessage.arg1 = 0;
            obtainMessage.sendToTarget();
        } else if (((Activity) d.getContext()).isFinishing()) {
            e.e("ResultDialog", "Activity is finished!");
        } else {
            this.lB.dismiss();
            this.lB.kk.a(this.lB.al);
            this.lB.kk.a(this.lB.io);
            Message obtainMessage2 = this.lB.b.obtainMessage();
            obtainMessage2.what = 5;
            obtainMessage2.obj = this.lB.kk;
            obtainMessage2.sendToTarget();
        }
    }
}
