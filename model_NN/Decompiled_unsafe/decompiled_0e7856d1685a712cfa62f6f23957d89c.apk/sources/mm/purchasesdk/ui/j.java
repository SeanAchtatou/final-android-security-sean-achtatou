package mm.purchasesdk.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.WindowManager;
import android.widget.LinearLayout;
import mm.purchasesdk.k.d;

public class j extends Dialog {
    private int B = 1003;
    private LinearLayout kR;
    private k kS;

    public j(Context context) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.alpha = 0.8f;
        getWindow().setAttributes(attributes);
    }

    public void dismiss() {
        if (getOwnerActivity() == d.getContext()) {
            super.dismiss();
        }
    }

    public void show() {
        this.kR = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.kR.setOrientation(1);
        this.kR.setLayoutParams(layoutParams);
        this.kS = new k();
        this.kR.addView(this.kS.cw());
        setContentView(this.kR);
        setCancelable(false);
        super.show();
    }
}
