package mm.purchasesdk.ui;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;

public class a {
    public static Drawable d(int i, int i2, int i3) {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-1, -1});
        gradientDrawable.setStroke(2, -4802890);
        gradientDrawable.setShape(0);
        gradientDrawable.setCornerRadii(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f});
        return gradientDrawable;
    }
}
