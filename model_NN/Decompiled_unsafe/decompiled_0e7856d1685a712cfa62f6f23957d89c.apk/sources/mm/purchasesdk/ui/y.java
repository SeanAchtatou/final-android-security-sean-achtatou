package mm.purchasesdk.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class y {
    public static float a = 40.0f;
    public static int as = 20;
    private static ArrayList b = new ArrayList();
    private static HashMap d = new HashMap();
    public static Boolean iw = false;
    public static int lC = 20;
    public static int lD = 80;
    public static int lE = 55;
    public static int lF = 480;
    public static int lG = 280;
    public static int lH = 5;
    public static int lI = 5;
    public static int lJ = 15;
    public static float lK = 80.0f;
    public static float lL = 1.0f;
    public static float lM = 1.0f;
    public static float lN = 1.0f;
    public static int lO = 10;
    public static float lP = 1.0f;
    public static int lQ;
    public static float lR = 16.0f;
    public static int lS = 20;
    public static int lT = 10;
    public static int lU = 10;
    public static float lV = 16.0f;
    public static float lW = 18.0f;
    public static float lX = 20.0f;

    public static void N() {
        for (Map.Entry value : d.entrySet()) {
            Bitmap bitmap = (Bitmap) value.getValue();
            if (bitmap != null && !bitmap.isRecycled()) {
                bitmap.recycle();
            }
        }
        d.clear();
    }

    public static void O() {
        if (b.size() != 0) {
            b.clear();
        }
        b.add("mmiap/image/vertical/logo1.png");
        b.add("mmiap/image/vertical/title1_bg.png");
        b.add("mmiap/image/vertical/title2_bg.png");
        b.add("mmiap/image/vertical/button_back.png");
        b.add("mmiap/image/vertical/icon_success.png");
        b.add("mmiap/image/vertical/icon_false.png");
        b.add("mmiap/image/vertical/icon_info.png");
        b.add("mmiap/image/vertical/logo3.png");
        b.add("mmiap/image/vertical/infoline.png");
        b.add("mmiap/image/vertical/logo2.png");
        b.add("mmiap/image/vertical/icon_chifubao.png");
        b.add("mmiap/image/vertical/button_back.png");
        b.add("mmiap/image/vertical/button_back_Press.png");
        b.add("mmiap/image/vertical/button_finishbilling.png");
        b.add("mmiap/image/vertical/button_finishbilling_press.png");
        b.add("mmiap/image/vertical/top_button_back.png");
        b.add("mmiap/image/vertical/top_button_back_press.png");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    private static Bitmap a(Bitmap bitmap) {
        float f;
        if (d.jQ > 1.0f) {
            f = lM;
        } else if (d.jQ >= 1.0f) {
            return bitmap;
        } else {
            f = lM;
        }
        Matrix matrix = new Matrix();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > 1 && width > 1) {
            matrix.postScale(f, f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (height <= 1) {
            matrix.postScale(f, 1.0f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (width > 1) {
            return bitmap;
        } else {
            matrix.postScale(1.0f, f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
    }

    public static Bitmap as(String str) {
        Bitmap bitmap = (Bitmap) d.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        Bitmap bitmap2 = str.equals("mmiap/image/vertical/logo2.png") ? null : bitmap;
        if (bitmap2 == null) {
            return null;
        }
        if (b.contains(str)) {
            Bitmap a2 = a(bitmap2);
            if (a2 != bitmap2) {
                bitmap2.recycle();
            }
            d.put(str, a2);
            return a2;
        }
        d.put(str, bitmap2);
        return bitmap2;
    }

    public static Bitmap n(Context context, String str) {
        return o(context, str);
    }

    public static Bitmap o(Context context, String str) {
        Bitmap bitmap = (Bitmap) d.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(context.getAssets().open(str));
            if (b.contains(str)) {
                Bitmap a2 = a(decodeStream);
                if (a2 != decodeStream) {
                    decodeStream.recycle();
                }
                d.put(str, a2);
                return a2;
            }
            d.put(str, decodeStream);
            return decodeStream;
        } catch (IOException e) {
            e.e("ReadImageFile", "read image fail.......");
            e.printStackTrace();
            return null;
        }
    }
}
