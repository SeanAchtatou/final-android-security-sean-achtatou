package mm.purchasesdk.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import mm.purchasesdk.k.d;

public class b extends Dialog {
    protected Drawable kr;
    /* access modifiers changed from: protected */
    public Drawable ks;
    /* access modifiers changed from: protected */
    public Drawable kt;
    private Drawable ku;
    protected Bitmap kv;
    private Bitmap kw;
    protected Bitmap kx;
    private Bitmap ky;
    private Bitmap kz;
    private Context mContext;

    public b(Context context, int i) {
        super(context, 16973829);
        Bitmap n;
        Bitmap n2;
        Bitmap n3;
        this.mContext = context;
        if (this.kr == null) {
            this.kr = new BitmapDrawable(y.o(d.getContext(), "mmiap/image/vertical/title1_bg.png"));
        }
        if (this.ks == null && (n3 = y.n(this.mContext, "mmiap/image/vertical/button1_Confirm.9.png")) != null) {
            byte[] ninePatchChunk = n3.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.ks = new NinePatchDrawable(n3, ninePatchChunk, new Rect(), null);
        }
        if (this.kt == null && (n2 = y.n(this.mContext, "mmiap/image/vertical/button1_Confirm_Press.9.png")) != null) {
            byte[] ninePatchChunk2 = n2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.kt = new NinePatchDrawable(n2, ninePatchChunk2, new Rect(), null);
        }
        if (this.ku == null && (n = y.n(d.getContext(), "mmiap/image/vertical/title2_bg.png")) != null) {
            this.ku = new BitmapDrawable(n);
        }
        if (this.kv == null) {
            this.kv = y.n(d.getContext(), "mmiap/image/vertical/top_button_back.png");
        }
        if (this.kw == null) {
            this.kw = y.n(d.getContext(), "mmiap/image/vertical/button_back_Press.png");
        }
        if (this.kx == null) {
            this.kx = y.n(d.getContext(), "mmiap/image/vertical/button_finishbilling.png");
        }
        if (this.kz == null) {
            this.kz = y.as("mmiap/image/vertical/logo2.png");
        }
        if (this.ky == null) {
            this.ky = y.n(d.getContext(), "mmiap/image/vertical/top_button_back.png");
        }
    }

    public static View L(Context context) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        relativeLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        relativeLayout.setPadding(0, 0, 0, 10);
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.bottomMargin = 5;
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(1);
        ImageView imageView = new ImageView(context);
        Bitmap n = y.n(context, "mmiap/image/vertical/logo3.png");
        if (n != null) {
            imageView.setImageBitmap(n);
        }
        linearLayout.addView(imageView);
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -1);
        layoutParams2.leftMargin = 5;
        layoutParams2.gravity = 17;
        textView.setGravity(17);
        textView.setTextColor(-16777216);
        textView.setLayoutParams(layoutParams2);
        textView.setTextSize(y.lV);
        textView.setText("版权所有 中国移动");
        linearLayout.addView(textView);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12);
        relativeLayout.addView(linearLayout, layoutParams3);
        return relativeLayout;
    }

    public final View K(Context context) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        if (d.jQ < 1.0f) {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 50));
        } else {
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        }
        imageView.setImageBitmap(y.o(d.getContext(), "mmiap/image/vertical/logo1.png"));
        imageView.setBackgroundDrawable(this.kr);
        return imageView;
    }

    public final View a(Context context, View.OnClickListener onClickListener) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        new RelativeLayout.LayoutParams(-1, -2);
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        imageView.setImageBitmap(y.o(d.getContext(), "mmiap/image/vertical/logo1.png"));
        imageView.setBackgroundDrawable(this.kr);
        relativeLayout.addView(imageView);
        ImageView imageView2 = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(15);
        imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView2.setBackgroundColor(0);
        imageView2.setImageBitmap(this.ky);
        imageView2.setPadding(5, 0, 0, 0);
        imageView2.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView2, layoutParams);
        return relativeLayout;
    }

    public final View a(Button button, View.OnClickListener onClickListener, String str) {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.leftMargin = 10;
        layoutParams.rightMargin = 10;
        layoutParams.topMargin = y.lS;
        linearLayout.setOrientation(1);
        layoutParams.gravity = 1;
        linearLayout.setLayoutParams(layoutParams);
        button.setGravity(17);
        button.setOnTouchListener(new c(this));
        LinearLayout.LayoutParams layoutParams2 = null;
        if (d.jQ < 1.0f) {
            layoutParams2 = y.iw.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.jR) * 0.6d), 40) : new LinearLayout.LayoutParams(-1, 40);
        }
        if (d.jQ == 1.0f) {
            layoutParams2 = y.iw.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.jR) * 0.6d), -2) : new LinearLayout.LayoutParams(-1, -2);
        }
        if (d.jQ > 1.0f) {
            layoutParams2 = y.iw.booleanValue() ? new LinearLayout.LayoutParams((int) (((double) d.jR) * 0.6d), 80) : new LinearLayout.LayoutParams(-1, 80);
        }
        layoutParams2.gravity = 1;
        button.setGravity(17);
        button.setPadding(0, 8, 0, 12);
        button.setText(str);
        button.setTextSize(1, (float) y.as);
        button.setTextColor(-1);
        button.setLayoutParams(layoutParams2);
        button.setBackgroundDrawable(this.ks);
        button.setOnClickListener(onClickListener);
        linearLayout.addView(button);
        return linearLayout;
    }

    public final View b(Context context, View.OnClickListener onClickListener) {
        RelativeLayout relativeLayout = new RelativeLayout(context);
        RelativeLayout.LayoutParams layoutParams = d.jQ < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2);
        relativeLayout.setPadding(0, y.lT, 0, y.lU);
        relativeLayout.setLayoutParams(layoutParams);
        y.n(context, "mmiap/image/vertical/title2_bg.png");
        relativeLayout.setBackgroundDrawable(this.ku);
        ImageView imageView = new ImageView(context);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(15);
        imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageView.setBackgroundColor(0);
        imageView.setImageBitmap(this.kw);
        imageView.setPadding(5, 0, 0, 0);
        imageView.setOnClickListener(onClickListener);
        relativeLayout.addView(imageView, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(13);
        Context context2 = d.getContext();
        LinearLayout linearLayout = new LinearLayout(context2);
        linearLayout.setOrientation(0);
        if (this.kz != null) {
            ImageView imageView2 = new ImageView(context2);
            imageView2.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
            imageView2.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView2.setBackgroundColor(0);
            imageView2.setImageBitmap(this.kz);
            linearLayout.addView(imageView2);
        }
        TextView textView = new TextView(context2);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-2, -1));
        textView.setGravity(17);
        if (d.cg().equals("100000000000")) {
            textView.setText("手机话费支付(自测试)");
        } else {
            textView.setText("手机话费支付");
        }
        textView.setTextColor(-16777216);
        textView.setSingleLine(true);
        textView.setTextSize(y.lX);
        linearLayout.addView(textView);
        relativeLayout.addView(linearLayout, layoutParams3);
        TextView textView2 = new TextView(context);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(0, 0, 1.0f));
        textView2.setBackgroundColor(0);
        relativeLayout.addView(textView2);
        return relativeLayout;
    }

    public void dismiss() {
        super.dismiss();
    }
}
