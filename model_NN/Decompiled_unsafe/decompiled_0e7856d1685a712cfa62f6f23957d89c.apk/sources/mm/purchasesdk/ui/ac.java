package mm.purchasesdk.ui;

import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

class ac extends WebViewClient {
    final /* synthetic */ aa kp;
    final /* synthetic */ RelativeLayout.LayoutParams kq;

    ac(aa aaVar, RelativeLayout.LayoutParams layoutParams) {
        this.kp = aaVar;
        this.kq = layoutParams;
    }

    public void onPageFinished(WebView webView, String str) {
        e.c("WebViewLayout", "Finished loading URL: " + str);
        this.kp.kn.removeView(this.kp.ki);
        ProgressBar unused = this.kp.ki = (ProgressBar) null;
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        if (this.kp.ki == null) {
            ProgressBar unused = this.kp.ki = new ProgressBar(d.getContext());
            this.kp.kn.addView(this.kp.ki, this.kq);
        }
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        this.kp.kh.loadUrl(str);
        return true;
    }
}
