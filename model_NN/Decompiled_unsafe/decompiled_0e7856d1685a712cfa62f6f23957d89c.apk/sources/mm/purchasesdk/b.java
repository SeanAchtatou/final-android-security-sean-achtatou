package mm.purchasesdk;

import android.os.Handler;
import java.util.HashMap;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;
import mm.purchasesdk.ui.u;
import mm.purchasesdk.ui.y;

public class b {
    private static int b = 0;
    private int a = 0;

    /* renamed from: b  reason: collision with other field name */
    private Handler f2b;
    private Handler hL;
    private OnPurchaseListener in;
    private HashMap io = null;

    public b(OnPurchaseListener onPurchaseListener, Handler handler, Handler handler2) {
        this.in = onPurchaseListener;
        this.hL = handler;
        this.f2b = handler2;
    }

    public final void a() {
        e.e("onBillingfinish", "code=" + this.a + "." + PurchaseCode.getReason(this.a));
        u.cG().M();
        d.unlock();
        y.N();
        this.in.onBillingFinish(this.a, this.io);
    }

    public final void a(int i) {
        this.a = i;
    }

    public final void a(int i, HashMap hashMap) {
        int i2 = PurchaseCode.ORDER_OK;
        e.e("onDialogShow", "code=" + i + "." + d.cc());
        this.io = hashMap;
        if (i != 102 || !d.cc().equals(com.a.a.h.b.SMS_RESULT_RECV_SUCCESS)) {
            if (i != 101) {
                i2 = i;
            }
            u.cG().a(i2, this, this.hL, this.f2b, hashMap);
            return;
        }
        u.cG().a(i, d.bT(), this, this.hL, this.f2b, hashMap);
    }

    public final void a(HashMap hashMap) {
        this.io = hashMap;
    }

    public final void b(int i) {
        b = i;
    }

    public final void onAfterApply() {
        this.in.onAfterApply();
    }

    public final void onBeforeApply() {
        this.in.onBeforeApply();
    }

    public final void onInitFinish(int i) {
        e.e("onInitFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        d.unlock();
        this.in.onInitFinish(i);
    }

    public final void onQueryFinish(int i, HashMap hashMap) {
        e.e("onQueryFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        d.unlock();
        this.in.onQueryFinish(i, hashMap);
        b = 0;
    }
}
