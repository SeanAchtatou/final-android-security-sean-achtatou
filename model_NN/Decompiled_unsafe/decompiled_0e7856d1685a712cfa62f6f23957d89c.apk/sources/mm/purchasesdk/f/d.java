package mm.purchasesdk.f;

import android.content.Context;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import mm.purchasesdk.g.f;
import mm.purchasesdk.k.e;

public class d implements c {
    private static final String TAG = d.class.getSimpleName();

    public static String b(Context context) {
        return b("VERSION", context);
    }

    private static String b(String str, Context context) {
        InputStream resourceAsStream = context.getClass().getClassLoader().getResourceAsStream(str);
        if (resourceAsStream == null) {
            e.e(TAG, "failed to find resource file(" + str + "}");
            return null;
        }
        StringBuilder sb = new StringBuilder();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        while (bufferedReader.ready()) {
            try {
                sb.append(bufferedReader.readLine());
            } catch (IOException e) {
                e.e(TAG, "failed to read resource file(" + str + ")");
                return null;
            }
        }
        bufferedReader.close();
        e.c(TAG, "file content->" + sb.toString());
        return sb.toString();
    }

    public static String r() {
        return b("mmiap.xml", mm.purchasesdk.k.d.getContext());
    }

    public final String a(f fVar) {
        return null;
    }

    public final boolean a(mm.purchasesdk.g.e eVar, f fVar) {
        return false;
    }

    public final String b(mm.purchasesdk.g.e eVar, f fVar) {
        return null;
    }

    public final String q() {
        return b("CopyrightDeclaration.xml", mm.purchasesdk.k.d.getContext());
    }
}
