package mm.purchasesdk.f;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.Calendar;
import javax.microedition.media.control.MetaDataControl;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.f;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class b implements c {
    private static final String TAG = b.class.getSimpleName();
    protected SQLiteDatabase ji;
    protected a jj;

    private String c(String str) {
        open();
        String valueOf = String.valueOf(Calendar.getInstance().getTimeInMillis());
        Cursor query = this.ji.query("auth", new String[]{str}, "appid=? and paycode=? and imsi=? and notAfter>?", new String[]{d.bX(), d.bZ(), d.ce(), valueOf}, null, null, null);
        query.moveToFirst();
        if (query.getCount() == 0) {
            query.close();
            this.jj.close();
            return null;
        }
        String string = query.getString(query.getColumnIndex(str));
        query.close();
        this.jj.close();
        return string;
    }

    private void open() {
        this.jj = new a(d.getContext());
        this.ji = this.jj.getWritableDatabase();
    }

    public final String a(f fVar) {
        String c = c("content");
        String c2 = c("orderId");
        e.c(TAG, "content =" + c + " orderID=" + c2);
        if (c == null || mm.purchasesdk.a.e.c(c, mm.purchasesdk.e.b.bH().iU.ja, c2) != 104) {
            return null;
        }
        return c2;
    }

    public final void a(String str, String str2, String str3) {
        open();
        this.ji.delete("auth", "appID = ? and paycode = ? and imsi = ?", new String[]{str, str2, str3});
        this.jj.close();
    }

    public final void a(String str, String str2, String str3, long j, long j2, String str4, String str5) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("imsi", str5);
        contentValues.put("appid", str);
        contentValues.put("paycode", str2);
        contentValues.put("notBefore", Long.valueOf(j));
        contentValues.put("notAfter", Long.valueOf(j2));
        contentValues.put("orderId", str4);
        contentValues.put("content", str3);
        open();
        this.ji.delete("auth", "appID = ? and paycode = ? and imsi = ?", new String[]{str, str2, str5});
        this.ji.insert("auth", null, contentValues);
        this.jj.close();
    }

    public final boolean a(mm.purchasesdk.g.e eVar, f fVar) {
        String c = c("content");
        String c2 = c("orderId");
        if (c != null) {
            int c3 = mm.purchasesdk.a.e.c(c, mm.purchasesdk.e.b.bH().iU.ja, c2);
            PurchaseCode.setStatusCode(c3);
            if (c3 == 104) {
                fVar.p(c2);
                return true;
            }
        }
        return false;
    }

    public final String b(mm.purchasesdk.g.e eVar, f fVar) {
        return null;
    }

    public final void d(String str, String str2, String str3) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("imsi", str3);
        contentValues.put("appid", str);
        contentValues.put("content", str2);
        open();
        this.ji.delete(MetaDataControl.COPYRIGHT_KEY, "appID = ? and imsi = ?", new String[]{str, str3});
        this.ji.insert(MetaDataControl.COPYRIGHT_KEY, null, contentValues);
        this.jj.close();
    }

    public final String q() {
        open();
        Cursor query = this.ji.query(MetaDataControl.COPYRIGHT_KEY, new String[]{"content"}, "appid=? and imsi=?", new String[]{d.bX(), d.ce()}, null, null, null);
        query.moveToFirst();
        if (query.getCount() == 0) {
            query.close();
            this.jj.close();
            return null;
        }
        String string = query.getString(query.getColumnIndex("content"));
        query.close();
        this.jj.close();
        return string;
    }
}
