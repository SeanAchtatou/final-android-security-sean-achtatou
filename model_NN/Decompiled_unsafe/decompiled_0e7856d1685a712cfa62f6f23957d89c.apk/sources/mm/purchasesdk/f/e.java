package mm.purchasesdk.f;

import android.content.Context;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import com.ccit.mmwlan.vo.SignView;
import java.io.UnsupportedEncodingException;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.c;
import mm.purchasesdk.g.f;
import mm.purchasesdk.g.h;
import mm.purchasesdk.k.a;
import mm.purchasesdk.k.b;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.g;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;

public class e implements c {
    private static final String TAG = e.class.getSimpleName();

    private static String a(mm.purchasesdk.g.e eVar, String str, f fVar) {
        HttpClient I = g.I(d.getContext());
        if (I == null) {
            PurchaseCode.setStatusCode(PurchaseCode.NONE_NETWORK);
            throw new mm.purchasesdk.d(PurchaseCode.NONE_NETWORK);
        }
        HttpPost httpPost = new HttpPost(str);
        mm.purchasesdk.k.e.c(TAG, "url=" + str);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 3) {
                if (d.d()) {
                    try {
                        SignView SIDSign = MMClientSDK_ForPhone.SIDSign(d.bX(), null);
                        if (SIDSign.getResult() != 0) {
                            MMClientSDK_ForPhone.DestorySecCert(null);
                            mm.purchasesdk.k.e.e(TAG, "mobile failed to make sidSignature.code=" + SIDSign.getResult());
                            PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                            return null;
                        }
                        eVar.a(SIDSign);
                    } catch (Exception e) {
                        mm.purchasesdk.k.e.e(TAG, "mobile failed to make sidSignature.code=118");
                        PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                        return null;
                    }
                } else {
                    SignView sidSign_PAD = MMClientSDK_ForPad.sidSign_PAD(d.bX(), null, null, null);
                    if (sidSign_PAD.getResult() != 0) {
                        try {
                            MMClientSDK_ForPad.DestorySecCert(null);
                        } catch (Exception e2) {
                            e2.printStackTrace();
                        }
                        mm.purchasesdk.k.e.e(TAG, "pad failed to make sidSignature.code=" + sidSign_PAD.getResult());
                        PurchaseCode.setStatusCode(PurchaseCode.INVALID_SIDSIGN_ERR);
                        return null;
                    }
                    eVar.a(sidSign_PAD);
                }
                try {
                    String a = eVar.a();
                    if (a == null) {
                        mm.purchasesdk.k.e.e(TAG, "failed to make query request.code=");
                        return null;
                    }
                    mm.purchasesdk.k.e.c(TAG, "request : code = " + a);
                    try {
                        StringEntity stringEntity = new StringEntity(a);
                        mm.purchasesdk.k.e.c(TAG, "request : code = " + a);
                        httpPost.setEntity(stringEntity);
                        try {
                            long currentTimeMillis = System.currentTimeMillis();
                            HttpResponse execute = I.execute(httpPost);
                            mm.purchasesdk.k.e.c(TAG, "Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                            if (execute.getStatusLine().getStatusCode() == 200) {
                                String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                                mm.purchasesdk.k.e.c(TAG, entityUtils);
                                if (fVar.parse(entityUtils)) {
                                    return entityUtils;
                                }
                                PurchaseCode.setStatusCode(PurchaseCode.PROTOCOL_ERR);
                                return null;
                            }
                            continue;
                            i = i2 + 1;
                        } catch (Exception e3) {
                            mm.purchasesdk.k.e.a(TAG, "network failed", e3);
                        }
                    } catch (UnsupportedEncodingException e4) {
                        e4.printStackTrace();
                        return null;
                    }
                } catch (Exception e5) {
                    mm.purchasesdk.k.e.a(TAG, "failed to make query request.exception.code=", e5);
                    return null;
                }
            } else {
                mm.purchasesdk.k.e.e(TAG, "http response network timeout");
                throw new mm.purchasesdk.d(PurchaseCode.NETWORKTIMEOUT_ERR);
            }
        }
    }

    private static boolean a(String str, String str2, f fVar) {
        int i = 0;
        HttpClient I = g.I(d.getContext());
        if (I == null) {
            PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
            throw new mm.purchasesdk.d(PurchaseCode.NETWORKTIMEOUT_ERR);
        }
        HttpPost httpPost = new HttpPost(str2);
        try {
            httpPost.setEntity(new StringEntity(str));
            mm.purchasesdk.k.e.c(TAG, str);
            while (i < 3) {
                try {
                    long currentTimeMillis = System.currentTimeMillis();
                    HttpResponse execute = I.execute(httpPost);
                    mm.purchasesdk.k.e.c(TAG, "Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                    if (execute.getStatusLine().getStatusCode() == 200) {
                        String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                        mm.purchasesdk.k.e.c(TAG, entityUtils);
                        if (fVar.parse(entityUtils)) {
                            PurchaseCode.setStatusCode(0);
                            return true;
                        }
                        PurchaseCode.setStatusCode(PurchaseCode.COPYRIGHT_PROTOCOL_ERR);
                        throw new mm.purchasesdk.d(PurchaseCode.COPYRIGHT_PROTOCOL_ERR);
                    }
                    i++;
                } catch (Exception e) {
                    PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
                    throw new mm.purchasesdk.d(PurchaseCode.NETWORKTIMEOUT_ERR);
                }
            }
            PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
            mm.purchasesdk.k.e.e(TAG, "http response network timeout");
            throw new mm.purchasesdk.d(PurchaseCode.NETWORKTIMEOUT_ERR);
        } catch (UnsupportedEncodingException e2) {
            PurchaseCode.setStatusCode(300);
            e2.printStackTrace();
            throw new mm.purchasesdk.d(300);
        }
    }

    private static String c(mm.purchasesdk.g.e eVar, f fVar) {
        boolean z;
        Context context = d.getContext();
        HttpClient I = g.I(context);
        if (I == null) {
            fVar.X("短信验证码获取失败,请检查网络连接是否正常");
            return null;
        }
        HttpPost httpPost = new HttpPost(d.G(context));
        try {
            String gVar = ((mm.purchasesdk.g.g) eVar).toString();
            b.a(0, TAG, "Query request : code = " + gVar);
            try {
                StringEntity stringEntity = new StringEntity(gVar);
                b.a(0, TAG, gVar);
                httpPost.setEntity(stringEntity);
                int i = 0;
                String str = null;
                while (true) {
                    if (i >= 3) {
                        z = false;
                        break;
                    }
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        HttpResponse execute = I.execute(httpPost);
                        b.a(0, TAG, "SMSRequest Req/Resp Time: " + (System.currentTimeMillis() - currentTimeMillis));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            String entityUtils = EntityUtils.toString(execute.getEntity(), "utf-8");
                            try {
                                b.a(0, TAG, "response:" + entityUtils);
                                String str2 = entityUtils;
                                z = true;
                                str = str2;
                                break;
                            } catch (Exception e) {
                                Exception exc = e;
                                str = entityUtils;
                                e = exc;
                            }
                        } else {
                            continue;
                            i++;
                        }
                    } catch (Exception e2) {
                        e = e2;
                        fVar.X("短信验证码获取失败,请检查网络连接是否正常");
                        b.a(2, TAG, "sms network failed.code=" + fVar.bM(), e);
                        i++;
                    }
                }
                if (!z) {
                    fVar.X("短信验证码获取失败,请检查网络连接是否正常");
                    b.a(1, TAG, "SMS http response status code is " + fVar.bM());
                    return null;
                } else if (str != null) {
                    return str;
                } else {
                    b.a(1, TAG, "cannot read authorization response");
                    fVar.X("短信验证码获取失败,请检查网络连接是否正常");
                    return null;
                }
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
                fVar.X("短信验证码获取失败，系统出现错误");
                return null;
            }
        } catch (Exception e4) {
            b.a(2, TAG, "failed to make query request", e4);
            fVar.X("短信验证码获取失败,短信请求错误");
            return null;
        }
    }

    public final String a(f fVar) {
        int i = PurchaseCode.QUERY_OK;
        mm.purchasesdk.g.d dVar = (mm.purchasesdk.g.d) fVar;
        String a = a(new c(), d.G(d.getContext()), dVar);
        if (a != null) {
            mm.purchasesdk.k.e.c(TAG, "queryOrderId retcode=" + dVar.bM());
            int intValue = Integer.valueOf(dVar.bM()).intValue();
            if ((intValue == 0 || intValue == 100) && !a.ad(a).booleanValue()) {
                PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
                return null;
            }
            switch (intValue) {
                case 0:
                    String b = mm.purchasesdk.a.e.b(a);
                    if (b != null && b.length() != 0) {
                        if (!d.bV().booleanValue()) {
                            int c = mm.purchasesdk.a.e.c(b, mm.purchasesdk.e.b.bH().iU.ja, dVar.k());
                            PurchaseCode.setStatusCode(c);
                            if (c != 104) {
                                i = c;
                                break;
                            }
                        }
                    } else {
                        PurchaseCode.setStatusCode(PurchaseCode.AUTH_PARSE_FAIL);
                        mm.purchasesdk.k.e.e(TAG, "auth file is null,code=" + ((int) PurchaseCode.AUTH_PARSE_FAIL));
                        i = 241;
                        break;
                    }
                    break;
                case 1:
                    i = PurchaseCode.QUERY_FROZEN;
                    break;
                case 2:
                    i = PurchaseCode.QUERY_NOT_FOUND;
                    break;
                case 5:
                    i = PurchaseCode.QUERY_PAYCODE_ERROR;
                    break;
                case 11:
                    i = PurchaseCode.QUERY_NO_AUTHORIZATION;
                    break;
                case 12:
                    i = PurchaseCode.QUERY_CSSP_BUSY;
                    break;
                case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
                    i = PurchaseCode.QUERY_OTHER_ERROR;
                    break;
                case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
                    try {
                        if (d.d()) {
                            MMClientSDK_ForPhone.DestorySecCert(null);
                        } else {
                            MMClientSDK_ForPad.DestorySecCert(null);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    i = PurchaseCode.QUERY_INVALID_USER;
                    break;
                case DefaultVirtualDevice.WIDGET_TYPE_DYNAMICJOYSTICK /*15*/:
                    i = PurchaseCode.QUERY_INVALID_APP;
                    break;
                case 16:
                    i = PurchaseCode.QUERY_LICENSE_ERROR;
                    break;
                case 17:
                    i = PurchaseCode.QUERY_INVALID_SIGN;
                    break;
                case 18:
                    i = PurchaseCode.QUERY_NO_ABILITY;
                    break;
                case 19:
                    i = 512;
                    break;
                case 100:
                    i = PurchaseCode.AUTH_TIME_LIMIT;
                    break;
                default:
                    i = 0;
                    break;
            }
            PurchaseCode.setStatusCode(i);
            d.an(dVar.k());
            d.b(dVar.b());
            d.am(dVar.e());
            return dVar.k();
        }
        mm.purchasesdk.k.e.e(TAG, "get getOrderId failed: " + dVar.bM());
        return null;
    }

    public final boolean a(mm.purchasesdk.g.e eVar, f fVar) {
        int i;
        String a = a(eVar, d.d(d.getContext()), fVar);
        if (a == null || a.length() <= 0) {
            return false;
        }
        int intValue = Integer.valueOf(fVar.bM()).intValue();
        if ((intValue == 0 || intValue == 2 || intValue == 6 || intValue == 100) && !a.ad(a).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
            return false;
        }
        mm.purchasesdk.k.e.c(TAG, "checkAuth return code =" + intValue);
        if (intValue == 0) {
            String b = mm.purchasesdk.a.e.b(a);
            if (b == null || b.length() == 0) {
                PurchaseCode.setStatusCode(PurchaseCode.AUTH_PARSE_FAIL);
                mm.purchasesdk.k.e.e(TAG, "auth file is null,code=" + ((int) PurchaseCode.AUTH_PARSE_FAIL));
                return false;
            }
            int c = mm.purchasesdk.a.e.c(b, mm.purchasesdk.e.b.bH().iU.ja, fVar.k());
            PurchaseCode.setStatusCode(c);
            return c == 104;
        }
        if (intValue == 2) {
            i = PurchaseCode.AUTH_NOT_FOUND;
        } else if (intValue == 1) {
            i = PurchaseCode.AUTH_FROZEN;
        } else if (intValue == 4) {
            i = PurchaseCode.AUTH_NOT_DOWNLOAD;
        } else if (intValue == 3) {
            i = PurchaseCode.AUTH_FORBIDDEN;
        } else if (intValue == 5) {
            i = PurchaseCode.AUTH_PAYCODE_ERROR;
        } else if (intValue == 6) {
            i = PurchaseCode.AUTH_NO_PICODE;
        } else if (intValue == 37) {
            i = PurchaseCode.AUTH_INVALID_ORDERCOUNT;
        } else if (intValue == 11) {
            i = PurchaseCode.AUTH_NO_AUTHORIZATION;
        } else if (intValue == 12) {
            i = PurchaseCode.AUTH_CSSP_BUSY;
        } else if (intValue == 13) {
            i = PurchaseCode.AUTH_OTHER_ERROR;
        } else if (intValue == 14) {
            try {
                if (d.d()) {
                    MMClientSDK_ForPhone.DestorySecCert(null);
                } else {
                    MMClientSDK_ForPad.DestorySecCert(null);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            i = PurchaseCode.AUTH_INVALID_USER;
        } else if (intValue == 15) {
            i = PurchaseCode.AUTH_INVALID_APP;
        } else if (intValue == 16) {
            i = 256;
        } else if (intValue == 17) {
            i = PurchaseCode.AUTH_INVALID_SIGN;
        } else if (intValue == 18) {
            i = PurchaseCode.AUTH_NO_ABILITY;
        } else if (intValue == 19) {
            i = PurchaseCode.AUTH_NO_APP;
        } else if (intValue == 100) {
            i = PurchaseCode.AUTH_TIME_LIMIT;
        } else if (intValue == 101) {
            i = PurchaseCode.AUTH_NO_BUSINESS;
        } else if (intValue == 99) {
            i = PurchaseCode.AUTH_FORBID_ORDER;
        } else if (intValue == 39) {
            i = PurchaseCode.AUTH_STATICMARK_DECRY_FAILED;
        } else if (intValue == 40) {
            i = PurchaseCode.AUTH_STATICMARK_VERIFY_FAILED;
        } else if (intValue == 41) {
            i = PurchaseCode.AUTH_NO_DYQUESTION;
        } else if (intValue == 201) {
            i = 270;
        } else if (intValue == 202) {
            i = PurchaseCode.AUTH_OVER_LIMIT;
        } else if (intValue == 401) {
            i = PurchaseCode.AUTH_TRADEID_ERROR;
        } else if (intValue == 2008) {
            i = PurchaseCode.AUTH_INSUFFICIENT_FUNDS;
        } else {
            i = PurchaseCode.AUTH_UNDEFINED_ERROR;
            mm.purchasesdk.k.e.e(TAG, "unknown error.code=" + intValue);
        }
        PurchaseCode.setStatusCode(i);
        return false;
    }

    public final String b(mm.purchasesdk.g.e eVar, f fVar) {
        return a(eVar, d.d(d.getContext()), fVar);
    }

    public final String d(mm.purchasesdk.g.e eVar, f fVar) {
        String c = c(eVar, fVar);
        if (c == null) {
            return null;
        }
        try {
            fVar = h.d(c.getBytes(), "utf-8");
            b.a(0, TAG, "SMS response: code = " + c);
            String bM = fVar.bM();
            return com.a.a.h.b.SMS_RESULT_SEND_SUCCESS.equals(bM) ? "短信正在下发,请稍后。。。" : com.a.a.h.b.SMS_RESULT_SEND_FAIL.equals(bM) ? "请确认设备是否为pad，或者您是否已经启用了密码" : "11".equals(bM) ? "本次申请无效，请重新申请" : "12".equals(bM) ? "后台服务繁忙，请稍后再试" : "13".equals(bM) ? "未知错误，请稍后重新申请" : bM;
        } catch (Exception e) {
            b.a(2, TAG, "parse failed", e);
            fVar.X("短信验证码获取失败,响应解析错误");
            return null;
        }
    }

    public final String q() {
        mm.purchasesdk.g.a aVar = new mm.purchasesdk.g.a();
        mm.purchasesdk.g.b bVar = new mm.purchasesdk.g.b();
        a(aVar.a(), d.ch(), bVar);
        if (Integer.valueOf(bVar.jm).intValue() == 1) {
            return bVar.jo;
        }
        mm.purchasesdk.k.e.e(TAG, "get copyright failed: " + bVar.jn);
        return null;
    }
}
