package mm.purchasesdk;

import java.util.HashMap;

public interface OnPurchaseListener {
    public static final String LEFTDAY = "LeftDay";
    public static final String ORDERID = "OrderId";
    public static final String ORDERTYPE = "OrderType";
    public static final String PAYCODE = "Paycode";
    public static final String TRADEID = "TradeID";

    void onAfterApply();

    void onBeforeApply();

    void onBillingFinish(int i, HashMap hashMap);

    void onInitFinish(int i);

    void onQueryFinish(int i, HashMap hashMap);
}
