package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class g implements View.OnTouchListener {
    final /* synthetic */ a iS;

    g(a aVar) {
        this.iS = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.iS.ks);
            return false;
        }
        view.setBackgroundDrawable(this.iS.kt);
        return false;
    }
}
