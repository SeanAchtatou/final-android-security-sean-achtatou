package mm.purchasesdk.c;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.CycleInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.vo.SignView;
import mm.purchasesdk.i.e;
import mm.purchasesdk.k.d;
import mm.purchasesdk.ui.b;
import mm.purchasesdk.ui.y;

public class a extends b {
    private final String S = "我的设备";
    private String T;
    private Button a;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public EditText f4a;
    private int ag = 5;
    private int ai = 3;
    /* access modifiers changed from: private */
    public Handler b;

    /* renamed from: b  reason: collision with other field name */
    private TextView f5b;
    private Button c;
    private int f = 5;
    private int g = 20;
    private int h = 25;
    private int i = 15;
    /* access modifiers changed from: private */
    public mm.purchasesdk.b iA;
    private Drawable iB = null;
    private Drawable iC = null;
    private Drawable iD = null;
    /* access modifiers changed from: private */
    public EditText iE;
    private TextView iF;
    private Button iG;
    /* access modifiers changed from: private */
    public Drawable iH = null;
    /* access modifiers changed from: private */
    public Drawable iI = null;
    private Drawable iJ = null;
    private Drawable iK = null;
    private int iL = 20;
    private int iM = 10;
    /* access modifiers changed from: private */
    public k iN;
    private LinearLayout iO;
    private Handler iP;
    private View.OnTouchListener iQ = new b(this);
    private View.OnClickListener iR = new c(this);
    private Boolean iw = false;
    private ScrollView ix;
    private Drawable iy;
    private Handler iz;
    private int k = 15;
    private int l = 5;
    private int m = 15;
    private int n = 5;
    private int o = 10;
    private int p = 15;
    private int q = 10;
    private int s = 16;
    private int u = 20;
    private int w = 15;

    public a(Context context, mm.purchasesdk.b bVar, Handler handler, Handler handler2) {
        super(context, 16973829);
        setOwnerActivity((Activity) context);
        getWindow().requestFeature(1);
        if (d.getContext() == null || ((Activity) d.getContext()) != getOwnerActivity()) {
            d.setContext(context);
        }
        this.iz = handler;
        this.b = handler2;
        this.iA = bVar;
        this.iN = new k(60, this);
        this.iP = new d(this);
        context.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, new e(this.iP, (Activity) context));
    }

    private View H(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.setOrientation(0);
        linearLayout.setGravity(1);
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        this.f5b = new TextView(context);
        this.f5b.setSingleLine();
        this.f5b.setTextColor(-11444116);
        this.f5b.setTextSize((float) this.w);
        this.f5b.setVisibility(8);
        linearLayout.addView(this.f5b);
        return linearLayout;
    }

    private TextView V(String str) {
        TextView textView = new TextView(d.getContext());
        RelativeLayout.LayoutParams layoutParams = d.jQ < 1.0f ? new RelativeLayout.LayoutParams(-2, -2) : new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.rightMargin = this.ai;
        textView.setPadding(this.q, this.ag, this.q, this.ag);
        textView.setLayoutParams(layoutParams);
        textView.setText(str);
        textView.setTextColor(-8289919);
        textView.setTextSize((float) this.s);
        return textView;
    }

    private TextView a() {
        TextView textView = new TextView(d.getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, 2);
        layoutParams.setMargins(0, this.u, 0, this.iM);
        textView.setLayoutParams(layoutParams);
        textView.setBackgroundDrawable(new BitmapDrawable(y.n(d.getContext(), "mmiap/image/vertical/line.png")));
        return textView;
    }

    static /* synthetic */ void a(a aVar, View view) {
        aVar.iN.a((Button) view);
        k.x = 45;
        new Thread(new h(aVar)).start();
    }

    private View bA() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(16);
        linearLayout.addView(V("设 备 名 称 :"));
        this.iF = new TextView(d.getContext());
        this.iF.setId(2);
        this.iF.setLayoutParams(d.jQ < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2));
        this.iF.setText(this.T);
        this.iF.setCursorVisible(true);
        this.iF.setSingleLine();
        this.iF.setFocusableInTouchMode(true);
        this.iF.setTextSize((float) this.g);
        this.iF.setGravity(16);
        this.iF.setBackgroundDrawable(this.iD);
        linearLayout.addView(this.iF);
        return linearLayout;
    }

    private View bB() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.addView(V("手机授权码:"));
        linearLayout.setGravity(16);
        LinearLayout linearLayout2 = new LinearLayout(d.getContext());
        linearLayout2.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout2.setGravity(16);
        linearLayout2.setOrientation(0);
        this.iE = new EditText(d.getContext());
        this.iE.setId(1);
        this.iE.setLayoutParams(d.jQ < 1.0f ? new LinearLayout.LayoutParams(0, 30, 2.0f) : new LinearLayout.LayoutParams(0, -2, 2.5f));
        this.iE.setOnTouchListener(this.iQ);
        this.iE.setCursorVisible(true);
        this.iE.setSingleLine();
        this.iE.setFocusableInTouchMode(true);
        this.iE.setPadding(this.f, this.f, this.f, this.f);
        this.iE.setTextSize((float) this.g);
        this.iE.setBackgroundDrawable(this.iJ);
        this.iE.setInputType(2);
        linearLayout2.addView(this.iE);
        this.c = new Button(d.getContext());
        this.c.setId(3);
        this.c.setOnTouchListener(new e(this));
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(0, -1, 1.5f);
        layoutParams2.setMargins(this.n, 0, 0, 0);
        this.c.setPadding(0, this.o, 0, this.o);
        this.c.setText("获取授权码");
        this.c.setTextSize(1, (float) this.p);
        this.c.setTextColor(-1);
        this.c.setLayoutParams(layoutParams2);
        this.c.setBackgroundDrawable(this.iH);
        this.c.setOnClickListener(this.iR);
        this.c.setPadding(0, 5, 0, 5);
        linearLayout2.addView(this.c);
        linearLayout.addView(linearLayout2);
        return linearLayout;
    }

    private View bC() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        this.a = new Button(d.getContext());
        this.a.setOnTouchListener(new f(this));
        LinearLayout.LayoutParams layoutParams2 = d.jQ < 1.0f ? new LinearLayout.LayoutParams(0, 50, 1.0f) : new LinearLayout.LayoutParams(0, -2, 1.0f);
        layoutParams2.setMargins(this.n, 0, this.n, 0);
        this.a.setPadding(0, this.m, 0, this.m);
        this.a.setText("确 认");
        this.a.setTextSize(1, (float) y.as);
        this.a.setTextColor(-1);
        this.a.setLayoutParams(layoutParams2);
        this.a.setBackgroundDrawable(this.ks);
        this.a.setId(4);
        this.a.setOnClickListener(this.iR);
        linearLayout.addView(this.a);
        this.iG = new Button(d.getContext());
        this.iG.setId(5);
        this.iG.setOnTouchListener(new g(this));
        this.iG.setPadding(0, this.m, 0, this.m);
        this.iG.setText("取 消");
        this.iG.setTextSize(1, (float) y.as);
        this.iG.setTextColor(-1);
        this.iG.setLayoutParams(layoutParams2);
        this.iG.setBackgroundDrawable(this.kt);
        this.iG.setOnClickListener(this.iR);
        linearLayout.addView(this.iG);
        return linearLayout;
    }

    private static Animation bE() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 10.0f, 0.0f, 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setInterpolator(new CycleInterpolator(5.0f));
        return translateAnimation;
    }

    private TextView by() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        TextView textView = new TextView(d.getContext());
        textView.setLayoutParams(layoutParams);
        textView.setText("请输入您的手机号,短信验证码,用以取得设备授权");
        textView.setTextSize(y.lR);
        textView.setTextColor(-8289919);
        textView.setPadding(this.i, this.h, this.k, this.iL);
        return textView;
    }

    private View bz() {
        LinearLayout linearLayout = new LinearLayout(d.getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.topMargin = y.lO;
        layoutParams.leftMargin = 5;
        layoutParams.rightMargin = 5;
        linearLayout.setPadding(this.l, this.l, this.l, this.l);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setOrientation(0);
        linearLayout.addView(V("手 机 号 码 :"));
        Drawable drawable = this.iB;
        EditText editText = new EditText(d.getContext());
        editText.setId(0);
        editText.setLayoutParams(d.jQ < 1.0f ? new RelativeLayout.LayoutParams(-1, 30) : new RelativeLayout.LayoutParams(-1, -2));
        editText.setOnTouchListener(this.iQ);
        editText.setInputType(2);
        editText.setCursorVisible(true);
        editText.setSingleLine();
        editText.setFocusableInTouchMode(true);
        editText.setPadding(this.f, this.f, this.f, this.f);
        editText.setTextSize((float) this.g);
        editText.setBackgroundDrawable(drawable);
        this.f4a = editText;
        linearLayout.addView(this.f4a);
        return linearLayout;
    }

    private void c(String str, int i2) {
        this.f5b.setVisibility(0);
        this.f5b.setTextColor(i2);
        this.f5b.setText(str);
    }

    static /* synthetic */ void c(a aVar) {
        SignView sidSign_PAD = MMClientSDK_ForPad.sidSign_PAD(d.bX(), aVar.T, aVar.o(), null);
        int result = sidSign_PAD.getResult();
        if (result == 1) {
            aVar.e();
            aVar.c(sidSign_PAD.getErrMsg(), -65536);
            k.x = 5;
            ((InputMethodManager) d.getContext().getSystemService("input_method")).toggleSoftInput(0, 2);
        } else if (result == 2) {
            aVar.c("生成SID失败,请重新申请手机授权码", -65536);
            aVar.k();
            k.x = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 3) {
            aVar.c("准备PKI密钥对失败,请重新申请手机授权码", -65536);
            aVar.k();
            k.x = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 6) {
            aVar.c("申请参数错误，请核对手机号码是否正确", -65536);
            aVar.k();
            k.x = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 7) {
            aVar.c("申请参数错误，请核对手机号码是否正确", -65536);
            aVar.k();
            k.x = 0;
            MMClientSDK_ForPad.DestorySecCert(null);
        } else if (result == 0) {
            aVar.c("短信已经下发，收到短信后后请您输入验证码。", -16776961);
        }
    }

    static /* synthetic */ Boolean f(a aVar) {
        Boolean bool;
        String obj = aVar.iE.getText().toString();
        obj.trim();
        if (obj.equals("")) {
            aVar.c("授权码不能为空，请输入短信授权码，或者重新申请短信授权码", -65536);
            aVar.iE.startAnimation(bE());
            bool = false;
        } else {
            aVar.c("正在发送授权请求，请稍后.....", -16711936);
            bool = true;
        }
        return bool.booleanValue() && aVar.bD().booleanValue();
    }

    static /* synthetic */ void g(a aVar) {
        int result = MMClientSDK_ForPad.sidSign_PAD(d.bX(), null, aVar.o(), aVar.iE.getText().toString().trim()).getResult();
        if (result == 0) {
            Message obtainMessage = aVar.iz.obtainMessage();
            obtainMessage.what = d.cj;
            obtainMessage.arg1 = 0;
            obtainMessage.obj = aVar.iA;
            obtainMessage.sendToTarget();
            k.x = 0;
            aVar.dismiss();
        } else if (result == 4) {
            aVar.c("申请失败，请重新申请", -65536);
            aVar.k();
            k.x = 0;
        } else if (result == 5) {
            aVar.c("安全凭证保存失败，请重申安全凭证", -65536);
            aVar.k();
            k.x = 0;
        } else if (result == 6) {
            aVar.c("申请参数错误，请核对手机号码是否正确", -65536);
            aVar.k();
            k.x = 0;
        } else if (result == 7) {
            aVar.c("申请参数错误，请核对手机号码是否正确", -65536);
            aVar.k();
            k.x = 0;
        }
    }

    private void k() {
        this.c.setClickable(true);
        this.c.setBackgroundDrawable(this.iH);
    }

    private String o() {
        return this.f4a.getText().toString().trim();
    }

    /* access modifiers changed from: protected */
    public final Boolean bD() {
        char c2;
        String obj = this.f4a.getText().toString();
        if (obj != null || obj.trim().length() > 0) {
            i.bF();
            this.f5b.setVisibility(8);
            this.f4a.setTextColor(-11444116);
            c2 = 504;
        } else {
            this.f4a.startAnimation(bE());
            this.f5b.setVisibility(0);
            this.f5b.setTextColor(-65536);
            this.f5b.setText("手机号码不能为空，请输入手机号码！");
            this.f5b.append("\n");
            e();
            c2 = 503;
        }
        if (504 != c2) {
            return false;
        }
        this.iE.requestFocus();
        this.f4a.clearFocus();
        return true;
    }

    public final void e() {
        this.c.setClickable(true);
        this.c.setBackgroundDrawable(this.iH);
    }

    public final void f() {
        this.c.setClickable(false);
        this.c.setBackgroundDrawable(this.iI);
    }

    public final void g() {
        if (this.f5b != null) {
            this.f5b.setVisibility(8);
        }
    }

    public void show() {
        ScrollView scrollView;
        Bitmap n2;
        Bitmap n3;
        Bitmap n4;
        Bitmap n5;
        Bitmap n6;
        Bitmap n7;
        Bitmap n8;
        Bitmap n9;
        if (this.iy == null && (n9 = y.n(d.getContext(), "mmiap/image/vertical/bg.png")) != null) {
            this.iy = new BitmapDrawable(n9);
        }
        if (this.iC == null && (n8 = y.n(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk = n8.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk);
            this.iC = new NinePatchDrawable(n8, ninePatchChunk, new Rect(), null);
        }
        if (this.iK == null && (n7 = y.n(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk2 = n7.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk2);
            this.iK = new NinePatchDrawable(n7, ninePatchChunk2, new Rect(), null);
        }
        if (this.iB == null && (n6 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk3 = n6.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk3);
            this.iB = new NinePatchDrawable(n6, ninePatchChunk3, new Rect(), null);
        }
        if (this.iJ == null && (n5 = y.n(d.getContext(), "mmiap/image/vertical/infobg.9.png")) != null) {
            byte[] ninePatchChunk4 = n5.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk4);
            this.iJ = new NinePatchDrawable(n5, ninePatchChunk4, new Rect(), null);
        }
        if (this.iD == null && (n4 = y.n(d.getContext(), "mmiap/image/vertical/editbg.9.png")) != null) {
            byte[] ninePatchChunk5 = n4.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk5);
            this.iD = new NinePatchDrawable(n4, ninePatchChunk5, new Rect(), null);
        }
        if (this.iH == null && (n3 = y.n(d.getContext(), "mmiap/image/vertical/get_verificationcode.9.png")) != null) {
            byte[] ninePatchChunk6 = n3.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk6);
            this.iH = new NinePatchDrawable(n3, ninePatchChunk6, new Rect(), null);
        }
        if (this.iI == null && (n2 = y.n(d.getContext(), "mmiap/image/vertical/get_verificationcode_press.9.png")) != null) {
            byte[] ninePatchChunk7 = n2.getNinePatchChunk();
            NinePatch.isNinePatchChunk(ninePatchChunk7);
            this.iI = new NinePatchDrawable(n2, ninePatchChunk7, new Rect(), null);
        }
        String str = Build.DEVICE;
        if (str == null) {
            this.T = "我的设备";
        } else {
            this.T = str;
        }
        this.iw = y.iw;
        if (this.iw.booleanValue()) {
            this.iO = new LinearLayout(d.getContext());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            layoutParams.gravity = 1;
            this.iO.setOrientation(1);
            this.iO.setLayoutParams(layoutParams);
            this.iO.addView(K(d.getContext()));
            this.iO.addView(by());
            this.iO.addView(bz());
            this.iO.addView(bA());
            this.iO.addView(bB());
            this.iO.addView(a());
            this.iO.addView(H(d.getContext()));
            this.iO.addView(bC());
            this.iO.addView(L(d.getContext()));
            this.ix = new ScrollView(d.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -1);
            this.ix = new ScrollView(d.getContext());
            this.ix.setLayoutParams(layoutParams2);
            this.ix.setFillViewport(true);
            this.ix.setBackgroundDrawable(this.iy);
            this.ix.addView(this.iO);
            scrollView = this.ix;
        } else {
            this.iO = new LinearLayout(d.getContext());
            LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -1);
            layoutParams3.gravity = 1;
            this.iO.setOrientation(1);
            this.iO.setLayoutParams(layoutParams3);
            this.iO.addView(K(d.getContext()));
            this.iO.addView(by());
            this.iO.addView(bz());
            this.iO.addView(bA());
            this.iO.addView(bB());
            this.iO.addView(a());
            this.iO.addView(H(d.getContext()));
            this.iO.addView(bC());
            this.iO.addView(L(d.getContext()));
            this.ix = new ScrollView(d.getContext());
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -1);
            this.ix = new ScrollView(d.getContext());
            this.ix.setLayoutParams(layoutParams4);
            this.ix.setFillViewport(true);
            this.ix.setBackgroundDrawable(this.iy);
            this.ix.addView(this.iO);
            scrollView = this.ix;
        }
        setContentView(scrollView);
        setCancelable(false);
        super.show();
    }
}
