package mm.purchasesdk.c;

import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;
import mm.purchasesdk.k.g;
import org.apache.http.HttpHost;

public class j {
    private static final String TAG = j.class.getSimpleName();

    public static int a(b bVar) {
        int i;
        if (MMClientSDK_ForPhone.checkSecCert() == 0) {
            return 100;
        }
        try {
            bVar.onBeforeApply();
            String e = g.e(d.getContext());
            HttpHost httpHost = null;
            if (e != null) {
                httpHost = new HttpHost(e, 80, "http");
            }
            String bX = d.bX();
            e.c(TAG, "ip=" + d.ci() + ",port=" + d.cj() + ",appid=" + bX);
            int result = MMClientSDK_ForPhone.SIDSign(bX, httpHost).getResult();
            e.c(TAG, "SidSignature result--〉" + result);
            if (result != 0) {
                e.d(TAG, "applyforCert failure: " + result);
                i = result == 2 ? PurchaseCode.CETRT_SID_ERR : result == 3 ? PurchaseCode.CERT_PKI_ERR : result == 4 ? PurchaseCode.CERT_PUBKEY_ERR : result == 5 ? PurchaseCode.CERT_IMSI_ERR : result == 6 ? PurchaseCode.CERT_NETWORK_FAIL : result == 7 ? PurchaseCode.CERT_SMS_ERR : PurchaseCode.APPLYCERT_OTHER_ERR;
            } else {
                i = 100;
            }
            if (i != 100) {
                return i;
            }
            bVar.onAfterApply();
            return 100;
        } catch (Exception e2) {
            e.a(TAG, "checkPhoneCert exception", e2);
            return PurchaseCode.CERT_EXCEPTION;
        }
    }

    public static int d() {
        return MMClientSDK_ForPad.checkSecCert_PAD() == 0 ? 100 : 6;
    }
}
