package mm.purchasesdk.c;

import android.os.Message;
import android.view.View;
import java.util.HashMap;
import mm.purchasesdk.PurchaseCode;

class c implements View.OnClickListener {
    final /* synthetic */ a iS;

    c(a aVar) {
        this.iS = aVar;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case 3:
                if (this.iS.bD().booleanValue()) {
                    this.iS.f();
                    a.a(this.iS, view);
                    a.c(this.iS);
                    return;
                }
                return;
            case 4:
                if (a.f(this.iS).booleanValue()) {
                    a.g(this.iS);
                    return;
                }
                return;
            case 5:
                k.x = 0;
                this.iS.dismiss();
                this.iS.iA.a((int) PurchaseCode.CERT_REQUEST_CANCEL);
                this.iS.iA.a((HashMap) null);
                Message obtainMessage = this.iS.b.obtainMessage();
                obtainMessage.what = 5;
                obtainMessage.obj = this.iS.iA;
                obtainMessage.sendToTarget();
                return;
            default:
                return;
        }
    }
}
