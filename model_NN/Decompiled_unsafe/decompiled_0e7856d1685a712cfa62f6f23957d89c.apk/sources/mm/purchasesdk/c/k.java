package mm.purchasesdk.c;

import android.os.Handler;
import android.os.Message;
import android.widget.Button;

public class k extends Handler {
    public static int x = 0;
    private final String U = "获取授权码";
    private Button d;
    private Object iT;

    public k(int i, Object obj) {
        x = 60;
        this.iT = obj;
    }

    private void b(Button button) {
        if (this.iT instanceof a) {
            ((a) this.iT).e();
            ((a) this.iT).g();
        }
        button.setText("获取授权码");
    }

    public final void a(Button button) {
        this.d = button;
    }

    public void handleMessage(Message message) {
        super.handleMessage(message);
        int i = message.what;
        if (x < 0) {
            b(this.d);
            return;
        }
        switch (i) {
            case 30001:
                this.d.setText(x + "s");
                x--;
                return;
            case 30002:
                b(this.d);
                return;
            default:
                return;
        }
    }
}
