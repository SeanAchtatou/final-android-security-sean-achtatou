package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class b implements View.OnTouchListener {
    final /* synthetic */ a iS;

    b(a aVar) {
        this.iS = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        switch (view.getId()) {
            case 0:
                this.iS.iE.clearFocus();
                this.iS.f4a.requestFocus();
                return false;
            case 1:
                this.iS.iE.requestFocus();
                this.iS.f4a.clearFocus();
                return false;
            default:
                return false;
        }
    }
}
