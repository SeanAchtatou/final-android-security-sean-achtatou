package mm.purchasesdk.c;

import android.view.MotionEvent;
import android.view.View;

class e implements View.OnTouchListener {
    final /* synthetic */ a iS;

    e(a aVar) {
        this.iS = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.iS.iI);
            return false;
        }
        view.setBackgroundDrawable(this.iS.iH);
        return false;
    }
}
