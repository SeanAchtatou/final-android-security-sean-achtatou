package mm.purchasesdk.k;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.a.a.h.b;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.ui.y;

public class d {
    private static String E = "";
    private static String Q = b.SMS_RESULT_SEND_SUCCESS;
    private static int cV = 1;
    public static int cj = -1;
    private static boolean jE = false;
    private static String jF = "";
    private static String jG = "";
    private static String jH = "";
    private static String jI = "";
    private static String jJ = "";
    private static Boolean jK = false;
    private static Boolean jL = true;
    private static Boolean jM = false;
    private static String jN = null;
    private static String jO = b.SMS_RESULT_SEND_FAIL;
    private static String jP = "";
    public static float jQ;
    public static int jR;
    public static int jS;
    private static String jT = "";
    private static String jU = "";
    private static boolean jV = true;
    private static String jW = "";
    private static String jX = "";
    private static String jY = "";
    private static Boolean jZ = false;
    private static boolean k = false;
    private static Context mContext;

    public static String G(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.e("MMBillingSDk", "network not exists, pls check network");
            return null;
        }
        activeNetworkInfo.getExtraInfo();
        return "http://ospd.mmarket.com:8089/trust";
    }

    public static void Q() {
        reset();
        y.O();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) mContext.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        jQ = displayMetrics.density;
        jS = displayMetrics.heightPixels;
        int i = displayMetrics.widthPixels;
        jR = i;
        y.lF = i;
        if (jQ < 1.0f) {
            y.lO = 5;
        }
        if (mContext.getResources().getConfiguration().orientation == 2) {
            y.lM = ((float) jS) / 480.0f;
            y.lP = 39.0f;
            y.lQ = (int) ((39.0f * ((float) jS)) / 100.0f);
            y.lL = 31.0f;
            y.lG = (int) ((31.0f * ((float) jS)) / 100.0f);
            float f = (float) ((jR * 100) / 800);
            y.lN = f;
            y.lU = (int) ((f * ((float) y.lU)) / 100.0f);
            y.lT = (int) ((y.lN * ((float) y.lT)) / 100.0f);
            y.lO = (int) ((y.lN * ((float) y.lO)) / 100.0f);
            y.lS = (int) ((y.lN * ((float) y.lS)) / 100.0f);
            y.iw = true;
            if (jQ == 1.0f) {
                y.lX *= y.lM;
                y.lV = (y.lV * y.lN) / 100.0f;
                y.lW *= y.lM;
                return;
            }
            return;
        }
        y.lM = ((float) jS) / 800.0f;
        y.lP = 42.0f;
        y.lQ = (int) ((42.0f * ((float) jS)) / 100.0f);
        y.lL = 35.0f;
        y.lG = (int) ((35.0f * ((float) jS)) / 100.0f);
        float f2 = (float) ((jS * 100) / 800);
        y.lN = f2;
        y.lU = (int) ((f2 * ((float) y.lU)) / 100.0f);
        y.lT = (int) ((y.lN * ((float) y.lT)) / 100.0f);
        y.lO = (int) ((y.lN * ((float) y.lO)) / 100.0f);
        y.lS = (int) ((y.lN * ((float) y.lS)) / 100.0f);
        if (jQ == 1.0f) {
            y.lX *= y.lM;
            y.lV = (y.lV * y.lN) / 100.0f;
            y.lW *= y.lM;
        }
        y.iw = false;
    }

    public static int a() {
        return cV;
    }

    public static void a(Boolean bool) {
        jK = bool;
    }

    public static void ae(String str) {
        Q = str;
    }

    public static void af(String str) {
        jP = str;
    }

    public static String ag(String str) {
        jT = str;
        return str;
    }

    public static void ah(String str) {
        jU = str;
    }

    public static void ai(String str) {
        jN = str;
    }

    public static void aj(String str) {
        jO = str;
    }

    public static void ak(String str) {
        jX = str;
    }

    public static void al(String str) {
        jY = str;
    }

    public static void am(String str) {
        jI = str;
    }

    public static void an(String str) {
        jH = str;
    }

    public static void ao(String str) {
        jJ = str;
    }

    public static void ap(String str) {
        if (str.trim().equals(b.SMS_RESULT_SEND_SUCCESS)) {
            jZ = false;
        } else {
            jZ = true;
        }
    }

    public static String b() {
        return E;
    }

    public static void b(String str) {
        E = str;
    }

    public static void b(String str, String str2) {
        jF = str;
        jG = str2;
    }

    public static String bT() {
        return jP;
    }

    public static Boolean bU() {
        return jM;
    }

    public static Boolean bV() {
        return jL;
    }

    public static Boolean bW() {
        return jK;
    }

    public static String bX() {
        return jF;
    }

    public static String bY() {
        return jG;
    }

    public static String bZ() {
        return jT;
    }

    public static Boolean bu() {
        return jZ;
    }

    public static String ca() {
        return jV ? jM.booleanValue() ? b.SMS_RESULT_SEND_FAIL : b.SMS_RESULT_SEND_CANCEL : b.SMS_RESULT_SEND_NOTIFIED;
    }

    public static String cb() {
        return jN;
    }

    public static String cc() {
        return jO;
    }

    public static void cd() {
    }

    public static String ce() {
        String subscriberId = ((TelephonyManager) mContext.getSystemService("phone")).getSubscriberId();
        jW = subscriberId;
        if (subscriberId == null) {
            jW = "10086";
        }
        e.a(0, "MMBillingSDk", "Imsi-->" + jW);
        return jW;
    }

    public static String cf() {
        return jX;
    }

    public static String cg() {
        return jY;
    }

    public static String ch() {
        e.a(0, "MMBillingSDk", "Copyright url-->" + "http://ospd.mmarket.com:8089/taac");
        return "http://ospd.mmarket.com:8089/taac";
    }

    public static String ci() {
        return "ospd.mmarket.com";
    }

    public static String cj() {
        return "8089";
    }

    public static String ck() {
        return "MIICZzCCAdKgAwIBAgIDNJv2MAsGCSqGSIb3DQEBBTAzMQswCQYDVQQGEwJDTjEkMCIGA1UEAwwbQ01DQSBhcHBsaWNhdGlvbiBzaWduaW5nIENBMB4XDTExMDMyNDAyMjExOFoXDTMxMDMyNDAyMjExOFowXjELMAkGA1UEBhMCQ04xMzAxBgNVBAMMKuS4reWbveenu+WKqOe7iOerr+W6lOeUqOeJiOadg+S/neaKpOWjsOaYjjEaMBgGA1UEBRMRMjAxMTAzMjQxMDI0MjIyMjUwgZ0wCwYJKoZIhvcNAQEBA4GNADCBiQKBgQDb7UlB5k4kdWACNBmHM+Dw9NSD0Q4o7CR3gTaciZQlXeoCCwuYSAWuhoI5ujQsM47eH12OlIn2IwKYObwa6iVY6CLVnEhPkqQfLXPNCoOI+fFdKqLO1YD0+RRj+4oUXi7vAVBEASeyhZesT8P6m2nPpiExlZjDqJYzX/MKYcIkvwIDAQABo2QwYjATBgNVHSUEDDAKBggrBgEFBQcDAzAfBgNVHSMEGDAWgBSXIbIlzOk/0qZTaEGW5ldxZ9uyjTALBgNVHQ8EBAMCB4AwHQYDVR0OBBYEFDTw9zOSP/ZHrahKl9qApKmRNJZiMAsGCSqGSIb3DQEBBQOBgQB6KJgdTQoNXy4xErgbtiRXz7L+J05HM3K6ZFBUE4/cOFcEXiEuu2YekT+pAZcPm2A6iRdYSKo7LCMIDEZUXdMKzTzkxmk39wy05QAyS6QjW8AWp9A9ufvd741IOnjnRGfN4hzuxPjRHEG86T/+nkmYkVgl7gfLJ7mBpyRNKkzIDg==";
    }

    public static String cl() {
        return IdentifyApp.generateTransactionID(jF, jT, jX, ce());
    }

    public static String cm() {
        return jI;
    }

    public static String cn() {
        return jH;
    }

    public static String co() {
        return jJ;
    }

    public static String cp() {
        String ce = ce();
        return (ce == null || ce.trim().length() <= 6) ? "" : ce.substring(0, 5);
    }

    public static String cq() {
        String str = Build.MODEL;
        return (str == null || str.trim().length() <= 0) ? "" : str;
    }

    public static String cr() {
        String str = Build.VERSION.RELEASE;
        return (str == null || str.trim().length() <= 0) ? "" : str;
    }

    public static String d(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            e.e("MMBillingSDk", "network not exists, pls check network");
            return null;
        }
        activeNetworkInfo.getExtraInfo();
        return "http://ospd.mmarket.com:8089/trusted3";
    }

    public static void d(Boolean bool) {
        jM = bool;
    }

    public static boolean d() {
        return jV;
    }

    public static void e(Boolean bool) {
        jL = bool;
    }

    public static boolean e() {
        return k;
    }

    public static void f(boolean z) {
        jV = z;
    }

    public static Context getContext() {
        return mContext;
    }

    public static void h(int i) {
        cV = i;
    }

    public static String n() {
        return Q;
    }

    public static String r() {
        return jU;
    }

    private static void reset() {
        y.lU = 10;
        y.lT = 10;
        y.lO = 10;
        y.lS = 20;
        y.lP = 1.0f;
        y.lM = 1.0f;
        y.lR = 16.0f;
        y.lV = 15.0f;
        y.lW = 16.0f;
        y.lX = 18.0f;
    }

    public static void setContext(Context context) {
        mContext = context;
    }

    public static synchronized void unlock() {
        synchronized (d.class) {
            jE = false;
            cj = -1;
            mContext = null;
            jI = "";
            cV = 1;
            jH = "";
            E = "";
            jT = "";
            Q = b.SMS_RESULT_SEND_SUCCESS;
            jK = false;
            jL = false;
            jM = true;
            jZ = false;
            reset();
        }
    }

    public static synchronized boolean v(int i) {
        boolean z = true;
        synchronized (d.class) {
            if (!jE) {
                jE = true;
                cj = i;
            } else {
                z = false;
            }
        }
        return z;
    }
}
