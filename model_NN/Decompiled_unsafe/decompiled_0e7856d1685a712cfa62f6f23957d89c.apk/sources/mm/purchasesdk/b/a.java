package mm.purchasesdk.b;

import android.graphics.Bitmap;

public class a {
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String F;
    private int d = 1;

    /* renamed from: d  reason: collision with other field name */
    private boolean f3d;
    private boolean e = false;
    public boolean f = false;
    private boolean g = true;
    private Bitmap ip;
    private Boolean iq = true;
    private Boolean ir = false;
    private Boolean is = false;
    private e it;
    private String m;
    private String p;
    private String r;
    private String z = "";

    public final int a() {
        return this.d;
    }

    public final void a(e eVar) {
        this.it = eVar;
    }

    public final void a(boolean z2) {
        this.g = z2;
    }

    public final void b(Bitmap bitmap) {
        this.ip = bitmap;
    }

    public final void b(Boolean bool) {
        this.is = bool;
    }

    public final void b(boolean z2) {
        this.f3d = z2;
    }

    public final boolean b() {
        return this.f3d;
    }

    public final Boolean bs() {
        return this.iq;
    }

    public final e bv() {
        return this.it;
    }

    public final Bitmap bw() {
        return this.ip;
    }

    public final String c() {
        return this.C;
    }

    public final void c(Boolean bool) {
        this.iq = bool;
    }

    /* renamed from: c  reason: collision with other method in class */
    public final boolean m1c() {
        return this.e;
    }

    public final String d() {
        return this.D;
    }

    public final void e(String str) {
        this.C = str;
    }

    public final void f(String str) {
        this.D = str;
    }

    public final String g() {
        return this.p;
    }

    public final String h() {
        return this.r;
    }

    public final void j(String str) {
        this.p = str;
    }

    public final void l(String str) {
        this.r = str;
    }

    public final void s(String str) {
        this.m = str;
    }

    public final void t(String str) {
        this.F = str;
    }
}
