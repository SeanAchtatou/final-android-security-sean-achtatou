package mm.purchasesdk.b;

import android.os.Bundle;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.d;
import mm.purchasesdk.f.c;
import mm.purchasesdk.f.e;
import mm.purchasesdk.k.a;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;

public class b {
    static final String TAG = b.class.getSimpleName();

    public static int b(Bundle bundle) {
        String str;
        String str2 = null;
        ArrayList arrayList = new ArrayList();
        arrayList.add(new e());
        c cVar = new c();
        if (bundle != null) {
            cVar.v(bundle.getString("dyMark"));
            cVar.w(bundle.getString("CheckAnswer"));
            cVar.l(bundle.getString("CheckId"));
            cVar.S(bundle.getString("Password"));
            cVar.u(bundle.getString("RandomPwd"));
            cVar.j(bundle.getString("SessionId"));
            cVar.f(bundle.getInt("OrderCount"));
            cVar.c(bundle.getBoolean("multiSubs"));
            cVar.d(bundle.getBoolean("NeedPasswd"));
            cVar.e(bundle.getBoolean("NeedInput"));
        }
        d dVar = new d();
        Iterator it = arrayList.iterator();
        while (true) {
            if (!it.hasNext()) {
                str = str2;
                break;
            }
            try {
                str2 = ((c) it.next()).b(cVar, dVar);
                if (str2 != null) {
                    str = str2;
                    break;
                }
            } catch (d e) {
                PurchaseCode.setStatusCode(PurchaseCode.NONE_NETWORK);
                return PurchaseCode.NONE_NETWORK;
            }
        }
        if (str == null || dVar.bM() == null) {
            return 0;
        }
        int intValue = Integer.valueOf(dVar.bM()).intValue();
        mm.purchasesdk.k.e.c(TAG, "billing code:" + intValue);
        if ((intValue == 0 || intValue == 1) && !a.ad(str).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
            return PurchaseCode.RESPONSE_ERR;
        }
        switch (intValue) {
            case 0:
                String b = mm.purchasesdk.a.e.b(str);
                if (b == null || b.length() == 0) {
                    mm.purchasesdk.k.e.e(TAG, "no license file");
                    return mm.purchasesdk.k.d.cc().equals(com.a.a.h.b.SMS_RESULT_RECV_SUCCESS) ? PurchaseCode.BILL_THIRDTYPE_PAY : PurchaseCode.AUTH_PARSE_FAIL;
                }
                int c = mm.purchasesdk.a.e.c(b, mm.purchasesdk.e.b.bH().iU.ja, dVar.k());
                if (c != 104) {
                    return c;
                }
                bundle.putString(OnPurchaseListener.ORDERID, dVar.k());
                bundle.putString(OnPurchaseListener.LEFTDAY, dVar.b());
                bundle.putString(OnPurchaseListener.ORDERTYPE, dVar.n());
                return c;
            case 1:
                return PurchaseCode.BILL_CHECKCODE_ERROR;
            case 11:
                return PurchaseCode.BILL_INVALID_SESSION;
            case 12:
                return PurchaseCode.BILL_CSSP_BUSY;
            case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
                return PurchaseCode.BILL_INTERNAL_FAIL;
            case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
                try {
                    MMClientSDK_ForPhone.DestorySecCert(null);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return PurchaseCode.BILL_INVALID_USER;
            case DefaultVirtualDevice.WIDGET_TYPE_DYNAMICJOYSTICK /*15*/:
                return PurchaseCode.BILL_INVALID_APP;
            case 16:
                return PurchaseCode.BILL_LICENSE_ERROR;
            case 17:
                return PurchaseCode.BILL_INVALID_SIGN;
            case 18:
                return PurchaseCode.BILL_NO_ABILITY;
            case 19:
                return PurchaseCode.BILL_NO_APP;
            case 36:
                return PurchaseCode.BILL_PW_FAIL;
            case 38:
                return PurchaseCode.BILL_SMSCODE_ERROR;
            case com.a.a.d.b.KEY_STAR:
                return PurchaseCode.BILL_DYMARK_ERROR;
            case PurchaseCode.QUERY_OK:
                return PurchaseCode.BILL_NO_BUSINESS;
            default:
                return PurchaseCode.BILL_UNDEFINED_ERROR;
        }
    }
}
