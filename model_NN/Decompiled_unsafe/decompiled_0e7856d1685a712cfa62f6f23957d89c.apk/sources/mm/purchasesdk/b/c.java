package mm.purchasesdk.b;

import android.util.Xml;
import com.a.a.h.b;
import java.io.StringWriter;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.g.e;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlSerializer;

public class c extends e {
    private String G;
    private String H;
    private String I;
    private String J = " ";
    private String K = b.SMS_RESULT_SEND_SUCCESS;
    private final String TAG = c.class.getSimpleName();
    private int count = 1;
    private boolean e = false;
    private boolean h = false;
    private boolean i = false;
    private String p;
    private String r;
    private int statusCode;

    public final void S(String str) {
        this.H = str;
    }

    public final String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(com.umeng.common.b.e.f, true);
            newSerializer.startTag("", "Trusted3SubscribeReq");
            newSerializer.startTag("", "MsgType");
            newSerializer.text("Trusted3SubscribeReq");
            newSerializer.endTag("", "MsgType");
            newSerializer.startTag("", "Version");
            newSerializer.text(d.co());
            newSerializer.endTag("", "Version");
            newSerializer.startTag("", "SessionID");
            newSerializer.text(this.p);
            newSerializer.endTag("", "SessionID");
            newSerializer.startTag("", "AppID");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "AppID");
            newSerializer.startTag("", "BracketID");
            newSerializer.text("");
            newSerializer.endTag("", "BracketID");
            newSerializer.startTag("", "PayCode");
            newSerializer.text(d.bZ());
            newSerializer.endTag("", "PayCode");
            newSerializer.startTag("", "CheckID");
            newSerializer.text(this.r);
            newSerializer.endTag("", "CheckID");
            newSerializer.startTag("", "CheckAnswer");
            newSerializer.text(this.G);
            newSerializer.endTag("", "CheckAnswer");
            newSerializer.startTag("", "PayPwd");
            newSerializer.text(IdentifyApp.encryptPassword(this.H, this.p));
            newSerializer.endTag("", "PayPwd");
            newSerializer.startTag("", "SubsNumb");
            newSerializer.text(String.valueOf(this.count));
            newSerializer.endTag("", "SubsNumb");
            newSerializer.startTag("", "RandomPwd");
            newSerializer.text(this.J);
            newSerializer.endTag("", "RandomPwd");
            newSerializer.startTag("", "DynamicMark");
            newSerializer.text(this.I);
            newSerializer.endTag("", "DynamicMark");
            newSerializer.startTag("", "programHash");
            newSerializer.text("");
            newSerializer.endTag("", "programHash");
            newSerializer.startTag("", "imsi");
            newSerializer.text(d.ce());
            newSerializer.endTag("", "imsi");
            newSerializer.startTag("", "imei");
            newSerializer.text(d.cf());
            newSerializer.endTag("", "imei");
            newSerializer.startTag("", "ChannelID");
            newSerializer.text(d.r());
            newSerializer.endTag("", "ChannelID");
            newSerializer.startTag("", "PayWay");
            newSerializer.text(d.cc());
            newSerializer.endTag("", "PayWay");
            newSerializer.startTag("", "UserType");
            newSerializer.text(d.ca());
            newSerializer.endTag("", "UserType");
            newSerializer.startTag("", "sidSignature");
            newSerializer.text(bL().getUserSignature());
            newSerializer.endTag("", "sidSignature");
            newSerializer.endTag("", "Trusted3SubscribeReq");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e2) {
            mm.purchasesdk.k.e.a(this.TAG, "create BillingRequest xml file failed!!", e2);
            this.statusCode = PurchaseCode.XML_EXCPTION_ERROR;
            return null;
        }
    }

    public final void c(boolean z) {
        this.e = z;
    }

    public final void d(boolean z) {
        this.h = z;
    }

    public final void e(boolean z) {
        this.i = z;
    }

    public final void f(int i2) {
        this.count = i2;
    }

    public final void j(String str) {
        this.p = str;
    }

    public final void l(String str) {
        this.r = str;
    }

    public final void u(String str) {
        this.J = str;
    }

    public final void v(String str) {
        this.I = str;
    }

    public final void w(String str) {
        this.G = str;
    }
}
