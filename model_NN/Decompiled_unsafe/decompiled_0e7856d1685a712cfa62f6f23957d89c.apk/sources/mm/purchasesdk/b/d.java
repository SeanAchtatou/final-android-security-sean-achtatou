package mm.purchasesdk.b;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.g.f;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String E;
    private String L;
    private String M;
    private String N;
    private String O;
    private String P;
    private String Q;
    private final String TAG = d.class.getSimpleName();

    public final String b() {
        return this.E;
    }

    public final String n() {
        return this.Q;
    }

    public final boolean parse(String str) {
        if (str == null || str.length() <= 0) {
            return false;
        }
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"CheckID".equals(name)) {
                            if (!"CheckCode".equals(name)) {
                                if (!"OrderID".equals(name)) {
                                    if (!"randNum".equals(name)) {
                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                            if (!"RespMd5".equals(name)) {
                                                if (!"OnderValidDate".equals(name)) {
                                                    if (!"redirect_url".equals(name)) {
                                                        if (!OnPurchaseListener.ORDERTYPE.equals(name)) {
                                                            break;
                                                        } else {
                                                            this.Q = newPullParser.nextText();
                                                            break;
                                                        }
                                                    } else {
                                                        mm.purchasesdk.k.d.af(newPullParser.nextText());
                                                        break;
                                                    }
                                                } else {
                                                    this.E = newPullParser.nextText();
                                                    break;
                                                }
                                            } else {
                                                this.P = newPullParser.nextText();
                                                break;
                                            }
                                        } else {
                                            this.O = newPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        this.N = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    p(newPullParser.nextText());
                                    break;
                                }
                            } else {
                                this.M = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.L = newPullParser.nextText();
                            break;
                        }
                    } else {
                        X(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }
}
