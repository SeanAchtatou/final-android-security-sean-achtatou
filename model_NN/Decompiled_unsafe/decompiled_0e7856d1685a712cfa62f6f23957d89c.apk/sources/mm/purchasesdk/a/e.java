package mm.purchasesdk.a;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.fingerprint.IdentifyApp;
import mm.purchasesdk.fingerprint.a;
import mm.purchasesdk.fingerprint.b;
import mm.purchasesdk.fingerprint.c;
import mm.purchasesdk.k.d;

public class e {
    private static final String TAG = e.class.getSimpleName();

    private static boolean a(String str, String str2) {
        mm.purchasesdk.k.e.c(TAG, "verificationAuthSign certificate is: " + str2);
        int indexOf = str.indexOf("<Signature_content>");
        int indexOf2 = str.indexOf("</Signature_content>") + 20;
        if (indexOf < 0) {
            return false;
        }
        String substring = str.substring(indexOf, indexOf2);
        mm.purchasesdk.k.e.c(TAG, "verificationAuthSign signContent is: " + substring);
        int indexOf3 = str.indexOf("<SignatureValue>");
        if (indexOf3 < 0) {
            return false;
        }
        String substring2 = str.substring(indexOf3 + 16, str.indexOf("</SignatureValue>"));
        mm.purchasesdk.k.e.c(TAG, "verificationAuthSign signValue is: " + substring2);
        try {
            int a = a.a((int) PurchaseCode.AUTH_NO_ABILITY, str2.getBytes(), substring.getBytes(), substring2.getBytes());
            mm.purchasesdk.k.e.c(TAG, "verificationAuthSign verifyWithCert result is: " + a);
            return a == 0;
        } catch (b e) {
            mm.purchasesdk.k.e.a(TAG, "verify failed", e);
            return false;
        } catch (Exception e2) {
            mm.purchasesdk.k.e.e(TAG, "base64 decrypt license file failure");
            return false;
        }
    }

    public static String b(String str) {
        String substring = str.substring("<MM_User_Authorization>".length() + str.indexOf("<MM_User_Authorization>"), str.indexOf("</MM_User_Authorization>"));
        mm.purchasesdk.k.e.c(TAG, "before ndk decode: " + substring);
        return substring;
    }

    private static void b(long j) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTimeInMillis(j);
        mm.purchasesdk.k.e.c(TAG, "timer: " + new SimpleDateFormat().format(gregorianCalendar.getTime()));
    }

    public static int c(String str, String str2, String str3) {
        String str4;
        boolean z = true;
        mm.purchasesdk.f.b bVar = new mm.purchasesdk.f.b();
        String str5 = d.bX() + "&" + d.ce();
        mm.purchasesdk.k.e.c(TAG, "KEY--〉" + str5);
        try {
            byte[] decrypt = IdentifyApp.decrypt(c.base64decode(str), new String(str5));
            if (decrypt == null) {
                mm.purchasesdk.k.e.e(TAG, "Base64 decrypt license file failure,auth file is null,code=241");
                str4 = str;
            } else {
                str4 = "<MM_User_Authorization>" + new String(decrypt) + "</MM_User_Authorization>";
                mm.purchasesdk.k.e.c(TAG, "after ndk decode: " + str4);
            }
            try {
                mm.purchasesdk.k.e.c(TAG, str4);
                a c = b.c(str4.getBytes(), "utf-8");
                if (c == null) {
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                }
                if (!(d.bZ().equals(c.m) && d.bX().equals(c.f) && d.ce().equals(c.d))) {
                    bVar.a(d.bX(), d.bZ(), d.ce());
                    mm.purchasesdk.k.e.e(TAG, "Auth validate failed! paycode or appid or imsi error,code=242");
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                } else if (!a(str4, str2)) {
                    bVar.a(d.bX(), d.bZ(), d.ce());
                    mm.purchasesdk.k.e.e(TAG, "Auth validate failed! verify auth failed,code=242");
                    return PurchaseCode.AUTH_VALIDATE_FAIL;
                } else {
                    long j = c.il;
                    long timeInMillis = Calendar.getInstance().getTimeInMillis();
                    b(j);
                    b(timeInMillis);
                    if (timeInMillis < j) {
                        bVar.a(d.bX(), d.bZ(), d.ce());
                    }
                    if (c.il - c.ik <= 10000) {
                        z = false;
                    }
                    b(c.il);
                    b(c.ik);
                    if (d.e() && z) {
                        bVar.a(d.bX(), d.bZ(), str, c.bq(), c.bp(), str3, d.ce());
                    }
                    return PurchaseCode.AUTH_OK;
                }
            } catch (Exception e) {
                mm.purchasesdk.k.e.a(TAG, "parsed auth xml file failed!", e);
                bVar.a(d.bX(), d.bZ(), d.ce());
                return PurchaseCode.AUTH_PARSE_FAIL;
            }
        } catch (Exception e2) {
            mm.purchasesdk.k.e.e(TAG, "base64 decrypt license file failure");
            return PurchaseCode.AUTH_PARSE_FAIL;
        }
    }
}
