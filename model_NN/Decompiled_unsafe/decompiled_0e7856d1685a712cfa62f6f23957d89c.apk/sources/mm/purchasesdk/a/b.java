package mm.purchasesdk.a;

import android.graphics.BitmapFactory;
import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.b.a;
import mm.purchasesdk.f.c;
import mm.purchasesdk.f.e;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class b {
    private static final String TAG = b.class.getSimpleName();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void */
    public static Boolean a(a aVar) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new mm.purchasesdk.f.b());
        arrayList.add(new e());
        c cVar = new c();
        d dVar = new d();
        cVar.m0a(new StringBuilder().append(d.a()).toString());
        cVar.a(d.bW());
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            try {
                if (((c) it.next()).a(cVar, dVar)) {
                    d.an(dVar.k());
                    d.am(dVar.e());
                    d.b(dVar.b());
                    return true;
                } else if (dVar.bM() == null) {
                    continue;
                } else if (!dVar.bt().booleanValue()) {
                    return null;
                } else {
                    String f = dVar.f();
                    mm.purchasesdk.k.e.c(TAG, "dyQuestion-->" + f);
                    if (f == null || f.equals("")) {
                        mm.purchasesdk.fingerprint.c.jk = false;
                    } else {
                        mm.purchasesdk.fingerprint.c.jk = true;
                        mm.purchasesdk.fingerprint.c.m2a(f, dVar.g());
                    }
                    Boolean bs = dVar.bs();
                    try {
                        byte[] base64decode = mm.purchasesdk.fingerprint.c.base64decode(new String(dVar.i().getBytes()));
                        aVar.b(BitmapFactory.decodeByteArray(base64decode, 0, base64decode.length));
                        aVar.l(dVar.h());
                        aVar.j(dVar.g());
                        aVar.b(dVar.j().equals(com.a.a.h.b.SMS_RESULT_SEND_FAIL));
                        aVar.b(dVar.bu());
                        aVar.s(d.bZ());
                        aVar.t(mm.purchasesdk.e.b.bH().iU.ja);
                        aVar.c(bs);
                        aVar.a(d.d());
                        mm.purchasesdk.b.e br = dVar.br();
                        if (br == null) {
                            PurchaseCode.setStatusCode(PurchaseCode.AUTH_PRODUCT_ERROR);
                            return null;
                        }
                        aVar.a(br);
                        aVar.e(dVar.c());
                        aVar.f(dVar.d());
                        return false;
                    } catch (Exception e) {
                        mm.purchasesdk.k.e.e(TAG, "base 64 decrypt license file failure" + e.toString());
                        return null;
                    }
                }
            } catch (mm.purchasesdk.d e2) {
                PurchaseCode.setStatusCode(PurchaseCode.NETWORKTIMEOUT_ERR);
                mm.purchasesdk.k.e.e(TAG, "checkAuth failure " + e2.toString());
            }
        }
        return null;
    }

    private static a a(XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        a aVar = null;
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (!"MM_User_Authorization".equals(name)) {
                        if (!"IMEI".equals(name)) {
                            if (!"IMSI".equals(name)) {
                                if (!"mobile_phone".equals(name)) {
                                    if (!"APPUID".equals(name)) {
                                        if (!"cotnentID".equals(name)) {
                                            if (!"PayCode".equals(name)) {
                                                if (!"order_time".equals(name)) {
                                                    if (!"notBeforetime".equals(name)) {
                                                        if (!"notAftertime".equals(name)) {
                                                            if (!"digestAlg".equals(name)) {
                                                                if (!"digest".equals(name)) {
                                                                    if (!"timestamp".equals(name)) {
                                                                        if (!"SignatureValue".equals(name)) {
                                                                            break;
                                                                        } else {
                                                                            aVar.l = xmlPullParser.nextText();
                                                                            break;
                                                                        }
                                                                    } else {
                                                                        aVar.k = xmlPullParser.nextText();
                                                                        break;
                                                                    }
                                                                } else {
                                                                    aVar.j = xmlPullParser.nextText();
                                                                    break;
                                                                }
                                                            } else {
                                                                aVar.i = xmlPullParser.nextText();
                                                                break;
                                                            }
                                                        } else {
                                                            aVar.il = new SimpleDateFormat("yyyyMMddHHmmss").parse(xmlPullParser.nextText()).getTime();
                                                            mm.purchasesdk.k.e.c("AUTH_XML", "after: " + aVar.il);
                                                            break;
                                                        }
                                                    } else {
                                                        aVar.ik = new SimpleDateFormat("yyyyMMddHHmmss").parse(xmlPullParser.nextText()).getTime();
                                                        mm.purchasesdk.k.e.c("AUTH_XML", "before: " + aVar.ik);
                                                        break;
                                                    }
                                                } else {
                                                    aVar.h = xmlPullParser.nextText();
                                                    break;
                                                }
                                            } else {
                                                aVar.m = xmlPullParser.nextText();
                                                break;
                                            }
                                        } else {
                                            aVar.g = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.f = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.e = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.d = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.c = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar = new a();
                        break;
                    }
            }
            eventType = xmlPullParser.next();
        }
        if (aVar == null || aVar.f != null) {
            return aVar;
        }
        return null;
    }

    public static a c(byte[] bArr, String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(bArr), str);
        return a(newPullParser);
    }
}
