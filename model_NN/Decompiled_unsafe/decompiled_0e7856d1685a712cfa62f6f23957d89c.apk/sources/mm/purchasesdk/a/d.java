package mm.purchasesdk.a;

import com.a.a.h.b;
import com.umeng.common.a;
import java.io.ByteArrayInputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.b.e;
import mm.purchasesdk.g.f;
import mm.purchasesdk.ui.z;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String A = "";
    private String B = "";
    private String C = "";
    private String D = "";
    private String E = "";
    private e im = null;
    private String p;
    private String q;
    private String r;
    private String s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y = b.SMS_RESULT_SEND_SUCCESS;
    private String z = "";

    private static e R(String str) {
        e eVar = new e();
        Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str.getBytes())).getDocumentElement();
        NodeList elementsByTagName = documentElement.getElementsByTagName(a.b);
        for (int i = 0; i < elementsByTagName.getLength(); i++) {
            eVar.g(new Integer(((Element) elementsByTagName.item(i)).getAttribute("id")).intValue());
        }
        NodeList elementsByTagName2 = documentElement.getElementsByTagName("mobile");
        for (int i2 = 0; i2 < elementsByTagName2.getLength(); i2++) {
            eVar.T(((Element) elementsByTagName2.item(i2)).getAttribute("id"));
        }
        NodeList elementsByTagName3 = documentElement.getElementsByTagName("Item");
        for (int i3 = 0; i3 < elementsByTagName3.getLength(); i3++) {
            Element element = (Element) elementsByTagName3.item(i3);
            String attribute = element.getAttribute("id");
            String attribute2 = element.getAttribute("name");
            String attribute3 = element.getAttribute("value");
            z zVar = new z();
            zVar.lY = attribute;
            zVar.bM = attribute2 + ":";
            zVar.mValue = attribute3;
            if (zVar.lY.equals("totalprice") || zVar.lY.equals("itemcount") || zVar.lY.equals("itemprice") || zVar.lY.equals("renttime")) {
                zVar.ma = -39424;
            } else {
                zVar.ma = -16777216;
            }
            eVar.U(attribute2);
            eVar.a(attribute2, zVar);
        }
        return eVar;
    }

    public final String b() {
        return this.E;
    }

    public final e br() {
        return this.im;
    }

    public final Boolean bs() {
        return !bM().equals("6");
    }

    public final Boolean bt() {
        return bM().equals(b.SMS_RESULT_SEND_CANCEL) || bM().equals("6");
    }

    public final Boolean bu() {
        return !this.y.equals(b.SMS_RESULT_SEND_SUCCESS);
    }

    public final String c() {
        return this.C;
    }

    public final String d() {
        return this.D;
    }

    public final String e() {
        return this.B;
    }

    public final String f() {
        mm.purchasesdk.k.e.c("AuthResponse", "Dynamic Question->" + this.w);
        return this.w;
    }

    public final String g() {
        return this.p;
    }

    public final String h() {
        return this.r;
    }

    public final String i() {
        return this.s;
    }

    public final String j() {
        return this.t;
    }

    public final String k() {
        return this.x;
    }

    public final void p(String str) {
        this.x = str;
    }

    public final boolean parse(String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"SessionID".equals(name)) {
                            if (!"FeeRemind".equals(name)) {
                                if (!"CheckID".equals(name)) {
                                    if (!"CheckCode".equals(name)) {
                                        if (!"PayPassStatus".equals(name)) {
                                            if (!"SessionID".equals(name)) {
                                                if (!"MultiSubs".equals(name)) {
                                                    if (!"OrderID".equals(name)) {
                                                        if (!"randNum".equals(name)) {
                                                            if (!"DynamicQuest".equals(name)) {
                                                                if (!"SmsCodeSwitch".equals(name)) {
                                                                    if (!"OnderValidDate".equals(name)) {
                                                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                                                            if (!"PaycodeInfo".equals(name)) {
                                                                                if (!"PromptMsg".equals(name)) {
                                                                                    if (!"PromptUrl".equals(name)) {
                                                                                        if (!"RespMd5".equals(name)) {
                                                                                            if (!"AllPayWay".equals(name)) {
                                                                                                break;
                                                                                            } else {
                                                                                                mm.purchasesdk.k.d.ai(newPullParser.nextText());
                                                                                                break;
                                                                                            }
                                                                                        } else {
                                                                                            this.z = newPullParser.nextText();
                                                                                            break;
                                                                                        }
                                                                                    } else {
                                                                                        this.D = newPullParser.nextText();
                                                                                        break;
                                                                                    }
                                                                                } else {
                                                                                    this.C = newPullParser.nextText();
                                                                                    break;
                                                                                }
                                                                            } else {
                                                                                String substring = str.substring(str.indexOf("<PaycodeInfo>"), str.indexOf("</PaycodeInfo>") + 14);
                                                                                this.A = substring;
                                                                                try {
                                                                                    this.im = R(substring);
                                                                                    break;
                                                                                } catch (ParserConfigurationException e) {
                                                                                    e.printStackTrace();
                                                                                    break;
                                                                                } catch (SAXException e2) {
                                                                                    e2.printStackTrace();
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {
                                                                            String nextText = newPullParser.nextText();
                                                                            if (nextText == null) {
                                                                                break;
                                                                            } else {
                                                                                this.B = nextText;
                                                                                break;
                                                                            }
                                                                        }
                                                                    } else {
                                                                        this.E = newPullParser.nextText();
                                                                        break;
                                                                    }
                                                                } else {
                                                                    this.y = newPullParser.nextText();
                                                                    mm.purchasesdk.k.d.ap(this.y);
                                                                    break;
                                                                }
                                                            } else {
                                                                this.w = newPullParser.nextText();
                                                                break;
                                                            }
                                                        } else {
                                                            this.v = newPullParser.nextText();
                                                            break;
                                                        }
                                                    } else {
                                                        this.x = newPullParser.nextText();
                                                        break;
                                                    }
                                                } else {
                                                    this.u = newPullParser.nextText();
                                                    break;
                                                }
                                            } else {
                                                this.p = newPullParser.nextText();
                                                break;
                                            }
                                        } else {
                                            this.t = newPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        this.s = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.r = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                this.q = newPullParser.nextText();
                                break;
                            }
                        } else {
                            this.p = newPullParser.nextText();
                            break;
                        }
                    } else {
                        X(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }
}
