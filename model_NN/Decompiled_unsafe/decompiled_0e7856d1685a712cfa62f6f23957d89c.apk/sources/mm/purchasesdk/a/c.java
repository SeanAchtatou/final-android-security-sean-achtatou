package mm.purchasesdk.a;

import android.util.Xml;
import com.a.a.h.b;
import java.io.StringWriter;
import java.util.Calendar;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.e;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlSerializer;

public class c extends e {
    private final String TAG = c.class.getSimpleName();
    private String n = b.SMS_RESULT_SEND_FAIL;
    private String o = b.SMS_RESULT_SEND_SUCCESS;
    int statusCode;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v17, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(java.lang.String r6) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = mm.purchasesdk.k.d.bX()
            r0.<init>(r1)
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = mm.purchasesdk.k.d.cg()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.k.d.bY()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.k.d.r()
            r1.append(r2)
            java.lang.String r1 = r5.TAG
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "plain text: "
            r2.<init>(r3)
            java.lang.String r3 = r0.toString()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            mm.purchasesdk.k.e.c(r1, r2)
            java.lang.String r0 = r0.toString()
            byte[] r2 = mm.purchasesdk.fingerprint.IdentifyApp.a(r0)
            if (r2 != 0) goto L_0x0067
            java.lang.String r0 = r5.TAG
            java.lang.String r1 = "create signature failed."
            mm.purchasesdk.k.e.e(r0, r1)
            r0 = 0
        L_0x0066:
            return r0
        L_0x0067:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r3.<init>(r0)
            r0 = 0
        L_0x006f:
            int r1 = r2.length
            if (r0 >= r1) goto L_0x008b
            byte r1 = r2[r0]
            if (r1 >= 0) goto L_0x0078
            int r1 = r1 + 256
        L_0x0078:
            r4 = 16
            if (r1 >= r4) goto L_0x0081
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x0081:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
            r3.append(r1)
            int r0 = r0 + 1
            goto L_0x006f
        L_0x008b:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.toUpperCase()
            byte[] r0 = r0.getBytes()
            java.lang.String r0 = mm.purchasesdk.fingerprint.IdentifyApp.base64encode(r0)
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.a.c.a(java.lang.String):java.lang.String");
    }

    public final String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(com.umeng.common.b.e.f, true);
            newSerializer.startTag("", "Trusted3AuthReq");
            newSerializer.startTag("", "MsgType");
            newSerializer.text("Trusted3AuthReq");
            newSerializer.endTag("", "MsgType");
            newSerializer.startTag("", "Version");
            newSerializer.text(d.co());
            newSerializer.endTag("", "Version");
            newSerializer.startTag("", "BracketID");
            newSerializer.text("");
            newSerializer.endTag("", "BracketID");
            newSerializer.startTag("", "SessionID");
            newSerializer.text(b.SMS_RESULT_SEND_SUCCESS);
            newSerializer.endTag("", "SessionID");
            newSerializer.startTag("", "osid");
            newSerializer.text(b.SMS_RESULT_SEND_FAIL);
            newSerializer.endTag("", "osid");
            newSerializer.startTag("", "OSInfo");
            newSerializer.text(d.cr());
            newSerializer.endTag("", "OSInfo");
            newSerializer.startTag("", "PayCode");
            newSerializer.text(d.bZ());
            newSerializer.endTag("", "PayCode");
            newSerializer.startTag("", "AppID");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "AppID");
            newSerializer.startTag("", "BracketID");
            newSerializer.text("");
            newSerializer.endTag("", "BracketID");
            newSerializer.startTag("", "ProgramId");
            newSerializer.text(d.cg());
            newSerializer.endTag("", "ProgramId");
            newSerializer.startTag("", "IsNextCycle");
            newSerializer.text(this.o);
            newSerializer.endTag("", "IsNextCycle");
            newSerializer.startTag("", "Timestamp");
            String l = Long.valueOf(Calendar.getInstance().getTimeInMillis() / 1000).toString();
            newSerializer.text(l);
            newSerializer.endTag("", "Timestamp");
            newSerializer.startTag("", "Signature");
            newSerializer.text(a(l));
            newSerializer.endTag("", "Signature");
            newSerializer.startTag("", "SubsNumb");
            newSerializer.text(this.n);
            newSerializer.endTag("", "SubsNumb");
            newSerializer.startTag("", "MccMnc");
            newSerializer.text(d.cp());
            newSerializer.endTag("", "MccMnc");
            newSerializer.startTag("", "UAInfo");
            newSerializer.text(d.cq());
            newSerializer.endTag("", "UAInfo");
            newSerializer.startTag("", OnPurchaseListener.TRADEID);
            newSerializer.text(d.cm());
            newSerializer.endTag("", OnPurchaseListener.TRADEID);
            newSerializer.startTag("", "StaticMark");
            String d = mm.purchasesdk.fingerprint.c.d(l);
            if (mm.purchasesdk.fingerprint.c.getStatus() != 0) {
                this.statusCode = PurchaseCode.AUTH_STATICMARK_FIALED;
                mm.purchasesdk.k.e.e(this.TAG, "create static mark failed.code=" + this.statusCode);
                return null;
            }
            mm.purchasesdk.k.e.c(this.TAG, "Static Mark->" + d);
            newSerializer.text(d);
            newSerializer.endTag("", "StaticMark");
            newSerializer.startTag("", "UserType");
            newSerializer.text(d.ca());
            newSerializer.endTag("", "UserType");
            newSerializer.startTag("", "programHash");
            newSerializer.text("");
            newSerializer.endTag("", "programHash");
            newSerializer.startTag("", "imsi");
            newSerializer.text(d.ce());
            newSerializer.endTag("", "imsi");
            newSerializer.startTag("", "imei");
            newSerializer.text(d.cf());
            newSerializer.endTag("", "imei");
            newSerializer.startTag("", "ChannelID");
            newSerializer.text(d.r());
            newSerializer.endTag("", "ChannelID");
            newSerializer.startTag("", "sidSignature");
            newSerializer.text(bL().getUserSignature());
            newSerializer.endTag("", "sidSignature");
            newSerializer.endTag("", "Trusted3AuthReq");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (Exception e) {
            mm.purchasesdk.k.e.a(this.TAG, "create AuthRequest xml file failed!!", e);
            this.statusCode = PurchaseCode.XML_EXCPTION_ERROR;
            return null;
        }
    }

    public final void a(Boolean bool) {
        if (bool.booleanValue()) {
            this.o = b.SMS_RESULT_SEND_FAIL;
        } else {
            this.o = b.SMS_RESULT_SEND_SUCCESS;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public final void m0a(String str) {
        this.n = str;
    }
}
