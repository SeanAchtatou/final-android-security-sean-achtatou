package mm.purchasesdk.i;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import java.util.ArrayList;
import java.util.List;
import javax.microedition.media.control.MetaDataControl;

public class a {
    private Activity a;
    List jt = new ArrayList();
    private Uri uri;

    public a(Activity activity, Uri uri2) {
        this.a = activity;
        this.uri = uri2;
    }

    public final List bO() {
        Cursor managedQuery = this.a.managedQuery(this.uri, new String[]{"_id", "address", "person", "body", MetaDataControl.DATE_KEY, com.umeng.common.a.b}, null, null, "date desc");
        int columnIndex = managedQuery.getColumnIndex("person");
        int columnIndex2 = managedQuery.getColumnIndex("address");
        int columnIndex3 = managedQuery.getColumnIndex("body");
        int columnIndex4 = managedQuery.getColumnIndex(MetaDataControl.DATE_KEY);
        int columnIndex5 = managedQuery.getColumnIndex(com.umeng.common.a.b);
        if (managedQuery != null) {
            while (managedQuery.moveToNext()) {
                b bVar = new b();
                bVar.setName(managedQuery.getString(columnIndex));
                bVar.ab(managedQuery.getString(columnIndex4));
                bVar.aa(managedQuery.getString(columnIndex2));
                bVar.Z(managedQuery.getString(columnIndex3));
                bVar.ac(managedQuery.getString(columnIndex5));
                this.jt.add(bVar);
            }
            managedQuery.close();
        }
        return this.jt;
    }
}
