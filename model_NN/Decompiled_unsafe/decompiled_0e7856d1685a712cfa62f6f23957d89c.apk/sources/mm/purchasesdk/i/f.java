package mm.purchasesdk.i;

import android.os.Message;
import mm.purchasesdk.d;
import mm.purchasesdk.f.e;
import mm.purchasesdk.g.g;
import mm.purchasesdk.g.h;

public class f implements Runnable {
    private g jA;
    private Message jz;

    public f(Message message, g gVar) {
        this.jz = message;
        this.jA = gVar;
    }

    public void run() {
        h hVar = new h();
        String str = null;
        try {
            str = new e().d(this.jA, hVar);
        } catch (d e) {
            e.printStackTrace();
        }
        this.jz.obj = str;
        this.jz.sendToTarget();
    }
}
