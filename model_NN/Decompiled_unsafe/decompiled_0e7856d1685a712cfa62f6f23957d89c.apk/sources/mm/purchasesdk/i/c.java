package mm.purchasesdk.i;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import mm.purchasesdk.g.g;
import mm.purchasesdk.k.b;

public class c {
    private final String TAG = "SMSReqFactory";
    private Handler handler;
    private String js;

    public c(String str) {
        this.js = str;
        HandlerThread handlerThread = new HandlerThread("mm-requestSMS");
        handlerThread.start();
        this.handler = new d(this, handlerThread.getLooper());
    }

    public final void m() {
        g gVar = new g();
        gVar.Y(this.js);
        b.a(0, "SMSReqFactory", "smsrequest-->" + gVar.toString());
        Message obtainMessage = this.handler.obtainMessage(300);
        obtainMessage.obj = gVar;
        this.handler.post(new f(obtainMessage, gVar));
    }
}
