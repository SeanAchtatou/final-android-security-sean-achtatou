package mm.purchasesdk.i;

import android.app.Activity;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import java.util.List;

public class e extends ContentObserver {
    private Activity a;
    private Handler handler;
    private List jt;

    public e(Handler handler2, Activity activity) {
        super(handler2);
        this.a = activity;
        this.handler = handler2;
    }

    public void onChange(boolean z) {
        this.jt = new a(this.a, Uri.parse("content://sms/inbox")).bO();
        Message obtainMessage = this.handler.obtainMessage();
        obtainMessage.what = 1;
        obtainMessage.obj = ((b) this.jt.get(0)).bP();
        obtainMessage.sendToTarget();
        super.onChange(z);
    }
}
