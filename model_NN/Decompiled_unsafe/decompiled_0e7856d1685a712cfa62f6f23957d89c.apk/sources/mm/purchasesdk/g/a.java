package mm.purchasesdk.g;

import android.util.Xml;
import com.umeng.common.b.e;
import java.io.IOException;
import java.io.StringWriter;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlSerializer;

public class a extends e {
    private static final String TAG = a.class.getSimpleName();

    public final String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(e.f, true);
            newSerializer.startTag("", "applyCopyrightDeclaration");
            newSerializer.startTag("", "version");
            newSerializer.text("1.0.0");
            newSerializer.endTag("", "version");
            newSerializer.startTag("", "appuid");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "appuid");
            newSerializer.startTag("", "programid");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "programid");
            newSerializer.startTag("", "osid");
            newSerializer.text("9");
            newSerializer.endTag("", "osid");
            newSerializer.endTag("", "applyCopyrightDeclaration");
            newSerializer.endDocument();
            return stringWriter.toString();
        } catch (IllegalArgumentException e) {
            mm.purchasesdk.k.e.a(TAG, "", e);
            throw new RuntimeException();
        } catch (IllegalStateException e2) {
            mm.purchasesdk.k.e.a(TAG, "", e2);
            throw new RuntimeException();
        } catch (IOException e3) {
            mm.purchasesdk.k.e.a(TAG, "", e3);
            throw new RuntimeException();
        }
    }
}
