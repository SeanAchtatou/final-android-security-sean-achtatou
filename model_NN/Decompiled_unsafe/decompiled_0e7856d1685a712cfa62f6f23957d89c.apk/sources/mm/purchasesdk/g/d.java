package mm.purchasesdk.g;

import java.io.ByteArrayInputStream;
import mm.purchasesdk.OnPurchaseListener;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class d extends f {
    private String E;
    private String N;
    private String P;
    private String jp;

    public final String b() {
        return this.E;
    }

    public final String e() {
        return this.jp;
    }

    public final boolean parse(String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (!"ReturnCode".equals(name)) {
                        if (!"OrderID".equals(name)) {
                            if (!"randNum".equals(name)) {
                                if (!"RespMd5".equals(name)) {
                                    if (!"OnderValidDate".equals(name)) {
                                        if (!OnPurchaseListener.TRADEID.equals(name)) {
                                            break;
                                        } else {
                                            this.jp = newPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        this.E = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    this.P = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                this.N = newPullParser.nextText();
                                break;
                            }
                        } else {
                            p(newPullParser.nextText());
                            break;
                        }
                    } else {
                        X(newPullParser.nextText());
                        break;
                    }
            }
        }
        return true;
    }
}
