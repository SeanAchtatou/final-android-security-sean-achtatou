package mm.purchasesdk.g;

import android.util.Xml;
import com.a.a.h.b;
import com.umeng.common.b.e;
import java.io.IOException;
import java.io.StringWriter;
import org.xmlpull.v1.XmlSerializer;

public class g extends e {
    private String js = b.SMS_RESULT_SEND_SUCCESS;

    public final void Y(String str) {
        this.js = str;
    }

    public String toString() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(e.f, true);
            newSerializer.startTag("", "Trusted2SmsCodeReq");
            newSerializer.startTag("", "MsgType");
            newSerializer.text("Trusted2SmsCodeReq");
            newSerializer.endTag("", "MsgType");
            newSerializer.startTag("", "Version");
            newSerializer.text("1.0.0");
            newSerializer.endTag("", "Version");
            newSerializer.startTag("", "SessionID");
            newSerializer.text(this.js);
            newSerializer.endTag("", "SessionID");
            newSerializer.endTag("", "Trusted2SmsCodeReq");
            newSerializer.endDocument();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return stringWriter.toString();
    }
}
