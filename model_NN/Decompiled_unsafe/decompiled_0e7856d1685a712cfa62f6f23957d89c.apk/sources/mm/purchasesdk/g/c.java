package mm.purchasesdk.g;

import android.util.Xml;
import com.umeng.common.b.e;
import java.io.StringWriter;
import java.util.Calendar;
import mm.purchasesdk.OnPurchaseListener;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlSerializer;

public class c extends e {
    private static final String TAG = c.class.getSimpleName();
    private int z = 0;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r5) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = mm.purchasesdk.k.d.bX()
            r0.<init>(r1)
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = mm.purchasesdk.k.d.bZ()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.k.d.bY()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            r1.append(r5)
            java.lang.String r0 = r0.toString()
            byte[] r2 = mm.purchasesdk.fingerprint.IdentifyApp.a(r0)
            if (r2 != 0) goto L_0x003a
            r0 = 0
        L_0x0039:
            return r0
        L_0x003a:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r3.<init>(r0)
            r0 = 0
        L_0x0042:
            int r1 = r2.length
            if (r0 >= r1) goto L_0x005e
            byte r1 = r2[r0]
            if (r1 >= 0) goto L_0x004b
            int r1 = r1 + 256
        L_0x004b:
            r4 = 16
            if (r1 >= r4) goto L_0x0054
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x0054:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
            r3.append(r1)
            int r0 = r0 + 1
            goto L_0x0042
        L_0x005e:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.toUpperCase()
            byte[] r0 = r0.getBytes()
            java.lang.String r0 = mm.purchasesdk.fingerprint.IdentifyApp.base64encode(r0)
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.g.c.a(java.lang.String):java.lang.String");
    }

    public final String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(e.f, true);
            newSerializer.startTag("", "Trusted2QueryReq");
            newSerializer.startTag("", "MsgType");
            newSerializer.text("Trusted2QueryReq");
            newSerializer.endTag("", "MsgType");
            newSerializer.startTag("", "Version");
            newSerializer.text(d.co());
            newSerializer.endTag("", "Version");
            newSerializer.startTag("", "PayCode");
            newSerializer.text(d.bZ());
            newSerializer.endTag("", "PayCode");
            newSerializer.startTag("", "AppID");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "AppID");
            newSerializer.startTag("", "ProgramId");
            newSerializer.text(d.cg());
            newSerializer.endTag("", "ProgramId");
            newSerializer.startTag("", "Timestamp");
            String l = Long.valueOf(Calendar.getInstance().getTimeInMillis() / 1000).toString();
            newSerializer.text(l);
            newSerializer.endTag("", "Timestamp");
            newSerializer.startTag("", OnPurchaseListener.TRADEID);
            newSerializer.text(d.cm());
            newSerializer.endTag("", OnPurchaseListener.TRADEID);
            newSerializer.startTag("", "Signature");
            newSerializer.text(a(l));
            newSerializer.endTag("", "Signature");
            newSerializer.startTag("", "imsi");
            newSerializer.text(d.ce());
            newSerializer.endTag("", "imsi");
            newSerializer.startTag("", "imei");
            newSerializer.text(d.cf());
            newSerializer.endTag("", "imei");
            newSerializer.startTag("", "sidSignature");
            newSerializer.text(bL().getUserSignature());
            newSerializer.endTag("", "sidSignature");
            newSerializer.endTag("", "Trusted2QueryReq");
            newSerializer.endDocument();
        } catch (Exception e) {
            mm.purchasesdk.k.e.a(TAG, "create QueryRequest xml file failed!!", e);
            this.z = PurchaseCode.XML_EXCPTION_ERROR;
        }
        return stringWriter.toString();
    }
}
