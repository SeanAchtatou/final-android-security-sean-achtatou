package mm.purchasesdk.g;

import java.io.ByteArrayInputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class h extends f {
    public static h d(byte[] bArr, String str) {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(bArr), str);
        h hVar = null;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 0:
                    hVar = new h();
                    break;
                case 2:
                    if (!"ReturnCode".equals(newPullParser.getName())) {
                        break;
                    } else {
                        hVar.X(newPullParser.nextText());
                        break;
                    }
            }
        }
        return hVar;
    }
}
