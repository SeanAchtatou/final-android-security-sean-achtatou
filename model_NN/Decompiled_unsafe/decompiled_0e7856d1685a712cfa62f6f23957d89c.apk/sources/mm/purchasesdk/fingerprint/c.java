package mm.purchasesdk.fingerprint;

import android.content.ContextWrapper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import mm.purchasesdk.k.d;

public class c {
    public static Boolean jk = true;
    private static int status = 0;

    public c() {
        try {
            IdentifyApp.init(d.getContext(), s());
            status = IdentifyApp.getLastError();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String a(String str, String str2) {
        try {
            try {
                return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, null);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return null;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
            return null;
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public static void m2a(String str, String str2) {
        IdentifyApp.gatherAppSignature(str, d.bX(), str2);
        status = IdentifyApp.getLastError();
    }

    public static String bK() {
        String appSignature = IdentifyApp.getAppSignature();
        status = IdentifyApp.getLastError();
        return appSignature;
    }

    public static byte[] base64decode(String str) {
        byte[] base64decode = IdentifyApp.base64decode(str);
        if (base64decode != null) {
            return base64decode;
        }
        throw new Exception("base64 decrypt data failed!");
    }

    public static String d(String str) {
        IOException e;
        String str2;
        try {
            str2 = new String(IdentifyApp.getAppTrustInfo(d.bX(), s(), d.bY(), str));
            try {
                status = IdentifyApp.getLastError();
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return str2;
            }
        } catch (IOException e3) {
            IOException iOException = e3;
            str2 = null;
            e = iOException;
            e.printStackTrace();
            return str2;
        }
        return str2;
    }

    public static int getStatus() {
        return status;
    }

    public static void init() {
        try {
            IdentifyApp.init(d.getContext(), s());
            status = IdentifyApp.getLastError();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String
     arg types: [java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):void
      mm.purchasesdk.fingerprint.c.a(java.lang.String, java.lang.String):java.lang.String */
    private static String s() {
        String a = a("ro.product.cpu.abi", (String) null);
        String a2 = a("ro.product.cpu.abi2", (String) null);
        System.currentTimeMillis();
        if (a == null) {
            return null;
        }
        ZipFile zipFile = new ZipFile(((ContextWrapper) d.getContext()).getPackageCodePath());
        boolean z = false;
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (true) {
            boolean z2 = z;
            if (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.getName().startsWith("lib/" + a + "/")) {
                    System.currentTimeMillis();
                    return a;
                }
                z = zipEntry.getName().startsWith(new StringBuilder("lib/").append(a2).append("/").toString()) ? true : z2;
            } else {
                zipFile.close();
                if (z2) {
                    return a2;
                }
                return null;
            }
        }
    }
}
