package mm.purchasesdk.fingerprint;

import java.io.ByteArrayInputStream;
import java.security.PublicKey;
import java.security.Signature;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class a {
    public static int a(int i, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        if (bArr == null) {
            throw new b(-2147483641, "Verify failed! Key cann't be null!");
        } else if (bArr2 == null) {
            throw new b(-2147483641, "Verify failed! The data to be signed cann't be null!");
        } else if (bArr3 == null) {
            throw new b(-2147483641, "Verify failed! The signed data cann't be null!");
        } else {
            byte[] a = a(bArr);
            if (a != null) {
                bArr = c.base64decode(new String(a));
            }
            byte[] a2 = a(bArr3);
            if (a2 != null) {
                bArr3 = c.base64decode(new String(a2));
            }
            return a("SHA1WITHRSA", bArr, bArr2, bArr3);
        }
    }

    private static int a(String str, byte[] bArr, byte[] bArr2, byte[] bArr3) {
        try {
            PublicKey publicKey = ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey();
            Signature instance = Signature.getInstance(str);
            instance.initVerify(publicKey);
            instance.update(bArr2);
            return instance.verify(bArr3) ? 0 : 1;
        } catch (Throwable th) {
            return 1;
        }
    }

    private static byte[] a(byte[] bArr) {
        for (byte indexOf : bArr) {
            if ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789/+= \r\n-".indexOf(indexOf) == -1) {
                return null;
            }
        }
        StringBuffer stringBuffer = new StringBuffer(bArr.length);
        String str = new String(bArr);
        for (int i = 0; i < str.length(); i++) {
            if (!(str.charAt(i) == ' ' || str.charAt(i) == 13 || str.charAt(i) == 10)) {
                stringBuffer.append(str.charAt(i));
            }
        }
        return stringBuffer.toString().getBytes();
    }
}
