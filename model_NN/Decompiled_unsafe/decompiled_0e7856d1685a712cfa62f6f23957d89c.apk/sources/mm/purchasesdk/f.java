package mm.purchasesdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import mm.purchasesdk.b.a;
import mm.purchasesdk.c.j;
import mm.purchasesdk.e.b;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class f extends HandlerThread {
    /* access modifiers changed from: private */
    public static boolean c;
    private static a jh;
    Handler b;
    Handler hL;

    public f(String str) {
        super(str);
    }

    static /* synthetic */ void a(f fVar, b bVar) {
        e.c("TaskThread", "init() called");
        int c2 = e.c(d.getContext());
        if (c2 != 0) {
            fVar.b(bVar, c2);
            return;
        }
        int e = b.e();
        if (e != 0) {
            fVar.b(bVar, e);
        } else if (d.bU().booleanValue()) {
            int a = j.a(bVar);
            d.al(b.bH().iU.iX);
            fVar.b(bVar, a);
        } else {
            int d = j.d();
            d.al(b.bH().iU.iX);
            fVar.b(bVar, d);
        }
    }

    static /* synthetic */ void a(f fVar, b bVar, Bundle bundle) {
        a aVar = jh;
        int b2 = mm.purchasesdk.b.b.b(bundle);
        PurchaseCode.setStatusCode(b2);
        if (b2 == 104) {
            PurchaseCode.setStatusCode(PurchaseCode.ORDER_OK);
            String string = bundle.getString(OnPurchaseListener.ORDERID);
            String string2 = bundle.getString(OnPurchaseListener.LEFTDAY);
            String string3 = bundle.getString(OnPurchaseListener.ORDERTYPE);
            d.an(string);
            d.b(string2);
            d.ae(string3);
            Message obtainMessage = fVar.b.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = PurchaseCode.ORDER_OK;
            obtainMessage.obj = bVar;
            obtainMessage.setData(bundle);
            obtainMessage.sendToTarget();
            return;
        }
        e.e("TaskThread", " order fail =" + b2);
        Message obtainMessage2 = fVar.b.obtainMessage();
        obtainMessage2.what = 2;
        obtainMessage2.arg1 = PurchaseCode.getStatusCode();
        obtainMessage2.obj = bVar;
        obtainMessage2.sendToTarget();
    }

    public static boolean a() {
        return c;
    }

    private void b(b bVar, int i) {
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = 0;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    static /* synthetic */ void b(f fVar, b bVar) {
        int bN = mm.purchasesdk.h.a.bN();
        e.c("TaskThread", "query code=" + bN);
        if (bN != 0) {
            fVar.c(bVar, PurchaseCode.getStatusCode());
            return;
        }
        PurchaseCode.setStatusCode(PurchaseCode.QUERY_OK);
        fVar.c(bVar, (int) PurchaseCode.QUERY_OK);
    }

    public static a bJ() {
        return jh;
    }

    private void c(b bVar, int i) {
        Message obtainMessage = this.b.obtainMessage();
        obtainMessage.what = d.cj;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    static /* synthetic */ void c(f fVar, b bVar) {
        if (jh == null) {
            jh = new a();
        }
        Boolean a = mm.purchasesdk.a.b.a(jh);
        if (a == null) {
            e.e("TaskThread", "AuthManager checkAuth ret = null.code=" + PurchaseCode.getStatusCode());
            Message obtainMessage = fVar.b.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = PurchaseCode.getStatusCode();
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        } else if (a.booleanValue()) {
            e.c("TaskThread", "AuthManager checkAuth ret = " + PurchaseCode.getStatusCode());
            Message obtainMessage2 = fVar.b.obtainMessage();
            obtainMessage2.what = 2;
            obtainMessage2.arg1 = PurchaseCode.getStatusCode();
            obtainMessage2.obj = bVar;
            obtainMessage2.sendToTarget();
        } else {
            PurchaseCode.setStatusCode(PurchaseCode.AUTH_NOORDER);
            Message obtainMessage3 = fVar.b.obtainMessage();
            obtainMessage3.what = 2;
            obtainMessage3.arg1 = PurchaseCode.getStatusCode();
            obtainMessage3.obj = bVar;
            obtainMessage3.sendToTarget();
        }
    }

    static /* synthetic */ void d(f fVar, b bVar) {
        int bQ = new mm.purchasesdk.j.a().bQ();
        PurchaseCode.setStatusCode(bQ);
        Message obtainMessage = fVar.b.obtainMessage();
        obtainMessage.what = 3;
        obtainMessage.arg1 = bQ;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    static /* synthetic */ void e(f fVar, b bVar) {
        if (jh == null) {
            jh = new a();
        }
        Boolean a = mm.purchasesdk.a.b.a(jh);
        if (a != null && !a.booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.AUTH_NOORDER);
        }
        Message obtainMessage = fVar.b.obtainMessage();
        obtainMessage.what = 4;
        obtainMessage.arg1 = PurchaseCode.getStatusCode();
        obtainMessage.sendToTarget();
    }

    public final void a(Handler handler) {
        this.b = handler;
    }

    public final Handler bI() {
        if (this.hL == null) {
            init();
        }
        return this.hL;
    }

    public final void init() {
        this.hL = new g(this, getLooper());
    }
}
