package mm.purchasesdk;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import java.util.HashMap;
import mm.purchasesdk.d.a;
import mm.purchasesdk.k.c;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.f;
import mm.purchasesdk.k.g;
import mm.purchasesdk.ui.u;

public class e {
    private static boolean a = false;
    private static boolean b = false;

    public static int a(Context context) {
        if (a) {
            return 0;
        }
        f.aq(context.getPackageName());
        d.f(c.d());
        d.d(Boolean.valueOf(g.a(context)));
        String bG = a.bG();
        if (bG == null) {
            return 200;
        }
        d.ah(bG);
        mm.purchasesdk.fingerprint.c.init();
        int b2 = b(context);
        if (b2 != 0) {
            return b2;
        }
        a = true;
        return 0;
    }

    public static void a(Handler handler, Handler handler2, b bVar, int i) {
        switch (d.cj) {
            case 0:
                if (d.bU().booleanValue() || !d.d()) {
                    bVar.onInitFinish(i);
                    return;
                } else {
                    bVar.onInitFinish(100);
                    return;
                }
            case 1:
                if (i == 100) {
                    Message obtainMessage = handler.obtainMessage();
                    obtainMessage.what = 1;
                    obtainMessage.obj = bVar;
                    obtainMessage.sendToTarget();
                    return;
                } else if (i == 6) {
                    a(bVar, handler, handler2);
                    return;
                } else {
                    bVar.a(i, null);
                    return;
                }
            case 2:
                if (i == 100) {
                    Message obtainMessage2 = handler.obtainMessage();
                    obtainMessage2.what = 2;
                    obtainMessage2.obj = bVar;
                    obtainMessage2.arg1 = 0;
                    obtainMessage2.sendToTarget();
                    return;
                } else if (i == 6) {
                    a(bVar, handler, handler2);
                    return;
                } else {
                    bVar.a(i, null);
                    return;
                }
            default:
                return;
        }
    }

    public static void a(b bVar, int i) {
        if (i == 101) {
            HashMap hashMap = new HashMap();
            String cn = d.cn();
            hashMap.put(OnPurchaseListener.PAYCODE, d.bZ());
            if (!(cn == null || cn.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.ORDERID, cn);
            }
            String b2 = d.b();
            if (!(b2 == null || b2.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.LEFTDAY, b2);
            }
            String cm = d.cm();
            if (!(cm == null || cm.trim().length() == 0)) {
                hashMap.put(OnPurchaseListener.TRADEID, cm);
            }
            bVar.onQueryFinish(i, hashMap);
            return;
        }
        bVar.onQueryFinish(i, null);
    }

    private static void a(b bVar, Handler handler, Handler handler2) {
        new mm.purchasesdk.c.a(d.getContext(), bVar, handler, handler2).show();
    }

    private static int b(Context context) {
        if (!b) {
            try {
                int initialMMSDK = d.bU().booleanValue() ? MMClientSDK_ForPhone.initialMMSDK(context, g.cu()) : MMClientSDK_ForPad.initialMMSDK(context, g.cv());
                if (initialMMSDK == 1) {
                    return PurchaseCode.APPLYCERT_IMEI_ERR;
                }
                if (initialMMSDK == 2) {
                    return PurchaseCode.APPLYCERT_APP_ERR;
                }
                if (initialMMSDK == 3) {
                    return PurchaseCode.APPLYCERT_CONFIG_ERR;
                }
                if (initialMMSDK == 4) {
                    return PurchaseCode.APPLYCERT_VALUE_ERR;
                }
                if (d.bU().booleanValue()) {
                    MMClientSDK_ForPhone.getIMSI();
                    d.cd();
                    d.ak(MMClientSDK_ForPhone.getDeviceID());
                } else {
                    MMClientSDK_ForPad.getIMSI_PAD();
                    d.cd();
                    d.ak(MMClientSDK_ForPad.getDeviceID_PAD());
                }
                b = true;
            } catch (Exception e) {
                mm.purchasesdk.k.e.a("InitCASDK", "init mmclientsdk failure", e);
                return PurchaseCode.APPLYCERT_OTHER_ERR;
            }
        }
        return 0;
    }

    public static void b() {
        u.cG().c(f.bJ());
    }

    public static void b(Handler handler, Handler handler2, b bVar, int i) {
        HashMap hashMap = null;
        if (102 == i || 104 == i || 101 == i) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put(OnPurchaseListener.PAYCODE, d.bZ());
            String cn = d.cn();
            if (!(cn == null || cn.trim().length() == 0)) {
                hashMap2.put(OnPurchaseListener.ORDERID, cn);
            }
            String b2 = d.b();
            if (!(b2 == null || b2.trim().length() == 0)) {
                hashMap2.put(OnPurchaseListener.LEFTDAY, b2);
            }
            String cm = d.cm();
            if (!(cm == null || cm.trim().length() == 0)) {
                hashMap2.put(OnPurchaseListener.TRADEID, cm);
            }
            String n = d.n();
            if (!(n == null || n.trim().length() == 0)) {
                hashMap2.put(OnPurchaseListener.ORDERTYPE, n);
            }
            if (i == 102 || 101 == i) {
                bVar.a(i, hashMap2);
            } else if (i == 104) {
                bVar.a((int) PurchaseCode.AUTH_OK);
                bVar.a(hashMap2);
                bVar.a();
            }
        } else if (i == 240) {
            u.cG().a(handler, handler2, bVar, f.bJ());
        } else if (i == 422) {
            u.cG().a(i, d.bT(), bVar, handler, handler2, null);
        } else {
            if (f.a()) {
                hashMap = new HashMap();
            }
            mm.purchasesdk.k.e.e("PurchaseInternal", " order fail =" + i);
            bVar.a(i, hashMap);
        }
    }

    public static int c(Context context) {
        String bX = d.bX();
        String bY = d.bY();
        if (bX == null || bX.trim().length() == 0) {
            mm.purchasesdk.k.e.e("PurchaseInternal", "appid is null");
            return PurchaseCode.PARAMETER_ERR;
        } else if (bY == null || bY.trim().length() == 0) {
            mm.purchasesdk.k.e.e("PurchaseInternal", "appkey is null");
            return PurchaseCode.PARAMETER_ERR;
        } else {
            String b2 = mm.purchasesdk.f.d.b(context);
            if (b2 == null) {
                return PurchaseCode.PARAMETER_ERR;
            }
            d.ao(b2.trim());
            d.b(bX, bY);
            if (!g.b(context)) {
                return PurchaseCode.NONE_NETWORK;
            }
            d.d(Boolean.valueOf(g.a(context)));
            return 0;
        }
    }
}
