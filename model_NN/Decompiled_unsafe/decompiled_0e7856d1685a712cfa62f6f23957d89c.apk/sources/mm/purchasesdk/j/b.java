package mm.purchasesdk.j;

import android.util.Xml;
import java.io.StringWriter;
import java.util.Calendar;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.g.e;
import mm.purchasesdk.k.d;
import org.xmlpull.v1.XmlSerializer;

public class b extends e {
    private final String TAG = b.class.getSimpleName();

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r5) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = mm.purchasesdk.k.d.bX()
            r0.<init>(r1)
            java.lang.String r1 = "&"
            java.lang.StringBuilder r1 = r0.append(r1)
            java.lang.String r2 = mm.purchasesdk.k.d.bZ()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.k.d.bY()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = "&"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = mm.purchasesdk.k.d.r()
            r1.append(r2)
            java.lang.String r0 = r0.toString()
            byte[] r2 = mm.purchasesdk.fingerprint.IdentifyApp.a(r0)
            if (r2 != 0) goto L_0x0048
            r0 = 0
        L_0x0047:
            return r0
        L_0x0048:
            java.lang.StringBuffer r3 = new java.lang.StringBuffer
            java.lang.String r0 = ""
            r3.<init>(r0)
            r0 = 0
        L_0x0050:
            int r1 = r2.length
            if (r0 >= r1) goto L_0x006c
            byte r1 = r2[r0]
            if (r1 >= 0) goto L_0x0059
            int r1 = r1 + 256
        L_0x0059:
            r4 = 16
            if (r1 >= r4) goto L_0x0062
            java.lang.String r4 = "0"
            r3.append(r4)
        L_0x0062:
            java.lang.String r1 = java.lang.Integer.toHexString(r1)
            r3.append(r1)
            int r0 = r0 + 1
            goto L_0x0050
        L_0x006c:
            java.lang.String r0 = r3.toString()
            java.lang.String r0 = r0.toUpperCase()
            byte[] r0 = r0.getBytes()
            java.lang.String r0 = mm.purchasesdk.fingerprint.IdentifyApp.base64encode(r0)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: mm.purchasesdk.j.b.a(java.lang.String):java.lang.String");
    }

    public final String a() {
        XmlSerializer newSerializer = Xml.newSerializer();
        StringWriter stringWriter = new StringWriter();
        try {
            newSerializer.setOutput(stringWriter);
            newSerializer.startDocument(com.umeng.common.b.e.f, true);
            newSerializer.startTag("", "Trusted2UnSubsReq");
            newSerializer.startTag("", "MsgType");
            newSerializer.text("Trusted2UnSubsReq");
            newSerializer.endTag("", "MsgType");
            newSerializer.startTag("", "Version");
            newSerializer.text(d.co());
            newSerializer.endTag("", "Version");
            newSerializer.startTag("", "PayCode");
            newSerializer.text(d.bZ());
            newSerializer.endTag("", "PayCode");
            newSerializer.startTag("", "AppID");
            newSerializer.text(d.bX());
            newSerializer.endTag("", "AppID");
            newSerializer.startTag("", "Timestamp");
            String l = Long.valueOf(Calendar.getInstance().getTimeInMillis() / 1000).toString();
            newSerializer.text(l);
            newSerializer.endTag("", "Timestamp");
            newSerializer.startTag("", "ChannelID");
            newSerializer.text(d.r());
            newSerializer.endTag("", "ChannelID");
            newSerializer.startTag("", "Signature");
            newSerializer.text(a(l));
            newSerializer.endTag("", "Signature");
            newSerializer.startTag("", "sidSignature");
            newSerializer.text(bL().getUserSignature());
            newSerializer.endTag("", "sidSignature");
            newSerializer.endTag("", "Trusted2UnSubsReq");
            newSerializer.endDocument();
        } catch (Exception e) {
            mm.purchasesdk.k.e.a(this.TAG, "create QueryRequest xml file failed!!", e);
            PurchaseCode.setStatusCode(PurchaseCode.XML_EXCPTION_ERROR);
        }
        return stringWriter.toString();
    }
}
