package mm.purchasesdk.j;

import com.ccit.mmwlan.MMClientSDK_ForPad;
import com.ccit.mmwlan.phone.MMClientSDK_ForPhone;
import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.d;
import mm.purchasesdk.f.b;
import mm.purchasesdk.f.c;
import mm.purchasesdk.f.e;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;

public class a {
    private final String TAG = a.class.getSimpleName();

    public final int bQ() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new e());
        b bVar = new b();
        c cVar = new c();
        Iterator it = arrayList.iterator();
        String str = null;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            try {
                String b = ((c) it.next()).b(bVar, cVar);
                if (b != null) {
                    str = b;
                    break;
                }
                str = b;
            } catch (d e) {
                e.printStackTrace();
            }
        }
        int i = 0;
        if (str == null) {
            return 0;
        }
        if (!mm.purchasesdk.k.a.ad(str).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.RESPONSE_ERR);
            return PurchaseCode.RESPONSE_ERR;
        }
        int intValue = Integer.valueOf(cVar.bM()).intValue();
        mm.purchasesdk.k.e.c(this.TAG, "retCode =" + intValue);
        switch (intValue) {
            case 0:
                i = PurchaseCode.UNSUB_OK;
                new b().a(mm.purchasesdk.k.d.bX(), mm.purchasesdk.k.d.bZ(), mm.purchasesdk.k.d.ce());
                break;
            case 2:
                i = PurchaseCode.UNSUB_NOT_FOUND;
                break;
            case 5:
                i = PurchaseCode.UNSUB_PAYCODE_ERROR;
                break;
            case 11:
                i = PurchaseCode.UNSUB_NO_AUTHORIZATION;
                break;
            case 12:
                i = PurchaseCode.UNSUB_CSSP_BUSY;
                break;
            case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
                i = PurchaseCode.UNSUB_INTERNAL_ERROR;
                break;
            case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
                try {
                    if (mm.purchasesdk.k.d.d()) {
                        MMClientSDK_ForPhone.DestorySecCert(null);
                    } else {
                        MMClientSDK_ForPad.DestorySecCert(null);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                i = PurchaseCode.UNSUB_INVALID_USER;
                break;
            case 17:
                i = PurchaseCode.UNSUB_LICENSE_ERROR;
                break;
            case 18:
                i = PurchaseCode.UNSUB_NO_ABILITY;
                break;
            case 19:
                i = PurchaseCode.UNSUB_NO_APP;
                break;
        }
        PurchaseCode.setStatusCode(i);
        return i;
    }
}
