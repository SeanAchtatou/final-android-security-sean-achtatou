package mm.purchasesdk.d;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import mm.purchasesdk.f.d;
import mm.purchasesdk.k.e;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class a {
    private static final String TAG = a.class.getSimpleName();
    private static String V = null;

    public static String bG() {
        if (V != null) {
            return V;
        }
        String r = d.r();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new ByteArrayInputStream(r.getBytes()), "utf-8");
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        if (!com.umeng.common.a.d.equals(newPullParser.getName())) {
                            break;
                        } else {
                            V = newPullParser.nextText();
                            break;
                        }
                }
            }
            e.c(TAG, "LOCAL ChannelID:" + V);
            return V;
        } catch (XmlPullParserException e) {
            e.a(TAG, "failed to read mmiap.xml excepiton. ", e);
            V = null;
            return null;
        } catch (IOException e2) {
            e.a(TAG, "failed to read mmiap.xml. io excetion ", e2);
            V = null;
            return null;
        }
    }
}
