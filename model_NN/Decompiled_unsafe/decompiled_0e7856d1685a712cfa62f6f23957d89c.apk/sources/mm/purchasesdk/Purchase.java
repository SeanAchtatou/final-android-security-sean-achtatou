package mm.purchasesdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;
import mm.purchasesdk.ui.u;

public class Purchase {
    public static final String TAG = Purchase.class.getSimpleName();
    private static Purchase hJ;
    /* access modifiers changed from: private */
    public Handler b;
    private f hK = new f("purchase-task");
    /* access modifiers changed from: private */
    public Handler hL;
    private HandlerThread hM;

    private Purchase() {
        this.hK.start();
        this.hK.init();
        this.hM = new HandlerThread("Response-thread");
        this.hM.start();
        this.b = new c(this, this.hM.getLooper());
        this.hK.a(this.b);
        this.hL = this.hK.bI();
    }

    public static Purchase bo() {
        if (hJ == null) {
            hJ = new Purchase();
        }
        return hJ;
    }

    public static String getReason(int i) {
        return PurchaseCode.getReason(i);
    }

    public static void j(String str, String str2) {
        if (str == null || str2 == null || str.trim().length() == 0 || str2.trim().length() == 0) {
            throw new Exception("invalid app parameter, pls check it");
        }
        e.R();
        d.b(str.trim(), str2.trim());
    }

    public final String a(Context context, String str, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (str == null || str.trim().length() == 0) {
            throw new Exception("Paycode is null");
        } else if (!d.v(2)) {
            throw new Exception("Another request is processing");
        } else {
            d.setContext(context);
            d.ag(str);
            d.h(1);
            d.a(false);
            u.cG().J();
            b bVar = new b(onPurchaseListener, this.hL, this.b);
            d.Q();
            int a = e.a(context);
            if (a != 0) {
                bVar.a(a, null);
                return null;
            }
            String cl = d.cl();
            d.am(cl);
            Message obtainMessage = this.hL.obtainMessage();
            obtainMessage.what = 0;
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
            return cl;
        }
    }

    public final void a(Context context, OnPurchaseListener onPurchaseListener) {
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        } else if (onPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (!d.v(0)) {
            throw new Exception("Another request is processing");
        } else {
            d.setContext(context);
            b bVar = new b(onPurchaseListener, this.hL, this.b);
            d.Q();
            int a = e.a(context);
            if (a != 0) {
                bVar.onInitFinish(a);
            } else if (!d.d()) {
                bVar.onInitFinish(100);
            } else {
                Message obtainMessage = this.hL.obtainMessage();
                obtainMessage.what = 0;
                obtainMessage.obj = bVar;
                obtainMessage.sendToTarget();
            }
        }
    }
}
