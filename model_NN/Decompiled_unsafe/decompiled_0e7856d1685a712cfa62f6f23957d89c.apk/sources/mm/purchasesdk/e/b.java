package mm.purchasesdk.e;

import java.util.ArrayList;
import java.util.Iterator;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.f.c;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;

public class b {
    private static final String TAG = b.class.getSimpleName();
    private static a jg = null;

    private static int a(String str, c cVar) {
        try {
            a W = a.W(str);
            if (W == null) {
                return PurchaseCode.COPYRIGHT_PARSE_ERR;
            }
            if (!a.a(str, W)) {
                e.e(TAG, "copyright verify failed, code=233");
                return PurchaseCode.COPYRIGHT_VALIDATE_FAIL;
            }
            e.c(TAG, "Copyright validate success");
            if (W.W.equals(d.bX()) || W.W.equals("100000000000")) {
                if (!(cVar instanceof mm.purchasesdk.f.b)) {
                    new mm.purchasesdk.f.b().d(d.bX(), str, d.ce());
                }
                jg = W;
                return 0;
            }
            e.e(TAG, "appid not same: " + d.bX() + " - " + W.W);
            return PurchaseCode.COPYRIGHT_VALIDATE_FAIL;
        } catch (Exception e) {
            e.a(TAG, "copyright parse failed.", e);
            return PurchaseCode.COPYRIGHT_PARSE_ERR;
        }
    }

    public static a bH() {
        return jg;
    }

    public static int e() {
        if (jg != null) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(new mm.purchasesdk.f.b());
        arrayList.add(new mm.purchasesdk.f.d());
        arrayList.add(new mm.purchasesdk.f.e());
        Iterator it = arrayList.iterator();
        int i = 0;
        while (it.hasNext()) {
            c cVar = (c) it.next();
            try {
                String q = cVar.q();
                if (q == null) {
                    continue;
                } else {
                    int a = a(q, cVar);
                    if (a == 0) {
                        return a;
                    }
                    i = a;
                }
            } catch (mm.purchasesdk.d e) {
                e.e(TAG, "load copyright failure: " + e.c);
                i = e.c;
            }
        }
        return i;
    }
}
