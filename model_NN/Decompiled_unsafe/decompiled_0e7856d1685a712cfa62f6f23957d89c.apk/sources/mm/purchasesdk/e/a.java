package mm.purchasesdk.e;

import java.io.ByteArrayInputStream;
import me.gall.tinybee.TinybeeLogger;
import mm.purchasesdk.PurchaseCode;
import mm.purchasesdk.k.d;
import mm.purchasesdk.k.e;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

public class a {
    private static final String TAG = a.class.getSimpleName();
    public String W;
    public C0004a iU = null;
    public b iV = null;

    /* renamed from: mm.purchasesdk.e.a$a  reason: collision with other inner class name */
    public class C0004a {
        public String iW;
        public String iX;
        public String iY;
        public String iZ;
        public String ja;
        public String jb;

        public C0004a() {
        }
    }

    public class b {
        public String iY;
        public String iZ;
        public String jb;
        public String jd;
        public String je;
        public String jf;

        public b() {
        }
    }

    public static a W(String str) {
        a aVar = new a();
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case 2:
                    String name = newPullParser.getName();
                    if (name.compareToIgnoreCase(TinybeeLogger.EventTask.APPID) != 0) {
                        if (name.compareToIgnoreCase("contentSignature") != 0) {
                            if (name.compareToIgnoreCase("copyrightSignature") != 0) {
                                break;
                            } else {
                                b(aVar, newPullParser);
                                break;
                            }
                        } else {
                            a(aVar, newPullParser);
                            break;
                        }
                    } else {
                        aVar.W = newPullParser.nextText();
                        break;
                    }
            }
        }
        return aVar;
    }

    private static void a(a aVar, XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (aVar.iU == null) {
                        aVar.getClass();
                        aVar.iU = new C0004a();
                    }
                    if (name.compareToIgnoreCase("time") != 0) {
                        if (name.compareToIgnoreCase("programID") != 0) {
                            if (name.compareToIgnoreCase("digestAlg") != 0) {
                                if (name.compareToIgnoreCase("digest") != 0) {
                                    if (name.compareToIgnoreCase("certificate") != 0) {
                                        if (name.compareToIgnoreCase("signature") != 0) {
                                            break;
                                        } else {
                                            aVar.iU.jb = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.iU.ja = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.iU.iZ = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.iU.iY = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.iU.iX = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar.iU.iW = xmlPullParser.nextText();
                        break;
                    }
                case 3:
                    if (xmlPullParser.getName().compareToIgnoreCase("contentSignature") != 0) {
                        break;
                    } else {
                        return;
                    }
            }
            eventType = xmlPullParser.next();
        }
    }

    public static boolean a(String str, a aVar) {
        int i;
        int i2;
        int indexOf = str.indexOf("<APPID>");
        int indexOf2 = str.indexOf("</APPID>") + "</APPID>".length();
        e.c(TAG, "localCopyright-->" + str);
        if (indexOf >= 0) {
            if (indexOf2 >= "</APPID>".length() + "<APPID>".length() + 6) {
                String substring = str.substring(indexOf, indexOf2);
                e.c(TAG, "CIDC content is: " + substring);
                try {
                    i = mm.purchasesdk.fingerprint.a.a((int) PurchaseCode.AUTH_NO_ABILITY, aVar.iU.ja.trim().getBytes(), substring.trim().getBytes(), aVar.iU.jb.trim().getBytes());
                } catch (mm.purchasesdk.fingerprint.b e) {
                    e.c(TAG, e.getMessage());
                    i = 1;
                } catch (Exception e2) {
                    e.d(TAG, "base 64 decrypt license file failure");
                    i = 1;
                }
                e.c(TAG, "CIDC verifyWithCert result is: " + i);
                if (i != 0) {
                    return false;
                }
                String substring2 = str.substring(str.indexOf("<APPID>"), "</contentSignature>".length() + str.indexOf("</contentSignature>"));
                e.c(TAG, "TAAC content is: " + substring2);
                try {
                    i2 = mm.purchasesdk.fingerprint.a.a((int) PurchaseCode.AUTH_NO_ABILITY, d.ck().getBytes(), substring2.trim().getBytes(), aVar.iV.jb.trim().getBytes());
                } catch (mm.purchasesdk.fingerprint.b e3) {
                    e.e(TAG, e3.getMessage());
                    i2 = 1;
                } catch (Exception e4) {
                    e.d(TAG, "base 64 decrypt license file failure");
                    i2 = 1;
                }
                e.c(TAG, "TAAC verifyWithCert result is: " + i2);
                return i2 == 0;
            }
        }
        return false;
    }

    private static void b(a aVar, XmlPullParser xmlPullParser) {
        int eventType = xmlPullParser.getEventType();
        while (eventType != 1) {
            switch (eventType) {
                case 2:
                    String name = xmlPullParser.getName();
                    if (aVar.iV == null) {
                        aVar.getClass();
                        aVar.iV = new b();
                    }
                    if (name.compareToIgnoreCase("copyrightID") != 0) {
                        if (name.compareToIgnoreCase("copyRightType") != 0) {
                            if (name.compareToIgnoreCase("digestAlg") != 0) {
                                if (name.compareToIgnoreCase("digest") != 0) {
                                    if (name.compareToIgnoreCase("timestamp") != 0) {
                                        if (name.compareToIgnoreCase("signature") != 0) {
                                            break;
                                        } else {
                                            aVar.iV.jb = xmlPullParser.nextText();
                                            break;
                                        }
                                    } else {
                                        aVar.iV.jf = xmlPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    aVar.iV.iZ = xmlPullParser.nextText();
                                    break;
                                }
                            } else {
                                aVar.iV.iY = xmlPullParser.nextText();
                                break;
                            }
                        } else {
                            aVar.iV.je = xmlPullParser.nextText();
                            break;
                        }
                    } else {
                        aVar.iV.jd = xmlPullParser.nextText();
                        break;
                    }
                case 3:
                    if (xmlPullParser.getName().compareToIgnoreCase("copyrightSignature") != 0) {
                        break;
                    } else {
                        return;
                    }
            }
            eventType = xmlPullParser.next();
        }
    }
}
