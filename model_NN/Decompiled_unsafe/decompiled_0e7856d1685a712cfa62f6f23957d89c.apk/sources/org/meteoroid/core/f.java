package org.meteoroid.core;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;
import mm.purchasesdk.PurchaseCode;
import org.meteoroid.core.InputManager;

public final class f extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final f mo = new f();
    private static final LinkedHashSet<a> mr = new LinkedHashSet<>();
    private static final LinkedHashSet<b> ms = new LinkedHashSet<>();
    private static final LinkedHashSet<e> mt = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<d> mu = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<InputManager.RawMotionEventListener> mv = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public GestureDetector mp;
    private SensorManager mq;

    public interface a {
        boolean a(KeyEvent keyEvent);
    }

    public interface b {
        boolean f(int i, int i2, int i3, int i4);
    }

    private final class c implements View.OnTouchListener {
        private c() {
        }

        /* synthetic */ c(f fVar, byte b) {
            this();
        }

        public final boolean onTouch(View view, MotionEvent motionEvent) {
            boolean z = false;
            SystemClock.sleep(34);
            if (!f.mu.isEmpty()) {
                f.this.mp.onTouchEvent(motionEvent);
            }
            if (!f.mv.isEmpty()) {
                Iterator it = f.mv.iterator();
                while (it.hasNext()) {
                    it.next();
                }
            }
            int action = motionEvent.getAction();
            int i = action & PurchaseCode.AUTH_INVALID_APP;
            int i2 = action >> 8;
            if (i == 6) {
                return f.f(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            if (i == 5) {
                return f.f(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            int pointerCount = motionEvent.getPointerCount();
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (f.f(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                    z = true;
                }
            }
            return z;
        }
    }

    public interface d {
        public static final int GESTURE_SLIDE_DOWN = 2;
        public static final int GESTURE_SLIDE_LEFT = 3;
        public static final int GESTURE_SLIDE_RIGHT = 4;
        public static final int GESTURE_SLIDE_UP = 1;

        boolean cM();
    }

    public interface e {
    }

    protected static void a(Activity activity) {
        mo.mp = new GestureDetector(activity, mo);
        mo.mq = (SensorManager) activity.getSystemService("sensor");
    }

    public static final void a(SensorEventListener sensorEventListener) {
        mo.mq.registerListener(sensorEventListener, mo.mq.getDefaultSensor(1), 3);
    }

    public static final void a(a aVar) {
        mr.add(aVar);
    }

    public static final void a(b bVar) {
        ms.add(bVar);
    }

    public static final void a(e eVar) {
        mt.add(eVar);
    }

    public static final void b(SensorEventListener sensorEventListener) {
        mo.mq.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        View.OnTouchListener onTouchListener;
        if (!l.nk) {
            view.setOnKeyListener(mo);
        }
        if (k.di() >= 5) {
            f fVar = mo;
            fVar.getClass();
            onTouchListener = new c(fVar, (byte) 0);
        } else {
            onTouchListener = mo;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void b(b bVar) {
        ms.remove(bVar);
    }

    protected static void c(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final boolean f(int i, int i2, int i3, int i4) {
        Iterator<b> it = ms.iterator();
        while (it.hasNext()) {
            if (it.next().f(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        mu.clear();
        ms.clear();
        mt.clear();
        mr.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (!mt.isEmpty()) {
            Iterator<e> it = mt.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator<d> it = mu.iterator();
        while (it.hasNext()) {
            if (it.next().cM()) {
                return true;
            }
        }
        return false;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (!mr.isEmpty()) {
            Iterator<a> it = mr.iterator();
            while (it.hasNext()) {
                it.next().a(keyEvent);
            }
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!mu.isEmpty()) {
            this.mp.onTouchEvent(motionEvent);
        }
        if (!mv.isEmpty()) {
            Iterator<InputManager.RawMotionEventListener> it = mv.iterator();
            while (it.hasNext()) {
                it.next();
            }
        }
        return f(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
