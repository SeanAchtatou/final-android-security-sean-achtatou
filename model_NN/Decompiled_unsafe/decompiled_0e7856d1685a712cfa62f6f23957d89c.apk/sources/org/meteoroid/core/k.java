package org.meteoroid.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import com.a.a.j.c;
import com.androidbox.hywxfktj4.R;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import org.meteoroid.core.h;

public final class k {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47880;
    public static final int MSG_SYSTEM_DEVICE_INIT_COMPLETE = 47878;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FEATURE_ADDED_COMPLETE = 47876;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47882;
    public static final int MSG_SYSTEM_GRAPHICS_INIT_COMPLETE = 47877;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_APPENDER = 47886;
    public static final int MSG_SYSTEM_LOG_EVENT = 47887;
    public static final int MSG_SYSTEM_LOG_EVENT_BY_APPENDER = 47902;
    public static final int MSG_SYSTEM_NOTIFY_EXIT = 47881;
    public static final int MSG_SYSTEM_ONLINE_PARAM = 47885;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    public static final int MSG_SYSTEM_VD_INIT_COMPLETE = 47879;
    private static Handler handler;
    /* access modifiers changed from: private */
    public static Activity mZ;
    private static int na = 0;
    public static boolean nb = false;
    /* access modifiers changed from: private */
    public static boolean nc = false;
    private static final Timer timer = new Timer();

    public static void E(int i) {
        mZ.setRequestedOrientation(i);
    }

    protected static void a(Activity activity) {
        mZ = activity;
        h.b((int) MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.b((int) MSG_SYSTEM_FEATURE_ADDED_COMPLETE, "MSG_SYSTEM_FEATURE_ADDED_COMPLETE");
        h.b((int) MSG_SYSTEM_GRAPHICS_INIT_COMPLETE, "MSG_SYSTEM_GRAPHICS_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_DEVICE_INIT_COMPLETE, "MSG_SYSTEM_DEVICE_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_VD_INIT_COMPLETE, "MSG_SYSTEM_VD_INIT_COMPLETE");
        h.b((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.b((int) MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
        h.b((int) MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
        h.b((int) MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
        h.b((int) MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
        h.b((int) MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
        h.b((int) MSG_SYSTEM_NOTIFY_EXIT, "MSG_SYSTEM_NOTIFY_EXIT");
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(c.aM("globe")));
            handler = new Handler();
            dd();
            if (properties.containsKey("ThrowIOExceptions")) {
                nb = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                l.nk = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            if (!properties.containsKey("DisableWakeLock")) {
                activity.getWindow().setFlags(128, 128);
            }
            h.cH();
            e.a(activity);
            d.cH();
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        d.o(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                        split[i] + " has been added.";
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            h.C(MSG_SYSTEM_FEATURE_ADDED_COMPLETE);
            a.cH();
            f.a(activity);
            if (properties.containsKey("VolumeMode")) {
                g.w(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            g.a(activity);
            l.a(activity);
            i.cH();
            c.ay(properties.getProperty("device"));
            h.C(MSG_SYSTEM_DEVICE_INIT_COMPLETE);
            h.C(MSG_SYSTEM_VD_INIT_COMPLETE);
            if (properties.containsKey("AdaptiveVirtualDevice")) {
                m.ns = Boolean.parseBoolean(properties.getProperty("AdaptiveVirtualDevice"));
            }
            m.aE(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            h.a(new h.a() {
                public final boolean a(Message message) {
                    if (message.what == 47875) {
                        k.cT();
                        return true;
                    } else if (message.what == 47881) {
                        if (k.nc) {
                            return true;
                        }
                        l.a(k.getString(R.string.alert), k.getString(R.string.exit_dialog), k.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                k.cU();
                                boolean unused = k.nc = false;
                            }
                        }, k.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public final void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                k.resume();
                                boolean unused = k.nc = false;
                            }
                        }, true, new DialogInterface.OnCancelListener() {
                            public final void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                k.resume();
                                boolean unused = k.nc = false;
                            }
                        });
                        boolean unused = k.nc = true;
                        k.pause();
                        return true;
                    } else if (message.what == 47882) {
                        return k.aB((String) message.obj);
                    } else {
                        return false;
                    }
                }
            });
            h.b(h.a(MSG_SYSTEM_LOG_EVENT, new String[]{"Launch", mZ.getString(R.string.app_name)}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static boolean aA(String str) {
        h.b(h.a(MSG_SYSTEM_FUNCTION_REQUEST, str));
        return true;
    }

    public static boolean aB(final String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            handler.post(new Runnable() {
                public final void run() {
                    k.mZ.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                }
            });
            return true;
        } else if (str.startsWith("tel:")) {
            handler.post(new Runnable() {
                public final void run() {
                    k.mZ.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
                }
            });
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    public static boolean aC(String str) {
        try {
            return mZ.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean aD(String str) {
        PackageManager packageManager = mZ.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(packageInfo.packageName);
            ResolveInfo next = packageManager.queryIntentActivities(intent, 0).iterator().next();
            if (next != null) {
                String str2 = next.activityInfo.name;
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.addCategory("android.intent.category.LAUNCHER");
                intent2.setComponent(new ComponentName(str, str2));
                mZ.startActivity(intent2);
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static final InputStream az(String str) {
        InputStream inputStream = null;
        try {
            inputStream = mZ.getAssets().open(str);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str + " is not exist.");
            if (nb) {
                throw new IOException();
            }
        }
        "Load assert " + str + (inputStream != null ? " success." : " failed.");
        return inputStream;
    }

    protected static void cT() {
        h.cS();
        timer.cancel();
        timer.purge();
        a.onDestroy();
        i.onDestroy();
        e.onDestroy();
        f.onDestroy();
        g.onDestroy();
        l.onDestroy();
        if (c.mf != null) {
            c.mf.onDestroy();
        }
        m.onDestroy();
        d.onDestroy();
        h.onDestroy();
        mZ.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static void cU() {
        h.b(h.a(MSG_SYSTEM_EXIT, null));
    }

    public static String cV() {
        TelephonyManager db = db();
        if (mZ.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Log.e(LOG_TAG, "Get CarrierIMSI failed for permission not properly config.");
            return null;
        } else if (db.getSimState() == 5) {
            return db.getSubscriberId();
        } else {
            throw new Exception("Sim card is not ready yet.");
        }
    }

    public static final boolean cW() {
        return cX() || cY();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (dc().getNetworkInfo(1).isConnected() == true) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean cX() {
        /*
            r0 = 1
            android.app.Activity r1 = org.meteoroid.core.k.mZ
            java.lang.String r2 = "android.permission.ACCESS_NETWORK_STATE"
            int r1 = r1.checkCallingOrSelfPermission(r2)
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            return r0
        L_0x000c:
            r1 = 0
            android.net.ConnectivityManager r2 = dc()     // Catch:{ Exception -> 0x002b }
            r3 = 1
            android.net.NetworkInfo r2 = r2.getNetworkInfo(r3)     // Catch:{ Exception -> 0x002b }
            boolean r2 = r2.isConnected()     // Catch:{ Exception -> 0x002b }
            if (r2 != r0) goto L_0x0031
        L_0x001c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Wifi state: "
            r1.<init>(r2)
            java.lang.StringBuilder r1 = r1.append(r0)
            r1.toString()
            goto L_0x000b
        L_0x002b:
            r0 = move-exception
            java.lang.String r2 = "SystemManager"
            android.util.Log.w(r2, r0)
        L_0x0031:
            r0 = r1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.k.cX():boolean");
    }

    private static boolean cY() {
        boolean z = true;
        if (mZ.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            try {
                if (!dc().getNetworkInfo(0).isConnectedOrConnecting()) {
                    if (db().getDataState() != 2) {
                        z = false;
                    }
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                z = false;
            }
            "DataConnect state: " + z;
        }
        return z;
    }

    public static String cZ() {
        return mZ.getString(R.string.app_name);
    }

    public static void d(final String str, final int i) {
        handler.post(new Runnable() {
            public final void run() {
                Toast.makeText(k.mZ, str, i).show();
            }
        });
    }

    public static void da() {
        h.C(MSG_SYSTEM_NOTIFY_EXIT);
    }

    public static TelephonyManager db() {
        return (TelephonyManager) mZ.getSystemService("phone");
    }

    public static ConnectivityManager dc() {
        return (ConnectivityManager) mZ.getSystemService("connectivity");
    }

    public static void dd() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) mZ.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
                "Kill background process:" + next.processName + " pid:" + next.pid;
            }
        }
    }

    public static int de() {
        return mZ.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int df() {
        return mZ.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static int dg() {
        return mZ.getResources().getConfiguration().orientation;
    }

    public static Timer dh() {
        return timer;
    }

    public static int di() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static String dj() {
        return Build.MODEL;
    }

    public static String dk() {
        return Build.MANUFACTURER;
    }

    public static String dl() {
        String str = null;
        if (mZ.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = db().getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            "imei=" + str;
        }
        return str;
    }

    public static String dm() {
        String str;
        String string = Settings.Secure.getString(mZ.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
            }
            "androidId=" + str;
            return str;
        }
        str = string;
        "androidId=" + str;
        return str;
    }

    public static Activity getActivity() {
        return mZ;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getString(int i) {
        return mZ.getString(i);
    }

    public static void pause() {
        int i = na + 1;
        na = i;
        if (i == 1) {
            h.b(h.a(MSG_SYSTEM_ON_PAUSE, null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + na);
        }
    }

    public static void resume() {
        int i = na - 1;
        na = i;
        if (i == 0) {
            h.b(h.a(MSG_SYSTEM_ON_RESUME, null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + na);
        }
        if (na <= 0) {
            na = 0;
        }
    }
}
