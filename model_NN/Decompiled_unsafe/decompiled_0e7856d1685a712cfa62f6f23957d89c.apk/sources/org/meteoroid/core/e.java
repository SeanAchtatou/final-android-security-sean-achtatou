package org.meteoroid.core;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class e implements SurfaceHolder.Callback {
    public static final String LOG_TAG = "GraphicsManager";
    public static SurfaceView mh;
    public static final e mi = new e();
    private static boolean mj = false;
    private static SurfaceHolder mk;
    private static final Rect ml = new Rect(-1, -1, -1, -1);
    private static final ConcurrentLinkedQueue<a> mn = new ConcurrentLinkedQueue<>();

    /* renamed from: mm  reason: collision with root package name */
    private boolean f13mm;

    public interface a {
        void onDraw(Canvas canvas);
    }

    public static Bitmap a(int i, int i2, boolean z, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
        createBitmap.eraseColor(-16777216);
        return createBitmap;
    }

    protected static void a(Activity activity) {
        SurfaceView surfaceView = new SurfaceView(activity);
        mh = surfaceView;
        surfaceView.setId(268049792);
        mh.setFocusable(true);
        mh.setFocusableInTouchMode(true);
        mh.setLongClickable(true);
        mh.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        mk = mh.getHolder();
        f.b(mh);
        mk.addCallback(mi);
        try {
            mk.setType(1);
        } catch (Exception e) {
            try {
                mk.setType(2);
            } catch (Exception e2) {
                mk.setType(0);
            }
        }
    }

    public static final void a(a aVar) {
        mn.add(aVar);
    }

    public static final void b(a aVar) {
        mn.remove(aVar);
    }

    public static Bitmap c(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            throw new IOException();
        }
    }

    protected static final void cJ() {
        if (!mi.f13mm) {
            synchronized (mh) {
                try {
                    mh.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (mk == null) {
            mk = mh.getHolder();
        }
        synchronized (mk) {
            Canvas lockCanvas = mk.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(-16777216);
                Iterator<a> it = mn.iterator();
                while (it.hasNext()) {
                    it.next().onDraw(lockCanvas);
                }
                mk.unlockCanvasAndPost(lockCanvas);
            }
        }
        return;
    }

    protected static void onDestroy() {
        mi.f13mm = false;
        mk.removeCallback(mi);
        f.c(mh);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        mi.f13mm = true;
        synchronized (mh) {
            mh.notifyAll();
        }
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        mi.f13mm = true;
        synchronized (mh) {
            mh.notifyAll();
        }
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mi.f13mm = false;
    }
}
