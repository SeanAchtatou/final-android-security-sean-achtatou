package org.meteoroid.core;

import android.os.Message;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

public final class h implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final LinkedBlockingQueue<Message> mO = new LinkedBlockingQueue<>();
    private static final LinkedBlockingQueue<a> mP = new LinkedBlockingQueue<>();
    private static final h mQ = new h();
    private static final SparseArray<String> mR = new SparseArray<>();
    private static final ArrayList<a> mT = new ArrayList<>();
    private boolean mS;

    public interface a {
        boolean a(Message message);
    }

    public static void C(int i) {
        b(i, (Object) null);
    }

    public static Message a(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = 0;
        obtain.arg2 = i3;
        return obtain;
    }

    public static void a(a aVar) {
        if (!mP.contains(aVar)) {
            mP.add(aVar);
        }
    }

    public static void b(int i, Object obj) {
        b(a(i, obj, 0, 0));
    }

    public static void b(int i, String str) {
        mR.append(i, str);
    }

    public static void b(Message message) {
        mO.add(message);
    }

    public static void b(a aVar) {
        mT.add(aVar);
    }

    public static synchronized void c(int i, Object obj) {
        synchronized (h.class) {
            c(a(i, obj, 0, 0));
        }
    }

    private static final void c(Message message) {
        boolean z;
        Iterator<a> it = mP.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            a next = it.next();
            if (next.a(message)) {
                "The message [" + mR.get(message.what) + "] has been consumed by [" + next.getClass().getName() + "]. And " + mO.size() + " messages are waiting.";
                z = true;
                break;
            }
        }
        if (!z) {
            "There is a message [" + mR.get(message.what, Integer.toHexString(message.what)) + "] which no consumer could deal with it. " + mO.size() + " messages left.";
        }
        if (message.arg2 != -13295) {
            message.recycle();
        }
    }

    protected static void cH() {
        new Thread(mQ).start();
    }

    public static final void cS() {
        mO.clear();
    }

    protected static void onDestroy() {
        mQ.mS = true;
        mO.clear();
    }

    public final void run() {
        while (!this.mS) {
            while (true) {
                Message poll = mO.poll();
                if (poll == null) {
                    break;
                }
                c(poll);
            }
            if (!mT.isEmpty()) {
                Iterator<a> it = mT.iterator();
                while (it.hasNext()) {
                    mP.remove(it.next());
                }
                mT.clear();
            }
            Thread.yield();
        }
    }
}
