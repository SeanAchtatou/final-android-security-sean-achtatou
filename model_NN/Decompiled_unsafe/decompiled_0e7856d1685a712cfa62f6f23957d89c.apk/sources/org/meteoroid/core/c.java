package org.meteoroid.core;

import android.util.Log;
import com.a.a.i.a;

public final class c {
    private static final String LOG_TAG = "DeviceManager";
    public static a mf;

    protected static a ay(String str) {
        try {
            a aVar = (a) Class.forName(str).newInstance();
            mf = aVar;
            aVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create device. " + e);
            e.printStackTrace();
        }
        return mf;
    }
}
