package org.meteoroid.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.meteoroid.core.h;

public final class l {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static a nh;
    /* access modifiers changed from: private */
    public static a ni;
    /* access modifiers changed from: private */
    public static FrameLayout nj;
    public static boolean nk = false;
    /* access modifiers changed from: private */
    public static AlertDialog nl;
    /* access modifiers changed from: private */
    public static ProgressDialog progressDialog;

    public interface a {
        void aE();

        void aM();

        View getView();

        boolean onBack();
    }

    protected static void a(Activity activity) {
        h.b((int) MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        FrameLayout frameLayout = new FrameLayout(activity);
        nj = frameLayout;
        frameLayout.setBackgroundColor(-16777216);
        nj.setId(ROOT_VIEW_ID);
        nj.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        nj.requestFocus();
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    if (l.nh == null) {
                        return false;
                    }
                    l.nh.aM();
                    return false;
                } else if (message.what != 47874 || l.nh == null) {
                    return false;
                } else {
                    l.nh.aE();
                    return false;
                }
            }
        });
    }

    public static void a(String str, String str2, View view, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, null, view, null, null, null, null, null, null, null, null, false, null);
    }

    private static void a(String str, String str2, View view, String[] strArr, DialogInterface.OnClickListener onClickListener, String str3, DialogInterface.OnClickListener onClickListener2, String str4, DialogInterface.OnClickListener onClickListener3, String str5, DialogInterface.OnClickListener onClickListener4, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(k.getActivity());
        if (str != null) {
            builder.setTitle(str);
        }
        if (str2 != null) {
            builder.setMessage(str2);
        }
        if (view != null) {
            builder.setView(view);
        }
        if (str3 != null) {
            builder.setPositiveButton(str3, onClickListener2);
        }
        if (str4 != null) {
            builder.setNegativeButton(str4, onClickListener3);
        }
        if (str5 != null) {
            builder.setNeutralButton(str5, onClickListener4);
        }
        builder.setCancelable(z);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        if (builder != null) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    AlertDialog unused = l.nl = builder.create();
                    l.nl.show();
                }
            });
        }
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        a(str, str2, str3, onClickListener, null, null, null, null, false, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, false, null);
    }

    private static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, null, null, null, str3, onClickListener, str4, onClickListener2, null, null, z, onCancelListener);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, true, onCancelListener);
    }

    public static void a(String str, final String str2, boolean z, boolean z2) {
        k.getHandler().post(new Runnable(null, false, true) {
            public final void run() {
                if (l.progressDialog == null) {
                    ProgressDialog unused = l.progressDialog = new ProgressDialog(k.getActivity());
                }
                if (null != null) {
                    l.progressDialog.setTitle(null);
                }
                if (str2 != null) {
                    l.progressDialog.setMessage(str2);
                }
                l.progressDialog.setCancelable(false);
                l.progressDialog.setIndeterminate(true);
                if (!l.progressDialog.isShowing()) {
                    l.progressDialog.show();
                }
            }
        });
    }

    public static void a(final a aVar) {
        if (aVar != null && aVar != nh && aVar.getView() != null) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    if (l.nj.getParent() == null) {
                        k.getActivity().setContentView(l.nj);
                    }
                    if (!(l.nh == null || l.nh.getView() == null)) {
                        l.nj.removeView(l.nh.getView());
                    }
                    if (l.nh != null) {
                        l.nh.aM();
                        a unused = l.ni = l.nh;
                    }
                    l.nj.addView(aVar.getView());
                    if (aVar != null) {
                        aVar.aE();
                    }
                    a unused2 = l.nh = aVar;
                    l.nj.invalidate();
                    l.nh.getView().requestFocus();
                    h.b((int) l.MSG_VIEW_CHANGED, l.nh);
                }
            });
        }
    }

    public static void b(a aVar) {
        ni = nh;
        nh = aVar;
    }

    public static void c(long j) {
        k.getHandler().postDelayed(new Runnable() {
            public final void run() {
                if (l.nl != null && l.nl.isShowing()) {
                    l.nl.dismiss();
                }
            }
        }, j);
    }

    public static void dp() {
        k.getHandler().post(new Runnable() {
            public final void run() {
                if (l.progressDialog != null && l.progressDialog.isShowing()) {
                    l.progressDialog.dismiss();
                }
            }
        });
    }

    public static void dq() {
        k.getHandler().post(new Runnable() {
            public final void run() {
                if (l.nl != null && l.nl.isShowing()) {
                    l.nl.dismiss();
                }
            }
        });
    }

    protected static boolean onBack() {
        if (nh != null) {
            return nh.onBack() || nk;
        }
        return false;
    }

    protected static void onDestroy() {
        ni = null;
        nh = null;
    }
}
