package org.meteoroid.core;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.a.a.i.c;
import org.meteoroid.core.h;

public final class m {
    public static final String LOG_TAG = "VDManager";
    public static final int MSG_SYSTEM_VD_RELOAD = 6913;
    public static final int MSG_SYSTEM_VD_RELOAD_COMPLETE = 6914;
    public static c nr;
    public static boolean ns = false;
    public static boolean nt;
    private static int nu;
    private static int nv;
    /* access modifiers changed from: private */
    public static boolean nw;
    /* access modifiers changed from: private */
    public static boolean nx;
    private static final Thread ny = new Thread() {
        public final void run() {
            while (m.nw) {
                m.dy();
                SystemClock.sleep(200);
            }
        }
    };
    private static int orientation;

    protected static c aE(String str) {
        try {
            c cVar = (c) Class.forName(str).newInstance();
            nr = cVar;
            cVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        h.b((int) MSG_SYSTEM_VD_RELOAD, "MSG_SYSTEM_VD_RELOAD");
        h.b((int) MSG_SYSTEM_VD_RELOAD_COMPLETE, "MSG_SYSTEM_VD_RELOAD_COMPLETE");
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 40965 && m.ns && m.dw()) {
                    k.pause();
                    h.C(m.MSG_SYSTEM_VD_RELOAD);
                } else if (message.what == 6913) {
                    m.nt = true;
                    SystemClock.sleep(100);
                    while (m.nx) {
                        Thread.yield();
                    }
                    m.aF((String) message.obj);
                    h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"ReloadSkin", k.cZ() + "=" + ((String) message.obj)});
                    h.C(m.MSG_SYSTEM_VD_RELOAD_COMPLETE);
                    return true;
                } else if (message.what == 6914) {
                    SystemClock.sleep(100);
                    k.resume();
                    m.nt = false;
                }
                return false;
            }
        });
        dv();
        return nr;
    }

    protected static void aF(String str) {
        nr.aF(str);
        dv();
    }

    private static final void dv() {
        nu = k.de();
        nv = k.df();
        orientation = k.dg();
    }

    public static final boolean dw() {
        return (nu == k.de() && nv == k.df() && orientation == k.dg()) ? false : true;
    }

    public static void dx() {
        if (!nw && !ny.isAlive()) {
            nw = true;
            ny.start();
        }
    }

    public static void dy() {
        if (!nt) {
            nx = true;
            e.cJ();
            nx = false;
        }
    }

    protected static void onDestroy() {
        if (nr != null) {
            nr.onDestroy();
        }
    }
}
