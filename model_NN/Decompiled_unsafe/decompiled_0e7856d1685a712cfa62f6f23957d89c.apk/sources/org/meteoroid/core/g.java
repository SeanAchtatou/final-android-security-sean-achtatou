package org.meteoroid.core;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Message;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.meteoroid.core.h;

public final class g {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static int mA = 15;
    /* access modifiers changed from: private */
    public static int mB;
    private static float mC;
    private static AudioManager mD;
    /* access modifiers changed from: private */
    public static final ConcurrentLinkedQueue<a> mE = new ConcurrentLinkedQueue<>();
    private static int mF = 0;
    private static int mG;
    private static int mH = 0;
    /* access modifiers changed from: private */
    public static int mI = 0;
    private static final g mx = new g();
    private static int my = -1;
    private static String mz;

    public class a {
        public String mJ;
        public boolean mK;
        public int mL;
        public MediaPlayer mM;
        public String name;
        public String type;

        public a() {
        }

        public final void recycle() {
            if (this.name != null) {
                g.mE.remove(this.name);
            }
            if (this.mM != null) {
                this.mM.release();
            }
            this.mM = null;
        }
    }

    public static a a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        mF++;
        g gVar = mx;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str2.indexOf("mid") != -1) {
            str3 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str3 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str3 = ".amr";
        }
        aVar.name = null;
        aVar.type = str2;
        aVar.mJ = mF + str3;
        FileOutputStream openFileOutput = k.getActivity().openFileOutput(aVar.mJ, 1);
        byte[] bArr = new byte[256];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                aVar.mM = new MediaPlayer();
                aVar.mJ = mz + aVar.mJ;
                mE.add(aVar);
                "Create a media clip " + ((String) null) + " [" + str2 + "].";
                return aVar;
            }
        }
    }

    protected static void a(Activity activity) {
        mz = activity.getFilesDir().getAbsolutePath() + File.separator;
        AudioManager audioManager = (AudioManager) activity.getSystemService("audio");
        mD = audioManager;
        mA = audioManager.getStreamMaxVolume(3);
        "Max volume is" + mA;
        mC = ((float) mA) / 100.0f;
        "VOLUME_TRANS_RATIO is" + mC;
        mB = cN();
        "Init volume is" + mB;
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    Iterator it = g.mE.iterator();
                    while (it.hasNext()) {
                        a aVar = (a) it.next();
                        try {
                            if (aVar.mM != null && aVar.mM.isPlaying() && aVar.mK) {
                                aVar.mM.pause();
                                "force to pause:" + aVar.name;
                            }
                        } catch (IllegalStateException e) {
                            Log.w(g.LOG_TAG, "error to pause:" + aVar.name);
                        }
                    }
                    int unused = g.mI = g.mD.getStreamVolume(3);
                    "Pause volume is" + g.mI;
                    g.y(g.mB);
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    g.mD.setStreamVolume(3, g.mI, 16);
                    Iterator it2 = g.mE.iterator();
                    while (it2.hasNext()) {
                        a aVar2 = (a) it2.next();
                        try {
                            if (aVar2.mM != null && aVar2.mK && !aVar2.mM.isPlaying()) {
                                aVar2.mM.start();
                                "force to resume:" + aVar2.name;
                            }
                        } catch (IllegalStateException e2) {
                            Log.w(g.LOG_TAG, "error to resume:" + aVar2.name);
                        }
                    }
                    return false;
                }
            }
        });
        if (my == -2) {
            y(70);
        }
    }

    public static void a(a aVar) {
        if (mE.contains(aVar)) {
            mE.remove(aVar.name);
        }
    }

    public static int cN() {
        "Current volume is:" + mD.getStreamVolume(3);
        return (int) (((float) mD.getStreamVolume(3)) / mC);
    }

    public static void h(boolean z) {
        "Mute works:" + z;
        if (z) {
            mH = cN();
            x(0);
        } else if (mH != 0) {
            x(mH);
        }
    }

    protected static void onDestroy() {
        mE.clear();
        y(mB);
    }

    public static void w(int i) {
        my = i;
    }

    public static void x(int i) {
        if (my == -1) {
            y(i);
        } else if (my == -2) {
            if (i == 0) {
                mG = cN();
                y(0);
            } else if (mG != 0) {
                y(mG);
                mG = 0;
            }
            "Failed to set volume because the globe volume mode is control by device." + mG;
        }
    }

    /* access modifiers changed from: private */
    public static void y(int i) {
        "Set device volume to " + i;
        mD.setStreamVolume(3, (int) (((float) i) * mC), 16);
    }
}
