package org.meteoroid.core;

import android.util.Log;
import com.a.a.i.b;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class d {
    public static final String LOG_TAG = "FeatureManager";
    private static LinkedHashSet<b> mg = new LinkedHashSet<>();

    protected static void cH() {
    }

    public static b o(String str, String str2) {
        Exception e;
        b bVar;
        try {
            bVar = (b) Class.forName(str).newInstance();
            try {
                bVar.aG(str2);
                mg.add(bVar);
                bVar.getName() + " has added.";
            } catch (Exception e2) {
                e = e2;
                Log.w(LOG_TAG, e);
                return bVar;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            bVar = null;
            e = exc;
            Log.w(LOG_TAG, e);
            return bVar;
        }
        return bVar;
    }

    protected static void onDestroy() {
        if (!mg.isEmpty()) {
            Iterator<b> it = mg.iterator();
            while (it.hasNext()) {
                b next = it.next();
                next.onDestroy();
                next.getName() + " has destroyed.";
            }
        }
    }
}
