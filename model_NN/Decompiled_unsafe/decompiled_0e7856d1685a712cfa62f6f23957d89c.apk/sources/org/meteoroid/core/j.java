package org.meteoroid.core;

import android.content.SharedPreferences;

public final class j {

    public static final class a {
        private SharedPreferences mY;

        private a(int i) {
            this.mY = k.getActivity().getPreferences(i);
        }

        /* synthetic */ a(int i, byte b) {
            this(i);
        }

        public final SharedPreferences.Editor getEditor() {
            return this.mY.edit();
        }

        public final SharedPreferences getSharedPreferences() {
            return this.mY;
        }
    }

    public static a D(int i) {
        return new a(0, (byte) 0);
    }
}
