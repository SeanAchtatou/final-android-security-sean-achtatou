package org.meteoroid.core;

import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public final class b {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    private static String au(String str) {
        return str.replace(File.separator, "_");
    }

    public static boolean av(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String au = au(str);
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String au2 = au(au);
        String[] fileList = k.getActivity().fileList();
        ArrayList<String> arrayList = new ArrayList<>();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(au2 == null || fileList[i].indexOf(au2) == -1) || au2 == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - 4));
                    "Find data file:" + fileList[i];
                }
            }
        }
        for (String equals : arrayList) {
            if (equals.equals(au)) {
                "Find data:" + au;
                return true;
            }
        }
        "Find no data:" + au;
        return false;
    }

    public static OutputStream aw(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String au = au(str);
        "Update data:" + au;
        return k.getActivity().openFileOutput(au + DATA_STORE_FILE, 1);
    }

    public static InputStream ax(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String au = au(str);
        "Read data:" + au;
        return k.getActivity().openFileInput(au + DATA_STORE_FILE);
    }
}
