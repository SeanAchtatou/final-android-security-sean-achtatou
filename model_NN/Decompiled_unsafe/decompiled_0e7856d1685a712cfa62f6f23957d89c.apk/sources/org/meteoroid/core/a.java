package org.meteoroid.core;

import android.os.Message;
import android.util.Log;
import java.util.HashMap;
import org.meteoroid.core.h;

public final class a {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_NEED_PAUSE = 47623;
    public static final int MSG_APPLICATION_NEED_START = 47622;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static C0005a md;

    /* renamed from: me  reason: collision with root package name */
    private static final HashMap<String, String> f12me = new HashMap<>();

    /* renamed from: org.meteoroid.core.a$a  reason: collision with other inner class name */
    public interface C0005a {
        public static final int EXIT = 3;
        public static final int INIT = 0;
        public static final int PAUSE = 2;
        public static final int RUN = 1;

        void aY();

        int getState();

        void onDestroy();

        void onPause();

        void onResume();

        void onStart();
    }

    public static void at(String str) {
        if (str != null) {
            try {
                md = (C0005a) Class.forName(str).newInstance();
                h.b(h.a(MSG_APPLICATION_LAUNCH, md));
            } catch (Exception e) {
                e.printStackTrace();
                Log.w(LOG_TAG, e.toString());
            }
            if (md != null) {
                md.aY();
            } else {
                Log.w(LOG_TAG, "The application failed to launch.");
            }
        } else {
            Log.w(LOG_TAG, "No available application could launch.");
        }
    }

    protected static void cH() {
        h.b((int) MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        h.b((int) MSG_APPLICATION_START, "MSG_APPLICATION_START");
        h.b((int) MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        h.b((int) MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        h.b((int) MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        h.b((int) MSG_APPLICATION_NEED_START, "MSG_APPLICATION_NEED_START");
        h.b((int) MSG_APPLICATION_NEED_PAUSE, "MSG_APPLICATION_NEED_PAUSE");
        h.a(new h.a() {
            public final boolean a(Message message) {
                if (message.what == 47873) {
                    a.pause();
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    a.resume();
                    return false;
                }
            }
        });
    }

    public static boolean cI() {
        return md != null && md.getState() == 1;
    }

    public static void n(String str, String str2) {
        f12me.put(str, str2);
    }

    protected static void onDestroy() {
        if (md != null && md.getState() != 3) {
            md.onDestroy();
            h.b(h.a(MSG_APPLICATION_QUIT, md));
        }
    }

    public static void pause() {
        if (md == null) {
            return;
        }
        if (md.getState() == 1) {
            md.onPause();
            h.b(h.a(MSG_APPLICATION_PAUSE, md));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + md.getState());
    }

    public static void resume() {
        if (md == null) {
            return;
        }
        if (md.getState() == 2) {
            md.onResume();
            h.b(h.a(MSG_APPLICATION_RESUME, md));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + md.getState());
    }

    public static void start() {
        if (md == null) {
            return;
        }
        if (md.getState() == 0) {
            md.onStart();
            h.b(h.a(MSG_APPLICATION_START, md));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + md.getState());
    }
}
