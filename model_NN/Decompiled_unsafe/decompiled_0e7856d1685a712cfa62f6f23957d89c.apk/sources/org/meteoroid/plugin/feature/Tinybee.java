package org.meteoroid.plugin.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Message;
import com.a.a.i.b;
import java.util.HashMap;
import java.util.Map;
import me.gall.tinybee.Logger;
import me.gall.tinybee.LoggerManager;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;

public class Tinybee implements b, Logger.OnlineParamCallback, h.a {
    private static final long DISABLE = 0;
    private String appId;
    private String channelId = "";
    private String channelName = "";
    private String pK;
    private String pL;
    private String pM;
    private Logger pN;
    /* access modifiers changed from: private */
    public Logger pO;
    private long pP = 0;

    public final boolean a(Message message) {
        HashMap hashMap;
        HashMap hashMap2;
        int i = 1;
        if (message.what == 40960) {
            if (this.pO != null) {
                this.pO.onPause((Context) message.obj);
            }
            if (this.pN != null) {
                this.pN.onPause((Context) message.obj);
            }
        } else if (message.what == 40961) {
            if (this.pO != null) {
                this.pO.onResume((Context) message.obj);
            }
            if (this.pN != null) {
                this.pN.onResume((Context) message.obj);
            }
        } else if (message.what == 47886) {
            if (message.obj != null) {
                if (message.obj instanceof String) {
                    this.pK = (String) message.obj;
                    this.pN = LoggerManager.getLogger(k.getActivity(), this.pK);
                } else if (message.obj instanceof String[]) {
                    String[] strArr = (String[]) message.obj;
                    if (strArr.length == 3) {
                        this.pK = strArr[0];
                        this.pL = strArr[1];
                        this.pM = strArr[2];
                    }
                    this.pN = LoggerManager.getLogger(k.getActivity(), this.pK, this.pL, this.pM);
                }
            }
        } else if (message.what == 47887) {
            if (message.obj instanceof String[]) {
                String[] strArr2 = (String[]) message.obj;
                if (strArr2.length == 1) {
                    this.pO.send(strArr2[0]);
                } else {
                    if (strArr2.length == 2) {
                        HashMap hashMap3 = new HashMap();
                        hashMap3.put("value", strArr2[1]);
                        hashMap2 = hashMap3;
                    } else if (strArr2.length > 2) {
                        HashMap hashMap4 = new HashMap();
                        while (i < strArr2.length) {
                            if (i + 1 < strArr2.length) {
                                hashMap4.put(strArr2[i], strArr2[i + 1]);
                            }
                            i += 2;
                        }
                        hashMap2 = hashMap4;
                    }
                    this.pO.send(strArr2[0], hashMap2);
                }
            }
        } else if (message.what == 47902 && (message.obj instanceof String[]) && this.pN != null) {
            String[] strArr3 = (String[]) message.obj;
            if (strArr3.length == 1) {
                this.pN.send(strArr3[0]);
            } else {
                if (strArr3.length == 2) {
                    HashMap hashMap5 = new HashMap();
                    hashMap5.put("value", strArr3[1]);
                    hashMap = hashMap5;
                } else if (strArr3.length > 2) {
                    HashMap hashMap6 = new HashMap();
                    while (i < strArr3.length) {
                        if (i + 1 < strArr3.length) {
                            hashMap6.put(strArr3[i], strArr3[i + 1]);
                        }
                        i += 2;
                    }
                    hashMap = hashMap6;
                }
                this.pN.send(strArr3[0], hashMap);
            }
        }
        return false;
    }

    public final void aG(String str) {
        boolean z = false;
        com.a.a.j.b bVar = new com.a.a.j.b(str);
        String aI = bVar.aI("COLLECTINFO");
        if (aI != null) {
            this.pP = Long.parseLong(aI) * 1000 * 60 * 60 * 24;
        }
        if (this.pP != 0) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = j.D(0).getSharedPreferences();
            long j = sharedPreferences.getLong("LastDay", 0);
            if (j == 0 || currentTimeMillis - j >= this.pP) {
                sharedPreferences.edit().putLong("LastDay", currentTimeMillis).commit();
                new Thread() {
                    public final void run() {
                        String dm;
                        StringBuffer stringBuffer = new StringBuffer();
                        try {
                            dm = k.db().getDeviceId();
                        } catch (Exception e) {
                            Exception exc = e;
                            dm = k.dm();
                            exc.printStackTrace();
                        }
                        stringBuffer.append(dm + "=");
                        stringBuffer.append(k.dk() + "=");
                        stringBuffer.append(k.dj() + "=");
                        for (ApplicationInfo next : k.getActivity().getPackageManager().getInstalledApplications(128)) {
                            if (!next.packageName.startsWith("com.google") && !next.packageName.startsWith("com.android")) {
                                stringBuffer.append(next.packageName + ",");
                            }
                        }
                        HashMap hashMap = new HashMap();
                        hashMap.put("value", stringBuffer.toString());
                        Tinybee.this.pO.send("CollectAppInfo", hashMap);
                    }
                }.start();
                getClass().getSimpleName();
            }
        }
        String aI2 = bVar.aI("APP_ID");
        if (aI2 != null) {
            this.appId = aI2;
        }
        String aI3 = bVar.aI("CHANNEL_ID");
        if (aI3 != null) {
            this.channelId = aI3;
        }
        String aI4 = bVar.aI("CHANNEL_NAME");
        if (aI4 != null) {
            this.channelName = aI4;
        }
        String aI5 = bVar.aI("TEST");
        if (aI5 != null) {
            z = Boolean.parseBoolean(aI5);
        }
        this.pO = LoggerManager.getLogger(k.getActivity(), this.appId, this.channelId, this.channelName, z);
        this.pO.setOnlineParamCallback(this);
        h.a(this);
    }

    public final String getName() {
        return getClass().getSimpleName();
    }

    public final void onDestroy() {
        if (this.pO != null) {
            this.pO.finish();
        }
        if (this.pN != null) {
            this.pN.finish();
        }
    }

    public void requestComplete(Map<String, String> map) {
        getClass().getSimpleName();
        if (map != null && !map.isEmpty()) {
            getClass().getSimpleName();
            "requestComplete online param size=" + map.size();
            h.b((int) k.MSG_SYSTEM_ONLINE_PARAM, map);
        }
    }

    public void requestError() {
        getClass().getSimpleName();
    }
}
