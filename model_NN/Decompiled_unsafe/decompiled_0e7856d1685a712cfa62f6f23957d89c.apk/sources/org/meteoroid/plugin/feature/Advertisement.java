package org.meteoroid.plugin.feature;

import android.content.Context;
import com.a.a.i.b;
import java.util.TimerTask;
import org.meteoroid.core.h;

public abstract class Advertisement extends TimerTask implements b, h.a {
    private static final String ADVERTISEMENTS = "_AB_ADVERTISEMENTS";
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    public static final int MSG_ADVERTISEMENT_CLICK = -2128412415;
    public static final int MSG_ADVERTISEMENT_SHOW = -2128412416;
    private static final String STATUS = "STATUS";
    private int duration = 20;
    private boolean pA = false;
    private int pB = 1000;
    private boolean pC = true;
    private boolean pv = false;
    private boolean pw = false;
    private int px = 60;
    private boolean py = false;
    private boolean pz = false;

    public static boolean O(Context context) {
        return context.getSharedPreferences(ADVERTISEMENTS, 0).getBoolean(STATUS, false);
    }
}
