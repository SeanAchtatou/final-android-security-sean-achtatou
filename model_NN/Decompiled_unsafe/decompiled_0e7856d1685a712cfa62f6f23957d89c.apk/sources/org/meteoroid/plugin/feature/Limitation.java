package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Message;
import android.util.Log;
import com.a.a.i.b;
import com.androidbox.hywxfktj4.R;
import java.util.Arrays;
import java.util.Map;
import java.util.TimerTask;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;
import org.meteoroid.core.l;

public class Limitation extends TimerTask implements DialogInterface.OnClickListener, b, h.a {
    private static final String CMCC = "cmcc";
    private static final String CT = "ct";
    private static final String LIMITATION_PACKAGE = "LIMITATION_PACKAGE";
    private static final String OTHER = "other";
    private static final String UNICOM = "unicom";
    private String pD;
    private String[] pE;
    private String pF;
    private boolean pG;
    private com.a.a.j.b pg;
    private String targetPackage;
    private long time = 0;
    private String url;

    private void dE() {
        l.a(k.getString(R.string.alert), this.pF, k.getString(R.string.confirm), this, k.getString(R.string.exit), this);
    }

    private boolean dF() {
        if (this.pE != null) {
            boolean z = false;
            for (int i = 0; i < this.pE.length; i++) {
                if (this.pE[i] != null && k.aC(this.pE[i])) {
                    getClass().getSimpleName();
                    this.pE[i] + " is " + (this.pG ? "include" : "exclude");
                    h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"Limitation", "目标", this.pE[i], "应用程序", k.cZ()});
                    this.targetPackage = this.pE[i];
                    z = true;
                }
            }
            if (z && this.pG) {
                return true;
            }
            if (z && !this.pG) {
                return false;
            }
            if (z || !this.pG) {
                return !z && !this.pG;
            }
            return false;
        } else if (this.pD == null) {
            return true;
        } else {
            String str = null;
            try {
                String cV = k.cV();
                if (cV != null) {
                    if (cV.startsWith("46000") || cV.startsWith("46002")) {
                        getClass().getSimpleName();
                        str = CMCC;
                    } else if (cV.startsWith("46001")) {
                        getClass().getSimpleName();
                        str = UNICOM;
                    } else if (cV.startsWith("46003")) {
                        getClass().getSimpleName();
                        str = CT;
                    } else {
                        str = OTHER;
                    }
                }
            } catch (Exception e) {
                Log.w(getClass().getSimpleName(), e);
                str = OTHER;
            }
            return !str.equalsIgnoreCase(this.pD);
        }
    }

    public final boolean a(Message message) {
        if (message.what == 47872) {
            if (dF()) {
                dE();
                return true;
            }
            if (this.time > 0) {
                k.dh().schedule(this, this.time, this.time);
            }
            h.b(this);
        } else if (message.what == 47885) {
            Map map = (Map) message.obj;
            if (map.containsKey("PACKAGE")) {
                j.D(0).getSharedPreferences().edit().putString(LIMITATION_PACKAGE, (String) map.get("PACKAGE")).commit();
                getClass().getSimpleName();
                "Update " + ((String) map.get("PACKAGE")) + " complete.";
            }
        }
        return false;
    }

    public final void aG(String str) {
        this.pg = new com.a.a.j.b(str);
        String aI = this.pg.aI("CARRIER");
        if (aI != null) {
            this.pD = aI;
        }
        if (j.D(0).getSharedPreferences().contains(LIMITATION_PACKAGE)) {
            this.pE = j.D(0).getSharedPreferences().getString(LIMITATION_PACKAGE, "").split(";");
            getClass().getSimpleName();
            "Find update. " + Arrays.toString(this.pE);
        } else {
            String aI2 = this.pg.aI("PACKAGE");
            if (aI2 != null) {
                this.pE = aI2.split(";");
                getClass().getSimpleName();
                "Use local. " + Arrays.toString(this.pE);
            }
        }
        String aI3 = this.pg.aI("URL");
        if (aI3 != null) {
            this.url = aI3;
        }
        String aI4 = this.pg.aI("MSG");
        if (aI4 != null) {
            this.pF = aI4;
        }
        String aI5 = this.pg.aI("INCLUDE");
        if (aI5 != null) {
            this.pG = Boolean.parseBoolean(aI5);
        }
        String aI6 = this.pg.aI("TIME");
        if (aI6 != null) {
            this.time = Long.parseLong(aI6) * 1000;
        }
        h.a(this);
    }

    public final String getName() {
        return getClass().getSimpleName();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        if (i == -1) {
            if (this.targetPackage != null && this.pG) {
                k.getActivity().startActivity(new Intent("android.intent.action.DELETE", Uri.parse("package:" + this.targetPackage)));
            }
            if (this.url != null) {
                k.aB(this.url);
            }
            k.cU();
        }
        if (i == -2) {
            k.cU();
        }
    }

    public final void onDestroy() {
    }

    public void run() {
        if (dF()) {
            dE();
        }
    }
}
