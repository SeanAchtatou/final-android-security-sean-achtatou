package org.meteoroid.plugin.feature;

import android.os.Message;
import android.util.Log;
import com.a.a.j.b;
import me.gall.totalpay.android.e;
import me.gall.totalpay.android.f;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.plugin.device.MIDPDevice;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public class Totalpay implements f.a, h.a, AbstractPaymentManager.Payment {
    private static final String[] pR = {"话费", "第三方支付", "禁用"};
    private String bd;
    private String pD;
    private int pS;
    private String pT;
    private String pU;
    private String pV;
    private int pW;
    private String type;

    private void dG() {
        if (this.pS == 0) {
            this.pS = 400;
        }
        if (this.pT == null) {
            this.pT = k.cZ();
        }
        try {
            this.pD = me.gall.totalpay.android.h.getCarrierName();
            f.pay(this.pS, this.pT, this.type, this.bd, this);
        } catch (Exception e) {
            e.printStackTrace();
            onCancel();
        }
    }

    public final boolean a(Message message) {
        if (message.what == 15391744) {
            if (message.obj == null) {
                dG();
            } else {
                com.a.a.g.f fVar = (com.a.a.g.f) message.obj;
                String address = fVar.getAddress();
                if (address.startsWith("sms://")) {
                    address = address.substring(6);
                }
                if (address.startsWith("+86")) {
                    address = address.substring(3);
                }
                if (address.length() == 11 && (address.startsWith("13") || address.startsWith("15") || address.startsWith("18") || address.startsWith("14"))) {
                    return false;
                }
                String bd2 = fVar.bd();
                try {
                    if (bd2.contains(":::")) {
                        String[] split = bd2.split(":::");
                        this.pT = split[0];
                        this.type = split[1];
                        this.bd = split[2];
                        this.pS = Integer.parseInt(split[3]);
                    } else {
                        this.pS = Integer.parseInt(bd2);
                    }
                } catch (Exception e) {
                    Log.w(getClass().getSimpleName(), "Sms content is not valid price no[" + fVar.bd() + "]." + e);
                }
                dG();
            }
            return true;
        }
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        return false;
    }

    public final String aF() {
        return getClass().getSimpleName();
    }

    public final void aG(String str) {
        b bVar = new b(str);
        String aI = bVar.aI("PRICE");
        if (aI != null) {
            this.pS = Integer.parseInt(aI);
        }
        String aI2 = bVar.aI("CID");
        if (aI2 != null) {
            f.setChannelId(k.getActivity(), aI2);
        }
        String aI3 = bVar.aI("PRODUCT");
        if (aI3 != null) {
            this.pT = aI3;
        }
        String aI4 = bVar.aI("TYPE");
        if (aI4 != null) {
            this.type = aI4;
        }
        String aI5 = bVar.aI("MSG");
        if (aI5 != null) {
            this.bd = aI5;
        }
        String aI6 = bVar.aI("TEST");
        if (aI6 != null) {
            f.setTestMode(k.getActivity(), Boolean.parseBoolean(aI6));
        }
        try {
            f.init(k.getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String aI7 = bVar.aI("BLACKLIST");
        if (aI7 != null && !Boolean.parseBoolean(aI7)) {
            f.disableBlacklist(k.getActivity());
        }
        h.a(this);
    }

    public final String getName() {
        return getClass().getSimpleName();
    }

    public void onCancel() {
        h.b(h.a(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND_FAIL, null));
        h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"TotalpayCancel", "应用名称", k.cZ(), "运营商", this.pD});
        getClass().getSimpleName();
        "Totalpay cancel." + this.pU;
    }

    public void onComplete() {
        h.b(h.a(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, null));
        h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"TotalpayComplete", "total", k.cZ() + "_" + this.pS, "应用名称", k.cZ(), "运营商", this.pD, "总价", String.valueOf(this.pS)});
    }

    public final void onDestroy() {
        f.destroy();
    }

    public void onFail(String str, e eVar) {
        if (eVar == null) {
            this.pU = "-1";
            this.pV = "未知";
            this.pW = 0;
        } else {
            this.pU = eVar.getId();
            this.pV = eVar.getRemark();
            if (this.pV == null) {
                this.pV = "";
            }
            this.pW = eVar.getPrice();
        }
        h.b(h.a(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND_FAIL, null));
        h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"TotalpayFail", "应用名称", k.cZ(), "fid", this.pU, "价格", String.valueOf(this.pW), "运营商", this.pD, "类型", pR[eVar.getMode()]});
        getClass().getSimpleName();
        "Totalpay fail..." + this.pU;
    }

    public void onSuccess(e eVar) {
        if (eVar == null) {
            h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"TotalpayEmpty", "应用名称", k.cZ(), "运营商", this.pD, "机型", k.dj(), "IMEI", k.dl()});
        } else {
            this.pU = eVar.getId();
            this.pV = eVar.getRemark();
            if (this.pV == null) {
                this.pV = "";
            }
            this.pW = eVar.getPrice();
            if (eVar.getType().equals("mercury")) {
                this.pW = 0;
            }
            h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"TotalpaySuccess", "应用名称", k.cZ(), "fid", this.pU, "价格", String.valueOf(this.pW), "运营商", this.pD, "类型", pR[eVar.getMode()]});
        }
        getClass().getSimpleName();
        "Totalpay success." + this.pU;
    }
}
