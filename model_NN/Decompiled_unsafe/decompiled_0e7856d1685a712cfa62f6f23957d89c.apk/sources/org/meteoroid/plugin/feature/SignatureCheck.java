package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import com.a.a.i.b;
import com.a.a.j.a;
import java.security.MessageDigest;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;

public class SignatureCheck implements b, h.a {
    private boolean pH = false;
    private String pI;

    public final boolean a(Message message) {
        if (message.what != 47872 || this.pH) {
            return false;
        }
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"SignatureCheckError", "应用名称", k.cZ(), "sign", this.pI});
        l.a("警告", "您正在使用破解版产品，该产品可能会侵害您的利益，请打客服电话010-68948836举报。", "退出", new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                k.cU();
            }
        });
        "Invalid signature:" + this.pI;
        return true;
    }

    public final void aG(String str) {
        int i = 0;
        String aI = new com.a.a.j.b(str).aI("SIGN");
        try {
            this.pI = a.q(MessageDigest.getInstance("SHA-1").digest(k.getActivity().getPackageManager().getPackageInfo(k.getActivity().getPackageName(), 64).signatures[0].toByteArray()));
            if (aI != null) {
                String[] split = aI.split(";");
                while (true) {
                    if (i >= split.length) {
                        break;
                    } else if (split[i].equals(this.pI)) {
                        this.pH = true;
                        break;
                    } else {
                        i++;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        h.a(this);
    }

    public final String getName() {
        return "SignatureCheck";
    }

    public final void onDestroy() {
    }
}
