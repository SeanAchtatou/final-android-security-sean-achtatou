package org.meteoroid.plugin.feature;

import android.content.Context;
import android.os.Message;
import com.a.a.i.b;
import com.chinaMobile.MobileAgent;
import org.meteoroid.core.h;
import org.meteoroid.core.k;

public class MMStats implements b, h.a {
    public final boolean a(Message message) {
        if (message.what == 40960) {
            MobileAgent.onPause((Context) message.obj);
        } else if (message.what == 40961) {
            MobileAgent.onResume((Context) message.obj);
        } else if (message.what == 47887) {
            String[] strArr = (String[]) message.obj;
            if (strArr.length == 1) {
                MobileAgent.onEvent(k.getActivity(), strArr[0]);
            } else if (strArr.length == 2) {
                MobileAgent.onEvent(k.getActivity(), strArr[0], strArr[1]);
            } else if (strArr.length > 2) {
                MobileAgent.onEvent(k.getActivity(), strArr[0], strArr[2]);
            }
        }
        return false;
    }

    public final void aG(String str) {
        h.a(this);
    }

    public final String getName() {
        return getClass().getSimpleName();
    }

    public final void onDestroy() {
    }
}
