package org.meteoroid.plugin.feature;

import android.os.Message;
import com.a.a.i.b;
import org.meteoroid.core.h;
import org.meteoroid.core.i;

public abstract class AbstractOptionMenuItemFeature implements b, h.a, i.c {
    private String label = "Unknown";
    private com.a.a.j.b pg;

    public final boolean a(Message message) {
        if (message.what != 47872) {
            return false;
        }
        i.a(this);
        h.b(this);
        return false;
    }

    public final String aF() {
        return this.label;
    }

    public void aG(String str) {
        this.pg = new com.a.a.j.b(str);
        String aI = this.pg.aI("LABEL");
        if (aI != null) {
            this.label = aI;
        }
        h.a(this);
    }

    public final void aH(String str) {
        this.label = str;
    }

    public final int getId() {
        return 143858403;
    }

    public final String getName() {
        return this.label;
    }

    public final String getValue(String str) {
        return this.pg.aI(str);
    }
}
