package org.meteoroid.plugin.feature;

import org.meteoroid.core.h;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractSMSPayment implements h.a, AbstractPaymentManager.Payment {
    public static final int CARRIER_CMCC = 1;
    public static final int CARRIER_TELECOM = 3;
    public static final int CARRIER_UNICOM = 2;
    public static final int CARRIER_UNKNOWN = 0;
    public static final String SMS_DELIEVERED_ACTION = "org.meteoroid.plugin.feature.sms_delievered";
    public static final String SMS_RECV_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    public static final String SMS_SENT_ACTION = "org.meteoroid.plugin.feature.sms_sent";
    private static final String[] pr = {"未知运营商", "中国移动", "中国联通", "中国电信"};
}
