package org.meteoroid.plugin.feature;

public abstract class AbstractTrialActivation extends AbstractPaymentManager {
    public static final int LAUNCH_LIMIT = 1;
    public static final int TIME_LIMIT = 0;
    private int mode = 0;
    private int ps = 60000;
    private int pt = 0;
    private boolean pu = false;
}
