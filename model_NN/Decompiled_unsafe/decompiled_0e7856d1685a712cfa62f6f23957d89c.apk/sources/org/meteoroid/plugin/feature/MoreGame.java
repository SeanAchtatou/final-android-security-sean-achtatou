package org.meteoroid.plugin.feature;

import com.androidbox.hywxfktj4.R;
import org.meteoroid.core.k;

public class MoreGame extends AbstractOptionMenuItemFeature {
    private String url;

    public final void aG(String str) {
        super.aG(str);
        String value = getValue("URL");
        if (value != null) {
            this.url = value;
        }
        aH(k.getString(R.string.more_game));
    }

    public final void aH() {
        if (this.url != null) {
            k.aB(this.url);
        }
    }

    public final void onDestroy() {
    }
}
