package org.meteoroid.plugin.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import com.a.a.j.c;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.meteoroid.core.h;
import org.meteoroid.core.j;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractDownloadAndInstall extends BroadcastReceiver implements h.a, AbstractPaymentManager.Payment {
    public static final int INSTALL_FAIL = 3;
    public static final int INSTALL_NOT_START = 0;
    public static final int INSTALL_RUNNING = 1;
    public static final int INSTALL_SUCCESS = 2;
    private static final String fileName = "target.apk";
    private a[] pa;
    private ArrayList<a> pb;
    private a pc;
    private boolean pd;
    private int pe = 0;
    private boolean pf;
    public com.a.a.j.b pg;
    public boolean ph;
    /* access modifiers changed from: private */
    public String pi;
    /* access modifiers changed from: private */
    public String pj;

    public class a {
        public String packageName;
        public String pk;
        public boolean pl;
        public String pm;
        public String pn;

        public a() {
        }
    }

    private final class b extends AsyncTask<URL, Integer, Long> {
        private b() {
        }

        /* synthetic */ b(AbstractDownloadAndInstall abstractDownloadAndInstall, byte b) {
            this();
        }

        /* access modifiers changed from: private */
        public Long doInBackground(URL... urlArr) {
            File file;
            try {
                if (!new File(AbstractDownloadAndInstall.this.pj).exists()) {
                    File file2 = new File(AbstractDownloadAndInstall.this.pi);
                    file2.mkdirs();
                    if (!k.cW()) {
                        return 0L;
                    }
                    HttpURLConnection httpURLConnection = (HttpURLConnection) urlArr[0].openConnection();
                    httpURLConnection.setRequestMethod("GET");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    File file3 = new File(file2, AbstractDownloadAndInstall.fileName);
                    try {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        AbstractDownloadAndInstall.d(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        file = file3;
                        Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                        if (file != null && file.exists()) {
                            file.delete();
                        }
                        return -1L;
                    }
                }
                return 100L;
            } catch (IOException e2) {
                file = null;
                Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                file.delete();
                return -1L;
            }
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Long l = (Long) obj;
            if (l.longValue() == 100) {
                l.dp();
                AbstractDownloadAndInstall.this.installApk();
            } else if (l.longValue() == -1) {
                l.dp();
                if (!AbstractDownloadAndInstall.this.ph) {
                    h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                }
            } else {
                AbstractDownloadAndInstall.this.getName();
                "On Post Execute result..." + l;
            }
        }
    }

    private void b(Exception exc) {
        this.pe = 3;
        int i = this.pe;
        if (exc != null) {
            Log.w(getName(), "Invalid package:" + this.pc.packageName + exc);
        }
        if (!this.ph) {
            h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        }
    }

    /* access modifiers changed from: private */
    public static void d(InputStream inputStream) {
        FileOutputStream openFileOutput = k.getActivity().openFileOutput(fileName, 1);
        byte[] bArr = new byte[102400];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void installApk() {
        if (this.pj == null) {
            this.pi = k.getActivity().getFilesDir().getAbsolutePath() + File.separator;
            this.pj = this.pi + fileName;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.pj)), "application/vnd.android.package-archive");
        k.getActivity().startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.l.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.l.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.l.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public final void a(a aVar) {
        if (aVar.pl) {
            k.d("不能重复安装同一个应用程序", 0);
            if (!this.ph) {
                h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                return;
            }
            return;
        }
        this.pc = aVar;
        this.pe = 1;
        int i = this.pe;
        h.b(h.a(k.MSG_SYSTEM_LOG_EVENT, new String[]{"StartInstallApp", k.cZ() + "=" + aVar.packageName}));
        if (aVar.pk.startsWith("market:") || aVar.pk.startsWith("http:") || aVar.pk.startsWith("https:")) {
            k.aB(aVar.pk);
        } else if (aVar.pk.startsWith("download:")) {
            String str = "http:" + aVar.pk.substring(9);
            l.a((String) null, "正在下载", false, true);
            try {
                new b(this, (byte) 0).execute(new URL(str));
            } catch (MalformedURLException e) {
                l.dp();
                b(e);
            }
        } else {
            try {
                InputStream az = k.az(c.aK(this.pc.pk));
                d(az);
                az.close();
                installApk();
            } catch (Exception e2) {
                b(e2);
            }
        }
    }

    public final boolean a(Message message) {
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        if (message.what != 40961 || this.pe != 1) {
            return false;
        }
        b((Exception) null);
        return false;
    }

    public final void aG(String str) {
        this.pb = new ArrayList<>();
        this.pi = k.getActivity().getFilesDir().getAbsolutePath() + File.separator;
        this.pj = this.pi + fileName;
        this.pg = new com.a.a.j.b(str);
        String aI = this.pg.aI("DOWNLOAD");
        if (aI != null) {
            String[] split = aI.split("\\;");
            this.pa = new a[split.length];
            for (int i = 0; i < split.length; i++) {
                this.pa[i] = new a();
                this.pa[i].pk = split[i];
                getName();
                "app[" + i + "] target is " + this.pa[i].pk;
            }
        } else {
            Log.e(getName(), "No app target url !!!!!!!");
        }
        String aI2 = this.pg.aI("PACKAGE");
        if (aI2 != null) {
            String[] split2 = aI2.split("\\;");
            for (int i2 = 0; i2 < split2.length; i2++) {
                this.pa[i2].packageName = split2[i2];
                this.pa[i2].pl = j.D(0).getSharedPreferences().getBoolean(this.pa[i2].packageName, false);
                getName();
                "app[" + i2 + "] packageName is " + this.pa[i2].packageName + " and install state is " + this.pa[i2].pl;
            }
        }
        String aI3 = this.pg.aI("IMAGE");
        if (aI3 != null) {
            String[] split3 = aI3.split("\\;");
            for (int i3 = 0; i3 < split3.length; i3++) {
                this.pa[i3].pm = split3[i3];
                getName();
                "app[" + i3 + "] image is " + this.pa[i3].pm;
            }
        }
        String aI4 = this.pg.aI("THUMB");
        if (aI4 != null) {
            String[] split4 = aI4.split("\\;");
            for (int i4 = 0; i4 < split4.length; i4++) {
                this.pa[i4].pn = split4[i4];
                getName();
                "app[" + i4 + "] thumb is " + this.pa[i4].pn;
            }
        }
        String aI5 = this.pg.aI("FORCELAUNCH");
        if (aI5 != null) {
            this.pd = Boolean.parseBoolean(aI5);
        }
        String aI6 = this.pg.aI("SKIPCHECK");
        if (aI6 != null) {
            this.pf = Boolean.parseBoolean(aI6);
        }
        for (int i5 = 0; i5 < this.pa.length; i5++) {
            if (!this.pa[i5].pl) {
                this.pb.add(this.pa[i5]);
            }
        }
        h.a(this);
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(com.umeng.common.a.c);
        k.getActivity().registerReceiver(this, intentFilter);
    }

    public final List<a> dD() {
        if (this.pf) {
            getName();
        } else {
            for (int i = 0; i < this.pa.length; i++) {
                if (k.aC(this.pa[i].packageName)) {
                    this.pb.remove(this.pa[i]);
                }
            }
            getName();
            "Available apps are " + this.pb.size();
        }
        return this.pb;
    }

    public final void onDestroy() {
        k.getActivity().unregisterReceiver(this);
    }

    public void onReceive(Context context, Intent intent) {
        getName();
        "The package has installed." + intent.getDataString();
        if (this.pc != null && this.pc.packageName != null && intent.getDataString().indexOf(this.pc.packageName) != -1) {
            this.pe = 2;
            int i = this.pe;
            j.D(0).getEditor().putBoolean(this.pc.packageName, true).commit();
            this.pc.pl = true;
            if (dD().isEmpty()) {
                h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_NO_MORE, this));
            }
            h.b(h.a(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
            h.b(h.a(k.MSG_SYSTEM_LOG_EVENT, new String[]{"InstallAppSuccess", this.pc.packageName + "=" + k.cZ()}));
            if (this.pd) {
                k.aD(this.pc.packageName);
            }
        }
    }
}
