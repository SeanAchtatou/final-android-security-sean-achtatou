package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import com.androidbox.hywxfktj4.R;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimerTask;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.midlet.MIDlet;
import javax.wireless.messaging.MessagePart;
import mm.purchasesdk.PurchaseCode;
import org.meteoroid.core.f;
import org.meteoroid.core.g;
import org.meteoroid.core.h;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;

public class MIDPDevice extends ScreenWidget implements com.a.a.i.a, f.a, f.e, h.a {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_MIDLET_NOTIFYDESTROYED = 44036;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    private static final HashMap<String, Integer> nH = new HashMap<>(nz.length);
    private static final Properties nP = new Properties();
    public static String nZ = "";
    public static final String[] nz = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    public UUID fR;
    private int height = -1;
    private boolean nA;
    private boolean nB;
    private boolean nC;
    private boolean nD;
    private boolean nE;
    private boolean nF;
    private boolean nG;
    private Bitmap nI;
    private Bitmap nJ;
    private Bitmap nK;
    private Canvas nL;
    private e nM;
    private e nN;
    private e nO;
    /* access modifiers changed from: private */
    public volatile int nQ = -1;
    /* access modifiers changed from: private */
    public volatile int nR = -1;
    private boolean nS = false;
    private int nT;
    private int nU;
    private int nV;
    private int nW;
    private int nX;
    public g nY;
    public h oa;
    public boolean ob = false;
    public String oc = "";
    private int width = -1;

    private final class a extends TimerTask {
        private a() {
        }

        /* synthetic */ a(MIDPDevice mIDPDevice, byte b) {
            this();
        }

        public final void run() {
            if (MIDPDevice.this.nQ == -1 && MIDPDevice.this.nR != -1) {
                MIDPDevice.this.p(1, MIDPDevice.this.F(MIDPDevice.this.nR));
                int unused = MIDPDevice.this.nR = -1;
            }
        }
    }

    public static final class b implements com.a.a.c.a {
        File file;
        DataInputStream og;
        DataOutputStream oh;
        InputStream oi;
        OutputStream oj;
        String url;

        public b(String str) {
            this.url = str;
            String substring = str.substring(7);
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.file = new File(Environment.getExternalStorageDirectory() + substring);
                return;
            }
            throw new IOException("SD card not ready.");
        }

        public final void close() {
            if (this.oh != null) {
                this.oh.close();
                this.oh = null;
            }
            if (this.og != null) {
                this.og.close();
                this.og = null;
            }
            if (this.oj != null) {
                this.oj.close();
                this.oj = null;
            }
            if (this.oi != null) {
                this.oi.close();
                this.oi = null;
            }
        }
    }

    public static final class c {
        public final Paint gm = new Paint(1);
        public Paint.FontMetricsInt ok;
        private final char[] ol = new char[1];

        public c(Typeface typeface, int i, boolean z) {
            this.gm.setTypeface(typeface);
            this.gm.setTextSize((float) i);
            this.gm.setUnderlineText(z);
            this.ok = this.gm.getFontMetricsInt();
        }

        public final int d(char c) {
            this.ol[0] = c;
            return (int) this.gm.measureText(this.ol, 0, 1);
        }
    }

    public static final class d {
        public static int SIZE_LARGE = 16;
        public static int SIZE_MEDIUM = 14;
        public static int SIZE_SMALL = 12;
        private static final HashMap<com.a.a.d.g, c> om = new HashMap<>();

        public static int a(com.a.a.d.g gVar, char c) {
            return b(gVar).d(c);
        }

        public static int a(com.a.a.d.g gVar, String str) {
            return (int) b(gVar).gm.measureText(str);
        }

        public static c b(com.a.a.d.g gVar) {
            boolean z = true;
            int i = 0;
            c cVar = om.get(gVar);
            if (cVar != null) {
                return cVar;
            }
            Typeface typeface = Typeface.SANS_SERIF;
            if (gVar.aO() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (gVar.aO() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (gVar.aO() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((gVar.getStyle() & 0) != 0) {
            }
            int i2 = (gVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((gVar.getStyle() & 2) != 0) {
                i2 |= 2;
            }
            if ((gVar.getStyle() & 4) == 0) {
                z = false;
            }
            if (gVar.getSize() == 8) {
                i = SIZE_SMALL;
            } else if (gVar.getSize() == 0) {
                i = SIZE_MEDIUM;
            } else if (gVar.getSize() == 16) {
                i = SIZE_LARGE;
            }
            c cVar2 = new c(Typeface.create(typeface, i2), i, z);
            om.put(gVar, cVar2);
            return cVar2;
        }

        public static int c(com.a.a.d.g gVar) {
            c b = b(gVar);
            return b.gm.getFontMetricsInt(b.ok);
        }
    }

    public static class e extends com.a.a.d.h {
        private static final DashPathEffect oq = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
        private Paint on;
        private Paint oo;
        private c op;
        private Canvas or;
        private com.a.a.d.g os;
        private int ot;
        private Matrix ou;
        private Bitmap ov;
        private int ow;
        private int ox;
        private int oy;
        private int oz;

        private e() {
            this.on = new Paint();
            this.oo = new Paint();
            this.ot = 0;
            this.ou = new Matrix();
            this.ow = 0;
            this.ox = 0;
            this.oy = 0;
            this.oz = 0;
            this.on.setAntiAlias(true);
            this.on.setStyle(Paint.Style.STROKE);
            this.oo.setAntiAlias(true);
            this.oo.setStyle(Paint.Style.FILL);
        }

        public e(Bitmap bitmap) {
            this();
            this.ov = bitmap;
            this.or = new Canvas(bitmap);
            a(this.or);
        }

        public e(Canvas canvas) {
            this();
            a(canvas);
        }

        private void a(Canvas canvas) {
            this.or = canvas;
            a(com.a.a.d.g.aN());
            translate(-aR(), -aS());
            setColor(0);
            if (this.ov != null) {
                e(0, 0, this.ov.getWidth(), this.ov.getHeight());
            } else {
                e(0, 0, org.meteoroid.core.c.mf.getWidth(), org.meteoroid.core.c.mf.getHeight());
            }
        }

        public final void a(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.oy > 0 && this.oz > 0 && i3 > 0 && i4 > 0) {
                this.or.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), 0.0f, (float) i6, true, this.oo);
            }
        }

        public final void a(com.a.a.d.g gVar) {
            if (gVar == null) {
                gVar = com.a.a.d.g.aN();
            }
            this.os = gVar;
            this.op = d.b(gVar);
        }

        public final void a(com.a.a.d.i iVar, int i, int i2, int i3) {
            if (iVar != null && this.oy > 0 && this.oz > 0 && iVar.gO != null) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 8) != 0) {
                    i -= iVar.width;
                } else if ((i3 & 1) != 0) {
                    i -= iVar.width / 2;
                }
                if ((i3 & 32) != 0) {
                    i2 -= iVar.height;
                } else if ((i3 & 2) != 0) {
                    i2 -= iVar.height / 2;
                }
                this.or.drawBitmap(iVar.gO, (float) i, (float) i2, (Paint) null);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:40:0x0090, code lost:
            if ((r15 & 34) != 0) goto L_0x0092;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0099, code lost:
            if ((r15 & 9) != 0) goto L_0x009b;
         */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x009e  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0131  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x0147  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void a(com.a.a.d.i r7, int r8, int r9, int r10, int r11, int r12, int r13, int r14, int r15) {
            /*
                r6 = this;
                r1 = 1
                r4 = 1065353216(0x3f800000, float:1.0)
                r3 = -1082130432(0xffffffffbf800000, float:-1.0)
                if (r7 == 0) goto L_0x0013
                android.graphics.Bitmap r0 = r7.gO
                if (r0 == 0) goto L_0x0013
                int r0 = r6.oy
                if (r0 <= 0) goto L_0x0013
                int r0 = r6.oz
                if (r0 > 0) goto L_0x0014
            L_0x0013:
                return
            L_0x0014:
                if (r8 < 0) goto L_0x0018
                if (r9 >= 0) goto L_0x0037
            L_0x0018:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                java.lang.String r2 = "Area out of Image: x"
                r1.<init>(r2)
                java.lang.StringBuilder r1 = r1.append(r8)
                java.lang.String r2 = " y:"
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.StringBuilder r1 = r1.append(r9)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0037:
                boolean r0 = r7.isMutable()
                if (r0 == 0) goto L_0x004b
                com.a.a.d.h r0 = r7.aU()
                if (r0 != r6) goto L_0x004b
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r1 = "Image is source and target"
                r0.<init>(r1)
                throw r0
            L_0x004b:
                int r0 = r8 + r10
                int r2 = r7.getWidth()
                if (r0 <= r2) goto L_0x0059
                int r0 = r7.getWidth()
                int r10 = r0 - r8
            L_0x0059:
                int r0 = r9 + r11
                int r2 = r7.getHeight()
                if (r0 <= r2) goto L_0x0171
                int r0 = r7.getHeight()
                int r0 = r0 - r9
            L_0x0066:
                if (r10 <= 0) goto L_0x0013
                if (r0 <= 0) goto L_0x0013
                android.graphics.Matrix r2 = r6.ou
                r2.reset()
                switch(r12) {
                    case 0: goto L_0x007a;
                    case 1: goto L_0x00f3;
                    case 2: goto L_0x00d2;
                    case 3: goto L_0x00b4;
                    case 4: goto L_0x0108;
                    case 5: goto L_0x00a6;
                    case 6: goto L_0x00c4;
                    case 7: goto L_0x00df;
                    default: goto L_0x0072;
                }
            L_0x0072:
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r1 = "Bad transform"
                r0.<init>(r1)
                throw r0
            L_0x007a:
                int r2 = -r8
                int r3 = -r9
                r4 = r3
                r3 = r2
                r2 = r10
                r10 = r0
            L_0x0080:
                r0 = 0
                if (r15 != 0) goto L_0x0085
                r15 = 20
            L_0x0085:
                r5 = r15 & 64
                if (r5 == 0) goto L_0x008a
                r0 = r1
            L_0x008a:
                r5 = r15 & 16
                if (r5 == 0) goto L_0x011b
                r5 = r15 & 34
                if (r5 == 0) goto L_0x0093
            L_0x0092:
                r0 = r1
            L_0x0093:
                r5 = r15 & 4
                if (r5 == 0) goto L_0x0131
                r5 = r15 & 9
                if (r5 == 0) goto L_0x009c
            L_0x009b:
                r0 = r1
            L_0x009c:
                if (r0 == 0) goto L_0x0147
                java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
                java.lang.String r1 = "Bad Anchor"
                r0.<init>(r1)
                throw r0
            L_0x00a6:
                android.graphics.Matrix r2 = r6.ou
                r3 = 1119092736(0x42b40000, float:90.0)
                r2.preRotate(r3)
                int r2 = r0 + r9
                int r3 = -r8
                r4 = r3
                r3 = r2
                r2 = r0
                goto L_0x0080
            L_0x00b4:
                android.graphics.Matrix r2 = r6.ou
                r3 = 1127481344(0x43340000, float:180.0)
                r2.preRotate(r3)
                int r2 = r10 + r8
                int r3 = r0 + r9
                r4 = r3
                r3 = r2
                r2 = r10
                r10 = r0
                goto L_0x0080
            L_0x00c4:
                android.graphics.Matrix r2 = r6.ou
                r3 = 1132920832(0x43870000, float:270.0)
                r2.preRotate(r3)
                int r2 = -r9
                int r3 = r10 + r8
                r4 = r3
                r3 = r2
                r2 = r0
                goto L_0x0080
            L_0x00d2:
                android.graphics.Matrix r2 = r6.ou
                r2.preScale(r3, r4)
                int r2 = r10 + r8
                int r3 = -r9
                r4 = r3
                r3 = r2
                r2 = r10
                r10 = r0
                goto L_0x0080
            L_0x00df:
                android.graphics.Matrix r2 = r6.ou
                r2.preScale(r3, r4)
                android.graphics.Matrix r2 = r6.ou
                r3 = -1028390912(0xffffffffc2b40000, float:-90.0)
                r2.preRotate(r3)
                int r2 = r0 + r9
                int r3 = r10 + r8
                r4 = r3
                r3 = r2
                r2 = r0
                goto L_0x0080
            L_0x00f3:
                android.graphics.Matrix r2 = r6.ou
                r2.preScale(r3, r4)
                android.graphics.Matrix r2 = r6.ou
                r3 = -1020002304(0xffffffffc3340000, float:-180.0)
                r2.preRotate(r3)
                int r2 = -r8
                int r3 = r0 + r9
                r4 = r3
                r3 = r2
                r2 = r10
                r10 = r0
                goto L_0x0080
            L_0x0108:
                android.graphics.Matrix r2 = r6.ou
                r2.preScale(r3, r4)
                android.graphics.Matrix r2 = r6.ou
                r3 = -1014562816(0xffffffffc3870000, float:-270.0)
                r2.preRotate(r3)
                int r2 = -r9
                int r3 = -r8
                r4 = r3
                r3 = r2
                r2 = r0
                goto L_0x0080
            L_0x011b:
                r5 = r15 & 32
                if (r5 == 0) goto L_0x0126
                r5 = r15 & 2
                if (r5 != 0) goto L_0x0092
                int r14 = r14 - r10
                goto L_0x0093
            L_0x0126:
                r5 = r15 & 2
                if (r5 == 0) goto L_0x0092
                int r5 = r10 + -1
                int r5 = r5 >>> 1
                int r14 = r14 - r5
                goto L_0x0093
            L_0x0131:
                r5 = r15 & 8
                if (r5 == 0) goto L_0x013c
                r5 = r15 & 1
                if (r5 != 0) goto L_0x009b
                int r13 = r13 - r2
                goto L_0x009c
            L_0x013c:
                r5 = r15 & 1
                if (r5 == 0) goto L_0x009b
                int r1 = r2 + -1
                int r1 = r1 >>> 1
                int r13 = r13 - r1
                goto L_0x009c
            L_0x0147:
                android.graphics.Canvas r0 = r6.or
                r0.save()
                android.graphics.Canvas r0 = r6.or
                int r1 = r13 + r2
                int r2 = r14 + r10
                r0.clipRect(r13, r14, r1, r2)
                android.graphics.Canvas r0 = r6.or
                int r1 = r13 + r3
                float r1 = (float) r1
                int r2 = r14 + r4
                float r2 = (float) r2
                r0.translate(r1, r2)
                android.graphics.Canvas r0 = r6.or
                android.graphics.Bitmap r1 = r7.gO
                android.graphics.Matrix r2 = r6.ou
                r3 = 0
                r0.drawBitmap(r1, r2, r3)
                android.graphics.Canvas r0 = r6.or
                r0.restore()
                goto L_0x0013
            L_0x0171:
                r0 = r11
                goto L_0x0066
            */
            throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.e.a(com.a.a.d.i, int, int, int, int, int, int, int, int):void");
        }

        public final void a(String str, int i, int i2, int i3) {
            if (this.oy > 0 && this.oz > 0) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 16) != 0) {
                    i2 -= this.op.ok.top;
                } else if ((i3 & 32) != 0) {
                    i2 -= this.op.ok.bottom;
                } else if ((i3 & 2) != 0) {
                    i2 += ((this.op.ok.descent - this.op.ok.ascent) / 2) - this.op.ok.descent;
                }
                if ((i3 & 1) != 0) {
                    this.op.gm.setTextAlign(Paint.Align.CENTER);
                } else if ((i3 & 8) != 0) {
                    this.op.gm.setTextAlign(Paint.Align.RIGHT);
                } else if ((i3 & 4) != 0) {
                    this.op.gm.setTextAlign(Paint.Align.LEFT);
                }
                this.op.gm.setColor(this.on.getColor());
                this.or.drawText(str, (float) i, (float) i2, this.op.gm);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void}
         arg types: [java.lang.String, int, int, int, float, android.graphics.Paint]
         candidates:
          ClspMth{android.graphics.Canvas.drawText(java.lang.CharSequence, int, int, float, float, android.graphics.Paint):void}
          ClspMth{android.graphics.Canvas.drawText(char[], int, int, float, float, android.graphics.Paint):void}
          ClspMth{android.graphics.Canvas.drawText(java.lang.String, int, int, float, float, android.graphics.Paint):void} */
        public final void a(String str, int i, int i2, int i3, int i4, int i5) {
            if (this.oy > 0 && this.oz > 0) {
                int i6 = i4 - this.op.ok.top;
                this.op.gm.setTextAlign(Paint.Align.LEFT);
                this.op.gm.setColor(this.on.getColor());
                String str2 = str;
                this.or.drawText(str2, 0, i2 + 0, 25.0f, (float) i6, this.op.gm);
            }
        }

        public final int aP() {
            return this.oz;
        }

        public final int aQ() {
            return this.oy;
        }

        public final void aT() {
            a(this.or);
        }

        public final void b(int i, int i2, int i3, int i4) {
            if (this.oy > 0 && this.oz > 0) {
                if (i > i3) {
                    i++;
                } else {
                    i3++;
                }
                if (i2 > i4) {
                    i2++;
                } else {
                    i4++;
                }
                this.or.drawLine((float) i, (float) i2, (float) i3, (float) i4, this.on);
            }
        }

        public final void b(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.oy > 0 && this.oz > 0) {
                Path path = new Path();
                path.moveTo((float) i, (float) i2);
                path.lineTo((float) i3, (float) i4);
                path.lineTo((float) i5, (float) i6);
                path.lineTo((float) i, (float) i2);
                this.or.drawPath(path, this.oo);
            }
        }

        public final void c(int i, int i2, int i3, int i4) {
            if (this.oy > 0 && this.oz > 0 && i3 > 0 && i4 > 0) {
                this.or.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.on);
            }
        }

        public final void d(int i, int i2, int i3, int i4) {
            if (this.oy > 0 && this.oz > 0 && i3 > 0 && i4 > 0) {
                this.or.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.oo);
            }
        }

        public final void e(int i, int i2, int i3, int i4) {
            this.ow = i;
            this.ox = i2;
            this.oy = i3;
            this.oz = i4;
            if (i3 > 0 && i4 > 0) {
                this.or.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
            }
        }

        public final void setColor(int i) {
            this.on.setColor(-16777216 | i);
            this.oo.setColor(-16777216 | i);
            super.setColor(i);
        }

        public final void translate(int i, int i2) {
            if (i != 0 || i2 != 0) {
                super.translate(i, i2);
                this.or.translate((float) i, (float) i2);
                e(this.ow - i, this.ox - i2, this.oy, this.oz);
            }
        }
    }

    public static final class f implements com.a.a.b.e {
        private HttpURLConnection oA;
        private URL url;

        public f(String str, int i, boolean z) {
            try {
                if (!cW()) {
                    throw new IOException("No avaliable connection.");
                }
                this.url = new URL(str);
                this.oA = (HttpURLConnection) this.url.openConnection();
                this.oA.setDoInput(true);
                if (i != 1) {
                    this.oA.setDoOutput(true);
                }
                if (z) {
                    this.oA.setConnectTimeout(10000);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

        private static boolean cW() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager dc = org.meteoroid.core.k.dc();
                return dc != null && (activeNetworkInfo = dc.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                e.toString();
            }
        }

        public final void close() {
            "Connection " + this.url.toString() + " disconnected.";
            this.oA.disconnect();
        }
    }

    public static final class g implements com.a.a.a.d {
        public BluetoothDevice gh = null;
        BluetoothAdapter oB;
        BluetoothSocket oC;
        com.a.a.a.f oD;

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0077 A[LOOP:1: B:14:0x0077->B:31:0x00fb, LOOP_START, PHI: r2 r3 
          PHI: (r2v5 android.bluetooth.BluetoothSocket) = (r2v4 android.bluetooth.BluetoothSocket), (r2v7 android.bluetooth.BluetoothSocket) binds: [B:13:0x0075, B:31:0x00fb] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r3v3 int) = (r3v2 int), (r3v4 int) binds: [B:13:0x0075, B:31:0x00fb] A[DONT_GENERATE, DONT_INLINE]] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00e6  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public g(java.lang.String r9) {
            /*
                r8 = this;
                r1 = 0
                r4 = 0
                r8.<init>()
                r8.gh = r1
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                java.lang.String r2 = "open StreamConnection   name = "
                r0.<init>(r2)
                java.lang.StringBuilder r0 = r0.append(r9)
                r0.toString()
                android.bluetooth.BluetoothAdapter r0 = android.bluetooth.BluetoothAdapter.getDefaultAdapter()
                r8.oB = r0
                java.lang.String r0 = "MACadd"
                int r0 = r9.lastIndexOf(r0)
                int r2 = r0 + 6
                java.lang.String r0 = ""
                r3 = r0
                r0 = r2
            L_0x0027:
                int r5 = r2 + 17
                if (r0 >= r5) goto L_0x0043
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r3 = r5.append(r3)
                char r5 = r9.charAt(r0)
                java.lang.StringBuilder r3 = r3.append(r5)
                java.lang.String r3 = r3.toString()
                int r0 = r0 + 1
                goto L_0x0027
            L_0x0043:
                com.a.a.i.a r0 = org.meteoroid.core.c.mf
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                com.a.a.a.b r2 = com.a.a.a.b.fQ
                java.util.UUID r2 = r2.fR
                r0.fR = r2
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011e }
                java.lang.String r2 = "ADD = "
                r0.<init>(r2)     // Catch:{ Exception -> 0x011e }
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x011e }
                r0.toString()     // Catch:{ Exception -> 0x011e }
                android.bluetooth.BluetoothAdapter r0 = r8.oB     // Catch:{ Exception -> 0x011e }
                android.bluetooth.BluetoothDevice r2 = r0.getRemoteDevice(r3)     // Catch:{ Exception -> 0x011e }
                com.a.a.i.a r0 = org.meteoroid.core.c.mf     // Catch:{ Exception -> 0x011e }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ Exception -> 0x011e }
                java.util.UUID r0 = r0.fR     // Catch:{ Exception -> 0x011e }
                android.bluetooth.BluetoothSocket r1 = r2.createRfcommSocketToServiceRecord(r0)     // Catch:{ Exception -> 0x011e }
                if (r1 != 0) goto L_0x00f6
                java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ Exception -> 0x0073 }
                r0.<init>()     // Catch:{ Exception -> 0x0073 }
                throw r0     // Catch:{ Exception -> 0x0073 }
            L_0x0073:
                r0 = move-exception
                r0 = r1
            L_0x0075:
                r2 = r0
                r3 = r4
            L_0x0077:
                com.a.a.a.b r0 = com.a.a.a.b.fQ
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fZ
                int r0 = r0.size()
                if (r3 >= r0) goto L_0x0124
                com.a.a.i.a r0 = org.meteoroid.core.c.mf     // Catch:{ IOException -> 0x0101 }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ IOException -> 0x0101 }
                java.util.UUID r0 = r0.fR     // Catch:{ IOException -> 0x0101 }
                if (r0 == 0) goto L_0x0122
                com.a.a.a.b r0 = com.a.a.a.b.fQ     // Catch:{ IOException -> 0x0101 }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fZ     // Catch:{ IOException -> 0x0101 }
                java.lang.Object r0 = r0.elementAt(r3)     // Catch:{ IOException -> 0x0101 }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x0101 }
                r0.getName()     // Catch:{ IOException -> 0x0101 }
                com.a.a.a.b r0 = com.a.a.a.b.fQ     // Catch:{ IOException -> 0x0101 }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fZ     // Catch:{ IOException -> 0x0101 }
                java.lang.Object r0 = r0.elementAt(r3)     // Catch:{ IOException -> 0x0101 }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x0101 }
                r0.getAddress()     // Catch:{ IOException -> 0x0101 }
                com.a.a.a.b r0 = com.a.a.a.b.fQ     // Catch:{ IOException -> 0x0101 }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fZ     // Catch:{ IOException -> 0x0101 }
                java.lang.Object r0 = r0.elementAt(r3)     // Catch:{ IOException -> 0x0101 }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x0101 }
                com.a.a.i.a r1 = org.meteoroid.core.c.mf     // Catch:{ IOException -> 0x0101 }
                org.meteoroid.plugin.device.MIDPDevice r1 = (org.meteoroid.plugin.device.MIDPDevice) r1     // Catch:{ IOException -> 0x0101 }
                java.util.UUID r1 = r1.fR     // Catch:{ IOException -> 0x0101 }
                android.bluetooth.BluetoothSocket r1 = r0.createRfcommSocketToServiceRecord(r1)     // Catch:{ IOException -> 0x0101 }
            L_0x00b7:
                if (r1 == 0) goto L_0x00fa
                com.a.a.a.b r0 = com.a.a.a.b.fQ     // Catch:{ IOException -> 0x0119 }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fZ     // Catch:{ IOException -> 0x0119 }
                java.lang.Object r0 = r0.elementAt(r3)     // Catch:{ IOException -> 0x0119 }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x0119 }
                r8.gh = r0     // Catch:{ IOException -> 0x0119 }
                r1.connect()     // Catch:{ IOException -> 0x0119 }
            L_0x00c8:
                r8.oC = r1
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                java.lang.String r1 = "远程设备描述内容 = "
                r0.<init>(r1)
                android.bluetooth.BluetoothSocket r1 = r8.oC
                android.bluetooth.BluetoothDevice r1 = r1.getRemoteDevice()
                int r1 = r1.describeContents()
                java.lang.StringBuilder r0 = r0.append(r1)
                r0.toString()
                com.a.a.a.f r0 = r8.oD
                if (r0 != 0) goto L_0x00ef
                com.a.a.a.f r0 = new com.a.a.a.f
                android.bluetooth.BluetoothSocket r1 = r8.oC
                r0.<init>(r1, r4)
                r8.oD = r0
            L_0x00ef:
                com.a.a.i.a r0 = org.meteoroid.core.c.mf
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                r0.nY = r8
                return
            L_0x00f6:
                r1.connect()     // Catch:{ Exception -> 0x0073 }
                goto L_0x00c8
            L_0x00fa:
                r0 = r1
            L_0x00fb:
                int r1 = r3 + 1
                r2 = r0
                r3 = r1
                goto L_0x0077
            L_0x0101:
                r0 = move-exception
                r1 = r0
                r0 = r2
            L_0x0104:
                java.lang.String r2 = "MIDPDevice"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                java.lang.String r6 = "error = "
                r5.<init>(r6)
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                android.util.Log.e(r2, r1)
                goto L_0x00fb
            L_0x0119:
                r0 = move-exception
                r7 = r0
                r0 = r1
                r1 = r7
                goto L_0x0104
            L_0x011e:
                r0 = move-exception
                r0 = r1
                goto L_0x0075
            L_0x0122:
                r1 = r2
                goto L_0x00b7
            L_0x0124:
                r1 = r2
                goto L_0x00c8
            */
            throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.g.<init>(java.lang.String):void");
        }

        public final void close() {
        }
    }

    public static final class h implements com.a.a.a.e {
        private static final String LOG_TAG = "L2CapConnectionNotifier";
        public static final int STATE_CONNECTED = 3;
        public static final int STATE_CONNECTING = 2;
        public static final int STATE_LISTEN = 1;
        public static final int STATE_NONE = 0;
        public static int oG;
        public final BluetoothServerSocket oE;
        BluetoothAdapter oF = BluetoothAdapter.getDefaultAdapter();
        public boolean oH = false;

        public h(String str) {
            BluetoothServerSocket bluetoothServerSocket;
            ((MIDPDevice) org.meteoroid.core.c.mf).oc = this.oF.getName();
            int indexOf = str.indexOf(";name=");
            if (indexOf != -1) {
                int i = indexOf + 6;
                String substring = str.substring(i, str.length());
                int indexOf2 = substring.indexOf(59);
                "nameindex2 = " + indexOf2;
                if (indexOf2 != -1) {
                    MIDPDevice.nZ = substring.substring(0, indexOf2);
                } else {
                    MIDPDevice.nZ = str.substring(i, str.length());
                }
            }
            "ServerName = " + MIDPDevice.nZ;
            try {
                ((MIDPDevice) org.meteoroid.core.c.mf).fR = UUID.fromString("11111111-2222-3333-4444-555555555555");
                if (MIDPDevice.nZ != "") {
                    this.oF.setName(MIDPDevice.nZ);
                }
                ((MIDPDevice) org.meteoroid.core.c.mf).ob = true;
                bluetoothServerSocket = this.oF.listenUsingRfcommWithServiceRecord("MIDPBlueTooth", ((MIDPDevice) org.meteoroid.core.c.mf).fR);
            } catch (IOException e) {
                Log.e(LOG_TAG, "listen() failed", e);
                bluetoothServerSocket = null;
            }
            this.oE = bluetoothServerSocket;
            oG = 1;
            ((MIDPDevice) org.meteoroid.core.c.mf).oa = this;
        }

        public final void close() {
        }
    }

    public static final class i implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, Player {
        private static final String LOG_TAG = "MIDP player";
        private g.a oI;
        private HashSet<PlayerListener> oJ;
        private int oK = 1;
        private int oL;
        private boolean oM;
        private m oN;
        private int state = 100;

        public i(g.a aVar) {
            this.oI = aVar;
            aVar.mL = org.meteoroid.core.g.cN();
            aVar.mM.setOnCompletionListener(this);
            aVar.mM.setOnPreparedListener(this);
            this.oJ = new HashSet<>();
        }

        private final void a(String str, Object obj) {
            Iterator<PlayerListener> it = this.oJ.iterator();
            while (it.hasNext()) {
                PlayerListener next = it.next();
                if (next != null) {
                    next.aW();
                }
            }
        }

        public final Control L(String str) {
            if (str.indexOf("VolumeControl") == -1) {
                return null;
            }
            if (this.oN == null) {
                this.oN = new m(this.oI);
            }
            return this.oN;
        }

        public final long a(long j) {
            long j2 = 0;
            "setMediaTime " + 0L + ".";
            int duration = this.oI.mM.getDuration();
            if (((long) duration) < 0) {
                j2 = (long) duration;
            }
            try {
                this.oI.mM.seekTo((int) j2);
                return j2;
            } catch (IllegalStateException e) {
                throw new MediaException();
            }
        }

        public final void aV() {
            try {
                if (this.state == 100) {
                    this.oI.mM.reset();
                    this.oI.mM.setDataSource(this.oI.mJ);
                    this.state = 200;
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                throw new MediaException();
            }
        }

        public final void close() {
            this.state = 100;
            this.state = 0;
            this.oI.recycle();
            if (this.oI.mJ != null) {
                File file = new File(this.oI.mJ);
                if (file.exists()) {
                    file.delete();
                }
            }
            org.meteoroid.core.g.a(this.oI);
            a(PlayerListener.CLOSED, null);
        }

        public final int getState() {
            return this.state;
        }

        public final void onCompletion(MediaPlayer mediaPlayer) {
            "playedCount:" + this.oL + " loopCount:" + this.oK;
            if (mediaPlayer == this.oI.mM) {
                this.oL++;
                if (this.oL < this.oK) {
                    try {
                        this.state = 100;
                        start();
                    } catch (MediaException e) {
                        e.printStackTrace();
                    }
                } else if (this.oJ.isEmpty()) {
                    try {
                        stop();
                    } catch (MediaException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    a(PlayerListener.END_OF_MEDIA, null);
                }
            }
        }

        public final void onPrepared(MediaPlayer mediaPlayer) {
            "onPrepared:" + this.oL + " loopCount:" + this.oK;
        }

        public final void r(int i) {
            "setLoopCount " + i + ".";
            this.oK = i;
        }

        public final void start() {
            aV();
            aV();
            if (this.state == 200) {
                try {
                    this.oI.mM.prepare();
                    this.state = 300;
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                    throw new MediaException();
                }
            }
            if (this.state == 300) {
                try {
                    if (this.oK == -1) {
                        this.oI.mM.setLooping(true);
                    } else {
                        this.oI.mM.setLooping(false);
                    }
                    if (this.oM) {
                        this.oI.mM.prepare();
                        this.oM = false;
                    }
                    this.oI.mM.start();
                    this.oI.mK = true;
                    this.state = 400;
                    a(PlayerListener.STARTED, null);
                } catch (Exception e2) {
                    a(PlayerListener.ERROR, e2.getMessage());
                    throw new MediaException();
                }
            }
        }

        public final void stop() {
            if (this.state == 400) {
                try {
                    if (this.oI.mM.isPlaying()) {
                        this.oI.mM.stop();
                    }
                    this.oI.mK = false;
                    this.oL = 0;
                    this.state = 300;
                    this.oM = true;
                    a(PlayerListener.STOPPED, null);
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                    e.printStackTrace();
                    throw new MediaException();
                } catch (Throwable th) {
                    this.state = 300;
                    this.oM = true;
                    a(PlayerListener.STOPPED, null);
                    throw th;
                }
            }
        }
    }

    public static final class j implements com.a.a.g.c, h.a {
        public static final int MSG_GCF_SMS_CONNECTION_RECEIVE = 15391747;
        public static final int MSG_GCF_SMS_CONNECTION_SEND = 15391744;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_COMPLETE = 15391745;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_FAIL = 15391746;
        private static final int NOT_SENT = 0;
        private static final int SENT_FAIL = 2;
        private static final int SENT_OK = 1;
        private String oO;
        private int oP = 0;
        private com.a.a.g.d oQ;
        private com.a.a.g.b oR;

        public static final class a implements com.a.a.g.a {
            private String oS;

            public final String getAddress() {
                return this.oS;
            }

            public final void setAddress(String str) {
                this.oS = str;
            }
        }

        public static final class b implements com.a.a.g.f {
            private String oO;
            private String text;

            public final void O(String str) {
                this.text = str;
            }

            public final String bd() {
                return this.text;
            }

            public final String getAddress() {
                return this.oO;
            }

            public final void setAddress(String str) {
                this.oO = str;
            }
        }

        public j(String str) {
            org.meteoroid.core.h.b((int) MSG_GCF_SMS_CONNECTION_SEND, "MSG_GCF_SMS_CONNECTION_SEND");
            org.meteoroid.core.h.b((int) MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, "MSG_GCF_SMS_CONNECTION_SEND_COMPLETE");
            org.meteoroid.core.h.b((int) MSG_GCF_SMS_CONNECTION_SEND_FAIL, "MSG_GCF_SMS_CONNECTION_SEND_FAIL");
            org.meteoroid.core.h.b((int) MSG_GCF_SMS_CONNECTION_RECEIVE, "MSG_GCF_SMS_CONNECTION_RECEIVE");
            this.oO = str;
            org.meteoroid.core.h.a(this);
        }

        public final com.a.a.g.b N(String str) {
            com.a.a.g.b kVar;
            if (str.equals(com.a.a.g.c.TEXT_MESSAGE)) {
                kVar = new b();
            } else if (str.equals(com.a.a.g.c.BINARY_MESSAGE)) {
                kVar = new a();
            } else if (str.equals(com.a.a.g.c.MULTIPART_MESSAGE)) {
                kVar = new k();
            } else {
                throw new IllegalArgumentException(str);
            }
            if (this.oO != null) {
                kVar.setAddress(this.oO);
            }
            return kVar;
        }

        public final void a(com.a.a.g.b bVar) {
            org.meteoroid.core.h.a(this);
            org.meteoroid.core.h.b(org.meteoroid.core.h.a(MSG_GCF_SMS_CONNECTION_SEND, bVar));
            synchronized (this) {
                try {
                    this.oP = 0;
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            org.meteoroid.core.h.b(this);
            if (this.oP != 1) {
                throw new InterruptedIOException("Fail to send.");
            }
            return;
        }

        public final boolean a(Message message) {
            if (message.what == 15391745) {
                this.oP = 1;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what == 15391746) {
                this.oP = 2;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what != 15391747) {
                return false;
            } else {
                this.oR = (com.a.a.g.b) message.obj;
                if (this.oQ != null) {
                    com.a.a.g.d dVar = this.oQ;
                }
                synchronized (this) {
                    notifyAll();
                }
                return true;
            }
        }

        public final void close() {
            org.meteoroid.core.h.b(this);
        }
    }

    public static final class k implements com.a.a.g.e {
        private ArrayList<MessagePart> oT = new ArrayList<>();
        private String oU;
        private ArrayList<String> oV = new ArrayList<>();
        private ArrayList<String> oW = new ArrayList<>();
        private ArrayList<String> oX = new ArrayList<>();
        private HashMap<String, String> oY = new HashMap<>();

        public final String getAddress() {
            return this.oU != null ? this.oU : this.oV.get(0);
        }

        public final void setAddress(String str) {
            this.oU = str;
        }
    }

    public static final class l implements com.a.a.b.h {
        private int mode;
        private Socket oZ;

        public l(String str, int i) {
            this.mode = i;
            try {
                if (!cW()) {
                    throw new IOException("No avaliable connection.");
                }
                URI uri = new URI(str);
                this.oZ = new Socket(uri.getHost(), uri.getPort());
                "Connection " + uri.toString() + " established.";
            } catch (Exception e) {
                throw new IOException("URISyntaxException");
            }
        }

        private static boolean cW() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager dc = org.meteoroid.core.k.dc();
                return dc != null && (activeNetworkInfo = dc.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                e.toString();
            }
        }

        public final void close() {
            this.oZ.close();
            "Connection " + this.oZ.getInetAddress().getHostAddress() + " close.";
            this.oZ = null;
            System.gc();
        }
    }

    public static final class m implements VolumeControl {
        private g.a oI;

        public m(g.a aVar) {
            this.oI = aVar;
        }

        public final int s(int i) {
            int i2 = 100;
            int i3 = i < 0 ? 0 : i;
            if (i3 <= 100) {
                i2 = i3;
            }
            this.oI.mL = i2;
            org.meteoroid.core.g.x(i2);
            "Volume is set to " + i2;
            return i2;
        }
    }

    /* access modifiers changed from: private */
    public final int F(int i2) {
        switch (i2) {
            case 4:
                if (org.meteoroid.core.k.dj() == null || !org.meteoroid.core.k.dj().startsWith("R800")) {
                    return 0;
                }
                return nH.get("NUM_5").intValue();
            case 7:
            case PurchaseCode.NOMOREREQUEST_ERR:
                return nH.get("NUM_0").intValue();
            case 8:
            case com.a.a.d.b.KEY_NUM3:
                return nH.get("NUM_1").intValue();
            case 9:
            case 33:
                return nH.get("NUM_2").intValue();
            case 10:
            case 46:
                return nH.get("NUM_3").intValue();
            case 11:
            case 47:
                return nH.get("NUM_4").intValue();
            case 12:
            case 32:
                return nH.get("NUM_5").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
            case 34:
                return nH.get("NUM_6").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
            case com.a.a.d.b.KEY_NUM6:
            case 99:
                return nH.get("NUM_7").intValue();
            case DefaultVirtualDevice.WIDGET_TYPE_DYNAMICJOYSTICK /*15*/:
            case com.a.a.d.b.KEY_NUM4:
                return nH.get("NUM_8").intValue();
            case 16:
            case 31:
            case 100:
                return nH.get("NUM_9").intValue();
            case 17:
            case 61:
            case PurchaseCode.ORDER_OK:
                return nH.get(nz[11]).intValue();
            case 18:
            case com.a.a.d.b.KEY_NUM7:
            case PurchaseCode.UNSUB_OK:
                return nH.get(nz[12]).intValue();
            case 19:
                return nH.get(nz[0]).intValue();
            case 20:
                return nH.get(nz[1]).intValue();
            case 21:
                return nH.get(nz[2]).intValue();
            case 22:
                return nH.get(nz[3]).intValue();
            case 23:
                return nH.get(nz[4]).intValue();
            case 44:
            case 86:
            case 88:
            case 93:
            case 108:
            case 186:
                return H(2);
            case 45:
            case 85:
            case 87:
            case 92:
            case 109:
            case 183:
                return H(1);
            case 66:
                return nH.get(nz[9]).intValue();
            default:
                return 0;
        }
    }

    public static int G(int i2) {
        if (nH.containsKey(nz[0]) && i2 == nH.get(nz[0]).intValue()) {
            return 1;
        }
        if (nH.containsKey(nz[1]) && i2 == nH.get(nz[1]).intValue()) {
            return 6;
        }
        if (nH.containsKey(nz[2]) && i2 == nH.get(nz[2]).intValue()) {
            return 2;
        }
        if (nH.containsKey(nz[3]) && i2 == nH.get(nz[3]).intValue()) {
            return 5;
        }
        if (nH.containsKey(nz[4]) && i2 == nH.get(nz[4]).intValue()) {
            return 8;
        }
        if (nH.containsKey(nz[5]) && i2 == nH.get(nz[5]).intValue()) {
            return 9;
        }
        if (nH.containsKey(nz[6]) && i2 == nH.get(nz[6]).intValue()) {
            return 10;
        }
        if (nH.containsKey(nz[7]) && i2 == nH.get(nz[7]).intValue()) {
            return 11;
        }
        if (nH.containsKey(nz[12]) && i2 == nH.get(nz[12]).intValue()) {
            return 35;
        }
        if (nH.containsKey(nz[11]) && i2 == nH.get(nz[11]).intValue()) {
            return 42;
        }
        if (nH.containsKey("NUM_0") && i2 == nH.get("NUM_0").intValue()) {
            return 48;
        }
        if (nH.containsKey("NUM_1") && i2 == nH.get("NUM_1").intValue()) {
            return 49;
        }
        if (nH.containsKey("NUM_2") && i2 == nH.get("NUM_2").intValue()) {
            return 50;
        }
        if (nH.containsKey("NUM_3") && i2 == nH.get("NUM_3").intValue()) {
            return 51;
        }
        if (nH.containsKey("NUM_4") && i2 == nH.get("NUM_4").intValue()) {
            return 52;
        }
        if (nH.containsKey("NUM_5") && i2 == nH.get("NUM_5").intValue()) {
            return 53;
        }
        if (nH.containsKey("NUM_6") && i2 == nH.get("NUM_6").intValue()) {
            return 54;
        }
        if (nH.containsKey("NUM_7") && i2 == nH.get("NUM_7").intValue()) {
            return 55;
        }
        if (!nH.containsKey("NUM_8") || i2 != nH.get("NUM_8").intValue()) {
            return (!nH.containsKey("NUM_9") || i2 != nH.get("NUM_9").intValue()) ? 0 : 57;
        }
        return 56;
    }

    public static int H(int i2) {
        if (i2 == 1) {
            return nH.get(nz[9]).intValue();
        }
        if (i2 == 2) {
            return nH.get(nz[10]).intValue();
        }
        return 0;
    }

    private final int I(int i2) {
        if (i2 == H(1)) {
            return 17;
        }
        if (i2 == H(2)) {
            return 18;
        }
        switch (G(i2)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case com.a.a.d.b.KEY_POUND:
                return 11;
            case com.a.a.d.b.KEY_STAR:
                return 10;
            case com.a.a.d.b.KEY_NUM0:
                return 0;
            case com.a.a.d.b.KEY_NUM1:
                return 1;
            case com.a.a.d.b.KEY_NUM2:
                return 2;
            case com.a.a.d.b.KEY_NUM3:
                return 3;
            case com.a.a.d.b.KEY_NUM4:
                return 4;
            case com.a.a.d.b.KEY_NUM5:
                return 5;
            case com.a.a.d.b.KEY_NUM6:
                return 6;
            case com.a.a.d.b.KEY_NUM7:
                return 7;
            case com.a.a.d.b.KEY_NUM8:
                return 8;
            case com.a.a.d.b.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private boolean J(int i2) {
        return ((1 << I(i2)) & this.nX) != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.plugin.device.MIDPDevice.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      org.meteoroid.plugin.device.MIDPDevice.b(org.meteoroid.plugin.device.MIDPDevice, int):int
      org.meteoroid.plugin.device.MIDPDevice.b(java.lang.String, boolean):boolean */
    private void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
                "Set screen width " + this.width;
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
                "Set screen height " + this.height;
            }
            if (properties.containsKey("font.size.large")) {
                d.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
                "Set font.size.large " + d.SIZE_LARGE;
            }
            if (properties.containsKey("font.size.medium")) {
                d.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
                "Set font.size.medium " + d.SIZE_MEDIUM;
            }
            if (properties.containsKey("font.size.small")) {
                d.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
                "Set font.size.small " + d.SIZE_SMALL;
            }
            for (int i2 = 0; i2 < 10; i2++) {
                if (properties.containsKey("key." + i2)) {
                    nH.put("NUM_" + i2, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i2).trim())));
                }
            }
            for (int i3 = 0; i3 < nz.length; i3++) {
                if (properties.containsKey("key." + nz[i3])) {
                    nH.put(nz[i3], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + nz[i3]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e2) {
            Log.w("MIDPDevice", "Device property exception. " + e2);
        }
        this.nB = b("hasRepeatEvents", false);
        this.nD = b("hasPointerMotionEvents", true);
        this.nE = b("isDoubleBuffered", true);
        this.nA = b("isPositiveUpdate", false);
        this.nF = b("enableMultiTouch", false);
        this.nG = b("fixTouchPosition", false);
    }

    private boolean b(String str, boolean z) {
        "Get device property:" + str;
        String property = nP.getProperty(str);
        return property == null ? z : Boolean.parseBoolean(property);
    }

    private static e d(Bitmap bitmap) {
        return new e(new Canvas(bitmap));
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    private final void o(int i2, int i3) {
        int I = I(i3);
        if (I == -1) {
            return;
        }
        if (i2 == 0) {
            this.nX = (1 << I) | this.nX;
            return;
        }
        this.nX = (1 << I) ^ this.nX;
    }

    private synchronized void unlock() {
        notify();
    }

    public final com.a.a.b.a a(String str, int i2, boolean z) {
        "Connector open " + str + ".";
        String trim = str.trim();
        if (trim.startsWith("http:")) {
            return new f(trim, i2, z);
        }
        if (trim.startsWith("sms:")) {
            return new j(trim);
        }
        if (trim.startsWith("socket:")) {
            return new l(trim, i2);
        }
        if (trim.startsWith("file:")) {
            return new b(trim);
        }
        if (trim.indexOf("MACadd") != -1) {
            return new g(trim);
        }
        if (trim.startsWith("btl2cap://") || trim.startsWith("btspp://")) {
            return new h(trim);
        }
        throw new IOException("Unkown protocol:" + trim);
    }

    public final void a(int i2, float f2, float f3, int i3) {
        if (this.nF || i3 == 0) {
            org.meteoroid.core.h.b(org.meteoroid.core.h.a(com.a.a.i.a.MSG_DEVICE_TOUCH_EVENT, new int[]{i2, (int) f2, (int) f3, i3}));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        setTouchable(attributeSet.getAttributeBooleanValue(str, "touchable", false));
        this.nC = isTouchable();
        j(attributeSet.getAttributeBooleanValue(str, "filter", true));
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.qB = com.a.a.j.c.aN(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            a(com.a.a.j.c.aN(attributeValue2));
        }
    }

    public final void a(com.a.a.i.c cVar) {
        if (!org.meteoroid.core.m.nt) {
            if (this.nE) {
                this.nI = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.nJ = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.nN = d(this.nI);
                this.nO = d(this.nJ);
                this.nM = this.nN;
                this.nK = this.nJ;
            } else {
                this.nI = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.nJ = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.nK = this.nJ;
                this.nL = new Canvas(this.nK);
                this.nM = d(this.nI);
            }
        }
        dR();
        if (this.nA) {
            org.meteoroid.core.m.dx();
        }
        com.a.a.d.e.a((MIDlet) null);
        com.a.a.d.e.l(getWidth(), getHeight());
    }

    public final boolean a(Message message) {
        boolean z;
        boolean z2 = false;
        if (message.what == 47872) {
            final HashMap hashMap = new HashMap();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(org.meteoroid.core.k.getActivity().getAssets().open(com.a.a.j.c.aK("META-INF" + File.separator + "MANIFEST.MF")), com.umeng.common.b.e.f));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    readLine.trim();
                    int indexOf = readLine.indexOf(58);
                    if (indexOf >= 0) {
                        String trim = readLine.substring(0, indexOf).trim();
                        String trim2 = readLine.substring(indexOf + 1).trim();
                        if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                            int indexOf2 = trim2.indexOf(44);
                            int lastIndexOf = trim2.lastIndexOf(44);
                            if (indexOf2 < 0 || lastIndexOf < 0) {
                                Log.w("MIDPDevice", "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                            } else {
                                String trim3 = trim2.substring(0, indexOf2).trim();
                                trim2.substring(indexOf2 + 1, lastIndexOf).trim();
                                hashMap.put(trim3, trim2.substring(lastIndexOf + 1).trim());
                                "The midlet " + readLine + " has added.";
                            }
                        } else {
                            org.meteoroid.core.a.n(trim, trim2);
                        }
                    }
                }
                if (hashMap.isEmpty()) {
                    Log.w("MIDPDevice", "No midlets found in MANIFEST.MF.");
                    org.meteoroid.core.k.d(org.meteoroid.core.k.getString(R.string.no_midlet_found), 1);
                    return true;
                } else if (hashMap.size() == 1) {
                    org.meteoroid.core.a.at((String) hashMap.values().toArray()[0]);
                    return true;
                } else {
                    final String[] strArr = new String[hashMap.keySet().size()];
                    hashMap.keySet().toArray(strArr);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(org.meteoroid.core.k.getActivity());
                    builder.setItems(strArr, new DialogInterface.OnClickListener() {
                        public final void onClick(DialogInterface dialogInterface, int i) {
                            org.meteoroid.core.a.at((String) hashMap.get(strArr[i]));
                        }
                    });
                    builder.setCancelable(false);
                    org.meteoroid.core.k.getHandler().post(new Runnable() {
                        public final void run() {
                            builder.create().show();
                        }
                    });
                    return true;
                }
            } catch (IOException e2) {
                Log.w("MIDPDevice", "MANIFEST.MF may not exist or invalid.");
                org.meteoroid.core.k.d(org.meteoroid.core.k.getString(R.string.no_midlet_found), 1);
                return true;
            }
        } else if (message.what == 44036) {
            org.meteoroid.core.h.C(org.meteoroid.core.k.MSG_SYSTEM_EXIT);
            return true;
        } else if (message.what == -2023686143) {
            org.meteoroid.core.k.aA((String) message.obj);
            org.meteoroid.core.h.c(MIDlet.MIDLET_PLATFORM_REQUEST_FINISH, Boolean.FALSE);
            return true;
        } else {
            switch (message.what) {
                case MSG_MIDP_COMMAND_EVENT /*44034*/:
                    com.a.a.d.e.a((MIDlet) null);
                    com.a.a.d.e.a((com.a.a.d.c) message.obj);
                    return true;
                case com.a.a.i.a.MSG_DEVICE_TOUCH_EVENT:
                    int[] iArr = (int[]) message.obj;
                    "action[" + iArr[0] + "]x:" + iArr[1] + " y:" + iArr[2];
                    switch (iArr[0]) {
                        case 0:
                            if (this.nC) {
                                com.a.a.d.e.a((MIDlet) null).b(iArr[1], iArr[2]);
                                this.nT = iArr[1];
                                this.nU = iArr[2];
                            }
                            z = true;
                            break;
                        case 1:
                            if (this.nC) {
                                com.a.a.d.e.a((MIDlet) null).k(iArr[1], iArr[2]);
                            }
                            z = true;
                            break;
                        case 2:
                            if (this.nC && this.nD) {
                                int i2 = iArr[1];
                                int i3 = iArr[2];
                                if (!this.nG) {
                                    z2 = true;
                                } else {
                                    if (this.nV == 0) {
                                        this.nV = getWidth() / 60;
                                    }
                                    if (this.nW == 0) {
                                        this.nW = getHeight() / 60;
                                    }
                                    if (Math.abs(i2 - this.nT) >= this.nV || Math.abs(i3 - this.nU) >= this.nW) {
                                        z2 = true;
                                    }
                                }
                                if (z2) {
                                    com.a.a.d.e.a((MIDlet) null).d(iArr[1], iArr[2]);
                                }
                            }
                            z = true;
                            break;
                        default:
                            return false;
                    }
                case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                    int dH = ((VirtualKey) message.obj).dH();
                    String dT = ((VirtualKey) message.obj).dT();
                    return p(dH, nH.containsKey(dT) ? nH.get(dT).intValue() : 65535);
                default:
                    z = false;
                    break;
            }
            return z;
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        if (!this.nS) {
            org.meteoroid.core.k.dh().schedule(new a(this, (byte) 0), 500, 700);
            this.nS = true;
        }
        "Native key event:" + keyEvent.getAction() + "[" + keyEvent.getKeyCode() + "]";
        if (keyEvent.getAction() == 1) {
            this.nR = keyEvent.getKeyCode();
            this.nQ = -1;
        } else if (keyEvent.getAction() == 0) {
            if (this.nQ == -1 && this.nR == -1) {
                p(0, F(keyEvent.getKeyCode()));
            }
            this.nQ = keyEvent.getKeyCode();
        }
        return false;
    }

    public final com.a.a.d.h aU() {
        return this.nM;
    }

    public final void dB() {
        if (this.nE) {
            if (this.nK == this.nI) {
                this.nK = this.nJ;
                this.nM = this.nN;
            } else {
                this.nK = this.nI;
                this.nM = this.nO;
            }
        } else if (this.nI != null && !this.nI.isRecycled()) {
            this.nL.drawBitmap(this.nI, 0.0f, 0.0f, (Paint) null);
        }
        if (this.nM != null) {
            this.nM.aT();
        }
        if (this.nA) {
            unlock();
        } else {
            org.meteoroid.core.m.dy();
        }
    }

    public final Bitmap dC() {
        return this.nK;
    }

    public final int getHeight() {
        return this.height == -1 ? dQ().height() : this.height;
    }

    public final int getWidth() {
        return this.width == -1 ? dQ().width() : this.width;
    }

    public final void onCreate() {
        org.meteoroid.core.h.b((int) com.a.a.i.a.MSG_DEVICE_TOUCH_EVENT, "MSG_DEVICE_TOUCH_EVENT");
        org.meteoroid.core.h.b((int) MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        org.meteoroid.core.h.b((int) MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        org.meteoroid.core.h.b((int) com.a.a.i.a.MSG_DEVICE_REQUEST_REFRESH, "MSG_DEVICE_REQUEST_REFRESH");
        org.meteoroid.core.h.a(this);
        nP.clear();
        try {
            nP.load(org.meteoroid.core.k.getActivity().getResources().openRawResource(com.a.a.j.c.aM("device")));
        } catch (IOException e2) {
            Log.e("MIDPDevice", "device.properties not exist or valid." + e2);
        }
        a(nP);
        org.meteoroid.core.f.a((f.a) this);
        org.meteoroid.core.f.a((f.e) this);
        System.gc();
    }

    public final void onDestroy() {
        if (this.nI != null) {
            this.nI.recycle();
        }
        this.nI = null;
        if (this.nJ != null) {
            this.nJ.recycle();
        }
        this.nJ = null;
        if (this.nK != null) {
            this.nK.recycle();
        }
        this.nK = null;
        this.nM = null;
        boolean z = this.ob;
    }

    public final void onDraw(Canvas canvas) {
        if (this.nK != null && !this.nK.isRecycled()) {
            canvas.drawBitmap(this.nK, this.qB, dQ(), this.qA);
        }
        if (this.nA) {
            lock();
        }
    }

    public final boolean p(int i2, int i3) {
        if (i3 == 0) {
            return false;
        }
        switch (i2) {
            case 0:
                if (!J(i3)) {
                    com.a.a.d.e.a((MIDlet) null).d(i3);
                    "Dispatch key event type: DOWN [" + i3 + "]";
                } else if (this.nB) {
                    com.a.a.d.e.a((MIDlet) null).p(i3);
                    "Dispatch key event type: REPEAT [" + i3 + "]";
                }
                o(i2, i3);
                return true;
            case 1:
                if (J(i3)) {
                    com.a.a.d.e.a((MIDlet) null).e(i3);
                    "Dispatch key event type: UP [" + i3 + "]";
                    o(i2, i3);
                }
                return true;
            default:
                "Unkown key event type:" + i2 + "[" + i3 + "]";
                return false;
        }
    }

    public final void setVisible(boolean z) {
    }
}
