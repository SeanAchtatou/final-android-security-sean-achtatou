package org.meteoroid.plugin.vd;

public abstract class Switcher extends AbstractButton {
    public abstract void dK();

    public final boolean g(int i, int i2, int i3, int i4) {
        if (!this.pY.contains(i2, i3) || i != 0) {
            return false;
        }
        dK();
        return false;
    }
}
