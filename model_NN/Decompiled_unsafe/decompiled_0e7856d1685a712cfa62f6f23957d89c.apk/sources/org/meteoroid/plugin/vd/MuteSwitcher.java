package org.meteoroid.plugin.vd;

import org.meteoroid.core.g;

public final class MuteSwitcher extends BooleanSwitcher {
    public final void dL() {
        g.h(true);
    }

    public final void dM() {
        g.h(false);
    }
}
