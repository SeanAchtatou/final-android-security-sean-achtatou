package org.meteoroid.plugin.vd;

import org.meteoroid.core.k;

public class CallOptionMenu extends SimpleButton {
    private boolean hc;

    public final void onClick() {
        if (this.hc) {
            k.getActivity().closeOptionsMenu();
        } else {
            k.getActivity().openOptionsMenu();
        }
        this.hc = !this.hc;
    }
}
