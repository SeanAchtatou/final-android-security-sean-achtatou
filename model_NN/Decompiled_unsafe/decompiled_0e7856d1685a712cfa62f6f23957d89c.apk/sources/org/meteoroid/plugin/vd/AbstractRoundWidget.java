package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.k;

public abstract class AbstractRoundWidget extends AbstractButton {
    int centerX;
    int centerY;
    int qh;
    int qi;
    VirtualKey qj;

    public final float a(float f, float f2) {
        float f3 = f - ((float) this.centerX);
        float f4 = f2 - ((float) this.centerY);
        double degrees = Math.toDegrees(Math.atan((double) (f4 / f3)));
        if (f3 >= 0.0f && f4 < 0.0f) {
            degrees = Math.abs(degrees);
        } else if (f3 < 0.0f && f4 <= 0.0f) {
            degrees = 180.0d - degrees;
        } else if (f3 <= 0.0f && f4 > 0.0f) {
            degrees = Math.abs(degrees) + 180.0d;
        } else if (f3 > 0.0f && f4 >= 0.0f) {
            degrees = 360.0d - Math.abs(degrees);
        }
        return (float) degrees;
    }

    public void a(AttributeSet attributeSet, String str) {
        this.centerX = attributeSet.getAttributeIntValue(str, "x", k.de() / 2);
        this.centerY = attributeSet.getAttributeIntValue(str, "y", k.df() / 2);
        this.qh = attributeSet.getAttributeIntValue(str, "max", 60);
        this.qi = attributeSet.getAttributeIntValue(str, "min", 5);
        String attributeValue = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue != null) {
            String[] split = attributeValue.split(",");
            if (split.length > 0) {
                this.qc = Integer.parseInt(split[0]);
            } else {
                this.qc = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
    }

    public boolean g(int i, int i2, int i3, int i4) {
        return false;
    }

    public final void release() {
        this.id = -1;
        if (this.qj != null && this.qj.state == 0) {
            this.qj.state = 1;
            VirtualKey.b(this.qj);
        }
        reset();
    }

    public abstract void reset();
}
