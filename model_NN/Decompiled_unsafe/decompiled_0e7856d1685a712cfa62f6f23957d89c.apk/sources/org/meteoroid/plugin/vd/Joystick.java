package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.j.c;
import mm.purchasesdk.PurchaseCode;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] qk = new VirtualKey[5];
    private static final VirtualKey[] qu = new VirtualKey[9];
    int mode;
    private boolean qg = true;
    int qv;
    int qw;
    Bitmap[] qx;
    Bitmap[] qy;

    static VirtualKey a(float f, int i) {
        return i == 8 ? qu[(int) ((((double) f) + 22.5d) / 45.0d)] : qk[(int) ((45.0f + f) / 90.0f)];
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qx = c.aP(attributeSet.getAttributeValue(str, "stick"));
        this.qy = c.aP(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public final void a(com.a.a.i.c cVar) {
        super.a(cVar);
        if (this.mode == 8) {
            qu[0] = new VirtualKey();
            qu[0].qR = "RIGHT";
            qu[1] = new VirtualKey();
            qu[1].qR = "NUM_3";
            qu[2] = new VirtualKey();
            qu[2].qR = "UP";
            qu[3] = new VirtualKey();
            qu[3].qR = "NUM_1";
            qu[4] = new VirtualKey();
            qu[4].qR = "LEFT";
            qu[5] = new VirtualKey();
            qu[5].qR = "NUM_7";
            qu[6] = new VirtualKey();
            qu[6].qR = "DOWN";
            qu[7] = new VirtualKey();
            qu[7].qR = "NUM_9";
            qu[8] = qu[0];
        } else if (this.mode == 10) {
            qu[0] = new VirtualKey();
            qu[0].qR = "NUM_6";
            qu[1] = new VirtualKey();
            qu[1].qR = "NUM_3";
            qu[2] = new VirtualKey();
            qu[2].qR = "NUM_2";
            qu[3] = new VirtualKey();
            qu[3].qR = "NUM_1";
            qu[4] = new VirtualKey();
            qu[4].qR = "NUM_4";
            qu[5] = new VirtualKey();
            qu[5].qR = "NUM_7";
            qu[6] = new VirtualKey();
            qu[6].qR = "NUM_8";
            qu[7] = new VirtualKey();
            qu[7].qR = "NUM_9";
            qu[8] = qu[0];
        } else if (this.mode == 6) {
            qk[0] = new VirtualKey();
            qk[0].qR = "NUM_6";
            qk[1] = new VirtualKey();
            qk[1].qR = "NUM_2";
            qk[2] = new VirtualKey();
            qk[2].qR = "NUM_4";
            qk[3] = new VirtualKey();
            qk[3].qR = "NUM_8";
            qk[4] = qk[0];
        } else {
            qk[0] = new VirtualKey();
            qk[0].qR = "RIGHT";
            qk[1] = new VirtualKey();
            qk[1].qR = "UP";
            qk[2] = new VirtualKey();
            qk[2].qR = "LEFT";
            qk[3] = new VirtualKey();
            qk[3].qR = "DOWN";
            qk[4] = qk[0];
        }
        reset();
    }

    public final boolean dA() {
        return (this.qy == null && this.qx == null) ? false : true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean g(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.qh) || sqrt < ((double) this.qi)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        this.state = 0;
        this.qv = i2;
        this.qw = i3;
        VirtualKey a = a(a((float) i2, (float) i3), this.mode);
        if (this.qj != a) {
            if (this.qj != null && this.qj.state == 0) {
                this.qj.state = 1;
                VirtualKey.b(this.qj);
            }
            this.qj = a;
        }
        this.qj.state = 0;
        VirtualKey.b(this.qj);
        return true;
    }

    public void onDraw(Canvas canvas) {
        Paint paint = null;
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.qd = 0;
            this.qg = true;
        }
        if (this.qg) {
            if (this.qc > 0 && this.state == 1) {
                this.qd++;
                this.gm.setAlpha(255 - ((this.qd * PurchaseCode.AUTH_INVALID_APP) / this.qc));
                if (this.qd >= this.qc) {
                    this.qd = 0;
                    this.qg = false;
                }
            }
            if (a(this.qy)) {
                canvas.drawBitmap(this.qy[this.state], (float) (this.centerX - (this.qy[this.state].getWidth() / 2)), (float) (this.centerY - (this.qy[this.state].getHeight() / 2)), (this.qc == -1 || this.state != 1) ? null : this.gm);
            }
            if (a(this.qx)) {
                Bitmap bitmap = this.qx[this.state];
                float width = (float) (this.qv - (this.qx[this.state].getWidth() / 2));
                float height = (float) (this.qw - (this.qx[this.state].getHeight() / 2));
                if (this.qc != -1 && this.state == 1) {
                    paint = this.gm;
                }
                canvas.drawBitmap(bitmap, width, height, paint);
            }
        }
    }

    public final void reset() {
        this.qv = this.centerX;
        this.qw = this.centerY;
        this.state = 1;
    }

    public final void setVisible(boolean z) {
        this.qg = z;
    }
}
