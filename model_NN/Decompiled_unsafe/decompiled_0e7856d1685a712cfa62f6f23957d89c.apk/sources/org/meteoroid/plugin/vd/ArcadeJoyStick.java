package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.j.c;
import mm.purchasesdk.PurchaseCode;

public class ArcadeJoyStick extends AbstractRoundWidget {
    public static final int CENTER = 0;
    public static final int DOWN = 2;
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int UP = 1;
    private static final VirtualKey[] qk = new VirtualKey[5];
    private boolean qg = true;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qa = c.aP(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(com.a.a.i.c cVar) {
        super.a(cVar);
        qk[0] = new VirtualKey();
        qk[0].qR = "RIGHT";
        qk[1] = new VirtualKey();
        qk[1].qR = "UP";
        qk[2] = new VirtualKey();
        qk[2].qR = "LEFT";
        qk[3] = new VirtualKey();
        qk[3].qR = "DOWN";
        qk[4] = qk[0];
        this.state = 0;
    }

    public final boolean dA() {
        return this.qa != null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean g(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.qh) || sqrt < ((double) this.qi)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        float a = a((float) i2, (float) i3);
        this.state = (a < 45.0f || a >= 135.0f) ? (a < 135.0f || a >= 225.0f) ? (a < 225.0f || a >= 315.0f) ? (a >= 315.0f || a < 45.0f) ? 4 : 0 : 2 : 3 : 1;
        VirtualKey virtualKey = qk[(int) ((a + 45.0f) / 90.0f)];
        if (this.qj != virtualKey) {
            if (this.qj != null && this.qj.state == 0) {
                this.qj.state = 1;
                VirtualKey.b(this.qj);
            }
            this.qj = virtualKey;
        }
        this.qj.state = 0;
        VirtualKey.b(this.qj);
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.qd = 0;
            this.qg = true;
        }
        if (this.qg) {
            if (this.qc > 0 && this.state == 1) {
                this.qd++;
                this.gm.setAlpha(255 - ((this.qd * PurchaseCode.AUTH_INVALID_APP) / this.qc));
                if (this.qd >= this.qc) {
                    this.qd = 0;
                    this.qg = false;
                }
            }
            if (a(this.qa)) {
                canvas.drawBitmap(this.qa[this.state], (float) (this.centerX - (this.qa[this.state].getWidth() / 2)), (float) (this.centerY - (this.qa[this.state].getHeight() / 2)), (Paint) null);
            }
        }
    }

    public final void reset() {
        this.state = 0;
    }

    public final void setVisible(boolean z) {
        this.qg = z;
    }
}
