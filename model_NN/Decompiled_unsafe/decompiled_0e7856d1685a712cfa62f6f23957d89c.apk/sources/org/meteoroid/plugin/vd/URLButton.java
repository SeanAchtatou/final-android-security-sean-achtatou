package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;
import org.meteoroid.core.k;

public final class URLButton extends SimpleButton {
    private int index;
    public String[] qQ;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qQ = attributeSet.getAttributeValue(str, "value").split(",");
    }

    public final void onClick() {
        if (this.qQ != null && this.qQ[this.index] != null) {
            h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"URLClick", k.cZ() + "=" + this.qQ[this.index]});
            k.aB(this.qQ[this.index]);
        }
    }
}
