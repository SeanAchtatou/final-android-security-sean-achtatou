package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.i.c;

public class Background implements c.a {
    int color = -16777216;
    private c nr;
    private Paint oo;
    private boolean qg = true;
    Bitmap ql;
    Rect rect;

    public void a(AttributeSet attributeSet, String str) {
        this.rect = com.a.a.j.c.aN(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.ql = com.a.a.j.c.aO(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public final void a(c cVar) {
        this.nr = cVar;
        if (this.ql == null) {
            this.oo = new Paint();
            this.oo.setAntiAlias(true);
            this.oo.setStyle(Paint.Style.FILL);
        }
    }

    public final boolean dA() {
        return true;
    }

    public final c dI() {
        return this.nr;
    }

    public final boolean f(int i, int i2, int i3, int i4) {
        return false;
    }

    public final boolean isTouchable() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.qg) {
            if (this.ql == null) {
                this.oo.setColor(this.color);
                canvas.drawRect(this.rect, this.oo);
                return;
            }
            canvas.drawBitmap(this.ql, (Rect) null, this.rect, (Paint) null);
        }
    }

    public final void setVisible(boolean z) {
        this.qg = z;
    }
}
