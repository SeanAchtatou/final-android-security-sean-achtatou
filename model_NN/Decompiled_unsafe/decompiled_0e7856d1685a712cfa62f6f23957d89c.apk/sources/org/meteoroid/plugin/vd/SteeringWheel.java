package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.a.a.j.c;
import mm.purchasesdk.PurchaseCode;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] qL = new VirtualKey[2];
    private final Matrix gR = new Matrix();
    private int orientation = 0;
    private float qM = -1.0f;
    private float qN = 0.0f;
    public int qO = 135;
    public int qP = 5;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qO = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.qP = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.qa = c.aP(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(com.a.a.i.c cVar) {
        super.a(cVar);
        qL[0] = new VirtualKey();
        qL[0].qR = "LEFT";
        qL[1] = new VirtualKey();
        qL[1].qR = "RIGHT";
        reset();
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        VirtualKey virtualKey;
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.qh) || sqrt < ((double) this.qi)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.qM == -1.0f) {
                    this.qM = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    float a = a((float) i2, (float) i3);
                    float f = this.qM;
                    float f2 = a - f;
                    if (this.orientation == 0) {
                        if (Math.abs(f2) > 180.0f) {
                            if (f2 > 0.0f) {
                                this.orientation = 1;
                            } else if (f2 < 0.0f) {
                                this.orientation = 2;
                            }
                        } else if (a > f) {
                            this.orientation = 2;
                        } else if (a < f) {
                            this.orientation = 1;
                        }
                    }
                    if (this.orientation == 1 && f2 > 0.0f) {
                        f2 -= 360.0f;
                    } else if (this.orientation == 2 && f2 < 0.0f) {
                        f2 += 360.0f;
                    }
                    this.qN = f2;
                    if (this.qN > ((float) this.qO)) {
                        this.qN = (float) this.qO;
                    } else if (this.qN < ((float) (this.qO * -1))) {
                        this.qN = (float) (this.qO * -1);
                    }
                    if (this.qN >= ((float) this.qP) && this.qN <= ((float) this.qO)) {
                        virtualKey = qL[0];
                    } else if (this.qN > ((float) (this.qP * -1)) || this.qN < ((float) (this.qO * -1))) {
                        if (this.qN < ((float) this.qP) && this.qN > ((float) (this.qP * -1))) {
                            this.orientation = 0;
                            break;
                        } else {
                            virtualKey = null;
                        }
                    } else {
                        virtualKey = qL[1];
                    }
                    if (this.qj != virtualKey) {
                        if (this.qj != null && this.qj.state == 0) {
                            this.qj.state = 1;
                            VirtualKey.b(this.qj);
                        }
                        this.qj = virtualKey;
                    }
                    if (this.qj != null) {
                        this.qj.state = 0;
                        VirtualKey.b(this.qj);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.qd = 0;
            this.qg = true;
        }
        if (this.qg) {
            if (this.qc > 0 && this.state == 1) {
                this.qd++;
                this.gm.setAlpha(255 - ((this.qd * PurchaseCode.AUTH_INVALID_APP) / this.qc));
                if (this.qd >= this.qc) {
                    this.qd = 0;
                    this.qg = false;
                }
            }
            if (this.state == 1) {
                if (this.qN > 0.0f) {
                    this.qN -= 15.0f;
                    this.qN = this.qN < 0.0f ? 0.0f : this.qN;
                } else if (this.qN < 0.0f) {
                    this.qN += 15.0f;
                    this.qN = this.qN > 0.0f ? 0.0f : this.qN;
                }
            }
            if (this.qa != null) {
                canvas.save();
                if (this.qN != 0.0f) {
                    this.gR.setRotate(this.qN * -1.0f, (float) (this.qa[this.state].getWidth() >> 1), (float) (this.qa[this.state].getHeight() >> 1));
                }
                canvas.translate((float) (this.centerX - (this.qa[this.state].getWidth() >> 1)), (float) (this.centerY - (this.qa[this.state].getHeight() >> 1)));
                canvas.drawBitmap(this.qa[this.state], this.gR, null);
                canvas.restore();
                this.gR.reset();
            }
        }
    }

    public final void reset() {
        this.state = 1;
        this.qM = -1.0f;
        this.orientation = 0;
    }
}
