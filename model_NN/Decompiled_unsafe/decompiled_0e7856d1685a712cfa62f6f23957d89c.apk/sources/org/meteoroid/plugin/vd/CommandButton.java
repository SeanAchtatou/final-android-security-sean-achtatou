package org.meteoroid.plugin.vd;

import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.k;
import org.meteoroid.plugin.device.MIDPDevice;
import org.meteoroid.plugin.feature.AbstractDownloadAndInstall;
import org.meteoroid.plugin.feature.Advertisement;

public final class CommandButton extends SimpleButton {
    public static final int MSG_APPLOTTERY = 74710;
    public static final int MSG_CHECKIN = -2004318072;
    public static final int MSG_DISABLE_AD = 9520139;
    public static final int MSG_REQUEST_RELOAD_SKIN = 952848;
    public static final int MSG_SOCIAL_PLATFORM_LAUNCH = 1013249;
    public static final int MSG_TIME_UP = 9520138;
    private String qm;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qm = attributeSet.getAttributeValue(str, "value").trim();
        h.b((int) MSG_APPLOTTERY, "APPLOTTERY");
        h.b((int) MSG_CHECKIN, "CHECKIN");
        h.b((int) MSG_SOCIAL_PLATFORM_LAUNCH, "MSG_SOCIAL_PLATFORM_LAUNCH");
        h.b((int) MSG_REQUEST_RELOAD_SKIN, "MSG_REQUEST_RELOAD_SKIN");
        h.b((int) MSG_TIME_UP, "MSG_TIME_UP");
        h.b((int) MSG_DISABLE_AD, "MSG_DISABLE_AD");
    }

    public final void onClick() {
        int indexOf;
        String str = null;
        if (this.qm.startsWith("CMD_EXIT")) {
            k.da();
        } else if (this.qm.equalsIgnoreCase("CMD_ABOUT")) {
            h.C(i.MSG_OPTIONMENU_ABOUT);
        } else if (this.qm.equalsIgnoreCase("CMD_MENU")) {
            k.getHandler().post(new Runnable() {
                public final void run() {
                    k.getActivity().openOptionsMenu();
                }
            });
        } else if (this.qm.startsWith("CMD_CHECKIN")) {
            h.C(MSG_CHECKIN);
        } else if (this.qm.startsWith("CMD_APPLOTTERY")) {
            h.C(MSG_APPLOTTERY);
        } else if (this.qm.startsWith("CMD_SNS")) {
            h.C(MSG_SOCIAL_PLATFORM_LAUNCH);
        } else if (this.qm.startsWith("CMD_RELOADSKIN")) {
            h.C(MSG_REQUEST_RELOAD_SKIN);
        } else if (this.qm.startsWith("CMD_TIMEUP")) {
            h.C(MSG_TIME_UP);
        } else if (this.qm.startsWith("CMD_INSTALLAPP")) {
            AnonymousClass2 r1 = new AbstractDownloadAndInstall() {
                public final String aF() {
                    return "AbstractDownloadAndInstall";
                }

                public final String getName() {
                    return "AbstractDownloadAndInstall";
                }
            };
            String str2 = this.qm;
            int indexOf2 = str2.indexOf(40);
            if (!(indexOf2 == -1 || (indexOf = str2.indexOf(41, indexOf2)) == -1)) {
                str = str2.substring(indexOf2 + 1, indexOf);
            }
            r1.aG(str);
            r1.ph = true;
            if (r1.dD().isEmpty()) {
                k.d("您已经安装了该应用", 0);
            } else {
                r1.a(r1.dD().get(0));
            }
        } else if (!this.qm.startsWith("CMD_DISABLE_AD")) {
            Log.w("CommandButton", "NULL command:" + this.qm);
        } else if (Advertisement.O(k.getActivity())) {
            k.d("广告功能已经关闭", 0);
        } else {
            h.a(new h.a() {
                public final boolean a(Message message) {
                    if (message.what != 61698) {
                        return false;
                    }
                    h.C(CommandButton.MSG_DISABLE_AD);
                    k.d("广告功能关闭成功", 0);
                    return true;
                }
            });
            h.C(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND);
        }
    }
}
