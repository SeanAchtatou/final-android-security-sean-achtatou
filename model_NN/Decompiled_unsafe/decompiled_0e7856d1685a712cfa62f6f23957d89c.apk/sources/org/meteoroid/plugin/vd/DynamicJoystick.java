package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.util.AttributeSet;

public class DynamicJoystick extends Joystick {
    private int qs;
    private int qt;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qs = this.centerX;
        this.qt = this.centerY;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        if (this.state == 1 && i == 0 && Math.sqrt(Math.pow((double) (i2 - this.qs), 2.0d) + Math.pow((double) (i3 - this.qt), 2.0d)) <= ((double) this.qh)) {
            this.id = i4;
            this.centerX = i2;
            this.centerY = i3;
            this.state = 0;
        }
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.qh) || sqrt < ((double) this.qi)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                if (this.id != i4) {
                    return true;
                }
                this.state = 0;
                this.qv = i2;
                this.qw = i3;
                VirtualKey a = a(a((float) i2, (float) i3), this.mode);
                if (this.qj != a) {
                    if (this.qj != null && this.qj.state == 0) {
                        this.qj.state = 1;
                        VirtualKey.b(this.qj);
                    }
                    this.qj = a;
                }
                this.qj.state = 0;
                VirtualKey.b(this.qj);
                return true;
            default:
                return true;
        }
    }

    public final void onDraw(Canvas canvas) {
        if (this.state != 1) {
            super.onDraw(canvas);
        }
    }
}
