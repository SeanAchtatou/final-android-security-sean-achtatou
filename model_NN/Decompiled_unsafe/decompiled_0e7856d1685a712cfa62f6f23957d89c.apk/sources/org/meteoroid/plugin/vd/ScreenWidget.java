package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import com.a.a.i.c;

public abstract class ScreenWidget implements c.a {
    public final Paint qA = new Paint();
    public Rect qB;
    private float qC;
    private float qD;
    private boolean qE;
    private final Rect qz = new Rect();

    public abstract void a(int i, float f, float f2, int i2);

    public final void a(Rect rect) {
        this.qz.left = rect.left;
        this.qz.top = rect.top;
        this.qz.right = rect.right;
        this.qz.bottom = rect.bottom;
    }

    public final boolean dA() {
        return true;
    }

    public abstract Bitmap dC();

    public final Rect dQ() {
        return this.qz;
    }

    public final void dR() {
        if (dC() != null) {
            this.qC = ((float) this.qz.width()) / ((float) dC().getWidth());
            this.qD = ((float) this.qz.height()) / ((float) dC().getHeight());
            if (this.qC == 1.0f && this.qD == 1.0f) {
                this.qA.setFilterBitmap(false);
            } else {
                this.qA.setFilterBitmap(true);
            }
            "Scale Screen to " + this.qz.width() + "x" + this.qz.height();
        }
    }

    public final boolean f(int i, int i2, int i3, int i4) {
        if (!this.qz.contains(i2, i3) || !this.qE) {
            return false;
        }
        a(i, ((float) (i2 - this.qz.left)) / this.qC, ((float) (i3 - this.qz.top)) / this.qD, i4);
        return false;
    }

    public final boolean isTouchable() {
        return this.qE;
    }

    public final void j(boolean z) {
        if (this.qC != 1.0f || this.qD != 1.0f) {
            this.qA.setFilterBitmap(z);
        }
    }

    public final void setTouchable(boolean z) {
        this.qE = z;
    }
}
