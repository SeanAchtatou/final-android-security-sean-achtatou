package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;

public class VirtualKey extends AbstractButton {
    public static final int MSG_VIRTUAL_KEY_EVENT = 7833601;
    public String qR;
    private boolean qS = true;

    public static final synchronized void b(VirtualKey virtualKey) {
        synchronized (VirtualKey.class) {
            h.b(h.a(MSG_VIRTUAL_KEY_EVENT, virtualKey));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qR = attributeSet.getAttributeValue(str, "keyname");
        this.qS = attributeSet.getAttributeBooleanValue(str, "clickThrough", true);
    }

    public final String dT() {
        return this.qR;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        if (this.pY.contains(i2, i3)) {
            switch (i) {
                case 0:
                case 2:
                    this.id = i4;
                    this.state = 0;
                    b(this);
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    b(this);
                    break;
            }
            if (!this.qS) {
                return true;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
            b(this);
        }
        return false;
    }
}
