package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.i.c;
import mm.purchasesdk.PurchaseCode;

public abstract class AbstractButton implements c.a {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    private int count;
    int delay;
    Paint gm = new Paint();
    int id = -1;
    private c nr;
    Rect pY;
    Rect pZ;
    int px;
    Bitmap[] qa;
    boolean qb;
    int qc;
    int qd;
    boolean qe = true;
    private int qf;
    boolean qg = true;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.qa = com.a.a.j.c.aP(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.pY = com.a.a.j.c.aN(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.pZ = com.a.a.j.c.aN(attributeValue2);
        }
        String attributeValue3 = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                this.qc = Integer.parseInt(split[0]);
            } else {
                this.qc = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
        if (this.qa != null && this.qa.length > 2) {
            this.qb = true;
            this.px = attributeSet.getAttributeIntValue(str, "interval", 40);
        }
    }

    public void a(c cVar) {
        this.nr = cVar;
        if (this.pY == null) {
            this.pY = this.pZ;
        }
    }

    public final boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length + -1 && bitmapArr[this.state] != null;
    }

    public boolean dA() {
        return this.qa != null;
    }

    public final int dH() {
        return this.state;
    }

    public final boolean f(int i, int i2, int i3, int i4) {
        return g(i, i2, i3, i4);
    }

    public abstract boolean g(int i, int i2, int i3, int i4);

    public final boolean isTouchable() {
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.qb) {
            this.count++;
            if (this.count >= this.px) {
                this.count = 0;
                this.qf++;
                if (this.qa != null && this.qf >= this.qa.length) {
                    this.qf = 0;
                }
            }
        } else if (this.delay > 0) {
            this.delay--;
            return;
        } else {
            if (this.state == 0) {
                this.qd = 0;
                this.qg = true;
            }
            if (this.qg) {
                if (this.qc > 0 && this.state == 1) {
                    this.qd++;
                    this.gm.setAlpha(255 - ((this.qd * PurchaseCode.AUTH_INVALID_APP) / this.qc));
                    if (this.qd >= this.qc) {
                        this.qd = 0;
                        this.qg = false;
                    }
                }
                this.qf = this.state;
            } else {
                return;
            }
        }
        if (a(this.qa)) {
            canvas.drawBitmap(this.qa[this.qf], (Rect) null, this.pZ, (this.qc == -1 || this.state != 1) ? null : this.gm);
        }
    }

    public void setVisible(boolean z) {
        this.qg = z;
    }
}
