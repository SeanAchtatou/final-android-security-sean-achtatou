package org.meteoroid.plugin.vd;

import org.meteoroid.core.h;
import org.meteoroid.core.k;

public class CheckinButton extends SimpleButton {
    public final void onClick() {
        h.b((int) k.MSG_SYSTEM_LOG_EVENT, new String[]{"Checkin", k.cZ()});
        h.C(CommandButton.MSG_CHECKIN);
    }
}
