package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class BooleanSwitcher extends Switcher {
    public static final int OFF = 1;
    public static final int ON = 0;

    private void dJ() {
        if (this.state == 0) {
            dL();
        } else {
            dM();
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        int i = 0;
        super.a(attributeSet, str);
        if (attributeSet.getAttributeBooleanValue(str, "value", false)) {
            i = 1;
        }
        this.state = i;
        dJ();
    }

    public final void dK() {
        int i = 1;
        if (this.state == 1) {
            i = 0;
        }
        this.state = i;
        dJ();
    }

    public abstract void dL();

    public abstract void dM();
}
