package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;
import com.a.a.j.c;
import org.meteoroid.core.f;
import org.meteoroid.core.m;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static final VirtualKey[] qk = new VirtualKey[4];
    private int[] qF;
    private int qG = 2;
    private int qH = 2;
    private VirtualKey qI;
    private final int[] qJ = new int[3];
    private int qK = 0;
    private int qr;
    private int x;
    private int y;
    private int z;

    private synchronized void a(VirtualKey virtualKey) {
        if (virtualKey != this.qI) {
            dS();
        }
        if (virtualKey != null && virtualKey.state == 1) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.qI = virtualKey;
        }
        if (virtualKey != null) {
            "sensor vb:[" + virtualKey.qR + "|" + virtualKey.state + "]";
        }
    }

    private void dS() {
        if (this.qI != null && this.qI.state == 0) {
            this.qI.state = 1;
            VirtualKey.b(this.qI);
            this.qI = null;
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        this.qa = c.aP(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.pY = c.aN(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.pZ = c.aN(attributeValue2);
        }
        this.state = 1;
        this.qc = attributeSet.getAttributeIntValue(str, "fade", -1);
        String attributeValue3 = attributeSet.getAttributeValue(str, "value");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                try {
                    if (Integer.parseInt(split[0]) == 0) {
                        dL();
                    }
                } catch (NumberFormatException e) {
                    if (Boolean.parseBoolean(split[0])) {
                        dL();
                    }
                }
            }
            if (split.length >= 3) {
                this.qG = Integer.parseInt(split[1]);
                this.qH = Integer.parseInt(split[2]);
            }
            if (split.length >= 6) {
                this.qF = new int[3];
                this.qF[0] = Integer.parseInt(split[3]);
                this.qF[1] = Integer.parseInt(split[4]);
                this.qF[2] = Integer.parseInt(split[5]);
            }
        }
    }

    public final void a(com.a.a.i.c cVar) {
        super.a(cVar);
        qk[0] = new VirtualKey();
        qk[0].qR = "UP";
        qk[0].state = 1;
        qk[1] = new VirtualKey();
        qk[1].qR = "DOWN";
        qk[1].state = 1;
        qk[2] = new VirtualKey();
        qk[2].qR = "LEFT";
        qk[2].state = 1;
        qk[3] = new VirtualKey();
        qk[3].qR = "RIGHT";
        qk[3].state = 1;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.qK = 0;
        this.qJ[0] = 0;
        this.qJ[1] = 0;
        this.qJ[2] = 0;
        dS();
    }

    public final void dL() {
        f.a((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch on.");
    }

    public final void dM() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.qK = 0;
        this.qJ[0] = 0;
        this.qJ[1] = 0;
        this.qJ[2] = 0;
        dS();
        f.b((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch off.");
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        "sensor:[" + ((int) sensorEvent.values[0]) + "|" + ((int) sensorEvent.values[1]) + "|" + ((int) sensorEvent.values[2]) + "]";
        this.qr = m.nr.getOrientation();
        if (this.qr == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.qr == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w("SensorSwitcher", "deviceOrientation:" + this.qr);
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.qK <= 0) {
            if (this.qF != null) {
                this.qJ[0] = this.qF[0];
                this.qJ[1] = this.qF[1];
                this.qJ[2] = this.qF[2];
            } else {
                this.qJ[0] = this.x;
                this.qJ[1] = this.y;
                this.qJ[2] = this.z;
            }
            "sensor_init:[" + this.qJ[0] + "|" + this.qJ[1] + "|" + this.qJ[2] + "]";
            this.qK++;
            return;
        }
        int i = this.x - this.qJ[0];
        int i2 = this.y - this.qJ[1];
        int i3 = this.z - this.qJ[2];
        if (Math.abs(i) < this.qG && Math.abs(i2) < this.qH && Math.abs(i3) < this.qH) {
            dS();
        } else if (Math.abs(i) >= this.qG) {
            if (i < 0) {
                a(qk[2]);
            } else {
                a(qk[3]);
            }
        } else if (Math.abs(i2) >= this.qH) {
            if (i2 > 0) {
                a(qk[1]);
            } else {
                a(qk[0]);
            }
            a(this.qI);
        } else if (Math.abs(i3) < this.qH) {
        } else {
            if (i3 > 0) {
                a(qk[0]);
            } else {
                a(qk[1]);
            }
        }
    }
}
