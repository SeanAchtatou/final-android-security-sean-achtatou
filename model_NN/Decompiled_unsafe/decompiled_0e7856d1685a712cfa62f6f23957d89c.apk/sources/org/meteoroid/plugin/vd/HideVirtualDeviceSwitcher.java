package org.meteoroid.plugin.vd;

import com.a.a.i.c;
import java.util.Iterator;
import org.meteoroid.core.m;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public final void dL() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) m.nr;
        Iterator<c.a> it = defaultVirtualDevice.dN().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.dO()) {
                next.setVisible(false);
            }
        }
    }

    public final void dM() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) m.nr;
        Iterator<c.a> it = defaultVirtualDevice.dN().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.dO()) {
                next.setVisible(true);
            }
        }
    }

    public final void setVisible(boolean z) {
    }
}
