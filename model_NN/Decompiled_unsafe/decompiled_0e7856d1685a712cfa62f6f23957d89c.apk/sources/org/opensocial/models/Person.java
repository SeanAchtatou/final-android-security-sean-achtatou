package org.opensocial.models;

public class Person extends Model {
    public static final String GENDER_FEMALE = "female";
    public static final String GENDER_MALE = "male";
    public static final String GENDER_UNDISCLOSED = "undisclosed";
    public static final String NETWORK_PRESENCE_AWAY = "AWAY";
    public static final String NETWORK_PRESENCE_CHAT = "CHAT";
    public static final String NETWORK_PRESENCE_DND = "DND";
    public static final String NETWORK_PRESENCE_OFFLINE = "_OFFLINE";
    public static final String NETWORK_PRESENCE_ONLINE = "ONLINE";
    public static final String NETWORK_PRESENCE_XA = "XA";
    private String rm = GENDER_UNDISCLOSED;
}
