package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Billing extends Model {
    public static final int BILLING_TYPE_DEFAULT = 0;
    public static final int BILLING_TYPE_OTHER = 4;
    public static final int BILLING_TYPE_SMS = 1;
    public static final int BILLING_TYPE_WAP = 2;
    public static final int BILLING_TYPE_WEB = 3;
}
