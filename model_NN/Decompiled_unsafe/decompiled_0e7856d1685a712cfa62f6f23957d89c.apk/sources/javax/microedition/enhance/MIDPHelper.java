package javax.microedition.enhance;

import android.graphics.Paint;
import android.graphics.Rect;
import com.a.a.j.c;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.meteoroid.core.k;

public final class MIDPHelper {
    public static final int HORIZONTAL = 1;
    private static final String LOG_TAG = "MIDPHelper";
    public static final int MOVEDOWN = 2;
    public static final int MOVELEFT = 3;
    public static final int MOVERIGHT = 4;
    public static final int MOVEUP = 0;
    public static final int NOMOVE = -1;
    public static final int VERTICAL = 0;
    private static final String[] gl = {".png", ".mp3", ".jpg", ".jpeg", ".mpeg", ".bmp"};
    public static Paint gm = new Paint();
    public static Rect rect = new Rect();

    public static InputStream H(String str) {
        String str2;
        if (str == null) {
            throw new IOException("Can't load resource noname.");
        }
        "Load assert " + str + " .";
        while (str.startsWith(File.separator)) {
            str = str.substring(1);
        }
        String[] split = str.split("\\.");
        String aQ = c.aQ(split[0]);
        String aR = c.aR(split[0]);
        if (split.length == 1) {
            str2 = "a_b";
        } else if (split.length == 2) {
            str2 = split[1];
        } else {
            str2 = "";
            for (int i = 1; i < split.length; i++) {
                str2 = str2 + split[i];
            }
        }
        String str3 = (aR.length() == 0 || aR.charAt(0) == '_') ? "b_a" + aR : aR;
        int i2 = 0;
        while (true) {
            if (i2 >= gl.length) {
                break;
            } else if (str2.equalsIgnoreCase(gl[i2])) {
                str2 = str2.toLowerCase();
                break;
            } else {
                i2++;
            }
        }
        String str4 = aQ + str3 + "." + str2;
        "Load assert " + str4 + " .";
        return k.az(c.aK(str4));
    }
}
