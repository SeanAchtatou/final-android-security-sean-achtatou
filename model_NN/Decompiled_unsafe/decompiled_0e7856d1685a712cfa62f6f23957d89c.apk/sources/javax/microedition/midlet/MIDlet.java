package javax.microedition.midlet;

import android.os.Message;
import android.util.Log;
import com.a.a.b.b;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class MIDlet implements a.C0005a, h.a {
    public static final int MIDLET_PLATFORM_REQUEST = -2023686143;
    public static final int MIDLET_PLATFORM_REQUEST_FINISH = -2023686142;
    private boolean gW;
    private int gX;

    protected MIDlet() {
    }

    /* access modifiers changed from: protected */
    public abstract void D();

    /* access modifiers changed from: protected */
    public abstract void E();

    public final boolean M(String str) {
        h.b((int) MIDLET_PLATFORM_REQUEST, "MIDLET_PLATFORM_REQUEST");
        h.b((int) MIDLET_PLATFORM_REQUEST_FINISH, "MIDLET_PLATFORM_REQUEST_FINISH");
        h.c(MIDLET_PLATFORM_REQUEST, str);
        if (!this.gW) {
            return false;
        }
        throw new b();
    }

    public final boolean a(Message message) {
        if (message.what == -2023686142) {
            if (message.obj != null) {
                this.gW = ((Boolean) message.obj).booleanValue();
            }
            return true;
        } else if (message.what == 47623) {
            this.gX = 2;
            E();
            return true;
        } else if (message.what != 47622) {
            return false;
        } else {
            onStart();
            return true;
        }
    }

    public final void aX() {
        getClass().getSimpleName();
        h.C(MIDPDevice.MSG_MIDP_MIDLET_NOTIFYDESTROYED);
    }

    public final void aY() {
        this.gX = 0;
        h.a(this);
        a.start();
    }

    public final int getState() {
        return this.gX;
    }

    public final void onDestroy() {
        this.gX = 3;
        try {
            t();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " in MIDlet destroyApp");
        }
    }

    public final void onPause() {
        this.gX = 2;
    }

    public final void onResume() {
        this.gX = 1;
    }

    public final void onStart() {
        this.gX = 1;
        try {
            D();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " restart in MIDlet startApp");
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public abstract void t();
}
