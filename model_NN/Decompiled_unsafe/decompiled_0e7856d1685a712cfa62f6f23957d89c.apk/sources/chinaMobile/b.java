package chinaMobile;

import com.a.a.d.g;
import com.a.a.d.h;
import com.a.a.d.i;

public final class b extends a {
    public static boolean ao = false;
    private static boolean ap = true;
    private i[] aa;
    private int am = 0;
    private String[] aq;
    private String[] ar = {"是", "否"};
    private int as = 0;
    private int m = 0;

    public b(MIDlet mIDlet, int i, int i2, String[] strArr) {
        this.ah = mIDlet;
        this.q = getWidth();
        this.ag = getHeight();
        if (this.q == 176 && this.ag == 144) {
            this.ag = 208;
        } else if (this.q == 176 && this.ag == 179) {
            this.ag = 205;
        } else if (this.q == 176 && this.ag == 182) {
            this.ag = 204;
        } else if (this.q == 128 && this.ag == 131) {
            this.ag = 149;
        } else if (this.q == 360 && this.ag == 640) {
            this.q = 640;
            this.ag = 360;
        }
        this.o = this.q;
        this.p = this.ag;
        this.ai = i;
        this.u = i2;
        this.aj = g.b(0, 0, 8);
        this.w = this.aj.J("中");
        this.w += this.w >> 2;
        this.am = 0;
        this.aq = strArr;
        try {
            this.aa = new i[2];
            for (int i3 = 0; i3 < this.aa.length; i3++) {
                this.aa[i3] = i.K(new StringBuffer("/logo/logo").append(i3).append(".png").toString());
            }
        } catch (Exception e) {
        }
        d();
    }

    public final void a(h hVar) {
        hVar.a(this.aj);
        hVar.e(0, 0, this.q, this.ag);
        hVar.setColor(0);
        hVar.d(0, 0, this.q, this.ag);
        hVar.translate((this.q - this.o) / 2, 0);
        hVar.e(0, 0, this.o, this.p);
        if (this.m == 0) {
            hVar.setColor(0);
            hVar.d(0, 0, hVar.aQ(), hVar.aP());
            hVar.a(this.aa[0], this.o / 2, this.p / 2, 3);
            this.as++;
            if (this.as == 10) {
                this.as = 0;
                this.m++;
            }
        } else if (this.m == 1) {
            hVar.setColor(0);
            hVar.d(0, 0, hVar.aQ(), hVar.aP());
            hVar.a(this.aa[1], this.o / 2, this.p / 2, 3);
            this.as++;
            if (this.as == 10) {
                this.as = 0;
                if (ap) {
                    this.m++;
                } else {
                    this.m = 3;
                }
            }
        } else if (this.m == 2) {
            hVar.setColor(0);
            hVar.d(0, 0, hVar.aQ(), hVar.aP());
            hVar.setColor(16777215);
            for (int i = 0; i < this.aq.length; i++) {
                hVar.a(this.aq[i], this.o / 2, (this.p / 2) + (this.w * i), 17);
            }
            hVar.a(this.ar[this.am], 0, this.ag - 2, 36);
            hVar.a(this.ar[1 - this.am], this.q, this.ag - 2, 40);
        } else {
            hVar.setColor(0);
            hVar.d(0, 0, hVar.aQ(), hVar.aP());
            hVar.setColor(16777215);
            hVar.a("游戏装载中", this.o / 2, this.p / 2, 17);
        }
        hVar.translate((this.o - this.q) / 2, 0);
        hVar.e(0, 0, this.q, this.ag);
    }

    public final void d(int i) {
        if (this.m != 2) {
            return;
        }
        if (i == this.ai) {
            ao = true;
            this.m++;
        } else if (i == this.u) {
            ao = false;
            this.m++;
        }
    }

    public final void e() {
        this.aa = null;
    }

    public final void e(int i) {
        this.al = 0;
    }

    public final void run() {
        while (this.m <= 2) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                aC();
                aD();
                long currentTimeMillis2 = System.currentTimeMillis();
                if (currentTimeMillis2 - currentTimeMillis < ((long) B)) {
                    Thread.sleep(((long) B) - (currentTimeMillis2 - currentTimeMillis));
                }
            } catch (Exception e) {
                return;
            } finally {
                aC();
                aD();
                c();
                this.ah.b();
            }
        }
    }
}
