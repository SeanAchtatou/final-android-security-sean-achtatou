package game;

import com.a.a.d.i;
import com.a.a.h.b;
import mm.purchasesdk.PurchaseCode;

public final class f extends r {
    public int B;
    public int[] X = new int[20];
    public int[] af = {0, 0, 0, 0};
    public int ag;
    public int ai = 100;
    public int al;
    public int am;
    private short[][] an;
    public boolean ao = true;
    public boolean ap = false;
    public int as;
    public int[] b = {0, 0};
    public boolean bH = false;
    public int bR;
    public int bS;
    public int bT = 0;
    public int bU;
    public boolean bX = false;
    public int bY;
    public int bZ;
    public int cA = 0;
    public int cB = 0;
    public int ca;
    public int cb;
    private short[][][] cd;
    private short[][][] cg;
    private int ch = 0;
    private int ci = 1;
    private int cj = -1;
    public boolean ck;
    public boolean cl;
    public boolean cm;
    public boolean cn;
    public boolean co;
    public boolean cp;
    public int[] cq = new int[4];
    public boolean cr = false;
    public int[] cs = {0, 0};
    public boolean ct = false;
    public int cu;
    public int cv;
    public boolean cw = false;
    public int cx = -1;
    public boolean cy = false;
    public boolean cz = false;
    public int[] d = {0, 0};
    public int m;
    public int o = 0;
    public int p = 0;
    public int q = 0;
    public int u = 100;
    public int w;
    public int x = 0;
    public int y;
    public int z;

    public f() {
    }

    public f(f fVar) {
        this.an = fVar.an;
        this.cd = fVar.cd;
        this.cg = fVar.cg;
    }

    public f(String str) {
        this.aa = new i[8];
        for (int i = 0; i < this.aa.length; i++) {
            this.aa[i] = F(new StringBuffer().append(str).append("Img").append(i).toString());
        }
        this.an = aS(new StringBuffer().append(str).append("Slice.mid").toString());
        this.cd = aT(new StringBuffer().append(str).append("Frame.mid").toString());
        this.cg = aT(new StringBuffer().append(str).append("Action.mid").toString());
    }

    public f(i[] iVarArr, String str) {
        this.an = aS(new StringBuffer().append(str).append("Slice.mid").toString());
        this.cd = aT(new StringBuffer().append(str).append("Frame.mid").toString());
        this.cg = aT(new StringBuffer().append(str).append("Action.mid").toString());
        for (int i = 0; i < iVarArr.length; i++) {
            if (iVarArr[i] == null) {
                iVarArr[i] = F(new StringBuffer().append(str).append("Img").append(i).toString());
            }
        }
    }

    public f(i[] iVarArr, String str, boolean z2, i[] iVarArr2) {
        this.an = aS(new StringBuffer().append(str).append("Slice.mid").toString());
        this.cd = aT(new StringBuffer().append(str).append("Frame.mid").toString());
        this.cg = aT(new StringBuffer().append(str).append("Action.mid").toString());
        if (z2) {
            for (int i = 0; i < iVarArr.length - 1; i++) {
                if (iVarArr[i] == null) {
                    iVarArr[i] = F(new StringBuffer().append(str).append("Img").append(i).toString());
                }
            }
            if (iVarArr2[0] == null) {
                iVarArr2[0] = F("/npc/specific/Img0");
            }
            iVarArr[iVarArr.length - 1] = iVarArr2[0];
            return;
        }
        for (int i2 = 0; i2 < iVarArr.length - 1; i2++) {
            if (iVarArr[i2] == null) {
                iVarArr[i2] = F(new StringBuffer().append(str).append("Img").append(i2).toString());
            }
        }
    }

    public f(i[] iVarArr, String str, boolean z2, i[] iVarArr2, int i) {
        this.an = aS(new StringBuffer().append(str).append("Slice.mid").toString());
        this.cd = aT(new StringBuffer().append(str).append("Frame.mid").toString());
        this.cg = aT(new StringBuffer().append(str).append("Action.mid").toString());
        int[][] iArr = {new int[]{15, -1, 0, 0}, new int[]{19, -1, 2}, new int[]{25, 0, -1}, new int[]{28, -1, -1, -1, 4}, new int[]{PurchaseCode.ORDER_OK, -1, 2}, new int[]{PurchaseCode.QUERY_OK, -1, 4}, new int[]{105, 0, -1}};
        String[][] strArr = {new String[]{"-1", "/npc/3/", "/npc/5/"}, new String[]{"-1", "/npc/Plane/"}, new String[]{"/npc/0/", "-1"}, new String[]{"-1", "-1", "-1", "/gun/"}, new String[]{b.SMS_RESULT_SEND_SUCCESS, "/npc/Plane/"}, new String[]{"-1", "/npc/startAnimation/"}, new String[]{"/npc/19/"}};
        if (z2) {
            for (int i2 = 0; i2 < iVarArr.length - 1; i2++) {
                if (iVarArr[i2] == null) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= iArr.length) {
                            break;
                        } else if (i != iArr[i3][0]) {
                            i3++;
                        } else if (i2 < iArr[i3].length - 1 && iArr[i3][i2 + 1] != -1) {
                            iVarArr[i2] = F(new StringBuffer().append(strArr[i3][i2]).append("Img").append(iArr[i3][i2 + 1]).toString());
                        } else if (iVarArr[i2] == null) {
                            iVarArr[i2] = F(new StringBuffer().append(str).append("Img").append(i2).toString());
                        }
                    }
                    if (iVarArr[i2] == null) {
                        iVarArr[i2] = F(new StringBuffer().append(str).append("Img").append(i2).toString());
                    }
                }
            }
            if (iVarArr2[0] == null) {
                iVarArr2[0] = F("/npc/specific/Img0");
            }
            iVarArr[iVarArr.length - 1] = iVarArr2[0];
            return;
        }
        for (int i4 = 0; i4 < iVarArr.length - 1; i4++) {
            if (iVarArr[i4] == null) {
                int i5 = 0;
                while (true) {
                    if (i5 >= iArr.length) {
                        break;
                    } else if (i != iArr[i5][0]) {
                        i5++;
                    } else if (i4 < iArr[i5].length - 1 && iArr[i5][i4 + 1] != -1) {
                        iVarArr[i4] = F(new StringBuffer().append(strArr[i5][i4]).append("Img").append(iArr[i5][i4 + 1]).toString());
                    } else if (iVarArr[i4] == null) {
                        iVarArr[i4] = F(new StringBuffer().append(str).append("Img").append(i4).toString());
                    }
                }
                if (iVarArr[i4] == null) {
                    iVarArr[i4] = F(new StringBuffer().append(str).append("Img").append(i4).toString());
                }
            }
        }
    }

    public final void a(int i, int i2) {
        if (i < 0 || i >= this.cg.length) {
            System.out.println(new StringBuffer("Have No ActionNum!").append(i).append(" action3D.length:").append(this.cg.length).toString());
        } else {
            this.p = i;
        }
        this.ch = i2;
        this.o = 0;
        this.ao = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0127 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.a.a.d.i[] r13, java.lang.String r14, com.a.a.d.h r15) {
        /*
            r12 = this;
            int r0 = r12.o
            if (r0 < 0) goto L_0x000f
            int r0 = r12.o
            short[][][] r1 = r12.cg
            int r2 = r12.p
            r1 = r1[r2]
            int r1 = r1.length
            if (r0 < r1) goto L_0x002d
        L_0x000f:
            int r0 = r12.ch
            r1 = 1
            if (r0 == r1) goto L_0x0038
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.StringBuffer r1 = new java.lang.StringBuffer
            java.lang.String r2 = "freamIndexOutOfTheArrary : "
            r1.<init>(r2)
            int r2 = r12.o
            java.lang.StringBuffer r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.print(r1)
            r0 = 0
            r12.o = r0
        L_0x002d:
            short[][][] r0 = r12.cd
            if (r0 != 0) goto L_0x0038
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.String r1 = "The FrameArrary didn't be init!"
            r0.print(r1)
        L_0x0038:
            short[][][] r0 = r12.cg     // Catch:{ RuntimeException -> 0x00d9 }
            int r1 = r12.p     // Catch:{ RuntimeException -> 0x00d9 }
            r0 = r0[r1]     // Catch:{ RuntimeException -> 0x00d9 }
            int r1 = r12.o     // Catch:{ RuntimeException -> 0x00d9 }
            r0 = r0[r1]     // Catch:{ RuntimeException -> 0x00d9 }
            r1 = 1
            short r11 = r0[r1]     // Catch:{ RuntimeException -> 0x00d9 }
            r0 = 0
            r10 = r0
        L_0x0047:
            short[][][] r0 = r12.cd
            r0 = r0[r11]
            int r0 = r0.length
            if (r10 >= r0) goto L_0x0127
            short[][][] r0 = r12.cd
            r0 = r0[r11]
            r0 = r0[r10]
            r1 = 0
            short r0 = r0[r1]
            short[][] r1 = r12.an
            r1 = r1[r0]
            r2 = 0
            short r1 = r1[r2]
            r1 = r13[r1]
            if (r1 != 0) goto L_0x0089
            short[][] r1 = r12.an
            r1 = r1[r0]
            r2 = 0
            short r1 = r1[r2]
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.StringBuffer r2 = r2.append(r14)
            java.lang.String r3 = "Img"
            java.lang.StringBuffer r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            short[][] r3 = r12.an
            r3 = r3[r0]
            r4 = 0
            short r3 = r3[r4]
            com.a.a.d.i r2 = b(r2, r3)
            r13[r1] = r2
        L_0x0089:
            short[][][] r1 = r12.cd
            r1 = r1[r11]
            r1 = r1[r10]
            r2 = 1
            short r1 = r1[r2]
            int r2 = r12.ed
            int r1 = r1 + r2
            short[][][] r2 = r12.cd
            r2 = r2[r11]
            r2 = r2[r10]
            r3 = 2
            short r2 = r2[r3]
            int r3 = r12.ei
            int r2 = r2 + r3
            short[][] r3 = r12.an
            r3 = r3[r0]
            r4 = 0
            short r3 = r3[r4]
            r3 = r13[r3]
            short[][] r4 = r12.an
            r4 = r4[r0]
            r5 = 1
            short r4 = r4[r5]
            short[][] r5 = r12.an
            r5 = r5[r0]
            r6 = 2
            short r5 = r5[r6]
            short[][] r6 = r12.an
            r6 = r6[r0]
            r7 = 3
            short r6 = r6[r7]
            short[][] r7 = r12.an
            r0 = r7[r0]
            r7 = 4
            short r7 = r0[r7]
            short[][][] r0 = r12.cd
            r0 = r0[r11]
            r0 = r0[r10]
            r8 = 3
            short r8 = r0[r8]
            r9 = 0
            r0 = r15
            a(r0, r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            r10 = r0
            goto L_0x0047
        L_0x00d9:
            r0 = move-exception
            r0.printStackTrace()
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            java.lang.String r3 = "strPath: "
            r2.<init>(r3)
            java.lang.StringBuffer r2 = r2.append(r14)
            java.lang.String r2 = r2.toString()
            r1.println(r2)
            java.io.PrintStream r1 = java.lang.System.out
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            java.lang.StringBuffer r0 = r2.append(r0)
            java.lang.String r2 = "frame_id Errors : 0"
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " "
            java.lang.StringBuffer r0 = r0.append(r2)
            int r2 = r12.p
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " "
            java.lang.StringBuffer r0 = r0.append(r2)
            int r2 = r12.o
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r2 = " 1"
            java.lang.StringBuffer r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            r1.println(r0)
        L_0x0127:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: game.f.a(com.a.a.d.i[], java.lang.String, com.a.a.d.h):void");
    }

    public final void c() {
        this.eN++;
        if (this.cg[this.p][this.o][0] <= 1 || this.eN % (this.cg[this.p][this.o][0] * this.ci) == 0) {
            switch (this.ch) {
                case 0:
                    if (this.o < this.cg[this.p].length - 1) {
                        this.o++;
                        this.ao = false;
                        return;
                    }
                    this.ao = true;
                    this.o = 0;
                    return;
                case 1:
                default:
                    return;
                case 2:
                    if (this.o < this.cg[this.p].length - 1) {
                        this.o++;
                        this.ao = false;
                        return;
                    }
                    this.ao = true;
                    return;
                case 3:
                    if (this.o < this.cg[this.p].length) {
                        this.o++;
                        return;
                    } else {
                        a(this.cj, 0);
                        return;
                    }
                case 4:
                    if (this.o >= this.cg[this.p].length - 1 || this.ao) {
                        this.o = 0;
                        this.ao = true;
                        return;
                    }
                    this.o++;
                    this.ao = false;
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void c(int i, int i2) {
        this.bY = i;
        this.bZ = i2;
    }
}
