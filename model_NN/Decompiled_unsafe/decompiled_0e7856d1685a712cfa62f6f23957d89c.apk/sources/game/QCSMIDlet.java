package game;

import chinaMobile.MIDlet;
import chinaMobile.b;
import com.a.a.d.e;
import com.a.a.d.f;

public class QCSMIDlet extends MIDlet {
    public g bP;
    private b bQ = new b(this, this.o, this.p, new String[]{"是否开启声音?", "关闭声音可以提高游戏性能"});
    private int o = -6;
    private int p = -7;

    public static boolean F() {
        return b.ao;
    }

    public final void D() {
        if (this.bP != null) {
            this.bP.H();
        }
    }

    public final void E() {
        if (this.bP != null) {
            this.bP.G();
        }
    }

    public final void a(f fVar) {
        e.a(this).a(fVar);
    }

    public final void b() {
        this.bQ.e();
        this.bQ = null;
        this.bP = new g(this);
        new Thread(this.bP).start();
        a(this.bP);
    }

    public final void t() {
        aX();
    }
}
