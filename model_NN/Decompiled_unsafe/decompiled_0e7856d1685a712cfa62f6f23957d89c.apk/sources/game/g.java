package game;

import com.a.a.d.b;
import com.a.a.d.h;
import com.a.a.d.i;
import com.nokia.mid.ui.DirectGraphics;
import mm.purchasesdk.PurchaseCode;
import org.meteoroid.plugin.vd.DefaultVirtualDevice;

public final class g extends b implements Runnable {
    private int B;
    private int[] X;
    private int ag;
    private int ai = q.bN;
    private com.a.a.d.g aj = q.aj;
    private int al;
    private int am;
    public boolean ao = false;
    private String[] aq = {""};
    private int as;
    private int[] b;
    private int bN;
    private int bO;
    private int bR;
    private int bS;
    private int bT;
    private int bU;
    private int bY;
    private int bZ;
    private i bs = i.m(640, 360);
    QCSMIDlet cC;
    private long cD;
    private long cE;
    private h cF = this.bs.aU();
    h cG;
    private int cH;
    private int ca;
    private int cb;
    i ce;
    private int m;
    int o;
    int p;
    public int q;
    private int u = q.bO;
    private int w;
    private int x;

    public g(QCSMIDlet qCSMIDlet) {
        new int[1][0] = 0;
        this.m = 0;
        byte[][] bArr = {new byte[]{0, 18, 20, 22}, new byte[]{0, 12, 13, 19}, new byte[]{0, 4, 18, 20}};
        this.as = 640;
        this.bN = DirectGraphics.ROTATE_180;
        this.bO = 21;
        this.o = 0;
        this.cH = 200;
        this.bR = 10;
        this.bS = 295;
        this.bT = 164;
        this.bU = PurchaseCode.BILL_LICENSE_ERROR;
        this.bY = 300;
        this.X = new int[]{6, 6, 6, 40, 90, 139};
        this.bZ = 10;
        this.ca = 52;
        this.p = 25;
        this.q = 0;
        this.cb = 0;
        this.x = 0;
        this.b = new int[]{0, 0, 0};
        this.cC = qCSMIDlet;
        this.ag = 100;
        this.ce = new i(this);
    }

    private static void a(int i, int i2, h hVar, int i3, int i4, i[] iVarArr) {
        int[] iArr = {0, 0};
        int[] iArr2 = {0, 0, 0};
        iArr[0] = i / 10;
        iArr[1] = i % 10;
        iArr2[0] = i2 / 100;
        iArr2[1] = (i2 % 100) / 10;
        iArr2[2] = i2 % 10;
        for (int i5 = 0; i5 <= 5; i5++) {
            switch (i5) {
                case 0:
                case 1:
                    hVar.a(iVarArr[iArr[i5]], (i5 * 13) + i3, i4, 20);
                    break;
                case 2:
                    hVar.a(iVarArr[10], (i5 * 13) + i3, i4, 20);
                    break;
                case 3:
                case 4:
                case 5:
                    hVar.a(iVarArr[iArr2[i5 - 3]], ((i5 * 13) + i3) - 4, i4, 20);
                    break;
            }
        }
    }

    public static void a(int i, h hVar) {
        hVar.setColor(i);
        hVar.d(0, 0, 640, 360);
    }

    private static void a(int i, h hVar, int i2, int i3, i[] iVarArr) {
        int[] iArr = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        int i4 = 0;
        while (true) {
            if (i4 <= 10) {
                if (i / 10 <= 0) {
                    iArr[i4] = i % 10;
                    break;
                }
                iArr[i4] = i % 10;
                i /= 10;
                i4++;
            } else {
                i4 = 0;
                break;
            }
        }
        int i5 = 0;
        for (int i6 = i4; i6 >= 0; i6--) {
            hVar.a(iVarArr[iArr[i6]], (iVarArr[0].getWidth() * i5) + i2, i3, 20);
            i5++;
        }
    }

    private void a(h hVar, int i) {
        int i2;
        if (this.q <= 6) {
            hVar.setColor(0);
            switch (i) {
                case 0:
                    i2 = 6 - this.q;
                    break;
                case 1:
                    i2 = this.q;
                    break;
                default:
                    i2 = 0;
                    break;
            }
            for (int i3 = 0; i3 < 10; i3++) {
                hVar.d(i3 * 64, 0, 64 - ((i2 * 64) / 6), 360);
            }
            this.q++;
        }
        if (this.q > 6) {
            hVar.setColor(0);
            hVar.d(0, 0, 640, 360);
            hVar.setColor(16777215);
            a(i.bf[0][this.m], 25, 30, this.u * 12, this.ai * 10, 11, this.ce.dA);
        }
    }

    private static void a(h hVar, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        int i8 = i5 - i3;
        int i9 = i & PurchaseCode.AUTH_INVALID_APP;
        int i10 = (i >> 8) & PurchaseCode.AUTH_INVALID_APP;
        int i11 = (i >> 16) & PurchaseCode.AUTH_INVALID_APP;
        int i12 = i2 & PurchaseCode.AUTH_INVALID_APP;
        int i13 = i12 - i9;
        int i14 = ((i2 >> 8) & PurchaseCode.AUTH_INVALID_APP) - i10;
        int i15 = ((i2 >> 16) & PurchaseCode.AUTH_INVALID_APP) - i11;
        for (int i16 = i3; i16 < i5; i16 += 2) {
            int i17 = i16 - i3;
            hVar.setColor(((((i17 * i15) / i8) + i11) << 16) + ((i13 * i17) / i8) + i9 + ((((i14 * i17) / i8) + i10) << 8));
            hVar.d(i16, i4, i6, i7);
        }
    }

    private static void a(h hVar, i iVar, int i, int i2) {
        a(0, hVar);
        hVar.a(iVar, i, i2, 17);
    }

    private static void a(h hVar, String str, int i, int i2, int i3, int i4) {
        hVar.setColor(i3);
        hVar.a(str, i - 1, i2 - 1, 17);
        hVar.a(str, i - 1, i2, 17);
        hVar.a(str, i - 1, i2 - 1, 17);
        hVar.a(str, i, i2 - 1, 17);
        hVar.a(str, i, i2 + 1, 17);
        hVar.a(str, i + 1, i2 - 1, 17);
        hVar.a(str, i + 1, i2, 17);
        hVar.a(str, i + 1, i2 + 1, 17);
        hVar.setColor(i4);
        hVar.a(str, i, i2, 17);
    }

    private static void a(h hVar, String str, int i, int i2, i iVar) {
        int length = str.length();
        int width = iVar.getWidth() / 11;
        int height = iVar.getHeight();
        for (int i3 = 0; i3 < length; i3++) {
            q.a(hVar, (i3 * width) + i + 2, i2, iVar, (str.charAt(i3) - '/') * width, 0, width, height, 0, 0);
        }
    }

    private void a(String str, int i, int i2, int i3, int i4, int i5, int[] iArr) {
        String[] strArr = new String[(str.length() % i5 == 0 ? str.length() / i5 : (str.length() / i5) + 1)];
        int i6 = 0;
        for (int i7 = 0; i7 < strArr.length; i7++) {
            if (i5 > str.length() - i6) {
                strArr[i7] = str.substring(i6, str.length());
            } else {
                strArr[i7] = str.substring(i6, i6 + i5);
            }
            i6 += i5;
        }
        int length = this.ai * strArr.length;
        this.cG.e(i, i2, i3, i4);
        for (int i8 = 0; i8 < strArr.length; i8++) {
            if (length < i4) {
                this.cG.a(strArr[i8], i, ((i4 - (strArr.length * this.ai)) / 2) + i2 + (this.ai * i8), 0);
            } else {
                this.cG.a(strArr[i8], i, ((i2 + i4) - iArr[1]) + (this.ai * i8), 0);
            }
        }
        this.cG.e(0, 0, 640, 360);
        if (length < i4) {
            iArr[1] = 0;
        } else {
            iArr[1] = iArr[1] + 2;
        }
    }

    private void a(String str, int i, int i2, int[] iArr) {
        String[] strArr = new String[(str.length() % 10 == 0 ? str.length() / 10 : (str.length() / 10) + 1)];
        int i3 = 0;
        for (int i4 = 0; i4 < strArr.length; i4++) {
            if (10 > str.length() - i3) {
                strArr[i4] = str.substring(i3, str.length());
            } else {
                strArr[i4] = str.substring(i3, i3 + 10);
            }
            i3 += 10;
        }
        this.cG.e(0, i, 640, i2);
        for (int i5 = 0; i5 < strArr.length; i5++) {
            this.cG.a(strArr[i5], 320, ((i + i2) - iArr[1]) + (this.ai * i5), 17);
        }
        this.cG.e(0, 0, 640, 360);
        if (iArr[1] < i2) {
            iArr[1] = iArr[1] + 2;
        }
    }

    private boolean a(h hVar, String[] strArr) {
        a(0, hVar);
        hVar.setColor(16777215);
        int length = strArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = strArr[i].length();
        }
        if (this.x != 5) {
            this.x++;
        } else if (this.cb == 1) {
            int[] iArr2 = this.b;
            iArr2[0] = iArr2[0] + 1;
            if (this.b[0] >= iArr[this.b[1]]) {
                this.x = 0;
                this.b[0] = 0;
                int[] iArr3 = this.b;
                iArr3[1] = iArr3[1] + 1;
                if (this.b[1] >= length) {
                    this.b[1] = 0;
                    int[] iArr4 = this.b;
                    iArr4[2] = iArr4[2] + 1;
                }
            }
            this.cb = 0;
        } else {
            this.cb++;
        }
        for (int i2 = 0; i2 <= this.b[1]; i2++) {
            if (i2 < this.b[1]) {
                hVar.a(strArr[i2], 25, (i2 * 23) + 60, 0);
            } else {
                hVar.a(strArr[i2], 0, this.b[0], 25, (i2 * 23) + 60, 0);
            }
        }
        if (this.b[2] == 0) {
            return false;
        }
        for (int i3 = 0; i3 < length; i3++) {
            hVar.a(strArr[i3], 25, (i3 * 23) + 60, 0);
        }
        this.b[2] = 0;
        return true;
    }

    private void b(int i, h hVar) {
        for (int i2 = 0; i2 < this.ce.eE.length; i2++) {
            if (this.ce.eE[i2].ca == i && this.ce.eE[i2].cr && this.ce.eE[i2].ex - this.ce.cM[0].ed > (-this.ce.eE[i2].bY) && this.ce.eE[i2].ex - this.ce.cM[0].ed < 640) {
                this.ce.eE[i2].a(this.ce.rO[2], i.rP[2], hVar);
            }
        }
    }

    public static void b(h hVar) {
        String[][] strArr = {new String[]{"开启音乐？", "开", "关"}};
        hVar.setColor(0);
        hVar.d(-80, 0, 720, 360);
        hVar.setColor(15972160);
        hVar.a(strArr[0][0], 320, (int) DirectGraphics.ROTATE_180, 33);
        String str = strArr[0][1];
        String str2 = strArr[0][2];
        hVar.setColor(15972160);
        hVar.a(str, 5, 355, 36);
        hVar.a(str2, 635, 355, 40);
    }

    private static void b(h hVar, i iVar, int i, int i2) {
        hVar.a(iVar, i, i2, 20);
    }

    private static void b(h hVar, String[] strArr) {
        a(0, hVar);
        hVar.setColor(16777215);
        int length = strArr.length;
        int[] iArr = new int[length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = strArr[i].length();
        }
        for (int i2 = 0; i2 < length; i2++) {
            hVar.a(strArr[i2], 25, (i2 * 23) + 60, 0);
        }
        hVar.a("点此继续", 320, 320, 17);
    }

    private void c(int i, h hVar) {
        for (int i2 = 0; i2 < this.ce.eF.length; i2++) {
            if (this.ce.eF[i2].ca == i && this.ce.eF[i2].cr && this.ce.eF[i2].ex - this.ce.cM[0].ed > (-this.ce.eF[i2].bY) && this.ce.eF[i2].ex - this.ce.cM[0].ed < 640) {
                this.ce.eF[i2].a(this.ce.rO[12], i.rP[12], hVar);
            }
        }
    }

    private void c(h hVar) {
        hVar.setColor(65280);
        for (int i = 0; i < this.ce.ee.length; i++) {
            if (i != 0) {
                hVar.d(this.ce.ee[i][0], this.ce.ee[i][1], this.ce.ee[i][2], this.ce.ee[i][3]);
            }
        }
    }

    private void e(h hVar) {
        for (int i = 0; i < this.ce.dF.length; i++) {
            if (!(this.ce.dF[i] == null || i == 11 || i == 21)) {
                hVar.a(this.ce.dF[i], this.ce.dE[i << 1], this.ce.dE[(i << 1) + 1], 20);
            }
        }
        if (this.ce.cN[0].cr) {
            this.ce.cN[0].a(this.ce.rO[12], i.rP[12], hVar);
        }
        if (this.ce.bs != null) {
            hVar.a(this.ce.bs, this.ce.ei, this.ce.ej, 20);
        }
    }

    private void f(h hVar) {
        if (this.ce.cu == 1) {
            for (int i = 0; i < this.ce.cN.length; i++) {
                if (this.ce.cN[i].cr) {
                    this.ce.cN[i].a(this.ce.rO[13], i.rP[13], hVar);
                }
            }
            this.o++;
            if (this.o == 20) {
                this.o = 0;
            }
            if (this.o % 4 > 2) {
                a(hVar, "点此商店", 320, 360 - this.bO, 2633781, 15264459);
                return;
            }
            a(hVar, "点此商店", 320, 360 - this.bO, 2633781, 11120567);
        }
    }

    private void h(h hVar) {
        int[][] iArr = {new int[]{0, 18, 21, 0}, new int[]{1, 17, 21, 1}, new int[]{2, 20, -1, 2}, new int[]{3, 7, -1, 3}};
        int[][] iArr2 = {new int[]{14, 25, -1, 6, 0}, new int[]{8, 23, 25, 0, -1}, new int[]{9, 24, 25, 1, -1}, new int[]{12, 23, 25, 4, 0}, new int[]{13, 24, 25, 5, 0}, new int[]{10, 23, 25, 2, 0}, new int[]{11, 24, 25, 3, 0}, new int[]{15, 25, -1, 7, 0}, new int[]{16, 25, -1, 8, 0}, new int[]{26, 25, -1, 9, 0}};
        if (this.ce.dt[4] != 0) {
            if (this.ce.dt[4] == 1) {
                for (int i = this.ce.dt[3]; i < this.ce.dt[3] + 4; i++) {
                    hVar.a(this.ce.dJ[iArr2[i][0]], this.ce.du[i << 1] + 150, this.ce.du[(i << 1) + 1] + 7, 20);
                    hVar.a(this.ce.dJ[iArr2[i][1]], this.ce.du[i << 1] + 155 + this.ce.dJ[iArr2[i][0]].getWidth() + 7, this.ce.du[(i << 1) + 1] + 7, 20);
                    if (iArr2[i][2] >= 0) {
                        hVar.a(this.ce.dJ[iArr2[i][2]], this.ce.du[i << 1] + 155 + this.ce.dJ[iArr2[i][0]].getWidth() + 7 + this.ce.dJ[iArr2[i][1]].getWidth(), this.ce.du[(i << 1) + 1] + 7, 20);
                    }
                    if (iArr2[i][4] < 0 && i.ac[1][iArr2[i][3]] < 0) {
                        hVar.a(this.ce.dJ[27], 360, this.ce.du[(i << 1) + 1] + 8, 20);
                    } else if (i.ac[1][iArr2[i][3]] < 0) {
                        hVar.a(this.ce.dJ[27], 360, this.ce.du[(i << 1) + 1] + 8, 20);
                    } else {
                        a(i.ac[1][iArr2[i][3]], hVar, 367, this.ce.du[(i << 1) + 1] + 14, this.ce.dK);
                    }
                }
                switch (this.ce.dt[1]) {
                    case 0:
                        if (this.ce.sc[8] > 0) {
                            hVar.a(this.ce.dG[this.ce.sc[8] + 15], this.ce.sc[8] == 4 ? 524 : 528, this.ce.sc[8] == 4 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[31], 528, 94, 20);
                            break;
                        }
                        break;
                    case 1:
                        if (this.ce.sc[1] > 0) {
                            hVar.a(this.ce.sc[1] < 6 ? this.ce.dG[this.ce.sc[1] + 14] : this.ce.dG[19], this.ce.sc[1] >= 5 ? 524 : 528, this.ce.sc[1] >= 5 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[20], 529, 92, 20);
                            break;
                        }
                        break;
                    case 2:
                        if (this.ce.sc[1] > 0) {
                            hVar.a(this.ce.dG[20], 521, 90, 20);
                            break;
                        }
                        break;
                    case 3:
                        if (this.ce.sc[11] > 0) {
                            hVar.a(this.ce.sc[11] < 6 ? this.ce.dG[this.ce.sc[11] + 14] : this.ce.dG[19], this.ce.sc[11] >= 5 ? 524 : 528, this.ce.sc[11] >= 5 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[22], 530, 98, 20);
                            break;
                        }
                        break;
                    case 4:
                        if (this.ce.sc[11] > 0) {
                            hVar.a(this.ce.dG[22], 530, 98, 20);
                            break;
                        }
                        break;
                    case 5:
                        if (this.ce.sc[6] > 0) {
                            hVar.a(this.ce.sc[6] < 6 ? this.ce.dG[this.ce.sc[6] + 14] : this.ce.dG[19], this.ce.sc[6] >= 5 ? 524 : 528, this.ce.sc[6] >= 5 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[21], 520, 96, 20);
                            break;
                        }
                        break;
                    case 6:
                        if (this.ce.sc[6] > 0) {
                            hVar.a(this.ce.dG[21], 530, 96, 20);
                            break;
                        }
                        break;
                    case 7:
                        if (this.ce.sc[3] > 0) {
                            hVar.a(this.ce.dG[this.ce.sc[3] + 15], this.ce.sc[3] >= 4 ? 524 : 528, this.ce.sc[3] >= 4 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[23], 529, 96, 20);
                            break;
                        }
                        break;
                    case 8:
                        if (this.ce.sc[9] > 0) {
                            hVar.a(this.ce.dG[this.ce.sc[9] + 15], this.ce.sc[9] >= 4 ? 524 : 528, this.ce.sc[9] >= 4 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[32], 528, 96, 20);
                            break;
                        }
                        break;
                    case 9:
                        if (this.ce.sc[13] > 0) {
                            hVar.a(this.ce.dG[this.ce.sc[13] + 15], this.ce.sc[13] >= 4 ? 524 : 528, this.ce.sc[13] >= 4 ? 96 : 94, 20);
                            hVar.a(this.ce.dG[33], 528, 96, 20);
                            break;
                        }
                        break;
                }
            }
        } else {
            for (int i2 = this.ce.dt[2]; i2 < this.ce.dt[2] + 4; i2++) {
                hVar.a(this.ce.dJ[iArr[i2][0]], this.ce.du[i2 << 1] + 150, this.ce.du[(i2 << 1) + 1] + 7, 20);
                hVar.a(this.ce.dJ[iArr[i2][1]], this.ce.du[i2 << 1] + 155 + this.ce.dJ[0].getWidth() + 7, this.ce.du[(i2 << 1) + 1] + 7, 20);
                if (iArr[i2][2] >= 0) {
                    hVar.a(this.ce.dJ[iArr[i2][2]], this.ce.du[i2 << 1] + 155 + this.ce.dJ[0].getWidth() + 7 + this.ce.dJ[iArr[i2][1]].getWidth(), this.ce.du[(i2 << 1) + 1] + 7, 20);
                }
                a(i.ac[0][iArr[i2][3]], hVar, 367, this.ce.du[(i2 << 1) + 1] + 12, this.ce.dK);
            }
        }
        a(this.ce.bO, hVar, (int) PurchaseCode.AUTH_FROZEN, 311, this.ce.dK);
        if (this.ce.ry[0] > 0) {
            a(hVar, "点此购买金钱", 320, 360 - (this.ai + 4), 3548957, 16771413);
        }
    }

    public final void G() {
        this.ce.e();
    }

    public final void H() {
        this.ce.au();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x0307  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x0366  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0425  */
    /* JADX WARNING: Removed duplicated region for block: B:192:0x049b  */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x05bc  */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x066d  */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0692  */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x06b7  */
    /* JADX WARNING: Removed duplicated region for block: B:221:0x06eb  */
    /* JADX WARNING: Removed duplicated region for block: B:254:0x0825  */
    /* JADX WARNING: Removed duplicated region for block: B:269:0x08c3  */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x0974  */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x09f7  */
    /* JADX WARNING: Removed duplicated region for block: B:331:0x0a65  */
    /* JADX WARNING: Removed duplicated region for block: B:342:0x0ab4  */
    /* JADX WARNING: Removed duplicated region for block: B:350:0x0ae9 A[FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:355:0x0b0f  */
    /* JADX WARNING: Removed duplicated region for block: B:360:0x0b37  */
    /* JADX WARNING: Removed duplicated region for block: B:371:0x0b88  */
    /* JADX WARNING: Removed duplicated region for block: B:386:0x0bf5  */
    /* JADX WARNING: Removed duplicated region for block: B:391:0x0c1b  */
    /* JADX WARNING: Removed duplicated region for block: B:399:0x0c4a  */
    /* JADX WARNING: Removed duplicated region for block: B:410:0x0c9b  */
    /* JADX WARNING: Removed duplicated region for block: B:424:0x0d41  */
    /* JADX WARNING: Removed duplicated region for block: B:435:0x0d92  */
    /* JADX WARNING: Removed duplicated region for block: B:443:0x0dcf  */
    /* JADX WARNING: Removed duplicated region for block: B:461:0x0e51  */
    /* JADX WARNING: Removed duplicated region for block: B:479:0x0ed3  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0175  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a(com.a.a.d.h r13) {
        /*
            r12 = this;
            r11 = 50
            r9 = 1
            r10 = 6
            r8 = 0
            monitor-enter(r12)
            com.a.a.d.h r0 = r12.cG     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x000c
            r12.cG = r13     // Catch:{ all -> 0x00f8 }
        L_0x000c:
            com.a.a.d.h r0 = r12.cF     // Catch:{ all -> 0x00f8 }
            com.a.a.d.g r1 = r12.aj     // Catch:{ all -> 0x00f8 }
            r0.a(r1)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r1 = r12.cF     // Catch:{ all -> 0x00f8 }
            r0.b(r1)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x0028
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0067
        L_0x0028:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 66
            if (r0 != r1) goto L_0x0037
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 2
            if (r0 == r1) goto L_0x0067
        L_0x0037:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 19
            if (r0 == r1) goto L_0x0067
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 72
            if (r0 == r1) goto L_0x0067
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 71
            if (r0 == r1) goto L_0x0067
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 69
            if (r0 == r1) goto L_0x0067
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 52
            if (r0 == r1) goto L_0x0067
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 46
            if (r0 != r1) goto L_0x00ec
        L_0x0067:
            boolean r0 = game.i.cr     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x00ec
            r0 = r9
        L_0x006c:
            if (r0 == 0) goto L_0x00ef
            r0 = 84
            r12.w = r0     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r12.B = r0     // Catch:{ all -> 0x00f8 }
            r0 = 483(0x1e3, float:6.77E-43)
            r12.al = r0     // Catch:{ all -> 0x00f8 }
            r0 = 320(0x140, float:4.48E-43)
            r12.am = r0     // Catch:{ all -> 0x00f8 }
            int r0 = r12.w     // Catch:{ all -> 0x00f8 }
            r1 = 0
            int r2 = r12.al     // Catch:{ all -> 0x00f8 }
            int r3 = r12.am     // Catch:{ all -> 0x00f8 }
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i r0 = r12.bs     // Catch:{ all -> 0x00f8 }
            r1 = 80
            r2 = 0
            r3 = 0
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r1 = 0
            r2 = 640(0x280, float:8.97E-43)
            r3 = 360(0x168, float:5.04E-43)
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0099:
            boolean r0 = game.i.cr     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x0164
            com.a.a.d.h r0 = r12.cG     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r2 = 0
            r3 = 640(0x280, float:8.97E-43)
            r4 = 360(0x168, float:5.04E-43)
            r0.e(r1, r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x00bd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 4
            if (r0 != r1) goto L_0x00bd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0158
        L_0x00bd:
            r0 = r8
        L_0x00be:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.cu     // Catch:{ all -> 0x00f8 }
            if (r0 >= r1) goto L_0x0158
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.o     // Catch:{ all -> 0x00f8 }
            r2 = 20
            if (r1 != r2) goto L_0x00fb
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.B     // Catch:{ all -> 0x00f8 }
            r2 = 5
            if (r1 != r2) goto L_0x00fb
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.cu     // Catch:{ all -> 0x00f8 }
            r2 = 3
            if (r1 != r2) goto L_0x00fb
            if (r0 == r9) goto L_0x00e9
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            r1 = r1[r0]     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r2 = r12.cG     // Catch:{ all -> 0x00f8 }
            r3 = 0
            r4 = 0
            r1.a(r2, r3, r4)     // Catch:{ all -> 0x00f8 }
        L_0x00e9:
            int r0 = r0 + 1
            goto L_0x00be
        L_0x00ec:
            r0 = r8
            goto L_0x006c
        L_0x00ef:
            com.a.a.d.i r0 = r12.bs     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r2 = 0
            r3 = 0
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0099
        L_0x00f8:
            r0 = move-exception
            monitor-exit(r12)
            throw r0
        L_0x00fb:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.o     // Catch:{ all -> 0x00f8 }
            r2 = 60
            if (r1 != r2) goto L_0x012f
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.cu     // Catch:{ all -> 0x00f8 }
            if (r1 != r9) goto L_0x011c
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            r1 = r1[r0]     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r2 = r12.cG     // Catch:{ all -> 0x00f8 }
            r3 = 0
            r4 = 0
            r1.a(r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r1 = r12.cG     // Catch:{ all -> 0x00f8 }
            r12.e(r1)     // Catch:{ all -> 0x00f8 }
            goto L_0x00e9
        L_0x011c:
            com.a.a.d.h r1 = r12.cG     // Catch:{ all -> 0x00f8 }
            r12.e(r1)     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            r1 = r1[r0]     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r2 = r12.cG     // Catch:{ all -> 0x00f8 }
            r3 = 0
            r4 = 0
            r1.a(r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            goto L_0x00e9
        L_0x012f:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            if (r1 == 0) goto L_0x00e9
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            r1 = r1[r0]     // Catch:{ all -> 0x00f8 }
            com.a.a.d.h r2 = r12.cG     // Catch:{ all -> 0x00f8 }
            r3 = 0
            r4 = 0
            r1.a(r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.o     // Catch:{ all -> 0x00f8 }
            r2 = 64
            if (r1 != r2) goto L_0x00e9
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.cu     // Catch:{ all -> 0x00f8 }
            if (r1 <= r9) goto L_0x00e9
            if (r0 != 0) goto L_0x00e9
            com.a.a.d.h r1 = r12.cG     // Catch:{ all -> 0x00f8 }
            r12.h(r1)     // Catch:{ all -> 0x00f8 }
            goto L_0x00e9
        L_0x0158:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.cn     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0166
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            a(r0, r13)     // Catch:{ all -> 0x00f8 }
        L_0x0164:
            monitor-exit(r12)
            return
        L_0x0166:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            switch(r0) {
                case 18: goto L_0x08dd;
                case 19: goto L_0x01b3;
                case 20: goto L_0x02b6;
                case 46: goto L_0x08ed;
                case 52: goto L_0x016d;
                case 59: goto L_0x08f2;
                case 64: goto L_0x08e2;
                case 65: goto L_0x016d;
                default: goto L_0x016d;
            }     // Catch:{ all -> 0x00f8 }
        L_0x016d:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 65
            if (r0 != r1) goto L_0x091c
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.y     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x091c
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.y     // Catch:{ all -> 0x00f8 }
            r1 = 2
            if (r0 > r1) goto L_0x090d
            java.lang.String[][] r0 = game.i.bl     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.y     // Catch:{ all -> 0x00f8 }
            int r1 = 2 - r1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            boolean r0 = r12.a(r13, r0)     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0164
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r0.y     // Catch:{ all -> 0x00f8 }
            int r1 = r1 + 4
            r0.y = r1     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r12.cb = r0     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r12.cb = r0     // Catch:{ all -> 0x00f8 }
            int[] r0 = r12.b     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r2 = 0
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
            int[] r0 = r12.b     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r2 = 0
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
            int[] r0 = r12.b     // Catch:{ all -> 0x00f8 }
            r1 = 2
            r2 = 0
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
            goto L_0x0164
        L_0x01b3:
            r0 = 0
            a(r0, r13)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 0
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 25
            if (r0 >= r1) goto L_0x01f8
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 0
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x01f8
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            int r0 = r0.length     // Catch:{ all -> 0x00f8 }
            r1 = 76
            if (r0 < r1) goto L_0x01f8
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 75
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r0.c()     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 75
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r1 = r1.rO     // Catch:{ all -> 0x00f8 }
            r2 = 18
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r2 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r3 = 18
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            r0.a(r1, r2, r13)     // Catch:{ all -> 0x00f8 }
        L_0x01f8:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 0
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0211
            r0 = 10
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 0
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            long r1 = (long) r1     // Catch:{ all -> 0x00f8 }
            int r0 = defpackage.q.b(r0, r1)     // Catch:{ all -> 0x00f8 }
            r12.m = r0     // Catch:{ all -> 0x00f8 }
        L_0x0211:
            java.lang.String r0 = ""
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 0
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 % 4
            switch(r1) {
                case 0: goto L_0x025c;
                case 1: goto L_0x0260;
                case 2: goto L_0x0264;
                case 3: goto L_0x0268;
                default: goto L_0x021f;
            }     // Catch:{ all -> 0x00f8 }
        L_0x021f:
            r2 = r0
        L_0x0220:
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String[][] r0 = game.i.bf     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r12.m     // Catch:{ all -> 0x00f8 }
            r3 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r3.length()     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 10
            if (r0 != 0) goto L_0x026c
            int r0 = r3.length()     // Catch:{ all -> 0x00f8 }
            int r0 = r0 / 10
        L_0x023d:
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ all -> 0x00f8 }
            r0 = r8
            r1 = r8
        L_0x0241:
            int r5 = r4.length     // Catch:{ all -> 0x00f8 }
            if (r0 >= r5) goto L_0x027e
            r5 = 10
            int r6 = r3.length()     // Catch:{ all -> 0x00f8 }
            int r6 = r6 - r1
            if (r5 <= r6) goto L_0x0275
            int r5 = r3.length()     // Catch:{ all -> 0x00f8 }
            java.lang.String r5 = r3.substring(r1, r5)     // Catch:{ all -> 0x00f8 }
            r4[r0] = r5     // Catch:{ all -> 0x00f8 }
        L_0x0257:
            int r1 = r1 + 10
            int r0 = r0 + 1
            goto L_0x0241
        L_0x025c:
            java.lang.String r0 = "加载中"
            r2 = r0
            goto L_0x0220
        L_0x0260:
            java.lang.String r0 = "加载中 ."
            r2 = r0
            goto L_0x0220
        L_0x0264:
            java.lang.String r0 = "加载中 . ."
            r2 = r0
            goto L_0x0220
        L_0x0268:
            java.lang.String r0 = "加载中 . . ."
            r2 = r0
            goto L_0x0220
        L_0x026c:
            int r0 = r3.length()     // Catch:{ all -> 0x00f8 }
            int r0 = r0 / 10
            int r0 = r0 + 1
            goto L_0x023d
        L_0x0275:
            int r5 = r1 + 10
            java.lang.String r5 = r3.substring(r1, r5)     // Catch:{ all -> 0x00f8 }
            r4[r0] = r5     // Catch:{ all -> 0x00f8 }
            goto L_0x0257
        L_0x027e:
            r0 = r8
        L_0x027f:
            int r1 = r4.length     // Catch:{ all -> 0x00f8 }
            if (r0 >= r1) goto L_0x0294
            r1 = r4[r0]     // Catch:{ all -> 0x00f8 }
            r3 = 50
            int r5 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r5 = r5 * r0
            int r5 = r5 + 2
            int r5 = r5 + 30
            r6 = 0
            r13.a(r1, r3, r5, r6)     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            goto L_0x027f
        L_0x0294:
            int r0 = r12.u     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * 6
            int r0 = 640 - r0
            int r0 = r0 + -160
            int r1 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r1 = 360 - r1
            int r1 = r1 + -40
            r3 = 0
            r13.a(r2, r0, r1, r3)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 0
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            if (r0 > 0) goto L_0x016d
            r0 = 10
            defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x02b6:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            switch(r0) {
                case 5: goto L_0x03b8;
                case 6: goto L_0x03c0;
                default: goto L_0x02bd;
            }     // Catch:{ all -> 0x00f8 }
        L_0x02bd:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 4
            if (r0 == r1) goto L_0x016d
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x016d
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dK     // Catch:{ all -> 0x00f8 }
            r1 = 11
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r12.bU     // Catch:{ all -> 0x00f8 }
            int r2 = r12.bY     // Catch:{ all -> 0x00f8 }
            r3 = 20
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.bO     // Catch:{ all -> 0x00f8 }
            int r1 = r12.bU     // Catch:{ all -> 0x00f8 }
            int r1 = r1 + 13
            int r2 = r12.bY     // Catch:{ all -> 0x00f8 }
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r3 = r3.dK     // Catch:{ all -> 0x00f8 }
            a(r0, r13, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x0361
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            if (r0 == r10) goto L_0x0361
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 == r1) goto L_0x0361
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.bH     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0425
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dC     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 7
            if (r0 >= r1) goto L_0x03cd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.eB     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r1 = r1 << 1
            int r1 = r1 / 3
            if (r0 > r1) goto L_0x03cd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.eB     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r1 = r1 / 3
            if (r0 <= r1) goto L_0x03c5
            r1 = 10710547(0xa36e13, float:1.5008673E-38)
            r0 = 16640261(0xfde905, float:2.3317972E-38)
        L_0x0331:
            r13.setColor(r1)     // Catch:{ all -> 0x00f8 }
            r1 = 87
            int r2 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r3 = r12.bT     // Catch:{ all -> 0x00f8 }
            r4 = 20
            r13.d(r1, r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            r0 = 87
            int r1 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r2 = r12.bT     // Catch:{ all -> 0x00f8 }
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eB     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * r3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eC     // Catch:{ all -> 0x00f8 }
            int r2 = r2 / r3
            r3 = 20
            r13.d(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dC     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            switch(r0) {
                case 7: goto L_0x03fd;
                case 8: goto L_0x03d5;
                case 9: goto L_0x03fd;
                case 10: goto L_0x03d5;
                case 11: goto L_0x03fd;
                case 12: goto L_0x03d5;
                case 13: goto L_0x03fd;
                case 14: goto L_0x03d5;
                case 15: goto L_0x03d5;
                default: goto L_0x0361;
            }     // Catch:{ all -> 0x00f8 }
        L_0x0361:
            r0 = r8
        L_0x0362:
            r1 = 30
            if (r0 >= r1) goto L_0x047b
            r1 = 4
            if (r0 > r1) goto L_0x0378
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r1 = r1.bv     // Catch:{ all -> 0x00f8 }
            r2 = 21
            if (r1 == r2) goto L_0x03b5
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.cv     // Catch:{ all -> 0x00f8 }
            r2 = 2
            if (r1 == r2) goto L_0x03b5
        L_0x0378:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r1 = r1.cN     // Catch:{ all -> 0x00f8 }
            int r2 = r0 + 43
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            boolean r1 = r1.cr     // Catch:{ all -> 0x00f8 }
            if (r1 == 0) goto L_0x03b5
            r1 = 5
            if (r0 > r1) goto L_0x039c
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.o     // Catch:{ all -> 0x00f8 }
            r2 = 20
            if (r1 != r2) goto L_0x039c
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.B     // Catch:{ all -> 0x00f8 }
            if (r1 == r10) goto L_0x03b5
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.B     // Catch:{ all -> 0x00f8 }
            r2 = 5
            if (r1 == r2) goto L_0x03b5
        L_0x039c:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r1 = r1.cN     // Catch:{ all -> 0x00f8 }
            int r2 = r0 + 43
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r2 = r2.rO     // Catch:{ all -> 0x00f8 }
            r3 = 12
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r3 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r4 = 12
            r3 = r3[r4]     // Catch:{ all -> 0x00f8 }
            r1.a(r2, r3, r13)     // Catch:{ all -> 0x00f8 }
        L_0x03b5:
            int r0 = r0 + 1
            goto L_0x0362
        L_0x03b8:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x02bd
            goto L_0x0164
        L_0x03c0:
            r12.g(r13)     // Catch:{ all -> 0x00f8 }
            goto L_0x02bd
        L_0x03c5:
            r1 = 6883104(0x690720, float:9.645283E-39)
            r0 = 16716049(0xff1111, float:2.3424174E-38)
            goto L_0x0331
        L_0x03cd:
            r1 = 1539604(0x177e14, float:2.157445E-39)
            r0 = 65280(0xff00, float:9.1477E-41)
            goto L_0x0331
        L_0x03d5:
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.bT     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eB     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * r1
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r0 = r0 / r1
            int r0 = r0 + 7
            int r0 = r0 + 80
            int r1 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r2 = r12.bT     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 300
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eC     // Catch:{ all -> 0x00f8 }
            int r2 = r2 / r3
            r3 = 20
            r13.d(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0361
        L_0x03fd:
            r0 = 65280(0xff00, float:9.1477E-41)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.bT     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eB     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * r1
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r0 = r0 / r1
            int r0 = r0 + 7
            int r0 = r0 + 80
            int r1 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r2 = r12.bT     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 300
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eC     // Catch:{ all -> 0x00f8 }
            int r2 = r2 / r3
            r3 = 20
            r13.d(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0361
        L_0x0425:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.eB     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r1 = r1 << 1
            int r1 = r1 / 3
            if (r0 <= r1) goto L_0x0461
            r1 = 1539604(0x177e14, float:2.157445E-39)
            r0 = 65280(0xff00, float:9.1477E-41)
        L_0x0439:
            r13.setColor(r1)     // Catch:{ all -> 0x00f8 }
            r1 = 87
            int r2 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r3 = r12.bT     // Catch:{ all -> 0x00f8 }
            r4 = 20
            r13.d(r1, r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            r0 = 87
            int r1 = r12.bS     // Catch:{ all -> 0x00f8 }
            int r2 = r12.bT     // Catch:{ all -> 0x00f8 }
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eB     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * r3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r3 = r3.eC     // Catch:{ all -> 0x00f8 }
            int r2 = r2 / r3
            r3 = 20
            r13.d(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0361
        L_0x0461:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.eB     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.eC     // Catch:{ all -> 0x00f8 }
            int r1 = r1 / 3
            if (r0 <= r1) goto L_0x0474
            r1 = 10710547(0xa36e13, float:1.5008673E-38)
            r0 = 16640261(0xfde905, float:2.3317972E-38)
            goto L_0x0439
        L_0x0474:
            r1 = 6883104(0x690720, float:9.645283E-39)
            r0 = 16716049(0xff1111, float:2.3424174E-38)
            goto L_0x0439
        L_0x047b:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x04d7
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            if (r0 == r10) goto L_0x04d7
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 == r1) goto L_0x04d7
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 7
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.cr     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x04b3
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 7
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r1 = r1.rO     // Catch:{ all -> 0x00f8 }
            r2 = 12
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r2 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r3 = 12
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            r0.a(r1, r2, r13)     // Catch:{ all -> 0x00f8 }
        L_0x04b3:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.sb     // Catch:{ all -> 0x00f8 }
            r1 = 4
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r1 = r1.cN     // Catch:{ all -> 0x00f8 }
            r2 = 7
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1.ed     // Catch:{ all -> 0x00f8 }
            int r1 = r1 + 42
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r2 = r2.cN     // Catch:{ all -> 0x00f8 }
            r3 = 7
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            int r2 = r2.ei     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + 8
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r3 = r3.dK     // Catch:{ all -> 0x00f8 }
            a(r0, r13, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x04d7:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r0 = r0.cK     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0.m     // Catch:{ all -> 0x00f8 }
            r1 = 23
            if (r0 == r1) goto L_0x04f7
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r0 = r0.cK     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0.m     // Catch:{ all -> 0x00f8 }
            r1 = 28
            if (r0 != r1) goto L_0x05b5
        L_0x04f7:
            int[][] r0 = game.i.ab     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.dD     // Catch:{ all -> 0x00f8 }
            r2 = 1
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 * 25
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r2 = r2.dD     // Catch:{ all -> 0x00f8 }
            r3 = 2
            byte r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 + r2
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 2
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 4
            if (r0 == r1) goto L_0x05b5
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r0 = r0.cK     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0.ai     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r1 = r1.cK     // Catch:{ all -> 0x00f8 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1.u     // Catch:{ all -> 0x00f8 }
            r2 = 5665660(0x56737c, float:7.93928E-39)
            r13.setColor(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 + -2
            int r2 = r2 / 2
            r3 = 9
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r4 = r4 + 2
            int r5 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r5 = r5 + 2
            r13.d(r2, r3, r4, r5)     // Catch:{ all -> 0x00f8 }
            r2 = 8104118(0x7ba8b6, float:1.1356288E-38)
            r13.setColor(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 / 2
            r3 = 10
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r5 = r12.bR     // Catch:{ all -> 0x00f8 }
            r13.d(r2, r3, r4, r5)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 / 2
            r3 = 10
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r1 = r1 * r4
            int r0 = r1 / r0
            int r1 = r12.bR     // Catch:{ all -> 0x00f8 }
            r13.e(r2, r3, r0, r1)     // Catch:{ all -> 0x00f8 }
            r1 = 7741194(0x761f0a, float:1.0847723E-38)
            r2 = 15533831(0xed0707, float:2.1767534E-38)
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r3 = r0 / 2
            r4 = 10
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r5 = r0 + 100
            int r6 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r7 = r12.bR     // Catch:{ all -> 0x00f8 }
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r1 = 15533831(0xed0707, float:2.1767534E-38)
            r2 = 16417798(0xfa8406, float:2.3006235E-38)
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r3 = r0 + 100
            r4 = 10
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r0 = r0 + 100
            int r5 = r0 + 94
            int r6 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r7 = r12.bR     // Catch:{ all -> 0x00f8 }
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r1 = 0
            r2 = 640(0x280, float:8.97E-43)
            r3 = 360(0x168, float:5.04E-43)
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x05b5:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cv     // Catch:{ all -> 0x00f8 }
            r1 = 2
            if (r0 != r1) goto L_0x0661
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r0 = r0.cK     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0.ai     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[][] r1 = r1.cK     // Catch:{ all -> 0x00f8 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1.u     // Catch:{ all -> 0x00f8 }
            r2 = 5665660(0x56737c, float:7.93928E-39)
            r13.setColor(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 + -2
            int r2 = r2 / 2
            r3 = 9
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r4 = r4 + 2
            int r5 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r5 = r5 + 2
            r13.d(r2, r3, r4, r5)     // Catch:{ all -> 0x00f8 }
            r2 = 8104118(0x7ba8b6, float:1.1356288E-38)
            r13.setColor(r2)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 / 2
            r3 = 10
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r5 = r12.bR     // Catch:{ all -> 0x00f8 }
            r13.d(r2, r3, r4, r5)     // Catch:{ all -> 0x00f8 }
            int r2 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r2 = 640 - r2
            int r2 = r2 / 2
            r3 = 10
            int r4 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r1 = r1 * r4
            int r0 = r1 / r0
            r1 = 10
            r13.e(r2, r3, r0, r1)     // Catch:{ all -> 0x00f8 }
            r1 = 7741194(0x761f0a, float:1.0847723E-38)
            r2 = 15533831(0xed0707, float:2.1767534E-38)
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r3 = r0 / 2
            r4 = 10
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r0 = r0 + 20
            int r5 = r0 + 100
            int r6 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r7 = r12.bR     // Catch:{ all -> 0x00f8 }
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r1 = 15533831(0xed0707, float:2.1767534E-38)
            r2 = 16417798(0xfa8406, float:2.3006235E-38)
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r3 = r0 + 120
            r4 = 10
            int r0 = r12.cH     // Catch:{ all -> 0x00f8 }
            int r0 = 640 - r0
            int r0 = r0 / 2
            int r0 = r0 + 120
            int r5 = r0 + 92
            int r6 = r12.bR     // Catch:{ all -> 0x00f8 }
            int r7 = r12.bR     // Catch:{ all -> 0x00f8 }
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r1 = 0
            r2 = 640(0x280, float:8.97E-43)
            r3 = 360(0x168, float:5.04E-43)
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0661:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 87
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.cr     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0686
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 87
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r1 = r1.rO     // Catch:{ all -> 0x00f8 }
            r2 = 16
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r2 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r3 = 16
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            r0.a(r1, r2, r13)     // Catch:{ all -> 0x00f8 }
        L_0x0686:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 73
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.cr     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x06ab
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 73
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r1 = r1.rO     // Catch:{ all -> 0x00f8 }
            r2 = 16
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r2 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r3 = 16
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            r0.a(r1, r2, r13)     // Catch:{ all -> 0x00f8 }
        L_0x06ab:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 74
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.cr     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0741
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cu     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0741
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.b r0 = r0.cL     // Catch:{ all -> 0x00f8 }
            int r0 = r0.p     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 == r1) goto L_0x0741
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0741
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 74
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[][] r1 = r1.rO     // Catch:{ all -> 0x00f8 }
            r2 = 19
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.String[] r2 = game.i.rP     // Catch:{ all -> 0x00f8 }
            r3 = 19
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            r0.a(r1, r2, r13)     // Catch:{ all -> 0x00f8 }
        L_0x06e5:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0806
            int r0 = r12.o     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.o = r0     // Catch:{ all -> 0x00f8 }
            int r0 = r12.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x06fa
            r0 = 0
            r12.o = r0     // Catch:{ all -> 0x00f8 }
        L_0x06fa:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dq     // Catch:{ all -> 0x00f8 }
            r1 = 3
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            if (r0 < 0) goto L_0x0806
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cu     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0806
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cv     // Catch:{ all -> 0x00f8 }
            if (r0 != 0) goto L_0x0806
            int[][] r0 = game.i.ab     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.dD     // Catch:{ all -> 0x00f8 }
            r2 = 1
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 * 25
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r2 = r2.dD     // Catch:{ all -> 0x00f8 }
            r3 = 2
            byte r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 + r2
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 2
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 4
            if (r0 == r1) goto L_0x0806
            r1 = r8
            r0 = r8
        L_0x072c:
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dr     // Catch:{ all -> 0x00f8 }
            int r2 = r2.length     // Catch:{ all -> 0x00f8 }
            if (r1 >= r2) goto L_0x0769
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dr     // Catch:{ all -> 0x00f8 }
            r2 = r2[r1]     // Catch:{ all -> 0x00f8 }
            r3 = 3
            if (r2 != r3) goto L_0x073e
            int r0 = r1 + 4
        L_0x073e:
            int r1 = r1 + 1
            goto L_0x072c
        L_0x0741:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 74
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0.o = r1     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 74
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0.eN = r1     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            game.f[] r0 = r0.cN     // Catch:{ all -> 0x00f8 }
            r1 = 74
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0.cr = r1     // Catch:{ all -> 0x00f8 }
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0.eN = r1     // Catch:{ all -> 0x00f8 }
            goto L_0x06e5
        L_0x0769:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r2 = r2.cu     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + -1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = "label"
            r2.<init>(r3)     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuffer r2 = r2.append(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00f8 }
            g r1 = r1.E(r2)     // Catch:{ all -> 0x00f8 }
            int r1 = r1.o     // Catch:{ all -> 0x00f8 }
            int r5 = r1 + 10
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            g[] r1 = r1.br     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r2 = r2.cu     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + -1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ all -> 0x00f8 }
            java.lang.String r3 = "label"
            r2.<init>(r3)     // Catch:{ all -> 0x00f8 }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f8 }
            g r0 = r1.E(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r0.p     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 34
            int r1 = r12.o     // Catch:{ all -> 0x00f8 }
            int r1 = r1 % 2
            if (r1 != 0) goto L_0x07c5
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r1 = r1.dw     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dw     // Catch:{ all -> 0x00f8 }
            r3 = 1
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            if (r1 >= r2) goto L_0x087a
        L_0x07c5:
            int r7 = r12.bZ     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r1 = r1.dw     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dw     // Catch:{ all -> 0x00f8 }
            r3 = 1
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            int r3 = r5 + -2
            int r4 = r0 + 2
            int r6 = r12.ca     // Catch:{ all -> 0x00f8 }
            int r6 = r6 + 8
            int r1 = r1 * r6
            int r1 = r1 / r2
            int r1 = r1 + 1
            int r2 = r7 + -2
            r13.e(r3, r4, r1, r2)     // Catch:{ all -> 0x00f8 }
            r1 = 14512434(0xdd7132, float:2.0336251E-38)
            r2 = 16708270(0xfef2ae, float:2.3413273E-38)
            int r3 = r5 + -2
            int r4 = r0 + 1
            int r0 = r12.ca     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + r5
            int r0 = r0 + 8
            int r5 = r0 + 1
            r6 = 10
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r1 = 0
            r2 = 640(0x280, float:8.97E-43)
            r3 = 360(0x168, float:5.04E-43)
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0806:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 != r1) goto L_0x08bd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            if (r0 == r10) goto L_0x08bd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.B     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 == r1) goto L_0x08bd
            r6 = r8
        L_0x081c:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dr     // Catch:{ all -> 0x00f8 }
            int r0 = r0.length     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + -1
            if (r6 >= r0) goto L_0x08bd
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dr     // Catch:{ all -> 0x00f8 }
            r0 = r0[r6]     // Catch:{ all -> 0x00f8 }
            r1 = -1
            if (r0 == r1) goto L_0x0876
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dr     // Catch:{ all -> 0x00f8 }
            r0 = r0[r6]     // Catch:{ all -> 0x00f8 }
            r1 = 3
            if (r0 == r1) goto L_0x0876
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bv     // Catch:{ all -> 0x00f8 }
            r1 = 21
            if (r0 == r1) goto L_0x0876
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cv     // Catch:{ all -> 0x00f8 }
            r1 = 2
            if (r0 == r1) goto L_0x0876
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r0 = r0.dp     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r1 = r1.dr     // Catch:{ all -> 0x00f8 }
            r1 = r1[r6]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 << 1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r1 = r1.dp     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dr     // Catch:{ all -> 0x00f8 }
            r2 = r2[r6]     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 1
            int r2 = r2 + 1
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int[] r2 = r12.X     // Catch:{ all -> 0x00f8 }
            r3 = r2[r6]     // Catch:{ all -> 0x00f8 }
            int[] r2 = r12.X     // Catch:{ all -> 0x00f8 }
            int r4 = r6 + 3
            r4 = r2[r4]     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r5 = r2.dL     // Catch:{ all -> 0x00f8 }
            r2 = r13
            a(r0, r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00f8 }
        L_0x0876:
            int r0 = r6 + 1
            r6 = r0
            goto L_0x081c
        L_0x087a:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r1 = r1.dw     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r2 = r2.dw     // Catch:{ all -> 0x00f8 }
            r3 = 1
            r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            if (r1 < r2) goto L_0x0806
            int r1 = r5 + -2
            int r2 = r0 + 2
            int r3 = r12.ca     // Catch:{ all -> 0x00f8 }
            int r3 = r3 + 8
            int r3 = r3 + 1
            int r4 = r12.bZ     // Catch:{ all -> 0x00f8 }
            int r4 = r4 + -2
            r13.e(r1, r2, r3, r4)     // Catch:{ all -> 0x00f8 }
            r1 = 16777215(0xffffff, float:2.3509886E-38)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            int r3 = r5 + -2
            int r4 = r0 + 1
            int r0 = r12.ca     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + r5
            int r5 = r0 + 1
            r6 = 10
            int r7 = r12.bZ     // Catch:{ all -> 0x00f8 }
            r0 = r13
            a(r0, r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r1 = 0
            r2 = 640(0x280, float:8.97E-43)
            r3 = 360(0x168, float:5.04E-43)
            r13.e(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0806
        L_0x08bd:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cu     // Catch:{ all -> 0x00f8 }
            if (r0 > r9) goto L_0x016d
            r0 = 3
            r12.b(r0, r13)     // Catch:{ all -> 0x00f8 }
            r0 = 1
            r12.b(r0, r13)     // Catch:{ all -> 0x00f8 }
            r0 = 0
            r12.c(r0, r13)     // Catch:{ all -> 0x00f8 }
            r0 = 2
            r12.c(r0, r13)     // Catch:{ all -> 0x00f8 }
            r0 = 3
            r12.c(r0, r13)     // Catch:{ all -> 0x00f8 }
            r0 = 1
            r12.c(r0, r13)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x08dd:
            r12.f(r13)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x08e2:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cu     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x016d
            r12.h(r13)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x08ed:
            r12.c(r13)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x08f2:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 0
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 >= r1) goto L_0x0905
            com.a.a.d.h r0 = r12.cG     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r12.a(r0, r1)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x0905:
            com.a.a.d.h r0 = r12.cG     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r12.a(r0, r1)     // Catch:{ all -> 0x00f8 }
            goto L_0x016d
        L_0x090d:
            java.lang.String[][] r0 = game.i.bl     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r1 = r1.y     // Catch:{ all -> 0x00f8 }
            int r1 = 6 - r1
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            b(r13, r0)     // Catch:{ all -> 0x00f8 }
            goto L_0x0164
        L_0x091c:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 65
            if (r0 == r1) goto L_0x092a
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0164
        L_0x092a:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            r1 = -1
            if (r0 == r1) goto L_0x0164
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bC     // Catch:{ all -> 0x00f8 }
            switch(r0) {
                case 0: goto L_0x0938;
                case 1: goto L_0x0974;
                case 2: goto L_0x09f7;
                case 3: goto L_0x0a65;
                case 4: goto L_0x0ab4;
                case 5: goto L_0x0ae9;
                case 6: goto L_0x0b0f;
                case 7: goto L_0x0c1b;
                case 8: goto L_0x0b88;
                case 9: goto L_0x0bf5;
                case 10: goto L_0x0c1b;
                case 11: goto L_0x0c1b;
                case 12: goto L_0x0c1b;
                case 13: goto L_0x0c1b;
                case 14: goto L_0x0c1b;
                case 15: goto L_0x0c1b;
                case 16: goto L_0x0938;
                case 17: goto L_0x0c4a;
                case 18: goto L_0x0c1b;
                case 19: goto L_0x0c1b;
                case 20: goto L_0x0c1b;
                case 21: goto L_0x0c1b;
                case 22: goto L_0x0c1b;
                case 23: goto L_0x0c1b;
                case 24: goto L_0x0d41;
                case 25: goto L_0x0c1b;
                case 26: goto L_0x0dcf;
                case 27: goto L_0x0c1b;
                case 28: goto L_0x0e51;
                case 29: goto L_0x0ed3;
                case 30: goto L_0x0c1b;
                case 31: goto L_0x0c1b;
                case 32: goto L_0x0c1b;
                case 33: goto L_0x0c1b;
                case 34: goto L_0x0938;
                case 35: goto L_0x0938;
                case 36: goto L_0x0938;
                case 37: goto L_0x0938;
                case 38: goto L_0x0938;
                case 39: goto L_0x0938;
                case 40: goto L_0x0938;
                case 41: goto L_0x0938;
                case 42: goto L_0x0938;
                case 43: goto L_0x0d92;
                case 44: goto L_0x0938;
                case 45: goto L_0x0b37;
                case 46: goto L_0x0c1b;
                case 47: goto L_0x0c1b;
                case 48: goto L_0x0c1b;
                case 49: goto L_0x0c1b;
                case 50: goto L_0x0c1b;
                case 51: goto L_0x0c9b;
                default: goto L_0x0938;
            }     // Catch:{ all -> 0x00f8 }
        L_0x0938:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.o     // Catch:{ all -> 0x00f8 }
            r1 = 65
            if (r0 == r1) goto L_0x0946
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            boolean r0 = r0.ap     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0164
        L_0x0946:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bC     // Catch:{ all -> 0x00f8 }
            r1 = 4
            if (r0 == r1) goto L_0x0965
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bC     // Catch:{ all -> 0x00f8 }
            r1 = 45
            if (r0 == r1) goto L_0x0965
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bC     // Catch:{ all -> 0x00f8 }
            r1 = 8
            if (r0 == r1) goto L_0x0965
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte r0 = r0.bC     // Catch:{ all -> 0x00f8 }
            r1 = 9
            if (r0 != r1) goto L_0x0164
        L_0x0965:
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0164
        L_0x0974:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x09a7
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x09f0
            r0 = 6
            int r8 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
        L_0x099b:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r2 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x00f8 }
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
        L_0x09a7:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r8 + 320
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x09d3
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x09d3:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x09dd
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x09dd:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x09f0:
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            int r8 = -r0
            goto L_0x099b
        L_0x09f7:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0a55
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 <= r1) goto L_0x0a38
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 1
            r1 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r12.u     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * 10
            int r0 = 640 - r0
            int r2 = r0 / 2
            r3 = 210(0xd2, float:2.94E-43)
            int r0 = r12.u     // Catch:{ all -> 0x00f8 }
            int r4 = r0 * 10
            int r0 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r5 = r0 * 3
            r6 = 10
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r7 = r0.dA     // Catch:{ all -> 0x00f8 }
            r0 = r12
            r0.a(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
        L_0x0a38:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0a42
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0a42:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0a55:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0a65:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 <= r1) goto L_0x0a97
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 2
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0a97:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0aa1
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0aa1:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0ab4:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0ae9
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0ad8
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0ad8:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0ae9
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0ae9:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0938
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0b0f:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 >= r1) goto L_0x0938
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0b37:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 <= r1) goto L_0x0b6b
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 3
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            int r2 = r2 + -5
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0b6b:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0b75
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0b75:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0b88:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x0bbb
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x0bee
            r0 = 6
            int r8 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
        L_0x0baf:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r2 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x00f8 }
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
        L_0x0bbb:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r8 + 320
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0bd3
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0bd3:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 30
            if (r0 < r1) goto L_0x0938
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0bee:
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            int r8 = -r0
            goto L_0x0baf
        L_0x0bf5:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0938
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0c1b:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            if (r0 == 0) goto L_0x0c38
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
        L_0x0c38:
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0938
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0c4a:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0c7e
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 4
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            int r2 = r2 + -5
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0c7e:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0c88
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0c88:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0c9b:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x0f9f
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * 640
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 / r1
            int r0 = 640 - r0
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 6
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 * 360
            game.i r2 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r2 = r2.rU     // Catch:{ all -> 0x00f8 }
            r3 = 16
            byte r2 = r2[r3]     // Catch:{ all -> 0x00f8 }
            int r1 = r1 / r2
            int r8 = 360 - r1
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 6
            byte r3 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r3 = r3 + 1
            byte r3 = (byte) r3     // Catch:{ all -> 0x00f8 }
            r1[r2] = r3     // Catch:{ all -> 0x00f8 }
        L_0x0ce6:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r1 = r1.dN     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r0 = 327 - r0
            int r2 = r8 + 0
            a(r13, r1, r0, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0d24
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 11
            r1 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * 10
            int r0 = 640 - r0
            int r2 = r0 / 2
            r3 = 245(0xf5, float:3.43E-43)
            int r0 = r12.u     // Catch:{ all -> 0x00f8 }
            int r4 = r0 * 10
            int r0 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r0 = r0 * 3
            int r5 = r0 + -1
            r6 = 10
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r7 = r0.dA     // Catch:{ all -> 0x00f8 }
            r0 = r12
            r0.a(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f8 }
        L_0x0d24:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0d2e
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0d2e:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0d41:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0d75
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 5
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            int r2 = r2 + -5
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0d75:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0d7f
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0d7f:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0d92:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 320(0x140, float:4.48E-43)
            r2 = 40
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0dc3
            java.lang.String r0 = "恭喜通关"
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            int r2 = r2 + -5
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0dc3:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0938
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0dcf:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x0e02
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x0e4a
            r0 = 6
            int r8 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
        L_0x0df6:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r2 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x00f8 }
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
        L_0x0e02:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r8 + 320
            r2 = 5
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0e2d
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 6
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 180(0xb4, float:2.52E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 << 2
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0e2d:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0e37
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0e37:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0e4a:
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            int r8 = -r0
            goto L_0x0df6
        L_0x0e51:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x0e84
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x0ecc
            r0 = 6
            int r8 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
        L_0x0e78:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r2 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r2 = r2 + 1
            byte r2 = (byte) r2     // Catch:{ all -> 0x00f8 }
            r0[r1] = r2     // Catch:{ all -> 0x00f8 }
        L_0x0e84:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r0 = r0.dN     // Catch:{ all -> 0x00f8 }
            r1 = 0
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r1 = r8 + 320
            r2 = 5
            a(r13, r0, r1, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 10
            if (r0 <= r1) goto L_0x0eaf
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 7
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 120(0x78, float:1.68E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0eaf:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0eb9
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0eb9:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0ecc:
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            int r8 = -r0
            goto L_0x0e78
        L_0x0ed3:
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 <= 0) goto L_0x0938
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 16
            byte r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            if (r0 == r1) goto L_0x0f9c
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r0 = r0.rU     // Catch:{ all -> 0x00f8 }
            r1 = 6
            byte r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 % 2
            if (r0 != 0) goto L_0x0f55
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
        L_0x0efa:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            byte[] r1 = r1.rU     // Catch:{ all -> 0x00f8 }
            r2 = 6
            byte r3 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r3 = r3 + 1
            byte r3 = (byte) r3     // Catch:{ all -> 0x00f8 }
            r1[r2] = r3     // Catch:{ all -> 0x00f8 }
        L_0x0f06:
            game.i r1 = r12.ce     // Catch:{ all -> 0x00f8 }
            com.a.a.d.i[] r1 = r1.dN     // Catch:{ all -> 0x00f8 }
            r2 = 0
            r1 = r1[r2]     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 320
            r2 = 5
            a(r13, r1, r0, r2)     // Catch:{ all -> 0x00f8 }
            r0 = 16777215(0xffffff, float:2.3509886E-38)
            r13.setColor(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 20
            if (r0 <= r1) goto L_0x0f5c
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            if (r0 != r9) goto L_0x0f5c
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 8
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 215(0xd7, float:3.01E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x0f38:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 >= r11) goto L_0x0f42
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            int r0 = r0 + 1
            r12.q = r0     // Catch:{ all -> 0x00f8 }
        L_0x0f42:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            if (r0 < r11) goto L_0x0938
            java.lang.String r0 = "点此继续"
            r1 = 320(0x140, float:4.48E-43)
            int r2 = r12.p     // Catch:{ all -> 0x00f8 }
            int r2 = 360 - r2
            r3 = 17
            r13.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0938
        L_0x0f55:
            r0 = 6
            int r0 = defpackage.q.M(r0)     // Catch:{ all -> 0x00f8 }
            int r0 = -r0
            goto L_0x0efa
        L_0x0f5c:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 <= r1) goto L_0x0f7c
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            r1 = 2
            if (r0 != r1) goto L_0x0f7c
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 9
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 215(0xd7, float:3.01E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0f38
        L_0x0f7c:
            int r0 = r12.q     // Catch:{ all -> 0x00f8 }
            r1 = 5
            if (r0 <= r1) goto L_0x0f38
            game.i r0 = r12.ce     // Catch:{ all -> 0x00f8 }
            int r0 = r0.cx     // Catch:{ all -> 0x00f8 }
            r1 = 3
            if (r0 != r1) goto L_0x0f38
            java.lang.String[] r0 = game.i.bi     // Catch:{ all -> 0x00f8 }
            r1 = 10
            r0 = r0[r1]     // Catch:{ all -> 0x00f8 }
            r1 = 215(0xd7, float:3.01E-43)
            int r2 = r12.ai     // Catch:{ all -> 0x00f8 }
            int r2 = r2 * 3
            game.i r3 = r12.ce     // Catch:{ all -> 0x00f8 }
            int[] r3 = r3.dA     // Catch:{ all -> 0x00f8 }
            r12.a(r0, r1, r2, r3)     // Catch:{ all -> 0x00f8 }
            goto L_0x0f38
        L_0x0f9c:
            r0 = r8
            goto L_0x0f06
        L_0x0f9f:
            r0 = r8
            goto L_0x0ce6
        */
        throw new UnsupportedOperationException("Method not decompiled: game.g.a(com.a.a.d.h):void");
    }

    /* access modifiers changed from: protected */
    public final void b(int i, int i2) {
        this.ce.a(i, i2);
    }

    /* access modifiers changed from: protected */
    public final void c(int i) {
        this.ce.f(i);
    }

    public final void d(int i) {
        this.ce.a(i);
    }

    /* access modifiers changed from: protected */
    public final void d(int i, int i2) {
        this.ce.c(i, i2);
    }

    public final void d(h hVar) {
        switch (this.ce.B) {
            case 0:
                a(0, hVar);
                return;
            case 1:
            case 2:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                int i = (this.ce.cL.ao || this.ce.ao) ? 20 : 16;
                a(this.ce.rF[this.ce.bu], hVar);
                if (this.ce.cn) {
                    a(16777215, hVar);
                    return;
                }
                this.ce.cM[0].a(0, this.ce.rE[this.ce.bq], this.ce.aq[this.ce.bq], hVar, this.ce.x);
                if (this.ce.B == 5 || this.ce.B == 6) {
                    this.ce.cM[0].a(1, this.ce.rE[this.ce.bq], this.ce.aq[this.ce.bq], hVar, this.ce.x);
                    return;
                }
                for (int i2 = 0; i2 < this.ce.cK[2].length; i2++) {
                    this.ce.cK[2][i2].a(this.ce.rH[this.ce.cd[2][i2][0]], i.rI[this.ce.cd[2][i2][0]], hVar);
                }
                this.ce.cM[0].a(1, this.ce.rE[this.ce.bq], this.ce.aq[this.ce.bq], hVar, this.ce.x);
                hVar.setColor(0);
                for (int i3 = 0; i3 < this.ce.cS[1].length; i3++) {
                    if (this.ce.cS[1][i3][0] >= 0) {
                        hVar.d(this.ce.cS[1][i3][0] - this.ce.cM[0].ed, this.ce.cS[1][i3][1] - this.ce.cM[0].ei, 2, 2);
                    }
                }
                for (int i4 = 0; i4 < this.ce.cK[1].length; i4++) {
                    if (!this.ce.cK[1][i4].ap && this.ce.cK[1][i4] != null) {
                        if (this.ce.bZ == 6 && i4 == 0) {
                            this.ce.cK[1][i4].a(this.ce.rH[25], i.rI[25], hVar);
                            if (this.ce.cK[1][i4].co) {
                                hVar.setColor(PurchaseCode.AUTH_INVALID_APP);
                                hVar.c((this.ce.cK[1][i4].ed + (this.ce.cK[1][i4].bY >> 1)) - 26, (this.ce.cK[1][i4].ei - 20) - 3, 52, 6);
                                hVar.setColor(65535);
                                hVar.d((this.ce.cK[1][i4].ed + (this.ce.cK[1][i4].bY >> 1)) - 26, (this.ce.cK[1][i4].ei - 20) - 2, (26 - this.ce.cK[1][i4].X[0]) << 1, 4);
                            }
                        } else if (this.ce.cK[1][i4].bU == 1 || this.ce.cK[1][i4].bU == 2 || this.ce.cK[1][i4].bU == 3) {
                            this.ce.cK[1][i4].a(this.ce.rH[this.ce.cd[1][i4][0]], i.rI[this.ce.cd[1][i4][0]], hVar);
                        } else if (this.ce.cK[1][i4].bU == 6 || this.ce.cK[1][i4].bU == 7) {
                            this.ce.cK[1][i4].a(this.ce.rH[this.ce.cd[1][i4][0]], i.rI[this.ce.cd[1][i4][0]], hVar);
                        } else {
                            this.ce.cK[1][i4].a(this.ce.rH[this.ce.cd[1][i4][0]], i.rI[this.ce.cd[1][i4][0]], hVar);
                        }
                        if (!this.ce.cK[1][i4].cw) {
                            this.ce.cK[1][i4].cx = -1;
                        } else if (this.ce.cK[1][i4].cx <= 13 && this.ce.cK[1][i4].am != 9 && ((this.ce.cK[1][i4].am < 32 || this.ce.cK[1][i4].am > 37) && this.ce.cK[1][i4].am != 8 && (this.ce.cK[1][i4].am > 29 || this.ce.cK[1][i4].am < 25))) {
                            if (this.ce.cK[1][i4].u >= 0) {
                                if (this.ce.cK[1][i4].cx >= 13) {
                                    this.ce.cK[1][i4].cx = 0;
                                } else if (this.ce.cK[1][i4].cx < 13) {
                                    this.ce.cK[1][i4].cx++;
                                }
                                switch (this.ce.cK[1][i4].cx) {
                                    case 0:
                                    case 1:
                                    case 2:
                                        this.ce.cN[11].a(0, 0);
                                        break;
                                    case 3:
                                        this.ce.cN[11].a(1, 0);
                                        break;
                                    case 4:
                                        this.ce.cN[11].a(2, 0);
                                        break;
                                    case 5:
                                    case 8:
                                    case 11:
                                        this.ce.cN[11].a(9, 0);
                                        break;
                                    case 6:
                                    case 9:
                                    case 12:
                                        this.ce.cN[11].a(10, 0);
                                        break;
                                    case 7:
                                    case 10:
                                    case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
                                        this.ce.cN[11].a(11, 0);
                                        break;
                                }
                                this.ce.cN[11].e(this.ce.cK[1][i4].ed + this.ce.cK[1][i4].ez + (this.ce.cK[1][i4].bY / 2), this.ce.cK[1][i4].ei + this.ce.cK[1][i4].eA + 17);
                                if (!(this.ce.cK[1][i4].cx > 13 || this.ce.cK[1][i4].m == 19 || this.ce.cK[1][i4].m == 20 || this.ce.cK[1][i4].m == 21 || this.ce.cK[1][i4].m == 22 || this.ce.cK[1][i4].m == 23 || this.ce.cK[1][i4].m == 24 || this.ce.cK[1][i4].m == 27 || this.ce.cK[1][i4].m == 28 || this.ce.cK[1][i4].m == 30 || (((this.ce.cK[1][i4].bU == 3 || this.ce.cK[1][i4].bU == 9) && this.ce.cK[1][i4].am == 30) || (this.ce.cL.p != 0 && this.ce.cL.p != 3)))) {
                                    this.ce.cN[11].a(this.ce.rO[11], i.rP[11], hVar);
                                }
                            }
                            if (!(this.ce.cK[1][i4].m == 19 || this.ce.cK[1][i4].m == 21 || this.ce.cK[1][i4].m == 20 || this.ce.cK[1][i4].m == 22 || this.ce.cK[1][i4].m == 23 || this.ce.cK[1][i4].m == 24 || this.ce.cK[1][i4].m == 28 || ((this.ce.cK[1][i4].m == 30 && i4 <= 1) || ((this.ce.cK[1][i4].bU == 3 || this.ce.cK[1][i4].bU == 9) && this.ce.cK[1][i4].am == 30)))) {
                                hVar.a((this.ce.cL.ao || this.ce.ao) ? this.ce.rA[9] : this.ce.rA[2], this.ce.cK[1][i4].ed + ((this.ce.cK[1][i4].bY - i) >> 1), this.ce.cK[1][i4].ei - 15, 20);
                                hVar.setColor(0);
                                hVar.a(((this.ce.cK[1][i4].bY - i) >> 1) + this.ce.cK[1][i4].ed, this.ce.cK[1][i4].ei - 15, i, i, 0, this.ce.cK[1][i4].u == this.ce.cK[1][i4].ai ? 0 : 360 - ((this.ce.cK[1][i4].u * 360) / this.ce.cK[1][i4].ai));
                            }
                        }
                    }
                    if (this.ce.eP[i4] != 0 && !this.ce.cK[1][i4].ap) {
                        if (this.ce.cK[1][i4].cl || this.ce.cK[1][i4].cn) {
                            this.ce.cN[11].a(8, 0);
                        } else {
                            this.ce.cN[11].a(7, 0);
                        }
                        this.ce.cN[11].e(this.ce.cK[1][i4].ed + this.ce.cK[1][i4].ez + this.ce.cK[1][i4].bY, this.ce.cK[1][i4].ei + 10);
                        if (!this.ce.cK[1][i4].ap && this.ce.cK[1][i4].X[0] % 2 == 0) {
                            this.ce.cN[11].a(this.ce.rO[11], i.rP[11], hVar);
                        }
                    } else if (this.ce.cK[1][i4].ap) {
                        this.ce.eP[i4] = 0;
                    }
                }
                this.ce.cM[0].a(2, this.ce.rE[this.ce.bq], this.ce.aq[this.ce.bq], hVar, this.ce.x);
                for (int i5 = 0; i5 < this.ce.cK[0].length; i5++) {
                    this.ce.cK[0][i5].a(this.ce.rH[this.ce.cd[0][i5][0]], i.rI[this.ce.cd[0][i5][0]], hVar);
                }
                hVar.setColor(0);
                for (int i6 = 0; i6 < 9; i6++) {
                    if (this.ce.cN[i6 + 76].cr) {
                        if (this.ce.cK[1][i6].m == 27) {
                            this.ce.cN[i6 + 76].a(this.ce.rO[25], i.rP[25], hVar);
                        } else {
                            this.ce.cN[i6 + 76].a(this.ce.rO[17], i.rP[17], hVar);
                        }
                    }
                }
                for (int i7 = 0; i7 < 9; i7++) {
                    if (this.ce.cN[i7 + 90].cr) {
                        this.ce.cN[i7 + 90].a(this.ce.rO[27], i.rP[27], hVar);
                    }
                }
                for (int i8 = 0; i8 < this.ce.cS[0].length; i8++) {
                    if (this.ce.cS[0][i8][0] >= 0) {
                        hVar.d(this.ce.cS[0][i8][0] - this.ce.cM[0].ed, this.ce.cS[0][i8][1] - this.ce.cM[0].ei, 2, 2);
                    }
                }
                if (this.ce.cv == 2) {
                    if (this.ce.cN[86].cr) {
                        this.ce.cN[86].a(this.ce.rO[24], i.rP[24], hVar);
                    }
                } else if (i.ab[(this.ce.dD[1] * 25) + this.ce.dD[2]][6] - 1 >= 0 && this.ce.cN[86].cr) {
                    this.ce.cN[86].a(this.ce.rO[(i.ab[(this.ce.dD[1] * 25) + this.ce.dD[2]][6] + 22) - 1], i.rP[(i.ab[(this.ce.dD[1] * 25) + this.ce.dD[2]][6] + 22) - 1], hVar);
                }
                for (int i9 = 0; i9 < this.ce.eG.length; i9++) {
                    if (this.ce.eG[i9].cr) {
                        this.ce.eG[i9].a(this.ce.rO[4], i.rP[4], hVar);
                    }
                }
                if (this.ce.cN[4].cr) {
                    this.ce.cN[4].a(this.ce.rO[4], i.rP[4], hVar);
                }
                switch (this.ce.bZ) {
                    case 0:
                        if (this.ce.cN[0].cr) {
                            hVar.a(this.ce.rA[8], this.ce.cN[0].ed, this.ce.cN[0].ei, 0);
                            break;
                        }
                        break;
                }
                int[][] iArr = this.ce.cU;
                i iVar = this.ce.rA[3];
                for (int i10 = 0; i10 < iArr.length; i10++) {
                    if (iArr[i10][0] >= 0) {
                        a(hVar, new StringBuffer("/").append(iArr[i10][6]).toString(), iArr[i10][1], iArr[i10][2], iVar);
                    }
                }
                if (!(this.ce.B == 7 || this.ce.B == 8 || this.ce.B == 9)) {
                    this.ce.cL.a(this.ce.rS, this.ce.rT[0], hVar);
                }
                if (this.ce.fo[0] == 1) {
                    this.ce.cN[87].cr = true;
                    this.ce.cN[87].c();
                    if (this.ce.fo[1] > 0) {
                        int[] iArr2 = this.ce.fo;
                        iArr2[1] = iArr2[1] - 1;
                        if (!this.ce.bH) {
                            switch (this.ce.ds[6]) {
                                case 0:
                                    i iVar2 = this.ce;
                                    iVar2.eB -= 5;
                                    break;
                                case 1:
                                    if (this.ce.cv == 2) {
                                        i iVar3 = this.ce;
                                        iVar3.eB -= 5;
                                        break;
                                    } else {
                                        i iVar4 = this.ce;
                                        iVar4.eB -= 3;
                                        break;
                                    }
                                case 2:
                                    if (this.ce.cv == 2) {
                                        i iVar5 = this.ce;
                                        iVar5.eB -= 5;
                                        break;
                                    } else {
                                        i iVar6 = this.ce;
                                        iVar6.eB--;
                                        break;
                                    }
                                case 3:
                                    if (this.ce.cv == 2) {
                                        i iVar7 = this.ce;
                                        iVar7.eB -= 2;
                                        break;
                                    } else {
                                        i iVar8 = this.ce;
                                        iVar8.eB = iVar8.eB;
                                        break;
                                    }
                            }
                            if (this.ce.eB < 150 && this.ce.sb[4] > 0) {
                                this.ce.bH = true;
                                this.ce.dC[0] = 0;
                                byte[] bArr = this.ce.sb;
                                bArr[4] = (byte) (bArr[4] - 1);
                            }
                        }
                        if (this.ce.eB < 0) {
                            this.ce.eB = 0;
                            if (this.ce.bH) {
                                this.ce.eB++;
                            }
                        }
                    } else {
                        this.ce.fo[0] = 0;
                    }
                } else {
                    this.ce.cN[87].cr = false;
                }
                if (!this.ce.cl || (this.ce.cL.p == 5 && !this.ce.ct)) {
                    this.ce.cN[73].cr = false;
                    this.ce.cl = false;
                    this.ce.rU[3] = 0;
                } else {
                    this.ce.cN[73].cr = true;
                    this.ce.cN[73].c();
                    if (this.ce.rU[3] == this.ce.rU[13]) {
                        this.ce.cl = false;
                    } else {
                        byte[] bArr2 = this.ce.rU;
                        bArr2[3] = (byte) (bArr2[3] + 1);
                    }
                }
                if ((this.ce.cL.p == 0 && this.ce.m <= 60) || (this.ce.cL.p == 1 && this.ce.as <= 16)) {
                    a(hVar, "点触补充弹药图标购买子弹！", (int) PurchaseCode.AUTH_NOORDER, 80, 16774004, 197378);
                }
                if (this.ce.cN[9].cr) {
                    this.ce.cN[9].a(this.ce.rO[9], i.rP[9], hVar);
                    if (this.ce.cN[9].o > 5 && !this.ce.ao) {
                        a(hVar, "游戏已保存", 320, (int) PurchaseCode.AUTH_NO_APP, 0, 16777215);
                    }
                }
                if (this.ce.bX) {
                    this.ce.cB++;
                    if (this.ce.cB < (((((this.aq[0].length() + 1) * this.u) + 640) / this.ce.ag) * 3) - 10) {
                        hVar.setColor(65280);
                        String[] strArr = this.aq;
                        int i11 = this.ai + 90;
                        int[] iArr3 = this.ce.dT;
                        boolean[] zArr = this.ce.dU;
                        if (zArr[1]) {
                            iArr3[1] = 640;
                            zArr[1] = false;
                        } else if (iArr3[1] < 0 - (strArr[0].length() * this.u)) {
                            iArr3[1] = 640;
                        }
                        hVar.e(0, i11 + 0 + (this.ai >> 1), 640, this.ai + 2);
                        hVar.setColor(65280);
                        hVar.a(strArr[0], iArr3[1], i11 + (this.ai >> 1), 20);
                        hVar.e(0, 0, 640, 360);
                        return;
                    }
                    if (this.ce.ao) {
                        switch (this.ce.cs[0]) {
                            case 1:
                                this.aq[0] = i.bk[0];
                                break;
                            case 2:
                            case 3:
                            case 4:
                                this.aq[0] = i.bk[1];
                                break;
                            case 6:
                                this.aq[0] = i.bk[2];
                                break;
                        }
                    } else {
                        switch (q.b(3, (long) this.ce.rU[0])) {
                            case 0:
                                if (this.ce.ds[8] <= 0) {
                                    this.aq[0] = i.bk[4];
                                    break;
                                } else {
                                    this.aq[0] = i.bk[2];
                                    break;
                                }
                            case 1:
                                this.aq[0] = i.bk[4];
                                break;
                            case 2:
                                int i12 = this.ce.ds[0] > 0 ? 1 : 0;
                                if (this.ce.ds[2] > 0) {
                                    i12++;
                                }
                                if (this.ce.ds[4] > 0) {
                                    i12++;
                                }
                                if (i12 < 2) {
                                    this.aq[0] = i.bk[4];
                                    break;
                                } else {
                                    this.aq[0] = i.bk[1];
                                    break;
                                }
                        }
                        if ((this.ce.cL.p == 0 && this.ce.m <= 50) || (this.ce.cL.p == 1 && this.ce.as <= 20)) {
                            this.aq[0] = i.bk[3];
                        }
                    }
                    this.ce.bX = false;
                    this.ce.cB = 0;
                    this.ce.dT[1] = 640;
                    return;
                }
                return;
            case 3:
            default:
                return;
            case 4:
                byte b2 = this.ce.rU[0];
                hVar.setColor(0);
                if (b2 <= 1) {
                    this.ce.d();
                    this.as = 640;
                    this.bN = DirectGraphics.ROTATE_180;
                    return;
                } else if (b2 <= 10) {
                    this.as -= 72;
                    hVar.b(this.as + 1, 179, 720, 179);
                    hVar.b(this.as, DirectGraphics.ROTATE_180, 720, DirectGraphics.ROTATE_180);
                    hVar.b(this.as + 1, 181, 720, 181);
                    return;
                } else if (b2 < 19) {
                    this.bN -= 18;
                    hVar.d(0, this.bN - 80, 720, ((180 - this.bN) << 1) + 160);
                    return;
                } else if (b2 == 19) {
                    hVar.d(-80, 0, 720, 360);
                    return;
                } else {
                    return;
                }
        }
    }

    public final void e(int i) {
        this.ce.b(i);
    }

    public final void g(h hVar) {
        for (int i = 0; i < this.ce.cu; i++) {
            this.ce.br[i].a(hVar, 0, 0);
        }
        for (int i2 = 0; i2 < this.ce.sj.length; i2++) {
            if (this.ce.sj[i2][4] == 0) {
                hVar.a(this.ce.dI[23], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
            } else {
                hVar.a(this.ce.dI[this.ce.sj[i2][2]], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
            }
            switch (this.ce.sj[i2][3]) {
                case 2:
                    hVar.a(this.ce.dI[16], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
                    break;
                case 3:
                    hVar.a(this.ce.dI[17], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
                    break;
                case 5:
                    hVar.a(this.ce.dI[19], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
                    break;
                case 6:
                    hVar.a(this.ce.dI[18], this.ce.sj[i2][0], this.ce.sj[i2][1], 20);
                    break;
            }
            if (this.o % 10 < 5) {
                hVar.a(this.ce.dI[20], this.ce.sj[this.ce.dD[2]][0], this.ce.sj[this.ce.dD[2]][1], 20);
            }
            if (this.o % 2 == 0) {
                hVar.a(this.ce.dI[21], this.ce.sj[this.ce.bB][0] - 2, this.ce.sj[this.ce.bB][1] - 2, 20);
            }
        }
        this.o++;
        for (int i3 = 0; i3 < 4; i3++) {
            this.ce.cN[i3 + 43 + 7].e(this.ce.sj[this.ce.dD[2]][0] - 2, this.ce.sj[this.ce.dD[2]][1] - 3);
            if (this.ce.cN[i3 + 43 + 7].cr) {
                this.ce.cN[i3 + 43 + 7].a(this.ce.rO[12], i.rP[12], hVar);
            }
        }
        if (this.o == 20) {
            this.o = 0;
        }
    }

    public final void i(h hVar) {
        int[] iArr = {0, 0, 546, 492, 493, 547};
        int[] iArr2 = {0, 169, PurchaseCode.CERT_PKI_ERR, PurchaseCode.APPLYCERT_CONFIG_ERR, 142, 223};
        switch (this.ce.bC) {
            case 0:
                a(0, hVar);
                if (this.ce.cx == 0) {
                    i iVar = this.ce.dM[this.ce.cA];
                    int height = 360 - this.ce.dM[this.ce.cA].getHeight();
                    hVar.a(iVar, 0, height, 20);
                    hVar.setColor(0);
                    int width = iVar.getWidth() / 10;
                    int height2 = iVar.getHeight();
                    for (int i = 0; i < 10; i++) {
                        hVar.d((width * i) + 0, height, width - ((this.q * width) / 10), height2);
                    }
                    if (this.q < 10) {
                        this.q++;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                a(0, hVar);
                return;
            case 2:
            case 3:
            case 4:
            case 5:
                if (this.ce.cx <= 0) {
                    a(hVar, this.ce.dN[0], 320, 40);
                    return;
                }
                return;
            case 6:
            case 7:
                if (this.ce.cx <= 0) {
                    a(hVar, this.ce.dN[0], 320, 40);
                    if (!this.ce.dh[this.ce.cu - 1].equals("/uidata/querentanchu2.uid")) {
                        b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                        return;
                    }
                    return;
                }
                return;
            case 8:
            case 17:
            case 18:
            case 24:
            case 25:
            case 45:
            case b.KEY_NUM3:
                if (this.ce.cx <= 0) {
                    a(hVar, this.ce.dN[0], 320, 40);
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                }
                return;
            case 9:
                if (this.ce.cx <= 0) {
                    a(hVar, this.ce.dN[0], 320, 40);
                    if (!this.ce.dh[this.ce.cu - 1].equals("/uidata/querentanchu2.uid")) {
                        b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                        return;
                    }
                    return;
                }
                return;
            case 10:
            case 11:
            case 12:
            case DefaultVirtualDevice.WIDGET_TYPE_CHECKIN /*13*/:
            case DefaultVirtualDevice.WIDGET_TYPE_SNSBUTTON /*14*/:
            case DefaultVirtualDevice.WIDGET_TYPE_DYNAMICJOYSTICK /*15*/:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 31:
            case 32:
            case 33:
            case 34:
            case b.KEY_POUND:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case b.KEY_STAR:
            case 43:
            case 47:
            case b.KEY_NUM0:
                if (this.ce.cx <= 0 && !this.ce.dh[this.ce.cu - 1].equals("/uidata/querentanchu2.uid")) {
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                }
                return;
            case 16:
            case 44:
            default:
                return;
            case 26:
            case 28:
                if (this.ce.cx <= 0) {
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                } else {
                    a(hVar, this.ce.dN[0], 320, 5);
                    return;
                }
            case 27:
            case 29:
            case 30:
                if (this.ce.cx <= 0) {
                    a(hVar, this.ce.dN[0], 320, 5);
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                }
                return;
            case 46:
                if (this.ce.cx <= 0 && !this.ce.dh[this.ce.cu - 1].equals("/uidata/querentanchu2.uid")) {
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                }
                return;
            case b.KEY_NUM1:
            case b.KEY_NUM2:
                a(0, hVar);
                if (this.ce.cx <= 0 && !this.ce.dh[this.ce.cu - 1].equals("/uidata/querentanchu2.uid")) {
                    b(hVar, this.ce.dM[this.ce.cA], iArr[this.ce.cA], iArr2[this.ce.cA]);
                    return;
                }
                return;
        }
    }

    public final void run() {
        while (!this.ao) {
            try {
                this.cD = System.currentTimeMillis();
                this.ce.c();
                aC();
                aD();
                this.cE = System.currentTimeMillis();
                if (this.cE - this.cD < ((long) this.ag)) {
                    Thread.sleep((((long) this.ag) - this.cE) + this.cD);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.cC.t();
    }
}
