package me.gall.skuld.util;

import java.util.HashMap;

public class DataMapper<V> extends HashMap<Integer, V> {
    private static final long serialVersionUID = 1;

    public final DataMapper<V> b(V[] vArr) {
        if (vArr == null || vArr.length <= 0) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < vArr.length; i++) {
            put(Integer.valueOf(i + 1), vArr[i]);
        }
        return this;
    }
}
