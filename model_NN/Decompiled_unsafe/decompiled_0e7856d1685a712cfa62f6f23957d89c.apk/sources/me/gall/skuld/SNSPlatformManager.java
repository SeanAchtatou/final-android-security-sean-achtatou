package me.gall.skuld;

import android.app.Activity;
import android.util.Log;
import java.util.Map;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.Configuration;

public final class SNSPlatformManager {
    public static final String META_TAG_SKULD_ADAPTER_NAME = "SKULD_ADAPTER_NAME";
    private static final String TAG = "SNSPlatformManager";
    private static Activity context;
    private static SNSPlatformAdapter he;
    private static String hf;
    private static boolean hg;

    public static void a(Activity activity, String str, Map<String, String> map) {
        context = activity;
        hf = str;
        try {
            Class<?> cls = Class.forName("me.gall.skuld.adapter." + Configuration.getManifestMetaValue(context, META_TAG_SKULD_ADAPTER_NAME) + "Adapter");
            cls.getName();
            SNSPlatformAdapter sNSPlatformAdapter = (SNSPlatformAdapter) cls.newInstance();
            he = sNSPlatformAdapter;
            sNSPlatformAdapter.b(map);
            hg = true;
        } catch (Exception e) {
            Log.e(TAG, "The SDK is not initilized because of " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static SNSPlatformAdapter be() {
        if (hg) {
            return he;
        }
        throw new NullPointerException("SNSPlatformAdapter has not initilized. May be SKULD_ADAPTER_NAME in the manifest is wrong spelled or check the log for detail.");
    }

    public static Activity bf() {
        return context;
    }

    public static String bg() {
        return hf;
    }

    public static final void onDestroy() {
        if (hg) {
            he.onDestroy();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }

    public static final void onPause() {
        if (hg) {
            he.onPause();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }

    public static final void onResume() {
        if (hg) {
            he.onResume();
        } else {
            Log.w(TAG, "Skuld is not initilized yet.");
        }
    }
}
