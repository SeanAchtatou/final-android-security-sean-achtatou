package me.gall.skuld.adapter;

import android.os.AsyncTask;
import java.util.Map;

public interface SNSPlatformAdapter {
    public static final int LAUNCH_DASHBOARD = 0;
    public static final int LAUNCH_GAME_ARCHIEVEMENT = 3;
    public static final int LAUNCH_GAME_DETAIL = 1;
    public static final int LAUNCH_GAME_LEADERBOARD = 2;
    public static final int LAUNCH_GAME_OTHER = 4;

    public interface AsyncResultCallback<R> {
        void bk();

        void bl();
    }

    public interface AsyncWorkerAndResultCallback<R> extends AsyncResultCallback<R> {
        R bm();
    }

    public static class SkuldAsyncTask<R> extends AsyncTask<Void, Void, R> {
        private AsyncWorkerAndResultCallback<R> hp;
        private String hq;

        private R bn() {
            try {
                if (this.hp != null) {
                    return this.hp.bm();
                }
                throw new Exception("AsyncResultCallback could not be null");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public /* synthetic */ Object doInBackground(Object... objArr) {
            return bn();
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(R r) {
            if (r != null) {
                this.hp.bk();
                return;
            }
            AsyncWorkerAndResultCallback<R> asyncWorkerAndResultCallback = this.hp;
            String str = this.hq;
            asyncWorkerAndResultCallback.bl();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }
    }

    void b(Map<String, String> map);

    void onDestroy();

    void onPause();

    void onResume();

    void u(int i);
}
