package me.gall.skuld.adapter;

import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.util.Configuration;
import me.gall.skuld.util.DataMapper;

abstract class a implements SNSPlatformAdapter {
    public static final String META_TAG_SKULD_ADAPTER_APP_ID = "SKULD_ADAPTER_APP_ID";
    public static final String META_TAG_SKULD_ADAPTER_APP_KEY = "SKULD_ADAPTER_APP_KEY";
    public static final String META_TAG_SKULD_ADAPTER_APP_SECRET = "SKULD_ADAPTER_APP_SECRET";
    public static final String META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER = "SKULD_ARCHIEVEMENT_ID_MAPPER";
    public static final String META_TAG_SKULD_BILLING_ID_MAPPER = "SKULD_BILLING_ID_MAPPER";
    public static final String META_TAG_SKULD_LEADERBOARD_ID_MAPPER = "SKULD_LEADERBOARD_ID_MAPPER";
    private DataMapper<String> hr;
    private DataMapper<String> hs;
    private DataMapper<String> ht;
    private String hu;
    private String hv;
    private String hw;

    a() {
    }

    public void P(String str) {
        this.hv = str;
    }

    public void Q(String str) {
        this.hw = str;
    }

    public void a(DataMapper<String> dataMapper) {
        this.hs = dataMapper;
    }

    public void b(Map<String, String> map) {
        String manifestMetaValue = Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_LEADERBOARD_ID_MAPPER);
        if (manifestMetaValue != null) {
            c(new DataMapper().b(manifestMetaValue.split(";")));
        }
        String manifestMetaValue2 = Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER);
        if (manifestMetaValue2 != null) {
            a(new DataMapper().b(manifestMetaValue2.split(";")));
        }
        String manifestMetaValue3 = Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_BILLING_ID_MAPPER);
        if (manifestMetaValue3 != null) {
            b((DataMapper<String>) new DataMapper().b(manifestMetaValue3.split(";")));
        }
        setKey(Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_ADAPTER_APP_KEY));
        P(Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_ADAPTER_APP_SECRET));
        Q(Configuration.getManifestMetaValue(SNSPlatformManager.bf(), META_TAG_SKULD_ADAPTER_APP_ID));
    }

    public void b(DataMapper<String> dataMapper) {
        this.ht = dataMapper;
    }

    public String bh() {
        return this.hv;
    }

    public void c(DataMapper<String> dataMapper) {
        this.hr = dataMapper;
    }

    public String getId() {
        return this.hw;
    }

    public String getKey() {
        return this.hu;
    }

    public void setKey(String str) {
        this.hu = str;
    }
}
