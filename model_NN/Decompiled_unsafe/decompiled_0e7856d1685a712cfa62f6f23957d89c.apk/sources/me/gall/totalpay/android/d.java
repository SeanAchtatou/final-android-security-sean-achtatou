package me.gall.totalpay.android;

import android.content.Context;
import org.json.JSONObject;

public interface d {
    public static final String TYPE_ALIPAYWS = "alipayws";
    public static final String TYPE_LTHJ = "lthj";
    public static final String TYPE_MMIAP = "mmiap";
    public static final String TYPE_SMS = "sms";
    public static final String TYPE_VACSMS = "vacsms";
    public static final String TYPE_YEEPAYCP = "yeepaycp";

    JSONObject getBilling();

    Context getContext();

    void sendUIMessage(int i, String str);
}
