package me.gall.totalpay.android;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import com.a.a.h.b;
import com.a.a.h.f;
import com.umeng.common.b.e;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.microedition.media.control.ToneControl;
import org.json.JSONException;
import org.json.JSONObject;

public final class a {
    public static final int BILLING_STATE_CANCEL = 5;
    public static final int BILLING_STATE_COMPLETE = 1;
    public static final int BILLING_STATE_PAY_FAIL = 4;
    public static final int BILLING_STATE_REQUEST = 6;
    public static final int BILLING_STATE_REQUEST_FAIL = 2;
    public static final int BILLING_STATE_SIGN_FAIL = 3;
    public static final int BILLING_STATE_START = 0;
    private static final String HOST_PRODUCTION = "http://totalpay.savenumber.com/totalpayserver/";
    private static final String HOST_TEST = "http://test.gall.me/totalpayserver/";
    private static final String TP_BILLINGS = "TP_BILLINGS";
    private static a billingManager;
    /* access modifiers changed from: private */
    public Map<String, String> billingProperties;
    /* access modifiers changed from: private */
    public Context context;

    private a(Context context2) {
        this.context = context2;
    }

    private String createLocalBillingOrderId() {
        return "tplocal-" + UUID.randomUUID().toString();
    }

    public static String getBillingHost(Context context2) {
        return f.isTestMode(context2) ? HOST_TEST : HOST_PRODUCTION;
    }

    public static int getBillingId(JSONObject jSONObject) {
        return jSONObject.getInt("id");
    }

    public static String getBillingProductName(JSONObject jSONObject) {
        return jSONObject.getString("productname");
    }

    public static String getBillingProductType(JSONObject jSONObject) {
        return jSONObject.getString("producttype");
    }

    private SharedPreferences getBillings() {
        return this.context.getApplicationContext().getSharedPreferences(TP_BILLINGS, 0);
    }

    public static a getInstance(Context context2) {
        if (billingManager == null) {
            billingManager = new a(context2);
        }
        return billingManager;
    }

    /* access modifiers changed from: private */
    public void removeBilling(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("orderid");
            h.getLogName();
            "Remove billing by orderid:" + string;
            if (getBillings().contains(string)) {
                getBillings().edit().remove(string).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void addBillingProperty(String str, String str2) {
        if (this.billingProperties == null) {
            this.billingProperties = new HashMap();
        }
        this.billingProperties.put(str, str2);
    }

    public final JSONObject createBillingRequest(JSONObject jSONObject) {
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(String.valueOf(getBillingHost(this.context)) + "tpbilling");
            h.getLogName();
            "CREATE:" + url.toString();
            HttpURLConnection httpURLConnection2 = (HttpURLConnection) url.openConnection();
            try {
                httpURLConnection2.addRequestProperty("content-type", "application/json");
                httpURLConnection2.addRequestProperty("Auth", getInstance(this.context).signRequest(jSONObject));
                httpURLConnection2.setDoOutput(true);
                httpURLConnection2.setRequestMethod("POST");
                httpURLConnection2.setConnectTimeout(10000);
                OutputStream outputStream = httpURLConnection2.getOutputStream();
                h.getLogName();
                "CONTENT:" + jSONObject.toString();
                outputStream.write(jSONObject.toString().getBytes("utf-8"));
                outputStream.flush();
                outputStream.close();
                httpURLConnection2.connect();
                int responseCode = httpURLConnection2.getResponseCode();
                h.getLogName();
                "ResponseCode:" + responseCode;
                if (responseCode == 201) {
                    JSONObject jSONObject2 = new JSONObject(h.consumeStream(httpURLConnection2.getInputStream()));
                    jSONObject.put("id", jSONObject2.getInt("id"));
                    if (httpURLConnection2 != null) {
                        httpURLConnection2.disconnect();
                    }
                    return jSONObject2;
                }
                throw new IllegalArgumentException("responseCode:" + responseCode);
            } catch (Exception e) {
                Exception exc = e;
                httpURLConnection = httpURLConnection2;
                e = exc;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                httpURLConnection = httpURLConnection2;
                th = th3;
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            throw e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    public final JSONObject createProxyRequest(int i, String str, String str2, String str3, String str4) {
        JSONObject createRequestJson = createRequestJson(i, str, str2, str3, str4);
        try {
            createRequestJson.put("isProxy", true);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return createRequestJson;
    }

    public final JSONObject createRequestJson(int i, String str, String str2, String str3, String str4) {
        String str5;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("paymentamt", i);
            jSONObject.put("productname", str2);
            jSONObject.put("producttype", str3);
            jSONObject.put("uid", h.getDeviceUniqueID(this.context));
            jSONObject.put("cid", Integer.parseInt(str));
            jSONObject.put("app", this.context.getPackageName());
            jSONObject.put("sign", h.gainSignature(this.context));
            jSONObject.put("iccid", h.getDeviceICCID(this.context));
            jSONObject.put("imei", h.getDeviceIMEI(this.context));
            jSONObject.put("mac", h.getMacAddress(this.context));
            jSONObject.put("imsi", h.getDeviceIMSI(this.context));
            jSONObject.put("phonenumber", h.getDevicePhonenumber(this.context));
            jSONObject.put("osVersion", h.getOSVersion());
            jSONObject.put("brand", Build.MANUFACTURER);
            jSONObject.put("model", Build.MODEL);
            if (str4 == null) {
                str4 = "";
            }
            jSONObject.put("merOrderId", str4);
            jSONObject.put("createtime", System.currentTimeMillis());
            jSONObject.put("orderid", createLocalBillingOrderId());
            if (h.isWifiConnect(this.context)) {
                str5 = "wifi";
            } else if (h.isMobileConnect(this.context)) {
                str5 = h.getDataConnectionNetworkInfo(this.context);
                if (str5 == null) {
                    str5 = "unknown";
                }
            } else {
                str5 = "none";
            }
            jSONObject.put("network", str5);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        } catch (PackageManager.NameNotFoundException e3) {
            e3.printStackTrace();
        }
        return jSONObject;
    }

    public final void requestBillingOrderId(e eVar, final f.a aVar, final JSONObject jSONObject) {
        h.executeTask(new Thread() {
            /* JADX WARNING: Removed duplicated region for block: B:36:0x0175  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r6 = this;
                    r1 = 0
                    org.json.JSONObject r0 = r4     // Catch:{ Exception -> 0x017b }
                    java.lang.String r2 = "id"
                    boolean r0 = r0.has(r2)     // Catch:{ Exception -> 0x017b }
                    if (r0 == 0) goto L_0x001f
                    org.json.JSONObject r0 = r4     // Catch:{ Exception -> 0x017b }
                    java.lang.String r2 = "id"
                    boolean r0 = r0.has(r2)     // Catch:{ Exception -> 0x017b }
                    if (r0 == 0) goto L_0x0026
                    org.json.JSONObject r0 = r4     // Catch:{ Exception -> 0x017b }
                    java.lang.String r2 = "id"
                    int r0 = r0.getInt(r2)     // Catch:{ Exception -> 0x017b }
                    if (r0 != 0) goto L_0x0026
                L_0x001f:
                    me.gall.totalpay.android.a r0 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x017b }
                    org.json.JSONObject r2 = r4     // Catch:{ Exception -> 0x017b }
                    r0.createBillingRequest(r2)     // Catch:{ Exception -> 0x017b }
                L_0x0026:
                    java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x017b }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017b }
                    me.gall.totalpay.android.a r3 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x017b }
                    android.content.Context r3 = r3.context     // Catch:{ Exception -> 0x017b }
                    java.lang.String r3 = me.gall.totalpay.android.a.getBillingHost(r3)     // Catch:{ Exception -> 0x017b }
                    java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x017b }
                    r2.<init>(r3)     // Catch:{ Exception -> 0x017b }
                    java.lang.String r3 = "tpbilling/"
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x017b }
                    org.json.JSONObject r3 = r4     // Catch:{ Exception -> 0x017b }
                    int r3 = me.gall.totalpay.android.a.getBillingId(r3)     // Catch:{ Exception -> 0x017b }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x017b }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x017b }
                    r0.<init>(r2)     // Catch:{ Exception -> 0x017b }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x017b }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x017b }
                    java.lang.String r3 = "REQUEST:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x017b }
                    java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ Exception -> 0x017b }
                    r2.toString()     // Catch:{ Exception -> 0x017b }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x017b }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x017b }
                    java.lang.String r1 = "POST"
                    r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = "content-type"
                    java.lang.String r2 = "application/json"
                    r0.addRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = "Auth"
                    me.gall.totalpay.android.a r2 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    android.content.Context r2 = r2.context     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    me.gall.totalpay.android.a r2 = me.gall.totalpay.android.a.getInstance(r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    org.json.JSONObject r3 = r4     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r2 = r2.signRequest(r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r0.addRequestProperty(r1, r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1 = 1
                    r0.setDoOutput(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.<init>()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = "paymenttype"
                    org.json.JSONObject r3 = r4     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r4 = "paymenttype"
                    int r3 = r3.getInt(r4)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.put(r1, r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = "cliState"
                    r3 = 6
                    r2.put(r1, r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    me.gall.totalpay.android.a r1 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.util.Map r1 = r1.billingProperties     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    if (r1 == 0) goto L_0x00c7
                    me.gall.totalpay.android.a r1 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.util.Map r1 = r1.billingProperties     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.util.Set r1 = r1.keySet()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.util.Iterator r3 = r1.iterator()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                L_0x00c1:
                    boolean r1 = r3.hasNext()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    if (r1 != 0) goto L_0x0136
                L_0x00c7:
                    java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r4 = "utf-8"
                    byte[] r3 = r3.getBytes(r4)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.write(r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.flush()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.close()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r3 = "DATA:"
                    r1.<init>(r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.toString()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r0.connect()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r3 = "RESPONSE:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.toString()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2 = 200(0xc8, float:2.8E-43)
                    if (r1 != r2) goto L_0x015a
                    java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = me.gall.totalpay.android.h.consumeStream(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.<init>(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    org.json.JSONObject r1 = r4     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r3 = "orderid"
                    java.lang.String r4 = "orderid"
                    java.lang.String r2 = r2.getString(r4)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.put(r3, r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    com.a.a.h.f$a r1 = r3     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    org.json.JSONObject r2 = r4     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r1.requestBillingComplete(r2)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    if (r0 == 0) goto L_0x0135
                    r0.disconnect()
                L_0x0135:
                    return
                L_0x0136:
                    java.lang.Object r1 = r3.next()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    me.gall.totalpay.android.a r4 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.util.Map r4 = r4.billingProperties     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.Object r4 = r4.get(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.put(r1, r4)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    goto L_0x00c1
                L_0x014b:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                L_0x014f:
                    com.a.a.h.f$a r2 = r3     // Catch:{ all -> 0x0179 }
                    r2.requestBillingError(r0)     // Catch:{ all -> 0x0179 }
                    if (r1 == 0) goto L_0x0135
                    r1.disconnect()
                    goto L_0x0135
                L_0x015a:
                    java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r4 = "ResponseCode:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    r2.<init>(r1)     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                    throw r2     // Catch:{ Exception -> 0x014b, all -> 0x016f }
                L_0x016f:
                    r1 = move-exception
                    r5 = r1
                    r1 = r0
                    r0 = r5
                L_0x0173:
                    if (r1 == 0) goto L_0x0178
                    r1.disconnect()
                L_0x0178:
                    throw r0
                L_0x0179:
                    r0 = move-exception
                    goto L_0x0173
                L_0x017b:
                    r0 = move-exception
                    goto L_0x014f
                */
                throw new UnsupportedOperationException("Method not decompiled: me.gall.totalpay.android.a.AnonymousClass2.run():void");
            }
        });
    }

    public final void saveBilling(JSONObject jSONObject) {
        try {
            getBillings().edit().putString(jSONObject.getString("orderid"), jSONObject.toString()).commit();
            h.getLogName();
            "Save billing " + jSONObject.getString("orderid") + " at first.";
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public final String signRequest(JSONObject jSONObject) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(jSONObject.getInt("paymentamt"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getInt("cid"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("uid"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("imsi"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("merOrderId"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("sign"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("app"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("iccid"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("imei"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("mac"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("phonenumber"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("osVersion"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("brand"));
        stringBuffer.append(":::");
        stringBuffer.append(jSONObject.getString("model"));
        String stringBuffer2 = stringBuffer.toString();
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.reset();
        instance.update(stringBuffer2.getBytes(e.f));
        byte[] digest = instance.digest();
        StringBuffer stringBuffer3 = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                stringBuffer3.append(b.SMS_RESULT_SEND_SUCCESS).append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            } else {
                stringBuffer3.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            }
        }
        String stringBuffer4 = stringBuffer3.toString();
        StringBuffer stringBuffer5 = new StringBuffer();
        stringBuffer5.append(stringBuffer4);
        stringBuffer5.append("totalpay");
        String stringBuffer6 = stringBuffer5.toString();
        instance.reset();
        instance.update(stringBuffer6.getBytes(e.f));
        byte[] digest2 = instance.digest();
        StringBuffer stringBuffer7 = new StringBuffer();
        for (int i2 = 0; i2 < digest2.length; i2++) {
            if (Integer.toHexString(digest2[i2] & ToneControl.SILENCE).length() == 1) {
                stringBuffer7.append(b.SMS_RESULT_SEND_SUCCESS).append(Integer.toHexString(digest2[i2] & ToneControl.SILENCE));
            } else {
                stringBuffer7.append(Integer.toHexString(digest2[i2] & ToneControl.SILENCE));
            }
        }
        return stringBuffer7.toString();
    }

    public final void updateBillingClientState(final JSONObject jSONObject) {
        saveBilling(jSONObject);
        if (h.isConnectedOrConnecting(this.context)) {
            h.executeTask(new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:28:0x01b2  */
                /* JADX WARNING: Removed duplicated region for block: B:31:0x01b9  */
                /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public final void run() {
                    /*
                        r9 = this;
                        r7 = 201(0xc9, float:2.82E-43)
                        r1 = 0
                        org.json.JSONObject r0 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r2 = "id"
                        boolean r0 = r0.has(r2)     // Catch:{ Exception -> 0x01c2 }
                        if (r0 == 0) goto L_0x0148
                        org.json.JSONObject r0 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r2 = "id"
                        int r0 = r0.getInt(r2)     // Catch:{ Exception -> 0x01c2 }
                        if (r0 <= 0) goto L_0x0148
                        java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c2 }
                        me.gall.totalpay.android.a r3 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x01c2 }
                        android.content.Context r3 = r3.context     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = me.gall.totalpay.android.a.getBillingHost(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01c2 }
                        r0.<init>(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = "tpbilling/"
                        java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01c2 }
                        org.json.JSONObject r3 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r4 = "id"
                        int r3 = r3.getInt(r4)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01c2 }
                        r2.<init>(r0)     // Catch:{ Exception -> 0x01c2 }
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = "UPDATE:"
                        r0.<init>(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01c2 }
                        r0.toString()     // Catch:{ Exception -> 0x01c2 }
                        org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x01c2 }
                        r0.<init>()     // Catch:{ Exception -> 0x01c2 }
                        org.json.JSONObject r3 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r4 = "cliState"
                        int r3 = r3.getInt(r4)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r4 = "paymenttype"
                        org.json.JSONObject r5 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r6 = "paymenttype"
                        int r5 = r5.getInt(r6)     // Catch:{ Exception -> 0x01c2 }
                        r0.put(r4, r5)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r4 = "paymentActAmt"
                        org.json.JSONObject r5 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r6 = "paymentActAmt"
                        int r5 = r5.getInt(r6)     // Catch:{ Exception -> 0x01c2 }
                        r0.put(r4, r5)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r4 = "cliState"
                        r0.put(r4, r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = "orderid"
                        org.json.JSONObject r4 = r2     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r5 = "orderid"
                        java.lang.String r4 = r4.getString(r5)     // Catch:{ Exception -> 0x01c2 }
                        r0.put(r3, r4)     // Catch:{ Exception -> 0x01c2 }
                        r8 = r0
                        r0 = r2
                        r2 = r8
                    L_0x0096:
                        java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x01c2 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01c2 }
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = "CONTENT:"
                        r1.<init>(r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1 = 10000(0x2710, float:1.4013E-41)
                        r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r1 = "content-type"
                        java.lang.String r3 = "application/json"
                        r0.addRequestProperty(r1, r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r1 = "Auth"
                        me.gall.totalpay.android.a r3 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        org.json.JSONObject r4 = r2     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = r3.signRequest(r4)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r0.addRequestProperty(r1, r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1 = 1
                        r0.setDoOutput(r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.io.OutputStream r1 = r0.getOutputStream()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = "utf-8"
                        byte[] r2 = r2.getBytes(r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.write(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.flush()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.close()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r0.connect()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = "ResponseCode:"
                        r2.<init>(r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r2.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r2 = 200(0xc8, float:2.8E-43)
                        if (r1 == r2) goto L_0x0103
                        if (r1 != r7) goto L_0x0186
                    L_0x0103:
                        if (r1 != r7) goto L_0x0138
                        org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r2 = me.gall.totalpay.android.h.consumeStream(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.<init>(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        org.json.JSONObject r2 = r2     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = "id"
                        java.lang.String r4 = "id"
                        int r1 = r1.getInt(r4)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r2.put(r3, r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r2 = "It is a offline billing."
                        r1.<init>(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        org.json.JSONObject r2 = r2     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r3 = "id"
                        int r2 = r2.getInt(r3)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                    L_0x0138:
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        me.gall.totalpay.android.a r1 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        org.json.JSONObject r2 = r2     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r1.removeBilling(r2)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        if (r0 == 0) goto L_0x0147
                        r0.disconnect()
                    L_0x0147:
                        return
                    L_0x0148:
                        java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c2 }
                        me.gall.totalpay.android.a r3 = me.gall.totalpay.android.a.this     // Catch:{ Exception -> 0x01c2 }
                        android.content.Context r3 = r3.context     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = me.gall.totalpay.android.a.getBillingHost(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x01c2 }
                        r0.<init>(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = "tpbilling"
                        java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01c2 }
                        r2.<init>(r0)     // Catch:{ Exception -> 0x01c2 }
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = "CREATE:"
                        r0.<init>(r3)     // Catch:{ Exception -> 0x01c2 }
                        java.lang.String r3 = r2.toString()     // Catch:{ Exception -> 0x01c2 }
                        java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x01c2 }
                        r0.toString()     // Catch:{ Exception -> 0x01c2 }
                        org.json.JSONObject r0 = r2     // Catch:{ Exception -> 0x01c2 }
                        r8 = r0
                        r0 = r2
                        r2 = r8
                        goto L_0x0096
                    L_0x0186:
                        java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r4 = "Not valid response:"
                        r3.<init>(r4)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        r2.<init>(r1)     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                        throw r2     // Catch:{ Exception -> 0x019b, all -> 0x01bd }
                    L_0x019b:
                        r1 = move-exception
                        r8 = r1
                        r1 = r0
                        r0 = r8
                    L_0x019f:
                        me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x01b6 }
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b6 }
                        java.lang.String r3 = "Upload fail:"
                        r2.<init>(r3)     // Catch:{ all -> 0x01b6 }
                        java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x01b6 }
                        r0.toString()     // Catch:{ all -> 0x01b6 }
                        if (r1 == 0) goto L_0x0147
                        r1.disconnect()
                        goto L_0x0147
                    L_0x01b6:
                        r0 = move-exception
                    L_0x01b7:
                        if (r1 == 0) goto L_0x01bc
                        r1.disconnect()
                    L_0x01bc:
                        throw r0
                    L_0x01bd:
                        r1 = move-exception
                        r8 = r1
                        r1 = r0
                        r0 = r8
                        goto L_0x01b7
                    L_0x01c2:
                        r0 = move-exception
                        goto L_0x019f
                    */
                    throw new UnsupportedOperationException("Method not decompiled: me.gall.totalpay.android.a.AnonymousClass1.run():void");
                }
            });
        } else {
            h.getLogName();
        }
    }

    public final void updateBillingClientState(JSONObject jSONObject, int i) {
        updateBillingClientState(jSONObject, i, 0, null);
    }

    public final void updateBillingClientState(JSONObject jSONObject, int i, int i2) {
        updateBillingClientState(jSONObject, i, i2, null);
    }

    public final void updateBillingClientState(JSONObject jSONObject, int i, int i2, String str) {
        if (i <= 0 && i >= 6) {
            throw new IllegalStateException("cliState could not be " + i);
        } else if (!jSONObject.has("paymenttype")) {
            throw new IllegalArgumentException("paymenttype has not be setting.");
        } else if (!jSONObject.has("orderid")) {
            throw new IllegalArgumentException("orderid has not be setting.");
        } else if (i != 1 || i2 > 0) {
            try {
                jSONObject.put("cliState", i);
                jSONObject.put("paymentActAmt", i2);
                if (str != null) {
                    jSONObject.put("failRemark", str);
                }
                updateBillingClientState(jSONObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            throw new IllegalArgumentException("paymentActAmt could not be " + i2);
        }
    }

    public final void updateBillingClientState(JSONObject jSONObject, int i, String str) {
        updateBillingClientState(jSONObject, i, 0, str);
    }

    public final void uploadRemainBillings() {
        Map<String, ?> all = getBillings().getAll();
        h.getLogName();
        "Remain:" + all.size();
        for (String str : all.keySet()) {
            try {
                updateBillingClientState(new JSONObject((String) all.get(str)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
