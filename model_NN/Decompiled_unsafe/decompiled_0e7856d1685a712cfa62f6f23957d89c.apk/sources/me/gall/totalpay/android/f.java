package me.gall.totalpay.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.a.a.h.b;
import java.io.FileNotFoundException;
import java.io.InputStream;

public final class f {
    public static final int CARRIER_CMCC = 1;
    public static final int CARRIER_TELECOM = 3;
    public static final int CARRIER_UNICOM = 2;
    public static final int CARRIER_UNKNOWN = 0;
    private static final int DISABLE = 2;
    private static final int ENABLE = 1;
    private static final int INIT = 0;
    private static final String NOTIFICATION_TEXT = "NOTIFICATION_TEXT";
    private static final int NOT_CONNECTED = 3;
    private static final String PAY_CHARGES_FIRST = "NOTIFICATION_ENABLE";
    private static final String TEST_MODE = "TP_TEST_MODE";
    static final String TP_BLACKLIST_DISABLE = "TP_BLACKLIST_DISABLE";
    private static final String TP_CHANNEL_ID = "TP_CHANNEL_ID";
    public static final String TYPE_ALL = "all";
    public static final String TYPE_CHARGES = "charges";
    public static final String TYPE_ESCROW = "escrow";
    /* access modifiers changed from: private */
    public static volatile int channel_valid = 0;
    private static Activity context;
    private static e currentPayment;
    private static a listener;

    public interface a {
        public static final String APN_PROBLEM = "APN problem.";
        public static final String LBE_PROBLEM = "LBE安全大师";
        public static final String NETWORK_PROBLEM = "Network problem.";
        public static final String OTHER_PROBLEM = "Other problem.";
        public static final String PERMISSION_PROBLEM = "Permission problem.";
        public static final String TIMEOUT_PROBLEM = "Timout problem.";

        void onCancel();

        void onComplete();

        void onFail(String str, e eVar);

        void onSuccess(e eVar);
    }

    public static void destroy() {
        h.finishTask();
    }

    static final void detectCIdStatus(final Context context2, final String str) {
        h.executeTask(new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:29:0x0181  */
            /* JADX WARNING: Removed duplicated region for block: B:37:0x01a4  */
            /* JADX WARNING: Removed duplicated region for block: B:43:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r9 = this;
                    r2 = 1
                    r1 = 0
                    java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x01aa }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
                    android.content.Context r4 = r1     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = me.gall.totalpay.android.a.getBillingHost(r4)     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x01aa }
                    r3.<init>(r4)     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = "channel/"
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = r2     // Catch:{ Exception -> 0x01aa }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x01aa }
                    r0.<init>(r3)     // Catch:{ Exception -> 0x01aa }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x01aa }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = "URL:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x01aa }
                    java.lang.String r4 = r0.toString()     // Catch:{ Exception -> 0x01aa }
                    java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x01aa }
                    r3.toString()     // Catch:{ Exception -> 0x01aa }
                    java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x01aa }
                    java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x01aa }
                    r1 = 10000(0x2710, float:1.4013E-41)
                    r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r0.connect()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r4 = "ResponseCode:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r3.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r3 = 404(0x194, float:5.66E-43)
                    if (r1 == r3) goto L_0x0066
                    r3 = 503(0x1f7, float:7.05E-43)
                    if (r1 != r3) goto L_0x0099
                L_0x0066:
                    java.lang.String r1 = me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = "channelId:"
                    r2.<init>(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = r2     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = " is disable or missing."
                    java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.util.Log.w(r1, r2)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1 = 2
                    me.gall.totalpay.android.f.channel_valid = r1     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                L_0x0088:
                    android.content.Context r1 = r1     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = r2     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    int r3 = me.gall.totalpay.android.f.channel_valid     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    me.gall.totalpay.android.f.saveChannelIdStatus(r1, r2, r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    if (r0 == 0) goto L_0x0098
                    r0.disconnect()
                L_0x0098:
                    return
                L_0x0099:
                    r3 = 200(0xc8, float:2.8E-43)
                    if (r1 != r3) goto L_0x0189
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = "channelId:"
                    r1.<init>(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = r2     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = " is work."
                    java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1 = 1
                    me.gall.totalpay.android.f.channel_valid = r1     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = me.gall.totalpay.android.h.consumeStream(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.<init>(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r3 = "channelName"
                    java.lang.String r3 = r1.getString(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r4 = "isPopMsg"
                    int r4 = r1.getInt(r4)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r5 = "popMsg"
                    java.lang.String r5 = r1.getString(r5)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.Context r1 = r1     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.SharedPreferences r1 = me.gall.totalpay.android.h.getSetting(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.SharedPreferences$Editor r6 = r1.edit()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r7 = "NOTIFICATION_ENABLE"
                    if (r4 != 0) goto L_0x0186
                    r1 = r2
                L_0x00e8:
                    android.content.SharedPreferences$Editor r1 = r6.putBoolean(r7, r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.commit()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.<init>(r2)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = " NOTIFICATION_ENABLE:"
                    java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    if (r4 != 0) goto L_0x0088
                    int r1 = r5.length()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    if (r1 <= 0) goto L_0x0088
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = java.lang.String.valueOf(r3)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.<init>(r2)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = " NOTIFICATION_TEXT:"
                    java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.Context r1 = r1     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.SharedPreferences r1 = me.gall.totalpay.android.h.getSetting(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r2 = "NOTIFICATION_TEXT"
                    android.content.SharedPreferences$Editor r1 = r1.putString(r2, r5)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r1.commit()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    goto L_0x0088
                L_0x013e:
                    r1 = move-exception
                    r8 = r1
                    r1 = r0
                    r0 = r8
                L_0x0142:
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x01a8 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a8 }
                    java.lang.String r3 = "Network problem:"
                    r2.<init>(r3)     // Catch:{ all -> 0x01a8 }
                    java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x01a8 }
                    r0.toString()     // Catch:{ all -> 0x01a8 }
                    android.content.Context r0 = r1     // Catch:{ all -> 0x01a8 }
                    java.lang.String r2 = r2     // Catch:{ all -> 0x01a8 }
                    int r0 = me.gall.totalpay.android.f.getChannelStateById(r0, r2)     // Catch:{ all -> 0x01a8 }
                    me.gall.totalpay.android.f.channel_valid = r0     // Catch:{ all -> 0x01a8 }
                    me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x01a8 }
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x01a8 }
                    java.lang.String r2 = "channelId:"
                    r0.<init>(r2)     // Catch:{ all -> 0x01a8 }
                    java.lang.String r2 = r2     // Catch:{ all -> 0x01a8 }
                    java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x01a8 }
                    java.lang.String r2 = " is "
                    java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x01a8 }
                    int r2 = me.gall.totalpay.android.f.channel_valid     // Catch:{ all -> 0x01a8 }
                    java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x01a8 }
                    r0.toString()     // Catch:{ all -> 0x01a8 }
                    if (r1 == 0) goto L_0x0098
                    r1.disconnect()
                    goto L_0x0098
                L_0x0186:
                    r1 = 0
                    goto L_0x00e8
                L_0x0189:
                    java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r4 = "Not expected response:"
                    r3.<init>(r4)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    r2.<init>(r1)     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                    throw r2     // Catch:{ Exception -> 0x013e, all -> 0x019e }
                L_0x019e:
                    r1 = move-exception
                    r8 = r1
                    r1 = r0
                    r0 = r8
                L_0x01a2:
                    if (r1 == 0) goto L_0x01a7
                    r1.disconnect()
                L_0x01a7:
                    throw r0
                L_0x01a8:
                    r0 = move-exception
                    goto L_0x01a2
                L_0x01aa:
                    r0 = move-exception
                    goto L_0x0142
                */
                throw new UnsupportedOperationException("Method not decompiled: me.gall.totalpay.android.f.AnonymousClass2.run():void");
            }
        });
    }

    public static void disableBlacklist(Context context2) {
        h.getSetting(context2).edit().putBoolean(TP_BLACKLIST_DISABLE, true).commit();
    }

    public static String getChannelId(Context context2) {
        String valueOf = String.valueOf(h.getManifestMetaIntValue(context2, TP_CHANNEL_ID, 0));
        if (valueOf != null && !valueOf.equals(b.SMS_RESULT_SEND_SUCCESS)) {
            return valueOf;
        }
        if (h.getSetting(context2).contains(TP_CHANNEL_ID)) {
            return h.getSetting(context2).getString(TP_CHANNEL_ID, null);
        }
        Log.e(h.getLogName(), "TP_CHANNEL_ID has not been setup yet.");
        return null;
    }

    /* access modifiers changed from: private */
    public static int getChannelStateById(Context context2, String str) {
        return h.getSetting(context2).getInt("CHANNEL_STATE_" + str, 1);
    }

    protected static final Object getMetaInfo(Context context2, String str) {
        return currentPayment != null ? currentPayment.getParam(str) : "";
    }

    private static String getPaymentNotificationText(Context context2, int i) {
        return h.getSetting(context2).getString(NOTIFICATION_TEXT, "继续游戏体验更多精彩内容，支付$PRICE元，激活游戏功能。").replace("$PRICE", h.convertPriceIntoString(i));
    }

    protected static a getTotalPayListener() {
        return listener;
    }

    public static void init(final Activity activity) {
        context = activity;
        h.detectCarrier(activity);
        validateRuntime(activity);
        if (h.isConnectedOrConnecting(activity)) {
            h.executeTask(new Runnable() {
                public final void run() {
                    String channelId = f.getChannelId(activity);
                    f.detectCIdStatus(activity, channelId);
                    b.getInstance(activity).updateLocalCache(channelId, true);
                    a.getInstance(activity).uploadRemainBillings();
                    b.uploadRemainPaymentResults(activity);
                }
            });
        }
    }

    public static void init(Activity activity, String str, boolean z) {
        setChannelId(activity, str);
        setTestMode(activity, z);
        init(activity);
    }

    private static void initByCarrier() {
    }

    public static boolean isTestMode(Context context2) {
        return h.containsManifestMetaKey(context2, TEST_MODE) ? h.getManifestMetaBooleanValue(context2, TEST_MODE, false) : h.getSetting(context2).getBoolean(TEST_MODE, false);
    }

    public static synchronized void pay(int i, String str, String str2, String str3, String str4, String str5, a aVar) {
        synchronized (f.class) {
            String channelId = getChannelId(context);
            if (channelId == null) {
                throw new g();
            }
            if (channel_valid == 0) {
                channel_valid = getChannelStateById(context, channelId);
            }
            if (channel_valid == 2) {
                throw new g();
            }
            String paymentNotificationText = getPaymentNotificationText(context, i);
            listener = aVar;
            Intent intent = new Intent();
            if (paymentNotificationText != null) {
                if (str4 == null) {
                    str4 = paymentNotificationText;
                }
                intent.putExtra("TEXT", str4);
            }
            if (i <= 0) {
                throw new IllegalArgumentException("price is invalid:" + i);
            }
            intent.putExtra("PRICE", i);
            if (str3 == null) {
                str3 = TYPE_ALL;
            }
            if (str3.equals(TYPE_ALL) || str3.equals(TYPE_CHARGES) || str3.equals(TYPE_ESCROW)) {
                intent.putExtra("TYPE", str3);
                intent.putExtra("PRODUCT", str);
                intent.putExtra("PRODUCTTYPE", str2);
                intent.putExtra("MERCHANTORDERID", str5);
                intent.putExtra("PAYCHARGESFIRST", h.getSetting(context).getBoolean(PAY_CHARGES_FIRST, true));
                intent.setClass(context, PaymentActivity.class);
                context.startActivity(intent);
            } else {
                throw new IllegalArgumentException("paymenttype is invalid:" + str3);
            }
        }
    }

    public static synchronized void pay(int i, String str, String str2, String str3, a aVar) {
        synchronized (f.class) {
            pay(i, str, null, str2, str3, null, aVar);
        }
    }

    public static synchronized void pay(int i, String str, a aVar) {
        synchronized (f.class) {
            pay(i, null, str, null, aVar);
        }
    }

    public static synchronized void pay(int i, a aVar) {
        synchronized (f.class) {
            pay(i, null, null, null, aVar);
        }
    }

    /* access modifiers changed from: private */
    public static void saveChannelIdStatus(Context context2, String str, int i) {
        h.getSetting(context2).edit().putInt("CHANNEL_STATE_" + str, i).commit();
    }

    public static void setChannelId(Context context2, String str) {
        h.getSetting(context2).edit().putString(TP_CHANNEL_ID, str).commit();
    }

    public static void setTestMode(Context context2, boolean z) {
        h.getSetting(context2).edit().putBoolean(TEST_MODE, z).commit();
    }

    private static void validateRuntime(Context context2) {
        try {
            h.getDrawableIdentifier(context2, "tp_img_top");
            h.getLogName();
            h.getLayoutIdentifier(context2, "tp_layout_multipay");
            h.getLayoutIdentifier(context2, "alipay_product_item");
            h.getLayoutIdentifier(context2, "alipay_title_320_480");
            h.getLayoutIdentifier(context2, "tp_yeepaycp_main");
            h.getLogName();
            h.getStringIdentifier(context2, "tp_label_titlebar");
            h.getStringIdentifier(context2, "confirm_install");
            h.getLogName();
            InputStream openFile = b.getInstance(context2).openFile("totalpay.conf");
            if (openFile == null) {
                throw new FileNotFoundException("totalpay.conf is not found in assets.");
            }
            openFile.close();
            InputStream openFile2 = b.getInstance(context2).openFile("escrow.conf");
            if (openFile2 == null) {
                throw new FileNotFoundException("escrow.conf is not found in assets.");
            }
            openFile2.close();
            InputStream openFile3 = b.getInstance(context2).openFile("blacklist.conf");
            if (openFile3 == null) {
                throw new FileNotFoundException("blacklist.conf is not found in assets.");
            }
            openFile3.close();
            InputStream openFile4 = b.getInstance(context2).openFile("20121018133442msp.apk");
            if (openFile4 == null) {
                throw new FileNotFoundException("alipay plugin is not found in assets.");
            }
            openFile4.close();
            h.getLogName();
        } catch (Exception e) {
            Log.e(h.getLogName(), "Totalpay runtime is not valid. Some resource is missing." + e);
            throw e;
        }
    }
}
