package me.gall.totalpay.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.a.a.h.a;
import com.a.a.h.b;
import com.a.a.h.d;
import com.a.a.h.e;
import com.a.a.h.f;
import com.a.a.h.g;
import com.a.a.h.h;
import com.a.a.h.i;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import me.gall.totalpay.android.f;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PaymentActivity extends Activity implements Handler.Callback, d {
    public static final int HIDE_PROCESSING_DIALOG = 2;
    private static final int PAY_CANCEL_TOAST = 6;
    private static final int PAY_DISABLE_TOAST = 7;
    private static final int PAY_FAIL_LBE_TOAST = 11;
    private static final int PAY_FAIL_TIMOUT_TOAST = 10;
    private static final int PAY_FAIL_TOAST = 5;
    private static final int PAY_SUCCESS_TOAST = 4;
    private static final int SHOW_CONNECT_DIALOG = 9;
    private static final int SHOW_INTRO_UI = 8;
    private static final int SHOW_MULTIPAY_UI = 3;
    public static final int SHOW_PROCESSING_DIALOG = 1;
    private static final String[] chargesTypeImages = {"null", "tp_img_cmcc", "tp_img_unicom", "tp_img_telecom"};
    private boolean backPressed = false;
    /* access modifiers changed from: private */
    public JSONObject billing;
    /* access modifiers changed from: private */
    public ArrayList<e> chargesPayments = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean containChargesPayment;
    private e currentPayment;
    private f currentPaymentAdapter;
    private ArrayList<e> escrowPayments = new ArrayList<>();
    /* access modifiers changed from: private */
    public Handler handler;
    /* access modifiers changed from: private */
    public boolean hasAnySuccessPaid;
    /* access modifiers changed from: private */
    public boolean isCheckingNetwork;
    /* access modifiers changed from: private */
    public c listener;
    private String mer_orderId;
    /* access modifiers changed from: private */
    public boolean payChargesFirstAutoTry = false;
    /* access modifiers changed from: private */
    public String pay_type;
    /* access modifiers changed from: private */
    public int paymentActAmt = 0;
    /* access modifiers changed from: private */
    public int price;
    private String productName;
    private String productType;
    private ProgressDialog progressDialog;
    private String text;

    private void cancelPayment() {
        cancelPayment(null);
    }

    /* access modifiers changed from: private */
    public void cancelPayment(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(6, str));
        dismissRunningProgress();
        try {
            this.billing.put("paymenttype", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        a.getInstance(this).updateBillingClientState(this.billing, 5);
        if (f.getTotalPayListener() != null) {
            f.getTotalPayListener().onCancel();
        }
        finish();
    }

    /* access modifiers changed from: private */
    public void continuePayBillingMode() {
        AnonymousClass11 r1 = new f.a() {
            public final void onCancel() {
                a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 5, PaymentActivity.this.paymentActAmt);
                h.getLogName();
                PaymentActivity.this.cancelPayment(null);
            }

            public final void onComplete() {
                Log.e(h.getLogName(), "Should not call onComplete directly!");
            }

            public final void onFail(String str, e eVar) {
                PaymentActivity.this.handler.sendEmptyMessage(5);
                PaymentActivity.this.dismissRunningProgress();
                h.getLogName();
                "billingPaymentListener onFail:" + eVar.getId();
                a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 4, PaymentActivity.this.paymentActAmt, str);
                if (f.getTotalPayListener() != null) {
                    f.getTotalPayListener().onFail(str, eVar);
                }
                PaymentActivity.this.finish();
            }

            public final void onSuccess(e eVar) {
                PaymentActivity paymentActivity = PaymentActivity.this;
                paymentActivity.paymentActAmt = paymentActivity.paymentActAmt + eVar.getPrice();
                PaymentActivity.this.handler.sendEmptyMessage(4);
                PaymentActivity.this.dismissRunningProgress();
                h.getLogName();
                "billingPaymentListener onSuccess:" + eVar.getId();
                a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 1, PaymentActivity.this.paymentActAmt);
                if (f.getTotalPayListener() != null) {
                    f.getTotalPayListener().onSuccess(eVar);
                    f.getTotalPayListener().onComplete();
                }
                PaymentActivity.this.finish();
            }
        };
        try {
            this.billing.put("paymenttype", this.currentPaymentAdapter.getPaymentType());
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.currentPaymentAdapter.pay(this, this.currentPayment, r1);
    }

    /* access modifiers changed from: private */
    public void dismissRunningProgress() {
        this.handler.sendEmptyMessage(2);
    }

    private List<e> initOfflinePayments() {
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray jSONArray = new JSONArray(h.consumeStream(b.getInstance(this).openFile("totalpay.conf")));
            h.getLogName();
            "There are " + jSONArray.length() + " payments in all.";
            for (int i = 0; i < jSONArray.length(); i++) {
                e eVar = new e(jSONArray.getJSONObject(i), 0);
                if (b.isPaymentConsumed(this, eVar)) {
                    h.getLogName();
                    "FID[" + eVar.getId() + "]:" + eVar.getRemark() + " is not repeatable and consumed.";
                } else if (!h.isPaymentAvailableForCurrentCarrier(eVar)) {
                    h.getLogName();
                    "FID[" + eVar.getId() + "]:" + eVar.getRemark() + " is not available current carrier or network.";
                } else {
                    arrayList.add(eVar);
                    h.getLogName();
                    "FID[" + eVar.getId() + "]:" + eVar.getRemark() + " has added.";
                }
            }
            h.getLogName();
        } catch (Exception e) {
            Log.w(h.getLogName(), "Init totalpay.conf error." + e);
        }
        try {
            JSONArray jSONArray2 = new JSONArray(h.consumeStream(b.getInstance(this).openFile("escrow.conf")));
            h.getLogName();
            "There are " + jSONArray2.length() + " payments in all.";
            for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                arrayList.add(new e(jSONArray2.getJSONObject(i2), 1));
            }
            h.getLogName();
        } catch (Exception e2) {
            Log.w(h.getLogName(), "Init escrow.conf error." + e2);
        }
        return arrayList;
    }

    private boolean isMMIAPPaymentType(List<e> list) {
        for (e next : list) {
            if (next.getMode() == 0 && next.getType().equals(d.TYPE_MMIAP)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void offlineRequest(String str, int i) {
        h.getLogName();
        for (e next : initOfflinePayments()) {
            if (next.getMode() == 0) {
                if (next.getPrice() == i) {
                    this.chargesPayments.add(next);
                    i -= next.getPrice();
                    h.getLogName();
                    "Offline HIT:" + next.getId() + " " + next.getPrice();
                } else if (next.getPrice() < i) {
                    this.chargesPayments.add(next);
                    i -= next.getPrice();
                    h.getLogName();
                    "Offline ADD:" + next.getId() + " " + next.getPrice();
                    h.getLogName();
                    "LocalPick LEFT:" + i;
                } else {
                    h.getLogName();
                    "Offline SKIP:" + next.getId() + " " + next.getPrice();
                }
            } else if (next.getMode() == 1) {
                h.getLogName();
                "Offline Escrow:" + next.getId();
                this.escrowPayments.add(next);
            }
        }
        requestComplete();
    }

    /* access modifiers changed from: private */
    public void onlineRequest(String str, int i) {
        showRunningProgress("正在获取支付信息");
        h.getLogName();
        try {
            JSONObject createBillingRequest = a.getInstance(this).createBillingRequest(this.billing);
            JSONArray jSONArray = createBillingRequest.getJSONArray(f.TYPE_CHARGES);
            h.getLogName();
            "There are " + jSONArray.length() + " charges in all.";
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                this.chargesPayments.add(new e(jSONArray.getJSONObject(i2), 0));
            }
            JSONArray jSONArray2 = createBillingRequest.getJSONArray("escrows");
            h.getLogName();
            "There are " + jSONArray2.length() + " escrow in all.";
            for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                this.escrowPayments.add(new e(jSONArray2.getJSONObject(i3), 1));
            }
            requestComplete();
        } catch (Exception e) {
            h.getLogName();
            "Create billing request failed." + e;
            this.handler.sendEmptyMessage(7);
            cancelPayment("网络连接错误");
        }
    }

    /* access modifiers changed from: private */
    public void requestComplete() {
        dismissRunningProgress();
        if (!this.chargesPayments.isEmpty() || !this.escrowPayments.isEmpty()) {
            if (this.payChargesFirstAutoTry && !this.chargesPayments.isEmpty() && !this.pay_type.equals(f.TYPE_ESCROW)) {
                h.getLogName();
                this.handler.post(new Runnable() {
                    public final void run() {
                        PaymentActivity.this.hasAnySuccessPaid = false;
                        PaymentActivity.this.payChargesMode();
                    }
                });
            } else if (!this.chargesPayments.isEmpty() && this.escrowPayments.isEmpty() && !this.pay_type.equals(f.TYPE_ESCROW)) {
                h.getLogName();
                this.handler.post(new Runnable() {
                    public final void run() {
                        PaymentActivity.this.hasAnySuccessPaid = false;
                        PaymentActivity.this.payChargesMode();
                    }
                });
            } else if (!this.chargesPayments.isEmpty() || this.escrowPayments.size() != 1 || this.pay_type.equals(f.TYPE_CHARGES)) {
                showMultiPaySelectView();
            } else {
                h.getLogName();
                this.handler.post(new Runnable() {
                    public final void run() {
                        PaymentActivity.this.payEscrowMode(0);
                    }
                });
            }
        } else if (!h.isConnectedOrConnecting(this)) {
            checkNetwork(new c() {
                public final void cancel() {
                    PaymentActivity.this.cancelPayment("网络连接不可用");
                }

                public final void connected() {
                    PaymentActivity.this.onlineRequest(PaymentActivity.this.pay_type, PaymentActivity.this.price);
                }

                public final void disconnected() {
                    PaymentActivity.this.checkNetwork(this);
                }
            });
        } else {
            cancelPayment("无可用计费");
        }
    }

    private void showMultiPaySelectView() {
        this.handler.sendEmptyMessage(3);
    }

    private void showRunningProgress() {
        showRunningProgress(null);
    }

    /* access modifiers changed from: private */
    public void showRunningProgress(String str) {
        this.handler.sendMessage(this.handler.obtainMessage(1, str));
    }

    private void startRequestPayments(final int i, final String str) {
        h.getLogName();
        this.chargesPayments.clear();
        this.escrowPayments.clear();
        this.paymentActAmt = 0;
        this.billing = a.getInstance(this).createRequestJson(i, f.getChannelId(this), this.productName, this.productType, this.mer_orderId);
        h.executeTask(new Runnable() {
            public final void run() {
                PaymentActivity.this.showRunningProgress("正在检查网络");
                boolean testConnection = h.testConnection(PaymentActivity.this);
                PaymentActivity.this.dismissRunningProgress();
                if (testConnection) {
                    PaymentActivity.this.onlineRequest(str, i);
                } else {
                    PaymentActivity.this.offlineRequest(str, i);
                }
            }
        });
    }

    public void checkNetwork(c cVar) {
        h.getLogName();
        dismissRunningProgress();
        this.listener = cVar;
        this.handler.sendMessage(this.handler.obtainMessage(9));
    }

    public JSONObject getBilling() {
        return this.billing;
    }

    public Context getContext() {
        return this;
    }

    public boolean handleMessage(Message message) {
        switch (message.what) {
            case 1:
                if (this.progressDialog == null) {
                    this.progressDialog = new ProgressDialog(this);
                    this.progressDialog.setCancelable(false);
                }
                this.progressDialog.setMessage(message.obj == null ? "请稍候" : (String) message.obj);
                this.progressDialog.show();
                h.getLogName();
                break;
            case 2:
                if (this.progressDialog != null && this.progressDialog.isShowing()) {
                    this.progressDialog.hide();
                    h.getLogName();
                    break;
                }
            case 3:
                h.getLogName();
                try {
                    ViewGroup viewGroup = (ViewGroup) h.inflateView(this, "tp_layout_multipay");
                    setContentView(viewGroup);
                    if (this.text != null) {
                        ((TextView) h.findViewByName(this, viewGroup, "tp_intro_msg")).setText(this.text);
                    }
                    if (this.productName != null) {
                        ((TextView) h.findViewByName(this, viewGroup, "tp_intro_product")).setText(String.valueOf(getString(h.getStringIdentifier(this, "tp_label_product"))) + this.productName);
                    }
                    ((TextView) h.findViewByName(this, viewGroup, "tp_intro_price")).setText(Html.fromHtml(String.valueOf(getString(h.getStringIdentifier(this, "tp_label_price"))) + "<b>" + h.convertPriceIntoString(this.price) + "</b>" + getString(h.getStringIdentifier(this, "tp_label_price_unit"))));
                    ListView listView = (ListView) h.findViewByName(this, viewGroup, "tp_list_content");
                    ArrayList arrayList = new ArrayList();
                    if (!this.chargesPayments.isEmpty() && !this.pay_type.equals(f.TYPE_ESCROW)) {
                        if (h.getCurrentCarrier() != 1 || !isMMIAPPaymentType(this.chargesPayments)) {
                            arrayList.add(Integer.valueOf(h.getDrawableIdentifier(this, chargesTypeImages[h.getCurrentCarrier()])));
                        } else {
                            arrayList.add(Integer.valueOf(h.getDrawableIdentifier(this, "tp_img_mmiap")));
                        }
                        this.containChargesPayment = true;
                    }
                    if (!this.pay_type.equals(f.TYPE_CHARGES)) {
                        Iterator<e> it = this.escrowPayments.iterator();
                        while (it.hasNext()) {
                            arrayList.add(Integer.valueOf(h.getDrawableIdentifier(this, "tp_img_" + it.next().getType())));
                        }
                    }
                    h.getLogName();
                    "All showing payment " + arrayList.size();
                    listView.setAdapter((ListAdapter) new ArrayAdapter<Integer>(this, h.getLayoutIdentifier(this, "tp_layout_listitem_img"), arrayList) {
                        public final View getView(int i, View view, ViewGroup viewGroup) {
                            View view2;
                            if (view == null) {
                                try {
                                    view2 = h.inflateView(PaymentActivity.this, "tp_layout_listitem_img");
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                ((ImageView) view2).setImageResource(((Integer) getItem(i)).intValue());
                                return view2;
                            }
                            view2 = view;
                            ((ImageView) view2).setImageResource(((Integer) getItem(i)).intValue());
                            return view2;
                        }
                    });
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                            if (!PaymentActivity.this.containChargesPayment) {
                                PaymentActivity.this.payEscrowMode(i);
                            } else if (i == 0) {
                                PaymentActivity.this.hasAnySuccessPaid = false;
                                PaymentActivity.this.payChargesMode();
                            } else {
                                PaymentActivity.this.payEscrowMode(i - 1);
                            }
                        }
                    });
                    setListViewHeightBasedOnChildren(listView);
                    ScrollView scrollView = (ScrollView) h.findViewByName(this, viewGroup, "tp_main_scrollview");
                    scrollView.computeScroll();
                    scrollView.fullScroll(33);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                    break;
                }
            case 4:
                Toast.makeText(this, "计费成功", 1).show();
                break;
            case 5:
                Toast.makeText(this, "计费失败", 1).show();
                break;
            case 6:
                String str = (String) message.obj;
                Toast.makeText(this, "计费取消" + (str == null ? "" : ":" + str), 1).show();
                break;
            case 7:
                Toast.makeText(this, "暂不支持话费支付，请使用其他支付方式", 1).show();
                break;
            case 9:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("无法连接网络，请检查您的网络设置");
                builder.setPositiveButton("设置网络", new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        PaymentActivity.this.isCheckingNetwork = true;
                        PaymentActivity.this.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
                    }
                });
                builder.setNegativeButton("返回", new DialogInterface.OnClickListener() {
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        h.getLogName();
                        if (PaymentActivity.this.listener != null) {
                            PaymentActivity.this.listener.cancel();
                        }
                    }
                });
                builder.setCancelable(false);
                builder.show();
                h.getLogName();
                break;
            case 10:
                Toast.makeText(this, "支付失败，请勿使用安全软件拦截短信发送功能", 0).show();
                break;
            case 11:
                Toast.makeText(this, "请先卸载LBE安全大师，该软件会导致支付功能异常", 1).show();
                break;
        }
        return false;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        h.getLogName();
        "onActivityResult:" + intent.getAction();
        if (this.currentPaymentAdapter != null) {
            this.currentPaymentAdapter.onResult(intent);
        }
    }

    public void onBackPressed() {
        if (this.backPressed) {
            this.backPressed = false;
            cancelPayment(null);
            return;
        }
        this.backPressed = true;
        try {
            Toast.makeText(this, h.getStringIdentifier(this, "tp_string_exit_notify"), 0).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        h.setAppRunning(true);
        h.detectCarrier(this);
        Intent intent = getIntent();
        this.text = null;
        this.price = 0;
        this.pay_type = null;
        this.handler = new Handler(this);
        if (intent.hasExtra("TEXT")) {
            this.text = intent.getStringExtra("TEXT");
        }
        this.price = intent.getIntExtra("PRICE", 0);
        this.pay_type = intent.getStringExtra("TYPE");
        this.productName = intent.getStringExtra("PRODUCT");
        if (this.productName == null) {
            this.productName = h.getDefaultProductName(this);
        }
        this.productType = intent.getStringExtra("PRODUCTTYPE");
        if (this.productType == null) {
            this.productType = "app";
        }
        this.mer_orderId = intent.getStringExtra("MERCHANTORDERID");
        if (intent.getBooleanExtra("PAYCHARGESFIRST", true) && h.getCurrentCarrier() != 0 && !this.pay_type.equals(f.TYPE_ESCROW)) {
            this.payChargesFirstAutoTry = true;
        }
        startRequestPayments(this.price, this.pay_type);
    }

    public void onDestroy() {
        if (this.progressDialog != null) {
            this.progressDialog.dismiss();
        }
        this.progressDialog = null;
        h.clearReceivers(this);
        h.setAppRunning(false);
        super.onDestroy();
    }

    public void onNewIntent(Intent intent) {
        h.getLogName();
        "onNewIntent:" + intent.getAction();
        if (this.currentPaymentAdapter != null) {
            this.currentPaymentAdapter.onResult(intent);
        }
    }

    public void onPause() {
        super.onPause();
        if (this.currentPaymentAdapter != null) {
            this.currentPaymentAdapter.onPause();
        }
    }

    public void onResume() {
        super.onResume();
        if (this.isCheckingNetwork) {
            this.isCheckingNetwork = false;
            if (h.isConnectedOrConnecting(this)) {
                showRunningProgress("正在检查网络");
                h.executeTask(new Runnable() {
                    public final void run() {
                        SystemClock.sleep(3000);
                        h.testConnection(PaymentActivity.this, 10, PaymentActivity.this.listener);
                    }
                });
            } else {
                this.listener.disconnected();
            }
        } else {
            h.getLogName();
        }
        if (this.currentPaymentAdapter != null) {
            this.currentPaymentAdapter.onResume();
        }
    }

    public void payChargesMode() {
        this.currentPayment = this.chargesPayments.remove(0);
        showRunningProgress(null);
        AnonymousClass13 r0 = new f.a() {
            public final void onCancel() {
                if (PaymentActivity.this.payChargesFirstAutoTry) {
                    PaymentActivity.this.payChargesFirstAutoTry = false;
                    PaymentActivity.this.requestComplete();
                    return;
                }
                h.getLogName();
                a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 5, PaymentActivity.this.paymentActAmt);
                PaymentActivity.this.cancelPayment(null);
            }

            public final void onComplete() {
                Log.e(h.getLogName(), "Should not call onComplete directly!");
            }

            public final void onFail(String str, e eVar) {
                PaymentActivity.this.dismissRunningProgress();
                h.getLogName();
                "chargesPaymentListener onFail:" + str + " id:" + eVar.getId();
                if (eVar.getMode() != 0) {
                    Log.w(h.getLogName(), "Not a charges mode payment:" + eVar.getId());
                } else if (PaymentActivity.this.hasAnySuccessPaid) {
                    PaymentActivity.this.handler.sendEmptyMessage(4);
                    h.getLogName();
                    a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 1, PaymentActivity.this.paymentActAmt, str);
                    if (f.getTotalPayListener() != null) {
                        f.getTotalPayListener().onComplete();
                    }
                } else if (PaymentActivity.this.payChargesFirstAutoTry) {
                    PaymentActivity.this.payChargesFirstAutoTry = false;
                    PaymentActivity.this.requestComplete();
                    return;
                } else {
                    if (str != null && str.equals(f.a.TIMEOUT_PROBLEM)) {
                        PaymentActivity.this.handler.sendEmptyMessage(10);
                    } else if (str == null || !str.equals(f.a.LBE_PROBLEM)) {
                        PaymentActivity.this.handler.sendEmptyMessage(5);
                    } else {
                        PaymentActivity.this.handler.sendEmptyMessage(11);
                    }
                    a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 4, PaymentActivity.this.paymentActAmt, str);
                    if (f.getTotalPayListener() != null) {
                        f.getTotalPayListener().onFail(str, eVar);
                    }
                }
                PaymentActivity.this.finish();
            }

            public final void onSuccess(e eVar) {
                h.getLogName();
                "chargesPaymentListener onSuccess:" + eVar.getId();
                PaymentActivity.this.hasAnySuccessPaid = true;
                if (f.getTotalPayListener() != null) {
                    f.getTotalPayListener().onSuccess(eVar);
                }
                if (eVar.getMode() == 0) {
                    PaymentActivity paymentActivity = PaymentActivity.this;
                    paymentActivity.paymentActAmt = paymentActivity.paymentActAmt + eVar.getPrice();
                    if (PaymentActivity.this.chargesPayments.isEmpty()) {
                        a.getInstance(PaymentActivity.this).updateBillingClientState(PaymentActivity.this.billing, 1, PaymentActivity.this.paymentActAmt);
                        PaymentActivity.this.dismissRunningProgress();
                        PaymentActivity.this.handler.sendEmptyMessage(4);
                        if (f.getTotalPayListener() != null) {
                            f.getTotalPayListener().onComplete();
                        }
                        PaymentActivity.this.finish();
                        return;
                    }
                    h.getLogName();
                    "chargesPaymentListener start next payment:" + ((e) PaymentActivity.this.chargesPayments.get(0)).getRemark();
                    PaymentActivity.this.payChargesMode();
                }
            }
        };
        String type = this.currentPayment.getType();
        try {
            this.billing.put("paymenttype", 6);
        } catch (JSONException e) {
        }
        this.currentPaymentAdapter = null;
        h.getLogName();
        "Type:" + type;
        if (type.equals(d.TYPE_SMS)) {
            this.currentPaymentAdapter = new g();
        } else if (type.equals(d.TYPE_MMIAP)) {
            this.currentPaymentAdapter = new e();
        } else if (type.equals(d.TYPE_VACSMS)) {
            this.currentPaymentAdapter = new h();
        }
        if (this.currentPaymentAdapter != null) {
            this.currentPaymentAdapter.pay(this, this.currentPayment, r0);
        } else if (this.payChargesFirstAutoTry) {
            this.payChargesFirstAutoTry = false;
            requestComplete();
        } else {
            h.getLogName();
            "Unkown payment type:" + type;
            cancelPayment("未知计费类型");
        }
    }

    public void payEscrowMode(int i) {
        if (i < 0 || i >= this.escrowPayments.size()) {
            i = 0;
        }
        this.currentPayment = this.escrowPayments.get(i);
        if (this.currentPayment.getMode() == 1) {
            this.currentPayment.setPrice(this.price);
        }
        String type = this.currentPayment.getType();
        this.currentPaymentAdapter = null;
        h.getLogName();
        "Type:" + type;
        if (type.equals(d.TYPE_LTHJ)) {
            this.currentPaymentAdapter = new d();
        } else if (type.equals(d.TYPE_YEEPAYCP)) {
            this.currentPaymentAdapter = new i();
        } else if (type.equals(d.TYPE_ALIPAYWS)) {
            this.currentPaymentAdapter = new a();
        }
        if (this.currentPaymentAdapter == null) {
            h.getLogName();
            "Unkown payment type:" + type;
            cancelPayment("未知计费类型");
        } else if (!this.currentPaymentAdapter.forceNetwork() || h.isConnectedOrConnecting(this)) {
            continuePayBillingMode();
        } else {
            checkNetwork(new c() {
                public final void cancel() {
                    PaymentActivity.this.cancelPayment("网络连接不可用");
                }

                public final void connected() {
                    PaymentActivity.this.dismissRunningProgress();
                    PaymentActivity.this.handler.post(new Runnable() {
                        public final void run() {
                            PaymentActivity.this.continuePayBillingMode();
                        }
                    });
                }

                public final void disconnected() {
                    PaymentActivity.this.checkNetwork(this);
                }
            });
        }
    }

    public void sendUIMessage(int i, String str) {
        this.handler.sendMessage(this.handler.obtainMessage(i, str));
    }

    public void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter adapter = listView.getAdapter();
        if (adapter != null) {
            int i = 0;
            for (int i2 = 0; i2 < adapter.getCount(); i2++) {
                View view = adapter.getView(i2, null, listView);
                view.measure(0, 0);
                i += view.getMeasuredHeight();
            }
            ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
            layoutParams.height = (listView.getDividerHeight() * (adapter.getCount() - 1)) + i;
            layoutParams.height += 5;
            listView.setLayoutParams(layoutParams);
        }
    }
}
