package me.gall.totalpay.android;

import org.json.JSONException;
import org.json.JSONObject;

public final class e {
    public static final int MODE_CHARGES = 0;
    public static final int MODE_ESCROW = 1;
    private static final String[] payTypes = {"", d.TYPE_LTHJ, "uppay", "yeepayhc", d.TYPE_YEEPAYCP, d.TYPE_ALIPAYWS, d.TYPE_ALIPAYWS};
    private int mode;
    private JSONObject payDetail;
    private int price;

    e(JSONObject jSONObject, int i) {
        this.payDetail = jSONObject;
        this.mode = i;
    }

    private int getParamInt(String str) {
        if (this.payDetail.has(str)) {
            try {
                return this.payDetail.getInt(str);
            } catch (JSONException e) {
                e.printStackTrace();
                return 0;
            }
        } else {
            throw new NullPointerException(String.valueOf(str) + " not found.");
        }
    }

    public final boolean containsKey(String str) {
        return this.payDetail.has(str);
    }

    public final int getCarrier() {
        return Integer.parseInt(getParam("TP_carrier"));
    }

    public final String getId() {
        if (this.mode == 1) {
            return getParam("id");
        }
        if (this.mode == 0) {
            return getParam("TP_fid");
        }
        return null;
    }

    public final int getMode() {
        return this.mode;
    }

    public final String getName() {
        if (this.mode == 1) {
            return getParam("name");
        }
        if (this.mode == 0) {
            return getParam("TP_remark");
        }
        return null;
    }

    public final String getParam(String str) {
        if (this.payDetail.has(str)) {
            try {
                return this.payDetail.getString(str);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public final int getPrice() {
        if (this.mode == 1) {
            return this.price;
        }
        if (this.mode == 0) {
            return Integer.parseInt(getParam("TP_price"));
        }
        return 0;
    }

    public final String getRemark() {
        return getParam("TP_remark");
    }

    public final String getType() {
        if (this.mode == 1) {
            return payTypes[getParamInt("paytype")];
        }
        if (this.mode == 0) {
            return getParam("TP_type");
        }
        return null;
    }

    public final boolean isRepeatable() {
        if (this.mode == 1) {
            return true;
        }
        return Boolean.parseBoolean(getParam("TP_repeat"));
    }

    public final void setPrice(int i) {
        this.price = i;
    }
}
