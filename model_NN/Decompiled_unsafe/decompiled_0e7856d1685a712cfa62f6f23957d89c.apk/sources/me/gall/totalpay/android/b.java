package me.gall.totalpay.android;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

public final class b {
    static final String CHARGES_CONF_FILE = "totalpay.conf";
    static final String ESCROW_CONF_FILE = "escrow.conf";
    static final String SMS_BLACKLIST_FILE = "blacklist.conf";
    static final String TP_LAST_UPDATE_TIMESTAMP = "TP_LAST_UPDATE_TIMESTAMP";
    private static b dataFileManager;
    private Context context;

    private class a extends Thread {
        private Context context;
        private String filename;
        private String url;

        public a(Context context2, String str, String str2) {
            this.context = context2;
            this.url = str;
            this.filename = str2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x00c9  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r6 = this;
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00cf }
                java.lang.String r2 = r6.url     // Catch:{ Exception -> 0x00cf }
                r0.<init>(r2)     // Catch:{ Exception -> 0x00cf }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00cf }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cf }
                java.lang.String r3 = "FILE:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x00cf }
                java.lang.String r3 = r0.toString()     // Catch:{ Exception -> 0x00cf }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x00cf }
                r2.toString()     // Catch:{ Exception -> 0x00cf }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00cf }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00cf }
                r1 = 1
                r0.setDoInput(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r0.connect()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r3 = "ResponseCode:"
                r2.<init>(r3)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r2.toString()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r2 = 304(0x130, float:4.26E-43)
                if (r1 != r2) goto L_0x0056
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
            L_0x0050:
                if (r0 == 0) goto L_0x0055
                r0.disconnect()
            L_0x0055:
                return
            L_0x0056:
                r2 = 200(0xc8, float:2.8E-43)
                if (r1 != r2) goto L_0x00ae
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                android.content.Context r1 = r6.context     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r2 = r6.filename     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r3 = 0
                java.io.FileOutputStream r1 = r1.openFileOutput(r2, r3)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                me.gall.totalpay.android.h.copy(r2, r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r1.close()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                long r1 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                me.gall.totalpay.android.b r3 = me.gall.totalpay.android.b.this     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r4 = r6.filename     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r3.setFileUpdateTimestamp(r4, r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r4 = "Update complete at "
                r3.<init>(r4)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r1.toString()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                goto L_0x0050
            L_0x008d:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x0091:
                me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x00cd }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cd }
                java.lang.String r3 = "Network problem:"
                r2.<init>(r3)     // Catch:{ all -> 0x00cd }
                java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00cd }
                java.lang.String r2 = ". Will try it next time."
                java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x00cd }
                r0.toString()     // Catch:{ all -> 0x00cd }
                if (r1 == 0) goto L_0x0055
                r1.disconnect()
                goto L_0x0055
            L_0x00ae:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r4 = "responseCode:"
                r3.<init>(r4)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
                throw r2     // Catch:{ Exception -> 0x008d, all -> 0x00c3 }
            L_0x00c3:
                r1 = move-exception
                r5 = r1
                r1 = r0
                r0 = r5
            L_0x00c7:
                if (r1 == 0) goto L_0x00cc
                r1.disconnect()
            L_0x00cc:
                throw r0
            L_0x00cd:
                r0 = move-exception
                goto L_0x00c7
            L_0x00cf:
                r0 = move-exception
                goto L_0x0091
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.totalpay.android.b.a.run():void");
        }
    }

    private b(Context context2) {
        this.context = context2;
    }

    public static b getInstance(Context context2) {
        if (dataFileManager == null) {
            dataFileManager = new b(context2);
        }
        dataFileManager.context = context2;
        return dataFileManager;
    }

    private String getTodayTimestamp() {
        Calendar instance = Calendar.getInstance();
        int i = instance.get(1);
        return String.valueOf(i) + "-" + instance.get(6);
    }

    private void updateBlacklist(String str) {
        updateFile(String.valueOf(a.getBillingHost(this.context)) + "blacklist?since=" + getFileUpdateTimestamp(SMS_BLACKLIST_FILE) + "&cid=" + str, SMS_BLACKLIST_FILE, false);
    }

    private void updateChargesPayments(String str) {
        updateFile(String.valueOf(a.getBillingHost(this.context)) + "payments/" + str + "/@all?uid=" + h.getDeviceUniqueID(this.context) + "&since=" + getFileUpdateTimestamp(CHARGES_CONF_FILE), CHARGES_CONF_FILE, false);
    }

    private void updateEscrowPayments(String str) {
        updateFile(String.valueOf(a.getBillingHost(this.context)) + "escrow/" + str + "?carrier=" + h.getCurrentCarrier(), ESCROW_CONF_FILE, false);
    }

    public final long getFileUpdateTimestamp(String str) {
        return h.getSetting(this.context).getLong(String.valueOf(str) + "_TP_LAST_UPDATE_TIMESTAMP", 0);
    }

    public final InputStream openFile(String str) {
        try {
            return this.context.openFileInput(str);
        } catch (Exception e) {
            h.getLogName();
            "Updated " + str + " not found. Try default one." + e;
            try {
                return this.context.getAssets().open(str);
            } catch (IOException e2) {
                Log.w(h.getLogName(), "Offline " + str + " not found in assets. " + e2);
                return null;
            }
        }
    }

    public final void setFileUpdateTimestamp(String str, long j) {
        h.getSetting(this.context).edit().putLong(String.valueOf(str) + "_TP_LAST_UPDATE_TIMESTAMP", j).commit();
    }

    public final void updateFile(String str, String str2, boolean z) {
        a aVar = new a(this.context, str, str2);
        if (z) {
            aVar.run();
        } else {
            h.executeTask(aVar);
        }
    }

    public final void updateLocalCache(String str, boolean z) {
        if (str != null && str.length() > 0) {
            int i = h.getSetting(this.context).getInt(getTodayTimestamp(), 0);
            if (i <= 2 || z) {
                updateChargesPayments(str);
                updateEscrowPayments(str);
                updateBlacklist(str);
                h.getSetting(this.context).edit().putInt(getTodayTimestamp(), i + 1);
                return;
            }
            h.getLogName();
            String.valueOf(getTodayTimestamp()) + " already update " + i + " times.";
        }
    }
}
