package me.gall.totalpay.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.alipay.android.Base64;
import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

public final class h {
    private static final String[] CARRIER_NAMES = {"未知", "中国移动", "中国联通", "中国电信"};
    private static final String LOCAL_UID = "LOCAL_UID";
    public static final String TP_SETTING = "TP_SETTING";
    private static int carrier = -1;
    private static ExecutorService executorService;
    private static Map<String, BroadcastReceiver> receivers = new ConcurrentHashMap();
    private static boolean running;
    /* access modifiers changed from: private */
    public static volatile boolean runningConnectionTest;

    private static final class a implements Callable<Boolean> {
        private c listener;

        a(c cVar) {
            this.listener = cVar;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
            r1 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x008c, code lost:
            r2 = r0;
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a8, code lost:
            r8.listener.disconnected();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00af, code lost:
            r2.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b9, code lost:
            r1.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c0, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x00c1, code lost:
            r7 = r1;
            r1 = r0;
            r0 = r7;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00a8 A[Catch:{ all -> 0x00c5 }] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00af  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x00b9  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00c0 A[ExcHandler: all (r1v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x002a] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Boolean call() {
            /*
                r8 = this;
                r2 = 1
                r3 = 0
                me.gall.totalpay.android.h.runningConnectionTest = r2
                r1 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.String r4 = "http://www.baidu.com"
                r0.<init>(r4)     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.String r5 = "TEST:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                r4.toString()     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00c8, all -> 0x00b6 }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                r1 = 5000(0x1388, float:7.006E-42)
                r0.setConnectTimeout(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                r0.connect()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                int r1 = r0.getResponseCode()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                me.gall.totalpay.android.h.getLogName()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.String r5 = "ResponseCode:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                r4.toString()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                r4 = 200(0xc8, float:2.8E-43)
                if (r1 != r4) goto L_0x0076
                java.io.InputStream r1 = r0.getInputStream()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.String r1 = me.gall.totalpay.android.h.consumeStream(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.String r4 = "百度"
                boolean r1 = r1.contains(r4)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                if (r1 == 0) goto L_0x00d4
                me.gall.totalpay.android.c r1 = r8.listener     // Catch:{ Exception -> 0x00cd, all -> 0x00c0 }
                if (r1 == 0) goto L_0x00d2
                me.gall.totalpay.android.c r1 = r8.listener     // Catch:{ Exception -> 0x00cd, all -> 0x00c0 }
                r1.connected()     // Catch:{ Exception -> 0x00cd, all -> 0x00c0 }
                r1 = r2
            L_0x0068:
                if (r0 == 0) goto L_0x006d
                r0.disconnect()
            L_0x006d:
                me.gall.totalpay.android.h.runningConnectionTest = r3
                r0 = r1
            L_0x0071:
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
                return r0
            L_0x0076:
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.String r5 = "responseCode:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                r2.<init>(r1)     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
                throw r2     // Catch:{ Exception -> 0x008b, all -> 0x00c0 }
            L_0x008b:
                r1 = move-exception
                r2 = r0
                r0 = r3
            L_0x008e:
                java.lang.String r4 = me.gall.totalpay.android.h.getLogName()     // Catch:{ all -> 0x00c5 }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c5 }
                java.lang.String r6 = "Network problem:"
                r5.<init>(r6)     // Catch:{ all -> 0x00c5 }
                java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x00c5 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00c5 }
                android.util.Log.w(r4, r1)     // Catch:{ all -> 0x00c5 }
                me.gall.totalpay.android.c r1 = r8.listener     // Catch:{ all -> 0x00c5 }
                if (r1 == 0) goto L_0x00ad
                me.gall.totalpay.android.c r1 = r8.listener     // Catch:{ all -> 0x00c5 }
                r1.disconnected()     // Catch:{ all -> 0x00c5 }
            L_0x00ad:
                if (r2 == 0) goto L_0x00b2
                r2.disconnect()
            L_0x00b2:
                me.gall.totalpay.android.h.runningConnectionTest = r3
                goto L_0x0071
            L_0x00b6:
                r0 = move-exception
            L_0x00b7:
                if (r1 == 0) goto L_0x00bc
                r1.disconnect()
            L_0x00bc:
                me.gall.totalpay.android.h.runningConnectionTest = r3
                throw r0
            L_0x00c0:
                r1 = move-exception
                r7 = r1
                r1 = r0
                r0 = r7
                goto L_0x00b7
            L_0x00c5:
                r0 = move-exception
                r1 = r2
                goto L_0x00b7
            L_0x00c8:
                r0 = move-exception
                r2 = r1
                r1 = r0
                r0 = r3
                goto L_0x008e
            L_0x00cd:
                r1 = move-exception
                r7 = r0
                r0 = r2
                r2 = r7
                goto L_0x008e
            L_0x00d2:
                r1 = r2
                goto L_0x0068
            L_0x00d4:
                r1 = r3
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.totalpay.android.h.a.call():java.lang.Boolean");
        }
    }

    public static void clearReceivers(Context context) {
        for (String unregisterReceiver : receivers.keySet()) {
            unregisterReceiver(context, unregisterReceiver);
        }
        receivers.clear();
    }

    public static final String consumeStream(InputStream inputStream) {
        return consumeStream(inputStream, e.f);
    }

    public static final String consumeStream(InputStream inputStream, String str) {
        byte[] bArr = new byte[com.a.a.e.a.GAME_B_PRESSED];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(com.a.a.e.a.GAME_B_PRESSED);
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
                String str2 = new String(byteArrayOutputStream.toByteArray(), str);
                getLogName();
                "Data:" + str2;
                inputStream.close();
                return str2;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static boolean containsManifestMetaKey(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.containsKey(str);
        } catch (Exception e) {
            getLogName();
            String.valueOf(e.getMessage()) + " unknown meta key or not exist:" + str;
            return false;
        }
    }

    public static String convertPriceIntoString(int i) {
        return NumberFormat.getCurrencyInstance().format((double) (((float) i) / 100.0f));
    }

    public static void copy(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[com.a.a.e.a.GAME_B_PRESSED];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                inputStream.close();
                return;
            }
            outputStream.write(bArr, 0, read);
        }
    }

    public static void detectCarrier(Context context) {
        carrier = getCarrierByIMSI(getDeviceIMSI(context));
    }

    public static void executeTask(Runnable runnable) {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        if (executorService.isShutdown() || executorService.isTerminated()) {
            Log.e(getLogName(), "ExecutorService is stopped.");
            executorService = Executors.newSingleThreadExecutor();
        }
        executorService.execute(runnable);
    }

    public static String extactString(String str, String str2, String str3) {
        int indexOf;
        int indexOf2 = str.indexOf(str2);
        if (indexOf2 == -1 || (indexOf = str.indexOf(str3, indexOf2 + 1)) == -1) {
            return null;
        }
        return str.substring(indexOf2, indexOf);
    }

    public static final View findViewByName(Context context, View view, String str) {
        return view.findViewById(getViewIdentifier(context, str));
    }

    public static void finishTask() {
        if (executorService != null) {
            executorService.shutdown();
        }
        executorService = null;
    }

    public static String gainSignature(Context context) {
        return Base64.p(MessageDigest.getInstance("SHA-1").digest(context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray()));
    }

    public static int getCarrierByIMSI(String str) {
        if (str.startsWith("46000") || str.startsWith("46002") || str.startsWith("46007")) {
            return 1;
        }
        if (str.startsWith("46001") || str.startsWith("46006")) {
            return 2;
        }
        return (str.startsWith("46003") || str.startsWith("46005")) ? 3 : 0;
    }

    public static String getCarrierName() {
        return CARRIER_NAMES[getCurrentCarrier()];
    }

    public static int getCurrentCarrier() {
        if (carrier != -1) {
            return carrier;
        }
        Log.e(getLogName(), "Call detectCarrier() at first in your initilization.");
        return 0;
    }

    public static String getDataConnectionNetworkInfo(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            getLogName();
            return null;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0).getExtraInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getDefaultProductName(Context context) {
        String str = "";
        try {
            str = String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        context.getPackageName().equals("me.gall.totalpay.android");
        return String.valueOf(context.getPackageName()) + " " + str;
    }

    public static String getDeviceICCID(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getSimSerialNumber();
            } catch (Exception e) {
                e.printStackTrace();
            }
            getLogName();
            "iccid=" + str;
        }
        return str == null ? "" : str;
    }

    private static String getDeviceIDHash(Context context) {
        String str;
        try {
            str = Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Exception e) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e2) {
                str = null;
            }
        }
        getLogName();
        "androidId=" + str;
        if (str == null || "9774d56d682e549c".equals(str)) {
            return str;
        }
        return null;
    }

    public static String getDeviceIMEI(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            getLogName();
            "imei=" + str;
        }
        return str == null ? "" : str;
    }

    public static final String getDeviceIMSI(Context context) {
        String str;
        if (isDualSim()) {
            try {
                str = getDeviceIMSI(context, getReadySim(context));
            } catch (Exception e) {
                e.printStackTrace();
                str = null;
            }
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            try {
                if (telephonyManager.getSimState() == 5 || telephonyManager.getSimState() == 0) {
                    getLogName();
                    str = telephonyManager.getSubscriberId();
                } else {
                    throw new Exception("Sim card is not ready yet.");
                }
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.e(getLogName(), "Couldn't get the operator.");
                str = null;
            }
        }
        return str == null ? "" : str;
    }

    public static String getDeviceIMSI(Context context, int i) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            Object invoke = TelephonyManager.class.getMethod("getSimStateGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i));
            Object invoke2 = TelephonyManager.class.getMethod("getSubscriberIdGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i));
            if (invoke.toString().equals("5")) {
                getLogName();
                "IMSI of sim " + i + " is " + ((String) null);
                return invoke2.toString();
            }
            getLogName();
            "Sim " + i + " is not ready.";
            return null;
        } catch (Exception e) {
            try {
                Object invoke3 = TelephonyManager.class.getMethod("getSimState", Integer.TYPE).invoke(telephonyManager, new Integer(i));
                Object invoke4 = TelephonyManager.class.getMethod("getSubscriberId", Integer.TYPE).invoke(telephonyManager, new Integer(i));
                if (invoke3.toString().equals("5")) {
                    getLogName();
                    "IMSI of sim " + i + " is " + ((String) null);
                    return invoke4.toString();
                }
                getLogName();
                "Sim " + i + " is not ready.";
                return null;
            } catch (Exception e2) {
                getLogName();
                "It may not be a dual sim device." + e;
                return null;
            }
        }
    }

    public static String getDevicePhonenumber(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = getTelephonyManager(context).getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
            }
            getLogName();
            "phoneNumber=" + str;
        }
        return str == null ? "" : str;
    }

    public static String getDeviceUniqueID(Context context) {
        String devicePhonenumber = getDevicePhonenumber(context);
        if (devicePhonenumber == null || devicePhonenumber.trim().equals("")) {
            getLogName();
            devicePhonenumber = getDeviceICCID(context);
        }
        if (devicePhonenumber == null || devicePhonenumber.trim().length() < 20) {
            getLogName();
            devicePhonenumber = getDeviceIMEI(context);
        }
        if (devicePhonenumber == null || devicePhonenumber.trim().length() < 10) {
            getLogName();
            devicePhonenumber = getMacAddress(context);
        }
        if (devicePhonenumber == null || devicePhonenumber.trim().equals("")) {
            getLogName();
            devicePhonenumber = getDeviceIDHash(context);
        }
        if (devicePhonenumber == null || devicePhonenumber.trim().length() < 10) {
            getLogName();
            devicePhonenumber = getLocalGenerateUUID(context);
        }
        getLogName();
        "uniqueID is " + devicePhonenumber;
        return devicePhonenumber.trim();
    }

    public static final int getDrawableIdentifier(Context context, String str) {
        return getIdentifier(context, str, "drawable");
    }

    public static final int getIdentifier(Context context, String str, String str2) {
        int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
        if (identifier != 0) {
            return identifier;
        }
        throw new FileNotFoundException(String.valueOf(str) + " is not found in res/" + str2 + "/.");
    }

    public static final int getLayoutIdentifier(Context context, String str) {
        return getIdentifier(context, str, "layout");
    }

    public static String getLocalGenerateUUID(Context context) {
        if (!getSetting(context).contains(LOCAL_UID)) {
            getSetting(context).edit().putString(LOCAL_UID, UUID.randomUUID().toString()).commit();
        }
        return getSetting(context).getString(LOCAL_UID, "");
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        getLogName();
                        "IP:" + nextElement.getHostAddress().toString();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException e) {
            Log.e("WifiPreference IpAddress", e.toString());
        }
        return null;
    }

    public static final String getLogName() {
        return "TotalPay";
    }

    public static String getMacAddress(Context context) {
        String str = null;
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            try {
                str = getWifiManager(context).getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
            getLogName();
            "mac=" + str;
        }
        return str == null ? "" : str;
    }

    public static boolean getManifestMetaBooleanValue(Context context, String str, boolean z) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getBoolean(str);
        } catch (Exception e) {
            getLogName();
            String.valueOf(e.getMessage()) + " unknown meta key or not exist:" + str;
            return z;
        }
    }

    public static int getManifestMetaIntValue(Context context, String str, int i) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt(str);
        } catch (Exception e) {
            getLogName();
            String.valueOf(e.getMessage()) + " unknown meta key or not exist:" + str;
            return i;
        }
    }

    public static String getManifestMetaValue(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(str);
        } catch (Exception e) {
            getLogName();
            String.valueOf(e.getMessage()) + " unknown meta key or not exist:" + str;
            return null;
        }
    }

    public static String getOSVersion() {
        return "android-" + Build.VERSION.SDK_INT;
    }

    public static int getReadySim(Context context) {
        boolean isSimReady = isSimReady(context, 0);
        boolean isSimReady2 = isSimReady(context, 1);
        if (isSimReady && isSimReady2) {
            return (getCarrierByIMSI(getDeviceIMSI(context, 0)) != 1 && getCarrierByIMSI(getDeviceIMSI(context, 1)) == 1) ? 1 : 0;
        } else if (isSimReady) {
            return 0;
        } else {
            if (isSimReady2) {
                return 1;
            }
            throw new IllegalStateException("None sim is ready.");
        }
    }

    public static SharedPreferences getSetting(Context context) {
        return context.getApplicationContext().getSharedPreferences(TP_SETTING, 0);
    }

    public static final int getStringIdentifier(Context context, String str) {
        return getIdentifier(context, str, "string");
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    public static final int getViewIdentifier(Context context, String str) {
        try {
            return getIdentifier(context, str, "id");
        } catch (FileNotFoundException e) {
            throw new NullPointerException(String.valueOf(str) + " is not found.");
        }
    }

    public static WifiManager getWifiManager(Context context) {
        return (WifiManager) context.getSystemService("wifi");
    }

    public static final View inflateView(Context context, String str) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getLayoutIdentifier(context, str), (ViewGroup) null);
    }

    public static boolean isAppRunning() {
        return running;
    }

    public static boolean isApplicationInstalled(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static final boolean isConnectedOrConnecting(Context context) {
        return isWifiConnect(context) || isMobileConnect(context);
    }

    public static boolean isDualSim() {
        Class<TelephonyManager> cls = TelephonyManager.class;
        try {
            cls.getMethod("getSimStateGemini", Integer.TYPE);
            getLogName();
            return true;
        } catch (Exception e) {
            Class<TelephonyManager> cls2 = TelephonyManager.class;
            try {
                cls2.getMethod("getSimState", Integer.TYPE);
                getLogName();
                return true;
            } catch (Exception e2) {
                return false;
            }
        }
    }

    public static boolean isMobileConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            getLogName();
            return true;
        }
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
            if (networkInfo.isConnectedOrConnecting() && networkInfo.isAvailable() && getTelephonyManager(context).getDataState() == 2) {
                getLogName();
                "mobile is connected. Type:" + networkInfo.getTypeName() + " APN:" + networkInfo.getExtraInfo();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isPaymentAvailableForCurrentCarrier(e eVar) {
        return getCurrentCarrier() == eVar.getCarrier();
    }

    public static boolean isSimReady(Context context, int i) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        Class<TelephonyManager> cls = TelephonyManager.class;
        try {
            return cls.getMethod("getSimStateGemini", Integer.TYPE).invoke(telephonyManager, new Integer(i)).toString().equals("5");
        } catch (Exception e) {
            Class<TelephonyManager> cls2 = TelephonyManager.class;
            try {
                return cls2.getMethod("getSimState", Integer.TYPE).invoke(telephonyManager, new Integer(i)).toString().equals("5");
            } catch (Exception e2) {
                e2.printStackTrace();
                return false;
            }
        }
    }

    public static boolean isTestingConnection() {
        return runningConnectionTest;
    }

    public static boolean isWifiConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            getLogName();
            return true;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnectedOrConnecting();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void registerReceiver(Context context, String str, BroadcastReceiver broadcastReceiver) {
        receivers.put(str, broadcastReceiver);
        context.registerReceiver(broadcastReceiver, new IntentFilter(str));
        getLogName();
        "Register receiver:" + str;
    }

    public static void sendTextMessage(Context context, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        SmsManager smsManager = SmsManager.getDefault();
        for (String next : smsManager.divideMessage(str2)) {
            if (!isDualSim() || getReadySim(context) != 1) {
                smsManager.sendTextMessage(str, "", next, pendingIntent, pendingIntent2);
            } else {
                try {
                    Class.forName("android.telephony.gemini.GeminiSmsManager").getMethod("sendTextMessageGemini", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(null, str, "", next, Integer.valueOf(getReadySim(context)), pendingIntent, pendingIntent2);
                } catch (Exception e) {
                    Log.e(getLogName(), "It may be a mstar device." + e);
                }
            }
            getLogName();
            "Send " + str2 + " to " + str;
        }
    }

    public static void setAppRunning(boolean z) {
        running = z;
    }

    public static void setMobileDataEnabled(Context context, boolean z) {
        Method declaredMethod;
        getLogName();
        "SDK_INT is " + Build.VERSION.SDK_INT;
        if (Build.VERSION.SDK_INT >= 9) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField("mService");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(connectivityManager);
            Method declaredMethod2 = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
            declaredMethod2.setAccessible(true);
            declaredMethod2.invoke(obj, Boolean.valueOf(z));
        } else {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager.getDataState() == 2 && z) {
                getLogName();
            } else if (telephonyManager.getDataState() != 2 && !z) {
                getLogName();
            }
            Method declaredMethod3 = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            declaredMethod3.setAccessible(true);
            Object invoke = declaredMethod3.invoke(telephonyManager, new Object[0]);
            Class<?> cls = Class.forName(invoke.getClass().getName());
            if (!z) {
                declaredMethod = cls.getDeclaredMethod("disableDataConnectivity", new Class[0]);
                getLogName();
            } else {
                declaredMethod = cls.getDeclaredMethod("enableDataConnectivity", new Class[0]);
                getLogName();
            }
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(invoke, new Object[0]);
            getLogName();
        }
        SystemClock.sleep(2000);
    }

    public static void setWifiEnabled(Context context, boolean z) {
        ((WifiManager) context.getSystemService("wifi")).setWifiEnabled(z);
        SystemClock.sleep(2000);
    }

    public static void showAlert(Context context, String str, String str2, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        final Context context2 = context;
        final String str3 = str;
        final String str4 = str2;
        final DialogInterface.OnClickListener onClickListener3 = onClickListener;
        final DialogInterface.OnClickListener onClickListener4 = onClickListener2;
        ((Activity) context).runOnUiThread(new Runnable() {
            public final void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(context2);
                if (str3 != null) {
                    builder.setTitle(str3);
                }
                if (str4 != null) {
                    builder.setMessage(str4);
                }
                if (onClickListener3 != null) {
                    builder.setPositiveButton("确定", onClickListener3);
                }
                if (onClickListener4 != null) {
                    builder.setNegativeButton("取消", onClickListener4);
                }
                builder.setCancelable(false);
                builder.create().show();
            }
        });
    }

    public static Future<?> submitTask(Runnable runnable) {
        if (executorService == null) {
            executorService = Executors.newSingleThreadExecutor();
        }
        if (executorService.isShutdown() || executorService.isTerminated()) {
            Log.e(getLogName(), "ExecutorService is stopped.");
            executorService = Executors.newSingleThreadExecutor();
        }
        return executorService.submit(runnable);
    }

    public static boolean testConnection(Context context) {
        return testConnection(context, 5, null);
    }

    public static synchronized boolean testConnection(Context context, int i, c cVar) {
        boolean z;
        synchronized (h.class) {
            if (isConnectedOrConnecting(context)) {
                FutureTask futureTask = new FutureTask(new a(cVar));
                new Thread(futureTask).start();
                try {
                    z = ((Boolean) futureTask.get((long) i, TimeUnit.SECONDS)).booleanValue();
                } catch (Exception e) {
                    e.printStackTrace();
                    if (cVar != null) {
                        cVar.cancel();
                    }
                }
            } else {
                if (cVar != null) {
                    cVar.disconnected();
                }
                z = false;
            }
        }
        return z;
    }

    public static void unregisterReceiver(Context context, String str) {
        BroadcastReceiver remove = receivers.remove(str);
        if (remove != null) {
            try {
                context.unregisterReceiver(remove);
            } catch (Exception e) {
            }
            getLogName();
            "Unregister receiver:" + str;
        }
    }
}
