package me.gall.totalpay.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.a.a.h.b;

public class NetworkStateChangeReceiver extends BroadcastReceiver {
    protected static final String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    protected static final String WIFI_STATE_CHANGED = "android.net.wifi.WIFI_STATE_CHANGED";
    protected static final String WIFI_STATE_CHANGED_2 = "android.net.wifi.supplicant.CONNECTION_CHANGE";

    public void onReceive(Context context, Intent intent) {
        if (h.isAppRunning()) {
            h.getLogName();
            return;
        }
        String action = intent.getAction();
        if (action.equals(CONNECTIVITY_CHANGE) || action.equals(WIFI_STATE_CHANGED) || action.equals(WIFI_STATE_CHANGED_2)) {
            try {
                if (h.testConnection(context)) {
                    String channelId = f.getChannelId(context);
                    f.detectCIdStatus(context, channelId);
                    b.getInstance(context).updateLocalCache(channelId, false);
                    h.getLogName();
                    a.getInstance(context).uploadRemainBillings();
                    h.getLogName();
                    b.uploadRemainPaymentResults(context);
                    h.getLogName();
                    return;
                }
                h.getLogName();
            } catch (Exception e) {
                h.getLogName();
                "Stop update when error." + e;
            }
        }
    }
}
