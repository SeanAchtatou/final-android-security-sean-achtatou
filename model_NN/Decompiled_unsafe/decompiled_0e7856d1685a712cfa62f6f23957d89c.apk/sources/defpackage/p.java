package defpackage;

import com.a.a.d.h;

/* renamed from: p  reason: default package */
public final class p extends g implements o {
    private l bo;
    private int m = (this.by * 3);
    private s rp;

    public p(s sVar, String str, int i, int i2, int i3, int i4) {
        this.rp = sVar;
        this.bK = str;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.bH = true;
        a("是,否,");
    }

    public final void a(int i) {
        this.bz = (byte) i;
        for (int i2 = 0; i2 < this.bI; i2++) {
            this.br[i2].a(this.bz);
        }
    }

    public final void a(h hVar, int i, int i2) {
        int i3;
        int i4;
        super.b(hVar, i, i2);
        this.br[this.bG].bD = (char) this.bD;
        this.br[this.bG].ao = this.ao;
        this.br[this.bG].a(hVar, i, i2);
        switch (this.bD) {
            case 2:
                this.br[this.bG].ao = this.ao;
                this.br[this.bG].a(hVar, i, i2);
                for (int i5 = 0; i5 < this.bI; i5++) {
                    this.br[i5].ao = false;
                    if (this.o == 0) {
                        i4 = this.q + i;
                        i3 = this.p + (this.bI * this.ag) > this.rp.ag ? (((this.ag * i5) + i2) + 1) - ((this.bI - 1) * this.ag) : (this.ag * i5) + i2 + 1;
                    } else if (this.p + (this.bI * this.ag) > this.rp.ag) {
                        i3 = ((((i5 + 1) * this.ag) + i2) + 1) - ((this.bI + 1) * this.ag);
                        i4 = i;
                    } else {
                        i3 = ((i5 + 1) * this.ag) + i2 + 1;
                        i4 = i;
                    }
                    this.br[i5].a(hVar, i4, i3);
                }
                break;
        }
        super.c(hVar, i, i2);
        hVar.setColor(this.u);
        if (this.ap) {
            hVar.b((this.by / 2) + (((this.o + i) + this.q) - this.m), (this.by / 2) + this.p + i2 + this.by, ((this.o + i) + this.q) - (this.by / 2), (this.by / 2) + this.p + i2 + this.by, ((this.o + i) + this.q) - (this.m / 2), (this.by / 2) + this.p + i2 + (this.by * 2));
        }
    }

    public final void a(String str) {
        int i;
        this.bI = 0;
        this.bF = 0;
        super.a(str);
        this.bM = str;
        int i2 = 0;
        int i3 = 0;
        while (i2 < str.length()) {
            if (str.charAt(i2) == ',') {
                String substring = str.substring(i3, i2);
                if (this.bI > this.bq - 1) {
                    System.out.println("Item index too big.");
                } else {
                    if (this.bI == 0) {
                        this.br = new g[(this.bI + 1)];
                    } else {
                        g[] gVarArr = new g[(this.bI + 1)];
                        System.arraycopy(this.br, 0, gVarArr, 0, this.bI);
                        this.br = new g[(this.bI + 1)];
                        System.arraycopy(gVarArr, 0, this.br, 0, this.bI);
                    }
                    this.br[this.bI] = new m(substring, this.o, this.p, this.q - this.m, this.ag);
                    this.br[this.bI].ap = this.ap;
                    this.br[this.bI].ao = false;
                    this.br[this.bI].a(substring);
                    this.br[this.bI].a(this.bz);
                    this.br[this.bI].h(this.u);
                    this.br[this.bI].B = this.u;
                    this.br[this.bI].al = this.al;
                    this.br[this.bI].ai = this.ai;
                    this.br[this.bI].g(this.w);
                    if (this.br[this.bI].bH) {
                        g gVar = this.br[this.bI];
                        byte b = this.bF;
                        this.bF = (byte) (b + 1);
                        gVar.bE = b;
                    }
                    this.bI = (byte) (this.bI + 1);
                }
                i = i2 + 1;
            } else {
                i = i3;
            }
            i2++;
            i3 = i;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
    }

    public final void b(int i) {
        this.bD = 1;
    }

    public final void b(l lVar) {
        this.bo = lVar;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.br[this.bG].bD = 1;
        this.bo.a(new StringBuffer().append(this.bL).append(this.br[this.bG].bK).toString());
        this.bw.b(2);
        this.bD = 1;
    }
}
