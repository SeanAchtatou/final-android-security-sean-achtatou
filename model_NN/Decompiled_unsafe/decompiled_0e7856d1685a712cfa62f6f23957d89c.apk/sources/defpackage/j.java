package defpackage;

import com.a.a.d.h;

/* renamed from: j  reason: default package */
public final class j extends g {
    private int as = 0;
    private int bN = this.by;
    private int bO = 0;
    private int cH = 0;
    private int m = 0;

    public j(String str, String str2, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.bM = str2;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.as = (i4 - (this.by * 2)) / (this.bx + this.by);
        f(this.bM);
        this.bH = true;
    }

    private void f(String str) {
        int i;
        this.br = new g[128];
        this.bI = 0;
        this.bF = 0;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = i3;
            int i5 = i2;
            if (i4 < str.length()) {
                if (str.charAt(i4) == ',') {
                    String substring = str.substring(i5, i4);
                    int i6 = this.q - 16;
                    int i7 = 0;
                    String[] strArr = new String[128];
                    while (true) {
                        int i8 = 0;
                        int i9 = 0;
                        while (true) {
                            if (i9 >= substring.length()) {
                                i = 0;
                                break;
                            } else if (substring.charAt(i9) == 10) {
                                i = i9 + 1;
                                break;
                            } else {
                                i8 += this.bx;
                                if (i8 > i6) {
                                    i = i9;
                                    break;
                                }
                                i9++;
                            }
                        }
                        if (i == 0) {
                            break;
                        }
                        strArr[i7] = substring.charAt(i + -1) == 10 ? substring.substring(0, i - 1) : substring.substring(0, i);
                        substring = substring.substring(i, substring.length());
                        i7++;
                    }
                    int i10 = i7 + 1;
                    strArr[i7] = substring;
                    String[] strArr2 = new String[i10];
                    System.arraycopy(strArr, 0, strArr2, 0, i10);
                    int i11 = 0;
                    while (true) {
                        int i12 = i11;
                        if (i12 >= strArr2.length) {
                            break;
                        }
                        String str2 = strArr2[i12];
                        if (this.bI > this.bq - 1) {
                            System.out.println("Item index too large.");
                        } else {
                            this.br[this.bI] = new i(str2, this.o + (this.by / 2), this.p, (this.q - this.by) - this.bN, this.bx + this.by);
                            this.br[this.bI].ap = false;
                            this.br[this.bI].ao = true;
                            this.br[this.bI].a(str2);
                            this.br[this.bI].a(this.bz);
                            this.br[this.bI].h(this.u);
                            this.br[this.bI].B = this.B;
                            this.br[this.bI].al = this.al;
                            this.br[this.bI].ai = this.ai;
                            this.br[this.bI].g(this.w);
                            this.br[this.bI].bH = true;
                            if (this.br[this.bI].bH) {
                                g gVar = this.br[this.bI];
                                byte b = this.bF;
                                this.bF = (byte) (b + 1);
                                gVar.bE = b;
                            }
                            this.bI = (byte) (this.bI + 1);
                            this.bB = this.bI;
                        }
                        i11 = i12 + 1;
                    }
                    i2 = i4 + 1;
                } else {
                    i2 = i5;
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    public final void a(int i) {
        this.bz = (byte) i;
        for (int i2 = 0; i2 < this.bI; i2++) {
            this.br[i2].a(this.bz);
        }
    }

    public final void a(h hVar, int i, int i2) {
        this.m++;
        super.b(hVar, i, i2);
        switch (this.bD) {
            case 1:
                hVar.setColor(this.B);
                hVar.c((this.o + i) - 1, (this.p + i2) - 1, this.q + 2, this.ag + 2);
                break;
            case 2:
                if (!this.ao) {
                    hVar.setColor(this.al);
                    hVar.d(this.o + i, this.p + i2, this.q, this.ag);
                    break;
                }
                break;
        }
        if (this.bG > (this.bO + this.as) - 1) {
            this.bO = (this.bG + 1) - this.as;
        } else if (this.bG < this.bO) {
            this.bO = this.bG;
        }
        this.cH = this.bO + this.as;
        if (this.cH > this.bI) {
            this.cH = this.bI;
        }
        for (int i3 = this.bO; i3 < this.cH; i3++) {
            this.br[i3].ai = this.ai;
            this.br[i3].a(hVar, i, (this.by / 2) + i2 + ((i3 - this.bO) * this.br[i3].ag));
        }
        if (this.ap) {
            hVar.setColor(this.u);
            hVar.c((((this.o + i) + this.q) - this.bN) - 1, this.p + i2, this.bN + 1, this.ag);
            hVar.d((((this.o + i) + this.q) - this.bN) + 1, this.p + i2 + 2 + ((this.bO * (this.ag - 3)) / this.bI), this.bN - 2, ((this.cH - this.bO) * (this.ag - 3)) / this.bI);
            hVar.setColor(this.u);
            hVar.c(this.o + i, this.p + i2, this.q, this.ag);
        }
    }

    public final void a(String str) {
        if (!str.endsWith(",")) {
            str = new StringBuffer().append(str).append(",").toString();
        }
        super.a(str);
        this.bM = str;
        f(str);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
    }

    public final int az() {
        return this.as;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.bw.b(2);
        this.bD = 1;
    }

    /* access modifiers changed from: protected */
    public final void e() {
    }
}
