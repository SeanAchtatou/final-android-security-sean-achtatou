package defpackage;

import com.a.a.d.h;
import com.a.a.d.i;

/* renamed from: g  reason: default package */
public class g {
    public int B = 16777215;
    public int ag;
    public int ai = 4473924;
    public com.a.a.d.g aj = com.a.a.d.g.b(0, 0, 8);
    public int al = 10066329;
    int am;
    public boolean ao = true;
    public boolean ap = true;
    private int as;
    private byte bA = 1;
    byte bB = 1;
    byte bC = 1;
    public char bD = 0;
    public byte bE;
    public byte bF;
    public byte bG = 0;
    public boolean bH;
    public byte bI;
    public byte bJ;
    public String bK = "";
    public String bL = "command";
    public String bM = "";
    private int bN;
    private int bO;
    byte bq = Byte.MAX_VALUE;
    public g[] br;
    private i bs = null;
    private i bt = null;
    public byte bu = 0;
    public byte bv = 0;
    o bw;
    byte bx = ((byte) this.aj.d(20013));
    byte by = 4;
    byte bz = 1;
    private int m;
    public int o;
    public int p;
    public int q;
    public int u = 16777215;
    public int w = 16777215;

    private void a(h hVar, i iVar, int i, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        if (iVar != null) {
            int i8 = 0;
            int i9 = 0;
            if (i5 >= 4) {
                switch (this.bA) {
                    case 0:
                        int i10 = this.o + i3;
                        i6 = this.p + i4 + ((this.ag - i) / 2);
                        i7 = i10;
                        break;
                    case 1:
                        int i11 = ((this.q - i2) / 2) + this.o + i3;
                        i6 = this.p + i4 + ((this.ag - i) / 2);
                        i7 = i11;
                        break;
                    case 2:
                        int i12 = (this.q - i2) + this.o + i3;
                        i6 = this.p + i4 + ((this.ag - i) / 2);
                        i7 = i12;
                        break;
                    default:
                        i6 = 0;
                        i7 = 0;
                        break;
                }
            } else {
                switch (this.bA) {
                    case 0:
                        int i13 = this.o + i3;
                        i6 = this.p + i4 + ((this.ag - i2) / 2);
                        i7 = i13;
                        break;
                    case 1:
                        int i14 = ((this.q - i) / 2) + this.o + i3;
                        i6 = this.p + i4 + ((this.ag - i2) / 2);
                        i7 = i14;
                        break;
                    case 2:
                        i8 = (this.q - i) + this.o + i3;
                        i9 = this.p + i4 + ((this.ag - i2) / 2);
                    default:
                        i6 = i9;
                        i7 = i8;
                        break;
                }
            }
            if (i5 > 0) {
                hVar.a(iVar, 0, 0, i, i2, i5, i7, i6, 0);
            } else {
                hVar.a(iVar, i7, i6, 0);
            }
        }
    }

    public final g E(String str) {
        for (int i = 0; i < this.bI; i++) {
            if (this.br[i].bK.equals(str)) {
                return this.br[i];
            }
        }
        return null;
    }

    public void a(int i) {
        this.bz = (byte) i;
    }

    public void a(h hVar, int i, int i2) {
    }

    public final void a(i iVar) {
        this.bs = iVar;
        if (this.bs != null) {
            this.m = this.bs.getWidth();
            this.as = this.bs.getHeight();
        }
    }

    public void a(String str) {
        if (!str.equals(this.bM)) {
            this.bM = str;
            this.am = this.aj.J(str);
        }
    }

    /* access modifiers changed from: protected */
    public void a(o oVar) {
    }

    /* access modifiers changed from: protected */
    public final void b(h hVar, int i, int i2) {
        hVar.a(this.aj);
        if (this.bD == 0) {
            if (!this.ao) {
                hVar.setColor(this.ai);
                hVar.d(this.o + i, this.p + i2, this.q, this.ag);
            }
            if (this.bs != null) {
                a(hVar, this.bs, this.m, this.as, i, i2, this.bu);
                return;
            }
            return;
        }
        if (!this.ao) {
            hVar.setColor(this.al);
            hVar.d(this.o + i, this.p + i2, this.q, this.ag);
        }
        if (this.bt != null) {
            a(hVar, this.bt, this.bN, this.bO, i, i2, this.bv);
        }
    }

    public final void b(i iVar) {
        this.bt = iVar;
        if (this.bt != null) {
            this.bN = this.bt.getWidth();
            this.bO = this.bt.getHeight();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
    }

    /* access modifiers changed from: protected */
    public final void c(h hVar, int i, int i2) {
        if (this.ap) {
            hVar.setColor(this.u);
            hVar.c(this.o + i, this.p + i2, this.q, this.ag);
            if (this.bD == 1) {
                hVar.setColor(this.B);
                hVar.c((this.o + i) - 1, (this.p + i2) - 1, this.q + 2, this.ag + 2);
            }
        }
    }

    public void d() {
    }

    /* access modifiers changed from: protected */
    public void e() {
        for (int i = 0; i < this.bI; i++) {
            if (this.br[i].bH) {
                if (this.br[i].bE == this.bG) {
                    this.br[i].bD = 1;
                } else {
                    this.br[i].bD = 0;
                }
            }
        }
    }

    public void f(int i) {
    }

    public final void g(int i) {
        this.w = i;
        for (int i2 = 0; i2 < this.bI; i2++) {
            this.br[i2].g(this.w);
        }
    }

    public final void h(int i) {
        this.u = i;
        for (int i2 = 0; i2 < this.bI; i2++) {
            this.br[i2].h(this.w);
        }
    }

    public final void i(int i) {
        this.bA = (byte) i;
    }

    /* access modifiers changed from: protected */
    public final void j(int i) {
        switch (i) {
            case 1:
                this.bG = (byte) (this.bG - this.bC);
                if (this.bG >= 0) {
                    return;
                }
                if (this.bG + this.bC == 0) {
                    this.bG = (byte) (this.bF - 1);
                    return;
                } else {
                    this.bG = (byte) (((this.bG + (this.bC * this.bB)) - 1) % (this.bC * this.bB));
                    return;
                }
            case 2:
                this.bG = (byte) (this.bG + this.bC);
                if (this.bG > this.bF - this.bC && this.bG > this.bF - 1) {
                    if (this.bG >= (this.bF + this.bC) - 1) {
                        this.bG = 0;
                        return;
                    } else {
                        this.bG = (byte) ((this.bG + 1) % this.bC);
                        return;
                    }
                } else {
                    return;
                }
            case 4:
                this.bG = (byte) (this.bG - 1);
                if (this.bG < 0) {
                    this.bG = (byte) (this.bF - 1);
                    return;
                }
                return;
            case 8:
                this.bG = (byte) (this.bG + 1);
                if (this.bG > this.bF - 1) {
                    this.bG = 0;
                    return;
                }
                return;
            case 16:
                c();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public final int k(int i) {
        for (int i2 = 0; i2 < this.bI; i2++) {
            if (this.br[i2].bH && this.br[i2].bE == i) {
                return i2;
            }
        }
        return -1;
    }
}
