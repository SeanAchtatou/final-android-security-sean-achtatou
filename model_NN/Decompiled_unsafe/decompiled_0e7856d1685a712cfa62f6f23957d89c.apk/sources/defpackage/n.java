package defpackage;

/* renamed from: n  reason: default package */
public final class n implements Runnable {
    private Thread ak = null;
    private boolean ao = false;
    private String bK;
    private String bL;
    private String bM;
    private a mb = null;
    private String mc;

    public n() {
        Class.forName("com.a.a.g.b");
        this.mb = null;
        if (this.ak == null) {
            this.ak = new Thread(this);
            this.ak.start();
        }
    }

    public n(a aVar) {
        Class.forName("com.a.a.g.b");
        this.mb = aVar;
        if (this.ak == null) {
            this.ak = new Thread(this);
            this.ak.start();
        }
    }

    public final synchronized void a(String str) {
        this.bL = str;
    }

    public final synchronized boolean m(String str, String str2) {
        boolean z = true;
        synchronized (this) {
            if (!this.ao) {
                this.bM = str;
                this.mc = str2;
                this.ao = true;
                this.bK = null;
                notify();
            } else {
                z = false;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b3 A[SYNTHETIC, Splitter:B:50:0x00b3] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ba A[Catch:{ IOException -> 0x00c1, Exception -> 0x00e0, all -> 0x00ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00d2 A[SYNTHETIC, Splitter:B:63:0x00d2] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00d9 A[Catch:{ IOException -> 0x00c1, Exception -> 0x00e0, all -> 0x00ee }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00d5 A[EDGE_INSN: B:83:0x00d5->B:65:0x00d5 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r4 = 3
        L_0x0001:
            monitor-enter(r6)
            boolean r0 = r6.ao     // Catch:{ all -> 0x0076 }
            if (r0 != 0) goto L_0x0009
            r6.wait()     // Catch:{ Exception -> 0x00f3 }
        L_0x0009:
            java.lang.String r0 = r6.bM     // Catch:{ all -> 0x0076 }
            int r0 = r0.length()     // Catch:{ all -> 0x0076 }
            if (r0 < r4) goto L_0x0079
            java.lang.String r1 = r6.bM     // Catch:{ all -> 0x0076 }
            r2 = 0
            r3 = 3
            java.lang.String r1 = r1.substring(r2, r3)     // Catch:{ all -> 0x0076 }
            java.lang.String r2 = "+86"
            if (r1 != r2) goto L_0x0079
            java.lang.String r0 = r6.bM     // Catch:{ all -> 0x0076 }
            r1 = 3
            java.lang.String r0 = r0.substring(r1)     // Catch:{ all -> 0x0076 }
        L_0x0024:
            r1 = 0
            java.lang.String r2 = r6.bK     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            if (r2 == 0) goto L_0x0092
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r2.<init>()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r3 = r6.bL     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r2 = ":"
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r2 = r6.bK     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r2 = r0
        L_0x0049:
            com.a.a.b.a r0 = com.a.a.b.c.I(r2)     // Catch:{ Exception -> 0x00a7 }
            com.a.a.g.c r0 = (com.a.a.g.c) r0     // Catch:{ Exception -> 0x00a7 }
            java.lang.String r1 = "text"
            com.a.a.g.b r1 = r0.N(r1)     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            com.a.a.g.f r1 = (com.a.a.g.f) r1     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            r1.setAddress(r2)     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            java.lang.String r2 = r6.mc     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            r1.O(r2)     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            r0.a(r1)     // Catch:{ Exception -> 0x00fb, all -> 0x00f6 }
            if (r0 == 0) goto L_0x0067
            r0.close()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x0067:
            a r0 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            if (r0 == 0) goto L_0x0071
            a r0 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r1 = 1
            r0.a(r1)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x0071:
            r0 = 0
            r6.ao = r0     // Catch:{ all -> 0x0076 }
        L_0x0074:
            monitor-exit(r6)     // Catch:{ all -> 0x0076 }
            goto L_0x0001
        L_0x0076:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0079:
            if (r0 <= 0) goto L_0x008f
            java.lang.String r0 = r6.bM     // Catch:{ all -> 0x0076 }
            r1 = 0
            r2 = 1
            java.lang.String r0 = r0.substring(r1, r2)     // Catch:{ all -> 0x0076 }
            java.lang.String r1 = "+"
            if (r0 != r1) goto L_0x008f
            java.lang.String r0 = r6.bM     // Catch:{ all -> 0x0076 }
            r1 = 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ all -> 0x0076 }
            goto L_0x0024
        L_0x008f:
            java.lang.String r0 = r6.bM     // Catch:{ all -> 0x0076 }
            goto L_0x0024
        L_0x0092:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r2.<init>()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r3 = r6.bL     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r2 = r0
            goto L_0x0049
        L_0x00a7:
            r0 = move-exception
        L_0x00a8:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00cf }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00cf }
            r2.println(r0)     // Catch:{ all -> 0x00cf }
            if (r1 == 0) goto L_0x00b6
            r1.close()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x00b6:
            a r0 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            if (r0 == 0) goto L_0x0071
            a r0 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r1 = 0
            r0.a(r1)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            goto L_0x0071
        L_0x00c1:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ all -> 0x00ee }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00ee }
            r1.println(r0)     // Catch:{ all -> 0x00ee }
            r0 = 0
            r6.ao = r0     // Catch:{ all -> 0x0076 }
            goto L_0x0074
        L_0x00cf:
            r0 = move-exception
        L_0x00d0:
            if (r1 == 0) goto L_0x00d5
            r1.close()     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x00d5:
            a r1 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            if (r1 == 0) goto L_0x00df
            a r1 = r6.mb     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
            r2 = 0
            r1.a(r2)     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x00df:
            throw r0     // Catch:{ IOException -> 0x00c1, Exception -> 0x00e0 }
        L_0x00e0:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ all -> 0x00ee }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00ee }
            r1.println(r0)     // Catch:{ all -> 0x00ee }
            r0 = 0
            r6.ao = r0     // Catch:{ all -> 0x0076 }
            goto L_0x0074
        L_0x00ee:
            r0 = move-exception
            r1 = 0
            r6.ao = r1     // Catch:{ all -> 0x0076 }
            throw r0     // Catch:{ all -> 0x0076 }
        L_0x00f3:
            r0 = move-exception
            goto L_0x0009
        L_0x00f6:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00d0
        L_0x00fb:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00a8
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.n.run():void");
    }
}
