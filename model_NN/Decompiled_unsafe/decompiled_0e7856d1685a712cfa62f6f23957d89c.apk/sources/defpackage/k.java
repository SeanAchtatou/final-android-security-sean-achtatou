package defpackage;

import com.a.a.d.h;
import com.a.a.h.b;

/* renamed from: k  reason: default package */
public final class k extends g {
    private int as;
    private int bN;
    private l bo;
    private j hd;
    private int m;

    public k(String str, int i, int i2, int i3, int i4, int i5) {
        this.bK = str;
        this.bM = new StringBuffer(b.SMS_RESULT_RECV_SUCCESS).append(",3").append(",").append(i5).append(",").toString();
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.bH = true;
        c(3, 3, i5);
    }

    private void c(int i, int i2, int i3) {
        this.bC = (byte) i2;
        this.bB = (byte) i;
        this.as = this.q / this.bC;
        this.m = i3;
        this.bN = (this.ag - this.m) / this.bB;
        this.br = new g[128];
        this.bI = 0;
        this.bF = 0;
        for (int i4 = 0; i4 < i * i2; i4++) {
            String stringBuffer = new StringBuffer("item").append(i4).toString();
            if (this.bI > this.bq - 1) {
                System.out.println("Item index too large.");
            } else {
                if (this.bI == 0) {
                    this.br = new g[(this.bI + 1)];
                } else {
                    g[] gVarArr = new g[(this.bI + 1)];
                    System.arraycopy(this.br, 0, gVarArr, 0, this.bI);
                    this.br = new g[(this.bI + 1)];
                    System.arraycopy(gVarArr, 0, this.br, 0, this.bI);
                }
                this.br[this.bI] = new m(stringBuffer, this.o + (this.as * (this.bI % this.bC)), this.p + (this.bN * (this.bI / this.bC)), this.as, this.bN);
                this.br[this.bI].ap = false;
                this.br[this.bI].a("");
                this.br[this.bI].ao = true;
                if (this.br[this.bI].bH) {
                    g gVar = this.br[this.bI];
                    byte b = this.bF;
                    this.bF = (byte) (b + 1);
                    gVar.bE = b;
                }
                this.bI = (byte) (this.bI + 1);
            }
        }
        this.hd = new j("tb", new String("hp:+50,3s,"), this.o, (this.p + this.ag) - this.m, this.q, this.m);
        this.hd.ap = false;
        this.hd.g(16777215);
        this.hd.ao = true;
    }

    public final void a(h hVar, int i, int i2) {
        super.b(hVar, i, i2);
        for (int i3 = 0; i3 < this.bI; i3++) {
            this.br[i3].a(hVar, i, i2);
            this.br[i3].ap = this.ap;
            this.br[i3].ao = this.ao;
            this.br[i3].h(this.u);
            this.br[i3].B = this.B;
            this.br[i3].al = this.al;
            this.br[i3].ai = this.ai;
            this.br[i3].g(this.w);
        }
        this.hd.ap = this.ap;
        this.hd.ao = this.ao;
        this.hd.h(this.u);
        this.hd.B = this.B;
        this.hd.al = this.al;
        this.hd.ai = this.ai;
        this.hd.g(this.w);
        this.br[this.bG].bD = (char) this.bD;
        this.br[this.bG].a(hVar, i, i2);
        this.hd.a(hVar, i, i2);
        super.c(hVar, i, i2);
    }

    public final void a(String str) {
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < str.length(); i3++) {
            if (str.charAt(i3) == ',') {
                if (i == 0) {
                    this.bB = (byte) Integer.parseInt(str.substring(i2, i3));
                } else if (i == 1) {
                    this.bC = (byte) Integer.parseInt(str.substring(i2, i3));
                } else {
                    this.m = Integer.parseInt(str.substring(i2, i3));
                }
                i2 = i3 + 1;
                i++;
            }
        }
        c(this.bB, this.bC, this.m);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
    }

    public final void b(l lVar) {
        this.bo = lVar;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.bw.b(2);
        this.bo.a(new StringBuffer().append(this.bL).append(this.br[this.bG].bL).toString());
        this.bD = 1;
    }
}
