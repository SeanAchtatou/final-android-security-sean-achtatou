package defpackage;

import com.a.a.d.h;

/* renamed from: s  reason: default package */
public final class s extends g implements o {
    private l bo;

    public s(String str, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.bM = str;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.br = new g[this.bq];
        this.bH = true;
        this.bD = 2;
    }

    public final void a(h hVar, int i, int i2) {
        super.b(hVar, i, i2);
        for (int i3 = 0; i3 < this.bI; i3++) {
            this.br[i3].a(hVar, this.o + i, this.p + i2);
        }
        int k = k(this.bG);
        if (k != -1) {
            this.br[k].a(hVar, this.o + i, this.p + i2);
        }
        super.c(hVar, i, i2);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
    }

    public final void b(int i) {
        this.bD = (char) i;
        if (this.bw != null) {
            this.bw.b(2);
        }
    }

    public final void b(l lVar) {
        this.bo = lVar;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        if (this.bF > 0) {
            int k = k(this.bG);
            if (k != -1) {
                if (this.bw != null) {
                    this.bw.b(1);
                }
                this.bD = 1;
                this.br[k].a(this);
            }
        } else if (this.bw != null) {
            this.bw.b(2);
        }
    }

    public final void d() {
        int k = k(this.bG);
        if (this.bF == 1) {
            this.bD = 1;
            if (k >= 0) {
                g gVar = this.br[k];
                gVar.bw = this;
                gVar.bD = 2;
            }
        }
        switch (this.bD) {
            case 0:
            default:
                return;
            case 1:
                if (k >= 0) {
                    this.br[k].e();
                    return;
                }
                return;
            case 2:
                e();
                return;
        }
    }

    public final void f(int i) {
        if (i == 32) {
            this.bo.a(new StringBuffer().append(this.bL).append("LSOFT").toString());
        } else if (i == 64) {
            this.bo.a(new StringBuffer().append(this.bL).append("RSOFT").toString());
        } else {
            switch (this.bD) {
                case 0:
                default:
                    return;
                case 1:
                    int k = k(this.bG);
                    if (k >= 0) {
                        this.br[k].j(i);
                        return;
                    }
                    return;
                case 2:
                    j(i);
                    return;
            }
        }
    }
}
