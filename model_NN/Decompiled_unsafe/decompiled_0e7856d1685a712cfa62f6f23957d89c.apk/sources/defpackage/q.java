package defpackage;

import com.a.a.d.g;
import com.a.a.d.h;
import com.a.a.d.i;
import com.a.a.f.b;
import game.f;
import java.io.DataInputStream;
import java.util.Random;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.ToneControl;
import javax.microedition.media.control.VolumeControl;

/* renamed from: q  reason: default package */
public class q {
    private static int[] X = {0, 5, 3, 6, 2, 4, 1, 7};
    public static g aj;
    private static int[] b = {0, 1, 2, 3, 4, 5, 6, 7};
    public static int bN;
    public static int bO = aj.J("是");
    public static int cH;
    private static b rq;
    public static Player rr;

    static {
        g b2 = g.b(0, 0, 8);
        aj = b2;
        bN = b2.getHeight();
    }

    public static boolean D(String str) {
        try {
            rq = b.a(str, false);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static i F(String str) {
        try {
            return i.K(new StringBuffer().append(str).append(".png").toString());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(new StringBuffer("Read Image Errors : ").append(str).append(".png").toString());
            return null;
        }
    }

    public static int M(int i) {
        return (new Random().nextInt() & Integer.MAX_VALUE) % i;
    }

    public static synchronized byte[] N(int i) {
        byte[] bArr;
        synchronized (q.class) {
            bArr = new byte[]{i >> 24, (byte) (i >> 16), (byte) (i >> 8), (byte) i};
        }
        return bArr;
    }

    private Player O(int i) {
        try {
            getClass();
            Player a = Manager.a(MIDPHelper.H(new StringBuffer().append("/music/").append(i).append(".mid").toString()), "audio/midi");
            rr = a;
            a.aV();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(new StringBuffer("Read Music Error,音乐文件路径 : ").append("/music/").append(".mid").toString());
        }
        return rr;
    }

    public static int a(int i, long j) {
        Random random = new Random();
        random.setSeed(System.currentTimeMillis() + j);
        return random.nextInt() % i;
    }

    public static void a(int i, i[] iVarArr, String str) {
        for (int i2 = 0; i2 < i; i2++) {
            iVarArr[i2] = F(new StringBuffer().append(str).append(i2).toString());
        }
    }

    public static void a(h hVar, int i, int i2, i iVar, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9;
        if (i8 == 1) {
            try {
                i9 = iVar.getWidth();
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(new StringBuffer().append(iVar.getWidth()).append(",").append(iVar.getHeight()).append("; ").append(i3).append(",").append(i4).append("; ").append(i5).append(",").append(i6).toString());
                System.out.println(new StringBuffer("manipulation = ").append(i7).append("   ,").append(i8 == 0 ? X[i7] : b[i7]).toString());
                return;
            }
        } else {
            i9 = i5;
        }
        hVar.a(iVar, i3, i4, i9, i8 == 1 ? iVar.getHeight() : i6, i8 == 0 ? X[i7] : b[i7], i, i2, 20);
    }

    public static void a(String str, byte[] bArr, boolean z) {
        if (z) {
            try {
                b a = b.a(str, true);
                rq = a;
                a.d(bArr, 0, bArr.length);
            } catch (Exception e) {
                System.out.println("Save Database Error");
            }
        } else {
            rq.a(1, bArr, 0, bArr.length);
        }
        c();
    }

    public static void a(Player player) {
        if (player != null && player.getState() != 0) {
            try {
                player.stop();
                player.aV();
                player.a(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void a(Player player, int i, int i2) {
        a(player);
        if (player == null) {
            return;
        }
        if (i2 != 0) {
            try {
                player.r(1);
                player.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            a(player);
        }
    }

    private static void a(int[] iArr) {
        for (int i = 0; i < iArr.length; i++) {
            for (int i2 = 0; i2 < (iArr.length - i) - 1; i2++) {
                if (iArr[i2] > iArr[i2 + 1]) {
                    int i3 = iArr[i2];
                    iArr[i2] = iArr[i2 + 1];
                    iArr[i2 + 1] = i3;
                }
            }
        }
    }

    public static void a(int[] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr[i2] = i;
        }
    }

    public static void a(i[] iVarArr) {
        if (iVarArr != null) {
            for (int i = 0; i < iVarArr.length; i++) {
                iVarArr[i] = null;
            }
        }
    }

    public static void a(f[] fVarArr) {
        if (fVarArr != null) {
            for (int i = 0; i < fVarArr.length; i++) {
                if (fVarArr[i] != null) {
                    fVarArr[i] = null;
                }
            }
        }
    }

    public static void a(int[][] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            for (int i3 = 0; i3 < iArr[i2].length; i3++) {
                iArr[i2][i3] = i;
            }
        }
    }

    public static void a(i[][] iVarArr) {
        if (iVarArr != null) {
            for (int i = 0; i < iVarArr.length; i++) {
                for (int i2 = 0; i2 < iVarArr[i].length; i2++) {
                    iVarArr[i][i2] = null;
                }
            }
        }
    }

    public static void a(int[][][] iArr, int i) {
        for (int i2 = 0; i2 < iArr.length; i2++) {
            for (int i3 = 0; i3 < iArr[i2].length; i3++) {
                for (int i4 = 0; i4 < iArr[i2][i3].length; i4++) {
                    iArr[i2][i3][i4] = i;
                }
            }
        }
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return i6 + i8 >= i2 && i4 + i2 >= i6 && i5 + i7 >= i && i + i3 >= i5;
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int[] iArr) {
        if (i6 + i8 < i2 || i4 + i2 < i6 || i5 + i7 < i || i + i3 < i5) {
            iArr[0] = 0;
            iArr[1] = 0;
            iArr[2] = 0;
            iArr[3] = 0;
            return false;
        }
        int[] iArr2 = {i, i + i3, i5, i5 + i7};
        int[] iArr3 = {i2, i2 + i4, i6, i6 + i8};
        a(iArr2);
        a(iArr3);
        iArr[0] = iArr2[1];
        iArr[1] = iArr3[1];
        iArr[2] = iArr2[2] - iArr2[1];
        iArr[3] = iArr3[2] - iArr3[1];
        return true;
    }

    public static boolean a(int i, int i2, short[] sArr) {
        return i > sArr[0] && i < sArr[0] + sArr[2] && i2 > sArr[1] && i2 < sArr[1] + sArr[3];
    }

    public static synchronized byte[] a(short s) {
        byte[] bArr;
        synchronized (q.class) {
            bArr = new byte[]{(byte) (s >> 8), (byte) s};
        }
        return bArr;
    }

    public static byte[] a(byte[] bArr) {
        try {
            bArr = rq.t(1);
        } catch (Exception e) {
            System.out.println("Read Database Error");
        }
        c();
        return bArr;
    }

    public static byte[][] a(byte[] bArr, int[] iArr) {
        int c = c(bArr, iArr);
        byte[][] bArr2 = new byte[c][];
        for (int i = 0; i < c; i++) {
            int c2 = c(bArr, iArr);
            bArr2[i] = new byte[c2];
            for (int i2 = 0; i2 < c2; i2++) {
                byte b2 = bArr[iArr[0]];
                iArr[0] = iArr[0] + 1;
                bArr2[i][i2] = b2;
            }
        }
        return bArr2;
    }

    public static short[][] aS(String str) {
        Exception e;
        short[][] sArr;
        try {
            new Object().getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.H(str));
            sArr = new short[dataInputStream.readShort()][];
            int i = 0;
            while (i < sArr.length) {
                try {
                    sArr[i] = new short[dataInputStream.readShort()];
                    for (int i2 = 0; i2 < sArr[i].length; i2++) {
                        sArr[i][i2] = dataInputStream.readShort();
                    }
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return sArr;
                }
            }
            dataInputStream.close();
        } catch (Exception e3) {
            Exception exc = e3;
            sArr = null;
            e = exc;
            e.printStackTrace();
            return sArr;
        }
        return sArr;
    }

    public static short[][][] aT(String str) {
        Exception e;
        short[][][] sArr;
        try {
            new Object().getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.H(str));
            sArr = new short[dataInputStream.readShort()][][];
            int i = 0;
            while (i < sArr.length) {
                try {
                    sArr[i] = new short[dataInputStream.readShort()][];
                    for (int i2 = 0; i2 < sArr[i].length; i2++) {
                        sArr[i][i2] = new short[dataInputStream.readShort()];
                        for (int i3 = 0; i3 < sArr[i][i2].length; i3++) {
                            sArr[i][i2][i3] = dataInputStream.readShort();
                        }
                    }
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return sArr;
                }
            }
            dataInputStream.close();
        } catch (Exception e3) {
            Exception exc = e3;
            sArr = null;
            e = exc;
            e.printStackTrace();
            return sArr;
        }
        return sArr;
    }

    public static int b(int i, long j) {
        Random random = new Random();
        random.setSeed(System.currentTimeMillis() + j);
        return (random.nextInt() & Integer.MAX_VALUE) % i;
    }

    public static synchronized int b(byte[] bArr, int i) {
        int i2;
        synchronized (q.class) {
            i2 = (bArr[i + 3] & ToneControl.SILENCE) + ((bArr[i + 2] & ToneControl.SILENCE) << 8) + ((bArr[i + 1] & ToneControl.SILENCE) << 16) + ((bArr[i] & ToneControl.SILENCE) << 24);
        }
        return i2;
    }

    public static i b(String str, int i) {
        try {
            return i.K(new StringBuffer().append(str).append(i).append(".png").toString());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(new StringBuffer("Read Image Errors : ").append(str).append(i).append(".png").toString());
            return null;
        }
    }

    public static void b(Player player) {
        if (player != null) {
            try {
                a(player);
                player.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static byte[] b(byte[] bArr, int[] iArr) {
        int c = c(bArr, iArr);
        byte[] bArr2 = new byte[c];
        for (int i = 0; i < c; i++) {
            bArr2[i] = bArr[iArr[0]];
            iArr[0] = iArr[0] + 1;
        }
        return bArr2;
    }

    public static synchronized int c(byte[] bArr, int i) {
        int i2;
        synchronized (q.class) {
            i2 = (bArr[i + 1] & ToneControl.SILENCE) + ((bArr[i] & ToneControl.SILENCE) << 8);
        }
        return i2;
    }

    private static short c(byte[] bArr, int[] iArr) {
        int i = iArr[0];
        iArr[0] = i + 1;
        int i2 = iArr[0];
        iArr[0] = i2 + 1;
        return (short) (((bArr[i] & ToneControl.SILENCE) << 8) | (bArr[i2] & ToneControl.SILENCE));
    }

    private static void c() {
        try {
            rq.bc();
        } catch (Exception e) {
            System.out.println("Close Database Error");
        }
    }

    public static void e(byte[] bArr, String str) {
        try {
            new Object().getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.H(new StringBuffer("/").append(str).toString()));
            dataInputStream.skip(0);
            dataInputStream.read(bArr, 0, bArr.length);
            dataInputStream.close();
        } catch (Exception e) {
        }
    }

    public static int k(int i) {
        return new Random().nextInt() % i;
    }

    public final void c(int i, int i2, int i3) {
        b(rr);
        if (i3 != 0) {
            try {
                Player O = O(i);
                rr = O;
                if (!(O == null || O.getState() == 0)) {
                    try {
                        O.aV();
                    } catch (MediaException e) {
                    }
                    ((VolumeControl) O.L("VolumeControl")).s(i3 * 20);
                }
                rr.r(1);
                rr.start();
                cH = i;
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else {
            a(rr);
        }
    }
}
