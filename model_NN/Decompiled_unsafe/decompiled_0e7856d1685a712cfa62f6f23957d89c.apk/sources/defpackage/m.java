package defpackage;

import com.a.a.d.h;

/* renamed from: m  reason: default package */
public final class m extends g {
    private i bn;
    private l bo;

    public m(String str, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.bM = str;
        this.bL = str;
        this.bn = new i(this.bM, i, i2, i3, i4);
        this.bn.ao = true;
        this.bn.a(this.bM);
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.bH = true;
    }

    public final void a(int i) {
        this.bz = (byte) i;
        this.bn.a(i);
    }

    public final void a(h hVar, int i, int i2) {
        super.b(hVar, i, i2);
        this.bn.g(this.w);
        this.bn.a(hVar, i, i2);
        super.c(hVar, i, i2);
    }

    public final void a(String str) {
        this.bM = str;
        super.a(this.bM);
        this.bn.a(this.bM);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
        c();
    }

    public final void b(l lVar) {
        this.bo = lVar;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.bo.a(this.bL);
        this.bw.b(2);
        this.bD = 1;
    }
}
