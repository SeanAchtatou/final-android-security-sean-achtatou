package defpackage;

import com.a.a.d.h;

/* renamed from: f  reason: default package */
public final class f extends g implements o {
    private int as = this.bx;
    private m[] bp = new m[this.bq];
    private int m = (this.bx * 3);

    public f(int i, int i2) {
        this.o = 0;
        this.p = 0;
        this.q = i;
        this.ag = i2;
        this.bD = 2;
    }

    public final s C() {
        return (s) this.br[this.bG];
    }

    public final boolean D(String str) {
        if (this.bI > this.bq - 1) {
            System.out.println("Item index too big.");
            return false;
        }
        if (this.bI == 0) {
            this.br = new g[(this.bI + 1)];
        } else {
            g[] gVarArr = new g[(this.bI + 1)];
            System.arraycopy(this.br, 0, gVarArr, 0, this.bI);
            this.br = new g[(this.bI + 1)];
            System.arraycopy(gVarArr, 0, this.br, 0, this.bI);
        }
        this.br[this.bI] = new s(str, this.o, this.p, this.q, this.ag);
        this.br[this.bI].bE = this.bF;
        this.br[this.bI].a(str);
        this.bp[this.bI] = new m(this.br[this.bI].bK, this.o, this.p, this.m, this.as);
        this.bp[this.bI].a(this.br[this.bI].bM);
        this.bp[this.bI].ap = this.ap;
        this.bp[this.bI].ao = this.ao;
        this.bp[this.bI].h(this.u);
        this.bp[this.bI].g(this.w);
        this.bG = this.bF;
        this.bF = (byte) (this.bF + 1);
        this.bI = (byte) (this.bI + 1);
        e();
        return true;
    }

    public final void a(h hVar, int i, int i2) {
        for (int i3 = 0; i3 < this.bI; i3++) {
            if (this.br[i3].ao) {
                this.bp[i3].a(this.br[i3].bM);
                this.bp[i3].a(hVar, (this.m * i3) + i, i2);
            }
        }
        if (this.bI > 0) {
            if (C().ao) {
                hVar.setColor(this.ai);
                hVar.b(this.o + i + (this.bG * this.m) + 1, ((this.p + i2) + this.as) - 1, (((this.o + i) + (this.bG * this.m)) + this.m) - 1, ((this.p + i2) + this.as) - 1);
                hVar.b(this.o + i + (this.bG * this.m) + 1, this.p + i2 + this.as, (((this.o + i) + (this.bG * this.m)) + this.m) - 1, this.p + i2 + this.as);
            }
            C().a(hVar, i, (this.as + i2) - this.as);
        }
    }

    public final void b(int i) {
        this.bD = (char) i;
        e();
    }

    public final void d() {
        ((s) this.br[this.bG]).d();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        for (int i = 0; i < this.bI; i++) {
            if (this.br[i].bE == this.bG) {
                this.br[i].bD = (char) this.bD;
                this.bp[i].bD = (char) this.bD;
                this.br[i].a(this);
            } else {
                this.br[i].bD = 0;
                this.bp[i].bD = 0;
            }
        }
    }

    public final void f(int i) {
        switch (this.bD) {
            case 0:
                C().f(i);
                return;
            case 1:
                C().f(i);
                return;
            case 2:
                switch (i) {
                    case 4:
                        this.bG = (byte) (this.bG - 1);
                        if (this.bG < 0) {
                            this.bG = (byte) (this.bF - 1);
                        }
                        e();
                        return;
                    case 8:
                        this.bG = (byte) (this.bG + 1);
                        if (this.bG > this.bF - 1) {
                            this.bG = 0;
                        }
                        e();
                        return;
                    default:
                        C().f(i);
                        return;
                }
            default:
                return;
        }
    }
}
