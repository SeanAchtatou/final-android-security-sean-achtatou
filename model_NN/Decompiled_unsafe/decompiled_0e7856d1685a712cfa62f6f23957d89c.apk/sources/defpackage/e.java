package defpackage;

import com.a.a.d.h;

/* renamed from: e  reason: default package */
public final class e extends g {
    private int as = 0;
    private i bn;
    l bo;
    private int m;

    public e(String str, String str2, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.bM = str2;
        this.m = this.aj.J(str2);
        this.bn = new i(str2, i, i2, i3, i4);
        this.bn.ao = true;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.bH = true;
    }

    public final void a(int i) {
        this.bz = (byte) i;
        this.bn.a(i);
    }

    public final void a(h hVar, int i, int i2) {
        this.as++;
        super.b(hVar, i, i2);
        switch (this.bD) {
            case 1:
                hVar.setColor(this.al);
                if (this.as % 24 > 12) {
                    hVar.b(this.o + i + this.m + this.by, this.p + i2 + ((this.ag - this.bx) / 2), this.o + i + this.m + this.by, this.p + i2 + ((this.ag - this.bx) / 2) + this.bx);
                    break;
                }
                break;
        }
        this.bn.g(this.w);
        this.bn.a(hVar, i, i2);
        super.c(hVar, i, i2);
    }

    public final void a(String str) {
        super.a(str);
        this.bM = str;
        this.m = this.aj.J(this.bM);
        this.bn.a(str);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        this.bw = oVar;
        this.bD = 2;
        h.a(this.bo);
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.bw.b(2);
        this.bD = 1;
    }
}
