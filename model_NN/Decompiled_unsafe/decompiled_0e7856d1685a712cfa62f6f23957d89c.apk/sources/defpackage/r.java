package defpackage;

import com.a.a.d.i;
import game.c;
import mm.purchasesdk.PurchaseCode;

/* renamed from: r  reason: default package */
public abstract class r extends q {
    public i[] aa;
    public int cQ = 0;
    public int cR = 0;
    public int eA = 0;
    public int eB;
    public int eC = PurchaseCode.AUTH_NOORDER;
    public int eD;
    public int eI = 320;
    public int eJ;
    public int eN;
    public int ed;
    public int ei;
    public int ej;
    public int ev;
    public int ex;
    public int ey;
    public int ez = 0;
    private int o = 0;
    public short[][][] rs;

    public final void a(int i) {
        this.ej = i;
        this.ev = i;
    }

    public void a(int i, int i2, int i3, int i4) {
        this.eC = i3;
        this.eD = i;
        this.eI = i4;
        this.eJ = i2;
    }

    public final void a(int i, int i2, c cVar) {
        this.ex = i;
        this.ey = i2;
        e(i - cVar.ed, i2 - cVar.ei);
    }

    public final void au() {
        switch (this.eB) {
            case 2:
                this.ey -= this.ev;
                if (this.ey < this.eJ) {
                    this.ey = this.eJ;
                    break;
                }
                break;
            case 4:
                this.ex -= this.ej;
                if (this.ex < this.eD) {
                    this.ex = this.eD;
                    break;
                }
                break;
            case 6:
                this.ex += this.ej;
                if (this.ex > this.eC) {
                    this.ex = this.eC;
                    break;
                }
                break;
            case 8:
                this.ey += this.ev;
                if (this.ey > this.eI) {
                    this.ey = this.eI;
                    break;
                }
                break;
        }
        c();
    }

    public final void b(int i) {
        this.eB = i;
    }

    public abstract void c();

    public final void d() {
        switch (this.eB) {
            case 2:
                this.ei -= this.ev;
                if (this.ei < this.eJ) {
                    this.ei = this.eJ;
                    break;
                }
                break;
            case 4:
                this.ed -= this.ej;
                if (this.ed < this.eD) {
                    this.ed = this.eD;
                    break;
                }
                break;
            case 6:
                this.ed += this.ej;
                if (this.ed > this.eC) {
                    this.ed = this.eC;
                    break;
                }
                break;
            case 8:
                this.ei += this.ev;
                if (this.ei > this.eI) {
                    this.ei = this.eI;
                    break;
                }
                break;
        }
        c();
        this.ed += this.cQ;
        this.ei = this.ei;
    }

    public final void e() {
        this.ed += this.ej;
        if (this.ed > this.eC) {
            this.ed = this.eC;
        }
        if (this.ed < this.eD) {
            this.ed = this.eD;
        }
        this.ei += this.ev;
        if (this.ei > this.eI) {
            this.ei = this.eI;
        }
        if (this.ei < this.eJ) {
            this.ei = this.eJ;
        }
    }

    public final void e(int i, int i2) {
        this.ed = i;
        this.ei = i2;
    }

    public final void f(int i, int i2) {
        this.ed = i;
        this.ei = i2;
        this.ex = i;
        this.ey = i2;
    }
}
