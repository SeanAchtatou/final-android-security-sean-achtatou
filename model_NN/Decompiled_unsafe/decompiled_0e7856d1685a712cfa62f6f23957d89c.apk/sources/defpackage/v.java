package defpackage;

import mm.purchasesdk.PurchaseCode;

/* renamed from: v  reason: default package */
public final class v {
    private static v sk;
    private int[] X;
    private int[] b;
    private int[] cq;
    private int[] d;
    private int o;
    private byte[][] rC;
    private byte[] rw;

    private v(String str) {
        try {
            byte[] bArr = new byte[PurchaseCode.PROTOCOL_ERR];
            q.e(bArr, str);
            int[] iArr = {0};
            this.rw = q.b(bArr, iArr);
            this.rC = q.a(bArr, iArr);
            this.X = new int[24];
            this.cq = new int[24];
            for (int i = 0; i < this.X.length; i++) {
                this.X[i] = 1 << i;
            }
            this.b = new int[2];
            this.d = new int[2];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized v P(int i) {
        v vVar;
        synchronized (v.class) {
            if (sk == null) {
                sk = new v(new StringBuffer("game").append(0).append(".key").toString());
            }
            vVar = sk;
        }
        return vVar;
    }

    private int k(int i) {
        int i2 = 0;
        byte[] bArr = this.rC[this.o];
        if (i == 35) {
            i2 = 12;
        } else if (i == 42) {
            i2 = 13;
        } else if (i < 48 || i > 57) {
            int i3 = 0;
            while (i3 < this.rw.length && i != this.rw[i3]) {
                i3++;
            }
            if (i3 < this.rw.length) {
                i2 = i3 + 1;
            }
        } else {
            i2 = i - 34;
        }
        return bArr[i2];
    }

    public final boolean Q(int i) {
        return i == -1 ? this.b[0] != 0 : (this.b[0] & this.X[i]) != 0;
    }

    public final void a(int i) {
        this.o = i;
        c();
    }

    public final void au() {
        if (this.d[0] == 1) {
            int[] iArr = this.b;
            iArr[1] = iArr[1] & (this.d[1] ^ -1);
            for (int i = 0; i < this.cq.length; i++) {
                if ((this.d[1] & (1 << i)) != 0) {
                    this.cq[i] = -1;
                }
            }
            this.d[0] = 0;
            this.d[1] = 0;
        }
    }

    public final void b(int i) {
        int k = k(i);
        int[] iArr = this.b;
        iArr[0] = iArr[0] | (1 << k);
        int[] iArr2 = this.b;
        iArr2[1] = iArr2[1] | (1 << k);
        if (this.cq[k] <= 0) {
            this.cq[k] = 1;
        }
    }

    public final void c() {
        this.b = new int[2];
        this.cq = new int[this.cq.length];
        this.d = new int[2];
    }

    public final void d() {
        for (int i = 0; i < this.cq.length; i++) {
            if (this.cq[i] > 0) {
                int[] iArr = this.cq;
                iArr[i] = iArr[i] + 1;
                if (this.cq[i] >= 128) {
                    this.cq[i] = 1;
                }
            } else if (this.cq[i] < 0) {
                int[] iArr2 = this.cq;
                iArr2[i] = iArr2[i] - 1;
                if (this.cq[i] <= -128) {
                    this.cq[i] = 0;
                }
            }
        }
    }

    public final byte dV() {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        for (int i3 = 1; i3 < 5; i3++) {
            if (this.cq[i3] < i && this.cq[i3] > 0) {
                i = this.cq[i3];
                i2 = i3 - 1;
            }
        }
        return (byte) i2;
    }

    public final byte dW() {
        int i = -1;
        int i2 = Integer.MAX_VALUE;
        for (int i3 = 1; i3 < 5; i3++) {
            if (this.cq[i3] < i2 && this.cq[i3] > 0 && this.cq[i3] % 5 == 1) {
                i2 = this.cq[i3];
                i = i3 - 1;
            }
        }
        return (byte) i;
    }

    public final boolean dX() {
        return this.cq[5] > 0;
    }

    public final void e() {
        this.b[0] = 0;
    }

    public final void f(int i) {
        int k = k(i);
        int[] iArr = this.b;
        iArr[1] = iArr[1] & ((1 << k) ^ -1);
        this.cq[k] = -1;
    }

    public final void i() {
        for (int i = 0; i < this.cq.length; i++) {
            this.cq[i] = 0;
        }
    }

    public final void j() {
        this.b = new int[2];
    }
}
