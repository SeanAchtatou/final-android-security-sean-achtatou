package defpackage;

import com.a.a.d.h;

/* renamed from: c  reason: default package */
public final class c extends g {
    private int m = 0;

    public c(String str, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        a("10");
    }

    public final void a(h hVar, int i, int i2) {
        super.b(hVar, i, i2);
        hVar.setColor(this.w);
        hVar.d(this.o + i + (this.by >> 1), this.p + i2 + (this.by >> 1), (((this.q - this.by) + 1) * this.m) / 100, (this.ag - this.by) + 1);
        super.c(hVar, i, i2);
    }

    public final void a(String str) {
        try {
            this.m = Integer.parseInt(str);
        } catch (Exception e) {
            this.m = 10;
        }
    }
}
