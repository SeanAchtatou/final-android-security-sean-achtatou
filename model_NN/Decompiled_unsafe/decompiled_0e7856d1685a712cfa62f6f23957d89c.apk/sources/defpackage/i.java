package defpackage;

import com.a.a.d.h;

/* renamed from: i  reason: default package */
public final class i extends g {
    public i(String str, int i, int i2, int i3, int i4) {
        this.bK = str;
        this.ap = false;
        this.o = i;
        this.p = i2;
        this.q = i3;
        this.ag = i4;
        this.bM = str;
    }

    public final void a(h hVar, int i, int i2) {
        super.b(hVar, i, i2);
        String str = this.bM;
        hVar.setColor(this.w);
        switch (this.bz) {
            case 0:
                hVar.a(str, this.o + i + (this.by >> 1), this.p + i2 + ((this.ag - this.bx) / 2), 0);
                break;
            case 1:
                hVar.a(str, this.o + i + ((this.q - this.am) / 2), this.p + i2 + ((this.ag - this.bx) / 2), 0);
                break;
            case 2:
                hVar.a(str, ((this.o + i) + (this.q - this.am)) - (this.by >> 1), this.p + i2 + ((this.ag - this.bx) / 2), 0);
                break;
        }
        super.c(hVar, i, i2);
    }
}
