package defpackage;

import com.a.a.d.i;
import com.a.a.e.a;
import java.io.DataInputStream;
import java.io.EOFException;
import javax.microedition.enhance.MIDPHelper;

/* renamed from: h  reason: default package */
public final class h {
    private static int ag;
    private static l bo;
    private static g fM;
    private static h fN;
    private static u fO;
    private static int o = 0;
    private static int p = 0;
    private static int q;
    private i[] aa = new i[a.GAME_B_PRESSED];
    private String[] aq = new String[a.GAME_B_PRESSED];

    private h(l lVar) {
        bo = lVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private i F(String str) {
        if (str.length() <= 0) {
            return null;
        }
        if (fO != null) {
            return fO.F(str.replace('\\', '/'));
        }
        int i = 0;
        while (true) {
            if (i >= p) {
                i = -1;
                break;
            } else if (str.equals(this.aq[i])) {
                break;
            } else {
                i++;
            }
        }
        if (i == -1) {
            return null;
        }
        return this.aa[i];
    }

    private static i G(String str) {
        try {
            if (str.length() > 4) {
                return i.K(str);
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private g a(DataInputStream dataInputStream) {
        s sVar;
        g jVar;
        try {
            String readUTF = dataInputStream.readUTF();
            int i = 0;
            for (int i2 = 0; i2 < readUTF.length(); i2++) {
                if (readUTF.charAt(i2) == ',') {
                    String substring = readUTF.substring(i, i2);
                    if (substring.length() > 0 && p < this.aq.length && fO == null) {
                        this.aq[p] = substring;
                        this.aa[p] = G(substring.replace('\\', '/'));
                        p++;
                    }
                    i = i2 + 1;
                }
            }
            o = dataInputStream.readByte();
            dataInputStream.readByte();
            short readShort = dataInputStream.readShort();
            if (q == 0) {
                q = readShort;
            }
            short readShort2 = dataInputStream.readShort();
            if (ag == 0) {
                ag = readShort2;
            }
            if (o == 1) {
                fM = new f((((readShort << 12) * q) / readShort) >> 12, (((readShort2 << 12) * ag) / readShort2) >> 12);
            }
            s sVar2 = null;
            g gVar = null;
            while (true) {
                String readUTF2 = dataInputStream.readUTF();
                if (readUTF2.equals("")) {
                    break;
                }
                int readShort3 = (((dataInputStream.readShort() << 12) * q) / readShort) >> 12;
                int readShort4 = (((dataInputStream.readShort() << 12) * ag) / readShort2) >> 12;
                int readShort5 = (((dataInputStream.readShort() << 12) * q) / readShort) >> 12;
                int readShort6 = (((dataInputStream.readShort() << 12) * ag) / readShort2) >> 12;
                String readUTF3 = dataInputStream.readUTF();
                String readUTF4 = dataInputStream.readUTF();
                String readUTF5 = dataInputStream.readUTF();
                int readInt = dataInputStream.readInt();
                boolean readBoolean = dataInputStream.readBoolean();
                boolean readBoolean2 = dataInputStream.readBoolean();
                int readInt2 = dataInputStream.readInt();
                int readInt3 = dataInputStream.readInt();
                int readInt4 = dataInputStream.readInt();
                int readInt5 = dataInputStream.readInt();
                short readShort7 = dataInputStream.readShort();
                String readUTF6 = dataInputStream.readUTF();
                String readUTF7 = dataInputStream.readUTF();
                short readShort8 = dataInputStream.readShort();
                short readShort9 = dataInputStream.readShort();
                byte readByte = dataInputStream.readByte();
                byte readByte2 = dataInputStream.readByte();
                if (readUTF2.equals("Label")) {
                    jVar = new i(readUTF3, readShort3, readShort4, readShort5, readShort6);
                } else if (readUTF2.equals("Button")) {
                    g mVar = new m(readUTF3, readShort3, readShort4, readShort5, readShort6);
                    ((m) mVar).b(bo);
                    jVar = mVar;
                } else if (readUTF2.equals("ComboBox")) {
                    g pVar = new p(sVar2, readUTF3, readShort3, readShort4, readShort5, readShort6);
                    ((p) pVar).b(bo);
                    jVar = pVar;
                } else if (readUTF2.equals("TextField")) {
                    g eVar = new e(readUTF3, readUTF4, readShort3, readShort4, readShort5, readShort6);
                    ((e) eVar).bo = bo;
                    jVar = eVar;
                } else if (readUTF2.equals("TextBox")) {
                    jVar = new j(readUTF3, readUTF4, readShort3, readShort4, readShort5, readShort6);
                } else if (readUTF2.equals("Grid")) {
                    g kVar = new k(readUTF3, readShort3, readShort4, readShort5, readShort6, readShort6 / 2);
                    ((k) kVar).b(bo);
                    jVar = kVar;
                } else if (readUTF2.equals("Progress")) {
                    jVar = new c(readUTF3, readShort3, readShort4, readShort5, readShort6);
                } else {
                    jVar = gVar;
                    if (readUTF2.equals("Form")) {
                        if (o == 0) {
                            s sVar3 = new s(readUTF3, readShort3, readShort4, readShort5, readShort6);
                            fM = sVar3;
                            sVar = sVar3;
                        } else {
                            ((f) fM).D(readUTF3);
                            sVar = ((f) fM).C();
                        }
                        sVar.b(bo);
                        sVar.o = readShort3;
                        sVar.p = readShort4;
                        sVar.q = readShort5;
                        sVar.ag = readShort6;
                        sVar2 = sVar;
                        jVar = sVar;
                    }
                }
                jVar.bL = readUTF5;
                jVar.ai = readInt;
                jVar.ao = readBoolean;
                jVar.ap = readBoolean2;
                jVar.h(readInt2);
                jVar.g(readInt3);
                jVar.B = readInt4;
                jVar.al = readInt5;
                if (!readUTF2.equals("Form")) {
                    if (sVar2.bI > sVar2.bq - 1) {
                        System.out.println("Item index too large.");
                    } else {
                        if (sVar2.bI == 0) {
                            sVar2.br = new g[(sVar2.bI + 1)];
                        } else {
                            g[] gVarArr = new g[(sVar2.bI + 1)];
                            System.arraycopy(sVar2.br, 0, gVarArr, 0, sVar2.bI);
                            sVar2.br = new g[(sVar2.bI + 1)];
                            System.arraycopy(gVarArr, 0, sVar2.br, 0, sVar2.bI);
                        }
                        sVar2.br[sVar2.bI] = jVar;
                        g gVar2 = sVar2.br[sVar2.bI];
                        byte b = sVar2.bJ;
                        sVar2.bJ = (byte) (b + 1);
                        gVar2.bJ = b;
                        if (sVar2.br[sVar2.bI].bH) {
                            sVar2.br[sVar2.bI].bE = sVar2.bF;
                            sVar2.bF = (byte) (sVar2.bF + 1);
                        }
                        sVar2.bI = (byte) (sVar2.bI + 1);
                    }
                }
                jVar.bE = (byte) ((char) readShort7);
                jVar.a(F(readUTF6));
                jVar.b(F(readUTF7));
                jVar.bu = (byte) ((char) readShort8);
                jVar.bv = (byte) ((char) readShort9);
                jVar.a((char) readByte);
                jVar.i((char) readByte2);
                jVar.a(readUTF4);
                gVar = jVar;
            }
            if (o == 1) {
                fM.bG = 0;
                fM.e();
            }
            return fM;
        } catch (EOFException e) {
            if (o == 1) {
                fM.bG = 0;
                fM.e();
            }
            return fM;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static synchronized h a(l lVar) {
        h hVar;
        synchronized (h.class) {
            if (fN == null) {
                bo = lVar;
                fN = new h(lVar);
            }
            hVar = fN;
        }
        return hVar;
    }

    public static void a(u uVar) {
        fO = uVar;
    }

    public static void ay() {
        q = 640;
        ag = 360;
    }

    public final g E(String str) {
        try {
            str.getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.H(str));
            g a = a(dataInputStream);
            dataInputStream.close();
            return a;
        } catch (Exception e) {
            return null;
        }
    }
}
