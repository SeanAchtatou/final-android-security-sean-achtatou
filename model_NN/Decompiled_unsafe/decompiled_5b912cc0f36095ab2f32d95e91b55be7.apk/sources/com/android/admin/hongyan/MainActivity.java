package com.android.admin.hongyan;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class MainActivity extends Activity {
    Handler han;

    @Override
    public void onCreate(Bundle bundle) {
        LogCatBroadcaster.start(this);
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        boolean requestWindowFeature = requestWindowFeature(1);
        setContentView((int) R.layout.main);
        boolean sendEmptyMessageDelayed = this.han.sendEmptyMessageDelayed(0, (long) 3500);
    }

    public MainActivity() {
        Handler handler;
        new Handler(this) {
            private String SHARE_APP_TAG;
            private final MainActivity this$0;

            {
                this.this$0 = r6;
            }

            static MainActivity access$0(AnonymousClass100000000 r4) {
                return r4.this$0;
            }

            @Override
            public void handleMessage(Message message) {
                Boolean bool;
                Intent intent;
                Throwable th;
                Intent intent2;
                Throwable th2;
                super.handleMessage(message);
                new Boolean(this.this$0.getSharedPreferences(this.SHARE_APP_TAG, 0).getBoolean("FIRST", true));
                if (bool.booleanValue()) {
                    Intent intent3 = intent2;
                    try {
                        new Intent(this.this$0, Class.forName("com.android.admin.hongyan.newone"));
                        this.this$0.startActivity(intent3);
                        this.this$0.finish();
                    } catch (ClassNotFoundException e) {
                        ClassNotFoundException classNotFoundException = e;
                        Throwable th3 = th2;
                        new NoClassDefFoundError(classNotFoundException.getMessage());
                        throw th3;
                    }
                } else {
                    Intent intent4 = intent;
                    try {
                        new Intent(this.this$0, Class.forName("com.android.admin.hongyan.lock"));
                        this.this$0.startActivity(intent4);
                        this.this$0.finish();
                    } catch (ClassNotFoundException e2) {
                        ClassNotFoundException classNotFoundException2 = e2;
                        Throwable th4 = th;
                        new NoClassDefFoundError(classNotFoundException2.getMessage());
                        throw th4;
                    }
                }
            }
        };
        this.han = handler;
    }
}
