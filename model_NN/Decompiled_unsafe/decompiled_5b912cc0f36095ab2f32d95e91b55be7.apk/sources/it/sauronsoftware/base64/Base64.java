package it.sauronsoftware.base64;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class Base64 {
    private static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        InputStream inputStream2 = inputStream;
        OutputStream outputStream2 = outputStream;
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream2.read(bArr);
            int i = read;
            if (read != -1) {
                outputStream2.write(bArr, 0, i);
            } else {
                return;
            }
        }
    }

    public static String decode(String str) throws RuntimeException {
        Throwable th;
        String str2;
        try {
            new String(decode(str.getBytes("ASCII")));
            return str2;
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException unsupportedEncodingException = e;
            Throwable th2 = th;
            new RuntimeException("ASCII is not supported!", unsupportedEncodingException);
            throw th2;
        }
    }

    public static String decode(String str, String str2) throws RuntimeException {
        Throwable th;
        Throwable th2;
        StringBuffer stringBuffer;
        String str3;
        String str4 = str2;
        try {
            try {
                String str5 = str3;
                new String(decode(str.getBytes("ASCII")), str4);
                return str5;
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException unsupportedEncodingException = e;
                Throwable th3 = th2;
                new StringBuffer();
                new RuntimeException(stringBuffer.append("Unsupported charset: ").append(str4).toString(), unsupportedEncodingException);
                throw th3;
            }
        } catch (UnsupportedEncodingException e2) {
            UnsupportedEncodingException unsupportedEncodingException2 = e2;
            Throwable th4 = th;
            new RuntimeException("ASCII is not supported!", unsupportedEncodingException2);
            throw th4;
        }
    }

    public static void decode(File file, File file2) throws IOException {
        Throwable th;
        InputStream inputStream;
        OutputStream outputStream;
        File file3 = file2;
        InputStream inputStream2 = null;
        OutputStream outputStream2 = null;
        try {
            new FileInputStream(file);
            inputStream2 = inputStream;
            new FileOutputStream(file3);
            outputStream2 = outputStream;
            decode(inputStream2, outputStream2);
            if (outputStream2 != null) {
                try {
                    outputStream2.close();
                } catch (Throwable th2) {
                }
            }
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                    return;
                } catch (Throwable th3) {
                    return;
                }
            } else {
                return;
            }
        } catch (Throwable th4) {
        }
        throw th;
        if (inputStream2 != null) {
            inputStream2.close();
        }
        throw th;
    }

    public static void decode(InputStream inputStream, OutputStream outputStream) throws IOException {
        InputStream inputStream2;
        new Base64InputStream(inputStream);
        copy(inputStream2, outputStream);
    }

    public static byte[] decode(byte[] bArr) throws RuntimeException {
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        Throwable th;
        new ByteArrayInputStream(bArr);
        ByteArrayInputStream byteArrayInputStream2 = byteArrayInputStream;
        new ByteArrayOutputStream();
        ByteArrayOutputStream byteArrayOutputStream2 = byteArrayOutputStream;
        try {
            decode(byteArrayInputStream2, byteArrayOutputStream2);
            try {
                byteArrayInputStream2.close();
            } catch (Throwable th2) {
            }
            try {
                byteArrayOutputStream2.close();
            } catch (Throwable th3) {
            }
            return byteArrayOutputStream2.toByteArray();
        } catch (IOException e) {
            IOException iOException = e;
            Throwable th4 = th;
            new RuntimeException("Unexpected I/O error", iOException);
            throw th4;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            try {
                byteArrayInputStream2.close();
            } catch (Throwable th7) {
            }
            byteArrayOutputStream2.close();
            throw th6;
        }
    }

    public static String encode(String str) throws RuntimeException {
        Throwable th;
        String str2;
        try {
            String str3 = str2;
            new String(encode(str.getBytes()), "ASCII");
            return str3;
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException unsupportedEncodingException = e;
            Throwable th2 = th;
            new RuntimeException("ASCII is not supported!", unsupportedEncodingException);
            throw th2;
        }
    }

    public static String encode(String str, String str2) throws RuntimeException {
        Throwable th;
        StringBuffer stringBuffer;
        Throwable th2;
        String str3;
        String str4 = str2;
        try {
            try {
                String str5 = str3;
                new String(encode(str.getBytes(str4)), "ASCII");
                return str5;
            } catch (UnsupportedEncodingException e) {
                UnsupportedEncodingException unsupportedEncodingException = e;
                Throwable th3 = th2;
                new RuntimeException("ASCII is not supported!", unsupportedEncodingException);
                throw th3;
            }
        } catch (UnsupportedEncodingException e2) {
            UnsupportedEncodingException unsupportedEncodingException2 = e2;
            Throwable th4 = th;
            new StringBuffer();
            new RuntimeException(stringBuffer.append("Unsupported charset: ").append(str4).toString(), unsupportedEncodingException2);
            throw th4;
        }
    }

    public static void encode(File file, File file2) throws IOException {
        Throwable th;
        InputStream inputStream;
        OutputStream outputStream;
        File file3 = file2;
        InputStream inputStream2 = null;
        OutputStream outputStream2 = null;
        try {
            new FileInputStream(file);
            inputStream2 = inputStream;
            new FileOutputStream(file3);
            outputStream2 = outputStream;
            encode(inputStream2, outputStream2);
            if (outputStream2 != null) {
                try {
                    outputStream2.close();
                } catch (Throwable th2) {
                }
            }
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                    return;
                } catch (Throwable th3) {
                    return;
                }
            } else {
                return;
            }
        } catch (Throwable th4) {
        }
        throw th;
        if (inputStream2 != null) {
            inputStream2.close();
        }
        throw th;
    }

    public static void encode(File file, File file2, int i) throws IOException {
        Throwable th;
        InputStream inputStream;
        OutputStream outputStream;
        File file3 = file2;
        int i2 = i;
        InputStream inputStream2 = null;
        OutputStream outputStream2 = null;
        try {
            new FileInputStream(file);
            inputStream2 = inputStream;
            new FileOutputStream(file3);
            outputStream2 = outputStream;
            encode(inputStream2, outputStream2, i2);
            if (outputStream2 != null) {
                try {
                    outputStream2.close();
                } catch (Throwable th2) {
                }
            }
            if (inputStream2 != null) {
                try {
                    inputStream2.close();
                    return;
                } catch (Throwable th3) {
                    return;
                }
            } else {
                return;
            }
        } catch (Throwable th4) {
        }
        throw th;
        if (inputStream2 != null) {
            inputStream2.close();
        }
        throw th;
    }

    public static void encode(InputStream inputStream, OutputStream outputStream) throws IOException {
        encode(inputStream, outputStream, 0);
    }

    public static void encode(InputStream inputStream, OutputStream outputStream, int i) throws IOException {
        Base64OutputStream base64OutputStream;
        new Base64OutputStream(outputStream, i);
        Base64OutputStream base64OutputStream2 = base64OutputStream;
        copy(inputStream, base64OutputStream2);
        base64OutputStream2.commit();
    }

    public static byte[] encode(byte[] bArr) throws RuntimeException {
        return encode(bArr, 0);
    }

    public static byte[] encode(byte[] bArr, int i) throws RuntimeException {
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayOutputStream byteArrayOutputStream;
        Throwable th;
        new ByteArrayInputStream(bArr);
        ByteArrayInputStream byteArrayInputStream2 = byteArrayInputStream;
        new ByteArrayOutputStream();
        ByteArrayOutputStream byteArrayOutputStream2 = byteArrayOutputStream;
        try {
            encode(byteArrayInputStream2, byteArrayOutputStream2, i);
            try {
                byteArrayInputStream2.close();
            } catch (Throwable th2) {
            }
            try {
                byteArrayOutputStream2.close();
            } catch (Throwable th3) {
            }
            return byteArrayOutputStream2.toByteArray();
        } catch (IOException e) {
            IOException iOException = e;
            Throwable th4 = th;
            new RuntimeException("Unexpected I/O error", iOException);
            throw th4;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            try {
                byteArrayInputStream2.close();
            } catch (Throwable th7) {
            }
            byteArrayOutputStream2.close();
            throw th6;
        }
    }
}
