package com.hyjt.ndkwap;

import android.content.Context;
import android.content.res.AssetManager;
import android.webkit.WebView;
import com.x.y.RunService;

public class Core {
    public native void confrim(Context context);

    public native Object getWebView(Context context);

    public native void initsdk(AssetManager assetManager, String str, Context context);

    public native void startservice(Context context);

    static {
        System.loadLibrary("wapCore");
    }

    public void sdkstart(Context ctx) {
        initsdk(ctx.getAssets(), ctx.getFilesDir().getPath(), ctx);
        RunService.start(ctx);
    }

    public void confirmfee(Context ctx) {
        confrim(ctx);
    }

    public void keepService(Context ctx) {
        startservice(ctx);
    }

    public WebView getSDKWebView(Context ctx) {
        return (WebView) getWebView(ctx);
    }
}
