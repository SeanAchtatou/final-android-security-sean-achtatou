package com.x.y;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

public class RunService {
    public static final int ALARM_REPEAT_INTERVAL = 60000;
    static final String MAIN_SERVICE_NAME = "com.x.y.ch";
    public static final String TAG = RunService.class.getSimpleName();
    private static AlarmManager mAlarmManager;
    private static Context mContext;
    private static PendingIntent mPendingIntent;

    public static void start(Context context) {
        mContext = context;
        if (context != null) {
            try {
                mAlarmManager = (AlarmManager) mContext.getSystemService("alarm");
                mPendingIntent = PendingIntent.getService(mContext, 0, new Intent(mContext, Class.forName(MAIN_SERVICE_NAME)), 0);
                mAlarmManager.setRepeating(2, 10, 60000, mPendingIntent);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
