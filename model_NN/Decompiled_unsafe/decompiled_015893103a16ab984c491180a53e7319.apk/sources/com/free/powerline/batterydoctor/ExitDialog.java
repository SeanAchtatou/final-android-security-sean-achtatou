package com.free.powerline.batterydoctor;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;

public class ExitDialog extends Dialog {
    private Context context;
    private LinearLayout view;

    public ExitDialog(Context context2) {
        super(context2, R.style.DialogThem);
        this.context = context2;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.notice);
        this.view = (LinearLayout) findViewById(R.id.llnotice);
        setCancelable(true);
    }

    public boolean showDialog() {
        if (SUtils.used()) {
            return false;
        }
        show();
        return true;
    }

    public void onStart() {
        super.onStart();
        try {
            getWindow().setGravity(80);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
