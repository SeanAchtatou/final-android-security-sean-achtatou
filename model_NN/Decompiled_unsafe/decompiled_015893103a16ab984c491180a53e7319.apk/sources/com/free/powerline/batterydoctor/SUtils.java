package com.free.powerline.batterydoctor;

import android.content.Context;
import android.content.SharedPreferences;

public class SUtils {
    private static Context context = null;
    private static final String filename = "tip";
    private static long interval = 259200000;
    private static int maxTimes = 2;
    private static SharedPreferences sharedPreferences;

    public static void init(Context ctx) {
        context = ctx;
        sharedPreferences = context.getSharedPreferences(filename, 0);
    }

    public static void setUsed(boolean used) {
        int showTimes = showTimes();
        if (showTimes < maxTimes) {
            long curr = System.currentTimeMillis();
            if (curr - getLastComfirmTime() > interval) {
                setLastComfirmTime(curr);
                setShowTimes(showTimes + 1);
            }
        }
    }

    public static boolean used() {
        long curr = System.currentTimeMillis();
        long last = getLastComfirmTime();
        int showTimes = showTimes();
        return showTimes != 0 && (showTimes != maxTimes + -1 || curr - last <= interval);
    }

    private static void setShowTimes(int times) {
        sharedPreferences.edit().putInt("times", times).apply();
    }

    private static int showTimes() {
        return sharedPreferences.getInt("times", 0);
    }

    private static void setLastComfirmTime(long time) {
        sharedPreferences.edit().putLong("time", time).apply();
    }

    private static long getLastComfirmTime() {
        return sharedPreferences.getLong("time", 0);
    }
}
