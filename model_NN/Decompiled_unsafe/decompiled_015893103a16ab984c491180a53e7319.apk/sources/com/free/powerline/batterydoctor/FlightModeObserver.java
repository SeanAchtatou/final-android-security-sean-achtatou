package com.free.powerline.batterydoctor;

import android.database.ContentObserver;
import android.os.Handler;

public class FlightModeObserver extends ContentObserver {
    private Handler handler;

    public FlightModeObserver(Handler handler2) {
        super(handler2);
        this.handler = handler2;
    }

    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        this.handler.obtainMessage(3).sendToTarget();
    }
}
