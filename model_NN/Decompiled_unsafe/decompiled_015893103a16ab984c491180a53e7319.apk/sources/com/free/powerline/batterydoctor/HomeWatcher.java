package com.free.powerline.batterydoctor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class HomeWatcher {
    private static final String TAG = "HomeWatcher";
    private Context mContext;
    private IntentFilter mFilter = new IntentFilter("android.intent.action.CLOSE_SYSTEM_DIALOGS");
    /* access modifiers changed from: private */
    public OnHomePressedListener mListener;
    private InnerRecevier mRecevier = new InnerRecevier(this, null);

    public interface OnHomePressedListener {
        void onHomeLongPressed();

        void onHomePressed();
    }

    public HomeWatcher(Context context) {
        this.mContext = context;
    }

    public void setOnHomePressedListener(OnHomePressedListener listener) {
        this.mListener = listener;
    }

    public void startWatch() {
        if (this.mRecevier != null) {
            this.mContext.registerReceiver(this.mRecevier, this.mFilter);
        }
    }

    public void stopWatch() {
        if (this.mRecevier != null) {
            this.mContext.unregisterReceiver(this.mRecevier);
        }
    }

    private class InnerRecevier extends BroadcastReceiver {
        final String SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS;
        final String SYSTEM_DIALOG_REASON_HOME_KEY;
        final String SYSTEM_DIALOG_REASON_KEY;
        final String SYSTEM_DIALOG_REASON_RECENT_APPS;

        private InnerRecevier() {
            this.SYSTEM_DIALOG_REASON_KEY = "reason";
            this.SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS = "globalactions";
            this.SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
            this.SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
        }

        /* synthetic */ InnerRecevier(HomeWatcher homeWatcher, InnerRecevier innerRecevier) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            String reason;
            String action = intent.getAction();
            if (action.equals("android.intent.action.CLOSE_SYSTEM_DIALOGS") && (reason = intent.getStringExtra("reason")) != null) {
                Log.i(HomeWatcher.TAG, "action:" + action + ",reason:" + reason);
                if (HomeWatcher.this.mListener == null) {
                    return;
                }
                if (reason.equals("homekey")) {
                    HomeWatcher.this.mListener.onHomePressed();
                } else if (reason.equals("recentapps")) {
                    HomeWatcher.this.mListener.onHomeLongPressed();
                }
            }
        }
    }
}
