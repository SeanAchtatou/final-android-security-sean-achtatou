package com.free.powerline.batterydoctor;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.free.powerline.batterydoctor.TipDialog;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.x.y.RunService;
import java.util.Random;

public class MainActivity extends Activity implements View.OnClickListener, TipDialog.Callback {
    public static boolean hadShow = false;
    /* access modifiers changed from: private */
    public AudioManager audioManager;
    private float batterPercent;
    BroadcastReceiver batterReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                MainActivity.this.setBatterPercent((float) ((intent.getIntExtra("level", 0) * 100) / intent.getIntExtra("scale", 100)));
            }
        }
    };
    private BroadcastReceiver blueToothReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.STATE_CHANGED".equals(intent.getAction())) {
                switch (intent.getIntExtra("android.bluetooth.adapter.extra.STATE", Integer.MIN_VALUE)) {
                    case ToggleUtils.FILE_SCAN /*10*/:
                        MainActivity.this.isOpenBlueTooth = false;
                        MainActivity.this.ivBluetooth.setSelected(false);
                        return;
                    case ToggleUtils.FILE_CLEAN /*11*/:
                    case ToggleUtils.BATTER_OPTIMIZE /*13*/:
                    default:
                        return;
                    case ToggleUtils.BATTER_ANALYZE /*12*/:
                        MainActivity.this.isOpenBlueTooth = true;
                        MainActivity.this.ivBluetooth.setSelected(true);
                        return;
                }
            }
        }
    };
    private BluetoothAdapter bluetoothAdapter;
    private int brightnessLevel = 0;
    int cleanPoint = 0;
    private Context context;
    private FrameLayout flBatterPercentage;
    private FlightModeObserver flightModeObserver;
    private BroadcastReceiver gpsStatusReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.location.PROVIDERS_CHANGED")) {
                MainActivity.this.isOpenGps = MainActivity.this.getGPSState(context);
                MainActivity.this.ivGPS.setSelected(MainActivity.this.isOpenGps);
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler handler = new Handler(new Handler.Callback() {
        public boolean handleMessage(Message msg) {
            String string;
            boolean z = false;
            switch (msg.what) {
                case ToggleUtils.TOGGLE_FLIGHT_MODE /*3*/:
                    int value = MainActivity.this.getFlightMode();
                    ImageView access$1 = MainActivity.this.ivFlightmode;
                    if (value == 1) {
                        z = true;
                    }
                    access$1.setSelected(z);
                    break;
                case ToggleUtils.TOGGLE_SOUND /*5*/:
                    int mode = MainActivity.this.audioManager.getRingerMode();
                    if (mode == 0) {
                        MainActivity.this.soundState = 0;
                    } else if (mode == 2) {
                        MainActivity.this.soundState = 1;
                    } else {
                        MainActivity.this.soundState = 2;
                    }
                    MainActivity.this.ivSound.setImageLevel(MainActivity.this.soundState);
                    break;
                case ToggleUtils.FILE_SCAN /*10*/:
                    MainActivity.this.optiBack.setClickable(false);
                    MainActivity.this.ivOptimize.setClickable(false);
                    MainActivity.this.optiBack.setImageLevel(0);
                    MainActivity.this.optiBar.setImageLevel(0);
                    MainActivity.this.optiBar.setVisibility(0);
                    MainActivity.this.optPoint.setText("");
                    MainActivity.this.optResult.setText(MainActivity.this.getString(R.string.scaner_file));
                    MainActivity.this.optiState = 10;
                    MainActivity.this.cleanAnimation(11);
                    break;
                case ToggleUtils.FILE_CLEAN /*11*/:
                    MainActivity.this.optiState = 11;
                    MainActivity.this.optiBack.setImageLevel(0);
                    MainActivity.this.optiBar.setImageLevel(0);
                    MainActivity.this.optiBar.setVisibility(0);
                    MainActivity.this.optPoint.setText("");
                    MainActivity.this.optResult.setText(MainActivity.this.getString(R.string.clean_file));
                    MainActivity.this.cleanAnimation(12);
                    break;
                case ToggleUtils.BATTER_ANALYZE /*12*/:
                    MainActivity.this.optiState = 12;
                    MainActivity.this.optiBack.setImageLevel(0);
                    MainActivity.this.optiBar.setImageLevel(0);
                    MainActivity.this.optiBar.setVisibility(0);
                    MainActivity.this.optPoint.setText("");
                    MainActivity.this.optResult.setText(MainActivity.this.getString(R.string.battery_analysis));
                    MainActivity.this.cleanAnimation(13);
                    break;
                case ToggleUtils.BATTER_OPTIMIZE /*13*/:
                    MainActivity.this.optiState = 13;
                    MainActivity.this.optiBack.setImageLevel(0);
                    MainActivity.this.optiBar.setImageLevel(1);
                    MainActivity.this.optiBar.setVisibility(0);
                    MainActivity.this.optPoint.setText("");
                    MainActivity.this.optResult.setText(MainActivity.this.getString(R.string.optimize_now));
                    MainActivity.this.cleanAnimation(14);
                    break;
                case ToggleUtils.OPTIMIZE_SUGGEST /*14*/:
                    MainActivity.this.optiState = 14;
                    MainActivity.this.optiBack.setImageLevel(1);
                    MainActivity.this.optiBar.setVisibility(8);
                    MainActivity.this.optResult.setText(MainActivity.this.getString(R.string.opti1));
                    MainActivity.this.optiBack.setClickable(true);
                    MainActivity.this.ivOptimize.setClickable(true);
                    int max = SUtils.used() ? 100 : 50;
                    int min = SUtils.used() ? 90 : 20;
                    int s = (new Random(System.currentTimeMillis()).nextInt(max) % ((max - min) + 1)) + min;
                    MainActivity.this.optPoint.setText(String.valueOf(s) + " Points");
                    ImageView access$6 = MainActivity.this.optiBack;
                    if (s < 80) {
                        z = true;
                    }
                    access$6.setImageLevel(z ? 1 : 0);
                    TextView access$10 = MainActivity.this.optResult;
                    if (s >= 90) {
                        string = MainActivity.this.getString(R.string.opti2);
                    } else {
                        string = MainActivity.this.getString(R.string.opti1);
                    }
                    access$10.setText(string);
                    break;
            }
            return true;
        }
    });
    /* access modifiers changed from: private */
    public boolean isOpenBlueTooth = false;
    private boolean isOpenData = false;
    private boolean isOpenFlightMode = false;
    /* access modifiers changed from: private */
    public boolean isOpenGps = false;
    private boolean isSavingMode;
    /* access modifiers changed from: private */
    public ImageView ivBluetooth;
    private ImageView ivBrightness;
    /* access modifiers changed from: private */
    public ImageView ivData;
    /* access modifiers changed from: private */
    public ImageView ivFlightmode;
    /* access modifiers changed from: private */
    public ImageView ivGPS;
    /* access modifiers changed from: private */
    public TextView ivOptimize;
    private ImageButton ivSavingMode;
    /* access modifiers changed from: private */
    public ImageView ivSound;
    private ImageView ivTimeout;
    /* access modifiers changed from: private */
    public ImageView ivWifi;
    private FrameLayout.LayoutParams lpBatterPercentage;
    private float maxBatterWidth;
    BroadcastReceiver netWorkChangeReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.net.wifi.WIFI_STATE_CHANGED".equals(intent.getAction())) {
                switch (intent.getIntExtra("wifi_state", 0)) {
                    case 1:
                        MainActivity.this.ivWifi.setSelected(false);
                        return;
                    case 2:
                    default:
                        return;
                    case ToggleUtils.TOGGLE_FLIGHT_MODE /*3*/:
                        MainActivity.this.ivWifi.setSelected(true);
                        return;
                }
            }
        }
    };
    BroadcastReceiver netWorkReciever = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo;
            try {
                if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction()) && (networkInfo = ((ConnectivityManager) context.getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo()) != null) {
                    if (!networkInfo.isConnected()) {
                        MainActivity.this.ivData.setSelected(false);
                        MainActivity.this.ivWifi.setSelected(false);
                    } else if (networkInfo.getType() == 1) {
                        MainActivity.this.ivWifi.setSelected(true);
                    } else {
                        MainActivity.this.ivData.setSelected(true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    /* access modifiers changed from: private */
    public TextView optPoint;
    /* access modifiers changed from: private */
    public TextView optResult;
    /* access modifiers changed from: private */
    public ImageView optiBack;
    /* access modifiers changed from: private */
    public ImageView optiBar;
    /* access modifiers changed from: private */
    public int optiState = 14;
    private BroadcastReceiver ringerReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.media.RINGER_MODE_CHANGED")) {
                switch (((AudioManager) MainActivity.this.getSystemService("audio")).getRingerMode()) {
                    case 0:
                        MainActivity.this.soundState = 0;
                        break;
                    case 1:
                        MainActivity.this.soundState = 2;
                        break;
                    case 2:
                        MainActivity.this.soundState = 1;
                        break;
                }
                MainActivity.this.ivSound.setImageLevel(MainActivity.this.soundState);
            }
        }
    };
    /* access modifiers changed from: private */
    public int soundState = 0;
    private int timeoutstate = 0;
    private TextView tvBatterPercentage;
    private WifiManager wifiManager;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.context = getApplicationContext();
        this.flBatterPercentage = (FrameLayout) findViewById(R.id.llfillrect);
        this.tvBatterPercentage = (TextView) findViewById(R.id.txtbattery_text);
        this.lpBatterPercentage = (FrameLayout.LayoutParams) this.flBatterPercentage.getLayoutParams();
        this.maxBatterWidth = (float) this.lpBatterPercentage.width;
        this.ivSavingMode = (ImageButton) findViewById(R.id.imgbtnmodeon);
        this.ivOptimize = (TextView) findViewById(R.id.txtoptimize);
        this.ivWifi = (ImageView) findViewById(R.id.imgbtnwifi);
        this.ivBrightness = (ImageView) findViewById(R.id.imgbtnbrightness);
        this.ivSound = (ImageView) findViewById(R.id.imgbtnSound);
        this.ivTimeout = (ImageView) findViewById(R.id.imgbtnTimeout);
        this.ivBluetooth = (ImageView) findViewById(R.id.imgbtnbluetooth);
        this.ivGPS = (ImageView) findViewById(R.id.imgbtngps);
        this.ivData = (ImageView) findViewById(R.id.imgbtnData);
        this.ivFlightmode = (ImageView) findViewById(R.id.imgbtnFlightmode);
        this.optiBack = (ImageView) findViewById(R.id.opti_back);
        this.optiBar = (ImageView) findViewById(R.id.opti_progress);
        this.optPoint = (TextView) findViewById(R.id.opti_point);
        this.optResult = (TextView) findViewById(R.id.opti_result);
        this.optiBack.setOnClickListener(this);
        this.ivWifi.setOnClickListener(this);
        findViewById(R.id.llbrightness).setOnClickListener(this);
        findViewById(R.id.llwifi).setOnClickListener(this);
        findViewById(R.id.llbluetooth).setOnClickListener(this);
        findViewById(R.id.llgps).setOnClickListener(this);
        findViewById(R.id.llFlightmode).setOnClickListener(this);
        findViewById(R.id.llData).setOnClickListener(this);
        findViewById(R.id.llSound).setOnClickListener(this);
        findViewById(R.id.llTimeout).setOnClickListener(this);
        this.ivSavingMode.setOnClickListener(this);
        this.ivOptimize.setOnClickListener(this);
        this.audioManager = (AudioManager) this.context.getApplicationContext().getSystemService("audio");
        this.soundState = this.audioManager.getRingerMode();
        this.ivSound.setImageLevel(this.soundState);
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.wifiManager = (WifiManager) this.context.getApplicationContext().getSystemService("wifi");
        setToggle();
        registerAll();
        if (!SUtils.used()) {
            TipDialog.showDialog(this, this);
            this.handler.obtainMessage(14).sendToTarget();
            return;
        }
        this.optiBack.setClickable(false);
        this.ivOptimize.setClickable(false);
        this.handler.obtainMessage(10).sendToTarget();
    }

    private void registerAll() {
        ContentResolver resolver = getContentResolver();
        this.flightModeObserver = new FlightModeObserver(this.handler);
        resolver.registerContentObserver(Settings.System.getUriFor(Build.VERSION.SDK_INT > 16 ? "airplane_mode_on" : "airplane_mode_on"), true, this.flightModeObserver);
        registerReceiver(this.ringerReceiver, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        registerReceiver(this.gpsStatusReceiver, new IntentFilter("android.location.PROVIDERS_CHANGED"));
        registerReceiver(this.blueToothReceiver, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        registerReceiver(this.netWorkChangeReceiver, new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED"));
        registerReceiver(this.netWorkReciever, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        registerReceiver(this.batterReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(this.flightModeObserver);
        unregisterReceiver(this.ringerReceiver);
        unregisterReceiver(this.gpsStatusReceiver);
        unregisterReceiver(this.blueToothReceiver);
        unregisterReceiver(this.netWorkChangeReceiver);
        unregisterReceiver(this.netWorkReciever);
        unregisterReceiver(this.batterReceiver);
    }

    private void setToggle() {
        boolean z;
        Intent intent = registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (intent != null) {
            setBatterPercent((float) ((intent.getIntExtra("level", 0) * 100) / intent.getIntExtra("scale", 100)));
        }
        this.ivTimeout.setImageLevel(time2State(getScreenOffTime()));
        this.ivBrightness.setImageLevel(lightness2State(getLightness()));
        int flightmode = getFlightMode();
        ImageView imageView = this.ivFlightmode;
        if (flightmode == 1) {
            z = true;
        } else {
            z = false;
        }
        imageView.setSelected(z);
        int mode = this.audioManager.getRingerMode();
        if (mode == 2) {
            this.soundState = 1;
        } else if (mode == 0) {
            this.soundState = 0;
        } else {
            this.soundState = 2;
        }
        this.ivSound.setImageLevel(this.soundState);
        if (this.bluetoothAdapter != null) {
            this.isOpenBlueTooth = this.bluetoothAdapter.isEnabled();
            this.ivBluetooth.setSelected(this.isOpenBlueTooth);
        }
        this.ivWifi.setSelected(this.wifiManager.isWifiEnabled());
        this.isOpenData = isOpenData();
        this.ivData.setSelected(this.isOpenData);
        this.isOpenGps = isOpenGPS();
        this.ivGPS.setSelected(this.isOpenGps);
    }

    /* access modifiers changed from: private */
    public void setBatterPercent(float percent) {
        if (percent > 100.0f) {
            percent = 100.0f;
        }
        this.batterPercent = percent;
        this.lpBatterPercentage.width = (int) ((this.maxBatterWidth * percent) / 100.0f);
        this.flBatterPercentage.setLayoutParams(this.lpBatterPercentage);
        this.tvBatterPercentage.setText(String.valueOf((int) percent) + "%");
    }

    private void setCharge(float percent) {
        if (percent > 100.0f) {
            percent = 100.0f;
        }
        this.batterPercent = percent;
        this.lpBatterPercentage.width = (int) ((this.maxBatterWidth * percent) / 100.0f);
        this.flBatterPercentage.setLayoutParams(this.lpBatterPercentage);
    }

    public void onClick(View v) {
        boolean z;
        int i = 1;
        boolean z2 = false;
        if (!TipDialog.showDialog(this, this)) {
            switch (v.getId()) {
                case R.id.imgbtnmodeon:
                    if (!this.isSavingMode) {
                        z2 = true;
                    }
                    this.isSavingMode = z2;
                    this.ivSavingMode.setSelected(this.isSavingMode);
                    return;
                case R.id.txtoptimize:
                case R.id.opti_back:
                    this.optiBack.setClickable(false);
                    this.ivOptimize.setClickable(false);
                    if (this.optiState == 14) {
                        this.handler.obtainMessage(10).sendToTarget();
                        return;
                    }
                    return;
                case R.id.llwifi:
                    if (this.wifiManager.isWifiEnabled()) {
                        this.wifiManager.setWifiEnabled(false);
                        return;
                    } else {
                        this.wifiManager.setWifiEnabled(true);
                        return;
                    }
                case R.id.llbluetooth:
                    if (this.bluetoothAdapter == null) {
                        Toast.makeText(this.context, getString(R.string.Device_does_not_support_Bluetooth), 1).show();
                        return;
                    }
                    try {
                        this.isOpenBlueTooth = this.bluetoothAdapter.isEnabled();
                        if (this.isOpenBlueTooth) {
                            this.bluetoothAdapter.disable();
                            return;
                        } else {
                            this.bluetoothAdapter.enable();
                            return;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                case R.id.llgps:
                    openGPSSetting();
                    return;
                case R.id.llFlightmode:
                    if (this.isOpenFlightMode) {
                        z = false;
                    } else {
                        z = true;
                    }
                    this.isOpenFlightMode = z;
                    if (!this.isOpenFlightMode) {
                        i = 0;
                    }
                    setFlightMode(i);
                    return;
                case R.id.llData:
                    try {
                        this.isOpenData = isOpenData();
                        this.ivData.setSelected(this.isOpenData);
                        return;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        return;
                    }
                case R.id.llSound:
                    this.soundState++;
                    if (this.soundState > 2) {
                        this.soundState = 0;
                    }
                    int mode = 0;
                    if (this.soundState == 1) {
                        mode = 2;
                    } else if (this.soundState == 2) {
                        mode = 1;
                    }
                    this.audioManager.setRingerMode(mode);
                    return;
                case R.id.llTimeout:
                    this.timeoutstate++;
                    if (this.timeoutstate > 6) {
                        this.timeoutstate = 0;
                    }
                    setScreenOffTim(state2Time(this.timeoutstate));
                    this.ivTimeout.setImageLevel(this.timeoutstate);
                    return;
                case R.id.llbrightness:
                    this.brightnessLevel++;
                    if (this.brightnessLevel > 4) {
                        this.brightnessLevel = 0;
                    }
                    setLightness(state2Lightness(this.brightnessLevel));
                    this.ivBrightness.setImageLevel(this.brightnessLevel);
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void cleanAnimation(final int next) {
        ObjectAnimator objectAnimator = ObjectAnimator.ofFloat(this.optiBar, "rotation", 0.0f, 359.0f);
        objectAnimator.setDuration(300L);
        objectAnimator.setInterpolator(new LinearInterpolator());
        objectAnimator.setRepeatCount(100);
        objectAnimator.start();
        objectAnimator.addListener(new Animator.AnimatorListener() {
            public void onAnimationStart(Animator animation) {
                MainActivity.this.cleanPoint = 0;
                MainActivity.this.optPoint.setText(String.valueOf(MainActivity.this.cleanPoint) + "%");
            }

            public void onAnimationEnd(Animator animation) {
                MainActivity.this.handler.obtainMessage(next).sendToTarget();
            }

            public void onAnimationCancel(Animator animation) {
            }

            public void onAnimationRepeat(Animator animation) {
                MainActivity.this.cleanPoint++;
                if (MainActivity.this.cleanPoint > 100) {
                    MainActivity.this.cleanPoint = 0;
                }
                MainActivity.this.optPoint.setText(String.valueOf(MainActivity.this.cleanPoint) + "%");
            }
        });
    }

    private boolean isOpenData() {
        try {
            return ((ConnectivityManager) this.context.getApplicationContext().getSystemService("connectivity")).getNetworkInfo(0).isConnected();
        } catch (Exception e) {
            return false;
        }
    }

    private int time2State(int time) {
        int state;
        if (time <= 15000) {
            return 0;
        }
        if (time <= 30000) {
            return 1;
        }
        if (time <= 60000) {
            return 2;
        }
        if (time <= 120000) {
            state = 3;
        } else if (time <= 300000) {
            state = 4;
        } else if (time <= 600000) {
            state = 5;
        } else {
            state = 6;
        }
        return state;
    }

    private int state2Time(int state) {
        if (state == 1) {
            return 30000;
        }
        if (state == 2) {
            return RunService.ALARM_REPEAT_INTERVAL;
        }
        if (state == 3) {
            return 120000;
        }
        if (state == 4) {
            return 300000;
        }
        if (state == 5) {
            return 600000;
        }
        if (state == 6) {
            return 1800000;
        }
        return 15000;
    }

    private int getScreenOffTime() {
        try {
            return Settings.System.getInt(getContentResolver(), "screen_off_timeout");
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void setScreenOffTim(int timeout) {
        try {
            Settings.System.putInt(getContentResolver(), "screen_off_timeout", timeout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.ivTimeout.setImageLevel(time2State(getScreenOffTime()));
    }

    private int getLightness() {
        try {
            return Settings.System.getInt(getContentResolver(), "screen_brightness", 0);
        } catch (Exception e) {
            return 0;
        }
    }

    private void setLightness(int value) {
        try {
            Settings.System.putInt(getContentResolver(), "screen_brightness_mode", 0);
            Settings.System.putInt(getContentResolver(), "screen_brightness", value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int lightness2State(int value) {
        return value / 51;
    }

    private int state2Lightness(int state) {
        if (state == 4) {
            return 255;
        }
        return state * 51;
    }

    /* access modifiers changed from: private */
    public int getFlightMode() {
        try {
            if (Build.VERSION.SDK_INT < 17) {
                return Settings.System.getInt(getContentResolver(), "airplane_mode_on", 0);
            }
            return Settings.Global.getInt(getContentResolver(), "airplane_mode_on", 0);
        } catch (Exception e) {
            return 0;
        }
    }

    private void setFlightMode(int value) {
        try {
            startActivity(new Intent("android.settings.AIRPLANE_MODE_SETTINGS"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openGPSSetting() {
        startActivityForResult(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"), 0);
    }

    /* access modifiers changed from: private */
    public boolean getGPSState(Context context2) {
        return ((LocationManager) context2.getSystemService("location")).isProviderEnabled("gps");
    }

    private boolean isOpenGPS() {
        try {
            LocationManager locationManager = (LocationManager) this.context.getApplicationContext().getSystemService("location");
            boolean gps = locationManager.isProviderEnabled("gps");
            boolean netWork = locationManager.isProviderEnabled("network");
            if (gps || netWork) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public void onBackPressed() {
        if (this.optiState != 14) {
            new ExitDialog(this).show();
        } else {
            super.onBackPressed();
        }
    }

    public void okClick() {
        this.handler.obtainMessage(10).sendToTarget();
    }

    public void cancelClick() {
    }
}
