package com.free.powerline.batterydoctor;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int battery_center_color = 2131099655;
        public static final int battery_first_color = 2131099656;
        public static final int battery_second_color = 2131099657;
        public static final int colorAccent = 2131099650;
        public static final int colorPrimary = 2131099648;
        public static final int colorPrimaryDark = 2131099649;
        public static final int line_first_color = 2131099653;
        public static final int line_second_color = 2131099654;
        public static final int optimize_center_color = 2131099658;
        public static final int optimize_first_color = 2131099659;
        public static final int optimize_second_color = 2131099660;
        public static final int textcolor = 2131099651;
        public static final int textwhitecolor = 2131099652;
    }

    public static final class dimen {
        public static final int batter_heidth = 2131165191;
        public static final int batter_left = 2131165192;
        public static final int batter_top = 2131165193;
        public static final int batter_widt = 2131165190;
        public static final int opti_size = 2131165187;
        public static final int opti_text_size = 2131165188;
        public static final int opti_text_small_size = 2131165189;
        public static final int textlargesize = 2131165184;
        public static final int textnormalsize = 2131165185;
        public static final int textsmallsize = 2131165186;
    }

    public static final class drawable {
        public static final int betteryimage_100 = 2130837504;
        public static final int betteryimagechanges_100 = 2130837505;
        public static final int bluetooh36 = 2130837506;
        public static final int bluetooh_36 = 2130837507;
        public static final int brightness_1_36 = 2130837508;
        public static final int brightness_2_36 = 2130837509;
        public static final int brightness_3_36 = 2130837510;
        public static final int brightness_4_36 = 2130837511;
        public static final int brightness_5_36 = 2130837512;
        public static final int data36 = 2130837513;
        public static final int data_36 = 2130837514;
        public static final int error = 2130837515;
        public static final int flight36 = 2130837516;
        public static final int flight_36 = 2130837517;
        public static final int gps36 = 2130837518;
        public static final int gps_36 = 2130837519;
        public static final int gradient_battery_icon_fill_color = 2130837520;
        public static final int gradient_header_tab = 2130837521;
        public static final int gradient_header_tab_click = 2130837522;
        public static final int hand = 2130837523;
        public static final int header_frame = 2130837524;
        public static final int ic_luacher = 2130837525;
        public static final int opti_back_selector = 2130837526;
        public static final int opti_rotate_button_back = 2130837527;
        public static final int opti_rotate_button_mask2 = 2130837528;
        public static final int opti_rotate_button_round_green = 2130837529;
        public static final int opti_rotate_button_round_orange = 2130837530;
        public static final int opti_rotate_button_round_red = 2130837531;
        public static final int opti_round_selector = 2130837532;
        public static final int optimize_button_back_color = 2130837533;
        public static final int optimize_button_background = 2130837534;
        public static final int optimize_button_color = 2130837535;
        public static final int options_frame = 2130837536;
        public static final int selector_bluetooth = 2130837537;
        public static final int selector_brightness = 2130837538;
        public static final int selector_gps = 2130837539;
        public static final int selector_sound = 2130837540;
        public static final int selector_timeout = 2130837541;
        public static final int selector_wifi36 = 2130837542;
        public static final int seletor_data = 2130837543;
        public static final int seletor_flight = 2130837544;
        public static final int silent_32 = 2130837545;
        public static final int sound_3_32 = 2130837546;
        public static final int timeout_10m_36 = 2130837547;
        public static final int timeout_15s36 = 2130837548;
        public static final int timeout_1m_36 = 2130837549;
        public static final int timeout_2m_36 = 2130837550;
        public static final int timeout_30m_36 = 2130837551;
        public static final int timeout_30s_36 = 2130837552;
        public static final int timeout_5m_36 = 2130837553;
        public static final int vibrate_32 = 2130837554;
        public static final int vibrate_32_white = 2130837555;
        public static final int wifi36 = 2130837556;
        public static final int wifi_36 = 2130837557;
    }

    public static final class id {
        public static final int btnDialogOK = 2131230733;
        public static final int btnclose = 2131230735;
        public static final int hand = 2131230734;
        public static final int imgInfo = 2131230736;
        public static final int imgSettings = 2131230738;
        public static final int imgbtnData = 2131230753;
        public static final int imgbtnFlightmode = 2131230750;
        public static final int imgbtnSound = 2131230756;
        public static final int imgbtnTimeout = 2131230759;
        public static final int imgbtnbluetooth = 2131230744;
        public static final int imgbtnbrightness = 2131230762;
        public static final int imgbtngps = 2131230747;
        public static final int imgbtnmodeon = 2131230726;
        public static final int imgbtnwifi = 2131230741;
        public static final int llBatterymain = 2131230720;
        public static final int llData = 2131230752;
        public static final int llFlightmode = 2131230749;
        public static final int llSound = 2131230755;
        public static final int llTimeout = 2131230758;
        public static final int ll_optionview = 2131230727;
        public static final int llbetteryimage = 2131230722;
        public static final int llbluetooth = 2131230743;
        public static final int llbrightness = 2131230761;
        public static final int llfillrect = 2131230723;
        public static final int llgps = 2131230746;
        public static final int llmain = 2131230721;
        public static final int llnotice = 2131230739;
        public static final int llwifi = 2131230740;
        public static final int opti_back = 2131230729;
        public static final int opti_point = 2131230731;
        public static final int opti_progress = 2131230730;
        public static final int opti_result = 2131230732;
        public static final int txtData = 2131230754;
        public static final int txtFlightmodeoption = 2131230751;
        public static final int txtSound = 2131230757;
        public static final int txtTimeout = 2131230760;
        public static final int txtbattery_text = 2131230724;
        public static final int txtbluetoothoption = 2131230745;
        public static final int txtbrightnessoption = 2131230763;
        public static final int txtgpsoption = 2131230748;
        public static final int txtinfoname = 2131230737;
        public static final int txtmode = 2131230725;
        public static final int txtoptimize = 2131230728;
        public static final int txtwifioption = 2131230742;
    }

    public static final class layout {
        public static final int activity_main = 2130903040;
        public static final int clean = 2130903041;
        public static final int confirm = 2130903042;
        public static final int header_info_main = 2130903043;
        public static final int notice = 2130903044;
        public static final int wifi_options = 2130903045;
    }

    public static final class string {
        public static final int Battery = 2130968578;
        public static final int Bluetooth = 2130968584;
        public static final int Brightness = 2130968590;
        public static final int Charge = 2130968579;
        public static final int Data = 2130968587;
        public static final int Device_does_not_support_Bluetooth = 2130968592;
        public static final int Flight_Mode = 2130968586;
        public static final int GPS = 2130968585;
        public static final int Mode = 2130968580;
        public static final int Sound = 2130968588;
        public static final int Task_Killer = 2130968581;
        public static final int Timeout = 2130968589;
        public static final int Wifi = 2130968583;
        public static final int app_name = 2130968576;
        public static final int battery_analysis = 2130968598;
        public static final int batttery_Details = 2130968582;
        public static final int clean_file = 2130968597;
        public static final int opti1 = 2130968593;
        public static final int opti2 = 2130968594;
        public static final int opti3 = 2130968595;
        public static final int optimize = 2130968577;
        public static final int optimize_now = 2130968599;
        public static final int optiziming = 2130968601;
        public static final int saving_mode = 2130968591;
        public static final int scaner_file = 2130968596;
        public static final int show_tip = 2130968602;
        public static final int start_now = 2130968600;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131034112;
        public static final int AppTheme = 2131034113;
        public static final int ButtonText = 2131034114;
        public static final int DialogThem = 2131034115;
    }
}
