package com.free.powerline.batterydoctor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class TipDialog extends Dialog implements View.OnClickListener {
    public static boolean isShow = false;
    private Callback callback;
    private View close;
    private Context context;
    private View hand;
    private View ok;

    public interface Callback {
        void cancelClick();

        void okClick();
    }

    public TipDialog(Context context2) {
        super(context2, R.style.DialogThem);
        this.context = context2;
    }

    private void InitView() {
        this.ok = findViewById(R.id.btnDialogOK);
        this.hand = findViewById(R.id.hand);
        this.close = findViewById(R.id.btnclose);
        this.ok.setOnClickListener(this);
        this.close.setOnClickListener(this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.confirm);
        InitView();
        setCancelable(false);
    }

    public void show() {
        if (!isShowing() && !SUtils.used()) {
            isShow = true;
            super.show();
        }
    }

    public boolean showDialog() {
        if (SUtils.used()) {
            return false;
        }
        show();
        return true;
    }

    public void onStart() {
        super.onStart();
        WindowManager m = getWindow().getWindowManager();
        WindowManager.LayoutParams lParams = getWindow().getAttributes();
        Display display = m.getDefaultDisplay();
        lParams.width = (int) (((double) display.getWidth()) * 0.75d);
        lParams.height = (int) (((double) display.getHeight()) * 0.65d);
        getWindow().setAttributes(lParams);
        showHand();
    }

    private void showHand() {
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(this.hand, "scaleX", 1.0f, 0.8f);
        scaleX.setRepeatCount(-1);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(this.hand, "scaleY", 1.0f, 0.8f);
        scaleY.setRepeatCount(-1);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(800L);
        animatorSet.play(scaleX).with(scaleY);
        animatorSet.start();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDialogOK:
            case R.id.btnclose:
                MainActivity.hadShow = true;
                XApplication.getInstance().ConfirmFee();
                if (this.callback != null) {
                    this.callback.okClick();
                    break;
                }
                break;
        }
        dismiss();
    }

    public void setCallback(Callback callback2) {
        this.callback = callback2;
    }

    public void dismiss() {
        super.dismiss();
        isShow = false;
    }

    public static boolean showDialog(Activity context2, Callback callback2) {
        if (isShow) {
            return true;
        }
        TipDialog showTipDialog = new TipDialog(context2);
        showTipDialog.setCallback(callback2);
        return showTipDialog.showDialog();
    }
}
