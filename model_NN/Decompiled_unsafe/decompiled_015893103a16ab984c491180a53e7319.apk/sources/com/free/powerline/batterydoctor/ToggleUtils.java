package com.free.powerline.batterydoctor;

public class ToggleUtils {
    public static final int BATTER_ANALYZE = 12;
    public static final int BATTER_OPTIMIZE = 13;
    public static final int FILE_CLEAN = 11;
    public static final int FILE_SCAN = 10;
    public static final int OPTIMIZE_SUGGEST = 14;
    public static final int TOGGLE_BLUETOOTH = 1;
    public static final int TOGGLE_BRIGHTNESS = 7;
    public static final int TOGGLE_DATA = 4;
    public static final int TOGGLE_FLIGHT_MODE = 3;
    public static final int TOGGLE_GPS = 2;
    public static final int TOGGLE_SOUND = 5;
    public static final int TOGGLE_TIMEOUT = 6;
    public static final int TOGGLE_WIFI = 0;
}
