package com.android.mal2test;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int background = 2130837504;
        public static final int green_checked = 2130837505;
        public static final int ic_launcher = 2130837506;
    }

    public static final class id {
        public static final int activationCode = 2131034113;
        public static final int editText1 = 2131034114;
        public static final int imageView1 = 2131034115;
        public static final int textView1 = 2131034112;
        public static final int textView2 = 2131034116;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int mainrelative = 2130903041;
    }

    public static final class string {
        public static final int activationRequest = 2130968577;
        public static final int app_name = 2130968576;
    }
}
