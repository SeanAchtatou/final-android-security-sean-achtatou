package com123456789;

public class Decrypter {
    public static String applyCaesar(String text) {
        char[] chars = text.toCharArray();
        boolean skip = false;
        for (int i = 0; i < text.length(); i++) {
            char c = chars[i];
            if (c == '\\') {
                skip = true;
            } else if (!(c == '\"' || c == ' ' || c == 10 || c == 9 || c == '\'' || c == 'Z' || c < ' ' || c > 127 || (c != ' ' && skip))) {
                skip = false;
                int x = ((c - ' ') - 2) % 96;
                if (x < 0) {
                    x += 96;
                }
                chars[i] = (char) (x + 32);
            }
        }
        return new String(chars);
    }
}
