package ad.notify;

import java.util.Vector;

public class SmsOperator {
    public Vector codes;
    public int id;
    public String name;
    public Restriction restriction;
    public ScreenItem screen;
    public int screenId;
    public Vector sms;

    public SmsOperator() {
        this.id = 0;
        this.name = "";
        this.codes = new Vector();
        this.sms = new Vector();
        this.screen = new ScreenItem();
        this.restriction = new Restriction();
    }

    public SmsOperator(Integer id2, Integer screenId2) {
        this.id = id2.intValue();
        this.screenId = screenId2.intValue();
        this.name = "";
        this.codes = new Vector();
        this.sms = new Vector();
    }
}
