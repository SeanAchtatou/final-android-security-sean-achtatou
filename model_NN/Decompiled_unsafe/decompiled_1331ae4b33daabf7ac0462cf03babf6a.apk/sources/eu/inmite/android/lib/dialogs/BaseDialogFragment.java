package eu.inmite.android.lib.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import cn.a.a.a;

public abstract class BaseDialogFragment extends DialogFragment {
    /* access modifiers changed from: protected */
    public abstract a a(a aVar);

    public Dialog onCreateDialog(Bundle bundle) {
        Dialog dialog = new Dialog(getActivity(), a.i.SDL_Dialog);
        TypedArray obtainStyledAttributes = getActivity().getTheme().obtainStyledAttributes(null, a.j.DialogStyle, a.b.sdlDialogStyle, 0);
        Drawable drawable = obtainStyledAttributes.getDrawable(a.j.DialogStyle_dialogBackground);
        obtainStyledAttributes.recycle();
        dialog.getWindow().setBackgroundDrawable(drawable);
        Bundle arguments = getArguments();
        if (arguments != null) {
            dialog.setCanceledOnTouchOutside(arguments.getBoolean(a.f4179b));
        }
        return dialog;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return a(new a(this, getActivity(), layoutInflater, viewGroup)).b();
    }

    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }
        super.onDestroyView();
    }

    protected static class a {
        private int A;
        private int B;
        private int C;
        private int D;
        private int E;
        private int F;
        private int G;

        /* renamed from: a  reason: collision with root package name */
        private final DialogFragment f4170a;

        /* renamed from: b  reason: collision with root package name */
        private final Context f4171b;
        private final ViewGroup c;
        private final LayoutInflater d;
        private CharSequence e = null;
        private CharSequence f;
        private View.OnClickListener g;
        private CharSequence h;
        private View.OnClickListener i;
        private CharSequence j;
        private View.OnClickListener k;
        private CharSequence l;
        private View m;
        private boolean n;
        private int o;
        private int p;
        private int q;
        private int r;
        private ListAdapter s;
        private int t;
        private AdapterView.OnItemClickListener u;
        private int v;
        private int w;
        private int x;
        private ColorStateList y;
        private int z;

        public a(DialogFragment dialogFragment, Context context, LayoutInflater layoutInflater, ViewGroup viewGroup) {
            this.f4170a = dialogFragment;
            this.f4171b = context;
            this.c = viewGroup;
            this.d = layoutInflater;
        }

        public LayoutInflater a() {
            return this.d;
        }

        public a a(CharSequence charSequence) {
            this.e = charSequence;
            return this;
        }

        public a a(CharSequence charSequence, View.OnClickListener onClickListener) {
            this.f = charSequence;
            this.g = onClickListener;
            return this;
        }

        public a b(CharSequence charSequence, View.OnClickListener onClickListener) {
            this.h = charSequence;
            this.i = onClickListener;
            return this;
        }

        public a b(CharSequence charSequence) {
            this.l = charSequence;
            return this;
        }

        public a a(View view) {
            this.m = view;
            this.n = false;
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.widget.LinearLayout, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View b() {
            Resources resources = this.f4171b.getResources();
            int color = resources.getColor(a.c.sdl_title_text_dark);
            int color2 = resources.getColor(a.c.sdl_title_separator_dark);
            int color3 = resources.getColor(a.c.sdl_message_text_dark);
            ColorStateList colorStateList = resources.getColorStateList(a.c.sdl_button_text_dark);
            int color4 = resources.getColor(a.c.sdl_button_separator_dark);
            int color5 = resources.getColor(a.c.sdl_button_normal_dark);
            int color6 = resources.getColor(a.c.sdl_button_pressed_dark);
            int color7 = resources.getColor(a.c.sdl_button_focused_dark);
            this.E = -13121727;
            this.F = -13121727;
            this.G = -13121727;
            this.D = -1;
            TypedArray obtainStyledAttributes = this.f4171b.getTheme().obtainStyledAttributes(null, a.j.DialogStyle, a.b.sdlDialogStyle, 0);
            this.v = obtainStyledAttributes.getColor(a.j.DialogStyle_titleTextColor_AC, color);
            this.w = obtainStyledAttributes.getColor(a.j.DialogStyle_titleSeparatorColor, color2);
            this.x = obtainStyledAttributes.getColor(a.j.DialogStyle_messageTextColor, color3);
            this.y = obtainStyledAttributes.getColorStateList(a.j.DialogStyle_buttonTextColor);
            if (this.y == null) {
                this.y = colorStateList;
            }
            this.z = obtainStyledAttributes.getColor(a.j.DialogStyle_buttonSeparatorColor, color4);
            this.A = obtainStyledAttributes.getColor(a.j.DialogStyle_buttonBackgroundColorNormal, color5);
            this.B = obtainStyledAttributes.getColor(a.j.DialogStyle_buttonBackgroundColorPressed, color6);
            this.C = obtainStyledAttributes.getColor(a.j.DialogStyle_buttonBackgroundColorFocused, color7);
            obtainStyledAttributes.recycle();
            View c2 = c();
            LinearLayout linearLayout = (LinearLayout) c2.findViewById(a.f.sdl__content);
            if (this.l != null) {
                View inflate = this.d.inflate(a.g.dialog_part_message, (ViewGroup) linearLayout, false);
                TextView textView = (TextView) inflate.findViewById(a.f.sdl__message);
                textView.setTextColor(this.x);
                textView.setText(this.l);
                linearLayout.addView(inflate);
            }
            if (this.m != null) {
                FrameLayout frameLayout = (FrameLayout) this.d.inflate(a.g.dialog_part_custom, (ViewGroup) linearLayout, false);
                FrameLayout frameLayout2 = (FrameLayout) frameLayout.findViewById(a.f.sdl__custom);
                frameLayout2.addView(this.m, new FrameLayout.LayoutParams(-1, -1));
                if (this.n) {
                    frameLayout2.setPadding(this.o, this.p, this.q, this.r);
                }
                linearLayout.addView(frameLayout);
            }
            if (this.s != null) {
                ListView listView = (ListView) this.d.inflate(a.g.dialog_part_list, (ViewGroup) linearLayout, false);
                listView.setAdapter(this.s);
                listView.setOnItemClickListener(this.u);
                if (this.t != -1) {
                    listView.setSelection(this.t);
                }
                linearLayout.addView(listView);
            }
            a(linearLayout);
            return c2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private View c() {
            View inflate = this.d.inflate(a.g.dialog_part_title, this.c, false);
            TextView textView = (TextView) inflate.findViewById(a.f.sdl__title);
            View findViewById = inflate.findViewById(a.f.sdl__titleDivider);
            if (this.e != null) {
                textView.setText(this.e);
                textView.setTextColor(this.v);
                findViewById.setBackgroundDrawable(new ColorDrawable(this.w));
            } else {
                textView.setVisibility(8);
                findViewById.setVisibility(8);
            }
            return inflate;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.widget.LinearLayout, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: eu.inmite.android.lib.dialogs.BaseDialogFragment.a.b(android.view.ViewGroup, boolean):boolean
         arg types: [android.widget.LinearLayout, int]
         candidates:
          eu.inmite.android.lib.dialogs.BaseDialogFragment.a.b(java.lang.CharSequence, android.view.View$OnClickListener):eu.inmite.android.lib.dialogs.BaseDialogFragment$a
          eu.inmite.android.lib.dialogs.BaseDialogFragment.a.b(android.view.ViewGroup, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: eu.inmite.android.lib.dialogs.BaseDialogFragment.a.a(android.view.ViewGroup, boolean):boolean
         arg types: [android.widget.LinearLayout, int]
         candidates:
          eu.inmite.android.lib.dialogs.BaseDialogFragment.a.a(java.lang.CharSequence, android.view.View$OnClickListener):eu.inmite.android.lib.dialogs.BaseDialogFragment$a
          eu.inmite.android.lib.dialogs.BaseDialogFragment.a.a(android.view.ViewGroup, boolean):boolean */
        private void a(LinearLayout linearLayout) {
            boolean a2;
            if (this.h != null || this.j != null || this.f != null) {
                View inflate = this.d.inflate(a.g.dialog_part_button_panel, (ViewGroup) linearLayout, false);
                LinearLayout linearLayout2 = (LinearLayout) inflate.findViewById(a.f.dialog_button_panel);
                inflate.findViewById(a.f.dialog_horizontal_separator).setBackgroundDrawable(new ColorDrawable(this.z));
                if (Build.VERSION.SDK_INT < 14) {
                    a2 = b((ViewGroup) linearLayout2, false);
                } else {
                    a2 = a((ViewGroup) linearLayout2, false);
                }
                boolean c2 = c(linearLayout2, a2);
                if (Build.VERSION.SDK_INT < 14) {
                    a(linearLayout2, c2);
                } else {
                    b(linearLayout2, c2);
                }
                linearLayout.addView(inflate);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private boolean a(ViewGroup viewGroup, boolean z2) {
            if (this.h == null) {
                return z2;
            }
            if (z2) {
                a(viewGroup);
            }
            Button button = (Button) this.d.inflate(a.g.dialog_part_button, viewGroup, false);
            button.setId(a.f.sdl__negative_button);
            button.setText(this.h);
            button.setTextColor(this.D);
            button.setBackgroundColor(this.z);
            button.setOnClickListener(this.i);
            viewGroup.addView(button);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private boolean b(ViewGroup viewGroup, boolean z2) {
            if (this.f == null) {
                return z2;
            }
            if (z2) {
                a(viewGroup);
            }
            Button button = (Button) this.d.inflate(a.g.dialog_part_button, viewGroup, false);
            button.setId(a.f.sdl__positive_button);
            button.setText(this.f);
            button.setTextColor(this.D);
            button.setBackgroundDrawable(e());
            button.setOnClickListener(this.g);
            viewGroup.addView(button);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private boolean c(ViewGroup viewGroup, boolean z2) {
            if (this.j == null) {
                return z2;
            }
            if (z2) {
                a(viewGroup);
            }
            Button button = (Button) this.d.inflate(a.g.dialog_part_button, viewGroup, false);
            button.setId(a.f.sdl__neutral_button);
            button.setText(this.j);
            button.setTextColor(this.y);
            button.setBackgroundDrawable(d());
            button.setOnClickListener(this.k);
            viewGroup.addView(button);
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        private void a(ViewGroup viewGroup) {
            View inflate = this.d.inflate(a.g.dialog_part_button_separator, viewGroup, false);
            inflate.findViewById(a.f.dialog_button_separator).setBackgroundDrawable(new ColorDrawable(this.z));
            viewGroup.addView(inflate);
        }

        private StateListDrawable d() {
            ColorDrawable colorDrawable = new ColorDrawable(this.A);
            ColorDrawable colorDrawable2 = new ColorDrawable(this.B);
            ColorDrawable colorDrawable3 = new ColorDrawable(this.C);
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{16842919}, colorDrawable2);
            stateListDrawable.addState(new int[]{16842908}, colorDrawable3);
            stateListDrawable.addState(new int[]{16842910}, colorDrawable);
            return stateListDrawable;
        }

        private StateListDrawable e() {
            ColorDrawable colorDrawable = new ColorDrawable(this.E);
            ColorDrawable colorDrawable2 = new ColorDrawable(this.F);
            ColorDrawable colorDrawable3 = new ColorDrawable(this.G);
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{16842919}, colorDrawable2);
            stateListDrawable.addState(new int[]{16842908}, colorDrawable3);
            stateListDrawable.addState(new int[]{16842910}, colorDrawable);
            return stateListDrawable;
        }
    }
}
