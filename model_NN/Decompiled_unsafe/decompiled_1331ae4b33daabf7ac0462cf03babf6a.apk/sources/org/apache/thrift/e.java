package org.apache.thrift;

import org.apache.thrift.protocol.a;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.h;
import org.apache.thrift.transport.c;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final f f4209a;

    /* renamed from: b  reason: collision with root package name */
    private final c f4210b;

    public e() {
        this(new a.C0067a());
    }

    public e(h hVar) {
        this.f4210b = new c();
        this.f4209a = hVar.a(this.f4210b);
    }

    public void a(b bVar, byte[] bArr) {
        try {
            this.f4210b.a(bArr);
            bVar.a(this.f4209a);
        } finally {
            this.f4209a.y();
        }
    }
}
