package org.apache.thrift;

import java.io.ByteArrayOutputStream;
import org.apache.thrift.protocol.a;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.h;
import org.apache.thrift.transport.a;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f4211a;

    /* renamed from: b  reason: collision with root package name */
    private final a f4212b;
    private f c;

    public g() {
        this(new a.C0067a());
    }

    public g(h hVar) {
        this.f4211a = new ByteArrayOutputStream();
        this.f4212b = new org.apache.thrift.transport.a(this.f4211a);
        this.c = hVar.a(this.f4212b);
    }

    public byte[] a(b bVar) {
        this.f4211a.reset();
        bVar.b(this.c);
        return this.f4211a.toByteArray();
    }
}
