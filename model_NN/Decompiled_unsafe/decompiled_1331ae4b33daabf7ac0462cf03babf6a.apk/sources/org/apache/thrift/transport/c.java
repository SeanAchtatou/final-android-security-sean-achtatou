package org.apache.thrift.transport;

public final class c extends d {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f4238a;

    /* renamed from: b  reason: collision with root package name */
    private int f4239b;
    private int c;

    public int a(byte[] bArr, int i, int i2) {
        int c2 = c();
        if (i2 > c2) {
            i2 = c2;
        }
        if (i2 > 0) {
            System.arraycopy(this.f4238a, this.f4239b, bArr, i, i2);
            a(i2);
        }
        return i2;
    }

    public void a(int i) {
        this.f4239b += i;
    }

    public void a(byte[] bArr) {
        c(bArr, 0, bArr.length);
    }

    public byte[] a() {
        return this.f4238a;
    }

    public int b() {
        return this.f4239b;
    }

    public void b(byte[] bArr, int i, int i2) {
        throw new UnsupportedOperationException("No writing allowed!");
    }

    public int c() {
        return this.c - this.f4239b;
    }

    public void c(byte[] bArr, int i, int i2) {
        this.f4238a = bArr;
        this.f4239b = i;
        this.c = i + i2;
    }
}
