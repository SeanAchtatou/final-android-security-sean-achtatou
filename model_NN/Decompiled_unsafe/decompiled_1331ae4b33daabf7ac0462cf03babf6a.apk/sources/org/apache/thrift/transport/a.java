package org.apache.thrift.transport;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f4236a = null;

    /* renamed from: b  reason: collision with root package name */
    protected OutputStream f4237b = null;

    protected a() {
    }

    public a(OutputStream outputStream) {
        this.f4237b = outputStream;
    }

    public int a(byte[] bArr, int i, int i2) {
        if (this.f4236a == null) {
            throw new e(1, "Cannot read from null inputStream");
        }
        try {
            int read = this.f4236a.read(bArr, i, i2);
            if (read >= 0) {
                return read;
            }
            throw new e(4);
        } catch (IOException e) {
            throw new e(0, e);
        }
    }

    public void b(byte[] bArr, int i, int i2) {
        if (this.f4237b == null) {
            throw new e(1, "Cannot write to null outputStream");
        }
        try {
            this.f4237b.write(bArr, i, i2);
        } catch (IOException e) {
            throw new e(0, e);
        }
    }
}
