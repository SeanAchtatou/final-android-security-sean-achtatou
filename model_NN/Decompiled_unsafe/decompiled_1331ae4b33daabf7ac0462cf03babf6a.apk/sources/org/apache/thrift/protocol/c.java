package org.apache.thrift.protocol;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public final String f4225a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f4226b;
    public final short c;

    public c() {
        this("", (byte) 0, 0);
    }

    public c(String str, byte b2, short s) {
        this.f4225a = str;
        this.f4226b = b2;
        this.c = s;
    }
}
