package org.apache.thrift.protocol;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    public final byte f4233a;

    /* renamed from: b  reason: collision with root package name */
    public final int f4234b;

    public j() {
        this((byte) 0, 0);
    }

    public j(byte b2, int i) {
        this.f4233a = b2;
        this.f4234b = i;
    }
}
