package org.apache.thrift.protocol;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    public final byte f4227a;

    /* renamed from: b  reason: collision with root package name */
    public final int f4228b;

    public d() {
        this((byte) 0, 0);
    }

    public d(byte b2, int i) {
        this.f4227a = b2;
        this.f4228b = i;
    }
}
