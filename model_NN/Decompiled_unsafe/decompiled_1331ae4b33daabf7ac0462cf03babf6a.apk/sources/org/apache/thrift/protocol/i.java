package org.apache.thrift.protocol;

import org.apache.thrift.f;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static int f4232a = Integer.MAX_VALUE;

    public static void a(f fVar, byte b2) {
        a(fVar, b2, f4232a);
    }

    public static void a(f fVar, byte b2, int i) {
        int i2 = 0;
        if (i <= 0) {
            throw new f("Maximum skip depth exceeded");
        }
        switch (b2) {
            case 2:
                fVar.q();
                return;
            case 3:
                fVar.r();
                return;
            case 4:
                fVar.v();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                fVar.s();
                return;
            case 8:
                fVar.t();
                return;
            case 10:
                fVar.u();
                return;
            case 11:
                fVar.x();
                return;
            case 12:
                fVar.g();
                while (true) {
                    c i3 = fVar.i();
                    if (i3.f4226b == 0) {
                        fVar.h();
                        return;
                    } else {
                        a(fVar, i3.f4226b, i - 1);
                        fVar.j();
                    }
                }
            case 13:
                e k = fVar.k();
                while (i2 < k.c) {
                    a(fVar, k.f4229a, i - 1);
                    a(fVar, k.f4230b, i - 1);
                    i2++;
                }
                fVar.l();
                return;
            case 14:
                j o = fVar.o();
                while (i2 < o.f4234b) {
                    a(fVar, o.f4233a, i - 1);
                    i2++;
                }
                fVar.p();
                return;
            case 15:
                d m = fVar.m();
                while (i2 < m.f4228b) {
                    a(fVar, m.f4227a, i - 1);
                    i2++;
                }
                fVar.n();
                return;
        }
    }
}
