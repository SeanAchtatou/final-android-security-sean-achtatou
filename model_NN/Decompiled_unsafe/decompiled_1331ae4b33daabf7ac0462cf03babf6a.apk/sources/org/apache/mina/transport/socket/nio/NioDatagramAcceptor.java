package org.apache.mina.transport.socket.nio;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Set;
import java.util.concurrent.Executor;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.service.TransportMetadata;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.transport.socket.DatagramAcceptor;
import org.apache.mina.transport.socket.DatagramSessionConfig;
import org.apache.mina.transport.socket.DefaultDatagramSessionConfig;

public final class NioDatagramAcceptor extends AbstractPollingConnectionlessIoAcceptor<NioSession, DatagramChannel> implements DatagramAcceptor {
    private volatile Selector selector;

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ AbstractIoSession newSession(IoProcessor ioProcessor, Object obj, SocketAddress socketAddress) throws Exception {
        return newSession((IoProcessor<NioSession>) ioProcessor, (DatagramChannel) obj, socketAddress);
    }

    public NioDatagramAcceptor() {
        super(new DefaultDatagramSessionConfig());
    }

    public NioDatagramAcceptor(Executor executor) {
        super(new DefaultDatagramSessionConfig(), executor);
    }

    /* access modifiers changed from: protected */
    public void init() throws Exception {
        this.selector = Selector.open();
    }

    /* access modifiers changed from: protected */
    public void destroy() throws Exception {
        if (this.selector != null) {
            this.selector.close();
        }
    }

    public TransportMetadata getTransportMetadata() {
        return NioDatagramSession.METADATA;
    }

    public DatagramSessionConfig getSessionConfig() {
        return (DatagramSessionConfig) super.getSessionConfig();
    }

    public InetSocketAddress getLocalAddress() {
        return (InetSocketAddress) super.getLocalAddress();
    }

    public InetSocketAddress getDefaultLocalAddress() {
        return (InetSocketAddress) super.getDefaultLocalAddress();
    }

    public void setDefaultLocalAddress(InetSocketAddress inetSocketAddress) {
        setDefaultLocalAddress((SocketAddress) inetSocketAddress);
    }

    /* access modifiers changed from: protected */
    public DatagramChannel open(SocketAddress socketAddress) throws Exception {
        DatagramChannel open = DatagramChannel.open();
        try {
            new NioDatagramSessionConfig(open).setAll(getSessionConfig());
            open.configureBlocking(false);
            open.socket().bind(socketAddress);
            open.register(this.selector, 1);
            return open;
        } catch (Throwable th) {
            close(open);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isReadable(DatagramChannel datagramChannel) {
        SelectionKey keyFor = datagramChannel.keyFor(this.selector);
        if (keyFor == null || !keyFor.isValid()) {
            return false;
        }
        return keyFor.isReadable();
    }

    /* access modifiers changed from: protected */
    public boolean isWritable(DatagramChannel datagramChannel) {
        SelectionKey keyFor = datagramChannel.keyFor(this.selector);
        if (keyFor == null || !keyFor.isValid()) {
            return false;
        }
        return keyFor.isWritable();
    }

    /* access modifiers changed from: protected */
    public SocketAddress localAddress(DatagramChannel datagramChannel) throws Exception {
        InetSocketAddress inetSocketAddress = (InetSocketAddress) datagramChannel.socket().getLocalSocketAddress();
        InetAddress address = inetSocketAddress.getAddress();
        if (!(address instanceof Inet6Address) || !((Inet6Address) address).isIPv4CompatibleAddress()) {
            return inetSocketAddress;
        }
        byte[] address2 = ((Inet6Address) address).getAddress();
        byte[] bArr = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr[i] = address2[i + 12];
        }
        return new InetSocketAddress(Inet4Address.getByAddress(bArr), inetSocketAddress.getPort());
    }

    /* access modifiers changed from: protected */
    public NioSession newSession(IoProcessor<NioSession> ioProcessor, DatagramChannel datagramChannel, SocketAddress socketAddress) {
        SelectionKey keyFor = datagramChannel.keyFor(this.selector);
        if (keyFor == null || !keyFor.isValid()) {
            return null;
        }
        NioDatagramSession nioDatagramSession = new NioDatagramSession(this, datagramChannel, ioProcessor, socketAddress);
        nioDatagramSession.setSelectionKey(keyFor);
        return nioDatagramSession;
    }

    /* access modifiers changed from: protected */
    public SocketAddress receive(DatagramChannel datagramChannel, IoBuffer ioBuffer) throws Exception {
        return datagramChannel.receive(ioBuffer.buf());
    }

    /* access modifiers changed from: protected */
    public int select() throws Exception {
        return this.selector.select();
    }

    /* access modifiers changed from: protected */
    public int select(long j) throws Exception {
        return this.selector.select(j);
    }

    /* access modifiers changed from: protected */
    public Set<SelectionKey> selectedHandles() {
        return this.selector.selectedKeys();
    }

    /* access modifiers changed from: protected */
    public int send(NioSession nioSession, IoBuffer ioBuffer, SocketAddress socketAddress) throws Exception {
        return ((DatagramChannel) nioSession.getChannel()).send(ioBuffer.buf(), socketAddress);
    }

    /* access modifiers changed from: protected */
    public void setInterestedInWrite(NioSession nioSession, boolean z) throws Exception {
        int i;
        SelectionKey selectionKey = nioSession.getSelectionKey();
        if (selectionKey != null) {
            int interestOps = selectionKey.interestOps();
            if (z) {
                i = interestOps | 4;
            } else {
                i = interestOps & -5;
            }
            selectionKey.interestOps(i);
        }
    }

    /* access modifiers changed from: protected */
    public void close(DatagramChannel datagramChannel) throws Exception {
        SelectionKey keyFor = datagramChannel.keyFor(this.selector);
        if (keyFor != null) {
            keyFor.cancel();
        }
        datagramChannel.disconnect();
        datagramChannel.close();
    }

    /* access modifiers changed from: protected */
    public void wakeup() {
        this.selector.wakeup();
    }
}
