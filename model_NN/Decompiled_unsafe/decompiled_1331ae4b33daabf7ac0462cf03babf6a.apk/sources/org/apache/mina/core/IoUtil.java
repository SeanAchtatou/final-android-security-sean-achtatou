package org.apache.mina.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.future.IoFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.session.IoSession;

public class IoUtil {
    private static final IoSession[] EMPTY_SESSIONS = new IoSession[0];

    public static List<WriteFuture> broadcast(Object obj, Collection<IoSession> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        broadcast(obj, collection.iterator(), arrayList);
        return arrayList;
    }

    public static List<WriteFuture> broadcast(Object obj, Iterable<IoSession> iterable) {
        ArrayList arrayList = new ArrayList();
        broadcast(obj, iterable.iterator(), arrayList);
        return arrayList;
    }

    public static List<WriteFuture> broadcast(Object obj, Iterator<IoSession> it) {
        ArrayList arrayList = new ArrayList();
        broadcast(obj, it, arrayList);
        return arrayList;
    }

    public static List<WriteFuture> broadcast(Object obj, IoSession... ioSessionArr) {
        if (ioSessionArr == null) {
            ioSessionArr = EMPTY_SESSIONS;
        }
        ArrayList arrayList = new ArrayList(ioSessionArr.length);
        if (obj instanceof IoBuffer) {
            for (IoSession write : ioSessionArr) {
                arrayList.add(write.write(((IoBuffer) obj).duplicate()));
            }
        } else {
            for (IoSession write2 : ioSessionArr) {
                arrayList.add(write2.write(obj));
            }
        }
        return arrayList;
    }

    private static void broadcast(Object obj, Iterator<IoSession> it, Collection<WriteFuture> collection) {
        if (obj instanceof IoBuffer) {
            while (it.hasNext()) {
                collection.add(it.next().write(((IoBuffer) obj).duplicate()));
            }
            return;
        }
        while (it.hasNext()) {
            collection.add(it.next().write(obj));
        }
    }

    public static void await(Iterable<? extends IoFuture> iterable) throws InterruptedException {
        for (IoFuture await : iterable) {
            await.await();
        }
    }

    public static void awaitUninterruptably(Iterable<? extends IoFuture> iterable) {
        for (IoFuture awaitUninterruptibly : iterable) {
            awaitUninterruptibly.awaitUninterruptibly();
        }
    }

    public static boolean await(Iterable<? extends IoFuture> iterable, long j, TimeUnit timeUnit) throws InterruptedException {
        return await(iterable, timeUnit.toMillis(j));
    }

    public static boolean await(Iterable<? extends IoFuture> iterable, long j) throws InterruptedException {
        return await0(iterable, j, true);
    }

    public static boolean awaitUninterruptibly(Iterable<? extends IoFuture> iterable, long j, TimeUnit timeUnit) {
        return awaitUninterruptibly(iterable, timeUnit.toMillis(j));
    }

    public static boolean awaitUninterruptibly(Iterable<? extends IoFuture> iterable, long j) {
        try {
            return await0(iterable, j, false);
        } catch (InterruptedException e) {
            throw new InternalError();
        }
    }

    private static boolean await0(Iterable<? extends IoFuture> iterable, long j, boolean z) throws InterruptedException {
        boolean z2;
        long currentTimeMillis = j <= 0 ? 0 : System.currentTimeMillis();
        Iterator<? extends IoFuture> it = iterable.iterator();
        boolean z3 = true;
        long j2 = j;
        while (true) {
            if (!it.hasNext()) {
                z2 = z3;
                break;
            }
            IoFuture ioFuture = (IoFuture) it.next();
            do {
                if (z) {
                    z2 = ioFuture.await(j2);
                } else {
                    z2 = ioFuture.awaitUninterruptibly(j2);
                }
                j2 = j - (System.currentTimeMillis() - currentTimeMillis);
                if (z2 || j2 <= 0) {
                }
            } while (!z2);
            if (j2 <= 0) {
                break;
            }
            z3 = z2;
        }
        if (!z2 || it.hasNext()) {
            return false;
        }
        return true;
    }

    private IoUtil() {
    }
}
