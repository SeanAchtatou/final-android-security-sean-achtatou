package org.apache.mina.core.polling;

import java.net.ConnectException;
import java.net.SocketAddress;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.DefaultConnectFuture;
import org.apache.mina.core.service.AbstractIoConnector;
import org.apache.mina.core.service.AbstractIoService;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.service.SimpleIoProcessorPool;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.core.session.IoSessionInitializer;
import org.apache.mina.util.ExceptionMonitor;

public abstract class AbstractPollingIoConnector<T extends AbstractIoSession, H> extends AbstractIoConnector {
    /* access modifiers changed from: private */
    public final Queue<AbstractPollingIoConnector<T, H>.ConnectionRequest> cancelQueue;
    /* access modifiers changed from: private */
    public final Queue<AbstractPollingIoConnector<T, H>.ConnectionRequest> connectQueue;
    /* access modifiers changed from: private */
    public final AtomicReference<AbstractPollingIoConnector<T, H>.Connector> connectorRef;
    /* access modifiers changed from: private */
    public final boolean createdProcessor;
    /* access modifiers changed from: private */
    public final AbstractIoService.ServiceOperationFuture disposalFuture;
    /* access modifiers changed from: private */
    public final IoProcessor<T> processor;
    /* access modifiers changed from: private */
    public volatile boolean selectable;

    /* access modifiers changed from: protected */
    public abstract Iterator<H> allHandles();

    /* access modifiers changed from: protected */
    public abstract void close(Object obj) throws Exception;

    /* access modifiers changed from: protected */
    public abstract boolean connect(Object obj, SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void destroy() throws Exception;

    /* access modifiers changed from: protected */
    public abstract boolean finishConnect(Object obj) throws Exception;

    /* access modifiers changed from: protected */
    public abstract AbstractPollingIoConnector<T, H>.ConnectionRequest getConnectionRequest(Object obj);

    /* access modifiers changed from: protected */
    public abstract void init() throws Exception;

    /* access modifiers changed from: protected */
    public abstract H newHandle(SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract T newSession(IoProcessor ioProcessor, Object obj) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void register(Object obj, AbstractPollingIoConnector<T, H>.ConnectionRequest connectionRequest) throws Exception;

    /* access modifiers changed from: protected */
    public abstract int select(int i) throws Exception;

    /* access modifiers changed from: protected */
    public abstract Iterator<H> selectedHandles();

    /* access modifiers changed from: protected */
    public abstract void wakeup();

    protected AbstractPollingIoConnector(IoSessionConfig ioSessionConfig, Class cls) {
        this(ioSessionConfig, null, new SimpleIoProcessorPool(cls), true);
    }

    protected AbstractPollingIoConnector(IoSessionConfig ioSessionConfig, Class cls, int i) {
        this(ioSessionConfig, null, new SimpleIoProcessorPool(cls, i), true);
    }

    protected AbstractPollingIoConnector(IoSessionConfig ioSessionConfig, IoProcessor ioProcessor) {
        this(ioSessionConfig, null, ioProcessor, false);
    }

    protected AbstractPollingIoConnector(IoSessionConfig ioSessionConfig, Executor executor, IoProcessor ioProcessor) {
        this(ioSessionConfig, executor, ioProcessor, false);
    }

    private AbstractPollingIoConnector(IoSessionConfig ioSessionConfig, Executor executor, IoProcessor<T> ioProcessor, boolean z) {
        super(ioSessionConfig, executor);
        this.connectQueue = new ConcurrentLinkedQueue();
        this.cancelQueue = new ConcurrentLinkedQueue();
        this.disposalFuture = new AbstractIoService.ServiceOperationFuture();
        this.connectorRef = new AtomicReference<>();
        if (ioProcessor == null) {
            throw new IllegalArgumentException("processor");
        }
        this.processor = ioProcessor;
        this.createdProcessor = z;
        try {
            init();
            this.selectable = true;
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e) {
                    ExceptionMonitor.getInstance().exceptionCaught(e);
                }
            }
        } catch (RuntimeException e2) {
            throw e2;
        } catch (Exception e3) {
            throw new RuntimeIoException("Failed to initialize.", e3);
        } catch (Throwable th) {
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e4) {
                    ExceptionMonitor.getInstance().exceptionCaught(e4);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final void dispose0() throws Exception {
        startupWorker();
        wakeup();
    }

    /* access modifiers changed from: protected */
    public final ConnectFuture connect0(SocketAddress socketAddress, SocketAddress socketAddress2, IoSessionInitializer<? extends ConnectFuture> ioSessionInitializer) {
        try {
            Object newHandle = newHandle(socketAddress2);
            if (connect(newHandle, socketAddress)) {
                DefaultConnectFuture defaultConnectFuture = new DefaultConnectFuture();
                AbstractIoSession newSession = newSession(this.processor, newHandle);
                initSession(newSession, defaultConnectFuture, ioSessionInitializer);
                newSession.getProcessor().add(newSession);
                return defaultConnectFuture;
            }
            ConnectionRequest connectionRequest = new ConnectionRequest(newHandle, ioSessionInitializer);
            this.connectQueue.add(connectionRequest);
            startupWorker();
            wakeup();
            return connectionRequest;
        } catch (Exception e) {
            ConnectFuture newFailedFuture = DefaultConnectFuture.newFailedFuture(e);
            if (0 == 0) {
                return newFailedFuture;
            }
            try {
                close(null);
                return newFailedFuture;
            } catch (Exception e2) {
                ExceptionMonitor.getInstance().exceptionCaught(e2);
                return newFailedFuture;
            }
        } catch (Throwable th) {
            if (0 != 0) {
                try {
                    close(null);
                } catch (Exception e3) {
                    ExceptionMonitor.getInstance().exceptionCaught(e3);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: private */
    public void startupWorker() {
        if (!this.selectable) {
            this.connectQueue.clear();
            this.cancelQueue.clear();
        }
        if (this.connectorRef.get() == null) {
            Connector connector = new Connector();
            if (this.connectorRef.compareAndSet(null, connector)) {
                executeWorker(connector);
            }
        }
    }

    /* access modifiers changed from: private */
    public int registerNew() {
        int i = 0;
        while (true) {
            int i2 = i;
            ConnectionRequest poll = this.connectQueue.poll();
            if (poll == null) {
                return i2;
            }
            Object access$100 = poll.handle;
            try {
                register(access$100, poll);
                i2++;
            } catch (Exception e) {
                poll.setException(e);
                try {
                    close(access$100);
                } catch (Exception e2) {
                    ExceptionMonitor.getInstance().exceptionCaught(e2);
                }
            }
            i = i2;
        }
    }

    /* access modifiers changed from: private */
    public int cancelKeys() {
        int i;
        int i2 = 0;
        while (true) {
            i = i2;
            ConnectionRequest poll = this.cancelQueue.poll();
            if (poll == null) {
                break;
            }
            try {
                close(poll.handle);
            } catch (Exception e) {
                ExceptionMonitor.getInstance().exceptionCaught(e);
            } catch (Throwable th) {
                int i3 = i + 1;
                throw th;
            }
            i2 = i + 1;
        }
        if (i > 0) {
            wakeup();
        }
        return i;
    }

    /* access modifiers changed from: private */
    public int processConnections(Iterator<H> it) {
        int i = 0;
        while (it.hasNext()) {
            H next = it.next();
            it.remove();
            AbstractPollingIoConnector<T, H>.ConnectionRequest connectionRequest = getConnectionRequest(next);
            if (connectionRequest != null) {
                try {
                    if (finishConnect(next)) {
                        AbstractIoSession newSession = newSession(this.processor, next);
                        initSession(newSession, connectionRequest, connectionRequest.getSessionInitializer());
                        newSession.getProcessor().add(newSession);
                        i++;
                    }
                } catch (Throwable th) {
                    this.cancelQueue.offer(connectionRequest);
                    throw th;
                }
            }
        }
        return i;
    }

    /* access modifiers changed from: private */
    public void processTimedOutSessions(Iterator<H> it) {
        long currentTimeMillis = System.currentTimeMillis();
        while (it.hasNext()) {
            AbstractPollingIoConnector<T, H>.ConnectionRequest connectionRequest = getConnectionRequest(it.next());
            if (connectionRequest != null && currentTimeMillis >= connectionRequest.deadline) {
                connectionRequest.setException(new ConnectException("Connection timed out."));
                this.cancelQueue.offer(connectionRequest);
            }
        }
    }

    private class Connector implements Runnable {
        static final /* synthetic */ boolean $assertionsDisabled = (!AbstractPollingIoConnector.class.desiredAssertionStatus());

        private Connector() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(long, long):long}
         arg types: [long, int]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(float, float):float}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(long, long):long} */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x0188, code lost:
            r2 = th;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x0068 A[ExcHandler: ClosedSelectorException (e java.nio.channels.ClosedSelectorException), Splitter:B:9:0x0020] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r7 = this;
                r1 = 0
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.Connector.$assertionsDisabled
                if (r0 != 0) goto L_0x0017
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef
                java.lang.Object r0 = r0.get()
                if (r0 == r7) goto L_0x0017
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x0017:
                r0 = r1
            L_0x0018:
                org.apache.mina.core.polling.AbstractPollingIoConnector r2 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                boolean r2 = r2.selectable
                if (r2 == 0) goto L_0x0069
                org.apache.mina.core.polling.AbstractPollingIoConnector r2 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                long r2 = r2.getConnectTimeoutMillis()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                r4 = 1000(0x3e8, double:4.94E-321)
                long r2 = java.lang.Math.min(r2, r4)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                int r2 = (int) r2     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r3 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                int r3 = r3.select(r2)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r2 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                int r2 = r2.registerNew()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x0188 }
                int r2 = r2 + r0
                if (r2 != 0) goto L_0x0103
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r4 = 0
                r0.set(r4)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.Queue r0 = r0.connectQueue     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                boolean r0 = r0.isEmpty()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 == 0) goto L_0x00ae
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.Connector.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != 0) goto L_0x0069
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != r7) goto L_0x0069
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
            L_0x0068:
                r0 = move-exception
            L_0x0069:
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                boolean r0 = r0.selectable
                if (r0 == 0) goto L_0x00ad
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                boolean r0 = r0.isDisposing()
                if (r0 == 0) goto L_0x00ad
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                boolean unused = r0.selectable = r1
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0148 }
                boolean r0 = r0.createdProcessor     // Catch:{ all -> 0x0148 }
                if (r0 == 0) goto L_0x008f
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0148 }
                org.apache.mina.core.service.IoProcessor r0 = r0.processor     // Catch:{ all -> 0x0148 }
                r0.dispose()     // Catch:{ all -> 0x0148 }
            L_0x008f:
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ Exception -> 0x012a }
                java.lang.Object r1 = r0.disposalLock     // Catch:{ Exception -> 0x012a }
                monitor-enter(r1)     // Catch:{ Exception -> 0x012a }
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0127 }
                boolean r0 = r0.isDisposing()     // Catch:{ all -> 0x0127 }
                if (r0 == 0) goto L_0x00a3
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0127 }
                r0.destroy()     // Catch:{ all -> 0x0127 }
            L_0x00a3:
                monitor-exit(r1)     // Catch:{ all -> 0x0127 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                r0.setDone()
            L_0x00ad:
                return
            L_0x00ae:
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r4 = 0
                boolean r0 = r0.compareAndSet(r4, r7)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != 0) goto L_0x00ed
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.Connector.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != 0) goto L_0x0069
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != r7) goto L_0x0069
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
            L_0x00d1:
                r0 = move-exception
                r6 = r0
                r0 = r2
                r2 = r6
            L_0x00d5:
                org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
                r3.exceptionCaught(r2)
                r2 = 1000(0x3e8, double:4.94E-321)
                java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x00e3 }
                goto L_0x0018
            L_0x00e3:
                r2 = move-exception
                org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
                r3.exceptionCaught(r2)
                goto L_0x0018
            L_0x00ed:
                boolean r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.Connector.$assertionsDisabled     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 != 0) goto L_0x0103
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.concurrent.atomic.AtomicReference r0 = r0.connectorRef     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.lang.Object r0 = r0.get()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                if (r0 == r7) goto L_0x0103
                java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r0.<init>()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
            L_0x0103:
                if (r3 <= 0) goto L_0x0112
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r3 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.Iterator r3 = r3.selectedHandles()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                int r0 = r0.processConnections(r3)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                int r2 = r2 - r0
            L_0x0112:
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r3 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                java.util.Iterator r3 = r3.allHandles()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                r0.processTimedOutSessions(r3)     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                int r0 = r0.cancelKeys()     // Catch:{ ClosedSelectorException -> 0x0068, Throwable -> 0x00d1 }
                int r0 = r2 - r0
                goto L_0x0018
            L_0x0127:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0127 }
                throw r0     // Catch:{ Exception -> 0x012a }
            L_0x012a:
                r0 = move-exception
                org.apache.mina.util.ExceptionMonitor r1 = org.apache.mina.util.ExceptionMonitor.getInstance()     // Catch:{ all -> 0x013d }
                r1.exceptionCaught(r0)     // Catch:{ all -> 0x013d }
                org.apache.mina.core.polling.AbstractPollingIoConnector r0 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                r0.setDone()
                goto L_0x00ad
            L_0x013d:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                throw r0
            L_0x0148:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ Exception -> 0x016b }
                java.lang.Object r2 = r1.disposalLock     // Catch:{ Exception -> 0x016b }
                monitor-enter(r2)     // Catch:{ Exception -> 0x016b }
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0168 }
                boolean r1 = r1.isDisposing()     // Catch:{ all -> 0x0168 }
                if (r1 == 0) goto L_0x015d
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this     // Catch:{ all -> 0x0168 }
                r1.destroy()     // Catch:{ all -> 0x0168 }
            L_0x015d:
                monitor-exit(r2)     // Catch:{ all -> 0x0168 }
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
            L_0x0167:
                throw r0
            L_0x0168:
                r1 = move-exception
                monitor-exit(r2)     // Catch:{ all -> 0x0168 }
                throw r1     // Catch:{ Exception -> 0x016b }
            L_0x016b:
                r1 = move-exception
                org.apache.mina.util.ExceptionMonitor r2 = org.apache.mina.util.ExceptionMonitor.getInstance()     // Catch:{ all -> 0x017d }
                r2.exceptionCaught(r1)     // Catch:{ all -> 0x017d }
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                goto L_0x0167
            L_0x017d:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingIoConnector r1 = org.apache.mina.core.polling.AbstractPollingIoConnector.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                r1.setDone()
                throw r0
            L_0x0188:
                r2 = move-exception
                goto L_0x00d5
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoConnector.Connector.run():void");
        }
    }

    public final class ConnectionRequest extends DefaultConnectFuture {
        /* access modifiers changed from: private */
        public final long deadline;
        /* access modifiers changed from: private */
        public final H handle;
        private final IoSessionInitializer<? extends ConnectFuture> sessionInitializer;

        public ConnectionRequest(H h, IoSessionInitializer<? extends ConnectFuture> ioSessionInitializer) {
            this.handle = h;
            long connectTimeoutMillis = AbstractPollingIoConnector.this.getConnectTimeoutMillis();
            if (connectTimeoutMillis <= 0) {
                this.deadline = Long.MAX_VALUE;
            } else {
                this.deadline = connectTimeoutMillis + System.currentTimeMillis();
            }
            this.sessionInitializer = ioSessionInitializer;
        }

        public H getHandle() {
            return this.handle;
        }

        public long getDeadline() {
            return this.deadline;
        }

        public IoSessionInitializer<? extends ConnectFuture> getSessionInitializer() {
            return this.sessionInitializer;
        }

        public void cancel() {
            if (!isDone()) {
                super.cancel();
                AbstractPollingIoConnector.this.cancelQueue.add(this);
                AbstractPollingIoConnector.this.startupWorker();
                AbstractPollingIoConnector.this.wakeup();
            }
        }
    }
}
