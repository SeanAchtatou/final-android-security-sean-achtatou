package org.apache.mina.core.service;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class IoServiceStatistics {
    private double largestReadBytesThroughput;
    private double largestReadMessagesThroughput;
    private double largestWrittenBytesThroughput;
    private double largestWrittenMessagesThroughput;
    private long lastReadBytes;
    private long lastReadMessages;
    private long lastReadTime;
    private long lastThroughputCalculationTime;
    private long lastWriteTime;
    private long lastWrittenBytes;
    private long lastWrittenMessages;
    private final AtomicLong readBytes = new AtomicLong();
    private double readBytesThroughput;
    private final AtomicLong readMessages = new AtomicLong();
    private double readMessagesThroughput;
    private final AtomicInteger scheduledWriteBytes = new AtomicInteger();
    private final AtomicInteger scheduledWriteMessages = new AtomicInteger();
    private AbstractIoService service;
    private int throughputCalculationInterval = 3;
    private final Object throughputCalculationLock = new Object();
    private final AtomicLong writtenBytes = new AtomicLong();
    private double writtenBytesThroughput;
    private final AtomicLong writtenMessages = new AtomicLong();
    private double writtenMessagesThroughput;

    public IoServiceStatistics(AbstractIoService abstractIoService) {
        this.service = abstractIoService;
    }

    public final int getLargestManagedSessionCount() {
        return this.service.getListeners().getLargestManagedSessionCount();
    }

    public final long getCumulativeManagedSessionCount() {
        return this.service.getListeners().getCumulativeManagedSessionCount();
    }

    public final long getLastIoTime() {
        return Math.max(this.lastReadTime, this.lastWriteTime);
    }

    public final long getLastReadTime() {
        return this.lastReadTime;
    }

    public final long getLastWriteTime() {
        return this.lastWriteTime;
    }

    public final long getReadBytes() {
        return this.readBytes.get();
    }

    public final long getWrittenBytes() {
        return this.writtenBytes.get();
    }

    public final long getReadMessages() {
        return this.readMessages.get();
    }

    public final long getWrittenMessages() {
        return this.writtenMessages.get();
    }

    public final double getReadBytesThroughput() {
        resetThroughput();
        return this.readBytesThroughput;
    }

    public final double getWrittenBytesThroughput() {
        resetThroughput();
        return this.writtenBytesThroughput;
    }

    public final double getReadMessagesThroughput() {
        resetThroughput();
        return this.readMessagesThroughput;
    }

    public final double getWrittenMessagesThroughput() {
        resetThroughput();
        return this.writtenMessagesThroughput;
    }

    public final double getLargestReadBytesThroughput() {
        return this.largestReadBytesThroughput;
    }

    public final double getLargestWrittenBytesThroughput() {
        return this.largestWrittenBytesThroughput;
    }

    public final double getLargestReadMessagesThroughput() {
        return this.largestReadMessagesThroughput;
    }

    public final double getLargestWrittenMessagesThroughput() {
        return this.largestWrittenMessagesThroughput;
    }

    public final int getThroughputCalculationInterval() {
        return this.throughputCalculationInterval;
    }

    public final long getThroughputCalculationIntervalInMillis() {
        return ((long) this.throughputCalculationInterval) * 1000;
    }

    public final void setThroughputCalculationInterval(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("throughputCalculationInterval: " + i);
        }
        this.throughputCalculationInterval = i;
    }

    /* access modifiers changed from: protected */
    public final void setLastReadTime(long j) {
        this.lastReadTime = j;
    }

    /* access modifiers changed from: protected */
    public final void setLastWriteTime(long j) {
        this.lastWriteTime = j;
    }

    private void resetThroughput() {
        if (this.service.getManagedSessionCount() == 0) {
            this.readBytesThroughput = 0.0d;
            this.writtenBytesThroughput = 0.0d;
            this.readMessagesThroughput = 0.0d;
            this.writtenMessagesThroughput = 0.0d;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void updateThroughput(long r20) {
        /*
            r19 = this;
            r0 = r19
            java.lang.Object r5 = r0.throughputCalculationLock
            monitor-enter(r5)
            r0 = r19
            long r6 = r0.lastThroughputCalculationTime     // Catch:{ all -> 0x010a }
            long r6 = r20 - r6
            int r4 = (int) r6     // Catch:{ all -> 0x010a }
            long r6 = r19.getThroughputCalculationIntervalInMillis()     // Catch:{ all -> 0x010a }
            r8 = 0
            int r8 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r8 == 0) goto L_0x001b
            long r8 = (long) r4     // Catch:{ all -> 0x010a }
            int r6 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
            if (r6 >= 0) goto L_0x001d
        L_0x001b:
            monitor-exit(r5)     // Catch:{ all -> 0x010a }
        L_0x001c:
            return
        L_0x001d:
            r0 = r19
            java.util.concurrent.atomic.AtomicLong r6 = r0.readBytes     // Catch:{ all -> 0x010a }
            long r6 = r6.get()     // Catch:{ all -> 0x010a }
            r0 = r19
            java.util.concurrent.atomic.AtomicLong r8 = r0.writtenBytes     // Catch:{ all -> 0x010a }
            long r8 = r8.get()     // Catch:{ all -> 0x010a }
            r0 = r19
            java.util.concurrent.atomic.AtomicLong r10 = r0.readMessages     // Catch:{ all -> 0x010a }
            long r10 = r10.get()     // Catch:{ all -> 0x010a }
            r0 = r19
            java.util.concurrent.atomic.AtomicLong r12 = r0.writtenMessages     // Catch:{ all -> 0x010a }
            long r12 = r12.get()     // Catch:{ all -> 0x010a }
            r0 = r19
            long r14 = r0.lastReadBytes     // Catch:{ all -> 0x010a }
            long r14 = r6 - r14
            double r14 = (double) r14     // Catch:{ all -> 0x010a }
            r16 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r14 = r14 * r16
            double r0 = (double) r4     // Catch:{ all -> 0x010a }
            r16 = r0
            double r14 = r14 / r16
            r0 = r19
            r0.readBytesThroughput = r14     // Catch:{ all -> 0x010a }
            r0 = r19
            long r14 = r0.lastWrittenBytes     // Catch:{ all -> 0x010a }
            long r14 = r8 - r14
            double r14 = (double) r14     // Catch:{ all -> 0x010a }
            r16 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r14 = r14 * r16
            double r0 = (double) r4     // Catch:{ all -> 0x010a }
            r16 = r0
            double r14 = r14 / r16
            r0 = r19
            r0.writtenBytesThroughput = r14     // Catch:{ all -> 0x010a }
            r0 = r19
            long r14 = r0.lastReadMessages     // Catch:{ all -> 0x010a }
            long r14 = r10 - r14
            double r14 = (double) r14     // Catch:{ all -> 0x010a }
            r16 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r14 = r14 * r16
            double r0 = (double) r4     // Catch:{ all -> 0x010a }
            r16 = r0
            double r14 = r14 / r16
            r0 = r19
            r0.readMessagesThroughput = r14     // Catch:{ all -> 0x010a }
            r0 = r19
            long r14 = r0.lastWrittenMessages     // Catch:{ all -> 0x010a }
            long r14 = r12 - r14
            double r14 = (double) r14     // Catch:{ all -> 0x010a }
            r16 = 4652007308841189376(0x408f400000000000, double:1000.0)
            double r14 = r14 * r16
            double r0 = (double) r4     // Catch:{ all -> 0x010a }
            r16 = r0
            double r14 = r14 / r16
            r0 = r19
            r0.writtenMessagesThroughput = r14     // Catch:{ all -> 0x010a }
            r0 = r19
            double r14 = r0.readBytesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            double r0 = r0.largestReadBytesThroughput     // Catch:{ all -> 0x010a }
            r16 = r0
            int r4 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00af
            r0 = r19
            double r14 = r0.readBytesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.largestReadBytesThroughput = r14     // Catch:{ all -> 0x010a }
        L_0x00af:
            r0 = r19
            double r14 = r0.writtenBytesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            double r0 = r0.largestWrittenBytesThroughput     // Catch:{ all -> 0x010a }
            r16 = r0
            int r4 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00c5
            r0 = r19
            double r14 = r0.writtenBytesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.largestWrittenBytesThroughput = r14     // Catch:{ all -> 0x010a }
        L_0x00c5:
            r0 = r19
            double r14 = r0.readMessagesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            double r0 = r0.largestReadMessagesThroughput     // Catch:{ all -> 0x010a }
            r16 = r0
            int r4 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00db
            r0 = r19
            double r14 = r0.readMessagesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.largestReadMessagesThroughput = r14     // Catch:{ all -> 0x010a }
        L_0x00db:
            r0 = r19
            double r14 = r0.writtenMessagesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            double r0 = r0.largestWrittenMessagesThroughput     // Catch:{ all -> 0x010a }
            r16 = r0
            int r4 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r4 <= 0) goto L_0x00f1
            r0 = r19
            double r14 = r0.writtenMessagesThroughput     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.largestWrittenMessagesThroughput = r14     // Catch:{ all -> 0x010a }
        L_0x00f1:
            r0 = r19
            r0.lastReadBytes = r6     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.lastWrittenBytes = r8     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.lastReadMessages = r10     // Catch:{ all -> 0x010a }
            r0 = r19
            r0.lastWrittenMessages = r12     // Catch:{ all -> 0x010a }
            r0 = r20
            r2 = r19
            r2.lastThroughputCalculationTime = r0     // Catch:{ all -> 0x010a }
            monitor-exit(r5)     // Catch:{ all -> 0x010a }
            goto L_0x001c
        L_0x010a:
            r4 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x010a }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.service.IoServiceStatistics.updateThroughput(long):void");
    }

    public final void increaseReadBytes(long j, long j2) {
        this.readBytes.addAndGet(j);
        this.lastReadTime = j2;
    }

    public final void increaseReadMessages(long j) {
        this.readMessages.incrementAndGet();
        this.lastReadTime = j;
    }

    public final void increaseWrittenBytes(int i, long j) {
        this.writtenBytes.addAndGet((long) i);
        this.lastWriteTime = j;
    }

    public final void increaseWrittenMessages(long j) {
        this.writtenMessages.incrementAndGet();
        this.lastWriteTime = j;
    }

    public final int getScheduledWriteBytes() {
        return this.scheduledWriteBytes.get();
    }

    public final void increaseScheduledWriteBytes(int i) {
        this.scheduledWriteBytes.addAndGet(i);
    }

    public final int getScheduledWriteMessages() {
        return this.scheduledWriteMessages.get();
    }

    public final void increaseScheduledWriteMessages() {
        this.scheduledWriteMessages.incrementAndGet();
    }

    public final void decreaseScheduledWriteMessages() {
        this.scheduledWriteMessages.decrementAndGet();
    }

    /* access modifiers changed from: protected */
    public void setLastThroughputCalculationTime(long j) {
        this.lastThroughputCalculationTime = j;
    }
}
