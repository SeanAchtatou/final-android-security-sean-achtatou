package org.apache.mina.core.future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.apache.mina.core.polling.AbstractPollingIoProcessor;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.util.ExceptionMonitor;

public class DefaultIoFuture implements IoFuture {
    private static final long DEAD_LOCK_CHECK_INTERVAL = 5000;
    private IoFutureListener<?> firstListener;
    private final Object lock = this;
    private List<IoFutureListener<?>> otherListeners;
    private boolean ready;
    private Object result;
    private final IoSession session;
    private int waiters;

    public DefaultIoFuture(IoSession ioSession) {
        this.session = ioSession;
    }

    public IoSession getSession() {
        return this.session;
    }

    @Deprecated
    public void join() {
        awaitUninterruptibly();
    }

    @Deprecated
    public boolean join(long j) {
        return awaitUninterruptibly(j);
    }

    /* JADX INFO: finally extract failed */
    public IoFuture await() throws InterruptedException {
        synchronized (this.lock) {
            while (!this.ready) {
                this.waiters++;
                try {
                    this.lock.wait(DEAD_LOCK_CHECK_INTERVAL);
                    this.waiters--;
                    if (!this.ready) {
                        checkDeadLock();
                    }
                } catch (Throwable th) {
                    this.waiters--;
                    if (!this.ready) {
                        checkDeadLock();
                    }
                    throw th;
                }
            }
        }
        return this;
    }

    public boolean await(long j, TimeUnit timeUnit) throws InterruptedException {
        return await(timeUnit.toMillis(j));
    }

    public boolean await(long j) throws InterruptedException {
        return await0(j, true);
    }

    public IoFuture awaitUninterruptibly() {
        try {
            await0(Long.MAX_VALUE, false);
        } catch (InterruptedException e) {
        }
        return this;
    }

    public boolean awaitUninterruptibly(long j, TimeUnit timeUnit) {
        return awaitUninterruptibly(timeUnit.toMillis(j));
    }

    public boolean awaitUninterruptibly(long j) {
        try {
            return await0(j, false);
        } catch (InterruptedException e) {
            throw new InternalError();
        }
    }

    private boolean await0(long j, boolean z) throws InterruptedException {
        boolean z2;
        long currentTimeMillis = System.currentTimeMillis() + j;
        if (currentTimeMillis < 0) {
            currentTimeMillis = Long.MAX_VALUE;
        }
        synchronized (this.lock) {
            if (this.ready) {
                z2 = this.ready;
            } else if (j <= 0) {
                z2 = this.ready;
            } else {
                this.waiters++;
                do {
                    try {
                        this.lock.wait(Math.min(j, (long) DEAD_LOCK_CHECK_INTERVAL));
                    } catch (InterruptedException e) {
                        if (z) {
                            throw e;
                        }
                    } catch (Throwable th) {
                        this.waiters--;
                        if (!this.ready) {
                            checkDeadLock();
                        }
                        throw th;
                    }
                    if (this.ready) {
                        z2 = true;
                        this.waiters--;
                        if (!this.ready) {
                            checkDeadLock();
                        }
                    }
                } while (currentTimeMillis >= System.currentTimeMillis());
                z2 = this.ready;
                this.waiters--;
                if (!this.ready) {
                    checkDeadLock();
                }
            }
        }
        return z2;
    }

    private void checkDeadLock() {
        int i = 0;
        if ((this instanceof CloseFuture) || (this instanceof WriteFuture) || (this instanceof ReadFuture) || (this instanceof ConnectFuture)) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            for (StackTraceElement className : stackTrace) {
                if (AbstractPollingIoProcessor.class.getName().equals(className.getClassName())) {
                    new IllegalStateException("t").getStackTrace();
                    throw new IllegalStateException("DEAD LOCK: " + IoFuture.class.getSimpleName() + ".await() was invoked from an I/O processor thread.  " + "Please use " + IoFutureListener.class.getSimpleName() + " or configure a proper thread model alternatively.");
                }
            }
            int length = stackTrace.length;
            while (i < length) {
                try {
                    if (IoProcessor.class.isAssignableFrom(DefaultIoFuture.class.getClassLoader().loadClass(stackTrace[i].getClassName()))) {
                        throw new IllegalStateException("DEAD LOCK: " + IoFuture.class.getSimpleName() + ".await() was invoked from an I/O processor thread.  " + "Please use " + IoFutureListener.class.getSimpleName() + " or configure a proper thread model alternatively.");
                    }
                    i++;
                } catch (Exception e) {
                }
            }
        }
    }

    public boolean isDone() {
        boolean z;
        synchronized (this.lock) {
            z = this.ready;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0018, code lost:
        notifyListeners();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setValue(java.lang.Object r3) {
        /*
            r2 = this;
            java.lang.Object r1 = r2.lock
            monitor-enter(r1)
            boolean r0 = r2.ready     // Catch:{ all -> 0x001c }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
        L_0x0008:
            return
        L_0x0009:
            r2.result = r3     // Catch:{ all -> 0x001c }
            r0 = 1
            r2.ready = r0     // Catch:{ all -> 0x001c }
            int r0 = r2.waiters     // Catch:{ all -> 0x001c }
            if (r0 <= 0) goto L_0x0017
            java.lang.Object r0 = r2.lock     // Catch:{ all -> 0x001c }
            r0.notifyAll()     // Catch:{ all -> 0x001c }
        L_0x0017:
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            r2.notifyListeners()
            goto L_0x0008
        L_0x001c:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.future.DefaultIoFuture.setValue(java.lang.Object):void");
    }

    /* access modifiers changed from: protected */
    public Object getValue() {
        Object obj;
        synchronized (this.lock) {
            obj = this.result;
        }
        return obj;
    }

    public IoFuture addListener(IoFutureListener<?> ioFutureListener) {
        boolean z = true;
        if (ioFutureListener == null) {
            throw new IllegalArgumentException("listener");
        }
        synchronized (this.lock) {
            if (!this.ready) {
                if (this.firstListener == null) {
                    this.firstListener = ioFutureListener;
                    z = false;
                } else {
                    if (this.otherListeners == null) {
                        this.otherListeners = new ArrayList(1);
                    }
                    this.otherListeners.add(ioFutureListener);
                    z = false;
                }
            }
        }
        if (z) {
            notifyListener(ioFutureListener);
        }
        return this;
    }

    public IoFuture removeListener(IoFutureListener<?> ioFutureListener) {
        if (ioFutureListener == null) {
            throw new IllegalArgumentException("listener");
        }
        synchronized (this.lock) {
            if (!this.ready) {
                if (ioFutureListener == this.firstListener) {
                    if (this.otherListeners == null || this.otherListeners.isEmpty()) {
                        this.firstListener = null;
                    } else {
                        this.firstListener = this.otherListeners.remove(0);
                    }
                } else if (this.otherListeners != null) {
                    this.otherListeners.remove(ioFutureListener);
                }
            }
        }
        return this;
    }

    private void notifyListeners() {
        if (this.firstListener != null) {
            notifyListener(this.firstListener);
            this.firstListener = null;
            if (this.otherListeners != null) {
                for (IoFutureListener<?> notifyListener : this.otherListeners) {
                    notifyListener(notifyListener);
                }
                this.otherListeners = null;
            }
        }
    }

    private void notifyListener(IoFutureListener ioFutureListener) {
        try {
            ioFutureListener.operationComplete(this);
        } catch (Throwable th) {
            ExceptionMonitor.getInstance().exceptionCaught(th);
        }
    }
}
