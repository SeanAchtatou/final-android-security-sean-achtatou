package org.apache.mina.core.polling;

import java.io.IOException;
import java.net.PortUnreachableException;
import java.nio.channels.ClosedSelectorException;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.file.FileRegion;
import org.apache.mina.core.future.DefaultIoFuture;
import org.apache.mina.core.service.AbstractIoService;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.core.session.SessionState;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;
import org.apache.mina.transport.socket.AbstractDatagramSessionConfig;
import org.apache.mina.util.ExceptionMonitor;
import org.apache.mina.util.NamePreservingRunnable;

public abstract class AbstractPollingIoProcessor<S extends AbstractIoSession> implements IoProcessor<S> {
    /* access modifiers changed from: private */
    public static final b LOG = c.a(IoProcessor.class);
    private static final long SELECT_TIMEOUT = 1000;
    private static final int WRITE_SPIN_COUNT = 256;
    private static final ConcurrentHashMap<Class<?>, AtomicInteger> threadIds = new ConcurrentHashMap<>();
    /* access modifiers changed from: private */
    public final DefaultIoFuture disposalFuture = new DefaultIoFuture(null);
    /* access modifiers changed from: private */
    public final Object disposalLock = new Object();
    private volatile boolean disposed;
    /* access modifiers changed from: private */
    public volatile boolean disposing;
    private final Executor executor;
    private final Queue<S> flushingSessions = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */
    public long lastIdleCheckTime;
    /* access modifiers changed from: private */
    public final Queue<S> newSessions = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */
    public final AtomicReference<AbstractPollingIoProcessor<S>.Processor> processorRef = new AtomicReference<>();
    private final Queue<S> removingSessions = new ConcurrentLinkedQueue();
    private final String threadName;
    private final Queue<S> trafficControllingSessions = new ConcurrentLinkedQueue();
    protected AtomicBoolean wakeupCalled = new AtomicBoolean(false);

    /* access modifiers changed from: protected */
    public abstract Iterator<S> allSessions();

    /* access modifiers changed from: protected */
    public abstract void destroy(S s) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void doDispose() throws Exception;

    /* access modifiers changed from: protected */
    public abstract SessionState getState(S s);

    /* access modifiers changed from: protected */
    public abstract void init(S s) throws Exception;

    /* access modifiers changed from: protected */
    public abstract boolean isBrokenConnection() throws IOException;

    /* access modifiers changed from: protected */
    public abstract boolean isInterestedInRead(S s);

    /* access modifiers changed from: protected */
    public abstract boolean isInterestedInWrite(S s);

    /* access modifiers changed from: protected */
    public abstract boolean isReadable(S s);

    /* access modifiers changed from: protected */
    public abstract boolean isSelectorEmpty();

    /* access modifiers changed from: protected */
    public abstract boolean isWritable(S s);

    /* access modifiers changed from: protected */
    public abstract int read(S s, IoBuffer ioBuffer) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void registerNewSelector() throws IOException;

    /* access modifiers changed from: protected */
    public abstract int select() throws Exception;

    /* access modifiers changed from: protected */
    public abstract int select(long j) throws Exception;

    /* access modifiers changed from: protected */
    public abstract Iterator<S> selectedSessions();

    /* access modifiers changed from: protected */
    public abstract void setInterestedInRead(S s, boolean z) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void setInterestedInWrite(S s, boolean z) throws Exception;

    /* access modifiers changed from: protected */
    public abstract int transferFile(S s, FileRegion fileRegion, int i) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void wakeup();

    /* access modifiers changed from: protected */
    public abstract int write(S s, IoBuffer ioBuffer, int i) throws Exception;

    protected AbstractPollingIoProcessor(Executor executor2) {
        if (executor2 == null) {
            throw new IllegalArgumentException("executor");
        }
        this.threadName = nextThreadName();
        this.executor = executor2;
    }

    private String nextThreadName() {
        int incrementAndGet;
        Class<?> cls = getClass();
        AtomicInteger putIfAbsent = threadIds.putIfAbsent(cls, new AtomicInteger(1));
        if (putIfAbsent == null) {
            incrementAndGet = 1;
        } else {
            incrementAndGet = putIfAbsent.incrementAndGet();
        }
        return cls.getSimpleName() + '-' + incrementAndGet;
    }

    public final boolean isDisposing() {
        return this.disposing;
    }

    public final boolean isDisposed() {
        return this.disposed;
    }

    public final void dispose() {
        if (!this.disposed && !this.disposing) {
            synchronized (this.disposalLock) {
                this.disposing = true;
                startupProcessor();
            }
            this.disposalFuture.awaitUninterruptibly();
            this.disposed = true;
        }
    }

    public final void add(S s) {
        if (this.disposed || this.disposing) {
            throw new IllegalStateException("Already disposed.");
        }
        this.newSessions.add(s);
        startupProcessor();
    }

    public final void remove(S s) {
        scheduleRemove(s);
        startupProcessor();
    }

    /* access modifiers changed from: private */
    public void scheduleRemove(S s) {
        this.removingSessions.add(s);
    }

    public void write(S s, WriteRequest writeRequest) {
        s.getWriteRequestQueue().offer(s, writeRequest);
        if (!s.isWriteSuspended()) {
            flush((AbstractIoSession) s);
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void flush(S r2) {
        /*
            r1 = this;
            r0 = 1
            boolean r0 = r2.setScheduledForFlush(r0)
            if (r0 == 0) goto L_0x000f
            java.util.Queue<S> r0 = r1.flushingSessions
            r0.add(r2)
            r1.wakeup()
        L_0x000f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.flush(org.apache.mina.core.session.AbstractIoSession):void");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void scheduleFlush(S r2) {
        /*
            r1 = this;
            r0 = 1
            boolean r0 = r2.setScheduledForFlush(r0)
            if (r0 == 0) goto L_0x000c
            java.util.Queue<S> r0 = r1.flushingSessions
            r0.add(r2)
        L_0x000c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.scheduleFlush(org.apache.mina.core.session.AbstractIoSession):void");
    }

    public final void updateTrafficMask(S s) {
        this.trafficControllingSessions.add(s);
        wakeup();
    }

    private void startupProcessor() {
        if (this.processorRef.get() == null) {
            Processor processor = new Processor();
            if (this.processorRef.compareAndSet(null, processor)) {
                this.executor.execute(new NamePreservingRunnable(processor, this.threadName));
            }
        }
        wakeup();
    }

    /* access modifiers changed from: private */
    public int handleNewSessions() {
        int i = 0;
        AbstractIoSession abstractIoSession = (AbstractIoSession) this.newSessions.poll();
        while (abstractIoSession != null) {
            if (addNow(abstractIoSession)) {
                i++;
            }
            i = i;
            abstractIoSession = (AbstractIoSession) this.newSessions.poll();
        }
        return i;
    }

    private boolean addNow(S s) {
        try {
            init(s);
            s.getService().getFilterChainBuilder().buildFilterChain(s.getFilterChain());
            ((AbstractIoService) s.getService()).getListeners().fireSessionCreated(s);
            return true;
        } catch (Throwable th) {
            ExceptionMonitor.getInstance().exceptionCaught(th);
            try {
                destroy(s);
                return false;
            } catch (Exception e) {
                ExceptionMonitor.getInstance().exceptionCaught(e);
                return false;
            }
        }
    }

    /* access modifiers changed from: private */
    public int removeSessions() {
        int i = 0;
        AbstractIoSession abstractIoSession = (AbstractIoSession) this.removingSessions.poll();
        while (abstractIoSession != null) {
            SessionState state = getState(abstractIoSession);
            switch (state) {
                case OPENED:
                    if (!removeNow(abstractIoSession)) {
                        break;
                    } else {
                        i++;
                        break;
                    }
                case CLOSING:
                    break;
                case OPENING:
                    this.newSessions.remove(abstractIoSession);
                    if (!removeNow(abstractIoSession)) {
                        break;
                    } else {
                        i++;
                        break;
                    }
                default:
                    throw new IllegalStateException(String.valueOf(state));
            }
            i = i;
            abstractIoSession = (AbstractIoSession) this.removingSessions.poll();
        }
        return i;
    }

    private boolean removeNow(S s) {
        clearWriteRequestQueue(s);
        try {
            destroy(s);
            clearWriteRequestQueue(s);
            ((AbstractIoService) s.getService()).getListeners().fireSessionDestroyed(s);
            return true;
        } catch (Exception e) {
            s.getFilterChain().fireExceptionCaught(e);
            clearWriteRequestQueue(s);
            ((AbstractIoService) s.getService()).getListeners().fireSessionDestroyed(s);
            return false;
        } catch (Throwable th) {
            Throwable th2 = th;
            clearWriteRequestQueue(s);
            ((AbstractIoService) s.getService()).getListeners().fireSessionDestroyed(s);
            throw th2;
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void clearWriteRequestQueue(S r6) {
        /*
            r5 = this;
            org.apache.mina.core.write.WriteRequestQueue r1 = r6.getWriteRequestQueue()
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            org.apache.mina.core.write.WriteRequest r3 = r1.poll(r6)
            if (r3 == 0) goto L_0x003b
            java.lang.Object r0 = r3.getMessage()
            boolean r4 = r0 instanceof org.apache.mina.core.buffer.IoBuffer
            if (r4 == 0) goto L_0x0037
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0
            boolean r4 = r0.hasRemaining()
            if (r4 == 0) goto L_0x002f
            r0.reset()
            r2.add(r3)
        L_0x0025:
            org.apache.mina.core.write.WriteRequest r0 = r1.poll(r6)
            if (r0 == 0) goto L_0x003b
            r2.add(r0)
            goto L_0x0025
        L_0x002f:
            org.apache.mina.core.filterchain.IoFilterChain r0 = r6.getFilterChain()
            r0.fireMessageSent(r3)
            goto L_0x0025
        L_0x0037:
            r2.add(r3)
            goto L_0x0025
        L_0x003b:
            boolean r0 = r2.isEmpty()
            if (r0 != 0) goto L_0x0068
            org.apache.mina.core.write.WriteToClosedSessionException r1 = new org.apache.mina.core.write.WriteToClosedSessionException
            r1.<init>(r2)
            java.util.Iterator r2 = r2.iterator()
        L_0x004a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0061
            java.lang.Object r0 = r2.next()
            org.apache.mina.core.write.WriteRequest r0 = (org.apache.mina.core.write.WriteRequest) r0
            r6.decreaseScheduledBytesAndMessages(r0)
            org.apache.mina.core.future.WriteFuture r0 = r0.getFuture()
            r0.setException(r1)
            goto L_0x004a
        L_0x0061:
            org.apache.mina.core.filterchain.IoFilterChain r0 = r6.getFilterChain()
            r0.fireExceptionCaught(r1)
        L_0x0068:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.clearWriteRequestQueue(org.apache.mina.core.session.AbstractIoSession):void");
    }

    /* access modifiers changed from: private */
    public void process() throws Exception {
        Iterator selectedSessions = selectedSessions();
        while (selectedSessions.hasNext()) {
            process((AbstractIoSession) selectedSessions.next());
            selectedSessions.remove();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void process(S r2) {
        /*
            r1 = this;
            boolean r0 = r1.isReadable(r2)
            if (r0 == 0) goto L_0x000f
            boolean r0 = r2.isReadSuspended()
            if (r0 != 0) goto L_0x000f
            r1.read(r2)
        L_0x000f:
            boolean r0 = r1.isWritable(r2)
            if (r0 == 0) goto L_0x0027
            boolean r0 = r2.isWriteSuspended()
            if (r0 != 0) goto L_0x0027
            r0 = 1
            boolean r0 = r2.setScheduledForFlush(r0)
            if (r0 == 0) goto L_0x0027
            java.util.Queue<S> r0 = r1.flushingSessions
            r0.add(r2)
        L_0x0027:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.process(org.apache.mina.core.session.AbstractIoSession):void");
    }

    private void read(S s) {
        int read;
        IoSessionConfig config = s.getConfig();
        IoBuffer allocate = IoBuffer.allocate(config.getReadBufferSize());
        boolean hasFragmentation = s.getTransportMetadata().hasFragmentation();
        int i = 0;
        if (hasFragmentation) {
            do {
                try {
                    read = read(s, allocate);
                    if (read > 0) {
                        i += read;
                    }
                } catch (Throwable th) {
                    if ((th instanceof IOException) && (!(th instanceof PortUnreachableException) || !AbstractDatagramSessionConfig.class.isAssignableFrom(config.getClass()) || ((AbstractDatagramSessionConfig) config).isCloseOnPortUnreachable())) {
                        scheduleRemove(s);
                    }
                    s.getFilterChain().fireExceptionCaught(th);
                    return;
                }
            } while (allocate.hasRemaining());
        } else {
            read = read(s, allocate);
            if (read > 0) {
                i = read;
            }
        }
        allocate.flip();
        if (i > 0) {
            s.getFilterChain().fireMessageReceived(allocate);
            if (hasFragmentation) {
                if ((i << 1) < config.getReadBufferSize()) {
                    s.decreaseReadBufferSize();
                } else if (i == config.getReadBufferSize()) {
                    s.increaseReadBufferSize();
                }
            }
        }
        if (read < 0) {
            scheduleRemove(s);
        }
    }

    private static String byteArrayToHex(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        int i = 0;
        for (byte b2 : bArr) {
            int i2 = (b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) >> 4;
            int i3 = i + 1;
            cArr[i] = (char) (i2 > 9 ? i2 + 55 : i2 + 48);
            byte b3 = b2 & 15;
            i = i3 + 1;
            cArr[i3] = (char) (b3 > 9 ? b3 + 55 : b3 + 48);
            if (i > 60) {
                break;
            }
        }
        return new String(cArr);
    }

    /* access modifiers changed from: private */
    public void notifyIdleSessions(long j) throws Exception {
        if (j - this.lastIdleCheckTime >= SELECT_TIMEOUT) {
            this.lastIdleCheckTime = j;
            AbstractIoSession.notifyIdleness(allSessions(), j);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public void flush(long j) {
        if (!this.flushingSessions.isEmpty()) {
            do {
                AbstractIoSession abstractIoSession = (AbstractIoSession) this.flushingSessions.poll();
                if (abstractIoSession != null) {
                    abstractIoSession.unscheduledForFlush();
                    SessionState state = getState(abstractIoSession);
                    switch (state) {
                        case OPENED:
                            try {
                                if (flushNow(abstractIoSession, j) && !abstractIoSession.getWriteRequestQueue().isEmpty(abstractIoSession) && !abstractIoSession.isScheduledForFlush()) {
                                    scheduleFlush(abstractIoSession);
                                    break;
                                }
                            } catch (Exception e) {
                                scheduleRemove(abstractIoSession);
                                abstractIoSession.getFilterChain().fireExceptionCaught(e);
                                break;
                            }
                        case CLOSING:
                            break;
                        case OPENING:
                            scheduleFlush(abstractIoSession);
                            return;
                        default:
                            throw new IllegalStateException(String.valueOf(state));
                    }
                } else {
                    return;
                }
            } while (!this.flushingSessions.isEmpty());
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean flushNow(S r15, long r16) {
        /*
            r14 = this;
            boolean r0 = r15.isConnected()
            if (r0 != 0) goto L_0x000b
            r14.scheduleRemove(r15)
            r0 = 0
        L_0x000a:
            return r0
        L_0x000b:
            org.apache.mina.core.service.TransportMetadata r0 = r15.getTransportMetadata()
            boolean r4 = r0.hasFragmentation()
            org.apache.mina.core.write.WriteRequestQueue r9 = r15.getWriteRequestQueue()
            org.apache.mina.core.session.IoSessionConfig r0 = r15.getConfig()
            int r0 = r0.getMaxReadBufferSize()
            org.apache.mina.core.session.IoSessionConfig r1 = r15.getConfig()
            int r1 = r1.getMaxReadBufferSize()
            int r1 = r1 >>> 1
            int r10 = r0 + r1
            r0 = 0
            r3 = 0
            r1 = 0
            r14.setInterestedInWrite(r15, r1)     // Catch:{ Exception -> 0x00b2 }
        L_0x0031:
            r8 = r0
            org.apache.mina.core.write.WriteRequest r3 = r15.getCurrentWriteRequest()     // Catch:{ Exception -> 0x00b2 }
            if (r3 != 0) goto L_0x0043
            org.apache.mina.core.write.WriteRequest r3 = r9.poll(r15)     // Catch:{ Exception -> 0x00b2 }
            if (r3 != 0) goto L_0x0040
        L_0x003e:
            r0 = 1
            goto L_0x000a
        L_0x0040:
            r15.setCurrentWriteRequest(r3)     // Catch:{ Exception -> 0x00b2 }
        L_0x0043:
            java.lang.Object r0 = r3.getMessage()     // Catch:{ Exception -> 0x00b2 }
            boolean r1 = r0 instanceof org.apache.mina.core.buffer.IoBuffer     // Catch:{ Exception -> 0x00b2 }
            if (r1 == 0) goto L_0x0067
            int r5 = r10 - r8
            r1 = r14
            r2 = r15
            r6 = r16
            int r1 = r1.writeBuffer(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00b2 }
            if (r1 <= 0) goto L_0x00c6
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0     // Catch:{ Exception -> 0x00b2 }
            boolean r0 = r0.hasRemaining()     // Catch:{ Exception -> 0x00b2 }
            if (r0 == 0) goto L_0x00c6
            int r0 = r8 + r1
            r0 = 1
            r14.setInterestedInWrite(r15, r0)     // Catch:{ Exception -> 0x00b2 }
            r0 = 0
            goto L_0x000a
        L_0x0067:
            boolean r1 = r0 instanceof org.apache.mina.core.file.FileRegion     // Catch:{ Exception -> 0x00b2 }
            if (r1 == 0) goto L_0x008b
            int r5 = r10 - r8
            r1 = r14
            r2 = r15
            r6 = r16
            int r1 = r1.writeFile(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x00b2 }
            if (r1 <= 0) goto L_0x00c6
            org.apache.mina.core.file.FileRegion r0 = (org.apache.mina.core.file.FileRegion) r0     // Catch:{ Exception -> 0x00b2 }
            long r6 = r0.getRemainingBytes()     // Catch:{ Exception -> 0x00b2 }
            r12 = 0
            int r0 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r0 <= 0) goto L_0x00c6
            int r0 = r8 + r1
            r0 = 1
            r14.setInterestedInWrite(r15, r0)     // Catch:{ Exception -> 0x00b2 }
            r0 = 0
            goto L_0x000a
        L_0x008b:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00b2 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b2 }
            r2.<init>()     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r4 = "Don't know how to handle message of type '"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x00b2 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = r0.getName()     // Catch:{ Exception -> 0x00b2 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r2 = "'.  Are you missing a protocol encoder?"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00b2 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00b2 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00b2 }
            throw r1     // Catch:{ Exception -> 0x00b2 }
        L_0x00b2:
            r0 = move-exception
            if (r3 == 0) goto L_0x00bc
            org.apache.mina.core.future.WriteFuture r1 = r3.getFuture()
            r1.setException(r0)
        L_0x00bc:
            org.apache.mina.core.filterchain.IoFilterChain r1 = r15.getFilterChain()
            r1.fireExceptionCaught(r0)
            r0 = 0
            goto L_0x000a
        L_0x00c6:
            r0 = r1
            if (r0 != 0) goto L_0x00d0
            r0 = 1
            r14.setInterestedInWrite(r15, r0)     // Catch:{ Exception -> 0x00b2 }
            r0 = 0
            goto L_0x000a
        L_0x00d0:
            int r0 = r0 + r8
            if (r0 < r10) goto L_0x00d9
            r14.scheduleFlush(r15)     // Catch:{ Exception -> 0x00b2 }
            r0 = 0
            goto L_0x000a
        L_0x00d9:
            if (r0 < r10) goto L_0x0031
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.flushNow(org.apache.mina.core.session.AbstractIoSession, long):boolean");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private int writeBuffer(S r4, org.apache.mina.core.write.WriteRequest r5, boolean r6, int r7, long r8) throws java.lang.Exception {
        /*
            r3 = this;
            java.lang.Object r0 = r5.getMessage()
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0
            r1 = 0
            boolean r2 = r0.hasRemaining()
            if (r2 == 0) goto L_0x001b
            if (r6 == 0) goto L_0x0036
            int r2 = r0.remaining()
            int r2 = java.lang.Math.min(r2, r7)
        L_0x0017:
            int r1 = r3.write(r4, r0, r2)     // Catch:{ IOException -> 0x003b }
        L_0x001b:
            r4.increaseWrittenBytes(r1, r8)
            boolean r2 = r0.hasRemaining()
            if (r2 == 0) goto L_0x0028
            if (r6 != 0) goto L_0x0035
            if (r1 == 0) goto L_0x0035
        L_0x0028:
            int r2 = r0.position()
            r0.reset()
            r3.fireMessageSent(r4, r5)
            r0.position(r2)
        L_0x0035:
            return r1
        L_0x0036:
            int r2 = r0.remaining()
            goto L_0x0017
        L_0x003b:
            r2 = move-exception
            r2 = 1
            r4.close(r2)
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.writeBuffer(org.apache.mina.core.session.AbstractIoSession, org.apache.mina.core.write.WriteRequest, boolean, int, long):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private int writeFile(S r10, org.apache.mina.core.write.WriteRequest r11, boolean r12, int r13, long r14) throws java.lang.Exception {
        /*
            r9 = this;
            r6 = 0
            java.lang.Object r0 = r11.getMessage()
            org.apache.mina.core.file.FileRegion r0 = (org.apache.mina.core.file.FileRegion) r0
            long r2 = r0.getRemainingBytes()
            int r1 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r1 <= 0) goto L_0x0044
            if (r12 == 0) goto L_0x0037
            long r2 = r0.getRemainingBytes()
            long r4 = (long) r13
            long r2 = java.lang.Math.min(r2, r4)
            int r1 = (int) r2
        L_0x001c:
            int r1 = r9.transferFile(r10, r0, r1)
            long r2 = (long) r1
            r0.update(r2)
        L_0x0024:
            r10.increaseWrittenBytes(r1, r14)
            long r2 = r0.getRemainingBytes()
            int r0 = (r2 > r6 ? 1 : (r2 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0033
            if (r12 != 0) goto L_0x0036
            if (r1 == 0) goto L_0x0036
        L_0x0033:
            r9.fireMessageSent(r10, r11)
        L_0x0036:
            return r1
        L_0x0037:
            r2 = 2147483647(0x7fffffff, double:1.060997895E-314)
            long r4 = r0.getRemainingBytes()
            long r2 = java.lang.Math.min(r2, r4)
            int r1 = (int) r2
            goto L_0x001c
        L_0x0044:
            r1 = 0
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.writeFile(org.apache.mina.core.session.AbstractIoSession, org.apache.mina.core.write.WriteRequest, boolean, int, long):int");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void fireMessageSent(S r2, org.apache.mina.core.write.WriteRequest r3) {
        /*
            r1 = this;
            r0 = 0
            r2.setCurrentWriteRequest(r0)
            org.apache.mina.core.filterchain.IoFilterChain r0 = r2.getFilterChain()
            r0.fireMessageSent(r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingIoProcessor.fireMessageSent(org.apache.mina.core.session.AbstractIoSession, org.apache.mina.core.write.WriteRequest):void");
    }

    /* access modifiers changed from: private */
    public void updateTrafficMask() {
        int size = this.trafficControllingSessions.size();
        while (size > 0) {
            AbstractIoSession abstractIoSession = (AbstractIoSession) this.trafficControllingSessions.poll();
            if (abstractIoSession != null) {
                SessionState state = getState(abstractIoSession);
                switch (state) {
                    case OPENED:
                        updateTrafficControl(abstractIoSession);
                        break;
                    case CLOSING:
                        break;
                    case OPENING:
                        this.trafficControllingSessions.add(abstractIoSession);
                        break;
                    default:
                        throw new IllegalStateException(String.valueOf(state));
                }
                size--;
            } else {
                return;
            }
        }
    }

    public void updateTrafficControl(S s) {
        boolean z;
        try {
            setInterestedInRead(s, !s.isReadSuspended());
        } catch (Exception e) {
            s.getFilterChain().fireExceptionCaught(e);
        }
        try {
            if (s.getWriteRequestQueue().isEmpty(s) || s.isWriteSuspended()) {
                z = false;
            } else {
                z = true;
            }
            setInterestedInWrite(s, z);
        } catch (Exception e2) {
            s.getFilterChain().fireExceptionCaught(e2);
        }
    }

    private class Processor implements Runnable {
        static final /* synthetic */ boolean $assertionsDisabled = (!AbstractPollingIoProcessor.class.desiredAssertionStatus());

        private Processor() {
        }

        public void run() {
            int i = 0;
            if ($assertionsDisabled || AbstractPollingIoProcessor.this.processorRef.get() == this) {
                long unused = AbstractPollingIoProcessor.this.lastIdleCheckTime = System.currentTimeMillis();
                while (true) {
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        int select = AbstractPollingIoProcessor.this.select(AbstractPollingIoProcessor.SELECT_TIMEOUT);
                        long currentTimeMillis2 = System.currentTimeMillis();
                        long j = currentTimeMillis2 - currentTimeMillis;
                        if (select != 0 || AbstractPollingIoProcessor.this.wakeupCalled.get() || j >= 100) {
                            int access$400 = i + AbstractPollingIoProcessor.this.handleNewSessions();
                            AbstractPollingIoProcessor.this.updateTrafficMask();
                            if (select > 0) {
                                AbstractPollingIoProcessor.this.process();
                            }
                            long currentTimeMillis3 = System.currentTimeMillis();
                            AbstractPollingIoProcessor.this.flush(currentTimeMillis3);
                            i = access$400 - AbstractPollingIoProcessor.this.removeSessions();
                            AbstractPollingIoProcessor.this.notifyIdleSessions(currentTimeMillis3);
                            if (i == 0) {
                                AbstractPollingIoProcessor.this.processorRef.set(null);
                                if (!AbstractPollingIoProcessor.this.newSessions.isEmpty() || !AbstractPollingIoProcessor.this.isSelectorEmpty()) {
                                    if (!$assertionsDisabled && AbstractPollingIoProcessor.this.processorRef.get() == this) {
                                        throw new AssertionError();
                                    } else if (!AbstractPollingIoProcessor.this.processorRef.compareAndSet(null, this)) {
                                        if (!$assertionsDisabled && AbstractPollingIoProcessor.this.processorRef.get() == this) {
                                            throw new AssertionError();
                                        }
                                    } else if (!$assertionsDisabled && AbstractPollingIoProcessor.this.processorRef.get() != this) {
                                        throw new AssertionError();
                                    }
                                } else if (!$assertionsDisabled && AbstractPollingIoProcessor.this.processorRef.get() == this) {
                                    throw new AssertionError();
                                }
                            }
                            if (AbstractPollingIoProcessor.this.isDisposing()) {
                                Iterator allSessions = AbstractPollingIoProcessor.this.allSessions();
                                while (allSessions.hasNext()) {
                                    AbstractPollingIoProcessor.this.scheduleRemove((AbstractIoSession) allSessions.next());
                                }
                                AbstractPollingIoProcessor.this.wakeup();
                            }
                        } else if (AbstractPollingIoProcessor.this.isBrokenConnection()) {
                            AbstractPollingIoProcessor.LOG.d("Broken connection");
                            AbstractPollingIoProcessor.this.wakeupCalled.getAndSet(false);
                        } else {
                            AbstractPollingIoProcessor.LOG.d("Create a new selector. Selected is 0, delta = " + (currentTimeMillis2 - currentTimeMillis));
                            AbstractPollingIoProcessor.this.registerNewSelector();
                            AbstractPollingIoProcessor.this.wakeupCalled.getAndSet(false);
                        }
                    } catch (ClosedSelectorException e) {
                    } catch (Throwable th) {
                        ExceptionMonitor.getInstance().exceptionCaught(th);
                        try {
                            Thread.sleep(AbstractPollingIoProcessor.SELECT_TIMEOUT);
                        } catch (InterruptedException e2) {
                            ExceptionMonitor.getInstance().exceptionCaught(e2);
                        }
                    }
                }
                try {
                    synchronized (AbstractPollingIoProcessor.this.disposalLock) {
                        if (AbstractPollingIoProcessor.this.disposing) {
                            AbstractPollingIoProcessor.this.doDispose();
                        }
                    }
                    AbstractPollingIoProcessor.this.disposalFuture.setValue(true);
                } catch (Throwable th2) {
                    try {
                        ExceptionMonitor.getInstance().exceptionCaught(th2);
                    } finally {
                        AbstractPollingIoProcessor.this.disposalFuture.setValue(true);
                    }
                }
            } else {
                throw new AssertionError();
            }
        }
    }
}
