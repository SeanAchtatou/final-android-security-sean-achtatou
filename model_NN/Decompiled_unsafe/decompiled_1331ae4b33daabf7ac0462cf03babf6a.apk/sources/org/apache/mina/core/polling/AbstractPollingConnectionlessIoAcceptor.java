package org.apache.mina.core.polling;

import java.net.SocketAddress;
import java.nio.channels.SelectableChannel;
import java.nio.channels.SelectionKey;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.Semaphore;
import org.apache.mina.core.RuntimeIoException;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.service.AbstractIoAcceptor;
import org.apache.mina.core.service.AbstractIoService;
import org.apache.mina.core.service.IoProcessor;
import org.apache.mina.core.session.AbstractIoSession;
import org.apache.mina.core.session.ExpiringSessionRecycler;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.core.session.IoSessionRecycler;
import org.apache.mina.util.ExceptionMonitor;

public abstract class AbstractPollingConnectionlessIoAcceptor<S extends AbstractIoSession, H> extends AbstractIoAcceptor implements IoProcessor<S> {
    private static final IoSessionRecycler DEFAULT_RECYCLER = new ExpiringSessionRecycler();
    private static final long SELECT_TIMEOUT = 1000;
    /* access modifiers changed from: private */
    public AbstractPollingConnectionlessIoAcceptor<S, H>.Acceptor acceptor;
    private final Map<SocketAddress, H> boundHandles;
    /* access modifiers changed from: private */
    public final Queue<AbstractIoAcceptor.AcceptorOperationFuture> cancelQueue;
    /* access modifiers changed from: private */
    public final AbstractIoService.ServiceOperationFuture disposalFuture;
    private final Queue<S> flushingSessions;
    /* access modifiers changed from: private */
    public long lastIdleCheckTime;
    /* access modifiers changed from: private */
    public final Semaphore lock;
    /* access modifiers changed from: private */
    public final Queue<AbstractIoAcceptor.AcceptorOperationFuture> registerQueue;
    /* access modifiers changed from: private */
    public volatile boolean selectable;
    private IoSessionRecycler sessionRecycler;

    /* access modifiers changed from: protected */
    public abstract void close(H h) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void destroy() throws Exception;

    /* access modifiers changed from: protected */
    public abstract void init() throws Exception;

    /* access modifiers changed from: protected */
    public abstract boolean isReadable(H h);

    /* access modifiers changed from: protected */
    public abstract boolean isWritable(H h);

    /* access modifiers changed from: protected */
    public abstract SocketAddress localAddress(H h) throws Exception;

    /* access modifiers changed from: protected */
    public abstract S newSession(IoProcessor<S> ioProcessor, H h, SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract H open(SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract SocketAddress receive(H h, IoBuffer ioBuffer) throws Exception;

    /* access modifiers changed from: protected */
    public abstract int select() throws Exception;

    /* access modifiers changed from: protected */
    public abstract int select(long j) throws Exception;

    /* access modifiers changed from: protected */
    public abstract Set<SelectionKey> selectedHandles();

    /* access modifiers changed from: protected */
    public abstract int send(S s, IoBuffer ioBuffer, SocketAddress socketAddress) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void setInterestedInWrite(S s, boolean z) throws Exception;

    /* access modifiers changed from: protected */
    public abstract void wakeup();

    protected AbstractPollingConnectionlessIoAcceptor(IoSessionConfig ioSessionConfig) {
        this(ioSessionConfig, null);
    }

    protected AbstractPollingConnectionlessIoAcceptor(IoSessionConfig ioSessionConfig, Executor executor) {
        super(ioSessionConfig, executor);
        this.lock = new Semaphore(1);
        this.registerQueue = new ConcurrentLinkedQueue();
        this.cancelQueue = new ConcurrentLinkedQueue();
        this.flushingSessions = new ConcurrentLinkedQueue();
        this.boundHandles = Collections.synchronizedMap(new HashMap());
        this.sessionRecycler = DEFAULT_RECYCLER;
        this.disposalFuture = new AbstractIoService.ServiceOperationFuture();
        try {
            init();
            this.selectable = true;
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e) {
                    ExceptionMonitor.getInstance().exceptionCaught(e);
                }
            }
        } catch (RuntimeException e2) {
            throw e2;
        } catch (Exception e3) {
            throw new RuntimeIoException("Failed to initialize.", e3);
        } catch (Throwable th) {
            if (!this.selectable) {
                try {
                    destroy();
                } catch (Exception e4) {
                    ExceptionMonitor.getInstance().exceptionCaught(e4);
                }
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void dispose0() throws Exception {
        unbind();
        startupAcceptor();
        wakeup();
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public final Set<SocketAddress> bindInternal(List<? extends SocketAddress> list) throws Exception {
        AbstractIoAcceptor.AcceptorOperationFuture acceptorOperationFuture = new AbstractIoAcceptor.AcceptorOperationFuture(list);
        this.registerQueue.add(acceptorOperationFuture);
        startupAcceptor();
        try {
            this.lock.acquire();
            Thread.sleep(10);
            wakeup();
            this.lock.release();
            acceptorOperationFuture.awaitUninterruptibly();
            if (acceptorOperationFuture.getException() != null) {
                throw acceptorOperationFuture.getException();
            }
            HashSet hashSet = new HashSet();
            for (H localAddress : this.boundHandles.values()) {
                hashSet.add(localAddress(localAddress));
            }
            return hashSet;
        } catch (Throwable th) {
            this.lock.release();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public final void unbind0(List<? extends SocketAddress> list) throws Exception {
        AbstractIoAcceptor.AcceptorOperationFuture acceptorOperationFuture = new AbstractIoAcceptor.AcceptorOperationFuture(list);
        this.cancelQueue.add(acceptorOperationFuture);
        startupAcceptor();
        wakeup();
        acceptorOperationFuture.awaitUninterruptibly();
        if (acceptorOperationFuture.getException() != null) {
            throw acceptorOperationFuture.getException();
        }
    }

    public final IoSession newSession(SocketAddress socketAddress, SocketAddress socketAddress2) {
        IoSession newSessionWithoutLock;
        if (isDisposing()) {
            throw new IllegalStateException("Already disposed.");
        } else if (socketAddress == null) {
            throw new IllegalArgumentException("remoteAddress");
        } else {
            synchronized (this.bindLock) {
                if (!isActive()) {
                    throw new IllegalStateException("Can't create a session from a unbound service.");
                }
                try {
                    newSessionWithoutLock = newSessionWithoutLock(socketAddress, socketAddress2);
                } catch (RuntimeException e) {
                    throw e;
                } catch (Error e2) {
                    throw e2;
                } catch (Exception e3) {
                    throw new RuntimeIoException("Failed to create a session.", e3);
                }
            }
            return newSessionWithoutLock;
        }
    }

    private IoSession newSessionWithoutLock(SocketAddress socketAddress, SocketAddress socketAddress2) throws Exception {
        IoSession recycle;
        H h = this.boundHandles.get(socketAddress2);
        if (h == null) {
            throw new IllegalArgumentException("Unknown local address: " + socketAddress2);
        }
        synchronized (this.sessionRecycler) {
            recycle = this.sessionRecycler.recycle(socketAddress);
            if (recycle == null) {
                recycle = newSession(this, h, socketAddress);
                getSessionRecycler().put(recycle);
                initSession(recycle, null, null);
                try {
                    getFilterChainBuilder().buildFilterChain(recycle.getFilterChain());
                    getListeners().fireSessionCreated(recycle);
                } catch (Throwable th) {
                    ExceptionMonitor.getInstance().exceptionCaught(th);
                }
            }
        }
        return recycle;
    }

    public final IoSessionRecycler getSessionRecycler() {
        return this.sessionRecycler;
    }

    public final void setSessionRecycler(IoSessionRecycler ioSessionRecycler) {
        synchronized (this.bindLock) {
            if (isActive()) {
                throw new IllegalStateException("sessionRecycler can't be set while the acceptor is bound.");
            }
            if (ioSessionRecycler == null) {
                ioSessionRecycler = DEFAULT_RECYCLER;
            }
            this.sessionRecycler = ioSessionRecycler;
        }
    }

    public void add(S s) {
    }

    public void flush(S s) {
        if (scheduleFlush(s)) {
            wakeup();
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* JADX WARNING: CFG modification limit reached, blocks count: 135 */
    public void write(S r9, org.apache.mina.core.write.WriteRequest r10) {
        /*
            r8 = this;
            r3 = 0
            r1 = 0
            long r4 = java.lang.System.currentTimeMillis()
            org.apache.mina.core.write.WriteRequestQueue r6 = r9.getWriteRequestQueue()
            org.apache.mina.core.session.IoSessionConfig r0 = r9.getConfig()
            int r0 = r0.getMaxReadBufferSize()
            org.apache.mina.core.session.IoSessionConfig r2 = r9.getConfig()
            int r2 = r2.getMaxReadBufferSize()
            int r2 = r2 >>> 1
            int r7 = r0 + r2
            java.lang.Object r0 = r10.getMessage()
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0
            int r2 = r0.remaining()
            if (r2 != 0) goto L_0x00b0
            r9.setCurrentWriteRequest(r3)
            r0.reset()
            org.apache.mina.core.filterchain.IoFilterChain r0 = r9.getFilterChain()
            r0.fireMessageSent(r10)
        L_0x0037:
            return
        L_0x0038:
            r9.setCurrentWriteRequest(r0)     // Catch:{ Exception -> 0x009f }
        L_0x003b:
            r3 = r0
            java.lang.Object r0 = r3.getMessage()     // Catch:{ Exception -> 0x009f }
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0     // Catch:{ Exception -> 0x009f }
            int r2 = r0.remaining()     // Catch:{ Exception -> 0x009f }
            if (r2 != 0) goto L_0x0068
            r2 = 0
            r9.setCurrentWriteRequest(r2)     // Catch:{ Exception -> 0x009f }
            r0.reset()     // Catch:{ Exception -> 0x009f }
            org.apache.mina.core.filterchain.IoFilterChain r0 = r9.getFilterChain()     // Catch:{ Exception -> 0x009f }
            r0.fireMessageSent(r3)     // Catch:{ Exception -> 0x009f }
            r0 = r3
        L_0x0057:
            if (r0 != 0) goto L_0x003b
            org.apache.mina.core.write.WriteRequest r0 = r6.poll(r9)     // Catch:{ Exception -> 0x009f }
            if (r0 != 0) goto L_0x0038
            r0 = 0
            r8.setInterestedInWrite(r9, r0)     // Catch:{ Exception -> 0x009f }
            r0 = r1
        L_0x0064:
            r9.increaseWrittenBytes(r0, r4)
            goto L_0x0037
        L_0x0068:
            java.net.SocketAddress r2 = r3.getDestination()     // Catch:{ Exception -> 0x009f }
            if (r2 != 0) goto L_0x0072
            java.net.SocketAddress r2 = r9.getRemoteAddress()     // Catch:{ Exception -> 0x009f }
        L_0x0072:
            int r2 = r8.send(r9, r0, r2)     // Catch:{ Exception -> 0x009f }
            if (r2 == 0) goto L_0x007a
            if (r1 < r7) goto L_0x008a
        L_0x007a:
            r0 = 1
            r8.setInterestedInWrite(r9, r0)     // Catch:{ Exception -> 0x009f }
            org.apache.mina.core.write.WriteRequestQueue r0 = r9.getWriteRequestQueue()     // Catch:{ Exception -> 0x009f }
            r0.offer(r9, r3)     // Catch:{ Exception -> 0x009f }
            r8.scheduleFlush(r9)     // Catch:{ Exception -> 0x009f }
            r0 = r3
            goto L_0x0057
        L_0x008a:
            r6 = 0
            r8.setInterestedInWrite(r9, r6)     // Catch:{ Exception -> 0x009f }
            r6 = 0
            r9.setCurrentWriteRequest(r6)     // Catch:{ Exception -> 0x009f }
            int r1 = r1 + r2
            r0.reset()     // Catch:{ Exception -> 0x009f }
            org.apache.mina.core.filterchain.IoFilterChain r0 = r9.getFilterChain()     // Catch:{ Exception -> 0x009f }
            r0.fireMessageSent(r3)     // Catch:{ Exception -> 0x009f }
            r0 = r1
            goto L_0x0064
        L_0x009f:
            r0 = move-exception
            org.apache.mina.core.filterchain.IoFilterChain r2 = r9.getFilterChain()     // Catch:{ all -> 0x00ab }
            r2.fireExceptionCaught(r0)     // Catch:{ all -> 0x00ab }
            r9.increaseWrittenBytes(r1, r4)
            goto L_0x0037
        L_0x00ab:
            r0 = move-exception
            r9.increaseWrittenBytes(r1, r4)
            throw r0
        L_0x00b0:
            r0 = r10
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.write(org.apache.mina.core.session.AbstractIoSession, org.apache.mina.core.write.WriteRequest):void");
    }

    public void remove(S s) {
        getSessionRecycler().remove(s);
        getListeners().fireSessionDestroyed(s);
    }

    public void updateTrafficControl(S s) {
        throw new UnsupportedOperationException();
    }

    private void startupAcceptor() throws InterruptedException {
        if (!this.selectable) {
            this.registerQueue.clear();
            this.cancelQueue.clear();
            this.flushingSessions.clear();
        }
        this.lock.acquire();
        if (this.acceptor == null) {
            this.acceptor = new Acceptor();
            executeWorker(this.acceptor);
            return;
        }
        this.lock.release();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean scheduleFlush(S r3) {
        /*
            r2 = this;
            r0 = 1
            boolean r1 = r3.setScheduledForFlush(r0)
            if (r1 == 0) goto L_0x000d
            java.util.Queue<S> r1 = r2.flushingSessions
            r1.add(r3)
        L_0x000c:
            return r0
        L_0x000d:
            r0 = 0
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.scheduleFlush(org.apache.mina.core.session.AbstractIoSession):boolean");
    }

    private class Acceptor implements Runnable {
        private Acceptor() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:49:0x00fb, code lost:
            r2 = e;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00b4 A[ExcHandler: ClosedSelectorException (e java.nio.channels.ClosedSelectorException), Splitter:B:3:0x001d] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r8 = this;
                r1 = 0
                r6 = 1
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                long r2 = java.lang.System.currentTimeMillis()
                long unused = r0.lastIdleCheckTime = r2
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                java.util.concurrent.Semaphore r0 = r0.lock
                r0.release()
                r0 = r1
            L_0x0015:
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                boolean r2 = r2.selectable
                if (r2 == 0) goto L_0x005e
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                r4 = 1000(0x3e8, double:4.94E-321)
                int r3 = r2.select(r4)     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                int r2 = r2.registerHandles()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                int r2 = r2 + r0
                if (r2 != 0) goto L_0x008f
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ all -> 0x00b6 }
                java.util.concurrent.Semaphore r0 = r0.lock     // Catch:{ all -> 0x00b6 }
                r0.acquire()     // Catch:{ all -> 0x00b6 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ all -> 0x00b6 }
                java.util.Queue r0 = r0.registerQueue     // Catch:{ all -> 0x00b6 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00b6 }
                if (r0 == 0) goto L_0x0086
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ all -> 0x00b6 }
                java.util.Queue r0 = r0.cancelQueue     // Catch:{ all -> 0x00b6 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00b6 }
                if (r0 == 0) goto L_0x0086
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ all -> 0x00b6 }
                r3 = 0
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.Acceptor unused = r0.acceptor = r3     // Catch:{ all -> 0x00b6 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                java.util.concurrent.Semaphore r0 = r0.lock     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                r0.release()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
            L_0x005e:
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                boolean r0 = r0.selectable
                if (r0 == 0) goto L_0x0085
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                boolean r0 = r0.isDisposing()
                if (r0 == 0) goto L_0x0085
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                boolean unused = r0.selectable = r1
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ Exception -> 0x00d6 }
                r0.destroy()     // Catch:{ Exception -> 0x00d6 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
                r0.setValue(r1)
            L_0x0085:
                return
            L_0x0086:
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                java.util.concurrent.Semaphore r0 = r0.lock     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                r0.release()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
            L_0x008f:
                if (r3 <= 0) goto L_0x009c
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r3 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                java.util.Set r3 = r3.selectedHandles()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                r0.processReadySessions(r3)     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
            L_0x009c:
                long r4 = java.lang.System.currentTimeMillis()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                r0.flushSessions(r4)     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                int r0 = r0.unregisterHandles()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                int r0 = r2 - r0
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r2 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                r2.notifyIdleSessions(r4)     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00fb }
                goto L_0x0015
            L_0x00b4:
                r0 = move-exception
                goto L_0x005e
            L_0x00b6:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r3 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                java.util.concurrent.Semaphore r3 = r3.lock     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                r3.release()     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
                throw r0     // Catch:{ ClosedSelectorException -> 0x00b4, Exception -> 0x00c1 }
            L_0x00c1:
                r0 = move-exception
                r7 = r0
                r0 = r2
                r2 = r7
            L_0x00c5:
                org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
                r3.exceptionCaught(r2)
                r2 = 1000(0x3e8, double:4.94E-321)
                java.lang.Thread.sleep(r2)     // Catch:{ InterruptedException -> 0x00d3 }
                goto L_0x0015
            L_0x00d3:
                r2 = move-exception
                goto L_0x0015
            L_0x00d6:
                r0 = move-exception
                org.apache.mina.util.ExceptionMonitor r1 = org.apache.mina.util.ExceptionMonitor.getInstance()     // Catch:{ all -> 0x00ec }
                r1.exceptionCaught(r0)     // Catch:{ all -> 0x00ec }
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r0 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r0 = r0.disposalFuture
                java.lang.Boolean r1 = java.lang.Boolean.valueOf(r6)
                r0.setValue(r1)
                goto L_0x0085
            L_0x00ec:
                r0 = move-exception
                org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor r1 = org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.this
                org.apache.mina.core.service.AbstractIoService$ServiceOperationFuture r1 = r1.disposalFuture
                java.lang.Boolean r2 = java.lang.Boolean.valueOf(r6)
                r1.setValue(r2)
                throw r0
            L_0x00fb:
                r2 = move-exception
                goto L_0x00c5
            */
            throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.Acceptor.run():void");
        }
    }

    /* access modifiers changed from: private */
    public void processReadySessions(Set<SelectionKey> set) {
        Iterator<SelectionKey> it = set.iterator();
        while (it.hasNext()) {
            SelectionKey next = it.next();
            SelectableChannel channel = next.channel();
            it.remove();
            if (next != null) {
                try {
                    if (next.isValid() && next.isReadable()) {
                        readHandle(channel);
                    }
                } catch (Throwable th) {
                    ExceptionMonitor.getInstance().exceptionCaught(th);
                }
            }
            if (next != null && next.isValid() && next.isWritable()) {
                Iterator<IoSession> it2 = getManagedSessions().values().iterator();
                while (it2.hasNext()) {
                    scheduleFlush((AbstractIoSession) it2.next());
                }
            }
        }
    }

    private void readHandle(H h) throws Exception {
        IoBuffer allocate = IoBuffer.allocate(getSessionConfig().getReadBufferSize());
        SocketAddress receive = receive(h, allocate);
        if (receive != null) {
            IoSession newSessionWithoutLock = newSessionWithoutLock(receive, localAddress(h));
            allocate.flip();
            newSessionWithoutLock.getFilterChain().fireMessageReceived(allocate);
        }
    }

    /* access modifiers changed from: private */
    public void flushSessions(long j) {
        while (true) {
            AbstractIoSession abstractIoSession = (AbstractIoSession) this.flushingSessions.poll();
            if (abstractIoSession != null) {
                abstractIoSession.unscheduledForFlush();
                try {
                    if (flush(abstractIoSession, j) && !abstractIoSession.getWriteRequestQueue().isEmpty(abstractIoSession) && !abstractIoSession.isScheduledForFlush()) {
                        scheduleFlush(abstractIoSession);
                    }
                } catch (Exception e) {
                    abstractIoSession.getFilterChain().fireExceptionCaught(e);
                }
            } else {
                return;
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: S
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private boolean flush(S r11, long r12) throws java.lang.Exception {
        /*
            r10 = this;
            r1 = 1
            r2 = 0
            org.apache.mina.core.write.WriteRequestQueue r6 = r11.getWriteRequestQueue()
            org.apache.mina.core.session.IoSessionConfig r0 = r11.getConfig()
            int r0 = r0.getMaxReadBufferSize()
            org.apache.mina.core.session.IoSessionConfig r3 = r11.getConfig()
            int r3 = r3.getMaxReadBufferSize()
            int r3 = r3 >>> 1
            int r7 = r0 + r3
            r3 = r2
        L_0x001b:
            org.apache.mina.core.write.WriteRequest r0 = r11.getCurrentWriteRequest()     // Catch:{ all -> 0x004f }
            if (r0 != 0) goto L_0x0033
            org.apache.mina.core.write.WriteRequest r0 = r6.poll(r11)     // Catch:{ all -> 0x004f }
            if (r0 != 0) goto L_0x0030
            r0 = 0
            r10.setInterestedInWrite(r11, r0)     // Catch:{ all -> 0x004f }
            r11.increaseWrittenBytes(r3, r12)
            r0 = r1
        L_0x002f:
            return r0
        L_0x0030:
            r11.setCurrentWriteRequest(r0)     // Catch:{ all -> 0x004f }
        L_0x0033:
            r5 = r0
            java.lang.Object r0 = r5.getMessage()     // Catch:{ all -> 0x004f }
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0     // Catch:{ all -> 0x004f }
            int r4 = r0.remaining()     // Catch:{ all -> 0x004f }
            if (r4 != 0) goto L_0x0055
            r4 = 0
            r11.setCurrentWriteRequest(r4)     // Catch:{ all -> 0x004f }
            r0.reset()     // Catch:{ all -> 0x004f }
            org.apache.mina.core.filterchain.IoFilterChain r0 = r11.getFilterChain()     // Catch:{ all -> 0x004f }
            r0.fireMessageSent(r5)     // Catch:{ all -> 0x004f }
            goto L_0x001b
        L_0x004f:
            r0 = move-exception
            r1 = r3
            r11.increaseWrittenBytes(r1, r12)
            throw r0
        L_0x0055:
            java.net.SocketAddress r4 = r5.getDestination()     // Catch:{ all -> 0x004f }
            if (r4 != 0) goto L_0x005f
            java.net.SocketAddress r4 = r11.getRemoteAddress()     // Catch:{ all -> 0x004f }
        L_0x005f:
            int r4 = r10.send(r11, r0, r4)     // Catch:{ all -> 0x004f }
            if (r4 == 0) goto L_0x0067
            if (r3 < r7) goto L_0x0070
        L_0x0067:
            r0 = 1
            r10.setInterestedInWrite(r11, r0)     // Catch:{ all -> 0x004f }
            r11.increaseWrittenBytes(r3, r12)
            r0 = r2
            goto L_0x002f
        L_0x0070:
            r8 = 0
            r10.setInterestedInWrite(r11, r8)     // Catch:{ all -> 0x004f }
            r8 = 0
            r11.setCurrentWriteRequest(r8)     // Catch:{ all -> 0x004f }
            int r3 = r3 + r4
            r0.reset()     // Catch:{ all -> 0x004f }
            org.apache.mina.core.filterchain.IoFilterChain r0 = r11.getFilterChain()     // Catch:{ all -> 0x004f }
            r0.fireMessageSent(r5)     // Catch:{ all -> 0x004f }
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.flush(org.apache.mina.core.session.AbstractIoSession, long):boolean");
    }

    /* access modifiers changed from: private */
    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public int registerHandles() {
        /*
            r5 = this;
        L_0x0000:
            java.util.Queue<org.apache.mina.core.service.AbstractIoAcceptor$AcceptorOperationFuture> r0 = r5.registerQueue
            java.lang.Object r0 = r0.poll()
            org.apache.mina.core.service.AbstractIoAcceptor$AcceptorOperationFuture r0 = (org.apache.mina.core.service.AbstractIoAcceptor.AcceptorOperationFuture) r0
            if (r0 != 0) goto L_0x000c
            r0 = 0
        L_0x000b:
            return r0
        L_0x000c:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.List r1 = r0.getLocalAddresses()
            java.util.Iterator r3 = r1.iterator()     // Catch:{ Exception -> 0x0031 }
        L_0x0019:
            boolean r1 = r3.hasNext()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x005a
            java.lang.Object r1 = r3.next()     // Catch:{ Exception -> 0x0031 }
            java.net.SocketAddress r1 = (java.net.SocketAddress) r1     // Catch:{ Exception -> 0x0031 }
            java.lang.Object r1 = r5.open(r1)     // Catch:{ Exception -> 0x0031 }
            java.net.SocketAddress r4 = r5.localAddress(r1)     // Catch:{ Exception -> 0x0031 }
            r2.put(r4, r1)     // Catch:{ Exception -> 0x0031 }
            goto L_0x0019
        L_0x0031:
            r1 = move-exception
            r0.setException(r1)     // Catch:{ all -> 0x009d }
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x0000
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x0043:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0098
            java.lang.Object r1 = r0.next()
            r5.close(r1)     // Catch:{ Exception -> 0x0051 }
            goto L_0x0043
        L_0x0051:
            r1 = move-exception
            org.apache.mina.util.ExceptionMonitor r2 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r2.exceptionCaught(r1)
            goto L_0x0043
        L_0x005a:
            java.util.Map<java.net.SocketAddress, H> r1 = r5.boundHandles     // Catch:{ Exception -> 0x0031 }
            r1.putAll(r2)     // Catch:{ Exception -> 0x0031 }
            org.apache.mina.core.service.IoServiceListenerSupport r1 = r5.getListeners()     // Catch:{ Exception -> 0x0031 }
            r1.fireServiceActivated()     // Catch:{ Exception -> 0x0031 }
            r0.setDone()     // Catch:{ Exception -> 0x0031 }
            int r1 = r2.size()     // Catch:{ Exception -> 0x0031 }
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x0095
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x007b:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0092
            java.lang.Object r2 = r0.next()
            r5.close(r2)     // Catch:{ Exception -> 0x0089 }
            goto L_0x007b
        L_0x0089:
            r2 = move-exception
            org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r3.exceptionCaught(r2)
            goto L_0x007b
        L_0x0092:
            r5.wakeup()
        L_0x0095:
            r0 = r1
            goto L_0x000b
        L_0x0098:
            r5.wakeup()
            goto L_0x0000
        L_0x009d:
            r1 = move-exception
            java.lang.Exception r0 = r0.getException()
            if (r0 == 0) goto L_0x00c6
            java.util.Collection r0 = r2.values()
            java.util.Iterator r0 = r0.iterator()
        L_0x00ac:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00c3
            java.lang.Object r2 = r0.next()
            r5.close(r2)     // Catch:{ Exception -> 0x00ba }
            goto L_0x00ac
        L_0x00ba:
            r2 = move-exception
            org.apache.mina.util.ExceptionMonitor r3 = org.apache.mina.util.ExceptionMonitor.getInstance()
            r3.exceptionCaught(r2)
            goto L_0x00ac
        L_0x00c3:
            r5.wakeup()
        L_0x00c6:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.core.polling.AbstractPollingConnectionlessIoAcceptor.registerHandles():int");
    }

    /* access modifiers changed from: private */
    public int unregisterHandles() {
        int i = 0;
        while (true) {
            AbstractIoAcceptor.AcceptorOperationFuture poll = this.cancelQueue.poll();
            if (poll == null) {
                return i;
            }
            int i2 = i;
            for (SocketAddress remove : poll.getLocalAddresses()) {
                H remove2 = this.boundHandles.remove(remove);
                if (remove2 != null) {
                    try {
                        close(remove2);
                        wakeup();
                    } catch (Throwable th) {
                        ExceptionMonitor.getInstance().exceptionCaught(th);
                    } finally {
                        int i3 = i2 + 1;
                    }
                }
            }
            poll.setDone();
            i = i2;
        }
    }

    /* access modifiers changed from: private */
    public void notifyIdleSessions(long j) {
        if (j - this.lastIdleCheckTime >= SELECT_TIMEOUT) {
            this.lastIdleCheckTime = j;
            AbstractIoSession.notifyIdleness(getListeners().getManagedSessions().values().iterator(), j);
        }
    }
}
