package org.apache.mina.proxy.filter;

import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.filterchain.IoFilterAdapter;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.proxy.ProxyLogicHandler;
import org.apache.mina.proxy.event.IoSessionEvent;
import org.apache.mina.proxy.event.IoSessionEventType;
import org.apache.mina.proxy.handlers.ProxyRequest;
import org.apache.mina.proxy.handlers.http.HttpSmartProxyHandler;
import org.apache.mina.proxy.handlers.socks.Socks4LogicHandler;
import org.apache.mina.proxy.handlers.socks.Socks5LogicHandler;
import org.apache.mina.proxy.handlers.socks.SocksProxyRequest;
import org.apache.mina.proxy.session.ProxyIoSession;

public class ProxyFilter extends IoFilterAdapter {
    private static final b LOGGER = c.a(ProxyFilter.class);

    public void onPreAdd(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) {
        if (ioFilterChain.contains(ProxyFilter.class)) {
            throw new IllegalStateException("A filter chain cannot contain more than one ProxyFilter.");
        }
    }

    public void onPreRemove(IoFilterChain ioFilterChain, String str, IoFilter.NextFilter nextFilter) {
        ioFilterChain.getSession().removeAttribute(ProxyIoSession.PROXY_SESSION);
    }

    public void exceptionCaught(IoFilter.NextFilter nextFilter, IoSession ioSession, Throwable th) throws Exception {
        ((ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION)).setAuthenticationFailed(true);
        super.exceptionCaught(nextFilter, ioSession, th);
    }

    private ProxyLogicHandler getProxyHandler(IoSession ioSession) {
        ProxyLogicHandler handler = ((ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION)).getHandler();
        if (handler == null) {
            throw new IllegalStateException();
        } else if (handler.getProxyIoSession().getProxyFilter() == this) {
            return handler;
        } else {
            throw new IllegalArgumentException("Not managed by this filter.");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void messageReceived(org.apache.mina.core.filterchain.IoFilter.NextFilter r4, org.apache.mina.core.session.IoSession r5, java.lang.Object r6) throws org.apache.mina.proxy.ProxyAuthException {
        /*
            r3 = this;
            org.apache.mina.proxy.ProxyLogicHandler r1 = r3.getProxyHandler(r5)
            monitor-enter(r1)
            org.apache.mina.core.buffer.IoBuffer r6 = (org.apache.mina.core.buffer.IoBuffer) r6     // Catch:{ all -> 0x0041 }
            boolean r0 = r1.isHandshakeComplete()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0012
            r4.messageReceived(r5, r6)     // Catch:{ all -> 0x0041 }
        L_0x0010:
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
        L_0x0011:
            return
        L_0x0012:
            org.a.b r0 = org.apache.mina.proxy.filter.ProxyFilter.LOGGER     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = " Data Read: {} ({})"
            r0.a(r2, r1, r6)     // Catch:{ all -> 0x0041 }
        L_0x0019:
            boolean r0 = r6.hasRemaining()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0044
            boolean r0 = r1.isHandshakeComplete()     // Catch:{ all -> 0x0041 }
            if (r0 != 0) goto L_0x0044
            org.a.b r0 = org.apache.mina.proxy.filter.ProxyFilter.LOGGER     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = " Pre-handshake - passing to handler"
            r0.b(r2)     // Catch:{ all -> 0x0041 }
            int r0 = r6.position()     // Catch:{ all -> 0x0041 }
            r1.messageReceived(r4, r6)     // Catch:{ all -> 0x0041 }
            int r2 = r6.position()     // Catch:{ all -> 0x0041 }
            if (r2 == r0) goto L_0x003f
            boolean r0 = r5.isClosing()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0019
        L_0x003f:
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            goto L_0x0011
        L_0x0041:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0041 }
            throw r0
        L_0x0044:
            boolean r0 = r6.hasRemaining()     // Catch:{ all -> 0x0041 }
            if (r0 == 0) goto L_0x0010
            org.a.b r0 = org.apache.mina.proxy.filter.ProxyFilter.LOGGER     // Catch:{ all -> 0x0041 }
            java.lang.String r2 = " Passing remaining data to next filter"
            r0.b(r2)     // Catch:{ all -> 0x0041 }
            r4.messageReceived(r5, r6)     // Catch:{ all -> 0x0041 }
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.proxy.filter.ProxyFilter.messageReceived(org.apache.mina.core.filterchain.IoFilter$NextFilter, org.apache.mina.core.session.IoSession, java.lang.Object):void");
    }

    public void filterWrite(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) {
        writeData(nextFilter, ioSession, writeRequest, false);
    }

    public void writeData(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest, boolean z) {
        ProxyLogicHandler proxyHandler = getProxyHandler(ioSession);
        synchronized (proxyHandler) {
            if (proxyHandler.isHandshakeComplete()) {
                nextFilter.filterWrite(ioSession, writeRequest);
            } else if (z) {
                LOGGER.b("   handshake data: {}", writeRequest.getMessage());
                nextFilter.filterWrite(ioSession, writeRequest);
            } else if (!ioSession.isConnected()) {
                LOGGER.b(" Write request on closed session. Request ignored.");
            } else {
                LOGGER.b(" Handshaking is not complete yet. Buffering write request.");
                proxyHandler.enqueueWriteRequest(nextFilter, writeRequest);
            }
        }
    }

    public void messageSent(IoFilter.NextFilter nextFilter, IoSession ioSession, WriteRequest writeRequest) throws Exception {
        if (writeRequest.getMessage() == null || !(writeRequest.getMessage() instanceof ProxyHandshakeIoBuffer)) {
            nextFilter.messageSent(ioSession, writeRequest);
        }
    }

    public void sessionCreated(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        ProxyLogicHandler httpSmartProxyHandler;
        LOGGER.b("Session created: " + ioSession);
        ProxyIoSession proxyIoSession = (ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION);
        LOGGER.b("  get proxyIoSession: " + proxyIoSession);
        proxyIoSession.setProxyFilter(this);
        if (proxyIoSession.getHandler() == null) {
            ProxyRequest request = proxyIoSession.getRequest();
            if (!(request instanceof SocksProxyRequest)) {
                httpSmartProxyHandler = new HttpSmartProxyHandler(proxyIoSession);
            } else if (((SocksProxyRequest) request).getProtocolVersion() == 4) {
                httpSmartProxyHandler = new Socks4LogicHandler(proxyIoSession);
            } else {
                httpSmartProxyHandler = new Socks5LogicHandler(proxyIoSession);
            }
            proxyIoSession.setHandler(httpSmartProxyHandler);
            httpSmartProxyHandler.doHandshake(nextFilter);
        }
        proxyIoSession.getEventQueue().enqueueEventIfNecessary(new IoSessionEvent(nextFilter, ioSession, IoSessionEventType.CREATED));
    }

    public void sessionOpened(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        ((ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION)).getEventQueue().enqueueEventIfNecessary(new IoSessionEvent(nextFilter, ioSession, IoSessionEventType.OPENED));
    }

    public void sessionIdle(IoFilter.NextFilter nextFilter, IoSession ioSession, IdleStatus idleStatus) throws Exception {
        ((ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION)).getEventQueue().enqueueEventIfNecessary(new IoSessionEvent(nextFilter, ioSession, idleStatus));
    }

    public void sessionClosed(IoFilter.NextFilter nextFilter, IoSession ioSession) throws Exception {
        ((ProxyIoSession) ioSession.getAttribute(ProxyIoSession.PROXY_SESSION)).getEventQueue().enqueueEventIfNecessary(new IoSessionEvent(nextFilter, ioSession, IoSessionEventType.CLOSED));
    }
}
