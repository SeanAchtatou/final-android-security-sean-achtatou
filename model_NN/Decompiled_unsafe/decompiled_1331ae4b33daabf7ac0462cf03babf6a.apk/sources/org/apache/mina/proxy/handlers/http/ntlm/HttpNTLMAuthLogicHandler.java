package org.apache.mina.proxy.handlers.http.ntlm;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.a.b;
import org.a.c;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.proxy.ProxyAuthException;
import org.apache.mina.proxy.handlers.http.AbstractAuthLogicHandler;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;
import org.apache.mina.proxy.handlers.http.HttpProxyRequest;
import org.apache.mina.proxy.handlers.http.HttpProxyResponse;
import org.apache.mina.proxy.session.ProxyIoSession;
import org.apache.mina.proxy.utils.StringUtilities;
import org.apache.mina.util.Base64;

public class HttpNTLMAuthLogicHandler extends AbstractAuthLogicHandler {
    private static final b LOGGER = c.a(HttpNTLMAuthLogicHandler.class);
    private byte[] challengePacket = null;

    public HttpNTLMAuthLogicHandler(ProxyIoSession proxyIoSession) throws ProxyAuthException {
        super(proxyIoSession);
        ((HttpProxyRequest) this.request).checkRequiredProperties(HttpProxyConstants.USER_PROPERTY, HttpProxyConstants.PWD_PROPERTY, HttpProxyConstants.DOMAIN_PROPERTY, HttpProxyConstants.WORKSTATION_PROPERTY);
    }

    public void doHandshake(IoFilter.NextFilter nextFilter) throws ProxyAuthException {
        Map hashMap;
        LOGGER.b(" doHandshake()");
        if (this.step <= 0 || this.challengePacket != null) {
            HttpProxyRequest httpProxyRequest = (HttpProxyRequest) this.request;
            if (httpProxyRequest.getHeaders() != null) {
                hashMap = httpProxyRequest.getHeaders();
            } else {
                hashMap = new HashMap();
            }
            String str = httpProxyRequest.getProperties().get(HttpProxyConstants.DOMAIN_PROPERTY);
            String str2 = httpProxyRequest.getProperties().get(HttpProxyConstants.WORKSTATION_PROPERTY);
            if (this.step > 0) {
                LOGGER.b("  sending NTLM challenge response");
                StringUtilities.addValueToHeader(hashMap, "Proxy-Authorization", "NTLM " + new String(Base64.encodeBase64(NTLMUtilities.createType3Message(httpProxyRequest.getProperties().get(HttpProxyConstants.USER_PROPERTY), httpProxyRequest.getProperties().get(HttpProxyConstants.PWD_PROPERTY), NTLMUtilities.extractChallengeFromType2Message(this.challengePacket), str, str2, Integer.valueOf(NTLMUtilities.extractFlagsFromType2Message(this.challengePacket)), null))), true);
            } else {
                LOGGER.b("  sending NTLM negotiation packet");
                StringUtilities.addValueToHeader(hashMap, "Proxy-Authorization", "NTLM " + new String(Base64.encodeBase64(NTLMUtilities.createType1Message(str2, str, null, null))), true);
            }
            addKeepAliveHeaders(hashMap);
            httpProxyRequest.setHeaders(hashMap);
            writeRequest(nextFilter, httpProxyRequest);
            this.step++;
            return;
        }
        throw new IllegalStateException("NTLM Challenge packet not received");
    }

    private String getNTLMHeader(HttpProxyResponse httpProxyResponse) {
        for (String str : httpProxyResponse.getHeaders().get("Proxy-Authenticate")) {
            if (str.startsWith("NTLM")) {
                return str;
            }
        }
        return null;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v4, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r1v9, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public void handleResponse(HttpProxyResponse httpProxyResponse) throws ProxyAuthException {
        if (this.step == 0) {
            String nTLMHeader = getNTLMHeader(httpProxyResponse);
            this.step = 1;
            if (nTLMHeader == null || nTLMHeader.length() < 5) {
                return;
            }
        }
        if (this.step == 1) {
            String nTLMHeader2 = getNTLMHeader(httpProxyResponse);
            if (nTLMHeader2 == null || nTLMHeader2.length() < 5) {
                throw new ProxyAuthException("Unexpected error while reading server challenge !");
            }
            try {
                this.challengePacket = Base64.decodeBase64(nTLMHeader2.substring(5).getBytes(this.proxyIoSession.getCharsetName()));
                this.step = 2;
            } catch (IOException e) {
                throw new ProxyAuthException("Unable to decode the base64 encoded NTLM challenge", e);
            }
        } else {
            throw new ProxyAuthException("Received unexpected response code (" + httpProxyResponse.getStatusLine() + ").");
        }
    }
}
