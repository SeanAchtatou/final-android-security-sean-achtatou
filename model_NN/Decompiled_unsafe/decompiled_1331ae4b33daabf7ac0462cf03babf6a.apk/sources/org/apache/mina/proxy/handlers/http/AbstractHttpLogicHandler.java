package org.apache.mina.proxy.handlers.http;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.a.b;
import org.a.c;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.filterchain.IoFilter;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.IoFutureListener;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionInitializer;
import org.apache.mina.proxy.AbstractProxyLogicHandler;
import org.apache.mina.proxy.ProxyAuthException;
import org.apache.mina.proxy.session.ProxyIoSession;
import org.apache.mina.proxy.utils.IoBufferDecoder;
import org.apache.mina.proxy.utils.StringUtilities;

public abstract class AbstractHttpLogicHandler extends AbstractProxyLogicHandler {
    private static final byte[] CRLF_DELIMITER = {13, 10};
    private static final String DECODER = (AbstractHttpLogicHandler.class.getName() + ".Decoder");
    private static final byte[] HTTP_DELIMITER = {13, 10, 13, 10};
    /* access modifiers changed from: private */
    public static final b LOGGER = c.a(AbstractHttpLogicHandler.class);
    private int contentLength = -1;
    private int entityBodyLimitPosition;
    private int entityBodyStartPosition;
    private boolean hasChunkedData;
    private HttpProxyResponse parsedResponse = null;
    private IoBuffer responseData = null;
    private boolean waitingChunkedData;
    private boolean waitingFooters;

    public abstract void handleResponse(HttpProxyResponse httpProxyResponse) throws ProxyAuthException;

    public AbstractHttpLogicHandler(ProxyIoSession proxyIoSession) {
        super(proxyIoSession);
    }

    /* JADX WARN: Type inference failed for: r1v5, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.Throwable, org.apache.mina.proxy.ProxyAuthException] */
    public synchronized void messageReceived(IoFilter.NextFilter nextFilter, IoBuffer ioBuffer) throws ProxyAuthException {
        IoBufferDecoder ioBufferDecoder;
        String substring;
        LOGGER.b(" messageReceived()");
        IoBufferDecoder ioBufferDecoder2 = (IoBufferDecoder) getSession().getAttribute(DECODER);
        if (ioBufferDecoder2 == null) {
            IoBufferDecoder ioBufferDecoder3 = new IoBufferDecoder(HTTP_DELIMITER);
            getSession().setAttribute(DECODER, ioBufferDecoder3);
            ioBufferDecoder = ioBufferDecoder3;
        } else {
            ioBufferDecoder = ioBufferDecoder2;
        }
        try {
            if (this.parsedResponse == null) {
                this.responseData = ioBufferDecoder.decodeFully(ioBuffer);
                if (this.responseData != null) {
                    String string = this.responseData.getString(getProxyIoSession().getCharset().newDecoder());
                    this.entityBodyStartPosition = this.responseData.position();
                    LOGGER.b("  response header received:\n{}", string.replace("\r", "\\r").replace("\n", "\\n\n"));
                    this.parsedResponse = decodeResponse(string);
                    if (this.parsedResponse.getStatusCode() == 200 || (this.parsedResponse.getStatusCode() >= 300 && this.parsedResponse.getStatusCode() <= 307)) {
                        ioBuffer.position(0);
                        setHandshakeComplete();
                    } else {
                        String singleValuedHeader = StringUtilities.getSingleValuedHeader(this.parsedResponse.getHeaders(), "Content-Length");
                        if (singleValuedHeader == null) {
                            this.contentLength = 0;
                        } else {
                            this.contentLength = Integer.parseInt(singleValuedHeader.trim());
                            ioBufferDecoder.setContentLength(this.contentLength, true);
                        }
                    }
                }
            }
            if (!this.hasChunkedData) {
                if (this.contentLength > 0) {
                    IoBuffer decodeFully = ioBufferDecoder.decodeFully(ioBuffer);
                    if (decodeFully != null) {
                        this.responseData.setAutoExpand(true);
                        this.responseData.put(decodeFully);
                        this.contentLength = 0;
                    }
                }
                if ("chunked".equalsIgnoreCase(StringUtilities.getSingleValuedHeader(this.parsedResponse.getHeaders(), "Transfer-Encoding"))) {
                    LOGGER.b("Retrieving additional http response chunks");
                    this.hasChunkedData = true;
                    this.waitingChunkedData = true;
                }
            }
            if (this.hasChunkedData) {
                while (true) {
                    if (this.waitingChunkedData) {
                        if (this.contentLength == 0) {
                            ioBufferDecoder.setDelimiter(CRLF_DELIMITER, false);
                            IoBuffer decodeFully2 = ioBufferDecoder.decodeFully(ioBuffer);
                            if (decodeFully2 == null) {
                                break;
                            }
                            String string2 = decodeFully2.getString(getProxyIoSession().getCharset().newDecoder());
                            int indexOf = string2.indexOf(59);
                            if (indexOf >= 0) {
                                substring = string2.substring(0, indexOf);
                            } else {
                                substring = string2.substring(0, string2.length() - 2);
                            }
                            this.contentLength = Integer.decode("0x" + substring).intValue();
                            if (this.contentLength > 0) {
                                this.contentLength += 2;
                                ioBufferDecoder.setContentLength(this.contentLength, true);
                            }
                        }
                        if (this.contentLength != 0) {
                            IoBuffer decodeFully3 = ioBufferDecoder.decodeFully(ioBuffer);
                            if (decodeFully3 == null) {
                                break;
                            }
                            this.contentLength = 0;
                            this.responseData.put(decodeFully3);
                            ioBuffer.position(ioBuffer.position());
                        } else {
                            this.waitingChunkedData = false;
                            this.waitingFooters = true;
                            this.entityBodyLimitPosition = this.responseData.position();
                            break;
                        }
                    } else {
                        break;
                    }
                }
                while (true) {
                    if (!this.waitingFooters) {
                        break;
                    }
                    ioBufferDecoder.setDelimiter(CRLF_DELIMITER, false);
                    IoBuffer decodeFully4 = ioBufferDecoder.decodeFully(ioBuffer);
                    if (decodeFully4 == null) {
                        break;
                    } else if (decodeFully4.remaining() == 2) {
                        this.waitingFooters = false;
                        break;
                    } else {
                        String[] split = decodeFully4.getString(getProxyIoSession().getCharset().newDecoder()).split(":\\s?", 2);
                        StringUtilities.addValueToHeader(this.parsedResponse.getHeaders(), split[0], split[1], false);
                        this.responseData.put(decodeFully4);
                        this.responseData.put(CRLF_DELIMITER);
                    }
                }
            }
            this.responseData.flip();
            LOGGER.b("  end of response received:\n{}", this.responseData.getString(getProxyIoSession().getCharset().newDecoder()));
            this.responseData.position(this.entityBodyStartPosition);
            this.responseData.limit(this.entityBodyLimitPosition);
            this.parsedResponse.setBody(this.responseData.getString(getProxyIoSession().getCharset().newDecoder()));
            this.responseData.free();
            this.responseData = null;
            handleResponse(this.parsedResponse);
            this.parsedResponse = null;
            this.hasChunkedData = false;
            this.contentLength = -1;
            ioBufferDecoder.setDelimiter(HTTP_DELIMITER, true);
            if (!isHandshakeComplete()) {
                doHandshake(nextFilter);
            }
        } catch (Exception e) {
            if (e instanceof ProxyAuthException) {
                throw ((ProxyAuthException) e);
            }
            throw new ProxyAuthException("Handshake failed", e);
        }
    }

    public void writeRequest(IoFilter.NextFilter nextFilter, HttpProxyRequest httpProxyRequest) {
        if (getProxyIoSession().isReconnectionNeeded()) {
            reconnect(nextFilter, httpProxyRequest);
        } else {
            writeRequest0(nextFilter, httpProxyRequest);
        }
    }

    /* access modifiers changed from: private */
    public void writeRequest0(IoFilter.NextFilter nextFilter, HttpProxyRequest httpProxyRequest) {
        try {
            String httpString = httpProxyRequest.toHttpString();
            IoBuffer wrap = IoBuffer.wrap(httpString.getBytes(getProxyIoSession().getCharsetName()));
            LOGGER.b("   write:\n{}", httpString.replace("\r", "\\r").replace("\n", "\\n\n"));
            writeData(nextFilter, wrap);
        } catch (UnsupportedEncodingException e) {
            closeSession("Unable to send HTTP request: ", e);
        }
    }

    private void reconnect(final IoFilter.NextFilter nextFilter, final HttpProxyRequest httpProxyRequest) {
        LOGGER.b("Reconnecting to proxy ...");
        final ProxyIoSession proxyIoSession = getProxyIoSession();
        proxyIoSession.getConnector().connect(new IoSessionInitializer<ConnectFuture>() {
            public void initializeSession(IoSession ioSession, ConnectFuture connectFuture) {
                AbstractHttpLogicHandler.LOGGER.b("Initializing new session: {}", ioSession);
                ioSession.setAttribute(ProxyIoSession.PROXY_SESSION, proxyIoSession);
                proxyIoSession.setSession(ioSession);
                AbstractHttpLogicHandler.LOGGER.b("  setting up proxyIoSession: {}", proxyIoSession);
                connectFuture.addListener((IoFutureListener<?>) new IoFutureListener<ConnectFuture>() {
                    public void operationComplete(ConnectFuture connectFuture) {
                        proxyIoSession.setReconnectionNeeded(false);
                        AbstractHttpLogicHandler.this.writeRequest0(nextFilter, httpProxyRequest);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public HttpProxyResponse decodeResponse(String str) throws Exception {
        LOGGER.b("  parseResponse()");
        String[] split = str.split(HttpProxyConstants.CRLF);
        String[] split2 = split[0].trim().split(" ", 2);
        if (split2.length < 2) {
            throw new Exception("Invalid response status line (" + split2 + "). Response: " + str);
        } else if (split2[1].matches("^\\d\\d\\d")) {
            throw new Exception("Invalid response code (" + split2[1] + "). Response: " + str);
        } else {
            HashMap hashMap = new HashMap();
            for (int i = 1; i < split.length; i++) {
                String[] split3 = split[i].split(":\\s?", 2);
                StringUtilities.addValueToHeader(hashMap, split3[0], split3[1], false);
            }
            return new HttpProxyResponse(split2[0], split2[1], hashMap);
        }
    }
}
