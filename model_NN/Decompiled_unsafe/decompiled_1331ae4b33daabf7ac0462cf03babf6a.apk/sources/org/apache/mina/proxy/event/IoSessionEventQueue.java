package org.apache.mina.proxy.event;

import java.util.LinkedList;
import java.util.Queue;
import org.a.b;
import org.a.c;
import org.apache.mina.proxy.handlers.socks.SocksProxyRequest;
import org.apache.mina.proxy.session.ProxyIoSession;

public class IoSessionEventQueue {
    private static final b logger = c.a(IoSessionEventQueue.class);
    private ProxyIoSession proxyIoSession;
    private Queue<IoSessionEvent> sessionEventsQueue = new LinkedList();

    public IoSessionEventQueue(ProxyIoSession proxyIoSession2) {
        this.proxyIoSession = proxyIoSession2;
    }

    private void discardSessionQueueEvents() {
        synchronized (this.sessionEventsQueue) {
            this.sessionEventsQueue.clear();
            logger.b("Event queue CLEARED");
        }
    }

    public void enqueueEventIfNecessary(IoSessionEvent ioSessionEvent) {
        logger.b("??? >> Enqueue {}", ioSessionEvent);
        if (this.proxyIoSession.getRequest() instanceof SocksProxyRequest) {
            ioSessionEvent.deliverEvent();
        } else if (this.proxyIoSession.getHandler().isHandshakeComplete()) {
            ioSessionEvent.deliverEvent();
        } else if (ioSessionEvent.getType() == IoSessionEventType.CLOSED) {
            if (this.proxyIoSession.isAuthenticationFailed()) {
                this.proxyIoSession.getConnector().cancelConnectFuture();
                discardSessionQueueEvents();
                ioSessionEvent.deliverEvent();
                return;
            }
            discardSessionQueueEvents();
        } else if (ioSessionEvent.getType() == IoSessionEventType.OPENED) {
            enqueueSessionEvent(ioSessionEvent);
            ioSessionEvent.deliverEvent();
        } else {
            enqueueSessionEvent(ioSessionEvent);
        }
    }

    public void flushPendingSessionEvents() throws Exception {
        synchronized (this.sessionEventsQueue) {
            while (true) {
                IoSessionEvent poll = this.sessionEventsQueue.poll();
                if (poll != null) {
                    logger.b(" Flushing buffered event: {}", poll);
                    poll.deliverEvent();
                }
            }
        }
    }

    private void enqueueSessionEvent(IoSessionEvent ioSessionEvent) {
        synchronized (this.sessionEventsQueue) {
            logger.b("Enqueuing event: {}", ioSessionEvent);
            this.sessionEventsQueue.offer(ioSessionEvent);
        }
    }
}
