package org.apache.mina.proxy.utils;

import java.io.UnsupportedEncodingException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class ByteUtilities {
    public static int networkByteOrderToInt(byte[] bArr, int i, int i2) {
        if (i2 > 4) {
            throw new IllegalArgumentException("Cannot handle more than 4 bytes");
        }
        byte b2 = 0;
        for (int i3 = 0; i3 < i2; i3++) {
            b2 = (b2 << 8) | (bArr[i + i3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        }
        return b2;
    }

    public static byte[] intToNetworkByteOrder(int i, int i2) {
        byte[] bArr = new byte[i2];
        intToNetworkByteOrder(i, bArr, 0, i2);
        return bArr;
    }

    public static void intToNetworkByteOrder(int i, byte[] bArr, int i2, int i3) {
        if (i3 > 4) {
            throw new IllegalArgumentException("Cannot handle more than 4 bytes");
        }
        for (int i4 = i3 - 1; i4 >= 0; i4--) {
            bArr[i2 + i4] = (byte) (i & 255);
            i >>>= 8;
        }
    }

    public static final byte[] writeShort(short s) {
        return writeShort(s, new byte[2], 0);
    }

    public static final byte[] writeShort(short s, byte[] bArr, int i) {
        bArr[i] = (byte) s;
        bArr[i + 1] = (byte) (s >> 8);
        return bArr;
    }

    public static final byte[] writeInt(int i) {
        return writeInt(i, new byte[4], 0);
    }

    public static final byte[] writeInt(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) i;
        bArr[i2 + 1] = (byte) (i >> 8);
        bArr[i2 + 2] = (byte) (i >> 16);
        bArr[i2 + 3] = (byte) (i >> 24);
        return bArr;
    }

    public static final void changeWordEndianess(byte[] bArr, int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3 += 4) {
            byte b2 = bArr[i3];
            bArr[i3] = bArr[i3 + 3];
            bArr[i3 + 3] = b2;
            byte b3 = bArr[i3 + 1];
            bArr[i3 + 1] = bArr[i3 + 2];
            bArr[i3 + 2] = b3;
        }
    }

    public static final void changeByteEndianess(byte[] bArr, int i, int i2) {
        for (int i3 = i; i3 < i + i2; i3 += 2) {
            byte b2 = bArr[i3];
            bArr[i3] = bArr[i3 + 1];
            bArr[i3 + 1] = b2;
        }
    }

    public static final byte[] getOEMStringAsByteArray(String str) throws UnsupportedEncodingException {
        return str.getBytes("ASCII");
    }

    public static final byte[] getUTFStringAsByteArray(String str) throws UnsupportedEncodingException {
        return str.getBytes("UTF-16LE");
    }

    public static final byte[] encodeString(String str, boolean z) throws UnsupportedEncodingException {
        if (z) {
            return getUTFStringAsByteArray(str);
        }
        return getOEMStringAsByteArray(str);
    }

    public static String asHex(byte[] bArr) {
        return asHex(bArr, null);
    }

    public static String asHex(byte[] bArr, String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bArr.length; i++) {
            String hexString = Integer.toHexString(bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
            if ((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) < 16) {
                sb.append('0');
            }
            sb.append(hexString);
            if (str != null && i < bArr.length - 1) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public static byte[] asByteArray(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = (byte) Integer.parseInt(str.substring(i * 2, (i * 2) + 2), 16);
        }
        return bArr;
    }

    public static final int makeIntFromByte4(byte[] bArr) {
        return makeIntFromByte4(bArr, 0);
    }

    public static final int makeIntFromByte4(byte[] bArr, int i) {
        return (bArr[i] << 24) | ((bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[i + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    public static final int makeIntFromByte2(byte[] bArr) {
        return makeIntFromByte2(bArr, 0);
    }

    public static final int makeIntFromByte2(byte[] bArr, int i) {
        return ((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
    }

    public static final boolean isFlagSet(int i, int i2) {
        return (i & i2) > 0;
    }
}
