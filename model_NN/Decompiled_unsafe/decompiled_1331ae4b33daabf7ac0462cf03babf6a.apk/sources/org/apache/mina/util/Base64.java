package org.apache.mina.util;

import com.igexin.download.Downloads;
import java.security.InvalidParameterException;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class Base64 {
    static final int BASELENGTH = 255;
    static final byte[] CHUNK_SEPARATOR = HttpProxyConstants.CRLF.getBytes();
    static final int CHUNK_SIZE = 76;
    static final int EIGHTBIT = 8;
    static final int FOURBYTE = 4;
    static final int LOOKUPLENGTH = 64;
    static final byte PAD = 61;
    static final int SIGN = -128;
    static final int SIXTEENBIT = 16;
    static final int TWENTYFOURBITGROUP = 24;
    private static byte[] base64Alphabet = new byte[255];
    private static byte[] lookUpBase64Alphabet = new byte[64];

    static {
        int i = 0;
        for (int i2 = 0; i2 < 255; i2++) {
            base64Alphabet[i2] = -1;
        }
        for (int i3 = 90; i3 >= 65; i3--) {
            base64Alphabet[i3] = (byte) (i3 - 65);
        }
        for (int i4 = 122; i4 >= 97; i4--) {
            base64Alphabet[i4] = (byte) ((i4 - 97) + 26);
        }
        for (int i5 = 57; i5 >= 48; i5--) {
            base64Alphabet[i5] = (byte) ((i5 - 48) + 52);
        }
        base64Alphabet[43] = 62;
        base64Alphabet[47] = 63;
        for (int i6 = 0; i6 <= 25; i6++) {
            lookUpBase64Alphabet[i6] = (byte) (i6 + 65);
        }
        int i7 = 26;
        int i8 = 0;
        while (i7 <= 51) {
            lookUpBase64Alphabet[i7] = (byte) (i8 + 97);
            i7++;
            i8++;
        }
        int i9 = 52;
        while (i9 <= 61) {
            lookUpBase64Alphabet[i9] = (byte) (i + 48);
            i9++;
            i++;
        }
        lookUpBase64Alphabet[62] = 43;
        lookUpBase64Alphabet[63] = 47;
    }

    private static boolean isBase64(byte b2) {
        if (b2 != 61 && base64Alphabet[b2] == -1) {
            return false;
        }
        return true;
    }

    public static boolean isArrayByteBase64(byte[] bArr) {
        if (r4 == 0) {
            return true;
        }
        for (byte isBase64 : discardWhitespace(bArr)) {
            if (!isBase64(isBase64)) {
                return false;
            }
        }
        return true;
    }

    public static byte[] encodeBase64(byte[] bArr) {
        return encodeBase64(bArr, false);
    }

    public static byte[] encodeBase64Chunked(byte[] bArr) {
        return encodeBase64(bArr, true);
    }

    public Object decode(Object obj) {
        if (obj instanceof byte[]) {
            return decode((byte[]) obj);
        }
        throw new InvalidParameterException("Parameter supplied to Base64 decode is not a byte[]");
    }

    public byte[] decode(byte[] bArr) {
        return decodeBase64(bArr);
    }

    public static byte[] encodeBase64(byte[] bArr, boolean z) {
        int i;
        byte b2;
        int i2;
        int i3;
        int i4;
        int length = bArr.length * 8;
        int i5 = length % 24;
        int i6 = length / 24;
        int i7 = 0;
        if (i5 != 0) {
            i = (i6 + 1) * 4;
        } else {
            i = i6 * 4;
        }
        if (z) {
            if (CHUNK_SEPARATOR.length == 0) {
                i7 = 0;
            } else {
                i7 = (int) Math.ceil((double) (((float) i) / 76.0f));
            }
            i += CHUNK_SEPARATOR.length * i7;
        }
        byte[] bArr2 = new byte[i];
        int i8 = 76;
        int i9 = 0;
        int i10 = 0;
        int i11 = 0;
        while (i10 < i6) {
            int i12 = i10 * 3;
            byte b3 = bArr[i12];
            byte b4 = bArr[i12 + 1];
            byte b5 = bArr[i12 + 2];
            byte b6 = (byte) (b4 & 15);
            byte b7 = (byte) (b3 & 3);
            byte b8 = (b3 & Byte.MIN_VALUE) == 0 ? (byte) (b3 >> 2) : (byte) ((b3 >> 2) ^ Downloads.STATUS_RUNNING);
            byte b9 = (b4 & Byte.MIN_VALUE) == 0 ? (byte) (b4 >> 4) : (byte) ((b4 >> 4) ^ 240);
            if ((b5 & Byte.MIN_VALUE) == 0) {
                b2 = (byte) (b5 >> 6);
            } else {
                b2 = (byte) ((b5 >> 6) ^ 252);
            }
            bArr2[i11] = lookUpBase64Alphabet[b8];
            bArr2[i11 + 1] = lookUpBase64Alphabet[b9 | (b7 << 4)];
            bArr2[i11 + 2] = lookUpBase64Alphabet[b2 | (b6 << 2)];
            bArr2[i11 + 3] = lookUpBase64Alphabet[b5 & 63];
            int i13 = i11 + 4;
            if (!z || i13 != i8) {
                i2 = i9;
                i3 = i8;
                i4 = i13;
            } else {
                System.arraycopy(CHUNK_SEPARATOR, 0, bArr2, i13, CHUNK_SEPARATOR.length);
                i2 = i9 + 1;
                i3 = ((i2 + 1) * 76) + (CHUNK_SEPARATOR.length * i2);
                i4 = CHUNK_SEPARATOR.length + i13;
            }
            i10++;
            i11 = i4;
            i8 = i3;
            i9 = i2;
        }
        int i14 = i10 * 3;
        if (i5 == 8) {
            byte b10 = bArr[i14];
            byte b11 = (byte) (b10 & 3);
            bArr2[i11] = lookUpBase64Alphabet[(b10 & Byte.MIN_VALUE) == 0 ? (byte) (b10 >> 2) : (byte) ((b10 >> 2) ^ Downloads.STATUS_RUNNING)];
            bArr2[i11 + 1] = lookUpBase64Alphabet[b11 << 4];
            bArr2[i11 + 2] = PAD;
            bArr2[i11 + 3] = PAD;
        } else if (i5 == 16) {
            byte b12 = bArr[i14];
            byte b13 = bArr[i14 + 1];
            byte b14 = (byte) (b13 & 15);
            byte b15 = (byte) (b12 & 3);
            byte b16 = (b12 & Byte.MIN_VALUE) == 0 ? (byte) (b12 >> 2) : (byte) ((b12 >> 2) ^ Downloads.STATUS_RUNNING);
            byte b17 = (b13 & Byte.MIN_VALUE) == 0 ? (byte) (b13 >> 4) : (byte) ((b13 >> 4) ^ 240);
            bArr2[i11] = lookUpBase64Alphabet[b16];
            bArr2[i11 + 1] = lookUpBase64Alphabet[b17 | (b15 << 4)];
            bArr2[i11 + 2] = lookUpBase64Alphabet[b14 << 2];
            bArr2[i11 + 3] = PAD;
        }
        if (z && i9 < i7) {
            System.arraycopy(CHUNK_SEPARATOR, 0, bArr2, i - CHUNK_SEPARATOR.length, CHUNK_SEPARATOR.length);
        }
        return bArr2;
    }

    public static byte[] decodeBase64(byte[] bArr) {
        byte[] discardNonBase64 = discardNonBase64(bArr);
        if (discardNonBase64.length == 0) {
            return new byte[0];
        }
        int length = discardNonBase64.length / 4;
        int length2 = discardNonBase64.length;
        while (discardNonBase64[length2 - 1] == 61) {
            length2--;
            if (length2 == 0) {
                return new byte[0];
            }
        }
        byte[] bArr2 = new byte[(length2 - length)];
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i2 * 4;
            byte b2 = discardNonBase64[i3 + 2];
            byte b3 = discardNonBase64[i3 + 3];
            byte b4 = base64Alphabet[discardNonBase64[i3]];
            byte b5 = base64Alphabet[discardNonBase64[i3 + 1]];
            if (b2 != 61 && b3 != 61) {
                byte b6 = base64Alphabet[b2];
                byte b7 = base64Alphabet[b3];
                bArr2[i] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr2[i + 1] = (byte) (((b5 & 15) << 4) | ((b6 >> 2) & 15));
                bArr2[i + 2] = (byte) ((b6 << 6) | b7);
            } else if (b2 == 61) {
                bArr2[i] = (byte) ((b5 >> 4) | (b4 << 2));
            } else if (b3 == 61) {
                byte b8 = base64Alphabet[b2];
                bArr2[i] = (byte) ((b4 << 2) | (b5 >> 4));
                bArr2[i + 1] = (byte) (((b5 & 15) << 4) | ((b8 >> 2) & 15));
            }
            i += 3;
        }
        return bArr2;
    }

    static byte[] discardWhitespace(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            switch (bArr[i2]) {
                case 9:
                case 10:
                case 13:
                case 32:
                    break;
                default:
                    bArr2[i] = bArr[i2];
                    i++;
                    break;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        return bArr3;
    }

    static byte[] discardNonBase64(byte[] bArr) {
        byte[] bArr2 = new byte[bArr.length];
        int i = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            if (isBase64(bArr[i2])) {
                bArr2[i] = bArr[i2];
                i++;
            }
        }
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr2, 0, bArr3, 0, i);
        return bArr3;
    }

    public Object encode(Object obj) {
        if (obj instanceof byte[]) {
            return encode((byte[]) obj);
        }
        throw new InvalidParameterException("Parameter supplied to Base64 encode is not a byte[]");
    }

    public byte[] encode(byte[] bArr) {
        return encodeBase64(bArr, false);
    }
}
