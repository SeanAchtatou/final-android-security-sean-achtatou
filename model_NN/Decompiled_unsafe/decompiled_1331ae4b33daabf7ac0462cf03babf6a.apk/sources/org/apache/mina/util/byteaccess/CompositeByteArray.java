package org.apache.mina.util.byteaccess;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Collections;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.util.byteaccess.ByteArray;
import org.apache.mina.util.byteaccess.ByteArrayList;

public final class CompositeByteArray extends AbstractByteArray {
    /* access modifiers changed from: private */
    public final ByteArrayList bas;
    /* access modifiers changed from: private */
    public final ByteArrayFactory byteArrayFactory;
    /* access modifiers changed from: private */
    public ByteOrder order;

    public interface CursorListener {
        void enteredFirstComponent(int i, ByteArray byteArray);

        void enteredLastComponent(int i, ByteArray byteArray);

        void enteredNextComponent(int i, ByteArray byteArray);

        void enteredPreviousComponent(int i, ByteArray byteArray);
    }

    public CompositeByteArray() {
        this(null);
    }

    public CompositeByteArray(ByteArrayFactory byteArrayFactory2) {
        this.bas = new ByteArrayList();
        this.byteArrayFactory = byteArrayFactory2;
    }

    public ByteArray getFirst() {
        if (this.bas.isEmpty()) {
            return null;
        }
        return this.bas.getFirst().getByteArray();
    }

    public void addFirst(ByteArray byteArray) {
        addHook(byteArray);
        this.bas.addFirst(byteArray);
    }

    public ByteArray removeFirst() {
        ByteArrayList.Node removeFirst = this.bas.removeFirst();
        if (removeFirst == null) {
            return null;
        }
        return removeFirst.getByteArray();
    }

    public ByteArray removeTo(int i) {
        if (i < first() || i > last()) {
            throw new IndexOutOfBoundsException();
        }
        CompositeByteArray compositeByteArray = new CompositeByteArray(this.byteArrayFactory);
        int first = i - first();
        while (first > 0) {
            final ByteArray removeFirst = removeFirst();
            if (removeFirst.last() <= first) {
                compositeByteArray.addLast(removeFirst);
                first -= removeFirst.last();
            } else {
                IoBuffer singleIoBuffer = removeFirst.getSingleIoBuffer();
                int limit = singleIoBuffer.limit();
                singleIoBuffer.position(0);
                singleIoBuffer.limit(first);
                IoBuffer slice = singleIoBuffer.slice();
                singleIoBuffer.position(first);
                singleIoBuffer.limit(limit);
                IoBuffer slice2 = singleIoBuffer.slice();
                AnonymousClass1 r4 = new BufferByteArray(slice) {
                    public void free() {
                    }
                };
                compositeByteArray.addLast(r4);
                first -= r4.last();
                addFirst(new BufferByteArray(slice2) {
                    public void free() {
                        removeFirst.free();
                    }
                });
            }
        }
        return compositeByteArray;
    }

    public void addLast(ByteArray byteArray) {
        addHook(byteArray);
        this.bas.addLast(byteArray);
    }

    public ByteArray removeLast() {
        ByteArrayList.Node removeLast = this.bas.removeLast();
        if (removeLast == null) {
            return null;
        }
        return removeLast.getByteArray();
    }

    public void free() {
        while (!this.bas.isEmpty()) {
            this.bas.getLast().getByteArray().free();
            this.bas.removeLast();
        }
    }

    /* access modifiers changed from: private */
    public void checkBounds(int i, int i2) {
        int i3 = i + i2;
        if (i < first()) {
            throw new IndexOutOfBoundsException("Index " + i + " less than start " + first() + ".");
        } else if (i3 > last()) {
            throw new IndexOutOfBoundsException("Index " + i3 + " greater than length " + last() + ".");
        }
    }

    public Iterable<IoBuffer> getIoBuffers() {
        if (this.bas.isEmpty()) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        ByteArrayList.Node first = this.bas.getFirst();
        for (IoBuffer add : first.getByteArray().getIoBuffers()) {
            arrayList.add(add);
        }
        while (true) {
            ByteArrayList.Node node = first;
            if (!node.hasNextNode()) {
                return arrayList;
            }
            first = node.getNextNode();
            for (IoBuffer add2 : first.getByteArray().getIoBuffers()) {
                arrayList.add(add2);
            }
        }
    }

    public IoBuffer getSingleIoBuffer() {
        if (this.byteArrayFactory == null) {
            throw new IllegalStateException("Can't get single buffer from CompositeByteArray unless it has a ByteArrayFactory.");
        } else if (this.bas.isEmpty()) {
            return this.byteArrayFactory.create(1).getSingleIoBuffer();
        } else {
            int last = last() - first();
            ByteArray byteArray = this.bas.getFirst().getByteArray();
            if (byteArray.last() == last) {
                return byteArray.getSingleIoBuffer();
            }
            ByteArray create = this.byteArrayFactory.create(last);
            IoBuffer singleIoBuffer = create.getSingleIoBuffer();
            cursor().put(singleIoBuffer);
            while (!this.bas.isEmpty()) {
                ByteArray byteArray2 = this.bas.getLast().getByteArray();
                this.bas.removeLast();
                byteArray2.free();
            }
            this.bas.addLast(create);
            return singleIoBuffer;
        }
    }

    public ByteArray.Cursor cursor() {
        return new CursorImpl(this);
    }

    public ByteArray.Cursor cursor(int i) {
        return new CursorImpl(this, i);
    }

    public ByteArray.Cursor cursor(CursorListener cursorListener) {
        return new CursorImpl(this, cursorListener);
    }

    public ByteArray.Cursor cursor(int i, CursorListener cursorListener) {
        return new CursorImpl(i, cursorListener);
    }

    public ByteArray slice(int i, int i2) {
        return cursor(i).slice(i2);
    }

    public byte get(int i) {
        return cursor(i).get();
    }

    public void put(int i, byte b2) {
        cursor(i).put(b2);
    }

    public void get(int i, IoBuffer ioBuffer) {
        cursor(i).get(ioBuffer);
    }

    public void put(int i, IoBuffer ioBuffer) {
        cursor(i).put(ioBuffer);
    }

    public int first() {
        return this.bas.firstByte();
    }

    public int last() {
        return this.bas.lastByte();
    }

    private void addHook(ByteArray byteArray) {
        if (byteArray.first() != 0) {
            throw new IllegalArgumentException("Cannot add byte array that doesn't start from 0: " + byteArray.first());
        } else if (this.order == null) {
            this.order = byteArray.order();
        } else if (!this.order.equals(byteArray.order())) {
            throw new IllegalArgumentException("Cannot add byte array with different byte order: " + byteArray.order());
        }
    }

    public ByteOrder order() {
        if (this.order != null) {
            return this.order;
        }
        throw new IllegalStateException("Byte order not yet set.");
    }

    public void order(ByteOrder byteOrder) {
        if (byteOrder == null || !byteOrder.equals(this.order)) {
            this.order = byteOrder;
            if (!this.bas.isEmpty()) {
                for (ByteArrayList.Node first = this.bas.getFirst(); first.hasNextNode(); first = first.getNextNode()) {
                    first.getByteArray().order(byteOrder);
                }
            }
        }
    }

    public short getShort(int i) {
        return cursor(i).getShort();
    }

    public void putShort(int i, short s) {
        cursor(i).putShort(s);
    }

    public int getInt(int i) {
        return cursor(i).getInt();
    }

    public void putInt(int i, int i2) {
        cursor(i).putInt(i2);
    }

    public long getLong(int i) {
        return cursor(i).getLong();
    }

    public void putLong(int i, long j) {
        cursor(i).putLong(j);
    }

    public float getFloat(int i) {
        return cursor(i).getFloat();
    }

    public void putFloat(int i, float f) {
        cursor(i).putFloat(f);
    }

    public double getDouble(int i) {
        return cursor(i).getDouble();
    }

    public void putDouble(int i, double d) {
        cursor(i).putDouble(d);
    }

    public char getChar(int i) {
        return cursor(i).getChar();
    }

    public void putChar(int i, char c) {
        cursor(i).putChar(c);
    }

    private class CursorImpl implements ByteArray.Cursor {
        private ByteArray.Cursor componentCursor;
        private int componentIndex;
        private ByteArrayList.Node componentNode;
        private int index;
        private final CursorListener listener;

        public CursorImpl(CompositeByteArray compositeByteArray) {
            this(0, null);
        }

        public CursorImpl(CompositeByteArray compositeByteArray, int i) {
            this(i, null);
        }

        public CursorImpl(CompositeByteArray compositeByteArray, CursorListener cursorListener) {
            this(0, cursorListener);
        }

        public CursorImpl(int i, CursorListener cursorListener) {
            this.index = i;
            this.listener = cursorListener;
        }

        public int getIndex() {
            return this.index;
        }

        public void setIndex(int i) {
            CompositeByteArray.this.checkBounds(i, 0);
            this.index = i;
        }

        public void skip(int i) {
            setIndex(this.index + i);
        }

        public ByteArray slice(int i) {
            CompositeByteArray compositeByteArray = new CompositeByteArray(CompositeByteArray.this.byteArrayFactory);
            while (i > 0) {
                prepareForAccess(i);
                int min = Math.min(i, this.componentCursor.getRemaining());
                compositeByteArray.addLast(this.componentCursor.slice(min));
                this.index += min;
                i -= min;
            }
            return compositeByteArray;
        }

        public ByteOrder order() {
            return CompositeByteArray.this.order();
        }

        private void prepareForAccess(int i) {
            if (this.componentNode != null && this.componentNode.isRemoved()) {
                this.componentNode = null;
                this.componentCursor = null;
            }
            CompositeByteArray.this.checkBounds(this.index, i);
            ByteArrayList.Node node = this.componentNode;
            if (this.componentNode == null) {
                if (this.index <= ((CompositeByteArray.this.last() - CompositeByteArray.this.first()) / 2) + CompositeByteArray.this.first()) {
                    this.componentNode = CompositeByteArray.this.bas.getFirst();
                    this.componentIndex = CompositeByteArray.this.first();
                    if (this.listener != null) {
                        this.listener.enteredFirstComponent(this.componentIndex, this.componentNode.getByteArray());
                    }
                } else {
                    this.componentNode = CompositeByteArray.this.bas.getLast();
                    this.componentIndex = CompositeByteArray.this.last() - this.componentNode.getByteArray().last();
                    if (this.listener != null) {
                        this.listener.enteredLastComponent(this.componentIndex, this.componentNode.getByteArray());
                    }
                }
            }
            while (this.index < this.componentIndex) {
                this.componentNode = this.componentNode.getPreviousNode();
                this.componentIndex -= this.componentNode.getByteArray().last();
                if (this.listener != null) {
                    this.listener.enteredPreviousComponent(this.componentIndex, this.componentNode.getByteArray());
                }
            }
            while (this.index >= this.componentIndex + this.componentNode.getByteArray().length()) {
                this.componentIndex += this.componentNode.getByteArray().last();
                this.componentNode = this.componentNode.getNextNode();
                if (this.listener != null) {
                    this.listener.enteredNextComponent(this.componentIndex, this.componentNode.getByteArray());
                }
            }
            int i2 = this.index - this.componentIndex;
            if (this.componentNode == node) {
                this.componentCursor.setIndex(i2);
            } else {
                this.componentCursor = this.componentNode.getByteArray().cursor(i2);
            }
        }

        public int getRemaining() {
            return (CompositeByteArray.this.last() - this.index) + 1;
        }

        public boolean hasRemaining() {
            return getRemaining() > 0;
        }

        public byte get() {
            prepareForAccess(1);
            byte b2 = this.componentCursor.get();
            this.index++;
            return b2;
        }

        public void put(byte b2) {
            prepareForAccess(1);
            this.componentCursor.put(b2);
            this.index++;
        }

        public void get(IoBuffer ioBuffer) {
            while (ioBuffer.hasRemaining()) {
                int remaining = ioBuffer.remaining();
                prepareForAccess(remaining);
                this.componentCursor.get(ioBuffer);
                this.index = (remaining - ioBuffer.remaining()) + this.index;
            }
        }

        public void put(IoBuffer ioBuffer) {
            while (ioBuffer.hasRemaining()) {
                int remaining = ioBuffer.remaining();
                prepareForAccess(remaining);
                this.componentCursor.put(ioBuffer);
                this.index = (remaining - ioBuffer.remaining()) + this.index;
            }
        }

        public short getShort() {
            prepareForAccess(2);
            if (this.componentCursor.getRemaining() >= 4) {
                short s = this.componentCursor.getShort();
                this.index += 2;
                return s;
            }
            byte b2 = get();
            byte b3 = get();
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                return (short) ((b2 << 8) | (b3 << 0));
            }
            return (short) ((b2 << 0) | (b3 << 8));
        }

        public void putShort(short s) {
            byte b2;
            byte b3;
            prepareForAccess(2);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putShort(s);
                this.index += 2;
                return;
            }
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                b2 = (byte) ((s >> 8) & 255);
                b3 = (byte) ((s >> 0) & 255);
            } else {
                b2 = (byte) ((s >> 0) & 255);
                b3 = (byte) ((s >> 8) & 255);
            }
            put(b2);
            put(b3);
        }

        public int getInt() {
            prepareForAccess(4);
            if (this.componentCursor.getRemaining() >= 4) {
                int i = this.componentCursor.getInt();
                this.index += 4;
                return i;
            }
            byte b2 = get();
            byte b3 = get();
            byte b4 = get();
            byte b5 = get();
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                return (b2 << 24) | (b3 << 16) | (b4 << 8) | (b5 << 0);
            }
            return (b2 << 0) | (b3 << 8) | (b4 << 16) | (b5 << 24);
        }

        public void putInt(int i) {
            byte b2;
            byte b3;
            byte b4;
            byte b5;
            prepareForAccess(4);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putInt(i);
                this.index += 4;
                return;
            }
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                b2 = (byte) ((i >> 24) & 255);
                b3 = (byte) ((i >> 16) & 255);
                b4 = (byte) ((i >> 8) & 255);
                b5 = (byte) ((i >> 0) & 255);
            } else {
                b2 = (byte) ((i >> 0) & 255);
                b3 = (byte) ((i >> 8) & 255);
                b4 = (byte) ((i >> 16) & 255);
                b5 = (byte) ((i >> 24) & 255);
            }
            put(b2);
            put(b3);
            put(b4);
            put(b5);
        }

        public long getLong() {
            prepareForAccess(8);
            if (this.componentCursor.getRemaining() >= 4) {
                long j = this.componentCursor.getLong();
                this.index += 8;
                return j;
            }
            byte b2 = get();
            byte b3 = get();
            byte b4 = get();
            byte b5 = get();
            byte b6 = get();
            byte b7 = get();
            byte b8 = get();
            byte b9 = get();
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                return ((((long) b3) & 255) << 48) | ((((long) b2) & 255) << 56) | ((((long) b4) & 255) << 40) | ((((long) b5) & 255) << 32) | ((((long) b6) & 255) << 24) | ((((long) b7) & 255) << 16) | ((((long) b8) & 255) << 8) | ((((long) b9) & 255) << 0);
            }
            return ((((long) b2) & 255) << 0) | ((((long) b4) & 255) << 16) | ((((long) b6) & 255) << 32) | ((((long) b8) & 255) << 48) | ((((long) b9) & 255) << 56) | ((((long) b7) & 255) << 40) | ((((long) b5) & 255) << 24) | ((((long) b3) & 255) << 8);
        }

        public void putLong(long j) {
            byte b2;
            byte b3;
            byte b4;
            byte b5;
            byte b6;
            byte b7;
            byte b8;
            byte b9;
            prepareForAccess(8);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putLong(j);
                this.index += 8;
                return;
            }
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                b2 = (byte) ((int) ((j >> 56) & 255));
                b3 = (byte) ((int) ((j >> 48) & 255));
                b4 = (byte) ((int) ((j >> 40) & 255));
                b5 = (byte) ((int) ((j >> 32) & 255));
                b6 = (byte) ((int) ((j >> 24) & 255));
                b7 = (byte) ((int) ((j >> 16) & 255));
                b8 = (byte) ((int) ((j >> 8) & 255));
                b9 = (byte) ((int) ((j >> 0) & 255));
            } else {
                b2 = (byte) ((int) ((j >> 0) & 255));
                b3 = (byte) ((int) ((j >> 8) & 255));
                b4 = (byte) ((int) ((j >> 16) & 255));
                b5 = (byte) ((int) ((j >> 24) & 255));
                b6 = (byte) ((int) ((j >> 32) & 255));
                b7 = (byte) ((int) ((j >> 40) & 255));
                b8 = (byte) ((int) ((j >> 48) & 255));
                b9 = (byte) ((int) ((j >> 56) & 255));
            }
            put(b2);
            put(b3);
            put(b4);
            put(b5);
            put(b6);
            put(b7);
            put(b8);
            put(b9);
        }

        public float getFloat() {
            prepareForAccess(4);
            if (this.componentCursor.getRemaining() < 4) {
                return Float.intBitsToFloat(getInt());
            }
            float f = this.componentCursor.getFloat();
            this.index += 4;
            return f;
        }

        public void putFloat(float f) {
            prepareForAccess(4);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putFloat(f);
                this.index += 4;
                return;
            }
            putInt(Float.floatToIntBits(f));
        }

        public double getDouble() {
            prepareForAccess(8);
            if (this.componentCursor.getRemaining() < 4) {
                return Double.longBitsToDouble(getLong());
            }
            double d = this.componentCursor.getDouble();
            this.index += 8;
            return d;
        }

        public void putDouble(double d) {
            prepareForAccess(8);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putDouble(d);
                this.index += 8;
                return;
            }
            putLong(Double.doubleToLongBits(d));
        }

        public char getChar() {
            prepareForAccess(2);
            if (this.componentCursor.getRemaining() >= 4) {
                char c = this.componentCursor.getChar();
                this.index += 2;
                return c;
            }
            byte b2 = get();
            byte b3 = get();
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                return (char) ((b2 << 8) | (b3 << 0));
            }
            return (char) ((b2 << 0) | (b3 << 8));
        }

        public void putChar(char c) {
            byte b2;
            byte b3;
            prepareForAccess(2);
            if (this.componentCursor.getRemaining() >= 4) {
                this.componentCursor.putChar(c);
                this.index += 2;
                return;
            }
            if (CompositeByteArray.this.order.equals(ByteOrder.BIG_ENDIAN)) {
                b2 = (byte) ((c >> 8) & 255);
                b3 = (byte) ((c >> 0) & 255);
            } else {
                b2 = (byte) ((c >> 0) & 255);
                b3 = (byte) ((c >> 8) & 255);
            }
            put(b2);
            put(b3);
        }
    }
}
