package org.apache.mina.filter.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.AttributeKey;
import org.apache.mina.core.session.IoSession;

public abstract class CumulativeProtocolDecoder extends ProtocolDecoderAdapter {
    private final AttributeKey BUFFER = new AttributeKey(getClass(), "buffer");

    /* access modifiers changed from: protected */
    public abstract boolean doDecode(IoSession ioSession, IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    protected CumulativeProtocolDecoder() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x000c A[LOOP:0: B:2:0x000c->B:5:0x0016, LOOP_START] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void decode(org.apache.mina.core.session.IoSession r5, org.apache.mina.core.buffer.IoBuffer r6, org.apache.mina.filter.codec.ProtocolDecoderOutput r7) throws java.lang.Exception {
        /*
            r4 = this;
            r1 = 0
            r2 = 1
            org.apache.mina.core.service.TransportMetadata r0 = r5.getTransportMetadata()
            boolean r0 = r0.hasFragmentation()
            if (r0 != 0) goto L_0x0019
        L_0x000c:
            boolean r0 = r6.hasRemaining()
            if (r0 == 0) goto L_0x0018
            boolean r0 = r4.doDecode(r5, r6, r7)
            if (r0 != 0) goto L_0x000c
        L_0x0018:
            return
        L_0x0019:
            org.apache.mina.core.session.AttributeKey r0 = r4.BUFFER
            java.lang.Object r0 = r5.getAttribute(r0)
            org.apache.mina.core.buffer.IoBuffer r0 = (org.apache.mina.core.buffer.IoBuffer) r0
            if (r0 == 0) goto L_0x0076
            boolean r3 = r0.isAutoExpand()
            if (r3 == 0) goto L_0x002d
            r0.put(r6)     // Catch:{ IllegalStateException -> 0x009b, IndexOutOfBoundsException -> 0x009d }
            r1 = r2
        L_0x002d:
            if (r1 == 0) goto L_0x004b
            r0.flip()
        L_0x0032:
            r1 = r2
        L_0x0033:
            int r2 = r0.position()
            boolean r3 = r4.doDecode(r5, r0, r7)
            if (r3 == 0) goto L_0x007e
            int r3 = r0.position()
            if (r3 != r2) goto L_0x0078
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "doDecode() can't return true when buffer is not consumed."
            r0.<init>(r1)
            throw r0
        L_0x004b:
            r0.flip()
            int r1 = r0.remaining()
            int r3 = r6.remaining()
            int r1 = r1 + r3
            org.apache.mina.core.buffer.IoBuffer r1 = org.apache.mina.core.buffer.IoBuffer.allocate(r1)
            org.apache.mina.core.buffer.IoBuffer r1 = r1.setAutoExpand(r2)
            java.nio.ByteOrder r3 = r0.order()
            r1.order(r3)
            r1.put(r0)
            r1.put(r6)
            r1.flip()
            org.apache.mina.core.session.AttributeKey r0 = r4.BUFFER
            r5.setAttribute(r0, r1)
            r0 = r1
            goto L_0x0032
        L_0x0076:
            r0 = r6
            goto L_0x0033
        L_0x0078:
            boolean r2 = r0.hasRemaining()
            if (r2 != 0) goto L_0x0033
        L_0x007e:
            boolean r2 = r0.hasRemaining()
            if (r2 == 0) goto L_0x0094
            if (r1 == 0) goto L_0x0090
            boolean r1 = r0.isAutoExpand()
            if (r1 == 0) goto L_0x0090
            r0.compact()
            goto L_0x0018
        L_0x0090:
            r4.storeRemainingInSession(r0, r5)
            goto L_0x0018
        L_0x0094:
            if (r1 == 0) goto L_0x0018
            r4.removeSessionBuffer(r5)
            goto L_0x0018
        L_0x009b:
            r3 = move-exception
            goto L_0x002d
        L_0x009d:
            r3 = move-exception
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.mina.filter.codec.CumulativeProtocolDecoder.decode(org.apache.mina.core.session.IoSession, org.apache.mina.core.buffer.IoBuffer, org.apache.mina.filter.codec.ProtocolDecoderOutput):void");
    }

    public void dispose(IoSession ioSession) throws Exception {
        removeSessionBuffer(ioSession);
    }

    private void removeSessionBuffer(IoSession ioSession) {
        ioSession.removeAttribute(this.BUFFER);
    }

    private void storeRemainingInSession(IoBuffer ioBuffer, IoSession ioSession) {
        IoBuffer autoExpand = IoBuffer.allocate(ioBuffer.capacity()).setAutoExpand(true);
        autoExpand.order(ioBuffer.order());
        autoExpand.put(ioBuffer);
        ioSession.setAttribute(this.BUFFER, autoExpand);
    }
}
