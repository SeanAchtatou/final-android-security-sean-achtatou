package org.apache.mina.filter.codec.textline;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class LineDelimiter {
    public static final LineDelimiter AUTO = new LineDelimiter("");
    public static final LineDelimiter CRLF = new LineDelimiter(HttpProxyConstants.CRLF);
    public static final LineDelimiter DEFAULT;
    public static final LineDelimiter MAC = new LineDelimiter("\r");
    public static final LineDelimiter NUL = new LineDelimiter("\u0000");
    public static final LineDelimiter UNIX = new LineDelimiter("\n");
    public static final LineDelimiter WINDOWS = CRLF;
    private final String value;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void}
     arg types: [java.io.ByteArrayOutputStream, int]
     candidates:
      ClspMth{java.io.PrintWriter.<init>(java.io.File, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.lang.String, java.lang.String):void throws java.io.FileNotFoundException, java.io.UnsupportedEncodingException}
      ClspMth{java.io.PrintWriter.<init>(java.io.Writer, boolean):void}
      ClspMth{java.io.PrintWriter.<init>(java.io.OutputStream, boolean):void} */
    static {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        new PrintWriter((OutputStream) byteArrayOutputStream, true).println();
        DEFAULT = new LineDelimiter(new String(byteArrayOutputStream.toByteArray()));
    }

    public LineDelimiter(String str) {
        if (str == null) {
            throw new IllegalArgumentException("delimiter");
        }
        this.value = str;
    }

    public String getValue() {
        return this.value;
    }

    public int hashCode() {
        return this.value.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LineDelimiter)) {
            return false;
        }
        return this.value.equals(((LineDelimiter) obj).value);
    }

    public String toString() {
        if (this.value.length() == 0) {
            return "delimiter: auto";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("delimiter:");
        for (int i = 0; i < this.value.length(); i++) {
            sb.append(" 0x");
            sb.append(Integer.toHexString(this.value.charAt(i)));
        }
        return sb.toString();
    }
}
