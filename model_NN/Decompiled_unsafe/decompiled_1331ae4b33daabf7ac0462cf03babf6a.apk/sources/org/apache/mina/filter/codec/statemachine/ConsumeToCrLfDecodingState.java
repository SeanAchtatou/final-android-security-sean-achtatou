package org.apache.mina.filter.codec.statemachine;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public abstract class ConsumeToCrLfDecodingState implements DecodingState {
    private static final byte CR = 13;
    private static final byte LF = 10;
    private IoBuffer buffer;
    private boolean lastIsCR;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    public DecodingState decode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        int position = ioBuffer.position();
        int limit = ioBuffer.limit();
        int i = -1;
        int i2 = position;
        while (true) {
            if (i2 >= limit) {
                break;
            }
            byte b2 = ioBuffer.get(i2);
            if (b2 != 13) {
                if (b2 == 10 && this.lastIsCR) {
                    i = i2;
                    break;
                }
                this.lastIsCR = false;
            } else {
                this.lastIsCR = true;
            }
            i2++;
        }
        if (i >= 0) {
            int i3 = i - 1;
            if (position < i3) {
                ioBuffer.limit(i3);
                if (this.buffer == null) {
                    flip = ioBuffer.slice();
                } else {
                    this.buffer.put(ioBuffer);
                    flip = this.buffer.flip();
                    this.buffer = null;
                }
                ioBuffer.limit(limit);
            } else if (this.buffer == null) {
                flip = IoBuffer.allocate(0);
            } else {
                flip = this.buffer.flip();
                this.buffer = null;
            }
            ioBuffer.position(i + 1);
            return finishDecode(flip, protocolDecoderOutput);
        }
        ioBuffer.position(position);
        if (this.buffer == null) {
            this.buffer = IoBuffer.allocate(ioBuffer.remaining());
            this.buffer.setAutoExpand(true);
        }
        this.buffer.put(ioBuffer);
        if (!this.lastIsCR) {
            return this;
        }
        this.buffer.position(this.buffer.position() - 1);
        return this;
    }

    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        if (this.buffer == null) {
            flip = IoBuffer.allocate(0);
        } else {
            flip = this.buffer.flip();
            this.buffer = null;
        }
        return finishDecode(flip, protocolDecoderOutput);
    }
}
