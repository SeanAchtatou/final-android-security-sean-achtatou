package org.apache.mina.filter.codec.statemachine;

public abstract class ConsumeToLinearWhitespaceDecodingState extends ConsumeToDynamicTerminatorDecodingState {
    /* access modifiers changed from: protected */
    public boolean isTerminator(byte b2) {
        return b2 == 32 || b2 == 9;
    }
}
