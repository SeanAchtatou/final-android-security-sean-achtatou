package org.apache.mina.filter.codec.statemachine;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

public abstract class ConsumeToTerminatorDecodingState implements DecodingState {
    private IoBuffer buffer;
    private final byte terminator;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    public ConsumeToTerminatorDecodingState(byte b2) {
        this.terminator = b2;
    }

    public DecodingState decode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        int indexOf = ioBuffer.indexOf(this.terminator);
        if (indexOf >= 0) {
            int limit = ioBuffer.limit();
            if (ioBuffer.position() < indexOf) {
                ioBuffer.limit(indexOf);
                if (this.buffer == null) {
                    flip = ioBuffer.slice();
                } else {
                    this.buffer.put(ioBuffer);
                    flip = this.buffer.flip();
                    this.buffer = null;
                }
                ioBuffer.limit(limit);
            } else if (this.buffer == null) {
                flip = IoBuffer.allocate(0);
            } else {
                flip = this.buffer.flip();
                this.buffer = null;
            }
            ioBuffer.position(indexOf + 1);
            return finishDecode(flip, protocolDecoderOutput);
        }
        if (this.buffer == null) {
            this.buffer = IoBuffer.allocate(ioBuffer.remaining());
            this.buffer.setAutoExpand(true);
        }
        this.buffer.put(ioBuffer);
        return this;
    }

    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        IoBuffer flip;
        if (this.buffer == null) {
            flip = IoBuffer.allocate(0);
        } else {
            flip = this.buffer.flip();
            this.buffer = null;
        }
        return finishDecode(flip, protocolDecoderOutput);
    }
}
