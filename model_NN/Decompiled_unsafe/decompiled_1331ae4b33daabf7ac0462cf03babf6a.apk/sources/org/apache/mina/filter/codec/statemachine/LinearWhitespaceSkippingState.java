package org.apache.mina.filter.codec.statemachine;

public abstract class LinearWhitespaceSkippingState extends SkippingState {
    /* access modifiers changed from: protected */
    public boolean canSkip(byte b2) {
        return b2 == 32 || b2 == 9;
    }
}
