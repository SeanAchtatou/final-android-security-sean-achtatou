package org.apache.mina.filter.codec.statemachine;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.filter.codec.ProtocolDecoderException;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public abstract class CrLfDecodingState implements DecodingState {
    private static final byte CR = 13;
    private static final byte LF = 10;
    private boolean hasCR;

    /* access modifiers changed from: protected */
    public abstract DecodingState finishDecode(boolean z, ProtocolDecoderOutput protocolDecoderOutput) throws Exception;

    public DecodingState decode(IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        boolean z;
        boolean z2 = true;
        while (true) {
            if (!ioBuffer.hasRemaining()) {
                z2 = false;
                z = false;
                break;
            }
            byte b2 = ioBuffer.get();
            if (!this.hasCR) {
                if (b2 == 13) {
                    this.hasCR = true;
                } else if (b2 == 10) {
                    z = true;
                } else {
                    ioBuffer.position(ioBuffer.position() - 1);
                    z = false;
                }
            } else if (b2 == 10) {
                z = true;
            } else {
                throw new ProtocolDecoderException("Expected LF after CR but was: " + ((int) (b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)));
            }
        }
        if (!z2) {
            return this;
        }
        this.hasCR = false;
        return finishDecode(z, protocolDecoderOutput);
    }

    public DecodingState finishDecode(ProtocolDecoderOutput protocolDecoderOutput) throws Exception {
        return finishDecode(false, protocolDecoderOutput);
    }
}
