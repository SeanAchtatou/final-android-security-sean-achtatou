package org.a.b;

import android.util.Log;
import org.a.a.b;
import org.a.a.c;

/* compiled from: AndroidLogger */
public class a extends b {
    a(String str) {
        this.f4197b = str;
    }

    public void a(String str) {
        Log.v(this.f4197b, str);
    }

    public void a(String str, Object obj) {
        Log.v(this.f4197b, c(str, obj, null));
    }

    public void a(String str, Throwable th) {
        Log.v(this.f4197b, str, th);
    }

    public boolean a() {
        return Log.isLoggable(this.f4197b, 3);
    }

    public void b(String str) {
        Log.d(this.f4197b, str);
    }

    public void b(String str, Object obj) {
        Log.d(this.f4197b, c(str, obj, null));
    }

    public void a(String str, Object obj, Object obj2) {
        Log.d(this.f4197b, c(str, obj, obj2));
    }

    public void b(String str, Throwable th) {
        Log.d(this.f4197b, str, th);
    }

    public void c(String str) {
        Log.i(this.f4197b, str);
    }

    public void c(String str, Object obj) {
        Log.i(this.f4197b, c(str, obj, null));
    }

    public void c(String str, Throwable th) {
        Log.i(this.f4197b, str, th);
    }

    public boolean b() {
        return Log.isLoggable(this.f4197b, 5);
    }

    public void d(String str) {
        Log.w(this.f4197b, str);
    }

    public void d(String str, Object obj) {
        Log.w(this.f4197b, c(str, obj, null));
    }

    public void b(String str, Object obj, Object obj2) {
        Log.w(this.f4197b, c(str, obj, obj2));
    }

    public void d(String str, Throwable th) {
        Log.w(this.f4197b, str, th);
    }

    public void e(String str) {
        Log.e(this.f4197b, str);
    }

    public void e(String str, Object obj) {
        Log.e(this.f4197b, c(str, obj, null));
    }

    public void e(String str, Throwable th) {
        Log.e(this.f4197b, str, th);
    }

    private String c(String str, Object obj, Object obj2) {
        return c.a(str, obj, obj2).a();
    }
}
