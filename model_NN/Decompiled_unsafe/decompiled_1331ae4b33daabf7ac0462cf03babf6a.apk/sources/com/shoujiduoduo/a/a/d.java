package com.shoujiduoduo.a.a;

import java.util.ArrayList;

/* compiled from: ProcessingNotifyStack */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static int f1814a = 0;

    /* renamed from: b  reason: collision with root package name */
    private static ArrayList<a> f1815b = new ArrayList<>(2);

    /* compiled from: ProcessingNotifyStack */
    public static final class a {

        /* renamed from: a  reason: collision with root package name */
        public int f1816a;

        /* renamed from: b  reason: collision with root package name */
        public int f1817b;
        public int c;
    }

    public static a a(int i, int i2) {
        a aVar;
        if (f1814a == f1815b.size()) {
            aVar = new a();
            f1815b.add(aVar);
            com.shoujiduoduo.base.a.a.e("MessageManager", "同步通知嵌套达到" + (f1814a + 1) + "层");
        } else {
            aVar = f1815b.get(f1814a);
        }
        aVar.f1816a = i;
        aVar.f1817b = 0;
        aVar.c = i2;
        f1814a++;
        return aVar;
    }

    public static void a() {
        f1814a--;
    }

    public static void a(int i) {
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < f1814a) {
                a aVar = f1815b.get(i3);
                if (aVar.f1816a == i) {
                    aVar.c++;
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public static void b(int i, int i2) {
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 < f1814a) {
                a aVar = f1815b.get(i4);
                if (aVar.f1816a == i) {
                    aVar.c--;
                    if (i2 <= aVar.f1817b) {
                        aVar.f1817b--;
                    }
                }
                i3 = i4 + 1;
            } else {
                return;
            }
        }
    }

    static {
        for (int i = 0; i < 2; i++) {
            f1815b.add(new a());
        }
    }
}
