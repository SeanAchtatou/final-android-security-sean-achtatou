package com.shoujiduoduo.player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.widget.RemoteViews;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.player.b;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.WelcomeActivity;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.s;
import java.io.File;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class PlayerService extends Service implements i, b.C0032b {
    /* access modifiers changed from: private */
    public static com.shoujiduoduo.base.bean.i c;
    /* access modifiers changed from: private */
    public static d d;
    /* access modifiers changed from: private */
    public static boolean v = false;

    /* renamed from: a  reason: collision with root package name */
    public int f1981a = 0;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public m f1982b;
    /* access modifiers changed from: private */
    public b e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public int i;
    private int j;
    /* access modifiers changed from: private */
    public Timer k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public final Object m = new Object();
    private IBinder n = new b();
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public String q = "";
    /* access modifiers changed from: private */
    public boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    private c t = null;
    private NotificationManager u = null;
    private TelephonyManager w;
    /* access modifiers changed from: private */
    public int x = -1;
    /* access modifiers changed from: private */
    public Handler y = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, boolean):boolean
         arg types: [com.shoujiduoduo.player.PlayerService, int]
         candidates:
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, int):int
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.RingData, boolean):com.shoujiduoduo.base.bean.i
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, java.util.Timer):java.util.Timer
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, com.shoujiduoduo.base.bean.i):boolean
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.d, int):void
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.base.bean.i, int):void
          com.shoujiduoduo.a.c.i.a(com.shoujiduoduo.base.bean.i, int):void
          com.shoujiduoduo.player.PlayerService.a(com.shoujiduoduo.player.PlayerService, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.player.PlayerService, boolean):boolean
         arg types: [com.shoujiduoduo.player.PlayerService, int]
         candidates:
          com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.base.bean.d, int):void
          com.shoujiduoduo.player.PlayerService.b(com.shoujiduoduo.player.PlayerService, boolean):boolean */
        public void handleMessage(Message message) {
            com.shoujiduoduo.base.bean.i iVar;
            int e;
            switch (message.what) {
                case 3001:
                    com.shoujiduoduo.base.bean.i iVar2 = (com.shoujiduoduo.base.bean.i) message.obj;
                    if (PlayerService.c != null && iVar2 != null && iVar2.c == PlayerService.c.c) {
                        PlayerService.c.e = iVar2.e;
                        PlayerService.c.g = iVar2.g;
                        PlayerService.c.h = iVar2.h;
                        PlayerService.c.f = iVar2.f;
                        com.shoujiduoduo.base.a.a.a("PlayerService", "download_start message: bitrate = " + PlayerService.c.f + ", filesize:" + iVar2.e);
                        return;
                    }
                    return;
                case 3002:
                    com.shoujiduoduo.base.bean.i iVar3 = (com.shoujiduoduo.base.bean.i) message.obj;
                    if (PlayerService.c != null && iVar3 != null && iVar3.c == PlayerService.c.c && PlayerService.this.q.equals(iVar3.g)) {
                        PlayerService.c.d = iVar3.d;
                        if (PlayerService.this.g && PlayerService.this.f(PlayerService.c)) {
                            com.shoujiduoduo.base.a.a.a("PlayerService", "download_progress message: start play, bitrate = " + PlayerService.c.f);
                            com.shoujiduoduo.base.a.a.a("PlayerService", "Download Enough, Start Play!");
                            if (PlayerService.this.i() == 1) {
                                f.c("play from PlayerService download progress message handler, currentSong is null!");
                            }
                            boolean unused = PlayerService.this.g = false;
                        }
                        if (PlayerService.this.h) {
                            if (PlayerService.c.e > 0) {
                                e = PlayerService.c.d - ((int) ((((float) PlayerService.this.e.e()) * ((float) PlayerService.c.e)) / ((float) PlayerService.this.i)));
                            } else {
                                e = PlayerService.c.d - ((int) ((((float) PlayerService.c.f) * ((float) PlayerService.this.e.e())) / 8000.0f));
                            }
                            if (e > (PlayerService.c.f * 10) / 8) {
                                PlayerService.this.n();
                                boolean unused2 = PlayerService.this.h = false;
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    return;
                case 3003:
                    com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService: MESSAGE_DONLOAD_FINISH received!");
                    com.shoujiduoduo.base.bean.i iVar4 = (com.shoujiduoduo.base.bean.i) message.obj;
                    if (iVar4 != null && PlayerService.c != null && PlayerService.c.c == iVar4.c) {
                        if (PlayerService.this.g) {
                            if (PlayerService.this.i() == 1) {
                                f.c("play from PlayerService download finish message handler, currentSong is null!");
                            }
                            boolean unused3 = PlayerService.this.g = false;
                        }
                        if (PlayerService.this.h) {
                            PlayerService.this.n();
                            boolean unused4 = PlayerService.this.h = false;
                            return;
                        }
                        return;
                    }
                    return;
                case 3004:
                    com.shoujiduoduo.base.bean.i iVar5 = (com.shoujiduoduo.base.bean.i) message.obj;
                    if (iVar5 != null) {
                        PlayerService.this.f1982b.a(iVar5);
                        return;
                    }
                    return;
                case 3005:
                    a aVar = (a) message.obj;
                    if (aVar != null && (iVar = aVar.f1986a) != null) {
                        if (iVar.d > 0) {
                            PlayerService.this.f1982b.a(iVar.d, iVar.c);
                        }
                        if (PlayerService.c != null) {
                            if (iVar.c == PlayerService.c.c && PlayerService.this.f) {
                                PlayerService.this.m();
                            }
                            PlayerService.this.a(6);
                            return;
                        }
                        return;
                    }
                    return;
                case 3100:
                    if (PlayerService.c != null) {
                        int e2 = PlayerService.this.e.e();
                        int i = 1234567890;
                        if (PlayerService.c.e > 0 && PlayerService.c.d < PlayerService.c.e) {
                            i = PlayerService.c.d - ((int) ((((float) e2) * ((float) PlayerService.c.e)) / ((float) PlayerService.this.i)));
                        } else if (PlayerService.c.e < 0) {
                            i = PlayerService.c.d - ((int) ((((float) e2) * ((float) PlayerService.c.f)) / 8000.0f));
                        }
                        if (i < (PlayerService.c.f * 5) / 8) {
                            PlayerService.this.k();
                            boolean unused5 = PlayerService.this.h = true;
                            return;
                        }
                        return;
                    }
                    return;
                case 3101:
                    if (PlayerService.this.k != null) {
                        PlayerService.this.k.cancel();
                        Timer unused6 = PlayerService.this.k = (Timer) null;
                    }
                    PlayerService.this.m();
                    PlayerService.this.a(4);
                    if (f.w() && !PlayerService.v && PlayerService.d.c() > 1) {
                        int unused7 = PlayerService.this.x = (PlayerService.this.x + 1) % PlayerService.d.c();
                        PlayerService.this.a(PlayerService.d, PlayerService.this.x);
                        return;
                    }
                    return;
                case 3200:
                    PlayerService.this.b(PlayerService.d, PlayerService.this.x);
                    return;
                case 3201:
                    boolean unused8 = PlayerService.this.r = false;
                    if (PlayerService.this.s) {
                        boolean unused9 = PlayerService.this.s = false;
                        if (PlayerService.this.l == 3) {
                            PlayerService.this.n();
                            return;
                        } else if (PlayerService.this.i() == 1) {
                            f.c("play from PlayerService call idle message handler, currentSong is null!");
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case 3202:
                    boolean unused10 = PlayerService.this.r = true;
                    if (PlayerService.this.l == 2) {
                        PlayerService.this.j();
                        boolean unused11 = PlayerService.this.s = true;
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    public static void a(boolean z) {
        v = z;
    }

    public class b extends Binder {
        public b() {
        }

        public PlayerService a() {
            com.shoujiduoduo.base.a.a.a("PlayerService", "Service: getService finished!");
            return PlayerService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        com.shoujiduoduo.base.a.a.a("PlayerService", "Service: PlayerService onBind Finished!");
        return this.n;
    }

    public PlayerService() {
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService constructor.");
    }

    public int a() {
        return this.l;
    }

    public String b() {
        if (d == null || c == null) {
            return "";
        }
        return d.a();
    }

    public int c() {
        if (c == null) {
            return -1;
        }
        return this.x;
    }

    public void a(final int i2) {
        if (!(d == null || this.l == i2)) {
            final String a2 = d.a();
            final int i3 = this.x;
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "playerService, listid:" + a2 + ", status:" + i2);
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new c.a<o>() {
                public void a() {
                    ((o) this.f1812a).a(a2, i3, i2);
                }
            });
        }
        this.l = i2;
        r();
        if (this.f1981a == -12) {
            com.shoujiduoduo.util.widget.d.a((int) R.string.sdcard_full);
        }
    }

    private void r() {
        try {
            this.u.notify(2001, s());
        } catch (Exception e2) {
            e2.printStackTrace();
            com.umeng.analytics.b.a(RingDDApp.c(), e2);
        }
    }

    private Notification s() {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.setClass(this, WelcomeActivity.class);
        PendingIntent activity = PendingIntent.getActivity(this, 2001, intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
        Intent intent2 = new Intent("com.shoujiduoduo.ringtone.PlayerService.togglePlayPause");
        ComponentName componentName = new ComponentName(this, PlayerService.class);
        intent2.setComponent(componentName);
        PendingIntent service = PendingIntent.getService(this, 0, intent2, NTLMConstants.FLAG_UNIDENTIFIED_10);
        PendingIntent broadcast = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("com.shoujiduoduo.ringtone.exitapp"), NTLMConstants.FLAG_UNIDENTIFIED_10);
        Intent intent3 = new Intent("com.shoujiduoduo.ringtone.PlayerService.toggleNext");
        intent3.setComponent(componentName);
        PendingIntent service2 = PendingIntent.getService(this, 0, intent3, NTLMConstants.FLAG_UNIDENTIFIED_10);
        String string = getResources().getString(R.string.start_notification);
        String string2 = getResources().getString(R.string.app_name);
        if (c != null) {
            string = c.f1971a + " - " + c.f1972b;
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.duoduo_icon);
        builder.setContentTitle(string2);
        builder.setContentText(string);
        builder.setTicker(string2);
        builder.setContentIntent(activity);
        builder.setOngoing(true);
        if (Build.VERSION.SDK_INT >= 11) {
            RemoteViews remoteViews = new RemoteViews(getPackageName(), (int) R.layout.notif_bar);
            remoteViews.setImageViewResource(R.id.notif_icon, R.drawable.duoduo_icon);
            remoteViews.setTextViewText(R.id.notif_ring_name, c == null ? string2 : c.f1971a);
            remoteViews.setTextViewText(R.id.notif_ring_artist, c == null ? getResources().getString(R.string.start_notification) : c.f1972b);
            if (this.l == 2) {
                remoteViews.setImageViewResource(R.id.notif_toggle_playpause, R.drawable.btn_notif_pause);
            } else {
                remoteViews.setImageViewResource(R.id.notif_toggle_playpause, R.drawable.btn_notif_play);
            }
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_playpause, service);
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_next, service2);
            remoteViews.setOnClickPendingIntent(R.id.notif_toggle_exit, broadcast);
            builder.setContent(remoteViews);
        }
        return builder.build();
    }

    public void onCreate() {
        super.onCreate();
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService onCreate.");
        this.f1982b = m.a(getApplicationContext());
        this.f1982b.a((i) this);
        this.f1982b.a(this);
        this.t = new c();
        this.w = (TelephonyManager) getSystemService("phone");
        this.w.listen(this.t, 32);
        e.a().d().a(this);
        e.a().c().a(this);
        this.l = 5;
        c = null;
        this.u = (NotificationManager) getSystemService("notification");
        r();
        if (!NativeMP3Decoder.j()) {
            HashMap hashMap = new HashMap();
            hashMap.put("lib", "mad");
            com.umeng.analytics.b.a(getApplicationContext(), "LOAD_SO_ERROR", hashMap);
        }
        if (!NativeAACDecoder.j()) {
            HashMap hashMap2 = new HashMap();
            hashMap2.put("lib", "aac");
            com.umeng.analytics.b.a(getApplicationContext(), "LOAD_SO_ERROR", hashMap2);
        }
        com.shoujiduoduo.base.a.a.a("PlayerService", "Service: PlayerService onCreate Finished!");
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService onDestroy.");
        if (this.f) {
            m();
        }
        this.w.listen(this.t, 0);
        this.t = null;
        e.a().b();
        this.u.cancel(2001);
        this.u = null;
        stopSelf();
        c = null;
        synchronized (this.m) {
            this.y.removeCallbacksAndMessages(null);
            this.y = null;
            com.shoujiduoduo.base.a.a.a("PlayerService", "ServiceOnDestroy: mHandler = null!");
        }
        m.a(getApplicationContext()).b();
        stopForeground(true);
        com.shoujiduoduo.base.a.a.a("PlayerService", "Service: onDestroy Finished!");
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int i2, int i3) {
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService onStartCommand.");
        try {
            startForeground(2001, s());
        } catch (Exception e2) {
            e2.printStackTrace();
            com.umeng.analytics.b.a(RingDDApp.c(), e2);
        }
        if (intent != null) {
            String action = intent.getAction();
            if (action == null || !action.equals("com.shoujiduoduo.ringtone.PlayerService.togglePlayPause")) {
                if (!(action == null || !action.equals("com.shoujiduoduo.ringtone.PlayerService.toggleNext") || c == null)) {
                    if (this.f) {
                        m();
                    }
                    if (f.w() && !v && d.c() > 1) {
                        this.x = (this.x + 1) % d.c();
                        a(d, this.x);
                    }
                }
            } else if (c != null) {
                if (this.l == 2) {
                    j();
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_PAUSE_IN_NOTIF");
                } else if (this.l == 3) {
                    n();
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_RESUME_IN_NOTIF");
                } else {
                    if (i() == 1) {
                        com.shoujiduoduo.base.a.a.a("PlayerService", "fuck, current song is full from notification bar");
                        f.c("play from notification bar. currentSong is null!");
                    }
                    com.umeng.analytics.b.b(this, "CONTINUOUS_PLAY_PLAY_IN_NOTIF");
                }
            }
        }
        com.shoujiduoduo.base.a.a.a("PlayerService", "Service: onStartCommand Finished!");
        return super.onStartCommand(intent, i2, i3);
    }

    public void a(RingData ringData) {
        com.shoujiduoduo.b.c.i iVar = new com.shoujiduoduo.b.c.i();
        iVar.a(ringData);
        a(iVar, 0);
    }

    public void a(d dVar, int i2) {
        RingData ringData;
        com.shoujiduoduo.base.bean.i iVar;
        if (dVar != null && (ringData = (RingData) dVar.a(i2)) != null) {
            com.shoujiduoduo.base.a.a.a("PlayerService", "setSong in, listId:" + dVar.a() + ", index:" + i2);
            synchronized (this.m) {
                com.shoujiduoduo.base.a.a.a("PlayerService", "SetSong: enter, get mLock.");
                if (this.f) {
                    m();
                }
                if (d != dVar) {
                    if (d != null) {
                        final String a2 = d.a();
                        final int i3 = this.x;
                        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new c.a<o>() {
                            public void a() {
                                ((o) this.f1812a).b(a2, i3);
                            }
                        });
                    }
                    d = dVar;
                }
                this.x = i2;
                final String a3 = d.a();
                final int i4 = this.x;
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, new c.a<o>() {
                    public void a() {
                        ((o) this.f1812a).a(a3, i4);
                    }
                });
                this.h = false;
                this.y.removeMessages(3101);
                try {
                    iVar = a(ringData, this.p);
                    this.p = false;
                } catch (Exception e2) {
                    e2.printStackTrace();
                    this.p = false;
                    iVar = null;
                } catch (Throwable th) {
                    this.p = false;
                    throw th;
                }
                if (iVar == null) {
                    a(6);
                    return;
                }
                c = iVar;
                if (dVar.a().equals("cmcc_cailing")) {
                    if (ringData.p.equals("") || ringData.p.length() != 32) {
                        com.shoujiduoduo.util.widget.d.a("该彩铃为移动赠送彩铃，暂时无法试听，可以正常设置为默认彩铃");
                        return;
                    }
                } else if (dVar.a().equals("cucc_cailing") && ringData.C.startsWith("91789000")) {
                    com.shoujiduoduo.util.widget.d.a("该彩铃暂时无法试听, 可以正常设置为默认彩铃");
                    return;
                }
                if (e(c) || !t()) {
                    com.shoujiduoduo.base.a.a.a("PlayerService", "play complete resource.");
                    this.e = e.a().d();
                } else {
                    com.shoujiduoduo.base.a.a.a("PlayerService", "play incomplete resource.");
                    this.e = e.a().c();
                }
                com.shoujiduoduo.base.a.a.a("TAG", "setSong: currentSong rid = " + c.c);
                if (f(c)) {
                    this.g = false;
                    i();
                } else {
                    this.g = true;
                    a(1);
                }
                com.shoujiduoduo.base.a.a.a("PlayerService", "SetSong: leave, release mLock.");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(d dVar, int i2) {
        this.p = true;
        a(dVar, i2);
    }

    private boolean e(com.shoujiduoduo.base.bean.i iVar) {
        return c.d == c.e && c.e > 0;
    }

    /* access modifiers changed from: private */
    public boolean f(com.shoujiduoduo.base.bean.i iVar) {
        boolean z;
        boolean z2 = iVar.d == iVar.e;
        if (iVar.f <= 0 || iVar.d <= (iVar.f * 10) / 8) {
            z = false;
        } else {
            z = true;
        }
        if (iVar.g.equals("wav")) {
            return z2;
        }
        if ((!z || !t()) && !z2) {
            return false;
        }
        return true;
    }

    private boolean t() {
        return NativeAACDecoder.j() && NativeMP3Decoder.j();
    }

    public String d() {
        if (d != null) {
            return d.a();
        }
        return "";
    }

    private com.shoujiduoduo.base.bean.i a(RingData ringData, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4 = true;
        boolean z5 = false;
        this.o = false;
        if (ringData instanceof MakeRingData) {
            com.shoujiduoduo.base.a.a.a("PlayerService", "制作的铃声");
            MakeRingData makeRingData = (MakeRingData) ringData;
            File file = new File(makeRingData.o);
            if (file.exists()) {
                com.shoujiduoduo.base.a.a.a("PlayerService", "本地存在, path:" + makeRingData.o);
                String b2 = p.b(makeRingData.o);
                int a2 = q.a(makeRingData.g, 0);
                this.q = b2;
                this.o = true;
                com.shoujiduoduo.base.bean.i iVar = new com.shoujiduoduo.base.bean.i(makeRingData.e, makeRingData.f, a2, (int) file.length(), (int) file.length(), 128000, b2, "");
                iVar.e(makeRingData.o);
                return iVar;
            } else if (!makeRingData.g.equals("")) {
                this.q = "mp3";
                com.shoujiduoduo.base.a.a.a("PlayerService", "mCurFormat = mp3");
                return this.f1982b.a(ringData, this.q);
            } else {
                com.shoujiduoduo.base.a.a.c("PlayerService", "制作的铃声，但本地没有，线上也没有，无法播放");
                return null;
            }
        } else if (!ringData.o.equals("")) {
            File file2 = new File(ringData.o);
            if (!file2.exists()) {
                return null;
            }
            com.shoujiduoduo.base.a.a.a("PlayerService", "本地存在, path:" + ringData.o);
            String b3 = p.b(ringData.o);
            this.q = b3;
            this.o = true;
            com.shoujiduoduo.base.bean.i iVar2 = new com.shoujiduoduo.base.bean.i(ringData.e, ringData.f, 0, (int) file2.length(), (int) file2.length(), 128000, b3, "");
            iVar2.e(ringData.o);
            return iVar2;
        } else {
            com.shoujiduoduo.base.a.a.a("PlayerService", "在线铃声");
            if (!ringData.p.equals("") && ringData.s == 0) {
                com.shoujiduoduo.base.a.a.a("PlayerService", "SetSong: current ring is cmcc cailing");
                z2 = false;
                z3 = true;
                z4 = false;
            } else if (!ringData.u.equals("") && ringData.x == 0) {
                com.shoujiduoduo.base.a.a.a("PlayerService", "SetSong: current ring is ctcc cailing");
                if (ringData.u.startsWith("81007")) {
                    com.shoujiduoduo.base.a.a.a("PlayerService", "ctcc diy cailing");
                    z2 = true;
                    z3 = false;
                } else {
                    z2 = true;
                    z3 = false;
                    z4 = false;
                }
            } else if (ringData.C.equals("") || !ringData.F.equals("") || ringData.j()) {
                z4 = false;
                z2 = false;
                z3 = false;
            } else {
                com.shoujiduoduo.base.a.a.a("PlayerService", "SetSong: current ring is cucc cailing");
                z2 = false;
                z3 = false;
                z4 = false;
                z5 = true;
            }
            if (z4) {
                this.q = "wav";
            } else if (z3 || z2 || z5) {
                this.q = "mp3";
            } else if (z) {
                this.q = "mp3";
            } else if (ringData.j()) {
                this.q = "aac";
            } else if (ringData.i() || !ringData.F.equals("")) {
                this.q = "mp3";
            } else {
                this.q = "aac";
            }
            com.shoujiduoduo.base.a.a.a("PlayerService", "mCurFormat = " + this.q);
            return this.f1982b.a(ringData, this.q);
        }
    }

    public void e() {
        synchronized (this.m) {
            com.shoujiduoduo.base.a.a.a("PlayerService", "reset: enter: get mLock.");
            if (this.f) {
                m();
            }
            c = null;
            this.x = -1;
            this.h = false;
            com.shoujiduoduo.base.a.a.a("PlayerService", "reset: leave, release mLock.");
        }
    }

    public int f() {
        int i2;
        synchronized (this.m) {
            if (c != null) {
                i2 = c.c;
            } else {
                i2 = -1;
            }
        }
        return i2;
    }

    public int g() {
        return this.i;
    }

    public int h() {
        if (this.e != null) {
            return this.e.e();
        }
        return 0;
    }

    public int i() {
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService: play!");
        if (this.r) {
            this.s = true;
            com.shoujiduoduo.base.a.a.e("PlayerService", "play failed, mCallIn == true");
            return 0;
        } else if (c == null) {
            f.c("currentSong is null when Play!");
            com.shoujiduoduo.base.a.a.e("PlayerService", "play failed, currentSong == null");
            return 1;
        } else if (c.l() == null) {
            f.c("currentSong.getSongPath is null when Play!");
            com.shoujiduoduo.base.a.a.e("PlayerService", "play failed, currentSong.getSongPath() == null");
            return 2;
        } else {
            if (e(c) || !t()) {
                com.shoujiduoduo.base.a.a.b("PlayerService", "download finished data or local data");
                this.e = e.a().d();
                if (this.e.a(c.l()) != 0) {
                    com.shoujiduoduo.base.a.a.e("PlayerService", "system player play failed!");
                    if (this.o || this.q.equals("mp3")) {
                        com.shoujiduoduo.base.a.a.c("PlayerService", "duoduo player play failed! final fail!!");
                        a(6);
                    } else {
                        com.shoujiduoduo.base.a.a.b("PlayerService", "retry mp3 format");
                        this.y.sendEmptyMessage(3200);
                    }
                    return 4;
                }
            } else {
                com.shoujiduoduo.base.a.a.b("PlayerService", "download unfinished data");
                this.e = e.a().c();
                if (this.e.a(c.l()) != 0) {
                    com.shoujiduoduo.base.a.a.e("PlayerService", "duoduo player play failed");
                    if (this.o || this.q.equals("mp3")) {
                        com.shoujiduoduo.base.a.a.c("PlayerService", "duoduo player play failed! final fail!!");
                        a(6);
                    } else {
                        com.shoujiduoduo.base.a.a.b("PlayerService", "retry mp3 format");
                        this.y.sendEmptyMessage(3200);
                    }
                    return 4;
                }
            }
            StringBuilder sb = new StringBuilder();
            sb.append("&rid=").append(c.c).append("&listid=").append(d.a()).append("&listtype=").append(d.b().toString());
            s.a(Constants.PLAYMUSIC, cn.banshenggua.aichang.player.PlayerService.ACTION_PLAY, sb.toString());
            this.f = true;
            this.i = this.e.f();
            this.j = this.e.h();
            com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService: play success: duration = " + this.i + ", bitrate = " + this.j);
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    synchronized (PlayerService.this.m) {
                        if (PlayerService.this.y != null) {
                            PlayerService.this.y.sendEmptyMessage(3100);
                        }
                    }
                }
            }, 1000, 1000);
            a(2);
            return 0;
        }
    }

    public boolean j() {
        this.e.b();
        if (this.k != null) {
            this.k.cancel();
            this.k = null;
        }
        a(3);
        return true;
    }

    public boolean k() {
        this.e.b();
        if (this.k != null) {
            this.k.cancel();
            this.k = null;
        }
        a(1);
        return true;
    }

    public boolean l() {
        return this.f;
    }

    public boolean m() {
        if (this.e != null) {
            this.e.a();
        }
        if (this.k != null) {
            this.k.cancel();
            this.k = null;
        }
        this.f = false;
        this.s = false;
        a(5);
        return true;
    }

    public boolean n() {
        if (this.r) {
            this.s = true;
        } else {
            this.e.c();
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    synchronized (PlayerService.this.m) {
                        if (PlayerService.this.y != null) {
                            PlayerService.this.y.sendEmptyMessage(3100);
                        }
                    }
                }
            }, 1000, 1000);
            a(2);
        }
        return true;
    }

    public void a(com.shoujiduoduo.base.bean.i iVar) {
        if (c != null && iVar != null && iVar.c == c.c) {
            synchronized (this.m) {
                if (this.y != null) {
                    this.y.sendMessage(this.y.obtainMessage(3002, iVar));
                }
            }
        }
    }

    public void a(com.shoujiduoduo.base.bean.i iVar, int i2) {
        a aVar = new a();
        aVar.f1986a = iVar;
        aVar.f1987b = i2;
        this.f1981a = i2;
        synchronized (this.m) {
            if (this.y != null) {
                this.y.sendMessage(this.y.obtainMessage(3005, aVar));
            }
        }
    }

    public void b(com.shoujiduoduo.base.bean.i iVar) {
        com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService: onDownloadFinish!");
        synchronized (this.m) {
            if (this.y != null) {
                this.y.sendMessage(this.y.obtainMessage(3003, iVar));
            }
        }
    }

    public void c(com.shoujiduoduo.base.bean.i iVar) {
        if (c != null && iVar != null && iVar.c == c.c) {
            synchronized (this.m) {
                if (this.y != null) {
                    this.y.sendMessage(this.y.obtainMessage(3001, iVar));
                }
            }
        }
    }

    public void d(com.shoujiduoduo.base.bean.i iVar) {
        synchronized (this.m) {
            if (this.y != null) {
                this.y.sendMessage(this.y.obtainMessage(3004, iVar));
            }
        }
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        public com.shoujiduoduo.base.bean.i f1986a;

        /* renamed from: b  reason: collision with root package name */
        public int f1987b;

        private a() {
        }
    }

    public void a(b bVar) {
        synchronized (this.m) {
            if (this.y != null) {
                Message obtainMessage = this.y.obtainMessage(3101, null);
                com.shoujiduoduo.base.a.a.a("PlayerService", "PlayerService: send MESSAGE_PLAY_COMPLETE");
                this.y.sendMessage(obtainMessage);
            }
        }
    }

    private class c extends PhoneStateListener {
        private c() {
        }

        public void onCallStateChanged(int i, String str) {
            super.onCallStateChanged(i, str);
            switch (i) {
                case 0:
                    synchronized (PlayerService.this.m) {
                        if (PlayerService.this.y != null) {
                            PlayerService.this.y.sendEmptyMessage(3201);
                        }
                    }
                    return;
                case 1:
                case 2:
                    synchronized (PlayerService.this.m) {
                        if (PlayerService.this.y != null) {
                            PlayerService.this.y.sendEmptyMessage(3202);
                        }
                    }
                    return;
                default:
                    return;
            }
        }
    }
}
