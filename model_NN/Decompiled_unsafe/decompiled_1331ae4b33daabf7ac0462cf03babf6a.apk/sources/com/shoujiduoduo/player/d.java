package com.shoujiduoduo.player;

import android.media.AudioTrack;

/* compiled from: DuoduoPlayer */
public class d extends b {
    private final Object h = new Object();
    private AudioTrack i = null;
    private final Object j = new Object();
    private c k;
    private short[] l = null;
    private int m = 0;
    /* access modifiers changed from: private */
    public boolean n = false;
    private boolean o = false;
    private a p = null;
    private b q = null;
    private final Object r = new Object();

    public int e() {
        if (this.k == null) {
            return 0;
        }
        return this.k.f();
    }

    public int f() {
        if (this.k == null) {
            return 0;
        }
        return this.k.e() * 1000;
    }

    public void b() {
        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "func: pause");
        if (i()) {
            a(3);
            synchronized (this.h) {
                if (this.i != null) {
                    try {
                        this.i.pause();
                    } catch (IllegalStateException e) {
                        com.shoujiduoduo.base.a.a.a(e);
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:103:?, code lost:
        return 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:?, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        return 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:?, code lost:
        return 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0105, code lost:
        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "play, finish loading file.");
        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "ChannelNum: " + r0 + "  SampleRate: " + r2 + "  Bitrate: " + r3 + "  Duration: " + r4 + " SamplePerFame: " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x014c, code lost:
        if (r0 > 1) goto L_0x0184;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x014e, code lost:
        r3 = 4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x014f, code lost:
        r5 = android.media.AudioTrack.getMinBufferSize(r2, r3, 2) << 1;
        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "play buffer size = " + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x016e, code lost:
        if (r5 == -2) goto L_0x0172;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0170, code lost:
        if (r5 != -1) goto L_0x0187;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0174, code lost:
        if (r13.d == null) goto L_0x017b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x0176, code lost:
        r13.d.a(r13, r5, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x017b, code lost:
        a(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0184, code lost:
        r3 = 12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        r13.i = new android.media.AudioTrack(3, r2, r3, 2, r5, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0191, code lost:
        r0 = (((r5 >> 1) + 4096) - 1) & -4096;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x019b, code lost:
        if (r13.l == null) goto L_0x01a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01a0, code lost:
        if (r13.l.length == r0) goto L_0x01a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01a2, code lost:
        r13.l = null;
        r13.l = new short[r0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x01a9, code lost:
        a(2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x01ae, code lost:
        if (r13.f1993b == null) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x01b0, code lost:
        r13.f1993b.a(r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01b7, code lost:
        if (r13.f1992a == false) goto L_0x0232;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01b9, code lost:
        r1 = r13.h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01bb, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r13.i.play();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:?, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01c2, code lost:
        a(4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01c8, code lost:
        if (r13.f == null) goto L_0x01cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ca, code lost:
        r13.f.a(r13, 0, 1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01cf, code lost:
        r13.p = new com.shoujiduoduo.player.d.a(r13);
        r13.p.setName("decode_thread");
        r13.q = new com.shoujiduoduo.player.d.b(r13);
        r13.q.setName("play_thread");
        r13.p.start();
        r13.q.start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01f7, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01f8, code lost:
        com.shoujiduoduo.base.a.a.a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01fd, code lost:
        if (r13.d != null) goto L_0x01ff;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01ff, code lost:
        r13.d.a(r13, -1, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0204, code lost:
        a(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:?, code lost:
        com.shoujiduoduo.util.f.c("AudioTrack.play Error! \nbufferSize = " + r0 + "\npath = " + r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0232, code lost:
        a(3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int a(java.lang.String r14) {
        /*
            r13 = this;
            r9 = 3
            r8 = 2
            r12 = -1
            r10 = 1
            r7 = 0
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "play, file path:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r14)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            r13.d()
            r13.n = r7
            java.lang.String r0 = "."
            int r0 = r14.lastIndexOf(r0)
            if (r0 == r12) goto L_0x00a1
            int r0 = r0 + 1
            java.lang.String r1 = r14.substring(r0)
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "format:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            com.shoujiduoduo.base.a.a.a(r0, r2)
            com.shoujiduoduo.player.c r0 = r13.k
            if (r0 == 0) goto L_0x006b
            com.shoujiduoduo.player.c r0 = r13.k
            java.lang.String[] r2 = r0.i()
            r0 = r7
        L_0x0053:
            int r3 = r2.length
            if (r0 >= r3) goto L_0x0236
            r3 = r2[r0]
            boolean r3 = r3.equalsIgnoreCase(r1)
            if (r3 == 0) goto L_0x00aa
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.String r2 = "useCurrentDecoder = true!"
            com.shoujiduoduo.base.a.a.a(r0, r2)
            r0 = r10
        L_0x0066:
            if (r0 != 0) goto L_0x006b
            r0 = 0
            r13.k = r0
        L_0x006b:
            com.shoujiduoduo.player.c r0 = r13.k
            if (r0 != 0) goto L_0x007e
            java.lang.String r0 = "mp3"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00ad
            com.shoujiduoduo.player.NativeMP3Decoder r0 = new com.shoujiduoduo.player.NativeMP3Decoder
            r0.<init>()
            r13.k = r0
        L_0x007e:
            r13.a(r10)
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.String r1 = "play, now load file."
            com.shoujiduoduo.base.a.a.a(r0, r1)
            java.lang.Object r1 = r13.j
            monitor-enter(r1)
            com.shoujiduoduo.player.c r0 = r13.k     // Catch:{ all -> 0x0181 }
            int r0 = r0.a(r14)     // Catch:{ all -> 0x0181 }
            if (r0 != r12) goto L_0x00d7
            r0 = 0
            r13.a(r0)     // Catch:{ all -> 0x0181 }
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.String r2 = "decoder load error"
            com.shoujiduoduo.base.a.a.c(r0, r2)     // Catch:{ all -> 0x0181 }
            monitor-exit(r1)     // Catch:{ all -> 0x0181 }
            r7 = r9
        L_0x00a0:
            return r7
        L_0x00a1:
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.String r1 = "wrong audio file path"
            com.shoujiduoduo.base.a.a.c(r0, r1)
            r7 = r10
            goto L_0x00a0
        L_0x00aa:
            int r0 = r0 + 1
            goto L_0x0053
        L_0x00ad:
            java.lang.String r0 = "aac"
            boolean r0 = r1.equalsIgnoreCase(r0)
            if (r0 == 0) goto L_0x00bd
            com.shoujiduoduo.player.NativeAACDecoder r0 = new com.shoujiduoduo.player.NativeAACDecoder
            r0.<init>()
            r13.k = r0
            goto L_0x007e
        L_0x00bd:
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "not support format:"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.c(r0, r1)
            r7 = r8
            goto L_0x00a0
        L_0x00d7:
            com.shoujiduoduo.player.c r0 = r13.k     // Catch:{ all -> 0x0181 }
            int r0 = r0.b()     // Catch:{ all -> 0x0181 }
            com.shoujiduoduo.player.c r2 = r13.k     // Catch:{ all -> 0x0181 }
            int r2 = r2.d()     // Catch:{ all -> 0x0181 }
            com.shoujiduoduo.player.c r3 = r13.k     // Catch:{ all -> 0x0181 }
            int r3 = r3.c()     // Catch:{ all -> 0x0181 }
            com.shoujiduoduo.player.c r4 = r13.k     // Catch:{ all -> 0x0181 }
            int r4 = r4.e()     // Catch:{ all -> 0x0181 }
            com.shoujiduoduo.player.c r5 = r13.k     // Catch:{ all -> 0x0181 }
            int r5 = r5.g()     // Catch:{ all -> 0x0181 }
            if (r0 == 0) goto L_0x00fd
            if (r2 == 0) goto L_0x00fd
            if (r3 == 0) goto L_0x00fd
            if (r5 != 0) goto L_0x0104
        L_0x00fd:
            r0 = 0
            r13.a(r0)     // Catch:{ all -> 0x0181 }
            monitor-exit(r1)     // Catch:{ all -> 0x0181 }
            r7 = r9
            goto L_0x00a0
        L_0x0104:
            monitor-exit(r1)     // Catch:{ all -> 0x0181 }
            java.lang.String r1 = "DuoduoAudioPlayer"
            java.lang.String r6 = "play, finish loading file."
            com.shoujiduoduo.base.a.a.a(r1, r6)
            java.lang.String r1 = "DuoduoAudioPlayer"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r11 = "ChannelNum: "
            java.lang.StringBuilder r6 = r6.append(r11)
            java.lang.StringBuilder r6 = r6.append(r0)
            java.lang.String r11 = "  SampleRate: "
            java.lang.StringBuilder r6 = r6.append(r11)
            java.lang.StringBuilder r6 = r6.append(r2)
            java.lang.String r11 = "  Bitrate: "
            java.lang.StringBuilder r6 = r6.append(r11)
            java.lang.StringBuilder r3 = r6.append(r3)
            java.lang.String r6 = "  Duration: "
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r4 = " SamplePerFame: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            com.shoujiduoduo.base.a.a.a(r1, r3)
            if (r0 > r10) goto L_0x0184
            r3 = 4
        L_0x014f:
            int r0 = android.media.AudioTrack.getMinBufferSize(r2, r3, r8)
            int r5 = r0 << 1
            java.lang.String r0 = "DuoduoAudioPlayer"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "play buffer size = "
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r1 = r1.toString()
            com.shoujiduoduo.base.a.a.a(r0, r1)
            r0 = -2
            if (r5 == r0) goto L_0x0172
            if (r5 != r12) goto L_0x0187
        L_0x0172:
            com.shoujiduoduo.player.b$c r0 = r13.d
            if (r0 == 0) goto L_0x017b
            com.shoujiduoduo.player.b$c r0 = r13.d
            r0.a(r13, r5, r7)
        L_0x017b:
            r13.a(r7)
            r7 = 4
            goto L_0x00a0
        L_0x0181:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0181 }
            throw r0
        L_0x0184:
            r3 = 12
            goto L_0x014f
        L_0x0187:
            android.media.AudioTrack r0 = new android.media.AudioTrack     // Catch:{ Exception -> 0x01f7 }
            r1 = 3
            r4 = 2
            r6 = 1
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x01f7 }
            r13.i = r0     // Catch:{ Exception -> 0x01f7 }
            int r0 = r5 >> 1
            int r0 = r0 + 4096
            int r0 = r0 + -1
            r0 = r0 & -4096(0xfffffffffffff000, float:NaN)
            short[] r1 = r13.l
            if (r1 == 0) goto L_0x01a2
            short[] r1 = r13.l
            int r1 = r1.length
            if (r1 == r0) goto L_0x01a9
        L_0x01a2:
            r1 = 0
            r13.l = r1
            short[] r1 = new short[r0]
            r13.l = r1
        L_0x01a9:
            r13.a(r8)
            com.shoujiduoduo.player.b$e r1 = r13.f1993b
            if (r1 == 0) goto L_0x01b5
            com.shoujiduoduo.player.b$e r1 = r13.f1993b
            r1.a(r13)
        L_0x01b5:
            boolean r1 = r13.f1992a
            if (r1 == 0) goto L_0x0232
            java.lang.Object r1 = r13.h
            monitor-enter(r1)
            android.media.AudioTrack r2 = r13.i     // Catch:{ IllegalStateException -> 0x020a }
            r2.play()     // Catch:{ IllegalStateException -> 0x020a }
            monitor-exit(r1)     // Catch:{ all -> 0x022f }
            r0 = 4
            r13.a(r0)
            com.shoujiduoduo.player.b$d r0 = r13.f
            if (r0 == 0) goto L_0x01cf
            com.shoujiduoduo.player.b$d r0 = r13.f
            r0.a(r13, r7, r10)
        L_0x01cf:
            com.shoujiduoduo.player.d$a r0 = new com.shoujiduoduo.player.d$a
            r0.<init>()
            r13.p = r0
            com.shoujiduoduo.player.d$a r0 = r13.p
            java.lang.String r1 = "decode_thread"
            r0.setName(r1)
            com.shoujiduoduo.player.d$b r0 = new com.shoujiduoduo.player.d$b
            r0.<init>()
            r13.q = r0
            com.shoujiduoduo.player.d$b r0 = r13.q
            java.lang.String r1 = "play_thread"
            r0.setName(r1)
            com.shoujiduoduo.player.d$a r0 = r13.p
            r0.start()
            com.shoujiduoduo.player.d$b r0 = r13.q
            r0.start()
            goto L_0x00a0
        L_0x01f7:
            r0 = move-exception
            com.shoujiduoduo.base.a.a.a(r0)
            com.shoujiduoduo.player.b$c r0 = r13.d
            if (r0 == 0) goto L_0x0204
            com.shoujiduoduo.player.b$c r0 = r13.d
            r0.a(r13, r12, r7)
        L_0x0204:
            r13.a(r7)
            r7 = 5
            goto L_0x00a0
        L_0x020a:
            r2 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x022f }
            r2.<init>()     // Catch:{ all -> 0x022f }
            java.lang.String r3 = "AudioTrack.play Error! \nbufferSize = "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x022f }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x022f }
            java.lang.String r2 = "\npath = "
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ all -> 0x022f }
            java.lang.StringBuilder r0 = r0.append(r14)     // Catch:{ all -> 0x022f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x022f }
            com.shoujiduoduo.util.f.c(r0)     // Catch:{ all -> 0x022f }
            r7 = 6
            monitor-exit(r1)     // Catch:{ all -> 0x022f }
            goto L_0x00a0
        L_0x022f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x022f }
            throw r0
        L_0x0232:
            r13.a(r9)
            goto L_0x01cf
        L_0x0236:
            r0 = r7
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.player.d.a(java.lang.String):int");
    }

    /* access modifiers changed from: private */
    public synchronized void n() {
        int write;
        while (this.m == 0 && !this.o) {
            try {
                wait(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.m > 0) {
            synchronized (this.h) {
                write = this.i.write(this.l, 0, this.m);
            }
            if (write != -3 && write != -2) {
                this.m = 0;
            } else if (this.d != null) {
                this.d.a(this, write, 0);
            }
        }
        notifyAll();
        return;
    }

    /* access modifiers changed from: private */
    public synchronized int o() {
        int b2;
        int i2 = 0;
        synchronized (this) {
            while (this.m > 0) {
                try {
                    wait(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            synchronized (this.j) {
                this.m = this.k.a(this.l);
                if (this.m == 0) {
                    this.o = this.k.h();
                    b2 = 0;
                } else {
                    b2 = this.k.b();
                }
            }
            if (this.m > 0 && b2 > 0) {
                i2 = this.m / b2;
            }
            notifyAll();
        }
        return i2;
    }

    /* access modifiers changed from: private */
    public boolean p() {
        return this.o;
    }

    public void l() {
        this.n = true;
    }

    /* compiled from: DuoduoPlayer */
    class a extends Thread {

        /* renamed from: b  reason: collision with root package name */
        private boolean f1995b = false;

        a() {
        }

        public void run() {
            super.run();
            com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "mPlayOver = " + d.this.n);
            while (true) {
                if (!d.this.n) {
                    if (!this.f1995b) {
                        if (d.this.k()) {
                            break;
                        }
                        if (d.this.o() == 0) {
                            this.f1995b = d.this.p();
                        }
                        try {
                            Thread.sleep(30);
                        } catch (Exception e) {
                        }
                    } else {
                        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "finish decoding!");
                        d.this.a(5);
                        if (d.this.c != null) {
                            d.this.c.a(d.this);
                        }
                    }
                } else {
                    break;
                }
            }
            com.shoujiduoduo.base.a.a.b("DuoduoAudioPlayer", "decoder is stopping...");
        }
    }

    /* compiled from: DuoduoPlayer */
    class b extends Thread {
        b() {
        }

        public void run() {
            super.run();
            while (!d.this.n && !d.this.k()) {
                while (d.this.j()) {
                    try {
                        Thread.sleep(100);
                    } catch (Exception e) {
                        com.shoujiduoduo.base.a.a.a(e);
                        return;
                    }
                }
                d.this.n();
                try {
                    Thread.sleep(20);
                } catch (Exception e2) {
                    return;
                }
            }
        }
    }

    public void m() {
        com.shoujiduoduo.base.a.a.d("DuoduoAudioPlayer", "func: release");
        d();
        this.c = null;
        this.d = null;
        this.f1993b = null;
        this.e = null;
        this.f = null;
    }

    public void d() {
        l();
        if (this.p != null && this.p.isAlive()) {
            try {
                this.p.interrupt();
                this.p.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.p = null;
        }
        if (this.q != null && this.q.isAlive()) {
            try {
                this.q.interrupt();
                this.q.join();
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
            this.q = null;
        }
        synchronized (this.j) {
            if (this.k != null) {
                this.k.a();
            }
            this.m = 0;
            this.o = false;
            this.k = null;
        }
        synchronized (this.h) {
            try {
                if (this.i != null) {
                    this.i.stop();
                    this.i.release();
                    this.i = null;
                }
            } catch (IllegalStateException e3) {
            }
            this.i = null;
        }
        a(0);
    }

    public void c() {
        com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "func: resume");
        if (!j()) {
            com.shoujiduoduo.base.a.a.a("DuoduoAudioPlayer", "not paused");
            return;
        }
        synchronized (this.h) {
            if (this.i != null) {
                try {
                    this.i.play();
                } catch (IllegalStateException e) {
                    com.shoujiduoduo.base.a.a.a(e);
                    return;
                }
            }
        }
        a(4);
    }

    public int h() {
        int c;
        synchronized (this.j) {
            c = this.k == null ? 0 : this.k.c();
        }
        return c;
    }
}
