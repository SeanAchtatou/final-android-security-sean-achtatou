package com.shoujiduoduo.player;

/* compiled from: PlayerManager */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private d f1997a;

    /* renamed from: b  reason: collision with root package name */
    private h f1998b;

    /* compiled from: PlayerManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public static e f1999a = new e();
    }

    private e() {
        this.f1997a = null;
        this.f1998b = null;
        this.f1998b = new h();
        this.f1997a = new d();
    }

    public static e a() {
        return a.f1999a;
    }

    public void b() {
        if (this.f1998b != null) {
            this.f1998b.a();
            this.f1998b.l();
        }
        if (this.f1997a != null) {
            this.f1997a.a();
            this.f1997a.m();
        }
    }

    public b c() {
        if (this.f1997a == null) {
            this.f1997a = new d();
        }
        return this.f1997a;
    }

    public b d() {
        if (this.f1998b == null) {
            this.f1998b = new h();
        }
        return this.f1998b;
    }
}
