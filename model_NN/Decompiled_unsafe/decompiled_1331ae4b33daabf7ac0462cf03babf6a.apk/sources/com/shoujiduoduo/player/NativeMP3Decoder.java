package com.shoujiduoduo.player;

import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.x;

public class NativeMP3Decoder implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1979a = NativeMP3Decoder.class.getSimpleName();
    private static boolean c = x.a("mad");

    /* renamed from: b  reason: collision with root package name */
    private int f1980b = -1;

    private native void closeFile(int i);

    private native int getBitrate(int i);

    private native int getChannelNum(int i);

    private native int getCurrentPosition(int i);

    private native int getDuration(int i);

    private native int getSamplePerFrame(int i);

    private native int getSamplerate(int i);

    private native int openFile(String str);

    public native int readSamples(int i, short[] sArr, int i2);

    static {
        a.a(f1979a, "load mad codec, res:" + c);
    }

    public static boolean j() {
        return c;
    }

    public int a(String str) {
        a.a(f1979a, "NativeMP3Decoder: load " + str);
        this.f1980b = openFile(str);
        return this.f1980b;
    }

    public int b() {
        if (this.f1980b != -1) {
            return getChannelNum(this.f1980b);
        }
        return 0;
    }

    public int c() {
        return getBitrate(this.f1980b);
    }

    public int d() {
        return getSamplerate(this.f1980b);
    }

    public int e() {
        if (this.f1980b != -1) {
            return getDuration(this.f1980b);
        }
        return 0;
    }

    public int f() {
        if (this.f1980b != -1) {
            return getCurrentPosition(this.f1980b);
        }
        return 0;
    }

    public int g() {
        return getSamplePerFrame(this.f1980b);
    }

    public void a() {
        if (this.f1980b != -1) {
            closeFile(this.f1980b);
            this.f1980b = -1;
        }
    }

    public boolean k() {
        return this.f1980b == -1;
    }

    public int a(short[] sArr) {
        if (this.f1980b == -1 || sArr == null) {
            return 0;
        }
        int readSamples = readSamples(this.f1980b, sArr, sArr.length);
        if (readSamples != 0) {
            return readSamples;
        }
        a();
        return readSamples;
    }

    public boolean h() {
        return k() || f() / 1000 == e();
    }

    public String[] i() {
        return new String[]{"mp3"};
    }
}
