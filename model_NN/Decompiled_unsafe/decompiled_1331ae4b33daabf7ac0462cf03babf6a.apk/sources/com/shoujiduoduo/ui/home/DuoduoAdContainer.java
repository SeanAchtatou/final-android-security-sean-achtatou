package com.shoujiduoduo.ui.home;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.baidu.mobads.AdSettings;
import com.baidu.mobads.AdView;
import com.baidu.mobads.AdViewListener;
import com.baidu.mobads.BaiduManager;
import com.qq.e.ads.banner.ADSize;
import com.qq.e.ads.banner.AbstractBannerADListener;
import com.qq.e.ads.banner.BannerView;
import com.shoujiduoduo.a.c.f;
import com.shoujiduoduo.a.c.j;
import com.shoujiduoduo.a.c.r;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ab;
import com.umeng.analytics.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class DuoduoAdContainer extends RelativeLayout implements j {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static String f2315a = DuoduoAdContainer.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public View f2316b = null;
    /* access modifiers changed from: private */
    public View c = null;
    /* access modifiers changed from: private */
    public RelativeLayout d = null;
    /* access modifiers changed from: private */
    public DuoduoAdView e = null;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public ImageButton i = null;
    private Context j;
    /* access modifiers changed from: private */
    public boolean k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public ab.a m;
    /* access modifiers changed from: private */
    public Timer n = new Timer();
    private int o;
    private boolean p;
    /* access modifiers changed from: private */
    public ArrayList<ab.b> q;
    private boolean r;
    private f s = new f() {
        public void a(boolean z) {
            a.a(DuoduoAdContainer.f2315a, "ad config is update, init ad then");
            ArrayList unused = DuoduoAdContainer.this.q = ab.a().c();
            Iterator it = DuoduoAdContainer.this.q.iterator();
            while (it.hasNext()) {
                ab.b bVar = (ab.b) it.next();
                a.a(DuoduoAdContainer.f2315a, "order:" + bVar.f2988b + " duration:" + bVar.f2987a + " source:" + bVar.c);
            }
        }
    };
    private r t = new r() {
    };
    /* access modifiers changed from: private */
    public Handler u = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1104:
                    ab.a unused = DuoduoAdContainer.this.m = DuoduoAdContainer.this.getNextAdSource();
                    a.a(DuoduoAdContainer.f2315a, "next ad source = " + DuoduoAdContainer.this.m);
                    if (DuoduoAdContainer.this.m == null) {
                        if (DuoduoAdContainer.this.c != null) {
                            DuoduoAdContainer.this.c.setVisibility(8);
                        }
                        if (DuoduoAdContainer.this.e != null) {
                            DuoduoAdContainer.this.e.setVisibility(8);
                        }
                        if (DuoduoAdContainer.this.f2316b != null) {
                            DuoduoAdContainer.this.f2316b.setVisibility(8);
                        }
                        if (DuoduoAdContainer.this.i != null) {
                            DuoduoAdContainer.this.i.setVisibility(8);
                        }
                        if (DuoduoAdContainer.this.d != null) {
                            DuoduoAdContainer.this.d.setVisibility(8);
                        }
                        if (DuoduoAdContainer.this.n != null) {
                            DuoduoAdContainer.this.n.cancel();
                            Timer unused2 = DuoduoAdContainer.this.n = (Timer) null;
                        }
                        Timer unused3 = DuoduoAdContainer.this.n = new Timer();
                        DuoduoAdContainer.this.n.schedule(new TimerTask() {
                            public void run() {
                                if (DuoduoAdContainer.this.u != null) {
                                    DuoduoAdContainer.this.u.sendEmptyMessage(1104);
                                }
                            }
                        }, 10000);
                        return;
                    }
                    DuoduoAdContainer.this.f();
                    return;
                case 1105:
                    ab.a aVar = (ab.a) message.obj;
                    a.a(DuoduoAdContainer.f2315a, aVar + " ad ready.");
                    if (aVar == ab.a.BAIDU) {
                        boolean unused4 = DuoduoAdContainer.this.f = true;
                    } else if (aVar == ab.a.TENCENT) {
                        boolean unused5 = DuoduoAdContainer.this.g = true;
                    } else if (aVar == ab.a.TAOBAO) {
                        boolean unused6 = DuoduoAdContainer.this.h = true;
                    }
                    if (DuoduoAdContainer.this.m != null && aVar == DuoduoAdContainer.this.m) {
                        ab.a unused7 = DuoduoAdContainer.this.m = aVar;
                        DuoduoAdContainer.this.f();
                        return;
                    }
                    return;
                case 1106:
                    ab.a aVar2 = (ab.a) message.obj;
                    if (!DuoduoAdContainer.this.k && aVar2 == DuoduoAdContainer.this.m && DuoduoAdContainer.this.u != null) {
                        DuoduoAdContainer.this.u.sendEmptyMessage(1104);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    };

    public DuoduoAdContainer(Context context) {
        super(context);
        this.j = context;
    }

    public DuoduoAdContainer(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.j = context;
    }

    public DuoduoAdContainer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.j = context;
    }

    private void c() {
        a.a(f2315a, "initialize duoduo Ad");
        this.e = new DuoduoAdView(this.j);
        this.e.setVisibility(8);
        this.e.setAdListener(this);
        addView(this.e);
    }

    private void d() {
        a.a(f2315a, "initialize tencent Ad");
        this.f2316b = new BannerView((Activity) this.j, ADSize.BANNER, "1105772024", "9079537217916261398");
        addView(this.f2316b, new RelativeLayout.LayoutParams(-1, -2));
        this.f2316b.setVisibility(8);
        this.f2316b.setRefresh(30);
        this.f2316b.setADListener(new AbstractBannerADListener() {

            /* renamed from: a  reason: collision with root package name */
            boolean f2317a = true;

            public void onNoAD(int i) {
                a.a(DuoduoAdContainer.f2315a, "onNoAd tencent");
                DuoduoAdContainer.this.u.sendMessage(DuoduoAdContainer.this.u.obtainMessage(1106, ab.a.TENCENT));
            }

            public void onADReceiv() {
                if (this.f2317a) {
                    DuoduoAdContainer.this.f2316b.setVisibility(4);
                    this.f2317a = false;
                    DuoduoAdContainer.this.u.sendMessage(DuoduoAdContainer.this.u.obtainMessage(1105, ab.a.TENCENT));
                }
            }
        });
        this.f2316b.loadAD();
    }

    private void a(boolean z) {
        a.a(f2315a, "initialize baidu Ad");
        BaiduManager.init(this.j);
        this.c = new AdView(this.j, "2296609");
        AdSettings.setKey(new String[]{"baidu", "中 国 "});
        final long currentTimeMillis = System.currentTimeMillis();
        this.c.setListener(new AdViewListener() {

            /* renamed from: a  reason: collision with root package name */
            boolean f2319a = true;

            public void onAdSwitch() {
                a.a(DuoduoAdContainer.f2315a, "onAdSwitch baidu");
            }

            public void onAdClose(JSONObject jSONObject) {
            }

            public void onAdShow(JSONObject jSONObject) {
                a.a(DuoduoAdContainer.f2315a, "onAdShow baidu," + jSONObject.toString());
            }

            public void onAdReady(AdView adView) {
                a.a(DuoduoAdContainer.f2315a, "onAdReady baidu");
                if (this.f2319a) {
                    this.f2319a = false;
                    DuoduoAdContainer.this.u.sendMessage(DuoduoAdContainer.this.u.obtainMessage(1105, ab.a.BAIDU));
                }
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, boolean):boolean
             arg types: [com.shoujiduoduo.ui.home.DuoduoAdContainer, int]
             candidates:
              com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, com.shoujiduoduo.util.ab$a):com.shoujiduoduo.util.ab$a
              com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, java.util.ArrayList):java.util.ArrayList
              com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, java.util.Timer):java.util.Timer
              com.shoujiduoduo.ui.home.DuoduoAdContainer.a(com.shoujiduoduo.ui.home.DuoduoAdContainer, boolean):boolean */
            public void onAdFailed(String str) {
                a.a(DuoduoAdContainer.f2315a, "onAdFailed baidu, resson:" + str);
                if (!DuoduoAdContainer.this.l) {
                    boolean unused = DuoduoAdContainer.this.l = true;
                    HashMap hashMap = new HashMap();
                    hashMap.put("reason", str);
                    b.a(RingDDApp.c(), "BAIDU_BANNER_FAILED", hashMap, (int) (System.currentTimeMillis() - currentTimeMillis));
                }
                DuoduoAdContainer.this.u.sendMessage(DuoduoAdContainer.this.u.obtainMessage(1106, ab.a.BAIDU));
            }

            public void onAdClick(JSONObject jSONObject) {
                a.e("", "onAdClick " + jSONObject.toString());
            }
        });
        addView(this.c);
        if (!z) {
            this.c.setVisibility(8);
        }
    }

    private void e() {
        String str = null;
        try {
            if (this.o < this.q.size()) {
                String str2 = this.q.get(this.o).f2987a;
                a.a(f2315a, "show ad:" + this.q.get(this.o).c + " duration:" + str2);
                str = str2;
            }
            if (str != null) {
                if (this.n != null) {
                    this.n.cancel();
                    this.n = null;
                }
                this.n = new Timer();
                this.n.schedule(new TimerTask() {
                    public void run() {
                        if (DuoduoAdContainer.this.u != null) {
                            DuoduoAdContainer.this.u.sendEmptyMessage(1104);
                        }
                    }
                }, (long) (Integer.valueOf(str).intValue() * 1000));
            }
        } catch (IllegalStateException e2) {
        }
    }

    /* access modifiers changed from: private */
    public ab.a getNextAdSource() {
        if (this.q.size() <= 0) {
            return null;
        }
        for (int i2 = 0; i2 < this.q.size(); i2++) {
            this.o++;
            this.o %= this.q.size();
            a.a(f2315a, "morder:" + this.o);
            ab.a aVar = this.q.get(this.o).c;
            switch (aVar) {
                case DUODUO:
                    if (com.shoujiduoduo.util.a.i()) {
                        if (this.e != null) {
                            if (!this.e.a()) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            a.a(f2315a, "initializeDuoduoAd");
                            c();
                            break;
                        }
                    } else {
                        continue;
                    }
                case BAIDU:
                    if (com.shoujiduoduo.util.a.h()) {
                        if (this.c != null) {
                            if (!this.f) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            a.a(f2315a, "initializeBaiduAd");
                            a(false);
                            break;
                        }
                    } else {
                        continue;
                    }
                case TENCENT:
                    if (com.shoujiduoduo.util.a.g()) {
                        if (this.f2316b != null) {
                            if (!this.g) {
                                break;
                            } else {
                                return aVar;
                            }
                        } else {
                            a.a(f2315a, "initializeTencentAd");
                            d();
                            break;
                        }
                    } else {
                        continue;
                    }
            }
        }
        return null;
    }

    private void a(ab.a aVar) {
        int i2;
        int i3;
        int i4 = 4;
        if (!this.r || !ab.a().a("adturbo_enable").equals("1")) {
            if (this.c != null) {
                this.c.setVisibility(aVar == ab.a.BAIDU ? 0 : 4);
            }
            if (this.e != null) {
                DuoduoAdView duoduoAdView = this.e;
                if (aVar == ab.a.DUODUO) {
                    i3 = 0;
                } else {
                    i3 = 4;
                }
                duoduoAdView.setVisibility(i3);
            }
            if (this.f2316b != null) {
                View view = this.f2316b;
                if (aVar == ab.a.TENCENT) {
                    i2 = 0;
                } else {
                    i2 = 4;
                }
                view.setVisibility(i2);
            }
            if (this.d != null) {
                RelativeLayout relativeLayout = this.d;
                if (aVar == ab.a.TAOBAO) {
                    i4 = 0;
                }
                relativeLayout.setVisibility(i4);
            }
        } else {
            if (this.c != null) {
                this.c.setVisibility(4);
            }
            if (this.e != null) {
                this.e.setVisibility(4);
            }
            if (this.f2316b != null) {
                this.f2316b.setVisibility(4);
            }
            if (this.d != null) {
                this.d.setVisibility(4);
            }
        }
        if (this.i != null) {
            this.i.setVisibility(0);
            this.i.bringToFront();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        a.a(f2315a, "[banner]showAd");
        this.k = true;
        a(this.m);
        requestLayout();
        invalidate();
        e();
    }

    public void a() {
        if (this.u != null) {
            this.u.sendMessage(this.u.obtainMessage(1105, ab.a.DUODUO));
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        super.onLayout(z, i2, i3, i4, i5);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int measuredHeight;
        super.onMeasure(i2, i3);
        if (this.m != null && !this.p) {
            switch (this.m) {
                case DUODUO:
                    if (this.e != null) {
                        measuredHeight = this.e.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case BAIDU:
                    if (this.c != null) {
                        measuredHeight = this.c.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case TENCENT:
                    if (this.f2316b != null) {
                        measuredHeight = this.f2316b.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                case TAOBAO:
                    if (this.d != null) {
                        measuredHeight = this.d.getMeasuredHeight();
                        break;
                    } else {
                        return;
                    }
                default:
                    return;
            }
            setMeasuredDimension(View.MeasureSpec.getSize(i2), measuredHeight);
        }
    }
}
