package com.shoujiduoduo.ui.home;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.j;
import com.shoujiduoduo.b.a.d;
import com.shoujiduoduo.base.bean.e;
import com.shoujiduoduo.ui.utils.c;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.n;
import java.util.Timer;
import java.util.TimerTask;

public class DuoduoAdView extends ImageView implements g {
    /* access modifiers changed from: private */
    public static final String c = DuoduoAdView.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    Drawable[] f2327a = null;

    /* renamed from: b  reason: collision with root package name */
    Handler f2328b = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, boolean):boolean
         arg types: [com.shoujiduoduo.ui.home.DuoduoAdView, int]
         candidates:
          com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, java.lang.String):int
          com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.base.bean.d, int):void
          com.shoujiduoduo.a.c.g.a(com.shoujiduoduo.base.bean.d, int):void
          com.shoujiduoduo.ui.home.DuoduoAdView.a(com.shoujiduoduo.ui.home.DuoduoAdView, boolean):boolean */
        public void handleMessage(Message message) {
            int i;
            switch (message.what) {
                case 331:
                    com.shoujiduoduo.base.a.a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_IMAGE got!");
                    a aVar = (a) message.obj;
                    try {
                        DuoduoAdView.this.f2327a[DuoduoAdView.this.a(aVar.f2334b)] = aVar.f2333a;
                        if (DuoduoAdView.this.m < 0) {
                            DuoduoAdView.this.c();
                            boolean unused = DuoduoAdView.this.n = true;
                            DuoduoAdView.this.d.a();
                            String a2 = ab.a().a("ad_switch_time");
                            if (a2 == null) {
                                a2 = "10000";
                            }
                            try {
                                int intValue = Integer.valueOf(a2).intValue();
                                if (intValue <= 0) {
                                    intValue = 10000;
                                }
                                i = intValue;
                            } catch (NumberFormatException e) {
                                i = 10000;
                            }
                            try {
                                DuoduoAdView.this.h.schedule(DuoduoAdView.this.l, (long) i, (long) i);
                            } catch (IllegalStateException e2) {
                            }
                        }
                        com.shoujiduoduo.base.a.a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_IMAGE handler finishes!");
                        return;
                    } catch (Exception e3) {
                        return;
                    }
                case 332:
                    com.shoujiduoduo.base.a.a.a(DuoduoAdView.c, "DuoduoAdView: get MESSAGE_FINISH_LOAD_AD");
                    DuoduoAdView.this.f2327a = new Drawable[DuoduoAdView.this.g.b()];
                    for (int i2 = 0; i2 < DuoduoAdView.this.g.b(); i2++) {
                        Drawable a3 = c.a(DuoduoAdView.this.g.a(i2).f1963a, new c.a() {
                            public void a(Drawable drawable, String str) {
                                if (drawable != null) {
                                    Message obtainMessage = DuoduoAdView.this.f2328b.obtainMessage(331, new a(drawable, str));
                                    com.shoujiduoduo.base.a.a.a(DuoduoAdView.c, "AsyncImageLoader in DuoduoAdView: finish load image: " + str);
                                    DuoduoAdView.this.f2328b.sendMessage(obtainMessage);
                                }
                            }
                        });
                        if (a3 != null) {
                            Message obtainMessage = DuoduoAdView.this.f2328b.obtainMessage(331, new a(a3, DuoduoAdView.this.g.a(i2).f1963a));
                            com.shoujiduoduo.base.a.a.a(DuoduoAdView.c, "AsyncImageLoader in DuoduoAdView: finish load image: " + DuoduoAdView.this.g.a(i2).f1963a);
                            com.shoujiduoduo.base.a.a.d(DuoduoAdView.c, "ready to sendMessage.");
                            DuoduoAdView.this.f2328b.sendMessage(obtainMessage);
                            com.shoujiduoduo.base.a.a.d(DuoduoAdView.c, "sendMessage returns!");
                        }
                    }
                    com.shoujiduoduo.base.a.a.d(DuoduoAdView.c, "MESSAGE_FINISH_LOAD_AD handler finishes!");
                    return;
                case 333:
                    DuoduoAdView.this.c();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public j d = null;
    private int e = 0;
    /* access modifiers changed from: private */
    public Context f = null;
    /* access modifiers changed from: private */
    public d g = new d("duoduo_ad.tmp");
    /* access modifiers changed from: private */
    public Timer h = new Timer();
    private final int i = 331;
    private final int j = 332;
    private final int k = 333;
    /* access modifiers changed from: private */
    public TimerTask l = new TimerTask() {
        public void run() {
            DuoduoAdView.this.f2328b.sendMessage(DuoduoAdView.this.f2328b.obtainMessage(333));
        }
    };
    /* access modifiers changed from: private */
    public int m = -1;
    /* access modifiers changed from: private */
    public boolean n = false;

    public DuoduoAdView(Context context) {
        super(context);
        a(context);
    }

    public void setAdListener(j jVar) {
        this.d = jVar;
    }

    public DuoduoAdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.n;
    }

    /* access modifiers changed from: private */
    public void c() {
        int b2 = this.g.b();
        if (b2 != 0) {
            int i2 = this.m;
            while (true) {
                i2 = (i2 + 1) % b2;
                if (this.f2327a[i2] != null || i2 == this.m) {
                    this.m = i2;
                    setImageDrawable(this.f2327a[this.m]);
                }
            }
            this.m = i2;
            setImageDrawable(this.f2327a[this.m]);
            if (getVisibility() == 0) {
                getWidth();
                int height = getHeight();
                AnimationSet animationSet = new AnimationSet(true);
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) height, 0.0f);
                translateAnimation.setDuration(500);
                animationSet.addAnimation(translateAnimation);
                startAnimation(animationSet);
            }
        }
    }

    private void a(Context context) {
        this.f = context;
        this.e = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
        this.g.a(this);
        this.g.a();
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                e a2;
                if (DuoduoAdView.this.m >= 0 && (a2 = DuoduoAdView.this.g.a(DuoduoAdView.this.m)) != null) {
                    String str = a2.f1964b;
                    n.a(DuoduoAdView.this.f).a(a2.d, a2.f1964b);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        setMeasuredDimension(this.e, (this.e * 50) / 320);
    }

    /* access modifiers changed from: private */
    public int a(String str) throws Exception {
        for (int i2 = 0; i2 < this.g.b(); i2++) {
            if (this.g.a(i2).f1963a.equals(str)) {
                return i2;
            }
        }
        throw new Exception();
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        public Drawable f2333a;

        /* renamed from: b  reason: collision with root package name */
        public String f2334b;

        a(Drawable drawable, String str) {
            this.f2333a = drawable;
            this.f2334b = str;
        }
    }

    public void a(com.shoujiduoduo.base.bean.d dVar, int i2) {
        com.shoujiduoduo.base.a.a.a(c, "DuoduoAdView: onDataUpdate called by DuoduoFamilyData!");
        switch (i2) {
            case 0:
                this.f2328b.sendMessage(this.f2328b.obtainMessage(332));
                return;
            case 1:
                return;
            case 2:
                this.f2328b.sendMessage(this.f2328b.obtainMessage(332));
                return;
            default:
                return;
        }
    }
}
