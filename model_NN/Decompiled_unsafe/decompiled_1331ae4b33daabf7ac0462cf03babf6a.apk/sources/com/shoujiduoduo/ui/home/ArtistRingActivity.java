package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.a;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.z;
import java.text.DecimalFormat;

public class ArtistRingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2307a;

    /* renamed from: b  reason: collision with root package name */
    private DDListFragment f2308b;
    private a c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_artist_ring);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.f2307a = (TextView) findViewById(R.id.header_title);
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                ArtistRingActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            this.c = (a) RingDDApp.b().a(intent.getStringExtra("parakey"));
            if (this.c != null) {
                this.f2307a.setText(this.c.e);
                b();
                this.f2308b.a(new g(g.a.list_ring_artist, this.c.f, false, ""));
                a();
                return;
            }
            com.shoujiduoduo.base.a.a.c("ArtistRingActivity", "wrong collect data prarm");
            return;
        }
        com.shoujiduoduo.base.a.a.c("ArtistRingActivity", "wrong intent == null");
    }

    private void a() {
        TextView textView = (TextView) findViewById(R.id.sale);
        d.a().a(this.c.f1957a, (ImageView) findViewById(R.id.pic), com.shoujiduoduo.ui.utils.g.a().g());
        ((TextView) findViewById(R.id.content)).setText(this.c.d);
        int i = this.c.c;
        StringBuilder sb = new StringBuilder();
        sb.append("彩铃销量:");
        if (i > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) i) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(i);
        }
        textView.setText(sb.toString());
    }

    private void b() {
        this.f2308b = new DDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        this.f2308b.setArguments(bundle);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_layout, this.f2308b);
        beginTransaction.commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService b2 = z.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        super.onDestroy();
    }
}
