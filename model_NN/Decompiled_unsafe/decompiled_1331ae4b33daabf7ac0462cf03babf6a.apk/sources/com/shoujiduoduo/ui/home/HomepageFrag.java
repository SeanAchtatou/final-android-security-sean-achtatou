package com.shoujiduoduo.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.l;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.base.bean.j;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.HtmlFragment;
import com.shoujiduoduo.ui.utils.i;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.widget.f;
import com.tencent.connect.common.Constants;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;

public class HomepageFrag extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f2335a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public List<Fragment> f2336b = new ArrayList();
    private ArrayList<j> c;
    private RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    private boolean g;
    private u h = new u() {
        public void a(int i) {
            if (i == 1) {
                com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list data is ready, init view pager and show data");
                HomepageFrag.this.c();
                HomepageFrag.this.a();
                return;
            }
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list data load error, show failed view");
            HomepageFrag.this.b();
        }
    };
    private l i = new l() {
        public void a(String str, String str2) {
            HomepageFrag.this.a(str, str2);
        }
    };
    private net.lucode.hackware.magicindicator.b.a.a.a j = new net.lucode.hackware.magicindicator.b.a.a.a() {
        public int a() {
            if (b.f().c() && HomepageFrag.this.f2336b.size() > 0) {
                return b.f().d().size();
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "getCount() return 0");
            return 0;
        }

        public d a(Context context, final int i) {
            if (!b.f().c() || HomepageFrag.this.f2336b.size() <= 0) {
                return null;
            }
            f fVar = new f(context);
            fVar.setText(b.f().d().get(i).e);
            fVar.setTextSize(17.0f);
            fVar.setMinScale(0.8f);
            fVar.setNormalColor(i.a(R.color.text_black));
            fVar.setSelectedColor(i.a(R.color.text_green));
            fVar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    HomepageFrag.this.f2335a.setCurrentItem(i);
                }
            });
            return fVar;
        }

        public c a(Context context) {
            return null;
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "retry load top list data");
            HomepageFrag.this.d();
            b.f().e();
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.homepage, viewGroup, false);
        this.f2335a = (ViewPager) inflate.findViewById(R.id.vPager);
        this.f2335a.setOffscreenPageLimit(6);
        this.f2335a.setAdapter(new a(getChildFragmentManager()));
        this.d = (RelativeLayout) inflate.findViewById(R.id.failed_view);
        this.d.setOnClickListener(this.k);
        this.e = (RelativeLayout) inflate.findViewById(R.id.loading_view);
        this.f = (RelativeLayout) inflate.findViewById(R.id.home_lists);
        MagicIndicator magicIndicator = (MagicIndicator) inflate.findViewById(R.id.magic_indicator);
        net.lucode.hackware.magicindicator.b.a.a aVar = new net.lucode.hackware.magicindicator.b.a.a(getContext());
        aVar.setAdapter(this.j);
        magicIndicator.setNavigator(aVar);
        magicIndicator.setBackgroundColor(i.a(R.color.white));
        net.lucode.hackware.magicindicator.d.a(magicIndicator, this.f2335a);
        this.g = com.shoujiduoduo.util.a.f();
        if (b.f().c()) {
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list dat is  ready");
            c();
            a();
        } else {
            d();
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "top list dat is not ready,just wait");
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_AREA, this.i);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_TOP_LIST, this.h);
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_AREA, this.i);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_TOP_LIST, this.h);
    }

    /* access modifiers changed from: private */
    public void a() {
        g gVar;
        this.c = b.f().d();
        Iterator<j> it = this.c.iterator();
        while (it.hasNext()) {
            j next = it.next();
            com.shoujiduoduo.base.a.a.a("HomepageFrag", "listname:" + next.e + ", id:" + next.g + ", type:" + next.f);
            if (next.f.equals(j.f1973a)) {
                if (next.g == 20) {
                    if (com.shoujiduoduo.util.f.t()) {
                        next.e = "彩铃榜";
                        gVar = new g(g.a.list_ring_normal, "20", false, "");
                    } else {
                        next.e = "分享榜";
                        gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_normal, Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, false, "");
                    }
                } else if (next.g == 24) {
                    String a2 = ad.a(getContext(), "user_area", "");
                    if (a2.equals("")) {
                        com.shoujiduoduo.base.a.a.a("HomepageFrag", "全国榜");
                        next.e = "全国榜";
                        gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_normal, "24", false, "");
                    } else {
                        next.e = a2 + "榜";
                        com.shoujiduoduo.base.a.a.a("HomepageFrag", "用户选择的地域榜:" + a2);
                        gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_normal, "25", false, a2);
                    }
                } else {
                    gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_normal, "" + next.g, false, "");
                }
                DDListFragment dDListFragment = new DDListFragment();
                Bundle bundle = new Bundle();
                if (next.g == 24 || next.g == 25) {
                    bundle.putBoolean("support_area", true);
                }
                if (this.g) {
                    bundle.putBoolean("support_feed_ad", true);
                }
                bundle.putBoolean("support_lazy_load", true);
                bundle.putString("adapter_type", "ring_list_adapter");
                dDListFragment.setArguments(bundle);
                dDListFragment.a(gVar);
                this.f2336b.add(dDListFragment);
            } else if (next.f.equals(j.f1974b)) {
                DDListFragment dDListFragment2 = new DDListFragment();
                com.shoujiduoduo.b.c.c cVar = new com.shoujiduoduo.b.c.c("collect");
                Bundle bundle2 = new Bundle();
                bundle2.putString("adapter_type", "collect_list_adapter");
                bundle2.putBoolean("support_lazy_load", true);
                dDListFragment2.setArguments(bundle2);
                dDListFragment2.a(cVar);
                this.f2336b.add(dDListFragment2);
            } else if (next.f.equals(j.c)) {
                DDListFragment dDListFragment3 = new DDListFragment();
                Bundle bundle3 = new Bundle();
                bundle3.putString("adapter_type", "artist_list_adapter");
                bundle3.putBoolean("support_lazy_load", true);
                dDListFragment3.setArguments(bundle3);
                dDListFragment3.a(new com.shoujiduoduo.b.c.a("artist"));
                this.f2336b.add(dDListFragment3);
            } else if (next.f.equals(j.d)) {
                HtmlFragment htmlFragment = new HtmlFragment();
                Bundle bundle4 = new Bundle();
                bundle4.putString("url", next.h);
                htmlFragment.setArguments(bundle4);
                this.f2336b.add(htmlFragment);
            } else {
                com.shoujiduoduo.base.a.a.e("HomepageFrag", "不支持的列表类型，跳过吧。");
            }
        }
        this.f2335a.getAdapter().notifyDataSetChanged();
        this.j.b();
        this.f2335a.setCurrentItem(0);
    }

    /* access modifiers changed from: private */
    public void b() {
        this.d.setVisibility(0);
        this.e.setVisibility(4);
        this.f.setVisibility(4);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        this.f.setVisibility(0);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.d.setVisibility(4);
        this.e.setVisibility(0);
        this.f.setVisibility(4);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004c A[EDGE_INSN: B:20:0x004c->B:8:0x004c ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r7, java.lang.String r8) {
        /*
            r6 = this;
            r3 = 0
            com.shoujiduoduo.b.c.f r0 = com.shoujiduoduo.a.b.b.f()
            java.util.ArrayList r0 = r0.d()
            r6.c = r0
            android.content.Context r0 = r6.getContext()
            java.lang.String r1 = "user_area"
            com.shoujiduoduo.util.ad.c(r0, r1, r7)
            java.util.ArrayList<com.shoujiduoduo.base.bean.j> r0 = r6.c
            java.util.Iterator r1 = r0.iterator()
        L_0x001a:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x004c
            java.lang.Object r0 = r1.next()
            com.shoujiduoduo.base.bean.j r0 = (com.shoujiduoduo.base.bean.j) r0
            int r2 = r0.g
            r4 = 24
            if (r2 == r4) goto L_0x0032
            int r2 = r0.g
            r4 = 25
            if (r2 != r4) goto L_0x001a
        L_0x0032:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r1 = r1.append(r7)
            java.lang.String r2 = "榜"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.e = r1
            net.lucode.hackware.magicindicator.b.a.a.a r0 = r6.j
            r0.b()
        L_0x004c:
            r2 = r3
        L_0x004d:
            java.util.List<android.support.v4.app.Fragment> r0 = r6.f2336b
            int r0 = r0.size()
            if (r2 >= r0) goto L_0x0085
            java.util.List<android.support.v4.app.Fragment> r0 = r6.f2336b
            java.lang.Object r0 = r0.get(r2)
            android.support.v4.app.Fragment r0 = (android.support.v4.app.Fragment) r0
            boolean r1 = r0 instanceof com.shoujiduoduo.ui.utils.DDListFragment
            if (r1 == 0) goto L_0x0081
            r1 = r0
            com.shoujiduoduo.ui.utils.DDListFragment r1 = (com.shoujiduoduo.ui.utils.DDListFragment) r1
            java.lang.String r1 = r1.b()
            boolean r1 = r8.equals(r1)
            if (r1 == 0) goto L_0x0081
            com.shoujiduoduo.b.c.g r1 = new com.shoujiduoduo.b.c.g
            com.shoujiduoduo.base.bean.g$a r4 = com.shoujiduoduo.base.bean.g.a.list_ring_normal
            java.lang.String r5 = "25"
            r1.<init>(r4, r5, r3, r7)
            com.shoujiduoduo.ui.utils.DDListFragment r0 = (com.shoujiduoduo.ui.utils.DDListFragment) r0
            r0.a(r1)
            net.lucode.hackware.magicindicator.b.a.a.a r0 = r6.j
            r0.b()
        L_0x0081:
            int r0 = r2 + 1
            r2 = r0
            goto L_0x004d
        L_0x0085:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.home.HomepageFrag.a(java.lang.String, java.lang.String):void");
    }

    private class a extends FragmentPagerAdapter {
        public CharSequence getPageTitle(int i) {
            if (b.f().c()) {
                return b.f().d().get(i).e;
            }
            return "";
        }

        a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            if (b.f().c() && HomepageFrag.this.f2336b.size() > 0) {
                return (Fragment) HomepageFrag.this.f2336b.get(i % HomepageFrag.this.f2336b.size());
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "return null fragment 2");
            return null;
        }

        public int getCount() {
            if (b.f().c() && HomepageFrag.this.f2336b.size() > 0) {
                return b.f().d().size();
            }
            com.shoujiduoduo.base.a.a.c("HomepageFrag", "getCount() return 0");
            return 0;
        }
    }
}
