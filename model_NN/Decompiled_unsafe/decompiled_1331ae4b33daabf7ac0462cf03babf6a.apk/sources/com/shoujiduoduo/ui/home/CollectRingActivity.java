package com.shoujiduoduo.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.d.a.b.d;
import com.jaeger.library.a;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.z;
import java.text.DecimalFormat;

public class CollectRingActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2310a;

    /* renamed from: b  reason: collision with root package name */
    private DDListFragment f2311b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public b e;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_collect_ring);
        a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.c = (Button) findViewById(R.id.fav_btn);
        this.d = (Button) findViewById(R.id.share_btn);
        this.f2310a = (TextView) findViewById(R.id.header_title);
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CollectRingActivity.this.finish();
            }
        });
        Intent intent = getIntent();
        if (intent != null) {
            this.e = (b) RingDDApp.b().a(intent.getStringExtra("parakey"));
            if (this.e != null) {
                this.f2310a.setText(this.e.f1960b);
                a();
                this.f2311b.a(new g(g.a.list_ring_collect, this.e.g, false, ""));
                b();
                return;
            }
            com.shoujiduoduo.base.a.a.c("CollectListActivity", "wrong collect data prarm");
            return;
        }
        com.shoujiduoduo.base.a.a.c("CollectListActivity", "wrong intent null");
    }

    private void a() {
        this.f2311b = new DDListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("adapter_type", "ring_list_adapter");
        this.f2311b.setArguments(bundle);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_layout, this.f2311b);
        beginTransaction.commitAllowingStateLoss();
    }

    private void b() {
        TextView textView = (TextView) findViewById(R.id.fav_num);
        d.a().a(this.e.f1959a, (ImageView) findViewById(R.id.pic), com.shoujiduoduo.ui.utils.g.a().h());
        ((TextView) findViewById(R.id.content)).setText(this.e.c);
        int a2 = q.a(this.e.f, 1000);
        StringBuilder sb = new StringBuilder();
        if (a2 > 10000) {
            sb.append(new DecimalFormat("#.00").format((double) (((float) a2) / 10000.0f)));
            sb.append("万");
        } else {
            sb.append(a2);
        }
        textView.setText(sb.toString());
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.shoujiduoduo.a.b.b.b().a(CollectRingActivity.this.e);
                com.shoujiduoduo.util.widget.d.a("添加收藏成功", 0);
                ak.a(CollectRingActivity.this.e.g, 12, "&from=collect&listType=" + g.a.list_ring_collect);
            }
        });
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService b2 = z.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        super.onDestroy();
    }
}
