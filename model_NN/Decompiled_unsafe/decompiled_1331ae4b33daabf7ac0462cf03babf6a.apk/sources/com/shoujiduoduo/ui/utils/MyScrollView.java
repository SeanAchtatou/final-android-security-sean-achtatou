package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

public class MyScrollView extends ScrollView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f2811a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public a f2812b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public Handler d;

    public interface a {
        void c();

        void d();
    }

    public interface b {
        void a(int i);
    }

    public MyScrollView(Context context) {
        this(context, null);
    }

    public MyScrollView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public MyScrollView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.d = new Handler() {
            public void handleMessage(Message message) {
                int scrollY = MyScrollView.this.getScrollY();
                if (MyScrollView.this.c != scrollY) {
                    int unused = MyScrollView.this.c = scrollY;
                    MyScrollView.this.d.sendMessageDelayed(MyScrollView.this.d.obtainMessage(), 20);
                } else if (MyScrollView.this.f2812b != null) {
                    if (MyScrollView.this.a()) {
                        MyScrollView.this.f2812b.d();
                    }
                    if (MyScrollView.this.b()) {
                        MyScrollView.this.f2812b.c();
                    }
                }
                if (MyScrollView.this.f2811a != null) {
                    MyScrollView.this.f2811a.a(scrollY);
                }
            }
        };
    }

    public void setOnScrollListener(b bVar) {
        this.f2811a = bVar;
    }

    public void setOnBorderListener(a aVar) {
        this.f2812b = aVar;
    }

    public boolean a() {
        return getScrollY() <= 0;
    }

    public boolean b() {
        return getScrollY() == (getChildAt(getChildCount() + -1).getBottom() + getPaddingBottom()) - getHeight();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.f2811a != null) {
            b bVar = this.f2811a;
            int scrollY = getScrollY();
            this.c = scrollY;
            bVar.a(scrollY);
        }
        switch (motionEvent.getAction()) {
            case 1:
                this.d.sendMessageDelayed(this.d.obtainMessage(), 20);
                break;
        }
        return super.onTouchEvent(motionEvent);
    }
}
