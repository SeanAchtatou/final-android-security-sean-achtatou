package com.shoujiduoduo.ui.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.k;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.b.a.e;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.cailing.c;
import com.shoujiduoduo.ui.cailing.d;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.user.CommentActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.ak;
import com.shoujiduoduo.util.al;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.d.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.widget.CircleProgressBar;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.shoujiduoduo.util.z;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: RingListAdapter */
public class j extends d {
    private View.OnClickListener A = new View.OnClickListener() {
        public void onClick(View view) {
            RingData c = j.this.f2852b.a(j.this.c);
            if (c != null && !ag.c(c.j)) {
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", c.j);
                j.this.g.startActivity(intent);
                ak.a(c.g, 14, "&from=" + j.this.f2852b.a() + "&listType=" + j.this.f2852b.b() + "&uid=" + b.g().c().a() + "&tuid=" + c.j);
            }
        }
    };
    private View.OnClickListener B = new View.OnClickListener() {
        public void onClick(View view) {
            RingData c = j.this.f2852b.a(j.this.c);
            Intent intent = new Intent(RingDDApp.c(), CommentActivity.class);
            intent.putExtra("tuid", c.j);
            intent.putExtra("current_ring", c);
            RingDDApp.b().a("current_list", j.this.f2852b);
            j.this.g.startActivity(intent);
            ak.a(c.g, 15, "&from=" + j.this.f2852b.a() + "&listType=" + j.this.f2852b.b() + "&uid=" + b.g().c().a() + "&tuid=" + c.j);
        }
    };
    private View.OnClickListener C = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: CategoryScene: click apply button!");
            RingData c = j.this.f2852b.a(j.this.c);
            if (c != null) {
                b.b().a(c, "favorite_ring_list");
                new com.shoujiduoduo.ui.settings.b(j.this.g, R.style.DuoDuoDialog, c, j.this.f2852b.a(), j.this.f2852b.b().toString()).show();
            }
        }
    };
    private View.OnClickListener D = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click collect button!");
            RingData c = j.this.f2852b.a(j.this.c);
            if (c != null) {
                b.b().a(c, "favorite_ring_list");
                d.a((int) R.string.add_favorite_suc, 0);
                ak.a(c.g, 0, "&from=" + j.this.f2852b.a() + "&listType=" + j.this.f2852b.b() + "&cucid=" + c.C + "&uid=" + b.g().c().a() + "&tuid=" + c.j);
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    Html.ImageGetter f2851a = new Html.ImageGetter() {
        public Drawable getDrawable(String str) {
            int i = 0;
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            Resources resources = RingDDApp.c().getResources();
            Drawable drawable = resources.getDrawable(i);
            drawable.setBounds(-5, -5, (int) resources.getDimension(R.dimen.hot_icon_width), (int) resources.getDimension(R.dimen.hot_icon_height));
            return drawable;
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public g f2852b;
    /* access modifiers changed from: private */
    public int c = -1;
    private String d;
    private LayoutInflater e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public Context g;
    private boolean h;
    private int i;
    private int j;
    /* access modifiers changed from: private */
    public boolean k;
    private Map<Integer, e.a> l;
    /* access modifiers changed from: private */
    public RingData m;
    private Timer n;
    private a o;
    private k p = new k() {
        public void a() {
            j.this.notifyDataSetChanged();
        }
    };
    private o q = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.j, int]
         candidates:
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, int):int
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
          com.shoujiduoduo.ui.utils.j.a(android.view.View, int):void
          com.shoujiduoduo.ui.utils.j.a(android.widget.TextView, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.j.a(boolean, com.shoujiduoduo.util.f$b):void
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.j, int]
         candidates:
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean */
        public void a(String str, int i) {
            if (j.this.f2852b != null && j.this.f2852b.a().equals(str)) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "onSetPlay, listid:" + str);
                boolean unused = j.this.f = true;
                int unused2 = j.this.c = i;
                boolean unused3 = j.this.k = true;
                j.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.j, int]
         candidates:
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, int):int
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):com.shoujiduoduo.base.bean.RingData
          com.shoujiduoduo.ui.utils.j.a(android.view.View, int):void
          com.shoujiduoduo.ui.utils.j.a(android.widget.TextView, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.a(java.lang.String, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.j.a(boolean, com.shoujiduoduo.util.f$b):void
          com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.j, int]
         candidates:
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean */
        public void b(String str, int i) {
            if (j.this.f2852b != null && j.this.f2852b.a().equals(str)) {
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "onCanclePlay, listId:" + str);
                boolean unused = j.this.f = false;
                int unused2 = j.this.c = i;
                boolean unused3 = j.this.k = true;
                j.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.j, int]
         candidates:
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, boolean):boolean */
        public void a(String str, int i, int i2) {
            if (j.this.f2852b != null && j.this.f2852b.a().equals(str)) {
                boolean unused = j.this.k = true;
                j.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener r = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                if (b2.a() == 3) {
                    b2.n();
                } else {
                    b2.i();
                }
            }
        }
    };
    private View.OnClickListener s = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                b2.j();
            }
        }
    };
    private View.OnClickListener t = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                b2.a(j.this.f2852b, j.this.c);
            }
        }
    };
    private View.OnClickListener u = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click Cailing button!");
            RingData c = j.this.f2852b.a(j.this.c);
            RingData unused = j.this.m = c;
            switch (f.s()) {
                case cm:
                    j.this.b(c);
                    return;
                case cu:
                    j.this.d(c);
                    return;
                case ct:
                    if (c.y == 1 || c.y == 2) {
                        j.this.e(c);
                        return;
                    } else {
                        c.a(j.this.g).a(view, c, j.this.f2852b.a(), j.this.f2852b.b());
                        return;
                    }
                default:
                    d.a("不支持的运营商类型!");
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public Handler v = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "init chinamobile sdk");
            j.this.d();
            com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk init succeed");
                    j.this.c(j.this.m);
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    j.this.f();
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk init failed, try sms code init");
                    j.this.b(j.this.m, "", f.b.cm);
                }
            });
        }
    };
    private ProgressDialog w = null;
    private ProgressDialog x = null;
    private View.OnClickListener y = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click share button!");
            ai.a().a((Activity) j.this.g, j.this.f2852b.a(j.this.c), j.this.f2852b.a());
        }
    };
    private View.OnClickListener z = new View.OnClickListener() {
        public void onClick(View view) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "RingtoneDuoduo: click weixiu button!");
            RingData c = j.this.f2852b.a(j.this.c);
            Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
            intent.putExtra("musicid", c.g);
            intent.putExtra("title", "快秀");
            intent.putExtra("type", MusicAlbumActivity.a.ring_story);
            RingToneDuoduoActivity.a().startActivity(intent);
        }
    };

    public j(Context context) {
        this.g = context;
        this.e = LayoutInflater.from(this.g);
        this.l = new HashMap();
        this.n = new Timer();
        this.o = new a();
    }

    public void a() {
        this.i = q.a(com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_gap"), 10);
        this.j = q.a(com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_start_pos"), 6);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.q);
        if (this.h) {
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.p);
        }
        this.n.schedule(this.o, 0, 250);
    }

    /* compiled from: RingListAdapter */
    private class a extends TimerTask {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public CircleProgressBar f2917b;

        private a() {
        }

        public void a(CircleProgressBar circleProgressBar) {
            this.f2917b = circleProgressBar;
        }

        public void run() {
            com.shoujiduoduo.a.a.c.a().a(new c.b() {
                public void a() {
                    if (a.this.f2917b != null && j.this.f) {
                        a.this.f2917b.setMax(100);
                        PlayerService b2 = z.a().b();
                        if (b2 != null) {
                            switch (b2.a()) {
                                case 1:
                                case 3:
                                case 6:
                                default:
                                    return;
                                case 2:
                                    int g = b2.g();
                                    int h = b2.h();
                                    if (g > 0) {
                                        a.this.f2917b.setProgress((int) ((100.0d * ((double) h)) / ((double) g)));
                                        return;
                                    }
                                    return;
                                case 4:
                                    a.this.f2917b.setProgress(100);
                                    return;
                                case 5:
                                    a.this.f2917b.setProgress(0);
                                    return;
                            }
                        }
                    }
                }
            });
        }
    }

    public void b() {
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.q);
        if (this.h) {
            com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, this.p);
        }
        if (this.n != null) {
            this.n.cancel();
        }
    }

    public void a(boolean z2) {
        this.h = z2;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(com.shoujiduoduo.base.bean.d dVar) {
        if (this.f2852b != dVar) {
            this.f2852b = null;
            this.f2852b = (g) dVar;
            this.f = false;
            this.k = false;
            notifyDataSetChanged();
        }
    }

    public int getItemViewType(int i2) {
        if (!this.h || i2 + 1 < this.j || ((i2 + 1) - this.j) % this.i != 0) {
            return 0;
        }
        return 1;
    }

    public int getViewTypeCount() {
        if (this.h) {
            return 2;
        }
        return 1;
    }

    public int getCount() {
        if (this.f2852b == null) {
            return 0;
        }
        if (!this.h) {
            return this.f2852b.c();
        }
        if (this.f2852b.c() > this.j) {
            return this.f2852b.c() + ((this.f2852b.c() - this.j) / (this.i - 1));
        }
        return this.f2852b.c();
    }

    public Object getItem(int i2) {
        if (this.f2852b == null || i2 < 0 || i2 >= this.f2852b.c()) {
            return null;
        }
        return this.f2852b.a(i2);
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private boolean a(RingData ringData) {
        if (!ringData.p.equals("") && f.t()) {
            return true;
        }
        if (f.v()) {
            b.c b2 = com.shoujiduoduo.util.d.b.a().b(com.shoujiduoduo.a.b.b.g().c().l());
            boolean z2 = b2 == null || !b2.f3149a || b2.f3150b;
            if (!ringData.u.equals("")) {
                return true;
            }
            if (ringData.y != 2 || !z2) {
                return false;
            }
            return true;
        } else if (!f.u() || !com.shoujiduoduo.util.e.a.a().b()) {
            return false;
        } else {
            if (ringData.A != 2) {
                return false;
            }
            return true;
        }
    }

    private void a(View view, int i2) {
        String str;
        RingData c2 = this.f2852b.a(i2);
        TextView textView = (TextView) l.a(view, R.id.item_song_name);
        TextView textView2 = (TextView) l.a(view, R.id.item_artist);
        TextView textView3 = (TextView) l.a(view, R.id.tv_duradion);
        TextView textView4 = (TextView) l.a(view, R.id.tv_play_times);
        ImageView imageView = (ImageView) l.a(view, R.id.iv_cailing);
        imageView.setVisibility(8);
        ImageView imageView2 = (ImageView) l.a(view, R.id.iv_new);
        imageView2.setVisibility(8);
        String str2 = c2.e;
        if (a(c2)) {
            imageView.setVisibility(0);
        }
        ImageView imageView3 = (ImageView) l.a(view, R.id.iv_comment_small);
        TextView textView5 = (TextView) l.a(view, R.id.tv_comment_num);
        TextView textView6 = (TextView) l.a(view, R.id.tv_comment_num_small);
        ((ImageView) l.a(view, R.id.iv_comment)).setOnClickListener(this.B);
        textView5.setOnClickListener(this.B);
        if (c2.I > 0) {
            textView5.setText("" + c2.I);
            imageView3.setVisibility(0);
            textView6.setVisibility(0);
            textView6.setText(a(c2.I));
        } else {
            imageView3.setVisibility(8);
            textView6.setVisibility(8);
            textView5.setText("评论");
        }
        if (c2.n != 0) {
            imageView2.setVisibility(0);
        }
        if (c2.H != 0) {
            Drawable drawable = this.g.getResources().getDrawable(R.drawable.icon_hot);
            drawable.setBounds(0, 0, (int) this.g.getResources().getDimension(R.dimen.hot_icon_width), (int) this.g.getResources().getDimension(R.dimen.hot_icon_height));
            al alVar = new al(drawable);
            SpannableString spannableString = new SpannableString(str2 + "  ");
            spannableString.setSpan(alVar, spannableString.length() - 1, spannableString.length(), 17);
            textView.setText(spannableString);
        } else {
            textView.setText(str2);
        }
        textView2.setText(c2.f);
        if (c2.l > 60) {
            str = "" + (c2.l / 60) + "分" + (c2.l % 60) + "秒";
        } else {
            str = "" + c2.l + "秒";
        }
        textView3.setText(str);
        textView3.setVisibility(c2.l == 0 ? 4 : 0);
        textView4.setText(b(c2.m));
        textView4.setVisibility(c2.m == 0 ? 4 : 0);
    }

    private String a(int i2) {
        if (i2 <= 10000) {
            return "" + i2;
        }
        return "" + String.format("%.1f", Double.valueOf(((double) i2) / 10000.0d)) + "万";
    }

    private String b(int i2) {
        if (i2 > 100000000) {
            return "" + String.format("%.1f", Double.valueOf(((double) i2) / 1.0E8d)) + "亿";
        } else if (i2 > 10000) {
            return ("" + (i2 / 10000)) + "万";
        } else if (i2 < 100) {
            return "少于100";
        } else {
            return "" + i2;
        }
    }

    /* access modifiers changed from: private */
    public void b(final RingData ringData) {
        e();
        com.shoujiduoduo.util.c.b.a().a(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is inited");
                if (bVar instanceof c.n) {
                    c.n nVar = (c.n) bVar;
                    if (nVar.f3073a == c.n.a.sms_code || nVar.f3073a == c.n.a.all) {
                        com.shoujiduoduo.base.bean.k c = com.shoujiduoduo.a.b.b.g().c();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "sdk 已初始化，是sms_code 方式。isLogin:" + c.i() + ", mobile:" + nVar.d);
                        if (!c.i()) {
                            c.b(nVar.d);
                            c.a("phone_" + nVar.d);
                        }
                        c.d(nVar.d);
                        c.c(1);
                        com.shoujiduoduo.a.b.b.g().a(c);
                        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                            public void a() {
                                ((v) this.f1812a).a(1, true, "", "");
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "sdk 已初始化，但不是 sms_code 方式。");
                    }
                }
                j.this.c(ringData);
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "cmcc sdk is not inited");
                j.this.c();
                j.this.v.sendEmptyMessageDelayed(1, 3000);
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData) {
        com.shoujiduoduo.util.c.b.a().e(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void
             arg types: [com.shoujiduoduo.ui.utils.j, int, com.shoujiduoduo.util.f$b]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar instanceof c.d) {
                    c.d dVar = (c.d) bVar;
                    if (dVar.e() && dVar.d()) {
                        j.this.f();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "均开通，直接订购");
                        j.this.a(true, f.b.cm);
                        j.this.b(true);
                    } else if (dVar.e() && !dVar.d()) {
                        j.this.f();
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip开通，彩铃关闭");
                        j.this.a(ringData, f.b.cm, "", true);
                    } else if (dVar.e() || !dVar.d()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "均关闭");
                        j.this.f();
                        j.this.a(ringData, "", f.b.cm, true);
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "vip关闭，彩铃开通");
                        j.this.f();
                        j.this.a(ringData, "", f.b.cm, false);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "查询状态失败");
                d.a("查询状态失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(RingData ringData) {
        com.shoujiduoduo.base.bean.k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c2.l())) {
            b(ringData, "", f.b.cu);
            return;
        }
        String l2 = c2.l();
        if (com.shoujiduoduo.util.e.a.a().a(l2)) {
            a(ringData, l2, f.b.cu);
        } else {
            b(ringData, l2, f.b.cu);
        }
    }

    /* access modifiers changed from: private */
    public void e(final RingData ringData) {
        final com.shoujiduoduo.base.bean.k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (TextUtils.isEmpty(c2.l()) || !c2.i()) {
            String h2 = f.h();
            if (!TextUtils.isEmpty(h2)) {
                b("请稍候...");
                com.shoujiduoduo.base.a.a.a("fuck", "RingListAdapter 调用 findMdnByImsi");
                com.shoujiduoduo.util.d.b.a().b(h2, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        if (bVar == null || !(bVar instanceof c.i)) {
                            j.this.f();
                            j.this.b(ringData, "", f.b.ct);
                            return;
                        }
                        j.this.f();
                        String d = ((c.i) bVar).d();
                        if (!c2.i()) {
                            c2.b(d);
                            c2.a("phone_" + d);
                        }
                        c2.d(d);
                        c2.c(1);
                        com.shoujiduoduo.a.b.b.g().a(c2);
                        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                            public void a() {
                                ((v) this.f1812a).a(1, true, "", "");
                            }
                        });
                        j.this.a(ringData, c2.l(), f.b.ct);
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        j.this.f();
                        j.this.b(ringData, "", f.b.ct);
                    }
                });
                return;
            }
            b(ringData, "", f.b.ct);
            return;
        }
        a(ringData, c2.l(), f.b.ct);
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, String str, f.b bVar) {
        if (bVar.equals(f.b.cu)) {
            a(ringData, str);
        } else if (bVar.equals(f.b.ct)) {
            b(ringData, str);
        } else if (bVar.equals(f.b.cm)) {
            c(ringData);
        }
    }

    private void a(final RingData ringData, final String str) {
        b("请稍候...");
        com.shoujiduoduo.util.e.a.a().h(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.ah)) {
                    c.ah ahVar = (c.ah) bVar;
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "user location, provinceid:" + ahVar.f3054a + ", province name:" + ahVar.d);
                    j.this.a(ringData, str, com.shoujiduoduo.util.e.a.a().c(ahVar.f3054a));
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                new b.a(j.this.g).b("设置彩铃").a("获取当前手机号信息失败，请稍候再试试。").a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final RingData ringData, final String str, final boolean z2) {
        com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void
             arg types: [com.shoujiduoduo.ui.utils.j, int, com.shoujiduoduo.util.f$b]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar == null || !(bVar instanceof c.f)) {
                    j.this.f();
                    new b.a(j.this.g).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
                    return;
                }
                c.f fVar = (c.f) bVar;
                if (fVar.d() && fVar.e()) {
                    j.this.f();
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃与会员均开通，直接订购");
                    j.this.a(true, f.b.cu);
                    new b.a(j.this.g).b("设置彩铃(免费)").a(j.this.a(ringData, f.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            j.this.b("请稍候...");
                            j.this.c(ringData, str, false);
                            dialogInterface.dismiss();
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else if (fVar.d() && !fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃开通，会员关闭，提示开通会员");
                    j.this.f();
                    if (z2 || fVar.i()) {
                        j.this.a(ringData, str, f.b.cu, false);
                    } else {
                        new b.a(j.this.g).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    }
                } else if (!fVar.d() && fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃关闭，会员开通");
                    j.this.a(true, f.b.cu);
                    if (fVar.j()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围，先 帮用户自动开通彩铃功能， net type:" + fVar.g() + ", location:" + fVar.f());
                        com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                super.a(bVar);
                                j.this.f();
                                com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能");
                                new b.a(j.this.g).b("设置彩铃(免费)").a(j.this.a(ringData, f.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                     method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                                     arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                                     candidates:
                                      com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
                                      com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        j.this.b("请稍候...");
                                        j.this.c(ringData, str, true);
                                        dialogInterface.dismiss();
                                    }
                                }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                            }

                            public void b(c.b bVar) {
                                super.b(bVar);
                                j.this.f();
                                if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                    new b.a(j.this.g).b("设置彩铃(免费)").a(j.this.a(ringData, f.b.cu)).a("确定", new DialogInterface.OnClickListener() {
                                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                                         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                                         arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                                         candidates:
                                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
                                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            j.this.b("请稍候...");
                                            j.this.c(ringData, str, true);
                                            dialogInterface.dismiss();
                                        }
                                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                                } else {
                                    new b.a(j.this.g).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                }
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通彩铃");
                        j.this.f();
                        j.this.a(ringData, f.b.cu, str, true);
                    }
                } else if (!fVar.d() && !fVar.e()) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃会员均关闭");
                    if (!z2 && !fVar.i()) {
                        new b.a(j.this.g).b("设置彩铃").a("抱歉，您所在的当前区域不支持铃声多多的彩铃功能！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    } else if (fVar.j()) {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "属于免彩铃功能费范围， 帮用户自动开通彩铃，net type:" + fVar.g() + ", location:" + fVar.f());
                        com.shoujiduoduo.util.e.a.a().e("&phone=" + str, new com.shoujiduoduo.util.b.b() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
                             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
                             candidates:
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
                            public void a(c.b bVar) {
                                super.a(bVar);
                                j.this.f();
                                com.shoujiduoduo.base.a.a.a("RingListAdapter", "成功开通彩铃基础功能, 提示开通会员");
                                j.this.a(j.this.f2852b.a(j.this.c), str, f.b.cu, false);
                            }

                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
                             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
                             candidates:
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
                              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
                            public void b(c.b bVar) {
                                super.b(bVar);
                                j.this.f();
                                if (bVar.a().equals("000001") || bVar.a().equals("301000")) {
                                    j.this.a(j.this.f2852b.a(j.this.c), str, f.b.cu, false);
                                } else {
                                    new b.a(j.this.g).b("设置彩铃").a("为您免费开通彩铃功能失败， 原因：" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                                }
                            }
                        });
                    } else {
                        com.shoujiduoduo.base.a.a.a("RingListAdapter", "不属于免彩铃功能费范围， 提示开通会员");
                        j.this.f();
                        j.this.a(ringData, f.b.cu, str, false);
                    }
                }
                if (fVar.f3065a.a().equals("40307") || fVar.f3065a.a().equals("40308")) {
                    com.shoujiduoduo.util.e.a.a().a(str, "");
                    j.this.f();
                    j.this.b(ringData, str, f.b.cu);
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
            }
        });
    }

    private void b(final RingData ringData, final String str) {
        b("请稍候...");
        com.shoujiduoduo.util.d.b.a().a(str, new com.shoujiduoduo.util.b.b() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void
             arg types: [com.shoujiduoduo.ui.utils.j, int, com.shoujiduoduo.util.f$b]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b):android.view.View
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, com.shoujiduoduo.util.f$b):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar == null || !(bVar instanceof c.e)) {
                    j.this.f();
                    new b.a(j.this.g).b("设置彩铃").a(bVar != null ? bVar.b() : "设置失败").a("确定", (DialogInterface.OnClickListener) null).a().show();
                    com.shoujiduoduo.base.a.a.c("RingListAdapter", "checkCailingAndVip failed");
                    return;
                }
                c.e eVar = (c.e) bVar;
                if (eVar.d() && (eVar.e() || eVar.f())) {
                    j.this.f();
                    j.this.a(true, f.b.ct);
                    new b.a(j.this.g).b("设置彩铃(免费)").a(j.this.a(ringData, f.b.ct)).a("确定", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            j.this.b("请稍候...");
                            j.this.d(ringData, str, false);
                            dialogInterface.dismiss();
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else if (eVar.d() && !eVar.e() && !eVar.f()) {
                    j.this.f();
                    j.this.a(ringData, str, f.b.ct, false);
                } else if (!eVar.d() && (eVar.e() || eVar.f())) {
                    j.this.a(true, f.b.ct);
                    j.this.f();
                    if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                        d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                    } else {
                        j.this.a(ringData, f.b.ct, str, true);
                    }
                } else if (!eVar.d() && !eVar.e() && !eVar.f()) {
                    j.this.f();
                    if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                        d.a("正在为您开通彩铃业务，请耐心等待一会儿...");
                    } else {
                        j.this.a(ringData, f.b.ct, str, false);
                    }
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                new b.a(j.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData, final String str) {
        com.shoujiduoduo.util.d.b.a().g(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "基础业务开通状态");
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void */
            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "彩铃基础业务尚未开通");
                j.this.f();
                if (com.shoujiduoduo.util.d.b.a().a(str).equals(b.d.wait_open)) {
                    d.a("正在为您开通业务，请耐心等待一会儿.");
                } else {
                    j.this.a(ringData, f.b.ct, str, false);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, f.b bVar) {
        final int i2;
        int i3 = 0;
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setVipState, isVip:" + z2 + ", type:" + bVar.toString());
        com.shoujiduoduo.base.bean.k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (bVar.equals(f.b.cu)) {
            i2 = 3;
        } else if (bVar.equals(f.b.ct)) {
            i2 = 2;
        } else if (bVar.equals(f.b.cm)) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        if (z2) {
            i3 = i2;
        }
        c2.b(i3);
        if (!c2.i()) {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "user is not login, phoneNum:" + c2.l());
            if (bVar != f.b.cm || !ag.c(c2.l())) {
                c2.b(c2.l());
                c2.a("phone_" + c2.l());
            } else {
                c2.b("多多VIP");
                c2.a("phone_" + com.shoujiduoduo.util.c.b.a().b());
            }
            c2.c(1);
            com.shoujiduoduo.a.b.b.g().a(c2);
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1812a).a(1, true, "", "");
                }
            });
        } else {
            com.shoujiduoduo.base.a.a.a("RingListAdapter", "user is login, update userinfo");
            com.shoujiduoduo.a.b.b.g().a(c2);
        }
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, new c.a<x>() {
            public void a() {
                ((x) this.f1812a).a(i2);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final RingData ringData, String str, f.b bVar) {
        new com.shoujiduoduo.ui.cailing.e(this.g, R.style.DuoDuoDialog, str, bVar, new e.a() {
            public void a(String str) {
                com.shoujiduoduo.base.bean.k c = com.shoujiduoduo.a.b.b.g().c();
                if (!c.i()) {
                    c.b(str);
                    c.a("phone_" + str);
                }
                c.d(str);
                c.c(1);
                com.shoujiduoduo.a.b.b.g().a(c);
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1812a).a(1, true, "", "");
                    }
                });
                j.this.a(ringData, str, f.g(str));
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(final RingData ringData, final String str, final f.b bVar, boolean z2) {
        new com.shoujiduoduo.ui.cailing.d(this.g, bVar, ringData, "ringlist", false, z2, new d.c() {
            public void a(boolean z) {
                if (z) {
                    new b.a(j.this.g).a("多多VIP会员业务已成功受理，正在为您开通。 是否设置 《" + ringData.e + "》 为您的当前彩铃？").a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.j, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (bVar.equals(f.b.cu)) {
                                j.this.c(ringData, str, true);
                            } else if (bVar.equals(f.b.ct)) {
                                j.this.d(ringData, str, true);
                            } else if (bVar.equals(f.b.cm)) {
                                j.this.b(true);
                            }
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(RingData ringData, f.b bVar, String str, boolean z2) {
        final boolean z3 = z2;
        final RingData ringData2 = ringData;
        final f.b bVar2 = bVar;
        final String str2 = str;
        new com.shoujiduoduo.ui.cailing.f(this.g, R.style.DuoDuoDialog, bVar, new f.a() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void
             arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, int]
             candidates:
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, com.shoujiduoduo.util.f$b, java.lang.String, boolean):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, boolean, java.lang.String, java.lang.String, com.shoujiduoduo.base.bean.RingData):void
              com.shoujiduoduo.ui.utils.j.a(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b, boolean):void */
            public void a(f.a.C0036a aVar) {
                if (!aVar.equals(b.d.open)) {
                    return;
                }
                if (z3) {
                    new b.a(j.this.g).a("多多会员业务已成功受理，正在为您开通。 是否设置 《" + ringData2.e + "》 为您的当前彩铃？").a("确定", new DialogInterface.OnClickListener() {
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, com.shoujiduoduo.util.f$b):void
                          com.shoujiduoduo.ui.utils.j.b(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData, java.lang.String, boolean):void */
                        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                         method: com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void
                         arg types: [com.shoujiduoduo.ui.utils.j, int]
                         candidates:
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.base.bean.RingData, java.lang.String):void
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, com.shoujiduoduo.base.bean.RingData):void
                          com.shoujiduoduo.ui.utils.j.c(com.shoujiduoduo.ui.utils.j, boolean):void */
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            if (bVar2.equals(f.b.cu)) {
                                j.this.c(ringData2, str2, true);
                            } else if (bVar2.equals(f.b.ct)) {
                                j.this.d(ringData2, str2, true);
                            } else if (bVar2.equals(f.b.cm)) {
                                j.this.b(true);
                            }
                        }
                    }).b("取消", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    j.this.a(j.this.f2852b.a(j.this.c), str2, bVar2, false);
                }
            }
        }).show();
    }

    private void b(final RingData ringData, final String str, boolean z2) {
        final StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.g).append("&from=").append(this.f2852b.a()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(ringData.e, str, ringData.z, sb.toString() + ("&info=" + ag.a("ringname:" + ringData.e)), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.aa)) {
                    c.aa aaVar = (c.aa) bVar;
                    sb.append("&info=").append(ag.a("audioId:" + aaVar.f3045a + ", ringname:" + ringData.e));
                    com.shoujiduoduo.util.d.b.a().b(aaVar.f3045a, str, sb.toString(), new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            j.this.f();
                            new b.a(j.this.g).b("设置彩铃").a("已成功设置彩铃，预计十分钟生效，稍候请留意短信提醒").a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }

                        public void b(c.b bVar) {
                            String bVar2;
                            super.b(bVar);
                            j.this.f();
                            if (bVar.a().equals("3023")) {
                                bVar2 = com.umeng.a.a.a().a(RingDDApp.c(), "ctcc_audio_check_hint");
                                if (ag.b(bVar2)) {
                                    bVar2 = "您设置的彩铃已提交电信审核，审核通过后即可生效。可能需要1-2个小时，部分省份需24小时，请耐心等待一下。";
                                }
                            } else {
                                bVar2 = bVar.toString();
                            }
                            new b.a(j.this.g).b("设置彩铃").a(bVar2).a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }
                    });
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "diy_clip_upload onFailure:" + bVar.toString());
                new b.a(j.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        com.shoujiduoduo.ui.cailing.a aVar = new com.shoujiduoduo.ui.cailing.a(this.g, R.style.DuoDuoDialog, f.b.cm, z2);
        aVar.a(this.m, this.f2852b.a(), this.f2852b.b());
        aVar.show();
    }

    /* access modifiers changed from: private */
    public void c(final RingData ringData, String str, boolean z2) {
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(ringData.g).append("&from=").append(this.f2852b.a()).append("&cucid=").append(ringData.C).append("&phone=").append(str).append("&info=").append(ag.a("ringname:" + ringData.e + ", cusid:" + ringData.E));
        com.shoujiduoduo.util.e.a.a().a(ringData.C, ringData.E, sb.toString(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃成功");
                j.this.a(ringData.C, ringData);
                ak.a(ringData.g, 6, "&from=" + j.this.f2852b.a() + "&listType=" + j.this.f2852b.b().toString() + "&cucid=" + ringData.C);
            }

            public void b(c.b bVar) {
                super.b(bVar);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "0元会员订购彩铃失败， msg:" + bVar.toString());
                if (bVar.a().equals("400033")) {
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "set default");
                    j.this.a(ringData.C, ringData);
                    ak.a(ringData.g, 6, "&from=" + j.this.f2852b.a() + "&listType=" + j.this.f2852b.b() + "&cucid=" + ringData.C);
                    return;
                }
                j.this.f();
                new b.a(j.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(final RingData ringData, final String str, final boolean z2) {
        if (ringData.y == 2) {
            b(ringData, str, z2);
            return;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&ctcid=").append(ringData.u).append("&from=").append(this.f2852b.a()).append("&phone=").append(str);
        com.shoujiduoduo.util.d.b.a().a(str, ringData.u, sb.toString(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                j.this.d(ringData, str);
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onSuccess:" + bVar.toString());
                ad.b(j.this.g, "NeedUpdateCaiLingLib", 1);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1812a).a(f.b.ct);
                    }
                });
            }

            public void b(c.b bVar) {
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "vipOrder onFailure:" + bVar.toString());
                if (bVar.a().equals("0536") || bVar.a().equals("0538") || bVar.a().equals("0531")) {
                    j.this.d(ringData, str);
                } else if (bVar.a().equals("0703")) {
                    j.this.c(ringData, str);
                } else if ((bVar.a().equals("0574") || bVar.a().equals("0015") || bVar.a().equals("9001")) && z2) {
                    com.shoujiduoduo.util.widget.d.a("正在为您开通会员业务，请稍等一会儿... ", 1);
                } else if (bVar.a().equals("0556")) {
                    new b.a(j.this.g).b("设置彩铃").a("铃音数量已经达到最大限制, 请到我的彩铃里删除部分彩铃后再购买").a("确定", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    new b.a(j.this.g).b("设置彩铃").a(bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                }
                super.b(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(final RingData ringData, String str) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, id:" + ringData.u);
        com.shoujiduoduo.util.d.b.a().c(str, ringData.u, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                j.this.f();
                new b.a(j.this.g).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                ad.c(j.this.g, "DEFAULT_CAILING_ID", ringData.u);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1812a).a(16, ringData);
                    }
                });
                super.a(bVar);
            }

            public void b(c.b bVar) {
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
                new b.a(j.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                super.b(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final String str, final RingData ringData) {
        com.shoujiduoduo.base.a.a.a("RingListAdapter", "设置默认铃音");
        com.shoujiduoduo.util.e.a.a().h(new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                String str;
                boolean z = false;
                super.a(bVar);
                if (bVar != null && (bVar instanceof c.s)) {
                    c.s sVar = (c.s) bVar;
                    if (sVar.d != null) {
                        int i = 0;
                        while (true) {
                            if (i >= sVar.d.length) {
                                break;
                            } else if (sVar.d[i].c.equals("0")) {
                                z = true;
                                str = sVar.d[i].f3058a;
                                break;
                            } else {
                                i++;
                            }
                        }
                    }
                    str = "";
                    com.shoujiduoduo.base.a.a.a("RingListAdapter", "是否有timetype=0的默认铃声：" + z);
                    j.this.a(z, str, str, ringData);
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                com.shoujiduoduo.base.a.a.c("RingListAdapter", "查询用户铃音设置失败");
                new b.a(j.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(boolean z2, String str, final String str2, final RingData ringData) {
        com.shoujiduoduo.util.e.a.a().a(str2, z2, str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                j.this.f();
                new b.a(j.this.g).b("设置彩铃").a("已成功设置为您的当前彩铃.赶快试试吧！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                ad.c(j.this.g, "DEFAULT_CAILING_ID", str2);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1812a).a(16, ringData);
                    }
                });
                ad.b(j.this.g, "NeedUpdateCaiLingLib", 1);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1812a).a(f.b.cu);
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                j.this.f();
                com.shoujiduoduo.base.a.a.a("RingListAdapter", "setDefaultCtccCailing, onFailure, " + bVar.toString());
                new b.a(j.this.g).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public View a(RingData ringData, f.b bVar) {
        View inflate = LayoutInflater.from(this.g).inflate((int) R.layout.layout_cailing_info, (ViewGroup) null, false);
        ((ListView) inflate.findViewById(R.id.cailing_info_list)).setAdapter((ListAdapter) new SimpleAdapter(this.g, b(ringData, bVar), R.layout.listitem_cailing_info, new String[]{"cailing_info_des", "divider", "cailing_info_content"}, new int[]{R.id.cailing_info_des, R.id.devider, R.id.cailing_info_content}));
        return inflate;
    }

    private ArrayList<Map<String, Object>> b(RingData ringData, f.b bVar) {
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        HashMap hashMap = new HashMap();
        hashMap.put("cailing_info_des", "歌名");
        hashMap.put("divider", ":");
        hashMap.put("cailing_info_content", ringData.e);
        arrayList.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("cailing_info_des", "歌手");
        hashMap2.put("divider", ":");
        hashMap2.put("cailing_info_content", ringData.f);
        arrayList.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("cailing_info_des", "有效期");
        hashMap3.put("divider", ":");
        String str = "";
        switch (bVar) {
            case cm:
                str = ringData.q;
                break;
            case cu:
                if (ringData.A != 1) {
                    str = ringData.D;
                    break;
                } else {
                    str = g();
                    break;
                }
            case ct:
                if (ringData.y != 2) {
                    str = ringData.v;
                    break;
                } else {
                    str = g();
                    break;
                }
        }
        hashMap3.put("cailing_info_content", str);
        arrayList.add(hashMap3);
        return arrayList;
    }

    @SuppressLint({"SimpleDateFormat"})
    private String g() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        Date date = new Date();
        return simpleDateFormat.format(new Date(date.getYear() + 1, date.getMonth(), date.getDate()));
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.w == null) {
            this.w = new ProgressDialog(this.g);
            this.w.setMessage("您好，初始化彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            this.w.setIndeterminate(false);
            this.w.setCancelable(true);
            this.w.setCanceledOnTouchOutside(false);
            this.w.setButton(-1, "确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            this.w.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.w != null) {
            this.w.dismiss();
            this.w = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void e() {
        b("请稍候...");
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.x == null) {
            this.x = new ProgressDialog(this.g);
            this.x.setMessage(str);
            this.x.setIndeterminate(false);
            this.x.setCancelable(true);
            this.x.setCanceledOnTouchOutside(false);
            this.x.show();
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.x != null) {
            this.x.dismiss();
            this.x = null;
        }
    }

    private int c(int i2) {
        if (!this.h || i2 + 1 < this.j) {
            return i2;
        }
        return i2 - ((((i2 + 1) - this.j) / this.i) + 1);
    }

    private void a(TextView textView, String str) {
        com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_layout_type");
        textView.setText(str);
    }

    private int h() {
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_layout_type");
        if (a2.equals("1")) {
            return R.layout.listitem_feed_ad;
        }
        if (a2.equals("2") || a2.equals("3")) {
        }
        return R.layout.listitem_feed_ad2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0238  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x02f7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r23, android.view.View r24, android.view.ViewGroup r25) {
        /*
            r22 = this;
            r0 = r22
            com.shoujiduoduo.b.c.g r3 = r0.f2852b
            if (r3 != 0) goto L_0x0009
            r24 = 0
        L_0x0008:
            return r24
        L_0x0009:
            int r3 = r22.getItemViewType(r23)
            if (r3 != 0) goto L_0x03d0
            if (r24 != 0) goto L_0x003f
            r0 = r22
            android.view.LayoutInflater r3 = r0.e
            r4 = 2130903194(0x7f03009a, float:1.74132E38)
            r5 = 0
            r0 = r25
            android.view.View r24 = r3.inflate(r4, r0, r5)
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            java.lang.String r4 = "ring_tag"
            r0 = r24
            r0.setTag(r3, r4)
        L_0x0029:
            int r3 = r22.c(r23)
            r0 = r22
            com.shoujiduoduo.b.c.g r4 = r0.f2852b
            int r4 = r4.c()
            if (r3 < r4) goto L_0x0076
            java.lang.String r3 = "RingListAdapter"
            java.lang.String r4 = "fuck, 越界了"
            com.shoujiduoduo.base.a.a.a(r3, r4)
            goto L_0x0008
        L_0x003f:
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            r0 = r24
            java.lang.Object r3 = r0.getTag(r3)
            if (r3 == 0) goto L_0x0029
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "ring_tag"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0029
            java.lang.String r3 = "RingListAdapter"
            java.lang.String r4 = "View type is ring , but tag is not ring tag"
            com.shoujiduoduo.base.a.a.a(r3, r4)
            r0 = r22
            android.view.LayoutInflater r3 = r0.e
            r4 = 2130903194(0x7f03009a, float:1.74132E38)
            r5 = 0
            r0 = r25
            android.view.View r24 = r3.inflate(r4, r0, r5)
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            java.lang.String r4 = "ring_tag"
            r0 = r24
            r0.setTag(r3, r4)
            goto L_0x0029
        L_0x0076:
            int r3 = r22.c(r23)
            r0 = r22
            r1 = r24
            r0.a(r1, r3)
            r3 = 2131493661(0x7f0c031d, float:1.8610808E38)
            r0 = r24
            android.view.View r3 = com.shoujiduoduo.ui.utils.l.a(r0, r3)
            android.widget.ProgressBar r3 = (android.widget.ProgressBar) r3
            r4 = 2131493660(0x7f0c031c, float:1.8610806E38)
            r0 = r24
            android.view.View r4 = com.shoujiduoduo.ui.utils.l.a(r0, r4)
            com.shoujiduoduo.util.widget.CircleProgressBar r4 = (com.shoujiduoduo.util.widget.CircleProgressBar) r4
            r5 = 2131493659(0x7f0c031b, float:1.8610804E38)
            r0 = r24
            android.view.View r5 = com.shoujiduoduo.ui.utils.l.a(r0, r5)
            android.widget.TextView r5 = (android.widget.TextView) r5
            r6 = 2131493662(0x7f0c031e, float:1.861081E38)
            r0 = r24
            android.view.View r6 = com.shoujiduoduo.ui.utils.l.a(r0, r6)
            android.widget.ImageButton r6 = (android.widget.ImageButton) r6
            r7 = 2131493663(0x7f0c031f, float:1.8610813E38)
            r0 = r24
            android.view.View r7 = com.shoujiduoduo.ui.utils.l.a(r0, r7)
            android.widget.ImageButton r7 = (android.widget.ImageButton) r7
            r8 = 2131493664(0x7f0c0320, float:1.8610815E38)
            r0 = r24
            android.view.View r8 = com.shoujiduoduo.ui.utils.l.a(r0, r8)
            android.widget.ImageButton r8 = (android.widget.ImageButton) r8
            r0 = r22
            android.view.View$OnClickListener r9 = r0.r
            r6.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.s
            r7.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.t
            r8.setOnClickListener(r9)
            java.lang.String r9 = ""
            com.shoujiduoduo.util.z r10 = com.shoujiduoduo.util.z.a()
            com.shoujiduoduo.player.PlayerService r18 = r10.b()
            if (r18 == 0) goto L_0x00f0
            java.lang.String r9 = r18.b()
            int r10 = r18.c()
            r0 = r22
            r0.c = r10
        L_0x00f0:
            r0 = r22
            com.shoujiduoduo.b.c.g r10 = r0.f2852b
            java.lang.String r10 = r10.a()
            boolean r9 = r9.equals(r10)
            if (r9 == 0) goto L_0x032b
            int r9 = r22.c(r23)
            r0 = r22
            int r10 = r0.c
            if (r9 != r10) goto L_0x032b
            r0 = r22
            boolean r9 = r0.f
            if (r9 == 0) goto L_0x032b
            r0 = r22
            com.shoujiduoduo.b.c.g r9 = r0.f2852b
            int r10 = r22.c(r23)
            com.shoujiduoduo.base.bean.RingData r19 = r9.a(r10)
            r9 = 2131493699(0x7f0c0343, float:1.8610886E38)
            r0 = r24
            android.view.View r9 = com.shoujiduoduo.ui.utils.l.a(r0, r9)
            com.shoujiduoduo.util.widget.MyButton r9 = (com.shoujiduoduo.util.widget.MyButton) r9
            r10 = 2131493698(0x7f0c0342, float:1.8610883E38)
            r0 = r24
            android.view.View r10 = com.shoujiduoduo.ui.utils.l.a(r0, r10)
            com.shoujiduoduo.util.widget.MyButton r10 = (com.shoujiduoduo.util.widget.MyButton) r10
            r11 = 2131493697(0x7f0c0341, float:1.8610881E38)
            r0 = r24
            android.view.View r11 = com.shoujiduoduo.ui.utils.l.a(r0, r11)
            com.shoujiduoduo.util.widget.MyButton r11 = (com.shoujiduoduo.util.widget.MyButton) r11
            r12 = 2131493700(0x7f0c0344, float:1.8610888E38)
            r0 = r24
            android.view.View r12 = com.shoujiduoduo.ui.utils.l.a(r0, r12)
            com.shoujiduoduo.util.widget.MyButton r12 = (com.shoujiduoduo.util.widget.MyButton) r12
            r13 = 2131493701(0x7f0c0345, float:1.861089E38)
            r0 = r24
            android.view.View r13 = com.shoujiduoduo.ui.utils.l.a(r0, r13)
            com.shoujiduoduo.util.widget.MyButton r13 = (com.shoujiduoduo.util.widget.MyButton) r13
            r14 = 2131493714(0x7f0c0352, float:1.8610916E38)
            r0 = r24
            android.view.View r14 = com.shoujiduoduo.ui.utils.l.a(r0, r14)
            com.shoujiduoduo.util.widget.MyButton r14 = (com.shoujiduoduo.util.widget.MyButton) r14
            r15 = 2131493693(0x7f0c033d, float:1.8610873E38)
            r0 = r24
            android.view.View r15 = com.shoujiduoduo.ui.utils.l.a(r0, r15)
            android.widget.TextView r15 = (android.widget.TextView) r15
            boolean r16 = r19.j()
            if (r16 == 0) goto L_0x02d0
            r16 = 0
        L_0x016f:
            r0 = r16
            r9.setVisibility(r0)
            r0 = r19
            int r0 = r0.G
            r16 = r0
            r17 = 1
            r0 = r16
            r1 = r17
            if (r0 != r1) goto L_0x02d4
            r16 = 0
        L_0x0184:
            r0 = r16
            r13.setVisibility(r0)
            r16 = 0
            r0 = r16
            r10.setVisibility(r0)
            r16 = 0
            r0 = r16
            r11.setVisibility(r0)
            r0 = r22
            r1 = r19
            boolean r16 = r0.a(r1)
            if (r16 == 0) goto L_0x02d8
            r16 = 0
        L_0x01a3:
            r0 = r16
            r12.setVisibility(r0)
            r0 = r19
            java.lang.String r0 = r0.j
            r16 = r0
            boolean r16 = com.shoujiduoduo.util.ag.c(r16)
            if (r16 == 0) goto L_0x02dc
            r16 = 8
            r0 = r16
            r14.setVisibility(r0)
            r16 = 0
            r17 = r16
        L_0x01bf:
            r16 = 2131493041(0x7f0c00b1, float:1.860955E38)
            r0 = r24
            r1 = r16
            android.view.View r16 = com.shoujiduoduo.ui.utils.l.a(r0, r1)
            android.widget.RelativeLayout r16 = (android.widget.RelativeLayout) r16
            if (r17 == 0) goto L_0x0301
            r17 = 0
            r16.setVisibility(r17)
            r17 = 8
            r0 = r17
            r15.setVisibility(r0)
            r15 = 2131493715(0x7f0c0353, float:1.8610918E38)
            r0 = r16
            android.view.View r15 = r0.findViewById(r15)
            android.widget.ImageView r15 = (android.widget.ImageView) r15
            r17 = 2131493716(0x7f0c0354, float:1.861092E38)
            android.view.View r16 = r16.findViewById(r17)
            android.widget.TextView r16 = (android.widget.TextView) r16
            r0 = r22
            android.view.View$OnClickListener r0 = r0.A
            r17 = r0
            r0 = r17
            r15.setOnClickListener(r0)
            r0 = r22
            android.view.View$OnClickListener r0 = r0.A
            r17 = r0
            r16.setOnClickListener(r17)
            r0 = r22
            java.lang.String r0 = r0.d
            r17 = r0
            if (r17 == 0) goto L_0x02e9
            r0 = r22
            java.lang.String r0 = r0.d
            r17 = r0
            r0 = r19
            java.lang.String r0 = r0.j
            r20 = r0
            r0 = r17
            r1 = r20
            boolean r17 = r0.equals(r1)
            if (r17 == 0) goto L_0x02e9
            r17 = 4
            r0 = r17
            r15.setVisibility(r0)
            r17 = 4
            r16.setVisibility(r17)
        L_0x022c:
            r0 = r19
            java.lang.String r0 = r0.k
            r17 = r0
            boolean r17 = com.shoujiduoduo.util.ag.c(r17)
            if (r17 != 0) goto L_0x02f7
            com.d.a.b.d r17 = com.d.a.b.d.a()
            r0 = r19
            java.lang.String r0 = r0.k
            r20 = r0
            com.shoujiduoduo.ui.utils.g r21 = com.shoujiduoduo.ui.utils.g.a()
            com.d.a.b.c r21 = r21.e()
            r0 = r17
            r1 = r20
            r2 = r21
            r0.a(r1, r15, r2)
        L_0x0253:
            r0 = r19
            java.lang.String r15 = r0.f
            r0 = r16
            r0.setText(r15)
        L_0x025c:
            r0 = r22
            android.view.View$OnClickListener r15 = r0.y
            r9.setOnClickListener(r15)
            r9 = 2130837975(0x7f0201d7, float:1.728092E38)
            r15 = 0
            r16 = 0
            r17 = 0
            r0 = r16
            r1 = r17
            r10.setCompoundDrawablesWithIntrinsicBounds(r9, r15, r0, r1)
            r9 = 2131099782(0x7f060086, float:1.7811927E38)
            r10.setText(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.D
            r10.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.C
            r11.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.u
            r12.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.z
            r13.setOnClickListener(r9)
            r0 = r22
            android.view.View$OnClickListener r9 = r0.A
            r14.setOnClickListener(r9)
            r9 = 4
            r5.setVisibility(r9)
            r5 = 4
            r3.setVisibility(r5)
            r5 = 4
            r6.setVisibility(r5)
            r5 = 4
            r7.setVisibility(r5)
            r5 = 4
            r8.setVisibility(r5)
            r5 = 4
            r4.setVisibility(r5)
            r0 = r22
            com.shoujiduoduo.ui.utils.j$a r5 = r0.o
            r5.a(r4)
            r5 = 5
            if (r18 == 0) goto L_0x02c1
            int r5 = r18.a()
        L_0x02c1:
            switch(r5) {
                case 1: goto L_0x02c6;
                case 2: goto L_0x0317;
                case 3: goto L_0x030d;
                case 4: goto L_0x030d;
                case 5: goto L_0x030d;
                case 6: goto L_0x0321;
                default: goto L_0x02c4;
            }
        L_0x02c4:
            goto L_0x0008
        L_0x02c6:
            r5 = 0
            r3.setVisibility(r5)
            r3 = 4
            r4.setVisibility(r3)
            goto L_0x0008
        L_0x02d0:
            r16 = 8
            goto L_0x016f
        L_0x02d4:
            r16 = 8
            goto L_0x0184
        L_0x02d8:
            r16 = 8
            goto L_0x01a3
        L_0x02dc:
            r16 = 8
            r0 = r16
            r14.setVisibility(r0)
            r16 = 1
            r17 = r16
            goto L_0x01bf
        L_0x02e9:
            r17 = 0
            r0 = r17
            r15.setVisibility(r0)
            r17 = 0
            r16.setVisibility(r17)
            goto L_0x022c
        L_0x02f7:
            r17 = 2130837929(0x7f0201a9, float:1.7280826E38)
            r0 = r17
            r15.setImageResource(r0)
            goto L_0x0253
        L_0x0301:
            r17 = 8
            r16.setVisibility(r17)
            r16 = 0
            r15.setVisibility(r16)
            goto L_0x025c
        L_0x030d:
            r3 = 0
            r6.setVisibility(r3)
            r3 = 0
            r4.setVisibility(r3)
            goto L_0x0008
        L_0x0317:
            r3 = 0
            r7.setVisibility(r3)
            r3 = 0
            r4.setVisibility(r3)
            goto L_0x0008
        L_0x0321:
            r3 = 0
            r8.setVisibility(r3)
            r3 = 4
            r4.setVisibility(r3)
            goto L_0x0008
        L_0x032b:
            r9 = 2131493699(0x7f0c0343, float:1.8610886E38)
            r0 = r24
            android.view.View r9 = com.shoujiduoduo.ui.utils.l.a(r0, r9)
            com.shoujiduoduo.util.widget.MyButton r9 = (com.shoujiduoduo.util.widget.MyButton) r9
            r10 = 2131493698(0x7f0c0342, float:1.8610883E38)
            r0 = r24
            android.view.View r10 = com.shoujiduoduo.ui.utils.l.a(r0, r10)
            com.shoujiduoduo.util.widget.MyButton r10 = (com.shoujiduoduo.util.widget.MyButton) r10
            r11 = 2131493697(0x7f0c0341, float:1.8610881E38)
            r0 = r24
            android.view.View r11 = com.shoujiduoduo.ui.utils.l.a(r0, r11)
            com.shoujiduoduo.util.widget.MyButton r11 = (com.shoujiduoduo.util.widget.MyButton) r11
            r12 = 2131493700(0x7f0c0344, float:1.8610888E38)
            r0 = r24
            android.view.View r12 = com.shoujiduoduo.ui.utils.l.a(r0, r12)
            com.shoujiduoduo.util.widget.MyButton r12 = (com.shoujiduoduo.util.widget.MyButton) r12
            r13 = 2131493701(0x7f0c0345, float:1.861089E38)
            r0 = r24
            android.view.View r13 = com.shoujiduoduo.ui.utils.l.a(r0, r13)
            com.shoujiduoduo.util.widget.MyButton r13 = (com.shoujiduoduo.util.widget.MyButton) r13
            r14 = 2131493714(0x7f0c0352, float:1.8610916E38)
            r0 = r24
            android.view.View r14 = com.shoujiduoduo.ui.utils.l.a(r0, r14)
            com.shoujiduoduo.util.widget.MyButton r14 = (com.shoujiduoduo.util.widget.MyButton) r14
            r15 = 2131493041(0x7f0c00b1, float:1.860955E38)
            r0 = r24
            android.view.View r15 = com.shoujiduoduo.ui.utils.l.a(r0, r15)
            android.widget.RelativeLayout r15 = (android.widget.RelativeLayout) r15
            r16 = 2131493693(0x7f0c033d, float:1.8610873E38)
            r0 = r24
            r1 = r16
            android.view.View r16 = com.shoujiduoduo.ui.utils.l.a(r0, r1)
            android.widget.TextView r16 = (android.widget.TextView) r16
            r17 = 0
            r16.setVisibility(r17)
            r16 = 8
            r15.setVisibility(r16)
            r15 = 8
            r9.setVisibility(r15)
            r9 = 8
            r10.setVisibility(r9)
            r9 = 8
            r11.setVisibility(r9)
            r9 = 8
            r12.setVisibility(r9)
            r9 = 8
            r13.setVisibility(r9)
            r9 = 8
            r14.setVisibility(r9)
            int r9 = r23 + 1
            java.lang.String r9 = java.lang.Integer.toString(r9)
            r5.setText(r9)
            r9 = 0
            r5.setVisibility(r9)
            r5 = 4
            r3.setVisibility(r5)
            r3 = 4
            r4.setVisibility(r3)
            r3 = 4
            r6.setVisibility(r3)
            r3 = 4
            r7.setVisibility(r3)
            r3 = 4
            r8.setVisibility(r3)
            goto L_0x0008
        L_0x03d0:
            r4 = 1
            if (r3 != r4) goto L_0x0008
            if (r24 != 0) goto L_0x04d8
            r0 = r22
            android.view.LayoutInflater r3 = r0.e
            int r4 = r22.h()
            r5 = 0
            r0 = r25
            android.view.View r24 = r3.inflate(r4, r0, r5)
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            java.lang.String r4 = "ad_tag"
            r0 = r24
            r0.setTag(r3, r4)
        L_0x03ee:
            r3 = 2131493617(0x7f0c02f1, float:1.861072E38)
            r0 = r24
            android.view.View r3 = r0.findViewById(r3)
            android.widget.TextView r3 = (android.widget.TextView) r3
            r4 = 2131492952(0x7f0c0058, float:1.860937E38)
            r0 = r24
            android.view.View r4 = r0.findViewById(r4)
            android.widget.TextView r4 = (android.widget.TextView) r4
            r5 = 2131493022(0x7f0c009e, float:1.8609512E38)
            r0 = r24
            android.view.View r5 = r0.findViewById(r5)
            android.widget.TextView r5 = (android.widget.TextView) r5
            r6 = 2131493021(0x7f0c009d, float:1.860951E38)
            r0 = r24
            android.view.View r6 = r0.findViewById(r6)
            android.widget.ImageView r6 = (android.widget.ImageView) r6
            r7 = 2131493347(0x7f0c01e3, float:1.8610172E38)
            r0 = r24
            android.view.View r7 = r0.findViewById(r7)
            android.widget.ImageView r7 = (android.widget.ImageView) r7
            com.umeng.a.a r8 = com.umeng.a.a.a()
            android.content.Context r9 = com.shoujiduoduo.ringtone.RingDDApp.c()
            java.lang.String r10 = "feed_ad_layout_type"
            java.lang.String r8 = r8.a(r9, r10)
            java.lang.String r9 = "3"
            boolean r8 = r8.equals(r9)
            if (r8 == 0) goto L_0x0440
            r8 = 8
            r6.setVisibility(r8)
        L_0x0440:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = ""
            java.lang.StringBuilder r8 = r8.append(r9)
            int r9 = r23 + 1
            java.lang.StringBuilder r8 = r8.append(r9)
            java.lang.String r8 = r8.toString()
            r3.setText(r8)
            r0 = r22
            boolean r3 = r0.k
            if (r3 != 0) goto L_0x04d1
            r0 = r22
            java.util.Map<java.lang.Integer, com.shoujiduoduo.b.a.e$a> r3 = r0.l
            java.lang.Integer r8 = java.lang.Integer.valueOf(r23)
            boolean r3 = r3.containsKey(r8)
            if (r3 == 0) goto L_0x0511
            r0 = r22
            java.util.Map<java.lang.Integer, com.shoujiduoduo.b.a.e$a> r3 = r0.l
            java.lang.Integer r8 = java.lang.Integer.valueOf(r23)
            java.lang.Object r3 = r3.get(r8)
            com.shoujiduoduo.b.a.e$a r3 = (com.shoujiduoduo.b.a.e.a) r3
            r3.c()
            boolean r8 = r3.d()
            if (r8 == 0) goto L_0x048e
            r0 = r22
            java.util.Map<java.lang.Integer, com.shoujiduoduo.b.a.e$a> r8 = r0.l
            java.lang.Integer r9 = java.lang.Integer.valueOf(r23)
            r8.remove(r9)
        L_0x048e:
            if (r3 == 0) goto L_0x0531
            int r8 = r3.a()
            r9 = 4
            if (r8 != r9) goto L_0x0531
            r8 = 0
            r7.setVisibility(r8)
        L_0x049b:
            if (r3 == 0) goto L_0x0537
            java.lang.String r7 = r3.e()
            r0 = r22
            r0.a(r4, r7)
            java.lang.String r4 = r3.f()
            r5.setText(r4)
            com.d.a.b.d r4 = com.d.a.b.d.a()
            java.lang.String r5 = r3.g()
            com.shoujiduoduo.ui.utils.g r7 = com.shoujiduoduo.ui.utils.g.a()
            com.d.a.b.c r7 = r7.k()
            r4.a(r5, r6, r7)
            r0 = r24
            r3.a(r0)
            com.shoujiduoduo.ui.utils.j$29 r4 = new com.shoujiduoduo.ui.utils.j$29
            r0 = r22
            r4.<init>(r3)
            r0 = r24
            r0.setOnClickListener(r4)
        L_0x04d1:
            r3 = 0
            r0 = r22
            r0.k = r3
            goto L_0x0008
        L_0x04d8:
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            r0 = r24
            java.lang.Object r3 = r0.getTag(r3)
            if (r3 == 0) goto L_0x03ee
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "ad_tag"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x03ee
            java.lang.String r3 = "RingListAdapter"
            java.lang.String r4 = "View type is ad , but tag is not ad tag"
            com.shoujiduoduo.base.a.a.a(r3, r4)
            r0 = r22
            android.view.LayoutInflater r3 = r0.e
            int r4 = r22.h()
            r5 = 0
            r0 = r25
            android.view.View r24 = r3.inflate(r4, r0, r5)
            r3 = 2131492874(0x7f0c000a, float:1.8609212E38)
            java.lang.String r4 = "ad_tag"
            r0 = r24
            r0.setTag(r3, r4)
            goto L_0x03ee
        L_0x0511:
            com.shoujiduoduo.b.a.f r3 = com.shoujiduoduo.a.b.b.c()
            com.shoujiduoduo.b.a.e$a r3 = r3.i()
            if (r3 == 0) goto L_0x048e
            r3.c()
            boolean r8 = r3.d()
            if (r8 != 0) goto L_0x048e
            r0 = r22
            java.util.Map<java.lang.Integer, com.shoujiduoduo.b.a.e$a> r8 = r0.l
            java.lang.Integer r9 = java.lang.Integer.valueOf(r23)
            r8.put(r9, r3)
            goto L_0x048e
        L_0x0531:
            r8 = 4
            r7.setVisibility(r8)
            goto L_0x049b
        L_0x0537:
            java.lang.String r3 = "RingListAdapter"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "can not get valid feed ad, pos:"
            java.lang.StringBuilder r7 = r7.append(r8)
            int r8 = r23 + 1
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            com.shoujiduoduo.base.a.a.e(r3, r7)
            com.shoujiduoduo.b.a.f r3 = com.shoujiduoduo.a.b.b.c()
            boolean r3 = r3.f()
            if (r3 == 0) goto L_0x0590
            com.shoujiduoduo.b.a.f r3 = com.shoujiduoduo.a.b.b.c()
            com.shoujiduoduo.b.a.g$a r3 = r3.g()
            if (r3 == 0) goto L_0x04d1
            java.lang.String r7 = r3.f1840a
            r0 = r22
            r0.a(r4, r7)
            java.lang.String r4 = r3.c
            r5.setText(r4)
            com.d.a.b.d r4 = com.d.a.b.d.a()
            java.lang.String r5 = r3.d
            com.shoujiduoduo.ui.utils.g r7 = com.shoujiduoduo.ui.utils.g.a()
            com.d.a.b.c r7 = r7.j()
            r4.a(r5, r6, r7)
            com.shoujiduoduo.ui.utils.j$30 r4 = new com.shoujiduoduo.ui.utils.j$30
            r0 = r22
            r4.<init>(r3)
            r0 = r24
            r0.setOnClickListener(r4)
            goto L_0x04d1
        L_0x0590:
            java.lang.String r3 = "RingListAdapter"
            java.lang.String r7 = "检索广告数据尚未获取"
            com.shoujiduoduo.base.a.a.a(r3, r7)
            java.lang.String r3 = "儿歌多多"
            r0 = r22
            r0.a(r4, r3)
            java.lang.String r3 = "多多团队出品，最好的儿歌故事类应用"
            r5.setText(r3)
            r3 = 2130837843(0x7f020153, float:1.7280652E38)
            r6.setImageResource(r3)
            com.shoujiduoduo.ui.utils.j$31 r3 = new com.shoujiduoduo.ui.utils.j$31
            r0 = r22
            r3.<init>()
            r0 = r24
            r0.setOnClickListener(r3)
            goto L_0x04d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.j.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }
}
