package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.s;

/* compiled from: UserListAdapter */
public class k extends d {

    /* renamed from: a  reason: collision with root package name */
    private com.shoujiduoduo.b.c.k f2918a;

    /* renamed from: b  reason: collision with root package name */
    private Context f2919b;
    private String c;
    private boolean d;

    public k(Context context) {
        this.f2919b = context;
    }

    public void a(String str) {
        this.c = str;
        this.d = this.c != null && this.c.equals(b.g().f());
    }

    public void a(boolean z) {
    }

    public void a(d dVar) {
        this.f2918a = (com.shoujiduoduo.b.c.k) dVar;
    }

    public void a() {
    }

    public void b() {
    }

    public int getCount() {
        if (this.f2918a != null) {
            return this.f2918a.c();
        }
        a.a("UserListAdapter", "count:0");
        return 0;
    }

    public Object getItem(int i) {
        if (this.f2918a != null) {
            return this.f2918a.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.f2919b).inflate((int) R.layout.listitem_fans_follow, viewGroup, false);
        }
        Button button = (Button) l.a(view, R.id.btn_follow);
        UserData userData = (UserData) this.f2918a.a(i);
        com.d.a.b.d.a().a(userData.f1956b, (ImageView) l.a(view, R.id.user_head), g.a().e());
        ((TextView) l.a(view, R.id.user_name)).setText(userData.f1955a);
        final String str = userData.c;
        if (this.d) {
            button.setText("取消关注");
        } else {
            button.setText("关注");
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View view) {
                String charSequence = ((Button) view).getText().toString();
                com.shoujiduoduo.base.bean.k c = b.g().c();
                if ("关注".equals(charSequence)) {
                    s.a("follow", "&uid=" + c.a() + "&tuid=" + str, new s.a() {
                        public void a(String str) {
                            ((Button) view).setText("取消关注");
                            com.shoujiduoduo.util.widget.d.a("关注成功");
                        }

                        public void a(String str, String str2) {
                            com.shoujiduoduo.util.widget.d.a("关注失败");
                        }
                    });
                } else {
                    s.a("unfollow", "&uid=" + c.a() + "&tuid=" + str, new s.a() {
                        public void a(String str) {
                            ((Button) view).setText("关注");
                            com.shoujiduoduo.util.widget.d.a("取消关注成功");
                        }

                        public void a(String str, String str2) {
                            com.shoujiduoduo.util.widget.d.a("取消失败");
                        }
                    });
                }
            }
        });
        return view;
    }
}
