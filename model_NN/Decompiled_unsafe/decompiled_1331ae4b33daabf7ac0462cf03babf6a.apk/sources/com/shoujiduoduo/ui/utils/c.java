package com.shoujiduoduo.ui.utils;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.util.b;
import com.shoujiduoduo.util.d;
import com.shoujiduoduo.util.o;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;

/* compiled from: AsyncImageLoader */
public class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2828a = c.class.getSimpleName();

    /* compiled from: AsyncImageLoader */
    public interface a {
        void a(Drawable drawable, String str);
    }

    public static Drawable a(final String str, final a aVar) {
        String lowerCase = str.substring(str.lastIndexOf(46)).toLowerCase();
        final String b2 = o.b();
        com.shoujiduoduo.base.a.a.a(f2828a, "Cache Path = " + b2);
        if (b2.length() != 0) {
            b2 = b2 + b.a(str, "UTF-8", "mobile") + lowerCase;
            com.shoujiduoduo.base.a.a.a(f2828a, str);
            com.shoujiduoduo.base.a.a.a(f2828a, b2);
            File file = new File(b2);
            if (file != null && file.exists() && file.canRead()) {
                com.shoujiduoduo.base.a.a.a(f2828a, "Cache File Exist!");
                Drawable createFromPath = Drawable.createFromPath(b2);
                if (createFromPath == null) {
                    a("Load AdImage from cache Failed! cachePath = " + b2 + "; url = " + str);
                }
                return createFromPath;
            }
        }
        final AnonymousClass1 r1 = new Handler() {
            public void handleMessage(Message message) {
                aVar.a((Drawable) message.obj, str);
            }
        };
        new Thread() {
            public void run() {
                Drawable a2 = c.a(str, b2);
                com.shoujiduoduo.base.a.a.a(c.f2828a, "drawable = " + a2);
                r1.sendMessage(r1.obtainMessage(0, a2));
            }
        }.start();
        return null;
    }

    public static Drawable a(String str, String str2) {
        boolean z;
        Drawable drawable = null;
        com.shoujiduoduo.base.a.a.a(f2828a + ":loadImageFromUrl", "savePath = " + str2);
        try {
            InputStream inputStream = (InputStream) new URL(str).getContent();
            if (str2.length() > 0) {
                byte[] a2 = d.a(inputStream);
                com.shoujiduoduo.base.a.a.a(f2828a + ":loadImageFromUrl", str2);
                FileOutputStream fileOutputStream = new FileOutputStream(str2);
                fileOutputStream.write(a2);
                fileOutputStream.close();
            }
            drawable = Drawable.createFromPath(str2);
            if (inputStream != null) {
                inputStream.close();
            }
            z = false;
        } catch (Exception e) {
            a("Load AdImage from network failed, Exception caught! url = " + str + "; path = " + str2 + "\n" + com.shoujiduoduo.base.a.b.a(e));
            z = true;
        }
        if (drawable == null && !z) {
            a("Load AdImage from network failed, da = 0! url = " + str + "; path = " + str2);
        }
        return drawable;
    }

    private static void a(String str) {
    }
}
