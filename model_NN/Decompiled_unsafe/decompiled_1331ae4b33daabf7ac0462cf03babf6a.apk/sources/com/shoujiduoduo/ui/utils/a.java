package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.b.a.g;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.v;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: AdHeaderViewHolder */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private View f2821a;

    /* renamed from: b  reason: collision with root package name */
    private RelativeLayout f2822b;
    private TextView c;
    private TextView d;
    private ImageView e;
    private Button f;
    /* access modifiers changed from: private */
    public g.a g;
    private Context h;

    public a(Context context) {
        this.h = context;
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void b() {
        this.f2821a = LayoutInflater.from(this.h).inflate((int) R.layout.listitem_header_search_ad, (ViewGroup) null, false);
        this.f2822b = (RelativeLayout) this.f2821a.findViewById(R.id.header_layout);
        this.c = (TextView) this.f2821a.findViewById(R.id.app_name);
        this.d = (TextView) this.f2821a.findViewById(R.id.des);
        this.e = (ImageView) this.f2821a.findViewById(R.id.ad_icon);
        this.f = (Button) this.f2821a.findViewById(R.id.btn_install);
        this.f.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                HashMap hashMap = new HashMap();
                hashMap.put("package", a.this.g.f1841b);
                hashMap.put(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, a.this.g.f1840a);
                if (!ab.a().a("app_install_mode").equals("market")) {
                    v.a(a.this.g.f, a.this.g.f1840a);
                } else if (!v.a(a.this.g.f1841b)) {
                    v.a(a.this.g.f, a.this.g.f1840a);
                }
                b.a(RingDDApp.c(), "duoduo_app_ad_click", hashMap);
            }
        });
    }

    public View a() {
        return this.f2821a;
    }

    public void a(boolean z) {
        int i = 8;
        if (com.shoujiduoduo.a.b.b.g().h()) {
            this.f2822b.setVisibility(8);
            return;
        }
        RelativeLayout relativeLayout = this.f2822b;
        if (z) {
            i = 0;
        }
        relativeLayout.setVisibility(i);
    }

    public void a(g.a aVar) {
        this.g = aVar;
        this.c.setText(aVar.f1840a);
        this.d.setText(aVar.c);
        if (aVar.f1841b.equals("com.duoduo.child.story")) {
            this.e.setImageResource(R.drawable.child_story_logo);
        } else if (aVar.f1841b.equals("com.shoujiduoduo.wallpaper")) {
            this.e.setImageResource(R.drawable.wallpaper_logo);
        } else {
            d.a().a(aVar.d, this.e, g.a().j());
        }
    }
}
