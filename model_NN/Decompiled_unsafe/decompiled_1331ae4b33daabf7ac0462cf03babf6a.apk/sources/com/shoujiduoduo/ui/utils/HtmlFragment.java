package com.shoujiduoduo.ui.utils;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.z;
import com.umeng.analytics.b;

public class HtmlFragment extends LazyFragment {

    /* renamed from: b  reason: collision with root package name */
    private boolean f2808b;
    private WebView c;
    private String d;
    /* access modifiers changed from: private */
    public String e;
    private boolean f;
    private String g = "http://musicalbum.shoujiduoduo.com/malbum/serv/billboard.php?create=1&ddsrc=ring_gzh&needstory=1";
    private WebViewClient h = new WebViewClient() {
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            PlayerService b2;
            a.a("HtmlFragment", "override url:" + str);
            if (g.a(str)) {
                if (str.contains("w2c_open_webview") && (b2 = z.a().b()) != null && b2.l()) {
                    b2.m();
                }
                g.a(HtmlFragment.this.getActivity(), str);
                return true;
            } else if (HtmlFragment.this.e == null || str == null) {
                return false;
            } else {
                if (HtmlFragment.this.e.equals(str) || str.contains(HtmlFragment.this.e)) {
                    a.a("HtmlFragment", "same url or refresh inside");
                    return false;
                }
                Intent intent = new Intent(HtmlFragment.this.getActivity(), WebViewActivity.class);
                intent.putExtra("url", str);
                HtmlFragment.this.getActivity().startActivity(intent);
                return true;
            }
        }
    };

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        a.a("HtmlFragment", "onCreateView");
        try {
            View inflate = layoutInflater.inflate((int) R.layout.html_frag_layout, viewGroup, false);
            this.c = (WebView) inflate.findViewById(R.id.webview_content);
            Bundle arguments = getArguments();
            if (arguments == null || arguments.getString("url") == null) {
                a.e("HtmlFragment", "url is null");
            } else {
                this.d = arguments.getString("url");
                if (this.d.contains("?")) {
                    this.d += "&isrc=" + f.n();
                } else {
                    this.d += "?isrc=" + f.n();
                }
                a.a("HtmlFragment", "url:" + this.d);
                WebSettings settings = this.c.getSettings();
                settings.setJavaScriptEnabled(true);
                settings.setUseWideViewPort(true);
                settings.setLoadWithOverviewMode(true);
                settings.setBuiltInZoomControls(false);
                settings.setDomStorageEnabled(true);
                settings.setSupportZoom(false);
                if (Build.VERSION.SDK_INT >= 21) {
                    settings.setMixedContentMode(0);
                }
                this.c.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
                this.c.setWebViewClient(this.h);
            }
            this.f2808b = true;
            a();
            return inflate;
        } catch (Exception e2) {
            e2.printStackTrace();
            b.b(RingDDApp.c(), "kuaixiu_wall_crash");
            return super.onCreateView(layoutInflater, viewGroup, bundle);
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f2808b = false;
        this.f = false;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.f2808b && this.f2810a && !this.f && this.c != null) {
            String b2 = g.b(this.d);
            this.e = b2;
            this.c.loadUrl(b2);
            a.a("HtmlFragment", "final url:" + b2);
            this.f = true;
        }
    }
}
