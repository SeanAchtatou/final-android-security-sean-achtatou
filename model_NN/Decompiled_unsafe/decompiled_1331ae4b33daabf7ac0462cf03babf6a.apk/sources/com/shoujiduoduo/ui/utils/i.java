package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: ResUtils */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static Context f2850a = RingDDApp.c();

    public static int a(int i) {
        if (Build.VERSION.SDK_INT >= 23) {
            return f2850a.getResources().getColor(i, f2850a.getTheme());
        }
        return f2850a.getResources().getColor(i);
    }

    public static Drawable b(int i) {
        if (Build.VERSION.SDK_INT >= 21) {
            return f2850a.getResources().getDrawable(i, f2850a.getTheme());
        }
        return f2850a.getResources().getDrawable(i);
    }
}
