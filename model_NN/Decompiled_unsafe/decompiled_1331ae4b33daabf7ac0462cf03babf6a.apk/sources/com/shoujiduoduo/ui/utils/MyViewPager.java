package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2814a;

    public MyViewPager(Context context) {
        super(context);
    }

    public MyViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setCanScroll(boolean z) {
        this.f2814a = z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.f2814a) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.f2814a) {
            return false;
        }
        return super.onInterceptHoverEvent(motionEvent);
    }

    public void scrollTo(int i, int i2) {
        super.scrollTo(i, i2);
    }
}
