package com.shoujiduoduo.ui.utils;

import android.graphics.Color;
import com.d.a.b.c;
import com.d.a.b.c.b;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.e;

/* compiled from: ImageLoaderOption */
public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f2846a = new g();

    /* renamed from: b  reason: collision with root package name */
    private c f2847b;
    private c c;
    private c d;
    private c e;
    private c f;
    private c g;
    private c h;
    private c i;
    private c j;
    private c k;

    private g() {
    }

    public static g a() {
        return f2846a;
    }

    public c b() {
        if (this.f2847b == null) {
            this.f2847b = new c.a().a(true).b(true).c();
        }
        return this.f2847b;
    }

    public c c() {
        if (this.c == null) {
            this.c = new c.a().a(true).a(com.d.a.b.a.g.EXACTLY_STRETCHED).a(new b(1500)).b(true).c();
        }
        return this.c;
    }

    public c d() {
        if (this.d == null) {
            this.d = new c.a().b((int) R.drawable.user_head).c((int) R.drawable.user_head).d((int) R.drawable.user_head).a(true).a(com.d.a.b.a.g.EXACTLY_STRETCHED).a(new e(4.0f, Color.parseColor("#FFFFFF"))).b(true).c();
        }
        return this.d;
    }

    public c e() {
        if (this.e == null) {
            this.e = new c.a().b((int) R.drawable.icon_fans_def_head).c((int) R.drawable.icon_fans_def_head).d((int) R.drawable.icon_fans_def_head).a(true).a(com.d.a.b.a.g.EXACTLY_STRETCHED).a(new e(4.0f, Color.parseColor("#FFFFFF"))).b(true).c();
        }
        return this.e;
    }

    public c f() {
        if (this.f == null) {
            this.f = new c.a().b((int) R.drawable.cate_2_normal).c((int) R.drawable.cate_2_normal).d((int) R.drawable.cate_2_normal).a(true).b(true).c();
        }
        return this.f;
    }

    public c g() {
        if (this.g == null) {
            this.g = new c.a().b((int) R.drawable.icon_artist_default).c((int) R.drawable.icon_artist_default).d((int) R.drawable.icon_artist_default).a(true).b(true).c();
        }
        return this.g;
    }

    public c h() {
        if (this.h == null) {
            this.h = new c.a().b((int) R.drawable.icon_collect_default).c((int) R.drawable.icon_collect_default).d((int) R.drawable.icon_collect_default).a(true).b(true).c();
        }
        return this.h;
    }

    public c i() {
        if (this.i == null) {
            this.i = new c.a().b((int) R.drawable.skin_loading).c((int) R.drawable.skin_loading).d((int) R.drawable.skin_loading).a(true).b(true).c();
        }
        return this.i;
    }

    public c j() {
        if (this.j == null) {
            this.j = new c.a().b((int) R.drawable.icon_duoduo_default).c((int) R.drawable.icon_duoduo_default).d((int) R.drawable.icon_duoduo_default).a(true).b(true).c();
        }
        return this.j;
    }

    public c k() {
        if (this.k == null) {
            this.k = new c.a().a(true).b((int) R.color.white).a(com.d.a.b.a.g.EXACTLY_STRETCHED).c();
        }
        return this.k;
    }
}
