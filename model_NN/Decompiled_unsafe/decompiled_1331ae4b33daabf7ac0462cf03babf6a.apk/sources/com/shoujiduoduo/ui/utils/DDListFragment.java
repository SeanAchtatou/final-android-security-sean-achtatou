package com.shoujiduoduo.ui.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.m;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.f;
import com.shoujiduoduo.ui.home.ArtistRingActivity;
import com.shoujiduoduo.ui.home.CollectRingActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.q;

public class DDListFragment extends LazyFragment {
    private p A = new p() {
        public void a(int i, RingData ringData) {
            if (i == 16 && DDListFragment.this.f2783b != null) {
                if (DDListFragment.this.f2783b.a().equals("cmcc_cailing") || DDListFragment.this.f2783b.a().equals("ctcc_cailing") || DDListFragment.this.f2783b.a().equals("cucc_cailing")) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "default cailing change, refresh list");
                    if (DDListFragment.this.g != null) {
                        DDListFragment.this.g.notifyDataSetChanged();
                    }
                }
            }
        }
    };
    private com.shoujiduoduo.a.c.e B = new com.shoujiduoduo.a.c.e() {
        public void a(com.shoujiduoduo.base.bean.c cVar) {
        }

        public void a() {
            if (DDListFragment.this.f2783b != null && DDListFragment.this.f2783b.a().equals("comment_list")) {
                DDListFragment.this.a(f.LIST_LOADING);
                DDListFragment.this.f2783b.f();
            }
        }
    };
    private com.shoujiduoduo.a.c.c C = new com.shoujiduoduo.a.c.c() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.utils.DDListFragment, int]
         candidates:
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, int):int
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.base.bean.d):com.shoujiduoduo.base.bean.d
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, com.shoujiduoduo.ui.utils.DDListFragment$d):void
          com.shoujiduoduo.ui.utils.DDListFragment.a(com.shoujiduoduo.ui.utils.DDListFragment, boolean):boolean */
        public void a(boolean z, f.b bVar) {
            if (DDListFragment.this.f2783b != null && DDListFragment.this.f2783b.a().equals("cmcc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "on Cailing Status change:" + z);
                if (z) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "cailing is wait to open");
                    boolean unused = DDListFragment.this.w = false;
                    boolean unused2 = DDListFragment.this.x = true;
                    DDListFragment.this.a(f.LIST_LOADING);
                    DDListFragment.this.f2783b.e();
                    return;
                }
                boolean unused3 = DDListFragment.this.w = true;
                DDListFragment.this.a(f.LIST_FAILED);
            }
        }

        public void a(f.b bVar) {
            if (DDListFragment.this.f2783b == null) {
                return;
            }
            if (DDListFragment.this.f2783b.a().equals("cmcc_cailing") || DDListFragment.this.f2783b.a().equals("ctcc_cailing") || DDListFragment.this.f2783b.a().equals("cucc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onOrderCailing");
                DDListFragment.this.a(f.LIST_LOADING);
                DDListFragment.this.f2783b.f();
            }
        }

        public void b(f.b bVar) {
            if (DDListFragment.this.f2783b == null) {
                return;
            }
            if (DDListFragment.this.f2783b.a().equals("cmcc_cailing") || DDListFragment.this.f2783b.a().equals("ctcc_cailing") || DDListFragment.this.f2783b.a().equals("cucc_cailing")) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onDeleteCailing");
                DDListFragment.this.a(f.LIST_LOADING);
                DDListFragment.this.f2783b.f();
            }
        }
    };
    private com.shoujiduoduo.a.c.g D = new com.shoujiduoduo.a.c.g() {
        public void a(com.shoujiduoduo.base.bean.d dVar, int i) {
            if (DDListFragment.this.f2783b != null && dVar.a().equals(DDListFragment.this.f2783b.a())) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "onDataUpdate in, id:" + DDListFragment.this.f2783b.a());
                if (!dVar.a().equals(DDListFragment.this.f2783b.a())) {
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "onDataUpdate: the list update is not current list.");
                    return;
                }
                switch (i) {
                    case 0:
                        if (DDListFragment.this.q == f.LIST_LOADING) {
                            com.shoujiduoduo.base.a.a.a("DDListFragment", "show content now! listid:" + dVar.a());
                            DDListFragment.this.a(f.LIST_CONTENT);
                        }
                        DDListFragment.this.g.notifyDataSetChanged();
                        return;
                    case 1:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "show failed now. listid:" + dVar.a());
                        DDListFragment.this.a(f.LIST_FAILED);
                        return;
                    case 2:
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "more data ready. notify the adapter to update. listid:" + dVar.a());
                        com.shoujiduoduo.base.a.a.a("DDListFragment", "FooterState: set failed onDataUpdate.");
                        DDListFragment.this.a(d.RETRIEVE_FAILED);
                        DDListFragment.this.g.notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private AbsListView.OnScrollListener E = new AbsListView.OnScrollListener() {

        /* renamed from: a  reason: collision with root package name */
        boolean f2789a = false;

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (this.f2789a && i == 0) {
                if (DDListFragment.this.f2783b != null) {
                    if (DDListFragment.this.f2783b.g()) {
                        if (!DDListFragment.this.f2783b.d()) {
                            DDListFragment.this.f2783b.e();
                            DDListFragment.this.a(d.RETRIEVE);
                        }
                    } else if (DDListFragment.this.f2783b.c() > 1) {
                        DDListFragment.this.a(d.TOTAL);
                    } else {
                        DDListFragment.this.a(d.INVISIBLE);
                    }
                }
                this.f2789a = false;
            }
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (i + i2 == i3 && i3 > 0 && i2 < i3) {
                this.f2789a = true;
            }
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public com.shoujiduoduo.base.bean.d f2783b;
    /* access modifiers changed from: private */
    public RelativeLayout c;
    private RelativeLayout d;
    private RelativeLayout e;
    private RelativeLayout f;
    /* access modifiers changed from: private */
    public d g;
    private ListView h;
    private Button i;
    private Button j;
    private View k;
    private View l;
    private boolean m;
    private boolean n;
    private boolean o;
    private boolean p;
    /* access modifiers changed from: private */
    public f q = f.LIST_FAILED;
    private boolean r;
    private boolean s;
    private int t;
    private int u;
    private String v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    private View.OnClickListener y = new View.OnClickListener() {
        public void onClick(View view) {
            if (!DDListFragment.this.x) {
                new com.shoujiduoduo.ui.cailing.f(DDListFragment.this.getActivity(), R.style.DuoDuoDialog, f.b.cm, new f.a() {
                    public void a(f.a.C0036a aVar) {
                        if (aVar.equals(f.a.C0036a.open)) {
                        }
                    }
                }).show();
            } else if (DDListFragment.this.f2783b != null) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "重新获取彩铃库");
                DDListFragment.this.a(f.LIST_LOADING);
                DDListFragment.this.f2783b.e();
            }
        }
    };
    private x z = new x() {
        public void a(int i) {
            if (DDListFragment.this.f2783b != null && DDListFragment.this.h()) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "vipType:" + i);
                if ((i == 1 && !DDListFragment.this.f2783b.b().equals(g.a.list_ring_cmcc)) || ((i == 3 && !DDListFragment.this.f2783b.b().equals(g.a.list_ring_cucc)) || (i == 2 && !DDListFragment.this.f2783b.b().equals(g.a.list_ring_ctcc)))) {
                    switch (i) {
                        case 1:
                            com.shoujiduoduo.base.bean.d unused = DDListFragment.this.f2783b = new com.shoujiduoduo.b.c.g(g.a.list_ring_cmcc, "", false, "");
                            break;
                        case 2:
                            com.shoujiduoduo.base.bean.d unused2 = DDListFragment.this.f2783b = new com.shoujiduoduo.b.c.g(g.a.list_ring_ctcc, "", false, "");
                            break;
                        case 3:
                            com.shoujiduoduo.base.bean.d unused3 = DDListFragment.this.f2783b = new com.shoujiduoduo.b.c.g(g.a.list_ring_cucc, "", false, "");
                            break;
                    }
                    com.shoujiduoduo.base.a.a.a("DDListFragment", "vipType:" + i + ", cur list id:" + DDListFragment.this.f2783b.a());
                    DDListFragment.this.a(f.LIST_LOADING);
                    DDListFragment.this.f2783b.f();
                    DDListFragment.this.f();
                }
            }
        }
    };

    enum d {
        RETRIEVE,
        TOTAL,
        RETRIEVE_FAILED,
        INVISIBLE
    }

    public enum f {
        LIST_CONTENT,
        LIST_LOADING,
        LIST_FAILED
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.ring_list_panel, viewGroup, false);
        this.c = (RelativeLayout) inflate.findViewById(R.id.content_view);
        this.d = (RelativeLayout) inflate.findViewById(R.id.failed_view);
        this.e = (RelativeLayout) inflate.findViewById(R.id.cailing_not_open_view);
        this.e.setVisibility(8);
        this.f = (RelativeLayout) inflate.findViewById(R.id.loading_view);
        this.f.setVisibility(0);
        ((AnimationDrawable) ((ImageView) this.f.findViewById(R.id.loading)).getBackground()).start();
        this.e.findViewById(R.id.open).setOnClickListener(this.y);
        this.h = (ListView) this.c.findViewById(R.id.list_view);
        this.k = layoutInflater.inflate((int) R.layout.get_more_rings, (ViewGroup) null, false);
        if (this.k != null) {
            this.h.addFooterView(this.k);
            this.k.setVisibility(4);
        }
        this.h.setOnScrollListener(this.E);
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DDListFragment.this.f2783b != null) {
                    DDListFragment.this.a(f.LIST_LOADING);
                    DDListFragment.this.f2783b.e();
                }
            }
        });
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.m = arguments.getBoolean("support_area", false);
            if (this.m) {
                g();
            }
            this.n = arguments.getBoolean("support_batch", false);
            if (this.n) {
                c();
            }
            this.o = arguments.getBoolean("support_feed_ad", false);
            this.p = arguments.getBoolean("support_lazy_load", false);
            if (!this.p) {
                setUserVisibleHint(true);
            }
            this.v = arguments.getString("userlist_tuid");
            if (this.v == null) {
                this.v = "";
            }
            String string = arguments.getString("adapter_type");
            if ("ring_list_adapter".equals(string)) {
                this.g = new j(getActivity());
                if (!ag.c(this.v)) {
                    ((j) this.g).a(this.v);
                }
                this.h.setOnItemClickListener(new e());
            } else if ("cailing_list_adapter".equals(string)) {
                this.g = new com.shoujiduoduo.ui.cailing.b(getActivity());
                this.h.setOnItemClickListener(new e());
            } else if ("system_ring_list_adapter".equals(string)) {
                this.g = new com.shoujiduoduo.ui.mine.changering.a(getActivity());
                this.h.setOnItemClickListener(new e());
            } else if ("collect_list_adapter".equals(string)) {
                this.g = new e(getActivity());
                this.h.setOnItemClickListener(new b());
            } else if ("artist_list_adapter".equals(string)) {
                this.g = new b(getActivity());
                this.h.setOnItemClickListener(new a());
            } else if ("user_list_adapter".equals(string)) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "user list adapter");
                this.g = new k(getActivity());
                if (!ag.c(this.v)) {
                    ((k) this.g).a(this.v);
                }
                this.h.setOnItemClickListener(new g());
            } else if ("comment_list_adapter".equals(string)) {
                this.g = new f(getActivity());
                this.h.setOnItemClickListener(new c());
            } else {
                com.shoujiduoduo.base.a.a.c("DDListFragment", "not support adapter type");
            }
        }
        if (this.g != null) {
            if (this.o) {
                this.g.a(true);
            }
            this.g.a();
        }
        this.t = q.a(com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_gap"), 10);
        this.u = q.a(com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_start_pos"), 6);
        this.r = true;
        this.s = false;
        if (this.l != null) {
            a(this.l);
        }
        a();
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.A);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.C);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.D);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.z);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.B);
        return inflate;
    }

    public void onDestroy() {
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroy, list id:" + (this.f2783b == null ? "no id" : this.f2783b.a()));
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("DDListFragment", "onDestroyView, id:" + (this.f2783b == null ? "no id" : this.f2783b.a()));
        if (this.g != null) {
            this.g.b();
        }
        this.r = false;
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.A);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, this.C);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.D);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.z);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.B);
    }

    public void onDetach() {
        super.onDetach();
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.r && this.f2810a && this.f2783b != null && this.g != null && !this.s) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "lazyLoad, loadListData");
            f();
            this.s = true;
        }
    }

    public void a(View view) {
        if (!this.r) {
            this.l = view;
        } else if (this.l == null || this.h.getHeaderViewsCount() == 0) {
            this.l = view;
            this.h.addHeaderView(this.l);
        }
    }

    public class g implements AdapterView.OnItemClickListener {
        public g() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            com.shoujiduoduo.base.a.a.a("DDListFragment", "onItemClick, UserClickListener");
            if (DDListFragment.this.f2783b != null && j >= 0) {
                Intent intent = new Intent(DDListFragment.this.getActivity(), UserMainPageActivity.class);
                intent.putExtra("tuid", ((UserData) DDListFragment.this.f2783b.a(i)).c);
                intent.putExtra("fansNum", ((UserData) DDListFragment.this.f2783b.a(i)).j);
                intent.putExtra("followNum", ((UserData) DDListFragment.this.f2783b.a(i)).k);
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class c implements AdapterView.OnItemClickListener {
        public c() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long j) {
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, new c.a<com.shoujiduoduo.a.c.e>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.e) this.f1812a).a((com.shoujiduoduo.base.bean.c) DDListFragment.this.f2783b.a(i));
                }
            });
        }
    }

    public class a implements AdapterView.OnItemClickListener {
        public a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.f2783b != null && j >= 0) {
                RingDDApp.b().a("artistdata", DDListFragment.this.f2783b.a(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), ArtistRingActivity.class);
                intent.putExtra("parakey", "artistdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    public class b implements AdapterView.OnItemClickListener {
        public b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (DDListFragment.this.f2783b != null && j >= 0) {
                RingDDApp.b().a("collectdata", DDListFragment.this.f2783b.a(i));
                Intent intent = new Intent(DDListFragment.this.getActivity(), CollectRingActivity.class);
                intent.putExtra("parakey", "collectdata");
                DDListFragment.this.getActivity().startActivity(intent);
            }
        }
    }

    /* access modifiers changed from: private */
    public int a(int i2) {
        if (this.o && i2 + 1 >= this.u) {
            return (((i2 + 1) - this.u) / this.t) + 1;
        }
        return 0;
    }

    public class e implements AdapterView.OnItemClickListener {
        public e() {
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [android.widget.Adapter] */
        /* JADX WARN: Type inference failed for: r1v2, types: [android.widget.Adapter] */
        /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
            	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
            	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
            	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
            	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
            	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
            	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
            	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
            */
        public void onItemClick(android.widget.AdapterView<?> r5, android.view.View r6, int r7, long r8) {
            /*
                r4 = this;
                com.shoujiduoduo.ui.utils.DDListFragment r0 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.d r0 = r0.f2783b
                if (r0 == 0) goto L_0x000e
                r0 = 0
                int r0 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
                if (r0 >= 0) goto L_0x000f
            L_0x000e:
                return
            L_0x000f:
                int r0 = (int) r8
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                r2 = 1
                if (r1 == r2) goto L_0x000e
                android.widget.Adapter r1 = r5.getAdapter()
                int r1 = r1.getItemViewType(r7)
                if (r1 != 0) goto L_0x0040
                com.shoujiduoduo.util.z r1 = com.shoujiduoduo.util.z.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.d r2 = r2.f2783b
                com.shoujiduoduo.ui.utils.DDListFragment r3 = com.shoujiduoduo.ui.utils.DDListFragment.this
                int r3 = r3.a(r0)
                int r0 = r0 - r3
                r1.a(r2, r0)
                goto L_0x000e
            L_0x0040:
                com.shoujiduoduo.util.z r1 = com.shoujiduoduo.util.z.a()
                com.shoujiduoduo.player.PlayerService r1 = r1.b()
                if (r1 == 0) goto L_0x000e
                com.shoujiduoduo.ui.utils.DDListFragment r2 = com.shoujiduoduo.ui.utils.DDListFragment.this
                com.shoujiduoduo.base.bean.d r2 = r2.f2783b
                r1.a(r2, r0)
                goto L_0x000e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.DDListFragment.e.onItemClick(android.widget.AdapterView, android.view.View, int, long):void");
        }
    }

    public String b() {
        if (this.f2783b != null) {
            return this.f2783b.a();
        }
        return "";
    }

    public void a(com.shoujiduoduo.base.bean.d dVar) {
        if (dVar != this.f2783b) {
            this.f2783b = null;
            this.f2783b = dVar;
            if (this.r) {
                this.s = false;
                a();
            }
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.h.setAdapter((ListAdapter) this.g);
        if (this.f2783b != null) {
            this.g.a(this.f2783b);
            if (this.f2783b.c() == 0) {
                com.shoujiduoduo.base.a.a.a("DDListFragment", "loadListData: show loading panel, id:" + this.f2783b.a());
                a(f.LIST_LOADING);
                if (!this.f2783b.d()) {
                    this.f2783b.e();
                    return;
                }
                return;
            }
            com.shoujiduoduo.base.a.a.a("DDListFragment", "setRingList: Show list content, id:" + this.f2783b.a());
            a(f.LIST_CONTENT);
            return;
        }
        this.g.a((com.shoujiduoduo.base.bean.d) null);
        this.g.notifyDataSetChanged();
    }

    private void g() {
        this.j = (Button) this.c.findViewById(R.id.changeArea);
        this.j.setVisibility(0);
        this.j.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                new com.shoujiduoduo.ui.home.a(DDListFragment.this.getActivity(), R.style.DuoDuoDialog, DDListFragment.this.f2783b.a()).show();
            }
        });
    }

    public void c() {
        this.i = (Button) this.c.findViewById(R.id.changeBatch);
        this.i.setVisibility(0);
        this.i.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.umeng.analytics.b.b(RingDDApp.c(), "HOT_LIST_CHANGE_BATCH");
                com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CHANGE_BATCH, new c.a<m>() {
                    public void a() {
                        ((m) this.f1812a).a(DDListFragment.this.f2783b.b(), DDListFragment.this.f2783b.a());
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean h() {
        if (this.f2783b == null) {
            return false;
        }
        if (this.f2783b.b().equals(g.a.list_ring_cmcc) || this.f2783b.b().equals(g.a.list_ring_cucc) || this.f2783b.b().equals(g.a.list_ring_ctcc)) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        String str;
        if (this.k != null) {
            ImageView imageView = (ImageView) this.k.findViewById(R.id.more_data_loading);
            TextView textView = (TextView) this.k.findViewById(R.id.get_more_text);
            switch (dVar) {
                case RETRIEVE:
                    imageView.setVisibility(0);
                    ((AnimationDrawable) imageView.getBackground()).start();
                    textView.setText((int) R.string.ringlist_retrieving);
                    this.k.setVisibility(0);
                    return;
                case TOTAL:
                    imageView.setVisibility(8);
                    if (this.h.getCount() > (this.h.getHeaderViewsCount() > 0 ? 2 : 1)) {
                        String string = RingDDApp.c().getResources().getString(R.string.total);
                        if (this.f2783b.b().equals(g.a.list_artist)) {
                            str = "个歌手";
                        } else if (this.f2783b.b().equals(g.a.list_collect)) {
                            str = "个精选集";
                        } else if (this.f2783b.b().equals(g.a.list_user)) {
                            str = "个用户";
                        } else if (this.f2783b.b().equals(g.a.list_comment)) {
                            str = "个评论";
                        } else {
                            str = "首铃声";
                        }
                        int count = this.h.getCount();
                        if (this.h.getHeaderViewsCount() > 0) {
                            count -= this.h.getHeaderViewsCount();
                        }
                        textView.setText(string + (count - 1) + str);
                    }
                    if (this.f2783b.b().equals(g.a.list_comment)) {
                        this.k.setVisibility(8);
                        return;
                    } else {
                        this.k.setVisibility(0);
                        return;
                    }
                case RETRIEVE_FAILED:
                    imageView.setVisibility(8);
                    textView.setText((int) R.string.ringlist_retrieve_error);
                    this.k.setVisibility(0);
                    return;
                case INVISIBLE:
                    this.k.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(f fVar) {
        this.c.setVisibility(4);
        this.d.setVisibility(4);
        this.f.setVisibility(4);
        switch (fVar) {
            case LIST_CONTENT:
                this.h.post(new Runnable() {
                    public void run() {
                        DDListFragment.this.c.setVisibility(0);
                    }
                });
                break;
            case LIST_LOADING:
                this.f.setVisibility(0);
                break;
            case LIST_FAILED:
                if (!this.w) {
                    this.d.setVisibility(0);
                    break;
                } else {
                    this.e.setVisibility(0);
                    TextView textView = (TextView) this.e.findViewById(R.id.hint);
                    TextView textView2 = (TextView) this.e.findViewById(R.id.open_tips);
                    TextView textView3 = (TextView) this.e.findViewById(R.id.cost_hint);
                    if (!this.x) {
                        textView.setText("尊敬的移动用户,\n您目前尚未开通彩铃功能,无法使用彩铃。");
                        textView2.setText("立即开通");
                        textView3.setVisibility(0);
                        break;
                    } else {
                        textView.setText("正在为您开通彩铃业务，\n请稍候点击“查询彩铃”获取当前彩铃");
                        textView2.setText("查询彩铃");
                        textView3.setVisibility(4);
                        break;
                    }
                }
        }
        this.q = fVar;
    }
}
