package com.shoujiduoduo.ui.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

/* compiled from: ProgressWaitDlg */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static ProgressDialog f2848a = null;

    /* renamed from: b  reason: collision with root package name */
    private static ProgressDialog f2849b = null;

    public static void a(Context context) {
        a(context, "请稍候...");
    }

    public static void a(Context context, String str) {
        if (f2848a == null) {
            f2848a = new ProgressDialog(context);
            f2848a.setMessage(str);
            f2848a.setIndeterminate(false);
            f2848a.setCancelable(true);
            f2848a.setCanceledOnTouchOutside(false);
            f2848a.show();
        }
    }

    public static void a() {
        if (f2848a != null) {
            f2848a.dismiss();
            f2848a = null;
        }
    }

    public static void b(Context context) {
        if (f2849b == null) {
            f2849b = new ProgressDialog(context);
            f2849b.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如弹出警告，请选择允许。");
            f2849b.setIndeterminate(false);
            f2849b.setCancelable(true);
            f2849b.setCanceledOnTouchOutside(false);
            f2849b.setButton(-1, "确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            f2849b.show();
        }
    }

    public static void b() {
        if (f2849b != null) {
            f2849b.dismiss();
            f2849b = null;
        }
    }
}
