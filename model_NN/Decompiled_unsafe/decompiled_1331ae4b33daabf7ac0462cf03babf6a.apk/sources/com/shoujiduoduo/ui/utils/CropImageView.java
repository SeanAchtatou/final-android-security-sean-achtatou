package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.v4.view.InputDeviceCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

public class CropImageView extends ImageView {
    private static Point j;
    private static Point k;
    private static Point l;
    private float A;
    private float B;
    private float C;
    private float D;
    private boolean E;
    private boolean F;
    private Bitmap G = null;
    private String H;

    /* renamed from: a  reason: collision with root package name */
    int f2781a = 0;

    /* renamed from: b  reason: collision with root package name */
    int f2782b = 0;
    Paint c = new Paint();
    Paint d;
    Matrix e = new Matrix();
    float[] f = new float[9];
    private float g = 1.0f;
    private int h;
    private int i;
    private Bitmap m;
    private int n = 0;
    private float o;
    private float p;
    private Matrix q = new Matrix();
    private float r = 3.0f;
    private float s = 0.2f;
    private float t;
    private float u;
    private float v;
    private PointF w = new PointF();
    private PointF x = new PointF();
    private float y;
    private boolean z = false;

    public CropImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
    }

    public CropImageView(Context context) {
        super(context);
        d();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (!TextUtils.isEmpty(this.H)) {
            if (this.d == null) {
                this.d = new Paint(1);
                this.d.setColor(-7829368);
                this.d.setTextSize(24.0f);
            }
            Paint.FontMetrics fontMetrics = this.d.getFontMetrics();
            canvas.drawText(this.H, (float) ((getWidth() - ((int) this.d.measureText(this.H))) / 2), (float) ((int) ((Math.abs(fontMetrics.ascent) / 2.0f) + ((float) ((getHeight() - getWidth()) + 40)))), this.d);
            return;
        }
        if (j.equals(0, 0)) {
            a();
        }
        try {
            super.onDraw(canvas);
            if (this.m == null) {
                this.m = a(new Rect(j.x, j.y, k.x, k.y));
            }
            if (this.m != null) {
                canvas.drawBitmap(this.m, 0.0f, 0.0f, (Paint) null);
                return;
            }
            canvas.drawRect((float) j.x, (float) j.y, (float) k.x, (float) k.y, this.c);
        } catch (Exception e2) {
            setImageDrawable(null);
            e2.printStackTrace();
        }
    }

    public Bitmap a(Rect rect) {
        try {
            Bitmap createBitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            Paint paint = new Paint();
            paint.setColor(Color.argb(128, 0, 0, 0));
            canvas.drawRect(new Rect(0, 0, getWidth(), getHeight()), paint);
            Paint paint2 = new Paint();
            paint2.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint2.setColor(-12434878);
            paint2.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            paint2.setColor(Color.argb(0, 0, 0, 0));
            canvas.drawRect(rect, paint2);
            Paint paint3 = new Paint();
            paint3.setStyle(Paint.Style.STROKE);
            paint3.setColor(Color.rgb(70, 180, 231));
            paint3.setStrokeWidth(3.0f);
            canvas.drawRect(rect, paint3);
            return createBitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public void setImageRotate(boolean z2) {
        float f2;
        if (this.G != null) {
            if (z2) {
                f2 = -90.0f;
            } else {
                f2 = 90.0f;
            }
            Matrix matrix = new Matrix();
            matrix.postRotate(f2);
            try {
                Bitmap createBitmap = Bitmap.createBitmap(this.G, 0, 0, this.G.getWidth(), this.G.getHeight(), matrix, true);
                if (createBitmap != this.G) {
                    this.G.recycle();
                    this.G = null;
                }
                if (createBitmap != null) {
                    a(this.h, this.i, createBitmap.getWidth(), createBitmap.getHeight());
                    a();
                    setImageBitmap(createBitmap);
                    return;
                }
                setErrorHint("创建旋转图片出错了！");
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    public void setImageZoom(boolean z2) {
        if (this.e != null) {
            if (z2) {
                if (!this.E) {
                    this.e.postScale(1.1f, 1.1f, (float) l.x, (float) l.y);
                    this.F = false;
                }
            } else if (!this.F) {
                this.e.postScale(0.9f, 0.9f, (float) l.x, (float) l.y);
                this.E = false;
            }
            b();
            c();
            setImageMatrix(this.e);
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        switch (motionEvent.getAction() & 255) {
            case 0:
                this.q.set(this.e);
                this.w.set(motionEvent.getX(), motionEvent.getY());
                this.n = 1;
                break;
            case 1:
            case 6:
                this.n = 0;
                this.E = false;
                this.F = false;
                if (this.z) {
                    this.A *= this.t;
                    this.B *= this.t;
                    if (this.A < this.C || this.B < this.D) {
                        this.e.postScale(this.C / this.A, this.D / this.B, this.x.x, this.x.y);
                        this.A = this.C;
                        this.B = this.D;
                    }
                }
                this.z = false;
                break;
            case 2:
                if (this.n != 1) {
                    if (this.n == 2) {
                        float a2 = a(motionEvent);
                        if (a2 > 10.0f) {
                            this.e.set(this.q);
                            this.t = a2 / this.y;
                            if (this.E || this.F) {
                                if (!this.E) {
                                    if (this.F) {
                                        this.e.setScale(this.s, this.s, this.x.x, this.x.y);
                                        break;
                                    }
                                } else {
                                    this.e.setScale(this.r, this.r, this.x.x, this.x.y);
                                    break;
                                }
                            } else {
                                this.e.postScale(this.t, this.t, this.x.x, this.x.y);
                                this.z = true;
                                break;
                            }
                        }
                    }
                } else {
                    this.e.set(this.q);
                    this.e.postTranslate(motionEvent.getX() - this.w.x, motionEvent.getY() - this.w.y);
                    break;
                }
                break;
            case 5:
                this.y = a(motionEvent);
                if (this.y > 10.0f) {
                    this.q.set(this.e);
                    a(this.x, motionEvent);
                    this.n = 2;
                    break;
                }
                break;
        }
        if (this.n != 0) {
            c();
            b();
        }
        setImageMatrix(this.e);
        return true;
    }

    private void b() {
        if (this.G != null) {
            this.e.getValues(this.f);
            if (this.f[0] > this.r) {
                this.E = true;
                this.f[0] = this.r;
            } else if (this.f[0] < this.s) {
                this.F = true;
                this.f[0] = this.s;
            }
            if (this.f[4] > this.r) {
                this.f[4] = this.r;
            } else if (this.f[4] < this.s) {
                this.f[4] = this.s;
            }
            this.e.setValues(this.f);
        }
    }

    private void c() {
        if (this.G != null) {
            this.e.getValues(this.f);
            float f2 = (this.o * this.f[0]) - ((float) k.x);
            if (this.f[2] > ((float) j.x)) {
                this.f[2] = (float) j.x;
            } else if (this.f[2] < (-f2)) {
                this.f[2] = -f2;
            }
            float f3 = (this.p * this.f[4]) - ((float) k.y);
            if (this.f[5] > ((float) j.y)) {
                this.f[5] = (float) j.y;
            } else if (this.f[5] < (-f3)) {
                this.f[5] = -f3;
            }
            this.e.setValues(this.f);
        }
    }

    private float a(MotionEvent motionEvent) {
        float x2 = motionEvent.getX(0) - motionEvent.getX(1);
        float y2 = motionEvent.getY(0) - motionEvent.getY(1);
        return (float) Math.sqrt((double) ((x2 * x2) + (y2 * y2)));
    }

    private void a(PointF pointF, MotionEvent motionEvent) {
        pointF.set((motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f, (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f);
    }

    private void d() {
        setScaleType(ImageView.ScaleType.MATRIX);
        this.v = 1.0f;
        this.u = 1.0f;
        this.s = 0.8f;
        setImageMatrix(this.e);
        this.c.setColor((int) InputDeviceCompat.SOURCE_ANY);
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setStrokeWidth(5.0f);
        j = new Point();
        k = new Point();
        l = new Point();
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.h = i2;
        this.i = i3;
        this.o = (float) i4;
        this.p = (float) i5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public void a() {
        this.g = 1.0f;
        this.f2781a = (int) (((float) this.h) * this.g);
        this.f2782b = (int) (((float) this.i) * this.g);
        l.set(getMeasuredWidth() / 2, getMeasuredHeight() / 2);
        j.set((getMeasuredWidth() - this.f2781a) / 2, (getMeasuredHeight() - this.f2782b) / 2);
        k.set(j.x + this.f2781a, j.y + this.f2782b);
        this.v = ((float) this.f2781a) / this.o;
        this.u = ((float) this.f2782b) / this.p;
        this.s = this.v > this.u ? this.v : this.u;
        if (this.s > 1.0f) {
            this.r = this.s * 2.0f;
        } else {
            this.r = Math.min(4000.0f, ((float) ((int) Math.max(this.o, this.p))) / this.s) / ((float) Math.max(this.f2781a, this.f2782b));
            this.r = Math.min(5.0f, this.r);
            if (this.r < 2.0f) {
                this.r = 2.0f;
            }
        }
        float f2 = ((float) j.x) / this.s;
        float f3 = ((float) j.y) / this.s;
        if (this.o * this.s > ((float) this.f2781a)) {
            f2 -= (((this.o * this.s) - ((float) this.f2781a)) / 2.0f) / this.s;
        }
        if (this.p * this.s > ((float) this.f2782b)) {
            f3 -= (((this.p * this.s) - ((float) this.f2782b)) / 2.0f) / this.s;
        }
        this.e.setTranslate(f2, f3);
        this.e.postScale(this.s, this.s, 0.0f, 0.0f);
        setImageMatrix(this.e);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.graphics.Bitmap, byte[]] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ea A[SYNTHETIC, Splitter:B:47:0x00ea] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0104 A[SYNTHETIC, Splitter:B:57:0x0104] */
    /* JADX WARNING: Removed duplicated region for block: B:78:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] getCropImage() {
        /*
            r9 = this;
            r0 = 0
            android.graphics.Bitmap r1 = r9.G
            if (r1 != 0) goto L_0x0006
        L_0x0005:
            return r0
        L_0x0006:
            r1 = 9
            float[] r1 = new float[r1]
            android.graphics.Matrix r2 = r9.e
            r2.getValues(r1)
            r2 = 2
            r2 = r1[r2]
            r3 = 5
            r3 = r1[r3]
            r4 = 0
            r4 = r1[r4]
            android.graphics.Point r1 = com.shoujiduoduo.ui.utils.CropImageView.j
            int r1 = r1.x
            float r1 = (float) r1
            float r1 = r1 - r2
            int r5 = (int) r1
            android.graphics.Point r1 = com.shoujiduoduo.ui.utils.CropImageView.j
            int r1 = r1.y
            float r1 = (float) r1
            float r1 = r1 - r3
            int r3 = (int) r1
            android.graphics.Point r1 = com.shoujiduoduo.ui.utils.CropImageView.k
            int r1 = r1.x
            android.graphics.Point r2 = com.shoujiduoduo.ui.utils.CropImageView.j
            int r2 = r2.x
            int r2 = r1 - r2
            android.graphics.Point r1 = com.shoujiduoduo.ui.utils.CropImageView.k
            int r1 = r1.y
            android.graphics.Point r6 = com.shoujiduoduo.ui.utils.CropImageView.j
            int r6 = r6.y
            int r1 = r1 - r6
            r6 = 1065353216(0x3f800000, float:1.0)
            float r6 = r6 - r4
            float r6 = java.lang.Math.abs(r6)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            r7 = 897988541(0x358637bd, float:1.0E-6)
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 >= 0) goto L_0x00b5
            int r4 = r2 + r5
            android.graphics.Bitmap r6 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r6 = r6.getWidth()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            if (r4 <= r6) goto L_0x0066
            android.graphics.Bitmap r2 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r2 = r2.getWidth()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r2 = r2 - r5
            android.graphics.Bitmap r4 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r4 = r4.getWidth()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r2 = java.lang.Math.abs(r2)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r2 = java.lang.Math.min(r4, r2)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
        L_0x0066:
            int r4 = r1 + r3
            android.graphics.Bitmap r6 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r6 = r6.getHeight()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            if (r4 <= r6) goto L_0x0085
            android.graphics.Bitmap r1 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r1 = r1.getHeight()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r1 = r1 - r3
            android.graphics.Bitmap r4 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r4 = r4.getHeight()     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r1 = java.lang.Math.abs(r1)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r1 = java.lang.Math.min(r4, r1)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
        L_0x0085:
            android.graphics.Bitmap r4 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r4, r5, r3, r2, r1)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            r2 = r0
        L_0x008c:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x0122, all -> 0x0112 }
            r3.<init>()     // Catch:{ Throwable -> 0x0122, all -> 0x0112 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Throwable -> 0x0125 }
            r5 = 100
            r1.compress(r4, r5, r3)     // Catch:{ Throwable -> 0x0125 }
            r1.recycle()     // Catch:{ Throwable -> 0x0125 }
            byte[] r1 = r3.toByteArray()     // Catch:{ Throwable -> 0x0127, all -> 0x0119 }
            r3.close()     // Catch:{ Throwable -> 0x0127, all -> 0x0119 }
            r3 = 0
            if (r2 == 0) goto L_0x00a8
            r2.recycle()
        L_0x00a8:
            if (r0 == 0) goto L_0x00ad
            r0.recycle()
        L_0x00ad:
            if (r0 == 0) goto L_0x00b2
            r3.close()     // Catch:{ IOException -> 0x0108 }
        L_0x00b2:
            r0 = r1
            goto L_0x0005
        L_0x00b5:
            float r5 = (float) r5
            float r5 = r5 / r4
            int r5 = (int) r5
            float r3 = (float) r3
            float r3 = r3 / r4
            int r3 = (int) r3
            float r2 = (float) r2
            float r2 = r2 / r4
            int r2 = (int) r2
            float r1 = (float) r1
            float r1 = r1 / r4
            int r1 = (int) r1
            android.graphics.Bitmap r4 = r9.G     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createBitmap(r4, r5, r3, r2, r1)     // Catch:{ Throwable -> 0x00d7, all -> 0x00f2 }
            int r1 = r9.h     // Catch:{ Throwable -> 0x011e, all -> 0x010c }
            int r3 = r9.i     // Catch:{ Throwable -> 0x011e, all -> 0x010c }
            r4 = 1
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createScaledBitmap(r2, r1, r3, r4)     // Catch:{ Throwable -> 0x011e, all -> 0x010c }
            if (r2 == r1) goto L_0x008c
            r2.recycle()     // Catch:{ Throwable -> 0x0122, all -> 0x0112 }
            r2 = r0
            goto L_0x008c
        L_0x00d7:
            r1 = move-exception
            r1 = r0
            r2 = r0
            r3 = r0
        L_0x00db:
            java.lang.System.gc()     // Catch:{ all -> 0x0117 }
            if (r2 == 0) goto L_0x00e3
            r2.recycle()
        L_0x00e3:
            if (r1 == 0) goto L_0x00e8
            r1.recycle()
        L_0x00e8:
            if (r3 == 0) goto L_0x0005
            r3.close()     // Catch:{ IOException -> 0x00ef }
            goto L_0x0005
        L_0x00ef:
            r1 = move-exception
            goto L_0x0005
        L_0x00f2:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x00f8:
            if (r2 == 0) goto L_0x00fd
            r2.recycle()
        L_0x00fd:
            if (r1 == 0) goto L_0x0102
            r1.recycle()
        L_0x0102:
            if (r3 == 0) goto L_0x0107
            r3.close()     // Catch:{ IOException -> 0x010a }
        L_0x0107:
            throw r0
        L_0x0108:
            r0 = move-exception
            goto L_0x00b2
        L_0x010a:
            r1 = move-exception
            goto L_0x0107
        L_0x010c:
            r1 = move-exception
            r3 = r0
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x00f8
        L_0x0112:
            r3 = move-exception
            r8 = r3
            r3 = r0
            r0 = r8
            goto L_0x00f8
        L_0x0117:
            r0 = move-exception
            goto L_0x00f8
        L_0x0119:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00f8
        L_0x011e:
            r1 = move-exception
            r1 = r0
            r3 = r0
            goto L_0x00db
        L_0x0122:
            r3 = move-exception
            r3 = r0
            goto L_0x00db
        L_0x0125:
            r4 = move-exception
            goto L_0x00db
        L_0x0127:
            r1 = move-exception
            r1 = r0
            goto L_0x00db
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.ui.utils.CropImageView.getCropImage():byte[]");
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        if (this.G != null && !this.G.isRecycled()) {
            this.G.recycle();
            this.G = null;
        }
        this.G = bitmap;
        c();
        b();
    }

    public void setErrorHint(String str) {
        this.H = str;
    }
}
