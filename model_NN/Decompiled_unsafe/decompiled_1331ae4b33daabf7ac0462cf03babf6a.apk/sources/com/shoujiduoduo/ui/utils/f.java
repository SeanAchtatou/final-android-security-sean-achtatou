package com.shoujiduoduo.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.e;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.utils.TextViewFixTouchConsume;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.s;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: CommentListAdapter */
public class f extends d {

    /* renamed from: a  reason: collision with root package name */
    private e f2837a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f2838b;

    public f(Context context) {
        this.f2838b = context;
    }

    public void a(boolean z) {
    }

    public void a(d dVar) {
        this.f2837a = (e) dVar;
    }

    public void a() {
    }

    public void b() {
    }

    public int getCount() {
        if (this.f2837a != null) {
            return this.f2837a.c();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (this.f2837a != null) {
            return this.f2837a.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* access modifiers changed from: private */
    public boolean a(c cVar) {
        return ad.a(RingDDApp.c(), "upvote_comment_list", "").contains(cVar.g);
    }

    /* access modifiers changed from: private */
    public void b(c cVar) {
        String str;
        String a2 = ad.a(RingDDApp.c(), "upvote_comment_list", "");
        if (a2.equals("")) {
            str = cVar.g;
        } else {
            str = a2 + "|" + cVar.g;
        }
        ad.c(RingDDApp.c(), "upvote_comment_list", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = LayoutInflater.from(this.f2838b).inflate((int) R.layout.listitem_comment, viewGroup, false);
        }
        ImageView imageView = (ImageView) l.a(view, R.id.user_head);
        TextView textView = (TextView) l.a(view, R.id.user_name);
        TextView textView2 = (TextView) l.a(view, R.id.create_time);
        TextView textView3 = (TextView) l.a(view, R.id.vote_num);
        TextViewFixTouchConsume textViewFixTouchConsume = (TextViewFixTouchConsume) l.a(view, R.id.comment);
        TextViewFixTouchConsume textViewFixTouchConsume2 = (TextViewFixTouchConsume) l.a(view, R.id.tcomment);
        TextView textView4 = (TextView) l.a(view, R.id.comment_catetory);
        ImageView imageView2 = (ImageView) l.a(view, R.id.tv_upvote);
        final c cVar = (c) this.f2837a.a(i);
        if (!ag.c(cVar.l)) {
            com.d.a.b.d.a().a(cVar.l, imageView, g.a().e());
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", cVar.c);
                f.this.f2838b.startActivity(intent);
            }
        });
        textView.setText(cVar.j);
        try {
            textView2.setText(com.shoujiduoduo.util.f.a(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(cVar.n)));
        } catch (ParseException e) {
            e.printStackTrace();
            textView2.setText(cVar.n);
        }
        if (cVar.o > 0) {
            textView3.setText("" + cVar.o);
        } else {
            textView3.setText("");
        }
        if (a(cVar)) {
            imageView2.setImageResource(R.drawable.icon_upvote_pressed);
            textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_green));
        } else {
            imageView2.setImageResource(R.drawable.icon_upvote_normal);
            textView3.setTextColor(RingDDApp.c().getResources().getColor(R.color.text_gray));
        }
        l.a(view, R.id.vote_layout).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!f.this.a(cVar)) {
                    a.a("CommentListAdapter", "顶评论， comment：" + cVar.h + ", cid:" + cVar.g);
                    cVar.o++;
                    f.this.b(cVar);
                    f.this.notifyDataSetChanged();
                    s.a("upvote", "&rid=" + cVar.f1962b + "&uid=" + b.g().c().a() + "&cid=" + cVar.g, new s.a() {
                        public void a(String str) {
                            a.a("CommentListAdapter", "vote comment success:" + str);
                        }

                        public void a(String str, String str2) {
                            a.a("CommentListAdapter", "vote comment error");
                        }
                    });
                }
            }
        });
        if (!ag.c(cVar.f)) {
            textViewFixTouchConsume.setMovementMethod(TextViewFixTouchConsume.a.a());
            textViewFixTouchConsume.setFocusable(false);
            textViewFixTouchConsume2.setMovementMethod(TextViewFixTouchConsume.a.a());
            textViewFixTouchConsume2.setFocusable(false);
            SpannableString spannableString = new SpannableString("回复@" + cVar.k + ":" + cVar.h);
            SpannableString spannableString2 = new SpannableString("@" + cVar.k + ":" + cVar.i);
            final int color = this.f2838b.getResources().getColor(R.color.text_blue);
            AnonymousClass3 r3 = new ClickableSpan() {
                public void updateDrawState(TextPaint textPaint) {
                    super.updateDrawState(textPaint);
                    textPaint.setColor(color);
                    textPaint.setUnderlineText(false);
                }

                public void onClick(View view) {
                    Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                    intent.putExtra("tuid", cVar.d);
                    f.this.f2838b.startActivity(intent);
                }
            };
            spannableString.setSpan(r3, 2, ("@" + cVar.k).length() + 2, 17);
            spannableString2.setSpan(r3, 0, ("@" + cVar.k).length(), 17);
            textViewFixTouchConsume2.setText(spannableString2);
            textViewFixTouchConsume2.setVisibility(0);
            textViewFixTouchConsume.setText(spannableString);
        } else {
            textViewFixTouchConsume.setText(cVar.h);
            textViewFixTouchConsume2.setVisibility(8);
        }
        if (!ag.c(cVar.f1961a)) {
            textView4.setText(cVar.f1961a);
            textView4.setVisibility(0);
        } else {
            textView4.setVisibility(8);
        }
        return view;
    }
}
