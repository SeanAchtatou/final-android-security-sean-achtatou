package com.shoujiduoduo.ui.makering;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.shoujiduoduo.ringtone.R;

public class MakeRingTypeFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private Button f2406a;

    /* renamed from: b  reason: collision with root package name */
    private Button f2407b;
    /* access modifiers changed from: private */
    public a c;

    public interface a {
        void a(String str);
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.c = (a) activity;
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_makering_choose_type, viewGroup, false);
        this.f2406a = (Button) inflate.findViewById(R.id.btn_choosetype_record);
        this.f2406a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (!com.shoujiduoduo.player.a.a()) {
                    new AlertDialog.Builder(MakeRingTypeFragment.this.getActivity()).setTitle("录制").setMessage("对不起，录制模块加载失败！").setPositiveButton("确定", (DialogInterface.OnClickListener) null).show();
                } else {
                    MakeRingTypeFragment.this.c.a("record");
                }
            }
        });
        this.f2407b = (Button) inflate.findViewById(R.id.btn_choosetype_edit);
        this.f2407b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MakeRingTypeFragment.this.c.a("edit");
            }
        });
        return inflate;
    }
}
