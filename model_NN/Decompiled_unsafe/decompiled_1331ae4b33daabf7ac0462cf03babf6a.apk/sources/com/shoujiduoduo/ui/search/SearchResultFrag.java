package com.shoujiduoduo.ui.search;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.a.g;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.ag;

public class SearchResultFrag extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private ViewPager f2590a;

    /* renamed from: b  reason: collision with root package name */
    private com.shoujiduoduo.ui.utils.a f2591b;
    /* access modifiers changed from: private */
    public DDListFragment c;
    private String d;
    private String e;
    private boolean f;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_search_result, viewGroup, false);
        this.f2590a = (ViewPager) inflate.findViewById(R.id.vp_search_result);
        this.f2590a.setAdapter(new a(getActivity().getSupportFragmentManager()));
        this.f = com.shoujiduoduo.util.a.f();
        this.c = new DDListFragment();
        this.f2591b = new com.shoujiduoduo.ui.utils.a(getActivity());
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "ring_list_adapter");
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_list_id");
        if ((ag.b(a2) || a2.contains("search")) && this.f) {
            bundle2.putBoolean("support_feed_ad", true);
        }
        this.c.setArguments(bundle2);
        this.c.a(this.f2591b.a());
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "oncreateview");
        return inflate;
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void a(String str, String str2) {
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "showResult");
        this.d = str;
        this.e = str2;
        if (b.c().f()) {
            g.a g = b.c().g();
            if (g != null) {
                this.f2591b.a(g);
                this.f2591b.a(true);
                com.shoujiduoduo.base.a.a.a("SearchResultFrag", "显示搜索广告， " + g.toString());
            } else {
                com.shoujiduoduo.base.a.a.a("SearchResultFrag", "没有匹配检索词的搜索广告");
                this.f2591b.a(false);
            }
        } else {
            this.f2591b.a(false);
            com.shoujiduoduo.base.a.a.a("SearchResultFrag", "检索广告数据尚未获取");
        }
        this.c.a(new com.shoujiduoduo.b.c.g(g.a.list_ring_search, str, str2));
        com.shoujiduoduo.base.a.a.a("SearchResultFrag", "refreshList");
        this.f2590a.setCurrentItem(0);
        this.f2590a.getAdapter().notifyDataSetChanged();
    }

    private class a extends FragmentStatePagerAdapter {

        /* renamed from: b  reason: collision with root package name */
        private String[] f2593b = {"铃声", "相关壁纸"};

        public a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public CharSequence getPageTitle(int i) {
            return this.f2593b[i];
        }

        public Fragment getItem(int i) {
            if (i == 0) {
                return SearchResultFrag.this.c;
            }
            if (i == 1) {
            }
            return null;
        }

        public int getCount() {
            return 1;
        }
    }
}
