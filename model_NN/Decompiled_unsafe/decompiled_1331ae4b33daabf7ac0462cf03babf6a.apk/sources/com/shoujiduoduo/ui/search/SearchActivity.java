package com.shoujiduoduo.ui.search;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.b.d.e;
import com.shoujiduoduo.base.a.b;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.TestCmcc;
import com.shoujiduoduo.ui.cailing.TestCtcc;
import com.shoujiduoduo.ui.cailing.TestCucc;
import com.shoujiduoduo.ui.search.HotWordFrag;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.d;
import com.shoujiduoduo.util.z;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.IOException;

public class SearchActivity extends BaseFragmentActivity implements HotWordFrag.b {
    /* access modifiers changed from: private */
    public static int s = 1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HotWordFrag f2578a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public SearchResultFrag f2579b;
    private ImageButton c;
    /* access modifiers changed from: private */
    public Button d;
    /* access modifiers changed from: private */
    public ImageButton e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public boolean i = false;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public String[] m;
    /* access modifiers changed from: private */
    public AutoCompleteTextView n;
    /* access modifiers changed from: private */
    public a o = new a();
    private boolean p;
    private boolean q;
    private View.OnClickListener r = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null && b2.l()) {
                b2.m();
            }
            if (SearchActivity.this.f2578a.isVisible()) {
                SearchActivity.this.finish();
            } else {
                SearchActivity.this.a();
            }
        }
    };
    private View.OnClickListener t = new View.OnClickListener() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
         arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
         candidates:
          com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):void
          com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
        public void onClick(View view) {
            final String str;
            com.shoujiduoduo.base.a.a.a("SearchActivity", "Search Button Clicked!");
            final String obj = SearchActivity.this.n.getText().toString();
            if (TextUtils.isEmpty(obj)) {
                d.a("请输入搜索关键词");
                return;
            }
            PlayerService b2 = z.a().b();
            if (b2 != null && b2.l()) {
                b2.m();
            }
            SearchActivity.this.g();
            SearchActivity.this.b(obj);
            if (obj.equalsIgnoreCase("*#06#getinstallsrc")) {
                Toast.makeText(SearchActivity.this, f.n(), 1).show();
            } else if (obj.equalsIgnoreCase("*#06#testctcc")) {
                SearchActivity.this.startActivity(new Intent(SearchActivity.this, TestCtcc.class));
            } else if (obj.equalsIgnoreCase("*#06#testcucc")) {
                SearchActivity.this.startActivity(new Intent(SearchActivity.this, TestCucc.class));
            } else if (obj.equalsIgnoreCase("*#06#testcmcc")) {
                SearchActivity.this.startActivity(new Intent(SearchActivity.this, TestCmcc.class));
            } else if (obj.equalsIgnoreCase("*#06#debug")) {
                com.shoujiduoduo.base.a.a.f1947a = true;
                b.f1949a = false;
                d.a("已开启调试模式， 日志文件路径：/sdcard/shoujiduoduo/duoduo.log");
            } else if (obj.equalsIgnoreCase("*#06#logcat")) {
                try {
                    Process exec = Runtime.getRuntime().exec(new String[]{"logcat", "-v", "time", "-d", "-f", "/sdcard/logcat.log"});
                    exec.waitFor();
                    exec.exitValue();
                    Toast.makeText(SearchActivity.this, "日志已取出，请拷贝/sdcard/logcat.log", 1).show();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            } else {
                if (SearchActivity.this.g) {
                    obj = "&rid=" + obj;
                }
                if (SearchActivity.this.h) {
                    str = "push";
                } else if (SearchActivity.this.f) {
                    str = "hot";
                } else if (SearchActivity.this.i) {
                    str = "suggest";
                } else {
                    str = "input";
                }
                SearchActivity.this.b();
                c.a().b(new c.b() {
                    public void a() {
                        SearchActivity.this.f2579b.a(obj, str);
                    }
                });
                boolean unused = SearchActivity.this.f = false;
                boolean unused2 = SearchActivity.this.h = false;
                boolean unused3 = SearchActivity.this.g = false;
                boolean unused4 = SearchActivity.this.i = false;
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_search);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.n = (AutoCompleteTextView) findViewById(R.id.search_input);
        a(this.n);
        this.c = (ImageButton) findViewById(R.id.backButton);
        this.c.setOnClickListener(this.r);
        this.e = (ImageButton) findViewById(R.id.clear_edit_text);
        this.e.setVisibility(4);
        this.e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                SearchActivity.this.n.setText("");
                SearchActivity.this.e.setVisibility(4);
            }
        });
        this.d = (Button) findViewById(R.id.search_button);
        if (this.d != null) {
            this.d.setOnClickListener(this.t);
        }
        this.f2578a = new HotWordFrag();
        this.f2579b = new SearchResultFrag();
        this.l = com.shoujiduoduo.util.a.f();
        d();
        c.a().b(new c.b() {
            public void a() {
                SearchActivity.this.e();
            }
        });
    }

    private void d() {
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.hotword_layout, this.f2578a);
        this.p = true;
        beginTransaction.add((int) R.id.ringlist_layout, this.f2579b);
        this.q = true;
        beginTransaction.hide(this.f2579b);
        beginTransaction.commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        e();
    }

    /* access modifiers changed from: private */
    public void e() {
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("from");
            String stringExtra2 = intent.getStringExtra("key");
            if (!TextUtils.isEmpty(stringExtra)) {
                if (stringExtra.equals("push")) {
                    this.h = true;
                }
                this.n.setText(stringExtra2);
                this.d.performClick();
                return;
            }
            a();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.c.performClick();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.o != null) {
            this.o.removeCallbacksAndMessages(null);
        }
    }

    public void a() {
        com.shoujiduoduo.base.a.a.a("SearchActivity", "showHotwordlist");
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        if (!this.f2578a.isAdded() && !this.p) {
            beginTransaction.add((int) R.id.hotword_layout, this.f2578a);
        }
        beginTransaction.show(this.f2578a);
        if (this.f2579b.isAdded() && this.f2579b.isVisible()) {
            beginTransaction.hide(this.f2579b);
        }
        beginTransaction.commitAllowingStateLoss();
    }

    public void b() {
        com.shoujiduoduo.base.a.a.a("SearchActivity", "showSearchRingList");
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        if (!this.f2579b.isAdded() && !this.q) {
            beginTransaction.add((int) R.id.ringlist_layout, this.f2579b);
        }
        beginTransaction.show(this.f2579b);
        if (this.f2578a.isAdded() && this.f2578a.isVisible()) {
            beginTransaction.hide(this.f2578a);
        }
        beginTransaction.commitAllowingStateLoss();
    }

    public void a(String str) {
        if (this.d != null) {
            this.f = true;
            this.n.clearFocus();
            this.n.setText(str);
            this.d.performClick();
        }
    }

    private class a extends Handler {
        private a() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (!SearchActivity.this.isFinishing()) {
                SearchActivity.this.n.requestFocus();
                SearchActivity.this.n.showDropDown();
            }
        }
    }

    private void a(AutoCompleteTextView autoCompleteTextView) {
        String[] f2 = f();
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, (int) R.layout.dropdown_item, f2);
        this.m = f2;
        autoCompleteTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i != 3 && i != 0) {
                    return false;
                }
                SearchActivity.this.d.performClick();
                SearchActivity.this.n.dismissDropDown();
                return true;
            }
        });
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setDropDownHeight(-2);
        autoCompleteTextView.setThreshold(1);
        autoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) view;
                com.shoujiduoduo.base.a.a.a("SearchActivity", "autocompletetextview, onFocusChange:" + z);
                if (z && autoCompleteTextView.isShown() && !RingDDApp.c().getSharedPreferences("search_history", 0).getString("history", "清空搜索历史").equals("清空搜索历史")) {
                    autoCompleteTextView.showDropDown();
                }
            }
        });
        autoCompleteTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (SearchActivity.this.n.getText().toString().length() == 0 && !RingDDApp.c().getSharedPreferences("search_history", 0).getString("history", "清空搜索历史").equals("清空搜索历史")) {
                    SearchActivity.this.n.showDropDown();
                }
            }
        });
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
             arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
             candidates:
              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):java.lang.String
              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String[]):void
              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
             arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
             candidates:
              com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):void
              com.shoujiduoduo.ui.search.SearchActivity.b(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                boolean unused = SearchActivity.this.k = true;
                if (((TextView) view.findViewById(R.id.auto_complete_item)).getText().equals("清空搜索历史")) {
                    SearchActivity.this.h();
                    SearchActivity.this.n.dismissDropDown();
                    SearchActivity.this.n.setText("");
                } else if (SearchActivity.this.d != null) {
                    boolean unused2 = SearchActivity.this.i = true;
                    SearchActivity.this.d.performClick();
                }
            }
        });
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                com.shoujiduoduo.base.a.a.a("SearchActivity", "onTextChanged, :" + ((Object) charSequence));
                if (charSequence == null || charSequence.length() <= 0) {
                    SearchActivity.this.e.setVisibility(4);
                } else {
                    SearchActivity.this.e.setVisibility(0);
                }
                if (!SearchActivity.this.f && !SearchActivity.this.h && !SearchActivity.this.g) {
                    if (charSequence != null) {
                        String unused = SearchActivity.this.j = charSequence.toString();
                    }
                    if (TextUtils.isEmpty(SearchActivity.this.j)) {
                        SearchActivity.this.a(SearchActivity.this.m);
                    } else {
                        new e().a(SearchActivity.this.j, new e.b() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean
                             arg types: [com.shoujiduoduo.ui.search.SearchActivity, int]
                             candidates:
                              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String):java.lang.String
                              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, java.lang.String[]):void
                              com.shoujiduoduo.ui.search.SearchActivity.a(com.shoujiduoduo.ui.search.SearchActivity, boolean):boolean */
                            public void a(String str, String[] strArr) {
                                if (str != null && str.length() > 0 && !str.equals(SearchActivity.this.j)) {
                                    com.shoujiduoduo.base.a.a.a("SearchActivity", "不是当前检索词的联想结果， curword:" + SearchActivity.this.j + ", return word:" + str);
                                } else if (strArr == null || strArr.length <= 0) {
                                    com.shoujiduoduo.base.a.a.a("SearchActivity", "onSuggestData, key:" + str + ", data:null");
                                } else {
                                    com.shoujiduoduo.base.a.a.a("SearchActivity", "onSuggestData, key:" + str + " ,data:" + ag.a(strArr, MiPushClient.ACCEPT_TIME_SEPARATOR));
                                    if (!SearchActivity.this.k) {
                                        SearchActivity.this.a(strArr);
                                        com.shoujiduoduo.base.a.a.a("SearchActivity", "show drop down");
                                        SearchActivity.this.o.sendEmptyMessage(SearchActivity.s);
                                    } else {
                                        com.shoujiduoduo.base.a.a.a("SearchActivity", "from item click, not show");
                                    }
                                    boolean unused = SearchActivity.this.k = false;
                                }
                            }
                        });
                    }
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private String[] f() {
        String[] split = RingDDApp.c().getSharedPreferences("search_history", 0).getString("history", "清空搜索历史").split(MiPushClient.ACCEPT_TIME_SEPARATOR);
        if (split.length <= 25) {
            return split;
        }
        String[] strArr = new String[25];
        System.arraycopy(split, 0, strArr, 0, 24);
        strArr[24] = "清空搜索历史";
        return strArr;
    }

    /* access modifiers changed from: private */
    public void a(String[] strArr) {
        this.n.setAdapter(new ArrayAdapter(this, (int) R.layout.dropdown_item, strArr));
    }

    /* access modifiers changed from: private */
    public void g() {
        ((InputMethodManager) RingDDApp.c().getSystemService("input_method")).hideSoftInputFromWindow(this.n.getWindowToken(), 2);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        SharedPreferences sharedPreferences = RingDDApp.c().getSharedPreferences("search_history", 0);
        String string = sharedPreferences.getString("history", "清空搜索历史");
        if (!string.contains(str + MiPushClient.ACCEPT_TIME_SEPARATOR)) {
            StringBuilder sb = new StringBuilder(string);
            sb.insert(0, str + MiPushClient.ACCEPT_TIME_SEPARATOR);
            String[] split = sb.toString().split(MiPushClient.ACCEPT_TIME_SEPARATOR);
            if (split.length > 25) {
                String[] strArr = new String[25];
                System.arraycopy(split, 0, strArr, 0, 24);
                strArr[24] = "清空搜索历史";
                sharedPreferences.edit().putString("history", ag.a(strArr, MiPushClient.ACCEPT_TIME_SEPARATOR)).commit();
                this.m = strArr;
                a(strArr);
                return;
            }
            sharedPreferences.edit().putString("history", sb.toString()).commit();
            this.m = split;
            a(split);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        RingDDApp.c().getSharedPreferences("search_history", 0).edit().putString("history", "清空搜索历史").commit();
        this.m = new String[]{"清空搜索历史"};
        a(this.m);
    }
}
