package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.d;
import java.util.ArrayList;

public class GiveCailingActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private ImageButton f2083a;

    /* renamed from: b  reason: collision with root package name */
    private Button f2084b;
    private ListView c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public EditText e;
    private EditText f;
    private ImageButton g;
    /* access modifiers changed from: private */
    public RingData h;
    private String i;
    private String j;
    private TextView k;
    private ImageButton l;
    private ArrayList<String> m = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public int o;
    private Handler p = new Handler();
    /* access modifiers changed from: private */
    public f.b q;
    private RelativeLayout r;
    /* access modifiers changed from: private */
    public ProgressDialog s = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        PlayerService.a(true);
        super.onCreate(bundle);
        Intent intent = getIntent();
        com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "intent = " + intent);
        if (intent != null) {
            this.i = intent.getStringExtra("listid");
            this.j = intent.getStringExtra("listtype");
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "mListId = " + this.i);
            this.h = (RingData) intent.getExtras().getParcelable("ringdata");
            this.n = intent.getBooleanExtra("freering", false);
            this.o = intent.getIntExtra("apppoints", Constants.textWatch_result);
            int intExtra = intent.getIntExtra("operator_type", 0);
            if (intExtra == 0) {
                this.q = f.b.cm;
            } else if (intExtra == 1) {
                this.q = f.b.ct;
            } else {
                this.q = f.b.cu;
            }
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "operator type:" + this.q);
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "name = " + this.h.e);
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "artist = " + this.h.f);
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "rid = " + this.h.g);
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "low aac bitrate = " + this.h.f());
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "price = " + this.h.r);
            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "high aac url = " + this.h.c());
        }
        setContentView((int) R.layout.dialog_give_cailing);
        this.k = (TextView) findViewById(R.id.give_cailing_title);
        if (this.n) {
            this.k.setText("积分兑换彩铃");
        }
        this.f2083a = (ImageButton) findViewById(R.id.buy_cailing_close);
        this.f2083a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GiveCailingActivity.this.finish();
            }
        });
        this.c = (ListView) findViewById(R.id.cailing_info_list);
        this.c.setAdapter((ListAdapter) new b());
        this.c.setEnabled(false);
        this.d = (EditText) findViewById(R.id.give_cailing_phone_num);
        this.r = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        if (this.q == f.b.ct) {
            this.r.setVisibility(0);
        } else {
            this.r.setVisibility(8);
        }
        this.f = (EditText) findViewById(R.id.et_phone_code);
        this.e = (EditText) findViewById(R.id.et_phone_no);
        String a2 = ad.a(getApplicationContext(), "pref_phone_num", "");
        if (!TextUtils.isEmpty(a2)) {
            this.e.setText(a2);
        }
        this.g = (ImageButton) findViewById(R.id.btn_get_code);
        this.g.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = GiveCailingActivity.this.e.getText().toString();
                if (obj == null || !f.f(GiveCailingActivity.this.e.getText().toString())) {
                    Toast.makeText(GiveCailingActivity.this, "请输入正确的手机号", 1).show();
                    return;
                }
                GiveCailingActivity.this.b("请稍候...");
                if (GiveCailingActivity.this.q == f.b.ct) {
                    GiveCailingActivity.this.c(obj);
                }
            }
        });
        this.l = (ImageButton) findViewById(R.id.give_cailing_open_contact);
        this.l.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                GiveCailingActivity.this.startActivityForResult(new Intent("android.intent.action.PICK", ContactsContract.Contacts.CONTENT_URI), 780621);
            }
        });
        this.f2084b = (Button) findViewById(R.id.buy_cailing);
        if (this.n) {
            this.f2084b.setText("免费兑换彩铃");
        }
        this.f2084b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (GiveCailingActivity.this.q == f.b.cm) {
                    GiveCailingActivity.this.c();
                } else if (GiveCailingActivity.this.q == f.b.ct) {
                    GiveCailingActivity.this.b();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                d.a("验证码短信已发出，请注意查收");
                GiveCailingActivity.this.a();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                d.a("获取短信验证码失败，请重试");
                GiveCailingActivity.this.a();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.p.post(new Runnable() {
            public void run() {
                Toast.makeText(GiveCailingActivity.this, str, 1).show();
            }
        });
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        PlayerService.a(false);
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void b() {
        final String obj = this.e.getText().toString();
        final String obj2 = this.d.getText().toString();
        final String obj3 = this.f.getText().toString();
        if (obj == null || !f.f(obj)) {
            this.e.setError("正确输入您的手机号");
        } else if (obj2 == null || !f.f(obj2)) {
            this.d.setError("正确输入被赠送者的手机号");
        } else if (obj3 == null || obj3.length() != 6) {
            this.f.setError("请输入正确的验证码");
        } else {
            ad.c(this, "pref_phone_num", obj);
            b("请稍候...");
            com.shoujiduoduo.util.d.b.a().g(obj, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "本机号码已开通彩铃功能");
                    com.shoujiduoduo.util.d.b.a().g(obj2, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            GiveCailingActivity.this.a(obj, obj2, obj3);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            GiveCailingActivity.this.a();
                            com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "被赠送号码尚未开通彩铃功能，无法赠送！");
                            GiveCailingActivity.this.a("被赠送号码尚未开通彩铃功能，无法赠送！");
                        }
                    });
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    GiveCailingActivity.this.a();
                    com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "本机号码没有开通彩铃功能");
                    new AlertDialog.Builder(GiveCailingActivity.this).setTitle("订购彩铃").setMessage("对不起，您还未开通彩铃功能，是否立即开通？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            new f(GiveCailingActivity.this, R.style.DuoDuoDialog, f.b.ct, null).show();
                        }
                    }).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        String str4 = str;
        String str5 = str2;
        String str6 = str3;
        com.shoujiduoduo.util.d.b.a().a(str4, str5, this.h.u, str6, "&ctcid=" + this.h.u + "&phone=" + str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                GiveCailingActivity.this.a();
                com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "赠送成功");
                new AlertDialog.Builder(GiveCailingActivity.this).setTitle("赠送彩铃").setMessage("赠送彩铃成功，谢谢使用").setPositiveButton("确认", new a()).show();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                GiveCailingActivity.this.a();
                com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "赠送失败， code:" + bVar.a() + " , msg:" + bVar.b());
                new AlertDialog.Builder(GiveCailingActivity.this).setTitle("赠送彩铃").setMessage("赠送彩铃失败！ 原因:" + bVar.b() + ", code:" + bVar.a()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            }
        });
    }

    private class a implements DialogInterface.OnClickListener {
        private a() {
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            GiveCailingActivity.this.finish();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        String obj = this.d.getText().toString();
        if (f.f(obj)) {
            b("请稍候...");
            com.shoujiduoduo.util.c.b.a().c(obj, this.h.p, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    GiveCailingActivity.this.a();
                    new AlertDialog.Builder(GiveCailingActivity.this).setTitle("赠送彩铃").setMessage("赠送彩铃成功，谢谢使用").setPositiveButton("确认", new a()).show();
                }

                public void b(c.b bVar) {
                    GiveCailingActivity.this.a();
                    bVar.b();
                    if (bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
                        try {
                            new AlertDialog.Builder(GiveCailingActivity.this).setTitle("赠送彩铃").setMessage("对不起，您还未开通中移动彩铃功能，是否立即开通？").setPositiveButton("是", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    new f(GiveCailingActivity.this, R.style.DuoDuoDialog, f.b.cm, null).show();
                                }
                            }).setNegativeButton("否", (DialogInterface.OnClickListener) null).show();
                        } catch (Exception e) {
                            com.umeng.analytics.b.a(GiveCailingActivity.this, "AlertDialog Failed!");
                        }
                    } else {
                        if (bVar.a().equals("999002")) {
                            int i = 0;
                            try {
                                i = Integer.valueOf(GiveCailingActivity.this.h.g).intValue();
                            } catch (NumberFormatException e2) {
                                e2.printStackTrace();
                            }
                            s.a(i);
                        }
                        try {
                            new AlertDialog.Builder(GiveCailingActivity.this).setTitle("赠送彩铃").setMessage(bVar.b()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                        } catch (Exception e3) {
                            com.umeng.analytics.b.a(GiveCailingActivity.this, "AlertDialog Failed!");
                        }
                    }
                }
            });
            return;
        }
        Toast.makeText(this, "请输入正确的手机号", 1).show();
    }

    /* access modifiers changed from: package-private */
    public void b(final String str) {
        this.p.post(new Runnable() {
            public void run() {
                if (GiveCailingActivity.this.s == null) {
                    ProgressDialog unused = GiveCailingActivity.this.s = new ProgressDialog(GiveCailingActivity.this);
                    GiveCailingActivity.this.s.setMessage(str);
                    GiveCailingActivity.this.s.setIndeterminate(false);
                    GiveCailingActivity.this.s.setCancelable(false);
                    GiveCailingActivity.this.s.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.p.post(new Runnable() {
            public void run() {
                if (GiveCailingActivity.this.s != null) {
                    GiveCailingActivity.this.s.dismiss();
                    ProgressDialog unused = GiveCailingActivity.this.s = (ProgressDialog) null;
                }
            }
        });
    }

    private class b extends BaseAdapter {
        private b() {
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            String str;
            if (view == null) {
                view = GiveCailingActivity.this.getLayoutInflater().inflate((int) R.layout.listitem_buy_cailing, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.cailing_info_des);
            TextView textView2 = (TextView) view.findViewById(R.id.cailing_info_content);
            switch (i) {
                case 0:
                    textView.setText("歌曲名");
                    textView2.setText(GiveCailingActivity.this.h.e);
                    break;
                case 1:
                    textView.setText("歌   手");
                    textView2.setText(GiveCailingActivity.this.h.f);
                    break;
                case 2:
                    textView.setText("彩铃价格");
                    int i2 = GiveCailingActivity.this.h.r;
                    if (i2 == 0) {
                        str = "免费";
                    } else {
                        str = String.valueOf(((double) ((float) i2)) / 100.0d) + "元";
                    }
                    if (!GiveCailingActivity.this.n) {
                        textView2.setText(str);
                        break;
                    } else {
                        textView2.setText(Html.fromHtml("2元 <font color=\"#68ad2e\">(" + GiveCailingActivity.this.o + "个积分兑换)</font>"));
                        break;
                    }
                case 3:
                    textView.setText("有效期");
                    textView2.setText(GiveCailingActivity.this.h.q.equals("") ? "2016-06-30" : GiveCailingActivity.this.h.q);
                    break;
            }
            return view;
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        Cursor query;
        super.onActivityResult(i2, i3, intent);
        this.m.clear();
        switch (i2) {
            case 780621:
                if (i3 == -1) {
                    Cursor managedQuery = managedQuery(intent.getData(), null, null, null, null);
                    if (managedQuery.moveToFirst()) {
                        String string = managedQuery.getString(managedQuery.getColumnIndexOrThrow("_id"));
                        if (managedQuery.getString(managedQuery.getColumnIndex("has_phone_number")).equalsIgnoreCase("1") && (query = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + string, null, null)) != null) {
                            if (query.moveToFirst()) {
                                String string2 = query.getString(query.getColumnIndex("data1"));
                                com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "number is:" + string2);
                                if (string2 != null && string2.length() > 0) {
                                    this.m.add(string2);
                                }
                                while (query.moveToNext()) {
                                    String string3 = query.getString(query.getColumnIndex("data1"));
                                    com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "number is:" + string3);
                                    if (string3 != null && string3.length() > 0) {
                                        this.m.add(string3);
                                    }
                                }
                            }
                            query.close();
                        }
                        String string4 = managedQuery.getString(managedQuery.getColumnIndex("display_name"));
                        com.shoujiduoduo.base.a.a.a("GiveCailingActivity", "name is:" + string4);
                        str = string4;
                    } else {
                        str = "";
                    }
                    if (this.m.size() == 1) {
                        String str2 = this.m.get(0);
                        if (str2 != null) {
                            str2 = str2.trim().replace(" ", "").replace("-", "");
                        }
                        this.d.setText(str2);
                        return;
                    } else if (this.m.size() > 1) {
                        final String[] strArr = new String[this.m.size()];
                        for (int i4 = 0; i4 < this.m.size(); i4++) {
                            strArr[i4] = this.m.get(i4);
                        }
                        new AlertDialog.Builder(this).setTitle(str).setSingleChoiceItems(strArr, 0, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                String str = strArr[i];
                                if (str != null) {
                                    str = str.trim().replace(" ", "").replace("-", "");
                                }
                                GiveCailingActivity.this.d.setText(str);
                            }
                        }).show();
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }
}
