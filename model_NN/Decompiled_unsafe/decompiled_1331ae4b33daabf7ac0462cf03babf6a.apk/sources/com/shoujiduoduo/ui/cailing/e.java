package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;

/* compiled from: InputPhoneNumDialog */
public class e extends Dialog implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private EditText f2250a;

    /* renamed from: b  reason: collision with root package name */
    private EditText f2251b;
    private String c;
    /* access modifiers changed from: private */
    public a d;
    /* access modifiers changed from: private */
    public Handler e;
    /* access modifiers changed from: private */
    public b f;
    /* access modifiers changed from: private */
    public Button g;
    private String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public ContentObserver j;
    /* access modifiers changed from: private */
    public Context k;
    private f.b l;
    private ProgressDialog m = null;

    /* compiled from: InputPhoneNumDialog */
    public interface a {
        void a(String str);
    }

    public e(Context context, int i2, String str, f.b bVar, a aVar) {
        super(context, i2);
        this.k = context;
        this.d = aVar;
        this.f = new b(60000, 1000);
        this.i = str;
        this.l = bVar;
        this.e = new Handler() {
            public void handleMessage(Message message) {
                e.this.d.a((String) message.obj);
            }
        };
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_input_phone_num);
        setCanceledOnTouchOutside(false);
        this.f2250a = (EditText) findViewById(R.id.et_phone_no);
        this.f2251b = (EditText) findViewById(R.id.et_random_key);
        this.g = (Button) findViewById(R.id.reget_sms_code);
        this.g.setOnClickListener(this);
        findViewById(R.id.positiveButton).setOnClickListener(this);
        findViewById(R.id.negativeButton).setOnClickListener(this);
        this.f2250a.setText(this.i);
        String str = "";
        if (this.l.equals(f.b.ct)) {
            str = "118100";
        } else if (this.l.equals(f.b.cu)) {
            str = "1065515888";
        } else if (this.l.equals(f.b.cu)) {
            str = "10658830";
        }
        this.j = new ae(this.k, new Handler(), this.f2251b, str);
        this.k.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.j);
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                e.this.k.getContentResolver().unregisterContentObserver(e.this.j);
                if (e.this.f != null) {
                    e.this.f.cancel();
                }
            }
        });
        setOnCancelListener(new DialogInterface.OnCancelListener() {
            public void onCancel(DialogInterface dialogInterface) {
                s.a(e.this.a(), "close_back", "");
            }
        });
    }

    /* compiled from: InputPhoneNumDialog */
    private class b extends CountDownTimer {
        public b(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            e.this.g.setClickable(false);
            e.this.g.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            e.this.g.setClickable(true);
            e.this.g.setText("重新获取");
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* access modifiers changed from: private */
    public String a() {
        if (this.l.equals(f.b.cm)) {
            return "cm:cm_input_phone";
        }
        if (this.l.equals(f.b.cu)) {
            return "cu:cu_input_phone";
        }
        if (this.l.equals(f.b.ct)) {
            return "ct:ct_input_phone";
        }
        return "null";
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.reget_sms_code:
                this.i = this.f2250a.getText().toString();
                if (!f.f(this.i)) {
                    d.a("请输入正确的手机号");
                    return;
                }
                this.f.start();
                b();
                return;
            case R.id.positiveButton:
                this.i = this.f2250a.getText().toString();
                if (!f.f(this.i)) {
                    d.a("请输入正确的手机号");
                    return;
                }
                this.c = this.f2251b.getText().toString();
                if (this.l.equals(f.b.ct)) {
                    if (TextUtils.isEmpty(this.c) || TextUtils.isEmpty(this.h) || !this.c.equals(this.h)) {
                        d.a("请输入正确的验证码");
                        return;
                    }
                    Message obtainMessage = this.e.obtainMessage();
                    obtainMessage.obj = this.i;
                    this.e.sendMessage(obtainMessage);
                    ad.c(RingDDApp.c(), "pref_phone_num", this.i);
                    s.a(a(), "success", "&phone=" + this.i);
                    dismiss();
                    return;
                } else if (this.l.equals(f.b.cu)) {
                    if (TextUtils.isEmpty(this.c) || this.c.length() != 6) {
                        d.a("请输入正确的验证码");
                        return;
                    }
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "已获得手机号，发起获取用户token请求");
                    a("请稍候...");
                    com.shoujiduoduo.util.e.a.a().a(this.i, this.c, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            e.this.c();
                            if (bVar == null || !(bVar instanceof c.a)) {
                                new b.a(e.this.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                                return;
                            }
                            c.a aVar = (c.a) bVar;
                            com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "token:" + aVar.f3044a);
                            com.shoujiduoduo.util.e.a.a().a(e.this.i, aVar.f3044a);
                            Message obtainMessage = e.this.e.obtainMessage();
                            obtainMessage.obj = e.this.i;
                            e.this.e.sendMessage(obtainMessage);
                            ad.c(RingDDApp.c(), "pref_phone_num", e.this.i);
                            s.a(e.this.a(), "success", "&phone=" + e.this.i);
                            e.this.dismiss();
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            e.this.c();
                            new b.a(e.this.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                            com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "获取token失败");
                        }
                    });
                    return;
                } else if (this.l.equals(f.b.cm)) {
                    a("请稍候...");
                    com.shoujiduoduo.util.c.b.a().a(this.i, this.c, new com.shoujiduoduo.util.b.b() {
                        public void b(c.b bVar) {
                            super.b(bVar);
                            e.this.c();
                            new b.a(e.this.k).a("输入验证码不对，请重试！").a("确定", (DialogInterface.OnClickListener) null).a().show();
                        }

                        public void a(c.b bVar) {
                            super.a(bVar);
                            e.this.c();
                            Message obtainMessage = e.this.e.obtainMessage();
                            obtainMessage.obj = e.this.i;
                            e.this.e.sendMessage(obtainMessage);
                            ad.c(RingDDApp.c(), "pref_phone_num", e.this.i);
                            s.a(e.this.a(), "success", "&phone=" + e.this.i);
                            e.this.dismiss();
                        }
                    });
                    return;
                } else {
                    d.a("不支持的运营商类型");
                    return;
                }
            case R.id.negativeButton:
                s.a(a(), "close", "");
                dismiss();
                return;
            default:
                return;
        }
    }

    private void b() {
        this.i = this.f2250a.getText().toString();
        if (!f.f(this.i)) {
            d.a("请输入正确的手机号");
            return;
        }
        this.l = f.g(this.i);
        if (this.l == f.b.d) {
            d.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
            com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "unknown phone type :" + this.i);
        } else if (this.l.equals(f.b.ct)) {
            this.h = f.a(6);
            com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "random key:" + this.h);
            com.shoujiduoduo.util.d.b.a().a(this.i, this.h, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onSuccess   " + bVar.toString());
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onfailure  " + bVar.toString());
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else if (this.l.equals(f.b.cu)) {
            com.shoujiduoduo.util.e.a.a().c(this.i, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onSuccess   " + bVar.toString());
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onfailure  " + bVar.toString());
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else if (this.l.equals(f.b.cm)) {
            com.shoujiduoduo.util.c.b.a().a(this.i, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onSuccess   " + bVar.toString());
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    com.shoujiduoduo.base.a.a.a("InputPhoneNumDialog", "onfailure  " + bVar.toString());
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else {
            com.shoujiduoduo.base.a.a.c("InputPhoneNumDialog", "unsupport phone type");
        }
    }

    private void a(String str) {
        if (this.m == null) {
            this.m = new ProgressDialog(this.k);
            this.m.setMessage(str);
            this.m.setIndeterminate(false);
            this.m.setCancelable(true);
            this.m.setCanceledOnTouchOutside(false);
            this.m.show();
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.m != null) {
            this.m.dismiss();
            this.m = null;
        }
    }
}
