package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.cmsc.cmmusic.common.CMMusicCallback;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.Result;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;

/* compiled from: BuyCailingDialog */
public class a extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f2181a;

    /* renamed from: b  reason: collision with root package name */
    private ImageButton f2182b;
    private Button c;
    private ListView d;
    /* access modifiers changed from: private */
    public RingData e;
    private String f;
    private g.a g;
    /* access modifiers changed from: private */
    public f.b h;
    /* access modifiers changed from: private */
    public EditText i;
    private EditText j;
    private ImageButton k;
    private RelativeLayout l;
    private TextView m;
    /* access modifiers changed from: private */
    public boolean n;
    /* access modifiers changed from: private */
    public Handler o = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 111) {
                a.this.a((OrderResult) message.obj);
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog p = null;

    public a(Context context, int i2, f.b bVar, boolean z) {
        super(context, i2);
        this.f2181a = context;
        this.h = bVar;
        this.n = z;
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "onStart, threadId:" + Thread.currentThread().getId());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_buy_cailing);
        this.f2182b = (ImageButton) findViewById(R.id.buy_cailing_close);
        this.f2182b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a.this.dismiss();
            }
        });
        setCanceledOnTouchOutside(true);
        this.d = (ListView) findViewById(R.id.cailing_info_list);
        this.d.setAdapter((ListAdapter) new C0033a());
        this.d.setEnabled(false);
        this.c = (Button) findViewById(R.id.buy_cailing);
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (a.this.h == f.b.cm) {
                    a.this.a("请稍候...");
                    a.this.b();
                    return;
                }
                d.a("当前手机卡运营商类型错误！");
            }
        });
        this.j = (EditText) findViewById(R.id.et_phone_code);
        this.i = (EditText) findViewById(R.id.et_phone_no);
        this.m = (TextView) findViewById(R.id.buy_cailing_tips);
        this.l = (RelativeLayout) findViewById(R.id.random_key_auth_layout);
        if (this.h == f.b.cm) {
            this.l.setVisibility(8);
            this.m.setText((int) R.string.buy_cailing_hint_cmcc);
        } else if (this.h == f.b.ct) {
            this.l.setVisibility(0);
            this.m.setText((int) R.string.buy_cailing_hint_ctcc);
            this.i.setHint((int) R.string.ctcc_num);
        } else {
            this.l.setVisibility(0);
            this.m.setText((int) R.string.buy_cailing_hint_cmcc);
            this.m.setVisibility(8);
            this.i.setHint((int) R.string.cmcc_num);
        }
        this.k = (ImageButton) findViewById(R.id.btn_get_code);
        this.k.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = a.this.i.getText().toString();
                if (!f.f(a.this.i.getText().toString())) {
                    Toast.makeText(a.this.f2181a, "请输入正确的手机号", 1).show();
                } else if (a.this.h != f.b.ct) {
                    a.this.b(obj);
                }
            }
        });
        String l2 = b.g().c().l();
        this.i.setText(l2);
        if (!ag.c(l2) && !com.shoujiduoduo.util.c.b.a().a(l2).equals("")) {
            this.k.setVisibility(8);
            this.j.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        com.shoujiduoduo.util.c.b.a().a(str, new com.shoujiduoduo.util.b.b() {
            public void b(c.b bVar) {
                super.b(bVar);
                d.a("成功发送验证码失败，请重试");
            }

            public void a(c.b bVar) {
                super.a(bVar);
                d.a("成功发送验证码，请查收");
            }
        });
    }

    /* access modifiers changed from: private */
    public void b() {
        a("请稍候...");
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "id1:" + Thread.currentThread().getId());
        com.shoujiduoduo.util.c.b.a().a(RingToneDuoduoActivity.a(), new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                com.shoujiduoduo.util.c.b.a().b(a.this.e.p, "", new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        BizInfo bizInfo;
                        super.a(bVar);
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "getringbadkPolicy success");
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "id2:" + Thread.currentThread().getId());
                        c.l lVar = (c.l) bVar;
                        if (a.this.n) {
                            bizInfo = lVar.d;
                        } else {
                            bizInfo = lVar.f3071a;
                        }
                        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "bizCode:" + bizInfo.getBizCode() + ",bizType:" + bizInfo.getBizType() + ", salePrice:" + bizInfo.getSalePrice() + ", monLevel:" + lVar.g + ", hode2:" + bizInfo.getHold2());
                        try {
                            RingbackManagerInterface.buyRingback(a.this.f2181a, a.this.e.p, bizInfo.getBizCode(), bizInfo.getBizType(), bizInfo.getSalePrice(), lVar.g, bizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
                                /* renamed from: a */
                                public void operationResult(OrderResult orderResult) {
                                    Message message = new Message();
                                    message.what = 111;
                                    message.obj = orderResult;
                                    a.this.o.sendMessage(message);
                                }
                            });
                        } catch (Throwable th) {
                            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "buyRingback crash");
                            th.printStackTrace();
                            a.this.a();
                            new AlertDialog.Builder(a.this.f2181a).setTitle("订购彩铃").setMessage("订购失败, 原因：SDK初始化失败").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                        a.this.a();
                        new AlertDialog.Builder(a.this.f2181a).setTitle("订购彩铃").setMessage("订购失败，原因：" + bVar.toString()).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                    }
                });
            }

            public void b(c.b bVar) {
                super.b(bVar);
                a.this.a();
                d.a("初始化SDK失败！");
            }
        });
    }

    private String c() {
        if (this.e == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&rid=").append(this.e.g).append("&from=").append(this.f);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.cailing.a.a(boolean, boolean):void
     arg types: [int, int]
     candidates:
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, android.app.ProgressDialog):android.app.ProgressDialog
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, com.cmsc.cmmusic.common.data.Result):void
      com.shoujiduoduo.ui.cailing.a.a(com.shoujiduoduo.ui.cailing.a, java.lang.String):void
      com.shoujiduoduo.ui.cailing.a.a(boolean, boolean):void */
    /* access modifiers changed from: private */
    public void a(Result result) {
        if (result == null || result.getResCode() == null) {
            a();
            d.a("订购没有成功！");
            return;
        }
        com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "cmcc order result:" + result.toString());
        if (result.getResCode().equals("000000") || result.getResCode().equals("0")) {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "订购成功");
            a(true, false);
            dismiss();
            s.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else if (result.getResCode().equals("302011")) {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "重复订购");
            a(false, false);
            dismiss();
            s.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else if (result.getResCode().equals("100002")) {
            a(false, true);
            dismiss();
            s.a("cm:sun_buy_cailing", "success, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        } else {
            com.shoujiduoduo.base.a.a.a("BuyCailingDialog", "订购失败");
            a();
            String resMsg = result.getResMsg();
            if (result.getResCode().equals("303023")) {
                resMsg = "中国移动提醒您，您的彩铃库已达到上限，建议您去\"彩铃管理\"清理部分不用的彩铃后，再重新购买。";
            }
            if (ag.c(resMsg)) {
                resMsg = "订购未成功!";
            }
            try {
                new AlertDialog.Builder(this.f2181a).setTitle("订购彩铃").setMessage(resMsg).setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
            } catch (Exception e2) {
                com.umeng.analytics.b.a(this.f2181a, "AlertDialog Failed!");
            }
            s.a("cm:sun_buy_cailing", "fail, resCode:" + result.getResCode() + ", resMsg:" + result.getResMsg(), c());
        }
    }

    private void a(final boolean z, final boolean z2) {
        com.shoujiduoduo.util.c.b.a().f(this.e.p, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                a.this.a();
                try {
                    new AlertDialog.Builder(a.this.f2181a).setTitle("订购彩铃").setMessage("恭喜，订购成功，已帮您设置为默认彩铃").setPositiveButton("确认", (DialogInterface.OnClickListener) null).show();
                } catch (Exception e) {
                }
                ad.c(a.this.f2181a, "DEFAULT_CAILING_ID", a.this.e.p);
                com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, new c.a<p>() {
                    public void a() {
                        ((p) this.f1812a).a(16, a.this.e);
                    }
                });
                if (z) {
                    ad.b(a.this.f2181a, "NeedUpdateCaiLingLib", 1);
                    com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                        public void a() {
                            ((com.shoujiduoduo.a.c.c) this.f1812a).a(f.b.cm);
                        }
                    });
                }
            }

            public void b(c.b bVar) {
                super.b(bVar);
                a.this.a();
                if (z2) {
                    new b.a(a.this.f2181a).b("设置彩铃").a("设置未成功, 请打开“我的”->“彩铃”，删除几首不用的彩铃后再试试。彩铃有最大数量限制。").a("确定", (DialogInterface.OnClickListener) null).a().show();
                } else {
                    new b.a(a.this.f2181a).b("设置彩铃").a("设置未成功, 原因:" + bVar.b()).a("确定", (DialogInterface.OnClickListener) null).a().show();
                }
            }
        });
    }

    public void a(RingData ringData, String str, g.a aVar) {
        this.e = ringData;
        this.f = str;
        this.g = aVar;
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.o.post(new Runnable() {
            public void run() {
                if (a.this.p == null) {
                    ProgressDialog unused = a.this.p = new ProgressDialog(a.this.f2181a);
                    a.this.p.setMessage(str);
                    a.this.p.setIndeterminate(false);
                    a.this.p.setCancelable(false);
                    a.this.p.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.o.post(new Runnable() {
            public void run() {
                if (a.this.p != null) {
                    a.this.p.dismiss();
                    ProgressDialog unused = a.this.p = (ProgressDialog) null;
                }
            }
        });
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.a$a  reason: collision with other inner class name */
    /* compiled from: BuyCailingDialog */
    private class C0033a extends BaseAdapter {
        private C0033a() {
        }

        public int getCount() {
            return 4;
        }

        public Object getItem(int i) {
            return null;
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            String str;
            View inflate = a.this.getLayoutInflater().inflate((int) R.layout.listitem_buy_cailing, viewGroup, false);
            TextView textView = (TextView) inflate.findViewById(R.id.cailing_info_des);
            TextView textView2 = (TextView) inflate.findViewById(R.id.cailing_info_content);
            switch (i) {
                case 0:
                    textView.setText("歌曲名");
                    textView2.setText(a.this.e.e);
                    break;
                case 1:
                    textView.setText("歌   手");
                    textView2.setText(a.this.e.f);
                    break;
                case 2:
                    textView.setText("有效期");
                    String str2 = "";
                    if (a.this.h == f.b.cm) {
                        str2 = a.this.e.q;
                    } else if (a.this.h == f.b.ct) {
                        str2 = a.this.e.v;
                    }
                    if (TextUtils.isEmpty(str2)) {
                        str2 = "2017-06-30";
                    }
                    textView2.setText(str2);
                    break;
                case 3:
                    textView.setText("彩铃价格");
                    int i2 = Downloads.STATUS_SUCCESS;
                    if (a.this.h == f.b.cm) {
                        i2 = a.this.e.r;
                    } else if (a.this.h == f.b.ct) {
                        i2 = a.this.e.w;
                    }
                    if (i2 == 0 || a.this.n) {
                        str = "免费";
                    } else {
                        str = String.valueOf(((double) ((float) i2)) / 100.0d) + "元";
                    }
                    textView2.setText(str);
                    break;
            }
            return inflate;
        }
    }
}
