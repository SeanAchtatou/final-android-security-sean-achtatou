package com.shoujiduoduo.ui.cailing;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.Toast;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.cailing.e;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: CailingMenu */
public class c extends PopupWindow {
    private static c o = null;

    /* renamed from: a  reason: collision with root package name */
    boolean f2211a = false;

    /* renamed from: b  reason: collision with root package name */
    AdapterView.OnItemClickListener f2212b = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            String str = (String) ((Map) adapterView.getItemAtPosition(i)).get("TEXT");
            if (str.equals("订购彩铃")) {
                c.this.j();
            } else if (str.equals("赠送彩铃")) {
                c.this.k();
            } else if (str.equals("管理彩铃")) {
                c.this.i();
            }
            c.this.dismiss();
        }
    };
    private ListView c = null;
    /* access modifiers changed from: private */
    public Context d = null;
    private View e = null;
    private RingData f = null;
    private String g;
    private g.a h;
    private final String i = "订购彩铃";
    private final String j = "赠送彩铃";
    private final String k = "管理彩铃";
    private final String l = "免费获取彩铃";
    /* access modifiers changed from: private */
    public a m = a.f2221a;
    /* access modifiers changed from: private */
    public ArrayList<Map<String, Object>> n = new ArrayList<>();
    /* access modifiers changed from: private */
    public C0034c p = new C0034c();
    private ProgressDialog q = null;
    /* access modifiers changed from: private */
    public ProgressDialog r = null;

    /* compiled from: CailingMenu */
    private enum a {
        f2221a,
        buy,
        give,
        manage,
        openMem,
        checkState,
        toggleMenu
    }

    private ArrayList<Map<String, Object>> d() {
        HashMap hashMap = new HashMap();
        hashMap.put("TEXT", "订购彩铃");
        this.n.add(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("TEXT", "赠送彩铃");
        this.n.add(hashMap2);
        HashMap hashMap3 = new HashMap();
        hashMap3.put("TEXT", "管理彩铃");
        this.n.add(hashMap3);
        return this.n;
    }

    private c() {
    }

    private c(Context context) {
        super(context);
        this.d = context;
        e();
    }

    public Context a() {
        return this.d;
    }

    public static c a(Context context) {
        return new c(context);
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.c$c  reason: collision with other inner class name */
    /* compiled from: CailingMenu */
    private class C0034c extends Handler {
        private C0034c() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (c.this.d != null) {
                switch (message.what) {
                    case 1:
                        com.shoujiduoduo.util.c.b.a().c(new com.shoujiduoduo.util.b.b() {
                            public void a(c.b bVar) {
                                c.this.c();
                                ad.b(c.this.d, "NeedUpdateCaiLingLib", 1);
                                switch (c.this.m) {
                                    case buy:
                                        c.this.j();
                                        return;
                                    case give:
                                        c.this.k();
                                        return;
                                    case manage:
                                        c.this.i();
                                        return;
                                    case openMem:
                                        c.this.h();
                                        return;
                                    case f2221a:
                                    default:
                                        return;
                                }
                            }

                            public void b(c.b bVar) {
                                c.this.c();
                                if (c.this.d != null && !((Activity) c.this.d).isFinishing()) {
                                    new g(c.this.d, R.style.DuoDuoDialog, c.this.p, f.b.cm).show();
                                }
                            }
                        });
                        return;
                    case 2:
                        ad.b(c.this.d, "NeedUpdateCaiLingLib", 1);
                        switch (c.this.m) {
                            case buy:
                                c.this.j();
                                return;
                            case give:
                                c.this.k();
                                return;
                            case manage:
                                c.this.i();
                                return;
                            case openMem:
                                c.this.h();
                                return;
                            case f2221a:
                            default:
                                return;
                        }
                    case 3:
                        c.this.c();
                        Toast.makeText(RingDDApp.c(), "彩铃模块初始化失败", 0).show();
                        return;
                    default:
                        return;
                }
            }
        }
    }

    private void e() {
        this.e = ((LayoutInflater) this.d.getSystemService("layout_inflater")).inflate((int) R.layout.cailing_menu, (ViewGroup) null);
        this.c = (ListView) this.e.findViewById(R.id.cailing_menu_list);
        this.c.setAdapter((ListAdapter) new b(this.d, d(), R.layout.cailing_menu_item, new String[]{"TEXT"}, new int[]{R.id.cailing_menu_dest}));
        this.c.setItemsCanFocus(false);
        this.c.setChoiceMode(2);
        this.c.setOnItemClickListener(this.f2212b);
        setContentView(this.e);
        setWidth(-2);
        setHeight(-2);
        setFocusable(true);
        setOutsideTouchable(false);
        setOnDismissListener(new PopupWindow.OnDismissListener() {
            public void onDismiss() {
                c.this.f2211a = false;
            }
        });
    }

    private void f() {
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a("CailingMenu", "准备初始化移动sdk");
        if (this.d != null) {
            b();
            new Timer().schedule(new TimerTask() {
                public void run() {
                    if (c.this.d != null) {
                        c.this.p.sendEmptyMessage(1);
                    }
                }
            }, 3000);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
            if (!((Activity) this.d).isFinishing()) {
                new f(this.d, R.style.DuoDuoDialog, f.b.cm, null).show();
            } else {
                com.shoujiduoduo.base.a.a.c("CailingMenu", "fuck, main activity is finishing");
            }
        } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
            com.shoujiduoduo.base.a.a.b("CailingMenu", "开通彩铃，移动sdk正在初始化，提示稍后再试");
            Toast.makeText(this.d, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
        } else {
            com.shoujiduoduo.base.a.a.b("CailingMenu", "开通彩铃，准备初始化移动sdk");
            this.m = a.openMem;
            g();
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (f.t()) {
            com.shoujiduoduo.base.a.a.a("CailingMenu", "彩铃管理， cmcc");
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                this.d.startActivity(new Intent(this.d, CailingManageActivity.class));
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b("CailingMenu", "管理彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.d, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                com.shoujiduoduo.base.a.a.b("CailingMenu", "管理彩铃，准备初始化移动sdk");
                this.m = a.manage;
                g();
            }
        } else if (f.v()) {
            com.shoujiduoduo.base.a.a.a("CailingMenu", "彩铃管理， ctcc");
            String a2 = ad.a(a(), "pref_phone_num", "");
            com.shoujiduoduo.base.a.a.a("CailingMenu", "记录的手机号：" + a2);
            if (TextUtils.isEmpty(a2)) {
                new e(this.d, R.style.DuoDuoDialog, "", f.b.ct, new e.a() {
                    public void a(String str) {
                        c.this.d.startActivity(new Intent(c.this.d, CailingManageActivity.class));
                    }
                }).show();
                return;
            }
            this.d.startActivity(new Intent(this.d, CailingManageActivity.class));
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        com.shoujiduoduo.base.a.a.b("CailingMenu", "订购彩铃");
        if (f.t()) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                com.shoujiduoduo.base.a.a.a("CailingMenu", "buy cailing3");
                if (!((Activity) this.d).isFinishing()) {
                    a aVar = new a(this.d, R.style.DuoDuoDialog, f.b.cm, false);
                    aVar.a(this.f, this.g, this.h);
                    aVar.show();
                    return;
                }
                com.shoujiduoduo.base.a.a.c("CailingMenu", "fuck, main activity isfinishing, is " + this.d.toString());
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b("CailingMenu", "订购彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.d, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                this.m = a.buy;
                g();
            }
        } else if (f.v()) {
            a aVar2 = new a(this.d, R.style.DuoDuoDialog, f.b.ct, false);
            aVar2.a(this.f, this.g, this.h);
            aVar2.show();
        }
    }

    /* access modifiers changed from: private */
    public void k() {
        com.shoujiduoduo.base.a.a.b("CailingMenu", "赠送彩铃");
        if (f.t()) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                if (!((Activity) this.d).isFinishing()) {
                    Intent intent = new Intent(this.d, GiveCailingActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("ringdata", this.f);
                    intent.putExtras(bundle);
                    intent.putExtra("listid", this.g);
                    intent.putExtra("listtype", this.h.toString());
                    intent.putExtra("operator_type", 0);
                    this.d.startActivity(intent);
                    return;
                }
                com.shoujiduoduo.base.a.a.c("CailingMenu", "fuck, main activity isfinishing, is " + this.d.toString());
            } else if (com.shoujiduoduo.util.c.b.a().c() == b.a.initializing) {
                com.shoujiduoduo.base.a.a.b("CailingMenu", "赠送彩铃，移动sdk正在初始化，提示稍后再试");
                Toast.makeText(this.d, "中国移动彩铃业务正在初始化，请稍后再试", 0).show();
            } else {
                this.m = a.give;
                g();
            }
        } else if (f.v() && !((Activity) this.d).isFinishing()) {
            Intent intent2 = new Intent(this.d, GiveCailingActivity.class);
            Bundle bundle2 = new Bundle();
            bundle2.putParcelable("ringdata", this.f);
            intent2.putExtras(bundle2);
            intent2.putExtra("listid", this.g);
            intent2.putExtra("listtype", this.h.toString());
            intent2.putExtra("operator_type", 1);
            this.d.startActivity(intent2);
        }
    }

    public void a(View view, RingData ringData, String str, g.a aVar) {
        if (view != null && ringData != null) {
            if (com.shoujiduoduo.util.c.b.a().c() == b.a.success) {
                f();
            }
            this.f2211a = !this.f2211a;
            this.g = str;
            this.h = aVar;
            if (this.f2211a) {
                this.f = ringData;
                update();
                showAsDropDown(view);
                return;
            }
            dismiss();
        }
    }

    /* compiled from: CailingMenu */
    class b extends SimpleAdapter {
        public b(Context context, List<? extends Map<String, ?>> list, int i, String[] strArr, int[] iArr) {
            super(context, list, i, strArr, iArr);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            View view2 = super.getView(i, view, viewGroup);
            if (i == 0) {
                view2.setBackgroundDrawable(c.this.d.getResources().getDrawable(R.drawable.share_menu_top_bkg));
            } else if (i == c.this.n.size() - 1) {
                view2.setBackgroundDrawable(c.this.d.getResources().getDrawable(R.drawable.share_menu_bottom_bkg));
            } else {
                view2.setBackgroundDrawable(c.this.d.getResources().getDrawable(R.drawable.share_menu_middle_bkg));
            }
            return view2;
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.p.post(new Runnable() {
            public void run() {
                if (c.this.r == null) {
                    ProgressDialog unused = c.this.r = new ProgressDialog(c.this.d);
                    c.this.r.setMessage("中国移动提示您：首次使用彩铃功能需要发送一条免费短信，该过程自动完成，如有警告，请选择允许。");
                    c.this.r.setIndeterminate(false);
                    c.this.r.setCancelable(true);
                    c.this.r.setCanceledOnTouchOutside(false);
                    c.this.r.setButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            c.this.r.dismiss();
                        }
                    });
                    c.this.r.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.p.post(new Runnable() {
            public void run() {
                if (c.this.r != null) {
                    c.this.r.dismiss();
                    ProgressDialog unused = c.this.r = null;
                }
            }
        });
    }
}
