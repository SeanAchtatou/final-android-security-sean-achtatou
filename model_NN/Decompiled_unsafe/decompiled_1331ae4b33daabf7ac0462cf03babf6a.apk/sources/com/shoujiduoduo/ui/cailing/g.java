package com.shoujiduoduo.ui.cailing;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.c.b;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.widget.d;

/* compiled from: SmsAuthDialog */
public class g extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f2274a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public EditText f2275b;
    private ImageButton c;
    private Button d;
    private ImageButton e;
    /* access modifiers changed from: private */
    public ProgressDialog f = null;
    private TextView g;
    private TextView h;
    private String i;
    /* access modifiers changed from: private */
    public ContentObserver j;
    /* access modifiers changed from: private */
    public Context k;
    /* access modifiers changed from: private */
    public Handler l;
    private ImageView m;
    /* access modifiers changed from: private */
    public f.b n;

    public g(Context context, int i2, Handler handler, f.b bVar) {
        super(context, i2);
        this.k = context;
        this.l = handler;
        this.n = bVar;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        String str;
        String str2;
        super.onCreate(bundle);
        a.a("SmsAuthDialog", "onCreate");
        setContentView((int) R.layout.dialog_sms_auth);
        setOnDismissListener(new DialogInterface.OnDismissListener() {
            public void onDismiss(DialogInterface dialogInterface) {
                a.a("SmsAuthDialog", "dialog dismiss, unregister sms observer");
                if (g.this.k != null) {
                    g.this.k.getContentResolver().unregisterContentObserver(g.this.j);
                }
            }
        });
        if (this.n == f.b.cm) {
            this.i = this.k.getResources().getString(R.string.cmcc_sms_auth_dialog_title);
            str = this.k.getResources().getString(R.string.cmcc_auth_hint);
            str2 = "10658830";
        } else if (this.n == f.b.ct) {
            this.i = this.k.getResources().getString(R.string.ctcc_sms_auth_dialog_title);
            str = this.k.getResources().getString(R.string.cmcc_auth_hint);
            str2 = "118100";
        } else {
            if (this.n == f.b.cu) {
                this.i = this.k.getResources().getString(R.string.cucc_sms_auth_dialog_title);
            }
            str = "";
            str2 = "";
        }
        this.g = (TextView) findViewById(R.id.title);
        this.g.setText(this.i);
        this.h = (TextView) findViewById(R.id.verify_ins);
        this.h.setText(str);
        this.m = (ImageView) findViewById(R.id.type_icon);
        this.f2274a = (EditText) findViewById(R.id.et_phone_no);
        String b2 = f.b();
        String a2 = ad.a(getContext(), "pref_phone_num");
        if (!TextUtils.isEmpty(b2)) {
            this.f2274a.setText(b2);
        } else if (!TextUtils.isEmpty(a2)) {
            this.f2274a.setText(a2);
        }
        switch (this.n) {
            case cm:
                this.f2274a.setHint((int) R.string.cmcc_num);
                this.m.setImageResource(R.drawable.icon_cmcc);
                break;
            case ct:
                this.f2274a.setHint((int) R.string.ctcc_num);
                this.m.setImageResource(R.drawable.icon_ctcc);
                break;
            case cu:
                this.f2274a.setHint((int) R.string.cucc_num);
                break;
        }
        this.f2275b = (EditText) findViewById(R.id.et_phone_code);
        this.c = (ImageButton) findViewById(R.id.btn_phone_login_close);
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                g.this.dismiss();
            }
        });
        this.d = (Button) findViewById(R.id.btn_phone_no_login);
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = g.this.f2274a.getText().toString();
                String obj2 = g.this.f2275b.getText().toString();
                if (obj == null || !f.f(obj)) {
                    g.this.f2274a.setError("请输入正确的手机号");
                } else if (obj2 == null || obj2.length() != 6) {
                    g.this.f2275b.setError("请输入正确的验证码");
                } else {
                    ad.c(g.this.getContext(), "pref_phone_num", obj);
                    if (g.this.n == f.b.cm) {
                        g.this.a("请稍候...");
                        g.this.a(obj, obj2);
                    } else if (g.this.n == f.b.ct) {
                        Toast.makeText(g.this.getContext(), "注意：验证码十分钟内有效,过期会重新获取！", 0).show();
                    }
                }
            }
        });
        this.e = (ImageButton) findViewById(R.id.btn_get_code);
        this.e.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String obj = g.this.f2274a.getText().toString();
                if (obj == null || !f.f(g.this.f2274a.getText().toString())) {
                    Toast.makeText(g.this.k, "请输入正确的手机号", 1).show();
                    return;
                }
                g.this.a("请稍候...");
                if (g.this.n == f.b.cm) {
                    g.this.c(obj);
                } else if (g.this.n == f.b.ct) {
                    g.this.d(obj);
                }
            }
        });
        this.j = new ae(this.k, this.l, this.f2275b, str2);
        this.k.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.j);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        b.a().a(str, str2, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                g.this.a();
                g.this.b("初始化成功");
                g.this.l.sendEmptyMessage(2);
                g.this.dismiss();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                g.this.a();
                g.this.l.sendEmptyMessage(3);
                g.this.dismiss();
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        b.a().a(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                g.this.b("验证码短信已发出，请注意查收");
                g.this.a();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                g.this.b("获取短信验证码失败，请重试");
                g.this.a();
            }
        });
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        com.shoujiduoduo.util.d.b.a().d(str, new com.shoujiduoduo.util.b.b() {
            public void a(c.b bVar) {
                super.a(bVar);
                d.a("验证码短信已发出，请注意查收");
                g.this.a();
            }

            public void b(c.b bVar) {
                super.b(bVar);
                d.a("获取短信验证码失败，请重试");
                g.this.a();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.l.post(new Runnable() {
            public void run() {
                if (g.this.f == null) {
                    ProgressDialog unused = g.this.f = new ProgressDialog(g.this.k);
                    g.this.f.setMessage(str);
                    g.this.f.setIndeterminate(false);
                    g.this.f.setCancelable(false);
                    g.this.f.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.l.post(new Runnable() {
            public void run() {
                if (g.this.f != null) {
                    g.this.f.dismiss();
                    ProgressDialog unused = g.this.f = (ProgressDialog) null;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void b(final String str) {
        this.l.post(new Runnable() {
            public void run() {
                Toast.makeText(g.this.k, str, 1).show();
            }
        });
    }
}
