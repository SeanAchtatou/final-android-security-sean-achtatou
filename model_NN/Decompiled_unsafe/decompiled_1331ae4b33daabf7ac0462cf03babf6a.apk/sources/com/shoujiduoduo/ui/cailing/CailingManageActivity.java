package com.shoujiduoduo.ui.cailing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import com.jaeger.library.a;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.c.c;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.z;

public class CailingManageActivity extends BaseFragmentActivity {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2073a = null;

    /* renamed from: b  reason: collision with root package name */
    private DDListFragment f2074b;
    /* access modifiers changed from: private */
    public f.b c;
    private c d = new c() {
        public void a(boolean z, f.b bVar) {
            if (!z) {
                String str = "";
                try {
                    switch (AnonymousClass5.f2079a[bVar.ordinal()]) {
                        case 1:
                            str = "中国联通";
                            break;
                        case 2:
                            str = "中国移动";
                            break;
                        case 3:
                            str = "中国电信";
                            break;
                    }
                    new AlertDialog.Builder(CailingManageActivity.this).setTitle("").setMessage("尊敬的用户，您好！您还不是" + str + "彩铃用户，是否立即开通彩铃业务？").setPositiveButton("确认", CailingManageActivity.this.f).setNegativeButton("取消", CailingManageActivity.this.e).show();
                } catch (Exception e) {
                }
            }
        }

        public void a(f.b bVar) {
        }

        public void b(f.b bVar) {
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener e = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            dialogInterface.dismiss();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener f = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            new f(CailingManageActivity.this, R.style.DuoDuoDialog, CailingManageActivity.this.c, null).show();
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_cailing_manage);
        a.a(this, getResources().getColor(R.color.bkg_green), 0);
        findViewById(R.id.cailing_manage_back).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CailingManageActivity.this.a();
            }
        });
        com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_CAILING, this.d);
        this.f2074b = new DDListFragment();
        this.f2073a = (TextView) findViewById(R.id.cailing_bottom_tips);
        g gVar = null;
        if (f.t()) {
            this.c = f.b.cm;
            gVar = new g(g.a.list_ring_cmcc, "", false, "");
            this.f2073a.setText((int) R.string.cmcc_manage_hint);
        } else if (f.v()) {
            this.c = f.b.ct;
            gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_ctcc, "", false, "");
            this.f2073a.setText((int) R.string.ctcc_manage_hint);
        } else if (f.u()) {
            this.c = f.b.cu;
            gVar = new com.shoujiduoduo.b.c.g(g.a.list_ring_cucc, "", false, "");
            this.f2073a.setText((int) R.string.cucc_manage_hint);
        }
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "cailing_list_adapter");
        this.f2074b.setArguments(bundle2);
        this.f2074b.a(gVar);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.list_content, this.f2074b);
        beginTransaction.commit();
        PlayerService b2 = z.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
    }

    /* renamed from: com.shoujiduoduo.ui.cailing.CailingManageActivity$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2079a = new int[f.b.values().length];

        static {
            try {
                f2079a[f.b.cu.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2079a[f.b.cm.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2079a[f.b.ct.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, this.d);
    }

    /* access modifiers changed from: private */
    public void a() {
        setResult(0);
        PlayerService b2 = z.a().b();
        if (b2 != null && b2.l()) {
            b2.m();
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 4) {
            a();
        }
        return super.onKeyDown(i, keyEvent);
    }
}
