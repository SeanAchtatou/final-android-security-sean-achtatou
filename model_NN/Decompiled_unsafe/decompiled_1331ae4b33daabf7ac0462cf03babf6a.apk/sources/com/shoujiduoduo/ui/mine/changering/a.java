package com.shoujiduoduo.ui.mine.changering;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.d;
import com.shoujiduoduo.ui.utils.l;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.z;
import java.io.File;

/* compiled from: SystemRingListAdapter */
public class a extends d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public g f2554a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f2555b = -1;
    /* access modifiers changed from: private */
    public boolean c;
    /* access modifiers changed from: private */
    public Context d;
    private o e = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void a(String str, int i) {
            if (a.this.f2554a != null && a.this.f2554a.a().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onSetPlay, listid:" + str);
                if (str.equals(a.this.f2554a.a())) {
                    boolean unused = a.this.c = true;
                    int unused2 = a.this.f2555b = i;
                } else {
                    boolean unused3 = a.this.c = false;
                }
                a.this.notifyDataSetChanged();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.changering.a, int]
         candidates:
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, int):int
          com.shoujiduoduo.ui.mine.changering.a.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.changering.a.a(com.shoujiduoduo.ui.mine.changering.a, boolean):boolean */
        public void b(String str, int i) {
            if (a.this.f2554a != null && a.this.f2554a.a().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onCanclePlay, listId:" + str);
                boolean unused = a.this.c = false;
                int unused2 = a.this.f2555b = i;
                a.this.notifyDataSetChanged();
            }
        }

        public void a(String str, int i, int i2) {
            if (a.this.f2554a != null && a.this.f2554a.a().equals(str)) {
                com.shoujiduoduo.base.a.a.a("SystemRingListAdapter", "onStatusChange, listid:" + str);
                a.this.notifyDataSetChanged();
            }
        }
    };
    private View.OnClickListener f = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                if (b2.a() == 3) {
                    b2.n();
                } else {
                    b2.i();
                }
            }
        }
    };
    private View.OnClickListener g = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                b2.j();
            }
        }
    };
    private View.OnClickListener h = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                b2.a(a.this.f2554a, a.this.f2555b);
            }
        }
    };
    private View.OnClickListener i = new View.OnClickListener() {
        public void onClick(View view) {
            if (a.this.f2554a.a(a.this.f2555b) != null) {
                final int i = 0;
                switch (AnonymousClass6.f2561a[a.this.f2554a.h().ordinal()]) {
                    case 1:
                        i = 4;
                        break;
                    case 2:
                        i = 2;
                        break;
                    case 3:
                        i = 1;
                        break;
                }
                ah ahVar = new ah(RingDDApp.c());
                File file = new File(a.this.f2554a.a(a.this.f2555b).o);
                if (file.exists()) {
                    ahVar.a(i, Uri.fromFile(file), a.this.f2554a.a(a.this.f2555b).o);
                    String str = "";
                    String str2 = a.this.f2554a.a(a.this.f2555b).e;
                    String string = a.this.d.getResources().getString(R.string.set_ring_hint2);
                    switch (i) {
                        case 1:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_incoming_call);
                            break;
                        case 2:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_message);
                            break;
                        case 4:
                            str = str2 + string + a.this.d.getResources().getString(R.string.set_ring_alarm);
                            break;
                    }
                    com.shoujiduoduo.util.widget.d.a(str);
                    c.a().b(b.OBSERVER_RING_CHANGE, new c.a<p>() {
                        public void a() {
                            ((p) this.f1812a).a(i, new RingData());
                        }
                    });
                }
            }
        }
    };

    public a(Context context) {
        this.d = context;
    }

    public void a() {
        c.a().a(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public void b() {
        c.a().b(b.OBSERVER_PLAY_STATUS, this.e);
    }

    public int getCount() {
        if (this.f2554a == null) {
            return 0;
        }
        return this.f2554a.c();
    }

    public Object getItem(int i2) {
        if (this.f2554a != null && i2 >= 0 && i2 < this.f2554a.c()) {
            return this.f2554a.a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    private void a(View view, int i2) {
        RingData c2 = this.f2554a.a(i2);
        TextView textView = (TextView) l.a(view, R.id.item_duration);
        ((TextView) l.a(view, R.id.item_song_name)).setText(c2.e);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(c2.l / 60), Integer.valueOf(c2.l % 60)));
        if (c2.l == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
    }

    /* renamed from: com.shoujiduoduo.ui.mine.changering.a$6  reason: invalid class name */
    /* compiled from: SystemRingListAdapter */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2561a = new int[g.a.values().length];

        static {
            try {
                f2561a[g.a.sys_alarm.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2561a[g.a.sys_notify.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2561a[g.a.sys_ringtone.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (this.f2554a == null) {
            return null;
        }
        if (i2 >= this.f2554a.c()) {
            return view;
        }
        if (view == null) {
            view = LayoutInflater.from(this.d).inflate((int) R.layout.system_ring_item, viewGroup, false);
        }
        a(view, i2);
        ProgressBar progressBar = (ProgressBar) l.a(view, R.id.ringitem_download_progress);
        TextView textView = (TextView) l.a(view, R.id.ringitem_serial_number);
        ImageButton imageButton = (ImageButton) l.a(view, R.id.ringitem_play);
        imageButton.setOnClickListener(this.f);
        ImageButton imageButton2 = (ImageButton) l.a(view, R.id.ringitem_pause);
        imageButton2.setOnClickListener(this.g);
        ImageButton imageButton3 = (ImageButton) l.a(view, R.id.ringitem_failed);
        imageButton3.setOnClickListener(this.h);
        PlayerService b2 = z.a().b();
        if (i2 != this.f2555b || !this.c) {
            ((Button) l.a(view, R.id.ring_item_set)).setVisibility(8);
            textView.setText(Integer.toString(i2 + 1));
            textView.setVisibility(0);
            progressBar.setVisibility(4);
            imageButton.setVisibility(4);
            imageButton2.setVisibility(4);
            imageButton3.setVisibility(4);
            return view;
        }
        Button button = (Button) l.a(view, R.id.ring_item_set);
        button.setVisibility(0);
        button.setOnClickListener(this.i);
        textView.setVisibility(4);
        imageButton.setVisibility(4);
        imageButton2.setVisibility(4);
        imageButton3.setVisibility(4);
        int i3 = 5;
        if (b2 != null) {
            i3 = b2.a();
        }
        switch (i3) {
            case 1:
                progressBar.setVisibility(0);
                return view;
            case 2:
                imageButton2.setVisibility(0);
                return view;
            case 3:
            case 4:
            case 5:
                imageButton.setVisibility(0);
                return view;
            case 6:
                imageButton3.setVisibility(0);
                return view;
            default:
                return view;
        }
    }

    public void a(boolean z) {
    }

    public void a(com.shoujiduoduo.base.bean.d dVar) {
        if (this.f2554a != dVar) {
            this.f2554a = null;
            this.f2554a = (com.shoujiduoduo.b.c.g) dVar;
            this.c = false;
            notifyDataSetChanged();
        }
    }
}
