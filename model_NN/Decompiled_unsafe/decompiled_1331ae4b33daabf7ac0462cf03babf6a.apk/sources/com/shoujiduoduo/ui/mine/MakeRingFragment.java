package com.shoujiduoduo.ui.mine;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.shoujiduoduo.util.z;
import java.util.List;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class MakeRingFragment extends Fragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public c f2430a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ListView f2431b;
    /* access modifiers changed from: private */
    public boolean c;
    private View d;
    private Button e;
    /* access modifiers changed from: private */
    public Button f;
    /* access modifiers changed from: private */
    public int g;
    private LinearLayout h;
    /* access modifiers changed from: private */
    public a i;
    /* access modifiers changed from: private */
    public boolean j;
    private View.OnKeyListener k = new View.OnKeyListener() {
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (i != 4 || keyEvent.getAction() != 0 || !MakeRingFragment.this.c) {
                return false;
            }
            com.shoujiduoduo.base.a.a.a("MakeRingFragment", "edit mode, key back");
            MakeRingFragment.this.a(false);
            return true;
        }
    };
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            MakeRingFragment.this.a(false);
            int unused = MakeRingFragment.this.g = 0;
            MakeRingFragment.this.f.setText("删除");
        }
    };
    private View.OnClickListener m = new View.OnClickListener() {
        public void onClick(View view) {
            final List<Integer> b2 = MakeRingFragment.this.i.b();
            if (b2 == null || b2.size() == 0) {
                d.a("请选择要删除的铃声", 0);
            } else {
                new b.a(MakeRingFragment.this.getActivity()).b((int) R.string.hint).a("确定删除选中的铃声吗？").a((int) R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (com.shoujiduoduo.a.b.b.b().a("make_ring_list", b2)) {
                            d.a("已删除" + b2.size() + "首铃声", 0);
                            MakeRingFragment.this.a(false);
                        } else {
                            d.a("删除铃声失败", 0);
                        }
                        dialogInterface.dismiss();
                    }
                }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).a().show();
            }
        }
    };
    private p n = new p() {
        public void a(int i, RingData ringData) {
            if (MakeRingFragment.this.f2430a != null) {
                MakeRingFragment.this.f2430a.notifyDataSetChanged();
            }
        }
    };
    private g o = new g() {
        public void a(com.shoujiduoduo.base.bean.d dVar, int i) {
            com.shoujiduoduo.base.a.a.a("MakeRingFragment", "data update");
            if (dVar != null && dVar.a().equals("user_make")) {
                MakeRingFragment.this.a();
                MakeRingFragment.this.a(false);
                MakeRingFragment.this.f2430a.notifyDataSetChanged();
            }
        }
    };
    private w p = new w() {
        public void a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.MakeRingFragment.a(com.shoujiduoduo.ui.mine.MakeRingFragment, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.MakeRingFragment, int]
         candidates:
          com.shoujiduoduo.ui.mine.MakeRingFragment.a(com.shoujiduoduo.ui.mine.MakeRingFragment, int):int
          com.shoujiduoduo.ui.mine.MakeRingFragment.a(com.shoujiduoduo.ui.mine.MakeRingFragment, boolean):boolean */
        public void a(int i, List<RingData> list, String str) {
            if (str.equals("user_make")) {
                MakeRingFragment.this.a();
                MakeRingFragment.this.f2431b.setAdapter((ListAdapter) MakeRingFragment.this.f2430a);
                boolean unused = MakeRingFragment.this.j = true;
            }
        }
    };

    static /* synthetic */ int g(MakeRingFragment makeRingFragment) {
        int i2 = makeRingFragment.g;
        makeRingFragment.g = i2 + 1;
        return i2;
    }

    static /* synthetic */ int h(MakeRingFragment makeRingFragment) {
        int i2 = makeRingFragment.g;
        makeRingFragment.g = i2 - 1;
        return i2;
    }

    public void onCreate(Bundle bundle) {
        this.f2430a = new c(getActivity());
        this.f2430a.a();
        this.i = new a(getActivity(), com.shoujiduoduo.a.b.b.b().a("make_ring_list"), "make_ring_list");
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onCreate");
        super.onCreate(bundle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.my_ringtone_make, viewGroup, false);
        this.f2431b = (ListView) inflate.findViewById(R.id.my_ringtone_make_list);
        View inflate2 = layoutInflater.inflate((int) R.layout.ringtone_diy, (ViewGroup) null, false);
        this.h = (LinearLayout) inflate.findViewById(R.id.no_data);
        ((Button) inflate2.findViewById(R.id.btn_ringtone_diy)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                PlayerService b2 = z.a().b();
                if (b2 != null && b2.l()) {
                    b2.m();
                }
                Intent intent = new Intent();
                intent.setClass(MakeRingFragment.this.getActivity(), MakeRingActivity.class);
                intent.setFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                MakeRingFragment.this.getActivity().startActivity(intent);
            }
        });
        this.f2431b.addFooterView(inflate2);
        if (com.shoujiduoduo.a.b.b.b().c()) {
            this.f2431b.setAdapter((ListAdapter) this.f2430a);
            this.j = true;
            a();
        }
        this.d = inflate.findViewById(R.id.del_confirm);
        this.e = (Button) this.d.findViewById(R.id.cancel);
        this.e.setOnClickListener(this.l);
        this.f = (Button) this.d.findViewById(R.id.delete);
        this.f.setOnClickListener(this.m);
        this.d.setVisibility(4);
        this.f2431b.setChoiceMode(1);
        this.f2431b.setOnItemClickListener(new a());
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.p);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.o);
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onCreateview");
        return inflate;
    }

    public void onDestroy() {
        this.f2430a.b();
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onDestroy");
    }

    public void onResume() {
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onResume");
        super.onResume();
        getView().setFocusable(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(this.k);
    }

    public void onDestroyView() {
        this.j = false;
        this.c = false;
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.p);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.o);
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_CHANGE, this.n);
        super.onDestroyView();
        com.shoujiduoduo.base.a.a.a("MakeRingFragment", "onDestroyView");
    }

    public void a(boolean z) {
        if (this.c != z && this.j) {
            this.c = z;
            this.d.setVisibility(z ? 0 : 8);
            if (z) {
                this.i.a(com.shoujiduoduo.a.b.b.b().a("make_ring_list"));
                this.f2431b.setAdapter((ListAdapter) this.i);
                this.g = 0;
                this.f.setText("删除");
                return;
            }
            this.f2431b.setAdapter((ListAdapter) this.f2430a);
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (com.shoujiduoduo.a.b.b.b().a("make_ring_list").c() > 0) {
            this.h.setVisibility(8);
        } else {
            this.h.setVisibility(0);
        }
    }

    private class a implements AdapterView.OnItemClickListener {
        private a() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (MakeRingFragment.this.c) {
                CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
                checkBox.toggle();
                MakeRingFragment.this.i.a().set(i, Boolean.valueOf(checkBox.isChecked()));
                if (checkBox.isChecked()) {
                    MakeRingFragment.g(MakeRingFragment.this);
                } else {
                    MakeRingFragment.h(MakeRingFragment.this);
                }
                if (MakeRingFragment.this.g > 0) {
                    MakeRingFragment.this.f.setText("删除(" + MakeRingFragment.this.g + ")");
                } else {
                    MakeRingFragment.this.f.setText("删除");
                }
            } else {
                PlayerService b2 = z.a().b();
                if (b2 != null) {
                    b2.a(com.shoujiduoduo.a.b.b.b().a("make_ring_list"), i);
                    MakeRingFragment.this.f2430a.notifyDataSetChanged();
                    return;
                }
                com.shoujiduoduo.base.a.a.c("MakeRingFragment", "PlayerService is unavailable!");
            }
        }
    }
}
