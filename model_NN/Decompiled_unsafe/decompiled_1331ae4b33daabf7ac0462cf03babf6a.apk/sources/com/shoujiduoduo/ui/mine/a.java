package com.shoujiduoduo.ui.mine;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.l;
import com.shoujiduoduo.util.ad;
import java.util.ArrayList;
import java.util.List;

/* compiled from: EditModeAdapter */
public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Context f2482a;

    /* renamed from: b  reason: collision with root package name */
    private d f2483b;
    private List<Boolean> c = new ArrayList();
    private String d;

    public a(Context context, d dVar, String str) {
        this.f2482a = context;
        this.f2483b = dVar;
        this.d = str;
        for (int i = 0; i < dVar.c(); i++) {
            this.c.add(false);
        }
    }

    public void a(d dVar) {
        this.f2483b = dVar;
        if (dVar.c() > 0) {
            this.c.clear();
            this.c = null;
            this.c = new ArrayList();
            for (int i = 0; i < dVar.c(); i++) {
                this.c.add(false);
            }
        }
    }

    public List<Boolean> a() {
        return this.c;
    }

    public List<Integer> b() {
        ArrayList arrayList;
        ArrayList arrayList2 = null;
        int i = 0;
        while (i < this.c.size()) {
            if (this.c.get(i).booleanValue()) {
                if (arrayList2 == null) {
                    arrayList = new ArrayList();
                } else {
                    arrayList = arrayList2;
                }
                arrayList.add(Integer.valueOf(i));
            } else {
                arrayList = arrayList2;
            }
            i++;
            arrayList2 = arrayList;
        }
        return arrayList2;
    }

    public int getCount() {
        if (this.f2483b != null) {
            return this.f2483b.c();
        }
        return 0;
    }

    public Object getItem(int i) {
        if (i < this.f2483b.c()) {
            return this.f2483b.a(i);
        }
        return null;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (this.f2483b != null && i < this.f2483b.c()) {
            if (view == null) {
                view = LayoutInflater.from(this.f2482a).inflate((int) R.layout.ring_item_delete_mode, viewGroup, false);
            }
            ((CheckBox) l.a(view, R.id.checkbox)).setChecked(this.c.get(i).booleanValue());
            if (this.d.equals("favorite_ring_list")) {
                a(view, i);
            } else if (this.d.equals("make_ring_list")) {
                b(view, i);
            }
        }
        return view;
    }

    private void a(View view, int i) {
        RingData ringData = (RingData) b.b().a("favorite_ring_list").a(i);
        TextView textView = (TextView) l.a(view, R.id.item_duration);
        ((TextView) l.a(view, R.id.item_song_name)).setText(ringData.e);
        ((TextView) l.a(view, R.id.item_artist)).setText(ringData.f);
        textView.setText(String.format("%02d:%02d", Integer.valueOf(ringData.l / 60), Integer.valueOf(ringData.l % 60)));
        if (ringData.l == 0) {
            textView.setVisibility(4);
        } else {
            textView.setVisibility(0);
        }
        ad.a(this.f2482a, "user_ring_phone_select", "ringtoneduoduo_not_set");
        ad.a(this.f2482a, "user_ring_alarm_select", "ringtoneduoduo_not_set");
        ad.a(this.f2482a, "user_ring_notification_select", "ringtoneduoduo_not_set");
    }

    private void b(View view, int i) {
        String string;
        MakeRingData makeRingData = (MakeRingData) b.b().a("make_ring_list").a(i);
        if (makeRingData != null) {
            TextView textView = (TextView) l.a(view, R.id.item_duration);
            ((TextView) l.a(view, R.id.item_song_name)).setText(makeRingData.e);
            ((TextView) l.a(view, R.id.item_artist)).setText(makeRingData.c);
            if (makeRingData.f1951a != 0) {
                string = this.f2482a.getResources().getString(R.string.upload_suc);
            } else if (makeRingData.f1952b == -1) {
                string = this.f2482a.getResources().getString(R.string.upload_error);
            } else if (makeRingData.f1952b == 0) {
                string = "";
            } else {
                string = this.f2482a.getResources().getString(R.string.uploading) + String.valueOf(makeRingData.f1952b) + "%";
            }
            if (!b.g().g()) {
                textView.setText(String.format("%02d:%02d", Integer.valueOf(makeRingData.l / 60), Integer.valueOf(makeRingData.l % 60)));
            } else {
                textView.setText(String.format("%02d:%02d %s", Integer.valueOf(makeRingData.l / 60), Integer.valueOf(makeRingData.l % 60), string));
            }
            if (makeRingData.l == 0) {
                textView.setVisibility(4);
            } else {
                textView.setVisibility(0);
            }
        }
    }
}
