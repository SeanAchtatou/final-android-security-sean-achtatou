package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.base.bean.k;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.user.UserInfoEditActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.ui.utils.ListViewForScrollView;
import com.shoujiduoduo.ui.utils.MyScrollView;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.j;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.z;

public class UserMainPageActivity extends SlidingActivity implements View.OnClickListener, MyScrollView.a, MyScrollView.b {
    /* access modifiers changed from: private */
    public static int H = 0;
    private ListViewForScrollView A;
    /* access modifiers changed from: private */
    public j B;
    private View C;
    /* access modifiers changed from: private */
    public g D;
    /* access modifiers changed from: private */
    public c E = c.LIST_FAILED;
    private int F;
    private int G;
    private Handler I = new Handler() {
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == UserMainPageActivity.H) {
                UserMainPageActivity.this.f();
            }
        }
    };
    private com.shoujiduoduo.a.c.g J = new com.shoujiduoduo.a.c.g() {
        public void a(d dVar, int i) {
            if (UserMainPageActivity.this.D != null && dVar.a().equals(UserMainPageActivity.this.D.a())) {
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "onDataUpdate in, id:" + UserMainPageActivity.this.D.a());
                switch (i) {
                    case 0:
                        if (UserMainPageActivity.this.E == c.LIST_LOADING) {
                            com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "show content now! listid:" + dVar.a());
                            UserMainPageActivity.this.a(c.LIST_CONTENT);
                        }
                        if (dVar.c() == 0) {
                            UserMainPageActivity.this.q.setVisibility(0);
                        } else {
                            UserMainPageActivity.this.q.setVisibility(8);
                        }
                        UserMainPageActivity.this.B.notifyDataSetChanged();
                        return;
                    case 1:
                        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "show failed now. listid:" + dVar.a());
                        UserMainPageActivity.this.a(c.LIST_FAILED);
                        return;
                    case 2:
                        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "more data ready. notify the adapter to update. listid:" + dVar.a());
                        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "FooterState: set failed onDataUpdate.");
                        UserMainPageActivity.this.a(a.RETRIEVE_FAILED);
                        UserMainPageActivity.this.B.notifyDataSetChanged();
                        return;
                    default:
                        return;
                }
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private TextView f2467a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f2468b;
    private TextView c;
    private TextView d;
    private TextView e;
    private ImageView f;
    private ImageView g;
    private ImageView h;
    /* access modifiers changed from: private */
    public UserData i;
    private boolean j;
    private String k;
    private int l;
    private int m;
    /* access modifiers changed from: private */
    public Button n;
    /* access modifiers changed from: private */
    public Button o;
    private TextView p;
    /* access modifiers changed from: private */
    public TextView q;
    private final String r = "关注TA";
    private final String s = "取消关注";
    private final String t = "已关注";
    private final String u = "编辑资料";
    private RelativeLayout v;
    private RelativeLayout w;
    private MyScrollView x;
    private RelativeLayout y;
    private RelativeLayout z;

    private enum a {
        RETRIEVE,
        TOTAL,
        RETRIEVE_FAILED,
        INVISIBLE
    }

    private enum c {
        LIST_CONTENT,
        LIST_LOADING,
        LIST_FAILED
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.b.c.g.<init>(com.shoujiduoduo.base.bean.g$a, java.lang.String, boolean):void
     arg types: [com.shoujiduoduo.base.bean.g$a, java.lang.String, int]
     candidates:
      com.shoujiduoduo.b.c.g.<init>(com.shoujiduoduo.base.bean.g$a, java.lang.String, java.lang.String):void
      com.shoujiduoduo.b.c.g.<init>(com.shoujiduoduo.base.bean.g$a, java.lang.String, boolean):void */
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_mainpage);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.tv_fans_hint).setOnClickListener(this);
        findViewById(R.id.tv_follow_hint).setOnClickListener(this);
        this.f2467a = (TextView) findViewById(R.id.user_fans);
        this.f2467a.setOnClickListener(this);
        this.f2468b = (TextView) findViewById(R.id.user_follow);
        this.f2468b.setOnClickListener(this);
        this.c = (TextView) findViewById(R.id.user_name);
        this.d = (TextView) findViewById(R.id.user_id);
        this.g = (ImageView) findViewById(R.id.user_head);
        this.e = (TextView) findViewById(R.id.tv_user_intro);
        this.n = (Button) findViewById(R.id.btn_follow);
        this.n.setOnClickListener(this);
        this.h = (ImageView) findViewById(R.id.iv_bkg);
        this.f = (ImageView) findViewById(R.id.iv_sex);
        this.z = (RelativeLayout) findViewById(R.id.loading_view);
        ((AnimationDrawable) ((ImageView) this.z.findViewById(R.id.loading)).getBackground()).start();
        this.y = (RelativeLayout) findViewById(R.id.failed_view);
        this.x = (MyScrollView) findViewById(R.id.sv_content);
        this.x.setOnScrollListener(this);
        this.x.setOnBorderListener(this);
        this.x.smoothScrollTo(0, 0);
        this.w = (RelativeLayout) findViewById(R.id.topbanner);
        this.v = (RelativeLayout) findViewById(R.id.user_info_layout);
        this.p = (TextView) findViewById(R.id.tv_top_username);
        this.o = (Button) findViewById(R.id.btn_top_follow);
        this.o.setOnClickListener(this);
        this.q = (TextView) findViewById(R.id.tv_nodata_hint);
        Intent intent = getIntent();
        if (intent != null) {
            this.k = intent.getStringExtra("tuid");
            String f2 = com.shoujiduoduo.a.b.b.g().f();
            if (!ag.c(f2) && f2.equals(this.k)) {
                this.j = true;
            }
            a(this.k, this.j);
            if (this.j) {
                this.n.setBackgroundResource(R.drawable.btn_bkg_yellow);
                this.n.setText("编辑资料");
            } else {
                this.n.setBackgroundResource(R.drawable.btn_bkg_wine_red);
                this.n.setText("关注TA");
            }
            this.l = intent.getIntExtra("fansNum", 0);
            this.m = intent.getIntExtra("followNum", 0);
            this.f2467a.setText("" + this.l);
            this.f2468b.setText("" + this.m);
        }
        this.A = (ListViewForScrollView) findViewById(R.id.list_view);
        this.B = new j(this);
        this.B.a(this.k);
        this.B.a();
        this.A.setOnItemClickListener(new b());
        this.C = getLayoutInflater().inflate((int) R.layout.get_more_rings, (ViewGroup) null, false);
        if (this.C != null) {
            this.A.addFooterView(this.C);
            this.C.setVisibility(4);
        }
        this.y.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (UserMainPageActivity.this.D != null) {
                    UserMainPageActivity.this.a(c.LIST_LOADING);
                    UserMainPageActivity.this.D.e();
                }
            }
        });
        this.D = new g(g.a.list_ring_user_upload, this.k, true);
        this.D.a(600000L);
        this.D.b(25);
        this.I.sendEmptyMessageDelayed(H, 200);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.J);
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "onDestroy");
        if (this.B != null) {
            this.B.b();
        }
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.J);
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            this.F = this.w.getHeight();
            this.G = this.v.getHeight();
        }
    }

    public void a(int i2) {
        if (i2 > this.G - this.F) {
            this.w.setBackgroundColor(Color.parseColor("#a0000000"));
            this.p.setVisibility(0);
            if (!this.j) {
                this.o.setVisibility(0);
                return;
            }
            return;
        }
        this.w.setBackgroundColor(0);
        this.p.setVisibility(8);
        this.o.setVisibility(8);
    }

    public void c() {
        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "滚动至底部啦！");
        if (this.D == null) {
            return;
        }
        if (this.D.g()) {
            if (!this.D.d()) {
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "还有数据，加载");
                this.D.e();
                a(a.RETRIEVE);
            }
        } else if (this.D.c() > 1) {
            com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "没有数据啦，显示总数");
            a(a.TOTAL);
        } else {
            a(a.INVISIBLE);
        }
    }

    public void d() {
    }

    /* access modifiers changed from: private */
    public void f() {
        this.A.setAdapter((ListAdapter) this.B);
        if (this.D != null) {
            this.B.a(this.D);
            if (this.D.c() == 0) {
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "loadListData: show loading panel, id:" + this.D.a());
                a(c.LIST_LOADING);
                if (!this.D.d()) {
                    this.D.e();
                    return;
                }
                return;
            }
            com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "setRingList: Show list content, id:" + this.D.a());
            a(c.LIST_CONTENT);
            return;
        }
        this.B.a((d) null);
        this.B.notifyDataSetChanged();
    }

    public class b implements AdapterView.OnItemClickListener {
        public b() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (UserMainPageActivity.this.D != null && j >= 0) {
                int i2 = (int) j;
                PlayerService b2 = z.a().b();
                if (b2 != null) {
                    b2.a(UserMainPageActivity.this.D, i2);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(a aVar) {
        if (this.C != null) {
            ImageView imageView = (ImageView) this.C.findViewById(R.id.more_data_loading);
            TextView textView = (TextView) this.C.findViewById(R.id.get_more_text);
            switch (aVar) {
                case RETRIEVE:
                    imageView.setVisibility(0);
                    ((AnimationDrawable) imageView.getBackground()).start();
                    textView.setText((int) R.string.ringlist_retrieving);
                    this.C.setVisibility(0);
                    return;
                case TOTAL:
                    imageView.setVisibility(8);
                    if (this.A.getCount() > (this.A.getHeaderViewsCount() > 0 ? 2 : 1)) {
                        String string = RingDDApp.c().getResources().getString(R.string.total);
                        int count = this.A.getCount();
                        if (this.A.getHeaderViewsCount() > 0) {
                            count -= this.A.getHeaderViewsCount();
                        }
                        textView.setText(string + (count - 1) + "首铃声");
                    }
                    this.C.setVisibility(0);
                    return;
                case RETRIEVE_FAILED:
                    imageView.setVisibility(8);
                    textView.setText((int) R.string.ringlist_retrieve_error);
                    this.C.setVisibility(0);
                    return;
                case INVISIBLE:
                    this.C.setVisibility(8);
                    return;
                default:
                    return;
            }
        }
    }

    public void a(c cVar) {
        this.A.setVisibility(4);
        this.y.setVisibility(4);
        this.z.setVisibility(4);
        switch (cVar) {
            case LIST_CONTENT:
                this.A.setVisibility(0);
                break;
            case LIST_LOADING:
                this.z.setVisibility(0);
                break;
            case LIST_FAILED:
                this.y.setVisibility(0);
                break;
        }
        this.E = cVar;
    }

    private void a(String str, boolean z2) {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.a()).append("&tuid=").append(str);
        if (z2) {
            sb.append("&username=").append(s.i(c2.b()));
            sb.append("&headurl=").append(s.i(c2.c()));
        }
        s.a("getuserinfo", sb.toString(), new s.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "userinfo:" + str);
                UserData b2 = i.b(str);
                if (b2 != null) {
                    UserData unused = UserMainPageActivity.this.i = b2;
                    UserMainPageActivity.this.a(b2);
                    return;
                }
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "user 解析失败");
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(UserData userData) {
        this.m = userData.k;
        this.l = userData.j;
        com.shoujiduoduo.base.a.a.a("UserMainPageActivity", "fansNum:" + this.l + ", followNum:" + this.m);
        this.c.setText(userData.f1955a);
        this.p.setText(this.i.f1955a);
        if (userData.j >= 0) {
            this.f2467a.setText("" + userData.j);
        }
        if (userData.k >= 0) {
            this.f2468b.setText("" + userData.k);
        }
        if (!ag.c(userData.f1956b)) {
            com.d.a.b.d.a().a(userData.f1956b, this.g, com.shoujiduoduo.ui.utils.g.a().d());
        }
        if (!ag.c(userData.g)) {
            this.d.setText("多多ID: " + userData.g);
        } else {
            this.d.setVisibility(4);
        }
        if (!ag.c(userData.e)) {
            com.d.a.b.d.a().a(userData.e, this.h, com.shoujiduoduo.ui.utils.g.a().k());
        } else {
            this.h.setImageResource(R.drawable.main_page_bkg);
        }
        if (!ag.c(userData.f)) {
            this.e.setText(userData.f);
        }
        if (!ag.c(userData.d)) {
            String str = userData.d;
            char c2 = 65535;
            switch (str.hashCode()) {
                case 22899:
                    if (str.equals("女")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 30007:
                    if (str.equals("男")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 657289:
                    if (str.equals("保密")) {
                        c2 = 2;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.f.setImageResource(R.drawable.icon_boy);
                    break;
                case 1:
                    this.f.setImageResource(R.drawable.icon_girl);
                    break;
                case 2:
                    this.f.setImageResource(R.drawable.icon_sex_secket);
                    break;
                default:
                    this.f.setVisibility(8);
                    break;
            }
        }
        if (this.j) {
            return;
        }
        if (userData.h) {
            this.n.setText("已关注");
            this.o.setText("已关注");
            return;
        }
        this.n.setText("关注TA");
        this.o.setText("关注TA");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.user_fans:
            case R.id.tv_fans_hint:
                Intent intent = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent.putExtra("type", "fans");
                intent.putExtra("tuid", this.k);
                intent.putExtra("fansNum", this.l);
                intent.putExtra("followNum", this.m);
                startActivity(intent);
                return;
            case R.id.user_follow:
            case R.id.tv_follow_hint:
                Intent intent2 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent2.putExtra("type", "follow");
                intent2.putExtra("tuid", this.k);
                intent2.putExtra("fansNum", this.l);
                intent2.putExtra("followNum", this.m);
                startActivity(intent2);
                return;
            case R.id.btn_follow:
            case R.id.btn_top_follow:
                if (this.j) {
                    h();
                    return;
                } else {
                    g();
                    return;
                }
            default:
                return;
        }
    }

    private void g() {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.i()) {
            String charSequence = this.n.getText().toString();
            k c3 = com.shoujiduoduo.a.b.b.g().c();
            String str = "&uid=" + c2.a() + "&tuid=" + this.k + "&username=" + q.a(c3.b()) + "&headurl=" + q.a(c3.c());
            if ("关注TA".equals(charSequence)) {
                s.a("follow", str, new s.a() {
                    public void a(String str) {
                        UserMainPageActivity.this.n.setText("取消关注");
                        UserMainPageActivity.this.o.setText("取消关注");
                        com.shoujiduoduo.util.widget.d.a("关注成功");
                    }

                    public void a(String str, String str2) {
                        com.shoujiduoduo.util.widget.d.a("关注失败");
                    }
                });
            } else {
                s.a("unfollow", str, new s.a() {
                    public void a(String str) {
                        UserMainPageActivity.this.n.setText("关注TA");
                        UserMainPageActivity.this.o.setText("关注TA");
                        com.shoujiduoduo.util.widget.d.a("取消关注成功");
                    }

                    public void a(String str, String str2) {
                        com.shoujiduoduo.util.widget.d.a("取消失败");
                    }
                });
            }
        } else {
            startActivity(new Intent(this, UserLoginActivity.class));
        }
    }

    private void h() {
        if (this.i != null) {
            Intent intent = new Intent(RingDDApp.c(), UserInfoEditActivity.class);
            intent.putExtra("userdata", this.i);
            startActivityForResult(intent, 1);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        char c2;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 1 && i3 == -1) {
            UserData userData = (UserData) intent.getParcelableExtra("new_user_data");
            if (!ag.c(userData.f1955a)) {
                this.c.setText(userData.f1955a);
            }
            if (!ag.c(userData.e)) {
                com.d.a.b.d.a().a(userData.e, this.h, com.shoujiduoduo.ui.utils.g.a().k());
            }
            if (!ag.c(userData.f1956b)) {
                com.d.a.b.d.a().a(userData.f1956b, this.g, com.shoujiduoduo.ui.utils.g.a().d());
            }
            if (!ag.c(userData.f)) {
                this.e.setText(userData.f);
            }
            if (!ag.c(userData.d)) {
                String str = userData.d;
                switch (str.hashCode()) {
                    case 22899:
                        if (str.equals("女")) {
                            c2 = 1;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 30007:
                        if (str.equals("男")) {
                            c2 = 0;
                            break;
                        }
                        c2 = 65535;
                        break;
                    case 657289:
                        if (str.equals("保密")) {
                            c2 = 2;
                            break;
                        }
                        c2 = 65535;
                        break;
                    default:
                        c2 = 65535;
                        break;
                }
                switch (c2) {
                    case 0:
                        this.f.setImageResource(R.drawable.icon_boy);
                        return;
                    case 1:
                        this.f.setImageResource(R.drawable.icon_girl);
                        return;
                    case 2:
                        this.f.setImageResource(R.drawable.icon_sex_secket);
                        return;
                    default:
                        this.f.setVisibility(8);
                        return;
                }
            }
        }
    }
}
