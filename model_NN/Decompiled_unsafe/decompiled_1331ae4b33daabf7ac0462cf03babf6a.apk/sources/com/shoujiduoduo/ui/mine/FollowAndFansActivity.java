package com.shoujiduoduo.ui.mine;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.b.c.k;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.ui.utils.i;
import com.shoujiduoduo.util.ag;
import java.util.ArrayList;
import java.util.List;
import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.a.a.d;

public class FollowAndFansActivity extends SlidingActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ViewPager f2424a;

    /* renamed from: b  reason: collision with root package name */
    private String f2425b;
    private boolean c;
    /* access modifiers changed from: private */
    public List<Fragment> d = new ArrayList();
    /* access modifiers changed from: private */
    public String[] e = {"关注", "粉丝"};
    private net.lucode.hackware.magicindicator.b.a.a.a f = new net.lucode.hackware.magicindicator.b.a.a.a() {
        public int a() {
            return 2;
        }

        public d a(Context context, final int i) {
            if (FollowAndFansActivity.this.d.size() <= 0) {
                return null;
            }
            net.lucode.hackware.magicindicator.b.a.d.a aVar = new net.lucode.hackware.magicindicator.b.a.d.a(context);
            aVar.setNormalColor(i.a(R.color.text_black));
            aVar.setSelectedColor(i.a(R.color.text_green));
            aVar.setText(FollowAndFansActivity.this.e[i]);
            aVar.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    FollowAndFansActivity.this.f2424a.setCurrentItem(i);
                }
            });
            return aVar;
        }

        public c a(Context context) {
            net.lucode.hackware.magicindicator.b.a.b.a aVar = new net.lucode.hackware.magicindicator.b.a.b.a(context);
            aVar.setMode(1);
            aVar.setColors(Integer.valueOf(i.a(R.color.text_green)));
            return aVar;
        }
    };

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        int i;
        int i2 = 0;
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_follow_fans);
        com.jaeger.library.a.a(this, i.a(R.color.bkg_green), 0);
        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getStringExtra("type").equals("fans")) {
                i = 0;
            } else {
                i = 1;
            }
            this.f2425b = intent.getStringExtra("tuid");
            String f2 = b.g().f();
            if (!ag.c(f2) && f2.equals(this.f2425b)) {
                this.c = true;
            }
            int intExtra = intent.getIntExtra("fansNum", 0);
            int intExtra2 = intent.getIntExtra("followNum", 0);
            com.shoujiduoduo.base.a.a.a("FollowAndFansActivity", "fans:" + intExtra + ", follow:" + intExtra2);
            this.e[0] = "粉丝 " + intExtra;
            this.e[1] = "关注 " + intExtra2;
            i2 = i;
        }
        this.f2424a = (ViewPager) findViewById(R.id.vPager);
        findViewById(R.id.back).setOnClickListener(this);
        this.f2424a.setAdapter(new a(getSupportFragmentManager()));
        MagicIndicator magicIndicator = (MagicIndicator) findViewById(R.id.magic_indicator);
        net.lucode.hackware.magicindicator.b.a.a aVar = new net.lucode.hackware.magicindicator.b.a.a(this);
        aVar.setAdapter(this.f);
        aVar.setAdjustMode(true);
        magicIndicator.setNavigator(aVar);
        magicIndicator.setBackgroundColor(i.a(R.color.white));
        net.lucode.hackware.magicindicator.d.a(magicIndicator, this.f2424a);
        k kVar = new k(k.a.fans, this.f2425b);
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putBoolean("support_lazy_load", true);
        bundle2.putString("adapter_type", "user_list_adapter");
        dDListFragment.setArguments(bundle2);
        dDListFragment.a(kVar);
        k kVar2 = new k(k.a.follow, this.f2425b);
        DDListFragment dDListFragment2 = new DDListFragment();
        Bundle bundle3 = new Bundle();
        bundle3.putBoolean("support_lazy_load", true);
        bundle3.putString("userlist_tuid", this.f2425b);
        bundle3.putString("adapter_type", "user_list_adapter");
        dDListFragment2.setArguments(bundle3);
        dDListFragment2.a(kVar2);
        this.d.add(dDListFragment);
        this.d.add(dDListFragment2);
        this.f2424a.getAdapter().notifyDataSetChanged();
        this.f.b();
        this.f2424a.setCurrentItem(i2);
        com.shoujiduoduo.base.a.a.a("FollowAndFansActivity", "onCreate end");
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            default:
                return;
        }
    }

    private class a extends FragmentPagerAdapter {
        public CharSequence getPageTitle(int i) {
            return FollowAndFansActivity.this.e[i];
        }

        a(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            return (Fragment) FollowAndFansActivity.this.d.get(i);
        }

        public int getCount() {
            return 2;
        }
    }
}
