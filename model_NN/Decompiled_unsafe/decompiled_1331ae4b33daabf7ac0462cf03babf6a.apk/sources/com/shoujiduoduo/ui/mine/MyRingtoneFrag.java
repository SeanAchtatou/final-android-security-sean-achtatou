package com.shoujiduoduo.ui.mine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.k;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.changering.CurrentRingSettingActivity;
import com.shoujiduoduo.ui.settings.SettingActivity;
import com.shoujiduoduo.ui.user.RingListActivity;
import com.shoujiduoduo.ui.user.UserCenterActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.widget.WebViewActivity;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MyRingtoneFrag extends Fragment implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ListView f2443a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ArrayList<b> f2444b = new ArrayList<>();
    /* access modifiers changed from: private */
    public c c;
    /* access modifiers changed from: private */
    public LayoutInflater d;
    private AdapterView.OnItemClickListener e = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            switch (AnonymousClass6.f2450a[((b) MyRingtoneFrag.this.f2444b.get(i)).f2453a.ordinal()]) {
                case 1:
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "open user center");
                    MyRingtoneFrag.this.a();
                    return;
                case 2:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), com.shoujiduoduo.a.b.b.g().g() ? UserCenterActivity.class : UserLoginActivity.class));
                    return;
                case 3:
                    Intent intent = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent.putExtra("list_id", "user_favorite");
                    intent.putExtra("title", "我的铃声收藏");
                    MyRingtoneFrag.this.startActivity(intent);
                    return;
                case 4:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), CurrentRingSettingActivity.class));
                    return;
                case 5:
                    Intent intent2 = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent2.putExtra("list_id", "user_collect");
                    intent2.putExtra("title", "我的精选集");
                    MyRingtoneFrag.this.startActivity(intent2);
                    return;
                case 6:
                    Intent intent3 = new Intent(RingDDApp.c(), RingListActivity.class);
                    intent3.putExtra("list_id", "user_make");
                    intent3.putExtra("title", "我的作品");
                    MyRingtoneFrag.this.startActivity(intent3);
                    return;
                case 7:
                    Intent intent4 = new Intent(RingDDApp.c(), WebViewActivity.class);
                    intent4.putExtra("url", MyRingtoneFrag.this.b());
                    intent4.putExtra("title", "我的快秀");
                    MyRingtoneFrag.this.startActivity(intent4);
                    return;
                case 8:
                    MyRingtoneFrag.this.startActivity(new Intent(RingDDApp.c(), SettingActivity.class));
                    return;
                case 9:
                    if (com.shoujiduoduo.a.b.b.g().g()) {
                        Intent intent5 = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                        intent5.putExtra("tuid", com.shoujiduoduo.a.b.b.g().c().a());
                        MyRingtoneFrag.this.startActivity(intent5);
                        return;
                    }
                    MyRingtoneFrag.this.startActivity(new Intent(MyRingtoneFrag.this.getActivity(), UserLoginActivity.class));
                    return;
                default:
                    return;
            }
        }
    };
    private g f = new g() {
        public void a(d dVar, int i) {
            MyRingtoneFrag.this.a(dVar.a());
        }
    };
    private x g = new x() {
        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onVipStateChange");
            MyRingtoneFrag.this.c.notifyDataSetChanged();
        }
    };
    private v h = new v() {
        public void a(int i, boolean z, String str, String str2) {
            if (z) {
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onLogin");
                MyRingtoneFrag.this.c.notifyDataSetChanged();
            }
        }

        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onLogout");
            MyRingtoneFrag.this.c.notifyDataSetChanged();
        }

        public void b(int i) {
            MyRingtoneFrag.this.c.notifyDataSetChanged();
        }
    };
    private w i = new w() {
        public void a() {
        }

        public void a(int i, List<RingData> list, String str) {
            MyRingtoneFrag.this.a(str);
        }
    };

    private enum a {
        user_center,
        vip_center,
        ring_favorite,
        current_ring,
        collect,
        work,
        kuaixiu,
        setting,
        main_page
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate((int) R.layout.fragment_mine, viewGroup, false);
        this.d = layoutInflater;
        c();
        this.f2443a = (ListView) inflate.findViewById(R.id.lv_mine_item);
        this.c = new c();
        this.f2443a.setAdapter((ListAdapter) this.c);
        this.f2443a.setOnItemClickListener(this.e);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.g);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.h);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.i);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.f);
        return inflate;
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_icon:
            case R.id.tv_number:
                a();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
            intent.putExtra("tuid", com.shoujiduoduo.a.b.b.g().c().a());
            startActivity(intent);
            return;
        }
        startActivity(new Intent(getActivity(), UserLoginActivity.class));
    }

    /* renamed from: com.shoujiduoduo.ui.mine.MyRingtoneFrag$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2450a = new int[a.values().length];

        static {
            try {
                f2450a[a.user_center.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f2450a[a.vip_center.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f2450a[a.ring_favorite.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f2450a[a.current_ring.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f2450a[a.collect.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f2450a[a.work.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f2450a[a.kuaixiu.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                f2450a[a.setting.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                f2450a[a.main_page.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
        }
    }

    /* access modifiers changed from: private */
    public String b() {
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "personal_kuaixiu_url");
        if (ag.b(a2)) {
            a2 = "http://musicalbum.shoujiduoduo.com/malbum/serv/user.php?ddsrc=upload_ring_ar&owner=1";
        }
        com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "personal kuaixiu url:" + com.shoujiduoduo.util.g.b(a2));
        return com.shoujiduoduo.util.g.b(a2);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "onDataUpdate, listId:" + str);
        if ("user_favorite".equals(str)) {
            Iterator<b> it = this.f2444b.iterator();
            while (it.hasNext()) {
                b next = it.next();
                if (next.f2453a == a.ring_favorite) {
                    next.f = "" + com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").c();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user favorite ring size:" + next.f);
                }
            }
            this.c.notifyDataSetChanged();
        } else if ("user_collect".equals(str)) {
            Iterator<b> it2 = this.f2444b.iterator();
            while (it2.hasNext()) {
                b next2 = it2.next();
                if (next2.f2453a == a.collect) {
                    next2.f = "" + com.shoujiduoduo.a.b.b.b().a("collect_ring_list").c();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user collect ring size:" + next2.f);
                }
            }
            this.c.notifyDataSetChanged();
        } else if ("user_make".equals(str)) {
            Iterator<b> it3 = this.f2444b.iterator();
            while (it3.hasNext()) {
                b next3 = it3.next();
                if (next3.f2453a == a.work) {
                    next3.f = "" + com.shoujiduoduo.a.b.b.b().a("make_ring_list").c();
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user make ring size:" + next3.f);
                }
            }
            this.c.notifyDataSetChanged();
        }
    }

    private class b {

        /* renamed from: a  reason: collision with root package name */
        a f2453a;

        /* renamed from: b  reason: collision with root package name */
        String f2454b;
        String c;
        int d;
        boolean e = true;
        String f;

        b(String str, int i, a aVar) {
            this.f2454b = str;
            this.d = i;
            this.f2453a = aVar;
        }

        public void a(String str) {
            this.c = str;
        }

        public void a(boolean z) {
            this.e = z;
        }

        public void a(int i) {
            this.f = "" + i;
        }

        public void b(String str) {
            this.f = str;
        }
    }

    private void c() {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.i()) {
            b bVar = new b(c2.b(), R.drawable.user_head, a.user_center);
            bVar.a(true);
            bVar.b("个人主页");
            if (!c2.k()) {
                bVar.a("已登录，尚未开通VIP");
            }
            this.f2444b.add(bVar);
        } else {
            b bVar2 = new b("点击登录", R.drawable.user_head, a.user_center);
            bVar2.a(false);
            bVar2.b("个人主页");
            bVar2.a("登录开启云端收藏铃声");
            this.f2444b.add(bVar2);
        }
        if (!c2.k() || !c2.i()) {
            b bVar3 = new b("开通VIP", R.drawable.icon_vip_not_open, a.vip_center);
            bVar3.a("立即开通享有特权");
            this.f2444b.add(bVar3);
        } else {
            b bVar4 = new b("我的VIP特权", R.drawable.icon_vip_opened, a.vip_center);
            bVar4.a("已享VIP特权");
            this.f2444b.add(bVar4);
        }
        int c3 = com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").c();
        int c4 = com.shoujiduoduo.a.b.b.b().a("collect_ring_list").c();
        int c5 = com.shoujiduoduo.a.b.b.b().a("make_ring_list").c();
        b bVar5 = new b("我的铃声收藏", R.drawable.icon_my_favorite, a.ring_favorite);
        bVar5.a(c3);
        this.f2444b.add(bVar5);
        this.f2444b.add(new b("我的当前铃声", R.drawable.icon_my_current_ring, a.current_ring));
        b bVar6 = new b("我的精选集", R.drawable.icon_my_collect, a.collect);
        bVar6.a(c4);
        this.f2444b.add(bVar6);
        b bVar7 = new b("我的作品", R.drawable.icon_my_works, a.work);
        bVar7.a(c5);
        this.f2444b.add(bVar7);
        this.f2444b.add(new b("我的快秀", R.drawable.icon_my_kuaixiu, a.kuaixiu));
        this.f2444b.add(new b("设置", R.drawable.icon_setting, a.setting));
    }

    public void onDestroyView() {
        super.onDestroyView();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.g);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.h);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, this.i);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.f);
    }

    private class c extends BaseAdapter {
        private c() {
        }

        public int getCount() {
            return MyRingtoneFrag.this.f2444b.size();
        }

        public Object getItem(int i) {
            return MyRingtoneFrag.this.f2444b.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                if (getItemViewType(i) == 0) {
                    view = MyRingtoneFrag.this.d.inflate((int) R.layout.listitem_user, viewGroup, false);
                } else {
                    view = MyRingtoneFrag.this.d.inflate((int) R.layout.listitem_mine, viewGroup, false);
                }
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_icon);
            TextView textView = (TextView) view.findViewById(R.id.tv_title);
            TextView textView2 = (TextView) view.findViewById(R.id.tv_des);
            TextView textView3 = (TextView) view.findViewById(R.id.tv_number);
            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.divider_line);
            LinearLayout linearLayout2 = (LinearLayout) view.findViewById(R.id.divider_gray);
            ImageView imageView2 = (ImageView) view.findViewById(R.id.iv_right_arrow);
            b bVar = (b) MyRingtoneFrag.this.f2444b.get(i);
            if (bVar.f2453a == a.user_center || bVar.f2453a == a.vip_center || bVar.f2453a == a.kuaixiu) {
                linearLayout.setVisibility(4);
                linearLayout2.setVisibility(0);
            } else {
                linearLayout.setVisibility(0);
                linearLayout2.setVisibility(8);
            }
            if (bVar.f2453a == a.user_center) {
                k c = com.shoujiduoduo.a.b.b.g().c();
                textView3.setTextColor(MyRingtoneFrag.this.getResources().getColor(R.color.text_green));
                if (c.i()) {
                    textView3.setVisibility(0);
                    com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "user is login, number:" + bVar.f);
                    imageView2.setVisibility(0);
                    textView.setText(c.b());
                    if (!ag.c(c.c())) {
                        com.d.a.b.d.a().a(c.c(), imageView, com.shoujiduoduo.ui.utils.g.a().d());
                    } else {
                        imageView.setImageResource(bVar.d);
                    }
                    if (c.k()) {
                        textView2.setVisibility(4);
                        view.findViewById(R.id.iv_vip_show).setVisibility(0);
                    } else {
                        textView2.setVisibility(0);
                        textView2.setText("已登录，尚未开通VIP");
                        view.findViewById(R.id.iv_vip_show).setVisibility(4);
                    }
                } else {
                    textView3.setVisibility(4);
                    imageView2.setVisibility(4);
                    textView.setText("点击登录");
                    textView2.setText("登录开启云端收藏");
                    textView2.setVisibility(0);
                    imageView.setImageResource(bVar.d);
                    view.findViewById(R.id.iv_vip_show).setVisibility(4);
                }
            } else if (bVar.f2453a == a.vip_center) {
                imageView2.setVisibility(0);
                k c2 = com.shoujiduoduo.a.b.b.g().c();
                if (!c2.i() || !c2.k()) {
                    textView.setText("开通VIP会员");
                    textView2.setText("立即开通享特权");
                    imageView.setImageResource(R.drawable.icon_vip_not_open);
                } else {
                    textView.setText("我的VIP特权");
                    textView2.setText("已享VIP特权");
                    imageView.setImageResource(R.drawable.icon_vip_opened);
                }
            } else {
                textView.setText(bVar.f2454b);
                if (!ag.c(bVar.c)) {
                    textView2.setText(bVar.c);
                } else {
                    textView2.setVisibility(8);
                }
                imageView.setImageResource(bVar.d);
            }
            if (bVar.f2453a == a.ring_favorite) {
                bVar.f = "" + com.shoujiduoduo.a.b.b.b().a("favorite_ring_list").c();
            } else if (bVar.f2453a == a.work) {
                bVar.f = "" + com.shoujiduoduo.a.b.b.b().a("make_ring_list").c();
            } else if (bVar.f2453a == a.collect) {
                bVar.f = "" + com.shoujiduoduo.a.b.b.b().a("collect_ring_list").c();
                com.shoujiduoduo.base.a.a.a("MyRingtoneFrag", "getView, collect size;" + bVar.f);
            }
            if (!ag.c(bVar.f)) {
                textView3.setText(bVar.f);
            } else {
                textView3.setVisibility(4);
            }
            return view;
        }

        public int getItemViewType(int i) {
            if (i == 0) {
                return 0;
            }
            return 1;
        }

        public int getViewTypeCount() {
            return 2;
        }
    }
}
