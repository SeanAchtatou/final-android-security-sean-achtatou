package com.shoujiduoduo.ui.mine.changering;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.jaeger.library.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.mine.changering.RingSettingFragment;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;

public class CurrentRingSettingActivity extends BaseFragmentActivity implements RingSettingFragment.a {

    /* renamed from: a  reason: collision with root package name */
    private ImageButton f2513a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f2514b;
    private boolean c;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_current_ring);
        a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.c = false;
        this.f2514b = (TextView) findViewById(R.id.header_title);
        this.f2513a = (ImageButton) findViewById(R.id.backButton);
        this.f2513a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                CurrentRingSettingActivity.this.finish();
            }
        });
        RingSettingFragment ringSettingFragment = new RingSettingFragment();
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(R.id.frag, ringSettingFragment);
        beginTransaction.commitAllowingStateLoss();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c = true;
        super.onDestroy();
    }

    public void a(int i) {
        if (!this.c) {
            switch (i) {
                case 0:
                    this.f2514b.setText((int) R.string.set_ringtone);
                    break;
                case 1:
                    this.f2514b.setText((int) R.string.set_notification);
                    break;
                case 2:
                    this.f2514b.setText((int) R.string.set_alarm);
                    break;
                case 3:
                    this.f2514b.setText((int) R.string.set_coloring);
                    break;
            }
            ChangeRingFragment changeRingFragment = new ChangeRingFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("type", i);
            changeRingFragment.setArguments(bundle);
            FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
            beginTransaction.replace(R.id.frag, changeRingFragment);
            beginTransaction.addToBackStack("changering");
            beginTransaction.commitAllowingStateLoss();
        }
    }
}
