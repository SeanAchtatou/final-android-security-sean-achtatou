package com.shoujiduoduo.ui.mine;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.a.c.o;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.user.UserLoginActivity;
import com.shoujiduoduo.ui.utils.l;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ai;
import com.shoujiduoduo.util.widget.CircleProgressBar;
import com.shoujiduoduo.util.widget.d;
import com.shoujiduoduo.util.z;
import com.tencent.open.SocialConstants;
import java.io.File;

/* compiled from: MakeRingListAdapter */
class c extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f2494a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Activity f2495b;
    /* access modifiers changed from: private */
    public int c = -1;
    /* access modifiers changed from: private */
    public boolean d = false;
    private q e = new q() {
        public void a(RingData ringData) {
            c.this.notifyDataSetChanged();
        }

        public void a(RingData ringData, int i) {
            c.this.notifyDataSetChanged();
        }

        public void c(RingData ringData) {
            c.this.notifyDataSetChanged();
        }

        public void b(RingData ringData) {
            a.a("MyRingMakeAdapter", "makeringlistadapter, onuploadcomplete");
            d.a("铃声上传成功！");
            c.this.notifyDataSetChanged();
        }
    };
    private o f = new o() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.c, int]
         candidates:
          com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, int):int
          com.shoujiduoduo.ui.mine.c.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, boolean):boolean */
        public void a(String str, int i) {
            boolean unused = c.this.d = true;
            int unused2 = c.this.c = i;
            c.this.notifyDataSetChanged();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, boolean):boolean
         arg types: [com.shoujiduoduo.ui.mine.c, int]
         candidates:
          com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, int):int
          com.shoujiduoduo.ui.mine.c.a(android.view.View, int):void
          com.shoujiduoduo.ui.mine.c.a(com.shoujiduoduo.ui.mine.c, boolean):boolean */
        public void b(String str, int i) {
            boolean unused = c.this.d = false;
            int unused2 = c.this.c = -1;
            c.this.notifyDataSetChanged();
        }

        public void a(String str, int i, int i2) {
            c.this.notifyDataSetChanged();
        }
    };
    /* access modifiers changed from: private */
    public DialogInterface.OnClickListener g = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialogInterface, int i) {
            switch (i) {
                case -2:
                default:
                    return;
                case -1:
                    PlayerService b2 = z.a().b();
                    if (b2 != null && b.b().a("make_ring_list").a(c.this.c) != null) {
                        if (b2.f() == ((RingData) b.b().a("make_ring_list").a(c.this.c)).k()) {
                            b2.e();
                        }
                        int a2 = c.this.c;
                        int unused = c.this.c = -1;
                        b.b().a("make_ring_list", a2);
                        return;
                    }
                    return;
            }
        }
    };
    private View.OnClickListener h = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("MyRingMakeAdapter", "RingtoneDuoduo: click share button!");
            RingData ringData = (RingData) b.b().a("make_ring_list").a(c.this.c);
            if (ringData != null) {
                ai.a().a(c.this.f2495b, ringData, "makering");
            }
        }
    };
    private View.OnClickListener i = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("MyRingMakeAdapter", "RingtoneDuoduo: click upload button!");
            com.umeng.analytics.b.b(c.this.f2495b, "USER_CLICK_UPLOAD");
            if (!b.g().g()) {
                c.this.f2495b.startActivity(new Intent(c.this.f2495b, UserLoginActivity.class));
                return;
            }
            RingData ringData = (RingData) b.b().a("make_ring_list").a(c.this.c);
            if (ringData != null) {
                String str = ((MakeRingData) ringData).o;
                if (!TextUtils.isEmpty(str)) {
                    File file = new File(str);
                    if (!file.exists() || file.length() == 0) {
                        Toast.makeText(c.this.f2495b, (int) R.string.upload_file_error, 1);
                        a.c("MyRingMakeAdapter", "当前录制铃声长度太小，不满足上传要求");
                        return;
                    }
                    new d(c.this.f2495b, R.style.DuoDuoDialog, ringData).show();
                    return;
                }
                Toast.makeText(c.this.f2495b, (int) R.string.file_not_found, 1);
                a.c("MyRingMakeAdapter", "未找到当前录制铃声");
            }
        }
    };
    private View.OnClickListener j = new View.OnClickListener() {
        public void onClick(View view) {
            if (c.this.c >= 0) {
                new AlertDialog.Builder(c.this.f2495b).setTitle((int) R.string.hint).setMessage((int) R.string.delete_ring_confirm).setIcon(17301543).setPositiveButton((int) R.string.ok, c.this.g).setNegativeButton((int) R.string.cancel, c.this.g).show();
            }
        }
    };
    private View.OnClickListener k = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("MyRingMakeAdapter", "MyRingtoneScene:clickApplyButton:SetRingTone:getInstance.");
            RingData ringData = (RingData) b.b().a("make_ring_list").a(c.this.c);
            if (ringData != null) {
                new com.shoujiduoduo.ui.settings.b(c.this.f2495b, R.style.DuoDuoDialog, ringData, "user_make_ring", g.a.list_user_make.toString()).show();
            }
        }
    };
    private View.OnClickListener l = new View.OnClickListener() {
        public void onClick(View view) {
            a.a("MyRingMakeAdapter", "RingtoneDuoduo: click weixiu button!");
            RingData ringData = (RingData) b.b().a("make_ring_list").a(c.this.c);
            Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
            intent.putExtra("musicid", ringData.g);
            intent.putExtra("title", "快秀");
            intent.putExtra(SocialConstants.PARAM_APP_DESC, ringData.e);
            intent.putExtra("type", MusicAlbumActivity.a.create_ring_story);
            RingToneDuoduoActivity.a().startActivity(intent);
        }
    };
    private View.OnClickListener m = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                if (b2.a() == 3) {
                    b2.n();
                } else {
                    b2.i();
                }
            }
        }
    };
    private View.OnClickListener n = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                b2.j();
            }
        }
    };
    private View.OnClickListener o = new View.OnClickListener() {
        public void onClick(View view) {
            PlayerService b2;
            if (c.this.c >= 0 && (b2 = z.a().b()) != null) {
                b2.a(b.b().a("make_ring_list"), c.this.c);
            }
        }
    };

    public c(Activity activity) {
        this.f2495b = activity;
        this.f2494a = LayoutInflater.from(this.f2495b);
    }

    public void a() {
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.e);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.f);
    }

    public void b() {
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_RING_UPLOAD, this.e);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_PLAY_STATUS, this.f);
    }

    private void a(View view, int i2) {
        String string;
        String str;
        MakeRingData makeRingData = (MakeRingData) b.b().a("make_ring_list").a(i2);
        if (makeRingData != null) {
            TextView textView = (TextView) l.a(view, R.id.tv_duradion);
            TextView textView2 = (TextView) l.a(view, R.id.tv_upload_info);
            ((TextView) l.a(view, R.id.item_song_name)).setText(makeRingData.e);
            ((TextView) l.a(view, R.id.item_artist)).setText(makeRingData.c);
            if (!ag.c(makeRingData.g)) {
                string = this.f2495b.getResources().getString(R.string.upload_suc);
            } else if (makeRingData.f1952b == -1) {
                string = this.f2495b.getResources().getString(R.string.upload_error);
            } else if (makeRingData.f1952b == 0) {
                string = "";
            } else {
                string = this.f2495b.getResources().getString(R.string.uploading) + String.valueOf(makeRingData.f1952b) + "%";
            }
            textView2.setText(string);
            if (makeRingData.l > 60) {
                str = "" + (makeRingData.l / 60) + "分" + (makeRingData.l % 60) + "秒";
            } else {
                str = "" + makeRingData.l + "秒";
            }
            textView.setText(str);
            if (makeRingData.l == 0) {
                textView.setVisibility(4);
            } else {
                textView.setVisibility(0);
            }
        }
    }

    public int getCount() {
        return b.b().a("make_ring_list").c();
    }

    public Object getItem(int i2) {
        if (i2 < b.b().a("make_ring_list").c()) {
            return b.b().a("make_ring_list").a(i2);
        }
        return null;
    }

    public long getItemId(int i2) {
        return (long) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i2, View view, ViewGroup viewGroup) {
        if (i2 < b.b().a("make_ring_list").c()) {
            if (view == null) {
                view = this.f2494a.inflate((int) R.layout.listitem_make_ring, viewGroup, false);
            }
            a(view, i2);
            ProgressBar progressBar = (ProgressBar) l.a(view, R.id.ringitem_download_progress);
            CircleProgressBar circleProgressBar = (CircleProgressBar) l.a(view, R.id.play_progress_bar);
            TextView textView = (TextView) l.a(view, R.id.ringitem_serial_number);
            ImageButton imageButton = (ImageButton) l.a(view, R.id.ringitem_play);
            ImageButton imageButton2 = (ImageButton) l.a(view, R.id.ringitem_pause);
            ImageButton imageButton3 = (ImageButton) l.a(view, R.id.ringitem_failed);
            imageButton.setOnClickListener(this.m);
            imageButton2.setOnClickListener(this.n);
            imageButton3.setOnClickListener(this.o);
            String str = "";
            PlayerService b2 = z.a().b();
            if (b2 != null) {
                str = b2.b();
                this.c = b2.c();
            }
            if (i2 == this.c && str.equals(b.b().a("make_ring_list").a())) {
                Button button = (Button) l.a(view, R.id.ring_item_button0);
                Button button2 = (Button) l.a(view, R.id.ring_item_button1);
                Button button3 = (Button) l.a(view, R.id.ring_item_button2);
                Button button4 = (Button) l.a(view, R.id.ring_item_button4);
                button.setVisibility(0);
                if (!((MakeRingData) b.b().a("make_ring_list").a(i2)).g.equals("")) {
                    button.setOnClickListener(this.h);
                    button.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.icon_ringitem_share, 0, 0, 0);
                    button.setText((int) R.string.share);
                    if (!"false".equals(com.umeng.a.a.a().a(RingDDApp.c(), "quick_show_upload_switch"))) {
                        button4.setVisibility(0);
                    } else {
                        button4.setVisibility(8);
                    }
                } else {
                    button.setOnClickListener(this.i);
                    button.setCompoundDrawablesWithIntrinsicBounds((int) R.drawable.icon_ringitem_upload, 0, 0, 0);
                    button.setText((int) R.string.upload);
                    button4.setVisibility(8);
                }
                button4.setOnClickListener(this.l);
                button2.setVisibility(0);
                button2.setOnClickListener(this.j);
                button3.setVisibility(0);
                button3.setOnClickListener(this.k);
                textView.setVisibility(4);
                progressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
                PlayerService b3 = z.a().b();
                RingData ringData = (RingData) b.b().a("make_ring_list").a(i2);
                if (this.d && b3 != null && b3.f() == ringData.k()) {
                    switch (b3.a()) {
                        case 1:
                            progressBar.setVisibility(0);
                            circleProgressBar.setVisibility(4);
                            break;
                        case 2:
                            imageButton2.setVisibility(0);
                            circleProgressBar.setVisibility(0);
                            break;
                        case 3:
                        case 4:
                        case 5:
                            imageButton.setVisibility(0);
                            circleProgressBar.setVisibility(0);
                            break;
                        case 6:
                            imageButton3.setVisibility(0);
                            circleProgressBar.setVisibility(4);
                            break;
                    }
                } else {
                    imageButton.setVisibility(0);
                }
            } else {
                ((Button) l.a(view, R.id.ring_item_button0)).setVisibility(8);
                ((Button) l.a(view, R.id.ring_item_button1)).setVisibility(8);
                ((Button) l.a(view, R.id.ring_item_button2)).setVisibility(8);
                ((Button) l.a(view, R.id.ring_item_button4)).setVisibility(8);
                textView.setText(Integer.toString(i2 + 1));
                textView.setVisibility(0);
                progressBar.setVisibility(4);
                circleProgressBar.setVisibility(4);
                imageButton.setVisibility(4);
                imageButton2.setVisibility(4);
                imageButton3.setVisibility(4);
            }
        }
        return view;
    }
}
