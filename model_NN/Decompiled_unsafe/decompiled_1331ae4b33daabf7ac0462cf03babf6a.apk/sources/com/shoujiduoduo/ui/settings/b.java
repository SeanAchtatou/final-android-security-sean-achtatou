package com.shoujiduoduo.ui.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.util.ac;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.r;
import com.shoujiduoduo.util.widget.b;
import java.util.ArrayList;

/* compiled from: SetRingDialog */
public class b extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f2646a;

    /* renamed from: b  reason: collision with root package name */
    private ListView f2647b;
    private Button c;
    private Button d;
    /* access modifiers changed from: private */
    public ArrayList<C0039b> e;
    /* access modifiers changed from: private */
    public a f;
    /* access modifiers changed from: private */
    public RingData g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public int j = 0;
    private AdapterView.OnItemClickListener k = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            if (checkBox.isChecked()) {
                com.shoujiduoduo.base.a.a.a("SetRingDialog", "onItemClick:" + i + "set to unchecked");
                checkBox.setChecked(false);
            } else {
                com.shoujiduoduo.base.a.a.a("SetRingDialog", "onItemClick:" + i + "set to checked");
                checkBox.setChecked(true);
            }
            if (((C0039b) b.this.e.get(i)).e == c.contact) {
                com.umeng.analytics.b.b(b.this.f2646a, "SET_CONTACT_RING");
                Intent intent = new Intent(b.this.f2646a, ContactRingSettingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("ringdata", b.this.g);
                intent.putExtras(bundle);
                intent.putExtra("listid", b.this.h);
                intent.putExtra("listtype", b.this.i);
                b.this.f2646a.startActivity(intent);
                b.this.dismiss();
            }
        }
    };

    /* compiled from: SetRingDialog */
    private enum c {
        ring,
        sms,
        alarm,
        contact,
        cailing
    }

    public b(Context context, int i2, RingData ringData, String str, String str2) {
        super(context, i2);
        this.f2646a = context;
        this.g = ringData;
        this.h = str;
        this.i = str2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialog_set_ring);
        this.f2647b = (ListView) findViewById(R.id.set_ring_action_list);
        this.f2647b.setItemsCanFocus(false);
        this.f2647b.setChoiceMode(2);
        this.f2647b.setOnItemClickListener(this.k);
        this.c = (Button) findViewById(R.id.set_ring_cancel);
        this.f = new a();
        this.c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                b.this.dismiss();
            }
        });
        this.d = (Button) findViewById(R.id.set_ring__ok);
        this.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int i;
                int i2;
                final int i3;
                int i4;
                int i5 = 4;
                int i6 = 0;
                if (!r.b() || (!((C0039b) b.this.e.get(0)).c && !((C0039b) b.this.e.get(1)).c)) {
                    if (((C0039b) b.this.e.get(0)).c) {
                        i = 1;
                    } else {
                        i = 0;
                    }
                    if (((C0039b) b.this.e.get(1)).c) {
                        i2 = 2;
                    } else {
                        i2 = 0;
                    }
                    int i7 = i | i2;
                    if (!((C0039b) b.this.e.get(2)).c) {
                        i5 = 0;
                    }
                    i3 = i7 | i5;
                } else {
                    int i8 = ((C0039b) b.this.e.get(0)).c ? b.this.j == 0 ? 1 : 32 : 0;
                    int i9 = ((C0039b) b.this.e.get(1)).c ? b.this.j == 0 ? 64 : 128 : 0;
                    if (((C0039b) b.this.e.get(1)).c) {
                        i4 = 2;
                    } else {
                        i4 = 0;
                    }
                    if (((C0039b) b.this.e.get(2)).c) {
                        i6 = 4;
                    }
                    i3 = i8 | i9 | i4 | i6;
                }
                h.a(new Runnable() {
                    public void run() {
                        if (i3 != 0) {
                            ac.a().a(i3, b.this.g, b.this.h, b.this.i);
                        }
                    }
                });
                b.this.dismiss();
            }
        });
        setCanceledOnTouchOutside(true);
        this.e = new ArrayList<>();
        this.e.add(new C0039b(c.ring, this.f2646a.getResources().getString(R.string.set_ring_incoming_call), true, R.drawable.icon_ring_call));
        this.e.add(new C0039b(c.sms, this.f2646a.getResources().getString(R.string.set_ring_message), false, R.drawable.icon_ring_sms));
        this.e.add(new C0039b(c.alarm, this.f2646a.getResources().getString(R.string.set_ring_alarm), false, R.drawable.icon_ring_alarm));
        this.e.add(new C0039b(c.contact, this.f2646a.getResources().getString(R.string.set_ring_contact), false, R.drawable.icon_ring_contact));
        this.f2647b.setAdapter((ListAdapter) this.f);
    }

    /* renamed from: com.shoujiduoduo.ui.settings.b$b  reason: collision with other inner class name */
    /* compiled from: SetRingDialog */
    private class C0039b {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public String f2662b;
        /* access modifiers changed from: private */
        public boolean c;
        /* access modifiers changed from: private */
        public int d;
        /* access modifiers changed from: private */
        public c e;

        C0039b(c cVar, String str, boolean z, int i) {
            this.e = cVar;
            this.f2662b = str;
            this.c = z;
            this.d = i;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        if ((i2 == 0 || i2 == 1) && r.b() && r.c()) {
            new AlertDialog.Builder(this.f2646a).setTitle("请选择sim卡").setSingleChoiceItems(new String[]{"sim卡1", "sim卡2"}, 0, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    com.shoujiduoduo.base.a.a.a("SetRingDialog", "is gionee phone, select sim card:" + i);
                    int unused = b.this.j = i;
                    dialogInterface.dismiss();
                }
            }).show();
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        new b.a(this.f2646a).b((int) R.string.hint).a((int) R.string.buy_cailing_confirm).a((int) R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean unused = ((C0039b) b.this.e.get(b.this.e.size() - 2)).c = true;
                b.this.f.notifyDataSetChanged();
                dialogInterface.dismiss();
            }
        }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                boolean unused = ((C0039b) b.this.e.get(b.this.e.size() - 2)).c = false;
                b.this.f.notifyDataSetChanged();
                dialogInterface.dismiss();
            }
        }).a().show();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        PlayerService.a(true);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        PlayerService.a(false);
    }

    /* compiled from: SetRingDialog */
    private class a extends BaseAdapter {

        /* renamed from: a  reason: collision with root package name */
        Html.ImageGetter f2656a;

        private a() {
            this.f2656a = new Html.ImageGetter() {
                public Drawable getDrawable(String str) {
                    int i;
                    try {
                        i = Integer.parseInt(str);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        i = 0;
                    }
                    Drawable drawable = b.this.f2646a.getResources().getDrawable(i);
                    drawable.setBounds(0, 0, (int) (((double) drawable.getIntrinsicWidth()) * 0.7d), (int) (((double) drawable.getIntrinsicHeight()) * 0.7d));
                    return drawable;
                }
            };
        }

        public int getCount() {
            return b.this.e.size();
        }

        public Object getItem(int i) {
            return b.this.e.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(final int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = b.this.getLayoutInflater().inflate((int) R.layout.listitem_set_ring, viewGroup, false);
            }
            TextView textView = (TextView) view.findViewById(R.id.ringtype_desc);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            ImageView imageView = (ImageView) view.findViewById(R.id.ringtype_icon);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (((C0039b) b.this.e.get(i)).e == c.cailing && z && !((C0039b) b.this.e.get(i)).c) {
                        b.this.a();
                    }
                    if ((i == 1 || i == 0) && z && !((C0039b) b.this.e.get(i)).c) {
                        b.this.a(i);
                    }
                    boolean unused = ((C0039b) b.this.e.get(i)).c = z;
                }
            });
            if (((C0039b) b.this.e.get(i)).e == c.contact) {
                checkBox.setBackgroundResource(R.drawable.btn_right_arrow);
            } else {
                checkBox.setBackgroundResource(R.drawable.checkbox_bk);
            }
            if (((C0039b) b.this.e.get(i)).c) {
                checkBox.setChecked(true);
            } else {
                checkBox.setChecked(false);
            }
            if (((C0039b) b.this.e.get(i)).e == c.cailing) {
                textView.setText(Html.fromHtml(((C0039b) b.this.e.get(i)).f2662b + " " + "<img src=\"" + ((int) R.drawable.icon_cmcc_small) + "\" align='center'/>", this.f2656a, null));
            } else {
                textView.setText(((C0039b) b.this.e.get(i)).f2662b);
            }
            imageView.setImageResource(((C0039b) b.this.e.get(i)).d);
            return view;
        }
    }
}
