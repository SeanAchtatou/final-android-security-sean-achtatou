package com.shoujiduoduo.ui.settings;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.ui.utils.g;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.k;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ChangeSkinActivity extends BaseActivity implements View.OnClickListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public GridView f2595a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public RelativeLayout f2596b;
    private d c;
    /* access modifiers changed from: private */
    public ArrayList<c> d;
    /* access modifiers changed from: private */
    public String e = "pref_cur_url";
    private String f = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_change_skin);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.btn_camera).setOnClickListener(this);
        findViewById(R.id.btn_album).setOnClickListener(this);
        this.f2595a = (GridView) findViewById(R.id.skin_gridview);
        this.f2596b = (RelativeLayout) findViewById(R.id.loading);
        this.f2595a.setVisibility(4);
        this.f2596b.setVisibility(0);
        this.c = new d();
        this.d = new ArrayList<>();
        this.f2595a.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (i == 0) {
                    ad.c(RingDDApp.c(), ChangeSkinActivity.this.e, "default");
                    ad.c(RingDDApp.c(), "cur_splash_pic", "default");
                } else {
                    ad.c(RingDDApp.c(), ChangeSkinActivity.this.e, ((c) ChangeSkinActivity.this.d.get(i)).f2603a);
                    final String str = ((c) ChangeSkinActivity.this.d.get(i)).f2603a;
                    final String str2 = k.a(7) + p.d(str);
                    if (p.f(str2)) {
                        ad.c(RingDDApp.c(), "cur_splash_pic", str2);
                        com.shoujiduoduo.util.widget.d.a("个性启动图设置成功");
                    } else {
                        h.a(new Runnable() {
                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean):boolean
                             arg types: [java.lang.String, java.lang.String, int]
                             candidates:
                              com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, org.json.JSONObject):java.lang.String
                              com.shoujiduoduo.util.s.a(java.lang.String, boolean, java.lang.String):java.lang.String
                              com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.s$a):void
                              com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, java.lang.String):void
                              com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean):boolean */
                            public void run() {
                                String str = str2 + ".tmp";
                                if (s.a(str, str2, true)) {
                                    com.shoujiduoduo.util.widget.d.a("个性启动图设置成功");
                                    p.a(str, str2);
                                    ad.c(RingDDApp.c(), "cur_splash_pic", str2);
                                    return;
                                }
                                com.shoujiduoduo.util.widget.d.a("个性启动图下载失败，请稍候重试");
                            }
                        });
                    }
                }
                ((a) ChangeSkinActivity.this.f2595a.getAdapter()).notifyDataSetChanged();
            }
        });
        h.a(new b(this.c));
    }

    private class d extends Handler {
        private d() {
        }

        public void handleMessage(Message message) {
            super.handleMessage(message);
            ArrayList unused = ChangeSkinActivity.this.d = (ArrayList) message.obj;
            ChangeSkinActivity.this.d.add(0, new c("default", "default"));
            ChangeSkinActivity.this.f2595a.setAdapter((ListAdapter) new a());
            ChangeSkinActivity.this.f2595a.setVisibility(0);
            ChangeSkinActivity.this.f2596b.setVisibility(4);
        }
    }

    private class b implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private Handler f2602b;

        public b(Handler handler) {
            this.f2602b = handler;
        }

        public void run() {
            String a2 = s.a("&type=vipskins", "");
            if (a2 != null) {
                com.shoujiduoduo.base.a.a.a("changeskin", a2);
                this.f2602b.sendMessage(this.f2602b.obtainMessage(1, ChangeSkinActivity.this.a(new ByteArrayInputStream(a2.getBytes()))));
            }
        }
    }

    private class c {

        /* renamed from: a  reason: collision with root package name */
        public String f2603a;

        /* renamed from: b  reason: collision with root package name */
        public String f2604b;

        public c() {
        }

        public c(String str, String str2) {
            this.f2603a = str;
            this.f2604b = str2;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<c> a(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            NodeList elementsByTagName = documentElement.getElementsByTagName("skin");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<c> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                c cVar = new c();
                cVar.f2603a = f.a(attributes, "img_b");
                cVar.f2604b = f.a(attributes, "img_s");
                arrayList.add(cVar);
            }
            return arrayList;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (DOMException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (ParserConfigurationException e5) {
            e5.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.c != null) {
            this.c.removeCallbacksAndMessages(null);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.bottom_banner:
            default:
                return;
            case R.id.btn_camera:
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                this.f = String.valueOf(System.currentTimeMillis()) + ".jpg";
                intent.putExtra("output", Uri.fromFile(new File(k.a(6), this.f)));
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent, 65536) == null) {
                    com.shoujiduoduo.util.widget.d.a("请先安装相机");
                    return;
                } else {
                    startActivityForResult(intent, 2);
                    return;
                }
            case R.id.btn_album:
                Intent intent2 = new Intent("android.intent.action.PICK");
                intent2.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
                if (RingDDApp.b().getApplicationContext().getPackageManager().resolveActivity(intent2, 65536) == null) {
                    com.shoujiduoduo.util.widget.d.a("请先安装相册");
                    return;
                } else {
                    startActivityForResult(intent2, 2);
                    return;
                }
        }
    }

    private class a extends BaseAdapter {
        private a() {
        }

        public int getCount() {
            if (ChangeSkinActivity.this.d == null) {
                return 0;
            }
            return ChangeSkinActivity.this.d.size();
        }

        public Object getItem(int i) {
            if (ChangeSkinActivity.this.d == null) {
                return 0;
            }
            return ChangeSkinActivity.this.d.get(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, android.view.ViewGroup, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public View getView(int i, View view, ViewGroup viewGroup) {
            boolean z = false;
            if (view == null) {
                view = LayoutInflater.from(ChangeSkinActivity.this).inflate((int) R.layout.griditem_skin, viewGroup, false);
            }
            ImageView imageView = (ImageView) view.findViewById(R.id.skin);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            String a2 = ad.a(RingDDApp.c(), ChangeSkinActivity.this.e, "default");
            if (i == 0) {
                imageView.setImageResource(R.drawable.skin_default);
            } else {
                com.d.a.b.d.a().a(((c) ChangeSkinActivity.this.d.get(i)).f2604b, imageView, g.a().i());
            }
            checkBox.setVisibility(a2.equals(((c) ChangeSkinActivity.this.d.get(i)).f2603a) ? 0 : 4);
            if (a2.equals(((c) ChangeSkinActivity.this.d.get(i)).f2603a)) {
                z = true;
            }
            checkBox.setChecked(z);
            return view;
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Bundle extras;
        Uri fromFile;
        super.onActivityResult(i, i2, intent);
        if (i2 == -1) {
            switch (i) {
                case 2:
                    if (intent != null) {
                        fromFile = intent.getData();
                    } else if (this.f != null) {
                        fromFile = Uri.fromFile(new File(k.a(6), this.f));
                    } else {
                        return;
                    }
                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    if (fromFile == null) {
                        com.shoujiduoduo.util.widget.d.a("相机未提供图片,换个相机试试");
                        return;
                    } else {
                        a(fromFile, (displayMetrics.widthPixels * 3) / 4, (displayMetrics.heightPixels * 3) / 4, 3);
                        return;
                    }
                case 3:
                    Bitmap bitmap = null;
                    Uri data = intent.getData();
                    if (data != null) {
                        bitmap = BitmapFactory.decodeFile(data.getPath());
                    }
                    if (bitmap == null && (extras = intent.getExtras()) != null) {
                        bitmap = (Bitmap) extras.get("data");
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                        try {
                            byteArrayOutputStream.close();
                        } catch (IOException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (bitmap != null) {
                        a(bitmap);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private void a(Bitmap bitmap) {
        String str = k.a(7) + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".jpg";
        f.a(bitmap, Bitmap.CompressFormat.JPEG, 100, str);
        ad.c(this, "cur_splash_pic", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public void a(Uri uri, int i, int i2, int i3) {
        Intent intent = new Intent(this, CropImageActivity.class);
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", i);
        intent.putExtra("aspectY", i2);
        intent.putExtra("outputX", i);
        intent.putExtra("outputY", i2);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        intent.putExtra("return-data", false);
        intent.putExtra("save_path", k.a(6) + "skin_" + String.valueOf(System.currentTimeMillis()) + ".png");
        startActivityForResult(intent, i3);
    }
}
