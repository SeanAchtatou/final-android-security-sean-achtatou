package com.shoujiduoduo.ui.settings;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.d.a.b.d;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.INativeErrorCode;
import com.duoduo.mobads.INativeResponse;
import com.duoduo.mobads.gdt.IGdtNativeAd;
import com.duoduo.mobads.gdt.IGdtNativeAdDataRef;
import com.duoduo.mobads.gdt.IGdtNativeAdListener;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.g;
import com.shoujiduoduo.util.ab;
import java.util.ArrayList;
import java.util.List;

/* compiled from: QuitDialog */
public class a extends Dialog {

    /* renamed from: a  reason: collision with root package name */
    private Button f2636a;

    /* renamed from: b  reason: collision with root package name */
    private Button f2637b;
    private Context c;
    private TextView d;
    private TextView e;
    /* access modifiers changed from: private */
    public String f;
    private boolean g;
    private ImageView h;
    private RelativeLayout i;
    private ImageView j;
    /* access modifiers changed from: private */
    public List<C0038a> k = new ArrayList();
    private int l;
    /* access modifiers changed from: private */
    public long m;

    public a(Context context, int i2) {
        super(context, i2);
        this.c = context;
        this.f = ab.a().a("quit_ad_type");
        if ("baidu".equals(this.f)) {
            this.f = "baidu";
        } else if ("gdt".equals(this.f)) {
            this.f = "gdt";
        } else {
            this.f = "baidu";
        }
        com.shoujiduoduo.base.a.a.a("QuitDialog", "quit ad type:" + this.f);
        this.g = ab.a().b("quit_ad_enable");
        com.shoujiduoduo.base.a.a.a("QuitDialog", "quit ad switch:" + this.g);
        if (!this.g || !com.shoujiduoduo.util.a.e()) {
            this.g = false;
        } else {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onCreate");
        setContentView((int) R.layout.dialog_quit_app);
        this.f2636a = (Button) findViewById(R.id.ok);
        this.f2637b = (Button) findViewById(R.id.cancel);
        this.d = (TextView) findViewById(R.id.tips);
        this.i = (RelativeLayout) findViewById(R.id.ad_view);
        this.j = (ImageView) findViewById(R.id.ad_icon);
        this.h = (ImageView) findViewById(R.id.ad_image);
        this.e = (TextView) findViewById(R.id.ad_hint);
        this.f2637b.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                com.shoujiduoduo.base.a.a.a("QuitDialog", "quit dialog click cancel");
                a.this.dismiss();
            }
        });
        this.f2636a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingDDApp.g();
                a.this.dismiss();
            }
        });
    }

    private void a() {
        if (this.f.equals("baidu")) {
            b();
        } else if (this.f.equals("gdt")) {
            c();
        } else {
            com.shoujiduoduo.base.a.a.e("QuitDialog", "not support");
        }
    }

    private void b() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobBaiduFeed");
        IBaiduNative nativeAdIns = DuoMobAdUtils.Ins.BaiduIns.getNativeAdIns(this.c, "d7d3402a", "2652479", new IBaiduNativeNetworkListener() {
            public void onNativeFail(INativeErrorCode iNativeErrorCode) {
                com.shoujiduoduo.base.a.a.e("QuitDialog", "fetchDuomobBaiduFeed feed, onNativeFail reason:" + iNativeErrorCode.getErrorCode());
            }

            public void onNativeLoad(List<INativeResponse> list) {
                if (list.size() > 0) {
                    com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobBaiduFeed feed, onNativeLoad, ad size:" + list.size());
                    long unused = a.this.m = System.currentTimeMillis();
                    for (INativeResponse aVar : list) {
                        a.this.k.add(new C0038a(aVar));
                    }
                    return;
                }
                com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobBaiduFeed feed, onNativeLoad, ad size is 0");
            }
        });
        if (nativeAdIns != null) {
            nativeAdIns.makeRequest();
        }
    }

    private void c() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobGdtFeed");
        IGdtNativeAd nativeAd = DuoMobAdUtils.Ins.GdtIns.getNativeAd((Activity) this.c, "1105772024", "3090615613140947", new IGdtNativeAdListener() {
            public void onADLoaded(List<IGdtNativeAdDataRef> list) {
                if (list.size() > 0) {
                    com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobGdtFeed feed, onNativeLoad, ad size:" + list.size());
                    long unused = a.this.m = System.currentTimeMillis();
                    for (IGdtNativeAdDataRef aVar : list) {
                        a.this.k.add(new C0038a(aVar));
                    }
                    return;
                }
                com.shoujiduoduo.base.a.a.a("QuitDialog", "fetchDuomobGdtFeed feed, onNativeLoad, ad size is 0");
            }

            public void onADError(IGdtNativeAdDataRef iGdtNativeAdDataRef, int i) {
            }

            public void onNoAD(int i) {
            }

            public void onADStatusChanged(IGdtNativeAdDataRef iGdtNativeAdDataRef) {
            }
        });
        if (nativeAd != null) {
            nativeAd.loadAD(10);
        }
    }

    /* renamed from: com.shoujiduoduo.ui.settings.a$a  reason: collision with other inner class name */
    /* compiled from: QuitDialog */
    private class C0038a {

        /* renamed from: b  reason: collision with root package name */
        private Object f2645b;

        public C0038a(Object obj) {
            this.f2645b = obj;
        }

        public String a() {
            if (a.this.f.equals("baidu")) {
                return ((INativeResponse) this.f2645b).getTitle();
            }
            if (a.this.f.equals("gdt")) {
                return ((IGdtNativeAdDataRef) this.f2645b).getTitle();
            }
            return "";
        }

        public String b() {
            if (a.this.f.equals("baidu")) {
                return ((INativeResponse) this.f2645b).getImageUrl();
            }
            if (a.this.f.equals("gdt")) {
                return ((IGdtNativeAdDataRef) this.f2645b).getImgUrl();
            }
            return "";
        }

        public void a(View view) {
            if (a.this.f.equals("baidu")) {
                ((INativeResponse) this.f2645b).recordImpression(view);
            } else if (a.this.f.equals("gdt")) {
                ((IGdtNativeAdDataRef) this.f2645b).onExposured(view);
            }
        }

        public void b(View view) {
            if (a.this.f.equals("baidu")) {
                ((INativeResponse) this.f2645b).handleClick(view);
            } else if (a.this.f.equals("gdt")) {
                ((IGdtNativeAdDataRef) this.f2645b).onClicked(view);
            }
        }
    }

    private void d() {
        if (!this.g || !com.shoujiduoduo.util.a.e()) {
            com.shoujiduoduo.base.a.a.a("QuitDialog", "can not showQuitAD");
            return;
        }
        com.shoujiduoduo.base.a.a.a("QuitDialog", "showQuitAD");
        if (System.currentTimeMillis() - this.m > 1800000) {
            com.shoujiduoduo.base.a.a.a("QuitDialog", "ad is out of time, fetch new ad data");
            a();
        } else if (this.k != null && this.k.size() > 0) {
            this.l++;
            this.l %= this.k.size();
            if (this.l < this.k.size()) {
                final C0038a aVar = this.k.get(this.l);
                com.shoujiduoduo.base.a.a.a("QuitDialog", "current ad index:" + this.l);
                com.shoujiduoduo.base.a.a.a("QuitDialog", "ad title:" + aVar.a());
                com.shoujiduoduo.base.a.a.a("QuitDialog", "ad image:" + aVar.b());
                this.i.setVisibility(0);
                if (this.f.equals("gdt")) {
                    this.j.setVisibility(0);
                } else {
                    this.j.setVisibility(4);
                }
                d.a().a(aVar.b(), this.h, g.a().b());
                this.d.setText(aVar.a());
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                aVar.a(this.h);
                this.h.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        aVar.b(view);
                    }
                });
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onStart");
        d();
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a("QuitDialog", "onStop");
        if (this.g && com.shoujiduoduo.util.a.e()) {
            a();
        }
        super.onStop();
    }
}
