package com.shoujiduoduo.ui.user;

import android.app.AlertDialog;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.e;
import com.shoujiduoduo.b.c.g;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.k;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.mine.FollowAndFansActivity;
import com.shoujiduoduo.ui.mine.UserMainPageActivity;
import com.shoujiduoduo.ui.utils.DDListFragment;
import com.shoujiduoduo.ui.utils.SlidingActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class CommentActivity extends SlidingActivity implements View.OnClickListener {
    private TextWatcher A = new TextWatcher() {
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
         arg types: [com.shoujiduoduo.ui.user.CommentActivity, int]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.c):com.shoujiduoduo.base.bean.c
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean */
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence == null || charSequence.length() <= 0) {
                CommentActivity.this.j.setTextColor(CommentActivity.this.getResources().getColor(R.color.text_gray));
                boolean unused = CommentActivity.this.l = false;
                return;
            }
            CommentActivity.this.j.setTextColor(CommentActivity.this.getResources().getColor(R.color.text_green));
            boolean unused2 = CommentActivity.this.l = true;
        }

        public void afterTextChanged(Editable editable) {
        }
    };
    private View.OnFocusChangeListener B = new View.OnFocusChangeListener() {
        public void onFocusChange(View view, boolean z) {
            com.shoujiduoduo.base.a.a.a("CommentActivity", "EditText has focus:" + z);
            if (!z) {
                CommentActivity.this.k.setHint("说点什么...");
            }
        }
    };
    /* access modifiers changed from: private */
    public b C;
    private e D = new e() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [?, ?[OBJECT, ARRAY], int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        public void a(final c cVar) {
            c unused = CommentActivity.this.x = cVar;
            final k c = com.shoujiduoduo.a.b.b.g().c();
            View inflate = LayoutInflater.from(CommentActivity.this).inflate((int) R.layout.comment_click_choice, (ViewGroup) null, false);
            inflate.findViewById(R.id.answer_comment).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.C != null) {
                        CommentActivity.this.C.dismiss();
                    }
                    CommentActivity.this.y.sendEmptyMessageDelayed(1, 20);
                }
            });
            inflate.findViewById(R.id.copy_comment).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.C != null) {
                        CommentActivity.this.C.dismiss();
                    }
                    if (Build.VERSION.SDK_INT >= 11) {
                        ((ClipboardManager) CommentActivity.this.getSystemService("clipboard")).setText(cVar.h);
                    } else {
                        ((android.text.ClipboardManager) CommentActivity.this.getSystemService("clipboard")).setText(cVar.h);
                    }
                    d.a("已复制到剪贴板");
                }
            });
            inflate.findViewById(R.id.complain).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (CommentActivity.this.C != null) {
                        CommentActivity.this.C.dismiss();
                    }
                    if (!c.i()) {
                        CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                    } else if (CommentActivity.this.a(cVar.g)) {
                        d.a("已经举报过啦");
                    } else {
                        s.a("complain", "&cid=" + cVar.g + "&uid=" + c.a(), new s.a() {
                            public void a(String str) {
                                com.shoujiduoduo.base.a.a.a("CommentActivity", "complaint success, res:" + str);
                                d.a("举报成功");
                                CommentActivity.this.b(cVar.g);
                            }

                            public void a(String str, String str2) {
                                com.shoujiduoduo.base.a.a.a("CommentActivity", "complaint errror");
                                d.a("举报失败");
                            }
                        });
                    }
                }
            });
            View findViewById = inflate.findViewById(R.id.del_comment);
            if (c.a().equals(cVar.c) || CommentActivity.this.s || c.j()) {
                findViewById.setVisibility(0);
                findViewById.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.C != null) {
                            CommentActivity.this.C.dismiss();
                        }
                        if (!c.i()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            s.a("delcomment", "&rid=" + CommentActivity.this.t.g + "&uid=" + c.a() + "&cid=" + cVar.g + (c.j() ? "&superuser=1" : ""), new s.a() {
                                public void a(String str) {
                                    com.shoujiduoduo.base.a.a.a("CommentActivity", "del comment:" + str);
                                    d.a("删除成功");
                                }

                                public void a(String str, String str2) {
                                    com.shoujiduoduo.base.a.a.a("CommentActivity", "del comment error");
                                    d.a("删除失败");
                                }
                            });
                        }
                    }
                });
            } else {
                findViewById.setVisibility(8);
            }
            if (CommentActivity.this.u == null || !c.j()) {
                inflate.findViewById(R.id.blacklist).setVisibility(8);
                inflate.findViewById(R.id.del_all_comment).setVisibility(8);
            } else {
                inflate.findViewById(R.id.blacklist).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.C != null) {
                            CommentActivity.this.C.dismiss();
                        }
                        if (!c.i()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            new AlertDialog.Builder(CommentActivity.this).setMessage("确定屏蔽该用户发言？").setNegativeButton("再想想", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    s.a("blacklist", "&rid=" + CommentActivity.this.t.g + "&uid=" + c.a() + "&tuid=" + cVar.c, new s.a() {
                                        public void a(String str) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "blacklist user:" + str);
                                            d.a("操作成功");
                                        }

                                        public void a(String str, String str2) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "blacklist user error");
                                            d.a("操作失败");
                                        }
                                    });
                                }
                            }).show();
                        }
                    }
                });
                inflate.findViewById(R.id.del_all_comment).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (CommentActivity.this.C != null) {
                            CommentActivity.this.C.dismiss();
                        }
                        if (!c.i()) {
                            CommentActivity.this.startActivity(new Intent(RingDDApp.c(), UserLoginActivity.class));
                        } else {
                            new AlertDialog.Builder(CommentActivity.this).setMessage("确定删除该用户所有评论？").setNegativeButton("再想想", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    s.a("admindelcomment", "&rid=" + CommentActivity.this.t.g + "&uid=" + c.a() + "&tuid=" + cVar.c, new s.a() {
                                        public void a(String str) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "del user all comment:" + str);
                                            d.a("删除成功");
                                        }

                                        public void a(String str, String str2) {
                                            com.shoujiduoduo.base.a.a.a("CommentActivity", "del user all comment error");
                                            d.a("删除失败");
                                        }
                                    });
                                }
                            }).show();
                        }
                    }
                });
            }
            b unused2 = CommentActivity.this.C = new b.a(CommentActivity.this).a(inflate).a();
            CommentActivity.this.C.show();
        }

        public void a() {
        }
    };
    private String E;

    /* renamed from: a  reason: collision with root package name */
    private String f2665a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f2666b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private ImageView g;
    private ImageView h;
    /* access modifiers changed from: private */
    public Button i;
    /* access modifiers changed from: private */
    public Button j;
    /* access modifiers changed from: private */
    public EditText k;
    /* access modifiers changed from: private */
    public boolean l;
    private final String m = "关注TA";
    private final String n = "取消关注";
    private final String o = "已关注";
    private int p;
    private int q;
    /* access modifiers changed from: private */
    public TextView r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public RingData t;
    /* access modifiers changed from: private */
    public UserData u;
    private g v;
    /* access modifiers changed from: private */
    public com.shoujiduoduo.b.c.e w;
    /* access modifiers changed from: private */
    public c x;
    /* access modifiers changed from: private */
    public Handler y = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
         arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.c):com.shoujiduoduo.base.bean.c
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                if (CommentActivity.this.x != null) {
                    CommentActivity.this.k.setHint("回复给:" + CommentActivity.this.x.j);
                }
                CommentActivity.this.k.requestFocus();
                CommentActivity.this.k.performClick();
                CommentActivity.this.a((View) CommentActivity.this.k, (Context) CommentActivity.this);
            }
        }
    };
    private com.shoujiduoduo.a.c.g z = new com.shoujiduoduo.a.c.g() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
         arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
         candidates:
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.c):com.shoujiduoduo.base.bean.c
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
          com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
          com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
          com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void */
        public void a(com.shoujiduoduo.base.bean.d dVar, int i) {
            if (dVar.a().equals(CommentActivity.this.w.a())) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "onDataUpdate in, id:" + CommentActivity.this.w.a());
                switch (i) {
                    case 0:
                        if (dVar.c() == 0) {
                            CommentActivity.this.r.setVisibility(0);
                            CommentActivity.this.a((View) CommentActivity.this.k, (Context) CommentActivity.this);
                            return;
                        }
                        CommentActivity.this.r.setVisibility(8);
                        return;
                    default:
                        return;
                }
            }
        }
    };

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.D);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.z);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_comment);
        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.tv_fans_hint).setOnClickListener(this);
        findViewById(R.id.tv_follow_hint).setOnClickListener(this);
        findViewById(R.id.userinfo_layout).setOnClickListener(this);
        this.f2666b = (TextView) findViewById(R.id.user_fans);
        this.f2666b.setOnClickListener(this);
        this.c = (TextView) findViewById(R.id.user_follow);
        this.c.setOnClickListener(this);
        this.d = (TextView) findViewById(R.id.user_name);
        this.e = (TextView) findViewById(R.id.user_id);
        this.h = (ImageView) findViewById(R.id.user_head);
        this.h.setOnClickListener(this);
        this.i = (Button) findViewById(R.id.btn_follow);
        this.i.setOnClickListener(this);
        this.g = (ImageView) findViewById(R.id.iv_sex);
        this.f = (TextView) findViewById(R.id.tv_songname);
        this.k = (EditText) findViewById(R.id.et_write_comment);
        this.k.setOnFocusChangeListener(this.B);
        this.k.addTextChangedListener(this.A);
        this.j = (Button) findViewById(R.id.btn_commit);
        this.j.setOnClickListener(this);
        this.r = (TextView) findViewById(R.id.tv_nodata_hint);
        Intent intent = getIntent();
        if (intent != null) {
            this.f2665a = intent.getStringExtra("tuid");
            this.t = (RingData) intent.getParcelableExtra("current_ring");
            String f2 = com.shoujiduoduo.a.b.b.g().f();
            if (!ag.c(f2) && f2.equals(this.f2665a)) {
                this.s = true;
            }
            a(this.f2665a, this.s);
            this.f.setText(this.t.e);
        }
        this.v = (g) RingDDApp.b().a("current_ringlist");
        DDListFragment dDListFragment = new DDListFragment();
        Bundle bundle2 = new Bundle();
        bundle2.putString("adapter_type", "comment_list_adapter");
        dDListFragment.setArguments(bundle2);
        this.w = new com.shoujiduoduo.b.c.e(this.t.g);
        dDListFragment.a(this.w);
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        beginTransaction.add((int) R.id.comment_layout, dDListFragment);
        beginTransaction.commit();
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, this.D);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, this.z);
    }

    public void a() {
    }

    public void b() {
        finish();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!(motionEvent.getAction() != 0 || getCurrentFocus() == null || getCurrentFocus().getWindowToken() == null)) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 2);
        }
        return super.onTouchEvent(motionEvent);
    }

    public void a(View view, Context context) {
        if (view.requestFocus()) {
            ((InputMethodManager) context.getSystemService("input_method")).showSoftInput(view, 1);
        }
    }

    public static void a(EditText editText, Context context) {
        ((InputMethodManager) context.getSystemService("input_method")).hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    /* access modifiers changed from: private */
    public boolean a(String str) {
        return ad.a(RingDDApp.c(), "complain_comment_list", "").contains(str);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        String a2 = ad.a(RingDDApp.c(), "complain_comment_list", "");
        if (!a2.equals("")) {
            str = a2 + "|" + str;
        }
        ad.c(RingDDApp.c(), "complain_comment_list", str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void
     arg types: [android.widget.EditText, com.shoujiduoduo.ui.user.CommentActivity]
     candidates:
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.UserData):com.shoujiduoduo.base.bean.UserData
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.base.bean.c):com.shoujiduoduo.base.bean.c
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, com.shoujiduoduo.util.widget.b):com.shoujiduoduo.util.widget.b
      com.shoujiduoduo.ui.user.CommentActivity.a(java.lang.String, boolean):void
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, java.lang.String):boolean
      com.shoujiduoduo.ui.user.CommentActivity.a(com.shoujiduoduo.ui.user.CommentActivity, boolean):boolean
      com.shoujiduoduo.ui.user.CommentActivity.a(android.view.View, android.content.Context):void
      com.shoujiduoduo.ui.user.CommentActivity.a(android.widget.EditText, android.content.Context):void */
    private void a(String str, String str2, String str3, String str4) {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (!c2.i()) {
            startActivity(new Intent(this, UserLoginActivity.class));
            return;
        }
        this.E = str;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("rid", this.t.g);
            jSONObject.put("ruid", this.t.j);
            jSONObject.put("uid", c2.a());
            jSONObject.put("tuid", str4);
            jSONObject.put("ddid", this.u.g);
            jSONObject.put("tcid", str3);
            jSONObject.put("comment", str);
            jSONObject.put("tcomment", str2);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        com.shoujiduoduo.base.a.a.a("CommentActivity", "post comment json:" + jSONObject.toString());
        a(this.k, (Context) this);
        this.k.clearFocus();
        this.k.setText("");
        s.a("comment", "&rid=" + this.t.g + "&uid=" + c2.a(), jSONObject, new s.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "commitcomment success, res:" + str);
                a c = CommentActivity.this.c(str);
                if (c.a()) {
                    d.a("发表成功");
                    com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_COMMENT, new c.a<e>() {
                        public void a() {
                            ((e) this.f1812a).a();
                        }
                    });
                    return;
                }
                d.a("" + c.f2696b);
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "commitcomment error");
                d.a("发表失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public a c(String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            if (jSONObject != null) {
                a aVar = new a();
                aVar.f2696b = jSONObject.optString("msg");
                aVar.f2695a = jSONObject.optString(SocketMessage.MSG_RESULE_KEY);
                return aVar;
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return new a("failed", "发表失败!");
    }

    private class a {

        /* renamed from: a  reason: collision with root package name */
        String f2695a;

        /* renamed from: b  reason: collision with root package name */
        String f2696b;

        public a() {
            this.f2695a = "";
            this.f2696b = "";
        }

        public a(String str, String str2) {
            this.f2696b = str2;
            this.f2695a = str;
        }

        public boolean a() {
            return "success".equals(this.f2695a);
        }
    }

    private void a(String str, boolean z2) {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.a()).append("&tuid=").append(str);
        if (z2) {
            sb.append("&username=").append(s.i(c2.b()));
            sb.append("&headurl=").append(s.i(c2.c()));
        }
        s.a("getuserinfo", sb.toString(), new s.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "userinfo:" + str);
                UserData b2 = i.b(str);
                if (b2 != null) {
                    UserData unused = CommentActivity.this.u = b2;
                    CommentActivity.this.a(b2);
                    return;
                }
                com.shoujiduoduo.base.a.a.a("CommentActivity", "user 解析失败");
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("CommentActivity", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(UserData userData) {
        this.q = userData.k;
        this.p = userData.j;
        com.shoujiduoduo.base.a.a.a("CommentActivity", "fansNum:" + this.p + ", followNum:" + this.q);
        this.d.setText(userData.f1955a);
        if (userData.j >= 0) {
            this.f2666b.setText("" + userData.j);
        }
        if (userData.k >= 0) {
            this.c.setText("" + userData.k);
        }
        if (!ag.c(userData.f1956b)) {
            com.d.a.b.d.a().a(userData.f1956b, this.h, com.shoujiduoduo.ui.utils.g.a().d());
        }
        if (!ag.c(userData.g)) {
            this.e.setText("多多ID: " + userData.g);
        } else {
            this.e.setVisibility(4);
        }
        if (!ag.c(userData.d)) {
            String str = userData.d;
            char c2 = 65535;
            switch (str.hashCode()) {
                case 22899:
                    if (str.equals("女")) {
                        c2 = 1;
                        break;
                    }
                    break;
                case 30007:
                    if (str.equals("男")) {
                        c2 = 0;
                        break;
                    }
                    break;
                case 657289:
                    if (str.equals("保密")) {
                        c2 = 2;
                        break;
                    }
                    break;
            }
            switch (c2) {
                case 0:
                    this.g.setImageResource(R.drawable.icon_boy);
                    break;
                case 1:
                    this.g.setImageResource(R.drawable.icon_girl);
                    break;
                case 2:
                    this.g.setImageResource(R.drawable.icon_sex_secket);
                    break;
                default:
                    this.g.setVisibility(8);
                    break;
            }
        }
        if (this.s) {
            return;
        }
        if (userData.h) {
            this.i.setText("已关注");
        } else {
            this.i.setText("关注TA");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                return;
            case R.id.user_head:
            case R.id.userinfo_layout:
                Intent intent = new Intent(RingDDApp.c(), UserMainPageActivity.class);
                intent.putExtra("tuid", this.f2665a);
                startActivity(intent);
                return;
            case R.id.user_fans:
            case R.id.tv_fans_hint:
                Intent intent2 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent2.putExtra("type", "fans");
                intent2.putExtra("tuid", this.f2665a);
                intent2.putExtra("fansNum", this.p);
                intent2.putExtra("followNum", this.q);
                startActivity(intent2);
                return;
            case R.id.user_follow:
            case R.id.tv_follow_hint:
                Intent intent3 = new Intent(RingDDApp.c(), FollowAndFansActivity.class);
                intent3.putExtra("type", "follow");
                intent3.putExtra("tuid", this.f2665a);
                intent3.putExtra("fansNum", this.p);
                intent3.putExtra("followNum", this.q);
                startActivity(intent3);
                return;
            case R.id.btn_follow:
                if (!this.s) {
                    c();
                    return;
                }
                return;
            case R.id.btn_commit:
                if (this.l) {
                    String obj = this.k.getText().toString();
                    if (ag.c(obj)) {
                        d.a("请留下您的评论再发表吧！");
                        return;
                    } else if (obj.equals(this.E)) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "相同的评论不能多次提交");
                        return;
                    } else if (this.k.getHint().equals("说点什么...")) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "发表新评论, msg:" + obj);
                        a(obj, "", "", "");
                        return;
                    } else if (this.x != null) {
                        com.shoujiduoduo.base.a.a.a("CommentActivity", "回复评论, 回复：" + this.x.j + ", msg:" + obj);
                        a(obj, this.x.h, this.x.g, this.x.c);
                        return;
                    } else {
                        return;
                    }
                } else {
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        if (c2.i()) {
            String charSequence = this.i.getText().toString();
            k c3 = com.shoujiduoduo.a.b.b.g().c();
            String str = "&uid=" + c2.a() + "&tuid=" + this.f2665a + "&username=" + q.a(c3.b()) + "&headurl=" + q.a(c3.c());
            if ("关注TA".equals(charSequence)) {
                s.a("follow", str, new s.a() {
                    public void a(String str) {
                        CommentActivity.this.i.setText("取消关注");
                        d.a("关注成功");
                    }

                    public void a(String str, String str2) {
                        d.a("关注失败");
                    }
                });
            } else {
                s.a("unfollow", str, new s.a() {
                    public void a(String str) {
                        CommentActivity.this.i.setText("关注TA");
                        d.a("取消关注成功");
                    }

                    public void a(String str, String str2) {
                        d.a("取消失败");
                    }
                });
            }
        } else {
            startActivity(new Intent(this, UserLoginActivity.class));
        }
    }
}
