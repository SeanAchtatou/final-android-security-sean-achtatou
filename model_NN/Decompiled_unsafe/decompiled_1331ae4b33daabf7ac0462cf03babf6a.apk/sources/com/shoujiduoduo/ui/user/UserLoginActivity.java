package com.shoujiduoduo.ui.user;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.k;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ae;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import com.shoujiduoduo.util.widget.ClearEditText;
import com.shoujiduoduo.util.widget.d;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class UserLoginActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ClearEditText f2762a;

    /* renamed from: b  reason: collision with root package name */
    private String f2763b;
    private boolean c;
    private ContentObserver d;
    private RelativeLayout e;
    private RelativeLayout f;
    private RelativeLayout g;
    private EditText h;
    /* access modifiers changed from: private */
    public Button i;
    private boolean j;
    private a k;
    /* access modifiers changed from: private */
    public String l;
    private TextView m;
    private Handler n;
    private f.b o;
    /* access modifiers changed from: private */
    public String p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public UMShareAPI r = null;
    private UMAuthListener s = new UMAuthListener() {
        public void onComplete(com.umeng.socialize.c.a aVar, int i, Map<String, String> map) {
            if (map != null) {
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[AuthListener]:Auth onComplete:" + map.toString());
                if (aVar.equals(com.umeng.socialize.c.a.WEIXIN) || aVar.equals(com.umeng.socialize.c.a.QQ)) {
                    String unused = UserLoginActivity.this.p = aVar.toString().toLowerCase() + "_" + map.get("openid");
                } else {
                    String unused2 = UserLoginActivity.this.p = aVar.toString().toLowerCase() + "_" + map.get("uid");
                }
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "uid:" + UserLoginActivity.this.p);
                if (!TextUtils.isEmpty(UserLoginActivity.this.p)) {
                    ad.c(RingDDApp.c(), "user_uid", UserLoginActivity.this.p);
                    UserLoginActivity.this.r.getPlatformInfo(UserLoginActivity.this, aVar, UserLoginActivity.this.t);
                    return;
                }
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "doAuth, has not Authenticated, did not get uid or openid");
                final int a2 = UserLoginActivity.this.a(aVar);
                c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1812a).a(a2, false, "授权失败", "");
                    }
                });
            }
        }

        public void onError(com.umeng.socialize.c.a aVar, int i, Throwable th) {
            Toast.makeText(UserLoginActivity.this.getApplicationContext(), "get fail", 0).show();
            final String message = th != null ? th.getMessage() : "";
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[AuthListener]:doAuth error, msg:" + message);
            final int a2 = UserLoginActivity.this.a(aVar);
            c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1812a).a(a2, false, message, "");
                }
            });
        }

        public void onCancel(com.umeng.socialize.c.a aVar, int i) {
            Toast.makeText(UserLoginActivity.this.getApplicationContext(), "get cancel", 0).show();
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[AuthListener]:Auth, onCancel");
            final int a2 = UserLoginActivity.this.a(aVar);
            c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1812a).a(a2, false, "授权取消", "0");
                }
            });
        }
    };
    /* access modifiers changed from: private */
    public UMAuthListener t = new UMAuthListener() {
        public void onComplete(com.umeng.socialize.c.a aVar, int i, Map<String, String> map) {
            final String str;
            if (map != null) {
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[GetInfoListener]:onComplete" + map.toString());
                if (aVar.equals(com.umeng.socialize.c.a.WEIXIN)) {
                    String unused = UserLoginActivity.this.q = map.get("nickname") != null ? map.get("nickname").toString() : "";
                    str = map.get("headimgurl") != null ? map.get("headimgurl").toString() : "";
                } else if (aVar.equals(com.umeng.socialize.c.a.SINA)) {
                    try {
                        JSONObject jSONObject = (JSONObject) new JSONTokener(map.get(SocketMessage.MSG_RESULE_KEY)).nextValue();
                        if (jSONObject != null) {
                            String unused2 = UserLoginActivity.this.q = jSONObject.optString("screen_name");
                            str = jSONObject.optString("profile_image_url");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    str = "";
                } else {
                    String unused3 = UserLoginActivity.this.q = map.get("screen_name") != null ? map.get("screen_name").toString() : "";
                    str = map.get("profile_image_url") != null ? map.get("profile_image_url").toString() : "";
                }
                final int a2 = UserLoginActivity.this.a(aVar);
                c.a().a(new c.b() {
                    public void a() {
                        k kVar = new k();
                        kVar.a(UserLoginActivity.this.p);
                        kVar.b(UserLoginActivity.this.q);
                        kVar.c(str);
                        kVar.c(1);
                        kVar.a(a2);
                        com.shoujiduoduo.a.b.b.g().a(kVar);
                        UserLoginActivity.this.b();
                    }
                });
                c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
                    public void a() {
                        ((v) this.f1812a).a(a2, true, "", "");
                    }
                });
            }
            UserLoginActivity.this.finish();
        }

        public void onError(com.umeng.socialize.c.a aVar, int i, Throwable th) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[GetInfoListener]:onError:" + th.getMessage());
            final int a2 = UserLoginActivity.this.a(aVar);
            final String str = "" + i;
            c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
                public void a() {
                    ((v) this.f1812a).a(a2, false, "登录错误", str);
                }
            });
        }

        public void onCancel(com.umeng.socialize.c.a aVar, int i) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "[GetInfoListener]:onCancel");
        }
    };
    private v u = new v() {
        public void a(int i, boolean z, String str, String str2) {
            UserLoginActivity.this.a();
            if (z) {
                d.a("登录成功", 0);
                UserLoginActivity.this.finish();
                return;
            }
            d.a("登录失败 " + str, 0);
        }

        public void a(int i) {
        }

        public void b(int i) {
        }
    };
    /* access modifiers changed from: private */
    public ProgressDialog v = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_user_login);
        com.jaeger.library.a.a(this, getResources().getColor(R.color.bkg_green), 0);
        this.r = UMShareAPI.get(this);
        this.n = new Handler();
        this.f2762a = (ClearEditText) findViewById(R.id.et_phone_no);
        this.e = (RelativeLayout) findViewById(R.id.get_auth_code_layout);
        this.f = (RelativeLayout) findViewById(R.id.code_login_layout);
        this.e.setVisibility(0);
        this.f.setVisibility(4);
        this.i = (Button) findViewById(R.id.reget_sms_code);
        this.m = (TextView) findViewById(R.id.phone_num);
        this.k = new a(60000, 1000);
        this.h = (EditText) findViewById(R.id.et_auth_code);
        this.g = (RelativeLayout) findViewById(R.id.phone_auth_layout);
        findViewById(R.id.sina_weibo_login).setOnClickListener(this);
        findViewById(R.id.qq_login).setOnClickListener(this);
        findViewById(R.id.weixin_login).setOnClickListener(this);
        findViewById(R.id.user_center_back).setOnClickListener(this);
        findViewById(R.id.get_sms_code).setOnClickListener(this);
        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.reget_sms_code).setOnClickListener(this);
        c.a().a(b.OBSERVER_USER_CENTER, this.u);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.k.cancel();
        a();
        if (this.d != null && this.c) {
            getContentResolver().unregisterContentObserver(this.d);
        }
        c.a().b(b.OBSERVER_USER_CENTER, this.u);
    }

    private void b(String str) {
        if (this.d != null && this.c) {
            getContentResolver().unregisterContentObserver(this.d);
        }
        this.d = new ae(this, new Handler(), this.h, str);
        getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, this.d);
        this.c = true;
    }

    /* access modifiers changed from: private */
    public void b() {
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        StringBuilder sb = new StringBuilder();
        sb.append("&uid=").append(c2.a()).append("&tuid=").append(c2.a());
        sb.append("&username=").append(s.i(c2.b()));
        sb.append("&headurl=").append(s.i(c2.c()));
        s.a("getuserinfo", sb.toString(), new s.a() {
            public void a(String str) {
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "userinfo:" + str);
                UserData b2 = i.b(str);
                if (b2 != null) {
                    k c = com.shoujiduoduo.a.b.b.g().c();
                    if (!ag.c(b2.f1955a)) {
                        c.b(b2.f1955a);
                    }
                    if (!ag.c(b2.f1956b)) {
                        c.c(b2.f1956b);
                    }
                    c.d(b2.l);
                    com.shoujiduoduo.a.b.b.g().a(c);
                    return;
                }
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "user 解析失败");
            }

            public void a(String str, String str2) {
                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "user 信息获取失败");
            }
        });
    }

    /* access modifiers changed from: private */
    public int a(com.umeng.socialize.c.a aVar) {
        if (aVar.equals(com.umeng.socialize.c.a.QQ)) {
            return 2;
        }
        if (aVar.equals(com.umeng.socialize.c.a.SINA)) {
            return 3;
        }
        if (aVar.equals(com.umeng.socialize.c.a.WEIXIN)) {
            return 5;
        }
        if (aVar.equals(com.umeng.socialize.c.a.RENREN)) {
            return 4;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        this.r.onActivityResult(i2, i3, intent);
    }

    private class a extends CountDownTimer {
        public a(long j, long j2) {
            super(j, j2);
        }

        public void onTick(long j) {
            UserLoginActivity.this.i.setClickable(false);
            UserLoginActivity.this.i.setText((j / 1000) + "秒");
        }

        public void onFinish() {
            UserLoginActivity.this.i.setClickable(true);
            UserLoginActivity.this.i.setText("重新获取");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final String str) {
        this.n.post(new Runnable() {
            public void run() {
                if (UserLoginActivity.this.v == null) {
                    ProgressDialog unused = UserLoginActivity.this.v = new ProgressDialog(UserLoginActivity.this);
                    UserLoginActivity.this.v.setMessage(str);
                    UserLoginActivity.this.v.setIndeterminate(false);
                    UserLoginActivity.this.v.setCancelable(true);
                    UserLoginActivity.this.v.setCanceledOnTouchOutside(false);
                    if (!UserLoginActivity.this.isFinishing()) {
                        UserLoginActivity.this.v.show();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.n.post(new Runnable() {
            public void run() {
                if (UserLoginActivity.this.v != null) {
                    UserLoginActivity.this.v.dismiss();
                    ProgressDialog unused = UserLoginActivity.this.v = (ProgressDialog) null;
                }
            }
        });
    }

    public void onClick(View view) {
        HashMap hashMap = new HashMap();
        switch (view.getId()) {
            case R.id.get_sms_code:
                this.l = this.f2762a.getText().toString();
                if (!f.f(this.l)) {
                    d.a("请输入正确的手机号", 0);
                    return;
                }
                this.o = f.g(this.l);
                if (this.o == f.b.d) {
                    d.a("未知的手机号类型，无法判断运营商，请确认手机号输入正确！");
                    com.shoujiduoduo.base.a.a.c("UserLoginActivity", "unknown phone type :" + this.l);
                    return;
                }
                e();
                this.e.setVisibility(4);
                this.f.setVisibility(0);
                this.k.start();
                this.m.setText(this.l);
                this.j = true;
                return;
            case R.id.user_center_back:
                if (this.j) {
                    this.e.setVisibility(0);
                    this.f.setVisibility(4);
                    this.j = false;
                    return;
                }
                finish();
                return;
            case R.id.reget_sms_code:
                this.k.start();
                e();
                return;
            case R.id.login:
                String obj = this.h.getText().toString();
                if (this.o.equals(f.b.ct)) {
                    if (TextUtils.isEmpty(obj) || obj.length() != 6 || !obj.equals(this.f2763b)) {
                        d.a("请输入正确的验证码", 0);
                        return;
                    }
                    c();
                    finish();
                    return;
                } else if (this.o.equals(f.b.cu)) {
                    if (TextUtils.isEmpty(obj) || obj.length() != 6) {
                        d.a("请输入正确的验证码", 0);
                        return;
                    }
                    a("请稍候...");
                    com.shoujiduoduo.util.e.a.a().a(this.l, obj, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            UserLoginActivity.this.a();
                            if (bVar instanceof c.a) {
                                c.a aVar = (c.a) bVar;
                                com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token:" + aVar.f3044a);
                                com.shoujiduoduo.util.e.a.a().a(UserLoginActivity.this.l, aVar.f3044a);
                                UserLoginActivity.this.c();
                                UserLoginActivity.this.finish();
                                return;
                            }
                            d.a("登录失败，验证码不对或失效，请重新获取验证码", 1);
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            UserLoginActivity.this.a();
                            d.a("登录失败，验证码不对或失效，请重新获取验证码", 1);
                        }
                    });
                    return;
                } else if (!this.o.equals(f.b.cm)) {
                    d.a("登录失败，未识别的运营商类型", 0);
                    return;
                } else if (TextUtils.isEmpty(obj) || obj.length() != 6) {
                    d.a("请输入正确的验证码", 0);
                    return;
                } else {
                    a("请稍候...");
                    com.shoujiduoduo.util.c.b.a().a(this.l, obj, new com.shoujiduoduo.util.b.b() {
                        public void a(c.b bVar) {
                            super.a(bVar);
                            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "短信验证码成功登录，cmcc, phone:" + UserLoginActivity.this.l);
                            UserLoginActivity.this.c();
                            UserLoginActivity.this.finish();
                        }

                        public void b(c.b bVar) {
                            super.b(bVar);
                            UserLoginActivity.this.a();
                            d.a("登录失败，验证码不对或失效，请重新获取验证码", 1);
                        }
                    });
                    return;
                }
            case R.id.weixin_login:
                hashMap.put("platform", "weixin");
                this.r.doOauthVerify(this, com.umeng.socialize.c.a.WEIXIN, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            case R.id.qq_login:
                hashMap.put("platform", "qq");
                this.r.doOauthVerify(this, com.umeng.socialize.c.a.QQ, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            case R.id.sina_weibo_login:
                hashMap.put("platform", "sina");
                this.r.doOauthVerify(this, com.umeng.socialize.c.a.SINA, this.s);
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_LOGIN", hashMap);
                a("正在登录...");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        d.a("登录成功", 0);
        k c2 = com.shoujiduoduo.a.b.b.g().c();
        c2.a("phone_" + this.l);
        c2.b(this.l);
        c2.d(this.l);
        c2.c(1);
        c2.a(1);
        com.shoujiduoduo.a.b.b.g().a(c2);
        b();
        com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_USER_CENTER, new c.a<v>() {
            public void a() {
                ((v) this.f1812a).a(1, true, "成功", "0");
            }
        });
        d();
    }

    private void d() {
        if (this.o.equals(f.b.ct)) {
            if (f.v()) {
                com.shoujiduoduo.util.d.b.a().a(this.l, new com.shoujiduoduo.util.b.b() {
                    public void a(c.b bVar) {
                        super.a(bVar);
                        if (bVar != null && (bVar instanceof c.e)) {
                            c.e eVar = (c.e) bVar;
                            k c = com.shoujiduoduo.a.b.b.g().c();
                            if (eVar.e() || eVar.f()) {
                                c.b(2);
                            } else {
                                c.b(0);
                            }
                            com.shoujiduoduo.a.b.b.g().a(c);
                            com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_VIP, new c.a<x>() {
                                public void a() {
                                    ((x) this.f1812a).a(2);
                                }
                            });
                        }
                    }

                    public void b(c.b bVar) {
                        super.b(bVar);
                    }
                });
            }
        } else if (this.o.equals(f.b.cu)) {
            if (f.u()) {
                c(this.l);
            }
        } else if (!this.o.equals(f.b.cm)) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "not support service type");
        } else if (f.t()) {
            com.shoujiduoduo.util.c.b.a().d(new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    k c = com.shoujiduoduo.a.b.b.g().c();
                    c.b(1);
                    com.shoujiduoduo.a.b.b.g().a(c);
                    com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_VIP, new c.a<x>() {
                        public void a() {
                            ((x) this.f1812a).a(1);
                        }
                    });
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    k c = com.shoujiduoduo.a.b.b.g().c();
                    c.b(0);
                    com.shoujiduoduo.a.b.b.g().a(c);
                    com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_VIP, new c.a<x>() {
                        public void a() {
                            ((x) this.f1812a).a(0);
                        }
                    });
                }
            });
        }
    }

    private void c(final String str) {
        if (com.shoujiduoduo.util.e.a.a().a(str)) {
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "当前手机号有token， phone:" + str);
            com.shoujiduoduo.util.e.a.a().f(new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    if (bVar instanceof c.f) {
                        c.f fVar = (c.f) bVar;
                        k c = com.shoujiduoduo.a.b.b.g().c();
                        if (fVar.e()) {
                            c.b(3);
                        } else {
                            c.b(0);
                        }
                        com.shoujiduoduo.a.b.b.g().a(c);
                        com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_VIP, new c.a<x>() {
                            public void a() {
                                ((x) this.f1812a).a(3);
                            }
                        });
                        if (fVar.f3065a.a().equals("40307") || fVar.f3065a.a().equals("40308")) {
                            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token 失效");
                            com.shoujiduoduo.util.e.a.a().a(str, "");
                        }
                    }
                }

                public void b(c.b bVar) {
                    if (bVar.a().equals("40307") || bVar.a().equals("40308")) {
                        com.shoujiduoduo.base.a.a.a("UserLoginActivity", "token 失效");
                        com.shoujiduoduo.util.e.a.a().a(str, "");
                    }
                    super.b(bVar);
                }
            });
            return;
        }
        com.shoujiduoduo.base.a.a.a("UserLoginActivity", "当前手机号没有token， 不做vip查询");
    }

    private void e() {
        if (this.o.equals(f.b.ct)) {
            b("118100");
            this.f2763b = f.a(6);
            com.shoujiduoduo.base.a.a.a("UserLoginActivity", "random key:" + this.f2763b);
            com.shoujiduoduo.util.d.b.a().a(this.l, this.f2763b, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("UserLoginActivity", "onSuccess   " + bVar.toString());
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    com.shoujiduoduo.base.a.a.a("UserLoginActivity", "onfailure  " + bVar.toString());
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else if (this.o.equals(f.b.cu)) {
            b("1065515888");
            com.shoujiduoduo.util.e.a.a().c(this.l, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    com.shoujiduoduo.base.a.a.a("UserLoginActivity", "onSuccess   " + bVar.toString());
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    com.shoujiduoduo.base.a.a.a("UserLoginActivity", "onfailure  " + bVar.toString());
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else if (this.o.equals(f.b.cm)) {
            b("10658830");
            com.shoujiduoduo.util.c.b.a().a(this.l, new com.shoujiduoduo.util.b.b() {
                public void a(c.b bVar) {
                    super.a(bVar);
                    d.a("已成功发送验证码，请注意查收");
                }

                public void b(c.b bVar) {
                    super.b(bVar);
                    d.a("发送验证码失败，请重试发送");
                }
            });
        } else {
            com.shoujiduoduo.base.a.a.c("UserLoginActivity", "unknown service type");
        }
    }
}
