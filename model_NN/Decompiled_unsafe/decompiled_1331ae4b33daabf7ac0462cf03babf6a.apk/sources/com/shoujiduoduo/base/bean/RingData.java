package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.shoujiduoduo.util.l;

public class RingData implements Parcelable {
    public static final Parcelable.Creator<RingData> CREATOR = new Parcelable.Creator<RingData>() {
        /* renamed from: a */
        public RingData createFromParcel(Parcel parcel) {
            RingData ringData = new RingData();
            ringData.e = parcel.readString();
            ringData.f = parcel.readString();
            ringData.g = parcel.readString();
            ringData.h = parcel.readString();
            ringData.i = parcel.readInt();
            ringData.l = parcel.readInt();
            ringData.m = parcel.readInt();
            String unused = ringData.f1954b = parcel.readString();
            int unused2 = ringData.c = parcel.readInt();
            String unused3 = ringData.d = parcel.readString();
            int unused4 = ringData.J = parcel.readInt();
            ringData.p = parcel.readString();
            ringData.q = parcel.readString();
            ringData.r = parcel.readInt();
            ringData.s = parcel.readInt();
            ringData.t = parcel.readString();
            ringData.n = parcel.readInt();
            ringData.u = parcel.readString();
            ringData.v = parcel.readString();
            ringData.w = parcel.readInt();
            ringData.x = parcel.readInt();
            ringData.y = parcel.readInt();
            ringData.z = parcel.readString();
            ringData.A = parcel.readInt();
            ringData.B = parcel.readString();
            ringData.C = parcel.readString();
            ringData.E = parcel.readString();
            ringData.D = parcel.readString();
            ringData.F = parcel.readString();
            ringData.G = parcel.readInt();
            ringData.j = parcel.readString();
            ringData.H = parcel.readInt();
            ringData.k = parcel.readString();
            ringData.I = parcel.readInt();
            return ringData;
        }

        /* renamed from: a */
        public RingData[] newArray(int i) {
            return new RingData[i];
        }
    };
    public int A;
    public String B = "";
    public String C = "";
    public String D = "";
    public String E = "";
    public String F = "";
    public int G = 0;
    public int H = 0;
    public int I = 0;
    /* access modifiers changed from: private */
    public int J;
    private String K = "";
    private int L;

    /* renamed from: a  reason: collision with root package name */
    private String f1953a = "";
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1954b = "";
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public String d = "";
    public String e = "";
    public String f = "";
    public String g = "";
    public String h = "";
    public int i;
    public String j = "";
    public String k = "";
    public int l;
    public int m;
    public int n;
    public String o = "";
    public String p = "";
    public String q = "";
    public int r;
    public int s = 1;
    public String t = "";
    public String u = "";
    public String v = "";
    public int w;
    public int x = 0;
    public int y;
    public String z = "";

    public String a() {
        return "http://" + l.a().b();
    }

    public void a(String str) {
        this.f1953a = str;
    }

    public String b() {
        return a() + this.d;
    }

    public String c() {
        return this.d;
    }

    public void b(String str) {
        this.d = str;
    }

    public void a(int i2) {
        this.J = i2;
    }

    public int d() {
        return this.J;
    }

    public String e() {
        return this.f1954b;
    }

    public void c(String str) {
        this.f1954b = str;
    }

    public void b(int i2) {
        this.c = i2;
    }

    public int f() {
        return this.c;
    }

    public String g() {
        return this.K;
    }

    public void d(String str) {
        this.K = str;
    }

    public void c(int i2) {
        this.L = i2;
    }

    public int h() {
        return this.L;
    }

    public boolean i() {
        if (TextUtils.isEmpty(this.K) || this.L <= 0) {
            return false;
        }
        return true;
    }

    public boolean j() {
        if (!TextUtils.isEmpty(this.d) && this.J > 0) {
            return true;
        }
        if (TextUtils.isEmpty(this.f1954b) || this.c <= 0) {
            return false;
        }
        return true;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("name: ").append(this.e).append(", artist: ").append(this.f).append(", uid:").append(this.j).append(", rid: ").append(this.g).append(", duration: ").append(this.l).append(", score: ").append(this.i).append(", playcnt: ").append(this.m);
        return sb.toString();
    }

    public int k() {
        int i2 = 0;
        try {
            i2 = Integer.valueOf(this.g.equals("") ? "0" : this.g).intValue();
        } catch (NumberFormatException e2) {
            e2.printStackTrace();
        }
        if (this.p.equals("") || i2 > 900000000 || this.s == 1) {
            return i2;
        }
        return i2 + 1000000000;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeInt(this.i);
        parcel.writeInt(this.l);
        parcel.writeInt(this.m);
        parcel.writeString(this.f1954b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeInt(this.J);
        parcel.writeString(this.p);
        parcel.writeString(this.q);
        parcel.writeInt(this.r);
        parcel.writeInt(this.s);
        parcel.writeString(this.t);
        parcel.writeInt(this.n);
        parcel.writeString(this.u);
        parcel.writeString(this.v);
        parcel.writeInt(this.w);
        parcel.writeInt(this.x);
        parcel.writeInt(this.y);
        parcel.writeString(this.z);
        parcel.writeInt(this.A);
        parcel.writeString(this.B);
        parcel.writeString(this.C);
        parcel.writeString(this.E);
        parcel.writeString(this.D);
        parcel.writeString(this.F);
        parcel.writeInt(this.G);
        parcel.writeString(this.j);
        parcel.writeInt(this.H);
        parcel.writeString(this.k);
        parcel.writeInt(this.I);
    }
}
