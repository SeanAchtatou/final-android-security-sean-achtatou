package com.shoujiduoduo.base.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class UserData implements Parcelable {
    public static final Parcelable.Creator<UserData> CREATOR = new Parcelable.Creator<UserData>() {
        /* renamed from: a */
        public UserData createFromParcel(Parcel parcel) {
            return new UserData(parcel);
        }

        /* renamed from: a */
        public UserData[] newArray(int i) {
            return new UserData[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f1955a;

    /* renamed from: b  reason: collision with root package name */
    public String f1956b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public boolean h;
    public int i;
    public int j;
    public int k;
    public int l;

    public UserData() {
    }

    protected UserData(Parcel parcel) {
        this.f1955a = parcel.readString();
        this.f1956b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readByte() != 0;
        this.i = parcel.readInt();
        this.j = parcel.readInt();
        this.k = parcel.readInt();
        this.l = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.f1955a);
        parcel.writeString(this.f1956b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeByte((byte) (this.h ? 1 : 0));
        parcel.writeInt(this.i);
        parcel.writeInt(this.j);
        parcel.writeInt(this.k);
        parcel.writeInt(this.l);
    }
}
