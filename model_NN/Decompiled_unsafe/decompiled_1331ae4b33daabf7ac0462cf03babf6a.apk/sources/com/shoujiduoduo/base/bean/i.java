package com.shoujiduoduo.base.bean;

import android.text.TextUtils;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.o;

/* compiled from: RingCacheData */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public String f1971a;

    /* renamed from: b  reason: collision with root package name */
    public String f1972b;
    public int c;
    public int d;
    public int e;
    public int f;
    public String g;
    public String h;
    public String i = "";
    public int j = 0;
    public String k = "";
    public int l = 0;
    public String m = "";
    public String n = "";
    private String o = "";
    private String p = "";
    private String q;
    private int r;
    private String s;
    private int t;
    private String u;
    private int v;

    public i(String str, String str2, int i2, int i3, int i4, int i5, String str3, String str4) {
        this.f1971a = str;
        this.f1972b = str2;
        this.c = i2;
        this.d = i3;
        this.e = i4;
        this.f = i5;
        this.g = str3;
        this.h = str4;
    }

    public String a() {
        return "http://" + l.a().b();
    }

    public void a(String str) {
        this.p = str;
    }

    public String b() {
        return a() + this.s;
    }

    public void b(String str) {
        this.s = str;
    }

    public void a(int i2) {
        this.t = i2;
    }

    public int c() {
        return this.t;
    }

    public String d() {
        return a() + this.q;
    }

    public void c(String str) {
        this.q = str;
    }

    public void b(int i2) {
        this.r = i2;
    }

    public int e() {
        return this.r;
    }

    public String f() {
        return a() + this.u;
    }

    public void d(String str) {
        this.u = str;
    }

    public void c(int i2) {
        this.v = i2;
    }

    public int g() {
        return this.v;
    }

    public boolean h() {
        if (TextUtils.isEmpty(this.u) || this.v <= 0) {
            return false;
        }
        return true;
    }

    public boolean i() {
        if (!TextUtils.isEmpty(this.s) && this.t > 0) {
            return true;
        }
        if (TextUtils.isEmpty(this.q) || this.r <= 0) {
            return false;
        }
        return true;
    }

    public boolean j() {
        if (TextUtils.isEmpty(this.s) || this.t <= 0) {
            return false;
        }
        return true;
    }

    public boolean k() {
        if (TextUtils.isEmpty(this.q) || this.r <= 0) {
            return false;
        }
        return true;
    }

    public void e(String str) {
        this.o = str;
    }

    public String l() {
        return this.o;
    }

    public static String a(int i2, String str) {
        StringBuilder append = new StringBuilder().append(o.b()).append(i2).append(".");
        if (str == null || str.length() == 0) {
            str = "mp3";
        }
        return append.append(str).toString();
    }
}
