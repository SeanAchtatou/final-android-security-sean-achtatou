package com.shoujiduoduo.base.bean;

public class MakeRingData extends RingData {

    /* renamed from: a  reason: collision with root package name */
    public int f1951a;

    /* renamed from: b  reason: collision with root package name */
    public int f1952b;
    public String c = "";
    public int d;

    public MakeRingData a(RingData ringData) {
        this.f = ringData.f;
        this.h = ringData.h;
        this.p = ringData.p;
        this.g = ringData.g;
        this.l = ringData.l;
        this.s = ringData.s;
        a(ringData.a());
        a(ringData.d());
        b(ringData.c());
        this.n = ringData.n;
        b(ringData.f());
        c(ringData.e());
        this.t = ringData.t;
        this.e = ringData.e;
        this.m = ringData.m;
        this.r = ringData.r;
        this.i = ringData.i;
        this.q = ringData.q;
        this.o = ringData.o;
        if (ringData instanceof MakeRingData) {
            this.f1952b = ((MakeRingData) ringData).f1952b;
            this.f1951a = ((MakeRingData) ringData).f1951a;
            this.c = ((MakeRingData) ringData).c;
            this.d = ((MakeRingData) ringData).d;
        }
        return this;
    }
}
