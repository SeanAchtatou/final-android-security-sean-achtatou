package com.shoujiduoduo.base.bean;

import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.umeng.socialize.PlatformConfig;

/* compiled from: UserInfo */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private String f1975a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f1976b = "";
    private String c = "";
    private String d = "";
    private int e;
    private int f;
    private int g;
    private String h = "";
    private int i;

    public String a() {
        return this.f1975a;
    }

    public void a(String str) {
        this.f1975a = str;
    }

    public String b() {
        if (ag.c(this.f1976b)) {
            return this.h;
        }
        return this.f1976b;
    }

    public void b(String str) {
        this.f1976b = str;
    }

    public String c() {
        return this.c;
    }

    public void c(String str) {
        this.c = str;
    }

    public String d() {
        return this.d;
    }

    public int e() {
        return this.e;
    }

    public String f() {
        switch (this.e) {
            case 1:
                return "phone";
            case 2:
                return "qq";
            case 3:
                return Constants.WEIBO;
            case 4:
                return PlatformConfig.Renren.Name;
            case 5:
                return "weixin";
            default:
                return "";
        }
    }

    public void a(int i2) {
        this.e = i2;
    }

    public int g() {
        return this.g;
    }

    public void b(int i2) {
        this.g = i2;
    }

    public int h() {
        return this.f;
    }

    public void c(int i2) {
        this.f = i2;
    }

    public boolean i() {
        return this.f == 1;
    }

    public void d(int i2) {
        this.i = i2;
    }

    public boolean j() {
        return this.i == 1;
    }

    public boolean k() {
        return this.g != 0 && i();
    }

    public void d(String str) {
        ad.c(RingDDApp.c(), "pref_phone_num", str);
        this.h = str;
    }

    public String l() {
        return this.h;
    }
}
