package com.shoujiduoduo.b.f;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.k;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Element;

/* compiled from: FavoriteRingList */
public class b implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f1913a = k.b(3);

    /* renamed from: b  reason: collision with root package name */
    private a f1914b = new a(RingDDApp.c(), "duoduo.ringtone.database", null, 3);
    /* access modifiers changed from: private */
    public ArrayList<RingData> c = new ArrayList<>();
    /* access modifiers changed from: private */
    public ArrayList<RingData> d = new ArrayList<>();
    /* access modifiers changed from: private */
    public boolean e;
    /* access modifiers changed from: private */
    public String f = "";
    private v g = new v() {
        public void a(int i, boolean z, String str, String str2) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is login, success:" + z);
            if (z) {
                String f = com.shoujiduoduo.a.b.b.g().f();
                if (!TextUtils.isEmpty(f)) {
                    b.this.c(f);
                }
            }
        }

        public void a(int i) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is logout");
            ArrayList unused = b.this.d = b.this.c;
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1812a).a(b.this, 0);
                }
            });
        }

        public void b(int i) {
        }
    };

    public boolean h() {
        return this.e;
    }

    public void i() {
        com.shoujiduoduo.base.a.a.b("FavoriteRingList", "begin init favorite ring data");
        final boolean g2 = com.shoujiduoduo.a.b.b.g().g();
        final String f2 = com.shoujiduoduo.a.b.b.g().f();
        h.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList
             arg types: [com.shoujiduoduo.b.f.b, java.util.ArrayList<T>, java.util.ArrayList, int]
             candidates:
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, int):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, java.lang.String):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList */
            public void run() {
                final ArrayList a2 = b.this.m();
                ArrayList<T> arrayList = null;
                if (g2) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user is in login state, get online ring from cache");
                    f a3 = b.this.b(f2);
                    if (a3 != null) {
                        arrayList = a3.c;
                        b.this.a(a3.f1966b, a2);
                    }
                }
                final ArrayList a4 = b.this.a((ArrayList) arrayList, a2, true);
                c.a().a(new c.b() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, boolean):boolean
                     arg types: [com.shoujiduoduo.b.f.b, int]
                     candidates:
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.lang.String):com.shoujiduoduo.base.bean.f
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList):java.util.ArrayList
                      com.shoujiduoduo.b.f.b.a(java.util.HashMap<java.lang.String, java.lang.String>, java.util.ArrayList<com.shoujiduoduo.base.bean.RingData>):void
                      com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, boolean):boolean */
                    public void a() {
                        if (a2 != null) {
                            ArrayList unused = b.this.c = a2;
                        }
                        if (a4 != null) {
                            ArrayList unused2 = b.this.d = a4;
                        }
                        boolean unused3 = b.this.e = true;
                        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_RING, new c.a<w>() {
                            public void a() {
                                ((w) this.f1812a).a(0, null, "user_favorite");
                            }
                        });
                    }
                });
                if (g2) {
                    b.this.c(f2);
                }
            }
        });
        com.shoujiduoduo.base.a.a.b("FavoriteRingList", "end init favorite ring data");
    }

    public void j() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
    }

    public void k() {
        c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.g);
    }

    public String a() {
        return "user_favorite";
    }

    /* access modifiers changed from: private */
    public f<RingData> b(String str) {
        String str2 = k.a(2) + str + ".xml";
        File file = new File(str2);
        if (file.exists()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache exists, cache file:" + str2);
            try {
                f<RingData> e2 = i.e(new FileInputStream(file));
                if (e2 != null) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache size:" + e2.c.size());
                    this.f = e2.g;
                    return e2;
                }
                com.shoujiduoduo.base.a.a.c("FavoriteRingList", "parse user online ring cache failed!!");
            } catch (ArrayIndexOutOfBoundsException e3) {
                e3.printStackTrace();
            } catch (FileNotFoundException e4) {
                e4.printStackTrace();
            }
        } else {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user online ring cache not exists");
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void c(final String str) {
        h.a(new Runnable() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList
             arg types: [com.shoujiduoduo.b.f.b, java.util.ArrayList<T>, java.util.ArrayList, int]
             candidates:
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, int):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, org.w3c.dom.Element, java.lang.String, java.lang.String):void
              com.shoujiduoduo.b.f.b.a(com.shoujiduoduo.b.f.b, java.util.ArrayList, java.util.ArrayList, boolean):java.util.ArrayList */
            public void run() {
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "get user online ring  begin");
                String e = s.e(b.this.f);
                if (ag.c(e)) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "get user online ring error, return null");
                } else if ("no change".equals(e)) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite return no change");
                } else {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "save online ring to cache");
                    p.a(k.a(2) + str + ".xml", e.getBytes());
                    try {
                        f<RingData> e2 = i.e(new ByteArrayInputStream(e.getBytes()));
                        if (e2 != null) {
                            synchronized ("FavoriteRingList") {
                                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "merge user local ring with online data");
                                if (e2.c.size() > 0) {
                                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring num: " + e2.c.size());
                                    b.this.a(e2.f1966b, b.this.c);
                                    final ArrayList a2 = b.this.a((ArrayList) e2.c, b.this.c, false);
                                    c.a().a(new c.b() {
                                        public void a() {
                                            ArrayList unused = b.this.d = a2;
                                            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                                                public void a() {
                                                    ((g) this.f1812a).a(b.this, 0);
                                                }
                                            });
                                        }
                                    });
                                } else {
                                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring num: 0");
                                }
                            }
                            return;
                        }
                        com.shoujiduoduo.base.a.a.c("FavoriteRingList", "Get user favorite rings failed!!");
                    } catch (ArrayIndexOutOfBoundsException e3) {
                        com.shoujiduoduo.base.a.a.c("FavoriteRingList", "Get user favorite rings failed!!, crash");
                        e3.printStackTrace();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(HashMap<String, String> hashMap, ArrayList<RingData> arrayList) {
        boolean z;
        if (hashMap != null && arrayList != null) {
            boolean z2 = false;
            if (hashMap.size() > 0 && arrayList.size() > 0) {
                Iterator<RingData> it = arrayList.iterator();
                while (true) {
                    z = z2;
                    if (!it.hasNext()) {
                        break;
                    }
                    RingData next = it.next();
                    if (hashMap.containsKey(next.g)) {
                        String str = hashMap.get(next.g);
                        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "replace rid:" + next.g + " to " + str);
                        next.g = str;
                        z2 = true;
                    } else {
                        z2 = z;
                    }
                }
                z2 = z;
            }
            if (z2) {
                a(arrayList);
            }
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> m() {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "read Local ringlist begin");
        File file = new File(f1913a);
        if (file.exists()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite ring file exists, file:" + f1913a);
            try {
                f<RingData> d2 = i.d(new FileInputStream(file));
                if (d2 != null) {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user local favorite ring size:" + d2.c.size());
                    com.shoujiduoduo.util.f.a(RingDDApp.c(), "COLLECT_RING_NUM_NEW", new HashMap(), (long) d2.c.size());
                    return d2.c;
                }
                com.shoujiduoduo.base.a.a.c("FavoriteRingList", "parse user local favorite ring failed!!");
                return null;
            } catch (ArrayIndexOutOfBoundsException e2) {
                e2.printStackTrace();
                return null;
            } catch (FileNotFoundException e3) {
                e3.printStackTrace();
                return null;
            }
        } else {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "user favorite ring file not exists");
            return null;
        }
    }

    /* access modifiers changed from: private */
    public ArrayList<RingData> a(ArrayList<RingData> arrayList, ArrayList<RingData> arrayList2, boolean z) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "mergeRingdata begin, bFromCache:" + z);
        if (arrayList == null || arrayList.size() == 0) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online list is null or size is 0, return localRinglist");
            return arrayList2;
        } else if (arrayList2 == null || arrayList2.size() == 0) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "localRinglist is null or size is 0, return online list");
            return arrayList;
        } else {
            HashMap hashMap = new HashMap();
            ArrayList<RingData> arrayList3 = new ArrayList<>();
            ArrayList arrayList4 = new ArrayList();
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "local ring size:" + arrayList2.size());
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "online ring size:" + arrayList.size());
            Iterator<RingData> it = arrayList.iterator();
            while (it.hasNext()) {
                RingData next = it.next();
                hashMap.put("" + next.k(), next);
            }
            arrayList3.addAll(arrayList);
            Iterator<RingData> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                RingData next2 = it2.next();
                if (!hashMap.containsKey("" + next2.k())) {
                    arrayList3.add(next2);
                    arrayList4.add(next2);
                }
            }
            if (arrayList4.size() > 0 && !z) {
                StringBuilder sb = new StringBuilder();
                Iterator it3 = arrayList4.iterator();
                while (it3.hasNext()) {
                    sb.append(((RingData) it3.next()).g + "|");
                }
                final String substring = sb.substring(0, sb.length() - 1);
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "本地有新增铃声，需要同步至服务器");
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "add user favorite, size:" + arrayList4.size() + ", rid:" + substring);
                h.a(new Runnable() {
                    public void run() {
                        String f = s.f(substring);
                        if (ag.c(f)) {
                            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "sync to server error, return null");
                        } else {
                            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "sync to server, return:" + f);
                        }
                    }
                });
            }
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "merge ring size:" + arrayList3.size());
            return arrayList3;
        }
    }

    public boolean a(String str) {
        synchronized ("FavoriteRingList") {
            for (int i = 0; i < this.d.size(); i++) {
                if (str.equalsIgnoreCase(this.d.get(i).g)) {
                    return true;
                }
            }
            return false;
        }
    }

    private void a(final ArrayList<RingData> arrayList) {
        h.a(new Runnable() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r9 = this;
                    java.lang.String r2 = "FavoriteRingList"
                    monitor-enter(r2)
                    javax.xml.parsers.DocumentBuilderFactory r0 = javax.xml.parsers.DocumentBuilderFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    javax.xml.parsers.DocumentBuilder r0 = r0.newDocumentBuilder()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    org.w3c.dom.Document r3 = r0.newDocument()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r0 = "list"
                    org.w3c.dom.Element r4 = r3.createElement(r0)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r0 = "num"
                    java.util.ArrayList r1 = r2     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    int r1 = r1.size()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r4.setAttribute(r0, r1)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r3.appendChild(r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r0 = 0
                    r1 = r0
                L_0x0029:
                    java.util.ArrayList r0 = r2     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    int r0 = r0.size()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    if (r1 >= r0) goto L_0x017f
                    java.util.ArrayList r0 = r2     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.Object r0 = r0.get(r1)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.base.bean.RingData r0 = (com.shoujiduoduo.base.bean.RingData) r0     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r5 = "ring"
                    org.w3c.dom.Element r5 = r3.createElement(r5)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r6 = "name"
                    java.lang.String r7 = r0.e     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r6 = "artist"
                    java.lang.String r7 = r0.f     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "uid"
                    java.lang.String r8 = r0.j     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "duration"
                    int r8 = r0.l     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "score"
                    int r8 = r0.i     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "playcnt"
                    int r8 = r0.m     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r6 = "rid"
                    java.lang.String r7 = r0.g     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r5.setAttribute(r6, r7)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "bdurl"
                    java.lang.String r8 = r0.h     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "hbr"
                    int r8 = r0.d()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "hurl"
                    java.lang.String r8 = r0.c()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "lbr"
                    int r8 = r0.f()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "lurl"
                    java.lang.String r8 = r0.e()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "mp3br"
                    int r8 = r0.h()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "mp3url"
                    java.lang.String r8 = r0.g()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "isnew"
                    int r8 = r0.n     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cid"
                    java.lang.String r8 = r0.p     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "valid"
                    java.lang.String r8 = r0.q     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "hasmedia"
                    int r8 = r0.s     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "singerId"
                    java.lang.String r8 = r0.t     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "ctcid"
                    java.lang.String r8 = r0.u     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "ctvalid"
                    java.lang.String r8 = r0.v     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cthasmedia"
                    int r8 = r0.x     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "ctvip"
                    int r8 = r0.y     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "wavurl"
                    java.lang.String r8 = r0.z     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cuvip"
                    int r8 = r0.A     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cuftp"
                    java.lang.String r8 = r0.B     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cucid"
                    java.lang.String r8 = r0.C     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cusid"
                    java.lang.String r8 = r0.E     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cuurl"
                    java.lang.String r8 = r0.F     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "cuvalid"
                    java.lang.String r8 = r0.D     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "hasshow"
                    int r8 = r0.G     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "ishot"
                    int r8 = r0.H     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "head_url"
                    java.lang.String r8 = r0.k     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r8)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    com.shoujiduoduo.b.f.b r6 = com.shoujiduoduo.b.f.b.this     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r7 = "comment_num"
                    int r0 = r0.I     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r6.a(r5, r7, r0)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r4.appendChild(r5)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    int r0 = r1 + 1
                    r1 = r0
                    goto L_0x0029
                L_0x017f:
                    javax.xml.transform.TransformerFactory r0 = javax.xml.transform.TransformerFactory.newInstance()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    javax.xml.transform.Transformer r0 = r0.newTransformer()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r1 = "encoding"
                    java.lang.String r4 = "utf-8"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r1 = "indent"
                    java.lang.String r4 = "yes"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r1 = "standalone"
                    java.lang.String r4 = "yes"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r1 = "method"
                    java.lang.String r4 = "xml"
                    r0.setOutputProperty(r1, r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    javax.xml.transform.dom.DOMSource r1 = new javax.xml.transform.dom.DOMSource     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    org.w3c.dom.Element r3 = r3.getDocumentElement()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r1.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.io.File r3 = new java.io.File     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r4 = com.shoujiduoduo.b.f.b.f1913a     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r3.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    boolean r4 = r3.exists()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    if (r4 != 0) goto L_0x01be
                    r3.createNewFile()     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                L_0x01be:
                    java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r4.<init>(r3)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    javax.xml.transform.stream.StreamResult r3 = new javax.xml.transform.stream.StreamResult     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r3.<init>(r4)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    r0.transform(r1, r3)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    java.lang.String r0 = "FavoriteRingList"
                    java.lang.String r1 = "write local favorite ring file success"
                    com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ ParserConfigurationException -> 0x01d4, TransformerConfigurationException -> 0x01e4, FileNotFoundException -> 0x01e9, TransformerException -> 0x01ee, Exception -> 0x01f3 }
                    monitor-exit(r2)     // Catch:{ all -> 0x01e1 }
                L_0x01d3:
                    return
                L_0x01d4:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01e1 }
                L_0x01d8:
                    java.lang.String r0 = "FavoriteRingList"
                    java.lang.String r1 = "write favorite fing file error"
                    com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x01e1 }
                    monitor-exit(r2)     // Catch:{ all -> 0x01e1 }
                    goto L_0x01d3
                L_0x01e1:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x01e1 }
                    throw r0
                L_0x01e4:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01e1 }
                    goto L_0x01d8
                L_0x01e9:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01e1 }
                    goto L_0x01d8
                L_0x01ee:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01e1 }
                    goto L_0x01d8
                L_0x01f3:
                    r0 = move-exception
                    r0.printStackTrace()     // Catch:{ all -> 0x01e1 }
                    goto L_0x01d8
                */
                throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.AnonymousClass5.run():void");
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Element element, String str, String str2) {
        if (!ag.c(str2)) {
            element.setAttribute(str, str2);
        }
    }

    /* access modifiers changed from: private */
    public void a(Element element, String str, int i) {
        if (i != 0) {
            element.setAttribute(str, "" + i);
        }
    }

    public boolean a(List<RingData> list) {
        boolean z;
        synchronized ("FavoriteRingList") {
            if (list != null) {
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    for (RingData ringData : list) {
                        sb.append(ringData.g);
                        sb.append("|");
                    }
                    if (this.d == this.c) {
                        this.d.removeAll(list);
                    } else {
                        this.d.removeAll(list);
                        this.c.removeAll(list);
                    }
                    if (sb.toString().endsWith("|")) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    e(sb.toString());
                    a(this.c);
                    n();
                    z = true;
                }
            }
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r0 = a((java.util.List<com.shoujiduoduo.base.bean.RingData>) r2);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.util.Collection<java.lang.Integer> r7) {
        /*
            r6 = this;
            java.lang.String r1 = "FavoriteRingList"
            monitor-enter(r1)
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x003b }
            r2.<init>()     // Catch:{ all -> 0x003b }
            java.util.Iterator r3 = r7.iterator()     // Catch:{ all -> 0x003b }
        L_0x000c:
            boolean r0 = r3.hasNext()     // Catch:{ all -> 0x003b }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r3.next()     // Catch:{ all -> 0x003b }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x003b }
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            if (r4 < 0) goto L_0x002a
            int r4 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r5 = r6.d     // Catch:{ all -> 0x003b }
            int r5 = r5.size()     // Catch:{ all -> 0x003b }
            if (r4 <= r5) goto L_0x002d
        L_0x002a:
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
        L_0x002c:
            return r0
        L_0x002d:
            java.util.ArrayList<com.shoujiduoduo.base.bean.RingData> r4 = r6.d     // Catch:{ all -> 0x003b }
            int r0 = r0.intValue()     // Catch:{ all -> 0x003b }
            java.lang.Object r0 = r4.get(r0)     // Catch:{ all -> 0x003b }
            r2.add(r0)     // Catch:{ all -> 0x003b }
            goto L_0x000c
        L_0x003b:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            throw r0
        L_0x003e:
            boolean r0 = r6.a(r2)     // Catch:{ all -> 0x003b }
            monitor-exit(r1)     // Catch:{ all -> 0x003b }
            goto L_0x002c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.a(java.util.Collection):boolean");
    }

    public boolean b(int i) {
        boolean z;
        synchronized ("FavoriteRingList") {
            if (i >= this.d.size() || i < 0) {
                z = false;
            } else {
                String str = this.d.get(i).g;
                if (this.d != this.c) {
                    this.d.remove(i);
                }
                d(str);
                e(str);
                n();
                z = true;
            }
        }
        return z;
    }

    private void d(String str) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "delete from local data, rid:" + str);
        Iterator<RingData> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            RingData next = it.next();
            if (next.g.equals(str)) {
                com.shoujiduoduo.base.a.a.a("FavoriteRingList", "found in local, delete success");
                this.c.remove(next);
                a(this.c);
                break;
            }
        }
        this.f1914b.a(str);
    }

    private void e(final String str) {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            com.shoujiduoduo.base.a.a.a("FavoriteRingList", "delete from online data, rid:" + str);
            h.a(new Runnable() {
                public void run() {
                    s.g(str);
                }
            });
        }
    }

    public boolean a(RingData ringData) {
        boolean add;
        synchronized ("FavoriteRingList") {
            Iterator<RingData> it = this.d.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().g.equals(ringData.g)) {
                        add = true;
                        break;
                    }
                } else {
                    add = this.d.add(ringData);
                    b(ringData);
                    c(ringData);
                    n();
                    break;
                }
            }
        }
        return add;
    }

    private void b(RingData ringData) {
        com.shoujiduoduo.base.a.a.a("FavoriteRingList", "append to local data, name:" + ringData.e);
        if (this.d != this.c) {
            this.c.add(ringData);
        }
        this.f1914b.a(ringData);
        a(this.c);
    }

    private void c(final RingData ringData) {
        if (com.shoujiduoduo.a.b.b.g().g()) {
            h.a(new Runnable() {
                public void run() {
                    com.shoujiduoduo.base.a.a.a("FavoriteRingList", "append to online data, name:" + ringData.e);
                    s.f(ringData.g);
                }
            });
        }
    }

    private void n() {
        c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
            public void a() {
                ((g) this.f1812a).a(b.this, 0);
            }
        });
    }

    /* compiled from: FavoriteRingList */
    private class a extends SQLiteOpenHelper {

        /* renamed from: b  reason: collision with root package name */
        private final String f1929b = "UserRingDB";

        public a(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
            super(context, str, cursorFactory, i);
        }

        public void a(String str) {
            SQLiteDatabase sQLiteDatabase = null;
            try {
                sQLiteDatabase = getWritableDatabase();
                sQLiteDatabase.execSQL("delete from user_ring_table where rid='" + str + "'");
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
            } catch (Throwable th) {
                if (sQLiteDatabase != null) {
                    sQLiteDatabase.close();
                }
                throw th;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:10:0x00d4, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x00d5, code lost:
            r3 = r1;
            r1 = r0;
            r0 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x00da, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x00ce, code lost:
            if (r0 != null) goto L_0x00d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x00d0, code lost:
            r0.close();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x00da  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x00cd A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0001] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(com.shoujiduoduo.base.bean.RingData r5) {
            /*
                r4 = this;
                r0 = 0
                android.database.sqlite.SQLiteDatabase r0 = r4.getWritableDatabase()     // Catch:{ Exception -> 0x00cd, all -> 0x00d4 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r1.<init>()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = "insert into user_ring_table (rid, name, artist, duration, score, play_count, cailing_id, cailing_valid_date, has_media, singer_id, price)VALUES ("
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.g     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.e     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.f     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.l     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.i     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.m     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.p     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.q     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.s     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = r5.t     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = android.database.DatabaseUtils.sqlEscapeString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ","
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                int r2 = r5.r     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = java.lang.Integer.toString(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r2 = ");"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                r0.execSQL(r1)     // Catch:{ Exception -> 0x00cd, all -> 0x00de }
                if (r0 == 0) goto L_0x00cc
                r0.close()
            L_0x00cc:
                return
            L_0x00cd:
                r1 = move-exception
                if (r0 == 0) goto L_0x00cc
                r0.close()
                goto L_0x00cc
            L_0x00d4:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
            L_0x00d8:
                if (r1 == 0) goto L_0x00dd
                r1.close()
            L_0x00dd:
                throw r0
            L_0x00de:
                r1 = move-exception
                r3 = r1
                r1 = r0
                r0 = r3
                goto L_0x00d8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.b.f.b.a.a(com.shoujiduoduo.base.bean.RingData):void");
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        }
    }

    /* renamed from: c */
    public RingData a(int i) {
        if (i < 0 || i >= this.d.size()) {
            return null;
        }
        return this.d.get(i);
    }

    public int c() {
        return this.d.size();
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public boolean g() {
        return false;
    }

    public g.a b() {
        return g.a.list_user_favorite;
    }

    public void f() {
    }
}
