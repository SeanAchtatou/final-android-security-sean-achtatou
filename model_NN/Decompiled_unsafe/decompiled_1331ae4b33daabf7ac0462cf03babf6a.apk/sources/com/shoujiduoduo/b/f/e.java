package com.shoujiduoduo.b.f;

import android.text.TextUtils;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.q;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.c;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.s;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/* compiled from: RingUploader */
public class e {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Timer f1940a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1941b;

    static /* synthetic */ int a(e eVar) {
        int i = eVar.f1941b;
        eVar.f1941b = i + 1;
        return i;
    }

    public void a(final MakeRingData makeRingData, final int i) {
        c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                ((q) this.f1812a).a(makeRingData);
            }
        });
        h.a(new Runnable() {
            public void run() {
                int unused = e.this.f1941b = 0;
                Timer unused2 = e.this.f1940a = new Timer();
                e.this.f1940a.schedule(new TimerTask() {
                    public void run() {
                        e.a(e.this);
                        if (e.this.f1941b > 95) {
                            int unused = e.this.f1941b = 95;
                            e.this.f1940a.cancel();
                        }
                        e.this.b(makeRingData, e.this.f1941b);
                    }
                }, 0, 600);
                HashMap hashMap = new HashMap();
                if (makeRingData.g.equals("")) {
                    String e = s.e();
                    if (TextUtils.isEmpty(e) || e.equals("0")) {
                        a.c("RingUploader", "genrid error");
                        e.this.b(makeRingData);
                        hashMap.put("ret", "genrid error");
                        com.umeng.analytics.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                        return;
                    }
                    a.a("RingUploader", "genrid,success. rid:" + e);
                    makeRingData.g = e;
                }
                String str = makeRingData.o;
                if (!com.shoujiduoduo.util.c.a(str, makeRingData.g + "." + p.b(str), makeRingData.d == 0 ? c.a.recordRing : c.a.editRing)) {
                    a.c("RingUploader", "bcs 上传失败");
                    e.this.b(makeRingData);
                    hashMap.put("ret", "bcs upload error");
                    com.umeng.analytics.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                    return;
                }
                a.a("RingUploader", "bcs 上传成功");
                if (s.a(makeRingData, String.valueOf(i))) {
                    a.a("RingUploader", "上传后的铃声数据通知服务器。 成功");
                    e.this.a(makeRingData);
                    hashMap.put("ret", "success");
                    com.umeng.analytics.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
                    return;
                }
                a.a("RingUploader", "上传后的铃声数据通知服务器。 失败");
                e.this.b(makeRingData);
                hashMap.put("ret", "upload inform error");
                com.umeng.analytics.b.a(RingDDApp.c(), "USER_RING_UPLOAD", hashMap);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final MakeRingData makeRingData) {
        this.f1940a.cancel();
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.f1951a = 1;
                makeRingData.f1952b = 100;
                ((q) this.f1812a).b(makeRingData);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final MakeRingData makeRingData) {
        this.f1940a.cancel();
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.f1951a = 0;
                makeRingData.f1952b = -1;
                ((q) this.f1812a).c(makeRingData);
            }
        });
    }

    /* access modifiers changed from: private */
    public void b(final MakeRingData makeRingData, final int i) {
        com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_RING_UPLOAD, new c.a<q>() {
            public void a() {
                makeRingData.f1951a = 0;
                makeRingData.f1952b = i;
                ((q) this.f1812a).a(makeRingData, i);
            }
        });
    }
}
