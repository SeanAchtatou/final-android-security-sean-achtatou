package com.shoujiduoduo.b.f;

import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.w;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import java.util.Collection;

/* compiled from: UserRingListMgrImpl */
public class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1945a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1946b;
    private b c;
    private a d;
    private d e;

    public void a() {
        this.c = new b();
        this.e = new d();
        this.d = new a();
        this.c.j();
        this.e.j();
        this.f1945a = true;
        this.f1946b = false;
        c.a().a(b.OBSERVER_USER_RING, new c.a<w>() {
            public void a() {
                ((w) this.f1812a).a();
            }
        });
        e();
    }

    public void b() {
        this.c.k();
        this.e.k();
    }

    private void e() {
        this.c.i();
        this.e.i();
        this.d.i();
        this.f1945a = false;
        this.f1946b = true;
    }

    public boolean c() {
        return this.c.h() && this.e.h() && this.d.h();
    }

    public boolean a(RingData ringData, String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c.a(ringData);
        }
        if (str.equals("make_ring_list")) {
            if (ringData instanceof MakeRingData) {
                return this.e.a((MakeRingData) ringData);
            }
            a.c("UserRingListMgrImpl", "add make ring, but data is not makeringdata type");
        }
        return false;
    }

    public boolean a(String str, String str2) {
        if (str2.equals("favorite_ring_list")) {
            return this.c.a(str);
        }
        if (str2.equals("make_ring_list")) {
            return this.e.a(str);
        }
        return false;
    }

    public boolean a(String str, int i) {
        if (str.equals("favorite_ring_list")) {
            return this.c.b(i);
        }
        if (str.equals("make_ring_list")) {
            return this.e.b(i);
        }
        return false;
    }

    public d a(String str) {
        if (str.equals("favorite_ring_list")) {
            return this.c;
        }
        if (str.equals("make_ring_list")) {
            return this.e;
        }
        if (str.equals("collect_ring_list")) {
            return this.d;
        }
        return null;
    }

    public boolean a(com.shoujiduoduo.base.bean.b bVar) {
        return this.d.a(bVar);
    }

    public d d() {
        return this.d;
    }

    public boolean a(String str, Collection<Integer> collection) {
        if (str.equals("make_ring_list")) {
            return this.e.a(collection);
        }
        if (str.equals("favorite_ring_list")) {
            return this.c.a(collection);
        }
        return false;
    }

    public boolean a(Collection<Integer> collection) {
        return this.d.a(collection);
    }
}
