package com.shoujiduoduo.b.c;

import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.g;
import java.util.ArrayList;

/* compiled from: SimpleList */
public class i implements d {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<RingData> f1871a = new ArrayList<>();

    public void a(RingData ringData) {
        this.f1871a.add(ringData);
    }

    public Object a(int i) {
        if (i < 0 || i >= this.f1871a.size()) {
            return null;
        }
        return this.f1871a.get(i);
    }

    public String a() {
        return "";
    }

    public g.a b() {
        return g.a.list_simple;
    }

    public int c() {
        if (this.f1871a != null) {
            return this.f1871a.size();
        }
        return 0;
    }

    public boolean d() {
        return false;
    }

    public void e() {
    }

    public void f() {
    }

    public boolean g() {
        return false;
    }
}
