package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.o;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: RingListCache */
public class h extends o<f<RingData>> {
    h(String str) {
        super(str);
    }

    public f<RingData> a() {
        try {
            return i.d(new FileInputStream(c + this.f3216b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void a(f<RingData> fVar) {
        ArrayList<T> arrayList = fVar.c;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(fVar.d));
            createElement.setAttribute("area", String.valueOf(fVar.e));
            createElement.setAttribute("baseurl", fVar.f);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + fVar.h);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                RingData ringData = (RingData) arrayList.get(i);
                Element createElement2 = newDocument.createElement("ring");
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, ringData.e);
                createElement2.setAttribute("artist", ringData.f);
                a(createElement2, "uid", ringData.j);
                a(createElement2, "duration", ringData.l);
                a(createElement2, WBConstants.GAME_PARAMS_SCORE, ringData.i);
                a(createElement2, "playcnt", ringData.m);
                createElement2.setAttribute("rid", ringData.g);
                a(createElement2, "bdurl", ringData.h);
                a(createElement2, "hbr", ringData.d());
                a(createElement2, "hurl", ringData.c());
                a(createElement2, "lbr", ringData.f());
                a(createElement2, "lurl", ringData.e());
                a(createElement2, "mp3br", ringData.h());
                a(createElement2, "mp3url", ringData.g());
                a(createElement2, "isnew", ringData.n);
                a(createElement2, "cid", ringData.p);
                a(createElement2, "valid", ringData.q);
                a(createElement2, "hasmedia", ringData.s);
                a(createElement2, "singerId", ringData.t);
                a(createElement2, "price", ringData.r);
                a(createElement2, "ctcid", ringData.u);
                a(createElement2, "ctvalid", ringData.v);
                a(createElement2, "cthasmedia", ringData.x);
                a(createElement2, "ctprice", ringData.w);
                a(createElement2, "ctvip", ringData.y);
                a(createElement2, "wavurl", ringData.z);
                a(createElement2, "cuvip", ringData.A);
                a(createElement2, "cuftp", ringData.B);
                a(createElement2, "cucid", ringData.C);
                a(createElement2, "cusid", ringData.E);
                a(createElement2, "cuurl", ringData.F);
                a(createElement2, "cuvalid", ringData.D);
                a(createElement2, "hasshow", ringData.G);
                a(createElement2, "ishot", ringData.H);
                a(createElement2, "head_url", ringData.k);
                a(createElement2, "comment_num", ringData.I);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.f3216b))));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            a.a(e);
        } catch (TransformerException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (Exception e4) {
            e4.printStackTrace();
            a.a(e4);
        }
    }

    private void a(Element element, String str, String str2) {
        if (!ag.c(str2)) {
            element.setAttribute(str, str2);
        }
    }

    private void a(Element element, String str, int i) {
        if (i != 0) {
            element.setAttribute(str, "" + i);
        }
    }
}
