package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.o;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: CollectListCache */
public class d extends o<f<b>> {
    public d() {
    }

    public d(String str) {
        super(str);
    }

    public void a(f<b> fVar) {
        ArrayList<T> arrayList = fVar.c;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(fVar.d));
            createElement.setAttribute("area", String.valueOf(fVar.e));
            createElement.setAttribute("baseurl", fVar.f);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + fVar.h);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                b bVar = (b) arrayList.get(i);
                Element createElement2 = newDocument.createElement("collect");
                createElement2.setAttribute("id", bVar.g);
                createElement2.setAttribute("title", bVar.f1960b);
                createElement2.setAttribute("content", bVar.c);
                createElement2.setAttribute("time", bVar.d);
                createElement2.setAttribute("isnew", bVar.e ? "1" : "0");
                createElement2.setAttribute("fav", bVar.f);
                createElement2.setAttribute("pic", bVar.f1959a);
                createElement2.setAttribute("artist", bVar.h);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.f3216b))));
        } catch (DOMException e) {
            e.printStackTrace();
            a.a(e);
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            a.a(e2);
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            a.a(e3);
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            a.a(e4);
        } catch (TransformerException e5) {
            e5.printStackTrace();
            a.a(e5);
        } catch (Exception e6) {
            e6.printStackTrace();
            a.a(e6);
        }
    }

    public f<b> a() {
        try {
            return i.c(new FileInputStream(c + this.f3216b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
