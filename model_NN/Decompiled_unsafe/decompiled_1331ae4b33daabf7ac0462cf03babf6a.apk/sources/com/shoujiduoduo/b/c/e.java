package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;

/* compiled from: CommentList */
public class e implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f1858a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1859b = true;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public boolean d = false;
    private int e = -1;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public ArrayList<c> g = new ArrayList<>();
    private String h;
    private int i;
    private Handler j = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    f fVar = (f) message.obj;
                    if (fVar != null) {
                        a.a("RingList", "new obtained list data size = " + fVar.c.size());
                        if (e.this.f1858a) {
                            e.this.g.clear();
                            e.this.g.addAll(fVar.c);
                        } else if (e.this.g == null) {
                            ArrayList unused = e.this.g = fVar.c;
                        } else {
                            e.this.g.addAll(fVar.c);
                        }
                        boolean unused2 = e.this.f1859b = fVar.d;
                        fVar.c = e.this.g;
                        if (e.this.f && e.this.g.size() > 0) {
                            boolean unused3 = e.this.f = false;
                        }
                    }
                    boolean unused4 = e.this.c = false;
                    boolean unused5 = e.this.d = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = e.this.c = false;
                    boolean unused7 = e.this.d = true;
                    break;
            }
            final int i = message.what;
            com.shoujiduoduo.a.a.c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1812a).a(e.this, i);
                }
            });
        }
    };

    public e(String str) {
        this.h = str;
    }

    public Object a(int i2) {
        if (i2 >= this.g.size() || i2 < 0) {
            return null;
        }
        return this.g.get(i2);
    }

    public String a() {
        return "comment_list";
    }

    public g.a b() {
        return g.a.list_comment;
    }

    public int c() {
        return this.g.size();
    }

    public boolean d() {
        return this.c;
    }

    public void e() {
        if (this.g == null || this.g.size() == 0) {
            this.c = true;
            this.d = false;
            this.f1858a = false;
            h.a(new Runnable() {
                public void run() {
                    e.this.h();
                }
            });
        } else if (this.f1859b) {
            this.c = true;
            this.d = false;
            this.f1858a = false;
            h.a(new Runnable() {
                public void run() {
                    e.this.i();
                }
            });
        }
    }

    public void f() {
        this.c = true;
        this.d = false;
        this.f1858a = true;
        h.a(new Runnable() {
            public void run() {
                e.this.h();
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.i.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.f<com.shoujiduoduo.base.bean.c>
     arg types: [java.io.ByteArrayInputStream, int]
     candidates:
      com.shoujiduoduo.util.i.a(org.json.JSONObject, com.shoujiduoduo.base.bean.c):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.UserData):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.a):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.b):void
      com.shoujiduoduo.util.i.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.f<com.shoujiduoduo.base.bean.c> */
    /* access modifiers changed from: private */
    public void h() {
        a.a("CommentList", "getListData");
        String b2 = b(0);
        if (ag.c(b2)) {
            a.a("CommentList", "RingList: httpGetRingList Failed!");
            this.j.sendEmptyMessage(1);
            return;
        }
        f<com.shoujiduoduo.base.bean.c> a2 = i.a((InputStream) new ByteArrayInputStream(b2.getBytes()), false);
        if (a2 != null) {
            this.i = a2.f1965a;
            a.a("CommentList", "list data size = " + a2.c.size() + ", hotCommentNum:" + this.i);
            a.a("CommentList", "UserList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.f = true;
            this.e = 0;
            this.j.sendMessage(this.j.obtainMessage(0, a2));
            return;
        }
        a.a("CommentList", "RingList: but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.j.sendEmptyMessage(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.i.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.f<com.shoujiduoduo.base.bean.c>
     arg types: [java.io.ByteArrayInputStream, int]
     candidates:
      com.shoujiduoduo.util.i.a(org.json.JSONObject, com.shoujiduoduo.base.bean.c):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.RingData):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.UserData):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.a):void
      com.shoujiduoduo.util.i.a(org.w3c.dom.NamedNodeMap, com.shoujiduoduo.base.bean.b):void
      com.shoujiduoduo.util.i.a(java.io.InputStream, boolean):com.shoujiduoduo.base.bean.f<com.shoujiduoduo.base.bean.c> */
    /* access modifiers changed from: private */
    public void i() {
        int i2;
        f<com.shoujiduoduo.base.bean.c> fVar;
        a.a("CommentList", "retrieving more data, list size = " + (this.g.size() - this.i));
        if (this.e < 0) {
            i2 = (this.g.size() - this.i) / 25;
            a.a("CommentList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.e + 1;
            a.a("CommentList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String b2 = b(i2);
        if (ag.c(b2)) {
            this.j.sendEmptyMessage(2);
            return;
        }
        try {
            fVar = i.a((InputStream) new ByteArrayInputStream(b2.getBytes()), true);
        } catch (ArrayIndexOutOfBoundsException e2) {
            fVar = null;
        }
        if (fVar != null) {
            a.a("RingList", "list data size = " + fVar.c.size());
            this.f = true;
            this.e = i2;
            this.j.sendMessage(this.j.obtainMessage(0, fVar));
            return;
        }
        this.j.sendEmptyMessage(2);
    }

    private String b(int i2) {
        return s.a("getcommentlist", "&page=" + i2 + "&pagesize=" + 25 + "&uid=" + com.shoujiduoduo.a.b.b.g().c().a() + "&rid=" + this.h);
    }

    public boolean g() {
        return this.f1859b;
    }
}
