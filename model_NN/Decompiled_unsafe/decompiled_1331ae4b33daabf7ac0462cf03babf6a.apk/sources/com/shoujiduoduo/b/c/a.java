package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: ArtistList */
public class a implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public b f1848a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f1849b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public int f = -1;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public ArrayList<com.shoujiduoduo.base.bean.a> h = new ArrayList<>();
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    f fVar = (f) message.obj;
                    if (fVar != null) {
                        com.shoujiduoduo.base.a.a.a("RingList", "new obtained list data size = " + fVar.c.size());
                        if (a.this.h == null) {
                            ArrayList unused = a.this.h = fVar.c;
                        } else {
                            a.this.h.addAll(fVar.c);
                        }
                        boolean unused2 = a.this.c = fVar.d;
                        fVar.c = a.this.h;
                        if (a.this.f >= 0) {
                            fVar.h = a.this.f;
                        }
                        if (a.this.g && a.this.h.size() > 0) {
                            a.this.f1848a.a(fVar);
                            boolean unused3 = a.this.g = false;
                        }
                    }
                    boolean unused4 = a.this.d = false;
                    boolean unused5 = a.this.e = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = a.this.d = false;
                    boolean unused7 = a.this.e = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1812a).a(a.this, i);
                }
            });
        }
    };

    public a(String str) {
        this.f1848a = new b("artist_" + str + ".tmp");
        this.f1849b = str;
    }

    public String a() {
        return "artist";
    }

    public void e() {
        if (this.h == null || this.h.size() == 0) {
            this.d = true;
            this.e = false;
            h.a(new Runnable() {
                public void run() {
                    a.this.h();
                }
            });
        } else if (this.c) {
            this.d = true;
            this.e = false;
            h.a(new Runnable() {
                public void run() {
                    a.this.i();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.f1848a.a(14400000)) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "CollectList: cache is available! Use Cache!");
            f<com.shoujiduoduo.base.bean.a> a2 = this.f1848a.a();
            if (!(a2 == null || a2.c == null || a2.c.size() <= 0)) {
                com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read RingList Cache Success!");
                this.f = a2.h;
                this.i.sendMessage(this.i.obtainMessage(0, a2));
                return;
            }
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: cache is out of date or read cache failed!");
        String c2 = c(0);
        if (ag.c(c2)) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        f<com.shoujiduoduo.base.bean.a> b2 = i.b(new ByteArrayInputStream(c2.getBytes()));
        if (b2 != null) {
            com.shoujiduoduo.base.a.a.a("ArtistList", "list data size = " + b2.c.size());
            com.shoujiduoduo.base.a.a.a("ArtistList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.g = true;
            this.f = 0;
            this.i.sendMessage(this.i.obtainMessage(0, b2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("ArtistList", "RingList:parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void i() {
        int i2;
        f<com.shoujiduoduo.base.bean.a> fVar;
        com.shoujiduoduo.base.a.a.a("ArtistList", "retrieving more data, list size = " + this.h.size());
        if (this.f < 0) {
            i2 = this.h.size() / 25;
            com.shoujiduoduo.base.a.a.a("ArtistList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.f + 1;
            com.shoujiduoduo.base.a.a.a("ArtistList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String c2 = c(i2);
        if (ag.c(c2)) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            fVar = i.b(new ByteArrayInputStream(c2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            fVar = null;
        }
        if (fVar != null) {
            com.shoujiduoduo.base.a.a.a("RingList", "list data size = " + fVar.c.size());
            this.g = true;
            this.f = i2;
            this.i.sendMessage(this.i.obtainMessage(0, fVar));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String c(int i2) {
        return s.a("hotartist", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.f1849b);
    }

    public boolean d() {
        return this.d;
    }

    /* renamed from: b */
    public com.shoujiduoduo.base.bean.a a(int i2) {
        if (i2 < 0 || i2 >= this.h.size()) {
            return null;
        }
        return this.h.get(i2);
    }

    public int c() {
        return this.h.size();
    }

    public boolean g() {
        return this.c;
    }

    public g.a b() {
        return g.a.list_artist;
    }

    public void f() {
    }
}
