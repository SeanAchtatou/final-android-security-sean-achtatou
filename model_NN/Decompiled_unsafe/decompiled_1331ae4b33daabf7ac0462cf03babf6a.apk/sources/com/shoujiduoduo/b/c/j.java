package com.shoujiduoduo.b.c;

import android.util.Xml;
import cn.banshenggua.aichang.utils.Constants;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.u;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.o;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.s;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: TopListMgrImpl */
public class j implements f {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<com.shoujiduoduo.base.bean.j> f1872a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public a f1873b;
    private boolean c;
    private HashSet<String> d;

    public void a() {
        this.f1873b = new a("toplist.tmp");
        this.d = new HashSet<>();
        this.d.add("list");
        this.d.add("collect");
        this.d.add("artist");
        this.d.add("html");
        e();
    }

    public void b() {
    }

    public boolean c() {
        return this.c;
    }

    public ArrayList<com.shoujiduoduo.base.bean.j> d() {
        if (this.c) {
            return this.f1872a;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public ArrayList<com.shoujiduoduo.base.bean.j> a(InputStream inputStream) {
        try {
            Document parse = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream);
            if (parse == null) {
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "cannot find root node");
                return null;
            }
            NodeList elementsByTagName = documentElement.getElementsByTagName(Constants.ITEM);
            if (elementsByTagName == null) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "cannot find node named \"item\"");
                return null;
            }
            ArrayList<com.shoujiduoduo.base.bean.j> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                com.shoujiduoduo.base.bean.j jVar = new com.shoujiduoduo.base.bean.j();
                jVar.e = f.a(attributes, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                jVar.f = f.a(attributes, "type", "");
                jVar.g = f.a(attributes, "id", 0);
                jVar.h = f.a(attributes, "url");
                if (!this.d.contains(jVar.f)) {
                    com.shoujiduoduo.base.a.a.e("TopListMgrImpl", "not support top list type:" + jVar.f);
                } else if (!"html".equals(jVar.f)) {
                    arrayList.add(jVar);
                } else if (com.shoujiduoduo.util.a.d()) {
                    arrayList.add(jVar);
                }
            }
            return arrayList;
        } catch (IOException e) {
            com.shoujiduoduo.base.a.a.a(e);
            return null;
        } catch (SAXException e2) {
            com.shoujiduoduo.base.a.a.a(e2);
            return null;
        } catch (ParserConfigurationException e3) {
            com.shoujiduoduo.base.a.a.a(e3);
            return null;
        } catch (ArrayIndexOutOfBoundsException e4) {
            com.shoujiduoduo.base.a.a.a(e4);
            return null;
        } catch (DOMException e5) {
            com.shoujiduoduo.base.a.a.a(e5);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public boolean f() {
        String str = "";
        if (f.s().toString().contains("cu")) {
            str = "&cucc=1";
        }
        String a2 = s.a("gettabs", str);
        if (a2 != null) {
            this.f1872a = a(new ByteArrayInputStream(a2.getBytes()));
            if (this.f1872a == null || this.f1872a.size() <= 0) {
                com.shoujiduoduo.base.a.a.c("TopListMgrImpl", "parse net data error");
            } else {
                com.shoujiduoduo.base.a.a.a("TopListMgrImpl", this.f1872a.size() + " keywords.");
                this.f1873b.a(this.f1872a);
                this.c = true;
                c.a().a(b.OBSERVER_TOP_LIST, new c.a<u>() {
                    public void a() {
                        ((u) this.f1812a).a(1);
                    }
                });
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public boolean g() {
        this.f1872a = this.f1873b.a();
        if (this.f1872a == null || this.f1872a.size() <= 0) {
            com.shoujiduoduo.base.a.a.a("TopListMgrImpl", "cache is not valid");
            return false;
        }
        com.shoujiduoduo.base.a.a.a("TopListMgrImpl", this.f1872a.size() + " list. read from cache.");
        this.c = true;
        c.a().a(b.OBSERVER_TOP_LIST, new c.a<u>() {
            public void a() {
                ((u) this.f1812a).a(1);
            }
        });
        return true;
    }

    public void e() {
        if (this.f1872a == null) {
            h.a(new Runnable() {
                public void run() {
                    if (j.this.f1873b.a(21600000)) {
                        if (!j.this.f() && !j.this.g()) {
                            c.a().a(b.OBSERVER_TOP_LIST, new c.a<u>() {
                                public void a() {
                                    ((u) this.f1812a).a(2);
                                }
                            });
                        }
                    } else if (!j.this.g() && !j.this.f()) {
                        c.a().a(b.OBSERVER_TOP_LIST, new c.a<u>() {
                            public void a() {
                                ((u) this.f1812a).a(2);
                            }
                        });
                    }
                }
            });
        }
    }

    /* compiled from: TopListMgrImpl */
    class a extends o<ArrayList<com.shoujiduoduo.base.bean.j>> {
        a(String str) {
            super(str);
        }

        public ArrayList<com.shoujiduoduo.base.bean.j> a() {
            try {
                return j.this.a(new FileInputStream(c + this.f3216b));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(ArrayList<com.shoujiduoduo.base.bean.j> arrayList) {
            if (arrayList != null && arrayList.size() != 0) {
                XmlSerializer newSerializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                try {
                    newSerializer.setOutput(stringWriter);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag("", "root");
                    newSerializer.attribute("", "num", String.valueOf(arrayList.size()));
                    for (int i = 0; i < arrayList.size(); i++) {
                        com.shoujiduoduo.base.bean.j jVar = arrayList.get(i);
                        newSerializer.startTag("", Constants.ITEM);
                        newSerializer.attribute("", SelectCountryActivity.EXTRA_COUNTRY_NAME, jVar.e);
                        newSerializer.attribute("", "type", "" + jVar.f);
                        newSerializer.attribute("", "id", "" + jVar.g);
                        newSerializer.attribute("", "url", "" + jVar.h);
                        newSerializer.endTag("", Constants.ITEM);
                    }
                    newSerializer.endTag("", "root");
                    newSerializer.endDocument();
                    p.b(c + this.f3216b, stringWriter.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
