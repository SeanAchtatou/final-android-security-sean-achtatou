package com.shoujiduoduo.b.c;

import cn.banshenggua.aichang.utils.StringUtil;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.base.bean.a;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.o;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/* compiled from: ArtistListCache */
public class b extends o<f<a>> {
    public b() {
    }

    public b(String str) {
        super(str);
    }

    public void a(f<a> fVar) {
        ArrayList<T> arrayList = fVar.c;
        try {
            Document newDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
            Element createElement = newDocument.createElement("list");
            createElement.setAttribute("num", String.valueOf(arrayList.size()));
            createElement.setAttribute("hasmore", String.valueOf(fVar.d));
            createElement.setAttribute("area", String.valueOf(fVar.e));
            createElement.setAttribute("baseurl", fVar.f);
            createElement.setAttribute(WBPageConstants.ParamKey.PAGE, "" + fVar.h);
            newDocument.appendChild(createElement);
            for (int i = 0; i < arrayList.size(); i++) {
                a aVar = (a) arrayList.get(i);
                Element createElement2 = newDocument.createElement("artist");
                createElement2.setAttribute("id", aVar.f);
                createElement2.setAttribute(SelectCountryActivity.EXTRA_COUNTRY_NAME, aVar.e);
                createElement2.setAttribute("work", aVar.d);
                createElement2.setAttribute("isnew", aVar.f1958b ? "1" : "0");
                createElement2.setAttribute("pic", aVar.f1957a);
                createElement2.setAttribute("sale", "" + aVar.c);
                createElement.appendChild(createElement2);
            }
            Transformer newTransformer = TransformerFactory.newInstance().newTransformer();
            newTransformer.setOutputProperty("encoding", StringUtil.Encoding);
            newTransformer.setOutputProperty("indent", "yes");
            newTransformer.setOutputProperty("standalone", "yes");
            newTransformer.setOutputProperty(PushConstants.MZ_PUSH_MESSAGE_METHOD, "xml");
            newTransformer.transform(new DOMSource(newDocument.getDocumentElement()), new StreamResult(new FileOutputStream(new File(c + this.f3216b))));
        } catch (DOMException e) {
            e.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e);
        } catch (ParserConfigurationException e2) {
            e2.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e2);
        } catch (TransformerConfigurationException e3) {
            e3.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e3);
        } catch (FileNotFoundException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e4);
        } catch (TransformerException e5) {
            e5.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e5);
        } catch (Exception e6) {
            e6.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e6);
        }
    }

    public f<a> a() {
        try {
            return i.b(new FileInputStream(c + this.f3216b));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e2) {
            e2.printStackTrace();
            return null;
        }
    }
}
