package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: CollectList */
public class c implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public d f1853a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f1854b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public int f = -1;
    /* access modifiers changed from: private */
    public boolean g = false;
    /* access modifiers changed from: private */
    public ArrayList<b> h = new ArrayList<>();
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    f fVar = (f) message.obj;
                    if (fVar != null) {
                        a.a("RingList", "new obtained list data size = " + fVar.c.size());
                        if (c.this.h == null) {
                            ArrayList unused = c.this.h = fVar.c;
                        } else {
                            c.this.h.addAll(fVar.c);
                        }
                        boolean unused2 = c.this.c = fVar.d;
                        fVar.c = c.this.h;
                        if (c.this.f >= 0) {
                            fVar.h = c.this.f;
                        }
                        if (c.this.g && c.this.h.size() > 0) {
                            c.this.f1853a.a(fVar);
                            boolean unused3 = c.this.g = false;
                        }
                    }
                    boolean unused4 = c.this.d = false;
                    boolean unused5 = c.this.e = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = c.this.d = false;
                    boolean unused7 = c.this.e = true;
                    break;
            }
            final int i = message.what;
            com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1812a).a(c.this, i);
                }
            });
        }
    };

    public c(String str) {
        this.f1853a = new d("collect_" + str + ".tmp");
        this.f1854b = str;
    }

    public String a() {
        return "collect";
    }

    public void e() {
        if (this.h == null || this.h.size() == 0) {
            this.d = true;
            this.e = false;
            h.a(new Runnable() {
                public void run() {
                    c.this.h();
                }
            });
        } else if (this.c) {
            this.d = true;
            this.e = false;
            h.a(new Runnable() {
                public void run() {
                    c.this.i();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        if (!this.f1853a.a(14400000)) {
            a.a("CollectList", "CollectList: cache is available! Use Cache!");
            f<b> a2 = this.f1853a.a();
            if (!(a2 == null || a2.c == null || a2.c.size() <= 0)) {
                a.a("CollectList", "RingList: Read RingList Cache Success!");
                this.f = a2.h;
                this.i.sendMessage(this.i.obtainMessage(0, a2));
                return;
            }
        }
        a.a("CollectList", "RingList: cache is out of date or read cache failed!");
        String c2 = c(0);
        if (c2 == null) {
            a.a("CollectList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        f<b> c3 = i.c(new ByteArrayInputStream(c2.getBytes()));
        if (c3 != null) {
            a.a("CollectList", "list data size = " + c3.c.size());
            a.a("CollectList", "RingList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.g = true;
            this.f = 0;
            this.i.sendMessage(this.i.obtainMessage(0, c3));
            return;
        }
        a.a("CollectList", "RingList: parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void i() {
        int i2;
        f<b> fVar;
        a.a("CollectList", "retrieving more data, list size = " + this.h.size());
        if (this.f < 0) {
            i2 = this.h.size() / 25;
            a.a("CollectList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.f + 1;
            a.a("CollectList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String c2 = c(i2);
        if (c2 == null) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            fVar = i.c(new ByteArrayInputStream(c2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            fVar = null;
        }
        if (fVar != null) {
            a.a("RingList", "list data size = " + fVar.c.size());
            this.g = true;
            this.f = i2;
            this.i.sendMessage(this.i.obtainMessage(0, fVar));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String c(int i2) {
        return s.a("getcollects", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.f1854b);
    }

    public boolean g() {
        return this.c;
    }

    public boolean d() {
        return this.d;
    }

    /* renamed from: b */
    public b a(int i2) {
        if (i2 < 0 || i2 >= this.h.size()) {
            return null;
        }
        return this.h.get(i2);
    }

    public int c() {
        return this.h.size();
    }

    public g.a b() {
        return g.a.list_collect;
    }

    public void f() {
    }
}
