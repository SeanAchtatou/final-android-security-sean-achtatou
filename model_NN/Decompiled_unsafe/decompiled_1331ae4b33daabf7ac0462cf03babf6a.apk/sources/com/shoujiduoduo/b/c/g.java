package com.shoujiduoduo.b.c;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.os.EnvironmentCompat;
import android.text.TextUtils;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.ah;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.q;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/* compiled from: RingList */
public class g implements d {

    /* renamed from: a  reason: collision with root package name */
    private long f1864a = 14400000;

    /* renamed from: b  reason: collision with root package name */
    private int f1865b = 0;
    private boolean c = false;
    private String d = "";
    /* access modifiers changed from: private */
    public int e;
    private ah f;
    private String g = "";
    private boolean h = true;
    /* access modifiers changed from: private */
    public boolean i = true;
    /* access modifiers changed from: private */
    public boolean j = false;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public g.a n;
    /* access modifiers changed from: private */
    public h o = null;
    /* access modifiers changed from: private */
    public String p;
    private String q;
    private String r;
    /* access modifiers changed from: private */
    public String s = null;
    /* access modifiers changed from: private */
    public ArrayList<RingData> t = new ArrayList<>();
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v = -1;
    private Handler w = new Handler() {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.b.c.g.b(com.shoujiduoduo.b.c.g, boolean):boolean
         arg types: [com.shoujiduoduo.b.c.g, int]
         candidates:
          com.shoujiduoduo.b.c.g.b(com.shoujiduoduo.b.c.g, java.lang.String):java.lang.String
          com.shoujiduoduo.b.c.g.b(com.shoujiduoduo.b.c.g, boolean):boolean */
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    f fVar = (f) message.obj;
                    if (fVar != null) {
                        a.a("RingList", "new obtained list data size = " + fVar.c.size() + ", listid:" + g.this.e);
                        if (g.this.m) {
                            g.this.t.clear();
                            g.this.t.addAll(fVar.c);
                        } else if (g.this.t == null) {
                            ArrayList unused = g.this.t = fVar.c;
                        } else {
                            g.this.t.addAll(fVar.c);
                        }
                        String unused2 = g.this.p = fVar.e;
                        boolean unused3 = g.this.i = fVar.d;
                        String unused4 = g.this.s = fVar.f;
                        fVar.c = g.this.t;
                        if (g.this.v >= 0) {
                            fVar.h = g.this.v;
                        }
                        if (g.this.l && g.this.t.size() > 0) {
                            if (g.this.o != null) {
                                if (g.this.u <= 0) {
                                    g.this.o.a(fVar);
                                } else if (g.this.t.size() <= g.this.u) {
                                    g.this.o.a(fVar);
                                } else {
                                    a.a("RingList", "超过最大缓存限制，不缓存了， maxsize：" + g.this.u + ", datasize:" + g.this.t.size());
                                }
                            }
                            boolean unused5 = g.this.l = false;
                        }
                    } else {
                        g.this.t.clear();
                    }
                    boolean unused6 = g.this.j = false;
                    boolean unused7 = g.this.k = false;
                    break;
                case 1:
                case 2:
                    boolean unused8 = g.this.j = false;
                    boolean unused9 = g.this.k = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<com.shoujiduoduo.a.c.g>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.g) this.f1812a).a(g.this, i);
                }
            });
        }
    };

    public g(g.a aVar, String str, String str2) {
        this.q = str;
        this.g = str2;
        this.n = aVar;
        this.o = new h(this.q.hashCode() + ".tmp");
    }

    public g(g.a aVar) {
        this.n = aVar;
    }

    public g(g.a aVar, String str, boolean z) {
        this.n = aVar;
        this.r = str;
        this.h = z;
        if (this.h) {
            a(aVar, str, false, "");
        }
    }

    public g(g.a aVar, String str, boolean z, String str2) {
        this.n = aVar;
        this.e = q.a(str, 0);
        this.c = z;
        this.d = str2;
        a(aVar, str, z, str2);
    }

    private void a(g.a aVar, String str, boolean z, String str2) {
        if (aVar == g.a.list_ring_normal) {
            if (!TextUtils.isEmpty(str2)) {
                if (z) {
                    this.f1865b = new Random().nextInt(100) + 1;
                    this.o = new h("list_" + str + "_" + str2 + "_" + this.f1865b + ".tmp");
                    return;
                }
                this.o = new h("list_" + str + "_" + str2 + ".tmp");
            } else if (z) {
                this.f1865b = new Random().nextInt(100) + 1;
                this.o = new h("list_" + str + "_" + this.f1865b + ".tmp");
            } else {
                this.o = new h("list_" + str + ".tmp");
            }
        } else if (aVar == g.a.list_ring_cmcc) {
            this.o = new h("cmcc_cailing.tmp");
        } else if (aVar == g.a.list_ring_ctcc) {
            this.o = new h("ctcc_cailing.tmp");
        } else if (aVar == g.a.list_ring_cucc) {
            this.o = new h("cucc_cailing.tmp");
        } else if (aVar == g.a.list_ring_collect) {
            this.o = new h("collect_" + str + ".tmp");
        } else if (aVar == g.a.list_ring_artist) {
            this.o = new h("artist_" + str + ".tmp");
        } else if (aVar == g.a.list_ring_user_upload) {
            this.o = new h("user_" + str + ".tmp");
        } else {
            this.o = new h(aVar.toString() + "_" + str + ".tmp");
        }
    }

    public void a(long j2) {
        if (j2 > 0) {
            this.f1864a = j2;
        }
    }

    public void b(int i2) {
        if (i2 > 0) {
            this.u = i2;
        }
    }

    public g.a h() {
        return this.n;
    }

    public String a() {
        switch (this.n) {
            case list_ring_normal:
            case list_ring_collect:
                return "" + this.e;
            case list_ring_user_upload:
                return "user_ring_" + this.r;
            case list_ring_search:
                return "search_" + this.g;
            case list_ring_cmcc:
                return "cmcc_cailing";
            case list_ring_ctcc:
                return "ctcc_cailing";
            case list_ring_cucc:
                return "cucc_cailing";
            case sys_alarm:
                return "sys_ring_alram";
            case sys_notify:
                return "sys_ring_notify";
            case sys_ringtone:
                return "sys_ring_ringtone";
            default:
                return EnvironmentCompat.MEDIA_UNKNOWN;
        }
    }

    public void f() {
        this.j = true;
        this.k = false;
        this.m = true;
        h.a(new Runnable() {
            public void run() {
                switch (AnonymousClass7.f1870a[g.this.n.ordinal()]) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 11:
                        g.this.i();
                        return;
                    case 5:
                        g.this.k();
                        return;
                    case 6:
                        g.this.m();
                        return;
                    case 7:
                        g.this.l();
                        return;
                    case 8:
                    case 9:
                    case 10:
                        g.this.n();
                        return;
                    default:
                        return;
                }
            }
        });
    }

    public void e() {
        if (this.t == null || this.t.size() == 0) {
            this.j = true;
            this.k = false;
            this.m = false;
            h.a(new Runnable() {
                public void run() {
                    switch (AnonymousClass7.f1870a[g.this.n.ordinal()]) {
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 11:
                            g.this.i();
                            return;
                        case 5:
                            g.this.k();
                            return;
                        case 6:
                            g.this.m();
                            return;
                        case 7:
                            g.this.l();
                            return;
                        case 8:
                        case 9:
                        case 10:
                            g.this.n();
                            return;
                        default:
                            return;
                    }
                }
            });
        } else if (this.i) {
            this.j = true;
            this.k = false;
            this.m = false;
            h.a(new Runnable() {
                public void run() {
                    g.this.j();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void i() {
        if (this.o != null && !this.o.a(this.f1864a)) {
            a.a("RingList", "RingList: cache is available! Use Cache!, listid:" + this.e);
            f<RingData> a2 = this.o.a();
            if (!(a2 == null || a2.c == null || a2.c.size() <= 0)) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.v = a2.h;
                this.w.sendMessage(this.w.obtainMessage(0, a2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or read cache failed!");
        String d2 = d(0);
        if (ag.c(d2)) {
            a.a("RingList", "RingList: httpGetRingList Failed!, listid:" + a());
            this.w.sendEmptyMessage(1);
            return;
        }
        f<RingData> d3 = i.d(new ByteArrayInputStream(d2.getBytes()));
        if (d3 != null) {
            a.a("RingList", "list data size = " + d3.c.size() + ", listid:" + a());
            a.a("RingList", "RingList: send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.l = true;
            this.v = 0;
            this.w.sendMessage(this.w.obtainMessage(0, d3));
            return;
        }
        a.a("RingList", "RingList: but parse content FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.w.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void j() {
        int i2;
        f<RingData> fVar;
        a.a("RingList", "retrieving more data, list size = " + this.t.size());
        if (this.v < 0) {
            i2 = this.t.size() / 25;
            a.a("RingList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.v + 1;
            a.a("RingList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String d2 = d(i2);
        if (ag.c(d2)) {
            this.w.sendEmptyMessage(2);
            return;
        }
        try {
            fVar = i.d(new ByteArrayInputStream(d2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            fVar = null;
        }
        if (fVar != null) {
            a.a("RingList", "list data size = " + fVar.c.size());
            this.l = true;
            this.v = i2;
            this.w.sendMessage(this.w.obtainMessage(0, fVar));
            return;
        }
        this.w.sendEmptyMessage(2);
    }

    /* access modifiers changed from: private */
    public void k() {
        c.b bVar;
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.c.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1864a) && ad.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            f<RingData> a2 = this.o.a();
            if (a2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, a2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        c.b c2 = com.shoujiduoduo.util.c.b.a().c("");
        if (c2 == null) {
            a.e("RingList", "查询彩铃功能请求失败");
            this.w.sendEmptyMessage(1);
        } else if (c2.a().equals("000000")) {
            a.a("RingList", "彩铃功能已开通");
            if (this.n == g.a.list_ring_cmcc) {
                bVar = com.shoujiduoduo.util.c.b.a().f();
            } else {
                bVar = null;
            }
            if (com.shoujiduoduo.util.c.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                a.c("RingList", "main activity is destroyed 2");
            } else if (bVar == null) {
                a.c("RingList", "查询失败，cRsp == null");
                this.w.sendEmptyMessage(1);
            } else if (bVar.a() != null && bVar.a().equals(GetUserInfoRsp.NON_MEM_ERROR_CODE)) {
                a.a("RingList", "查询失败，用户非彩铃用户，提示开通咪咕特级会员");
                com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                    public void a() {
                        ((com.shoujiduoduo.a.c.c) this.f1812a).a(false, f.b.cm);
                    }
                });
                this.w.sendEmptyMessage(1);
            } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.z))) {
                this.w.sendEmptyMessage(1);
                a.c("RingList", "查询失败，res：" + bVar.b());
            } else {
                a.a("RingList", "查询成功");
                List<c.ae> d2 = ((c.z) bVar).d();
                if (d2 == null || d2.size() == 0) {
                    a.a("RingList", "userToneInfo == null");
                    this.w.sendMessage(this.w.obtainMessage(0, null));
                    return;
                }
                com.shoujiduoduo.base.bean.f fVar = new com.shoujiduoduo.base.bean.f();
                ArrayList<T> arrayList = new ArrayList<>();
                for (int i2 = 0; i2 < d2.size(); i2++) {
                    RingData ringData = new RingData();
                    ringData.e = d2.get(i2).c();
                    ringData.f = d2.get(i2).d();
                    if (this.n == g.a.list_ring_cmcc) {
                        ringData.r = q.a(d2.get(i2).a(), Downloads.STATUS_SUCCESS);
                        ringData.p = d2.get(i2).b();
                        a.a("RingList", "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.p);
                        String e2 = d2.get(i2).e();
                        try {
                            if (e2.indexOf(" ") > 0) {
                                ringData.q = e2.substring(0, e2.indexOf(" "));
                            } else {
                                ringData.q = e2;
                            }
                        } catch (Exception e3) {
                            ringData.q = "";
                            e3.printStackTrace();
                        }
                        ringData.s = 0;
                        if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                            a.c("RingList", "main activity is destroyed 3");
                            return;
                        }
                        ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.p);
                    }
                    arrayList.add(ringData);
                }
                fVar.c = arrayList;
                fVar.d = false;
                this.l = true;
                ad.b((Context) null, "NeedUpdateCaiLingLib", 0);
                this.w.sendMessage(this.w.obtainMessage(0, fVar));
            }
        } else {
            com.shoujiduoduo.a.a.c.a().b(b.OBSERVER_CAILING, new c.a<com.shoujiduoduo.a.c.c>() {
                public void a() {
                    ((com.shoujiduoduo.a.c.c) this.f1812a).a(false, f.b.cm);
                }
            });
            this.w.sendEmptyMessage(1);
            a.e("RingList", "彩铃功能尚未开通");
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.e.a.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1864a) && ad.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            com.shoujiduoduo.base.bean.f<RingData> a2 = this.o.a();
            if (a2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, a2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        c.b d2 = com.shoujiduoduo.util.e.a.a().d();
        if (d2 == null) {
            a.c("RingList", "查询失败，cRsp == null");
            this.w.sendEmptyMessage(1);
        } else if ((d2 instanceof c.u) && d2.a().equals("000000")) {
            a.a("RingList", "查询成功");
            c.ab[] abVarArr = ((c.u) d2).d;
            if (abVarArr == null || abVarArr.length == 0) {
                a.a("RingList", "userToneInfo == null");
                this.w.sendMessage(this.w.obtainMessage(0, null));
                return;
            }
            com.shoujiduoduo.base.bean.f fVar = new com.shoujiduoduo.base.bean.f();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < abVarArr.length; i2++) {
                RingData ringData = new RingData();
                ringData.e = abVarArr[i2].f3047b;
                ringData.f = abVarArr[i2].d;
                if (this.n == g.a.list_ring_cucc) {
                    ringData.C = abVarArr[i2].f3046a;
                    a.a("RingList", "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.C);
                    String str = abVarArr[i2].g;
                    try {
                        if (str.indexOf(" ") > 0) {
                            ringData.D = str.substring(0, str.indexOf(" "));
                        } else {
                            ringData.D = str;
                        }
                    } catch (Exception e2) {
                        ringData.D = "";
                        e2.printStackTrace();
                    }
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c("RingList", "main activity is destroyed 3");
                        return;
                    }
                    ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.C);
                }
                arrayList.add(ringData);
            }
            fVar.c = arrayList;
            fVar.d = false;
            this.l = true;
            ad.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.w.sendMessage(this.w.obtainMessage(0, fVar));
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        c.b bVar;
        a.a("RingList", "彩铃，查询个人铃音库, type:" + this.n.toString());
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed");
            return;
        }
        if (this.o != null && !this.o.a(this.f1864a) && ad.a((Context) null, "NeedUpdateCaiLingLib", 1) == 0) {
            a.a("RingList", "RingList: cache is available! Use Cache!");
            com.shoujiduoduo.base.bean.f<RingData> a2 = this.o.a();
            if (a2 != null) {
                a.a("RingList", "RingList: Read RingList Cache Success!");
                this.w.sendMessage(this.w.obtainMessage(0, a2));
                return;
            }
        }
        a.a("RingList", "RingList: cache is out of date or cache failed!");
        String a3 = ad.a(RingDDApp.c(), "pref_phone_num", "");
        if (this.n == g.a.list_ring_ctcc) {
            bVar = com.shoujiduoduo.util.d.b.a().c(a3);
        } else {
            bVar = null;
        }
        if (com.shoujiduoduo.util.d.b.a() == null || com.shoujiduoduo.util.c.a.a((Context) null) == null) {
            a.c("RingList", "main activity is destroyed 2");
        } else if (bVar == null) {
            a.c("RingList", "查询失败，cRsp == null");
            this.w.sendEmptyMessage(1);
        } else if (bVar.a() == null || ((!bVar.a().equals("000000") && !bVar.a().equals("0000")) || !(bVar instanceof c.z))) {
            this.w.sendEmptyMessage(1);
            a.c("RingList", "查询失败，res：" + bVar.b());
        } else {
            a.a("RingList", "查询成功");
            List<c.ae> d2 = ((c.z) bVar).d();
            if (d2 == null || d2.size() == 0) {
                a.a("RingList", "userToneInfo == null");
                this.w.sendMessage(this.w.obtainMessage(0, null));
                return;
            }
            com.shoujiduoduo.base.bean.f fVar = new com.shoujiduoduo.base.bean.f();
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i2 = 0; i2 < d2.size(); i2++) {
                RingData ringData = new RingData();
                ringData.e = d2.get(i2).c();
                ringData.f = d2.get(i2).d();
                if (this.n == g.a.list_ring_ctcc) {
                    ringData.u = d2.get(i2).b();
                    a.a("RingList", "用户铃音库：name:" + ringData.e + " 彩铃id：" + ringData.u);
                    String e2 = d2.get(i2).e();
                    try {
                        if (e2.indexOf(" ") > 0) {
                            ringData.v = e2.substring(0, e2.indexOf(" "));
                        } else {
                            ringData.v = e2;
                        }
                    } catch (Exception e3) {
                        ringData.v = "";
                        e3.printStackTrace();
                    }
                    ringData.x = 0;
                    if (com.shoujiduoduo.util.c.a.a((Context) null) == null) {
                        a.c("RingList", "main activity is destroyed 3");
                        return;
                    }
                    ringData.g = com.shoujiduoduo.util.c.a.a((Context) null).a(ringData.u);
                }
                arrayList.add(ringData);
            }
            fVar.c = arrayList;
            fVar.d = false;
            this.l = true;
            ad.b((Context) null, "NeedUpdateCaiLingLib", 0);
            this.w.sendMessage(this.w.obtainMessage(0, fVar));
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        int i2;
        switch (this.n) {
            case sys_alarm:
                i2 = 4;
                break;
            case sys_notify:
                i2 = 2;
                break;
            case sys_ringtone:
                i2 = 1;
                break;
            default:
                i2 = 0;
                break;
        }
        if (this.f == null) {
            this.f = new ah(RingDDApp.c());
        }
        List<ah.a> a2 = this.f.a(i2, true);
        if (a2 == null) {
            this.w.sendEmptyMessage(1);
            a.c("RingList", "get system ring error");
            return;
        }
        ArrayList<T> arrayList = new ArrayList<>();
        for (ah.a next : a2) {
            RingData ringData = new RingData();
            ringData.o = next.f3002a;
            ringData.e = next.f3003b;
            ringData.l = next.c / 1000 == 0 ? 1 : next.c / 1000;
            arrayList.add(ringData);
        }
        if (arrayList.size() > 0) {
            com.shoujiduoduo.base.bean.f fVar = new com.shoujiduoduo.base.bean.f();
            fVar.c = arrayList;
            this.w.sendMessage(this.w.obtainMessage(0, fVar));
            return;
        }
        this.w.sendEmptyMessage(1);
        a.c("RingList", "get system ring error");
    }

    private String d(int i2) {
        switch (this.n) {
            case list_ring_normal:
                String str = "&listid=" + this.e + "&page=" + i2 + "&pagesize=" + 25;
                if (!TextUtils.isEmpty(this.d)) {
                    if (this.c) {
                        str = str + "&area=" + this.d + "&ranid=" + this.f1865b;
                    } else {
                        str = str + "&area=" + this.d;
                    }
                } else if (this.c) {
                    str = str + "&ranid=" + this.f1865b;
                }
                return s.d(str);
            case list_ring_collect:
                return s.a("getlist&iscol=1", "&page=" + i2 + "&pagesize=" + 25 + "&listid=" + this.e);
            case list_ring_user_upload:
                return s.a("getuserringlist", "&uid=" + com.shoujiduoduo.a.b.b.g().c().a() + "&tuid=" + this.r + "&page=" + i2 + "&pagesize=" + 25);
            case list_ring_search:
                return s.a(this.q, this.g, i2, 25);
            case list_ring_cmcc:
            case list_ring_ctcc:
            case list_ring_cucc:
            case sys_alarm:
            case sys_notify:
            case sys_ringtone:
            default:
                String str2 = "&listid=" + this.e + "&page=" + i2 + "&pagesize=" + 25;
                if (this.c) {
                    str2 = str2 + "&ranid=" + this.f1865b;
                }
                return s.d(str2);
            case list_ring_artist:
                return s.a("getlist", "&page=" + i2 + "&pagesize=" + 25 + "&artistid=" + this.e);
        }
    }

    public boolean g() {
        return this.i;
    }

    public boolean d() {
        return this.j;
    }

    /* renamed from: c */
    public RingData a(int i2) {
        if (i2 < 0 || i2 >= this.t.size()) {
            return null;
        }
        return this.t.get(i2);
    }

    public int c() {
        return this.t.size();
    }

    public g.a b() {
        return this.n;
    }
}
