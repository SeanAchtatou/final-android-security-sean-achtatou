package com.shoujiduoduo.b.c;

import android.os.Handler;
import android.os.Message;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.g;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.d;
import com.shoujiduoduo.base.bean.f;
import com.shoujiduoduo.base.bean.g;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.i;
import com.shoujiduoduo.util.s;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;

/* compiled from: UserList */
public class k implements d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f1876a = true;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f1877b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    /* access modifiers changed from: private */
    public int d = -1;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public ArrayList<UserData> f = new ArrayList<>();
    private a g;
    private String h;
    private Handler i = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case 0:
                    f fVar = (f) message.obj;
                    if (fVar != null) {
                        com.shoujiduoduo.base.a.a.a("RingList", "new obtained list data size = " + fVar.c.size());
                        if (k.this.f == null) {
                            ArrayList unused = k.this.f = fVar.c;
                        } else {
                            k.this.f.addAll(fVar.c);
                        }
                        boolean unused2 = k.this.f1876a = fVar.d;
                        fVar.c = k.this.f;
                        if (k.this.d >= 0) {
                            fVar.h = k.this.d;
                        }
                        if (k.this.e && k.this.f.size() > 0) {
                            boolean unused3 = k.this.e = false;
                        }
                    }
                    boolean unused4 = k.this.f1877b = false;
                    boolean unused5 = k.this.c = false;
                    break;
                case 1:
                case 2:
                    boolean unused6 = k.this.f1877b = false;
                    boolean unused7 = k.this.c = true;
                    break;
            }
            final int i = message.what;
            c.a().a(b.OBSERVER_LIST_DATA, new c.a<g>() {
                public void a() {
                    ((g) this.f1812a).a(k.this, i);
                }
            });
        }
    };

    /* compiled from: UserList */
    public enum a {
        fans,
        follow
    }

    public k(a aVar, String str) {
        this.g = aVar;
        this.h = str;
    }

    public Object a(int i2) {
        if (i2 >= this.f.size() || i2 < 0) {
            return null;
        }
        return this.f.get(i2);
    }

    public String a() {
        return "userlist";
    }

    public g.a b() {
        return g.a.list_user;
    }

    public int c() {
        return this.f.size();
    }

    public boolean d() {
        return this.f1877b;
    }

    public void e() {
        if (this.f == null || this.f.size() == 0) {
            this.f1877b = true;
            this.c = false;
            h.a(new Runnable() {
                public void run() {
                    k.this.h();
                }
            });
        } else if (this.f1876a) {
            this.f1877b = true;
            this.c = false;
            h.a(new Runnable() {
                public void run() {
                    k.this.i();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        com.shoujiduoduo.base.a.a.a("UserList", "getListData");
        String b2 = b(0);
        if (ag.c(b2)) {
            com.shoujiduoduo.base.a.a.a("UserList", "RingList: httpGetRingList Failed!");
            this.i.sendEmptyMessage(1);
            return;
        }
        f<UserData> a2 = i.a(new ByteArrayInputStream(b2.getBytes()));
        if (a2 != null) {
            com.shoujiduoduo.base.a.a.a("UserList", "list data size = " + a2.c.size());
            com.shoujiduoduo.base.a.a.a("UserList", "UserList: Read from network Success! send MESSAGE_SUCCESS_RETRIEVE_DATA");
            this.e = true;
            this.d = 0;
            this.i.sendMessage(this.i.obtainMessage(0, a2));
            return;
        }
        com.shoujiduoduo.base.a.a.a("UserList", "RingList: but parse FAILED! send MESSAGE_FAIL_RETRIEVE_DATA");
        this.i.sendEmptyMessage(1);
    }

    /* access modifiers changed from: private */
    public void i() {
        int i2;
        f<UserData> fVar;
        com.shoujiduoduo.base.a.a.a("UserList", "retrieving more data, list size = " + this.f.size());
        if (this.d < 0) {
            i2 = this.f.size() / 25;
            com.shoujiduoduo.base.a.a.a("UserList", "没有cache current page 记录，通过list size 计算页数， 下一页，page：" + i2);
        } else {
            i2 = this.d + 1;
            com.shoujiduoduo.base.a.a.a("UserList", "有 cache current page 记录，下一页，page：" + i2);
        }
        String b2 = b(i2);
        if (ag.c(b2)) {
            this.i.sendEmptyMessage(2);
            return;
        }
        try {
            fVar = i.a(new ByteArrayInputStream(b2.getBytes()));
        } catch (ArrayIndexOutOfBoundsException e2) {
            fVar = null;
        }
        if (fVar != null) {
            com.shoujiduoduo.base.a.a.a("RingList", "list data size = " + fVar.c.size());
            this.e = true;
            this.d = i2;
            this.i.sendMessage(this.i.obtainMessage(0, fVar));
            return;
        }
        this.i.sendEmptyMessage(2);
    }

    private String b(int i2) {
        return s.a(this.g == a.fans ? "myfollower" : "myfollowing", "&page=" + i2 + "&pagesize=" + 25 + "&uid=" + com.shoujiduoduo.a.b.b.g().c().a() + "&tuid=" + this.h + "&pagesupport=1");
    }

    public void f() {
    }

    public boolean g() {
        return this.f1876a;
    }
}
