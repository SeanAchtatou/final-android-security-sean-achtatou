package com.shoujiduoduo.b.a;

import com.shoujiduoduo.b.a.b;
import com.shoujiduoduo.b.a.e;
import com.shoujiduoduo.b.a.g;
import java.util.ArrayList;

/* compiled from: AdMgrImpl */
public class a implements f {

    /* renamed from: a  reason: collision with root package name */
    private b f1820a = new b();

    /* renamed from: b  reason: collision with root package name */
    private g f1821b = new g();
    private e c = new e();

    public void a() {
        this.f1820a.a();
        this.f1821b.a();
        this.c.a();
    }

    public void b() {
        this.f1820a.c();
        this.f1821b.b();
        this.c.c();
    }

    public boolean c() {
        return this.f1820a.b();
    }

    public ArrayList<b.a> d() {
        if (this.f1820a.b()) {
            return this.f1820a.d();
        }
        return null;
    }

    public int e() {
        if (this.f1820a.b()) {
            return this.f1820a.e();
        }
        return 0;
    }

    public boolean f() {
        return this.f1821b.c();
    }

    public g.a g() {
        if (this.f1821b.c()) {
            return this.f1821b.d();
        }
        return null;
    }

    public void h() {
        this.c.b();
    }

    public e.a i() {
        return this.c.d();
    }
}
