package com.shoujiduoduo.b.a;

import android.view.View;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.INativeErrorCode;
import com.duoduo.mobads.INativeResponse;
import com.duoduo.mobads.gdt.IGdtNativeAd;
import com.duoduo.mobads.gdt.IGdtNativeAdDataRef;
import com.duoduo.mobads.gdt.IGdtNativeAdListener;
import com.qhad.ads.sdk.adcore.Qhad;
import com.qhad.ads.sdk.interfaces.IQhNativeAd;
import com.qhad.ads.sdk.interfaces.IQhNativeAdListener;
import com.qhad.ads.sdk.interfaces.IQhNativeAdLoader;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.k;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.q;
import com.tencent.open.SocialConstants;
import com.umeng.analytics.b;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

/* compiled from: FeedAdData */
public class e {
    private static int d = 1800000;
    private static int e = 1800;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f1830a;

    /* renamed from: b  reason: collision with root package name */
    private int f1831b;
    private int c;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public LinkedList<a> g = new LinkedList<>();
    /* access modifiers changed from: private */
    public LinkedList<a> h = new LinkedList<>();
    /* access modifiers changed from: private */
    public LinkedList<a> i = new LinkedList<>();
    /* access modifiers changed from: private */
    public final byte[] j = new byte[0];
    /* access modifiers changed from: private */
    public Object k;
    /* access modifiers changed from: private */
    public Object l;
    /* access modifiers changed from: private */
    public Object m;
    private Timer n;
    private int o;
    /* access modifiers changed from: private */
    public volatile boolean p;
    /* access modifiers changed from: private */
    public boolean q;
    private int r;
    private int s;
    private int t;

    /* compiled from: FeedAdData */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Object f1835a;

        /* renamed from: b  reason: collision with root package name */
        private long f1836b;
        private long c;
        private int d;
        private int e;
        private int f;

        public a() {
        }

        public a(Object obj, long j, long j2, int i) {
            this.f1835a = obj;
            this.f1836b = j;
            this.c = j2;
            this.d = i;
        }

        public int a() {
            return this.d;
        }

        public boolean b() {
            boolean z;
            switch (this.d) {
                case 1:
                    try {
                        return ((INativeResponse) this.f1835a).isAdAvailable(RingDDApp.c());
                    } catch (Exception e2) {
                        b.a(RingDDApp.c(), e2);
                        e2.printStackTrace();
                        return false;
                    }
                case 2:
                case 4:
                    if (System.currentTimeMillis() - this.f1836b < this.c) {
                        z = true;
                    } else {
                        z = false;
                    }
                    return z;
                case 3:
                default:
                    return false;
            }
        }

        public void c() {
            this.e++;
        }

        public void a(int i) {
            this.f = i;
        }

        public boolean d() {
            return this.e >= this.f;
        }

        public String e() {
            switch (this.d) {
                case 1:
                    return ((INativeResponse) this.f1835a).getTitle();
                case 2:
                    return ((IQhNativeAd) this.f1835a).getContent().optString("title", "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((IGdtNativeAdDataRef) this.f1835a).getTitle();
            }
        }

        public String f() {
            switch (this.d) {
                case 1:
                    return ((INativeResponse) this.f1835a).getDesc();
                case 2:
                    return ((IQhNativeAd) this.f1835a).getContent().optString(SocialConstants.PARAM_APP_DESC, "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((IGdtNativeAdDataRef) this.f1835a).getDesc();
            }
        }

        public String g() {
            switch (this.d) {
                case 1:
                    return ((INativeResponse) this.f1835a).getIconUrl();
                case 2:
                    return ((IQhNativeAd) this.f1835a).getContent().optString("contentimg", "");
                case 3:
                default:
                    return "";
                case 4:
                    return ((IGdtNativeAdDataRef) this.f1835a).getIconUrl();
            }
        }

        public void a(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "duomob_feed_ad_show");
                    ((INativeResponse) this.f1835a).recordImpression(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_show");
                    ((IQhNativeAd) this.f1835a).onAdShowed();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_show");
                    ((IGdtNativeAdDataRef) this.f1835a).onExposured(view);
                    return;
            }
        }

        public void b(View view) {
            switch (this.d) {
                case 1:
                    b.b(RingDDApp.c(), "duomob_feed_ad_click");
                    ((INativeResponse) this.f1835a).handleClick(view);
                    return;
                case 2:
                    b.b(RingDDApp.c(), "qihu_feed_ad_click");
                    ((IQhNativeAd) this.f1835a).onAdClicked();
                    return;
                case 3:
                default:
                    return;
                case 4:
                    b.b(RingDDApp.c(), "gdt_feed_ad_click");
                    ((IGdtNativeAdDataRef) this.f1835a).onClicked(view);
                    return;
            }
        }
    }

    public void a() {
        int i2 = 1;
        String a2 = ab.a().a("feed_ad_v1_type");
        if (a2.equalsIgnoreCase("baidu")) {
            this.o = 1;
        } else if (a2.equals("gdt")) {
            this.o = 4;
        } else if (a2.equals("mix")) {
            this.o = 99;
        } else {
            com.shoujiduoduo.base.a.a.c("FeedAdData", "not support ad type, use default value baidu");
            this.o = 1;
        }
        this.f1831b = ab.a().a("feed_ad_v1_percent_baidu", 1);
        this.c = ab.a().a("feed_ad_v1_percent_gdt", 1);
        if (this.f1831b < this.c) {
            i2 = 4;
        }
        this.t = i2;
        com.shoujiduoduo.base.a.a.a("FeedAdData", "ad type is :" + a2 + ", reuseLimit is: " + ab.a().a("feed_ad_v1_show_limit", 3));
        com.shoujiduoduo.base.a.a.a("FeedAdData", "baidu percent:" + this.f1831b);
        com.shoujiduoduo.base.a.a.a("FeedAdData", "gdt percent:" + this.c);
    }

    public void b() {
        if (!com.shoujiduoduo.util.a.f()) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "not support feed ad");
        } else if (this.o == 2) {
            g();
        } else if (this.o == 1) {
            e();
        } else if (this.o == 4) {
            f();
        } else if (this.o == 99) {
            e();
            f();
        }
    }

    private void e() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] init Duomob Baidu Feed ad");
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "baidu_feed_ad_valid_time");
        if (ag.c(a2)) {
            this.f = d;
        } else {
            this.f = q.a(a2, e) * 1000;
        }
        String a3 = com.umeng.a.a.a().a(RingDDApp.c(), "baidu_feed_ad_list_capacity");
        if (ag.c(a3)) {
            this.f1830a = 20;
        } else {
            this.f1830a = q.a(a3, 20);
        }
        this.m = DuoMobAdUtils.Ins.BaiduIns.getNativeAdIns(RingToneDuoduoActivity.a(), "d7d3402a", "2652329", new IBaiduNativeNetworkListener() {
            public void onNativeLoad(List<INativeResponse> list) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] Duomob feed ad load success");
                if (list == null || list.size() <= 0) {
                    boolean unused = e.this.q = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] onNativeAdLoadSucceeded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] onNativeAdLoadSucceeded, size:" + list.size());
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNativeLoad");
                b.a(RingDDApp.c(), "duomob_list_feed_ad", hashMap);
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    for (INativeResponse aVar : list) {
                        a aVar2 = new a(aVar, currentTimeMillis, (long) e.this.f, 1);
                        aVar2.a(e.this.i());
                        e.this.g.add(aVar2);
                    }
                }
                if (!e.this.p) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1812a).a();
                        }
                    });
                    boolean unused2 = e.this.p = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] ad list size:" + e.this.g.size());
                if (e.this.g.size() < e.this.f1830a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] ad list size is < " + e.this.f1830a + ", so fetch more data");
                    boolean unused3 = e.this.q = true;
                    ((IBaiduNative) e.this.m).makeRequest();
                    return;
                }
                boolean unused4 = e.this.q = false;
            }

            public void onNativeFail(INativeErrorCode iNativeErrorCode) {
                if (iNativeErrorCode != null) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("status", "onNativeFail");
                    hashMap.put("errCode", "" + iNativeErrorCode.getErrorCode());
                    b.a(RingDDApp.c(), "duomob_list_feed_ad", hashMap);
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] Duomob feed ad load failed, errcode:" + iNativeErrorCode.getErrorCode());
                }
            }
        });
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] Duomob feed ad confirmdown:" + "true".equals(com.umeng.a.a.a().a(RingDDApp.c(), "feed_ad_confirm_down")));
        this.q = true;
        if (this.m != null) {
            ((IBaiduNative) this.m).makeRequest();
        } else {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "[BAIDU] mBaiduAdLoader is null");
        }
    }

    private void f() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] init Duomob Gdt Feed ad");
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "gdt_feed_ad_valid_time");
        if (ag.c(a2)) {
            this.f = d;
        } else {
            this.f = q.a(a2, e) * 1000;
        }
        String a3 = com.umeng.a.a.a().a(RingDDApp.c(), "gdt_feed_ad_list_capacity");
        if (ag.c(a3)) {
            this.f1830a = 20;
        } else {
            this.f1830a = q.a(a3, 20);
        }
        this.k = DuoMobAdUtils.Ins.GdtIns.getNativeAd(RingToneDuoduoActivity.a(), "1105772024", "4090414688268566", new IGdtNativeAdListener() {
            public void onNoAD(int i) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onNoAd");
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNoAD");
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
            }

            public void onADStatusChanged(IGdtNativeAdDataRef iGdtNativeAdDataRef) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "onADStatusChanged");
            }

            public void onADLoaded(List<IGdtNativeAdDataRef> list) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] gdt feed ad load success");
                if (list == null || list.size() <= 0) {
                    boolean unused = e.this.q = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADLoaded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADLoaded, size:" + list.size());
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onNativeLoad");
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    for (IGdtNativeAdDataRef aVar : list) {
                        a aVar2 = new a(aVar, currentTimeMillis, (long) e.this.f, 4);
                        aVar2.a(e.this.i());
                        e.this.h.add(aVar2);
                    }
                }
                if (!e.this.p) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1812a).a();
                        }
                    });
                    boolean unused2 = e.this.p = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] ad list size:" + e.this.h.size());
                if (e.this.h.size() < e.this.f1830a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] ad list size is < " + e.this.f1830a + ", so fetch more data");
                    boolean unused3 = e.this.q = true;
                    ((IGdtNativeAd) e.this.k).loadAD(10);
                    return;
                }
                boolean unused4 = e.this.q = false;
            }

            public void onADError(IGdtNativeAdDataRef iGdtNativeAdDataRef, int i) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "[GDT] onADError:" + i);
                HashMap hashMap = new HashMap();
                hashMap.put("status", "onADError");
                hashMap.put("errCode", "" + i);
                b.a(RingDDApp.c(), "gdt_feed_ad", hashMap);
            }
        });
        this.q = true;
        if (this.k != null) {
            ((IGdtNativeAd) this.k).loadAD(10);
        }
    }

    private void g() {
        com.shoujiduoduo.base.a.a.a("FeedAdData", "init 360 Feed ad");
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "360_feed_ad_valid_time");
        if (ag.c(a2)) {
            this.f = d;
        } else {
            this.f = q.a(a2, e) * 1000;
        }
        String a3 = com.umeng.a.a.a().a(RingDDApp.c(), "360_feed_ad_list_capacity");
        if (ag.c(a3)) {
            this.f1830a = 20;
        } else {
            this.f1830a = q.a(a3, 20);
        }
        this.l = Qhad.initNativeAdLoader(RingToneDuoduoActivity.a(), "uaaGFTRq7O", new IQhNativeAdListener() {
            public void onNativeAdLoadSucceeded(ArrayList<IQhNativeAd> arrayList) {
                if (arrayList == null || arrayList.size() <= 0) {
                    boolean unused = e.this.q = false;
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadSucceeded, size is 0");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadSucceeded, size:" + arrayList.size());
                long currentTimeMillis = System.currentTimeMillis();
                synchronized (e.this.j) {
                    Iterator<IQhNativeAd> it = arrayList.iterator();
                    while (it.hasNext()) {
                        a aVar = new a(it.next(), currentTimeMillis, (long) e.this.f, 2);
                        aVar.a(e.this.i());
                        e.this.i.add(aVar);
                    }
                }
                if (!e.this.p) {
                    c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_FEED_AD, new c.a<k>() {
                        public void a() {
                            ((k) this.f1812a).a();
                        }
                    });
                    boolean unused2 = e.this.p = true;
                }
                com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size:" + e.this.i.size());
                if (e.this.i.size() < e.this.f1830a) {
                    com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size is < " + e.this.f1830a + ", so fetch more data");
                    boolean unused3 = e.this.q = true;
                    ((IQhNativeAdLoader) e.this.l).loadAds();
                    return;
                }
                boolean unused4 = e.this.q = false;
            }

            public void onNativeAdLoadFailed() {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "onNativeAdLoadFailed");
            }
        }, false);
        if (this.l != null) {
            this.q = true;
            ((IQhNativeAdLoader) this.l).loadAds();
        }
    }

    public void c() {
        if (this.n != null) {
            this.n.cancel();
        }
    }

    private void h() {
        switch (this.o) {
            case 1:
                if (this.m != null && !this.q) {
                    ((IBaiduNative) this.m).makeRequest();
                    return;
                }
                return;
            case 2:
                if (this.l != null && !this.q) {
                    ((IQhNativeAdLoader) this.l).loadAds();
                    return;
                }
                return;
            case 4:
                if (this.k != null && !this.q) {
                    ((IGdtNativeAd) this.k).loadAD(10);
                    return;
                }
                return;
            case 99:
                if (this.k != null && !this.q) {
                    ((IGdtNativeAd) this.k).loadAD(10);
                }
                if (this.m != null && !this.q) {
                    ((IBaiduNative) this.m).makeRequest();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int i() {
        return ab.a().a("feed_ad_v1_show_limit", 3);
    }

    public a d() {
        LinkedList<a> linkedList;
        com.shoujiduoduo.base.a.a.a("FeedAdData", "getNextAdItem");
        if (this.o != 99) {
            if (this.o == 1) {
                linkedList = this.g;
            } else if (this.o == 4) {
                linkedList = this.h;
            } else {
                com.shoujiduoduo.base.a.a.c("FeedAdData", "not supported ad type");
                return null;
            }
            return a(linkedList);
        } else if (this.t == 1) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current is baidu");
            this.r++;
            if (this.f1831b > 0 && this.c > 0 && this.r % this.f1831b == 0) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "switch to gdt");
                this.t = 4;
            }
            return a(this.g);
        } else {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current is gdt");
            this.s++;
            if (this.c > 0 && this.f1831b > 0 && this.s % this.c == 0) {
                com.shoujiduoduo.base.a.a.a("FeedAdData", "switch to baidu");
                this.t = 1;
            }
            return a(this.h);
        }
    }

    private a a(LinkedList<a> linkedList) {
        a aVar;
        synchronized (this.j) {
            while (true) {
                if (linkedList.size() <= 0) {
                    aVar = null;
                    break;
                }
                aVar = linkedList.poll();
                if (aVar.b()) {
                    break;
                }
            }
        }
        com.shoujiduoduo.base.a.a.a("FeedAdData", "ad list size is :" + linkedList.size());
        if (linkedList.size() < this.f1830a / 2) {
            com.shoujiduoduo.base.a.a.a("FeedAdData", "current list size < " + (this.f1830a / 2) + ", so fetch more data");
            h();
        }
        if (aVar == null) {
            com.shoujiduoduo.base.a.a.e("FeedAdData", "no valid ad, return null");
        }
        return aVar;
    }
}
