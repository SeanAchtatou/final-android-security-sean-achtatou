package com.shoujiduoduo.util.d;

import cn.banshenggua.aichang.utils.StringUtil;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.ag;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;

/* compiled from: HttpUtils */
public class c {
    public static String a(List<NameValuePair> list, String str, String str2) {
        HttpEntity entity;
        InputStream content;
        a.a("HttpUtils", "httpGetTool:methodName:" + str);
        if (list != null) {
            for (NameValuePair next : list) {
                a.a("HttpUtils", "name:" + next.getName() + " , value:" + next.getValue());
            }
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        StringBuilder sb = new StringBuilder();
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "ctcc_cailing_host");
        if (ag.b(a2)) {
            a2 = "http://api.118100.cn/openapi/services/v2";
        }
        sb.append(a2 + str + str2);
        if (list != null && list.size() > 0) {
            sb.append("?");
            for (int i = 0; i < list.size(); i++) {
                try {
                    sb.append(list.get(i).getName()).append("=").append(URLEncoder.encode(list.get(i).getValue(), StringUtil.Encoding)).append("&");
                } catch (UnsupportedEncodingException e) {
                }
            }
            if (sb.toString().endsWith("&")) {
                sb.deleteCharAt(sb.lastIndexOf("&"));
            }
        }
        HttpGet httpGet = new HttpGet(sb.toString());
        a.a("HttpUtils", "url=" + sb.toString());
        a(httpGet, list);
        try {
            HttpResponse execute = defaultHttpClient.execute(httpGet);
            a.a("HttpUtils", "status code:" + execute.getStatusLine().getStatusCode());
            if (!(execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null || (content = entity.getContent()) == null)) {
                byte[] bArr = new byte[2048];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bufferedInputStream.close();
                content.close();
                a.a("HttpUtils", "httpGet: content = " + byteArrayOutputStream.toString());
                if (byteArrayOutputStream.toByteArray() != null) {
                    return byteArrayOutputStream.toString(StringUtil.Encoding);
                }
                return "OK";
            }
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
            a.c("http", "httpGet: ClientProtocolException catched!");
        } catch (IOException e3) {
            e3.printStackTrace();
            a.c("http", "httpGet: IOException catched!");
        }
        return null;
    }

    public static String b(List<NameValuePair> list, String str, String str2) {
        HttpEntity entity;
        InputStream content;
        a.a("HttpUtils", "httpPostTool:" + str);
        for (NameValuePair next : list) {
            a.a("HttpUtils", "name:" + next.getName() + " , value:" + next.getValue());
        }
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "ctcc_cailing_host");
        if (ag.b(a2)) {
            a2 = "http://api.118100.cn/openapi/services/v2";
        }
        String str3 = a2 + str + str2;
        HttpPost httpPost = new HttpPost(str3);
        a.a("HttpUtils", "url=" + str3);
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        a(httpPost, list);
        try {
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            a.a("HttpUtils", "status code:" + execute.getStatusLine().getStatusCode());
            if (!(execute.getStatusLine().getStatusCode() != 200 || (entity = execute.getEntity()) == null || (content = entity.getContent()) == null)) {
                byte[] bArr = new byte[2048];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(content);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                while (true) {
                    int read = bufferedInputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    byteArrayOutputStream.write(bArr, 0, read);
                }
                bufferedInputStream.close();
                content.close();
                a.a("HttpUtils", "httpPost: content = " + byteArrayOutputStream.toString());
                if (byteArrayOutputStream.toByteArray() != null) {
                    return byteArrayOutputStream.toString(StringUtil.Encoding);
                }
                return "OK";
            }
        } catch (ClientProtocolException e2) {
            e2.printStackTrace();
            a.c("http", "httpPost: ClientProtocolException catched!");
        } catch (IOException e3) {
            e3.printStackTrace();
            a.c("http", "httpPost: IOException catched!");
        }
        return null;
    }

    private static void a(AbstractHttpMessage abstractHttpMessage, List<NameValuePair> list) {
        abstractHttpMessage.addHeader("auth-deviceid", "1000010404421");
        abstractHttpMessage.addHeader("auth-channelid", "5297");
        String format = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
        abstractHttpMessage.addHeader("auth-timestamp", format);
        abstractHttpMessage.addHeader("auth-signature-method", "HmacSHA1");
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("1000010404421").append("&");
        stringBuffer.append("5297").append("&");
        stringBuffer.append(format).append("&");
        if (list != null && list.size() > 0) {
            for (NameValuePair value : list) {
                stringBuffer.append(value.getValue()).append("&");
            }
        }
        abstractHttpMessage.addHeader("auth-signature", a.a("x4lRWHcgRqfH", stringBuffer.substring(0, stringBuffer.length() - 1)));
    }
}
