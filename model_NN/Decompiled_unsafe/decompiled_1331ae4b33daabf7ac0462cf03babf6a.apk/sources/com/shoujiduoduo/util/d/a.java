package com.shoujiduoduo.util.d;

import cn.banshenggua.aichang.utils.StringUtil;
import com.shoujiduoduo.util.b;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/* compiled from: AuthUtils */
public class a {
    public static String a(String str, String str2) {
        byte[] bArr;
        try {
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(new SecretKeySpec(str.getBytes(StringUtil.Encoding), "HmacSHA1"));
            bArr = instance.doFinal(str2.getBytes(StringUtil.Encoding));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            bArr = null;
        } catch (InvalidKeyException e2) {
            e2.printStackTrace();
            bArr = null;
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
            bArr = null;
        } catch (UnsupportedEncodingException e4) {
            e4.printStackTrace();
            bArr = null;
        }
        if (bArr == null) {
            return "";
        }
        String str3 = new String(b.a(bArr, bArr.length, (String) null));
        System.out.println("generateMacSignature is:" + str3);
        return str3;
    }
}
