package com.shoujiduoduo.util.a;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/* compiled from: CheapSoundFile */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f2976a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    static a[] c = {a.a(), b.a(), c.a(), e.a()};
    static ArrayList<String> d = new ArrayList<>();
    static HashMap<String, a> e = new HashMap<>();
    protected b f = null;
    protected File g = null;

    /* compiled from: CheapSoundFile */
    public interface a {
        d a();

        String[] b();
    }

    /* compiled from: CheapSoundFile */
    public interface b {
        boolean a(double d);
    }

    static {
        for (a aVar : c) {
            for (String str : aVar.b()) {
                d.add(str);
                e.put(str, aVar);
            }
        }
    }

    public static d a(String str, b bVar) throws FileNotFoundException, IOException {
        File file = new File(str);
        if (!file.exists()) {
            throw new FileNotFoundException(str);
        }
        String[] split = file.getName().toLowerCase().split("\\.");
        if (split.length < 2) {
            return null;
        }
        a aVar = e.get(split[split.length - 1]);
        if (aVar == null) {
            return null;
        }
        d a2 = aVar.a();
        a2.a(bVar);
        a2.a(file);
        return a2;
    }

    protected d() {
    }

    public void a(File file) throws FileNotFoundException, IOException {
        this.g = file;
    }

    public void a(b bVar) {
        this.f = bVar;
    }

    public int b() {
        return 0;
    }

    public int c() {
        return 0;
    }

    public int[] d() {
        return null;
    }

    public int e() {
        return 0;
    }

    public int f() {
        return 0;
    }

    public int a_(int i) {
        return -1;
    }

    public void a(File file, int i, int i2) throws IOException {
    }
}
