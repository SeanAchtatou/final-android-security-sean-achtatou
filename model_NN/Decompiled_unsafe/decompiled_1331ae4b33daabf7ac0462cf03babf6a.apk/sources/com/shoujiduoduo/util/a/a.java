package com.shoujiduoduo.util.a;

import com.shoujiduoduo.util.a.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: CheapAAC */
public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f2968a = {1684631142, 1751411826, 1835296868, 1835297121, 1835626086, 1836019574, 1836476516, 1936549988, 1937007212, 1937011556, 1937011578, 1937011827, 1953196132, 1953653099};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f2969b = {1684631142, 1751411826, 1835296868, 1836476516, 1936549988, 1953196132, 1937011556};
    private int h;
    private int[] i;
    private int[] j;
    private int[] k;
    private int l;
    private HashMap<Integer, C0040a> m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;

    public static d.a a() {
        return new d.a() {
            public d a() {
                return new a();
            }

            public String[] b() {
                return new String[]{"aac", "m4a"};
            }
        };
    }

    /* renamed from: com.shoujiduoduo.util.a.a$a  reason: collision with other inner class name */
    /* compiled from: CheapAAC */
    class C0040a {

        /* renamed from: a  reason: collision with root package name */
        public int f2970a;

        /* renamed from: b  reason: collision with root package name */
        public int f2971b;
        public byte[] c;

        C0040a() {
        }
    }

    public int b() {
        return this.h;
    }

    public int c() {
        return this.q;
    }

    public int[] d() {
        return this.k;
    }

    public int e() {
        return this.o;
    }

    public int f() {
        return this.p;
    }

    public String a(int i2) {
        return ((("" + ((char) ((i2 >> 24) & 255))) + ((char) ((i2 >> 16) & 255))) + ((char) ((i2 >> 8) & 255))) + ((char) (i2 & 255));
    }

    public void a(File file) throws FileNotFoundException, IOException {
        boolean z = false;
        super.a(file);
        this.p = 0;
        this.o = 0;
        this.n = 0;
        this.q = 0;
        this.h = 0;
        this.s = 255;
        this.t = 0;
        this.r = 0;
        this.u = -1;
        this.v = -1;
        this.m = new HashMap<>();
        this.l = (int) this.g.length();
        if (this.l < 128) {
            throw new IOException("File too small to parse");
        }
        byte[] bArr = new byte[8];
        new FileInputStream(this.g).read(bArr, 0, 8);
        if (bArr[0] == 0 && bArr[4] == 102 && bArr[5] == 116 && bArr[6] == 121 && bArr[7] == 112) {
            e(new FileInputStream(this.g), this.l);
            if (this.u <= 0 || this.v <= 0) {
                throw new IOException("Didn't find mdat");
            }
            FileInputStream fileInputStream = new FileInputStream(this.g);
            fileInputStream.skip((long) this.u);
            this.r = this.u;
            c(fileInputStream, this.v);
            for (int i2 : f2968a) {
                if (!this.m.containsKey(Integer.valueOf(i2))) {
                    System.out.println("Missing atom: " + a(i2));
                    z = true;
                }
            }
            if (z) {
                throw new IOException("Could not parse MP4 file");
            }
            return;
        }
        throw new IOException("Unknown file format");
    }

    private void e(InputStream inputStream, int i2) throws IOException {
        byte b2;
        int i3 = i2;
        while (i3 > 8) {
            int i4 = this.r;
            byte[] bArr = new byte[8];
            inputStream.read(bArr, 0, 8);
            byte b3 = ((bArr[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
            if (b3 > i3) {
                b2 = i3;
            } else {
                b2 = b3;
            }
            byte b4 = ((bArr[4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
            C0040a aVar = new C0040a();
            aVar.f2970a = this.r;
            aVar.f2971b = b2;
            this.m.put(Integer.valueOf(b4), aVar);
            this.r += 8;
            if (b4 == 1836019574 || b4 == 1953653099 || b4 == 1835297121 || b4 == 1835626086 || b4 == 1937007212) {
                e(inputStream, b2);
            } else if (b4 == 1937011578) {
                b(inputStream, b2 - 8);
            } else if (b4 == 1937011827) {
                a(inputStream, b2 - 8);
            } else if (b4 == 1835295092) {
                this.u = this.r;
                this.v = b2 - 8;
            } else {
                for (int i5 : f2969b) {
                    if (i5 == b4) {
                        byte[] bArr2 = new byte[(b2 - 8)];
                        inputStream.read(bArr2, 0, b2 - 8);
                        this.r += b2 - 8;
                        this.m.get(Integer.valueOf(b4)).c = bArr2;
                    }
                }
            }
            if (b4 == 1937011556) {
                g();
            }
            i3 -= b2;
            int i6 = b2 - (this.r - i4);
            if (i6 < 0) {
                throw new IOException("Went over by " + (-i6) + " bytes");
            }
            inputStream.skip((long) i6);
            this.r = i6 + this.r;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(InputStream inputStream, int i2) throws IOException {
        byte[] bArr = new byte[16];
        inputStream.read(bArr, 0, 16);
        this.r += 16;
        this.q = (bArr[15] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr[12] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[13] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[14] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8);
    }

    /* access modifiers changed from: package-private */
    public void b(InputStream inputStream, int i2) throws IOException {
        byte[] bArr = new byte[12];
        inputStream.read(bArr, 0, 12);
        this.r += 12;
        this.h = (bArr[11] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr[8] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[9] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[10] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8);
        this.i = new int[this.h];
        this.j = new int[this.h];
        this.k = new int[this.h];
        byte[] bArr2 = new byte[(this.h * 4)];
        inputStream.read(bArr2, 0, this.h * 4);
        this.r += this.h * 4;
        for (int i3 = 0; i3 < this.h; i3++) {
            this.j[i3] = ((bArr2[(i3 * 4) + 0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr2[(i3 * 4) + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr2[(i3 * 4) + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr2[(i3 * 4) + 3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        byte[] bArr = this.m.get(1937011556).c;
        this.p = ((bArr[32] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[33] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        this.o = (bArr[41] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr[40] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8);
    }

    /* access modifiers changed from: package-private */
    public void c(InputStream inputStream, int i2) throws IOException {
        int i3 = this.r;
        int i4 = 0;
        while (i4 < this.h) {
            this.i[i4] = this.r;
            if ((this.r - i3) + this.j[i4] > i2 - 8) {
                this.k[i4] = 0;
            } else {
                d(inputStream, i4);
            }
            if (this.k[i4] < this.s) {
                this.s = this.k[i4];
            }
            if (this.k[i4] > this.t) {
                this.t = this.k[i4];
            }
            if (this.f == null || this.f.a((((double) this.r) * 1.0d) / ((double) this.l))) {
                i4++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void d(InputStream inputStream, int i2) throws IOException {
        int i3;
        int i4;
        byte b2;
        int i5;
        int i6;
        int i7 = 0;
        if (this.j[i2] < 4) {
            this.k[i2] = 0;
            inputStream.skip((long) this.j[i2]);
            return;
        }
        int i8 = this.r;
        byte[] bArr = new byte[4];
        inputStream.read(bArr, 0, 4);
        this.r += 4;
        switch ((bArr[0] & 224) >> 5) {
            case 0:
                this.k[i2] = ((bArr[0] & 1) << 7) | ((bArr[1] & 254) >> 1);
                break;
            case 1:
                int i9 = (bArr[1] & 96) >> 5;
                int i10 = (bArr[1] & 16) >> 4;
                if (i9 == 2) {
                    byte b3 = bArr[1] & 15;
                    int i11 = (bArr[2] & 254) >> 1;
                    i3 = ((bArr[2] & 1) << 1) | ((bArr[3] & 128) >> 7);
                    i4 = 25;
                    b2 = b3;
                    i5 = i11;
                } else {
                    byte b4 = ((bArr[1] & 15) << 2) | ((bArr[2] & 192) >> 6);
                    i3 = (bArr[2] & 24) >> 3;
                    i4 = 21;
                    b2 = b4;
                    i5 = -1;
                }
                if (i3 == 1) {
                    int i12 = 0;
                    for (int i13 = 0; i13 < 7; i13++) {
                        if (((1 << i13) & i5) == 0) {
                            i12++;
                        }
                    }
                    i6 = ((i12 + 1) * b2) + i4;
                } else {
                    i6 = i4;
                }
                int i14 = ((i6 + 7) / 8) + 1;
                byte[] bArr2 = new byte[i14];
                bArr2[0] = bArr[0];
                bArr2[1] = bArr[1];
                bArr2[2] = bArr[2];
                bArr2[3] = bArr[3];
                inputStream.read(bArr2, 4, i14 - 4);
                this.r = (i14 - 4) + this.r;
                for (int i15 = 0; i15 < 8; i15++) {
                    int i16 = 7 - ((i15 + i6) % 8);
                    i7 += ((bArr2[(i15 + i6) / 8] & (1 << i16)) >> i16) << (7 - i15);
                }
                this.k[i2] = i7;
                break;
            default:
                if (i2 <= 0) {
                    this.k[i2] = 0;
                    break;
                } else {
                    this.k[i2] = this.k[i2 - 1];
                    break;
                }
        }
        int i17 = this.j[i2] - (this.r - i8);
        inputStream.skip((long) i17);
        this.r = i17 + this.r;
    }

    public void a(FileOutputStream fileOutputStream, int i2) throws IOException {
        int i3 = this.m.get(Integer.valueOf(i2)).f2971b;
        fileOutputStream.write(new byte[]{(byte) ((i3 >> 24) & 255), (byte) ((i3 >> 16) & 255), (byte) ((i3 >> 8) & 255), (byte) (i3 & 255), (byte) ((i2 >> 24) & 255), (byte) ((i2 >> 16) & 255), (byte) ((i2 >> 8) & 255), (byte) (i2 & 255)}, 0, 8);
    }

    public void b(FileOutputStream fileOutputStream, int i2) throws IOException {
        C0040a aVar = this.m.get(Integer.valueOf(i2));
        a(fileOutputStream, i2);
        fileOutputStream.write(aVar.c, 0, aVar.f2971b - 8);
    }

    public void a(int i2, byte[] bArr) {
        C0040a aVar = this.m.get(Integer.valueOf(i2));
        if (aVar == null) {
            aVar = new C0040a();
            this.m.put(Integer.valueOf(i2), aVar);
        }
        aVar.f2971b = bArr.length + 8;
        aVar.c = bArr;
    }

    public void a(File file, int i2, int i3) throws IOException {
        file.createNewFile();
        FileInputStream fileInputStream = new FileInputStream(this.g);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        a(1718909296, new byte[]{77, 52, 65, 32, 0, 0, 0, 0, 77, 52, 65, 32, 109, 112, 52, 50, 105, 115, 111, 109, 0, 0, 0, 0});
        a(1937011827, new byte[]{0, 0, 0, 0, 0, 0, 0, 1, (byte) ((i3 >> 24) & 255), (byte) ((i3 >> 16) & 255), (byte) ((i3 >> 8) & 255), (byte) (i3 & 255), (byte) ((this.q >> 24) & 255), (byte) ((this.q >> 16) & 255), (byte) ((this.q >> 8) & 255), (byte) (this.q & 255)});
        a(1937011555, new byte[]{0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, (byte) ((i3 >> 24) & 255), (byte) ((i3 >> 16) & 255), (byte) ((i3 >> 8) & 255), (byte) (i3 & 255), 0, 0, 0, 1});
        byte[] bArr = new byte[((i3 * 4) + 12)];
        bArr[8] = (byte) ((i3 >> 24) & 255);
        bArr[9] = (byte) ((i3 >> 16) & 255);
        bArr[10] = (byte) ((i3 >> 8) & 255);
        bArr[11] = (byte) (i3 & 255);
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i4 * 4) + 12] = (byte) ((this.j[i2 + i4] >> 24) & 255);
            bArr[(i4 * 4) + 13] = (byte) ((this.j[i2 + i4] >> 16) & 255);
            bArr[(i4 * 4) + 14] = (byte) ((this.j[i2 + i4] >> 8) & 255);
            bArr[(i4 * 4) + 15] = (byte) (this.j[i2 + i4] & 255);
        }
        a(1937011578, bArr);
        int i5 = this.m.get(1684631142).f2971b + (i3 * 4) + 144 + this.m.get(1937011556).f2971b + this.m.get(1937011555).f2971b + this.m.get(1836476516).f2971b + this.m.get(1953196132).f2971b + this.m.get(1835296868).f2971b + this.m.get(1751411826).f2971b + this.m.get(1936549988).f2971b;
        a(1937007471, new byte[]{0, 0, 0, 0, 0, 0, 0, 1, (byte) ((i5 >> 24) & 255), (byte) ((i5 >> 16) & 255), (byte) ((i5 >> 8) & 255), (byte) (i5 & 255)});
        this.m.get(1937007212).f2971b = this.m.get(1937007471).f2971b + this.m.get(1937011556).f2971b + 8 + this.m.get(1937011827).f2971b + this.m.get(1937011555).f2971b + this.m.get(1937011578).f2971b;
        this.m.get(1835626086).f2971b = this.m.get(1937007212).f2971b + this.m.get(1684631142).f2971b + 8 + this.m.get(1936549988).f2971b;
        this.m.get(1835297121).f2971b = this.m.get(1835626086).f2971b + this.m.get(1835296868).f2971b + 8 + this.m.get(1751411826).f2971b;
        this.m.get(1953653099).f2971b = this.m.get(1835297121).f2971b + this.m.get(1953196132).f2971b + 8;
        this.m.get(1836019574).f2971b = this.m.get(1953653099).f2971b + this.m.get(1836476516).f2971b + 8;
        int i6 = 8;
        for (int i7 = 0; i7 < i3; i7++) {
            i6 += this.j[i2 + i7];
        }
        this.m.get(1835295092).f2971b = i6;
        b(fileOutputStream, 1718909296);
        a(fileOutputStream, 1836019574);
        b(fileOutputStream, 1836476516);
        a(fileOutputStream, 1953653099);
        b(fileOutputStream, 1953196132);
        a(fileOutputStream, 1835297121);
        b(fileOutputStream, 1835296868);
        b(fileOutputStream, 1751411826);
        a(fileOutputStream, 1835626086);
        b(fileOutputStream, 1684631142);
        b(fileOutputStream, 1936549988);
        a(fileOutputStream, 1937007212);
        b(fileOutputStream, 1937011556);
        b(fileOutputStream, 1937011827);
        b(fileOutputStream, 1937011555);
        b(fileOutputStream, 1937011578);
        b(fileOutputStream, 1937007471);
        a(fileOutputStream, 1835295092);
        int i8 = 0;
        for (int i9 = 0; i9 < i3; i9++) {
            if (this.j[i2 + i9] > i8) {
                i8 = this.j[i2 + i9];
            }
        }
        byte[] bArr2 = new byte[i8];
        int i10 = 0;
        for (int i11 = 0; i11 < i3; i11++) {
            int i12 = this.i[i2 + i11] - i10;
            int i13 = this.j[i2 + i11];
            if (i12 >= 0) {
                if (i12 > 0) {
                    fileInputStream.skip((long) i12);
                    i10 += i12;
                }
                fileInputStream.read(bArr2, 0, i13);
                fileOutputStream.write(bArr2, 0, i13);
                i10 += i13;
            }
        }
        fileInputStream.close();
        fileOutputStream.close();
    }
}
