package com.shoujiduoduo.util;

import android.util.Base64;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: FormatUtils */
public class q {
    public static int a(String str, int i) {
        if (ag.c(str)) {
            return i;
        }
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return i;
        }
    }

    public static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String b(String str) {
        if (ag.c(str)) {
            return "";
        }
        return new String(Base64.encode(str.getBytes(), 0));
    }

    public static String c(String str) {
        if (ag.c(str)) {
            return "";
        }
        return new String(Base64.decode(str.getBytes(), 0));
    }
}
