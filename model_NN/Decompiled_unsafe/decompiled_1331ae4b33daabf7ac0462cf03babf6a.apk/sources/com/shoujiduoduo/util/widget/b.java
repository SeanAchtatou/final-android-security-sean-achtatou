package com.shoujiduoduo.util.widget;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.shoujiduoduo.ringtone.R;

/* compiled from: DuoduoAlertDialog */
public class b extends Dialog {
    public b(Context context, int i) {
        super(context, i);
    }

    /* compiled from: DuoduoAlertDialog */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Context f3276a;

        /* renamed from: b  reason: collision with root package name */
        private String f3277b;
        private String c;
        private String d;
        private String e;
        private View f;
        private View g;
        private boolean h;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener i;
        /* access modifiers changed from: private */
        public DialogInterface.OnClickListener j;

        public a(Context context) {
            this.f3276a = context;
        }

        public a a(String str) {
            this.c = str;
            return this;
        }

        public a a(int i2) {
            this.c = (String) this.f3276a.getText(i2);
            return this;
        }

        public a b(int i2) {
            this.f3277b = (String) this.f3276a.getText(i2);
            return this;
        }

        public a b(String str) {
            this.f3277b = str;
            return this;
        }

        public a a(View view) {
            this.f = view;
            return this;
        }

        public a a(int i2, DialogInterface.OnClickListener onClickListener) {
            this.d = (String) this.f3276a.getText(i2);
            this.i = onClickListener;
            return this;
        }

        public a a(String str, DialogInterface.OnClickListener onClickListener) {
            this.d = str;
            this.i = onClickListener;
            return this;
        }

        public a b(int i2, DialogInterface.OnClickListener onClickListener) {
            this.e = (String) this.f3276a.getText(i2);
            this.j = onClickListener;
            return this;
        }

        public a b(String str, DialogInterface.OnClickListener onClickListener) {
            this.e = str;
            this.j = onClickListener;
            return this;
        }

        public b a() {
            LayoutInflater layoutInflater = (LayoutInflater) this.f3276a.getSystemService("layout_inflater");
            b bVar = new b(this.f3276a, R.style.Dialog);
            if (this.g != null) {
                bVar.setContentView(this.g);
            } else {
                a(layoutInflater, bVar);
            }
            return bVar;
        }

        private void a(LayoutInflater layoutInflater, final b bVar) {
            View inflate = layoutInflater.inflate((int) R.layout.custom_dialog, (ViewGroup) null);
            bVar.addContentView(inflate, new ViewGroup.LayoutParams(-1, -2));
            if (!TextUtils.isEmpty(this.f3277b)) {
                ((TextView) inflate.findViewById(R.id.title)).setText(this.f3277b);
            } else {
                inflate.findViewById(R.id.top_title_layout).setVisibility(8);
            }
            if (this.d != null) {
                ((Button) inflate.findViewById(R.id.positiveButton)).setText(this.d);
                ((Button) inflate.findViewById(R.id.positiveButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (a.this.i != null) {
                            a.this.i.onClick(bVar, -1);
                        } else {
                            bVar.dismiss();
                        }
                    }
                });
            } else {
                inflate.findViewById(R.id.positiveButton).setVisibility(8);
            }
            if (this.e != null) {
                ((Button) inflate.findViewById(R.id.negativeButton)).setText(this.e);
                ((Button) inflate.findViewById(R.id.negativeButton)).setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (a.this.j != null) {
                            a.this.j.onClick(bVar, -2);
                        } else {
                            bVar.dismiss();
                        }
                    }
                });
            } else {
                inflate.findViewById(R.id.negativeButton).setVisibility(8);
            }
            if (this.e == null && this.d == null) {
                inflate.findViewById(R.id.confirm_cancel_layout).setVisibility(8);
            }
            if (this.h) {
                Button button = (Button) inflate.findViewById(R.id.close);
                button.setVisibility(0);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        bVar.dismiss();
                    }
                });
            } else {
                inflate.findViewById(R.id.close).setVisibility(8);
            }
            if (this.c != null) {
                ((TextView) inflate.findViewById(R.id.message)).setText(this.c);
            } else if (this.f != null) {
                ((LinearLayout) inflate.findViewById(R.id.content)).removeAllViews();
                ((LinearLayout) inflate.findViewById(R.id.content)).addView(this.f, new ViewGroup.LayoutParams(-1, -2));
            }
            bVar.setContentView(inflate);
        }
    }
}
