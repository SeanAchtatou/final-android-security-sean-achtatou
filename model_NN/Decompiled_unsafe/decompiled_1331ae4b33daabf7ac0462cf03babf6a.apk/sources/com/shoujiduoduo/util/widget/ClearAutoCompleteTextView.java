package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;
import com.shoujiduoduo.ringtone.R;

public class ClearAutoCompleteTextView extends AutoCompleteTextView implements TextWatcher, View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f3247a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f3248b;

    public ClearAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 16842862);
    }

    public ClearAutoCompleteTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private void a() {
        this.f3247a = getCompoundDrawables()[2];
        if (this.f3247a == null) {
            this.f3247a = getResources().getDrawable(R.drawable.btn_edittext_clear);
        }
        this.f3247a.setBounds(0, 0, this.f3247a.getIntrinsicWidth(), this.f3247a.getIntrinsicHeight());
        setClearIconVisible(false);
        setOnFocusChangeListener(this);
        addTextChangedListener(this);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z = true;
        if (motionEvent.getAction() == 1 && getCompoundDrawables()[2] != null) {
            if (motionEvent.getX() <= ((float) (getWidth() - getTotalPaddingRight())) || motionEvent.getX() >= ((float) (getWidth() - getPaddingRight()))) {
                z = false;
            }
            if (z) {
                setText("");
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void onFocusChange(View view, boolean z) {
        boolean z2 = false;
        this.f3248b = z;
        if (z) {
            if (getText().length() > 0) {
                z2 = true;
            }
            setClearIconVisible(z2);
            return;
        }
        setClearIconVisible(false);
    }

    /* access modifiers changed from: protected */
    public void setClearIconVisible(boolean z) {
        setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], z ? this.f3247a : null, getCompoundDrawables()[3]);
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (this.f3248b) {
            setClearIconVisible(charSequence.length() > 0);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
    }
}
