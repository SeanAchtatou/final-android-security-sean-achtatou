package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.widget.Adapter;
import android.widget.ListView;
import android.widget.SectionIndexer;

/* compiled from: IndexScroller */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final float f3284a;

    /* renamed from: b  reason: collision with root package name */
    private final float f3285b;
    private final float c;
    private final float d;
    private final float e;
    private float f = 1.0f;
    private int g = 1;
    private int h;
    private int i;
    private int j = -1;
    private boolean k = false;
    private ListView l = null;
    private SectionIndexer m = null;
    private String[] n = null;
    private RectF o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;

    public c(Context context, ListView listView) {
        this.d = context.getResources().getDisplayMetrics().density;
        this.e = context.getResources().getDisplayMetrics().scaledDensity;
        this.l = listView;
        a(this.l.getAdapter());
        this.f3284a = 15.0f * this.d;
        this.f3285b = this.d * 5.0f;
        this.c = this.d * 5.0f;
        this.p = -7829368;
        this.q = -1;
        this.r = -16759672;
        this.s = 0;
        this.t = ViewCompat.MEASURED_STATE_MASK;
    }

    public void a(int i2, int i3, int i4, int i5, int i6) {
        this.p = i2;
        this.q = i3;
        this.r = i4;
        this.s = i5;
        this.t = i6;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    public void a(Canvas canvas) {
        if (this.g != 0) {
            Paint paint = new Paint();
            if (this.j >= 0) {
                paint.setColor(this.t);
                paint.setAlpha((int) (64.0f * this.f));
                canvas.drawRect(this.o, paint);
            } else {
                paint.setColor(this.s);
                paint.setAntiAlias(true);
                canvas.drawRect(this.o, paint);
            }
            if (this.n != null && this.n.length > 0) {
                if (this.j >= 0) {
                    Paint paint2 = new Paint();
                    paint2.setColor((int) ViewCompat.MEASURED_STATE_MASK);
                    paint2.setAlpha(96);
                    paint2.setAntiAlias(true);
                    paint2.setShadowLayer(3.0f, 0.0f, 0.0f, Color.argb(64, 0, 0, 0));
                    Paint paint3 = new Paint();
                    paint3.setColor(-1);
                    paint3.setAntiAlias(true);
                    paint3.setTextSize(50.0f * this.e);
                    float measureText = paint3.measureText(this.n[this.j]);
                    float descent = ((this.c * 2.0f) + paint3.descent()) - paint3.ascent();
                    RectF rectF = new RectF((((float) this.h) - descent) / 2.0f, (((float) this.i) - descent) / 2.0f, ((((float) this.h) - descent) / 2.0f) + descent, ((((float) this.i) - descent) / 2.0f) + descent);
                    canvas.drawRoundRect(rectF, 5.0f * this.d, 5.0f * this.d, paint2);
                    canvas.drawText(this.n[this.j], (((descent - measureText) / 2.0f) + rectF.left) - 1.0f, ((rectF.top + this.c) - paint3.ascent()) + 1.0f, paint3);
                }
                Paint paint4 = new Paint();
                paint4.setAlpha((int) (255.0f * this.f));
                paint4.setAntiAlias(true);
                paint4.setTextSize(11.0f * this.e);
                float height = (this.o.height() - (this.f3285b * 2.0f)) / ((float) this.n.length);
                float descent2 = (height - (paint4.descent() - paint4.ascent())) / 2.0f;
                for (int i2 = 0; i2 < this.n.length; i2++) {
                    if (this.j < 0) {
                        paint4.setColor(this.p);
                    } else if (this.j == i2) {
                        paint4.setColor(this.r);
                    } else {
                        paint4.setColor(this.q);
                    }
                    canvas.drawText(this.n[i2], ((this.f3284a - paint4.measureText(this.n[i2])) / 2.0f) + this.o.left, (((this.o.top + this.f3285b) + (((float) i2) * height)) + descent2) - paint4.ascent(), paint4);
                }
            }
        }
    }

    public boolean a(MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                if (this.g != 0 && a(motionEvent.getX(), motionEvent.getY())) {
                    this.k = true;
                    this.j = a(motionEvent.getY());
                    if (this.l == null || this.m == null) {
                        return true;
                    }
                    this.l.setSelection(this.m.getPositionForSection(this.j));
                    return true;
                }
                break;
            case 1:
                if (this.k) {
                    this.k = false;
                    this.j = -1;
                    this.l.invalidate();
                    return true;
                }
                break;
            case 2:
                if (this.k) {
                    if (!a(motionEvent.getX(), motionEvent.getY())) {
                        return true;
                    }
                    this.j = a(motionEvent.getY());
                    if (this.l == null || this.m == null) {
                        return true;
                    }
                    this.l.setSelection(this.m.getPositionForSection(this.j));
                    return true;
                }
                break;
        }
        return false;
    }

    public void a(int i2, int i3, int i4, int i5) {
        this.h = i2;
        this.i = i3;
        this.o = new RectF((((float) i2) - this.f3285b) - this.f3284a, this.f3285b, ((float) i2) - this.f3285b, ((float) i3) - this.f3285b);
    }

    public void a() {
        a(1);
    }

    public void b() {
        a(0);
    }

    public void a(Adapter adapter) {
        if (adapter instanceof SectionIndexer) {
            this.m = (SectionIndexer) adapter;
            this.n = (String[]) this.m.getSections();
        }
    }

    private void a(int i2) {
        if (this.g != i2) {
            this.g = i2;
            switch (this.g) {
                case 0:
                    this.f = 0.0f;
                    this.l.invalidate();
                    return;
                case 1:
                    this.f = 1.0f;
                    this.l.invalidate();
                    return;
                default:
                    return;
            }
        }
    }

    private boolean a(float f2, float f3) {
        return f2 >= this.o.left && f3 >= this.o.top && f3 <= this.o.top + this.o.height();
    }

    private int a(float f2) {
        if (this.n == null || this.n.length == 0 || f2 < this.o.top + this.f3285b) {
            return 0;
        }
        if (f2 >= (this.o.top + this.o.height()) - this.f3285b) {
            return this.n.length - 1;
        }
        return (int) (((f2 - this.o.top) - this.f3285b) / ((this.o.height() - (2.0f * this.f3285b)) / ((float) this.n.length)));
    }
}
