package com.shoujiduoduo.util.widget;

import android.content.Context;
import net.lucode.hackware.magicindicator.b.a.d.a;

/* compiled from: ScaleTransitionPagerTitleView */
public class f extends a {
    private float c = 0.75f;

    public f(Context context) {
        super(context);
    }

    public void a(int i, int i2, float f, boolean z) {
        super.a(i, i2, f, z);
        setScaleX(this.c + ((1.0f - this.c) * f));
        setScaleY(this.c + ((1.0f - this.c) * f));
    }

    public void b(int i, int i2, float f, boolean z) {
        super.b(i, i2, f, z);
        setScaleX(((this.c - 1.0f) * f) + 1.0f);
        setScaleY(((this.c - 1.0f) * f) + 1.0f);
    }

    public float getMinScale() {
        return this.c;
    }

    public void setMinScale(float f) {
        this.c = f;
    }
}
