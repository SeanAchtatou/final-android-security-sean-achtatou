package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class Chronometer extends TextView {

    /* renamed from: a  reason: collision with root package name */
    long f3243a;

    /* renamed from: b  reason: collision with root package name */
    long f3244b;

    public Chronometer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setDuration(long j) {
        this.f3243a = j;
        setText(a(this.f3243a));
    }

    public void a(long j, long j2) {
        this.f3244b = j;
        this.f3243a = j2;
        setText(a(this.f3244b) + "/" + a(this.f3243a));
    }

    private String a(long j) {
        long j2 = j / 60;
        long j3 = j % 60;
        if (j2 == 0) {
            return "00:" + String.format("%02d", Long.valueOf(j));
        }
        return String.format("%02d", Long.valueOf(j2)) + ":" + String.format("%02d", Long.valueOf(j3));
    }
}
