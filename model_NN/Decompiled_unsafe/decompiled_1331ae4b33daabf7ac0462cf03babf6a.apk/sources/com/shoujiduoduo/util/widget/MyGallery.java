package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Gallery;

public class MyGallery extends Gallery {
    public MyGallery(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        c();
    }

    public MyGallery(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        c();
    }

    public MyGallery(Context context) {
        super(context);
        c();
    }

    private void c() {
        setSpacing(-1);
        setUnselectedAlpha(1.0f);
        setSoundEffectsEnabled(false);
        setFadingEdgeLength(0);
        setAnimationDuration(1000);
    }

    public void a() {
        onKeyDown(21, null);
    }

    public void b() {
        onKeyDown(22, null);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        if (motionEvent.getX() - motionEvent2.getX() > 0.0f) {
            b();
            return true;
        }
        a();
        return true;
    }
}
