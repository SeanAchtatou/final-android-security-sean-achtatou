package com.shoujiduoduo.util.widget;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.media.TransportMediator;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.igexin.download.Downloads;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ui.utils.BaseActivity;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.g;
import com.shoujiduoduo.util.n;
import java.io.File;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f3269a = this;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ValueCallback<Uri> f3270b;
    /* access modifiers changed from: private */
    public ValueCallback<Uri[]> c;
    private WebView d;
    private ImageButton e;
    private ImageButton f;
    /* access modifiers changed from: private */
    public ImageButton g;
    /* access modifiers changed from: private */
    public ImageButton h;
    private ImageButton i;
    /* access modifiers changed from: private */
    public ProgressBar j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m;
    private WebViewClient n = new WebViewClient() {
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            String d;
            a.a("WebViewActivity", "jump url:" + str);
            if (str.endsWith("apk")) {
                n a2 = n.a(WebViewActivity.this);
                if (WebViewActivity.this.l == null) {
                    d = "";
                } else {
                    d = WebViewActivity.this.l;
                }
                a2.a(str, d);
                return true;
            } else if (g.a(str)) {
                g.a(WebViewActivity.this, str);
                return true;
            } else if (str.startsWith("http:") || str.startsWith("https:")) {
                webView.loadUrl(str);
                return true;
            } else {
                try {
                    WebViewActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                    return true;
                } catch (Exception e) {
                    return true;
                }
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            a.a("WebViewActivity", "onPageFinished. goback = " + webView.canGoBack() + ", forward = " + webView.canGoForward());
            WebViewActivity.this.g.setEnabled(webView.canGoBack());
            WebViewActivity.this.h.setEnabled(webView.canGoForward());
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
        }
    };

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.webview_activity);
        this.d = (WebView) findViewById(R.id.webview_window);
        this.e = (ImageButton) findViewById(R.id.btn_exit_web_activity);
        this.f = (ImageButton) findViewById(R.id.btn_close_web_activity);
        this.g = (ImageButton) findViewById(R.id.web_back);
        this.h = (ImageButton) findViewById(R.id.web_forward);
        this.i = (ImageButton) findViewById(R.id.web_refresh);
        this.j = (ProgressBar) findViewById(R.id.progressWebLoading);
        this.k = (TextView) findViewById(R.id.textWebActivityTitle);
        this.e.setOnClickListener(this);
        this.f.setOnClickListener(this);
        this.g.setEnabled(false);
        this.g.setOnClickListener(this);
        this.h.setEnabled(false);
        this.h.setOnClickListener(this);
        this.i.setOnClickListener(this);
        this.j.setVisibility(8);
        Intent intent = getIntent();
        if (intent != null) {
            String stringExtra = intent.getStringExtra("url");
            this.m = intent.getStringExtra("title");
            a.a("WebViewActivity", "url:" + stringExtra);
            this.l = intent.getStringExtra("apkname");
            if (!ag.c(this.m)) {
                this.k.setText(this.m);
            }
            WebSettings settings = this.d.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setUseWideViewPort(true);
            settings.setLoadWithOverviewMode(true);
            settings.setBuiltInZoomControls(false);
            settings.setDomStorageEnabled(true);
            settings.setSupportZoom(false);
            if (Build.VERSION.SDK_INT >= 21) {
                settings.setMixedContentMode(0);
            }
            this.d.requestFocus(TransportMediator.KEYCODE_MEDIA_RECORD);
            this.d.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case 0:
                        case 1:
                            if (view.hasFocus()) {
                                return false;
                            }
                            view.requestFocus();
                            return false;
                        default:
                            return false;
                    }
                }
            });
            this.d.setWebViewClient(this.n);
            this.d.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView webView, int i) {
                    if (i < 100) {
                        WebViewActivity.this.j.setVisibility(0);
                        WebViewActivity.this.j.setProgress(i);
                    }
                    if (i == 100) {
                        WebViewActivity.this.j.setProgress(0);
                        WebViewActivity.this.j.setVisibility(8);
                    }
                    super.onProgressChanged(webView, i);
                }

                public void onReceivedTitle(WebView webView, String str) {
                    if (ag.c(WebViewActivity.this.m)) {
                        WebViewActivity.this.k.setText(str);
                    } else {
                        WebViewActivity.this.k.setText(WebViewActivity.this.m);
                    }
                }

                public void openFileChooser(ValueCallback<Uri> valueCallback) {
                    customOpenFileChooser(valueCallback);
                }

                /* access modifiers changed from: protected */
                public void openFileChooser(ValueCallback valueCallback, String str) {
                    customOpenFileChooser(valueCallback);
                }

                /* access modifiers changed from: protected */
                public void openFileChooser(ValueCallback<Uri> valueCallback, String str, String str2) {
                    customOpenFileChooser(valueCallback);
                }

                public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                    a.a("musicalbum", "onShowFileChooser");
                    ValueCallback unused = WebViewActivity.this.c = valueCallback;
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.addCategory("android.intent.category.OPENABLE");
                    intent.setType("image/*");
                    WebViewActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), 102);
                    return true;
                }

                /* access modifiers changed from: protected */
                public void customOpenFileChooser(ValueCallback<Uri> valueCallback) {
                    a.a("musicalbum", "customOpenFileChooser");
                    ValueCallback unused = WebViewActivity.this.f3270b = valueCallback;
                    Intent intent = new Intent("android.intent.action.GET_CONTENT");
                    intent.addCategory("android.intent.category.OPENABLE");
                    intent.setType("image/*");
                    WebViewActivity.this.startActivityForResult(Intent.createChooser(intent, "选择文件"), 102);
                }
            });
            if (Build.VERSION.SDK_INT >= 17) {
                this.d.getSettings().setMediaPlaybackRequiresUserGesture(false);
            }
            this.d.loadUrl(stringExtra);
            return;
        }
        a.e("WebViewActivity", "intent is null");
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 102) {
            a(i3, intent);
        }
    }

    private void a(int i2, Intent intent) {
        String str;
        if (Build.VERSION.SDK_INT < 21) {
            Uri data = (intent == null || i2 != -1) ? null : intent.getData();
            if ("smartisan".equals(Build.BRAND)) {
                try {
                    Cursor managedQuery = managedQuery(data, new String[]{Downloads._DATA}, null, null, null);
                    int columnIndexOrThrow = managedQuery.getColumnIndexOrThrow(Downloads._DATA);
                    managedQuery.moveToFirst();
                    str = managedQuery.getString(columnIndexOrThrow);
                } catch (Exception e2) {
                    str = null;
                }
                if (!TextUtils.isEmpty(str)) {
                    try {
                        data = Uri.fromFile(new File(str));
                    } catch (Exception e3) {
                    }
                }
            }
            if (this.f3270b != null) {
                this.f3270b.onReceiveValue(data);
            } else {
                a.c("WebViewActivity", "mUploadMsgOld is null");
            }
        } else if (this.c != null) {
            this.c.onReceiveValue(WebChromeClient.FileChooserParams.parseResult(i2, intent));
        } else {
            a.c("WebViewActivity", "mUploadMsg is null");
        }
        this.c = null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        a.a("WebViewActivity", "keycode back");
        if (this.d == null || !this.d.canGoBack()) {
            finish();
        } else {
            this.d.goBack();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.d != null) {
            this.d.loadUrl("about:blank");
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_close_web_activity:
            case R.id.btn_exit_web_activity:
                this.f3269a.finish();
                return;
            case R.id.web_forward:
                if (this.d.canGoForward()) {
                    this.d.goForward();
                    return;
                }
                return;
            case R.id.web_back:
                if (this.d.canGoBack()) {
                    this.d.goBack();
                    return;
                }
                return;
            case R.id.web_refresh:
                this.d.reload();
                return;
            default:
                return;
        }
    }
}
