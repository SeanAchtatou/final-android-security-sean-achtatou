package com.shoujiduoduo.util.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.widget.ProgressBar;
import com.shoujiduoduo.ringtone.R;
import java.lang.reflect.Field;

public class CircleProgressBar extends ProgressBar {

    /* renamed from: a  reason: collision with root package name */
    private final RectF f3245a;

    /* renamed from: b  reason: collision with root package name */
    private final RectF f3246b;
    private final Rect c;
    private final Paint d;
    private final Paint e;
    private final Paint f;
    private final Paint g;
    private float h;
    private float i;
    private float j;
    private float k;
    private int l;
    private int m;
    private float n;
    private float o;
    private float p;
    private float q;
    private int r;
    private int s;
    private int t;
    private int u;
    private boolean v;
    private String w;
    private int x;
    private int y;
    private Paint.Cap z;

    public CircleProgressBar(Context context) {
        this(context, null);
    }

    public CircleProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f3245a = new RectF();
        this.f3246b = new RectF();
        this.c = new Rect();
        this.d = new Paint(1);
        this.e = new Paint(1);
        this.f = new Paint(1);
        this.g = new Paint(1);
        c();
        a(context, attributeSet);
        a();
    }

    private void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CircleProgressBar);
        this.l = obtainStyledAttributes.getColor(3, 0);
        this.v = obtainStyledAttributes.getBoolean(1, true);
        this.m = obtainStyledAttributes.getInt(0, 45);
        this.w = obtainStyledAttributes.hasValue(11) ? obtainStyledAttributes.getString(11) : "%d%%";
        this.x = obtainStyledAttributes.getInt(12, 0);
        this.y = obtainStyledAttributes.getInt(13, 0);
        this.z = obtainStyledAttributes.hasValue(14) ? Paint.Cap.values()[obtainStyledAttributes.getInt(14, 0)] : Paint.Cap.BUTT;
        this.n = (float) obtainStyledAttributes.getDimensionPixelSize(2, g.a(getContext(), 4.0f));
        this.q = (float) obtainStyledAttributes.getDimensionPixelSize(7, g.a(getContext(), 11.0f));
        this.o = (float) obtainStyledAttributes.getDimensionPixelSize(9, g.a(getContext(), 1.0f));
        this.p = (float) obtainStyledAttributes.getDimensionPixelSize(8, g.a(getContext(), 1.0f));
        this.r = obtainStyledAttributes.getColor(4, Color.parseColor("#fff2a670"));
        this.s = obtainStyledAttributes.getColor(5, Color.parseColor("#fff2a670"));
        this.t = obtainStyledAttributes.getColor(6, Color.parseColor("#fff2a670"));
        this.u = obtainStyledAttributes.getColor(10, Color.parseColor("#ffe3e3e5"));
        obtainStyledAttributes.recycle();
    }

    private void a() {
        this.g.setTextAlign(Paint.Align.CENTER);
        this.g.setTextSize(this.q);
        this.d.setStyle(this.x == 1 ? Paint.Style.FILL : Paint.Style.STROKE);
        this.d.setStrokeWidth(this.p);
        this.d.setColor(this.r);
        this.d.setStrokeCap(this.z);
        this.e.setStyle(this.x == 1 ? Paint.Style.FILL : Paint.Style.STROKE);
        this.e.setStrokeWidth(this.o);
        this.e.setColor(this.u);
        this.e.setStrokeCap(this.z);
        this.f.setStyle(Paint.Style.FILL);
        this.f.setColor(this.l);
    }

    private void b() {
        double degrees;
        Shader shader = null;
        if (this.r != this.s) {
            switch (this.y) {
                case 0:
                    shader = new LinearGradient(this.f3245a.left, this.f3245a.top, this.f3245a.left, this.f3245a.bottom, this.r, this.s, Shader.TileMode.CLAMP);
                    break;
                case 1:
                    shader = new RadialGradient(this.j, this.k, this.h, this.r, this.s, Shader.TileMode.CLAMP);
                    break;
                case 2:
                    float f2 = (float) (((((double) this.o) / 3.141592653589793d) * 2.0d) / ((double) this.h));
                    if (this.z == Paint.Cap.BUTT && this.x == 2) {
                        degrees = 0.0d;
                    } else {
                        degrees = Math.toDegrees((double) f2);
                    }
                    float f3 = (float) (-90.0d - degrees);
                    shader = new SweepGradient(this.j, this.k, new int[]{this.r, this.s}, new float[]{0.0f, 1.0f});
                    Matrix matrix = new Matrix();
                    matrix.postRotate(f3, this.j, this.k);
                    shader.setLocalMatrix(matrix);
                    break;
            }
            this.d.setShader(shader);
            return;
        }
        this.d.setShader(null);
        this.d.setColor(this.r);
    }

    private void c() {
        try {
            Field declaredField = ProgressBar.class.getDeclaredField("mOnlyIndeterminate");
            declaredField.setAccessible(true);
            declaredField.set(this, false);
            Field declaredField2 = ProgressBar.class.getDeclaredField("mIndeterminate");
            declaredField2.setAccessible(true);
            declaredField2.set(this, false);
            Field declaredField3 = ProgressBar.class.getDeclaredField("mCurrentDrawable");
            declaredField3.setAccessible(true);
            declaredField3.set(this, null);
        } catch (NoSuchFieldException e2) {
            e2.printStackTrace();
        } catch (IllegalAccessException e3) {
            e3.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public synchronized void onDraw(Canvas canvas) {
        a(canvas);
        c(canvas);
        b(canvas);
    }

    private void a(Canvas canvas) {
        if (this.l != 0) {
            canvas.drawCircle(this.j, this.j, this.h, this.f);
        }
    }

    private void b(Canvas canvas) {
        if (this.v) {
            String format = String.format(this.w, Integer.valueOf(getProgress()));
            this.g.setTextSize(this.q);
            this.g.setColor(this.t);
            this.g.getTextBounds(format, 0, format.length(), this.c);
            canvas.drawText(format, this.j, this.k + ((float) (this.c.height() / 2)), this.g);
        }
    }

    private void c(Canvas canvas) {
        switch (this.x) {
            case 1:
                e(canvas);
                return;
            case 2:
                f(canvas);
                return;
            default:
                d(canvas);
                return;
        }
    }

    private void d(Canvas canvas) {
        float f2 = (float) (6.283185307179586d / ((double) this.m));
        float f3 = this.h;
        float f4 = this.h - this.n;
        int progress = (int) ((((float) getProgress()) / ((float) getMax())) * ((float) this.m));
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.m) {
                float f5 = ((float) i3) * f2;
                float sin = this.j + (((float) Math.sin((double) f5)) * f4);
                float cos = this.j - (((float) Math.cos((double) f5)) * f4);
                float sin2 = this.j + (((float) Math.sin((double) f5)) * f3);
                float cos2 = this.j - (((float) Math.cos((double) f5)) * f3);
                if (i3 < progress) {
                    canvas.drawLine(sin, cos, sin2, cos2, this.d);
                } else {
                    canvas.drawLine(sin, cos, sin2, cos2, this.e);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    private void e(Canvas canvas) {
        canvas.drawArc(this.f3245a, -90.0f, 360.0f, false, this.e);
        canvas.drawArc(this.f3245a, -90.0f, (((float) getProgress()) * 360.0f) / ((float) getMax()), true, this.d);
    }

    private void f(Canvas canvas) {
        canvas.drawArc(this.f3245a, -90.0f, 360.0f, false, this.e);
        canvas.drawArc(this.f3246b, -90.0f, (((float) getProgress()) * 360.0f) / ((float) getMax()), false, this.d);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.j = (float) (i2 / 2);
        this.k = (float) (i3 / 2);
        this.h = Math.min(this.j, this.k);
        this.f3245a.top = this.k - this.h;
        this.f3245a.bottom = this.k + this.h;
        this.f3245a.left = this.j - this.h;
        this.f3245a.right = this.j + this.h;
        b();
        this.f3245a.inset(this.o / 2.0f, this.o / 2.0f);
        this.i = this.h - ((this.p - this.o) / 4.0f);
        this.f3246b.top = this.k - this.i;
        this.f3246b.bottom = this.k + this.i;
        this.f3246b.left = this.j - this.i;
        this.f3246b.right = this.j + this.i;
        this.f3246b.inset(this.p / 2.0f, this.p / 2.0f);
    }

    public int getBackgroundColor() {
        return this.l;
    }

    public void setBackgroundColor(int i2) {
        this.l = i2;
        this.f.setColor(i2);
        invalidate();
    }

    public void setProgressTextFormatPattern(String str) {
        this.w = str;
        invalidate();
    }

    public String getProgressTextFormatPattern() {
        return this.w;
    }

    public void setProgressStrokeWidth(float f2) {
        this.o = f2;
        this.f3245a.inset(this.o / 2.0f, this.o / 2.0f);
        invalidate();
    }

    public float getProgressStrokeWidth() {
        return this.o;
    }

    public void setProgressTextSize(float f2) {
        this.q = f2;
        invalidate();
    }

    public float getProgressTextSize() {
        return this.q;
    }

    public void setProgressStartColor(int i2) {
        this.r = i2;
        b();
        invalidate();
    }

    public int getProgressStartColor() {
        return this.r;
    }

    public void setProgressEndColor(int i2) {
        this.s = i2;
        b();
        invalidate();
    }

    public int getProgressEndColor() {
        return this.s;
    }

    public void setProgressTextColor(int i2) {
        this.t = i2;
        invalidate();
    }

    public int getProgressTextColor() {
        return this.t;
    }

    public void setProgressBackgroundColor(int i2) {
        this.u = i2;
        this.e.setColor(this.u);
        invalidate();
    }

    public int getProgressBackgroundColor() {
        return this.u;
    }

    public int getLineCount() {
        return this.m;
    }

    public void setLineCount(int i2) {
        this.m = i2;
        invalidate();
    }

    public float getLineWidth() {
        return this.n;
    }

    public void setLineWidth(float f2) {
        this.n = f2;
        invalidate();
    }

    public int getStyle() {
        return this.x;
    }

    public void setStyle(int i2) {
        this.x = i2;
        this.d.setStyle(this.x == 1 ? Paint.Style.FILL : Paint.Style.STROKE);
        this.e.setStyle(this.x == 1 ? Paint.Style.FILL : Paint.Style.STROKE);
        invalidate();
    }

    public int getShader() {
        return this.y;
    }

    public void setShader(int i2) {
        this.y = i2;
        b();
        invalidate();
    }

    public Paint.Cap getCap() {
        return this.z;
    }

    public void setCap(Paint.Cap cap) {
        this.z = cap;
        this.d.setStrokeCap(cap);
        this.e.setStrokeCap(cap);
        invalidate();
    }
}
