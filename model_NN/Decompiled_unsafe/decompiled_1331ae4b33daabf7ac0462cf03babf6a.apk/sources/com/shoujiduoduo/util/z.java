package com.shoujiduoduo.util;

import com.shoujiduoduo.player.PlayerService;

/* compiled from: PlayerServiceUtil */
public class z {

    /* renamed from: b  reason: collision with root package name */
    private static z f3296b = null;

    /* renamed from: a  reason: collision with root package name */
    private PlayerService f3297a;

    private z() {
    }

    public static z a() {
        if (f3296b == null) {
            f3296b = new z();
        }
        return f3296b;
    }

    public void a(PlayerService playerService) {
        this.f3297a = playerService;
    }

    public PlayerService b() {
        return this.f3297a;
    }

    public void c() {
        this.f3297a = null;
        f3296b = null;
    }
}
