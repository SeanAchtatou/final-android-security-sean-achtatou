package com.shoujiduoduo.util;

import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: Base64Coder */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f3032a = new char[64];

    /* renamed from: b  reason: collision with root package name */
    private static byte[] f3033b = new byte[128];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            f3032a[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            f3032a[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            f3032a[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i2 = i + 1;
        f3032a[i] = '+';
        int i3 = i2 + 1;
        f3032a[i2] = '/';
        for (int i4 = 0; i4 < f3033b.length; i4++) {
            f3033b[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            f3033b[f3032a[i5]] = (byte) i5;
        }
    }

    public static String a(String str, String str2, String str3) {
        try {
            byte[] bytes = str.getBytes(str2);
            return new String(a(bytes, bytes.length, str3));
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static char[] a(byte[] bArr, int i, String str) {
        byte b2;
        byte b3;
        char c;
        char c2;
        if (str != null && !str.equals("")) {
            byte[] bytes = str.getBytes();
            int i2 = 0;
            while (i2 < bArr.length) {
                int i3 = 0;
                while (i3 < bytes.length && i2 < bArr.length) {
                    bArr[i2] = (byte) (bArr[i2] ^ bytes[i3]);
                    i3++;
                    i2++;
                }
            }
        }
        int i4 = ((i * 4) + 2) / 3;
        char[] cArr = new char[(((i + 2) / 3) * 4)];
        int i5 = 0;
        int i6 = 0;
        while (i6 < i) {
            int i7 = i6 + 1;
            byte b4 = bArr[i6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (i7 < i) {
                b2 = bArr[i7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                i7++;
            } else {
                b2 = 0;
            }
            if (i7 < i) {
                i6 = i7 + 1;
                b3 = bArr[i7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            } else {
                i6 = i7;
                b3 = 0;
            }
            int i8 = b4 >>> 2;
            int i9 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i11 = i5 + 1;
            cArr[i5] = f3032a[i8];
            int i12 = i11 + 1;
            cArr[i11] = f3032a[i9];
            if (i12 < i4) {
                c = f3032a[i10];
            } else {
                c = '=';
            }
            cArr[i12] = c;
            int i13 = i12 + 1;
            if (i13 < i4) {
                c2 = f3032a[b5];
            } else {
                c2 = '=';
            }
            cArr[i13] = c2;
            i5 = i13 + 1;
        }
        return cArr;
    }
}
