package com.shoujiduoduo.util.b;

import android.text.TextUtils;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.List;

/* compiled from: RequstResult */
public class c {

    /* compiled from: RequstResult */
    public static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3044a;
    }

    /* compiled from: RequstResult */
    public static class aa extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3045a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class ab {

        /* renamed from: a  reason: collision with root package name */
        public String f3046a = "";

        /* renamed from: b  reason: collision with root package name */
        public String f3047b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";
        public String h = "";
    }

    /* compiled from: RequstResult */
    public static class ac extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3048a;
    }

    /* compiled from: RequstResult */
    public static class ad extends b {

        /* renamed from: a  reason: collision with root package name */
        public p f3049a;
    }

    /* compiled from: RequstResult */
    public static class ag extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3053a = "";
    }

    /* compiled from: RequstResult */
    public static class ah extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3054a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class ai extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3055a;
    }

    /* compiled from: RequstResult */
    public static class aj {

        /* renamed from: a  reason: collision with root package name */
        public String f3056a;

        /* renamed from: b  reason: collision with root package name */
        public String f3057b;
        public String c;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class ak {

        /* renamed from: a  reason: collision with root package name */
        public String f3058a = "";

        /* renamed from: b  reason: collision with root package name */
        public String f3059b = "";
        public String c = "";
        public String d = "";
        public String e = "";
    }

    /* renamed from: com.shoujiduoduo.util.b.c$c  reason: collision with other inner class name */
    /* compiled from: RequstResult */
    public static class C0042c {

        /* renamed from: a  reason: collision with root package name */
        public String f3061a;

        /* renamed from: b  reason: collision with root package name */
        public String f3062b;
        public String c;
        public String d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class g extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3066a = "";
    }

    /* compiled from: RequstResult */
    public static class h extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3067a;
        public String d;
        public String e;
    }

    /* compiled from: RequstResult */
    public static class j extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3069a;
    }

    /* compiled from: RequstResult */
    public static class k extends b {

        /* renamed from: a  reason: collision with root package name */
        public C0042c f3070a;
        public String d;
    }

    /* compiled from: RequstResult */
    public static class l extends b {

        /* renamed from: a  reason: collision with root package name */
        public BizInfo f3071a;
        public BizInfo d;
        public MusicInfo e;
        public String f;
        public String g;
    }

    /* compiled from: RequstResult */
    public static class m extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3072a = "";
        public int d;
        public String e = "";
    }

    /* compiled from: RequstResult */
    public static class n extends b {

        /* renamed from: a  reason: collision with root package name */
        public a f3073a;
        public String d;

        /* compiled from: RequstResult */
        public enum a {
            f3074a,
            wap,
            sms_code,
            all
        }
    }

    /* compiled from: RequstResult */
    public static class p {

        /* renamed from: a  reason: collision with root package name */
        public String f3077a;

        /* renamed from: b  reason: collision with root package name */
        public String f3078b;
    }

    /* compiled from: RequstResult */
    public static class q extends b {

        /* renamed from: a  reason: collision with root package name */
        public y f3079a;
    }

    /* compiled from: RequstResult */
    public static class r extends b {

        /* renamed from: a  reason: collision with root package name */
        public boolean f3080a;
    }

    /* compiled from: RequstResult */
    public static class s extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f3081a;
        public ak[] d;
    }

    /* compiled from: RequstResult */
    public static class t extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3082a;
        public String d;
        public String e;
        public String f;
    }

    /* compiled from: RequstResult */
    public static class u extends b {

        /* renamed from: a  reason: collision with root package name */
        public int f3083a;
        public ab[] d;
    }

    /* compiled from: RequstResult */
    public static class v extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3084a;
        public boolean d;
    }

    /* compiled from: RequstResult */
    public static class w extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3085a = "";
    }

    /* compiled from: RequstResult */
    public static class x extends b {

        /* renamed from: a  reason: collision with root package name */
        public ae f3086a;
        public String d = "";
    }

    /* compiled from: RequstResult */
    public static class y {

        /* renamed from: a  reason: collision with root package name */
        public String f3087a;

        /* renamed from: b  reason: collision with root package name */
        public String f3088b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public String h;
    }

    /* compiled from: RequstResult */
    public static class b {

        /* renamed from: b  reason: collision with root package name */
        public String f3060b;
        public String c;

        public b() {
            this.f3060b = "";
            this.c = "";
        }

        public b(String str, String str2) {
            this.f3060b = str;
            this.c = str2;
        }

        public String a() {
            return this.f3060b;
        }

        public String b() {
            return this.c;
        }

        public String toString() {
            return "code:" + this.f3060b + ", msg:" + this.c;
        }

        public boolean c() {
            return this.f3060b.equals("0000") || this.f3060b.equals("000000") || this.f3060b.equals("0");
        }
    }

    /* compiled from: RequstResult */
    public static class ae {

        /* renamed from: a  reason: collision with root package name */
        public String f3050a = "";

        /* renamed from: b  reason: collision with root package name */
        public String f3051b = "";
        public String c = "";
        public String d = "";
        public String e = "";
        public String f = "";
        public String g = "";
        public String h = "";
        public String i = "";
        public String j = "";

        public String a() {
            return this.f;
        }

        public String b() {
            return this.f3050a;
        }

        public String c() {
            return this.f3051b;
        }

        public String d() {
            return this.d;
        }

        public String e() {
            return this.g;
        }
    }

    /* compiled from: RequstResult */
    public static class z extends b {

        /* renamed from: a  reason: collision with root package name */
        public List<ae> f3089a;

        public List<ae> d() {
            return this.f3089a;
        }
    }

    /* compiled from: RequstResult */
    public static class d extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f3063a;
        public b d;

        public boolean d() {
            return this.f3063a != null && this.f3063a.c();
        }

        public boolean e() {
            return this.d != null && this.d.c();
        }
    }

    /* compiled from: RequstResult */
    public static class o extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3076a;

        public String d() {
            return this.f3076a;
        }
    }

    /* compiled from: RequstResult */
    public static class i extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3068a;

        public String d() {
            return this.f3068a;
        }
    }

    /* compiled from: RequstResult */
    public static class e extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f3064a;
        public b d;
        public b e;

        public boolean d() {
            if (this.f3064a != null) {
                return this.f3064a.a().equals("0000");
            }
            return false;
        }

        public boolean e() {
            if (this.d instanceof v) {
                return ((v) this.d).d;
            }
            return false;
        }

        public boolean f() {
            if (this.e instanceof v) {
                return ((v) this.e).d;
            }
            return false;
        }
    }

    /* compiled from: RequstResult */
    public static class af extends b {

        /* renamed from: a  reason: collision with root package name */
        public String f3052a = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;

        public boolean d() {
            return this.f3052a.equals("0") || this.f3052a.equals("2") || this.f3052a.equals("4");
        }
    }

    /* compiled from: RequstResult */
    public static class f extends b {

        /* renamed from: a  reason: collision with root package name */
        public b f3065a;
        public b d;
        public b e;
        public b f;

        public boolean d() {
            if (this.f3065a == null || !this.f3065a.c() || !(this.f3065a instanceof af)) {
                return false;
            }
            return ((af) this.f3065a).d();
        }

        public boolean e() {
            if (this.d == null || !this.d.c() || !(this.d instanceof r)) {
                return false;
            }
            return ((r) this.d).f3080a;
        }

        public String f() {
            if (this.f == null || !this.f.c() || !(this.f instanceof ah)) {
                return "";
            }
            return ((ah) this.f).f3054a;
        }

        public String g() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ai)) {
                return "";
            }
            return ((ai) this.e).f3055a;
        }

        public boolean h() {
            if (this.e == null || !this.e.c() || !(this.e instanceof ai)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:false");
                return false;
            }
            ai aiVar = (ai) this.e;
            com.shoujiduoduo.base.a.a.a("RequestResult", "is4G, return:" + aiVar.f3055a.equals("4"));
            return aiVar.f3055a.equals("4");
        }

        public boolean i() {
            return h() && !com.umeng.a.a.a().a(RingDDApp.c(), "cucc_all_4g_open").equals("false");
        }

        public boolean j() {
            String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "cu_4g_cailing_free_switch");
            if (h() && !a2.equals("false")) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true");
                return true;
            } else if (this.f == null || !this.f.c() || !(this.f instanceof ah)) {
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false 2");
                return false;
            } else {
                ah ahVar = (ah) this.f;
                String a3 = com.umeng.a.a.a().a(RingDDApp.c(), "cu_3g_cailing_free");
                if (TextUtils.isEmpty(a3) || !a3.contains(ahVar.f3054a)) {
                    com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return false");
                    return false;
                }
                com.shoujiduoduo.base.a.a.a("RequestResult", "isFreeCailingQualified, return true 2, provinceId:" + ahVar.f3054a + ", ids:" + a3);
                return true;
            }
        }
    }
}
