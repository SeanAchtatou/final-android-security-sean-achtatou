package com.shoujiduoduo.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import cn.banshenggua.aichang.player.PlayerService;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* compiled from: PushConfigUtils */
public class aa {

    /* renamed from: a  reason: collision with root package name */
    private Context f2979a;

    public aa(Context context) {
        this.f2979a = context;
    }

    public void a(String str) {
        Document document;
        try {
            document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(str)));
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            document = null;
        } catch (SAXException e2) {
            e2.printStackTrace();
            document = null;
        } catch (IOException e3) {
            e3.printStackTrace();
            document = null;
        }
        if (document != null) {
            Element documentElement = document.getDocumentElement();
            if (documentElement == null) {
                a.a("PushConfigUtils", "push task failed to parse XML message!");
                return;
            }
            a.a("PushConfigUtils", "push task 2");
            a(documentElement);
        }
    }

    private void a(Element element) {
        a.a("PushConfigUtils", "==========parseMsg ");
        NodeList elementsByTagName = element.getElementsByTagName("msg");
        if (elementsByTagName != null && elementsByTagName.getLength() > 0) {
            Node item = elementsByTagName.item(0);
            a.a("PushConfigUtils", "push task 3");
            NamedNodeMap attributes = item.getAttributes();
            String a2 = f.a(attributes, "action");
            if (a2.equalsIgnoreCase("search") || a2.equalsIgnoreCase("music_album") || a2.equalsIgnoreCase(PlayerService.ACTION_PLAY) || a2.equalsIgnoreCase("update") || a2.equalsIgnoreCase("ad") || a2.equalsIgnoreCase("webview")) {
                a.a("PushConfigUtils", "push task, action = " + a2);
                String a3 = f.a(attributes, "para");
                String a4 = f.a(attributes, "title");
                Intent intent = new Intent(this.f2979a, RingToneDuoduoActivity.class);
                intent.addFlags(872415232);
                intent.putExtra("action", a2);
                if (a3 != null && a3.length() > 0) {
                    intent.putExtra("para", a3);
                }
                if (!TextUtils.isEmpty(a4)) {
                    intent.putExtra("title", a4);
                }
                this.f2979a.startActivity(intent);
                return;
            }
            a.e("PushConfigUtils", "push task, not support action = " + a2);
        }
    }
}
