package com.shoujiduoduo.util;

import android.util.Base64;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.UserData;
import com.shoujiduoduo.base.bean.a;
import com.shoujiduoduo.base.bean.b;
import com.shoujiduoduo.base.bean.c;
import com.shoujiduoduo.base.bean.f;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.constant.WBPageConstants;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/* compiled from: DataParse */
public class i {
    public static f<b> a(String str) {
        try {
            return c(new FileInputStream(str));
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
        }
        return null;
    }

    public static UserData b(String str) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        NodeList elementsByTagName;
        if (ag.c(str)) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(new InputSource(new StringReader(str)))) == null || (documentElement = parse.getDocumentElement()) == null || (elementsByTagName = documentElement.getElementsByTagName("user")) == null || elementsByTagName.getLength() == 0) {
                return null;
            }
            NamedNodeMap attributes = elementsByTagName.item(0).getAttributes();
            UserData userData = new UserData();
            a(attributes, userData);
            return userData;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<c> a(InputStream inputStream, boolean z) {
        boolean z2;
        JSONArray optJSONArray;
        String a2 = t.a(inputStream);
        if (a2 == null) {
            return null;
        }
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(a2).nextValue();
            f<c> fVar = new f<>();
            ArrayList<T> arrayList = new ArrayList<>();
            JSONObject optJSONObject = jSONObject.optJSONObject("hot");
            if (!(optJSONObject == null || (optJSONArray = optJSONObject.optJSONArray("list")) == null)) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    JSONObject optJSONObject2 = optJSONArray.optJSONObject(i);
                    if (optJSONObject2 != null) {
                        c cVar = new c();
                        if (i == 0 && !z) {
                            cVar.f1961a = "最热评论";
                        }
                        a(optJSONObject2, cVar);
                        arrayList.add(cVar);
                    }
                }
            }
            fVar.f1965a = arrayList.size();
            JSONObject optJSONObject3 = jSONObject.optJSONObject("new");
            if (optJSONObject3 != null) {
                if (optJSONObject3.optInt("hasmore") != 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                JSONArray optJSONArray2 = optJSONObject3.optJSONArray("list");
                if (optJSONArray2 != null) {
                    for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                        JSONObject optJSONObject4 = optJSONArray2.optJSONObject(i2);
                        if (optJSONObject4 != null) {
                            c cVar2 = new c();
                            if (i2 == 0 && !z) {
                                cVar2.f1961a = "最新评论";
                            }
                            a(optJSONObject4, cVar2);
                            arrayList.add(cVar2);
                        }
                    }
                }
            } else {
                z2 = false;
            }
            fVar.c = arrayList;
            fVar.d = z2;
            return fVar;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void a(JSONObject jSONObject, c cVar) {
        cVar.f1962b = jSONObject.optString("rid");
        cVar.c = jSONObject.optString("uid");
        cVar.d = jSONObject.optString("tuid");
        cVar.e = jSONObject.optString("ddid");
        cVar.f = jSONObject.optString("tcid");
        cVar.h = jSONObject.optString("comment");
        cVar.i = jSONObject.optString("tcomment");
        cVar.j = jSONObject.optString(SelectCountryActivity.EXTRA_COUNTRY_NAME);
        cVar.l = jSONObject.optString("head_url");
        cVar.k = jSONObject.optString("tname");
        cVar.m = jSONObject.optString("thead_url");
        cVar.g = jSONObject.optString("cid");
        cVar.n = jSONObject.optString("createtime");
        cVar.o = jSONObject.optInt("upvote");
    }

    public static f<UserData> a(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("user");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                UserData userData = new UserData();
                a(attributes, userData);
                arrayList.add(userData);
            }
            f<UserData> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<a> b(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("artist");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                a aVar = new a();
                a(attributes, aVar);
                arrayList.add(aVar);
            }
            f<a> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<b> c(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("collect");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                b bVar = new b();
                a(attributes, bVar);
                arrayList.add(bVar);
            }
            f<b> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<RingData> d(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                RingData ringData = new RingData();
                ringData.a(attribute2);
                a(attributes, ringData);
                arrayList.add(ringData);
            }
            f<RingData> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<RingData> e(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            HashMap<String, String> hashMap = new HashMap<>();
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                RingData ringData = new RingData();
                ringData.a(attribute2);
                a(attributes, ringData);
                String a3 = f.a(attributes, "frid", "");
                if (!ag.c(a3)) {
                    hashMap.put(a3, ringData.g);
                }
                arrayList.add(ringData);
            }
            f<RingData> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.f1966b = hashMap;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    public static f<MakeRingData> f(InputStream inputStream) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || inputStream == null || (parse = newDocumentBuilder.parse(inputStream)) == null || (documentElement = parse.getDocumentElement()) == null) {
                return null;
            }
            boolean equalsIgnoreCase = documentElement.getAttribute("hasmore").equalsIgnoreCase("true");
            String attribute = documentElement.getAttribute("area");
            String attribute2 = documentElement.getAttribute("baseurl");
            String attribute3 = documentElement.getAttribute("sig");
            int a2 = q.a(documentElement.getAttribute(WBPageConstants.ParamKey.PAGE), -1);
            NodeList elementsByTagName = documentElement.getElementsByTagName("ring");
            if (elementsByTagName == null) {
                return null;
            }
            ArrayList<T> arrayList = new ArrayList<>();
            com.shoujiduoduo.base.a.a.a("DataParse", "parseContent: listRing size = " + arrayList.size());
            for (int i = 0; i < elementsByTagName.getLength(); i++) {
                NamedNodeMap attributes = elementsByTagName.item(i).getAttributes();
                MakeRingData makeRingData = new MakeRingData();
                makeRingData.a(attribute2);
                a(attributes, makeRingData);
                makeRingData.c = f.a(attributes, "date");
                makeRingData.f1951a = q.a(f.a(attributes, "upload"), 0);
                makeRingData.d = q.a(f.a(attributes, "makeType"), 0);
                makeRingData.o = f.a(attributes, "localPath");
                arrayList.add(makeRingData);
            }
            f<MakeRingData> fVar = new f<>();
            fVar.c = arrayList;
            fVar.d = equalsIgnoreCase;
            fVar.e = attribute;
            fVar.f = attribute2;
            fVar.g = attribute3;
            fVar.h = a2;
            return fVar;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (DOMException e2) {
            e2.printStackTrace();
            return null;
        } catch (SAXException e3) {
            e3.printStackTrace();
            return null;
        } catch (ParserConfigurationException e4) {
            e4.printStackTrace();
            return null;
        } catch (ArrayIndexOutOfBoundsException e5) {
            e5.printStackTrace();
            return null;
        }
    }

    private static void a(NamedNodeMap namedNodeMap, UserData userData) {
        boolean z;
        userData.f1955a = f.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        userData.f1955a = new String(Base64.decode(userData.f1955a, 0));
        userData.c = f.a(namedNodeMap, "uid");
        userData.i = f.a(namedNodeMap, "rings_num", 0);
        userData.j = f.a(namedNodeMap, "follower_num", 0);
        userData.k = f.a(namedNodeMap, "following_num", 0);
        userData.f1956b = f.a(namedNodeMap, "head_url");
        userData.d = f.a(namedNodeMap, "sex", "未知");
        userData.g = f.a(namedNodeMap, "ddid");
        userData.f = q.c(f.a(namedNodeMap, "intro"));
        userData.e = f.a(namedNodeMap, "bgurl");
        if (f.a(namedNodeMap, "followed", 0) != 0) {
            z = true;
        } else {
            z = false;
        }
        userData.h = z;
        userData.l = f.a(namedNodeMap, "superuser", 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.f.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, a aVar) {
        aVar.f1957a = f.a(namedNodeMap, "pic");
        aVar.f1958b = f.a(namedNodeMap, "isNew", false);
        aVar.c = q.a(f.a(namedNodeMap, "sale"), 100);
        aVar.d = f.a(namedNodeMap, "work");
        aVar.e = f.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        aVar.f = f.a(namedNodeMap, "id");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean
     arg types: [org.w3c.dom.NamedNodeMap, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, int):int
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.f.a(android.content.Context, java.lang.String, int):void
      com.shoujiduoduo.util.f.a(org.w3c.dom.NamedNodeMap, java.lang.String, boolean):boolean */
    private static void a(NamedNodeMap namedNodeMap, b bVar) {
        bVar.f1959a = f.a(namedNodeMap, "pic");
        bVar.h = f.a(namedNodeMap, "artist");
        bVar.f1960b = f.a(namedNodeMap, "title");
        bVar.c = f.a(namedNodeMap, "content");
        bVar.d = f.a(namedNodeMap, "time");
        bVar.e = f.a(namedNodeMap, "isnew", false);
        bVar.f = f.a(namedNodeMap, "fav");
        bVar.g = f.a(namedNodeMap, "id");
    }

    private static void a(NamedNodeMap namedNodeMap, RingData ringData) {
        ringData.e = f.a(namedNodeMap, SelectCountryActivity.EXTRA_COUNTRY_NAME);
        ringData.f = f.a(namedNodeMap, "artist");
        ringData.j = f.a(namedNodeMap, "uid");
        ringData.l = q.a(f.a(namedNodeMap, "duration"), 0);
        ringData.i = q.a(f.a(namedNodeMap, WBConstants.GAME_PARAMS_SCORE), 0);
        ringData.m = q.a(f.a(namedNodeMap, "playcnt"), 0);
        ringData.g = f.a(namedNodeMap, "rid");
        ringData.h = f.a(namedNodeMap, "bdurl");
        ringData.a(q.a(f.a(namedNodeMap, "hbr"), 0));
        ringData.b(f.a(namedNodeMap, "hurl"));
        ringData.b(q.a(f.a(namedNodeMap, "lbr"), 0));
        ringData.c(f.a(namedNodeMap, "lurl"));
        ringData.c(q.a(f.a(namedNodeMap, "mp3br"), 0));
        ringData.d(f.a(namedNodeMap, "mp3url"));
        ringData.n = q.a(f.a(namedNodeMap, "isnew"), 0);
        ringData.p = f.a(namedNodeMap, "cid");
        ringData.q = f.a(namedNodeMap, "valid");
        ringData.t = f.a(namedNodeMap, "singerId");
        ringData.r = q.a(f.a(namedNodeMap, "price"), 0);
        ringData.s = q.a(f.a(namedNodeMap, "hasmedia"), 0);
        ringData.u = f.a(namedNodeMap, "ctcid");
        ringData.v = f.a(namedNodeMap, "ctvalid");
        ringData.w = q.a(f.a(namedNodeMap, "ctprice"), 0);
        ringData.x = q.a(f.a(namedNodeMap, "cthasmedia"), 0);
        ringData.y = q.a(f.a(namedNodeMap, "ctvip"), 0);
        ringData.z = f.a(namedNodeMap, "wavurl");
        ringData.A = q.a(f.a(namedNodeMap, "cuvip"), 0);
        ringData.B = f.a(namedNodeMap, "cuftp");
        ringData.C = f.a(namedNodeMap, "cucid");
        ringData.E = f.a(namedNodeMap, "cusid");
        ringData.F = f.a(namedNodeMap, "cuurl");
        ringData.D = f.a(namedNodeMap, "cuvalid");
        ringData.G = q.a(f.a(namedNodeMap, "hasshow"), 0);
        ringData.H = q.a(f.a(namedNodeMap, "ishot"), 0);
        ringData.k = f.a(namedNodeMap, "head_url");
        ringData.I = f.a(namedNodeMap, "comment_num", 0);
    }
}
