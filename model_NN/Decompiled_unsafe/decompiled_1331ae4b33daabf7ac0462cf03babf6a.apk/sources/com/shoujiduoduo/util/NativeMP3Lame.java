package com.shoujiduoduo.util;

public class NativeMP3Lame {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2960a = x.a("mp3lame");

    /* renamed from: b  reason: collision with root package name */
    private static NativeMP3Lame f2961b = null;

    public native void destroyEncoder();

    public native int encodeSamples(short[] sArr, int i);

    public native int initEncoder(int i, int i2, int i3, int i4, int i5);

    public native int setTargetFile(String str);

    public static NativeMP3Lame a() {
        if (f2961b == null) {
            f2961b = new NativeMP3Lame();
        }
        return f2961b;
    }
}
