package com.shoujiduoduo.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

/* compiled from: SharedPref */
public class ad {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences.Editor f2995a = null;

    /* renamed from: b  reason: collision with root package name */
    private static SharedPreferences f2996b = null;
    private static Context c = null;

    public static void a(Context context) {
        c = context;
    }

    public static String a(Context context, String str) {
        return a(context, str, (String) null);
    }

    public static String a(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (f2996b == null) {
            f2996b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return f2996b.getString(str, str2);
    }

    public static boolean b(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (f2995a == null) {
            f2995a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2995a.putString(str, str2);
        return f2995a.commit();
    }

    public static boolean c(Context context, String str, String str2) {
        if (context == null) {
            context = c;
        }
        if (f2995a == null) {
            f2995a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2995a.putString(str, str2);
        return a(f2995a);
    }

    public static int a(Context context, String str, int i) {
        if (context == null) {
            context = c;
        }
        if (f2996b == null) {
            f2996b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return f2996b.getInt(str, i);
    }

    public static boolean b(Context context, String str, int i) {
        if (context == null) {
            context = c;
        }
        if (f2995a == null) {
            f2995a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2995a.putInt(str, i);
        return a(f2995a);
    }

    public static long a(Context context, String str, long j) {
        if (context == null) {
            context = c;
        }
        if (f2996b == null) {
            f2996b = context.getSharedPreferences("ring.shoujiduoduo.com", 0);
        }
        return f2996b.getLong(str, j);
    }

    public static boolean b(Context context, String str, long j) {
        if (context == null) {
            context = c;
        }
        if (f2995a == null) {
            f2995a = context.getSharedPreferences("ring.shoujiduoduo.com", 0).edit();
        }
        f2995a.putLong(str, j);
        return a(f2995a);
    }

    @TargetApi(9)
    private static boolean a(SharedPreferences.Editor editor) {
        if (Build.VERSION.SDK_INT < 9) {
            return editor.commit();
        }
        editor.apply();
        return true;
    }
}
