package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;

public class NativeDES {

    /* renamed from: a  reason: collision with root package name */
    private static String f2958a = "NativeDES";

    /* renamed from: b  reason: collision with root package name */
    private static boolean f2959b = x.a("url_encode");

    public native String Encrypt(String str);

    static {
        a.a(f2958a, "load url_encode lib, res:" + f2959b);
    }

    public static boolean a() {
        return f2959b;
    }
}
