package com.shoujiduoduo.util;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import com.shoujiduoduo.a.b.b;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.base.bean.k;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.d;
import com.tencent.open.SocialConstants;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/* compiled from: DDIpUtils */
public class g {
    public static boolean a(String str) {
        if (!ag.c(str)) {
            return str.startsWith("ddip://");
        }
        return false;
    }

    public static void a(Activity activity, String str) {
        if (str.startsWith("ddip://")) {
            int indexOf = str.indexOf("//");
            int indexOf2 = str.indexOf("/", indexOf + 2);
            int indexOf3 = str.indexOf("?", indexOf2 + 1);
            if (indexOf != -1 && indexOf2 != -1 && indexOf3 != -1) {
                String str2 = "";
                try {
                    str2 = str.substring(indexOf2 + 1, indexOf3);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!TextUtils.isEmpty(str2)) {
                    String str3 = "";
                    if (str.indexOf("=") != -1) {
                        try {
                            str3 = URLDecoder.decode(str.substring(str.indexOf("=") + 1), "UTF-8");
                            a.a("DDIpUtils", "param:" + str3);
                        } catch (UnsupportedEncodingException e2) {
                            e2.printStackTrace();
                        }
                    }
                    if (str2.equals("w2c_share")) {
                        b(activity, str3);
                    } else if (str2.equals("w2c_setRing")) {
                        c(str3);
                    } else if (str2.equals("w2c_open_webview")) {
                        PlayerService b2 = z.a().b();
                        if (b2 != null && b2.l()) {
                            b2.m();
                        }
                        c(activity, str3);
                    } else if (str2.equals("w2c_create_story")) {
                        d(activity, str3);
                    } else {
                        a.e("DDIpUtils", "not support method, " + str2);
                    }
                }
            }
        } else {
            a.e("DDIpUtils", "not correct dd protocol");
        }
    }

    public static String b(String str) {
        String str2;
        String str3;
        int indexOf;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        k c = b.g().c();
        String a2 = c.a();
        a.a("DDIpUtils", "uid:" + a2);
        if (!TextUtils.isEmpty(a2) && (indexOf = a2.indexOf("_")) > 0) {
            a2 = a2.substring(indexOf + 1);
        }
        a.a("DDIpUtils", "dduid:" + a2);
        switch (c.e()) {
            case 2:
                str2 = a2;
                str3 = "qq";
                break;
            case 3:
                str2 = a2;
                str3 = "wb";
                break;
            case 4:
            default:
                str2 = "";
                str3 = "dd";
                break;
            case 5:
                str2 = a2;
                str3 = "wx";
                break;
        }
        sb.append("&dduid=").append(str2);
        a.a("DDIpUtils", "dddid:" + f.a(RingDDApp.c()));
        sb.append("&dddid=").append(f.a(RingDDApp.c()));
        a.a("DDIpUtils", "ddut:" + str3);
        sb.append("&ddut=").append(str3);
        try {
            sb.append("&nickname=").append(URLEncoder.encode(c.b(), "UTF-8"));
            sb.append("&portrait=").append(URLEncoder.encode(c.c(), "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        a.a("DDIpUtils", "url:" + sb.toString());
        return sb.toString();
    }

    private static void b(Activity activity, String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            ai.a().a(activity, jSONObject.optString("title"), jSONObject.optString(SocialConstants.PARAM_APP_DESC), jSONObject.optString("imgUrl"), jSONObject.optString("link"));
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
    }

    private static void c(String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            final String optString = jSONObject.optString("id");
            final String optString2 = jSONObject.optString("title");
            final String optString3 = jSONObject.optString("artist");
            final String optString4 = jSONObject.optString("url");
            final String str2 = "" + (jSONObject.optInt("duration") * 1000);
            h.a(new Runnable() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean):boolean
                 arg types: [java.lang.String, java.lang.String, int]
                 candidates:
                  com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, org.json.JSONObject):java.lang.String
                  com.shoujiduoduo.util.s.a(java.lang.String, boolean, java.lang.String):java.lang.String
                  com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.s$a):void
                  com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, java.lang.String):void
                  com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean):boolean */
                public void run() {
                    d.a("正在下载文件，请稍候...");
                    String str = k.a(2) + optString + "." + p.b(optString4);
                    if (!s.a(optString4, str, true) || !ac.a(1, str, optString2, optString3, str2)) {
                        d.a("铃声设置失败");
                    } else {
                        d.a("铃声设置成功");
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
    }

    private static void c(Activity activity, String str) {
        try {
            JSONObject jSONObject = (JSONObject) new JSONTokener(str).nextValue();
            String optString = jSONObject.optString("title");
            String optString2 = jSONObject.optString("link");
            Intent intent = new Intent(activity, WebViewActivity.class);
            intent.putExtra("url", optString2);
            intent.putExtra("title", optString);
            activity.startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e2) {
        }
    }

    private static void d(Activity activity, String str) {
    }
}
