package com.shoujiduoduo.util;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.cmsc.cmmusic.init.GetAppInfo;
import com.cmsc.cmmusic.init.GetAppInfoInterface;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.base.bean.h;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.umeng.analytics.b;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Locale;
import java.util.zip.GZIPInputStream;
import org.json.JSONObject;

/* compiled from: HttpRequest */
public class s {

    /* renamed from: a  reason: collision with root package name */
    public static final NativeDES f3220a = new NativeDES();

    /* renamed from: b  reason: collision with root package name */
    private static final String f3221b = f.a();
    private static final String c = f.o();
    private static final String d = f.n();
    private static final String e = f.r();
    private static final String f = f.c().replaceAll(":", "");
    private static final String g = URLEncoder.encode(f.i());
    /* access modifiers changed from: private */
    public static final String h = ("user=" + f3221b + "&prod=" + c + "&isrc=" + d + "&mac=" + (TextUtils.isEmpty(f.trim()) ? "unknown_mac" : f) + "&dev=" + g + "&vc=" + f.p() + "&loc=" + Locale.getDefault().getCountry() + "&sp=" + f.s().toString());

    /* compiled from: HttpRequest */
    public interface a {
        void a(String str);

        void a(String str, String str2);
    }

    public static String a() {
        return h;
    }

    private static byte[] k(String str) {
        String str2 = "http://" + str + "/ring_enc.php?" + "cmd=getconfig&q=" + j(h + "&type=getconfig");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "request config, url:" + str2);
        return ao.b(str2);
    }

    public static String b() {
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetConfig: " + h + "&type=getconfig");
        String a2 = ab.a().a("dd_server_ip_1");
        String a3 = ab.a().a("dd_server_ip_2");
        String a4 = ab.a().a("dd_server_ip_3");
        HashMap hashMap = new HashMap();
        byte[] k = k("www.shoujiduoduo.com");
        if (k == null) {
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetConfig retry 1");
            k = k(a2);
            if (k == null) {
                com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetConfig retry 2");
                k = k(a3);
                if (k == null) {
                    com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetConfig retry 3");
                    k = k(a4);
                    if (k == null) {
                        hashMap.put(Parameters.RESOLUTION, "fail");
                        hashMap.put("network", NetworkStateUtil.d());
                    } else {
                        hashMap.put(Parameters.RESOLUTION, "ip3-success");
                    }
                } else {
                    hashMap.put(Parameters.RESOLUTION, "ip2-success");
                }
            } else {
                hashMap.put(Parameters.RESOLUTION, "ip1-success");
            }
        } else {
            hashMap.put(Parameters.RESOLUTION, "domain_name-success");
        }
        if (NetworkStateUtil.a()) {
            b.a(RingDDApp.c(), "get_server_config_new2", hashMap);
        }
        if (k != null) {
            return ag.e(new String(k));
        }
        return "";
    }

    public static String a(String str) {
        String str2 = "http://" + l.a().c() + "/ringv1/suggest.php?";
        try {
            str2 = str2 + "word=" + URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        byte[] b2 = ao.b(str2 + "&count=10");
        if (b2 == null) {
            return null;
        }
        return ag.e(new String(b2));
    }

    public static String b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=geturlv1").append(str);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetAntiStealLink: paraString = " + sb.toString());
        return c("geturlv1", sb.toString());
    }

    public static String a(String str, boolean z, String str2) {
        String imsi = GetAppInfoInterface.getIMSI(RingDDApp.c());
        if (ag.c(imsi)) {
            imsi = GetAppInfo.getIMSI(RingDDApp.c());
        }
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=cailingurl&mode=").append(str2).append("&cid=").append(str).append("&nocache=").append(z ? "1" : "0").append(TextUtils.isEmpty(imsi) ? "" : "&imsi=" + imsi);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCMCailingUrl: " + sb.toString());
        return c("cailingurl", sb.toString());
    }

    public static h a(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=cucailingurl").append("&cucid=").append(str).append("&nocache=").append(z ? "1" : "0");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCUCailingUrl, paraString:" + sb.toString());
        String c2 = c("cucailingurl", sb.toString());
        if (ag.c(c2)) {
            return null;
        }
        try {
            String[] split = c2.split("\t", 0);
            if (split.length != 3) {
                return null;
            }
            for (int i = 0; i < split.length; i++) {
                com.shoujiduoduo.base.a.a.a("HttpRequest", "result " + i + ": " + split[i]);
            }
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCUCailingUrl: url =" + split[2]);
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCUCailingUrl: bitrate =" + split[0]);
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCUCailingUrl: format =" + split[1]);
            try {
                return new h(split[2], split[1].toLowerCase(), Integer.valueOf(split[0]).intValue());
            } catch (NumberFormatException e2) {
                return null;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    public static h b(String str, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=ctcailingurl").append("&ctcid=").append(str).append("&nocache=").append(z ? "1" : "0");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCTCailingUrl, paraString:" + sb.toString());
        String c2 = c("ctcailingurl", sb.toString());
        if (c2 == null) {
            return null;
        }
        try {
            String[] split = c2.split("\t", 0);
            if (split.length != 3) {
                return null;
            }
            for (int i = 0; i < split.length; i++) {
                com.shoujiduoduo.base.a.a.a("HttpRequest", "result " + i + ": " + split[i]);
            }
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCTCailingUrl: url =" + split[2]);
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCTCailingUrl: bitrate =" + split[0]);
            com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCTCailingUrl: format =" + split[1]);
            try {
                return new h(split[2], split[1].toLowerCase(), Integer.valueOf(split[0]).intValue());
            } catch (NumberFormatException e2) {
                return null;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    public static String a(String str, String str2, int i, int i2) {
        StringBuilder sb = new StringBuilder();
        try {
            StringBuilder append = sb.append(h).append("&type=search&keyword=");
            if (!NativeDES.a()) {
                str = URLEncoder.encode(str, "GBK");
            }
            append.append(str).append("&src=").append(str2).append("&page=").append(i).append("&pagesize=").append(i2).append("&include=all&ctdb=1&cudb=1");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        com.shoujiduoduo.base.a.a.a("HttpRequest", "paraString before encod:" + sb.toString());
        return c("search", sb.toString());
    }

    public static String c() {
        StringBuilder sb = new StringBuilder();
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCategoryList, st:" + f.s());
        sb.append(h).append("&type=getcategory");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetCategoryList: paraString = " + sb.toString());
        return c("getcategory", sb.toString());
    }

    public static String d() {
        String str = "http://" + l.a().c() + "/ringv1/xmldata/searchad.xml";
        com.shoujiduoduo.base.a.a.a("HttpRequest", str);
        byte[] b2 = ao.b(str);
        if (b2 != null) {
            com.shoujiduoduo.base.a.a.a("HttpRequest", "get search ad success");
            return ag.e(new String(b2));
        }
        com.shoujiduoduo.base.a.a.a("HttpRequest", "get search ad failed");
        return "";
    }

    private static String c(String str, String str2) {
        String j = j(str2);
        String str3 = c(str) + j;
        com.shoujiduoduo.base.a.a.a("HttpRequest", "realRequest: url = " + str3);
        byte[] b2 = ao.b(str3);
        if (b2 == null) {
            com.shoujiduoduo.base.a.a.a("HttpRequest", "retry request");
            b2 = d(str, j);
        }
        if (b2 != null) {
            return ag.e(new String(b2));
        }
        return "";
    }

    private static byte[] d(String str, String str2) {
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "duoduo_dns");
        if (a2 == null || a2.equals("")) {
            return null;
        }
        String str3 = e(str, a2) + str2;
        com.shoujiduoduo.base.a.a.a("HttpRequest", "retryRequest, url:" + str3);
        return ao.b(str3);
    }

    public static String c(String str) {
        if (NativeDES.a()) {
            return "http://" + l.a().c() + "/ring_enc.php?" + "cmd=" + str + "&q=";
        }
        return "http://www.shoujiduoduo.com/ring.php?";
    }

    private static String e(String str, String str2) {
        return "http://" + str2 + "/ring_enc.php?" + "cmd=" + str + "&q=";
    }

    public static String d(String str) {
        String str2 = h + "&type=getlist" + str;
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetRingList: paraString = " + str2);
        return c("getlist", str2);
    }

    public static void a(final String str, final String str2, final a aVar) {
        final Handler handler = new Handler(Looper.getMainLooper());
        h.a(new Runnable() {
            public void run() {
                final String a2 = s.a(str, str2);
                if (!ag.c(a2)) {
                    handler.post(new Runnable() {
                        public void run() {
                            aVar.a(a2);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            aVar.a(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, SocketMessage.MSG_ERROR_KEY);
                        }
                    });
                }
            }
        });
    }

    public static void a(String str, String str2, JSONObject jSONObject, a aVar) {
        final Handler handler = new Handler(Looper.getMainLooper());
        final String str3 = str;
        final String str4 = str2;
        final JSONObject jSONObject2 = jSONObject;
        final a aVar2 = aVar;
        h.a(new Runnable() {
            public void run() {
                final String a2 = s.a(str3, str4, jSONObject2);
                if (!ag.c(a2)) {
                    handler.post(new Runnable() {
                        public void run() {
                            aVar2.a(a2);
                        }
                    });
                } else {
                    handler.post(new Runnable() {
                        public void run() {
                            aVar2.a(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, SocketMessage.MSG_ERROR_KEY);
                        }
                    });
                }
            }
        });
    }

    public static final String a(String str, String str2, JSONObject jSONObject) {
        com.shoujiduoduo.base.a.a.b("HttpRequest", "method: " + str);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "param: " + str2);
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=").append(str);
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2);
        }
        com.shoujiduoduo.base.a.a.a("HttpRequest", "request param:" + sb.toString());
        String j = j(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c(str)).append(j);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "request url encoded:" + sb2.toString());
        return ao.a(sb2.toString(), jSONObject);
    }

    public static final String a(String str, String str2) {
        com.shoujiduoduo.base.a.a.b("HttpRequest", "method: " + str);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "param: " + str2);
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=").append(str);
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2);
        }
        com.shoujiduoduo.base.a.a.a("HttpRequest", "request param:" + sb.toString());
        String j = j(sb.toString());
        StringBuilder sb2 = new StringBuilder();
        sb2.append(c(str)).append(j);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "request url encoded:" + sb2.toString());
        byte[] b2 = ao.b(sb2.toString());
        if (b2 == null) {
            b2 = d(str, j);
        }
        if (b2 != null) {
            return ag.e(new String(b2));
        }
        return "";
    }

    public static String e() {
        String str = h + "&type=genrid&uid=" + ad.a(RingDDApp.c(), "user_uid", "");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGenRid: param = " + str);
        return c("genrid", str);
    }

    public static String f() {
        String str = h + "&type=userringlist&uid=" + ad.a(RingDDApp.c(), "user_uid", "");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetUserMakeRingList: param = " + str);
        return c("userringlist", str);
    }

    public static String e(String str) {
        String a2 = ad.a(RingDDApp.c(), "user_uid", "");
        if (TextUtils.isEmpty(a2)) {
            com.shoujiduoduo.base.a.a.c("HttpRequest", "not login, no online favorite ring list");
            return null;
        }
        String str2 = h + "&type=getuserfavorite&uid=" + a2 + "&sig=" + str + "&username=" + q.a(ad.a(RingDDApp.c(), "user_name", "")) + "&headurl=" + q.a(ad.a(RingDDApp.c(), "user_headpic", ""));
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetUserFavorite: param = " + str2);
        return c("getuserfavorite", str2);
    }

    public static String f(String str) {
        String str2 = h + "&type=adduserfavorite&uid=" + ad.a(RingDDApp.c(), "user_uid", "") + "&rid=" + str;
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpAddUserFavorite: param = " + str2);
        return c("adduserfavorite", str2);
    }

    public static String g(String str) {
        String str2 = h + "&type=deluserfavorite&uid=" + ad.a(RingDDApp.c(), "user_uid", "") + "&rid=" + str;
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpDelUserFavorite: param = " + str2);
        return c("deluserfavorite", str2);
    }

    public static boolean a(RingData ringData, String str) {
        String str2;
        String a2 = ad.a(RingDDApp.c(), "user_uid", "");
        int i = ((MakeRingData) ringData).d == 0 ? 1 : 0;
        String a3 = ad.a(RingDDApp.c(), "user_upload_name", "");
        String str3 = ((MakeRingData) ringData).o;
        File file = new File(str3);
        String b2 = p.b(str3);
        String str4 = "http://" + ab.a().a("bcs_domain_name") + "/duoduo-ring-user-upload";
        if (i == 1) {
            str2 = str4 + "/record-ring/" + ringData.g + "." + b2;
        } else {
            str2 = str4 + "/edit-ring/" + ringData.g + "." + b2;
        }
        String str5 = h + ("&type=uploadsucc&rid=" + ringData.g + "&uid=" + a2 + "&url=" + i(str2) + "&isrec=" + i + "&audiocate=" + str + "&username=" + i(a3) + "&artist=" + i(ringData.f) + "&duration=" + (ringData.l * 1000) + "&length=" + file.length() + "&fmt=" + b2 + "&name=" + i(ringData.e) + "&date" + i(((MakeRingData) ringData).c) + "&headurl=" + q.a(ad.a(RingDDApp.c(), "user_headpic", "")));
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpUploadSuccess: url = " + c("uploadsucc") + str5);
        String str6 = c("uploadsucc") + j(str5);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpUploadSuccess: url = " + str6);
        byte[] b3 = ao.b(str6);
        if (b3 == null || !new String(b3).equals("0")) {
            return false;
        }
        return true;
    }

    public static void h(final String str) {
        final String a2 = ad.a(RingDDApp.c(), "user_uid", "");
        h.a(new Runnable() {
            public void run() {
                String str = s.h + "&type=userringdel&uid=" + a2 + "&rid=" + str;
                com.shoujiduoduo.base.a.a.a("HttpRequest", "httpUploadSuccess: url = " + s.c("userringdel") + str);
                String str2 = s.c("userringdel") + s.j(str);
                com.shoujiduoduo.base.a.a.a("HttpRequest", "httpUploadSuccess: url = " + str2);
                byte[] b2 = ao.b(str2);
                if (b2 != null) {
                    com.shoujiduoduo.base.a.a.a("HttpRequest", "delete ring, ret:" + new String(b2));
                }
            }
        });
    }

    public static String i(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static void b(String str, String str2) {
        a(str, str2, "");
    }

    public static void a(final String str, final String str2, final String str3) {
        h.a(new Runnable() {
            public void run() {
                StringBuilder sb = new StringBuilder();
                String str = "";
                try {
                    str = URLEncoder.encode(str2, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                sb.append(s.h).append("&type=keyvalue").append("&key=").append("ringDD_ar:" + str).append("&param=").append(str).append(str3);
                String str2 = "http://log.shoujiduoduo.com/log.php?" + sb.toString();
                com.shoujiduoduo.base.a.a.a("HttpRequest", str2);
                ao.a(str2);
            }
        });
    }

    public static String j(String str) {
        if (!TextUtils.isEmpty(str) && NativeDES.a()) {
            return URLEncoder.encode(f3220a.Encrypt(str));
        }
        return str;
    }

    public static void a(final int i) {
        h.a(new Runnable() {
            public void run() {
                String str = s.h + "&type=cailing&method=error&param=offline&rid=" + (i > 1000000000 ? i - 1000000000 : i) + "&network=" + NetworkStateUtil.d();
                com.shoujiduoduo.base.a.a.a("HttpRequest", "paramString2:" + str);
                String str2 = s.c("cailing") + s.j(str);
                com.shoujiduoduo.base.a.a.a("HttpRequest", "paramUrl:" + str2);
                ao.a(str2);
            }
        });
    }

    public static String g() {
        StringBuilder sb = new StringBuilder();
        sb.append(h).append("&type=gethotkeyword");
        com.shoujiduoduo.base.a.a.a("HttpRequest", "httpGetHotKeyword: paraString = " + sb.toString());
        return c("gethotkeyword", sb.toString());
    }

    public static boolean a(String str, String str2, long j, com.shoujiduoduo.a.c.h hVar) {
        com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: url = " + str);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: path = " + str2);
        com.shoujiduoduo.base.a.a.a("HttpRequest", "start_pos = " + j);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: conn = " + httpURLConnection.toString());
            httpURLConnection.setRequestProperty("RANGE", "bytes=" + j + "-");
            httpURLConnection.connect();
            com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: connect finished!");
            int contentLength = httpURLConnection.getContentLength();
            if (contentLength <= 0) {
                com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: filesize Error! filesize= " + contentLength);
                if (hVar != null) {
                    hVar.a(0);
                }
                return false;
            }
            if (hVar != null) {
                hVar.b((long) contentLength);
            }
            com.shoujiduoduo.base.a.a.a("HttpRequest", "download soft: filesize = " + contentLength);
            InputStream inputStream = httpURLConnection.getInputStream();
            RandomAccessFile randomAccessFile = new RandomAccessFile(str2, "rw");
            randomAccessFile.seek(j);
            byte[] bArr = new byte[10240];
            while (true) {
                int read = inputStream.read(bArr, 0, 10240);
                if (read <= 0) {
                    break;
                }
                randomAccessFile.write(bArr, 0, read);
                j += (long) read;
                if (hVar != null) {
                    hVar.a(j);
                }
            }
            randomAccessFile.close();
            httpURLConnection.disconnect();
            if (hVar != null) {
                hVar.a();
            }
            return true;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return false;
        } catch (IOException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static InputStream a(String str, boolean z, int i, int i2) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            if (z) {
                httpURLConnection.setRequestProperty("Accept-Encoding", "gzip");
            }
            httpURLConnection.setConnectTimeout(i);
            if (i2 > 0) {
                httpURLConnection.setReadTimeout(i2);
            }
            if ("gzip".equalsIgnoreCase(httpURLConnection.getContentEncoding())) {
                return new GZIPInputStream(httpURLConnection.getInputStream());
            }
            return httpURLConnection.getInputStream();
        } catch (Exception e2) {
            System.out.println(e2.getMessage());
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035 A[SYNTHETIC, Splitter:B:18:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a A[Catch:{ Exception -> 0x0086 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0076 A[SYNTHETIC, Splitter:B:47:0x0076] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x007b A[Catch:{ Exception -> 0x007f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r7, java.lang.String r8, boolean r9, int r10, int r11) {
        /*
            r1 = 0
            r0 = 0
            r4 = 4096(0x1000, float:5.74E-42)
            r2 = 0
            java.io.InputStream r3 = a(r7, r9, r10, r11)     // Catch:{ Exception -> 0x0088, all -> 0x0072 }
            if (r3 == 0) goto L_0x0065
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            boolean r2 = r5.exists()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            if (r2 == 0) goto L_0x003e
            r5.delete()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
        L_0x0019:
            r5.createNewFile()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            r2.<init>(r5)     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            byte[] r1 = new byte[r4]     // Catch:{ Exception -> 0x0030, all -> 0x0083 }
        L_0x0023:
            if (r3 == 0) goto L_0x0054
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x0030, all -> 0x0083 }
            if (r4 <= 0) goto L_0x0054
            r5 = 0
            r2.write(r1, r5, r4)     // Catch:{ Exception -> 0x0030, all -> 0x0083 }
            goto L_0x0023
        L_0x0030:
            r1 = move-exception
            r1 = r2
            r2 = r3
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ Exception -> 0x0086 }
        L_0x0038:
            if (r2 == 0) goto L_0x003d
            r2.close()     // Catch:{ Exception -> 0x0086 }
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.String r2 = r5.getParent()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            r6.<init>(r2)     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            boolean r2 = r6.exists()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            if (r2 != 0) goto L_0x0019
            r6.mkdirs()     // Catch:{ Exception -> 0x0051, all -> 0x0081 }
            goto L_0x0019
        L_0x0051:
            r2 = move-exception
            r2 = r3
            goto L_0x0033
        L_0x0054:
            r2.flush()     // Catch:{ Exception -> 0x0030, all -> 0x0083 }
            r0 = 1
            if (r2 == 0) goto L_0x005d
            r2.close()     // Catch:{ Exception -> 0x0063 }
        L_0x005d:
            if (r3 == 0) goto L_0x003d
            r3.close()     // Catch:{ Exception -> 0x0063 }
            goto L_0x003d
        L_0x0063:
            r1 = move-exception
            goto L_0x003d
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r2.close()     // Catch:{ Exception -> 0x0070 }
        L_0x006a:
            if (r3 == 0) goto L_0x003d
            r3.close()     // Catch:{ Exception -> 0x0070 }
            goto L_0x003d
        L_0x0070:
            r1 = move-exception
            goto L_0x003d
        L_0x0072:
            r0 = move-exception
            r3 = r1
        L_0x0074:
            if (r1 == 0) goto L_0x0079
            r1.close()     // Catch:{ Exception -> 0x007f }
        L_0x0079:
            if (r3 == 0) goto L_0x007e
            r3.close()     // Catch:{ Exception -> 0x007f }
        L_0x007e:
            throw r0
        L_0x007f:
            r1 = move-exception
            goto L_0x007e
        L_0x0081:
            r0 = move-exception
            goto L_0x0074
        L_0x0083:
            r0 = move-exception
            r1 = r2
            goto L_0x0074
        L_0x0086:
            r1 = move-exception
            goto L_0x003d
        L_0x0088:
            r2 = move-exception
            r2 = r1
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean, int, int):boolean");
    }

    public static boolean a(String str, String str2, boolean z) {
        return a(str, str2, z, 10000, 0);
    }
}
