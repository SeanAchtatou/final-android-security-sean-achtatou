package com.shoujiduoduo.util;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.util.widget.d;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.media.g;
import com.umeng.socialize.media.p;
import com.umeng.socialize.utils.ShareBoardlistener;
import java.util.Map;

/* compiled from: UmengSocialUtils */
public class ai {
    private static ai c = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f3006a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public RingData f3007b;
    private UMShareListener d = new UMShareListener() {
        public void onResult(a aVar) {
            ak.a(ai.this.f3007b.g, 5, "&from=" + ai.this.f3006a);
            d.a("分享成功!");
        }

        public void onError(a aVar, Throwable th) {
            d.a("分享失败!");
        }

        public void onCancel(a aVar) {
            d.a("分享取消!");
        }
    };
    private UMShareListener e = new UMShareListener() {
        public void onResult(a aVar) {
            d.a("分享成功!");
        }

        public void onError(a aVar, Throwable th) {
            d.a("分享失败!");
        }

        public void onCancel(a aVar) {
            d.a("分享取消!");
        }
    };

    private ai() {
    }

    public static synchronized ai a() {
        ai aiVar;
        synchronized (ai.class) {
            if (c == null) {
                c = new ai();
            }
            aiVar = c;
        }
        return aiVar;
    }

    public void a(Activity activity, String str, String str2, String str3, String str4) {
        final Activity activity2 = activity;
        final String str5 = str;
        final String str6 = str2;
        final String str7 = str3;
        final String str8 = str4;
        new ShareAction(activity).setDisplayList(a.WEIXIN, a.WEIXIN_CIRCLE, a.QQ, a.QZONE).setShareboardclickCallback(new ShareBoardlistener() {
            public void a(com.umeng.socialize.shareboard.a aVar, a aVar2) {
                ai.this.a(activity2, str5, str6, str7, str8, aVar2);
            }
        }).open();
    }

    public void a(final Activity activity, final RingData ringData, String str) {
        this.f3006a = str;
        this.f3007b = ringData;
        new ShareAction(activity).setDisplayList(a.WEIXIN, a.WEIXIN_CIRCLE, a.QQ, a.QZONE, a.SINA).addButton("music_album", "music_album", "music_alblum_share_on", "music_alblum_share_off").setShareboardclickCallback(new ShareBoardlistener() {
            public void a(com.umeng.socialize.shareboard.a aVar, a aVar2) {
                if (aVar2 != null) {
                    ai.this.a(activity, ringData, aVar2);
                } else if (aVar.f3951b.equals("music_album")) {
                    Intent intent = new Intent(RingDDApp.c(), MusicAlbumActivity.class);
                    intent.putExtra("musicid", ringData.g);
                    intent.putExtra("title", "音乐相册");
                    intent.putExtra("type", MusicAlbumActivity.a.create_album);
                    RingToneDuoduoActivity.a().startActivity(intent);
                }
            }
        }).open();
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, String str, String str2, String str3, String str4, a aVar) {
        g gVar = new g(activity, str3);
        switch (aVar) {
            case WEIXIN:
            case WEIXIN_CIRCLE:
            case QQ:
            case QZONE:
                new ShareAction(activity).withTitle(str).withText(str2).withTargetUrl(str4).withMedia(gVar).setPlatform(aVar).setCallback(this.e).share();
                return;
            case SINA:
                new ShareAction(activity).withText(str2 + " 地址>>" + str4).withMedia(gVar).setPlatform(aVar).setCallback(this.e).share();
                return;
            default:
                com.shoujiduoduo.base.a.a.c("UmengSocialUtils", "not support media");
                return;
        }
    }

    /* access modifiers changed from: private */
    public void a(Activity activity, RingData ringData, a aVar) {
        String str = "分享手机铃声   “" + ringData.e + "”  来自  @铃声多多 ，快来听听吧!";
        String a2 = com.umeng.a.a.a().a(RingDDApp.c(), "share_icon");
        if (TextUtils.isEmpty(a2)) {
            a2 = "http://cdnringhlt.shoujiduoduo.com/ringres/software/ring/ar/icon114.png";
        }
        String str2 = ag.d(ab.a().a("share_url")) + "ddsid=" + ringData.g + "&ddsrc=" + aVar.toString();
        com.shoujiduoduo.base.a.a.a("UmengSocialUtils", "[shareRing] targeturl:" + str2);
        g gVar = new g(activity, a2);
        p pVar = new p(ringData.b());
        pVar.a(ringData.e);
        pVar.a(new g(activity, a2));
        pVar.b(str2);
        switch (aVar) {
            case WEIXIN:
            case WEIXIN_CIRCLE:
            case QQ:
            case QZONE:
                new ShareAction(activity).withTitle("铃声多多").withText(str).withTargetUrl(str2).withMedia(pVar).setPlatform(aVar).setCallback(this.d).share();
                return;
            case SINA:
                new ShareAction(activity).withText(str + " 试听地址>>" + str2).withMedia(gVar).setPlatform(aVar).setCallback(this.d).share();
                return;
            default:
                com.shoujiduoduo.base.a.a.c("UmengSocialUtils", "not support media");
                return;
        }
    }

    public void a(Activity activity, a aVar) {
        UMShareAPI.get(activity).deleteOauth(activity, aVar, new UMAuthListener() {
            public void onComplete(a aVar, int i, Map<String, String> map) {
                d.a("成功退出登录！");
            }

            public void onError(a aVar, int i, Throwable th) {
                d.a("退出登录失败！");
            }

            public void onCancel(a aVar, int i) {
            }
        });
    }
}
