package com.shoujiduoduo.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/* compiled from: StringUtil */
public class ag {
    public static String a(Object[] objArr, String str) {
        if (objArr == null || objArr.length == 0) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < objArr.length; i++) {
            sb.append(objArr[i].toString());
            if (i < objArr.length - 1) {
                sb.append(str);
            }
        }
        return sb.toString();
    }

    public static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Throwable th) {
        }
        return "";
    }

    public static boolean b(String str) {
        return str == null || "".equals(str) || "null".equals(str);
    }

    public static boolean c(String str) {
        return str == null || "".equals(str);
    }

    public static String d(String str) {
        if (c(str)) {
            return "";
        }
        return !str.contains("?") ? str + "?" : str;
    }

    public static int a(String str, String str2) {
        if (str == null && str2 == null) {
            return 0;
        }
        if (str == null) {
            return -1;
        }
        if (str2 == null) {
            return 1;
        }
        if (str.equals(str2)) {
            return 0;
        }
        if (str.length() == 0) {
            return -1;
        }
        if (str2.length() == 0) {
            return 1;
        }
        return b(str, str2);
    }

    public static boolean a(int i) {
        return (i >= 65 && i <= 90) || (i >= 97 && i <= 122);
    }

    private static int b(String str, String str2) {
        int i = 0;
        int min = Math.min(str.length(), str2.length());
        for (int i2 = 0; i2 < min; i2++) {
            i = a(str.charAt(i2), str2.charAt(i2));
            if (i != 0) {
                break;
            }
        }
        if (i == 0) {
            return str.length() - str2.length();
        }
        return i;
    }

    private static int a(char c, char c2) {
        int i;
        int i2 = 0;
        if (a(c) && a(c2)) {
            int lowerCase = Character.toLowerCase(c) - Character.toLowerCase(c2);
            if (lowerCase == 0) {
                return c - c2;
            }
            return lowerCase;
        } else if (a(c)) {
            char charAt = y.a(c2).charAt(0);
            if (!a(charAt)) {
                return -1;
            }
            int lowerCase2 = Character.toLowerCase(c) - Character.toLowerCase(charAt);
            if (lowerCase2 != 0) {
                return lowerCase2;
            }
            return 1;
        } else if (a(c2)) {
            char charAt2 = y.a(c).charAt(0);
            if (!a(charAt2)) {
                return 1;
            }
            int lowerCase3 = Character.toLowerCase(charAt2) - Character.toLowerCase(c2);
            if (lowerCase3 == 0) {
                return -1;
            }
            return lowerCase3;
        } else {
            String a2 = y.a(c);
            String a3 = y.a(c2);
            int min = Math.min(a2.length(), a3.length());
            int i3 = 0;
            while (true) {
                if (i3 >= min) {
                    i = i2;
                    break;
                }
                char charAt3 = a2.charAt(i3);
                char charAt4 = a3.charAt(i3);
                if (a(charAt3) && a(charAt4)) {
                    i2 = Character.toLowerCase(charAt3) - Character.toLowerCase(charAt4);
                    if (i2 == 0) {
                        i2 = charAt3 - charAt4;
                    }
                } else if (a(charAt3)) {
                    i2 = -1;
                } else if (a(charAt4)) {
                    i2 = 1;
                } else {
                    i2 = charAt3 - charAt4;
                }
                if (i2 != 0) {
                    i = i2;
                    break;
                }
                i3++;
            }
            if (i == 0) {
                return a2.length() - a3.length();
            }
            return i;
        }
    }

    public static String e(String str) {
        try {
            if (c(str)) {
                return str;
            }
            int indexOf = str.indexOf("<script");
            int indexOf2 = str.indexOf("</script>");
            if (indexOf == -1 || indexOf2 == -1) {
                return str;
            }
            StringBuilder sb = new StringBuilder(str);
            sb.delete(indexOf, indexOf2 + "</script>".length());
            return sb.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return str;
        }
    }
}
