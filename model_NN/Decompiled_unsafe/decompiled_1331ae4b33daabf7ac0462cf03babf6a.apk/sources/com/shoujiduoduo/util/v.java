package com.shoujiduoduo.util;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity;
import com.shoujiduoduo.util.n;
import com.shoujiduoduo.util.widget.d;
import com.umeng.analytics.b;
import java.util.HashMap;

/* compiled from: MarketInstallUtils */
public class v {
    public static boolean a(String str) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("market://details?id=" + str));
            HashMap hashMap = new HashMap();
            if (f.a("com.oppo.market")) {
                hashMap.put("market", "oppo");
                intent.setPackage("com.oppo.market");
                z = true;
            } else if (f.a("com.bbk.appstore")) {
                hashMap.put("market", "vivo");
                intent.setPackage("com.bbk.appstore");
                z = true;
            } else if (f.a("com.huawei.appmarket")) {
                hashMap.put("market", "huawei");
                intent.setPackage("com.huawei.appmarket");
                z = true;
            } else if (f.a("com.xiaomi.market")) {
                hashMap.put("market", "xiaomi");
                intent.setPackage("com.xiaomi.market");
                z = true;
            } else if (f.a("com.lenovo.leos.appstore")) {
                hashMap.put("market", "lenovo");
                intent.setPackage("com.lenovo.leos.appstore");
                z = true;
            } else if (f.a("com.letv.app.appstore")) {
                hashMap.put("market", "letv");
                intent.setPackage("com.letv.app.appstore");
                z = true;
            } else if (f.a("com.gionee.aora.market")) {
                hashMap.put("market", "jinli");
                intent.setPackage("com.gionee.aora.market");
                z = true;
            } else if (f.a("com.meizu.mstore")) {
                hashMap.put("market", "meizu");
                intent.setPackage("com.meizu.mstore");
                z = true;
            } else if (f.a("com.tencent.android.qqdownloader")) {
                hashMap.put("market", "gdt");
                intent.setPackage("com.tencent.android.qqdownloader");
                z = true;
            } else {
                hashMap.put("market", "others");
                HashMap hashMap2 = new HashMap();
                hashMap2.put("brand", Build.BRAND);
                b.a(RingDDApp.c(), "market_install_not_match_brand", hashMap2);
                HashMap hashMap3 = new HashMap();
                if (f.a("com.baidu.appsearch")) {
                    hashMap3.put("market", "baidu");
                } else if (f.a("com.qihoo.appstore")) {
                    hashMap3.put("market", "qh360");
                } else if (f.a("com.wandoujia.phoenix2")) {
                    hashMap3.put("market", "wdj");
                } else if (f.a("com.pp.assistant")) {
                    hashMap3.put("market", "pp");
                } else if (f.a("cn.goapk.market")) {
                    hashMap3.put("market", "anzhi");
                } else {
                    hashMap3.put("market", "other");
                }
                b.a(RingDDApp.c(), "market_install_not_match_market", hashMap3);
                z = false;
            }
            b.a(RingDDApp.c(), "market_install_market", hashMap);
            if (!z) {
                return false;
            }
            RingToneDuoduoActivity.a().startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void a(String str, String str2) {
        d.a("开始下载\"" + str2 + "\"");
        n.a(RingDDApp.c()).a(str, str2, n.a.immediatelly, true);
        HashMap hashMap = new HashMap();
        hashMap.put(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, str2);
        b.a(RingDDApp.c(), "CLICK_SEARCH_AD_DOWN", hashMap);
    }
}
