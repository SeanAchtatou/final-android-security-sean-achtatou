package com.shoujiduoduo.util;

import java.io.File;

/* compiled from: DeleteFileUtil */
public class j {
    public static boolean a(String str) {
        File file = new File(str);
        if (!file.exists()) {
            return false;
        }
        if (file.isFile()) {
            return b(str);
        }
        return c(str);
    }

    public static boolean b(String str) {
        File file = new File(str);
        if (!file.isFile() || !file.exists()) {
            return false;
        }
        file.delete();
        return true;
    }

    public static boolean c(String str) {
        boolean z;
        if (!str.endsWith(File.separator)) {
            str = str + File.separator;
        }
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            return false;
        }
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            z = true;
            for (int i = 0; i < listFiles.length; i++) {
                if (!listFiles[i].isFile()) {
                    z = c(listFiles[i].getAbsolutePath());
                    if (!z) {
                        break;
                    }
                } else {
                    z = b(listFiles[i].getAbsolutePath());
                    if (!z) {
                        break;
                    }
                }
            }
        } else {
            z = true;
        }
        if (!z || !file.delete()) {
            return false;
        }
        return true;
    }
}
