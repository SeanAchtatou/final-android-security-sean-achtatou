package com.shoujiduoduo.util.c;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/* compiled from: CailingIDManager */
public class a extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3094a = a.class.getSimpleName();
    private static a c = null;

    /* renamed from: b  reason: collision with root package name */
    private int f3095b = 0;

    private a(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
        com.shoujiduoduo.base.a.a.a(f3094a, "constructor!");
        a();
        this.f3095b = b();
    }

    public static a a(Context context) {
        a aVar;
        synchronized (f3094a) {
            if (c == null && context != null) {
                c = new a(context, "duoduo.ringtone.database", null, 3);
            }
            aVar = c;
        }
        return aVar;
    }

    private void a() {
        synchronized (f3094a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            try {
                writableDatabase.execSQL("CREATE TABLE IF NOT EXISTS cailing_id_table (id INTEGER PRIMARY KEY AUTOINCREMENT, cid VARCHAR, rid VARCHAR);");
                com.shoujiduoduo.base.a.a.a(f3094a, "Create cailing_id_table");
            } catch (SQLException e) {
                e.printStackTrace();
                com.shoujiduoduo.base.a.a.c(f3094a, "Create cailing_id_table failed!");
            }
            writableDatabase.close();
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        com.shoujiduoduo.base.a.a.a(f3094a, "CailingIDManager.onUpgrade.");
    }

    private int b() {
        int i;
        synchronized (f3094a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            Cursor rawQuery = writableDatabase.rawQuery("select count(*) from cailing_id_table;", null);
            rawQuery.moveToFirst();
            i = rawQuery.getInt(0);
            rawQuery.close();
            writableDatabase.close();
            com.shoujiduoduo.base.a.a.a(f3094a, "current cailing count = " + i);
        }
        return i;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        synchronized (f3094a) {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            try {
                Cursor rawQuery = writableDatabase.rawQuery("select * from cailing_id_table where cid='" + str + "' order by id;", null);
                if (rawQuery == null || rawQuery.getCount() == 0) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    String valueOf = String.valueOf(this.f3095b + 900000000);
                    writableDatabase.execSQL("insert into cailing_id_table (cid, rid)VALUES ('" + str + "','" + valueOf + "');");
                    writableDatabase.close();
                    this.f3095b++;
                    com.shoujiduoduo.base.a.a.c(f3094a, "insert into table. return cailing rid = " + valueOf);
                    return valueOf;
                } else if (rawQuery.getCount() != 1) {
                    rawQuery.close();
                    com.shoujiduoduo.base.a.a.c(f3094a, "getCailingRid: c.getCount() != 1");
                    writableDatabase.close();
                    com.shoujiduoduo.base.a.a.c(f3094a, "return cailing rid = null.");
                    return null;
                } else if (rawQuery.moveToNext()) {
                    String string = rawQuery.getString(2);
                    rawQuery.close();
                    writableDatabase.close();
                    com.shoujiduoduo.base.a.a.c(f3094a, "exists in table. return cailing rid = " + string);
                    return string;
                } else {
                    rawQuery.close();
                    writableDatabase.close();
                    com.shoujiduoduo.base.a.a.c(f3094a, "return cailing rid = null111111.");
                    return null;
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
                com.shoujiduoduo.base.a.a.c(f3094a, "return cailing rid = null222222222.");
                return null;
            }
        }
    }
}
