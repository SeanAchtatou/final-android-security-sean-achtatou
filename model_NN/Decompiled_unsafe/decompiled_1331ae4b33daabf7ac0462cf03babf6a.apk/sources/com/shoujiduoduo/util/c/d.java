package com.shoujiduoduo.util.c;

import android.content.Context;
import com.cmsc.cmmusic.init.GetAppInfo;
import com.cmsc.cmmusic.init.GetAppInfoInterface;
import com.shoujiduoduo.base.a.a;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HttpPostCore */
public class d {
    public static String a(Context context, String str, byte[] bArr, boolean z) {
        a.b("HttpPostCore", "address----" + str);
        try {
            a.a("HttpPostCore", "requestString : " + new String(bArr, "UTF-8"));
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setConnectTimeout(30000);
            httpURLConnection.setReadTimeout(30000);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            if (a(context)) {
                c(context, httpURLConnection, z);
            } else if (GetAppInfo.getIMEI(context).trim().length() != 0) {
                a(context, httpURLConnection, z);
            } else {
                b(context, httpURLConnection, z);
            }
            httpURLConnection.setRequestProperty("Content-length", "" + bArr.length);
            httpURLConnection.setRequestProperty("Accept", "*/*");
            httpURLConnection.setRequestProperty("Content-Type", "*/*");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Charset", "UTF-8");
            httpURLConnection.setRequestProperty("Token", GetAppInfoInterface.getToken(context));
            httpURLConnection.setRequestProperty("excode", GetAppInfo.getexCode(context));
            a.b("HttpPostCore", "output before");
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(bArr);
            outputStream.flush();
            a.b("HttpPostCore", "output flush");
            outputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            a.b("HttpPostCore", "responseCode-----" + responseCode);
            if (200 == responseCode) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] bArr2 = new byte[1024];
                while (true) {
                    int read = httpURLConnection.getInputStream().read(bArr2, 0, 1024);
                    if (read != -1) {
                        byteArrayOutputStream.write(bArr2, 0, read);
                    } else {
                        String str2 = new String(byteArrayOutputStream.toByteArray(), "UTF-8");
                        a.b("HttpPostCore", "responseBody------------\r\n" + str2);
                        return str2;
                    }
                }
            }
        } catch (Exception e) {
            a.a("HttpPostCore", "cm httppostcore return: null");
            e.printStackTrace();
        }
        return null;
    }

    private static void a(Context context, HttpURLConnection httpURLConnection, boolean z) {
        String appid;
        StringBuilder append = new StringBuilder().append("OEPAUTH realm=\"OEP\",IMSI=\"").append(GetAppInfo.getIMEI(context)).append("\",appID=\"");
        if (z) {
            appid = "004750246952188534";
        } else {
            appid = GetAppInfoInterface.getAppid(context);
        }
        httpURLConnection.addRequestProperty("Authorization", append.append(appid).append("\",pubKey=\"").append(GetAppInfoInterface.getSign(context)).append("\",netMode=\"").append(GetAppInfoInterface.getNetMode(context)).append("\",packageName=\"").append(GetAppInfoInterface.getPackageName(context)).append("\",version=\"").append(GetAppInfoInterface.getSDKVersion()).append("\",excode=\"").append(GetAppInfo.getexCode(context)).append("\"").toString());
        a.a("HttpPostCore", "===============================================================");
        a.a("HttpPostCore", "imsi\r\n" + GetAppInfoInterface.getIMSI(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "===============================================================");
    }

    private static void b(Context context, HttpURLConnection httpURLConnection, boolean z) {
        String appid;
        StringBuilder append = new StringBuilder().append("OEPAUTH realm=\"OEP\",IMSI=\"\",appID=\"");
        if (z) {
            appid = "004750246952188534";
        } else {
            appid = GetAppInfoInterface.getAppid(context);
        }
        httpURLConnection.addRequestProperty("Authorization", append.append(appid).append("\",pubKey=\"").append(GetAppInfoInterface.getSign(context)).append("\",netMode=\"").append(GetAppInfoInterface.getNetMode(context)).append("\",packageName=\"").append(GetAppInfoInterface.getPackageName(context)).append("\",version=\"").append(GetAppInfoInterface.getSDKVersion()).append("\",excode=\"").append(GetAppInfo.getexCode(context)).append("\"").toString());
        a.a("HttpPostCore", "===============================================================");
        a.a("HttpPostCore", "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "===============================================================");
    }

    private static void c(Context context, HttpURLConnection httpURLConnection, boolean z) {
        String appid;
        StringBuilder append = new StringBuilder().append("OEPAUTH realm=\"OEP\",Token=\"").append(GetAppInfoInterface.getToken(context)).append("\",appID=\"");
        if (z) {
            appid = "004750246952188534";
        } else {
            appid = GetAppInfoInterface.getAppid(context);
        }
        httpURLConnection.addRequestProperty("Authorization", append.append(appid).append("\",pubKey=\"").append(GetAppInfoInterface.getSign(context)).append("\",netMode=\"").append(GetAppInfoInterface.getNetMode(context)).append("\",packageName=\"").append(GetAppInfoInterface.getPackageName(context)).append("\",version=\"").append(GetAppInfoInterface.getSDKVersion()).append("\",excode=\"").append(GetAppInfo.getexCode(context)).append("\"").toString());
        a.a("HttpPostCore", "===============================================================");
        a.a("HttpPostCore", "token\r\n" + GetAppInfoInterface.getToken(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "sign\r\n" + GetAppInfoInterface.getSign(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "appid\r\n" + GetAppInfoInterface.getAppid(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "netmode\r\n" + GetAppInfoInterface.getNetMode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "packagename\r\n" + GetAppInfoInterface.getPackageName(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "sdkversion\r\n" + GetAppInfoInterface.getSDKVersion() + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "excode\r\n" + GetAppInfo.getexCode(context) + HttpProxyConstants.CRLF);
        a.a("HttpPostCore", "===============================================================");
    }

    static boolean a(Context context) {
        return GetAppInfoInterface.getToken(context).length() > 0;
    }
}
