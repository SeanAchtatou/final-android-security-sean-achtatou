package com.shoujiduoduo.util.c;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* compiled from: DualSimUtils */
public class c {
    private static c o = null;

    /* renamed from: a  reason: collision with root package name */
    private Context f3138a = RingDDApp.c();

    /* renamed from: b  reason: collision with root package name */
    private int f3139b;
    private int c;
    private String d;
    private String e;
    private String f;
    private String g;
    private int h;
    private int i;
    private String j;
    private boolean k;
    private boolean l;
    private boolean m;
    private String n;

    private c() {
        f();
        g();
        e();
    }

    public static c a() {
        if (o == null) {
            o = new c();
        }
        return o;
    }

    public void b() {
        if (o != null) {
            o = null;
        }
        if (this.f3138a != null) {
            this.f3138a = null;
        }
    }

    public boolean c() {
        return this.k || this.l || this.m;
    }

    public boolean d() {
        return a(this.d) || a(this.e);
    }

    private boolean a(String str) {
        if (str == null || str.length() == 0) {
            return false;
        }
        if (str.startsWith("46000") || str.startsWith("46002") || str.startsWith("46007")) {
            return true;
        }
        return false;
    }

    private void f() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f3138a.getSystemService("phone");
            Class<?> cls = Class.forName("com.android.internal.telephony.Phone");
            Field field = cls.getField("GEMINI_SIM_1");
            field.setAccessible(true);
            this.f3139b = ((Integer) field.get(null)).intValue();
            Field field2 = cls.getField("GEMINI_SIM_2");
            field2.setAccessible(true);
            this.c = ((Integer) field2.get(null)).intValue();
            Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getSubscriberIdGemini", Integer.TYPE);
            this.d = (String) declaredMethod.invoke(telephonyManager, Integer.valueOf(this.f3139b));
            this.e = (String) declaredMethod.invoke(telephonyManager, Integer.valueOf(this.c));
            Method declaredMethod2 = TelephonyManager.class.getDeclaredMethod("getDeviceIdGemini", Integer.TYPE);
            this.f = (String) declaredMethod2.invoke(telephonyManager, Integer.valueOf(this.f3139b));
            this.g = (String) declaredMethod2.invoke(telephonyManager, Integer.valueOf(this.c));
            Method declaredMethod3 = TelephonyManager.class.getDeclaredMethod("getPhoneTypeGemini", Integer.TYPE);
            this.h = ((Integer) declaredMethod3.invoke(telephonyManager, Integer.valueOf(this.f3139b))).intValue();
            this.i = ((Integer) declaredMethod3.invoke(telephonyManager, Integer.valueOf(this.c))).intValue();
            if (TextUtils.isEmpty(this.d) && !TextUtils.isEmpty(this.e)) {
                this.j = this.e;
            }
            if (TextUtils.isEmpty(this.e) && !TextUtils.isEmpty(this.d)) {
                this.j = this.d;
            }
            a.a("DualSimUtils", "MTK platformimsi 1:" + this.d + " imsi 2:" + this.e);
            this.k = true;
        } catch (Throwable th) {
            a.a("DualSimUtils", "not MTK platform");
            this.k = false;
        }
    }

    private void g() {
        try {
            Class<?> cls = Class.forName("com.android.internal.telephony.PhoneFactory");
            this.n = (String) cls.getMethod("getServiceName", String.class, Integer.TYPE).invoke(cls, "phone", 1);
            TelephonyManager telephonyManager = (TelephonyManager) this.f3138a.getSystemService("phone");
            this.d = telephonyManager.getSubscriberId();
            this.f = telephonyManager.getDeviceId();
            this.h = telephonyManager.getPhoneType();
            TelephonyManager telephonyManager2 = (TelephonyManager) this.f3138a.getSystemService(this.n);
            this.e = telephonyManager2.getSubscriberId();
            this.g = telephonyManager2.getDeviceId();
            this.i = telephonyManager2.getPhoneType();
            if (TextUtils.isEmpty(this.d) && !TextUtils.isEmpty(this.e)) {
                this.j = this.e;
            }
            if (TextUtils.isEmpty(this.e) && !TextUtils.isEmpty(this.d)) {
                this.j = this.d;
            }
            a.a("DualSimUtils", "Spread platformimsi 1:" + this.d + " imsi 2:" + this.e);
            this.l = true;
        } catch (Exception e2) {
            a.a("DualSimUtils", "not Spread platform");
            this.l = false;
        }
    }

    public void e() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.f3138a.getSystemService("phone");
            Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
            Object systemService = this.f3138a.getSystemService("phone_msim");
            this.f3139b = 0;
            this.c = 1;
            Method method = cls.getMethod("getDataState", new Class[0]);
            telephonyManager.getDataState();
            cls.getMethod("getDefault", new Class[0]);
            Method method2 = cls.getMethod("getDeviceId", Integer.TYPE);
            Method method3 = cls.getMethod("getSubscriberId", Integer.TYPE);
            cls.getMethod("getPhoneType", new Class[0]);
            this.f = (String) method2.invoke(systemService, Integer.valueOf(this.f3139b));
            this.g = (String) method2.invoke(systemService, Integer.valueOf(this.c));
            this.d = (String) method3.invoke(systemService, Integer.valueOf(this.f3139b));
            this.e = (String) method3.invoke(systemService, Integer.valueOf(this.c));
            int dataState = telephonyManager.getDataState();
            a.c(SocketMessage.MSG_FINGER_KEY, dataState + "---" + ((Integer) method.invoke(systemService, new Object[0])).intValue());
            a.a("DualSimUtils", "Qualcomm platformimsi 1:" + this.d + " imsi 2:" + this.e);
            this.m = true;
        } catch (Exception e2) {
            a.a("DualSimUtils", "not Qualcomm platform");
            this.m = false;
        }
    }
}
