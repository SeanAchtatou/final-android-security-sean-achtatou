package com.shoujiduoduo.util.c;

import android.app.Activity;
import android.content.Context;
import cn.banshenggua.aichang.utils.StringUtil;
import com.cmsc.cmmusic.common.RingbackManagerInterface;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.CrbtListRsp;
import com.cmsc.cmmusic.common.data.CrbtOpenCheckRsp;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.ToneInfo;
import com.cmsc.cmmusic.init.GetAppInfo;
import com.cmsc.cmmusic.init.GetAppInfoInterface;
import com.cmsc.cmmusic.init.InitCmmInterface;
import com.cmsc.cmmusic.init.SmsLoginInfoRsp;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.util.NetworkStateUtil;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.b.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.s;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

/* compiled from: ChinaMobileUtils */
public class b {
    private static final b i = new b();
    /* access modifiers changed from: private */
    public static final c.b m = new c.b(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE, "对不起，中国移动的彩铃服务正在进行系统维护，请谅解");

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, C0043b> f3096a = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public HashMap<String, Boolean> f3097b = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<String, Boolean> c = new HashMap<>();
    /* access modifiers changed from: private */
    public String d;
    private String e;
    /* access modifiers changed from: private */
    public boolean f;
    private boolean g;
    /* access modifiers changed from: private */
    public Context h = RingDDApp.c();
    /* access modifiers changed from: private */
    public a j = a.f3134a;
    /* access modifiers changed from: private */
    public c.n.a k = c.n.a.f3074a;
    private String l = "";
    private boolean n;
    /* access modifiers changed from: private */
    public int o = 600;

    /* compiled from: ChinaMobileUtils */
    public enum a {
        f3134a,
        checking,
        initializing,
        success,
        fail
    }

    /* renamed from: com.shoujiduoduo.util.c.b$b  reason: collision with other inner class name */
    /* compiled from: ChinaMobileUtils */
    private class C0043b {

        /* renamed from: a  reason: collision with root package name */
        public String f3136a;

        /* renamed from: b  reason: collision with root package name */
        public long f3137b;

        public C0043b(String str, long j) {
            this.f3136a = str;
            this.f3137b = j;
        }

        public boolean a() {
            return System.currentTimeMillis() - this.f3137b < ((long) (b.this.o * 1000));
        }
    }

    private b() {
    }

    /* access modifiers changed from: private */
    public String h() {
        return ab.a().a("cmcc_month_id");
    }

    public static b a() {
        return i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, long):long
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, long):long */
    public String a(String str) {
        String a2 = ad.a(RingDDApp.c(), str + "_smscode", "");
        long a3 = ad.a(RingDDApp.c(), str + "_smscode_gettime", 0L);
        if (this.f3096a.containsKey(str) && this.f3096a.get(str).a()) {
            return this.f3096a.get(str).f3136a;
        }
        if (ag.c(a2) || System.currentTimeMillis() - a3 >= ((long) (this.o * 1000))) {
            return "";
        }
        return a2;
    }

    private String a(Node node) {
        if (node == null) {
            return "";
        }
        try {
            if (node.getFirstChild() == null) {
                return "";
            }
            return node.getFirstChild().getNodeValue();
        } catch (DOMException e2) {
            e2.printStackTrace();
            return "";
        }
    }

    /* access modifiers changed from: private */
    public c.b a(String str, String str2) {
        DocumentBuilder newDocumentBuilder;
        Document parse;
        Element documentElement;
        NodeList childNodes;
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null || (newDocumentBuilder = newInstance.newDocumentBuilder()) == null || (parse = newDocumentBuilder.parse(new InputSource(new ByteArrayInputStream(str.getBytes((String) StringUtil.Encoding))))) == null || (documentElement = parse.getDocumentElement()) == null || (childNodes = documentElement.getChildNodes()) == null) {
                return null;
            }
            if (str2.equals("method_get_ringback_policy")) {
                c.l lVar = new c.l();
                lVar.f3071a = new BizInfo();
                lVar.d = new BizInfo();
                lVar.e = new MusicInfo();
                for (int i2 = 0; i2 < childNodes.getLength(); i2++) {
                    Node item = childNodes.item(i2);
                    if (item.getNodeName().equalsIgnoreCase("resMsg")) {
                        lVar.c = a(item);
                    } else if (item.getNodeName().equalsIgnoreCase("resCode")) {
                        lVar.f3060b = a(item);
                    } else if (item.getNodeName().equalsIgnoreCase("BizInfo")) {
                        BizInfo bizInfo = new BizInfo();
                        for (Node firstChild = item.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                            if (firstChild.getNodeName() == null || firstChild.getFirstChild() == null) {
                                com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "null pointer happens");
                            } else if (firstChild.getNodeName().equalsIgnoreCase("bizCode")) {
                                bizInfo.setBizCode(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("bizType")) {
                                bizInfo.setBizType(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("originalPrice")) {
                                bizInfo.setOriginalPrice(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("salePrice")) {
                                bizInfo.setSalePrice(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("description")) {
                                bizInfo.setDescription(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("resource")) {
                                bizInfo.setResource(a(firstChild));
                            } else if (firstChild.getNodeName().equalsIgnoreCase("hold2")) {
                                bizInfo.setHold2(a(firstChild));
                            }
                        }
                        if (Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE.equals(bizInfo.getBizType()) || "00".equals(bizInfo.getBizType())) {
                            lVar.f3071a = bizInfo;
                        }
                        if ("70".equals(bizInfo.getBizType())) {
                            lVar.d = bizInfo;
                        }
                    } else if (item.getNodeName().equalsIgnoreCase("monLevel")) {
                        lVar.g = a(item);
                    } else if (item.getNodeName().equalsIgnoreCase("mobile")) {
                        lVar.f = a(item);
                    }
                }
                return lVar;
            } else if (!str2.equals("method_get_crbt_month_policy")) {
                return null;
            } else {
                c.k kVar = new c.k();
                kVar.f3070a = new c.C0042c();
                for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                    Node item2 = childNodes.item(i3);
                    if (item2.getNodeName().equalsIgnoreCase("resMsg")) {
                        kVar.c = a(item2);
                    } else if (item2.getNodeName().equalsIgnoreCase("resCode")) {
                        kVar.f3060b = a(item2);
                    } else if (item2.getNodeName().equalsIgnoreCase("BizInfoMon")) {
                        for (Node firstChild2 = item2.getFirstChild(); firstChild2 != null; firstChild2 = firstChild2.getNextSibling()) {
                            if (firstChild2.getNodeName() == null || firstChild2.getFirstChild() == null) {
                                com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "null pointer happens");
                            } else if (firstChild2.getNodeName().equalsIgnoreCase("bizCode")) {
                                kVar.f3070a.f3061a = a(firstChild2);
                            } else if (!firstChild2.getNodeName().equalsIgnoreCase("bizType")) {
                                if (firstChild2.getNodeName().equalsIgnoreCase("originalPrice")) {
                                    kVar.f3070a.f3062b = a(firstChild2);
                                } else if (firstChild2.getNodeName().equalsIgnoreCase("salePrice")) {
                                    kVar.f3070a.c = a(firstChild2);
                                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "salePrice:" + a(firstChild2));
                                } else if (firstChild2.getNodeName().equalsIgnoreCase(SocialConstants.PARAM_APP_DESC)) {
                                    kVar.f3070a.d = a(firstChild2);
                                } else if (firstChild2.getNodeName().equalsIgnoreCase("offReason")) {
                                    kVar.f3070a.e = a(firstChild2);
                                } else if (firstChild2.getNodeName().equalsIgnoreCase("hold2")) {
                                    kVar.f3070a.f = a(firstChild2);
                                }
                            }
                        }
                    } else if (item2.getNodeName().equalsIgnoreCase("mobile")) {
                        kVar.d = a(item2);
                    }
                }
                return kVar;
            }
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        } catch (DOMException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    public c.b d(String str) {
        if (str == null || str.equals("")) {
            return null;
        }
        try {
            DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
            if (newInstance == null) {
                return null;
            }
            DocumentBuilder newDocumentBuilder = newInstance.newDocumentBuilder();
            if (newDocumentBuilder == null) {
                return null;
            }
            Document parse = newDocumentBuilder.parse(new InputSource(new ByteArrayInputStream(str.getBytes(StringUtil.Encoding))));
            if (parse == null) {
                return null;
            }
            Element documentElement = parse.getDocumentElement();
            if (documentElement == null) {
                return null;
            }
            c.b bVar = new c.b();
            NodeList childNodes = documentElement.getChildNodes();
            if (childNodes != null) {
                int i2 = 0;
                while (true) {
                    if (i2 >= childNodes.getLength()) {
                        break;
                    }
                    Node item = childNodes.item(i2);
                    if (item.getNodeName().equalsIgnoreCase("ToneInfo")) {
                        bVar = new c.z();
                        ((c.z) bVar).f3089a = new ArrayList();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("UserInfo")) {
                        bVar = new c.ag();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("crbtId")) {
                        bVar = new c.g();
                        break;
                    } else if (item.getNodeName().equalsIgnoreCase("streamUrl")) {
                        bVar = new c.m();
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            if (childNodes == null) {
                return null;
            }
            for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                Node item2 = childNodes.item(i3);
                if (item2.getNodeName().equalsIgnoreCase("resMsg")) {
                    bVar.c = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("resCode")) {
                    bVar.f3060b = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("ToneInfo")) {
                    c.ae aeVar = new c.ae();
                    for (Node firstChild = item2.getFirstChild(); firstChild != null; firstChild = firstChild.getNextSibling()) {
                        if (firstChild.getNodeName() == null || firstChild.getFirstChild() == null) {
                            com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "null pointer happens");
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneID")) {
                            aeVar.f3050a = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneName")) {
                            aeVar.f3051b = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("singerName")) {
                            aeVar.d = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("price")) {
                            aeVar.f = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneValidDay")) {
                            aeVar.g = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("tonePreListenAddress")) {
                            aeVar.i = firstChild.getFirstChild().getNodeValue();
                        } else if (firstChild.getNodeName().equalsIgnoreCase("toneType")) {
                            aeVar.j = firstChild.getFirstChild().getNodeValue();
                        }
                    }
                    ((c.z) bVar).f3089a.add(aeVar);
                } else if (item2.getNodeName().equalsIgnoreCase("UserInfo")) {
                    for (Node firstChild2 = item2.getFirstChild(); firstChild2 != null; firstChild2 = firstChild2.getNextSibling()) {
                        if (firstChild2.getNodeName().equalsIgnoreCase("memLevel")) {
                            ((c.ag) bVar).f3053a = firstChild2.getFirstChild().getNodeValue();
                        }
                    }
                } else if (item2.getNodeName().equalsIgnoreCase("crbtId")) {
                    ((c.g) bVar).f3066a = item2.getFirstChild().getNodeValue();
                } else if (item2.getNodeName().equalsIgnoreCase("streamUrl")) {
                    ((c.m) bVar).f3072a = item2.getFirstChild().getNodeValue();
                    ((c.m) bVar).d = 128;
                }
            }
            return bVar;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        } catch (ParserConfigurationException e3) {
            e3.printStackTrace();
            return null;
        } catch (SAXException e4) {
            e4.printStackTrace();
            return null;
        } catch (IOException e5) {
            e5.printStackTrace();
            return null;
        } catch (DOMException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    public String b() {
        String token = GetAppInfoInterface.getToken(RingDDApp.c());
        String imsi = GetAppInfoInterface.getIMSI(RingDDApp.c());
        if (ag.c(imsi)) {
            imsi = GetAppInfo.getIMSI(RingDDApp.c());
        }
        if (!ag.c(token)) {
            return token;
        }
        return ag.c(imsi) ? f.a() : imsi;
    }

    public void a(final com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "init check");
        this.j = a.checking;
        h.a(new Runnable() {
            public void run() {
                boolean z;
                boolean z2 = true;
                if (b.this.h != null) {
                    c.n nVar = new c.n();
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "***检查阳光sdk初始化情况***");
                    SmsLoginInfoRsp smsAuthLoginValidate = InitCmmInterface.smsAuthLoginValidate(b.this.h);
                    if (smsAuthLoginValidate == null) {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk 短信验证码初始化检查失败2");
                        z = false;
                    } else if (smsAuthLoginValidate.getResCode().equals("000000")) {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk 短信验证码初始化检查成功");
                        a unused = b.this.j = a.success;
                        nVar.f3060b = smsAuthLoginValidate.getResCode();
                        nVar.c = smsAuthLoginValidate.getResMsg();
                        nVar.d = smsAuthLoginValidate.getMobile();
                        nVar.f3073a = c.n.a.sms_code;
                        z = true;
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk 短信验证码初始化检查失败1");
                        z = false;
                    }
                    if ("0".equals(InitCmmInterface.initCheck(b.this.h))) {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk默认的初始化 检查成功");
                        a unused2 = b.this.j = a.success;
                        nVar.f3060b = "000000";
                        nVar.c = "smswap 初始化检查成功";
                        nVar.d = "";
                        nVar.f3073a = c.n.a.wap;
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshinesdk sdk默认的初始化 检查失败");
                        z2 = false;
                    }
                    a unused3 = b.this.j = (z || z2) ? a.success : a.fail;
                    if (b.this.j != a.success) {
                        nVar.f3060b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                        nVar.c = "初始化检查失败";
                    }
                    if (z) {
                        if (z2) {
                            nVar.f3073a = c.n.a.all;
                        } else {
                            nVar.f3073a = c.n.a.sms_code;
                        }
                    } else if (z2) {
                        nVar.f3073a = c.n.a.wap;
                    } else {
                        nVar.f3073a = c.n.a.f3074a;
                    }
                    c.n.a unused4 = b.this.k = nVar.f3073a;
                    if (bVar != null) {
                        bVar.g(nVar);
                    }
                }
            }
        });
    }

    public void b(final com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sms initCheck");
        this.j = a.checking;
        h.a(new Runnable() {
            public void run() {
                if (b.this.h == null) {
                    com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                    return;
                }
                c.ac acVar = new c.ac();
                SmsLoginInfoRsp smsAuthLoginValidate = InitCmmInterface.smsAuthLoginValidate(b.this.h);
                if (smsAuthLoginValidate != null) {
                    if (smsAuthLoginValidate.getResCode().equals("000000")) {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码已经初始化成功");
                        a unused = b.this.j = a.success;
                    } else {
                        a unused2 = b.this.j = a.fail;
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码初始化检查失败，res:" + smsAuthLoginValidate.toString());
                    }
                    acVar.f3060b = smsAuthLoginValidate.getResCode();
                    acVar.c = smsAuthLoginValidate.getResMsg();
                    acVar.f3048a = smsAuthLoginValidate.getMobile();
                } else {
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "短信验证码尚未初始化成功");
                    a unused3 = b.this.j = a.fail;
                    acVar.f3060b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                    acVar.c = "初始化检查失败";
                }
                bVar.g(acVar);
            }
        });
    }

    public a c() {
        return this.j;
    }

    public c.n.a d() {
        return this.k;
    }

    public boolean e() {
        return this.f;
    }

    public void a(final Activity activity, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                com.shoujiduoduo.a.a.c.a().a(100, new c.b() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, boolean):boolean
                     arg types: [com.shoujiduoduo.util.c.b, int]
                     candidates:
                      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String):com.shoujiduoduo.util.b.c$b
                      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String):com.shoujiduoduo.util.b.c$b
                      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.b.c$n$a):com.shoujiduoduo.util.b.c$n$a
                      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.c.b$a):com.shoujiduoduo.util.c.b$a
                      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.b.c$b, java.lang.String):void
                      com.shoujiduoduo.util.c.b.a(android.app.Activity, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.c.b.a(java.lang.String, com.shoujiduoduo.util.b.b):void
                      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, boolean):boolean */
                    public void a() {
                        c.b bVar = new c.b();
                        try {
                            if (!b.this.f) {
                                InitCmmInterface.initSDK(activity);
                                boolean unused = b.this.f = true;
                            }
                            bVar.f3060b = "000000";
                            bVar.c = "初始化成功";
                            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "SDK 初始化成功");
                        } catch (Throwable th) {
                            th.printStackTrace();
                            bVar.f3060b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                            bVar.c = "SDK 初始化失败";
                            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "SDK 初始化失败, msg:" + th.getMessage());
                        }
                        if (bVar != null) {
                            bVar.g(bVar);
                        }
                    }
                });
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean i() {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "load cmcc sdk JNI lib");
        try {
            if (!this.g) {
                ad.b(RingDDApp.c(), "pref_load_cmcc_sunshine_sdk_start", "1");
                ad.b(RingDDApp.c(), "pref_load_cmcc_sunshine_sdk_end", "0");
                System.loadLibrary("mg20pbase");
                RingDDApp.b().a(false);
                ad.b(RingDDApp.c(), "pref_load_cmcc_sunshine_sdk_end", "1");
                this.g = true;
            }
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "load cmcc sdk JNI lib success");
            return true;
        } catch (Throwable th) {
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "load cmcc sdk JNI lib crash");
            RingDDApp.b().a(true);
            return false;
        }
    }

    public void c(final com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "Sdk准备初始化User");
        this.j = a.initializing;
        h.a(new Runnable() {
            public void run() {
                if (b.this.h == null) {
                    com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                    return;
                }
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "sunshine sdk准备初始化");
                Hashtable<String, String> initCmmEnv = InitCmmInterface.initCmmEnv(b.this.h);
                c.b bVar = new c.b();
                if (initCmmEnv == null || initCmmEnv.get("code") == null) {
                    bVar.f3060b = WeiboAuthException.DEFAULT_AUTH_ERROR_CODE;
                    bVar.c = "对不起，中国移动的彩铃服务正在进行系统维护，请谅解";
                    com.shoujiduoduo.base.a.a.b("ChinaMobileUtils", "sunshine 初始化user失败");
                    a unused = b.this.j = a.fail;
                } else {
                    bVar.f3060b = initCmmEnv.get("code");
                    bVar.c = initCmmEnv.get(SocialConstants.PARAM_APP_DESC);
                    a unused2 = b.this.j = bVar.c() ? a.success : a.fail;
                    com.shoujiduoduo.base.a.a.b("ChinaMobileUtils", "sunshine 初始化， user code:" + bVar.a() + " desc:" + bVar.b());
                }
                b.this.a(bVar, "CMCC_SUNSHINE_SDK_USER_INIT");
                bVar.g(bVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str) {
        HashMap hashMap = new HashMap();
        if (bVar == null || !bVar.c()) {
            hashMap.put(Parameters.RESOLUTION, "failed," + bVar.toString());
        } else {
            hashMap.put(Parameters.RESOLUTION, "success");
        }
        hashMap.put("netType", NetworkStateUtil.d());
        com.umeng.analytics.b.a(this.h, str, hashMap);
    }

    public void a(final String str, final com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode");
        h.a(new Runnable() {
            public void run() {
                if (b.this.h == null) {
                    com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                    return;
                }
                try {
                    c.b bVar = new c.b();
                    Result validateCode = InitCmmInterface.getValidateCode(b.this.h, str);
                    if (validateCode != null && validateCode.getResCode().equals("000000")) {
                        bVar.f3060b = validateCode.getResCode();
                        bVar.c = validateCode.getResMsg();
                        bVar.g(bVar);
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, success");
                    } else if (validateCode != null) {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed, " + validateCode.toString());
                        bVar.g(b.m);
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed");
                    } else {
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed, res is null");
                        bVar.g(b.m);
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getValidateCode, failed");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e2) {
                    e2.printStackTrace();
                }
            }
        });
    }

    public void a(final String str, final String str2, final com.shoujiduoduo.util.b.b bVar) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth");
        h.a(new Runnable() {
            public void run() {
                if (b.this.h == null) {
                    com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                    return;
                }
                try {
                    c.b bVar = new c.b();
                    Result smsLoginAuth = InitCmmInterface.smsLoginAuth(b.this.h, str, str2);
                    if (smsLoginAuth == null || !smsLoginAuth.getResCode().equals("000000")) {
                        if (smsLoginAuth != null) {
                            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth failed, res:" + smsLoginAuth.toString());
                        }
                        a unused = b.this.j = a.fail;
                        com.umeng.analytics.b.b(b.this.h, "CM_SDK_INIT_PHONE_NUM_FAIL");
                        bVar.g(b.m);
                        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth, failed");
                        return;
                    }
                    bVar.f3060b = smsLoginAuth.getResCode();
                    bVar.c = smsLoginAuth.getResMsg();
                    bVar.g(bVar);
                    a unused2 = b.this.j = a.success;
                    com.umeng.analytics.b.b(b.this.h, "CM_SDK_INIT_PHONE_NUM_SUC");
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "smsLoginAuth, success");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (XmlPullParserException e2) {
                    e2.printStackTrace();
                }
            }
        });
    }

    public void b(final String str, final String str2, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                c.b a2;
                boolean unused = b.this.i();
                String ringbackPolicy = RingbackManagerInterface.getRingbackPolicy(RingDDApp.c(), str);
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "getRingbackPolicy, result:" + ringbackPolicy);
                if (ag.c(ringbackPolicy) || (a2 = b.this.a(ringbackPolicy, "method_get_ringback_policy")) == null) {
                    b.this.a("cm_get_ringback_policy", "fail, return content wrong!", str2);
                    bVar.g(b.m);
                    return;
                }
                b.this.a(a2, "cm_get_ringback_policy", str2);
                bVar.g(a2);
            }
        });
    }

    public void b(final String str, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                c.b a2;
                boolean unused = b.this.i();
                String queryCrbtMonthPolicy = RingbackManagerInterface.queryCrbtMonthPolicy(RingDDApp.c(), b.this.h());
                com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "queryCrbtMonthPolicy, result:" + queryCrbtMonthPolicy);
                if (ag.c(queryCrbtMonthPolicy) || (a2 = b.this.a(queryCrbtMonthPolicy, "method_get_crbt_month_policy")) == null) {
                    b.this.a("cm_get_crbt_month_policy", "fail, return content wrong!", str);
                    bVar.g(b.m);
                    return;
                }
                b.this.a(a2, "cm_get_crbt_month_policy", str);
                bVar.g(a2);
            }
        });
    }

    public void c(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        d(e("<receivemdn>" + str + "</receivemdn><musicId>" + str2 + "</musicId>"), "/crbt/present", bVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, boolean):void
     arg types: [java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, boolean):void */
    private void d(String str, String str2, com.shoujiduoduo.util.b.b bVar) {
        a(str, str2, bVar, false);
    }

    private void a(String str, String str2, com.shoujiduoduo.util.b.b bVar, boolean z) {
        a(str, str2, bVar, "", z);
    }

    private void a(String str, String str2, com.shoujiduoduo.util.b.b bVar, String str3, boolean z) {
        final String str4 = str;
        final boolean z2 = z;
        final String str5 = str2;
        final String str6 = str3;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        h.a(new Runnable() {
            public void run() {
                c.b g;
                try {
                    byte[] bytes = str4.getBytes("UTF-8");
                    if (b.this.h == null) {
                        com.shoujiduoduo.base.a.a.c("ChinaMobileUtils", "context is null, main activity is destroyed");
                        return;
                    }
                    String a2 = d.a(b.this.h, (z2 ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode(str5, "UTF-8"), bytes, z2);
                    if (a2 != null) {
                        g = b.this.d(a2);
                        if (g == null) {
                            g = b.m;
                        }
                    } else {
                        g = b.m;
                    }
                    b.this.a(str5, g, str6);
                    bVar2.g(g);
                } catch (UnsupportedEncodingException e2) {
                    e2.printStackTrace();
                    g = b.m;
                }
            }
        });
    }

    private void b(String str, String str2, com.shoujiduoduo.util.b.b bVar, boolean z) {
        final String str3 = str;
        final boolean z2 = z;
        final String str4 = str2;
        final com.shoujiduoduo.util.b.b bVar2 = bVar;
        h.a(new Runnable() {
            public void run() {
                c.d dVar = new c.d();
                dVar.f3060b = "000000";
                dVar.c = "成功";
                if (ag.c(b.this.d) || !b.this.f3097b.containsKey(b.this.d) || !((Boolean) b.this.f3097b.get(b.this.d)).booleanValue()) {
                    try {
                        String a2 = d.a(b.this.h, (z2 ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode("/crbt/querymonth", "UTF-8"), str3.getBytes("UTF-8"), z2);
                        if (a2 != null) {
                            c.b a3 = b.this.d(a2);
                            if (a3 != null) {
                                dVar.d = a3;
                            } else {
                                dVar.d = b.m;
                            }
                        } else {
                            dVar.d = b.m;
                        }
                        b.this.a("/crbt/querymonth", dVar.d, "");
                    } catch (UnsupportedEncodingException e2) {
                        e2.printStackTrace();
                        dVar.d = b.m;
                    }
                } else {
                    dVar.d = new c.b();
                    dVar.d.f3060b = "000000";
                    dVar.d.c = "开通";
                }
                if (ag.c(b.this.d) || !b.this.c.containsKey(b.this.d) || !((Boolean) b.this.c.get(b.this.d)).booleanValue()) {
                    try {
                        String a4 = d.a(b.this.h, (z2 ? "http://mm.shoujiduoduo.com/mm/mmweb.php?from=ringdd_ar&cmd=" : "http://mm.shoujiduoduo.com/mm/mm.php?from=ringdd_ar&cmd=") + URLEncoder.encode("/crbt/open/check", "UTF-8"), str4.getBytes("UTF-8"), z2);
                        if (a4 != null) {
                            c.b a5 = b.this.d(a4);
                            if (a5 != null) {
                                dVar.f3063a = a5;
                            } else {
                                dVar.f3063a = b.m;
                            }
                        } else {
                            dVar.f3063a = b.m;
                        }
                        b.this.a("/crbt/open/check", dVar.f3063a, "");
                    } catch (UnsupportedEncodingException e3) {
                        e3.printStackTrace();
                        dVar.f3063a = b.m;
                    }
                } else {
                    dVar.f3063a = new c.b();
                    dVar.f3063a.f3060b = "000000";
                    dVar.f3063a.c = "开通";
                }
                bVar2.g(dVar);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(String str, c.b bVar, String str2) {
        boolean z = true;
        if (str.equals("/crbt/open/check")) {
            a(bVar, "cm_cailing_check", str2);
            HashMap<String, Boolean> hashMap = this.c;
            String str3 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap.put(str3, Boolean.valueOf(z));
        } else if (str.equals("/crbt/openmonth")) {
            a(bVar, "cm_open_vip", str2);
            HashMap<String, Boolean> hashMap2 = this.f3097b;
            String str4 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap2.put(str4, Boolean.valueOf(z));
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/querymonth")) {
            a(bVar, "cm_query_month", str2);
            HashMap<String, Boolean> hashMap3 = this.f3097b;
            String str5 = this.d;
            if (!bVar.c()) {
                z = false;
            }
            hashMap3.put(str5, Boolean.valueOf(z));
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/order")) {
            a(bVar, "cm_buy", str2);
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/simpOrder")) {
            a(bVar, "cm_buy_with_open", str2);
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/present")) {
            a(bVar, "cm_give", str2);
        } else if (str.equals("/crbt/box/default")) {
            a(bVar, "cm_set_default", str2);
        } else if (str.equals("/crbt/box/delete")) {
            a(bVar, "cm_delete_ring", str2);
        } else if (str.equals("/crbt/msisdn/query")) {
            a(bVar, "cm_query_default", str2);
        } else if (str.equals("/crbt/box/query")) {
            a(bVar, "cm_box_query", str2);
        } else if (str.equals("/crbt/open")) {
            a(bVar, "cm_open_cailing", str2);
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            }
        } else if (str.equals("/crbt/smsLoginAuth")) {
            a(bVar, "cm_sms_login_web", str2);
            if (bVar.c()) {
                this.f3096a.put(this.d, new C0043b(this.e, System.currentTimeMillis()));
            } else {
                this.f3096a.remove(this.d);
            }
        } else if (str.equals("/crbt/getValidateCode")) {
            a(bVar, "cm_get_validate_code", str2);
        } else if (str.equals("/crbt/orderbackmonth")) {
            a(bVar, "cm_orderback_month", str2);
        }
    }

    /* access modifiers changed from: private */
    public void a(c.b bVar, String str, String str2) {
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "method:" + str + ", res:" + bVar.toString());
        if (bVar.c()) {
            a(str, "success", str2);
        } else {
            a(str, "fail, " + bVar.toString(), str2);
        }
    }

    public void c(String str, com.shoujiduoduo.util.b.b bVar) {
        a("", "/crbt/open", bVar, str, false);
    }

    public void d(final com.shoujiduoduo.util.b.b bVar) {
        if (this.n) {
            com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "彩铃处于开通状态，直接返回缓存状态");
            c.b bVar2 = new c.b();
            bVar2.f3060b = "000000";
            bVar2.c = "包月处于开通状态";
            bVar.g(bVar2);
            return;
        }
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                c.b bVar = new c.b();
                Result queryCrbtMonth = RingbackManagerInterface.queryCrbtMonth(b.this.h, b.this.h());
                if (queryCrbtMonth != null) {
                    bVar.f3060b = queryCrbtMonth.getResCode();
                    bVar.c = queryCrbtMonth.getResMsg();
                } else {
                    bVar = b.m;
                }
                b.this.a("/crbt/querymonth", bVar, "");
                bVar.g(bVar);
            }
        });
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, boolean):void
     arg types: [java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, int]
     candidates:
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, com.shoujiduoduo.util.b.c$b, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, com.shoujiduoduo.util.b.c$b, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(com.shoujiduoduo.util.c.b, java.lang.String, java.lang.String, java.lang.String):void
      com.shoujiduoduo.util.c.b.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.b.b, boolean):void */
    public void d(String str, com.shoujiduoduo.util.b.b bVar) {
        if (!this.f3097b.containsKey(str) || !this.f3097b.get(str).booleanValue()) {
            a(e("<serviceId>" + h() + "</serviceId>" + "<MSISDN>" + str + "</MSISDN>"), "/crbt/querymonth", bVar, true);
            return;
        }
        com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "彩铃处于开通状态，直接返回缓存状态");
        c.b bVar2 = new c.b();
        bVar2.f3060b = "000000";
        bVar2.c = "包月处于开通状态";
        bVar.g(bVar2);
    }

    public void e(String str, com.shoujiduoduo.util.b.b bVar) {
        this.d = str;
        b(e("<serviceId>" + h() + "</serviceId>" + "<MSISDN>" + str + "</MSISDN>"), e("<MSISDN>" + str + "</MSISDN>"), bVar, true);
    }

    public void e(final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                c.d dVar = new c.d();
                dVar.f3060b = "000000";
                dVar.c = "成功";
                if (ag.c(b.this.d) || !b.this.f3097b.containsKey(b.this.d) || !((Boolean) b.this.f3097b.get(b.this.d)).booleanValue()) {
                    boolean unused = b.this.i();
                    try {
                        Result queryCrbtMonth = RingbackManagerInterface.queryCrbtMonth(b.this.h, b.this.h());
                        dVar.d = new c.b();
                        if (queryCrbtMonth != null) {
                            dVar.d.f3060b = queryCrbtMonth.getResCode();
                            dVar.d.c = queryCrbtMonth.getResMsg();
                        } else {
                            dVar.d = b.m;
                        }
                        b.this.a("/crbt/querymonth", dVar.d, "");
                    } catch (Exception e) {
                        e.printStackTrace();
                        dVar.d = b.m;
                    }
                } else {
                    dVar.d = new c.b();
                    dVar.d.f3060b = "000000";
                    dVar.d.c = "开通";
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "查询包月开通状态，返回缓存状态：开通");
                }
                if (ag.c(b.this.d) || !b.this.c.containsKey(b.this.d) || !((Boolean) b.this.c.get(b.this.d)).booleanValue()) {
                    try {
                        boolean unused2 = b.this.i();
                        CrbtOpenCheckRsp crbtOpenCheck = RingbackManagerInterface.crbtOpenCheck(b.this.h, "");
                        dVar.f3063a = new c.b();
                        if (crbtOpenCheck != null) {
                            dVar.f3063a.f3060b = crbtOpenCheck.getResCode();
                            dVar.f3063a.c = crbtOpenCheck.getResMsg();
                        } else {
                            dVar.f3063a = b.m;
                        }
                        b.this.a("/crbt/open/check", dVar.f3063a, "");
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        dVar.f3063a = b.m;
                    }
                } else {
                    dVar.f3063a = new c.b();
                    dVar.f3063a.f3060b = "000000";
                    dVar.f3063a.c = "开通";
                    com.shoujiduoduo.base.a.a.a("ChinaMobileUtils", "查询彩铃开通状态，返回缓存状态：开通");
                }
                bVar.g(dVar);
            }
        });
    }

    public void f(final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                Result cancelRingbackMonth = RingbackManagerInterface.cancelRingbackMonth(b.this.h, b.this.h());
                c.b bVar = new c.b();
                if (cancelRingbackMonth != null) {
                    bVar.c = cancelRingbackMonth.getResMsg();
                    bVar.f3060b = cancelRingbackMonth.getResCode();
                } else {
                    bVar = b.m;
                }
                b.this.a("/crbt/orderbackmonth", bVar, "");
                if (bVar != null) {
                    bVar.g(bVar);
                }
            }
        });
    }

    public c.b f() {
        i();
        CrbtListRsp crbtBox = RingbackManagerInterface.getCrbtBox(this.h);
        if (crbtBox == null || crbtBox.getToneInfos() == null) {
            a("/crbt/box/query", m, "");
            return null;
        }
        c.z zVar = new c.z();
        zVar.f3060b = crbtBox.getResCode();
        zVar.c = crbtBox.getResMsg();
        zVar.f3089a = new ArrayList();
        List<ToneInfo> toneInfos = crbtBox.getToneInfos();
        if (toneInfos != null) {
            for (ToneInfo next : toneInfos) {
                c.ae aeVar = new c.ae();
                aeVar.f = next.getPrice();
                aeVar.d = next.getSingerName();
                aeVar.e = next.getSingerNameLetter();
                aeVar.f3050a = next.getToneID();
                aeVar.f3051b = next.getToneName();
                aeVar.c = next.getToneNameLetter();
                aeVar.i = next.getTonePreListenAddress();
                aeVar.j = next.getToneType();
                aeVar.g = next.getToneValidDay();
                zVar.f3089a.add(aeVar);
            }
        }
        a("/crbt/box/query", zVar, "");
        return zVar;
    }

    public void g(final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                if (bVar != null) {
                    c.b f = b.this.f();
                    if (f != null) {
                        bVar.g(f);
                    } else {
                        bVar.g(b.m);
                    }
                }
            }
        });
    }

    public void f(final String str, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                Result defaultCrbt = RingbackManagerInterface.setDefaultCrbt(b.this.h, str, "1");
                c.b bVar = new c.b();
                if (defaultCrbt != null) {
                    bVar.f3060b = defaultCrbt.getResCode();
                    bVar.c = defaultCrbt.getResMsg();
                } else {
                    bVar = b.m;
                }
                b.this.a("/crbt/box/default", bVar, "");
                bVar.g(bVar);
            }
        });
    }

    public void g(final String str, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                Result deletePersonRing = RingbackManagerInterface.deletePersonRing(b.this.h, str);
                c.b bVar = new c.b();
                if (deletePersonRing != null) {
                    bVar.f3060b = deletePersonRing.getResCode();
                    bVar.c = deletePersonRing.getResMsg();
                } else {
                    bVar = b.m;
                }
                b.this.a("/crbt/box/delete", bVar, "");
                bVar.g(bVar);
            }
        });
    }

    public c.b b(String str) {
        i();
        CrbtListRsp defaultCrbt = RingbackManagerInterface.getDefaultCrbt(this.h, str);
        if (defaultCrbt != null) {
            c.z zVar = new c.z();
            zVar.f3089a = new ArrayList();
            zVar.f3060b = defaultCrbt.getResCode();
            zVar.c = defaultCrbt.getResMsg();
            List<ToneInfo> toneInfos = defaultCrbt.getToneInfos();
            if (toneInfos != null) {
                for (ToneInfo next : toneInfos) {
                    c.ae aeVar = new c.ae();
                    aeVar.f = next.getPrice();
                    aeVar.d = next.getSingerName();
                    aeVar.e = next.getSingerNameLetter();
                    aeVar.f3050a = next.getToneID();
                    aeVar.f3051b = next.getToneName();
                    aeVar.c = next.getToneNameLetter();
                    aeVar.i = next.getTonePreListenAddress();
                    aeVar.j = next.getToneType();
                    aeVar.g = next.getToneValidDay();
                    zVar.f3089a.add(aeVar);
                }
            }
            a("/crbt/msisdn/query", zVar, "");
            return zVar;
        }
        a("/crbt/msisdn/query", m, "");
        return m;
    }

    public void h(final String str, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                CrbtListRsp defaultCrbt = RingbackManagerInterface.getDefaultCrbt(b.this.h, str);
                if (defaultCrbt != null) {
                    c.z zVar = new c.z();
                    zVar.f3089a = new ArrayList();
                    zVar.f3060b = defaultCrbt.getResCode();
                    zVar.c = defaultCrbt.getResMsg();
                    List<ToneInfo> toneInfos = defaultCrbt.getToneInfos();
                    if (toneInfos != null) {
                        for (ToneInfo next : toneInfos) {
                            c.ae aeVar = new c.ae();
                            aeVar.f = next.getPrice();
                            aeVar.d = next.getSingerName();
                            aeVar.e = next.getSingerNameLetter();
                            aeVar.f3050a = next.getToneID();
                            aeVar.f3051b = next.getToneName();
                            aeVar.c = next.getToneNameLetter();
                            aeVar.i = next.getTonePreListenAddress();
                            aeVar.j = next.getToneType();
                            aeVar.g = next.getToneValidDay();
                            zVar.f3089a.add(aeVar);
                        }
                    }
                    b.this.a("/crbt/msisdn/query", zVar, "");
                    bVar.g(zVar);
                    return;
                }
                b.this.a("/crbt/msisdn/query", b.m, "");
                bVar.g(b.m);
            }
        });
    }

    public void i(final String str, final com.shoujiduoduo.util.b.b bVar) {
        h.a(new Runnable() {
            public void run() {
                boolean unused = b.this.i();
                CrbtOpenCheckRsp crbtOpenCheck = RingbackManagerInterface.crbtOpenCheck(b.this.h, str);
                c.b bVar = new c.b();
                if (crbtOpenCheck != null) {
                    bVar.f3060b = crbtOpenCheck.getResCode();
                    bVar.c = crbtOpenCheck.getResMsg();
                    if (bVar.f3060b == null) {
                        bVar = b.m;
                    }
                } else {
                    bVar = b.m;
                }
                b.this.a("/crbt/open/check", bVar, "");
                bVar.g(bVar);
            }
        });
    }

    public c.b c(String str) {
        i();
        CrbtOpenCheckRsp crbtOpenCheck = RingbackManagerInterface.crbtOpenCheck(this.h, str);
        if (crbtOpenCheck != null) {
            c.b bVar = new c.b();
            bVar.f3060b = crbtOpenCheck.getResCode();
            bVar.c = crbtOpenCheck.getResMsg();
            a("/crbt/open/check", bVar, "");
            return bVar;
        }
        a("/crbt/open/check", m, "");
        return m;
    }

    private String e(String str) {
        return "<?xml version='1.0' encoding='UTF-8'?><request>" + str + "</request>";
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        s.a("cm:" + str, str2, str3);
    }
}
