package com.shoujiduoduo.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.media.RingtoneManager;
import android.net.Uri;
import android.provider.ContactsContract;
import cn.banshenggua.aichang.room.message.SocketMessage;
import com.igexin.download.Downloads;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/* compiled from: DownloadManager */
public class m extends SQLiteOpenHelper {
    /* access modifiers changed from: private */
    public static int A = 2;
    /* access modifiers changed from: private */
    public static int B = 3;

    /* renamed from: a  reason: collision with root package name */
    private static int f3200a = 0;
    private static m f;
    /* access modifiers changed from: private */
    public static int g = -1;
    /* access modifiers changed from: private */
    public static int h = -2;
    private static int i = -3;
    /* access modifiers changed from: private */
    public static int j = -4;
    /* access modifiers changed from: private */
    public static int k = -5;
    /* access modifiers changed from: private */
    public static int l = -6;
    /* access modifiers changed from: private */
    public static int m = -7;
    /* access modifiers changed from: private */
    public static int n = -8;
    /* access modifiers changed from: private */
    public static int o = -9;
    /* access modifiers changed from: private */
    public static int p = -10;
    /* access modifiers changed from: private */
    public static int q = -11;
    private static int r = -12;
    private static int s = -13;
    /* access modifiers changed from: private */
    public static int t = -14;
    private static int u = -15;
    /* access modifiers changed from: private */
    public static int v = -16;
    private static int w = -17;
    private static int x = -18;
    /* access modifiers changed from: private */
    public static int y = 0;
    /* access modifiers changed from: private */
    public static int z = 1;
    private int C = 10;
    /* access modifiers changed from: private */
    public HashMap<Integer, a> D = new HashMap<>(this.C);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public PlayerService f3201b = null;
    private b c = null;
    /* access modifiers changed from: private */
    public ArrayList<i> d = new ArrayList<>();
    /* access modifiers changed from: private */
    public Context e;

    public void a(i iVar) {
        if (iVar != null) {
            this.d.add(iVar);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private m(Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i2) {
        super(context, str, cursorFactory, i2);
        String str2 = null;
        this.e = context;
        com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager constructor begins.");
        s();
        SQLiteDatabase readableDatabase = getReadableDatabase();
        if (readableDatabase != null) {
            Cursor rawQuery = readableDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'ringtoneduoduo_downloadtable'", null);
            if (rawQuery == null || rawQuery.getCount() <= 0) {
                rawQuery = readableDatabase.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = 'ringtoneduoduo_downloadtable_2'", null);
                if (rawQuery != null && rawQuery.getCount() > 0) {
                    str2 = "ringtoneduoduo_downloadtable_2";
                }
            } else {
                str2 = "ringtoneduoduo_downloadtable";
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            readableDatabase.close();
        }
        if (str2 != null) {
            com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager: rename thread start!");
            this.c = new b(str2);
            this.c.start();
        }
        com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager constructor ends.");
    }

    public void a(PlayerService playerService) {
        this.f3201b = playerService;
    }

    public static m a(Context context) {
        m mVar;
        com.shoujiduoduo.base.a.a.a("DownloadManager", "enter DownloadManger.getInstance.");
        synchronized ("DownloadManager") {
            if (f == null) {
                f = new m(context, "duoduo.ringtone.database", null, 3);
            } else {
                com.shoujiduoduo.base.a.a.a("DownloadManager", "mThis = " + f.toString());
            }
            mVar = f;
        }
        return mVar;
    }

    private void s() {
        com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager CreateTable begins.");
        synchronized ("DownloadManager") {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager CreateTable 1");
                try {
                    writableDatabase.execSQL("CREATE TABLE IF NOT EXISTS ringtoneduoduo_resourcetable_3 (id INTEGER PRIMARY KEY AUTOINCREMENT, rid INTEGER, name VARCHAR, artist VARCHAR, album VARCHAR, down_size INTEGER, total_size INTEGER, bitrate INTEGER, format VARCHAR, url VARCHAR, path VARCHAR);");
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "Create ringtoneduoduo_resourcetable_3");
                } catch (SQLException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "Create ringtoneduoduo_resourcetable_3 failed!");
                }
            }
        }
        com.shoujiduoduo.base.a.a.a("DownloadManager", "DownloadManager CreateTable ends.");
    }

    public com.shoujiduoduo.base.bean.i a(int i2, String str, String str2, String str3, int i3, int i4, int i5, String str4, String str5) {
        com.shoujiduoduo.base.bean.i iVar;
        synchronized ("DownloadManager") {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                f.c("addDownloadInfo returns null because mDB is null!");
                iVar = null;
            } else {
                String a2 = a(writableDatabase, str, str2, i2, str4);
                String str6 = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i2 + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(str) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(str2) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(str3) + MiPushClient.ACCEPT_TIME_SEPARATOR + Integer.toString(i3) + MiPushClient.ACCEPT_TIME_SEPARATOR + Integer.toString(i4) + MiPushClient.ACCEPT_TIME_SEPARATOR + i5 + ",'" + str4 + "'," + DatabaseUtils.sqlEscapeString(str5) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(a2) + ");";
                com.shoujiduoduo.base.a.a.a("DownloadManager", "SQLITE: " + str6);
                try {
                    writableDatabase.execSQL(str6);
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "Success: Add new ring to the table.");
                    iVar = new com.shoujiduoduo.base.bean.i(str, str2, i2, i3, i4, i5, str4, str5);
                    iVar.e(a2);
                } catch (SQLException e2) {
                    e2.printStackTrace();
                    f.c("addDownloadInfo returns null because fail to insert into resource table!\n" + com.shoujiduoduo.base.a.b.a(e2));
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "Database: insert into table FAILED!");
                    iVar = null;
                }
            }
        }
        return iVar;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r6) {
        /*
            r5 = this;
            java.lang.String r1 = "DownloadManager"
            monitor-enter(r1)
            android.database.sqlite.SQLiteDatabase r0 = r5.getWritableDatabase()     // Catch:{ all -> 0x0030 }
            if (r0 != 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
        L_0x000a:
            return
        L_0x000b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0030 }
            r2.<init>()     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "delete from ringtoneduoduo_resourcetable_3 where rid='"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "'"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0030 }
            r0.execSQL(r2)     // Catch:{ SQLException -> 0x0033 }
            java.lang.String r0 = "DownloadManager"
            java.lang.String r3 = "Success: delete ring record from the table."
            com.shoujiduoduo.base.a.a.a(r0, r3)     // Catch:{ SQLException -> 0x0033 }
        L_0x002e:
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            goto L_0x000a
        L_0x0030:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0030 }
            throw r0
        L_0x0033:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x0030 }
            java.lang.String r0 = "DownloadManager"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0030 }
            r3.<init>()     // Catch:{ all -> 0x0030 }
            java.lang.String r4 = "Database: exec \""
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r2 = r3.append(r2)     // Catch:{ all -> 0x0030 }
            java.lang.String r3 = "\" FAILED!"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0030 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0030 }
            com.shoujiduoduo.base.a.a.c(r0, r2)     // Catch:{ all -> 0x0030 }
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.a(int):void");
    }

    public com.shoujiduoduo.base.bean.i b(int i2) {
        if (i2 == 0) {
            return null;
        }
        synchronized ("DownloadManager") {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase == null) {
                return null;
            }
            try {
                String str = "select * from ringtoneduoduo_resourcetable_3 where rid=" + i2 + " order by rid;";
                com.shoujiduoduo.base.a.a.a("DownloadManager", str);
                Cursor rawQuery = writableDatabase.rawQuery(str, null);
                if (rawQuery == null || rawQuery.getCount() == 0) {
                    if (rawQuery != null) {
                        rawQuery.close();
                    }
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: c == null || c.getCount() == 0");
                    return null;
                } else if (rawQuery.getCount() != 1) {
                    rawQuery.close();
                    a(i2);
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: c.getCount() != 1");
                    return null;
                } else if (rawQuery.moveToNext()) {
                    int i3 = rawQuery.getInt(1);
                    int i4 = rawQuery.getInt(5);
                    int i5 = rawQuery.getInt(6);
                    int i6 = rawQuery.getInt(7);
                    String string = rawQuery.getString(8);
                    String string2 = rawQuery.getString(2);
                    String string3 = rawQuery.getString(3);
                    String string4 = rawQuery.getString(9);
                    String string5 = rawQuery.getString(10);
                    rawQuery.close();
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "searchSongById: rid=" + i3 + ",down_size=" + i4 + ",total_size=" + i5 + "bitrate=" + i6 + "format=" + string + "url=" + string4 + ", path = " + string5);
                    com.shoujiduoduo.base.bean.i iVar = new com.shoujiduoduo.base.bean.i(string2, string3, i3, i4, i5, i6, string, string4);
                    iVar.e(string5);
                    return iVar;
                } else {
                    rawQuery.close();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: return null");
                    return null;
                }
            } catch (SQLiteException e2) {
                e2.printStackTrace();
                com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: database query failed!");
            }
        }
    }

    public void a() {
        synchronized ("DownloadManager") {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "select * from ringtoneduoduo_resourcetable_3;");
                    Cursor rawQuery = writableDatabase.rawQuery("select * from ringtoneduoduo_resourcetable_3;", null);
                    if (rawQuery == null || rawQuery.getCount() == 0) {
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: c == null || c.getCount() == 0");
                        return;
                    }
                    ArrayList arrayList = new ArrayList();
                    while (rawQuery.moveToNext()) {
                        int i2 = rawQuery.getInt(1);
                        int i3 = rawQuery.getInt(5);
                        int i4 = rawQuery.getInt(6);
                        String string = rawQuery.getString(8);
                        String string2 = rawQuery.getString(2);
                        String string3 = rawQuery.getString(3);
                        String string4 = rawQuery.getString(10);
                        com.shoujiduoduo.base.a.a.a("DownloadManager", "searchSongById: i1=" + i2 + ",i2=" + i3 + ",i3=" + i4);
                        if ((this.f3201b == null || this.f3201b.f() != i2) && i2 != f3200a && !com.shoujiduoduo.a.b.b.b().a("" + i2, "favorite_ring_list")) {
                            com.shoujiduoduo.base.bean.i iVar = new com.shoujiduoduo.base.bean.i(string2, string3, i2, i3, i4, 0, string, "");
                            iVar.e(string4);
                            arrayList.add(iVar);
                        }
                    }
                    rawQuery.close();
                    for (int i5 = 0; i5 < arrayList.size(); i5++) {
                        com.shoujiduoduo.base.bean.i iVar2 = (com.shoujiduoduo.base.bean.i) arrayList.get(i5);
                        a(iVar2.c);
                        j.a(iVar2.l());
                    }
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: return null");
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongById: database query failed!");
                }
            }
        }
    }

    public boolean a(int i2, int i3) {
        boolean z2 = false;
        synchronized ("DownloadManager") {
            SQLiteDatabase writableDatabase = getWritableDatabase();
            if (writableDatabase != null) {
                try {
                    String str = "UPDATE ringtoneduoduo_resourcetable_3 SET down_size=" + i2 + " WHERE rid=" + i3;
                    com.shoujiduoduo.base.a.a.a("DownloadManager", str);
                    writableDatabase.execSQL(str);
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "Success:updateDownloadProgress.");
                    z2 = true;
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "FAIL:updateDownloadProgress.");
                }
            }
        }
        return z2;
    }

    public boolean a(com.shoujiduoduo.base.bean.i iVar) {
        boolean z2 = false;
        if (iVar.c != 0) {
            synchronized ("DownloadManager") {
                SQLiteDatabase writableDatabase = getWritableDatabase();
                if (writableDatabase != null) {
                    try {
                        String str = "UPDATE ringtoneduoduo_resourcetable_3 SET down_size=" + iVar.d + ",total_size=" + iVar.e + ",bitrate=" + iVar.f + ",format='" + iVar.g + "',url=" + DatabaseUtils.sqlEscapeString(iVar.h) + " WHERE rid=" + iVar.c;
                        com.shoujiduoduo.base.a.a.a("DownloadManager", str);
                        writableDatabase.execSQL(str);
                        com.shoujiduoduo.base.a.a.a("DownloadManager", "Success:updateSongInfo.");
                        z2 = true;
                    } catch (SQLiteException e2) {
                        e2.printStackTrace();
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "FAIL:updateSongInfo.");
                    }
                }
            }
        }
        return z2;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
    }

    private boolean a(SQLiteDatabase sQLiteDatabase, String str) {
        boolean z2 = true;
        if (!(sQLiteDatabase == null || str == null || str.length() == 0)) {
            synchronized ("DownloadManager") {
                try {
                    String str2 = "select * from ringtoneduoduo_resourcetable_3 where path=" + DatabaseUtils.sqlEscapeString(str) + " order by rid;";
                    com.shoujiduoduo.base.a.a.a("DownloadManager", str2);
                    Cursor rawQuery = sQLiteDatabase.rawQuery(str2, null);
                    if (rawQuery == null || rawQuery.getCount() == 0) {
                        if (rawQuery != null) {
                            rawQuery.close();
                        }
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongByPath: c == null || c.getCount() == 0");
                    } else {
                        rawQuery.close();
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongByPath: return null");
                        z2 = false;
                    }
                } catch (SQLiteException e2) {
                    e2.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "searchSongByPath: database query failed!");
                }
            }
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public String a(SQLiteDatabase sQLiteDatabase, String str, String str2, int i2, String str3) {
        String str4;
        String str5;
        String str6 = null;
        if ((str == null || str.length() == 0) && (str2 == null || str2.length() == 0)) {
            StringBuilder append = new StringBuilder().append(o.b()).append(i2).append(".");
            if (str3 == null || str3.length() == 0) {
                str3 = "mp3";
            }
            return append.append(str3).toString();
        }
        if (str != null) {
            str4 = f.e(str.replaceAll("[\\$\\|\\&\\*\\[\\]\\\"\\'\\\\\\/:;<>@#{}]", "").replaceAll("\\s+", " "));
        } else {
            str4 = null;
        }
        if (str2 != null) {
            str6 = f.e(str2.replaceAll("[\\$\\|\\&\\*\\[\\]\\\"\\'\\\\\\/:;<>@#{}]", "").replaceAll("\\s+", " "));
        }
        String str7 = str4 + "_" + str6;
        if (str7.length() > 100) {
            str7 = str7.substring(0, 100);
        }
        String string = this.e.getResources().getString(R.string.cache_file_prefix);
        StringBuilder append2 = new StringBuilder().append(o.b()).append(string).append("_").append(str7).append(".");
        if (str3 == null || str3.length() == 0) {
            str5 = "mp3";
        } else {
            str5 = str3;
        }
        String sb = append2.append(str5).toString();
        if (a(sQLiteDatabase, sb)) {
            return sb;
        }
        StringBuilder append3 = new StringBuilder().append(o.b()).append(string).append("_").append(str7 + "_" + i2).append(".");
        if (str3 == null || str3.length() == 0) {
            str3 = "mp3";
        }
        return append3.append(str3).toString();
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i2, int i3) {
        com.shoujiduoduo.base.a.a.a("DownloadManager", "database upgrade begins!");
        try {
            sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS ringtoneduoduo_resourcetable_3 (id INTEGER PRIMARY KEY AUTOINCREMENT, rid INTEGER, name VARCHAR, artist VARCHAR, album VARCHAR, down_size INTEGER, total_size INTEGER, bitrate INTEGER, format VARCHAR, url VARCHAR, path VARCHAR);");
            com.shoujiduoduo.base.a.a.a("DownloadManager", "onUpgrade: Create ringtoneduoduo_resourcetable_3");
            if (i2 == 1) {
                try {
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "select * from ringtoneduoduo_downloadtable;");
                    Cursor rawQuery = sQLiteDatabase.rawQuery("select * from ringtoneduoduo_downloadtable;", null);
                    if (rawQuery == null) {
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: c == null");
                        return;
                    }
                    while (rawQuery.moveToNext()) {
                        int i4 = rawQuery.getInt(1);
                        String string = rawQuery.getString(2);
                        String string2 = rawQuery.getString(3);
                        String string3 = rawQuery.getString(4);
                        int i5 = rawQuery.getInt(5);
                        int i6 = rawQuery.getInt(6);
                        String a2 = com.shoujiduoduo.base.bean.i.a(i4, null);
                        if (i5 == i6) {
                            String str = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i4 + ",'" + DatabaseUtils.sqlEscapeString(string) + "','" + DatabaseUtils.sqlEscapeString(string2) + "','" + DatabaseUtils.sqlEscapeString(string3) + "'," + Integer.toString(i5) + MiPushClient.ACCEPT_TIME_SEPARATOR + Integer.toString(i6) + MiPushClient.ACCEPT_TIME_SEPARATOR + "128000" + MiPushClient.ACCEPT_TIME_SEPARATOR + "'mp3'" + ",'" + "" + "'" + ",'" + DatabaseUtils.sqlEscapeString(a2) + "'" + ");";
                            com.shoujiduoduo.base.a.a.a("DownloadManager", "SQLITE: " + str);
                            try {
                                sQLiteDatabase.execSQL(str);
                                com.shoujiduoduo.base.a.a.a("DownloadManager", "onUpgrade: Success: Add new ring to the table.");
                            } catch (SQLException e2) {
                                e2.printStackTrace();
                                com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: Database: insert into table FAILED!");
                            }
                        }
                    }
                    rawQuery.close();
                } catch (SQLiteException e3) {
                    e3.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: database operation failed!");
                }
            } else if (i2 == 2) {
                try {
                    com.shoujiduoduo.base.a.a.a("DownloadManager", "select * from ringtoneduoduo_downloadtable_2;");
                    Cursor rawQuery2 = sQLiteDatabase.rawQuery("select * from ringtoneduoduo_downloadtable_2;", null);
                    if (rawQuery2 == null) {
                        com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: c == null");
                        return;
                    }
                    while (rawQuery2.moveToNext()) {
                        int i7 = rawQuery2.getInt(1);
                        String string4 = rawQuery2.getString(2);
                        String string5 = rawQuery2.getString(3);
                        String string6 = rawQuery2.getString(4);
                        int i8 = rawQuery2.getInt(5);
                        int i9 = rawQuery2.getInt(6);
                        int i10 = rawQuery2.getInt(7);
                        String string7 = rawQuery2.getString(8);
                        String str2 = "insert into ringtoneduoduo_resourcetable_3 (rid, name, artist, album, down_size, total_size, bitrate, format, url, path)VALUES (" + i7 + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(string4) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(string5) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(string6) + MiPushClient.ACCEPT_TIME_SEPARATOR + i8 + MiPushClient.ACCEPT_TIME_SEPARATOR + i9 + MiPushClient.ACCEPT_TIME_SEPARATOR + i10 + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(string7) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(rawQuery2.getString(9)) + MiPushClient.ACCEPT_TIME_SEPARATOR + DatabaseUtils.sqlEscapeString(com.shoujiduoduo.base.bean.i.a(i7, string7)) + ");";
                        com.shoujiduoduo.base.a.a.a("DownloadManager", "SQLITE: " + str2);
                        try {
                            sQLiteDatabase.execSQL(str2);
                            com.shoujiduoduo.base.a.a.a("DownloadManager", "onUpgrade: Success: Add new ring to the table.");
                        } catch (SQLException e4) {
                            e4.printStackTrace();
                            com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: Database: insert into table FAILED!");
                        }
                    }
                    rawQuery2.close();
                } catch (SQLiteException e5) {
                    e5.printStackTrace();
                    com.shoujiduoduo.base.a.a.c("DownloadManager", "onUpgrade: database operation failed!");
                }
            }
        } catch (SQLException e6) {
            e6.printStackTrace();
            com.shoujiduoduo.base.a.a.c("DownloadManager", "Create database failed!");
        }
    }

    /* compiled from: DownloadManager */
    class a extends Thread {

        /* renamed from: b  reason: collision with root package name */
        private final String f3203b = "DownloadThread";
        private com.shoujiduoduo.base.bean.i c = null;
        /* access modifiers changed from: private */
        public boolean d = false;
        private boolean e = true;
        private String f = "success";
        private String g = "no_network";
        private String h = "get_url_failed";
        private String i = "build_connection_failed";
        private String j = "ringdown_fail_file_not_found";
        private String k = "ringdown_fail_io_exception";
        private String l = "ringdown_fail_disk_io_no_space";

        public a(com.shoujiduoduo.base.bean.i iVar) {
            this.c = iVar;
        }

        public void a(boolean z) {
            this.d = z;
        }

        public boolean a() {
            return this.e;
        }

        public void start() {
            super.start();
        }

        private void a(ArrayList<i> arrayList, com.shoujiduoduo.base.bean.i iVar, int i2) {
            if (arrayList == null) {
                return;
            }
            if (i2 < 0) {
                if (!this.d) {
                    Iterator<i> it = arrayList.iterator();
                    while (it.hasNext()) {
                        it.next().a(iVar, i2);
                    }
                }
            } else if (i2 == m.y) {
                if (!this.d) {
                    Iterator<i> it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        it2.next().d(iVar);
                    }
                } else if (iVar.e > 0 || iVar.d > 0) {
                    m.this.a(iVar);
                }
            } else if (i2 == m.A) {
                if (!this.d) {
                    Iterator<i> it3 = arrayList.iterator();
                    while (it3.hasNext()) {
                        it3.next().a(iVar);
                    }
                }
            } else if (i2 == m.B) {
                m.this.a(iVar);
                if (!this.d) {
                    Iterator<i> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        it4.next().b(iVar);
                    }
                }
            } else if (i2 == m.z) {
                m.this.a(iVar);
                if (!this.d) {
                    Iterator<i> it5 = arrayList.iterator();
                    while (it5.hasNext()) {
                        it5.next().c(iVar);
                    }
                }
            }
        }

        private void b() {
            synchronized (m.this.D) {
                if (m.this.D.get(Integer.valueOf(this.c.c)) == this) {
                    m.this.D.remove(Integer.valueOf(this.c.c));
                }
            }
        }

        /* compiled from: DownloadManager */
        private class b {

            /* renamed from: a  reason: collision with root package name */
            String f3206a;

            /* renamed from: b  reason: collision with root package name */
            String f3207b;
            int c;
            boolean d;

            public b(String str, String str2, int i, boolean z) {
                this.f3206a = str;
                this.f3207b = str2;
                this.c = i;
                this.d = z;
            }
        }

        /* renamed from: com.shoujiduoduo.util.m$a$a  reason: collision with other inner class name */
        /* compiled from: DownloadManager */
        private class C0047a extends Exception {

            /* renamed from: a  reason: collision with root package name */
            int f3204a;

            public C0047a(int i) {
                this.f3204a = i;
            }
        }

        private b a(String str) throws C0047a {
            int i2 = this.c.c;
            int i3 = this.c.f;
            StringBuilder sb = new StringBuilder();
            sb.append("&rid=").append(i2).append("&network=").append(NetworkStateUtil.d()).append("&fmt=").append(this.c.g).append("&br=").append(i3 == 0 ? "" : Integer.valueOf(i3)).append("&from=").append(m.this.f3201b != null ? m.this.f3201b.d() : "").append("&reason=").append(str).append("&cdn=").append(l.a().b());
            String b2 = s.b(sb.toString());
            if (ag.c(b2)) {
                com.shoujiduoduo.base.a.a.c("DownloadThread", "getAntiStealingLink: (" + i2 + "): fail to get ring URL 1.");
                throw new C0047a(m.h);
            } else if (this.d) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "getAntiStealingLink: (" + i2 + "): Cancel 1.");
                throw new C0047a(m.y);
            } else {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "getAntiStealingLink: (" + i2 + ") return res:" + b2);
                String[] split = b2.split("\t", 0);
                if (split.length != 3) {
                    com.shoujiduoduo.base.a.a.c("DownloadThread", "getAntiStealingLink: (" + i2 + "): fail to get ring URL 3. ");
                    f.c("get AntiStealingLink failure!\nparaString = " + sb.toString() + "\nreturn content = " + b2);
                    throw new C0047a(m.j);
                }
                com.shoujiduoduo.base.a.a.a("DownloadThread", "getAntiStealingLink: (" + i2 + "): bitrate =" + split[0]);
                com.shoujiduoduo.base.a.a.a("DownloadThread", "getAntiStealingLink: (" + i2 + "): format =" + split[1]);
                com.shoujiduoduo.base.a.a.a("DownloadThread", "getAntiStealingLink: (" + i2 + "): url =" + split[2]);
                int a2 = q.a(split[0], 0);
                if (a2 == 0) {
                    com.shoujiduoduo.base.a.a.c("DownloadThread", "getAntiStealingLink(" + i2 + "): fail to get ring URL 4. ");
                    f.c("parse Error! NumberformatException." + b2);
                    throw new C0047a(m.k);
                }
                String lowerCase = split[1].toLowerCase();
                if (this.c.g.compareToIgnoreCase(lowerCase) == 0) {
                    return new b(split[2], lowerCase, a2, false);
                }
                com.shoujiduoduo.base.a.a.c("DownloadThread", "getAntiStealingLink: (" + i2 + "), mData.format:" + this.c.g);
                f.c("format error! \n paraString = " + sb.toString() + "\nreturn content = " + b2);
                throw new C0047a(m.k);
            }
        }

        private b b(boolean z) throws C0047a {
            if (z) {
                return a("failconnect");
            }
            if (this.c.g != null && !this.c.g.equalsIgnoreCase("mp3") && this.c.g.length() != 0) {
                if (this.c.g.equalsIgnoreCase("aac")) {
                    if (NetworkStateUtil.b()) {
                        if (this.c.k()) {
                            return new b(this.c.d(), "aac", this.c.e(), true);
                        }
                    } else if (this.c.j()) {
                        return new b(this.c.b(), "aac", this.c.c(), true);
                    }
                }
                return a("nolink");
            } else if (this.c.h()) {
                return new b(this.c.f(), "mp3", this.c.g(), true);
            } else {
                if (!this.c.n.equals("")) {
                    return new b(this.c.n, "mp3", 128000, true);
                }
                return a("wantmp3");
            }
        }

        private String b(String str) {
            if (ag.c(str)) {
                return "";
            }
            try {
                int indexOf = str.indexOf("//");
                return str.substring(indexOf + "//".length(), str.indexOf("."));
            } catch (Exception e2) {
                return l.a().b().split(".")[0];
            }
        }

        private String c(String str) {
            return "cdn_build_connection_" + b(str);
        }

        private HttpURLConnection d(String str) throws C0047a {
            boolean z;
            boolean z2 = false;
            int i2 = this.c.d;
            com.shoujiduoduo.base.a.a.a("DownloadThread", "buildConnection, url = " + str);
            if (!ag.c(str)) {
                if (str.contains("shoujiduoduo") && str.contains("cdnring")) {
                    z2 = true;
                }
                z = z2;
            } else {
                z = false;
            }
            HashMap hashMap = new HashMap();
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                httpURLConnection.setConnectTimeout(q.a(com.umeng.a.a.a().a(m.this.e, "connect_time_out"), 8) * 1000);
                httpURLConnection.setRequestProperty("RANGE", "bytes=" + i2 + "-");
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): http status code = " + responseCode + " ,url = " + str);
                if ((responseCode < 200 || responseCode >= 300) && responseCode >= 0) {
                    com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): http status code error!");
                    a(str, "", "", responseCode, "status code error!");
                    if (z) {
                        hashMap.put(Parameters.RESOLUTION, "StatusCodeError");
                        hashMap.put("code", "" + responseCode);
                        com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                    }
                    throw new C0047a(m.p);
                }
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "success");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                return httpURLConnection;
            } catch (UnknownHostException e2) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): UnknownHostException!");
                a(str, "", "", -1, com.shoujiduoduo.base.a.b.a(e2));
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "UnknownHostException");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                throw new C0047a(m.l);
            } catch (IndexOutOfBoundsException e3) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): IndexOutOfBoundsException!");
                a(str, "", "", -1, com.shoujiduoduo.base.a.b.a(e3));
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "IndexOutOfBoundsException");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                throw new C0047a(m.m);
            } catch (MalformedURLException e4) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): MalformedURLException!");
                a(str, "", "", -1, com.shoujiduoduo.base.a.b.a(e4));
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "MalformedURLException");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                throw new C0047a(m.n);
            } catch (IOException e5) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): IOException!" + com.shoujiduoduo.base.a.b.a(e5));
                a(str, "", "", -1, com.shoujiduoduo.base.a.b.a(e5));
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "IOException");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                throw new C0047a(m.o);
            } catch (NullPointerException e6) {
                com.shoujiduoduo.base.a.a.a("DownloadThread", "downloadSong(" + this.c.c + "): Null Pointer Exception!");
                a(str, "", "", -1, com.shoujiduoduo.base.a.b.a(e6));
                if (z) {
                    hashMap.put(Parameters.RESOLUTION, "NullPointerException");
                    com.umeng.analytics.b.a(RingDDApp.c(), c(str), hashMap);
                }
                throw new C0047a(m.q);
            }
        }

        private void a(String str, String str2, String str3, int i2, String str4) {
            com.umeng.analytics.b.b(RingDDApp.c(), "CONNECTION_ERROR");
        }

        private void a(String str, String str2) {
            String str3;
            if (!ag.c(str2) && str2.contains("shoujiduoduo") && str2.contains("cdnring")) {
                if (ag.c(str2)) {
                    str3 = l.a().b().split(".")[0];
                } else {
                    try {
                        int indexOf = str2.indexOf("//");
                        str3 = str2.substring(indexOf + "//".length(), str2.indexOf("."));
                    } catch (Exception e2) {
                        str3 = SocketMessage.MSG_ERROR_KEY;
                    }
                }
                HashMap hashMap = new HashMap();
                hashMap.put(Parameters.RESOLUTION, str);
                com.shoujiduoduo.base.a.a.a("DownloadThread", "logRingDown, key:ring_down_" + str3 + ", res:" + str);
                com.umeng.analytics.b.a(RingDDApp.c(), "ring_down_" + str3, hashMap);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.s.a(java.lang.String, boolean, java.lang.String):java.lang.String
         arg types: [java.lang.String, int, java.lang.String]
         candidates:
          com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, org.json.JSONObject):java.lang.String
          com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, com.shoujiduoduo.util.s$a):void
          com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, java.lang.String):void
          com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String, boolean):boolean
          com.shoujiduoduo.util.s.a(java.lang.String, boolean, java.lang.String):java.lang.String */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.s.b(java.lang.String, boolean):com.shoujiduoduo.base.bean.h
         arg types: [java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.s.b(java.lang.String, java.lang.String):void
          com.shoujiduoduo.util.s.b(java.lang.String, boolean):com.shoujiduoduo.base.bean.h */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.s.a(java.lang.String, boolean):com.shoujiduoduo.base.bean.h
         arg types: [java.lang.String, int]
         candidates:
          com.shoujiduoduo.util.s.a(java.lang.String, java.lang.String):java.lang.String
          com.shoujiduoduo.util.s.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):boolean
          com.shoujiduoduo.util.s.a(java.lang.String, boolean):com.shoujiduoduo.base.bean.h */
        /* JADX WARNING: Code restructure failed: missing block: B:148:0x062f, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
            r2.printStackTrace();
            com.shoujiduoduo.base.a.a.c("DownloadThread", "IOException, check duoduo DIR and retry");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:151:0x063e, code lost:
            if (com.shoujiduoduo.util.k.a() != false) goto L_0x0640;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:154:0x064f, code lost:
            r7 = new java.io.RandomAccessFile(r0.c.l(), "rw");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:159:0x0670, code lost:
            r2 = r21;
            r7 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:160:0x0675, code lost:
            throw r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:161:0x0676, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:162:0x0677, code lost:
            r7 = r2;
            r2 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:216:0x0846, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:217:0x0847, code lost:
            r7 = r2;
            r2 = 0;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:158:0x066f A[Catch:{ IOException -> 0x062f, FileNotFoundException -> 0x066f, Exception -> 0x0846, FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }, ExcHandler: FileNotFoundException (e java.io.FileNotFoundException), Splitter:B:112:0x04e4] */
        /* JADX WARNING: Removed duplicated region for block: B:165:0x06ae  */
        /* JADX WARNING: Removed duplicated region for block: B:168:0x06be  */
        /* JADX WARNING: Removed duplicated region for block: B:173:0x06d3  */
        /* JADX WARNING: Removed duplicated region for block: B:194:0x07a7  */
        /* JADX WARNING: Removed duplicated region for block: B:201:0x07f6  */
        /* JADX WARNING: Removed duplicated region for block: B:204:0x0806  */
        /* JADX WARNING: Removed duplicated region for block: B:209:0x081e  */
        /* JADX WARNING: Removed duplicated region for block: B:213:0x0834  */
        /* JADX WARNING: Removed duplicated region for block: B:216:0x0846 A[ExcHandler: Exception (r2v61 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:112:0x04e4] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r27 = this;
                java.lang.String r2 = "DownloadThread"
                java.lang.String r3 = "begin running."
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                com.shoujiduoduo.base.bean.i r2 = r0.c
                int r5 = r2.c
                r2 = 0
                r0 = r27
                com.shoujiduoduo.util.m r3 = com.shoujiduoduo.util.m.this
                java.util.HashMap r3 = r3.D
                monitor-enter(r3)
                r0 = r27
                com.shoujiduoduo.util.m r4 = com.shoujiduoduo.util.m.this     // Catch:{ all -> 0x0067 }
                java.util.HashMap r4 = r4.D     // Catch:{ all -> 0x0067 }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0067 }
                boolean r4 = r4.containsKey(r6)     // Catch:{ all -> 0x0067 }
                if (r4 == 0) goto L_0x003b
                r0 = r27
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this     // Catch:{ all -> 0x0067 }
                java.util.HashMap r2 = r2.D     // Catch:{ all -> 0x0067 }
                java.lang.Integer r4 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0067 }
                java.lang.Object r2 = r2.get(r4)     // Catch:{ all -> 0x0067 }
                com.shoujiduoduo.util.m$a r2 = (com.shoujiduoduo.util.m.a) r2     // Catch:{ all -> 0x0067 }
            L_0x003b:
                r0 = r27
                com.shoujiduoduo.util.m r4 = com.shoujiduoduo.util.m.this     // Catch:{ all -> 0x0067 }
                java.util.HashMap r4 = r4.D     // Catch:{ all -> 0x0067 }
                java.lang.Integer r6 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0067 }
                r0 = r27
                r4.put(r6, r0)     // Catch:{ all -> 0x0067 }
                monitor-exit(r3)     // Catch:{ all -> 0x0067 }
                java.lang.String r3 = "DownloadThread"
                java.lang.String r4 = "run: 1"
                com.shoujiduoduo.base.a.a.a(r3, r4)
                if (r2 == 0) goto L_0x006a
            L_0x0056:
                boolean r3 = r2.isAlive()
                if (r3 == 0) goto L_0x006a
                r6 = 200(0xc8, double:9.9E-322)
                sleep(r6)     // Catch:{ InterruptedException -> 0x0062 }
                goto L_0x0056
            L_0x0062:
                r3 = move-exception
                r3.printStackTrace()
                goto L_0x0056
            L_0x0067:
                r2 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x0067 }
                throw r2
            L_0x006a:
                java.lang.String r2 = "DownloadThread"
                java.lang.String r3 = "run: 2"
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                com.shoujiduoduo.base.bean.i r2 = r0.c
                int r6 = r2.d
                r7 = 0
                r2 = 0
                r0 = r27
                com.shoujiduoduo.base.bean.i r3 = r0.c
                int r8 = r3.f
                r0 = r27
                com.shoujiduoduo.base.bean.i r3 = r0.c
                java.lang.String r3 = r3.f1971a
                r0 = r27
                com.shoujiduoduo.base.bean.i r4 = r0.c
                java.lang.String r4 = r4.f1972b
                r11 = 0
                r0 = r27
                boolean r9 = r0.d
                if (r9 == 0) goto L_0x00cf
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 1."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r11 = r2.d
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.m.y
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
            L_0x00ce:
                return
            L_0x00cf:
                boolean r9 = com.shoujiduoduo.util.NetworkStateUtil.a()
                if (r9 != 0) goto L_0x011d
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): network is unavailable."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                r0 = r27
                com.shoujiduoduo.util.m r3 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r3 = r3.d
                int r4 = com.shoujiduoduo.util.m.g
                r0 = r27
                r0.a(r3, r2, r4)
                r27.b()
                r0 = r27
                java.lang.String r2 = r0.g
                java.lang.String r3 = ""
                r0 = r27
                r0.a(r2, r3)
                goto L_0x00ce
            L_0x011d:
                r10 = 0
                r9 = 0
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                int r12 = r12.j
                if (r12 != 0) goto L_0x01da
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.i
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x01da
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r10 = "缓冲移动彩铃资源， url:"
                java.lang.StringBuilder r8 = r8.append(r10)
                java.lang.StringBuilder r8 = r8.append(r7)
                java.lang.String r8 = r8.toString()
                com.shoujiduoduo.base.a.a.a(r2, r8)
                r0 = r27
                com.shoujiduoduo.base.bean.i r2 = r0.c
                java.lang.String r2 = r2.i
                r8 = 0
                java.lang.String r10 = "ring"
                java.lang.String r2 = com.shoujiduoduo.util.s.a(r2, r8, r10)
                boolean r8 = com.shoujiduoduo.util.ag.c(r2)
                if (r8 != 0) goto L_0x0864
            L_0x0160:
                r8 = 128000(0x1f400, float:1.79366E-40)
                java.lang.String r10 = "mp3"
                r7 = 1
                r25 = r9
                r9 = r11
                r11 = r2
                r2 = r25
            L_0x016c:
                java.lang.String r12 = "DownloadThread"
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "downloadSong("
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r5)
                java.lang.String r14 = "): start_pos = "
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r6)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r14 = 0
                r13 = -1
                r0 = r27
                java.net.HttpURLConnection r12 = r0.d(r11)     // Catch:{ a -> 0x038b }
            L_0x0196:
                r0 = r27
                boolean r15 = r0.d
                if (r15 == 0) goto L_0x039f
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 2."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                r0 = r27
                com.shoujiduoduo.util.m r3 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r3 = r3.d
                int r4 = com.shoujiduoduo.util.m.y
                r0 = r27
                r0.a(r3, r2, r4)
                r27.b()
                goto L_0x00ce
            L_0x01da:
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                int r12 = r12.l
                if (r12 != 0) goto L_0x028d
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.k
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x028d
                java.lang.String r12 = "DownloadThread"
                java.lang.String r13 = "缓冲电信彩铃资源"
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.k
                java.lang.String r13 = "81007"
                boolean r12 = r12.startsWith(r13)
                if (r12 == 0) goto L_0x0247
                com.shoujiduoduo.b.e.a r12 = com.shoujiduoduo.a.b.b.g()
                com.shoujiduoduo.base.bean.k r12 = r12.c()
                com.shoujiduoduo.util.d.b r13 = com.shoujiduoduo.util.d.b.a()
                r0 = r27
                com.shoujiduoduo.base.bean.i r14 = r0.c
                java.lang.String r14 = r14.k
                java.lang.String r12 = r12.l()
                com.shoujiduoduo.util.b.c$h r12 = r13.a(r14, r12)
                if (r12 == 0) goto L_0x0225
                java.lang.String r7 = r12.e
                java.lang.String r2 = "wav"
            L_0x0225:
                java.lang.String r12 = "DownloadThread"
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "电信diy彩铃资源, format:wav, url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r25 = r9
                r9 = r11
                r11 = r7
                r7 = r10
                r10 = r2
                r2 = r25
                goto L_0x016c
            L_0x0247:
                r0 = r27
                com.shoujiduoduo.base.bean.i r9 = r0.c
                java.lang.String r9 = r9.k
                r12 = 0
                com.shoujiduoduo.base.bean.h r9 = com.shoujiduoduo.util.s.b(r9, r12)
                if (r9 == 0) goto L_0x0260
                java.lang.String r7 = r9.a()
                int r8 = r9.c()
                java.lang.String r2 = r9.b()
            L_0x0260:
                r9 = 1
                java.lang.String r12 = "DownloadThread"
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "电信自有彩铃资源, format:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r14 = ",url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r25 = r9
                r9 = r11
                r11 = r7
                r7 = r10
                r10 = r2
                r2 = r25
                goto L_0x016c
            L_0x028d:
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.m
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 != 0) goto L_0x032e
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                boolean r12 = r12.i()
                if (r12 != 0) goto L_0x032e
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.n
                java.lang.String r13 = ""
                boolean r12 = r12.equals(r13)
                if (r12 == 0) goto L_0x032e
                java.lang.String r12 = "DownloadThread"
                java.lang.String r13 = "缓冲联通彩铃资源"
                com.shoujiduoduo.base.a.a.a(r12, r13)
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c
                java.lang.String r12 = r12.m
                r13 = 0
                com.shoujiduoduo.base.bean.h r12 = com.shoujiduoduo.util.s.a(r12, r13)
                if (r12 == 0) goto L_0x02ff
                java.lang.String r7 = r12.a()
                int r8 = r12.c()
                java.lang.String r2 = r12.b()
                java.lang.String r12 = "DownloadThread"
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "联通彩铃， format:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r2)
                java.lang.String r14 = ", url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
            L_0x02f5:
                r25 = r9
                r9 = r11
                r11 = r7
                r7 = r10
                r10 = r2
                r2 = r25
                goto L_0x016c
            L_0x02ff:
                com.shoujiduoduo.util.e.a r12 = com.shoujiduoduo.util.e.a.a()
                r0 = r27
                com.shoujiduoduo.base.bean.i r13 = r0.c
                java.lang.String r13 = r13.m
                com.shoujiduoduo.util.b.c$q r12 = r12.d(r13)
                if (r12 == 0) goto L_0x0315
                com.shoujiduoduo.util.b.c$y r2 = r12.f3079a
                java.lang.String r7 = r2.h
                java.lang.String r2 = "wav"
            L_0x0315:
                java.lang.String r12 = "DownloadThread"
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "联通彩铃， format:wav, url:"
                java.lang.StringBuilder r13 = r13.append(r14)
                java.lang.StringBuilder r13 = r13.append(r7)
                java.lang.String r13 = r13.toString()
                com.shoujiduoduo.base.a.a.a(r12, r13)
                goto L_0x02f5
            L_0x032e:
                java.lang.String r2 = "DownloadThread"
                java.lang.String r7 = "缓冲多多资源"
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r2 = 0
                r0 = r27
                com.shoujiduoduo.util.m$a$b r2 = r0.b(r2)     // Catch:{ a -> 0x034e }
                java.lang.String r11 = r2.f3206a     // Catch:{ a -> 0x034e }
                int r8 = r2.c     // Catch:{ a -> 0x034e }
                java.lang.String r7 = r2.f3207b     // Catch:{ a -> 0x034e }
                r25 = r9
                r9 = r2
                r2 = r25
                r26 = r10
                r10 = r7
                r7 = r26
                goto L_0x016c
            L_0x034e:
                r2 = move-exception
                r11 = r2
                r0 = r27
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r12 = r2.d
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r0 = r27
                com.shoujiduoduo.base.bean.i r3 = r0.c
                java.lang.String r3 = r3.f1971a
                r0 = r27
                com.shoujiduoduo.base.bean.i r4 = r0.c
                java.lang.String r4 = r4.f1972b
                r0 = r27
                com.shoujiduoduo.base.bean.i r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = r11.f3204a
                r0 = r27
                r0.a(r12, r2, r3)
                r27.b()
                r0 = r27
                java.lang.String r2 = r0.h
                java.lang.String r3 = ""
                r0 = r27
                r0.a(r2, r3)
                goto L_0x00ce
            L_0x038b:
                r12 = move-exception
                r12 = 0
                java.lang.String r15 = "DownloadThread"
                java.lang.String r16 = "buildConnection failed 1"
                com.shoujiduoduo.base.a.a.a(r15, r16)
                r0 = r27
                java.lang.String r15 = r0.i
                r0 = r27
                r0.a(r15, r11)
                goto L_0x0196
            L_0x039f:
                if (r12 != 0) goto L_0x085e
                if (r9 == 0) goto L_0x043d
                boolean r12 = r9.d
                if (r12 == 0) goto L_0x043d
                r2 = 1
                r0 = r27
                com.shoujiduoduo.util.m$a$b r2 = r0.b(r2)     // Catch:{ a -> 0x0400 }
                java.lang.String r11 = r2.f3206a     // Catch:{ a -> 0x0400 }
                int r8 = r2.c     // Catch:{ a -> 0x0400 }
                java.lang.String r10 = r2.f3207b     // Catch:{ a -> 0x0400 }
                r9 = r10
                r10 = r11
            L_0x03b6:
                r0 = r27
                java.net.HttpURLConnection r12 = r0.d(r10)     // Catch:{ a -> 0x0478 }
                r0 = r27
                boolean r2 = r0.d
                if (r2 == 0) goto L_0x04b9
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r9 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r9 = "): Cancel 2."
                java.lang.StringBuilder r7 = r7.append(r9)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                r0 = r27
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r11 = r2.d
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                int r3 = com.shoujiduoduo.util.m.y
                r0 = r27
                r0.a(r11, r2, r3)
                r27.b()
                goto L_0x00ce
            L_0x0400:
                r2 = move-exception
                r11 = r2
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r0 = r27
                com.shoujiduoduo.base.bean.i r3 = r0.c
                java.lang.String r3 = r3.f1971a
                r0 = r27
                com.shoujiduoduo.base.bean.i r4 = r0.c
                java.lang.String r4 = r4.f1972b
                r0 = r27
                com.shoujiduoduo.base.bean.i r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                r0 = r27
                com.shoujiduoduo.util.m r3 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r3 = r3.d
                int r4 = r11.f3204a
                r0 = r27
                r0.a(r3, r2, r4)
                r27.b()
                r0 = r27
                java.lang.String r2 = r0.h
                java.lang.String r3 = ""
                r0 = r27
                r0.a(r2, r3)
                goto L_0x00ce
            L_0x043d:
                if (r9 != 0) goto L_0x085a
                if (r7 == 0) goto L_0x0459
                r0 = r27
                com.shoujiduoduo.base.bean.i r2 = r0.c
                java.lang.String r2 = r2.i
                r7 = 1
                java.lang.String r9 = "ring"
                java.lang.String r2 = com.shoujiduoduo.util.s.a(r2, r7, r9)
                boolean r7 = com.shoujiduoduo.util.ag.c(r2)
                if (r7 != 0) goto L_0x0455
                r11 = r2
            L_0x0455:
                r9 = r10
                r10 = r11
                goto L_0x03b6
            L_0x0459:
                if (r2 == 0) goto L_0x085a
                r0 = r27
                com.shoujiduoduo.base.bean.i r2 = r0.c
                java.lang.String r2 = r2.k
                r7 = 1
                com.shoujiduoduo.base.bean.h r2 = com.shoujiduoduo.util.s.b(r2, r7)
                if (r2 == 0) goto L_0x085a
                java.lang.String r11 = r2.a()
                int r8 = r2.c()
                java.lang.String r10 = r2.b()
                r9 = r10
                r10 = r11
                goto L_0x03b6
            L_0x0478:
                r11 = move-exception
                java.lang.String r2 = "DownloadThread"
                java.lang.String r3 = "buildConnection failed 2"
                com.shoujiduoduo.base.a.a.a(r2, r3)
                r0 = r27
                java.lang.String r2 = r0.i
                r0 = r27
                r0.a(r2, r10)
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i
                r0 = r27
                com.shoujiduoduo.base.bean.i r3 = r0.c
                java.lang.String r3 = r3.f1971a
                r0 = r27
                com.shoujiduoduo.base.bean.i r4 = r0.c
                java.lang.String r4 = r4.f1972b
                r0 = r27
                com.shoujiduoduo.base.bean.i r5 = r0.c
                int r5 = r5.c
                r7 = -1
                java.lang.String r9 = ""
                java.lang.String r10 = ""
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                r0 = r27
                com.shoujiduoduo.util.m r3 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r3 = r3.d
                int r4 = r11.f3204a
                r0 = r27
                r0.a(r3, r2, r4)
                r27.b()
                goto L_0x00ce
            L_0x04b9:
                r20 = r12
            L_0x04bb:
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r11 = "downloadSong("
                java.lang.StringBuilder r7 = r7.append(r11)
                java.lang.StringBuilder r7 = r7.append(r5)
                java.lang.String r11 = "): connect finished!"
                java.lang.StringBuilder r7 = r7.append(r11)
                java.lang.String r7 = r7.toString()
                com.shoujiduoduo.base.a.a.a(r2, r7)
                int r21 = r20.getContentLength()     // Catch:{ FileNotFoundException -> 0x0855, IOException -> 0x084e, Exception -> 0x07bc }
                if (r21 > 0) goto L_0x04e2
                r21 = 1000000000(0x3b9aca00, float:0.0047237873)
            L_0x04e2:
                r0 = r27
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.util.ArrayList r11 = r2.d     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.base.bean.i r2 = new com.shoujiduoduo.base.bean.i     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r7 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r7) goto L_0x0627
                r7 = -1
            L_0x04f4:
                r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                int r7 = com.shoujiduoduo.util.m.z     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r0 = r27
                r0.a(r11, r2, r7)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r7 = "DownloadThread"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r2.<init>()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r11 = "downloadSong("
                java.lang.StringBuilder r2 = r2.append(r11)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r11 = "): filesize = "
                java.lang.StringBuilder r11 = r2.append(r11)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r2 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r2) goto L_0x062b
                r2 = -1
            L_0x051f:
                java.lang.StringBuilder r2 = r11.append(r2)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r2 = r2.toString()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.base.a.a.a(r7, r2)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.io.InputStream r22 = r20.getInputStream()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r7 = 0
                java.lang.String r2 = "DownloadThread"
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r11.<init>()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r12 = "save file path:"
                java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r0 = r27
                com.shoujiduoduo.base.bean.i r12 = r0.c     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r12 = r12.l()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r11 = r11.toString()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.base.a.a.a(r2, r11)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x062f, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                r0 = r27
                com.shoujiduoduo.base.bean.i r11 = r0.c     // Catch:{ IOException -> 0x062f, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                java.lang.String r11 = r11.l()     // Catch:{ IOException -> 0x062f, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                java.lang.String r12 = "rw"
                r2.<init>(r11, r12)     // Catch:{ IOException -> 0x062f, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                r7 = r2
            L_0x055f:
                r2 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 == r2) goto L_0x056c
                int r2 = r21 + r6
                long r12 = (long) r2
                r7.setLength(r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
            L_0x056c:
                long r12 = (long) r6     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r7.seek(r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r2 = 1024(0x400, float:1.435E-42)
                byte[] r0 = new byte[r2]     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r23 = r0
                r2 = r14
            L_0x0577:
                r11 = 0
                r12 = 1024(0x400, float:1.435E-42)
                r0 = r22
                r1 = r23
                int r11 = r0.read(r1, r11, r12)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                if (r11 <= 0) goto L_0x0724
                r12 = 0
                r0 = r23
                r7.write(r0, r12, r11)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r2 = r2 + r11
                r0 = r27
                boolean r11 = r0.d     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                if (r11 == 0) goto L_0x06f0
                java.lang.String r11 = "DownloadThread"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r12.<init>()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r13 = "downloadSong("
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r13 = "): Cancel 2."
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r12 = r12.toString()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.a.a.a(r11, r12)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                com.shoujiduoduo.util.m r11 = com.shoujiduoduo.util.m.this     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.util.ArrayList r22 = r11.d     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r15 = r2 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x06ec
                r16 = -1
            L_0x05c4:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r9 = com.shoujiduoduo.util.m.y     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                r1 = r22
                r0.a(r1, r11, r9)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r7.close()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r20.disconnect()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r27.b()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                goto L_0x00ce
            L_0x05e6:
                r7 = move-exception
                r7 = r2
                r2 = r21
            L_0x05ea:
                java.lang.String r9 = "DownloadThread"
                java.lang.String r11 = "downloadSong(\" + songid + \"): create file exception"
                com.shoujiduoduo.base.a.a.a(r9, r11)
                r0 = r27
                com.shoujiduoduo.util.m r9 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r9 = r9.d
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i
                int r15 = r7 + r6
                r7 = 1000000000(0x3b9aca00, float:0.0047237873)
                if (r2 != r7) goto L_0x07a3
                r16 = -1
            L_0x0604:
                java.lang.String r18 = ""
                java.lang.String r19 = ""
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
                int r2 = com.shoujiduoduo.util.m.t
                r0 = r27
                r0.a(r9, r11, r2)
                r0 = r27
                java.lang.String r2 = r0.j
                r0 = r27
                r0.a(r2, r10)
                r27.b()
                goto L_0x00ce
            L_0x0627:
                int r7 = r21 + r6
                goto L_0x04f4
            L_0x062b:
                int r2 = r21 + r6
                goto L_0x051f
            L_0x062f:
                r2 = move-exception
                r2.printStackTrace()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r11 = "DownloadThread"
                java.lang.String r12 = "IOException, check duoduo DIR and retry"
                com.shoujiduoduo.base.a.a.c(r11, r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                boolean r11 = com.shoujiduoduo.util.k.a()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                if (r11 == 0) goto L_0x0675
                java.io.RandomAccessFile r2 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0652, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                r0 = r27
                com.shoujiduoduo.base.bean.i r11 = r0.c     // Catch:{ IOException -> 0x0652, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                java.lang.String r11 = r11.l()     // Catch:{ IOException -> 0x0652, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                java.lang.String r12 = "rw"
                r2.<init>(r11, r12)     // Catch:{ IOException -> 0x0652, FileNotFoundException -> 0x066f, Exception -> 0x0846 }
                r7 = r2
                goto L_0x055f
            L_0x0652:
                r2 = move-exception
                r2.printStackTrace()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                java.lang.String r2 = "DownloadThread"
                java.lang.String r11 = "inform sd card error!"
                com.shoujiduoduo.base.a.a.a(r2, r11)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.a.a.c r2 = com.shoujiduoduo.a.a.c.a()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.a.a.b r11 = com.shoujiduoduo.a.a.b.OBSERVER_APP     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                com.shoujiduoduo.util.m$a$1 r12 = new com.shoujiduoduo.util.m$a$1     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r0 = r27
                r12.<init>()     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                r2.b(r11, r12)     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
                goto L_0x055f
            L_0x066f:
                r2 = move-exception
                r2 = r21
                r7 = r14
                goto L_0x05ea
            L_0x0675:
                throw r2     // Catch:{ FileNotFoundException -> 0x066f, IOException -> 0x0676, Exception -> 0x0846 }
            L_0x0676:
                r2 = move-exception
                r7 = r2
                r2 = r14
            L_0x0679:
                java.lang.String r9 = "DownloadThread"
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "downloadSong("
                java.lang.StringBuilder r11 = r11.append(r12)
                java.lang.StringBuilder r11 = r11.append(r5)
                java.lang.String r12 = "): notify listener that disk failed!"
                java.lang.StringBuilder r11 = r11.append(r12)
                java.lang.String r11 = r11.toString()
                com.shoujiduoduo.base.a.a.a(r9, r11)
                java.lang.String r9 = r7.getLocalizedMessage()
                r0 = r27
                com.shoujiduoduo.util.m r11 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r20 = r11.d
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i
                int r15 = r2 + r6
                r2 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r2) goto L_0x07a7
                r16 = -1
            L_0x06b0:
                java.lang.String r18 = ""
                java.lang.String r19 = ""
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
                if (r9 == 0) goto L_0x07ab
                java.lang.String r2 = "No space left on device"
                boolean r2 = r9.contains(r2)
                if (r2 == 0) goto L_0x07ab
                int r2 = com.shoujiduoduo.util.m.v
            L_0x06ca:
                r0 = r27
                r1 = r20
                r0.a(r1, r11, r2)
                if (r9 == 0) goto L_0x07b1
                java.lang.String r2 = "No space left on device"
                boolean r2 = r9.contains(r2)
                if (r2 == 0) goto L_0x07b1
                r0 = r27
                java.lang.String r2 = r0.l
                r0 = r27
                r0.a(r2, r10)
            L_0x06e4:
                r7.printStackTrace()
                r27.b()
                goto L_0x00ce
            L_0x06ec:
                int r16 = r6 + r21
                goto L_0x05c4
            L_0x06f0:
                r0 = r27
                com.shoujiduoduo.util.m r11 = com.shoujiduoduo.util.m.this     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.util.ArrayList r24 = r11.d     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r15 = r2 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x0721
                r16 = -1
            L_0x0705:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r12 = com.shoujiduoduo.util.m.A     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                r1 = r24
                r0.a(r1, r11, r12)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                goto L_0x0577
            L_0x071e:
                r7 = move-exception
                goto L_0x0679
            L_0x0721:
                int r16 = r6 + r21
                goto L_0x0705
            L_0x0724:
                java.lang.String r11 = "DownloadThread"
                java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r12.<init>()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r13 = "downloadSong("
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.StringBuilder r12 = r12.append(r5)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r13 = "): out of read loop."
                java.lang.StringBuilder r12 = r12.append(r13)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r12 = r12.toString()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.a.a.a(r11, r12)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                com.shoujiduoduo.util.m r11 = com.shoujiduoduo.util.m.this     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.util.ArrayList r22 = r11.d     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r15 = r2 + r6
                r12 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r12) goto L_0x07a0
                int r16 = r6 + r2
            L_0x0757:
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r18 = r9
                r19 = r10
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                int r9 = com.shoujiduoduo.util.m.B     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                r1 = r22
                r0.a(r1, r11, r9)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r7.close()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r20.disconnect()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                java.lang.String r7 = r0.f     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r0 = r27
                r0.a(r7, r10)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r7 = "DownloadThread"
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r9.<init>()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r11 = "downloadSong("
                java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.StringBuilder r9 = r9.append(r5)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r11 = "): download thread end!"
                java.lang.StringBuilder r9 = r9.append(r11)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                java.lang.String r9 = r9.toString()     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                com.shoujiduoduo.base.a.a.a(r7, r9)     // Catch:{ FileNotFoundException -> 0x05e6, IOException -> 0x071e, Exception -> 0x084b }
                r27.b()
                goto L_0x00ce
            L_0x07a0:
                int r16 = r6 + r21
                goto L_0x0757
            L_0x07a3:
                int r16 = r6 + r2
                goto L_0x0604
            L_0x07a7:
                int r16 = r6 + r21
                goto L_0x06b0
            L_0x07ab:
                int r2 = com.shoujiduoduo.util.m.t
                goto L_0x06ca
            L_0x07b1:
                r0 = r27
                java.lang.String r2 = r0.k
                r0 = r27
                r0.a(r2, r10)
                goto L_0x06e4
            L_0x07bc:
                r2 = move-exception
                r7 = r2
                r21 = r13
                r2 = r14
            L_0x07c1:
                java.lang.String r9 = "DownloadThread"
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "downloadSong("
                java.lang.StringBuilder r11 = r11.append(r12)
                java.lang.StringBuilder r11 = r11.append(r5)
                java.lang.String r12 = "): notify failed, exception"
                java.lang.StringBuilder r11 = r11.append(r12)
                java.lang.String r11 = r11.toString()
                com.shoujiduoduo.base.a.a.a(r9, r11)
                java.lang.String r9 = r7.getLocalizedMessage()
                r0 = r27
                com.shoujiduoduo.util.m r11 = com.shoujiduoduo.util.m.this
                java.util.ArrayList r20 = r11.d
                com.shoujiduoduo.base.bean.i r11 = new com.shoujiduoduo.base.bean.i
                int r15 = r2 + r6
                r2 = 1000000000(0x3b9aca00, float:0.0047237873)
                r0 = r21
                if (r0 != r2) goto L_0x0834
                r16 = -1
            L_0x07f8:
                java.lang.String r18 = ""
                java.lang.String r19 = ""
                r12 = r3
                r13 = r4
                r14 = r5
                r17 = r8
                r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)
                if (r9 == 0) goto L_0x0837
                java.lang.String r2 = "No space left on device"
                boolean r2 = r9.contains(r2)
                if (r2 == 0) goto L_0x0837
                int r2 = com.shoujiduoduo.util.m.v
            L_0x0812:
                r0 = r27
                r1 = r20
                r0.a(r1, r11, r2)
                r7.printStackTrace()
                if (r9 == 0) goto L_0x083c
                java.lang.String r2 = "No space left on device"
                boolean r2 = r9.contains(r2)
                if (r2 == 0) goto L_0x083c
                r0 = r27
                java.lang.String r2 = r0.l
                r0 = r27
                r0.a(r2, r10)
            L_0x082f:
                r27.b()
                goto L_0x00ce
            L_0x0834:
                int r16 = r6 + r21
                goto L_0x07f8
            L_0x0837:
                int r2 = com.shoujiduoduo.util.m.t
                goto L_0x0812
            L_0x083c:
                r0 = r27
                java.lang.String r2 = r0.k
                r0 = r27
                r0.a(r2, r10)
                goto L_0x082f
            L_0x0846:
                r2 = move-exception
                r7 = r2
                r2 = r14
                goto L_0x07c1
            L_0x084b:
                r7 = move-exception
                goto L_0x07c1
            L_0x084e:
                r2 = move-exception
                r7 = r2
                r21 = r13
                r2 = r14
                goto L_0x0679
            L_0x0855:
                r2 = move-exception
                r2 = r13
                r7 = r14
                goto L_0x05ea
            L_0x085a:
                r9 = r10
                r10 = r11
                goto L_0x03b6
            L_0x085e:
                r20 = r12
                r9 = r10
                r10 = r11
                goto L_0x04bb
            L_0x0864:
                r2 = r7
                goto L_0x0160
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.a.run():void");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008e, code lost:
        if (r3 == null) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0092, code lost:
        if (r3.g == null) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x009a, code lost:
        if (r3.g.equalsIgnoreCase(r13) != false) goto L_0x019d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x009c, code lost:
        a(r1);
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a0, code lost:
        if (r0 == null) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a8, code lost:
        if (android.text.TextUtils.isEmpty(r0.g) != false) goto L_0x00ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ac, code lost:
        if (r0.f != 0) goto L_0x0122;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00ae, code lost:
        if (r0 != null) goto L_0x00c3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00b0, code lost:
        r0 = a(r1, r12.e, r12.f, "", 0, -1, 0, r13, "");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00bf, code lost:
        if (r0 != null) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c3, code lost:
        r0.g = r13;
        r0.e = -1;
        r0.d = 0;
        r0.f = 0;
        r0.h = "";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00cf, code lost:
        r0.i = r12.p;
        r0.j = r12.s;
        r0.k = r12.u;
        r0.l = r12.x;
        r0.m = r12.C;
        r0.n = r12.F;
        r0.a(r12.a());
        r0.b(r12.f());
        r0.c(r12.e());
        r0.a(r12.d());
        r0.b(r12.c());
        r0.d(r12.g());
        r0.c(r12.h());
        new com.shoujiduoduo.util.m.a(r11, r0).start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0122, code lost:
        r0.i = r12.p;
        r0.j = r12.s;
        r0.k = r12.u;
        r0.l = r12.x;
        r0.m = r12.C;
        r0.n = r12.F;
        r0.a(r12.a());
        r0.b(r12.f());
        r0.c(r12.e());
        r0.a(r12.d());
        r0.b(r12.c());
        r0.d(r12.g());
        r0.c(r12.h());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0178, code lost:
        if (new java.io.File(r0.l()).exists() != false) goto L_0x0189;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x017a, code lost:
        r0.d = 0;
        r0.e = -1;
        r0.g = r13;
        r0.f = 0;
        r0.h = "";
        a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x018d, code lost:
        if (r0.d < r0.e) goto L_0x0193;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0191, code lost:
        if (r0.e >= 0) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0193, code lost:
        new com.shoujiduoduo.util.m.a(r11, r0).start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x019d, code lost:
        r0 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        return r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.shoujiduoduo.base.bean.i a(com.shoujiduoduo.base.bean.RingData r12, java.lang.String r13) {
        /*
            r11 = this;
            r10 = 0
            r6 = -1
            r5 = 0
            int r1 = r12.k()
            com.shoujiduoduo.base.bean.i r3 = r11.b(r1)
            com.shoujiduoduo.util.m.f3200a = r1
            java.util.HashMap<java.lang.Integer, com.shoujiduoduo.util.m$a> r4 = r11.D
            monitor-enter(r4)
            java.util.HashMap<java.lang.Integer, com.shoujiduoduo.util.m$a> r0 = r11.D     // Catch:{ all -> 0x008a }
            java.util.Set r0 = r0.entrySet()     // Catch:{ all -> 0x008a }
            java.util.Iterator r7 = r0.iterator()     // Catch:{ all -> 0x008a }
        L_0x001a:
            boolean r0 = r7.hasNext()     // Catch:{ all -> 0x008a }
            if (r0 == 0) goto L_0x008d
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x008a }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ all -> 0x008a }
            java.lang.Object r2 = r0.getKey()     // Catch:{ all -> 0x008a }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ all -> 0x008a }
            int r2 = r2.intValue()     // Catch:{ all -> 0x008a }
            if (r2 != r1) goto L_0x0079
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x008a }
            com.shoujiduoduo.util.m$a r0 = (com.shoujiduoduo.util.m.a) r0     // Catch:{ all -> 0x008a }
            boolean r0 = r0.d     // Catch:{ all -> 0x008a }
            if (r0 != 0) goto L_0x001a
            java.lang.String r0 = "DownloadManager"
            java.lang.String r1 = "song in downloading and NOT CANCEL!"
            com.shoujiduoduo.base.a.a.a(r0, r1)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.a()     // Catch:{ all -> 0x008a }
            r3.a(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.f()     // Catch:{ all -> 0x008a }
            r3.b(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.e()     // Catch:{ all -> 0x008a }
            r3.c(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.d()     // Catch:{ all -> 0x008a }
            r3.a(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.c()     // Catch:{ all -> 0x008a }
            r3.b(r0)     // Catch:{ all -> 0x008a }
            java.lang.String r0 = r12.g()     // Catch:{ all -> 0x008a }
            r3.d(r0)     // Catch:{ all -> 0x008a }
            int r0 = r12.h()     // Catch:{ all -> 0x008a }
            r3.c(r0)     // Catch:{ all -> 0x008a }
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            r0 = r3
        L_0x0078:
            return r0
        L_0x0079:
            java.lang.Object r0 = r0.getValue()     // Catch:{ all -> 0x008a }
            com.shoujiduoduo.util.m$a r0 = (com.shoujiduoduo.util.m.a) r0     // Catch:{ all -> 0x008a }
            boolean r2 = r0.a()     // Catch:{ all -> 0x008a }
            if (r2 == 0) goto L_0x001a
            r2 = 1
            r0.a(r2)     // Catch:{ all -> 0x008a }
            goto L_0x001a
        L_0x008a:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            throw r0
        L_0x008d:
            monitor-exit(r4)     // Catch:{ all -> 0x008a }
            if (r3 == 0) goto L_0x019d
            java.lang.String r0 = r3.g
            if (r0 == 0) goto L_0x019d
            java.lang.String r0 = r3.g
            boolean r0 = r0.equalsIgnoreCase(r13)
            if (r0 != 0) goto L_0x019d
            r11.a(r1)
            r0 = r10
        L_0x00a0:
            if (r0 == 0) goto L_0x00ae
            java.lang.String r2 = r0.g
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 != 0) goto L_0x00ae
            int r2 = r0.f
            if (r2 != 0) goto L_0x0122
        L_0x00ae:
            if (r0 != 0) goto L_0x00c3
            java.lang.String r2 = r12.e
            java.lang.String r3 = r12.f
            java.lang.String r4 = ""
            java.lang.String r9 = ""
            r0 = r11
            r7 = r5
            r8 = r13
            com.shoujiduoduo.base.bean.i r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            if (r0 != 0) goto L_0x00cf
            r0 = r10
            goto L_0x0078
        L_0x00c3:
            r0.g = r13
            r0.e = r6
            r0.d = r5
            r0.f = r5
            java.lang.String r1 = ""
            r0.h = r1
        L_0x00cf:
            java.lang.String r1 = r12.p
            r0.i = r1
            int r1 = r12.s
            r0.j = r1
            java.lang.String r1 = r12.u
            r0.k = r1
            int r1 = r12.x
            r0.l = r1
            java.lang.String r1 = r12.C
            r0.m = r1
            java.lang.String r1 = r12.F
            r0.n = r1
            java.lang.String r1 = r12.a()
            r0.a(r1)
            int r1 = r12.f()
            r0.b(r1)
            java.lang.String r1 = r12.e()
            r0.c(r1)
            int r1 = r12.d()
            r0.a(r1)
            java.lang.String r1 = r12.c()
            r0.b(r1)
            java.lang.String r1 = r12.g()
            r0.d(r1)
            int r1 = r12.h()
            r0.c(r1)
            com.shoujiduoduo.util.m$a r1 = new com.shoujiduoduo.util.m$a
            r1.<init>(r0)
            r1.start()
            goto L_0x0078
        L_0x0122:
            java.lang.String r1 = r12.p
            r0.i = r1
            int r1 = r12.s
            r0.j = r1
            java.lang.String r1 = r12.u
            r0.k = r1
            int r1 = r12.x
            r0.l = r1
            java.lang.String r1 = r12.C
            r0.m = r1
            java.lang.String r1 = r12.F
            r0.n = r1
            java.lang.String r1 = r12.a()
            r0.a(r1)
            int r1 = r12.f()
            r0.b(r1)
            java.lang.String r1 = r12.e()
            r0.c(r1)
            int r1 = r12.d()
            r0.a(r1)
            java.lang.String r1 = r12.c()
            r0.b(r1)
            java.lang.String r1 = r12.g()
            r0.d(r1)
            int r1 = r12.h()
            r0.c(r1)
            java.io.File r1 = new java.io.File
            java.lang.String r2 = r0.l()
            r1.<init>(r2)
            boolean r1 = r1.exists()
            if (r1 != 0) goto L_0x0189
            r0.d = r5
            r0.e = r6
            r0.g = r13
            r0.f = r5
            java.lang.String r1 = ""
            r0.h = r1
            r11.a(r0)
        L_0x0189:
            int r1 = r0.d
            int r2 = r0.e
            if (r1 < r2) goto L_0x0193
            int r1 = r0.e
            if (r1 >= 0) goto L_0x0078
        L_0x0193:
            com.shoujiduoduo.util.m$a r1 = new com.shoujiduoduo.util.m$a
            r1.<init>(r0)
            r1.start()
            goto L_0x0078
        L_0x019d:
            r0 = r3
            goto L_0x00a0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.a(com.shoujiduoduo.base.bean.RingData, java.lang.String):com.shoujiduoduo.base.bean.i");
    }

    public void b() {
        this.f3201b = null;
        if (this.d != null) {
            this.d.clear();
        }
    }

    /* compiled from: DownloadManager */
    class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        public boolean f3208a = true;

        /* renamed from: b  reason: collision with root package name */
        public boolean f3209b = false;
        private String d;
        private SQLiteDatabase e;
        private HashSet<String> f;

        public b(String str) {
            this.d = str;
        }

        private void a() {
            String str;
            com.shoujiduoduo.base.a.a.a("DownloadManager", "getAllContactRingID");
            this.f = new HashSet<>();
            Uri actualDefaultRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(m.this.e, 1);
            String[] strArr = {"contact_id", "display_name", "custom_ringtone", "sort_key"};
            ContentResolver contentResolver = m.this.e.getContentResolver();
            if (contentResolver != null) {
                try {
                    Cursor query = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, strArr, null, null, "sort_key COLLATE LOCALIZED asc");
                    if (query != null && query.moveToFirst()) {
                        while (query.moveToNext()) {
                            String string = query.getString(2);
                            if (!(string == null || RingtoneManager.getRingtone(m.this.e, Uri.parse(string)) == null || string.equals("content://settings/system/ringtone") || actualDefaultRingtoneUri == null || string.equals(actualDefaultRingtoneUri.toString()))) {
                                Cursor query2 = contentResolver.query(Uri.parse(string), new String[]{Downloads._DATA}, null, null, null);
                                if (query2 != null) {
                                    int columnIndexOrThrow = query2.getColumnIndexOrThrow(Downloads._DATA);
                                    query2.moveToFirst();
                                    str = query2.getString(columnIndexOrThrow);
                                    query2.close();
                                } else {
                                    str = null;
                                }
                                if (str != null) {
                                    this.f.add(str);
                                }
                            }
                        }
                        query.close();
                    }
                } catch (Exception e2) {
                }
            }
        }

        /* JADX WARNING: CFG modification limit reached, blocks count: 201 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r12 = this;
                r7 = 0
                r11 = 1
                com.shoujiduoduo.util.m r0 = com.shoujiduoduo.util.m.this
                android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()
                r12.e = r0
                android.database.sqlite.SQLiteDatabase r0 = r12.e
                if (r0 == 0) goto L_0x0012
                java.lang.String r0 = r12.d
                if (r0 != 0) goto L_0x0013
            L_0x0012:
                return
            L_0x0013:
                r0 = 0
                r12.f3208a = r0
                r12.a()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x003b }
                r0.<init>()     // Catch:{ SQLException -> 0x003b }
                java.lang.String r1 = "select * from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x003b }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x003b }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x003b }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x003b }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x003b }
                r2 = 0
                android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLException -> 0x003b }
                if (r0 != 0) goto L_0x003f
                r0 = 1
                r12.f3208a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x003b:
                r0 = move-exception
                r12.f3208a = r11
                goto L_0x0012
            L_0x003f:
                int r1 = r0.getCount()     // Catch:{ SQLException -> 0x003b }
                if (r1 != 0) goto L_0x004c
                r0.close()     // Catch:{ SQLException -> 0x003b }
                r0 = 1
                r12.f3208a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x004c:
                java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ SQLException -> 0x003b }
                r1.<init>()     // Catch:{ SQLException -> 0x003b }
            L_0x0051:
                boolean r2 = r0.moveToNext()     // Catch:{ SQLException -> 0x003b }
                if (r2 == 0) goto L_0x006c
                boolean r2 = r12.f3209b     // Catch:{ SQLException -> 0x003b }
                if (r2 == 0) goto L_0x005f
                r0 = 1
                r12.f3208a = r0     // Catch:{ SQLException -> 0x003b }
                goto L_0x0012
            L_0x005f:
                r2 = 1
                int r2 = r0.getInt(r2)     // Catch:{ SQLException -> 0x003b }
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLException -> 0x003b }
                r1.add(r2)     // Catch:{ SQLException -> 0x003b }
                goto L_0x0051
            L_0x006c:
                r0.close()     // Catch:{ SQLException -> 0x003b }
                java.util.Iterator r8 = r1.iterator()
            L_0x0073:
                boolean r0 = r8.hasNext()
                if (r0 == 0) goto L_0x0230
                boolean r0 = r12.f3209b
                if (r0 == 0) goto L_0x0080
                r12.f3208a = r11
                goto L_0x0012
            L_0x0080:
                java.lang.Object r0 = r8.next()
                java.lang.Integer r0 = (java.lang.Integer) r0
                int r4 = r0.intValue()
                com.shoujiduoduo.util.m r0 = com.shoujiduoduo.util.m.this
                android.content.Context r0 = r0.e
                java.lang.String r1 = "user_ring_phone_select"
                java.lang.String r2 = ""
                java.lang.String r0 = com.shoujiduoduo.util.ad.a(r0, r1, r2)
                com.shoujiduoduo.util.m r1 = com.shoujiduoduo.util.m.this
                android.content.Context r1 = r1.e
                java.lang.String r2 = "user_ring_alarm_select"
                java.lang.String r3 = ""
                java.lang.String r1 = com.shoujiduoduo.util.ad.a(r1, r2, r3)
                com.shoujiduoduo.util.m r2 = com.shoujiduoduo.util.m.this
                android.content.Context r2 = r2.e
                java.lang.String r3 = "user_ring_notification_select"
                java.lang.String r5 = ""
                java.lang.String r2 = com.shoujiduoduo.util.ad.a(r2, r3, r5)
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = ""
                java.lang.StringBuilder r3 = r3.append(r5)
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                boolean r1 = r1.equals(r3)
                if (r1 != 0) goto L_0x00ff
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = ""
                java.lang.StringBuilder r1 = r1.append(r3)
                java.lang.StringBuilder r1 = r1.append(r4)
                java.lang.String r1 = r1.toString()
                boolean r0 = r0.equals(r1)
                if (r0 != 0) goto L_0x00ff
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = ""
                java.lang.StringBuilder r0 = r0.append(r1)
                java.lang.StringBuilder r0 = r0.append(r4)
                java.lang.String r0 = r0.toString()
                boolean r0 = r2.equals(r0)
                if (r0 == 0) goto L_0x0131
            L_0x00ff:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x012b }
                r0.<init>()     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x012b }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x012b }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x012b }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x012b }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x012b }
                goto L_0x0073
            L_0x012b:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0073
            L_0x0131:
                java.lang.String r9 = "DownloadManager"
                monitor-enter(r9)
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0192 }
                r0.<init>()     // Catch:{ SQLiteException -> 0x0192 }
                java.lang.String r1 = "select * from ringtoneduoduo_resourcetable_3 where rid="
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0192 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLiteException -> 0x0192 }
                java.lang.String r1 = " order by rid;"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x0192 }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x0192 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLiteException -> 0x0192 }
                r2 = 0
                android.database.Cursor r0 = r1.rawQuery(r0, r2)     // Catch:{ SQLiteException -> 0x0192 }
                r6 = r0
            L_0x0155:
                if (r6 == 0) goto L_0x015d
                int r0 = r6.getCount()     // Catch:{ all -> 0x018f }
                if (r0 != 0) goto L_0x01d1
            L_0x015d:
                if (r6 == 0) goto L_0x0162
                r6.close()     // Catch:{ all -> 0x018f }
            L_0x0162:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0198 }
                r0.<init>()     // Catch:{ SQLException -> 0x0198 }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0198 }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x0198 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0198 }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0198 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0198 }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0198 }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0198 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x0198 }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x0198 }
            L_0x018c:
                monitor-exit(r9)     // Catch:{ all -> 0x018f }
                goto L_0x0073
            L_0x018f:
                r0 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x018f }
                throw r0
            L_0x0192:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x018f }
                r6 = r7
                goto L_0x0155
            L_0x0198:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x018f }
                goto L_0x018c
            L_0x019d:
                com.shoujiduoduo.util.m r0 = com.shoujiduoduo.util.m.this     // Catch:{ all -> 0x018f }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ all -> 0x018f }
                java.lang.String r0 = r0.a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x018f }
                boolean r1 = com.shoujiduoduo.util.p.a(r10, r0)     // Catch:{ all -> 0x018f }
                if (r1 == 0) goto L_0x01d1
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x022b }
                r1.<init>()     // Catch:{ SQLiteException -> 0x022b }
                java.lang.String r2 = "UPDATE ringtoneduoduo_resourcetable_3 SET path="
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x022b }
                java.lang.String r0 = android.database.DatabaseUtils.sqlEscapeString(r0)     // Catch:{ SQLiteException -> 0x022b }
                java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ SQLiteException -> 0x022b }
                java.lang.String r1 = " WHERE rid="
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLiteException -> 0x022b }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLiteException -> 0x022b }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLiteException -> 0x022b }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLiteException -> 0x022b }
                r1.execSQL(r0)     // Catch:{ SQLiteException -> 0x022b }
            L_0x01d1:
                boolean r0 = r6.moveToNext()     // Catch:{ all -> 0x018f }
                if (r0 == 0) goto L_0x01f5
                r0 = 8
                java.lang.String r5 = r6.getString(r0)     // Catch:{ all -> 0x018f }
                r0 = 2
                java.lang.String r2 = r6.getString(r0)     // Catch:{ all -> 0x018f }
                r0 = 3
                java.lang.String r3 = r6.getString(r0)     // Catch:{ all -> 0x018f }
                r0 = 10
                java.lang.String r10 = r6.getString(r0)     // Catch:{ all -> 0x018f }
                java.util.HashSet<java.lang.String> r0 = r12.f     // Catch:{ all -> 0x018f }
                boolean r0 = r0.contains(r10)     // Catch:{ all -> 0x018f }
                if (r0 == 0) goto L_0x019d
            L_0x01f5:
                r6.close()     // Catch:{ all -> 0x018f }
                monitor-exit(r9)     // Catch:{ all -> 0x018f }
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0225 }
                r0.<init>()     // Catch:{ SQLException -> 0x0225 }
                java.lang.String r1 = "delete from "
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0225 }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x0225 }
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0225 }
                java.lang.String r1 = " where rid='"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0225 }
                java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ SQLException -> 0x0225 }
                java.lang.String r1 = "'"
                java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ SQLException -> 0x0225 }
                java.lang.String r0 = r0.toString()     // Catch:{ SQLException -> 0x0225 }
                android.database.sqlite.SQLiteDatabase r1 = r12.e     // Catch:{ SQLException -> 0x0225 }
                r1.execSQL(r0)     // Catch:{ SQLException -> 0x0225 }
                goto L_0x0073
            L_0x0225:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0073
            L_0x022b:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x018f }
                goto L_0x01d1
            L_0x0230:
                android.database.sqlite.SQLiteDatabase r0 = r12.e     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.String r1 = r12.d     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                long r0 = android.database.DatabaseUtils.queryNumEntries(r0, r1)     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                r2 = 0
                int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r0 != 0) goto L_0x025e
                android.database.sqlite.SQLiteDatabase r0 = r12.e     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                r1.<init>()     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.String r2 = "DROP TABLE "
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.String r2 = r12.d     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.String r2 = ";"
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                java.lang.String r1 = r1.toString()     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
                r0.execSQL(r1)     // Catch:{ SQLException -> 0x0262, NumberFormatException -> 0x0267, NullPointerException -> 0x026c }
            L_0x025e:
                r12.f3208a = r11
                goto L_0x0012
            L_0x0262:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x025e
            L_0x0267:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x025e
            L_0x026c:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x025e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.m.b.run():void");
        }
    }
}
