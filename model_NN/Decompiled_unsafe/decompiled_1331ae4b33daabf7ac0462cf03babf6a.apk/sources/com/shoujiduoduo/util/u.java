package com.shoujiduoduo.util;

/* compiled from: KwThreadPool */
public final class u {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static volatile int f3237a = 0;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static b[] f3238b = new b[5];

    /* compiled from: KwThreadPool */
    public enum a {
        NORMAL,
        NET,
        IMMEDIATELY
    }

    static /* synthetic */ int c() {
        int i = f3237a + 1;
        f3237a = i;
        return i;
    }

    public static void a(a aVar, Runnable runnable) {
        if (aVar == a.NET) {
        }
        d().a(runnable, 0);
    }

    private static b d() {
        if (f3237a == 0) {
            return new b();
        }
        synchronized (f3238b) {
            if (f3237a == 0) {
                b bVar = new b();
                return bVar;
            }
            f3237a--;
            b bVar2 = f3238b[f3237a];
            f3238b[f3237a] = null;
            return bVar2;
        }
    }

    /* compiled from: KwThreadPool */
    private static final class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private volatile Runnable f3241a;

        /* renamed from: b  reason: collision with root package name */
        private volatile int f3242b;
        private volatile boolean c;

        private b() {
        }

        public void a(Runnable runnable, int i) {
            this.f3241a = runnable;
            this.f3242b = i;
            if (!this.c) {
                start();
                this.c = true;
                return;
            }
            synchronized (this) {
                notify();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
            wait();
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r4 = this;
                r3 = 5
            L_0x0001:
                int r0 = r4.f3242b
                android.os.Process.setThreadPriority(r0)
                java.lang.Runnable r0 = r4.f3241a
                r0.run()
                int r0 = com.shoujiduoduo.util.u.f3237a
                if (r0 < r3) goto L_0x0015
            L_0x0011:
                r0 = 0
                r4.c = r0
                return
            L_0x0015:
                monitor-enter(r4)
                com.shoujiduoduo.util.u$b[] r1 = com.shoujiduoduo.util.u.f3238b     // Catch:{ all -> 0x0024 }
                monitor-enter(r1)     // Catch:{ all -> 0x0024 }
                int r0 = com.shoujiduoduo.util.u.f3237a     // Catch:{ all -> 0x003a }
                if (r0 < r3) goto L_0x0027
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0011
            L_0x0024:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                throw r0
            L_0x0027:
                com.shoujiduoduo.util.u$b[] r0 = com.shoujiduoduo.util.u.f3238b     // Catch:{ all -> 0x003a }
                int r2 = com.shoujiduoduo.util.u.f3237a     // Catch:{ all -> 0x003a }
                r0[r2] = r4     // Catch:{ all -> 0x003a }
                com.shoujiduoduo.util.u.c()     // Catch:{ all -> 0x003a }
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                r4.wait()     // Catch:{ InterruptedException -> 0x003d }
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0001
            L_0x003a:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x003a }
                throw r0     // Catch:{ all -> 0x0024 }
            L_0x003d:
                r0 = move-exception
                monitor-exit(r4)     // Catch:{ all -> 0x0024 }
                goto L_0x0011
            */
            throw new UnsupportedOperationException("Method not decompiled: com.shoujiduoduo.util.u.b.run():void");
        }
    }
}
