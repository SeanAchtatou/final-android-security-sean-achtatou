package com.shoujiduoduo.util.f;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: ID3V2 */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private File f3185a;

    /* renamed from: b  reason: collision with root package name */
    private int f3186b = -1;
    private Map<String, byte[]> c = new HashMap();

    public c(File file) {
        this.f3185a = file;
    }

    public void a() throws d, IOException {
        if (this.f3185a == null) {
            throw new NullPointerException("MP3 file is not found");
        }
        FileInputStream fileInputStream = new FileInputStream(this.f3185a);
        byte[] bArr = new byte[10];
        fileInputStream.read(bArr);
        if (bArr[0] == 73 && bArr[1] == 68 && bArr[2] == 51) {
            this.f3186b = ((bArr[6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 21) + (bArr[9] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) + ((bArr[8] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 7) + ((bArr[7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 14);
            int i = 10;
            while (i < this.f3186b) {
                byte[] bArr2 = new byte[10];
                fileInputStream.read(bArr2);
                if (bArr2[0] == 0) {
                    break;
                }
                String stringBuffer = new StringBuffer().append((char) bArr2[0]).append((char) bArr2[1]).append((char) bArr2[2]).append((char) bArr2[3]).toString();
                int a2 = a(bArr2, 4, 4);
                if (a2 < 0 || a2 > this.f3186b) {
                    throw new d("错误的MP3文件格式，无法切割");
                }
                byte[] bArr3 = new byte[a2];
                fileInputStream.read(bArr3);
                this.c.put(stringBuffer, bArr3);
                i = i + a2 + 10;
            }
            fileInputStream.close();
            return;
        }
        this.f3186b = 0;
        throw new d("not invalid mp3 ID3 tag");
    }

    private int a(byte[] bArr, int i, int i2) {
        int i3 = 0;
        for (int i4 = i; i4 < i + i2; i4++) {
            i3 = (int) (((long) i3) + ((long) ((bArr[i4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << ((((i2 - i4) + i) - 1) * 8))));
        }
        return i3;
    }

    public int b() {
        return this.f3186b;
    }
}
