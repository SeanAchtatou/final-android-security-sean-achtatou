package com.shoujiduoduo.util;

import android.content.Context;
import android.text.TextUtils;
import com.shoujiduoduo.ringtone.RingDDApp;

/* compiled from: AdUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Boolean f2966a;

    /* renamed from: b  reason: collision with root package name */
    private static Boolean f2967b;
    private static String c;
    private static Boolean d;

    public static String a() {
        if (c != null) {
            return c;
        }
        String a2 = ab.a().a("navi_ad_type");
        if ("video".equals(a2)) {
            c = "video";
        } else if ("shop".equals(a2)) {
            c = "shop";
        } else if ("news".equals(a2)) {
            c = "news";
        } else {
            c = "shop";
        }
        return c;
    }

    public static boolean b() {
        if (d != null) {
            return d.booleanValue();
        }
        d = Boolean.valueOf(ab.a().b("navi_ad_enable"));
        return d.booleanValue();
    }

    public static boolean c() {
        if (f2967b != null) {
            return f2967b.booleanValue();
        }
        if (ab.a().b("aichang_enable")) {
            f2967b = true;
            return true;
        }
        f2967b = false;
        return false;
    }

    public static boolean d() {
        return true;
    }

    public static boolean a(boolean z) {
        if (z) {
            return k();
        }
        String n = f.n();
        String a2 = ad.a(RingDDApp.c(), "pref_config_src", "");
        com.shoujiduoduo.base.a.a.a("AdUtils", "curSrc:" + n);
        com.shoujiduoduo.base.a.a.a("AdUtils", "configSrc:" + a2);
        if (!n.equalsIgnoreCase(a2)) {
            return false;
        }
        String a3 = ab.a().a("ad_delay_time");
        com.shoujiduoduo.base.a.a.a("AdUtils", "同版本的配置文件，使用缓存即可, ad_delay:" + a3);
        return "0".equals(a3);
    }

    public static boolean e() {
        if (j()) {
            return false;
        }
        return k();
    }

    public static boolean f() {
        boolean z;
        if (j()) {
            return false;
        }
        String a2 = ab.a().a("feed_ad_v1_channel");
        String a3 = ab.a().a("feed_ad_v1_enable");
        if (a2.contains(f.k()) || a2.equals("all")) {
            z = true;
        } else {
            z = false;
        }
        if (!a3.equals("true") || !z) {
            return false;
        }
        return k();
    }

    private static boolean j() {
        int a2 = ad.a(RingDDApp.c(), "user_vip_type", 0);
        if (ad.a(RingDDApp.c(), "user_loginStatus", 0) != 1 || a2 == 0) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, long):long
     arg types: [?[OBJECT, ARRAY], java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, int):int
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, long):long */
    private static synchronized boolean k() {
        long j;
        long a2;
        boolean z = false;
        synchronized (a.class) {
            if (f2966a == null || !f2966a.booleanValue()) {
                long a3 = ad.a((Context) null, "first_start_time:" + f.n(), 0L);
                long currentTimeMillis = System.currentTimeMillis();
                if (a3 == 0) {
                    ad.b((Context) null, "first_start_time:" + f.n(), currentTimeMillis);
                    j = currentTimeMillis;
                } else {
                    j = a3;
                }
                String a4 = ab.a().a("ad_delay_time");
                if (a4.equals("0")) {
                    a2 = 0;
                } else {
                    a2 = (long) (q.a(a4, 0) * 60 * 1000);
                }
                if (a2 == 0 || currentTimeMillis - j >= a2) {
                    f2966a = true;
                    z = true;
                } else {
                    f2966a = false;
                }
            } else {
                z = f2966a.booleanValue();
            }
        }
        return z;
    }

    public static synchronized boolean g() {
        boolean z;
        synchronized (a.class) {
            String a2 = ab.a().a("tencent_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean h() {
        boolean z;
        synchronized (a.class) {
            String a2 = ab.a().a("baidu_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }

    public static synchronized boolean i() {
        boolean z;
        synchronized (a.class) {
            String a2 = ab.a().a("duoduo_ad_duration");
            if (TextUtils.isEmpty(a2) || "0".equalsIgnoreCase(a2)) {
                z = false;
            } else {
                z = true;
            }
        }
        return z;
    }
}
