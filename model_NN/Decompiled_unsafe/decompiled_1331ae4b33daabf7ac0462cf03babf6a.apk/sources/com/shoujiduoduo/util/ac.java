package com.shoujiduoduo.util;

import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.widget.Toast;
import com.igexin.download.Downloads;
import com.shoujiduoduo.a.a.b;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.i;
import com.shoujiduoduo.a.c.p;
import com.shoujiduoduo.base.bean.MakeRingData;
import com.shoujiduoduo.base.bean.RingData;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;

/* compiled from: SetRingTone */
public class ac implements i {
    private static ac i = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RingData f2992a;

    /* renamed from: b  reason: collision with root package name */
    private String f2993b;
    private String c;
    /* access modifiers changed from: private */
    public Context d;
    private HashMap<String, Integer> e = new HashMap<>();
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public int g;
    private a h = new a(this);
    private String j;

    private ac(Context context) {
        this.d = context;
    }

    public static synchronized ac a(Context context) {
        ac acVar;
        synchronized (ac.class) {
            if (i == null) {
                i = new ac(context);
                m.a(context).a(i);
                com.shoujiduoduo.base.a.a.a("SetRingTone", "getInstance: SetRingTone created!");
            }
            acVar = i;
        }
        return acVar;
    }

    public static ac a() {
        if (i != null) {
            return i;
        }
        return null;
    }

    /* compiled from: SetRingTone */
    private static class a extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<ac> f2994a;

        public a(ac acVar) {
            this.f2994a = new WeakReference<>(acVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.shoujiduoduo.util.ac.a(com.shoujiduoduo.util.ac, boolean):boolean
         arg types: [com.shoujiduoduo.util.ac, int]
         candidates:
          com.shoujiduoduo.util.ac.a(com.shoujiduoduo.base.bean.RingData, int):void
          com.shoujiduoduo.util.ac.a(java.lang.String, android.net.Uri):boolean
          com.shoujiduoduo.util.ac.a(com.shoujiduoduo.base.bean.i, int):void
          com.shoujiduoduo.a.c.i.a(com.shoujiduoduo.base.bean.i, int):void
          com.shoujiduoduo.util.ac.a(com.shoujiduoduo.util.ac, boolean):boolean */
        public void handleMessage(Message message) {
            super.handleMessage(message);
            ac acVar = this.f2994a.get();
            if (acVar != null) {
                switch (message.what) {
                    case 1001:
                        Toast.makeText(acVar.d, acVar.d.getResources().getText(R.string.set_ring_error_message), 1).show();
                        return;
                    case 1002:
                        Toast.makeText(acVar.d, (String) message.obj, 1).show();
                        return;
                    case 2000:
                        com.shoujiduoduo.base.bean.i iVar = (com.shoujiduoduo.base.bean.i) message.obj;
                        if (acVar.f2992a != null && acVar.f && iVar.c == acVar.f2992a.k()) {
                            acVar.a(acVar.g);
                            boolean unused = acVar.f = false;
                            return;
                        }
                        return;
                    default:
                        return;
                }
            }
        }
    }

    public void a(HashMap<String, Integer> hashMap, RingData ringData, String str, String str2) {
        this.e.clear();
        this.e.putAll(hashMap);
        this.f = false;
        this.f2992a = ringData;
        this.f2993b = str;
        this.c = str2;
        com.shoujiduoduo.base.bean.i c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "不应该为空，找漏洞吧！");
            this.h.sendEmptyMessage(1001);
        } else if (c2.d < c2.e || c2.e < 0) {
            this.f = true;
            this.g = 0;
            Message message = new Message();
            message.what = 1002;
            message.obj = this.d.getResources().getString(R.string.set_ring_not_down_finish_hint) + this.d.getResources().getString(R.string.set_ring_contact);
            this.h.sendMessage(message);
        } else {
            a(8);
        }
    }

    public void a(int i2, RingData ringData, String str, String str2) {
        this.f = false;
        this.f2992a = ringData;
        this.f2993b = str;
        this.c = str2;
        com.shoujiduoduo.base.bean.i c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "不应该为空，找漏洞吧！");
            this.h.sendEmptyMessage(1001);
            return;
        }
        com.shoujiduoduo.base.a.a.a("SetRingTone", "type = " + i2);
        if (c2.d < c2.e || c2.e < 0) {
            com.shoujiduoduo.base.a.a.a("SetRingTone", "铃声尚未下载完成，下载完之后再设置铃声");
            this.f = true;
            this.g = i2;
            Message message = new Message();
            message.what = 1002;
            String string = this.d.getResources().getString(R.string.set_ring_not_down_finish_hint);
            String str3 = "";
            if (!((i2 & 1) == 0 && (i2 & 32) == 0)) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_incoming_call);
            }
            if (!((i2 & 2) == 0 && (i2 & 64) == 0 && (i2 & 128) == 0)) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_message);
            }
            if ((i2 & 4) != 0) {
                str3 = str3 + this.d.getResources().getString(R.string.set_ring_alarm);
            }
            message.obj = string + str3;
            this.h.sendMessage(message);
            return;
        }
        a(i2);
    }

    private boolean a(Uri uri) {
        for (String next : this.e.keySet()) {
            if (this.e.get(next).intValue() == 0) {
                a(next);
            } else if (this.e.get(next).intValue() == 1) {
                a(next, uri);
            }
        }
        return true;
    }

    private boolean a(String str, Uri uri) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", uri.toString());
            this.d.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private boolean a(String str) {
        try {
            Uri withAppendedPath = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, str);
            ContentValues contentValues = new ContentValues();
            contentValues.put("custom_ringtone", RingtoneManager.getActualDefaultRingtoneUri(this.d, 1).toString());
            this.d.getContentResolver().update(withAppendedPath, contentValues, null, null);
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private com.shoujiduoduo.base.bean.i c() {
        if (!(this.f2992a instanceof MakeRingData)) {
            return m.a(this.d).b(this.f2992a.k());
        }
        MakeRingData makeRingData = (MakeRingData) this.f2992a;
        File file = new File(makeRingData.o);
        if (file.exists()) {
            com.shoujiduoduo.base.bean.i iVar = new com.shoujiduoduo.base.bean.i(this.f2992a.e, this.f2992a.f, 0, (int) file.length(), (int) file.length(), 128000, p.b(makeRingData.o), "");
            iVar.e(makeRingData.o);
            return iVar;
        } else if (!makeRingData.g.equals("")) {
            return m.a(this.d).b(this.f2992a.k());
        } else {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public static boolean a(int i2, String str, String str2, String str3, String str4) {
        File file = new File(str);
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a("SetRingTone", "ringtone path: " + file.getAbsolutePath());
            String b2 = p.b(str);
            ContentValues contentValues = new ContentValues();
            if (b2.equals("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (b2.equals("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c("SetRingTone", "format data not support");
                return false;
            }
            contentValues.put(Downloads._DATA, file.getAbsolutePath());
            contentValues.put("title", str2);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", str3);
            contentValues.put("duration", str4);
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone:uri is NULL!");
                return false;
            }
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri decoded = " + contentUriForPath.getPath());
            try {
                RingDDApp.c().getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
            }
            try {
                Uri insert = RingDDApp.c().getContentResolver().insert(contentUriForPath, contentValues);
                if (insert == null) {
                    com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone: newURI is NULL!");
                    return false;
                }
                a(RingDDApp.c(), i2, insert);
                return true;
            } catch (Exception e3) {
                return false;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c("SetRingTone", "ringtone file missing!");
            return false;
        }
    }

    public boolean a(final int i2) {
        com.shoujiduoduo.base.a.a.a("SetRingTone", "in setRing");
        Uri d2 = d();
        if (d2 == null) {
            return false;
        }
        if ((i2 & 1) != 0) {
            a(this.f2992a, 1);
            ad.c(this.d, "user_ring_phone_select", this.f2992a.g);
            ad.c(this.d, "user_ring_phone_select_name", this.f2992a.e);
            if (r.b()) {
                r.a(this.d, d2, i2);
            } else {
                ah.a(this.d, d2, this.j);
                a(this.d, 1, d2);
            }
        }
        if ((i2 & 2) != 0) {
            a(this.f2992a, 2);
            ad.c(this.d, "user_ring_notification_select", this.f2992a.g);
            ad.c(this.d, "user_ring_notification_select_name", this.f2992a.e);
            if (r.b()) {
                r.a(this.d, d2, i2);
            } else {
                ah.b(this.d, d2, this.j);
                if (!Build.BRAND.equalsIgnoreCase("OPPO")) {
                    a(this.d, 2, d2);
                }
            }
        }
        if ((i2 & 4) != 0) {
            a(this.f2992a, 3);
            ad.c(this.d, "user_ring_alarm_select", this.f2992a.g);
            ad.c(this.d, "user_ring_alarm_select_name", this.f2992a.e);
            if (r.b()) {
                r.a(this.d, d2, i2);
            } else {
                ah.c(this.d, d2, this.j);
                a(this.d, 4, d2);
            }
        }
        if ((i2 & 8) != 0) {
            a(this.f2992a, 4);
            a(d2);
        }
        c.a().b(b.OBSERVER_RING_CHANGE, new c.a<p>() {
            public void a() {
                ((p) this.f1812a).a(i2, ac.this.f2992a);
            }
        });
        Message message = new Message();
        message.what = 1002;
        String string = this.d.getResources().getString(R.string.set_ring_hint);
        if ((i2 & 1) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_incoming_call)) + " ";
        }
        if ((i2 & 2) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_message)) + " ";
        }
        if ((i2 & 4) != 0) {
            string = (string + this.d.getResources().getString(R.string.set_ring_alarm)) + " ";
        }
        if ((i2 & 8) != 0) {
            string = string + this.d.getResources().getString(R.string.set_ring_contact);
        }
        message.obj = string;
        this.h.sendMessage(message);
        return true;
    }

    private static void a(Context context, int i2, Uri uri) {
        try {
            RingtoneManager.setActualDefaultRingtoneUri(context, i2, uri);
        } catch (UnsupportedOperationException e2) {
            e2.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e2);
        } catch (SecurityException e3) {
            e3.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e3);
        } catch (Exception e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.a(e4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    private Uri d() {
        com.shoujiduoduo.base.bean.i c2 = c();
        if (c2 == null) {
            com.shoujiduoduo.base.a.a.c("SetRingTone", "data is null");
            this.h.sendEmptyMessage(1001);
            return null;
        }
        String l = c2.l();
        this.j = l;
        File file = new File(l);
        try {
            FileInputStream fileInputStream = new FileInputStream(l);
            int available = fileInputStream.available();
            fileInputStream.close();
            com.shoujiduoduo.base.a.a.a("SetRingTone", "ringtone path: " + file.getAbsolutePath());
            ContentValues contentValues = new ContentValues();
            if (c2.g == null || c2.g.length() == 0) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.g.equalsIgnoreCase("mp3")) {
                contentValues.put("mime_type", "audio/mp3");
            } else if (c2.g.equalsIgnoreCase("aac")) {
                contentValues.put("mime_type", "audio/aac");
            } else {
                com.shoujiduoduo.base.a.a.c("SetRingTone", "format data not support");
                this.h.sendEmptyMessage(1001);
                return null;
            }
            contentValues.put(Downloads._DATA, file.getAbsolutePath());
            contentValues.put("title", this.f2992a.e);
            contentValues.put("_size", Integer.valueOf(available));
            contentValues.put("artist", this.f2992a.f);
            contentValues.put("duration", Double.valueOf(((double) this.f2992a.l) * 1000.0d));
            contentValues.put("is_ringtone", (Boolean) true);
            contentValues.put("is_notification", (Boolean) true);
            contentValues.put("is_alarm", (Boolean) true);
            contentValues.put("is_music", (Boolean) false);
            Uri contentUriForPath = MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
            if (contentUriForPath == null) {
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone:uri is NULL!");
                this.h.sendEmptyMessage(1001);
                return null;
            }
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri encoded = " + contentUriForPath.toString());
            com.shoujiduoduo.base.a.a.a("SetRingTone", "uri decoded = " + contentUriForPath.getPath());
            try {
                this.d.getContentResolver().delete(contentUriForPath, "_data=\"" + file.getAbsolutePath() + "\"", null);
            } catch (Exception e2) {
                f.c("delete media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e2));
            }
            try {
                Uri insert = this.d.getContentResolver().insert(contentUriForPath, contentValues);
                if (insert != null) {
                    return insert;
                }
                com.shoujiduoduo.base.a.a.a("SetRingTone", "setringtone: newURI is NULL!");
                this.h.sendEmptyMessage(1001);
                return null;
            } catch (Exception e3) {
                f.c("insert media uri error! uri = " + contentUriForPath.toString() + "\n" + "path = " + file.getAbsolutePath() + "\n" + com.shoujiduoduo.base.a.b.a(e3));
                this.h.sendEmptyMessage(1001);
                return null;
            }
        } catch (IOException e4) {
            e4.printStackTrace();
            com.shoujiduoduo.base.a.a.c("SetRingTone", "ringtone file missing!");
            this.h.sendEmptyMessage(1001);
            return null;
        }
    }

    private void a(RingData ringData, int i2) {
        ak.a(ringData.g, i2, "&from=" + this.f2993b + "&listType=" + this.c + "&cucid=" + ringData.C);
    }

    public void a(com.shoujiduoduo.base.bean.i iVar) {
    }

    public void a(com.shoujiduoduo.base.bean.i iVar, int i2) {
    }

    public void b(com.shoujiduoduo.base.bean.i iVar) {
        this.h.sendMessage(this.h.obtainMessage(2000, iVar));
    }

    public void c(com.shoujiduoduo.base.bean.i iVar) {
    }

    public void d(com.shoujiduoduo.base.bean.i iVar) {
    }

    public void b() {
        if (this.h != null) {
            this.h.removeCallbacksAndMessages(null);
        }
        i = null;
    }
}
