package com.shoujiduoduo.util;

import com.shoujiduoduo.base.a.a;
import java.io.File;

/* compiled from: DuoduoCache */
public abstract class o<T> {
    protected static String c = k.a(2);

    /* renamed from: b  reason: collision with root package name */
    protected String f3216b = null;

    public o(String str) {
        this.f3216b = str;
    }

    public o() {
    }

    public static String b() {
        return c;
    }

    public boolean a(long j) {
        long lastModified = new File(c + this.f3216b).lastModified();
        if (lastModified == 0) {
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - lastModified;
        long j2 = currentTimeMillis / 1000;
        a.a("DuoduoCache Base Class", "time since cached: " + (j2 / 3600) + "小时" + ((j2 % 3600) / 60) + "分钟" + (j2 % 60) + "秒");
        if (currentTimeMillis > j) {
            a.a("DuoduoCache Base Class", "cache out of date.");
            return true;
        }
        a.a("DuoduoCache Base Class", "cache is valid. use cache.");
        return false;
    }
}
