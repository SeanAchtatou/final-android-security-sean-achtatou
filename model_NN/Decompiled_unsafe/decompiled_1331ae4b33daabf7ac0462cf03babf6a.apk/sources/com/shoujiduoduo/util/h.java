package com.shoujiduoduo.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* compiled from: DDThreadPool */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f3191a = Executors.newCachedThreadPool();

    public static void a(Runnable runnable) {
        f3191a.execute(runnable);
    }

    public static void a() {
        if (f3191a != null) {
            f3191a.shutdown();
        }
    }
}
