package com.shoujiduoduo.ringtone;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import cn.banshenggua.aichang.sdk.ACContext;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.Constants;
import com.d.a.b.a.l;
import com.d.a.b.d;
import com.d.a.b.e;
import com.duoduo.dynamicdex.DuoMobAdUtils;
import com.duoduo.dynamicdex.DuoMobApp;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.base.a.a;
import com.shoujiduoduo.util.NetworkStateUtil;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.ag;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.h;
import com.shoujiduoduo.util.p;
import com.shoujiduoduo.util.w;
import com.umeng.analytics.b;
import com.umeng.socialize.PlatformConfig;
import java.util.HashMap;

public class RingDDApp extends Application {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2012a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static volatile boolean f2013b;
    private static RingDDApp c = null;
    private static long d = Thread.currentThread().getId();
    private static Handler e = new Handler();
    private static boolean g;
    private static boolean i = false;
    private HashMap<String, Object> f;
    private BroadcastReceiver h = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("com.shoujiduoduo.ringtone.exitapp".equals(intent.getAction())) {
                a.a("RingDDApp", "ExitAppReceiver received");
                RingDDApp.g();
            }
        }
    };

    public void onCreate() {
        super.onCreate();
        a.a("RingDDApp", "**********************************************************");
        a.a("RingDDApp", "\n\r\n\r");
        a.a("RingDDApp", "App Instance is created!, Main ThreadID = " + Thread.currentThread().getId());
        a.a("RingDDApp", "\n\r\n\r");
        a.a("RingDDApp", "**********************************************************");
        c = this;
        try {
            ACContext.initContext(this);
        } catch (ACException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
        }
        if (!f.c(this)) {
            a.a("RingDDApp", "若由其他非主进程的进程启动，不需要进行多次初始化");
        } else {
            i();
        }
    }

    private void i() {
        f.b(this);
        NetworkStateUtil.a(getApplicationContext());
        ad.a(this);
        f.b bVar = f.b.d;
        try {
            f.s();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        p.a(this);
        ab.a().b();
        this.f = new HashMap<>();
        h.a(new Runnable() {
            public void run() {
                w.a();
            }
        });
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.shoujiduoduo.ringtone.exitapp");
        registerReceiver(this.h, intentFilter);
        h();
        l();
        k();
        j();
        a.a("RingDDApp", "app version:" + f.o());
        a.a("RingDDApp", "app install src:" + f.n());
        a.a("RingDDApp", "device info:" + f.i());
        a.a("RingDDApp", "os version:" + f.j());
        a.a("RingDDApp", "density:" + getResources().getDisplayMetrics().density);
        a.a("RingDDApp", "Build.BRAND:" + Build.BRAND);
        a.a("RingDDApp", "Build.MANUFACTURER:" + Build.MANUFACTURER);
        a.a("RingDDApp", "Build.MODEL:" + Build.MODEL);
        a.a("RingDDApp", "Build.DEVICE:" + Build.DEVICE);
        a.a("RingDDApp", "Build.BOARD:" + Build.BOARD);
        a.a("RingDDApp", "Build.DISPLAY:" + Build.DISPLAY);
        a.a("RingDDApp", "Build.PRODUCT:" + Build.PRODUCT);
        a.a("RingDDApp", "Build.BOOTLOADER:" + Build.BOOTLOADER);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        android.support.multidex.a.a(this);
    }

    private void j() {
        String a2 = ad.a(this, "pref_load_cmcc_sunshine_sdk_start", "");
        String a3 = ad.a(this, "pref_load_cmcc_sunshine_sdk_end", "");
        if (a2.equals("1") && !a3.equals("1")) {
            a(true);
            HashMap hashMap = new HashMap();
            hashMap.put("phone", Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.SDK_INT);
            a.a("RingDDApp", "sunshine sdk crash last time:" + Build.BRAND + "-" + Build.MODEL + "-" + Build.VERSION.RELEASE);
            b.a(this, "cmcc_sunshine_303_sdk_crash", hashMap);
        }
    }

    public boolean a() {
        return g;
    }

    public void a(boolean z) {
        g = z;
    }

    private void k() {
        DuoMobApp.Ins.init(this);
        String a2 = com.umeng.a.a.a().a(c(), "duo_ad_jar_url");
        if (ag.c(a2)) {
            a2 = "http://bbhlt.shoujiduoduo.com/bb/jar/duomobad_0_1_0.jar";
        }
        DuoMobAdUtils.Ins.prepareFmAssert(3, "duomobad_0_1_0.jpg", a2, new DuoMobAdUtils.DuoMobAdPrepareListener() {
            public void loadFailed(int i) {
                HashMap hashMap = new HashMap();
                hashMap.put(Parameters.RESOLUTION, "fail");
                hashMap.put("errcode", "" + i);
                b.a(RingDDApp.c(), "load_duo_ad_dex", hashMap);
            }

            public void loaded() {
                HashMap hashMap = new HashMap();
                hashMap.put(Parameters.RESOLUTION, "success");
                b.a(RingDDApp.c(), "load_duo_ad_dex", hashMap);
            }
        });
    }

    public static RingDDApp b() {
        return c;
    }

    public static Context c() {
        return c;
    }

    public static long d() {
        return d;
    }

    public static Handler e() {
        return e;
    }

    public static boolean f() {
        return f2013b;
    }

    public static void g() {
        if (!f2012a) {
            f2012a = true;
            NetworkStateUtil.b(c.getApplicationContext());
            c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, new c.a<com.shoujiduoduo.a.c.a>() {
                public void a() {
                    try {
                        ((com.shoujiduoduo.a.c.a) this.f1812a).a();
                    } catch (Throwable th) {
                    }
                }
            });
            c.a().b(new c.b() {
                public void a() {
                    boolean unused = RingDDApp.f2013b = true;
                    c.a().b(new c.b() {
                        public void a() {
                            c.a().b();
                            try {
                                com.shoujiduoduo.a.b.b.a();
                            } catch (Throwable th) {
                            }
                            c.a().a((int) Constants.textWatch_result, new c.b() {
                                public void a() {
                                    h.a();
                                    b.c(RingDDApp.c());
                                    Process.killProcess(Process.myPid());
                                    System.exit(0);
                                }
                            });
                        }
                    });
                }
            });
        }
    }

    public void onTerminate() {
        a.a("RingDDApp", "App onTerminate.");
        unregisterReceiver(this.h);
        super.onTerminate();
    }

    public void a(String str, Object obj) {
        this.f.put(str, obj);
    }

    public Object a(String str) {
        if (this.f.containsKey(str)) {
            return this.f.get(str);
        }
        return null;
    }

    public static void h() {
        if (!i) {
            i = true;
            d.a().a(new e.a(c()).b(3).a().a(new com.d.a.a.a.b.c()).a(l.LIFO).c());
        }
    }

    private void l() {
        PlatformConfig.setWeixin("wxb4cd572ca73fd239", "48bccd48df1afa300a52bd75611a6710");
        PlatformConfig.setSinaWeibo("1981517408", "001b0b305f9cbb4f307866ea81cd473c");
        PlatformConfig.setQQZone("100382066", "2305e02edac0c67f822f453d92be2a66");
    }
}
