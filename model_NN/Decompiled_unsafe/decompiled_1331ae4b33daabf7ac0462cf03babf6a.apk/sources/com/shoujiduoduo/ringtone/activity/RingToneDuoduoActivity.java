package com.shoujiduoduo.ringtone.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.banshenggua.aichang.utils.Constants;
import com.cmsc.cmmusic.init.InitCmmInterface;
import com.igexin.sdk.PushManager;
import com.shoujiduoduo.a.a.c;
import com.shoujiduoduo.a.c.v;
import com.shoujiduoduo.a.c.x;
import com.shoujiduoduo.player.PlayerService;
import com.shoujiduoduo.ringtone.R;
import com.shoujiduoduo.ringtone.RingDDApp;
import com.shoujiduoduo.service.DuoDuoIntentService;
import com.shoujiduoduo.service.GTPushService;
import com.shoujiduoduo.ui.adwall.AdWallFrag;
import com.shoujiduoduo.ui.adwall.MoreOptionFrag;
import com.shoujiduoduo.ui.aichang.AiChangFrag;
import com.shoujiduoduo.ui.category.CategoryFrag;
import com.shoujiduoduo.ui.home.HomepageFrag;
import com.shoujiduoduo.ui.home.MusicAlbumActivity;
import com.shoujiduoduo.ui.makering.MakeRingActivity;
import com.shoujiduoduo.ui.mine.MyRingtoneFrag;
import com.shoujiduoduo.ui.search.SearchActivity;
import com.shoujiduoduo.ui.utils.BaseFragmentActivity;
import com.shoujiduoduo.ui.utils.MyViewPager;
import com.shoujiduoduo.ui.utils.i;
import com.shoujiduoduo.util.ab;
import com.shoujiduoduo.util.ac;
import com.shoujiduoduo.util.ad;
import com.shoujiduoduo.util.aj;
import com.shoujiduoduo.util.c.c;
import com.shoujiduoduo.util.f;
import com.shoujiduoduo.util.l;
import com.shoujiduoduo.util.m;
import com.shoujiduoduo.util.widget.MyRadioButton;
import com.shoujiduoduo.util.widget.WebViewActivity;
import com.shoujiduoduo.util.widget.b;
import com.shoujiduoduo.util.widget.d;
import com.shoujiduoduo.util.z;
import com.umeng.socialize.UMShareAPI;
import java.io.File;
import java.util.ArrayList;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class RingToneDuoduoActivity extends BaseFragmentActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private static final String k = ("start_time:" + f.n());
    private static RingToneDuoduoActivity n;
    private static boolean o;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public MyRadioButton f2017a;

    /* renamed from: b  reason: collision with root package name */
    private TextView f2018b;
    private Drawable c;
    private RelativeLayout d;
    private RelativeLayout e;
    /* access modifiers changed from: private */
    public PlayerService f;
    /* access modifiers changed from: private */
    public ProgressDialog g;
    private MyViewPager h;
    /* access modifiers changed from: private */
    public ArrayList<Fragment> i = new ArrayList<>();
    /* access modifiers changed from: private */
    public RadioGroup j;
    private a l;
    private boolean m;
    private boolean p;
    private com.shoujiduoduo.ui.settings.a q;
    private com.shoujiduoduo.a.c.f r = new com.shoujiduoduo.a.c.f() {
        public void a(boolean z) {
            if (z) {
                RingToneDuoduoActivity.this.g();
            }
        }
    };
    private v s = new v() {
        public void b(int i) {
        }

        public void a(int i) {
            RingToneDuoduoActivity.this.f2017a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, i.b(R.drawable.btn_navi_myring), (Drawable) null, (Drawable) null);
        }

        public void a(int i, boolean z, String str, String str2) {
        }
    };
    private x t = new x() {
        public void a(int i) {
            Drawable b2;
            if (com.shoujiduoduo.a.b.b.g().h()) {
                b2 = i.b(R.drawable.btn_navi_myring_vip);
            } else {
                b2 = i.b(R.drawable.btn_navi_myring);
            }
            RingToneDuoduoActivity.this.f2017a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, b2, (Drawable) null, (Drawable) null);
        }
    };
    private com.shoujiduoduo.a.c.a u = new com.shoujiduoduo.a.c.a() {
        public void a() {
            RingToneDuoduoActivity.this.d();
            RingToneDuoduoActivity.this.finish();
        }

        public void b() {
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "Sd card error");
            new b.a(RingToneDuoduoActivity.this).a((int) R.string.sdcard_not_access).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    RingToneDuoduoActivity.this.finish();
                    RingDDApp.g();
                }
            }).a().show();
        }
    };
    private ServiceConnection v = new ServiceConnection() {
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.shoujiduoduo.base.a.a.a("ServiceConnection", "ServiceConnection: onServiceConnected.");
            PlayerService unused = RingToneDuoduoActivity.this.f = ((PlayerService.b) iBinder).a();
            z.a().a(RingToneDuoduoActivity.this.f);
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "ServiceConnection: mPlayService = " + RingToneDuoduoActivity.this.f);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            com.shoujiduoduo.base.a.a.a("ServiceConnection", "ServiceConnection: onServiceDisconnected.");
        }
    };
    /* access modifiers changed from: private */
    @SuppressLint({"HandlerLeak"})
    public Handler w = new Handler() {
        public void handleMessage(Message message) {
            switch (message.what) {
                case Constants.CLEARIMGING:
                    new AlertDialog.Builder(RingToneDuoduoActivity.this).setTitle("提示").setMessage("目前检测您的网络连接可能有问题，请联系铃声多多客服解决一下，客服QQ：2262193174, errorContent:" + ((String) message.obj)).setPositiveButton((int) R.string.ok, (DialogInterface.OnClickListener) null).show();
                    return;
                case 1100:
                    RingToneDuoduoActivity.this.a((String) message.obj);
                    return;
                case 1101:
                    Uri fromFile = Uri.fromFile(new File((String) message.obj));
                    Intent intent = new Intent("android.intent.action.VIEW");
                    intent.setDataAndType(fromFile, "application/vnd.android.package-archive");
                    RingToneDuoduoActivity.this.startActivity(intent);
                    return;
                case 1102:
                    Toast.makeText(RingToneDuoduoActivity.this, (int) R.string.down_update_apk_error, 0).show();
                    return;
                case 1130:
                    if (RingToneDuoduoActivity.this.g != null) {
                        RingToneDuoduoActivity.this.g.cancel();
                    }
                    Toast.makeText(RingToneDuoduoActivity.this, (int) R.string.clean_cache_suc, 0).show();
                    return;
                case 1131:
                    Intent intent2 = new Intent(RingToneDuoduoActivity.this, SearchActivity.class);
                    intent2.putExtra("from", "push");
                    intent2.putExtra("key", (String) message.obj);
                    RingToneDuoduoActivity.this.startActivity(intent2);
                    return;
                case 1133:
                    String str = (String) message.obj;
                    Intent intent3 = new Intent(RingToneDuoduoActivity.this, WebViewActivity.class);
                    intent3.putExtra("url", str);
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "url:" + str);
                    RingToneDuoduoActivity.this.startActivity(intent3);
                    return;
                case 1134:
                    RingToneDuoduoActivity.this.g();
                    return;
                case 1135:
                    b bVar = (b) message.obj;
                    Intent intent4 = new Intent(RingToneDuoduoActivity.this, MusicAlbumActivity.class);
                    intent4.putExtra("url", bVar.f2039a);
                    intent4.putExtra("title", bVar.f2040b);
                    intent4.putExtra("type", MusicAlbumActivity.a.create_album);
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "title:" + bVar.f2040b + ", url:" + bVar.f2039a);
                    RingToneDuoduoActivity.this.startActivity(intent4);
                    return;
                default:
                    return;
            }
        }
    };

    public static RingToneDuoduoActivity a() {
        return n;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ad.b(android.content.Context, java.lang.String, int):boolean
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ad.b(android.content.Context, java.lang.String, long):boolean
      com.shoujiduoduo.util.ad.b(android.content.Context, java.lang.String, java.lang.String):boolean
      com.shoujiduoduo.util.ad.b(android.content.Context, java.lang.String, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, int):int
     arg types: [com.shoujiduoduo.ringtone.activity.RingToneDuoduoActivity, java.lang.String, int]
     candidates:
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, long):long
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.shoujiduoduo.util.ad.a(android.content.Context, java.lang.String, int):int */
    public void onCreate(Bundle bundle) {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate");
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_main);
        com.jaeger.library.a.a(this, i.a(R.color.bkg_green), 0);
        n = this;
        c.a();
        l.a().a(this.w);
        m.a(getApplicationContext());
        ac.a(getApplicationContext());
        com.shoujiduoduo.util.c.a.a(getApplicationContext());
        ad.b((Context) this, k, ad.a((Context) this, k, 0) + 1);
        b();
        this.p = com.shoujiduoduo.util.a.f();
        if (this.p) {
            com.shoujiduoduo.a.b.b.c().h();
        }
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate 1");
        setVolumeControlStream(3);
        Intent intent = new Intent(this, PlayerService.class);
        startService(intent);
        bindService(intent, this.v, 1);
        this.m = true;
        c();
        this.w.sendEmptyMessageDelayed(1134, 2000);
        this.q = new com.shoujiduoduo.ui.settings.a(this, R.style.DuoDuoDialog);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.r);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.t);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.u);
        com.shoujiduoduo.a.a.c.a().a(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.s);
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onCreate:end");
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_MusicAlbum:
                com.umeng.analytics.b.b(this, "CLICK_MUSIC_ALBUM");
                Intent intent = new Intent(this, MusicAlbumActivity.class);
                intent.putExtra("type", MusicAlbumActivity.a.my_album);
                intent.putExtra("title", "音乐相册");
                startActivity(intent);
                return;
            case R.id.tv_record:
                if (this.f != null && this.f.l()) {
                    this.f.m();
                }
                Intent intent2 = new Intent();
                intent2.setClass(this, MakeRingActivity.class);
                intent2.setFlags(NTLMConstants.FLAG_NEGOTIATE_128_BIT_ENCRYPTION);
                startActivity(intent2);
                return;
            default:
                return;
        }
    }

    private void b() {
        this.f2018b = (TextView) findViewById(R.id.header_text);
        this.d = (RelativeLayout) findViewById(R.id.header);
        this.e = (RelativeLayout) findViewById(R.id.search_layout);
        this.f2017a = (MyRadioButton) findViewById(R.id.buttonMyRingtone);
        if (com.shoujiduoduo.a.b.b.g().h() && com.shoujiduoduo.a.b.b.g().g()) {
            this.f2017a.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, i.b(R.drawable.btn_navi_myring_vip), (Drawable) null, (Drawable) null);
        }
        f();
        MyRadioButton myRadioButton = (MyRadioButton) findViewById(R.id.buttonMoreOptions);
        myRadioButton.setCompoundDrawablesWithIntrinsicBounds((Drawable) null, this.c, (Drawable) null, (Drawable) null);
        myRadioButton.setText(e());
        this.e.findViewById(R.id.tv_record).setOnClickListener(this);
        this.e.findViewById(R.id.tv_MusicAlbum).setOnClickListener(this);
        ((LinearLayout) this.e.findViewById(R.id.search_entrance)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                RingToneDuoduoActivity.this.startActivity(new Intent(RingToneDuoduoActivity.this, SearchActivity.class));
            }
        });
        this.i.add(new HomepageFrag());
        this.i.add(new CategoryFrag());
        this.i.add(new MyRingtoneFrag());
        if (com.shoujiduoduo.util.a.b()) {
            this.i.add(new AdWallFrag());
        } else if (com.shoujiduoduo.util.a.c()) {
            this.i.add(new AiChangFrag());
        } else {
            this.i.add(new MoreOptionFrag());
        }
        this.h = (MyViewPager) findViewById(R.id.vPager);
        this.h.setOffscreenPageLimit(4);
        this.h.setCanScroll(true);
        this.h.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        this.j = (RadioGroup) findViewById(R.id.navi_group);
        this.j.setOnCheckedChangeListener(this);
        this.j.check(R.id.buttonHomepage);
        this.h.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrolled(int i, float f, int i2) {
            }

            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        RingToneDuoduoActivity.this.j.check(R.id.buttonHomepage);
                        return;
                    case 1:
                        RingToneDuoduoActivity.this.j.check(R.id.buttonCategory);
                        return;
                    case 2:
                        RingToneDuoduoActivity.this.j.check(R.id.buttonMyRingtone);
                        return;
                    case 3:
                        RingToneDuoduoActivity.this.j.check(R.id.buttonMoreOptions);
                        return;
                    default:
                        return;
                }
            }

            public void onPageScrollStateChanged(int i) {
            }
        });
    }

    public class FragmentAdapter extends FragmentPagerAdapter {
        FragmentAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int i) {
            return (Fragment) RingToneDuoduoActivity.this.i.get(i);
        }

        public int getCount() {
            return RingToneDuoduoActivity.this.i.size();
        }
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i2) {
        switch (i2) {
            case R.id.buttonHomepage:
                this.h.setCurrentItem(0, false);
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                this.f2018b.setVisibility(4);
                return;
            case R.id.buttonCategory:
                this.h.setCurrentItem(1, false);
                this.d.setVisibility(0);
                this.e.setVisibility(0);
                this.f2018b.setVisibility(4);
                return;
            case R.id.buttonMyRingtone:
                this.h.setCurrentItem(2, false);
                this.d.setVisibility(0);
                this.e.setVisibility(4);
                this.f2018b.setVisibility(0);
                this.f2018b.setText("我的");
                return;
            case R.id.buttonMoreOptions:
                this.h.setCurrentItem(3, false);
                if (com.shoujiduoduo.util.a.b()) {
                    this.d.setVisibility(8);
                    return;
                } else if (com.shoujiduoduo.util.a.c()) {
                    this.d.setVisibility(8);
                    return;
                } else {
                    this.d.setVisibility(0);
                    this.e.setVisibility(4);
                    this.f2018b.setVisibility(0);
                    this.f2018b.setText("更多");
                    return;
                }
            default:
                return;
        }
    }

    private void c() {
        com.shoujiduoduo.a.a.c.a().b(new c.b() {
            public void a() {
                Intent intent = RingToneDuoduoActivity.this.getIntent();
                if (intent != null) {
                    String stringExtra = intent.getStringExtra("action");
                    com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "action = " + stringExtra);
                    if (stringExtra == null) {
                        return;
                    }
                    if (stringExtra.equalsIgnoreCase("search")) {
                        final String stringExtra2 = intent.getStringExtra("para");
                        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "push task: search, keyword = " + stringExtra2);
                        if (stringExtra2 != null) {
                            new Thread() {
                                public void run() {
                                    try {
                                        RingToneDuoduoActivity.this.w.sendMessageDelayed(RingToneDuoduoActivity.this.w.obtainMessage(1131, stringExtra2), 1000);
                                    } catch (Exception e) {
                                    }
                                }
                            }.start();
                        }
                    } else if (stringExtra.equalsIgnoreCase(cn.banshenggua.aichang.player.PlayerService.ACTION_PLAY)) {
                        final String stringExtra3 = intent.getStringExtra("para");
                        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "push task: play, keyword = " + stringExtra3);
                        if (stringExtra3 != null) {
                            new Thread() {
                                public void run() {
                                    try {
                                        RingToneDuoduoActivity.this.w.sendMessageDelayed(RingToneDuoduoActivity.this.w.obtainMessage(1132, stringExtra3), 1000);
                                    } catch (Exception e) {
                                    }
                                }
                            }.start();
                        }
                    } else if (stringExtra.equalsIgnoreCase("webview")) {
                        RingToneDuoduoActivity.this.w.sendMessageDelayed(RingToneDuoduoActivity.this.w.obtainMessage(1133, intent.getStringExtra("para")), 1000);
                    } else if (stringExtra.equalsIgnoreCase("music_album")) {
                        b bVar = new b();
                        bVar.f2039a = intent.getStringExtra("para");
                        bVar.f2040b = intent.getStringExtra("title");
                        RingToneDuoduoActivity.this.w.sendMessageDelayed(RingToneDuoduoActivity.this.w.obtainMessage(1135, bVar), 1000);
                    } else if (!stringExtra.equalsIgnoreCase("ad") && !stringExtra.equalsIgnoreCase("update")) {
                        com.shoujiduoduo.base.a.a.c("RingToneDuoduoActivity", "not support action");
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        UMShareAPI.get(this).onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onStart");
        super.onStart();
        this.l = new a();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("install_apk_from_start_ad");
        registerReceiver(this.l, intentFilter);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onStop");
        unregisterReceiver(this.l);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onDestroy");
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_VIP, this.t);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_USER_CENTER, this.s);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_APP, this.u);
        com.shoujiduoduo.a.a.c.a().b(com.shoujiduoduo.a.a.b.OBSERVER_CONFIG, this.r);
        if (com.shoujiduoduo.util.c.c.a() != null) {
            com.shoujiduoduo.util.c.c.a().b();
        }
        if (z.a() != null) {
            z.a().c();
        }
        ac.a(this).b();
        if (this.w != null) {
            this.w.removeCallbacksAndMessages(null);
        }
        if (com.shoujiduoduo.util.c.b.a().e() && ab.a().b("cm_sunshine_sdk_enable")) {
            try {
                InitCmmInterface.exitApp(this);
            } catch (Throwable th) {
            }
        }
        if (this == n) {
            n = null;
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.m) {
            unbindService(this.v);
            this.m = false;
        }
        this.v = null;
        stopService(new Intent(this, PlayerService.class));
        z.a().a(null);
        this.f = null;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onResume");
        super.onResume();
        PushManager.getInstance().initialize(getApplicationContext(), GTPushService.class);
        PushManager.getInstance().registerPushIntentService(getApplicationContext(), DuoDuoIntentService.class);
        PushManager.getInstance().setSilentTime(getApplicationContext(), 22, 9);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onRestart");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "RingToneDuoduoActivity:onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        c();
    }

    private class a extends BroadcastReceiver {
        private a() {
        }

        public void onReceive(Context context, Intent intent) {
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "receive broadcast");
            if (intent.getAction().equals("install_apk_from_start_ad")) {
                final String stringExtra = intent.getStringExtra("PackagePath");
                String stringExtra2 = intent.getStringExtra("PackageName");
                if (!TextUtils.isEmpty(stringExtra2) && !TextUtils.isEmpty(stringExtra)) {
                    new b.a(RingToneDuoduoActivity.this).b((int) R.string.hint).a(stringExtra2 + RingToneDuoduoActivity.this.getResources().getString(R.string.start_ad_down_apk_hint)).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            f.b(stringExtra);
                            dialogInterface.dismiss();
                        }
                    }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).a().show();
                }
            }
        }
    }

    private String e() {
        if (com.shoujiduoduo.util.a.b()) {
            return ab.a().a("navi_ad_title");
        }
        if (com.shoujiduoduo.util.a.c()) {
            return ab.a().a("aichang_title");
        }
        return "更多";
    }

    private void f() {
        if (com.shoujiduoduo.util.a.b()) {
            if (com.shoujiduoduo.util.a.a().equals("shop")) {
                this.c = getResources().getDrawable(R.drawable.btn_navi_shop);
            } else if (com.shoujiduoduo.util.a.a().equals("news")) {
                this.c = getResources().getDrawable(R.drawable.btn_navi_discover);
            } else if (com.shoujiduoduo.util.a.a().equals("video")) {
                this.c = getResources().getDrawable(R.drawable.btn_navi_aichang);
            } else {
                this.c = getResources().getDrawable(R.drawable.btn_navi_shop);
            }
        } else if (com.shoujiduoduo.util.a.c()) {
            this.c = getResources().getDrawable(R.drawable.btn_navi_aichang);
        } else {
            this.c = getResources().getDrawable(R.drawable.btn_navi_more);
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        this.q.show();
        return true;
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        public String f2039a;

        /* renamed from: b  reason: collision with root package name */
        public String f2040b;

        private b() {
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (str != null && str.length() > 0) {
            new b.a(this).b((int) R.string.update_hint).a((int) R.string.has_update_hint).a((int) R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    d.a("新版本已开始下载");
                    new aj(RingToneDuoduoActivity.this, ab.a().a("update_url")).execute(new Void[0]);
                }
            }).b((int) R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            }).a().show();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        String a2;
        if (!o) {
            o = true;
            String a3 = ab.a().a("update_version");
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: update version: " + a3);
            com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: cur version: " + f.q());
            if (a3.compareToIgnoreCase(f.q()) > 0 && (a2 = ab.a().a("update_url")) != null && a2.length() > 0) {
                Message obtainMessage = this.w.obtainMessage(1100, a2);
                com.shoujiduoduo.base.a.a.a("RingToneDuoduoActivity", "onConfigListener: update url: " + a2);
                this.w.sendMessage(obtainMessage);
            }
        }
    }
}
