package com.xiaomi.push.log;

import com.xiaomi.channel.commonutils.logger.LoggerInterface;

public class e implements LoggerInterface {

    /* renamed from: a  reason: collision with root package name */
    private LoggerInterface f4033a = null;

    /* renamed from: b  reason: collision with root package name */
    private LoggerInterface f4034b = null;

    public e(LoggerInterface loggerInterface, LoggerInterface loggerInterface2) {
        this.f4033a = loggerInterface;
        this.f4034b = loggerInterface2;
    }

    public void log(String str) {
        if (this.f4033a != null) {
            this.f4033a.log(str);
        }
        if (this.f4034b != null) {
            this.f4034b.log(str);
        }
    }

    public void log(String str, Throwable th) {
        if (this.f4033a != null) {
            this.f4033a.log(str, th);
        }
        if (this.f4034b != null) {
            this.f4034b.log(str, th);
        }
    }
}
