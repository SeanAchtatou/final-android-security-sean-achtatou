package com.xiaomi.push.service;

public abstract class z {
    public static String A = "ext_notify_type";
    public static String B = "ext_session";
    public static String C = "sig";

    /* renamed from: a  reason: collision with root package name */
    public static String f4062a = "1";

    /* renamed from: b  reason: collision with root package name */
    public static String f4063b = "2";
    public static String c = "3";
    public static String d = "com.xiaomi.push.OPEN_CHANNEL";
    public static String e = "com.xiaomi.push.SEND_MESSAGE";
    public static String f = "com.xiaomi.push.SEND_IQ";
    public static String g = "com.xiaomi.push.BATCH_SEND_MESSAGE";
    public static String h = "com.xiaomi.push.SEND_PRES";
    public static String i = "com.xiaomi.push.CLOSE_CHANNEL";
    public static String j = "com.xiaomi.push.FORCE_RECONN";
    public static String k = "com.xiaomi.push.RESET_CONN";
    public static String l = "com.xiaomi.push.UPDATE_CHANNEL_INFO";
    public static String m = "com.xiaomi.push.SEND_STATS";
    public static String n = "com.xiaomi.push.CHANGE_HOST";
    public static String o = "com.xiaomi.push.PING_TIMER";
    public static String p = "ext_user_id";
    public static String q = "ext_chid";
    public static String r = "ext_sid";
    public static String s = "ext_token";
    public static String t = "ext_auth_method";
    public static String u = "ext_security";
    public static String v = "ext_kick";
    public static String w = "ext_client_attr";
    public static String x = "ext_cloud_attr";
    public static String y = "ext_pkg_name";
    public static String z = "ext_notify_id";
}
