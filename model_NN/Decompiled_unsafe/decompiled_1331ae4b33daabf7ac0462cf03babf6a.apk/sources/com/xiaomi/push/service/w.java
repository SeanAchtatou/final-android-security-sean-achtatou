package com.xiaomi.push.service;

import android.util.Pair;
import com.xiaomi.channel.commonutils.misc.b;
import com.xiaomi.xmpush.thrift.c;
import com.xiaomi.xmpush.thrift.d;
import com.xiaomi.xmpush.thrift.e;
import com.xiaomi.xmpush.thrift.g;
import com.xiaomi.xmpush.thrift.p;
import com.xiaomi.xmpush.thrift.q;
import java.util.ArrayList;
import java.util.List;

public class w {
    public static int a(v vVar, c cVar) {
        int i = 0;
        String a2 = a(cVar);
        switch (cVar) {
            case MISC_CONFIG:
                i = 1;
                break;
        }
        return vVar.f4059a.getInt(a2, i);
    }

    private static String a(c cVar) {
        return "oc_version_" + cVar.a();
    }

    private static List<Pair<Integer, Object>> a(List<g> list, boolean z) {
        Pair pair;
        if (b.a(list)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (g next : list) {
            int a2 = next.a();
            d a3 = d.a(next.c());
            if (a3 != null) {
                if (!z || !next.c) {
                    switch (a3) {
                        case INT:
                            pair = new Pair(Integer.valueOf(a2), Integer.valueOf(next.f()));
                            break;
                        case LONG:
                            pair = new Pair(Integer.valueOf(a2), Long.valueOf(next.h()));
                            break;
                        case STRING:
                            pair = new Pair(Integer.valueOf(a2), next.j());
                            break;
                        case BOOLEAN:
                            pair = new Pair(Integer.valueOf(a2), Boolean.valueOf(next.l()));
                            break;
                        default:
                            pair = null;
                            break;
                    }
                    arrayList.add(pair);
                } else {
                    arrayList.add(new Pair(Integer.valueOf(a2), null));
                }
            }
        }
        return arrayList;
    }

    public static void a(v vVar, c cVar, int i) {
        vVar.f4059a.edit().putInt(a(cVar), i).commit();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.w.a(java.util.List<com.xiaomi.xmpush.thrift.g>, boolean):java.util.List<android.util.Pair<java.lang.Integer, java.lang.Object>>
     arg types: [java.util.List<com.xiaomi.xmpush.thrift.g>, int]
     candidates:
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.c):int
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.p):void
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.q):void
      com.xiaomi.push.service.w.a(java.util.List<com.xiaomi.xmpush.thrift.g>, boolean):java.util.List<android.util.Pair<java.lang.Integer, java.lang.Object>> */
    public static void a(v vVar, p pVar) {
        vVar.b(a(pVar.a(), true));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.xiaomi.push.service.w.a(java.util.List<com.xiaomi.xmpush.thrift.g>, boolean):java.util.List<android.util.Pair<java.lang.Integer, java.lang.Object>>
     arg types: [java.util.List<com.xiaomi.xmpush.thrift.g>, int]
     candidates:
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.c):int
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.p):void
      com.xiaomi.push.service.w.a(com.xiaomi.push.service.v, com.xiaomi.xmpush.thrift.q):void
      com.xiaomi.push.service.w.a(java.util.List<com.xiaomi.xmpush.thrift.g>, boolean):java.util.List<android.util.Pair<java.lang.Integer, java.lang.Object>> */
    public static void a(v vVar, q qVar) {
        for (e next : qVar.a()) {
            if (next.a() > a(vVar, next.d())) {
                a(vVar, next.d(), next.a());
                vVar.a(a(next.f4085b, false));
            }
        }
    }
}
