package com.xiaomi.push.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

public class ab {

    /* renamed from: a  reason: collision with root package name */
    private static ab f4038a;

    /* renamed from: b  reason: collision with root package name */
    private Context f4039b;
    private int c = 0;

    private ab(Context context) {
        this.f4039b = context.getApplicationContext();
    }

    public static ab a(Context context) {
        if (f4038a == null) {
            f4038a = new ab(context);
        }
        return f4038a;
    }

    @SuppressLint({"NewApi"})
    public int b() {
        if (this.c != 0) {
            return this.c;
        }
        if (Build.VERSION.SDK_INT >= 17) {
            this.c = Settings.Global.getInt(this.f4039b.getContentResolver(), "device_provisioned", 0);
            return this.c;
        }
        this.c = Settings.Secure.getInt(this.f4039b.getContentResolver(), "device_provisioned", 0);
        return this.c;
    }

    @SuppressLint({"NewApi"})
    public Uri c() {
        return Build.VERSION.SDK_INT >= 17 ? Settings.Global.getUriFor("device_provisioned") : Settings.Secure.getUriFor("device_provisioned");
    }
}
