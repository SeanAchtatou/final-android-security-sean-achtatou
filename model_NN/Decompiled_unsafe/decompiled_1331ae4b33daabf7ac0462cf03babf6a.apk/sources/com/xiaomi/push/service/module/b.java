package com.xiaomi.push.service.module;

import android.content.Context;
import android.text.TextUtils;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f4044a;

    /* renamed from: b  reason: collision with root package name */
    private String f4045b;
    private String c;
    private int d = 0;
    private ClassLoader e;
    private Object f;

    public b(String str, String str2, ClassLoader classLoader, String str3, int i) {
        this.f4044a = str;
        this.f4045b = str2;
        this.e = classLoader;
        this.d = i;
        this.c = str3;
        try {
            if (!TextUtils.isEmpty(str3)) {
                this.f = this.e.loadClass(str3).newInstance();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public ClassLoader a() {
        return this.e;
    }

    public void a(Context context) {
        if (this.f != null) {
            try {
                this.e.loadClass(this.c).getMethod("onCreate", Context.class, String.class).invoke(this.f, context, this.f4045b);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
