package com.xiaomi.push.service;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.channel.commonutils.reflect.a;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static String f4040a = null;

    /* renamed from: b  reason: collision with root package name */
    private static String f4041b = null;
    private static String c = null;

    public static String a() {
        if (Build.VERSION.SDK_INT > 8) {
            return Build.SERIAL;
        }
        return null;
    }

    @TargetApi(17)
    public static int b() {
        Object a2;
        if (Build.VERSION.SDK_INT >= 17 && (a2 = a.a("android.os.UserHandle", "myUserId", new Object[0])) != null) {
            return Integer.class.cast(a2).intValue();
        }
        return -1;
    }

    public static String b(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), "android_id");
        } catch (Throwable th) {
            b.a(th);
            return null;
        }
    }

    public static String c(Context context) {
        if (f4040a != null) {
            return f4040a;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String deviceId = telephonyManager.getDeviceId();
            int i = 10;
            while (deviceId == null) {
                int i2 = i - 1;
                if (i <= 0) {
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                }
                deviceId = telephonyManager.getDeviceId();
                i = i2;
            }
            if (deviceId != null) {
                f4040a = deviceId;
            }
            return deviceId;
        } catch (Throwable th) {
            b.a(th);
            return null;
        }
    }

    public static String d(Context context) {
        if (f4040a != null) {
            return f4040a;
        }
        try {
            String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            if (deviceId == null) {
                return deviceId;
            }
            f4040a = deviceId;
            return deviceId;
        } catch (Throwable th) {
            b.a(th);
            return null;
        }
    }

    public static synchronized String e(Context context) {
        String str;
        synchronized (d.class) {
            if (c != null) {
                str = c;
            } else {
                String b2 = b(context);
                c = com.xiaomi.channel.commonutils.string.d.b(b2 + a());
                str = c;
            }
        }
        return str;
    }
}
