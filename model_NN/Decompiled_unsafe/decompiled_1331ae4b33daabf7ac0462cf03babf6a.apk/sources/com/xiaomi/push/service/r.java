package com.xiaomi.push.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.RemoteViews;
import com.sina.weibo.sdk.constant.WBConstants;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.channel.commonutils.reflect.a;
import com.xiaomi.xmpush.thrift.i;
import com.xiaomi.xmpush.thrift.o;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class r {

    /* renamed from: a  reason: collision with root package name */
    public static long f4054a = 0;

    /* renamed from: b  reason: collision with root package name */
    private static LinkedList<Pair<Integer, String>> f4055b = new LinkedList<>();

    private static int a(Context context, String str, String str2) {
        if (str.equals(context.getPackageName())) {
            return context.getResources().getIdentifier(str2, "drawable", str);
        }
        return 0;
    }

    private static Notification a(Notification notification, String str) {
        try {
            Field declaredField = Notification.class.getDeclaredField("extraNotification");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(notification);
            Method declaredMethod = obj.getClass().getDeclaredMethod("setTargetPkg", CharSequence.class);
            declaredMethod.setAccessible(true);
            declaredMethod.invoke(obj, str);
        } catch (Exception e) {
            b.a(e);
        }
        return notification;
    }

    @SuppressLint({"NewApi"})
    private static Notification a(Context context, o oVar, byte[] bArr, RemoteViews remoteViews, PendingIntent pendingIntent) {
        Object a2;
        i m = oVar.m();
        Notification.Builder builder = new Notification.Builder(context);
        String[] a3 = a(context, m);
        builder.setContentTitle(a3[0]);
        builder.setContentText(a3[1]);
        if (remoteViews != null) {
            builder.setContent(remoteViews);
        } else if (Build.VERSION.SDK_INT >= 16) {
            builder.setStyle(new Notification.BigTextStyle().bigText(a3[1]));
        }
        builder.setWhen(System.currentTimeMillis());
        builder.setContentIntent(pendingIntent);
        Map<String, String> s = m.s();
        int i = 0;
        boolean z = s != null && s.containsKey("_wbxr") && "com.xiaomi.xmsf".equals(a(oVar)) && (i = a(context, a(oVar), "wbxr")) > 0;
        if (z) {
            builder.setSmallIcon(i);
            builder.setLargeIcon(a(context, i));
        } else {
            int a4 = a(context, a(oVar), "mipush_notification");
            int a5 = a(context, a(oVar), "mipush_small_notification");
            if (a4 <= 0 || a5 <= 0) {
                builder.setSmallIcon(f(context, a(oVar)));
            } else {
                builder.setLargeIcon(a(context, a4));
                builder.setSmallIcon(a5);
            }
        }
        builder.setAutoCancel(true);
        long currentTimeMillis = System.currentTimeMillis();
        if (s != null && s.containsKey("ticker")) {
            builder.setTicker(s.get("ticker"));
        }
        if (currentTimeMillis - f4054a > 10000) {
            f4054a = currentTimeMillis;
            int c = e(context, a(oVar)) ? c(context, a(oVar)) : m.f;
            builder.setDefaults(c);
            if (!(s == null || (c & 1) == 0)) {
                String str = s.get("sound_uri");
                if (!TextUtils.isEmpty(str) && str.startsWith("android.resource://" + a(oVar))) {
                    builder.setDefaults(c ^ 1);
                    builder.setSound(Uri.parse(str));
                }
            }
        }
        Notification notification = builder.getNotification();
        if (z && (a2 = a.a(notification, "extraNotification")) != null) {
            a.a(a2, "setCustomizedIcon", true);
        }
        return notification;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private static PendingIntent a(Context context, o oVar, i iVar, byte[] bArr) {
        if (iVar != null && !TextUtils.isEmpty(iVar.g)) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(iVar.g));
            intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            return PendingIntent.getActivity(context, 0, intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
        } else if (b(oVar)) {
            Intent intent2 = new Intent();
            intent2.setComponent(new ComponentName("com.xiaomi.xmsf", "com.xiaomi.mipush.sdk.PushMessageHandler"));
            intent2.putExtra("mipush_payload", bArr);
            intent2.putExtra("mipush_notified", true);
            intent2.addCategory(String.valueOf(iVar.q()));
            return PendingIntent.getService(context, 0, intent2, NTLMConstants.FLAG_UNIDENTIFIED_10);
        } else {
            Intent intent3 = new Intent("com.xiaomi.mipush.RECEIVE_MESSAGE");
            intent3.setComponent(new ComponentName(oVar.f, "com.xiaomi.mipush.sdk.PushMessageHandler"));
            intent3.putExtra("mipush_payload", bArr);
            intent3.putExtra("mipush_notified", true);
            intent3.addCategory(String.valueOf(iVar.q()));
            return PendingIntent.getService(context, 0, intent3, NTLMConstants.FLAG_UNIDENTIFIED_10);
        }
    }

    private static Bitmap a(Context context, int i) {
        return a(context.getResources().getDrawable(i));
    }

    public static Bitmap a(Drawable drawable) {
        int i = 1;
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        if (intrinsicWidth <= 0) {
            intrinsicWidth = 1;
        }
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicHeight > 0) {
            i = intrinsicHeight;
        }
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, i, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return createBitmap;
    }

    static String a(o oVar) {
        i m;
        if (!(!"com.xiaomi.xmsf".equals(oVar.f) || (m = oVar.m()) == null || m.s() == null)) {
            String str = m.s().get("miui_package_name");
            if (!TextUtils.isEmpty(str)) {
                return str;
            }
        }
        return oVar.f;
    }

    public static void a(Context context, o oVar, byte[] bArr) {
        Notification notification;
        int i;
        NotificationManager notificationManager = (NotificationManager) context.getSystemService("notification");
        i m = oVar.m();
        RemoteViews b2 = b(context, oVar, bArr);
        PendingIntent a2 = a(context, oVar, m, bArr);
        if (a2 == null) {
            b.a("The click PendingIntent is null. ");
            return;
        }
        if (Build.VERSION.SDK_INT >= 11) {
            notification = a(context, oVar, bArr, b2, a2);
        } else {
            Notification notification2 = new Notification(f(context, a(oVar)), null, System.currentTimeMillis());
            String[] a3 = a(context, m);
            try {
                notification2.getClass().getMethod("setLatestEventInfo", Context.class, CharSequence.class, CharSequence.class, PendingIntent.class).invoke(notification2, context, a3[0], a3[1], a2);
            } catch (NoSuchMethodException e) {
                b.a(e);
            } catch (IllegalAccessException e2) {
                b.a(e2);
            } catch (IllegalArgumentException e3) {
                b.a(e3);
            } catch (InvocationTargetException e4) {
                b.a(e4);
            }
            Map<String, String> s = m.s();
            if (s != null && s.containsKey("ticker")) {
                notification2.tickerText = s.get("ticker");
            }
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - f4054a > 10000) {
                f4054a = currentTimeMillis;
                int i2 = m.f;
                if (e(context, a(oVar))) {
                    i = c(context, a(oVar));
                } else {
                    i = i2;
                }
                notification2.defaults = i;
                if (!(s == null || (i & 1) == 0)) {
                    String str = s.get("sound_uri");
                    if (!TextUtils.isEmpty(str) && str.startsWith("android.resource://" + a(oVar))) {
                        notification2.defaults = i ^ 1;
                        notification2.sound = Uri.parse(str);
                    }
                }
            }
            notification2.flags |= 16;
            if (b2 != null) {
                notification2.contentView = b2;
            }
            notification = notification2;
        }
        if ("com.xiaomi.xmsf".equals(context.getPackageName())) {
            a(notification, a(oVar));
        }
        int q = m.q() + ((a(oVar).hashCode() / 10) * 10);
        notificationManager.notify(q, notification);
        Pair pair = new Pair(Integer.valueOf(q), a(oVar));
        synchronized (f4055b) {
            f4055b.add(pair);
            if (f4055b.size() > 100) {
                f4055b.remove();
            }
        }
    }

    public static boolean a(Map<String, String> map) {
        if (map == null || !map.containsKey("notify_foreground")) {
            return true;
        }
        return "1".equals(map.get("notify_foreground"));
    }

    private static String[] a(Context context, i iVar) {
        String h = iVar.h();
        String j = iVar.j();
        Map<String, String> s = iVar.s();
        if (s != null) {
            int intValue = Float.valueOf((((float) context.getResources().getDisplayMetrics().widthPixels) / context.getResources().getDisplayMetrics().density) + 0.5f).intValue();
            if (intValue <= 320) {
                String str = s.get("title_short");
                if (!TextUtils.isEmpty(str)) {
                    h = str;
                }
                String str2 = s.get("description_short");
                if (TextUtils.isEmpty(str2)) {
                    str2 = j;
                }
                j = str2;
            } else if (intValue > 360) {
                String str3 = s.get("title_long");
                if (!TextUtils.isEmpty(str3)) {
                    h = str3;
                }
                String str4 = s.get("description_long");
                if (!TextUtils.isEmpty(str4)) {
                    j = str4;
                }
            }
        }
        return new String[]{h, j};
    }

    private static RemoteViews b(Context context, o oVar, byte[] bArr) {
        i m = oVar.m();
        String a2 = a(oVar);
        Map<String, String> s = m.s();
        if (s == null) {
            return null;
        }
        String str = s.get("layout_name");
        String str2 = s.get("layout_value");
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return null;
        }
        try {
            Resources resourcesForApplication = context.getPackageManager().getResourcesForApplication(a2);
            int identifier = resourcesForApplication.getIdentifier(str, "layout", a2);
            if (identifier == 0) {
                return null;
            }
            RemoteViews remoteViews = new RemoteViews(a2, identifier);
            try {
                JSONObject jSONObject = new JSONObject(str2);
                if (jSONObject.has("text")) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject("text");
                    Iterator<String> keys = jSONObject2.keys();
                    while (keys.hasNext()) {
                        String next = keys.next();
                        String string = jSONObject2.getString(next);
                        int identifier2 = resourcesForApplication.getIdentifier(next, "id", a2);
                        if (identifier2 > 0) {
                            remoteViews.setTextViewText(identifier2, string);
                        }
                    }
                }
                if (jSONObject.has(WBConstants.GAME_PARAMS_GAME_IMAGE_URL)) {
                    JSONObject jSONObject3 = jSONObject.getJSONObject(WBConstants.GAME_PARAMS_GAME_IMAGE_URL);
                    Iterator<String> keys2 = jSONObject3.keys();
                    while (keys2.hasNext()) {
                        String next2 = keys2.next();
                        String string2 = jSONObject3.getString(next2);
                        int identifier3 = resourcesForApplication.getIdentifier(next2, "id", a2);
                        int identifier4 = resourcesForApplication.getIdentifier(string2, "drawable", a2);
                        if (identifier3 > 0) {
                            remoteViews.setImageViewResource(identifier3, identifier4);
                        }
                    }
                }
                if (jSONObject.has("time")) {
                    JSONObject jSONObject4 = jSONObject.getJSONObject("time");
                    Iterator<String> keys3 = jSONObject4.keys();
                    while (keys3.hasNext()) {
                        String next3 = keys3.next();
                        String string3 = jSONObject4.getString(next3);
                        if (string3.length() == 0) {
                            string3 = "yy-MM-dd hh:mm";
                        }
                        int identifier5 = resourcesForApplication.getIdentifier(next3, "id", a2);
                        if (identifier5 > 0) {
                            remoteViews.setTextViewText(identifier5, new SimpleDateFormat(string3).format(new Date(System.currentTimeMillis())));
                        }
                    }
                }
                return remoteViews;
            } catch (JSONException e) {
                b.a(e);
                return null;
            }
        } catch (PackageManager.NameNotFoundException e2) {
            b.a(e2);
            return null;
        }
    }

    public static boolean b(o oVar) {
        i m = oVar.m();
        return m != null && m.v();
    }

    static int c(Context context, String str) {
        return context.getSharedPreferences("pref_notify_type", 0).getInt(str, Integer.MAX_VALUE);
    }

    static boolean e(Context context, String str) {
        return context.getSharedPreferences("pref_notify_type", 0).contains(str);
    }

    private static int f(Context context, String str) {
        int a2 = a(context, str, "mipush_notification");
        int a3 = a(context, str, "mipush_small_notification");
        if (a2 <= 0) {
            a2 = a3 > 0 ? a3 : context.getApplicationInfo().icon;
        }
        return (a2 != 0 || Build.VERSION.SDK_INT < 9) ? a2 : context.getApplicationInfo().logo;
    }
}
