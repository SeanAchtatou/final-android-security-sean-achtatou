package com.xiaomi.push.service.receivers;

import android.content.BroadcastReceiver;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NetworkStatusReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static int f4056a = 1;

    /* renamed from: b  reason: collision with root package name */
    private static int f4057b = 1;
    private static int c = 2;
    private static BlockingQueue<Runnable> d = new LinkedBlockingQueue();
    private static ThreadPoolExecutor e = new ThreadPoolExecutor(f4056a, f4057b, (long) c, TimeUnit.SECONDS, d);
}
