package com.xiaomi.xmpush.thrift;

import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.g;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class j implements Serializable, Cloneable, b<j, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> f;
    private static final k g = new k("Target");
    private static final c h = new c("channelId", (byte) 10, 1);
    private static final c i = new c(Parameters.SESSION_USER_ID, (byte) 11, 2);
    private static final c j = new c("server", (byte) 11, 3);
    private static final c k = new c("resource", (byte) 11, 4);
    private static final c l = new c("isPreview", (byte) 2, 5);

    /* renamed from: a  reason: collision with root package name */
    public long f4102a = 5;

    /* renamed from: b  reason: collision with root package name */
    public String f4103b;
    public String c = "xiaomi.com";
    public String d = "";
    public boolean e = false;
    private BitSet m = new BitSet(2);

    public enum a {
        CHANNEL_ID(1, "channelId"),
        USER_ID(2, Parameters.SESSION_USER_ID),
        SERVER(3, "server"),
        RESOURCE(4, "resource"),
        IS_PREVIEW(5, "isPreview");
        
        private static final Map<String, a> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                f.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public String a() {
            return this.h;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.j$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.CHANNEL_ID, (Object) new org.apache.thrift.meta_data.b("channelId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.USER_ID, (Object) new org.apache.thrift.meta_data.b(Parameters.SESSION_USER_ID, (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.SERVER, (Object) new org.apache.thrift.meta_data.b("server", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.RESOURCE, (Object) new org.apache.thrift.meta_data.b("resource", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.IS_PREVIEW, (Object) new org.apache.thrift.meta_data.b("isPreview", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        f = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(j.class, f);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                if (!a()) {
                    throw new g("Required field 'channelId' was not found in serialized data! Struct: " + toString());
                }
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 10) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4102a = fVar.u();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4103b = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.f4226b != 2) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.e = fVar.q();
                        b(true);
                        break;
                    }
                default:
                    i.a(fVar, i2.f4226b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.m.set(0, z);
    }

    public boolean a() {
        return this.m.get(0);
    }

    public void b(f fVar) {
        f();
        fVar.a(g);
        fVar.a(h);
        fVar.a(this.f4102a);
        fVar.b();
        if (this.f4103b != null) {
            fVar.a(i);
            fVar.a(this.f4103b);
            fVar.b();
        }
        if (this.c != null && c()) {
            fVar.a(j);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && d()) {
            fVar.a(k);
            fVar.a(this.d);
            fVar.b();
        }
        if (e()) {
            fVar.a(l);
            fVar.a(this.e);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.m.set(1, z);
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean d() {
        return this.d != null;
    }

    public boolean e() {
        return this.m.get(1);
    }

    public void f() {
        if (this.f4103b == null) {
            throw new g("Required field 'userId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Target(");
        sb.append("channelId:");
        sb.append(this.f4102a);
        sb.append(", ");
        sb.append("userId:");
        if (this.f4103b == null) {
            sb.append("null");
        } else {
            sb.append(this.f4103b);
        }
        if (c()) {
            sb.append(", ");
            sb.append("server:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("resource:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("isPreview:");
            sb.append(this.e);
        }
        sb.append(")");
        return sb.toString();
    }
}
