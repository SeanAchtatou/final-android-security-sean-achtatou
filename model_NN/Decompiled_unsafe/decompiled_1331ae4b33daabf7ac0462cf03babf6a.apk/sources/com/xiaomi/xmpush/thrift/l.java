package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.g;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class l implements Serializable, Cloneable, b<l, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> c;
    private static final k d = new k("XmPushActionCheckClientInfo");
    private static final c e = new c("miscConfigVersion", (byte) 8, 1);
    private static final c f = new c("pluginConfigVersion", (byte) 8, 2);

    /* renamed from: a  reason: collision with root package name */
    public int f4110a;

    /* renamed from: b  reason: collision with root package name */
    public int f4111b;
    private BitSet g = new BitSet(2);

    public enum a {
        MISC_CONFIG_VERSION(1, "miscConfigVersion"),
        PLUGIN_CONFIG_VERSION(2, "pluginConfigVersion");
        
        private static final Map<String, a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                c.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public String a() {
            return this.e;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.l$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.MISC_CONFIG_VERSION, (Object) new org.apache.thrift.meta_data.b("miscConfigVersion", (byte) 1, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.PLUGIN_CONFIG_VERSION, (Object) new org.apache.thrift.meta_data.b("pluginConfigVersion", (byte) 1, new org.apache.thrift.meta_data.c((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(l.class, c);
    }

    public l a(int i) {
        this.f4110a = i;
        a(true);
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i = fVar.i();
            if (i.f4226b == 0) {
                fVar.h();
                if (!a()) {
                    throw new g("Required field 'miscConfigVersion' was not found in serialized data! Struct: " + toString());
                } else if (!b()) {
                    throw new g("Required field 'pluginConfigVersion' was not found in serialized data! Struct: " + toString());
                } else {
                    c();
                    return;
                }
            } else {
                switch (i.c) {
                    case 1:
                        if (i.f4226b != 8) {
                            i.a(fVar, i.f4226b);
                            break;
                        } else {
                            this.f4110a = fVar.t();
                            a(true);
                            break;
                        }
                    case 2:
                        if (i.f4226b != 8) {
                            i.a(fVar, i.f4226b);
                            break;
                        } else {
                            this.f4111b = fVar.t();
                            b(true);
                            break;
                        }
                    default:
                        i.a(fVar, i.f4226b);
                        break;
                }
                fVar.j();
            }
        }
    }

    public void a(boolean z) {
        this.g.set(0, z);
    }

    public boolean a() {
        return this.g.get(0);
    }

    public l b(int i) {
        this.f4111b = i;
        b(true);
        return this;
    }

    public void b(f fVar) {
        c();
        fVar.a(d);
        fVar.a(e);
        fVar.a(this.f4110a);
        fVar.b();
        fVar.a(f);
        fVar.a(this.f4111b);
        fVar.b();
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.g.set(1, z);
    }

    public boolean b() {
        return this.g.get(1);
    }

    public void c() {
    }

    public String toString() {
        return "XmPushActionCheckClientInfo(" + "miscConfigVersion:" + this.f4110a + ", " + "pluginConfigVersion:" + this.f4111b + ")";
    }
}
