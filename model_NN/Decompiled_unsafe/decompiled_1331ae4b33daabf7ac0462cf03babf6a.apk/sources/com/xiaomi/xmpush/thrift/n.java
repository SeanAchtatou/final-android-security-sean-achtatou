package com.xiaomi.xmpush.thrift;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.open.SocialConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.d;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class n implements Serializable, Cloneable, b<n, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> l;
    private static final k m = new k("XmPushActionCommandResult");
    private static final c n = new c("debug", (byte) 11, 1);
    private static final c o = new c("target", (byte) 12, 2);
    private static final c p = new c("id", (byte) 11, 3);
    private static final c q = new c("appId", (byte) 11, 4);
    private static final c r = new c("cmdName", (byte) 11, 5);
    private static final c s = new c(SocialConstants.TYPE_REQUEST, (byte) 12, 6);
    private static final c t = new c("errorCode", (byte) 10, 7);
    private static final c u = new c("reason", (byte) 11, 8);
    private static final c v = new c("packageName", (byte) 11, 9);
    private static final c w = new c("cmdArgs", (byte) 15, 10);
    private static final c x = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 12);

    /* renamed from: a  reason: collision with root package name */
    public String f4118a;

    /* renamed from: b  reason: collision with root package name */
    public j f4119b;
    public String c;
    public String d;
    public String e;
    public m f;
    public long g;
    public String h;
    public String i;
    public List<String> j;
    public String k;
    private BitSet y = new BitSet(1);

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        CMD_NAME(5, "cmdName"),
        REQUEST(6, SocialConstants.TYPE_REQUEST),
        ERROR_CODE(7, "errorCode"),
        REASON(8, "reason"),
        PACKAGE_NAME(9, "packageName"),
        CMD_ARGS(10, "cmdArgs"),
        CATEGORY(12, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY);
        
        private static final Map<String, a> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                l.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public String a() {
            return this.n;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.n$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.thrift.meta_data.b("debug", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CMD_NAME, (Object) new org.apache.thrift.meta_data.b("cmdName", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new org.apache.thrift.meta_data.b(SocialConstants.TYPE_REQUEST, (byte) 2, new g((byte) 12, m.class)));
        enumMap.put((Object) a.ERROR_CODE, (Object) new org.apache.thrift.meta_data.b("errorCode", (byte) 1, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.REASON, (Object) new org.apache.thrift.meta_data.b("reason", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CMD_ARGS, (Object) new org.apache.thrift.meta_data.b("cmdArgs", (byte) 2, new d((byte) 15, new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.thrift.meta_data.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        l = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(n.class, l);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                if (!h()) {
                    throw new org.apache.thrift.protocol.g("Required field 'errorCode' was not found in serialized data! Struct: " + toString());
                }
                o();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4118a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.f4226b != 12) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4119b = new j();
                        this.f4119b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.f4226b != 12) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f = new m();
                        this.f.a(fVar);
                        break;
                    }
                case 7:
                    if (i2.f4226b != 10) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.g = fVar.u();
                        a(true);
                        break;
                    }
                case 8:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                case 9:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.f4226b != 15) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        org.apache.thrift.protocol.d m2 = fVar.m();
                        this.j = new ArrayList(m2.f4228b);
                        for (int i3 = 0; i3 < m2.f4228b; i3++) {
                            this.j.add(fVar.w());
                        }
                        fVar.n();
                        break;
                    }
                case 11:
                default:
                    i.a(fVar, i2.f4226b);
                    break;
                case 12:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.k = fVar.w();
                        break;
                    }
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.y.set(0, z);
    }

    public boolean a() {
        return this.f4118a != null;
    }

    public void b(f fVar) {
        o();
        fVar.a(m);
        if (this.f4118a != null && a()) {
            fVar.a(n);
            fVar.a(this.f4118a);
            fVar.b();
        }
        if (this.f4119b != null && b()) {
            fVar.a(o);
            this.f4119b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(p);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(q);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null) {
            fVar.a(r);
            fVar.a(this.e);
            fVar.b();
        }
        if (this.f != null && g()) {
            fVar.a(s);
            this.f.b(fVar);
            fVar.b();
        }
        fVar.a(t);
        fVar.a(this.g);
        fVar.b();
        if (this.h != null && i()) {
            fVar.a(u);
            fVar.a(this.h);
            fVar.b();
        }
        if (this.i != null && j()) {
            fVar.a(v);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && l()) {
            fVar.a(w);
            fVar.a(new org.apache.thrift.protocol.d((byte) 11, this.j.size()));
            for (String a2 : this.j) {
                fVar.a(a2);
            }
            fVar.e();
            fVar.b();
        }
        if (this.k != null && n()) {
            fVar.a(x);
            fVar.a(this.k);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.f4119b != null;
    }

    public String e() {
        return this.e;
    }

    public boolean g() {
        return this.f != null;
    }

    public boolean h() {
        return this.y.get(0);
    }

    public boolean i() {
        return this.h != null;
    }

    public boolean j() {
        return this.i != null;
    }

    public List<String> k() {
        return this.j;
    }

    public boolean l() {
        return this.j != null;
    }

    public String m() {
        return this.k;
    }

    public boolean n() {
        return this.k != null;
    }

    public void o() {
        if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new org.apache.thrift.protocol.g("Required field 'cmdName' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("XmPushActionCommandResult(");
        boolean z2 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f4118a == null) {
                sb.append("null");
            } else {
                sb.append(this.f4118a);
            }
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.f4119b == null) {
                sb.append("null");
            } else {
                sb.append(this.f4119b);
            }
        } else {
            z = z2;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("cmdName:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        if (g()) {
            sb.append(", ");
            sb.append("request:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        sb.append(", ");
        sb.append("errorCode:");
        sb.append(this.g);
        if (i()) {
            sb.append(", ");
            sb.append("reason:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("cmdArgs:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("category:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
