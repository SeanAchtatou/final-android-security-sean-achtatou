package com.xiaomi.xmpush.thrift;

import com.igexin.assist.sdk.AssistPushConsts;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class h implements Serializable, Cloneable, b<h, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> i;
    private static final k j = new k("PushMessage");
    private static final c k = new c("to", (byte) 12, 1);
    private static final c l = new c("id", (byte) 11, 2);
    private static final c m = new c("appId", (byte) 11, 3);
    private static final c n = new c(AssistPushConsts.MSG_TYPE_PAYLOAD, (byte) 11, 4);
    private static final c o = new c("createAt", (byte) 10, 5);
    private static final c p = new c("ttl", (byte) 10, 6);
    private static final c q = new c("collapseKey", (byte) 11, 7);
    private static final c r = new c("packageName", (byte) 11, 8);

    /* renamed from: a  reason: collision with root package name */
    public j f4094a;

    /* renamed from: b  reason: collision with root package name */
    public String f4095b;
    public String c;
    public String d;
    public long e;
    public long f;
    public String g;
    public String h;
    private BitSet s = new BitSet(2);

    public enum a {
        TO(1, "to"),
        ID(2, "id"),
        APP_ID(3, "appId"),
        PAYLOAD(4, AssistPushConsts.MSG_TYPE_PAYLOAD),
        CREATE_AT(5, "createAt"),
        TTL(6, "ttl"),
        COLLAPSE_KEY(7, "collapseKey"),
        PACKAGE_NAME(8, "packageName");
        
        private static final Map<String, a> i = new HashMap();
        private final short j;
        private final String k;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                i.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.j = s;
            this.k = str;
        }

        public String a() {
            return this.k;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.h$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.TO, (Object) new org.apache.thrift.meta_data.b("to", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PAYLOAD, (Object) new org.apache.thrift.meta_data.b(AssistPushConsts.MSG_TYPE_PAYLOAD, (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CREATE_AT, (Object) new org.apache.thrift.meta_data.b("createAt", (byte) 2, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.TTL, (Object) new org.apache.thrift.meta_data.b("ttl", (byte) 2, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.COLLAPSE_KEY, (Object) new org.apache.thrift.meta_data.b("collapseKey", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        i = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(h.class, i);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                m();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 12) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4094a = new j();
                        this.f4094a.a(fVar);
                        break;
                    }
                case 2:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4095b = fVar.w();
                        break;
                    }
                case 3:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.f4226b != 10) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.e = fVar.u();
                        a(true);
                        break;
                    }
                case 6:
                    if (i2.f4226b != 10) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f = fVar.u();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.h = fVar.w();
                        break;
                    }
                default:
                    i.a(fVar, i2.f4226b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.s.set(0, z);
    }

    public boolean a() {
        return this.f4094a != null;
    }

    public String b() {
        return this.f4095b;
    }

    public void b(f fVar) {
        m();
        fVar.a(j);
        if (this.f4094a != null && a()) {
            fVar.a(k);
            this.f4094a.b(fVar);
            fVar.b();
        }
        if (this.f4095b != null) {
            fVar.a(l);
            fVar.a(this.f4095b);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(m);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(n);
            fVar.a(this.d);
            fVar.b();
        }
        if (i()) {
            fVar.a(o);
            fVar.a(this.e);
            fVar.b();
        }
        if (j()) {
            fVar.a(p);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && k()) {
            fVar.a(q);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && l()) {
            fVar.a(r);
            fVar.a(this.h);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z) {
        this.s.set(1, z);
    }

    public String d() {
        return this.c;
    }

    public String f() {
        return this.d;
    }

    public long h() {
        return this.e;
    }

    public boolean i() {
        return this.s.get(0);
    }

    public boolean j() {
        return this.s.get(1);
    }

    public boolean k() {
        return this.g != null;
    }

    public boolean l() {
        return this.h != null;
    }

    public void m() {
        if (this.f4095b == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'appId' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'payload' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PushMessage(");
        boolean z = true;
        if (a()) {
            sb.append("to:");
            if (this.f4094a == null) {
                sb.append("null");
            } else {
                sb.append(this.f4094a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.f4095b == null) {
            sb.append("null");
        } else {
            sb.append(this.f4095b);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("payload:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (i()) {
            sb.append(", ");
            sb.append("createAt:");
            sb.append(this.e);
        }
        if (j()) {
            sb.append(", ");
            sb.append("ttl:");
            sb.append(this.f);
        }
        if (k()) {
            sb.append(", ");
            sb.append("collapseKey:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (l()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
