package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.d;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;
import org.apache.thrift.protocol.k;

public class e implements Serializable, Cloneable, b<e, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> d;
    private static final k e = new k("NormalConfig");
    private static final c f = new c("version", (byte) 8, 1);
    private static final c g = new c("configItems", (byte) 15, 2);
    private static final c h = new c("type", (byte) 8, 3);

    /* renamed from: a  reason: collision with root package name */
    public int f4084a;

    /* renamed from: b  reason: collision with root package name */
    public List<g> f4085b;
    public c c;
    private BitSet i = new BitSet(1);

    public enum a {
        VERSION(1, "version"),
        CONFIG_ITEMS(2, "configItems"),
        TYPE(3, "type");
        
        private static final Map<String, a> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                d.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public String a() {
            return this.f;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.e$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.VERSION, (Object) new org.apache.thrift.meta_data.b("version", (byte) 1, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.CONFIG_ITEMS, (Object) new org.apache.thrift.meta_data.b("configItems", (byte) 1, new d((byte) 15, new g((byte) 12, g.class))));
        enumMap.put((Object) a.TYPE, (Object) new org.apache.thrift.meta_data.b("type", (byte) 1, new org.apache.thrift.meta_data.a((byte) 16, c.class)));
        d = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(e.class, d);
    }

    public int a() {
        return this.f4084a;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                if (!b()) {
                    throw new org.apache.thrift.protocol.g("Required field 'version' was not found in serialized data! Struct: " + toString());
                }
                f();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 8) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4084a = fVar.t();
                        a(true);
                        break;
                    }
                case 2:
                    if (i2.f4226b != 15) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        org.apache.thrift.protocol.d m = fVar.m();
                        this.f4085b = new ArrayList(m.f4228b);
                        for (int i3 = 0; i3 < m.f4228b; i3++) {
                            g gVar = new g();
                            gVar.a(fVar);
                            this.f4085b.add(gVar);
                        }
                        fVar.n();
                        break;
                    }
                case 3:
                    if (i2.f4226b != 8) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = c.a(fVar.t());
                        break;
                    }
                default:
                    i.a(fVar, i2.f4226b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z) {
        this.i.set(0, z);
    }

    public void b(f fVar) {
        f();
        fVar.a(e);
        fVar.a(f);
        fVar.a(this.f4084a);
        fVar.b();
        if (this.f4085b != null) {
            fVar.a(g);
            fVar.a(new org.apache.thrift.protocol.d((byte) 12, this.f4085b.size()));
            for (g b2 : this.f4085b) {
                b2.b(fVar);
            }
            fVar.e();
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(h);
            fVar.a(this.c.a());
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public boolean b() {
        return this.i.get(0);
    }

    public c d() {
        return this.c;
    }

    public void f() {
        if (this.f4085b == null) {
            throw new org.apache.thrift.protocol.g("Required field 'configItems' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'type' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("NormalConfig(");
        sb.append("version:");
        sb.append(this.f4084a);
        sb.append(", ");
        sb.append("configItems:");
        if (this.f4085b == null) {
            sb.append("null");
        } else {
            sb.append(this.f4085b);
        }
        sb.append(", ");
        sb.append("type:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }
}
