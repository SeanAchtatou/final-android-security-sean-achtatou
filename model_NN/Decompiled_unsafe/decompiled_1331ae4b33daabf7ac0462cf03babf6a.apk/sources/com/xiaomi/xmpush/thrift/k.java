package com.xiaomi.xmpush.thrift;

import com.sina.weibo.sdk.component.WidgetRequestParam;
import com.tencent.open.SocialConstants;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.g;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.i;

public class k implements Serializable, Cloneable, b<k, a> {
    private static final c A = new c(SocialConstants.TYPE_REQUEST, (byte) 12, 8);
    private static final c B = new c("packageName", (byte) 11, 9);
    private static final c C = new c(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 11, 10);
    private static final c D = new c("isOnline", (byte) 2, 11);
    private static final c E = new c("regId", (byte) 11, 12);
    private static final c F = new c("callbackUrl", (byte) 11, 13);
    private static final c G = new c("userAccount", (byte) 11, 14);
    private static final c H = new c("deviceStatus", (byte) 6, 15);
    private static final c I = new c("imeiMd5", (byte) 11, 20);
    private static final c J = new c("deviceId", (byte) 11, 21);
    public static final Map<a, org.apache.thrift.meta_data.b> r;
    private static final org.apache.thrift.protocol.k s = new org.apache.thrift.protocol.k("XmPushActionAckMessage");
    private static final c t = new c("debug", (byte) 11, 1);
    private static final c u = new c("target", (byte) 12, 2);
    private static final c v = new c("id", (byte) 11, 3);
    private static final c w = new c("appId", (byte) 11, 4);
    private static final c x = new c("messageTs", (byte) 10, 5);
    private static final c y = new c("topic", (byte) 11, 6);
    private static final c z = new c("aliasName", (byte) 11, 7);
    private BitSet K = new BitSet(3);

    /* renamed from: a  reason: collision with root package name */
    public String f4106a;

    /* renamed from: b  reason: collision with root package name */
    public j f4107b;
    public String c;
    public String d;
    public long e;
    public String f;
    public String g;
    public w h;
    public String i;
    public String j;
    public boolean k = false;
    public String l;
    public String m;
    public String n;
    public short o;
    public String p;
    public String q;

    public enum a {
        DEBUG(1, "debug"),
        TARGET(2, "target"),
        ID(3, "id"),
        APP_ID(4, "appId"),
        MESSAGE_TS(5, "messageTs"),
        TOPIC(6, "topic"),
        ALIAS_NAME(7, "aliasName"),
        REQUEST(8, SocialConstants.TYPE_REQUEST),
        PACKAGE_NAME(9, "packageName"),
        CATEGORY(10, WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY),
        IS_ONLINE(11, "isOnline"),
        REG_ID(12, "regId"),
        CALLBACK_URL(13, "callbackUrl"),
        USER_ACCOUNT(14, "userAccount"),
        DEVICE_STATUS(15, "deviceStatus"),
        IMEI_MD5(20, "imeiMd5"),
        DEVICE_ID(21, "deviceId");
        
        private static final Map<String, a> r = new HashMap();
        private final short s;
        private final String t;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                r.put(aVar.a(), aVar);
            }
        }

        private a(short s2, String str) {
            this.s = s2;
            this.t = str;
        }

        public String a() {
            return this.t;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.k$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.DEBUG, (Object) new org.apache.thrift.meta_data.b("debug", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TARGET, (Object) new org.apache.thrift.meta_data.b("target", (byte) 2, new g((byte) 12, j.class)));
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.APP_ID, (Object) new org.apache.thrift.meta_data.b("appId", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.MESSAGE_TS, (Object) new org.apache.thrift.meta_data.b("messageTs", (byte) 1, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.thrift.meta_data.b("topic", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.ALIAS_NAME, (Object) new org.apache.thrift.meta_data.b("aliasName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.REQUEST, (Object) new org.apache.thrift.meta_data.b(SocialConstants.TYPE_REQUEST, (byte) 2, new g((byte) 12, w.class)));
        enumMap.put((Object) a.PACKAGE_NAME, (Object) new org.apache.thrift.meta_data.b("packageName", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CATEGORY, (Object) new org.apache.thrift.meta_data.b(WidgetRequestParam.REQ_PARAM_COMMENT_CATEGORY, (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.IS_ONLINE, (Object) new org.apache.thrift.meta_data.b("isOnline", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        enumMap.put((Object) a.REG_ID, (Object) new org.apache.thrift.meta_data.b("regId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.CALLBACK_URL, (Object) new org.apache.thrift.meta_data.b("callbackUrl", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.USER_ACCOUNT, (Object) new org.apache.thrift.meta_data.b("userAccount", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.DEVICE_STATUS, (Object) new org.apache.thrift.meta_data.b("deviceStatus", (byte) 2, new org.apache.thrift.meta_data.c((byte) 6)));
        enumMap.put((Object) a.IMEI_MD5, (Object) new org.apache.thrift.meta_data.b("imeiMd5", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.DEVICE_ID, (Object) new org.apache.thrift.meta_data.b("deviceId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        r = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(k.class, r);
    }

    public k a(long j2) {
        this.e = j2;
        a(true);
        return this;
    }

    public k a(String str) {
        this.c = str;
        return this;
    }

    public k a(short s2) {
        this.o = s2;
        c(true);
        return this;
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                if (!e()) {
                    throw new org.apache.thrift.protocol.g("Required field 'messageTs' was not found in serialized data! Struct: " + toString());
                }
                r();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4106a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.f4226b != 12) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4107b = new j();
                        this.f4107b.a(fVar);
                        break;
                    }
                case 3:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.f4226b != 10) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.e = fVar.u();
                        a(true);
                        break;
                    }
                case 6:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f = fVar.w();
                        break;
                    }
                case 7:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.f4226b != 12) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.h = new w();
                        this.h.a(fVar);
                        break;
                    }
                case 9:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.i = fVar.w();
                        break;
                    }
                case 10:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.j = fVar.w();
                        break;
                    }
                case 11:
                    if (i2.f4226b != 2) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.k = fVar.q();
                        b(true);
                        break;
                    }
                case 12:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.l = fVar.w();
                        break;
                    }
                case 13:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.m = fVar.w();
                        break;
                    }
                case 14:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.n = fVar.w();
                        break;
                    }
                case 15:
                    if (i2.f4226b != 6) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.o = fVar.s();
                        c(true);
                        break;
                    }
                case 16:
                case 17:
                case 18:
                case 19:
                default:
                    i.a(fVar, i2.f4226b);
                    break;
                case 20:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.p = fVar.w();
                        break;
                    }
                case 21:
                    if (i2.f4226b != 11) {
                        i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.q = fVar.w();
                        break;
                    }
            }
            fVar.j();
        }
    }

    public void a(boolean z2) {
        this.K.set(0, z2);
    }

    public boolean a() {
        return this.f4106a != null;
    }

    public k b(String str) {
        this.d = str;
        return this;
    }

    public void b(f fVar) {
        r();
        fVar.a(s);
        if (this.f4106a != null && a()) {
            fVar.a(t);
            fVar.a(this.f4106a);
            fVar.b();
        }
        if (this.f4107b != null && b()) {
            fVar.a(u);
            this.f4107b.b(fVar);
            fVar.b();
        }
        if (this.c != null) {
            fVar.a(v);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null) {
            fVar.a(w);
            fVar.a(this.d);
            fVar.b();
        }
        fVar.a(x);
        fVar.a(this.e);
        fVar.b();
        if (this.f != null && f()) {
            fVar.a(y);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && g()) {
            fVar.a(z);
            fVar.a(this.g);
            fVar.b();
        }
        if (this.h != null && h()) {
            fVar.a(A);
            this.h.b(fVar);
            fVar.b();
        }
        if (this.i != null && i()) {
            fVar.a(B);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && j()) {
            fVar.a(C);
            fVar.a(this.j);
            fVar.b();
        }
        if (k()) {
            fVar.a(D);
            fVar.a(this.k);
            fVar.b();
        }
        if (this.l != null && l()) {
            fVar.a(E);
            fVar.a(this.l);
            fVar.b();
        }
        if (this.m != null && m()) {
            fVar.a(F);
            fVar.a(this.m);
            fVar.b();
        }
        if (this.n != null && n()) {
            fVar.a(G);
            fVar.a(this.n);
            fVar.b();
        }
        if (o()) {
            fVar.a(H);
            fVar.a(this.o);
            fVar.b();
        }
        if (this.p != null && p()) {
            fVar.a(I);
            fVar.a(this.p);
            fVar.b();
        }
        if (this.q != null && q()) {
            fVar.a(J);
            fVar.a(this.q);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z2) {
        this.K.set(1, z2);
    }

    public boolean b() {
        return this.f4107b != null;
    }

    public k c(String str) {
        this.f = str;
        return this;
    }

    public void c(boolean z2) {
        this.K.set(2, z2);
    }

    public k d(String str) {
        this.g = str;
        return this;
    }

    public boolean e() {
        return this.K.get(0);
    }

    public boolean f() {
        return this.f != null;
    }

    public boolean g() {
        return this.g != null;
    }

    public boolean h() {
        return this.h != null;
    }

    public boolean i() {
        return this.i != null;
    }

    public boolean j() {
        return this.j != null;
    }

    public boolean k() {
        return this.K.get(1);
    }

    public boolean l() {
        return this.l != null;
    }

    public boolean m() {
        return this.m != null;
    }

    public boolean n() {
        return this.n != null;
    }

    public boolean o() {
        return this.K.get(2);
    }

    public boolean p() {
        return this.p != null;
    }

    public boolean q() {
        return this.q != null;
    }

    public void r() {
        if (this.c == null) {
            throw new org.apache.thrift.protocol.g("Required field 'id' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new org.apache.thrift.protocol.g("Required field 'appId' was not present! Struct: " + toString());
        }
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("XmPushActionAckMessage(");
        boolean z3 = true;
        if (a()) {
            sb.append("debug:");
            if (this.f4106a == null) {
                sb.append("null");
            } else {
                sb.append(this.f4106a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("target:");
            if (this.f4107b == null) {
                sb.append("null");
            } else {
                sb.append(this.f4107b);
            }
        } else {
            z2 = z3;
        }
        if (!z2) {
            sb.append(", ");
        }
        sb.append("id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("appId:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("messageTs:");
        sb.append(this.e);
        if (f()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("aliasName:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("request:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("packageName:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("category:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("isOnline:");
            sb.append(this.k);
        }
        if (l()) {
            sb.append(", ");
            sb.append("regId:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("callbackUrl:");
            if (this.m == null) {
                sb.append("null");
            } else {
                sb.append(this.m);
            }
        }
        if (n()) {
            sb.append(", ");
            sb.append("userAccount:");
            if (this.n == null) {
                sb.append("null");
            } else {
                sb.append(this.n);
            }
        }
        if (o()) {
            sb.append(", ");
            sb.append("deviceStatus:");
            sb.append((int) this.o);
        }
        if (p()) {
            sb.append(", ");
            sb.append("imeiMd5:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        }
        if (q()) {
            sb.append(", ");
            sb.append("deviceId:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
