package com.xiaomi.xmpush.thrift;

import android.content.Context;
import com.xiaomi.channel.commonutils.android.b;
import com.xiaomi.channel.commonutils.misc.c;
import org.apache.thrift.e;
import org.apache.thrift.f;
import org.apache.thrift.g;
import org.apache.thrift.protocol.a;
import org.apache.thrift.protocol.l;

public class ad {
    public static short a(Context context, String str) {
        int i = 0;
        int a2 = (c.b(context) ? 4 : 0) + 0 + b.d(context, str).a();
        if (c.a(context)) {
            i = 8;
        }
        return (short) (a2 + i);
    }

    public static <T extends org.apache.thrift.b<T, ?>> void a(org.apache.thrift.b bVar, byte[] bArr) {
        if (bArr == null) {
            throw new f("the message byte is empty.");
        }
        new e(new l.a(true, true, bArr.length)).a(bVar, bArr);
    }

    public static <T extends org.apache.thrift.b<T, ?>> byte[] a(T t) {
        if (t == null) {
            return null;
        }
        try {
            return new g(new a.C0067a()).a(t);
        } catch (f e) {
            com.xiaomi.channel.commonutils.logger.b.a("convertThriftObjectToBytes catch TException.", e);
            return null;
        }
    }
}
