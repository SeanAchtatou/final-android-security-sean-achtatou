package com.xiaomi.xmpush.thrift;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.thrift.b;
import org.apache.thrift.meta_data.e;
import org.apache.thrift.protocol.c;
import org.apache.thrift.protocol.f;
import org.apache.thrift.protocol.g;
import org.apache.thrift.protocol.k;

public class i implements Serializable, Cloneable, b<i, a> {
    public static final Map<a, org.apache.thrift.meta_data.b> m;
    private static final k n = new k("PushMetaInfo");
    private static final c o = new c("id", (byte) 11, 1);
    private static final c p = new c("messageTs", (byte) 10, 2);
    private static final c q = new c("topic", (byte) 11, 3);
    private static final c r = new c("title", (byte) 11, 4);
    private static final c s = new c("description", (byte) 11, 5);
    private static final c t = new c("notifyType", (byte) 8, 6);
    private static final c u = new c("url", (byte) 11, 7);
    private static final c v = new c("passThrough", (byte) 8, 8);
    private static final c w = new c("notifyId", (byte) 8, 9);
    private static final c x = new c("extra", (byte) 13, 10);
    private static final c y = new c("internal", (byte) 13, 11);
    private static final c z = new c("ignoreRegInfo", (byte) 2, 12);
    private BitSet A = new BitSet(5);

    /* renamed from: a  reason: collision with root package name */
    public String f4098a;

    /* renamed from: b  reason: collision with root package name */
    public long f4099b;
    public String c;
    public String d;
    public String e;
    public int f;
    public String g;
    public int h;
    public int i;
    public Map<String, String> j;
    public Map<String, String> k;
    public boolean l = false;

    public enum a {
        ID(1, "id"),
        MESSAGE_TS(2, "messageTs"),
        TOPIC(3, "topic"),
        TITLE(4, "title"),
        DESCRIPTION(5, "description"),
        NOTIFY_TYPE(6, "notifyType"),
        URL(7, "url"),
        PASS_THROUGH(8, "passThrough"),
        NOTIFY_ID(9, "notifyId"),
        EXTRA(10, "extra"),
        INTERNAL(11, "internal"),
        IGNORE_REG_INFO(12, "ignoreRegInfo");
        
        private static final Map<String, a> m = new HashMap();
        private final short n;
        private final String o;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                m.put(aVar.a(), aVar);
            }
        }

        private a(short s, String str) {
            this.n = s;
            this.o = str;
        }

        public String a() {
            return this.o;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [com.xiaomi.xmpush.thrift.i$a, org.apache.thrift.meta_data.b]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.ID, (Object) new org.apache.thrift.meta_data.b("id", (byte) 1, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.MESSAGE_TS, (Object) new org.apache.thrift.meta_data.b("messageTs", (byte) 1, new org.apache.thrift.meta_data.c((byte) 10)));
        enumMap.put((Object) a.TOPIC, (Object) new org.apache.thrift.meta_data.b("topic", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.TITLE, (Object) new org.apache.thrift.meta_data.b("title", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.DESCRIPTION, (Object) new org.apache.thrift.meta_data.b("description", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.NOTIFY_TYPE, (Object) new org.apache.thrift.meta_data.b("notifyType", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.URL, (Object) new org.apache.thrift.meta_data.b("url", (byte) 2, new org.apache.thrift.meta_data.c((byte) 11)));
        enumMap.put((Object) a.PASS_THROUGH, (Object) new org.apache.thrift.meta_data.b("passThrough", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.NOTIFY_ID, (Object) new org.apache.thrift.meta_data.b("notifyId", (byte) 2, new org.apache.thrift.meta_data.c((byte) 8)));
        enumMap.put((Object) a.EXTRA, (Object) new org.apache.thrift.meta_data.b("extra", (byte) 2, new e((byte) 13, new org.apache.thrift.meta_data.c((byte) 11), new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.INTERNAL, (Object) new org.apache.thrift.meta_data.b("internal", (byte) 2, new e((byte) 13, new org.apache.thrift.meta_data.c((byte) 11), new org.apache.thrift.meta_data.c((byte) 11))));
        enumMap.put((Object) a.IGNORE_REG_INFO, (Object) new org.apache.thrift.meta_data.b("ignoreRegInfo", (byte) 2, new org.apache.thrift.meta_data.c((byte) 2)));
        m = Collections.unmodifiableMap(enumMap);
        org.apache.thrift.meta_data.b.a(i.class, m);
    }

    public i a(int i2) {
        this.f = i2;
        b(true);
        return this;
    }

    public i a(String str) {
        this.f4098a = str;
        return this;
    }

    public i a(Map<String, String> map) {
        this.j = map;
        return this;
    }

    public void a(String str, String str2) {
        if (this.j == null) {
            this.j = new HashMap();
        }
        this.j.put(str, str2);
    }

    public void a(f fVar) {
        fVar.g();
        while (true) {
            c i2 = fVar.i();
            if (i2.f4226b == 0) {
                fVar.h();
                if (!e()) {
                    throw new g("Required field 'messageTs' was not found in serialized data! Struct: " + toString());
                }
                x();
                return;
            }
            switch (i2.c) {
                case 1:
                    if (i2.f4226b != 11) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4098a = fVar.w();
                        break;
                    }
                case 2:
                    if (i2.f4226b != 10) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f4099b = fVar.u();
                        a(true);
                        break;
                    }
                case 3:
                    if (i2.f4226b != 11) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.c = fVar.w();
                        break;
                    }
                case 4:
                    if (i2.f4226b != 11) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.d = fVar.w();
                        break;
                    }
                case 5:
                    if (i2.f4226b != 11) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.e = fVar.w();
                        break;
                    }
                case 6:
                    if (i2.f4226b != 8) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.f = fVar.t();
                        b(true);
                        break;
                    }
                case 7:
                    if (i2.f4226b != 11) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.g = fVar.w();
                        break;
                    }
                case 8:
                    if (i2.f4226b != 8) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.h = fVar.t();
                        c(true);
                        break;
                    }
                case 9:
                    if (i2.f4226b != 8) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.i = fVar.t();
                        d(true);
                        break;
                    }
                case 10:
                    if (i2.f4226b != 13) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        org.apache.thrift.protocol.e k2 = fVar.k();
                        this.j = new HashMap(k2.c * 2);
                        for (int i3 = 0; i3 < k2.c; i3++) {
                            this.j.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 11:
                    if (i2.f4226b != 13) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        org.apache.thrift.protocol.e k3 = fVar.k();
                        this.k = new HashMap(k3.c * 2);
                        for (int i4 = 0; i4 < k3.c; i4++) {
                            this.k.put(fVar.w(), fVar.w());
                        }
                        fVar.l();
                        break;
                    }
                case 12:
                    if (i2.f4226b != 2) {
                        org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                        break;
                    } else {
                        this.l = fVar.q();
                        e(true);
                        break;
                    }
                default:
                    org.apache.thrift.protocol.i.a(fVar, i2.f4226b);
                    break;
            }
            fVar.j();
        }
    }

    public void a(boolean z2) {
        this.A.set(0, z2);
    }

    public i b(int i2) {
        this.h = i2;
        c(true);
        return this;
    }

    public i b(String str) {
        this.c = str;
        return this;
    }

    public String b() {
        return this.f4098a;
    }

    public void b(f fVar) {
        x();
        fVar.a(n);
        if (this.f4098a != null) {
            fVar.a(o);
            fVar.a(this.f4098a);
            fVar.b();
        }
        fVar.a(p);
        fVar.a(this.f4099b);
        fVar.b();
        if (this.c != null && g()) {
            fVar.a(q);
            fVar.a(this.c);
            fVar.b();
        }
        if (this.d != null && i()) {
            fVar.a(r);
            fVar.a(this.d);
            fVar.b();
        }
        if (this.e != null && k()) {
            fVar.a(s);
            fVar.a(this.e);
            fVar.b();
        }
        if (m()) {
            fVar.a(t);
            fVar.a(this.f);
            fVar.b();
        }
        if (this.g != null && n()) {
            fVar.a(u);
            fVar.a(this.g);
            fVar.b();
        }
        if (p()) {
            fVar.a(v);
            fVar.a(this.h);
            fVar.b();
        }
        if (r()) {
            fVar.a(w);
            fVar.a(this.i);
            fVar.b();
        }
        if (this.j != null && t()) {
            fVar.a(x);
            fVar.a(new org.apache.thrift.protocol.e((byte) 11, (byte) 11, this.j.size()));
            for (Map.Entry next : this.j.entrySet()) {
                fVar.a((String) next.getKey());
                fVar.a((String) next.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (this.k != null && u()) {
            fVar.a(y);
            fVar.a(new org.apache.thrift.protocol.e((byte) 11, (byte) 11, this.k.size()));
            for (Map.Entry next2 : this.k.entrySet()) {
                fVar.a((String) next2.getKey());
                fVar.a((String) next2.getValue());
            }
            fVar.d();
            fVar.b();
        }
        if (w()) {
            fVar.a(z);
            fVar.a(this.l);
            fVar.b();
        }
        fVar.c();
        fVar.a();
    }

    public void b(boolean z2) {
        this.A.set(1, z2);
    }

    public i c(int i2) {
        this.i = i2;
        d(true);
        return this;
    }

    public i c(String str) {
        this.d = str;
        return this;
    }

    public void c(boolean z2) {
        this.A.set(2, z2);
    }

    public long d() {
        return this.f4099b;
    }

    public i d(String str) {
        this.e = str;
        return this;
    }

    public void d(boolean z2) {
        this.A.set(3, z2);
    }

    public void e(boolean z2) {
        this.A.set(4, z2);
    }

    public boolean e() {
        return this.A.get(0);
    }

    public String f() {
        return this.c;
    }

    public boolean g() {
        return this.c != null;
    }

    public String h() {
        return this.d;
    }

    public boolean i() {
        return this.d != null;
    }

    public String j() {
        return this.e;
    }

    public boolean k() {
        return this.e != null;
    }

    public int l() {
        return this.f;
    }

    public boolean m() {
        return this.A.get(1);
    }

    public boolean n() {
        return this.g != null;
    }

    public int o() {
        return this.h;
    }

    public boolean p() {
        return this.A.get(2);
    }

    public int q() {
        return this.i;
    }

    public boolean r() {
        return this.A.get(3);
    }

    public Map<String, String> s() {
        return this.j;
    }

    public boolean t() {
        return this.j != null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("PushMetaInfo(");
        sb.append("id:");
        if (this.f4098a == null) {
            sb.append("null");
        } else {
            sb.append(this.f4098a);
        }
        sb.append(", ");
        sb.append("messageTs:");
        sb.append(this.f4099b);
        if (g()) {
            sb.append(", ");
            sb.append("topic:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("title:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        if (k()) {
            sb.append(", ");
            sb.append("description:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (m()) {
            sb.append(", ");
            sb.append("notifyType:");
            sb.append(this.f);
        }
        if (n()) {
            sb.append(", ");
            sb.append("url:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (p()) {
            sb.append(", ");
            sb.append("passThrough:");
            sb.append(this.h);
        }
        if (r()) {
            sb.append(", ");
            sb.append("notifyId:");
            sb.append(this.i);
        }
        if (t()) {
            sb.append(", ");
            sb.append("extra:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (u()) {
            sb.append(", ");
            sb.append("internal:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        if (w()) {
            sb.append(", ");
            sb.append("ignoreRegInfo:");
            sb.append(this.l);
        }
        sb.append(")");
        return sb.toString();
    }

    public boolean u() {
        return this.k != null;
    }

    public boolean v() {
        return this.l;
    }

    public boolean w() {
        return this.A.get(4);
    }

    public void x() {
        if (this.f4098a == null) {
            throw new g("Required field 'id' was not present! Struct: " + toString());
        }
    }
}
