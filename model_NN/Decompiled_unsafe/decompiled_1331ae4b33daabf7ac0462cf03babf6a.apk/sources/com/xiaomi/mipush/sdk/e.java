package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import java.util.List;

final class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f4019a;

    e(Context context) {
        this.f4019a = context;
    }

    public void run() {
        try {
            List<PackageInfo> installedPackages = this.f4019a.getPackageManager().getInstalledPackages(4);
            if (installedPackages != null) {
                for (PackageInfo access$100 : installedPackages) {
                    MiPushClient.awakePushServiceByPackageInfo(this.f4019a, access$100);
                }
            }
        } catch (Throwable th) {
        }
    }
}
