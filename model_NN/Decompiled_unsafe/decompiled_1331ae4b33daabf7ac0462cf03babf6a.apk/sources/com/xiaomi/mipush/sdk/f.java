package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.logger.b;

final class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String[] f4020a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f4021b;

    f(String[] strArr, Context context) {
        this.f4020a = strArr;
        this.f4021b = context;
    }

    public void run() {
        PackageInfo packageInfo;
        try {
            for (String str : this.f4020a) {
                if (!TextUtils.isEmpty(str) && (packageInfo = this.f4021b.getPackageManager().getPackageInfo(str, 4)) != null) {
                    MiPushClient.awakePushServiceByPackageInfo(this.f4021b, packageInfo);
                }
            }
        } catch (Throwable th) {
            b.a(th);
        }
    }
}
