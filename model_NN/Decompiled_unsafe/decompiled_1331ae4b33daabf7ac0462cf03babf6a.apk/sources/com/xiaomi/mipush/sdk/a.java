package com.xiaomi.mipush.sdk;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.xiaomi.channel.commonutils.android.b;
import com.xiaomi.push.service.d;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f4013a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f4014b;
    private C0066a c;

    /* renamed from: com.xiaomi.mipush.sdk.a$a  reason: collision with other inner class name */
    class C0066a {

        /* renamed from: a  reason: collision with root package name */
        public String f4015a;

        /* renamed from: b  reason: collision with root package name */
        public String f4016b;
        public String c;
        public String d;
        public String e;
        public String f;
        public String g;
        public boolean h;
        public boolean i;
        public int j;

        private C0066a() {
            this.h = true;
            this.i = false;
            this.j = 1;
        }

        private String d() {
            return b.a(a.this.f4014b, a.this.f4014b.getPackageName());
        }

        public void a(int i2) {
            this.j = i2;
        }

        public void a(String str, String str2) {
            this.c = str;
            this.d = str2;
            this.f = d.e(a.this.f4014b);
            this.e = d();
            this.h = true;
            SharedPreferences.Editor edit = a.this.j().edit();
            edit.putString("regId", str);
            edit.putString("regSec", str2);
            edit.putString("devId", this.f);
            edit.putString("vName", d());
            edit.putBoolean("valid", true);
            edit.commit();
        }

        public void a(String str, String str2, String str3) {
            this.f4015a = str;
            this.f4016b = str2;
            this.g = str3;
            SharedPreferences.Editor edit = a.this.j().edit();
            edit.putString("appId", this.f4015a);
            edit.putString("appToken", str2);
            edit.putString("regResource", str3);
            edit.commit();
        }

        public void a(boolean z) {
            this.i = z;
        }

        public boolean a() {
            return b(this.f4015a, this.f4016b);
        }

        public void b() {
            a.this.j().edit().clear().commit();
            this.f4015a = null;
            this.f4016b = null;
            this.c = null;
            this.d = null;
            this.f = null;
            this.e = null;
            this.h = false;
            this.i = false;
            this.j = 1;
        }

        public boolean b(String str, String str2) {
            return TextUtils.equals(this.f4015a, str) && TextUtils.equals(this.f4016b, str2) && !TextUtils.isEmpty(this.c) && !TextUtils.isEmpty(this.d) && TextUtils.equals(this.f, d.e(a.this.f4014b));
        }

        public void c() {
            this.h = false;
            a.this.j().edit().putBoolean("valid", this.h).commit();
        }
    }

    private a(Context context) {
        this.f4014b = context;
        o();
    }

    public static a a(Context context) {
        if (f4013a == null) {
            f4013a = new a(context);
        }
        return f4013a;
    }

    private void o() {
        this.c = new C0066a();
        SharedPreferences j = j();
        this.c.f4015a = j.getString("appId", null);
        this.c.f4016b = j.getString("appToken", null);
        this.c.c = j.getString("regId", null);
        this.c.d = j.getString("regSec", null);
        this.c.f = j.getString("devId", null);
        if (!TextUtils.isEmpty(this.c.f) && this.c.f.startsWith("a-")) {
            this.c.f = d.e(this.f4014b);
            j.edit().putString("devId", this.c.f).commit();
        }
        this.c.e = j.getString("vName", null);
        this.c.h = j.getBoolean("valid", true);
        this.c.i = j.getBoolean("paused", false);
        this.c.j = j.getInt("envType", 1);
        this.c.g = j.getString("regResource", null);
    }

    public void a(int i) {
        this.c.a(i);
        j().edit().putInt("envType", i).commit();
    }

    public void a(String str) {
        SharedPreferences.Editor edit = j().edit();
        edit.putString("vName", str);
        edit.commit();
        this.c.e = str;
    }

    public void a(String str, String str2, String str3) {
        this.c.a(str, str2, str3);
    }

    public void a(boolean z) {
        this.c.a(z);
        j().edit().putBoolean("paused", z).commit();
    }

    public boolean a() {
        return !TextUtils.equals(b.a(this.f4014b, this.f4014b.getPackageName()), this.c.e);
    }

    public boolean a(String str, String str2) {
        return this.c.b(str, str2);
    }

    public void b(String str, String str2) {
        this.c.a(str, str2);
    }

    public boolean b() {
        if (this.c.a()) {
            return true;
        }
        com.xiaomi.channel.commonutils.logger.b.a("Don't send message before initialization succeeded!");
        return false;
    }

    public String c() {
        return this.c.f4015a;
    }

    public String d() {
        return this.c.f4016b;
    }

    public String e() {
        return this.c.c;
    }

    public String f() {
        return this.c.d;
    }

    public String g() {
        return this.c.g;
    }

    public void h() {
        this.c.b();
    }

    public boolean i() {
        return this.c.a();
    }

    public SharedPreferences j() {
        return this.f4014b.getSharedPreferences("mipush", 0);
    }

    public void k() {
        this.c.c();
    }

    public boolean l() {
        return this.c.i;
    }

    public int m() {
        return this.c.j;
    }

    public boolean n() {
        return !this.c.h;
    }
}
