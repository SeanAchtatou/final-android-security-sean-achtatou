package com.xiaomi.mipush.sdk;

import android.app.IntentService;
import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PushMessageHandler extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static List<MiPushClient.MiPushClientCallback> f4012a = new ArrayList();

    interface a extends Serializable {
    }

    public PushMessageHandler() {
        super("mipush message handler");
    }

    protected static void a() {
        List<MiPushClient.MiPushClientCallback> list = f4012a;
        synchronized (f4012a) {
            f4012a.clear();
        }
    }

    public static void a(long j, String str, String str2) {
        if (XmSystemUtils.isBrandXiaoMi()) {
            List<MiPushClient.MiPushClientCallback> list = f4012a;
            synchronized (f4012a) {
                for (MiPushClient.MiPushClientCallback onInitializeResult : f4012a) {
                    onInitializeResult.onInitializeResult(j, str, str2);
                }
            }
        }
    }

    public static void a(Context context, MiPushMessage miPushMessage) {
        if (XmSystemUtils.isBrandXiaoMi()) {
            List<MiPushClient.MiPushClientCallback> list = f4012a;
            synchronized (f4012a) {
                for (MiPushClient.MiPushClientCallback next : f4012a) {
                    if (a(miPushMessage.getCategory(), next.getCategory())) {
                        next.onReceiveMessage(miPushMessage.getContent(), miPushMessage.getAlias(), miPushMessage.getTopic(), miPushMessage.isNotified());
                        next.onReceiveMessage(miPushMessage);
                    }
                }
            }
        }
    }

    public static void a(Context context, a aVar) {
        String str = null;
        if (XmSystemUtils.isBrandXiaoMi()) {
            if (aVar instanceof MiPushMessage) {
                a(context, (MiPushMessage) aVar);
            } else if (aVar instanceof MiPushCommandMessage) {
                MiPushCommandMessage miPushCommandMessage = (MiPushCommandMessage) aVar;
                String command = miPushCommandMessage.getCommand();
                if (MiPushClient.COMMAND_REGISTER.equals(command)) {
                    List<String> commandArguments = miPushCommandMessage.getCommandArguments();
                    if (commandArguments != null && !commandArguments.isEmpty()) {
                        str = commandArguments.get(0);
                    }
                    a(miPushCommandMessage.getResultCode(), miPushCommandMessage.getReason(), str);
                } else if (MiPushClient.COMMAND_SET_ALIAS.equals(command) || MiPushClient.COMMAND_UNSET_ALIAS.equals(command) || MiPushClient.COMMAND_SET_ACCEPT_TIME.equals(command)) {
                    a(context, miPushCommandMessage.getCategory(), command, miPushCommandMessage.getResultCode(), miPushCommandMessage.getReason(), miPushCommandMessage.getCommandArguments());
                } else if (MiPushClient.COMMAND_SUBSCRIBE_TOPIC.equals(command)) {
                    List<String> commandArguments2 = miPushCommandMessage.getCommandArguments();
                    a(context, miPushCommandMessage.getCategory(), miPushCommandMessage.getResultCode(), miPushCommandMessage.getReason(), (commandArguments2 == null || commandArguments2.isEmpty()) ? null : commandArguments2.get(0));
                } else if (MiPushClient.COMMAND_UNSUBSCRIBE_TOPIC.equals(command)) {
                    List<String> commandArguments3 = miPushCommandMessage.getCommandArguments();
                    b(context, miPushCommandMessage.getCategory(), miPushCommandMessage.getResultCode(), miPushCommandMessage.getReason(), (commandArguments3 == null || commandArguments3.isEmpty()) ? null : commandArguments3.get(0));
                }
            }
        }
    }

    protected static void a(Context context, String str, long j, String str2, String str3) {
        List<MiPushClient.MiPushClientCallback> list = f4012a;
        synchronized (f4012a) {
            for (MiPushClient.MiPushClientCallback next : f4012a) {
                if (a(str, next.getCategory())) {
                    next.onSubscribeResult(j, str2, str3);
                }
            }
        }
    }

    protected static void a(Context context, String str, String str2, long j, String str3, List<String> list) {
        List<MiPushClient.MiPushClientCallback> list2 = f4012a;
        synchronized (f4012a) {
            for (MiPushClient.MiPushClientCallback next : f4012a) {
                if (a(str, next.getCategory())) {
                    next.onCommandResult(str2, j, str3, list);
                }
            }
        }
    }

    protected static void a(MiPushClient.MiPushClientCallback miPushClientCallback) {
        List<MiPushClient.MiPushClientCallback> list = f4012a;
        synchronized (f4012a) {
            if (!f4012a.contains(miPushClientCallback)) {
                f4012a.add(miPushClientCallback);
            }
        }
    }

    protected static boolean a(String str, String str2) {
        return (TextUtils.isEmpty(str) && TextUtils.isEmpty(str2)) || TextUtils.equals(str, str2);
    }

    protected static void b(Context context, String str, long j, String str2, String str3) {
        List<MiPushClient.MiPushClientCallback> list = f4012a;
        synchronized (f4012a) {
            for (MiPushClient.MiPushClientCallback next : f4012a) {
                if (a(str, next.getCategory())) {
                    next.onUnsubscribeResult(j, str2, str3);
                }
            }
        }
    }

    public static boolean b() {
        return f4012a.isEmpty();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008d A[Catch:{ Exception -> 0x00a4, Throwable -> 0x0025 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00aa  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onHandleIntent(android.content.Intent r7) {
        /*
            r6 = this;
            boolean r0 = com.xiaomi.mipush.sdk.XmSystemUtils.isBrandXiaoMi()     // Catch:{ Throwable -> 0x0025 }
            if (r0 != 0) goto L_0x0007
        L_0x0006:
            return
        L_0x0007:
            java.lang.String r0 = "com.xiaomi.mipush.sdk.WAKEUP"
            java.lang.String r1 = r7.getAction()     // Catch:{ Throwable -> 0x0025 }
            boolean r0 = r0.equals(r1)     // Catch:{ Throwable -> 0x0025 }
            if (r0 == 0) goto L_0x002a
            com.xiaomi.mipush.sdk.a r0 = com.xiaomi.mipush.sdk.a.a(r6)     // Catch:{ Throwable -> 0x0025 }
            boolean r0 = r0.i()     // Catch:{ Throwable -> 0x0025 }
            if (r0 == 0) goto L_0x0006
            com.xiaomi.mipush.sdk.j r0 = com.xiaomi.mipush.sdk.j.a(r6)     // Catch:{ Throwable -> 0x0025 }
            r0.a()     // Catch:{ Throwable -> 0x0025 }
            goto L_0x0006
        L_0x0025:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)
            goto L_0x0006
        L_0x002a:
            r0 = 1
            int r1 = com.xiaomi.mipush.sdk.PushMessageHelper.getPushMode(r6)     // Catch:{ Throwable -> 0x0025 }
            if (r0 != r1) goto L_0x004b
            boolean r0 = b()     // Catch:{ Throwable -> 0x0025 }
            if (r0 == 0) goto L_0x003d
            java.lang.String r0 = "receive a message before application calling initialize"
            com.xiaomi.channel.commonutils.logger.b.d(r0)     // Catch:{ Throwable -> 0x0025 }
            goto L_0x0006
        L_0x003d:
            com.xiaomi.mipush.sdk.i r0 = com.xiaomi.mipush.sdk.i.a(r6)     // Catch:{ Throwable -> 0x0025 }
            com.xiaomi.mipush.sdk.PushMessageHandler$a r0 = r0.a(r7)     // Catch:{ Throwable -> 0x0025 }
            if (r0 == 0) goto L_0x0006
            a(r6, r0)     // Catch:{ Throwable -> 0x0025 }
            goto L_0x0006
        L_0x004b:
            android.content.Intent r2 = new android.content.Intent     // Catch:{ Throwable -> 0x0025 }
            java.lang.String r0 = "com.xiaomi.mipush.RECEIVE_MESSAGE"
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0025 }
            java.lang.String r0 = r6.getPackageName()     // Catch:{ Throwable -> 0x0025 }
            r2.setPackage(r0)     // Catch:{ Throwable -> 0x0025 }
            r2.putExtras(r7)     // Catch:{ Throwable -> 0x0025 }
            android.content.pm.PackageManager r0 = r6.getPackageManager()     // Catch:{ Throwable -> 0x0025 }
            r1 = 32
            java.util.List r0 = r0.queryBroadcastReceivers(r2, r1)     // Catch:{ Exception -> 0x00a4 }
            r1 = 0
            if (r0 == 0) goto L_0x00b1
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x00a4 }
        L_0x006d:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x00a4 }
            if (r0 == 0) goto L_0x00b1
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x00a4 }
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0     // Catch:{ Exception -> 0x00a4 }
            android.content.pm.ActivityInfo r4 = r0.activityInfo     // Catch:{ Exception -> 0x00a4 }
            if (r4 == 0) goto L_0x006d
            android.content.pm.ActivityInfo r4 = r0.activityInfo     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r4 = r4.packageName     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r5 = r6.getPackageName()     // Catch:{ Exception -> 0x00a4 }
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00a4 }
            if (r4 == 0) goto L_0x006d
        L_0x008b:
            if (r0 == 0) goto L_0x00aa
            android.content.pm.ActivityInfo r0 = r0.activityInfo     // Catch:{ Exception -> 0x00a4 }
            java.lang.String r0 = r0.name     // Catch:{ Exception -> 0x00a4 }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ Exception -> 0x00a4 }
            java.lang.Object r0 = r0.newInstance()     // Catch:{ Exception -> 0x00a4 }
            com.xiaomi.mipush.sdk.PushMessageReceiver r0 = (com.xiaomi.mipush.sdk.PushMessageReceiver) r0     // Catch:{ Exception -> 0x00a4 }
            android.content.Context r1 = r6.getApplicationContext()     // Catch:{ Exception -> 0x00a4 }
            r0.onReceive(r1, r2)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0006
        L_0x00a4:
            r0 = move-exception
            com.xiaomi.channel.commonutils.logger.b.a(r0)     // Catch:{ Throwable -> 0x0025 }
            goto L_0x0006
        L_0x00aa:
            java.lang.String r0 = "cannot find the receiver to handler this message, check your manifest"
            com.xiaomi.channel.commonutils.logger.b.d(r0)     // Catch:{ Exception -> 0x00a4 }
            goto L_0x0006
        L_0x00b1:
            r0 = r1
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.xiaomi.mipush.sdk.PushMessageHandler.onHandleIntent(android.content.Intent):void");
    }
}
