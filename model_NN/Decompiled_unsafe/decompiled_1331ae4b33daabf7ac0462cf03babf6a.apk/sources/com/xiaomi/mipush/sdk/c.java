package com.xiaomi.mipush.sdk;

import android.content.Context;
import com.xiaomi.channel.commonutils.logger.b;
import com.xiaomi.push.service.module.d;

final class c implements Runnable {
    c() {
    }

    public void run() {
        try {
            com.xiaomi.push.service.module.c.a(MiPushClient.sContext).a(d.MODULE_CDATA).a().loadClass("com.xiaomi.push.mpcd.MpcdPlugin").getMethod("main", Context.class).invoke(null, MiPushClient.sContext);
        } catch (Exception e) {
            b.a("plugin load fail");
        }
    }
}
