package com.xiaomi.mipush.sdk;

import android.content.Context;

final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f4017a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f4018b;
    final /* synthetic */ String c;

    b(Context context, String str, String str2) {
        this.f4017a = context;
        this.f4018b = str;
        this.c = str2;
    }

    public void run() {
        MiPushClient.initialize(this.f4017a, this.f4018b, this.c, null);
    }
}
