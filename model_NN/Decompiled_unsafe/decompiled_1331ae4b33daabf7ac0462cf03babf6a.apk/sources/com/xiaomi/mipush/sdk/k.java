package com.xiaomi.mipush.sdk;

import android.database.ContentObserver;
import android.os.Handler;
import com.xiaomi.channel.commonutils.network.d;
import com.xiaomi.push.service.ab;

class k extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f4032a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(j jVar, Handler handler) {
        super(handler);
        this.f4032a = jVar;
    }

    public void onChange(boolean z) {
        Integer unused = this.f4032a.g = Integer.valueOf(ab.a(this.f4032a.c).b());
        if (this.f4032a.g.intValue() != 0) {
            this.f4032a.c.getContentResolver().unregisterContentObserver(this);
            if (d.d(this.f4032a.c)) {
                this.f4032a.c();
            }
        }
    }
}
