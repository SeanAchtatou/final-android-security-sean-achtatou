package com.xiaomi.channel.commonutils.string;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4006a = System.getProperty("line.separator");

    /* renamed from: b  reason: collision with root package name */
    private static char[] f4007b = new char[64];
    private static byte[] c = new byte[128];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            f4007b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            f4007b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            f4007b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        int i2 = i + 1;
        f4007b[i] = '+';
        int i3 = i2 + 1;
        f4007b[i2] = '/';
        for (int i4 = 0; i4 < c.length; i4++) {
            c[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            c[f4007b[i5]] = (byte) i5;
        }
    }

    public static byte[] a(String str) {
        return a(str.toCharArray());
    }

    public static byte[] a(char[] cArr) {
        return a(cArr, 0, cArr.length);
    }

    public static byte[] a(char[] cArr, int i, int i2) {
        char c2;
        int i3;
        char c3;
        int i4;
        int i5;
        if (i2 % 4 != 0) {
            throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
        }
        while (i2 > 0 && cArr[(i + i2) - 1] == '=') {
            i2--;
        }
        int i6 = (i2 * 3) / 4;
        byte[] bArr = new byte[i6];
        int i7 = i + i2;
        int i8 = 0;
        while (i < i7) {
            int i9 = i + 1;
            char c4 = cArr[i];
            int i10 = i9 + 1;
            char c5 = cArr[i9];
            if (i10 < i7) {
                c2 = cArr[i10];
                i10++;
            } else {
                c2 = 'A';
            }
            if (i10 < i7) {
                int i11 = i10 + 1;
                c3 = cArr[i10];
                i3 = i11;
            } else {
                i3 = i10;
                c3 = 'A';
            }
            if (c4 > 127 || c5 > 127 || c2 > 127 || c3 > 127) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            byte b2 = c[c4];
            byte b3 = c[c5];
            byte b4 = c[c2];
            byte b5 = c[c3];
            if (b2 < 0 || b3 < 0 || b4 < 0 || b5 < 0) {
                throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
            }
            int i12 = (b2 << 2) | (b3 >>> 4);
            int i13 = ((b3 & 15) << 4) | (b4 >>> 2);
            byte b6 = ((b4 & 3) << 6) | b5;
            int i14 = i8 + 1;
            bArr[i8] = (byte) i12;
            if (i14 < i6) {
                i4 = i14 + 1;
                bArr[i14] = (byte) i13;
            } else {
                i4 = i14;
            }
            if (i4 < i6) {
                i5 = i4 + 1;
                bArr[i4] = (byte) b6;
            } else {
                i5 = i4;
            }
            i8 = i5;
            i = i3;
        }
        return bArr;
    }
}
