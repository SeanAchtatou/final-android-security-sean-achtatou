package com.xiaomi.channel.commonutils.misc;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.LinkedBlockingQueue;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private a f3996a;

    /* renamed from: b  reason: collision with root package name */
    private Handler f3997b;
    private volatile boolean c;
    private final boolean d;
    private int e;

    class a extends Thread {

        /* renamed from: b  reason: collision with root package name */
        private final LinkedBlockingQueue<b> f3999b = new LinkedBlockingQueue<>();

        public a() {
            super("PackageProcessor");
        }

        public void a(b bVar) {
            this.f3999b.add(bVar);
        }
    }

    public abstract class b {
    }

    public f() {
        this(false);
    }

    public f(boolean z) {
        this(z, 0);
    }

    public f(boolean z, int i) {
        this.f3997b = null;
        this.c = false;
        this.e = 0;
        this.f3997b = new g(this, Looper.getMainLooper());
        this.d = z;
        this.e = i;
    }

    public synchronized void a(b bVar) {
        if (this.f3996a == null) {
            this.f3996a = new a();
            this.f3996a.setDaemon(this.d);
            this.c = false;
            this.f3996a.start();
        }
        this.f3996a.a(bVar);
    }
}
