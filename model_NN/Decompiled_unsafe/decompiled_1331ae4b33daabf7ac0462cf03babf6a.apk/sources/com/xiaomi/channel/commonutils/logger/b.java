package com.xiaomi.channel.commonutils.logger;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private static int f3988a = 2;

    /* renamed from: b  reason: collision with root package name */
    private static LoggerInterface f3989b = new a();
    private static final HashMap<Integer, Long> c = new HashMap<>();
    private static final HashMap<Integer, String> d = new HashMap<>();
    private static final Integer e = -1;
    private static AtomicInteger f = new AtomicInteger(1);

    public static void a(int i, String str) {
        if (i >= f3988a) {
            f3989b.log(str);
        }
    }

    public static void a(int i, String str, Throwable th) {
        if (i >= f3988a) {
            f3989b.log(str, th);
        }
    }

    public static void a(int i, Throwable th) {
        if (i >= f3988a) {
            f3989b.log("", th);
        }
    }

    public static void a(LoggerInterface loggerInterface) {
        f3989b = loggerInterface;
    }

    public static void a(String str) {
        a(2, "[Thread:" + Thread.currentThread().getId() + "] " + str);
    }

    public static void a(String str, Throwable th) {
        a(4, str, th);
    }

    public static void a(Throwable th) {
        a(4, th);
    }

    public static void b(String str) {
        a(0, str);
    }

    public static void c(String str) {
        a(1, "[Thread:" + Thread.currentThread().getId() + "] " + str);
    }

    public static void d(String str) {
        a(4, str);
    }
}
