package com.d.a.c;

import android.util.Log;
import com.d.a.b.d;

/* compiled from: L */
public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static volatile boolean f1004a = false;

    public static void a(String str, Object... objArr) {
        a(3, null, str, objArr);
    }

    public static void b(String str, Object... objArr) {
        a(4, null, str, objArr);
    }

    public static void c(String str, Object... objArr) {
        a(5, null, str, objArr);
    }

    public static void a(Throwable th) {
        a(6, th, null, new Object[0]);
    }

    public static void d(String str, Object... objArr) {
        a(6, null, str, objArr);
    }

    private static void a(int i, Throwable th, String str, Object... objArr) {
        String str2;
        if (!f1004a) {
            if (objArr.length > 0) {
                str2 = String.format(str, objArr);
            } else {
                str2 = str;
            }
            if (th != null) {
                if (str2 == null) {
                    str2 = th.getMessage();
                }
                str2 = String.format("%1$s\n%2$s", str2, Log.getStackTraceString(th));
            }
            Log.println(i, d.f967a, str2);
        }
    }
}
