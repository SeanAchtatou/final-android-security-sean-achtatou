package com.d.a.c;

import android.opengl.GLES10;
import com.d.a.b.a.h;
import com.d.a.b.a.n;

/* compiled from: ImageSizeUtils */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static h f1002a;

    static {
        int[] iArr = new int[1];
        GLES10.glGetIntegerv(3379, iArr, 0);
        int max = Math.max(iArr[0], 2048);
        f1002a = new h(max, max);
    }

    public static h a(com.d.a.b.e.a aVar, h hVar) {
        int a2 = aVar.a();
        if (a2 <= 0) {
            a2 = hVar.a();
        }
        int b2 = aVar.b();
        if (b2 <= 0) {
            b2 = hVar.b();
        }
        return new h(a2, b2);
    }

    public static int a(h hVar, h hVar2, n nVar, boolean z) {
        int min;
        int a2 = hVar.a();
        int b2 = hVar.b();
        int a3 = hVar2.a();
        int b3 = hVar2.b();
        int i = a2 / a3;
        int i2 = b2 / b3;
        switch (nVar) {
            case FIT_INSIDE:
                if (!z) {
                    min = Math.max(i, i2);
                    break;
                } else {
                    int i3 = a2;
                    int i4 = b2;
                    min = 1;
                    while (true) {
                        if (i3 / 2 < a3 && i4 / 2 < b3) {
                            break;
                        } else {
                            i3 /= 2;
                            i4 /= 2;
                            min *= 2;
                        }
                    }
                }
                break;
            case CROP:
                if (!z) {
                    min = Math.min(i, i2);
                    break;
                } else {
                    int i5 = a2;
                    int i6 = b2;
                    int i7 = 1;
                    while (i5 / 2 >= a3 && i6 / 2 >= b3) {
                        i5 /= 2;
                        i6 /= 2;
                        i7 = min * 2;
                    }
                }
                break;
            default:
                min = 1;
                break;
        }
        if (min < 1) {
            return 1;
        }
        return min;
    }

    public static int a(h hVar) {
        int a2 = hVar.a();
        int b2 = hVar.b();
        return Math.max((int) Math.ceil((double) (((float) a2) / ((float) f1002a.a()))), (int) Math.ceil((double) (((float) b2) / ((float) f1002a.b()))));
    }

    public static float b(h hVar, h hVar2, n nVar, boolean z) {
        int i;
        int i2;
        int a2 = hVar.a();
        int b2 = hVar.b();
        int a3 = hVar2.a();
        int b3 = hVar2.b();
        float f = ((float) a2) / ((float) a3);
        float f2 = ((float) b2) / ((float) b3);
        if ((nVar != n.FIT_INSIDE || f < f2) && (nVar != n.CROP || f >= f2)) {
            i = (int) (((float) a2) / f2);
            i2 = b3;
        } else {
            i = a3;
            i2 = (int) (((float) b2) / f);
        }
        if ((z || i >= a2 || i2 >= b2) && (!z || i == a2 || i2 == b2)) {
            return 1.0f;
        }
        return ((float) i) / ((float) a2);
    }
}
