package com.d.a.b.e;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import com.d.a.b.a.h;
import com.d.a.b.a.n;

/* compiled from: ImageNonViewAware */
public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    protected final String f982a;

    /* renamed from: b  reason: collision with root package name */
    protected final h f983b;
    protected final n c;

    public b(String str, h hVar, n nVar) {
        this.f982a = str;
        this.f983b = hVar;
        this.c = nVar;
    }

    public int a() {
        return this.f983b.a();
    }

    public int b() {
        return this.f983b.b();
    }

    public n c() {
        return this.c;
    }

    public View d() {
        return null;
    }

    public boolean e() {
        return false;
    }

    public int f() {
        return TextUtils.isEmpty(this.f982a) ? super.hashCode() : this.f982a.hashCode();
    }

    public boolean a(Drawable drawable) {
        return true;
    }

    public boolean a(Bitmap bitmap) {
        return true;
    }
}
