package com.d.a.b.a;

/* compiled from: ImageSize */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final int f937a;

    /* renamed from: b  reason: collision with root package name */
    private final int f938b;

    public h(int i, int i2) {
        this.f937a = i;
        this.f938b = i2;
    }

    public h(int i, int i2, int i3) {
        if (i3 % 180 == 0) {
            this.f937a = i;
            this.f938b = i2;
            return;
        }
        this.f937a = i2;
        this.f938b = i;
    }

    public int a() {
        return this.f937a;
    }

    public int b() {
        return this.f938b;
    }

    public h a(int i) {
        return new h(this.f937a / i, this.f938b / i);
    }

    public h a(float f) {
        return new h((int) (((float) this.f937a) * f), (int) (((float) this.f938b) * f));
    }

    public String toString() {
        return new StringBuilder(9).append(this.f937a).append("x").append(this.f938b).toString();
    }
}
