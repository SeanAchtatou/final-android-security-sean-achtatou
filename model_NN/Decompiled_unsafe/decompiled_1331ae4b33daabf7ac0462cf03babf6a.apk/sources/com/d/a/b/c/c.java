package com.d.a.b.c;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.d.a.b.a.i;

/* compiled from: RoundedBitmapDisplayer */
public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    protected final int f963a;

    /* renamed from: b  reason: collision with root package name */
    protected final int f964b;

    public c(int i) {
        this(i, 0);
    }

    public c(int i, int i2) {
        this.f963a = i;
        this.f964b = i2;
    }

    public void display(Bitmap bitmap, com.d.a.b.e.a aVar, i iVar) {
        if (!(aVar instanceof com.d.a.b.e.c)) {
            throw new IllegalArgumentException("ImageAware should wrap ImageView. ImageViewAware is expected.");
        }
        aVar.a(new a(bitmap, this.f963a, this.f964b));
    }

    /* compiled from: RoundedBitmapDisplayer */
    protected static class a extends Drawable {

        /* renamed from: a  reason: collision with root package name */
        protected final float f965a;

        /* renamed from: b  reason: collision with root package name */
        protected final int f966b;
        protected final RectF c = new RectF();
        protected final BitmapShader d;
        protected final Paint e;

        a(Bitmap bitmap, int i, int i2) {
            this.f965a = (float) i;
            this.f966b = i2;
            this.d = new BitmapShader(bitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            this.e = new Paint();
            this.e.setAntiAlias(true);
            this.e.setShader(this.d);
        }

        /* access modifiers changed from: protected */
        public void onBoundsChange(Rect rect) {
            super.onBoundsChange(rect);
            this.c.set((float) this.f966b, (float) this.f966b, (float) (rect.width() - this.f966b), (float) (rect.height() - this.f966b));
        }

        public void draw(Canvas canvas) {
            canvas.drawRoundRect(this.c, this.f965a, this.f965a, this.e);
        }

        public int getOpacity() {
            return -3;
        }

        public void setAlpha(int i) {
            this.e.setAlpha(i);
        }

        public void setColorFilter(ColorFilter colorFilter) {
            this.e.setColorFilter(colorFilter);
        }
    }
}
