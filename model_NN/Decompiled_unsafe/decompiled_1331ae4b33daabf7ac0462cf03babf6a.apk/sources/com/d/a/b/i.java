package com.d.a.b;

import android.graphics.Bitmap;
import android.os.Handler;
import com.d.a.c.c;

/* compiled from: ProcessAndDisplayImageTask */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final f f1000a;

    /* renamed from: b  reason: collision with root package name */
    private final Bitmap f1001b;
    private final g c;
    private final Handler d;

    public i(f fVar, Bitmap bitmap, g gVar, Handler handler) {
        this.f1000a = fVar;
        this.f1001b = bitmap;
        this.c = gVar;
        this.d = handler;
    }

    public void run() {
        if (this.f1000a.f986a.u) {
            c.a("PostProcess image before displaying [%s]", this.c.f991b);
        }
        b bVar = new b(this.c.e.p().a(this.f1001b), this.c, this.f1000a, com.d.a.b.a.i.MEMORY_CACHE);
        bVar.a(this.f1000a.f986a.u);
        if (this.c.e.s()) {
            bVar.run();
        } else {
            this.d.post(bVar);
        }
    }
}
