package com.d.a.b.e;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.d.a.b.a.n;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;

/* compiled from: ImageViewAware */
public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    protected Reference<ImageView> f984a;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f985b;

    public c(ImageView imageView) {
        this(imageView, true);
    }

    public c(ImageView imageView, boolean z) {
        this.f984a = new WeakReference(imageView);
        this.f985b = z;
    }

    public int a() {
        int i = 0;
        ImageView imageView = this.f984a.get();
        if (imageView == null) {
            return 0;
        }
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        if (!(!this.f985b || layoutParams == null || layoutParams.width == -2)) {
            i = imageView.getWidth();
        }
        if (i <= 0 && layoutParams != null) {
            i = layoutParams.width;
        }
        if (i <= 0) {
            return a(imageView, "mMaxWidth");
        }
        return i;
    }

    public int b() {
        int i = 0;
        ImageView imageView = this.f984a.get();
        if (imageView == null) {
            return 0;
        }
        ViewGroup.LayoutParams layoutParams = imageView.getLayoutParams();
        if (!(!this.f985b || layoutParams == null || layoutParams.height == -2)) {
            i = imageView.getHeight();
        }
        if (i <= 0 && layoutParams != null) {
            i = layoutParams.height;
        }
        if (i <= 0) {
            return a(imageView, "mMaxHeight");
        }
        return i;
    }

    public n c() {
        ImageView imageView = this.f984a.get();
        if (imageView != null) {
            return n.a(imageView);
        }
        return null;
    }

    /* renamed from: g */
    public ImageView d() {
        return this.f984a.get();
    }

    public boolean e() {
        return this.f984a.get() == null;
    }

    public int f() {
        ImageView imageView = this.f984a.get();
        return imageView == null ? super.hashCode() : imageView.hashCode();
    }

    private static int a(Object obj, String str) {
        try {
            Field declaredField = ImageView.class.getDeclaredField(str);
            declaredField.setAccessible(true);
            int intValue = ((Integer) declaredField.get(obj)).intValue();
            if (intValue > 0 && intValue < Integer.MAX_VALUE) {
                return intValue;
            }
        } catch (Exception e) {
            com.d.a.c.c.a(e);
        }
        return 0;
    }

    public boolean a(Drawable drawable) {
        ImageView imageView = this.f984a.get();
        if (imageView == null) {
            return false;
        }
        imageView.setImageDrawable(drawable);
        return true;
    }

    public boolean a(Bitmap bitmap) {
        ImageView imageView = this.f984a.get();
        if (imageView == null) {
            return false;
        }
        imageView.setImageBitmap(bitmap);
        return true;
    }
}
