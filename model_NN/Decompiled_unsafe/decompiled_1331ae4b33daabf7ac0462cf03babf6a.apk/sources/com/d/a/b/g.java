package com.d.a.b;

import com.d.a.b.a.e;
import com.d.a.b.a.f;
import com.d.a.b.a.h;
import com.d.a.b.e.a;
import java.util.concurrent.locks.ReentrantLock;

/* compiled from: ImageLoadingInfo */
final class g {

    /* renamed from: a  reason: collision with root package name */
    final String f990a;

    /* renamed from: b  reason: collision with root package name */
    final String f991b;
    final a c;
    final h d;
    final c e;
    final e f;
    final f g;
    final ReentrantLock h;

    public g(String str, a aVar, h hVar, String str2, c cVar, e eVar, f fVar, ReentrantLock reentrantLock) {
        this.f990a = str;
        this.c = aVar;
        this.d = hVar;
        this.e = cVar;
        this.f = eVar;
        this.g = fVar;
        this.h = reentrantLock;
        this.f991b = str2;
    }
}
