package com.d.a.b;

import android.graphics.Bitmap;
import com.d.a.b.a.e;
import com.d.a.b.a.i;
import com.d.a.b.e.a;
import com.d.a.c.c;

/* compiled from: DisplayBitmapTask */
final class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Bitmap f948a;

    /* renamed from: b  reason: collision with root package name */
    private final String f949b;
    private final a c;
    private final String d;
    private final com.d.a.b.c.a e;
    private final e f;
    private final f g;
    private final i h;
    private boolean i;

    public b(Bitmap bitmap, g gVar, f fVar, i iVar) {
        this.f948a = bitmap;
        this.f949b = gVar.f990a;
        this.c = gVar.c;
        this.d = gVar.f991b;
        this.e = gVar.e.q();
        this.f = gVar.f;
        this.g = fVar;
        this.h = iVar;
    }

    public void run() {
        if (this.c.e()) {
            if (this.i) {
                c.a("ImageAware was collected by GC. Task is cancelled. [%s]", this.d);
            }
            this.f.onLoadingCancelled(this.f949b, this.c.d());
        } else if (a()) {
            if (this.i) {
                c.a("ImageAware is reused for another image. Task is cancelled. [%s]", this.d);
            }
            this.f.onLoadingCancelled(this.f949b, this.c.d());
        } else {
            if (this.i) {
                c.a("Display image in ImageAware (loaded from %1$s) [%2$s]", this.h, this.d);
            }
            this.e.display(this.f948a, this.c, this.h);
            this.f.onLoadingComplete(this.f949b, this.c.d(), this.f948a);
            this.g.b(this.c);
        }
    }

    private boolean a() {
        return !this.d.equals(this.g.a(this.c));
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.i = z;
    }
}
