package com.d.a.a.b;

import java.util.Collection;

/* compiled from: MemoryCacheAware */
public interface b<K, V> {
    V a(Object obj);

    Collection<K> a();

    boolean a(K k, V v);

    void b(K k);
}
