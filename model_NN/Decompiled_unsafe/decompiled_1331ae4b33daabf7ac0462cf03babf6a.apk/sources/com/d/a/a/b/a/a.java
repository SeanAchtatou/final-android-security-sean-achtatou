package com.d.a.a.b.a;

import com.d.a.a.b.b;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

/* compiled from: FuzzyKeyMemoryCache */
public class a<K, V> implements b<K, V> {

    /* renamed from: a  reason: collision with root package name */
    private final b<K, V> f917a;

    /* renamed from: b  reason: collision with root package name */
    private final Comparator<K> f918b;

    public a(b<K, V> bVar, Comparator<K> comparator) {
        this.f917a = bVar;
        this.f918b = comparator;
    }

    public boolean a(K k, V v) {
        K k2;
        synchronized (this.f917a) {
            Iterator<K> it = this.f917a.a().iterator();
            while (true) {
                if (!it.hasNext()) {
                    k2 = null;
                    break;
                }
                k2 = it.next();
                if (this.f918b.compare(k, k2) == 0) {
                    break;
                }
            }
            if (k2 != null) {
                this.f917a.b(k2);
            }
        }
        return this.f917a.a(k, v);
    }

    public V a(K k) {
        return this.f917a.a(k);
    }

    public void b(K k) {
        this.f917a.b(k);
    }

    public Collection<K> a() {
        return this.f917a.a();
    }
}
