package com.d.a.a.a;

import com.d.a.a.a.b.a;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: LimitedDiscCache */
public abstract class c extends a {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final AtomicInteger f914b;
    private final int c;
    /* access modifiers changed from: private */
    public final Map<File, Long> d = Collections.synchronizedMap(new HashMap());

    /* access modifiers changed from: protected */
    public abstract int a(File file);

    public c(File file, a aVar, int i) {
        super(file, aVar);
        this.c = i;
        this.f914b = new AtomicInteger();
        a();
    }

    private void a() {
        new Thread(new Runnable() {
            public void run() {
                File[] listFiles = c.this.f911a.listFiles();
                if (listFiles != null) {
                    int i = 0;
                    for (File file : listFiles) {
                        i += c.this.a(file);
                        c.this.d.put(file, Long.valueOf(file.lastModified()));
                    }
                    c.this.f914b.set(i);
                }
            }
        }).start();
    }

    public void a(String str, File file) {
        int b2;
        int a2 = a(file);
        int i = this.f914b.get();
        while (i + a2 > this.c && (b2 = b()) != -1) {
            i = this.f914b.addAndGet(-b2);
        }
        this.f914b.addAndGet(a2);
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        file.setLastModified(valueOf.longValue());
        this.d.put(file, valueOf);
    }

    public File a(String str) {
        File a2 = super.a(str);
        Long valueOf = Long.valueOf(System.currentTimeMillis());
        a2.setLastModified(valueOf.longValue());
        this.d.put(a2, valueOf);
        return a2;
    }

    private int b() {
        File file;
        Long l;
        File file2 = null;
        if (this.d.isEmpty()) {
            return -1;
        }
        Set<Map.Entry<File, Long>> entrySet = this.d.entrySet();
        synchronized (this.d) {
            Long l2 = null;
            for (Map.Entry next : entrySet) {
                if (file2 == null) {
                    file = (File) next.getKey();
                    l = (Long) next.getValue();
                } else {
                    Long l3 = (Long) next.getValue();
                    if (l3.longValue() < l2.longValue()) {
                        File file3 = (File) next.getKey();
                        l = l3;
                        file = file3;
                    } else {
                        file = file2;
                        l = l2;
                    }
                }
                file2 = file;
                l2 = l;
            }
        }
        if (file2 == null) {
            return 0;
        }
        if (file2.exists()) {
            int a2 = a(file2);
            if (!file2.delete()) {
                return a2;
            }
            this.d.remove(file2);
            return a2;
        }
        this.d.remove(file2);
        return 0;
    }
}
