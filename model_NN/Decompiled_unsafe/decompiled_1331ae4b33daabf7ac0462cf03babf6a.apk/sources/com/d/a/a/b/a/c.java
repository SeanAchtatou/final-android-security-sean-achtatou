package com.d.a.a.b.a;

import android.graphics.Bitmap;
import com.d.a.a.b.a;
import java.lang.ref.Reference;
import java.lang.ref.WeakReference;

/* compiled from: WeakMemoryCache */
public class c extends a<String, Bitmap> {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Reference<Bitmap> c(Bitmap bitmap) {
        return new WeakReference(bitmap);
    }
}
