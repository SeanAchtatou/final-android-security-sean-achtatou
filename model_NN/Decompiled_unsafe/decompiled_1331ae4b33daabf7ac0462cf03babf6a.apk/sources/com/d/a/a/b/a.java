package com.d.a.a.b;

import java.lang.ref.Reference;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/* compiled from: BaseMemoryCache */
public abstract class a<K, V> implements b<K, V> {

    /* renamed from: a  reason: collision with root package name */
    private final Map<K, Reference<V>> f916a = Collections.synchronizedMap(new HashMap());

    /* access modifiers changed from: protected */
    public abstract Reference<V> c(V v);

    public V a(Object obj) {
        Reference reference = this.f916a.get(obj);
        if (reference != null) {
            return reference.get();
        }
        return null;
    }

    public boolean a(K k, V v) {
        this.f916a.put(k, c(v));
        return true;
    }

    public void b(K k) {
        this.f916a.remove(k);
    }

    public Collection<K> a() {
        HashSet hashSet;
        synchronized (this.f916a) {
            hashSet = new HashSet(this.f916a.keySet());
        }
        return hashSet;
    }
}
