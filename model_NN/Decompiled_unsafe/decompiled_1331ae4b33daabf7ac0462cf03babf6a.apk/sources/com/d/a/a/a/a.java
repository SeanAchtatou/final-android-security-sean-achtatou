package com.d.a.a.a;

import java.io.File;

/* compiled from: BaseDiscCache */
public abstract class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected File f911a;

    /* renamed from: b  reason: collision with root package name */
    private com.d.a.a.a.b.a f912b;

    public a(File file, com.d.a.a.a.b.a aVar) {
        if (file == null) {
            throw new IllegalArgumentException(String.format("\"%s\" argument must be not null", "cacheDir"));
        } else if (aVar == null) {
            throw new IllegalArgumentException(String.format("\"%s\" argument must be not null", "fileNameGenerator"));
        } else {
            this.f911a = file;
            this.f912b = aVar;
        }
    }

    public File a(String str) {
        return new File(this.f911a, this.f912b.generate(str));
    }
}
