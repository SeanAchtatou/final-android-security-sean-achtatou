package com.b.a.a.a;

import java.io.IOException;

public class ad extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f796a;

    ad(ac acVar, Exception exc) {
        super(new StringBuffer().append(acVar).append(" ").append(exc).toString());
        this.f796a = null;
        this.f796a = exc;
    }

    public ad(ac acVar, String str) {
        super(new StringBuffer().append(acVar).append(" ").append(str).toString());
        this.f796a = null;
    }

    ad(ac acVar, String str, s sVar, String str2) {
        this(acVar, new StringBuffer().append(str).append(" got \"").append(a(sVar)).append("\" instead of expected ").append(str2).toString());
    }

    private static String a(s sVar) {
        try {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(b(sVar));
            if (sVar.f804a != -1) {
                sVar.a();
                stringBuffer.append(b(sVar));
                sVar.b();
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            return new StringBuffer().append("(cannot get  info: ").append(e).append(")").toString();
        }
    }

    private static String b(s sVar) {
        switch (sVar.f804a) {
            case -3:
                return sVar.c;
            case -2:
                return new StringBuffer().append(sVar.f805b).append("").toString();
            case -1:
                return "<end of expression>";
            default:
                return new StringBuffer().append((char) sVar.f804a).append("").toString();
        }
    }

    public Throwable getCause() {
        return this.f796a;
    }
}
