package com.b.a.a;

import java.util.Hashtable;

public class r {

    /* renamed from: a  reason: collision with root package name */
    private static d f827a = new s();

    /* renamed from: b  reason: collision with root package name */
    private static b f828b = new t();

    public interface a {
    }

    public interface b {
        a a();
    }

    private static class c extends Hashtable implements a {
        private c() {
        }

        c(s sVar) {
            this();
        }
    }

    public interface d {
        String a(String str);
    }

    static a a() {
        return f828b.a();
    }

    public static String a(String str) {
        return f827a.a(str);
    }
}
