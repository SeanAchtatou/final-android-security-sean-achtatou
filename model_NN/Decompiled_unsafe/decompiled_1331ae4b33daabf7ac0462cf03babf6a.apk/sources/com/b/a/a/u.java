package com.b.a.a;

import java.io.IOException;
import java.io.Writer;

public class u extends i {

    /* renamed from: a  reason: collision with root package name */
    private StringBuffer f830a;

    public u(String str) {
        this.f830a = new StringBuffer(str);
    }

    public String a() {
        return this.f830a.toString();
    }

    /* access modifiers changed from: package-private */
    public void a(Writer writer) throws IOException {
        writer.write(this.f830a.toString());
    }

    public void a(char[] cArr, int i, int i2) {
        this.f830a.append(cArr, i, i2);
        b();
    }

    /* access modifiers changed from: package-private */
    public void b(Writer writer) throws IOException {
        String stringBuffer = this.f830a.toString();
        if (stringBuffer.length() < 50) {
            i.a(writer, stringBuffer);
            return;
        }
        writer.write("<![CDATA[");
        writer.write(stringBuffer);
        writer.write("]]>");
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.f830a.toString().hashCode();
    }

    public Object clone() {
        return new u(this.f830a.toString());
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u)) {
            return false;
        }
        return this.f830a.toString().equals(((u) obj).f830a.toString());
    }
}
