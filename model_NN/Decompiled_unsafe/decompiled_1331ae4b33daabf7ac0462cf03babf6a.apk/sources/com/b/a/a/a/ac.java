package com.b.a.a.a;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

public class ac {
    private static Hashtable d = new Hashtable();

    /* renamed from: a  reason: collision with root package name */
    private Stack f794a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f795b;
    private String c;

    private ac(String str) throws ad {
        this(str, new InputStreamReader(new ByteArrayInputStream(str.getBytes())));
    }

    private ac(String str, Reader reader) throws ad {
        boolean z;
        boolean z2;
        this.f794a = new Stack();
        try {
            this.c = str;
            s sVar = new s(reader);
            sVar.a('/');
            sVar.a('.');
            sVar.a(':', ':');
            sVar.a('_', '_');
            if (sVar.a() == 47) {
                this.f795b = true;
                if (sVar.a() == 47) {
                    sVar.a();
                    z = true;
                } else {
                    z = false;
                }
            } else {
                this.f795b = false;
                z = false;
            }
            this.f794a.push(new t(this, z, sVar));
            while (sVar.f804a == 47) {
                if (sVar.a() == 47) {
                    sVar.a();
                    z2 = true;
                } else {
                    z2 = false;
                }
                this.f794a.push(new t(this, z2, sVar));
            }
            if (sVar.f804a != -1) {
                throw new ad(this, "at end of XPATH expression", sVar, "end of expression");
            }
        } catch (IOException e) {
            throw new ad(this, e);
        }
    }

    private ac(boolean z, t[] tVarArr) {
        this.f794a = new Stack();
        for (t addElement : tVarArr) {
            this.f794a.addElement(addElement);
        }
        this.f795b = z;
        this.c = null;
    }

    public static ac a(String str) throws ad {
        ac acVar;
        synchronized (d) {
            acVar = (ac) d.get(str);
            if (acVar == null) {
                acVar = new ac(str);
                d.put(str, acVar);
            }
        }
        return acVar;
    }

    private String d() {
        StringBuffer stringBuffer = new StringBuffer();
        boolean z = true;
        Enumeration elements = this.f794a.elements();
        while (true) {
            boolean z2 = z;
            if (!elements.hasMoreElements()) {
                return stringBuffer.toString();
            }
            t tVar = (t) elements.nextElement();
            if (!z2 || this.f795b) {
                stringBuffer.append('/');
                if (tVar.a()) {
                    stringBuffer.append('/');
                }
            }
            stringBuffer.append(tVar.toString());
            z = false;
        }
    }

    public boolean a() {
        return this.f795b;
    }

    public boolean b() {
        return ((t) this.f794a.peek()).b();
    }

    public Enumeration c() {
        return this.f794a.elements();
    }

    public Object clone() {
        t[] tVarArr = new t[this.f794a.size()];
        Enumeration elements = this.f794a.elements();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= tVarArr.length) {
                return new ac(this.f795b, tVarArr);
            }
            tVarArr[i2] = (t) elements.nextElement();
            i = i2 + 1;
        }
    }

    public String toString() {
        if (this.c == null) {
            this.c = d();
        }
        return this.c;
    }
}
