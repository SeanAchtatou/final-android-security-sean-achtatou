package com.b.a.a.a;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.IOException;

public class t {

    /* renamed from: a  reason: collision with root package name */
    public static t f806a = new t(z.f811a, aa.f793a);

    /* renamed from: b  reason: collision with root package name */
    private final o f807b;
    private final k c;
    private final boolean d;

    t(ac acVar, boolean z, s sVar) throws ad, IOException {
        this.d = z;
        switch (sVar.f804a) {
            case -3:
                if (sVar.c.equals("text")) {
                    if (sVar.a() == 40 && sVar.a() == 41) {
                        this.f807b = y.f810a;
                        break;
                    } else {
                        throw new ad(acVar, "after text", sVar, "()");
                    }
                } else {
                    this.f807b = new m(sVar.c);
                    break;
                }
                break;
            case 42:
                this.f807b = a.f792a;
                break;
            case 46:
                if (sVar.a() != 46) {
                    sVar.b();
                    this.f807b = z.f811a;
                    break;
                } else {
                    this.f807b = q.f802a;
                    break;
                }
            case 64:
                if (sVar.a() == -3) {
                    this.f807b = new j(sVar.c);
                    break;
                } else {
                    throw new ad(acVar, "after @ in node test", sVar, SelectCountryActivity.EXTRA_COUNTRY_NAME);
                }
            default:
                throw new ad(acVar, "at begininning of step", sVar, "'.' or '*' or name");
        }
        if (sVar.a() == 91) {
            sVar.a();
            this.c = n.a(acVar, sVar);
            if (sVar.f804a != 93) {
                throw new ad(acVar, "after predicate expression", sVar, "]");
            }
            sVar.a();
            return;
        }
        this.c = aa.f793a;
    }

    t(o oVar, k kVar) {
        this.f807b = oVar;
        this.c = kVar;
        this.d = false;
    }

    public boolean a() {
        return this.d;
    }

    public boolean b() {
        return this.f807b.a();
    }

    public o c() {
        return this.f807b;
    }

    public k d() {
        return this.c;
    }

    public String toString() {
        return new StringBuffer().append(this.f807b.toString()).append(this.c.toString()).toString();
    }
}
