package com.b.a.a;

class b {

    /* renamed from: a  reason: collision with root package name */
    private final int[] f812a;

    /* renamed from: b  reason: collision with root package name */
    private int f813b;
    private int c;

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer((this.f812a.length * 11) / 10);
        for (int length = this.c < this.f812a.length ? this.f812a.length - this.c : 0; length < this.f812a.length; length++) {
            int i = this.f812a[(this.f813b + length) % this.f812a.length];
            if (i < 65536) {
                stringBuffer.append((char) i);
            } else {
                stringBuffer.append(Integer.toString(i - 65536));
            }
        }
        return stringBuffer.toString();
    }
}
