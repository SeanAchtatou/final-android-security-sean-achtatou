package com.ffcs.inapppaylib.bean.response;

import java.io.Serializable;

public class UserPackage implements Serializable {
    private String count_down_num;
    private String order_time;
    private String package_id;
    private int status;
    private String unsubscribe_time;

    public String getPackage_id() {
        return this.package_id;
    }

    public void setPackage_id(String str) {
        this.package_id = str;
    }

    public String getCount_down_num() {
        return this.count_down_num;
    }

    public void setCount_down_num(String str) {
        this.count_down_num = str;
    }

    public String getOrder_time() {
        return this.order_time;
    }

    public void setOrder_time(String str) {
        this.order_time = str;
    }

    public String getUnsubscribe_time() {
        return this.unsubscribe_time;
    }

    public void setUnsubscribe_time(String str) {
        this.unsubscribe_time = str;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int i) {
        this.status = i;
    }
}
