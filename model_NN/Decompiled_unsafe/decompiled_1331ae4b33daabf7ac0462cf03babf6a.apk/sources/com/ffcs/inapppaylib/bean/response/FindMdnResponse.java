package com.ffcs.inapppaylib.bean.response;

public class FindMdnResponse extends EmpResponse {
    private String imsi;
    private String mdn;

    public String getMdn() {
        return this.mdn;
    }

    public void setMdn(String str) {
        this.mdn = str;
    }

    public String getImsi() {
        return this.imsi;
    }

    public void setImsi(String str) {
        this.imsi = str;
    }
}
