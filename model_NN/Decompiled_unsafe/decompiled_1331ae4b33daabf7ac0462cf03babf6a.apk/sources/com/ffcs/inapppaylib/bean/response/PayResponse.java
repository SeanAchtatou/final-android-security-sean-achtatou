package com.ffcs.inapppaylib.bean.response;

public class PayResponse extends BaseResponse {
    private String app_name;
    private String order_no;
    private String pay_code;
    private String pay_code_name;
    private String phone;
    private String price;
    private String sp_name;
    private String verify_code;

    public String getOrder_no() {
        return this.order_no;
    }

    public void setOrder_no(String str) {
        this.order_no = str;
    }

    public String getApp_name() {
        return this.app_name;
    }

    public void setApp_name(String str) {
        this.app_name = str;
    }

    public String getSp_name() {
        return this.sp_name;
    }

    public void setSp_name(String str) {
        this.sp_name = str;
    }

    public String getPay_code() {
        return this.pay_code;
    }

    public void setPay_code(String str) {
        this.pay_code = str;
    }

    public String getPay_code_name() {
        return this.pay_code_name;
    }

    public void setPay_code_name(String str) {
        this.pay_code_name = str;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setPhone(String str) {
        this.phone = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getVerify_code() {
        return this.verify_code;
    }

    public void setVerify_code(String str) {
        this.verify_code = str;
    }
}
