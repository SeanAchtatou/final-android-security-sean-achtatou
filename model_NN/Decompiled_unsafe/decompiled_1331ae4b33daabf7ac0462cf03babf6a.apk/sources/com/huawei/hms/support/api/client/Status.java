package com.huawei.hms.support.api.client;

import android.app.Activity;
import android.app.PendingIntent;
import cn.banshenggua.aichang.utils.Constants;
import java.util.Arrays;

public final class Status {
    public static final Status CoreException = new Status(Constants.textWatch_result);
    public static final Status MessageNotFound = new Status(404);
    public static final Status SUCCESS = new Status(0);

    /* renamed from: a  reason: collision with root package name */
    private int f1045a;

    /* renamed from: b  reason: collision with root package name */
    private String f1046b;
    private PendingIntent c;

    public Status(int i) {
        this(i, null);
    }

    public Status(int i, String str) {
        this(i, str, null);
    }

    public Status(int i, String str, PendingIntent pendingIntent) {
        this.f1045a = i;
        this.f1046b = str;
        this.c = pendingIntent;
    }

    public static boolean equal(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    public boolean equals(Object obj) {
        if (obj instanceof Status) {
            Status status = (Status) obj;
            return this.f1045a == status.f1045a && equal(this.f1046b, status.f1046b) && equal(this.c, status.c);
        }
    }

    public PendingIntent getResolution() {
        return this.c;
    }

    public int getStatusCode() {
        return this.f1045a;
    }

    public String getStatusMessage() {
        return this.f1046b;
    }

    public boolean hasResolution() {
        return this.c != null;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{Integer.valueOf(this.f1045a), this.f1046b, this.c});
    }

    public boolean isSuccess() {
        return this.f1045a <= 0;
    }

    public void startResolutionForResult(Activity activity, int i) {
        if (hasResolution()) {
            activity.startIntentSenderForResult(this.c.getIntentSender(), i, null, 0, 0, 0);
        }
    }

    public String toString() {
        return getClass().getName() + " {\n\tstatusCode: " + this.f1045a + "\n\tstatusMessage: " + this.f1046b + "\n\tmPendingIntent: " + this.c + "\n}";
    }
}
