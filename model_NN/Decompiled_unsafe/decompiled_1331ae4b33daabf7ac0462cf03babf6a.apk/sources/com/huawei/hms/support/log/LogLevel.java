package com.huawei.hms.support.log;

import android.util.SparseArray;
import java.util.HashMap;
import java.util.Map;

public enum LogLevel {
    ALL(0),
    VERBOSE(2),
    DEBUG(3),
    INFO(4),
    WARN(5),
    ERROR(6),
    ASSERT(7),
    OUT(100),
    NONE(255);
    

    /* renamed from: a  reason: collision with root package name */
    private static final SparseArray<LogLevel> f1065a = new SparseArray<>();

    /* renamed from: b  reason: collision with root package name */
    private static final Map<String, LogLevel> f1066b = new HashMap();
    private final int c;

    static {
        for (LogLevel logLevel : values()) {
            f1065a.put(logLevel.value(), logLevel);
            f1066b.put(logLevel.name(), logLevel);
        }
    }

    private LogLevel(int i) {
        this.c = i;
    }

    public static LogLevel get(int i) {
        return f1065a.get(i);
    }

    public static LogLevel get(String str) {
        return f1066b.get(str);
    }

    public boolean isEnable(LogLevel logLevel) {
        return logLevel.c <= this.c;
    }

    public int value() {
        return this.c;
    }
}
