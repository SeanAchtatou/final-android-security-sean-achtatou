package com.huawei.hms.support.api.push;

import android.content.Context;
import android.text.TextUtils;
import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.support.api.ResolvePendingResult;
import com.huawei.hms.support.api.client.ApiClient;
import com.huawei.hms.support.api.client.PendingResult;
import com.huawei.hms.support.api.client.Status;
import com.huawei.hms.support.api.entity.push.DeleteTokenReq;
import com.huawei.hms.support.api.entity.push.DeleteTokenResp;
import com.huawei.hms.support.api.entity.push.EnableNotifyReq;
import com.huawei.hms.support.api.entity.push.EnableNotifyResp;
import com.huawei.hms.support.api.entity.push.GetTagsReq;
import com.huawei.hms.support.api.entity.push.GetTagsResp;
import com.huawei.hms.support.api.entity.push.PushNaming;
import com.huawei.hms.support.api.entity.push.PushStateReq;
import com.huawei.hms.support.api.entity.push.PushStateResp;
import com.huawei.hms.support.api.entity.push.TagsReq;
import com.huawei.hms.support.api.entity.push.TagsResp;
import com.huawei.hms.support.api.entity.push.TokenReq;
import com.huawei.hms.support.api.entity.push.TokenResp;
import com.huawei.hms.support.api.push.HmsPushConst;
import com.huawei.hms.support.api.push.a.a.d;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class HuaweiPushApiImp implements HuaweiPushApi {

    class a extends com.huawei.hms.support.api.a<GetTagResult, GetTagsResp> {

        /* renamed from: a  reason: collision with root package name */
        private ApiClient f1049a;

        public a(ApiClient apiClient, String str, IMessageEntity iMessageEntity) {
            super(apiClient, str, iMessageEntity);
            this.f1049a = apiClient;
        }

        /* renamed from: a */
        public GetTagResult onComplete(GetTagsResp getTagsResp) {
            GetTagResult getTagResult = new GetTagResult();
            try {
                getTagsResp.setTags(new d(this.f1049a.getContext(), "tags_info").a());
                getTagResult.setTagsRes(getTagsResp);
                getTagResult.setStatus(Status.SUCCESS);
            } catch (Exception e) {
                if (com.huawei.hms.support.api.push.a.b.e()) {
                    com.huawei.hms.support.api.push.a.b.d("HuaweiPushApiImp", "get tags failed, error:" + e.getMessage());
                }
                getTagResult.setTagsRes(getTagsResp);
                getTagResult.setStatus(new Status(HmsPushConst.ErrorCode.REPORT_SYSTEM_ERROR));
            }
            return getTagResult;
        }
    }

    class b extends com.huawei.hms.support.api.a<TokenResult, TokenResp> {

        /* renamed from: a  reason: collision with root package name */
        private ApiClient f1050a;

        public b(ApiClient apiClient, String str, IMessageEntity iMessageEntity) {
            super(apiClient, str, iMessageEntity);
            this.f1050a = apiClient;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.String):boolean
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public TokenResult onError(int i) {
            TokenResult tokenResult = new TokenResult();
            tokenResult.setStatus(new Status(i));
            new d(this.f1050a.getContext(), "push_client_self_info").a("hasRequestToken", false);
            return tokenResult;
        }

        /* renamed from: a */
        public TokenResult onComplete(TokenResp tokenResp) {
            TokenResult tokenResult = new TokenResult();
            if (com.huawei.hms.support.api.push.a.b.c()) {
                com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "get token complete, the return code:" + tokenResp.getRetCode());
            }
            tokenResult.setStatus(new Status(tokenResp.getRetCode()));
            tokenResult.setTokenRes(tokenResp);
            return tokenResult;
        }
    }

    class c extends com.huawei.hms.support.api.a<HandleTagsResult, TagsResp> {

        /* renamed from: a  reason: collision with root package name */
        private ApiClient f1051a;

        public c(ApiClient apiClient, String str, IMessageEntity iMessageEntity) {
            super(apiClient, str, iMessageEntity);
            this.f1051a = apiClient;
        }

        /* renamed from: a */
        public HandleTagsResult onComplete(TagsResp tagsResp) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "report tag completely, retcode is:" + tagsResp.getRetCode());
            }
            if (907122001 == tagsResp.getRetCode()) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "report tag success.");
                }
                HuaweiPushApiImp.b(this.f1051a, tagsResp.getContent());
            }
            HandleTagsResult handleTagsResult = new HandleTagsResult();
            handleTagsResult.setStatus(new Status(tagsResp.getRetCode()));
            handleTagsResult.setTagsRes(tagsResp);
            return handleTagsResult;
        }
    }

    private PendingResult<HandleTagsResult> a(ApiClient apiClient, String str, long j, int i) {
        Context context = apiClient.getContext();
        String a2 = com.huawei.hms.support.api.push.a.a.c.a(context, "push_client_self_info", "token_info");
        if (TextUtils.isEmpty(a2)) {
            if (com.huawei.hms.support.api.push.a.b.d()) {
                com.huawei.hms.support.api.push.a.b.c("HuaweiPushApiImp", "token is null, should register a token first. error code:[907122004]");
            }
            throw new PushException(PushException.EXCEPITON_TOKEN_INVALID);
        }
        TagsReq tagsReq = new TagsReq();
        tagsReq.setContent(str);
        tagsReq.setCycle(j);
        tagsReq.setOperType(1);
        tagsReq.setPlusType(i);
        tagsReq.setToken(a2);
        tagsReq.setPkgName(context.getPackageName());
        tagsReq.setApkVersion(com.huawei.hms.support.api.push.a.a.b(context));
        return new c(apiClient, PushNaming.setTags, tagsReq);
    }

    private static Map<String, String> a(Context context, Map<String, String> map) {
        HashMap hashMap = new HashMap();
        d dVar = new d(context, "tags_info");
        for (Map.Entry next : map.entrySet()) {
            String str = (String) next.getKey();
            String str2 = (String) next.getValue();
            if (dVar.c(str)) {
                String b2 = dVar.b(str);
                if (!TextUtils.isEmpty(str2) && str2.equals(b2)) {
                    if (com.huawei.hms.support.api.push.a.b.b()) {
                        com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "tag has reported:" + next);
                    }
                }
            }
            hashMap.put(str, str2);
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.String):boolean
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean */
    /* access modifiers changed from: private */
    public static void b(ApiClient apiClient, String str) {
        if (apiClient != null) {
            try {
                JSONArray a2 = com.huawei.hms.support.api.push.a.a.a.a(str);
                if (a2 != null) {
                    d dVar = new d(apiClient.getContext(), "tags_info");
                    int length = a2.length();
                    for (int i = 0; i < length; i++) {
                        JSONObject optJSONObject = a2.optJSONObject(i);
                        if (optJSONObject != null) {
                            String optString = optJSONObject.optString("tagKey");
                            int optInt = optJSONObject.optInt("opType");
                            if (1 == optInt) {
                                dVar.a(optString, (Object) optJSONObject.optString("tagValue"));
                            } else if (2 == optInt) {
                                dVar.d(optString);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                if (com.huawei.hms.support.api.push.a.b.d()) {
                    com.huawei.hms.support.api.push.a.b.c("HuaweiPushApiImp", "when adding or deleting tags from file excepiton," + e.getMessage());
                }
            }
        } else if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "the client is null when adding or deleting tags from file.");
        }
    }

    public PendingResult<HandleTagsResult> deleteTags(ApiClient apiClient, List<String> list) {
        if (com.huawei.hms.support.api.push.a.b.c()) {
            com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "invoke method: deleteTags");
        }
        if (list != null) {
            try {
                if (!list.isEmpty()) {
                    Context context = apiClient.getContext();
                    if (com.huawei.hms.support.api.push.a.b.b()) {
                        com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "delete tags, pkgName:" + context.getPackageName());
                    }
                    JSONArray jSONArray = new JSONArray();
                    d dVar = new d(context, "tags_info");
                    for (String next : list) {
                        if (dVar.c(next)) {
                            JSONObject jSONObject = new JSONObject();
                            jSONObject.put("tagKey", next);
                            jSONObject.put("opType", 2);
                            if (jSONObject.length() > 0) {
                                jSONArray.put(jSONObject);
                            }
                        } else if (com.huawei.hms.support.api.push.a.b.b()) {
                            com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", next + " not exist, need not to remove");
                        }
                    }
                    if (jSONArray.length() > 0) {
                        if (com.huawei.hms.support.api.push.a.b.b()) {
                            com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "begin to deleTags: " + jSONArray.toString());
                        }
                        return a(apiClient, jSONArray.toString(), 0, 2);
                    }
                    if (com.huawei.hms.support.api.push.a.b.b()) {
                        com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", PushException.EXCEPITON_NO_TAG_NEED_DEL);
                    }
                    throw new PushException(PushException.EXCEPITON_NO_TAG_NEED_DEL);
                }
            } catch (Exception e) {
                if (com.huawei.hms.support.api.push.a.b.d()) {
                    com.huawei.hms.support.api.push.a.b.c("HuaweiPushApiImp", "delete tag error: " + e.getMessage());
                }
                throw new PushException(PushException.EXCEPITON_DEL_TAGS_FAILED, e);
            }
        }
        if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "the key list is null");
        }
        throw new PushException(PushException.EXCEPITON_DEL_TAGS_LIST_NULL);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.String):boolean
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void */
    public void deleteToken(ApiClient apiClient, String str) {
        Context context = apiClient.getContext();
        if (com.huawei.hms.support.api.push.a.b.c()) {
            com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "invoke method: deleteToken, pkgName:" + context.getPackageName() + com.huawei.hms.support.api.push.a.a.a.c.a(str));
        }
        if (TextUtils.isEmpty(str)) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "token is null, can not deregister token");
            }
            throw new PushException(PushException.EXCEPITON_TOKEN_INVALID);
        }
        try {
            d dVar = new d(context, "push_client_self_info");
            if (str.equals(com.huawei.hms.support.api.push.a.a.c.a(context, "push_client_self_info", "token_info"))) {
                dVar.a("hasRequestToken", false);
                com.huawei.hms.support.api.push.a.a.c.b(context, "push_client_self_info", "token_info");
            }
            DeleteTokenReq deleteTokenReq = new DeleteTokenReq();
            deleteTokenReq.setPkgName(context.getPackageName());
            deleteTokenReq.setToken(str);
            ResolvePendingResult.build(apiClient, PushNaming.deleteToken, deleteTokenReq, DeleteTokenResp.class).get();
        } catch (Exception e) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "delete token failed, e=" + e.getMessage());
            }
            throw new PushException(e + PushException.EXCEPITON_DEL_TOKEN_FAILED);
        }
    }

    public void enableReceiveNormalMsg(ApiClient apiClient, boolean z) {
        if (com.huawei.hms.support.api.push.a.b.c()) {
            com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "invoke enableReceiveNormalMsg, set flag:" + z);
        }
        new d(apiClient.getContext(), "push_switch").a("normal_msg_enable", !z);
    }

    public void enableReceiveNotifyMsg(ApiClient apiClient, boolean z) {
        if (com.huawei.hms.support.api.push.a.b.c()) {
            com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "invoke enableReceiveNotifyMsg, set flag:" + z);
        }
        new d(apiClient.getContext(), "push_switch").a("notify_msg_enable", !z);
        EnableNotifyReq enableNotifyReq = new EnableNotifyReq();
        enableNotifyReq.setPackageName(apiClient.getPackageName());
        enableNotifyReq.setEnable(z);
        ResolvePendingResult.build(apiClient, PushNaming.setNotifyFlag, enableNotifyReq, EnableNotifyResp.class).get();
    }

    public boolean getPushState(ApiClient apiClient) {
        PushStateReq pushStateReq = new PushStateReq();
        pushStateReq.setPkgName(apiClient.getPackageName());
        ResolvePendingResult.build(apiClient, PushNaming.getPushState, pushStateReq, PushStateResp.class).get();
        return true;
    }

    public PendingResult<GetTagResult> getTags(ApiClient apiClient) {
        return new a(apiClient, PushNaming.getTags, new GetTagsReq());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.String):boolean
      com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void */
    public PendingResult<TokenResult> getToken(ApiClient apiClient) {
        Context context = apiClient.getContext();
        if (com.huawei.hms.support.api.push.a.b.c()) {
            com.huawei.hms.support.api.push.a.b.b("HuaweiPushApiImp", "get token, pkgName:" + context.getPackageName());
        }
        new d(context, "push_client_self_info").a("hasRequestToken", true);
        TokenReq tokenReq = new TokenReq();
        tokenReq.setPackageName(apiClient.getPackageName());
        return new b(apiClient, PushNaming.getToken, tokenReq);
    }

    public PendingResult<HandleTagsResult> setTags(ApiClient apiClient, Map<String, String> map) {
        if (map == null) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", PushException.EXCEPITON_TAGS_NULL);
            }
            throw new PushException(PushException.EXCEPITON_TAGS_NULL);
        }
        Context context = apiClient.getContext();
        if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "set tags, pkgName:" + context.getPackageName());
        }
        Map<String, String> a2 = a(context, map);
        try {
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry next : a2.entrySet()) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("tagKey", (String) next.getKey());
                jSONObject.put("tagValue", (String) next.getValue());
                jSONObject.put("opType", 1);
                if (jSONObject.length() > 0) {
                    jSONArray.put(jSONObject);
                }
            }
            if (jSONArray.length() > 0) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "begin to setTags: " + jSONArray.toString());
                }
                return a(apiClient, jSONArray.toString(), 0, 2);
            }
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "no tag need to upload");
            }
            throw new PushException(PushException.EXCEPITON_TAGS_NULL);
        } catch (Exception e) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("HuaweiPushApiImp", "set tags exception," + e.toString());
            }
            throw new PushException(e + PushException.EXCEPITON_SET_TAGS_FAILED);
        }
    }
}
