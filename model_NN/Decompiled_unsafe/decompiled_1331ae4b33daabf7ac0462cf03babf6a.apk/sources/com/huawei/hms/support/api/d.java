package com.huawei.hms.support.api;

import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.support.api.a;
import com.huawei.hms.support.api.client.ResultCallback;
import com.huawei.hms.support.api.transport.DatagramTransport;

class d implements DatagramTransport.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a.C0018a f1047a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ResultCallback f1048b;
    final /* synthetic */ a c;

    d(a aVar, a.C0018a aVar2, ResultCallback resultCallback) {
        this.c = aVar;
        this.f1047a = aVar2;
        this.f1048b = resultCallback;
    }

    public void a(int i, IMessageEntity iMessageEntity) {
        this.c.a(i, iMessageEntity);
        this.f1047a.a(this.f1048b, this.c.f1041b);
    }
}
