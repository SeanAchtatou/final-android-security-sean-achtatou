package com.huawei.hms.support.api;

import com.huawei.hms.support.api.client.Result;

public class ResolveResult<T> extends Result {

    /* renamed from: a  reason: collision with root package name */
    private T f1039a;

    public ResolveResult() {
        this.f1039a = null;
    }

    public ResolveResult(T t) {
        this.f1039a = t;
    }

    public T getValue() {
        return this.f1039a;
    }
}
