package com.huawei.hms.support.log;

import android.content.Context;
import com.huawei.hms.support.api.push.a.b;

public final class HMSDebugger {

    /* renamed from: a  reason: collision with root package name */
    private static HMSDebugger f1064a = new HMSDebugger();

    private HMSDebugger() {
    }

    public static HMSDebugger log(Context context, LogLevel logLevel) {
        return log(context.getFilesDir().getAbsolutePath() + "/Log/", logLevel);
    }

    public static HMSDebugger log(String str, LogLevel logLevel) {
        c.a(str, logLevel, "HMSSDK");
        return f1064a;
    }

    public HMSDebugger on() {
        pushOn();
        return this;
    }

    public HMSDebugger pushOn() {
        b.a();
        return this;
    }
}
