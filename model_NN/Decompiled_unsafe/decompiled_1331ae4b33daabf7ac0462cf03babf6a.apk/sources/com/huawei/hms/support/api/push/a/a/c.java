package com.huawei.hms.support.api.push.a.a;

import android.content.Context;
import android.text.TextUtils;
import com.huawei.hms.support.api.push.a.a.a.d;
import com.huawei.hms.support.api.push.a.b;

public abstract class c {
    public static String a(Context context, String str, String str2) {
        String str3 = "";
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                str3 = d.b(context, new d(context, str).b(str2 + "_v2"));
            } catch (Exception e) {
                if (b.e()) {
                    b.d("PushDataEncrypterManager", "getSecureData" + e.getMessage());
                }
            }
            if (TextUtils.isEmpty(str3) && b.b()) {
                b.a("PushDataEncrypterManager", "not exist for:" + str2);
            }
        }
        return str3;
    }

    public static boolean a(Context context, String str, String str2, String str3) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return false;
        }
        return new d(context, str).a(str2 + "_v2", d.a(context, str3));
    }

    public static void b(Context context, String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            try {
                new d(context, str).d(str2 + "_v2");
            } catch (Exception e) {
                if (b.e()) {
                    b.d("PushDataEncrypterManager", "removeSecureData" + e.getMessage());
                }
            }
        }
    }
}
