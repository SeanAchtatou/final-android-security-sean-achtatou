package com.huawei.hms.support.api.push.a;

import com.huawei.hms.support.log.LogLevel;
import com.huawei.hms.support.log.d;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private static d f1063a = null;

    public static void a() {
        f1063a = d.b("PushLog");
    }

    public static void a(String str, String str2) {
        if (f()) {
            f1063a.a(str, str2);
        }
    }

    public static void a(String str, String str2, Throwable th) {
        if (f()) {
            f1063a.a(str, str2, th);
        }
    }

    public static void b(String str, String str2) {
        if (f()) {
            f1063a.b(str, str2);
        }
    }

    public static boolean b() {
        return f() && f1063a.c(LogLevel.DEBUG);
    }

    public static void c(String str, String str2) {
        if (f()) {
            f1063a.c(str, str2);
        }
    }

    public static boolean c() {
        return f() && f1063a.c(LogLevel.INFO);
    }

    public static void d(String str, String str2) {
        if (f()) {
            f1063a.d(str, str2);
        }
    }

    public static boolean d() {
        return f() && f1063a.c(LogLevel.WARN);
    }

    public static boolean e() {
        return f() && f1063a.c(LogLevel.ERROR);
    }

    private static boolean f() {
        return f1063a != null;
    }
}
