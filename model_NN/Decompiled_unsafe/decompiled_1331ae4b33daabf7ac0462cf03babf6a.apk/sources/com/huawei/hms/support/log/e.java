package com.huawei.hms.support.log;

import android.annotation.SuppressLint;
import android.os.Process;
import android.util.Log;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

@SuppressLint({"SimpleDateFormat"})
public class e {

    /* renamed from: a  reason: collision with root package name */
    String f1078a = null;

    /* renamed from: b  reason: collision with root package name */
    String f1079b = "HMS";
    LogLevel c = null;
    long d = 0;
    long e = 0;
    String f = null;
    String g;
    int h;
    int i;
    int j = 0;
    StringBuilder k = null;
    private SimpleDateFormat l = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public class a {

        /* renamed from: a  reason: collision with root package name */
        e f1080a;

        public a(String str, LogLevel logLevel) {
            this.f1080a = new e(str, logLevel);
        }

        public a a(int i) {
            this.f1080a.j = i;
            return this;
        }

        public a a(String str) {
            this.f1080a.f1078a = str;
            return this;
        }

        public e a() {
            return this.f1080a.a();
        }
    }

    protected e() {
    }

    protected e(String str, LogLevel logLevel) {
        if (str != null) {
            this.f1079b = str;
        }
        this.c = logLevel;
    }

    private com.huawei.hms.support.log.a.a a(com.huawei.hms.support.log.a.a aVar) {
        aVar.a(this.l.format(Long.valueOf(this.d)));
        aVar.a('[').a(Integer.valueOf(this.h)).a(']');
        if (this.f1078a != null) {
            aVar.a('[').a(this.f1078a).a(']');
        }
        aVar.a('[').a(this.f1079b).a(']');
        aVar.a('[').a(this.c).a(']');
        return aVar;
    }

    public static String a(Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static boolean a(e eVar) {
        return eVar == null || eVar.b();
    }

    private com.huawei.hms.support.log.a.a b(com.huawei.hms.support.log.a.a aVar) {
        aVar.a("[");
        aVar.a(this.f).a('{').a(Long.valueOf(this.e)).a('}');
        aVar.a("]");
        aVar.a(' ').a(this.k.toString());
        if (this.c.value() < LogLevel.OUT.value()) {
            aVar.a(' ').a('(');
            aVar.a(this.g).a(':').a(Integer.valueOf(this.i));
            aVar.a(')');
        }
        return aVar;
    }

    private <T> e b(Object obj) {
        this.k.append(obj);
        return this;
    }

    /* access modifiers changed from: protected */
    public e a() {
        this.d = System.currentTimeMillis();
        Thread currentThread = Thread.currentThread();
        this.e = currentThread.getId();
        this.f = currentThread.getName();
        this.h = Process.myPid();
        try {
            StackTraceElement stackTraceElement = currentThread.getStackTrace()[this.j + 7];
            this.g = stackTraceElement.getFileName();
            this.i = stackTraceElement.getLineNumber();
        } catch (Exception e2) {
            Log.e("HMS", "create log error");
        }
        this.k = new StringBuilder(32);
        return this;
    }

    public <T> e a(Object obj) {
        b(obj);
        return this;
    }

    public void a(f fVar) {
        if (this.k != null) {
            fVar.a(this);
        }
    }

    public e b(Throwable th) {
        b((Object) 10).b(a(th));
        return this;
    }

    public boolean b() {
        return this.k == null;
    }

    public e c() {
        return a((Object) 10);
    }

    public String d() {
        com.huawei.hms.support.log.a.a a2 = com.huawei.hms.support.log.a.a.a();
        a(a2);
        return a2.c();
    }

    public String e() {
        com.huawei.hms.support.log.a.a a2 = com.huawei.hms.support.log.a.a.a();
        b(a2);
        return a2.c();
    }

    public String toString() {
        com.huawei.hms.support.log.a.a a2 = com.huawei.hms.support.log.a.a.a();
        a(a2);
        b(a2);
        return a2.c();
    }
}
