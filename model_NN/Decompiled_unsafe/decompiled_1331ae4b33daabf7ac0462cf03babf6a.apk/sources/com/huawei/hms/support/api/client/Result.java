package com.huawei.hms.support.api.client;

public abstract class Result {
    protected Status status;

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }
}
