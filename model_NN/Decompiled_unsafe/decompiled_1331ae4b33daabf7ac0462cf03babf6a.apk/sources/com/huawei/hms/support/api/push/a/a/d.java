package com.huawei.hms.support.api.push.a.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.HashMap;
import java.util.Map;

public class d {

    /* renamed from: a  reason: collision with root package name */
    protected SharedPreferences f1062a;

    public d(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("context is null!");
        }
        this.f1062a = context.getSharedPreferences(str, 4);
    }

    public Map<String, ?> a() {
        return this.f1062a != null ? this.f1062a.getAll() : new HashMap();
    }

    public void a(String str, boolean z) {
        SharedPreferences.Editor edit;
        if (this.f1062a != null && (edit = this.f1062a.edit()) != null) {
            edit.putBoolean(str, z).commit();
        }
    }

    public boolean a(String str) {
        if (this.f1062a != null) {
            return this.f1062a.getBoolean(str, false);
        }
        return false;
    }

    public boolean a(String str, Object obj) {
        SharedPreferences.Editor edit = this.f1062a.edit();
        if (obj instanceof String) {
            edit.putString(str, String.valueOf(obj));
        } else if (obj instanceof Integer) {
            edit.putInt(str, ((Integer) obj).intValue());
        } else if (obj instanceof Short) {
            edit.putInt(str, ((Short) obj).shortValue());
        } else if (obj instanceof Byte) {
            edit.putInt(str, ((Byte) obj).byteValue());
        } else if (obj instanceof Long) {
            edit.putLong(str, ((Long) obj).longValue());
        } else if (obj instanceof Float) {
            edit.putFloat(str, ((Float) obj).floatValue());
        } else if (obj instanceof Double) {
            edit.putFloat(str, (float) ((Double) obj).doubleValue());
        } else if (obj instanceof Boolean) {
            edit.putBoolean(str, ((Boolean) obj).booleanValue());
        }
        return edit.commit();
    }

    public boolean a(String str, String str2) {
        SharedPreferences.Editor edit;
        if (this.f1062a == null || (edit = this.f1062a.edit()) == null) {
            return false;
        }
        return edit.putString(str, str2).commit();
    }

    public String b(String str) {
        return this.f1062a != null ? this.f1062a.getString(str, "") : "";
    }

    public boolean c(String str) {
        return this.f1062a != null && this.f1062a.contains(str);
    }

    public boolean d(String str) {
        if (this.f1062a == null || !this.f1062a.contains(str)) {
            return false;
        }
        SharedPreferences.Editor remove = this.f1062a.edit().remove(str);
        remove.commit();
        return remove.commit();
    }
}
