package com.huawei.hms.support.api.push.a.a;

import android.text.TextUtils;
import com.huawei.hms.support.api.push.a.b;
import java.io.UnsupportedEncodingException;
import org.json.JSONArray;
import org.json.JSONException;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f1061a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        if (bArr.length == 0) {
            return "";
        }
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i];
            cArr[i * 2] = f1061a[(b2 & 240) >> 4];
            cArr[(i * 2) + 1] = f1061a[b2 & 15];
        }
        return new String(cArr);
    }

    public static JSONArray a(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                return new JSONArray(str);
            } catch (JSONException e) {
                if (b.b()) {
                    b.a("BaseUtil", "cast jsonString to jsonArray error");
                    return null;
                }
            }
        } else if (b.b()) {
            b.a("BaseUtil", "jsonString is null");
        }
        return null;
    }

    public static byte[] b(String str) {
        byte[] bArr = new byte[(str.length() / 2)];
        try {
            byte[] bytes = str.getBytes("UTF-8");
            for (int i = 0; i < bArr.length; i++) {
                bArr[i] = (byte) (((byte) (Byte.decode("0x" + new String(new byte[]{bytes[i * 2]}, "UTF-8")).byteValue() << 4)) ^ Byte.decode("0x" + new String(new byte[]{bytes[(i * 2) + 1]}, "UTF-8")).byteValue());
            }
        } catch (UnsupportedEncodingException e) {
            if (b.b()) {
                b.a("BaseUtil", "hexString2ByteArray error" + e.getMessage());
            }
        }
        return bArr;
    }
}
