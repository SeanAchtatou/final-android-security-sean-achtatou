package com.huawei.hms.support.api.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.huawei.hms.api.HuaweiApiAvailability;
import com.huawei.hms.support.api.push.a.a.d;
import com.igexin.assist.sdk.AssistPushConsts;
import java.util.concurrent.Executors;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class PushReceiver extends BroadcastReceiver {

    public interface BOUND_KEY {
        public static final String deviceTokenKey = "deviceToken";
        public static final String pushMsgKey = "pushMsg";
        public static final String pushNotifyId = "pushNotifyId";
        public static final String pushStateKey = "pushState";
        public static final String receiveTypeKey = "receiveType";
    }

    public enum Event {
        NOTIFICATION_OPENED,
        NOTIFICATION_CLICK_BTN
    }

    class a implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private Context f1054b;
        private Bundle c;

        public a(Context context, Bundle bundle) {
            this.f1054b = context;
            this.c = bundle;
        }

        public void run() {
            try {
                if (this.c != null) {
                    int i = this.c.getInt(BOUND_KEY.receiveTypeKey);
                    if (i >= 0 && i < c.values().length) {
                        switch (a.f1059a[c.values()[i].ordinal()]) {
                            case 1:
                                PushReceiver.this.onToken(this.f1054b, this.c.getString(BOUND_KEY.deviceTokenKey), this.c);
                                return;
                            case 2:
                                byte[] byteArray = this.c.getByteArray(BOUND_KEY.pushMsgKey);
                                if (byteArray != null) {
                                    PushReceiver.this.onPushMsg(this.f1054b, byteArray, this.c);
                                    return;
                                }
                                return;
                            case 3:
                                PushReceiver.this.onPushState(this.f1054b, this.c.getBoolean(BOUND_KEY.pushStateKey));
                                return;
                            case 4:
                                PushReceiver.this.onEvent(this.f1054b, Event.NOTIFICATION_OPENED, this.c);
                                return;
                            case 5:
                                PushReceiver.this.onEvent(this.f1054b, Event.NOTIFICATION_CLICK_BTN, this.c);
                                return;
                            default:
                                return;
                        }
                    } else if (com.huawei.hms.support.api.push.a.b.b()) {
                        com.huawei.hms.support.api.push.a.b.a("PushReceiver", "invalid receiverType:" + i);
                    }
                }
            } catch (Exception e) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "call EventThread(ReceiveType cause:" + e.getMessage());
                }
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        Context f1055a;

        /* renamed from: b  reason: collision with root package name */
        String f1056b;

        public b(Context context, String str) {
            this.f1055a = context;
            this.f1056b = str;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.Object):boolean
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, java.lang.String):boolean
          com.huawei.hms.support.api.push.a.a.d.a(java.lang.String, boolean):void */
        public void run() {
            d dVar = new d(this.f1055a, "push_client_self_info");
            dVar.a("hasRequestToken", false);
            dVar.d("token_info");
            com.huawei.hms.support.api.push.a.a.c.a(this.f1055a, "push_client_self_info", "token_info", this.f1056b);
        }
    }

    enum c {
        ReceiveType_Init,
        ReceiveType_Token,
        ReceiveType_Msg,
        ReceiveType_PushState,
        ReceiveType_NotifyClick,
        ReceiveType_ClickBtn
    }

    private void a(Context context, Intent intent) {
        byte[] byteArrayExtra = intent.getByteArrayExtra("device_token");
        if (byteArrayExtra != null) {
            String str = new String(byteArrayExtra, "UTF-8");
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "get a deviceToken:" + com.huawei.hms.support.api.push.a.a.a.c.a(str));
            }
            boolean a2 = new d(context, "push_client_self_info").a("hasRequestToken");
            String a3 = com.huawei.hms.support.api.push.a.a.c.a(context, "push_client_self_info", "token_info");
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "my oldtoken is :" + com.huawei.hms.support.api.push.a.a.a.c.a(a3));
            }
            if (a2 || !str.equals(a3)) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "push client begin to receive the token");
                }
                Executors.newSingleThreadExecutor().execute(new b(context, str));
                Bundle bundle = new Bundle();
                bundle.putString(BOUND_KEY.deviceTokenKey, str);
                bundle.putByteArray(BOUND_KEY.pushMsgKey, null);
                bundle.putInt(BOUND_KEY.receiveTypeKey, c.ReceiveType_Token.ordinal());
                if (intent.getExtras() != null) {
                    bundle.putAll(intent.getExtras());
                }
                Executors.newSingleThreadExecutor().execute(new a(context, bundle));
            } else if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "get a deviceToken, but do not requested token, and new token is equals old token");
            }
            if (a2 && !str.equals(a3)) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "begin to report active state tag");
                }
                a(context, str);
            }
        } else if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("PushReceiver", "get a deviceToken, but it is null");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent} */
    private void a(Context context, String str) {
        try {
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("tagKey", "应用激活状态");
            jSONObject.put("tagValue", "已激活");
            jSONObject.put("opType", 1);
            jSONArray.put(jSONObject);
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "report active state tag, jarray:" + jSONArray);
            }
            Intent intent = new Intent("com.huawei.android.push.PLUGIN.TAG").setPackage(HuaweiApiAvailability.SERVICES_PACKAGE);
            intent.putExtra("content", jSONArray.toString()).putExtra("cycle", 0L).putExtra("operType", 1).putExtra("plusType", 2).putExtra(AssistPushConsts.MSG_TYPE_TOKEN, str).putExtra("pkgName", context.getPackageName()).putExtra("apkVersion", com.huawei.hms.support.api.push.a.a.b(context));
            context.sendBroadcast(intent);
        } catch (JSONException e) {
            if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "reportActiveStateTag error:" + e.getMessage());
            }
        }
    }

    private void b(Context context, Intent intent) {
        f(context, intent);
        boolean a2 = new d(context, "push_switch").a("normal_msg_enable");
        if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("PushReceiver", "closePush_Normal:" + a2);
        }
        if (!a2) {
            byte[] byteArrayExtra = intent.getByteArrayExtra("msg_data");
            byte[] byteArrayExtra2 = intent.getByteArrayExtra("device_token");
            if (byteArrayExtra != null && byteArrayExtra2 != null) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "PushReceiver receive a message success");
                }
                String str = new String(byteArrayExtra2, "UTF-8");
                Bundle bundle = new Bundle();
                bundle.putString(BOUND_KEY.deviceTokenKey, str);
                bundle.putByteArray(BOUND_KEY.pushMsgKey, byteArrayExtra);
                bundle.putInt(BOUND_KEY.receiveTypeKey, c.ReceiveType_Msg.ordinal());
                Executors.newSingleThreadExecutor().execute(new a(context, bundle));
            } else if (com.huawei.hms.support.api.push.a.b.b()) {
                com.huawei.hms.support.api.push.a.b.a("PushReceiver", "PushReceiver receive a message, but message is empty.");
            }
        } else if (com.huawei.hms.support.api.push.a.b.b()) {
            com.huawei.hms.support.api.push.a.b.a("PushReceiver", "close switch is true, message not dispatch");
        }
    }

    private void c(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("click");
        Bundle bundle = new Bundle();
        bundle.putString(BOUND_KEY.pushMsgKey, stringExtra);
        bundle.putInt(BOUND_KEY.receiveTypeKey, c.ReceiveType_NotifyClick.ordinal());
        Executors.newSingleThreadExecutor().execute(new a(context, bundle));
    }

    private void d(Context context, Intent intent) {
        String stringExtra = intent.getStringExtra("clickBtn");
        int intExtra = intent.getIntExtra("notifyId", 0);
        Bundle bundle = new Bundle();
        bundle.putString(BOUND_KEY.pushMsgKey, stringExtra);
        bundle.putInt(BOUND_KEY.pushNotifyId, intExtra);
        bundle.putInt(BOUND_KEY.receiveTypeKey, c.ReceiveType_ClickBtn.ordinal());
        Executors.newSingleThreadExecutor().execute(new a(context, bundle));
    }

    private void e(Context context, Intent intent) {
        boolean booleanExtra = intent.getBooleanExtra("push_state", false);
        Bundle bundle = new Bundle();
        bundle.putBoolean(BOUND_KEY.pushStateKey, booleanExtra);
        bundle.putInt(BOUND_KEY.receiveTypeKey, c.ReceiveType_PushState.ordinal());
        Executors.newSingleThreadExecutor().execute(new a(context, bundle));
    }

    private void f(Context context, Intent intent) {
        if (context != null && intent != null) {
            String stringExtra = intent.getStringExtra("msgIdStr");
            if (!TextUtils.isEmpty(stringExtra) && com.huawei.hms.support.api.push.a.a.a(context)) {
                Intent intent2 = new Intent("com.huawei.android.push.intent.MSG_RESPONSE");
                intent2.putExtra("msgIdStr", stringExtra);
                intent2.setPackage("android");
                intent2.setFlags(32);
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "send msg response broadcast to frameworkPush");
                }
                context.sendBroadcast(intent2);
            }
        }
    }

    public void onEvent(Context context, Event event, Bundle bundle) {
    }

    public void onPushMsg(Context context, byte[] bArr, String str) {
    }

    public boolean onPushMsg(Context context, byte[] bArr, Bundle bundle) {
        String str = "";
        if (bundle != null) {
            str = bundle.getString(BOUND_KEY.deviceTokenKey);
        }
        onPushMsg(context, bArr, str);
        return true;
    }

    public void onPushState(Context context, boolean z) {
    }

    public final void onReceive(Context context, Intent intent) {
        try {
            if (HmsSystemUtils.isBrandHuaWei()) {
                if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.a("PushReceiver", "enter PushMsgReceiver:onReceive(Intent:" + intent.getAction() + " pkgName:" + context.getPackageName() + ")");
                }
                String action = intent.getAction();
                if ("com.huawei.android.push.intent.REGISTRATION".equals(action) && intent.hasExtra("device_token")) {
                    a(context, intent);
                } else if ("com.huawei.android.push.intent.RECEIVE".equals(action) && intent.hasExtra("msg_data")) {
                    b(context, intent);
                } else if ("com.huawei.android.push.intent.CLICK".equals(action) && intent.hasExtra("click")) {
                    c(context, intent);
                } else if ("com.huawei.android.push.intent.CLICK".equals(action) && intent.hasExtra("clickBtn")) {
                    d(context, intent);
                } else if ("com.huawei.intent.action.PUSH_STATE".equals(action)) {
                    e(context, intent);
                } else if (com.huawei.hms.support.api.push.a.b.b()) {
                    com.huawei.hms.support.api.push.a.b.b("PushReceiver", "message can't be recognised:" + intent.toUri(0));
                }
            }
        } catch (Exception e) {
            if (com.huawei.hms.support.api.push.a.b.e()) {
                com.huawei.hms.support.api.push.a.b.d("PushReceiver", "call onReceive(intent:" + intent + ") cause:" + e.getMessage());
            }
        }
    }

    public void onToken(Context context, String str) {
    }

    public void onToken(Context context, String str, Bundle bundle) {
        onToken(context, str);
    }
}
