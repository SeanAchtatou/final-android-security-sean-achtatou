package com.huawei.hms.support.log;

import android.text.TextUtils;
import android.util.Log;
import com.huawei.hms.support.log.e;
import java.util.HashMap;
import java.util.Map;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static Map<String, d> f1074a = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static String f1075b = "log.log";
    private static LogLevel c = LogLevel.INFO;
    private a d = null;
    private String e;
    private final g f = new g();

    public class a {

        /* renamed from: a  reason: collision with root package name */
        private String f1076a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f1077b = false;
        private String c = d.f1075b;

        public a(String str) {
            this.f1076a = str;
        }

        public a a(boolean z) {
            this.f1077b = z;
            return this;
        }

        public d a() {
            d e = d.f(this.c, this.f1076a);
            if (this.f1077b) {
                e.b();
            }
            return e;
        }
    }

    private d() {
    }

    public static void a(LogLevel logLevel) {
        c = logLevel;
    }

    public static void a(String str) {
        f1075b = str;
    }

    private void a(String str, String str2, LogLevel logLevel, String str3, Throwable th) {
        e a2 = new e.a(str, logLevel).a(this.e).a(this.f.a()).a();
        if (!TextUtils.isEmpty(str3)) {
            a2.a("[").a(str3).a("]");
        }
        a2.a(str2);
        if (th != null) {
            a2.b(th);
        }
        a2.a((f) this.d);
    }

    public static d b(String str) {
        d f2;
        synchronized (d.class) {
            try {
                f2 = f(f1075b, str);
            } catch (Throwable th) {
                Class<d> cls = d.class;
                throw th;
            }
        }
        return f2;
    }

    /* access modifiers changed from: private */
    public void b() {
        new e.a(null, LogLevel.OUT).a().c().a(b.a()).a((f) this.d);
    }

    /* access modifiers changed from: private */
    public static d f(String str, String str2) {
        d dVar;
        synchronized (d.class) {
            try {
                Log.i("LogAdaptor", "createAppLog, file:" + str + ", module:" + str2);
                dVar = f1074a.get(str2);
                if (dVar == null) {
                    dVar = new d();
                    dVar.g(str, str2);
                    dVar.b(c);
                    f1074a.put(str2, dVar);
                }
            } catch (Throwable th) {
                Class<d> cls = d.class;
                throw th;
            }
        }
        return dVar;
    }

    private void g(String str, String str2) {
        this.e = str2;
        this.d = new a(str2, str, LogLevel.INFO);
    }

    public void a(String str, String str2) {
        a(str, str2, LogLevel.DEBUG, null, null);
    }

    public void a(String str, String str2, Throwable th) {
        a(str, str2, LogLevel.ERROR, null, th);
    }

    public void b(LogLevel logLevel) {
        this.d.b(logLevel);
    }

    public void b(String str, String str2) {
        a(str, str2, LogLevel.INFO, null, null);
    }

    public void c(String str, String str2) {
        a(str, str2, LogLevel.WARN, null, null);
    }

    public boolean c(LogLevel logLevel) {
        return this.d.a(logLevel);
    }

    public void d(String str, String str2) {
        a(str, str2, LogLevel.ERROR, null, null);
    }
}
