package com.huawei.hms.support.api;

import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.support.api.transport.DatagramTransport;
import java.util.concurrent.atomic.AtomicBoolean;

class c implements DatagramTransport.a {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AtomicBoolean f1043a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f1044b;

    c(a aVar, AtomicBoolean atomicBoolean) {
        this.f1044b = aVar;
        this.f1043a = atomicBoolean;
    }

    public void a(int i, IMessageEntity iMessageEntity) {
        if (!this.f1043a.get()) {
            this.f1044b.a(i, iMessageEntity);
        }
        this.f1044b.f1040a.countDown();
    }
}
