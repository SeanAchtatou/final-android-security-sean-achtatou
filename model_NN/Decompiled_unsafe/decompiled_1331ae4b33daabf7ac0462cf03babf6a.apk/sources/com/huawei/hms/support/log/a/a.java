package com.huawei.hms.support.log.a;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private StringBuilder f1068a = new StringBuilder(64);

    public static a a() {
        return new a();
    }

    public <T> a a(T t) {
        if (this.f1068a != null) {
            this.f1068a.append((Object) t);
        }
        return this;
    }

    public a b() {
        return a(10);
    }

    public String c() {
        if (this.f1068a == null) {
            return "";
        }
        String sb = this.f1068a.toString();
        this.f1068a = null;
        return sb;
    }

    public String toString() {
        return this.f1068a == null ? "" : this.f1068a.toString();
    }
}
