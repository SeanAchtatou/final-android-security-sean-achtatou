package com.huawei.hms.core.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.huawei.hms.core.aidl.c;

public interface d extends IInterface {

    public abstract class a extends Binder implements d {

        /* renamed from: com.huawei.hms.core.aidl.d$a$a  reason: collision with other inner class name */
        class C0017a implements d {

            /* renamed from: a  reason: collision with root package name */
            private IBinder f1038a;

            C0017a(IBinder iBinder) {
                this.f1038a = iBinder;
            }

            public void a(a aVar) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.huawei.hms.core.aidl.IAIDLInvoke");
                    if (aVar != null) {
                        obtain.writeInt(1);
                        aVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f1038a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }

            public void a(a aVar, c cVar) {
                IBinder iBinder = null;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.huawei.hms.core.aidl.IAIDLInvoke");
                    if (aVar != null) {
                        obtain.writeInt(1);
                        aVar.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (cVar != null) {
                        iBinder = cVar.asBinder();
                    }
                    obtain.writeStrongBinder(iBinder);
                    this.f1038a.transact(2, obtain, null, 1);
                } finally {
                    obtain.recycle();
                }
            }

            public IBinder asBinder() {
                return this.f1038a;
            }
        }

        public static d a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.huawei.hms.core.aidl.IAIDLInvoke");
            return (queryLocalInterface == null || !(queryLocalInterface instanceof d)) ? new C0017a(iBinder) : (d) queryLocalInterface;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
            a aVar = null;
            switch (i) {
                case 1:
                    parcel.enforceInterface("com.huawei.hms.core.aidl.IAIDLInvoke");
                    if (parcel.readInt() != 0) {
                        aVar = a.CREATOR.createFromParcel(parcel);
                    }
                    a(aVar);
                    parcel2.writeNoException();
                    return true;
                case 2:
                    parcel.enforceInterface("com.huawei.hms.core.aidl.IAIDLInvoke");
                    if (parcel.readInt() != 0) {
                        aVar = a.CREATOR.createFromParcel(parcel);
                    }
                    a(aVar, c.a.a(parcel.readStrongBinder()));
                    return true;
                case 1598968902:
                    parcel2.writeString("com.huawei.hms.core.aidl.IAIDLInvoke");
                    return true;
                default:
                    return super.onTransact(i, parcel, parcel2, i2);
            }
        }
    }

    void a(a aVar);

    void a(a aVar, c cVar);
}
