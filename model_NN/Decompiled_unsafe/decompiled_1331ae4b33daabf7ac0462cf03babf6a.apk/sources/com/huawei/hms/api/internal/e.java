package com.huawei.hms.api.internal;

import android.os.Handler;
import android.os.Looper;
import com.huawei.hms.support.api.ResolveResult;
import com.huawei.hms.support.api.client.ResultCallback;
import com.huawei.hms.support.api.entity.core.ConnectResp;

class e implements ResultCallback<ResolveResult<ConnectResp>> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f1028a;

    e(b bVar) {
        this.f1028a = bVar;
    }

    /* renamed from: a */
    public void onResult(ResolveResult<ConnectResp> resolveResult) {
        new Handler(Looper.getMainLooper()).post(new f(this, resolveResult));
    }
}
