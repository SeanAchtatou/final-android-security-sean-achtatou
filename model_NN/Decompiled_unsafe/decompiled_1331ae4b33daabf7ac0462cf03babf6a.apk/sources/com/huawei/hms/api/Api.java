package com.huawei.hms.api;

import com.huawei.hms.api.Api.ApiOptions;
import com.huawei.hms.support.api.entity.auth.PermissionInfo;
import com.huawei.hms.support.api.entity.auth.Scope;
import java.util.Collections;
import java.util.List;

public class Api<O extends ApiOptions> {

    /* renamed from: a  reason: collision with root package name */
    private final String f1014a;

    /* renamed from: b  reason: collision with root package name */
    private final Options<O> f1015b;

    public interface ApiOptions {

        public interface HasOptions extends ApiOptions {
        }

        public final class NoOptions implements NotRequiredOptions {
        }

        public interface NotRequiredOptions extends ApiOptions {
        }

        public interface Optional extends HasOptions, NotRequiredOptions {
        }
    }

    public abstract class Options<O> {
        public List<PermissionInfo> getPermissionInfoList(O o) {
            return Collections.emptyList();
        }

        public List<Scope> getScopeList(O o) {
            return Collections.emptyList();
        }
    }

    public Api(String str) {
        this.f1014a = str;
        this.f1015b = null;
    }

    public Api(String str, Options<O> options) {
        this.f1014a = str;
        this.f1015b = options;
    }

    public String getApiName() {
        return this.f1014a;
    }

    public Options<O> getOptions() {
        return this.f1015b;
    }
}
