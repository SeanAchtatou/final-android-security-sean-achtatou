package com.huawei.hms.api.internal;

import android.os.Bundle;
import android.os.RemoteException;
import com.huawei.hms.core.aidl.IMessageEntity;
import com.huawei.hms.core.aidl.RequestHeader;
import com.huawei.hms.core.aidl.a;
import com.huawei.hms.core.aidl.c;
import com.huawei.hms.core.aidl.e;
import com.huawei.hms.support.api.client.ApiClient;
import com.huawei.hms.support.api.entity.core.CommonCode;
import com.huawei.hms.support.api.transport.DatagramTransport;

public class IPCTransport implements DatagramTransport {

    /* renamed from: a  reason: collision with root package name */
    private final String f1019a;

    /* renamed from: b  reason: collision with root package name */
    private final IMessageEntity f1020b;
    private final Class<? extends IMessageEntity> c;

    public IPCTransport(String str, IMessageEntity iMessageEntity, Class<? extends IMessageEntity> cls) {
        this.f1019a = str;
        this.f1020b = iMessageEntity;
        this.c = cls;
    }

    private int a(ApiClient apiClient, c cVar) {
        a aVar = new a(this.f1019a);
        e eVar = new e();
        aVar.a(eVar.a(this.f1020b, new Bundle()));
        RequestHeader requestHeader = new RequestHeader();
        requestHeader.appId = apiClient.getAppID();
        requestHeader.packageName = apiClient.getPackageName();
        aVar.f1036b = eVar.a(requestHeader, new Bundle());
        try {
            ((b) apiClient).c().a(aVar, cVar);
            return 0;
        } catch (RemoteException e) {
            return CommonCode.ErrorCode.INTERNAL_ERROR;
        }
    }

    public final void a(ApiClient apiClient, DatagramTransport.a aVar) {
        int a2 = a(apiClient, new h(this.c, aVar));
        if (a2 != 0) {
            aVar.a(a2, null);
        }
    }

    public final void b(ApiClient apiClient, DatagramTransport.a aVar) {
        a(apiClient, aVar);
    }
}
