package com.huawei.hms.api.internal;

import android.app.Activity;
import android.app.Application;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.huawei.hms.a.d;
import com.huawei.hms.api.ConnectionResult;
import com.huawei.hms.api.HuaweiApiAvailability;
import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public class i implements Application.ActivityLifecycleCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f1033a;

    /* renamed from: b  reason: collision with root package name */
    private final HuaweiApiAvailability.OnUpdateListener f1034b;
    private final int c;
    private final AtomicBoolean d = new AtomicBoolean(false);
    private boolean e = false;

    public i(Activity activity, HuaweiApiAvailability.OnUpdateListener onUpdateListener, int i) {
        this.f1034b = onUpdateListener;
        this.f1033a = activity;
        this.c = i;
    }

    private static boolean a(Activity activity, Uri uri, int i) {
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(uri, "application/vnd.android.package-archive");
        intent.setFlags(3);
        try {
            activity.startActivityForResult(intent, i);
            return true;
        } catch (ActivityNotFoundException e2) {
            return false;
        }
    }

    private Uri b(File file) {
        d dVar = new d(this.f1033a);
        String packageName = this.f1033a.getPackageName();
        String str = packageName + ".hms.update.provider";
        if (Build.VERSION.SDK_INT <= 23 || dVar.e(packageName) > 23 || dVar.a(packageName, str)) {
        }
        return Uri.fromFile(file);
    }

    private void d() {
        if (!this.e) {
            this.e = true;
            this.f1033a.getApplication().registerActivityLifecycleCallbacks(this);
        }
    }

    private void e() {
        if (this.e) {
            this.e = false;
            this.f1033a.getApplication().unregisterActivityLifecycleCallbacks(this);
        }
    }

    public void a() {
        this.d.set(true);
    }

    public void a(int i, int i2, int i3) {
        if (1 == i || 202 == i || 103 == i) {
            this.d.set(false);
            this.f1034b.onUpdateFailed(new ConnectionResult(8));
        }
        if (2 == i) {
            this.d.set(false);
            this.f1034b.onUpdateFailed(new ConnectionResult(13));
        }
    }

    public void a(File file) {
        d dVar = new d(this.f1033a);
        d();
        Uri b2 = b(file);
        if (b2 == null || !dVar.a(file.toString(), HuaweiApiAvailability.SERVICES_PACKAGE, HuaweiApiAvailability.SERVICES_SIGNATURE)) {
            this.d.set(false);
            this.f1034b.onUpdateFailed(new ConnectionResult(8));
            return;
        }
        a(this.f1033a, b2, this.c);
    }

    public boolean b() {
        return this.d.get();
    }

    /* access modifiers changed from: protected */
    public void c() {
        e();
        this.d.set(false);
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
        if (activity == this.f1033a) {
            c();
        }
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }
}
