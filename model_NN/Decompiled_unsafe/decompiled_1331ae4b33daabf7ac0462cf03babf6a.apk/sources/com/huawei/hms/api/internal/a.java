package com.huawei.hms.api.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import com.huawei.hms.api.HuaweiApiAvailability;

public final class a extends HuaweiApiAvailability {

    /* renamed from: a  reason: collision with root package name */
    private static final a f1021a = new a();

    /* renamed from: b  reason: collision with root package name */
    private i f1022b;

    private a() {
    }

    public static a a() {
        return f1021a;
    }

    private static boolean a(Activity activity, String str, int i) {
        Uri fromParts = Uri.fromParts("package", str, null);
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(fromParts);
        try {
            activity.startActivityForResult(intent, i);
            return true;
        } catch (ActivityNotFoundException e) {
            com.huawei.hms.a.a.a.b("HuaweiApiAvailabilityImpl", "Failed to start package installer.", e);
            return false;
        }
    }

    public int isHuaweiMobileServicesAvailable(Context context) {
        com.huawei.hms.a.a.a(context, "context must not be null.");
        if (this.f1022b == null || !this.f1022b.b()) {
            return g.a(context);
        }
        return 18;
    }

    public boolean isUserResolvableError(int i) {
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 9:
                return true;
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            default:
                return false;
        }
    }

    public void resolveError(Activity activity, int i, int i2, HuaweiApiAvailability.OnUpdateListener onUpdateListener) {
        com.huawei.hms.a.a.a(activity, "activity must not be null.");
        com.huawei.hms.a.a.a(onUpdateListener, "listener must not be null.");
        if (isHuaweiMobileServicesAvailable(activity) != 18) {
            switch (i) {
                case 1:
                case 2:
                    this.f1022b = new i(activity, onUpdateListener, i2);
                    this.f1022b.a();
                    return;
                case 3:
                    a(activity, HuaweiApiAvailability.SERVICES_PACKAGE, i2);
                    return;
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                default:
                    return;
            }
        }
    }
}
