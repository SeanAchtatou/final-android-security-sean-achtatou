package com.c.a.b.b.b.a;

import com.c.a.b.b.b.g;

/* compiled from: AbstractContentBody */
public abstract class a implements b {

    /* renamed from: a  reason: collision with root package name */
    protected g.a f858a = g.a.f872a;

    /* renamed from: b  reason: collision with root package name */
    private final String f859b;
    private final String c;
    private final String d;

    public a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("MIME type may not be null");
        }
        this.f859b = str;
        int indexOf = str.indexOf(47);
        if (indexOf != -1) {
            this.c = str.substring(0, indexOf);
            this.d = str.substring(indexOf + 1);
            return;
        }
        this.c = str;
        this.d = null;
    }

    public String a() {
        return this.f859b;
    }

    public void a(g.a aVar) {
        this.f858a = aVar;
    }
}
