package com.c.a.b.b.c;

import android.support.v4.media.TransportMediator;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeaderValueParser;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.message.ParserCursor;
import org.apache.http.util.CharArrayBuffer;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: URLEncodedUtils */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f878a = {'&'};

    /* renamed from: b  reason: collision with root package name */
    private static final BitSet f879b = new BitSet(256);
    private static final BitSet c = new BitSet(256);
    private static final BitSet d = new BitSet(256);
    private static final BitSet e = new BitSet(256);
    private static final BitSet f = new BitSet(256);
    private static final BitSet g = new BitSet(256);
    private static final BitSet h = new BitSet(256);

    static {
        for (int i = 97; i <= 122; i++) {
            f879b.set(i);
        }
        for (int i2 = 65; i2 <= 90; i2++) {
            f879b.set(i2);
        }
        for (int i3 = 48; i3 <= 57; i3++) {
            f879b.set(i3);
        }
        f879b.set(95);
        f879b.set(45);
        f879b.set(46);
        f879b.set(42);
        h.or(f879b);
        f879b.set(33);
        f879b.set(TransportMediator.KEYCODE_MEDIA_PLAY);
        f879b.set(39);
        f879b.set(40);
        f879b.set(41);
        c.set(44);
        c.set(59);
        c.set(58);
        c.set(36);
        c.set(38);
        c.set(43);
        c.set(61);
        d.or(f879b);
        d.or(c);
        e.or(f879b);
        e.set(47);
        e.set(59);
        e.set(58);
        e.set(64);
        e.set(38);
        e.set(61);
        e.set(43);
        e.set(36);
        e.set(44);
        g.set(59);
        g.set(47);
        g.set(63);
        g.set(58);
        g.set(64);
        g.set(38);
        g.set(61);
        g.set(43);
        g.set(36);
        g.set(44);
        g.set(91);
        g.set(93);
        f.or(g);
        f.or(f879b);
    }

    public static List<NameValuePair> a(String str) {
        if (str == null) {
            return Collections.emptyList();
        }
        BasicHeaderValueParser basicHeaderValueParser = BasicHeaderValueParser.DEFAULT;
        CharArrayBuffer charArrayBuffer = new CharArrayBuffer(str.length());
        charArrayBuffer.append(str);
        ParserCursor parserCursor = new ParserCursor(0, charArrayBuffer.length());
        ArrayList arrayList = new ArrayList();
        while (!parserCursor.atEnd()) {
            NameValuePair parseNameValuePair = basicHeaderValueParser.parseNameValuePair(charArrayBuffer, parserCursor, f878a);
            if (parseNameValuePair.getName().length() > 0) {
                arrayList.add(new BasicNameValuePair(parseNameValuePair.getName(), parseNameValuePair.getValue()));
            }
        }
        return arrayList;
    }

    public static String a(List<? extends NameValuePair> list, String str) {
        StringBuilder sb = new StringBuilder();
        for (NameValuePair next : list) {
            String a2 = a(next.getName(), str);
            String a3 = a(next.getValue(), str);
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(a2);
            if (a3 != null) {
                sb.append("=");
                sb.append(a3);
            }
        }
        return sb.toString();
    }

    public static String a(Iterable<? extends NameValuePair> iterable, Charset charset) {
        StringBuilder sb = new StringBuilder();
        for (NameValuePair next : iterable) {
            String d2 = d(next.getName(), charset);
            String d3 = d(next.getValue(), charset);
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(d2);
            if (d3 != null) {
                sb.append("=");
                sb.append(d3);
            }
        }
        return sb.toString();
    }

    private static String a(String str, Charset charset, BitSet bitSet, boolean z) {
        if (str == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        ByteBuffer encode = charset.encode(str);
        while (encode.hasRemaining()) {
            byte b2 = encode.get() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (bitSet.get(b2)) {
                sb.append((char) b2);
            } else if (!z || b2 != 32) {
                sb.append("%");
                char upperCase = Character.toUpperCase(Character.forDigit((b2 >> 4) & 15, 16));
                char upperCase2 = Character.toUpperCase(Character.forDigit(b2 & 15, 16));
                sb.append(upperCase);
                sb.append(upperCase2);
            } else {
                sb.append('+');
            }
        }
        return sb.toString();
    }

    private static String a(String str, String str2) {
        Charset forName;
        if (str == null) {
            return null;
        }
        if (str2 != null) {
            forName = Charset.forName(str2);
        } else {
            forName = Charset.forName("UTF-8");
        }
        return a(str, forName, h, true);
    }

    private static String d(String str, Charset charset) {
        if (str == null) {
            return null;
        }
        if (charset == null) {
            charset = Charset.forName("UTF-8");
        }
        return a(str, charset, h, true);
    }

    static String a(String str, Charset charset) {
        return a(str, charset, d, false);
    }

    static String b(String str, Charset charset) {
        return a(str, charset, f, false);
    }

    static String c(String str, Charset charset) {
        return a(str, charset, e, false);
    }
}
