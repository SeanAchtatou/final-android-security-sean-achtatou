package com.c.a.b;

import android.text.TextUtils;
import com.c.a.b.b.b;
import com.c.a.c.a.c;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: HttpCache */
public class a {
    private static long c = 60000;
    private static final ConcurrentHashMap<String, Boolean> d = new ConcurrentHashMap<>(10);

    /* renamed from: a  reason: collision with root package name */
    private final c<String, String> f842a;

    /* renamed from: b  reason: collision with root package name */
    private int f843b;

    static {
        d.put(b.a.GET.toString(), true);
    }

    public a() {
        this(102400, 60000);
    }

    public a(int i, long j) {
        this.f843b = 102400;
        this.f843b = i;
        c = j;
        this.f842a = new c<String, String>(this.f843b) {
            /* access modifiers changed from: protected */
            public int a(String str, String str2) {
                if (str2 == null) {
                    return 0;
                }
                return str2.length();
            }
        };
    }

    public static long a() {
        return c;
    }

    public void a(String str, String str2, long j) {
        if (str != null && str2 != null && j >= 1) {
            this.f842a.a(str, str2, System.currentTimeMillis() + j);
        }
    }

    public String a(String str) {
        if (str != null) {
            return this.f842a.a(str);
        }
        return null;
    }

    public boolean b(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        Boolean bool = d.get(str.toUpperCase());
        return bool == null ? false : bool.booleanValue();
    }
}
