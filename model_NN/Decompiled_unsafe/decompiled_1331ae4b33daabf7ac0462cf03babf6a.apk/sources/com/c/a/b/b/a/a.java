package com.c.a.b.b.a;

import com.c.a.b.b.c.b;
import com.c.a.c.c;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.entity.AbstractHttpEntity;

/* compiled from: BodyParamsEntity */
public class a extends AbstractHttpEntity implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected byte[] f848a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f849b;
    private String c;
    private List<NameValuePair> d;

    public a() {
        this(null);
    }

    public a(String str) {
        this.f849b = true;
        this.c = "UTF-8";
        if (str != null) {
            this.c = str;
        }
        setContentType("application/x-www-form-urlencoded");
        this.d = new ArrayList();
    }

    public a(List<NameValuePair> list, String str) {
        this.f849b = true;
        this.c = "UTF-8";
        if (str != null) {
            this.c = str;
        }
        setContentType("application/x-www-form-urlencoded");
        this.d = list;
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.c.a.b.b.c.b.a(java.util.List<? extends org.apache.http.NameValuePair>, java.lang.String):java.lang.String
     arg types: [java.util.List<org.apache.http.NameValuePair>, java.lang.String]
     candidates:
      com.c.a.b.b.c.b.a(java.lang.Iterable<? extends org.apache.http.NameValuePair>, java.nio.charset.Charset):java.lang.String
      com.c.a.b.b.c.b.a(java.lang.String, java.lang.String):java.lang.String
      com.c.a.b.b.c.b.a(java.lang.String, java.nio.charset.Charset):java.lang.String
      com.c.a.b.b.c.b.a(java.util.List<? extends org.apache.http.NameValuePair>, java.lang.String):java.lang.String */
    private void a() {
        if (this.f849b) {
            try {
                this.f848a = b.a((List<? extends NameValuePair>) this.d, this.c).getBytes(this.c);
            } catch (UnsupportedEncodingException e) {
                c.a(e.getMessage(), e);
            }
            this.f849b = false;
        }
    }

    public boolean isRepeatable() {
        return true;
    }

    public long getContentLength() {
        a();
        return (long) this.f848a.length;
    }

    public InputStream getContent() throws IOException {
        a();
        return new ByteArrayInputStream(this.f848a);
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        a();
        outputStream.write(this.f848a);
        outputStream.flush();
    }

    public boolean isStreaming() {
        return false;
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
