package com.c.a.b;

import com.c.a.b.b.b.a.b;
import com.c.a.b.b.b.a.d;
import com.c.a.b.b.b.g;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

/* compiled from: RequestParams */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private String f880a = "UTF-8";

    /* renamed from: b  reason: collision with root package name */
    private List<a> f881b;
    private List<NameValuePair> c;
    private HttpEntity d;
    private List<NameValuePair> e;
    private HashMap<String, b> f;

    public String a() {
        return this.f880a;
    }

    public void a(String str, String str2) {
        if (this.f881b == null) {
            this.f881b = new ArrayList();
        }
        this.f881b.add(new a(str, str2));
    }

    public void b(String str, String str2) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.add(new BasicNameValuePair(str, str2));
    }

    public void c(String str, String str2) {
        if (this.e == null) {
            this.e = new ArrayList();
        }
        this.e.add(new BasicNameValuePair(str, str2));
    }

    public HttpEntity b() {
        if (this.d != null) {
            return this.d;
        }
        if (this.f != null && !this.f.isEmpty()) {
            HttpEntity gVar = new g(com.c.a.b.b.b.c.STRICT, null, Charset.forName(this.f880a));
            if (this.e != null && !this.e.isEmpty()) {
                for (NameValuePair next : this.e) {
                    try {
                        gVar.a(next.getName(), new d(next.getValue()));
                    } catch (UnsupportedEncodingException e2) {
                        com.c.a.c.c.a(e2.getMessage(), e2);
                    }
                }
            }
            for (Map.Entry next2 : this.f.entrySet()) {
                gVar.a((String) next2.getKey(), (b) next2.getValue());
            }
            return gVar;
        } else if (this.e == null || this.e.isEmpty()) {
            return null;
        } else {
            return new com.c.a.b.b.a.a(this.e, this.f880a);
        }
    }

    public List<NameValuePair> c() {
        return this.c;
    }

    public List<a> d() {
        return this.f881b;
    }

    /* compiled from: RequestParams */
    public class a {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f882a = false;

        /* renamed from: b  reason: collision with root package name */
        public final Header f883b;

        public a(String str, String str2) {
            this.f883b = new BasicHeader(str, str2);
        }
    }
}
