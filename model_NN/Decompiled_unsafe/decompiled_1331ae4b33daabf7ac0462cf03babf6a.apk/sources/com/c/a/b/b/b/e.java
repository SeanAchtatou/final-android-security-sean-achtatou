package com.c.a.b.b.b;

/* compiled from: MinimalField */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f866a;

    /* renamed from: b  reason: collision with root package name */
    private final String f867b;

    e(String str, String str2) {
        this.f866a = str;
        this.f867b = str2;
    }

    public String a() {
        return this.f866a;
    }

    public String b() {
        return this.f867b;
    }

    public String toString() {
        return this.f866a + ": " + this.f867b;
    }
}
