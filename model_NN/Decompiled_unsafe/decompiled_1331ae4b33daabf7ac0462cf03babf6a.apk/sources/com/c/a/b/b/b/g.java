package com.c.a.b.b.b;

import com.c.a.b.a.d;
import com.c.a.b.b.a.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Random;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.message.BasicHeader;

/* compiled from: MultipartEntity */
public class g implements b, HttpEntity {

    /* renamed from: b  reason: collision with root package name */
    private static final char[] f870b = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    /* renamed from: a  reason: collision with root package name */
    private a f871a;
    private final b c;
    private Header d;
    private long e;
    private volatile boolean f;
    private final String g;
    private final Charset h;
    private String i;

    public void a(d dVar) {
        this.f871a.f873b = dVar;
    }

    /* compiled from: MultipartEntity */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final a f872a = new a();

        /* renamed from: b  reason: collision with root package name */
        public d f873b = null;
        public long c = 0;
        public long d = 0;

        public boolean a(boolean z) {
            if (this.f873b != null) {
                return this.f873b.a(this.c, this.d, z);
            }
            return true;
        }
    }

    public g(c cVar, String str, Charset charset) {
        this.f871a = new a();
        this.i = "form-data";
        this.g = str == null ? a() : str;
        cVar = cVar == null ? c.STRICT : cVar;
        this.h = charset == null ? d.f865a : charset;
        this.c = new b(this.i, this.h, this.g, cVar);
        this.d = new BasicHeader("Content-Type", a(this.g, this.h));
        this.f = true;
    }

    public g() {
        this(c.STRICT, null, null);
    }

    /* access modifiers changed from: protected */
    public String a(String str, Charset charset) {
        StringBuilder sb = new StringBuilder();
        sb.append("multipart/" + this.i + "; boundary=");
        sb.append(str);
        return sb.toString();
    }

    /* access modifiers changed from: protected */
    public String a() {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        int nextInt = random.nextInt(11) + 30;
        for (int i2 = 0; i2 < nextInt; i2++) {
            sb.append(f870b[random.nextInt(f870b.length)]);
        }
        return sb.toString();
    }

    public void a(a aVar) {
        this.c.a(aVar);
        this.f = true;
    }

    public void a(String str, com.c.a.b.b.b.a.b bVar) {
        a(new a(str, bVar));
    }

    public boolean isRepeatable() {
        for (a b2 : this.c.a()) {
            if (b2.b().e() < 0) {
                return false;
            }
        }
        return true;
    }

    public boolean isChunked() {
        return !isRepeatable();
    }

    public boolean isStreaming() {
        return !isRepeatable();
    }

    public long getContentLength() {
        if (this.f) {
            this.e = this.c.c();
            this.f = false;
        }
        return this.e;
    }

    public Header getContentType() {
        return this.d;
    }

    public Header getContentEncoding() {
        return null;
    }

    public void consumeContent() throws IOException, UnsupportedOperationException {
        if (isStreaming()) {
            throw new UnsupportedOperationException("Streaming entity does not implement #consumeContent()");
        }
    }

    public InputStream getContent() throws IOException, UnsupportedOperationException {
        throw new UnsupportedOperationException("Multipart form entity does not implement #getContent()");
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        this.f871a.c = getContentLength();
        this.c.a(outputStream, this.f871a);
    }
}
