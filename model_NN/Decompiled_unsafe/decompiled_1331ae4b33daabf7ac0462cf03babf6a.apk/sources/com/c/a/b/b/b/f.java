package com.c.a.b.b.b;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/* compiled from: MinimalFieldHeader */
class f implements Iterable<e> {

    /* renamed from: a  reason: collision with root package name */
    private final List<e> f868a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    private final Map<String, List<e>> f869b = new HashMap();

    public void a(e eVar) {
        if (eVar != null) {
            String lowerCase = eVar.a().toLowerCase(Locale.US);
            Object obj = this.f869b.get(lowerCase);
            if (obj == null) {
                obj = new LinkedList();
                this.f869b.put(lowerCase, obj);
            }
            obj.add(eVar);
            this.f868a.add(eVar);
        }
    }

    public e a(String str) {
        if (str == null) {
            return null;
        }
        List list = this.f869b.get(str.toLowerCase(Locale.US));
        if (list == null || list.isEmpty()) {
            return null;
        }
        return (e) list.get(0);
    }

    public Iterator<e> iterator() {
        return Collections.unmodifiableList(this.f868a).iterator();
    }

    public String toString() {
        return this.f868a.toString();
    }
}
