package com.c.a.c;

import android.text.TextUtils;
import android.util.Log;

/* compiled from: LogUtils */
public class c {

    /* renamed from: a  reason: collision with root package name */
    public static String f907a = "";

    /* renamed from: b  reason: collision with root package name */
    public static boolean f908b = true;
    public static boolean c = true;
    public static boolean d = true;
    public static boolean e = true;
    public static boolean f = true;
    public static boolean g = true;
    public static a h;

    /* compiled from: LogUtils */
    public interface a {
        void a(String str, String str2);

        void a(String str, String str2, Throwable th);

        void a(String str, Throwable th);
    }

    private static String a(StackTraceElement stackTraceElement) {
        String className = stackTraceElement.getClassName();
        String format = String.format("%s.%s(L:%d)", className.substring(className.lastIndexOf(".") + 1), stackTraceElement.getMethodName(), Integer.valueOf(stackTraceElement.getLineNumber()));
        if (TextUtils.isEmpty(f907a)) {
            return format;
        }
        return String.valueOf(f907a) + ":" + format;
    }

    public static void a(String str) {
        if (c) {
            String a2 = a(d.a());
            if (h != null) {
                h.a(a2, str);
            } else {
                Log.e(a2, str);
            }
        }
    }

    public static void a(String str, Throwable th) {
        if (c) {
            String a2 = a(d.a());
            if (h != null) {
                h.a(a2, str, th);
            } else {
                Log.e(a2, str, th);
            }
        }
    }

    public static void a(Throwable th) {
        if (f) {
            String a2 = a(d.a());
            if (h != null) {
                h.a(a2, th);
            } else {
                Log.w(a2, th);
            }
        }
    }
}
