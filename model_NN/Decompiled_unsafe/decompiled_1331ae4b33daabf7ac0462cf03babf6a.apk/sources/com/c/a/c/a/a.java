package com.c.a.c.a;

import android.os.Handler;
import android.os.Message;
import android.os.Process;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: CompatibleAsyncTask */
public abstract class a<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    public static final Executor f887a = new ThreadPoolExecutor(5, 128, 1, TimeUnit.SECONDS, d, c);

    /* renamed from: b  reason: collision with root package name */
    public static final Executor f888b = new c(null);
    private static final ThreadFactory c = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f889a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "CompatibleAsyncTask #" + this.f889a.getAndIncrement());
        }
    };
    private static final BlockingQueue<Runnable> d = new LinkedBlockingQueue(10);
    private static final b e = new b(null);
    private static volatile Executor f = f888b;
    private static /* synthetic */ int[] l;
    private final e<Params, Result> g = new e<Params, Result>() {
        public Result call() throws Exception {
            a.this.k.set(true);
            Process.setThreadPriority(10);
            return a.this.d(a.this.c(this.f904b));
        }
    };
    private final FutureTask<Result> h = new FutureTask<Result>(this.g) {
        /* access modifiers changed from: protected */
        public void done() {
            try {
                a.this.c(get());
            } catch (InterruptedException e) {
                com.c.a.c.c.a(e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
            } catch (CancellationException e3) {
                a.this.c(null);
            }
        }
    };
    private volatile d i = d.PENDING;
    private final AtomicBoolean j = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final AtomicBoolean k = new AtomicBoolean();

    /* compiled from: CompatibleAsyncTask */
    public enum d {
        PENDING,
        RUNNING,
        FINISHED
    }

    /* access modifiers changed from: protected */
    public abstract Result c(Params... paramsArr);

    static /* synthetic */ int[] d() {
        int[] iArr = l;
        if (iArr == null) {
            iArr = new int[d.values().length];
            try {
                iArr[d.FINISHED.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[d.PENDING.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[d.RUNNING.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            l = iArr;
        }
        return iArr;
    }

    /* compiled from: CompatibleAsyncTask */
    private static class c implements Executor {

        /* renamed from: a  reason: collision with root package name */
        final com.c.a.c.a.a.a<Runnable> f898a;

        /* renamed from: b  reason: collision with root package name */
        Runnable f899b;

        private c() {
            this.f898a = new com.c.a.c.a.a.a<>();
        }

        /* synthetic */ c(c cVar) {
            this();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f898a.offer(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        c.this.a();
                    }
                }
            });
            if (this.f899b == null) {
                a();
            }
        }

        /* access modifiers changed from: protected */
        public synchronized void a() {
            Runnable poll = this.f898a.poll();
            this.f899b = poll;
            if (poll != null) {
                a.f887a.execute(this.f899b);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(Result result) {
        if (!this.k.get()) {
            d(result);
        }
    }

    /* access modifiers changed from: private */
    public Result d(Object obj) {
        e.obtainMessage(1, new C0012a(this, obj)).sendToTarget();
        return obj;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void a(Object obj) {
    }

    /* access modifiers changed from: protected */
    public void b(Progress... progressArr) {
    }

    /* access modifiers changed from: protected */
    public void b(Result result) {
        b();
    }

    /* access modifiers changed from: protected */
    public void b() {
    }

    public final boolean c() {
        return this.j.get();
    }

    public final a<Params, Progress, Result> a(Executor executor, Object... objArr) {
        if (this.i != d.PENDING) {
            switch (d()[this.i.ordinal()]) {
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case 3:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.i = d.RUNNING;
        a();
        this.g.f904b = objArr;
        executor.execute(this.h);
        return this;
    }

    /* access modifiers changed from: protected */
    public final void d(Object... objArr) {
        if (!c()) {
            e.obtainMessage(2, new C0012a(this, objArr)).sendToTarget();
        }
    }

    /* access modifiers changed from: private */
    public void e(Result result) {
        if (c()) {
            b(result);
        } else {
            a((Object) result);
        }
        this.i = d.FINISHED;
    }

    /* compiled from: CompatibleAsyncTask */
    private static class b extends Handler {
        private b() {
        }

        /* synthetic */ b(b bVar) {
            this();
        }

        public void handleMessage(Message message) {
            C0012a aVar = (C0012a) message.obj;
            switch (message.what) {
                case 1:
                    aVar.f892a.e(aVar.f893b[0]);
                    return;
                case 2:
                    aVar.f892a.b((Object[]) aVar.f893b);
                    return;
                default:
                    return;
            }
        }
    }

    /* compiled from: CompatibleAsyncTask */
    private static abstract class e<Params, Result> implements Callable<Result> {

        /* renamed from: b  reason: collision with root package name */
        Params[] f904b;

        private e() {
        }

        /* synthetic */ e(e eVar) {
            this();
        }
    }

    /* renamed from: com.c.a.c.a.a$a  reason: collision with other inner class name */
    /* compiled from: CompatibleAsyncTask */
    private static class C0012a<Data> {

        /* renamed from: a  reason: collision with root package name */
        final a f892a;

        /* renamed from: b  reason: collision with root package name */
        final Data[] f893b;

        C0012a(a aVar, Data... dataArr) {
            this.f892a = aVar;
            this.f893b = dataArr;
        }
    }
}
