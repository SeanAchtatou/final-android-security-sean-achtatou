package com.c.a.c;

import java.util.ArrayList;
import java.util.List;

/* compiled from: CharsetUtils */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final List<String> f886a = new ArrayList();

    public static String a(String str, String str2, int i) {
        try {
            return new String(str.getBytes(a(str, i)), str2);
        } catch (Throwable th) {
            c.a(th);
            return str;
        }
    }

    public static String a(String str, int i) {
        for (String next : f886a) {
            if (b(str, next, i)) {
                return next;
            }
        }
        return "ISO-8859-1";
    }

    public static boolean b(String str, String str2, int i) {
        try {
            if (str.length() > i) {
                str = str.substring(0, i);
            }
            return str.equals(new String(str.getBytes(str2), str2));
        } catch (Throwable th) {
            return false;
        }
    }

    static {
        f886a.add("ISO-8859-1");
        f886a.add("GB2312");
        f886a.add("GBK");
        f886a.add("GB18030");
        f886a.add("US-ASCII");
        f886a.add("ASCII");
        f886a.add("ISO-2022-KR");
        f886a.add("ISO-8859-2");
        f886a.add("ISO-2022-JP");
        f886a.add("ISO-2022-JP-2");
        f886a.add("UTF-8");
    }
}
