package com.duoduo.mobads.toutiao;

public interface IAdSplash {
    void loadAd();

    void setAdListener(IAdSplashListener iAdSplashListener);

    void setLogo(int i);

    void setTestMode(boolean z);
}
