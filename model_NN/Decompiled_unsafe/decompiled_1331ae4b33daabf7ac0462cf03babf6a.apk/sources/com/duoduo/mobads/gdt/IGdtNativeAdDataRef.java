package com.duoduo.mobads.gdt;

import android.view.View;

public interface IGdtNativeAdDataRef {
    double getAPPPrice();

    int getAPPScore();

    int getAPPStatus();

    String getDesc();

    long getDownloadCount();

    String getIconUrl();

    String getImgUrl();

    int getProgress();

    String getTitle();

    boolean isAPP();

    void onClicked(View view);

    void onExposured(View view);
}
