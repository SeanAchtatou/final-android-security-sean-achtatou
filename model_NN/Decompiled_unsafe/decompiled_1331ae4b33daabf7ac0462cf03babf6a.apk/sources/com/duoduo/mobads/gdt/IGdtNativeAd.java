package com.duoduo.mobads.gdt;

public interface IGdtNativeAd {

    public enum a {
        Default,
        Inner,
        Sys
    }

    public enum b {
        Default,
        NOConfirm
    }

    void loadAD(int i);

    void setBrowserType(a aVar);

    void setDownAPPConfirmPolicy(b bVar);
}
