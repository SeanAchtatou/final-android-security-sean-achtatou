package com.duoduo.mobads.gdt;

import java.util.List;

public interface IGdtNativeAdListener {
    void onADError(IGdtNativeAdDataRef iGdtNativeAdDataRef, int i);

    void onADLoaded(List<IGdtNativeAdDataRef> list);

    void onADStatusChanged(IGdtNativeAdDataRef iGdtNativeAdDataRef);

    void onNoAD(int i);
}
