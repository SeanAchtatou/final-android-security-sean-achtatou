package com.duoduo.mobads;

public interface ISplashAdListener {
    void onAdClick();

    void onAdDismissed();

    void onAdFailed(String str);

    void onAdPresent();
}
