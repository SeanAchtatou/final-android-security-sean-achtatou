package com.duoduo.dynamicdex;

import android.support.v4.os.EnvironmentCompat;
import com.duoduo.dynamicdex.DDexLoader;
import com.duoduo.dynamicdex.data.DBaiduUtils;
import com.duoduo.dynamicdex.data.DGdtUtils;
import com.duoduo.dynamicdex.data.DToutiaoUtils;
import java.io.File;
import java.lang.reflect.Field;

public enum DuoMobAdUtils {
    Ins;
    
    public static final int ALL_CHANNEL = 3;
    public static final int CHANNEL_BAIDU = 1;
    public static final int CHANNEL_GDT = 2;
    public static final int ERROR_CLASS_NOT_FOUND = -2002;
    public static final int ERROR_FILE_NOT_FOUND = -2001;
    public static final int ERROR_LOAD_CLASS_FAILED = -2006;
    public static final int ERROR_MV_ASSETS_FAILED = -2005;
    public static final int ERROR_MV_BDF_FAILED = -2003;
    public static final int ERROR_ONLINE_NULL = -2004;
    public DBaiduUtils BaiduIns = new DBaiduUtils();
    public DGdtUtils GdtIns = new DGdtUtils();
    private int INIT_CHANNEL = 3;
    private String INNER_FILE_JAR = "duobaidu.jar";
    public DToutiaoUtils ToutiaoIns = new DToutiaoUtils();
    private final String VERSION = "0.0.8";
    private String mInnerSdkVer = EnvironmentCompat.MEDIA_UNKNOWN;
    private DDexLoader.DexLoadListener mLoadListener = new DDexLoader.DexLoadListener() {
        public void loadFailed(int i) {
            if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(i);
            }
        }

        public void loaded(ClassLoader classLoader) {
            boolean z;
            boolean z2 = false;
            if (classLoader == null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(DuoMobAdUtils.ERROR_CLASS_NOT_FOUND);
                return;
            }
            try {
                Class<?> loadClass = classLoader.loadClass("com.duoduo.mobads.wrapper.Constants");
                Field field = loadClass.getField("SDK_VER");
                if (field != null) {
                    DuoMobAdUtils.access$3(DuoMobAdUtils.this, field.get(loadClass).toString());
                }
            } catch (Exception e) {
            }
            if ((DuoMobAdUtils.access$4(DuoMobAdUtils.this) & 1) != 0) {
                DuoMobAdUtils.this.BaiduIns.load(DuoMobAdUtils.access$5(DuoMobAdUtils.this), classLoader);
                z = (!DuoMobAdUtils.this.BaiduIns.isEmpty()) & true;
            } else {
                z = true;
            }
            if ((DuoMobAdUtils.access$4(DuoMobAdUtils.this) & 2) != 0) {
                DuoMobAdUtils.this.GdtIns.load(DuoMobAdUtils.access$5(DuoMobAdUtils.this), classLoader);
                if (!DuoMobAdUtils.this.GdtIns.isEmpty()) {
                    z2 = true;
                }
                z &= z2;
            }
            if (!z) {
                if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                    DuoMobAdUtils.access$2(DuoMobAdUtils.this).loadFailed(DuoMobAdUtils.ERROR_LOAD_CLASS_FAILED);
                }
                try {
                    new File(String.valueOf(DuoMobApp.Ins.getApp().getDir("duo_jar", 0).getAbsolutePath()) + File.separator + DuoMobAdUtils.access$5(DuoMobAdUtils.this)).delete();
                } catch (Exception e2) {
                }
            } else if (DuoMobAdUtils.access$2(DuoMobAdUtils.this) != null) {
                DuoMobAdUtils.access$2(DuoMobAdUtils.this).loaded();
            }
        }
    };
    private DuoMobAdPrepareListener mPrepareListener = null;

    public interface DuoMobAdPrepareListener {
        void loadFailed(int i);

        void loaded();
    }

    public String getJarVer() {
        return this.mInnerSdkVer;
    }

    public String getVer() {
        return "0.0.8";
    }

    public void prepareFmAssert(String str, String str2, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        prepareFmAssert(3, str, str2, duoMobAdPrepareListener);
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x008c A[SYNTHETIC, Splitter:B:32:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0091 A[SYNTHETIC, Splitter:B:35:0x0091] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x00fe A[SYNTHETIC, Splitter:B:82:0x00fe] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0103 A[SYNTHETIC, Splitter:B:85:0x0103] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void prepareFmAssert(int r12, java.lang.String r13, java.lang.String r14, com.duoduo.dynamicdex.DuoMobAdUtils.DuoMobAdPrepareListener r15) {
        /*
            r11 = this;
            r0 = 0
            r11.INIT_CHANNEL = r12
            if (r13 == 0) goto L_0x0009
            java.lang.String r1 = ""
            if (r13 != r1) goto L_0x000d
        L_0x0009:
            r11.prepareFromOnline(r14, r15)
        L_0x000c:
            return
        L_0x000d:
            r1 = 0
            r3 = 0
            java.lang.String r2 = ".jar"
            boolean r2 = r13.endsWith(r2)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            if (r2 == 0) goto L_0x0068
            r11.INNER_FILE_JAR = r13     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
        L_0x0019:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            com.duoduo.dynamicdex.DuoMobApp r4 = com.duoduo.dynamicdex.DuoMobApp.Ins     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            android.app.Application r4 = r4.getApp()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r5 = "duo_jar"
            r6 = 0
            java.io.File r4 = r4.getDir(r5, r6)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = java.io.File.separator     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = r11.INNER_FILE_JAR     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = r2.toString()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.io.File r5 = new java.io.File     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            boolean r2 = r5.exists()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            if (r2 == 0) goto L_0x0099
            long r6 = r5.length()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            r8 = 0
            int r2 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r2 <= 0) goto L_0x0099
            r11.prepare(r4, r15)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            if (r0 == 0) goto L_0x0060
            r1.close()     // Catch:{ IOException -> 0x0107 }
        L_0x0060:
            if (r0 == 0) goto L_0x000c
            r3.close()     // Catch:{ IOException -> 0x0066 }
            goto L_0x000c
        L_0x0066:
            r0 = move-exception
            goto L_0x000c
        L_0x0068:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = java.lang.String.valueOf(r13)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r4 = ".jar"
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            r11.INNER_FILE_JAR = r2     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            goto L_0x0019
        L_0x007e:
            r1 = move-exception
            r1 = r0
        L_0x0080:
            if (r15 == 0) goto L_0x0087
            r2 = -2005(0xfffffffffffff82b, float:NaN)
            r15.loadFailed(r2)     // Catch:{ all -> 0x0120 }
        L_0x0087:
            r11.prepareFromOnline(r14, r15)     // Catch:{ all -> 0x0120 }
            if (r1 == 0) goto L_0x008f
            r1.close()     // Catch:{ IOException -> 0x010c }
        L_0x008f:
            if (r0 == 0) goto L_0x000c
            r0.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x000c
        L_0x0096:
            r0 = move-exception
            goto L_0x000c
        L_0x0099:
            com.duoduo.dynamicdex.DuoMobApp r1 = com.duoduo.dynamicdex.DuoMobApp.Ins     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            android.app.Application r1 = r1.getApp()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            java.io.InputStream r2 = r1.open(r13)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            if (r2 != 0) goto L_0x00c2
            if (r15 == 0) goto L_0x00b0
            r1 = -2001(0xfffffffffffff82f, float:NaN)
            r15.loadFailed(r1)     // Catch:{ Exception -> 0x0126, all -> 0x0114 }
        L_0x00b0:
            r11.prepareFromOnline(r14, r15)     // Catch:{ Exception -> 0x0126, all -> 0x0114 }
            if (r2 == 0) goto L_0x00b8
            r2.close()     // Catch:{ IOException -> 0x010a }
        L_0x00b8:
            if (r0 == 0) goto L_0x000c
            r3.close()     // Catch:{ IOException -> 0x00bf }
            goto L_0x000c
        L_0x00bf:
            r0 = move-exception
            goto L_0x000c
        L_0x00c2:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0126, all -> 0x0114 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0126, all -> 0x0114 }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x00f3, all -> 0x0119 }
        L_0x00cb:
            int r5 = r2.read(r3)     // Catch:{ Exception -> 0x00f3, all -> 0x0119 }
            if (r5 > 0) goto L_0x00ee
            r2.close()     // Catch:{ Exception -> 0x00f3, all -> 0x0119 }
            r2 = 0
            r1.flush()     // Catch:{ Exception -> 0x012a, all -> 0x011b }
            r1.close()     // Catch:{ Exception -> 0x012a, all -> 0x011b }
            r1 = 0
            r11.prepare(r4, r15)     // Catch:{ Exception -> 0x007e, all -> 0x00f7 }
            if (r0 == 0) goto L_0x00e4
            r2.close()     // Catch:{ IOException -> 0x0112 }
        L_0x00e4:
            if (r0 == 0) goto L_0x000c
            r1.close()     // Catch:{ IOException -> 0x00eb }
            goto L_0x000c
        L_0x00eb:
            r0 = move-exception
            goto L_0x000c
        L_0x00ee:
            r6 = 0
            r1.write(r3, r6, r5)     // Catch:{ Exception -> 0x00f3, all -> 0x0119 }
            goto L_0x00cb
        L_0x00f3:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x0080
        L_0x00f7:
            r1 = move-exception
            r2 = r0
            r10 = r0
            r0 = r1
            r1 = r10
        L_0x00fc:
            if (r2 == 0) goto L_0x0101
            r2.close()     // Catch:{ IOException -> 0x010e }
        L_0x0101:
            if (r1 == 0) goto L_0x0106
            r1.close()     // Catch:{ IOException -> 0x0110 }
        L_0x0106:
            throw r0
        L_0x0107:
            r1 = move-exception
            goto L_0x0060
        L_0x010a:
            r1 = move-exception
            goto L_0x00b8
        L_0x010c:
            r1 = move-exception
            goto L_0x008f
        L_0x010e:
            r2 = move-exception
            goto L_0x0101
        L_0x0110:
            r1 = move-exception
            goto L_0x0106
        L_0x0112:
            r2 = move-exception
            goto L_0x00e4
        L_0x0114:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x00fc
        L_0x0119:
            r0 = move-exception
            goto L_0x00fc
        L_0x011b:
            r2 = move-exception
            r10 = r2
            r2 = r0
            r0 = r10
            goto L_0x00fc
        L_0x0120:
            r2 = move-exception
            r10 = r2
            r2 = r1
            r1 = r0
            r0 = r10
            goto L_0x00fc
        L_0x0126:
            r1 = move-exception
            r1 = r2
            goto L_0x0080
        L_0x012a:
            r2 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duoduo.dynamicdex.DuoMobAdUtils.prepareFmAssert(int, java.lang.String, java.lang.String, com.duoduo.dynamicdex.DuoMobAdUtils$DuoMobAdPrepareListener):void");
    }

    private void prepareFromOnline(String str, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        if (str != null && str != "") {
            this.mPrepareListener = duoMobAdPrepareListener;
            DDexLoader.Ins.load(str, this.INNER_FILE_JAR, this.mLoadListener);
        } else if (duoMobAdPrepareListener != null) {
            duoMobAdPrepareListener.loadFailed(ERROR_ONLINE_NULL);
        }
    }

    public void prepare(String str, DuoMobAdPrepareListener duoMobAdPrepareListener) {
        this.mPrepareListener = duoMobAdPrepareListener;
        DDexLoader.Ins.loadFmStorage(str, this.mLoadListener);
    }
}
