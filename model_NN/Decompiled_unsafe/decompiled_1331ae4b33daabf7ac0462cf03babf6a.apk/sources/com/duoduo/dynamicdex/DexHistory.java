package com.duoduo.dynamicdex;

import java.util.HashMap;

public class DexHistory {
    public static HashMap<String, String> mHistory;

    static {
        mHistory = new HashMap<>();
        mHistory = new HashMap<>();
        mHistory.put("0.0.7", "加入今日头条");
        mHistory.put("0.0.8", "加入广电通");
        mHistory.put("0.0.9", "更新百度sdk5.1");
        mHistory.put("0.1.0", "取消今日头条");
        mHistory.put("0.1.1", "百度内置jar包更名");
    }

    public static String getDes(String str) {
        if (mHistory.containsKey(str)) {
            return mHistory.get(str);
        }
        return "没有此版本";
    }

    public static String showHistory() {
        return mHistory.toString();
    }
}
