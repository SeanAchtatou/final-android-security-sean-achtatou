package com.duoduo.dynamicdex.data;

import android.content.Context;
import android.view.ViewGroup;
import com.duoduo.mobads.IAdView;
import com.duoduo.mobads.IAdViewListener;
import com.duoduo.mobads.IBaiduNative;
import com.duoduo.mobads.IBaiduNativeNetworkListener;
import com.duoduo.mobads.IInterstitialAd;
import com.duoduo.mobads.ISplashAdListener;
import java.lang.reflect.Constructor;

public class DBaiduUtils implements IAdUtils {
    private static final String KEY_SP_FILE_NAME = "key_sp_file_name";
    private Class<?> mAdViewClazz = null;
    private Class<?> mBaiduNativeClazz = null;
    private boolean mCopySuc = false;
    private Class<?> mInterstitialAdClazz = null;
    private Class<?> mSplashAdClazz = null;

    public boolean isEmpty() {
        return this.mBaiduNativeClazz == null && this.mSplashAdClazz == null && this.mAdViewClazz == null && this.mInterstitialAdClazz == null;
    }

    public void load(String str, ClassLoader classLoader) {
        if (classLoader != null) {
            try {
                this.mBaiduNativeClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.BaiduNativeWrapper");
            } catch (Exception e) {
            }
            try {
                this.mSplashAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.SplashAdWrapper");
            } catch (Exception e2) {
            }
            try {
                this.mAdViewClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.AdViewWrapper");
            } catch (Exception e3) {
            }
            try {
                this.mInterstitialAdClazz = classLoader.loadClass("com.duoduo.mobads.wrapper.InterstitialAdWrapper");
            } catch (Exception e4) {
            }
            try {
                this.mCopySuc = copyJar(str, classLoader);
            } catch (Exception e5) {
            }
        }
    }

    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r2v2, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r2v5 */
    /* JADX WARN: Type inference failed for: r2v6 */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00b7 A[SYNTHETIC, Splitter:B:60:0x00b7] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00bc A[SYNTHETIC, Splitter:B:63:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00c8 A[SYNTHETIC, Splitter:B:69:0x00c8] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00cd A[SYNTHETIC, Splitter:B:72:0x00cd] */
    /* JADX WARNING: Removed duplicated region for block: B:99:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean copyJar(java.lang.String r13, java.lang.ClassLoader r14) {
        /*
            r12 = this;
            r1 = 1
            r2 = 0
            r0 = 0
            java.lang.String r4 = "/assets/bdxadsdk.jar"
            java.lang.Class<?> r3 = r12.mBaiduNativeClazz
            if (r3 != 0) goto L_0x000a
        L_0x0009:
            return r0
        L_0x000a:
            r5 = 0
            r6 = 0
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            com.duoduo.dynamicdex.DuoMobApp r8 = com.duoduo.dynamicdex.DuoMobApp.Ins     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            android.app.Application r8 = r8.getApp()     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r9 = "baidu_ad_sdk"
            r10 = 0
            java.io.File r8 = r8.getDir(r9, r10)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r8 = r8.getAbsolutePath()     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            r3.<init>(r8)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r8 = java.io.File.separator     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r8 = "__xadsdk__remote__final__builtin__.jar"
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            r7.<init>(r3)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            if (r13 == 0) goto L_0x006a
            java.lang.String r3 = "key_sp_file_name"
            java.lang.String r8 = ""
            java.lang.String r3 = com.duoduo.dynamicdex.utils.AppSPUtils.loadPrefString(r3, r8)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            boolean r3 = r13.equals(r3)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            if (r3 == 0) goto L_0x006a
            r3 = r1
        L_0x004c:
            if (r3 == 0) goto L_0x006c
            boolean r3 = r7.exists()     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            if (r3 == 0) goto L_0x006c
            long r8 = r7.length()     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            r10 = 0
            int r3 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r3 <= 0) goto L_0x006c
            if (r2 == 0) goto L_0x0063
            r5.close()     // Catch:{ IOException -> 0x00d1 }
        L_0x0063:
            if (r2 == 0) goto L_0x0068
            r6.close()     // Catch:{ IOException -> 0x00d3 }
        L_0x0068:
            r0 = r1
            goto L_0x0009
        L_0x006a:
            r3 = r0
            goto L_0x004c
        L_0x006c:
            java.lang.Class<?> r3 = r12.mBaiduNativeClazz     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            java.io.InputStream r4 = r3.getResourceAsStream(r4)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            if (r4 != 0) goto L_0x0081
            if (r4 == 0) goto L_0x0079
            r4.close()     // Catch:{ IOException -> 0x00d5 }
        L_0x0079:
            if (r2 == 0) goto L_0x0009
            r6.close()     // Catch:{ IOException -> 0x007f }
            goto L_0x0009
        L_0x007f:
            r1 = move-exception
            goto L_0x0009
        L_0x0081:
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00ed, all -> 0x00e1 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x00ed, all -> 0x00e1 }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x00b2, all -> 0x00e3 }
        L_0x008a:
            int r6 = r4.read(r5)     // Catch:{ Exception -> 0x00b2, all -> 0x00e3 }
            if (r6 > 0) goto L_0x00ad
            r4.close()     // Catch:{ Exception -> 0x00b2, all -> 0x00e3 }
            r4 = 0
            r3.flush()     // Catch:{ Exception -> 0x00f1, all -> 0x00e6 }
            r3.close()     // Catch:{ Exception -> 0x00f1, all -> 0x00e6 }
            r3 = 0
            java.lang.String r5 = "key_sp_file_name"
            com.duoduo.dynamicdex.utils.AppSPUtils.savePrefString(r5, r13)     // Catch:{ Exception -> 0x00ea, all -> 0x00c4 }
            if (r2 == 0) goto L_0x00a5
            r4.close()     // Catch:{ IOException -> 0x00d7 }
        L_0x00a5:
            if (r2 == 0) goto L_0x00aa
            r3.close()     // Catch:{ IOException -> 0x00d9 }
        L_0x00aa:
            r0 = r1
            goto L_0x0009
        L_0x00ad:
            r7 = 0
            r3.write(r5, r7, r6)     // Catch:{ Exception -> 0x00b2, all -> 0x00e3 }
            goto L_0x008a
        L_0x00b2:
            r1 = move-exception
            r1 = r3
            r2 = r4
        L_0x00b5:
            if (r2 == 0) goto L_0x00ba
            r2.close()     // Catch:{ IOException -> 0x00db }
        L_0x00ba:
            if (r1 == 0) goto L_0x0009
            r1.close()     // Catch:{ IOException -> 0x00c1 }
            goto L_0x0009
        L_0x00c1:
            r1 = move-exception
            goto L_0x0009
        L_0x00c4:
            r0 = move-exception
            r4 = r2
        L_0x00c6:
            if (r4 == 0) goto L_0x00cb
            r4.close()     // Catch:{ IOException -> 0x00dd }
        L_0x00cb:
            if (r2 == 0) goto L_0x00d0
            r2.close()     // Catch:{ IOException -> 0x00df }
        L_0x00d0:
            throw r0
        L_0x00d1:
            r0 = move-exception
            goto L_0x0063
        L_0x00d3:
            r0 = move-exception
            goto L_0x0068
        L_0x00d5:
            r1 = move-exception
            goto L_0x0079
        L_0x00d7:
            r0 = move-exception
            goto L_0x00a5
        L_0x00d9:
            r0 = move-exception
            goto L_0x00aa
        L_0x00db:
            r2 = move-exception
            goto L_0x00ba
        L_0x00dd:
            r1 = move-exception
            goto L_0x00cb
        L_0x00df:
            r1 = move-exception
            goto L_0x00d0
        L_0x00e1:
            r0 = move-exception
            goto L_0x00c6
        L_0x00e3:
            r0 = move-exception
            r2 = r3
            goto L_0x00c6
        L_0x00e6:
            r0 = move-exception
            r4 = r2
            r2 = r3
            goto L_0x00c6
        L_0x00ea:
            r1 = move-exception
            r1 = r2
            goto L_0x00b5
        L_0x00ed:
            r1 = move-exception
            r1 = r2
            r2 = r4
            goto L_0x00b5
        L_0x00f1:
            r1 = move-exception
            r1 = r3
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.duoduo.dynamicdex.data.DBaiduUtils.copyJar(java.lang.String, java.lang.ClassLoader):boolean");
    }

    public IBaiduNative getNativeAdIns(Context context, String str, String str2, IBaiduNativeNetworkListener iBaiduNativeNetworkListener) {
        if (!isReady(this.mBaiduNativeClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mBaiduNativeClazz.getConstructor(Context.class, String.class, String.class, IBaiduNativeNetworkListener.class);
            if (constructor != null) {
                return (IBaiduNative) constructor.newInstance(context, str, str2, iBaiduNativeNetworkListener);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Object getSplashAd(Context context, ViewGroup viewGroup, ISplashAdListener iSplashAdListener, String str, String str2, boolean z) {
        if (!isReady(this.mSplashAdClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mSplashAdClazz.getConstructor(Context.class, ViewGroup.class, ISplashAdListener.class, String.class, String.class, Boolean.TYPE);
            if (constructor != null) {
                return constructor.newInstance(context, viewGroup, iSplashAdListener, str, str2, Boolean.valueOf(z));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public IAdView getAdViewIns(Context context, String str, String str2, IAdViewListener iAdViewListener) {
        if (!isReady(this.mAdViewClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mAdViewClazz.getConstructor(Context.class, IAdViewListener.class, String.class, String.class);
            if (constructor != null) {
                return (IAdView) constructor.newInstance(context, iAdViewListener, str, str2);
            }
        } catch (Exception e) {
        }
        return null;
    }

    public IInterstitialAd getInterstitialAdIns(Context context, String str, String str2) {
        if (!isReady(this.mInterstitialAdClazz)) {
            return null;
        }
        try {
            Constructor<?> constructor = this.mInterstitialAdClazz.getConstructor(Context.class, String.class, String.class);
            if (constructor != null) {
                return (IInterstitialAd) constructor.newInstance(context, str, str2);
            }
        } catch (Exception e) {
        }
        return null;
    }

    private boolean isReady(Class<?> cls) {
        return this.mCopySuc && cls != null;
    }
}
