package com.duoduo.dynamicdex.data;

public interface IAdUtils {
    boolean isEmpty();

    void load(String str, ClassLoader classLoader);
}
