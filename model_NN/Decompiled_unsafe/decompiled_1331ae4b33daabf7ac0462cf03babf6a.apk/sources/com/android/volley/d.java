package com.android.volley;

/* compiled from: DefaultRetryPolicy */
public class d implements p {

    /* renamed from: a  reason: collision with root package name */
    private int f728a;

    /* renamed from: b  reason: collision with root package name */
    private int f729b;
    private final int c;
    private final float d;

    public d() {
        this(2500, 1, 1.0f);
    }

    public d(int i, int i2, float f) {
        this.f728a = i;
        this.c = i2;
        this.d = f;
    }

    public int a() {
        return this.f728a;
    }

    public int b() {
        return this.f729b;
    }

    public void a(s sVar) throws s {
        this.f729b++;
        this.f728a = (int) (((float) this.f728a) + (((float) this.f728a) * this.d));
        if (!c()) {
            throw sVar;
        }
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        return this.f729b <= this.c;
    }
}
