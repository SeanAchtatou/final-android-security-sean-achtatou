package com.android.volley;

import android.os.Handler;
import java.util.concurrent.Executor;

/* compiled from: ExecutorDelivery */
public class e implements o {

    /* renamed from: a  reason: collision with root package name */
    private final Executor f730a;

    public e(final Handler handler) {
        this.f730a = new Executor() {
            public void execute(Runnable runnable) {
                handler.post(runnable);
            }
        };
    }

    public void a(l<?> lVar, n<?> nVar) {
        a(lVar, nVar, null);
    }

    public void a(l<?> lVar, n<?> nVar, Runnable runnable) {
        lVar.v();
        lVar.a("post-response");
        this.f730a.execute(new a(lVar, nVar, runnable));
    }

    public void a(l<?> lVar, s sVar) {
        lVar.a("post-error");
        this.f730a.execute(new a(lVar, n.a(sVar), null));
    }

    /* compiled from: ExecutorDelivery */
    private class a implements Runnable {

        /* renamed from: b  reason: collision with root package name */
        private final l f734b;
        private final n c;
        private final Runnable d;

        public a(l lVar, n nVar, Runnable runnable) {
            this.f734b = lVar;
            this.c = nVar;
            this.d = runnable;
        }

        public void run() {
            if (this.f734b.h()) {
                this.f734b.b("canceled-at-delivery");
                return;
            }
            if (this.c.a()) {
                this.f734b.b((Object) this.c.f749a);
            } else {
                this.f734b.b(this.c.c);
            }
            if (this.c.d) {
                this.f734b.a("intermediate-response");
            } else {
                this.f734b.b("done");
            }
            if (this.d != null) {
                this.d.run();
            }
        }
    }
}
