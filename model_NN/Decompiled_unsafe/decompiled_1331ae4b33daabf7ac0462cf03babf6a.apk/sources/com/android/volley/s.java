package com.android.volley;

/* compiled from: VolleyError */
public class s extends Exception {

    /* renamed from: a  reason: collision with root package name */
    public final i f751a;

    public s() {
        this.f751a = null;
    }

    public s(i iVar) {
        this.f751a = iVar;
    }

    public s(Throwable th) {
        super(th);
        this.f751a = null;
    }
}
