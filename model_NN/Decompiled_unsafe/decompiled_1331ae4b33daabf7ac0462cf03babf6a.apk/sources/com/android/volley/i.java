package com.android.volley;

import com.igexin.download.Downloads;
import java.util.Map;

/* compiled from: NetworkResponse */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public final int f737a;

    /* renamed from: b  reason: collision with root package name */
    public final byte[] f738b;
    public final Map<String, String> c;
    public final boolean d;

    public i(int i, byte[] bArr, Map<String, String> map, boolean z) {
        this.f737a = i;
        this.f738b = bArr;
        this.c = map;
        this.d = z;
    }

    public i(byte[] bArr, Map<String, String> map) {
        this(Downloads.STATUS_SUCCESS, bArr, map, false);
    }
}
