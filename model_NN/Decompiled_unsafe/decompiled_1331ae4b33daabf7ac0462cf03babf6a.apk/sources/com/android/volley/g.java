package com.android.volley;

import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import java.util.concurrent.BlockingQueue;

/* compiled from: NetworkDispatcher */
public class g extends Thread {

    /* renamed from: a  reason: collision with root package name */
    private final BlockingQueue<l> f735a;

    /* renamed from: b  reason: collision with root package name */
    private final f f736b;
    private final b c;
    private final o d;
    private volatile boolean e = false;

    public g(BlockingQueue<l> blockingQueue, f fVar, b bVar, o oVar) {
        this.f735a = blockingQueue;
        this.f736b = fVar;
        this.c = bVar;
        this.d = oVar;
    }

    public void a() {
        this.e = true;
        interrupt();
    }

    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                l take = this.f735a.take();
                try {
                    take.a("network-queue-take");
                    if (take.h()) {
                        take.b("network-discard-cancelled");
                    } else {
                        if (Build.VERSION.SDK_INT >= 14) {
                            TrafficStats.setThreadStatsTag(take.c());
                        }
                        i a2 = this.f736b.a(take);
                        take.a("network-http-complete");
                        if (!a2.d || !take.w()) {
                            n a3 = take.a(a2);
                            take.a("network-parse-complete");
                            if (take.r() && a3.f750b != null) {
                                this.c.a(take.e(), a3.f750b);
                                take.a("network-cache-written");
                            }
                            take.v();
                            this.d.a(take, a3);
                        } else {
                            take.b("not-modified");
                        }
                    }
                } catch (s e2) {
                    a(take, e2);
                } catch (Exception e3) {
                    t.a(e3, "Unhandled exception %s", e3.toString());
                    this.d.a(take, new s(e3));
                }
            } catch (InterruptedException e4) {
                if (this.e) {
                    return;
                }
            }
        }
    }

    private void a(l<?> lVar, s sVar) {
        this.d.a(lVar, lVar.a(sVar));
    }
}
