package com.android.volley;

import android.os.SystemClock;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: VolleyLog */
public class t {

    /* renamed from: a  reason: collision with root package name */
    public static String f752a = "Volley";

    /* renamed from: b  reason: collision with root package name */
    public static boolean f753b = Log.isLoggable(f752a, 2);

    public static void a(String str, Object... objArr) {
        if (f753b) {
            Log.v(f752a, e(str, objArr));
        }
    }

    public static void b(String str, Object... objArr) {
        Log.d(f752a, e(str, objArr));
    }

    public static void c(String str, Object... objArr) {
        Log.e(f752a, e(str, objArr));
    }

    public static void a(Throwable th, String str, Object... objArr) {
        Log.e(f752a, e(str, objArr), th);
    }

    public static void d(String str, Object... objArr) {
        Log.wtf(f752a, e(str, objArr));
    }

    private static String e(String str, Object... objArr) {
        String str2;
        if (objArr != null) {
            str = String.format(Locale.US, str, objArr);
        }
        StackTraceElement[] stackTrace = new Throwable().fillInStackTrace().getStackTrace();
        int i = 2;
        while (true) {
            if (i >= stackTrace.length) {
                str2 = "<unknown>";
                break;
            } else if (!stackTrace[i].getClass().equals(t.class)) {
                String className = stackTrace[i].getClassName();
                String substring = className.substring(className.lastIndexOf(46) + 1);
                str2 = String.valueOf(substring.substring(substring.lastIndexOf(36) + 1)) + "." + stackTrace[i].getMethodName();
                break;
            } else {
                i++;
            }
        }
        return String.format(Locale.US, "[%d] %s: %s", Long.valueOf(Thread.currentThread().getId()), str2, str);
    }

    /* compiled from: VolleyLog */
    static class a {

        /* renamed from: a  reason: collision with root package name */
        public static final boolean f754a = t.f753b;

        /* renamed from: b  reason: collision with root package name */
        private final List<C0009a> f755b = new ArrayList();
        private boolean c = false;

        a() {
        }

        /* renamed from: com.android.volley.t$a$a  reason: collision with other inner class name */
        /* compiled from: VolleyLog */
        private static class C0009a {

            /* renamed from: a  reason: collision with root package name */
            public final String f756a;

            /* renamed from: b  reason: collision with root package name */
            public final long f757b;
            public final long c;

            public C0009a(String str, long j, long j2) {
                this.f756a = str;
                this.f757b = j;
                this.c = j2;
            }
        }

        public synchronized void a(String str, long j) {
            if (this.c) {
                throw new IllegalStateException("Marker added to finished log");
            }
            this.f755b.add(new C0009a(str, j, SystemClock.elapsedRealtime()));
        }

        public synchronized void a(String str) {
            this.c = true;
            long a2 = a();
            if (a2 > 0) {
                long j = this.f755b.get(0).c;
                t.b("(%-4d ms) %s", Long.valueOf(a2), str);
                long j2 = j;
                for (C0009a next : this.f755b) {
                    long j3 = next.c;
                    t.b("(+%-4d) [%2d] %s", Long.valueOf(j3 - j2), Long.valueOf(next.f757b), next.f756a);
                    j2 = j3;
                }
            }
        }

        /* access modifiers changed from: protected */
        public void finalize() throws Throwable {
            if (!this.c) {
                a("Request on the loose");
                t.c("Marker log finalized without finish() - uncaught exit point for request", new Object[0]);
            }
        }

        private long a() {
            if (this.f755b.size() == 0) {
                return 0;
            }
            return this.f755b.get(this.f755b.size() - 1).c - this.f755b.get(0).c;
        }
    }
}
