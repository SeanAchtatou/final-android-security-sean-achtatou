package com.android.volley;

import com.android.volley.b;

/* compiled from: Response */
public class n<T> {

    /* renamed from: a  reason: collision with root package name */
    public final T f749a;

    /* renamed from: b  reason: collision with root package name */
    public final b.a f750b;
    public final s c;
    public boolean d;

    /* compiled from: Response */
    public interface a {
        void onErrorResponse(s sVar);
    }

    /* compiled from: Response */
    public interface b<T> {
        void onResponse(Object obj);
    }

    public static <T> n<T> a(T t, b.a aVar) {
        return new n<>(t, aVar);
    }

    public static <T> n<T> a(s sVar) {
        return new n<>(sVar);
    }

    public boolean a() {
        return this.c == null;
    }

    private n(T t, b.a aVar) {
        this.d = false;
        this.f749a = t;
        this.f750b = aVar;
        this.c = null;
    }

    private n(s sVar) {
        this.d = false;
        this.f749a = null;
        this.f750b = null;
        this.c = sVar;
    }
}
