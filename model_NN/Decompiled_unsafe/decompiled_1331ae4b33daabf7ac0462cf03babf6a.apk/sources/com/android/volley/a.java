package com.android.volley;

import android.content.Intent;

/* compiled from: AuthFailureError */
public class a extends s {

    /* renamed from: b  reason: collision with root package name */
    private Intent f721b;

    public a() {
    }

    public a(i iVar) {
        super(iVar);
    }

    public String getMessage() {
        if (this.f721b != null) {
            return "User needs to (re)enter credentials.";
        }
        return super.getMessage();
    }
}
