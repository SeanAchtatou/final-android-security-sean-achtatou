package com.android.volley.toolbox;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.android.volley.d;
import com.android.volley.k;
import com.android.volley.l;
import com.android.volley.n;
import com.android.volley.p;
import com.android.volley.t;

/* compiled from: ImageRequest */
public class i extends l<Bitmap> {
    private static final Object e = new Object();

    /* renamed from: a  reason: collision with root package name */
    private final n.b<Bitmap> f787a;

    /* renamed from: b  reason: collision with root package name */
    private final Bitmap.Config f788b;
    private final int c;
    private final int d;

    public i(String str, n.b<Bitmap> bVar, int i, int i2, Bitmap.Config config, n.a aVar) {
        super(0, str, aVar);
        a((p) new d(1000, 2, 2.0f));
        this.f787a = bVar;
        this.f788b = config;
        this.c = i;
        this.d = i2;
    }

    public l.a s() {
        return l.a.LOW;
    }

    private static int b(int i, int i2, int i3, int i4) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (i == 0) {
            return (int) ((((double) i2) / ((double) i4)) * ((double) i3));
        }
        if (i2 == 0) {
            return i;
        }
        double d2 = ((double) i4) / ((double) i3);
        if (((double) i) * d2 > ((double) i2)) {
            return (int) (((double) i2) / d2);
        }
        return i;
    }

    /* access modifiers changed from: protected */
    public n<Bitmap> a(com.android.volley.i iVar) {
        n<Bitmap> a2;
        synchronized (e) {
            try {
                a2 = b(iVar);
            } catch (OutOfMemoryError e2) {
                t.c("Caught OOM for %d byte image, url=%s", Integer.valueOf(iVar.f738b.length), d());
                a2 = n.a(new k(e2));
            }
        }
        return a2;
    }

    private n<Bitmap> b(com.android.volley.i iVar) {
        Bitmap bitmap;
        byte[] bArr = iVar.f738b;
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (this.c == 0 && this.d == 0) {
            options.inPreferredConfig = this.f788b;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i = options.outWidth;
            int i2 = options.outHeight;
            int b2 = b(this.c, this.d, i, i2);
            int b3 = b(this.d, this.c, i2, i);
            options.inJustDecodeBounds = false;
            options.inSampleSize = a(i, i2, b2, b3);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (decodeByteArray == null || (decodeByteArray.getWidth() <= b2 && decodeByteArray.getHeight() <= b3)) {
                bitmap = decodeByteArray;
            } else {
                bitmap = Bitmap.createScaledBitmap(decodeByteArray, b2, b3, true);
                decodeByteArray.recycle();
            }
        }
        if (bitmap == null) {
            return n.a(new k(iVar));
        }
        return n.a(bitmap, e.a(iVar));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void b(Bitmap bitmap) {
        this.f787a.onResponse(bitmap);
    }

    static int a(int i, int i2, int i3, int i4) {
        float f = 1.0f;
        while (((double) (f * 2.0f)) <= Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4))) {
            f *= 2.0f;
        }
        return (int) f;
    }
}
