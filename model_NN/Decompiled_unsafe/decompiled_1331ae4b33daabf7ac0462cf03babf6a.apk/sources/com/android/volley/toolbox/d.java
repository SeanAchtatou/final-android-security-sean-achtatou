package com.android.volley.toolbox;

import com.android.volley.a;
import com.android.volley.l;
import java.io.IOException;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

/* compiled from: HttpClientStack */
public class d implements f {

    /* renamed from: a  reason: collision with root package name */
    protected final HttpClient f773a;

    public d(HttpClient httpClient) {
        this.f773a = httpClient;
    }

    private static void a(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpUriRequest.setHeader(next, map.get(next));
        }
    }

    public HttpResponse a(l<?> lVar, Map<String, String> map) throws IOException, a {
        HttpUriRequest b2 = b(lVar, map);
        a(b2, map);
        a(b2, lVar.i());
        a(b2);
        HttpParams params = b2.getParams();
        int t = lVar.t();
        HttpConnectionParams.setConnectionTimeout(params, 5000);
        HttpConnectionParams.setSoTimeout(params, t);
        return this.f773a.execute(b2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.l<?>):void
     arg types: [org.apache.http.client.methods.HttpPost, com.android.volley.l<?>]
     candidates:
      com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
      com.android.volley.toolbox.d.a(com.android.volley.l<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.f.a(com.android.volley.l<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.l<?>):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.l<?>):void
     arg types: [org.apache.http.client.methods.HttpPut, com.android.volley.l<?>]
     candidates:
      com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpUriRequest, java.util.Map<java.lang.String, java.lang.String>):void
      com.android.volley.toolbox.d.a(com.android.volley.l<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.f.a(com.android.volley.l<?>, java.util.Map<java.lang.String, java.lang.String>):org.apache.http.HttpResponse
      com.android.volley.toolbox.d.a(org.apache.http.client.methods.HttpEntityEnclosingRequestBase, com.android.volley.l<?>):void */
    static HttpUriRequest b(l<?> lVar, Map<String, String> map) throws a {
        switch (lVar.a()) {
            case -1:
                byte[] m = lVar.m();
                if (m == null) {
                    return new HttpGet(lVar.d());
                }
                HttpPost httpPost = new HttpPost(lVar.d());
                httpPost.addHeader("Content-Type", lVar.l());
                httpPost.setEntity(new ByteArrayEntity(m));
                return httpPost;
            case 0:
                return new HttpGet(lVar.d());
            case 1:
                HttpPost httpPost2 = new HttpPost(lVar.d());
                httpPost2.addHeader("Content-Type", lVar.p());
                a((HttpEntityEnclosingRequestBase) httpPost2, lVar);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(lVar.d());
                httpPut.addHeader("Content-Type", lVar.p());
                a((HttpEntityEnclosingRequestBase) httpPut, lVar);
                return httpPut;
            case 3:
                return new HttpDelete(lVar.d());
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    private static void a(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, l<?> lVar) throws a {
        byte[] q = lVar.q();
        if (q != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(q));
        }
    }

    /* access modifiers changed from: protected */
    public void a(HttpUriRequest httpUriRequest) throws IOException {
    }
}
