package com.android.volley.toolbox;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import com.android.volley.l;
import com.android.volley.m;
import com.android.volley.n;
import com.android.volley.s;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/* compiled from: ImageLoader */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final m f776a;

    /* renamed from: b  reason: collision with root package name */
    private int f777b;
    private final b c;
    /* access modifiers changed from: private */
    public final HashMap<String, a> d;
    /* access modifiers changed from: private */
    public final HashMap<String, a> e;
    private final Handler f;
    /* access modifiers changed from: private */
    public Runnable g;

    /* compiled from: ImageLoader */
    public interface b {
        Bitmap a(String str);

        void a(String str, Bitmap bitmap);
    }

    /* compiled from: ImageLoader */
    public interface d extends n.a {
        void a(c cVar, boolean z);
    }

    public c a(String str, d dVar) {
        return a(str, dVar, 0, 0);
    }

    public c a(String str, d dVar, int i, int i2) {
        a();
        final String a2 = a(str, i, i2);
        Bitmap a3 = this.c.a(a2);
        if (a3 != null) {
            c cVar = new c(a3, str, null, null);
            dVar.a(cVar, true);
            return cVar;
        }
        c cVar2 = new c(null, str, a2, dVar);
        dVar.a(cVar2, true);
        a aVar = this.d.get(a2);
        if (aVar != null) {
            aVar.a(cVar2);
            return cVar2;
        }
        i iVar = new i(str, new n.b<Bitmap>() {
            /* renamed from: a */
            public void onResponse(Bitmap bitmap) {
                h.this.a(a2, bitmap);
            }
        }, i, i2, Bitmap.Config.RGB_565, new n.a() {
            public void onErrorResponse(s sVar) {
                h.this.a(a2, sVar);
            }
        });
        this.f776a.a((l) iVar);
        this.d.put(a2, new a(iVar, cVar2));
        return cVar2;
    }

    /* access modifiers changed from: private */
    public void a(String str, Bitmap bitmap) {
        this.c.a(str, bitmap);
        a remove = this.d.remove(str);
        if (remove != null) {
            remove.c = bitmap;
            a(str, remove);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, s sVar) {
        a remove = this.d.remove(str);
        remove.a(sVar);
        if (remove != null) {
            a(str, remove);
        }
    }

    /* compiled from: ImageLoader */
    public class c {
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public Bitmap f786b;
        /* access modifiers changed from: private */
        public final d c;
        private final String d;
        private final String e;

        public c(Bitmap bitmap, String str, String str2, d dVar) {
            this.f786b = bitmap;
            this.e = str;
            this.d = str2;
            this.c = dVar;
        }

        public void a() {
            if (this.c != null) {
                a aVar = (a) h.this.d.get(this.d);
                if (aVar == null) {
                    a aVar2 = (a) h.this.e.get(this.d);
                    if (aVar2 != null) {
                        aVar2.b(this);
                        if (aVar2.e.size() == 0) {
                            h.this.e.remove(this.d);
                        }
                    }
                } else if (aVar.b(this)) {
                    h.this.d.remove(this.d);
                }
            }
        }

        public Bitmap b() {
            return this.f786b;
        }

        public String c() {
            return this.e;
        }
    }

    /* compiled from: ImageLoader */
    private class a {

        /* renamed from: b  reason: collision with root package name */
        private final l<?> f784b;
        /* access modifiers changed from: private */
        public Bitmap c;
        private s d;
        /* access modifiers changed from: private */
        public final LinkedList<c> e = new LinkedList<>();

        public a(l<?> lVar, c cVar) {
            this.f784b = lVar;
            this.e.add(cVar);
        }

        public void a(s sVar) {
            this.d = sVar;
        }

        public s a() {
            return this.d;
        }

        public void a(c cVar) {
            this.e.add(cVar);
        }

        public boolean b(c cVar) {
            this.e.remove(cVar);
            if (this.e.size() != 0) {
                return false;
            }
            this.f784b.g();
            return true;
        }
    }

    private void a(String str, a aVar) {
        this.e.put(str, aVar);
        if (this.g == null) {
            this.g = new Runnable() {
                public void run() {
                    for (a aVar : h.this.e.values()) {
                        Iterator it = aVar.e.iterator();
                        while (it.hasNext()) {
                            c cVar = (c) it.next();
                            if (cVar.c != null) {
                                if (aVar.a() == null) {
                                    cVar.f786b = aVar.c;
                                    cVar.c.a(cVar, false);
                                } else {
                                    cVar.c.onErrorResponse(aVar.a());
                                }
                            }
                        }
                    }
                    h.this.e.clear();
                    h.this.g = (Runnable) null;
                }
            };
            this.f.postDelayed(this.g, (long) this.f777b);
        }
    }

    private void a() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("ImageLoader must be invoked from the main thread.");
        }
    }

    private static String a(String str, int i, int i2) {
        return new StringBuilder(str.length() + 12).append("#W").append(i).append("#H").append(i2).append(str).toString();
    }
}
