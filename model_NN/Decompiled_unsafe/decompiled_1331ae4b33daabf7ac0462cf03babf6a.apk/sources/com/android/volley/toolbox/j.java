package com.android.volley.toolbox;

import com.android.volley.i;
import com.android.volley.k;
import com.android.volley.n;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: JsonObjectRequest */
public class j extends k<JSONObject> {
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public j(int i, String str, JSONObject jSONObject, n.b<JSONObject> bVar, n.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public j(String str, JSONObject jSONObject, n.b<JSONObject> bVar, n.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }

    /* access modifiers changed from: protected */
    public n<JSONObject> a(i iVar) {
        try {
            return n.a(new JSONObject(new String(iVar.f738b, e.a(iVar.c))), e.a(iVar));
        } catch (UnsupportedEncodingException e) {
            return n.a(new k(e));
        } catch (JSONException e2) {
            return n.a(new k(e2));
        }
    }
}
