package com.android.volley.toolbox;

import com.android.volley.l;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

/* compiled from: HurlStack */
public class g implements f {

    /* renamed from: a  reason: collision with root package name */
    private final a f774a;

    /* renamed from: b  reason: collision with root package name */
    private final SSLSocketFactory f775b;

    /* compiled from: HurlStack */
    public interface a {
        String a(String str);
    }

    public g() {
        this(null);
    }

    public g(a aVar) {
        this(aVar, null);
    }

    public g(a aVar, SSLSocketFactory sSLSocketFactory) {
        this.f774a = aVar;
        this.f775b = sSLSocketFactory;
    }

    public HttpResponse a(l<?> lVar, Map<String, String> map) throws IOException, com.android.volley.a {
        String str;
        String d = lVar.d();
        HashMap hashMap = new HashMap();
        hashMap.putAll(lVar.i());
        hashMap.putAll(map);
        if (this.f774a != null) {
            str = this.f774a.a(d);
            if (str == null) {
                throw new IOException("URL blocked by rewriter: " + d);
            }
        } else {
            str = d;
        }
        HttpURLConnection a2 = a(new URL(str), lVar);
        for (String str2 : hashMap.keySet()) {
            a2.addRequestProperty(str2, (String) hashMap.get(str2));
        }
        a(a2, lVar);
        ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
        if (a2.getResponseCode() == -1) {
            throw new IOException("Could not retrieve response code from HttpUrlConnection.");
        }
        BasicHttpResponse basicHttpResponse = new BasicHttpResponse(new BasicStatusLine(protocolVersion, a2.getResponseCode(), a2.getResponseMessage()));
        basicHttpResponse.setEntity(a(a2));
        for (Map.Entry next : a2.getHeaderFields().entrySet()) {
            if (next.getKey() != null) {
                basicHttpResponse.addHeader(new BasicHeader((String) next.getKey(), (String) ((List) next.getValue()).get(0)));
            }
        }
        return basicHttpResponse;
    }

    private static HttpEntity a(HttpURLConnection httpURLConnection) {
        InputStream errorStream;
        BasicHttpEntity basicHttpEntity = new BasicHttpEntity();
        try {
            errorStream = httpURLConnection.getInputStream();
        } catch (IOException e) {
            errorStream = httpURLConnection.getErrorStream();
        }
        basicHttpEntity.setContent(errorStream);
        basicHttpEntity.setContentLength((long) httpURLConnection.getContentLength());
        basicHttpEntity.setContentEncoding(httpURLConnection.getContentEncoding());
        basicHttpEntity.setContentType(httpURLConnection.getContentType());
        return basicHttpEntity;
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }

    private HttpURLConnection a(URL url, l<?> lVar) throws IOException {
        HttpURLConnection a2 = a(url);
        int t = lVar.t();
        a2.setConnectTimeout(t);
        a2.setReadTimeout(t);
        a2.setUseCaches(false);
        a2.setDoInput(true);
        if ("https".equals(url.getProtocol()) && this.f775b != null) {
            ((HttpsURLConnection) a2).setSSLSocketFactory(this.f775b);
        }
        return a2;
    }

    static void a(HttpURLConnection httpURLConnection, l<?> lVar) throws IOException, com.android.volley.a {
        switch (lVar.a()) {
            case -1:
                byte[] m = lVar.m();
                if (m != null) {
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.addRequestProperty("Content-Type", lVar.l());
                    DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
                    dataOutputStream.write(m);
                    dataOutputStream.close();
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                b(httpURLConnection, lVar);
                return;
            case 2:
                httpURLConnection.setRequestMethod(HttpProxyConstants.PUT);
                b(httpURLConnection, lVar);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }

    private static void b(HttpURLConnection httpURLConnection, l<?> lVar) throws IOException, com.android.volley.a {
        byte[] q = lVar.q();
        if (q != null) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", lVar.p());
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.write(q);
            dataOutputStream.close();
        }
    }
}
