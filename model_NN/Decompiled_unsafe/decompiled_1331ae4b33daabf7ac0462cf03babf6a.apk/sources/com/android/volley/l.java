package com.android.volley;

import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import com.android.volley.b;
import com.android.volley.n;
import com.android.volley.t;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collections;
import java.util.Map;

/* compiled from: Request */
public abstract class l<T> implements Comparable<l<T>> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final t.a f739a;

    /* renamed from: b  reason: collision with root package name */
    private final int f740b;
    private final String c;
    private final int d;
    private final n.a e;
    private Integer f;
    private m g;
    private boolean h;
    private boolean i;
    private boolean j;
    private long k;
    private p l;
    private b.a m;
    private Object n;

    /* compiled from: Request */
    public enum a {
        LOW,
        NORMAL,
        HIGH,
        IMMEDIATE
    }

    /* access modifiers changed from: protected */
    public abstract n<T> a(i iVar);

    /* access modifiers changed from: protected */
    public abstract void b(T t);

    public l(int i2, String str, n.a aVar) {
        t.a aVar2;
        if (t.a.f754a) {
            aVar2 = new t.a();
        } else {
            aVar2 = null;
        }
        this.f739a = aVar2;
        this.h = true;
        this.i = false;
        this.j = false;
        this.k = 0;
        this.m = null;
        this.f740b = i2;
        this.c = str;
        this.e = aVar;
        a((p) new d());
        this.d = TextUtils.isEmpty(str) ? 0 : Uri.parse(str).getHost().hashCode();
    }

    public int a() {
        return this.f740b;
    }

    public void a(Object obj) {
        this.n = obj;
    }

    public Object b() {
        return this.n;
    }

    public int c() {
        return this.d;
    }

    public void a(p pVar) {
        this.l = pVar;
    }

    public void a(String str) {
        if (t.a.f754a) {
            this.f739a.a(str, Thread.currentThread().getId());
        } else if (this.k == 0) {
            this.k = SystemClock.elapsedRealtime();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(final String str) {
        if (this.g != null) {
            this.g.b(this);
        }
        if (t.a.f754a) {
            final long id = Thread.currentThread().getId();
            if (Looper.myLooper() != Looper.getMainLooper()) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        l.this.f739a.a(str, id);
                        l.this.f739a.a(toString());
                    }
                });
                return;
            }
            this.f739a.a(str, id);
            this.f739a.a(toString());
            return;
        }
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.k;
        if (elapsedRealtime >= 3000) {
            t.b("%d ms: %s", Long.valueOf(elapsedRealtime), toString());
        }
    }

    public void a(m mVar) {
        this.g = mVar;
    }

    public final void a(int i2) {
        this.f = Integer.valueOf(i2);
    }

    public String d() {
        return this.c;
    }

    public String e() {
        return d();
    }

    public void a(b.a aVar) {
        this.m = aVar;
    }

    public b.a f() {
        return this.m;
    }

    public void g() {
        this.i = true;
    }

    public boolean h() {
        return this.i;
    }

    public Map<String, String> i() throws a {
        return Collections.emptyMap();
    }

    /* access modifiers changed from: protected */
    public Map<String, String> j() throws a {
        return n();
    }

    /* access modifiers changed from: protected */
    public String k() {
        return o();
    }

    public String l() {
        return p();
    }

    public byte[] m() throws a {
        Map<String, String> j2 = j();
        if (j2 == null || j2.size() <= 0) {
            return null;
        }
        return a(j2, k());
    }

    /* access modifiers changed from: protected */
    public Map<String, String> n() throws a {
        return null;
    }

    /* access modifiers changed from: protected */
    public String o() {
        return "UTF-8";
    }

    public String p() {
        return "application/x-www-form-urlencoded; charset=" + o();
    }

    public byte[] q() throws a {
        Map<String, String> n2 = n();
        if (n2 == null || n2.size() <= 0) {
            return null;
        }
        return a(n2, o());
    }

    private byte[] a(Map<String, String> map, String str) {
        StringBuilder sb = new StringBuilder();
        try {
            for (Map.Entry next : map.entrySet()) {
                sb.append(URLEncoder.encode((String) next.getKey(), str));
                sb.append('=');
                sb.append(URLEncoder.encode((String) next.getValue(), str));
                sb.append('&');
            }
            return sb.toString().getBytes(str);
        } catch (UnsupportedEncodingException e2) {
            throw new RuntimeException("Encoding not supported: " + str, e2);
        }
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final boolean r() {
        return this.h;
    }

    public a s() {
        return a.NORMAL;
    }

    public final int t() {
        return this.l.a();
    }

    public p u() {
        return this.l;
    }

    public void v() {
        this.j = true;
    }

    public boolean w() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public s a(s sVar) {
        return sVar;
    }

    public void b(s sVar) {
        if (this.e != null) {
            this.e.onErrorResponse(sVar);
        }
    }

    /* renamed from: a */
    public int compareTo(l lVar) {
        a s = s();
        a s2 = lVar.s();
        if (s == s2) {
            return this.f.intValue() - lVar.f.intValue();
        }
        return s2.ordinal() - s.ordinal();
    }

    public String toString() {
        return String.valueOf(this.i ? "[X] " : "[ ] ") + d() + " " + ("0x" + Integer.toHexString(c())) + " " + s() + " " + this.f;
    }
}
