package com.meizu.cloud.pushsdk.handler.a;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.handler.c;
import com.meizu.cloud.pushsdk.notification.e;
import com.meizu.cloud.pushsdk.pushtracer.utils.Util;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.meizu.cloud.pushsdk.util.PushPreferencesUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class a<T> implements c {

    /* renamed from: a  reason: collision with root package name */
    private com.meizu.cloud.pushsdk.handler.a f1662a;

    /* renamed from: b  reason: collision with root package name */
    private Context f1663b;
    private Map<Integer, String> c;

    protected a(Context context, com.meizu.cloud.pushsdk.handler.a aVar) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        this.f1663b = context.getApplicationContext();
        this.f1662a = aVar;
        this.c = new HashMap();
        this.c.put(2, "MESSAGE_TYPE_PUSH_SERVICE_V2");
        this.c.put(4, "MESSAGE_TYPE_PUSH_SERVICE_V3");
        this.c.put(16, "MESSAGE_TYPE_REGISTER");
        this.c.put(32, "MESSAGE_TYPE_UNREGISTER");
        this.c.put(8, "MESSAGE_TYPE_THROUGH");
        this.c.put(64, "MESSAGE_TYPE_NOTIFICATION_CLICK");
        this.c.put(128, "MESSAGE_TYPE_NOTIFICATION_DELETE");
        this.c.put(256, "MESSAGE_TYPE_PUSH_SWITCH_STATUS");
        this.c.put(512, "MESSAGE_TYPE_PUSH_REGISTER_STATUS");
        this.c.put(2048, "MESSAGE_TYPE_PUSH_SUBTAGS_STATUS");
        this.c.put(1024, "MESSAGE_TYPE_PUSH_UNREGISTER_STATUS");
        this.c.put(4096, "MESSAGE_TYPE_PUSH_SUBALIAS_STATUS");
        this.c.put(8192, "MESSAGE_TYPE_SCHEDULE_NOTIFICATION");
        this.c.put(16384, "MESSAGE_TYPE_RECEIVE_NOTIFY_MESSAGE");
    }

    private String a(int i) {
        return this.c.get(Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    public e a(Object obj) {
        return null;
    }

    public String a(String str, Map<String, String> map) {
        if (TextUtils.isEmpty(str)) {
            if (map != null) {
                String str2 = map.get("sk");
                str = TextUtils.isEmpty(str2) ? Util.mapToJSONObject(map).toString() : str2;
            } else {
                str = null;
            }
        }
        DebugLogger.e("AbstractMessageHandler", "self json " + str);
        return str;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Object obj, e eVar);

    /* access modifiers changed from: protected */
    public boolean a(int i, String str) {
        boolean z = true;
        if (i == 0) {
            z = PushPreferencesUtils.getNotificationMessageSwitchStatus(c(), str);
        } else if (i == 1) {
            z = PushPreferencesUtils.getThroughMessageSwitchStatus(c(), str);
        }
        DebugLogger.e("AbstractMessageHandler", str + (i == 0 ? " canNotificationMessage " : " canThroughMessage ") + z);
        return z;
    }

    public boolean a(String str) {
        try {
            return c().getPackageName().equals(new JSONObject(str).getString("appId"));
        } catch (Exception e) {
            DebugLogger.e("AbstractMessageHandler", "parse notification error");
            return false;
        }
    }

    public com.meizu.cloud.pushsdk.handler.a b() {
        return this.f1662a;
    }

    /* access modifiers changed from: protected */
    public void b(Object obj) {
    }

    public boolean b(Intent intent) {
        boolean z = false;
        boolean z2 = true;
        if (a(intent)) {
            DebugLogger.e("AbstractMessageHandler", "current message Type " + a(a()));
            Object c2 = c(intent);
            DebugLogger.e("AbstractMessageHandler", "current Handler message " + c2);
            b(c2);
            switch (d(c2)) {
                case 0:
                    DebugLogger.e("AbstractMessageHandler", "schedule send message off, send message directly");
                    z = true;
                    break;
                case 1:
                    DebugLogger.e("AbstractMessageHandler", "expire notification, dont show message");
                    z2 = false;
                    break;
                case 2:
                    DebugLogger.e("AbstractMessageHandler", "notification on time ,show message");
                    z = true;
                    break;
                case 3:
                    DebugLogger.e("AbstractMessageHandler", "schedule notification");
                    e(c2);
                    z = true;
                    z2 = false;
                    break;
                default:
                    z2 = false;
                    break;
            }
            if (z && z2) {
                a(c2, a(c2));
                c(c2);
                DebugLogger.e("AbstractMessageHandler", "send message end ");
            }
        }
        return z;
    }

    public Context c() {
        return this.f1663b;
    }

    /* access modifiers changed from: protected */
    public abstract T c(Intent intent);

    /* access modifiers changed from: protected */
    public void c(Object obj) {
    }

    /* access modifiers changed from: protected */
    public int d(Object obj) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public String d(Intent intent) {
        String stringExtra = intent.getStringExtra(PushConstants.MZ_PUSH_MESSAGE_STATISTICS_IMEI_KEY);
        if (!TextUtils.isEmpty(stringExtra)) {
            return stringExtra;
        }
        String deviceId = PushPreferencesUtils.getDeviceId(c());
        if (!TextUtils.isEmpty(deviceId)) {
            return deviceId;
        }
        String deviceId2 = MzSystemUtils.getDeviceId(c());
        PushPreferencesUtils.putDeviceId(c(), deviceId2);
        DebugLogger.e("AbstractMessageHandler", "force get deviceId " + deviceId2);
        return deviceId2;
    }

    /* access modifiers changed from: protected */
    public String e(Intent intent) {
        return intent.getStringExtra(PushConstants.EXTRA_APP_PUSH_TASK_ID);
    }

    /* access modifiers changed from: protected */
    public void e(Object obj) {
    }

    /* access modifiers changed from: protected */
    public String f(Intent intent) {
        return intent.getStringExtra(PushConstants.EXTRA_APP_PUSH_SEQ_ID);
    }

    /* access modifiers changed from: protected */
    public String g(Intent intent) {
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_APP_PUSH_SERVICE_DEFAULT_PACKAGE_NAME);
        return TextUtils.isEmpty(stringExtra) ? c().getPackageName() : stringExtra;
    }

    /* access modifiers changed from: protected */
    public String h(Intent intent) {
        String stringExtra = intent.getStringExtra(PushConstants.EXTRA_APP_PUSH_TASK_TIMES_TAMP);
        DebugLogger.e("AbstractMessageHandler", "receive push timestamp from pushservice " + stringExtra);
        return TextUtils.isEmpty(stringExtra) ? String.valueOf(System.currentTimeMillis() / 1000) : stringExtra;
    }

    public String i(Intent intent) {
        return intent.getStringExtra(PushConstants.MZ_PUSH_MESSAGE_METHOD);
    }
}
