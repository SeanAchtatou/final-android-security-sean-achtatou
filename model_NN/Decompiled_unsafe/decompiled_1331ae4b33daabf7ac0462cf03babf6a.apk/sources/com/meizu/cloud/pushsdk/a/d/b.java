package com.meizu.cloud.pushsdk.a.d;

import java.util.ArrayList;
import java.util.List;

public class b extends j {

    /* renamed from: a  reason: collision with root package name */
    private static final g f1557a = g.a("application/x-www-form-urlencoded");

    /* renamed from: b  reason: collision with root package name */
    private final List<String> f1558b;
    private final List<String> c;

    public final class a {

        /* renamed from: a  reason: collision with root package name */
        private final List<String> f1559a = new ArrayList();

        /* renamed from: b  reason: collision with root package name */
        private final List<String> f1560b = new ArrayList();

        public a a(String str, String str2) {
            this.f1559a.add(f.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true));
            this.f1560b.add(f.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", false, false, true, true));
            return this;
        }

        public b a() {
            return new b(this.f1559a, this.f1560b);
        }

        public a b(String str, String str2) {
            this.f1559a.add(f.a(str, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true));
            this.f1560b.add(f.a(str2, " \"':;<=>@[]^`{}|/\\?#&!$(),~", true, false, true, true));
            return this;
        }
    }

    private b(List<String> list, List<String> list2) {
        this.f1558b = m.a(list);
        this.c = m.a(list2);
    }

    private long a(com.meizu.cloud.pushsdk.a.h.b bVar, boolean z) {
        long j = 0;
        com.meizu.cloud.pushsdk.a.h.a aVar = z ? new com.meizu.cloud.pushsdk.a.h.a() : bVar.b();
        int size = this.f1558b.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                aVar.b(38);
            }
            aVar.b(this.f1558b.get(i));
            aVar.b(61);
            aVar.b(this.c.get(i));
        }
        if (z) {
            j = aVar.a();
            aVar.j();
        }
        return j;
    }

    public g a() {
        return f1557a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.b.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long
     arg types: [com.meizu.cloud.pushsdk.a.h.b, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.io.File):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.lang.String):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, byte[]):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.b.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long */
    public void a(com.meizu.cloud.pushsdk.a.h.b bVar) {
        a(bVar, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.b.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.io.File):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, java.lang.String):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.j.a(com.meizu.cloud.pushsdk.a.d.g, byte[]):com.meizu.cloud.pushsdk.a.d.j
      com.meizu.cloud.pushsdk.a.d.b.a(com.meizu.cloud.pushsdk.a.h.b, boolean):long */
    public long b() {
        return a((com.meizu.cloud.pushsdk.a.h.b) null, true);
    }
}
