package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.a.b;
import com.meizu.cloud.pushsdk.a.a.d;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1595a = a.class.getSimpleName();
    private static a d = null;

    /* renamed from: b  reason: collision with root package name */
    private final Set<b> f1596b = new HashSet();
    private AtomicInteger c = new AtomicInteger();

    public static a a() {
        if (d == null) {
            synchronized (a.class) {
                if (d == null) {
                    d = new a();
                }
            }
        }
        return d;
    }

    public b a(b bVar) {
        synchronized (this.f1596b) {
            try {
                this.f1596b.add(bVar);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            bVar.a(b());
            if (bVar.d() == d.IMMEDIATE) {
                bVar.a(com.meizu.cloud.pushsdk.a.b.b.a().b().b().submit(new c(bVar)));
            } else {
                bVar.a(com.meizu.cloud.pushsdk.a.b.b.a().b().a().submit(new c(bVar)));
            }
            com.meizu.cloud.pushsdk.a.a.a.a("addRequest: after addition - mCurrentRequests size: " + this.f1596b.size());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return bVar;
    }

    public int b() {
        return this.c.incrementAndGet();
    }

    public void b(b bVar) {
        synchronized (this.f1596b) {
            try {
                this.f1596b.remove(bVar);
                com.meizu.cloud.pushsdk.a.a.a.a("finish: after removal - mCurrentRequests size: " + this.f1596b.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
