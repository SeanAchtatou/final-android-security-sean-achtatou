package com.meizu.cloud.pushsdk.a.h;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Logger;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static final Logger f1615a = Logger.getLogger(f.class.getName());

    private f() {
    }

    public static b a(k kVar) {
        if (kVar != null) {
            return new g(kVar);
        }
        throw new IllegalArgumentException("sink == null");
    }

    public static c a(l lVar) {
        if (lVar != null) {
            return new h(lVar);
        }
        throw new IllegalArgumentException("source == null");
    }

    public static k a(OutputStream outputStream) {
        return a(outputStream, new m());
    }

    private static k a(final OutputStream outputStream, final m mVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("out == null");
        } else if (mVar != null) {
            return new k() {
                public void a(a aVar, long j) {
                    n.a(aVar.f1610b, 0, j);
                    while (j > 0) {
                        m.this.a();
                        i iVar = aVar.f1609a;
                        int min = (int) Math.min(j, (long) (iVar.c - iVar.f1626b));
                        outputStream.write(iVar.f1625a, iVar.f1626b, min);
                        iVar.f1626b += min;
                        j -= (long) min;
                        aVar.f1610b -= (long) min;
                        if (iVar.f1626b == iVar.c) {
                            aVar.f1609a = iVar.a();
                            j.a(iVar);
                        }
                    }
                }

                public void close() {
                    outputStream.close();
                }

                public void flush() {
                    outputStream.flush();
                }

                public String toString() {
                    return "sink(" + outputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }

    public static l a(File file) {
        if (file != null) {
            return a(new FileInputStream(file));
        }
        throw new IllegalArgumentException("file == null");
    }

    public static l a(InputStream inputStream) {
        return a(inputStream, new m());
    }

    private static l a(final InputStream inputStream, final m mVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (mVar != null) {
            return new l() {
                public long b(a aVar, long j) {
                    if (j < 0) {
                        throw new IllegalArgumentException("byteCount < 0: " + j);
                    } else if (j == 0) {
                        return 0;
                    } else {
                        m.this.a();
                        i c = aVar.c(1);
                        int read = inputStream.read(c.f1625a, c.c, (int) Math.min(j, (long) (2048 - c.c)));
                        if (read == -1) {
                            return -1;
                        }
                        c.c += read;
                        aVar.f1610b += (long) read;
                        return (long) read;
                    }
                }

                public void close() {
                    inputStream.close();
                }

                public String toString() {
                    return "source(" + inputStream + ")";
                }
            };
        } else {
            throw new IllegalArgumentException("timeout == null");
        }
    }
}
