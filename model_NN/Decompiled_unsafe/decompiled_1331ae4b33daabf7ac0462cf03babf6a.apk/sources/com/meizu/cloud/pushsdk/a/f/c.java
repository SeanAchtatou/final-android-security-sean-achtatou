package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.a.b;
import com.meizu.cloud.pushsdk.a.a.d;
import com.meizu.cloud.pushsdk.a.a.e;
import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.k;

public class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    public final int f1598a;

    /* renamed from: b  reason: collision with root package name */
    public final b f1599b;
    private final d c;

    public c(b bVar) {
        this.f1599b = bVar;
        this.f1598a = bVar.f();
        this.c = bVar.d();
    }

    private void a(final b bVar, final a aVar) {
        com.meizu.cloud.pushsdk.a.b.b.a().b().c().execute(new Runnable() {
            public void run() {
                bVar.b(aVar);
                bVar.p();
            }
        });
    }

    private void b() {
        k kVar = null;
        try {
            kVar = b.a(this.f1599b);
            if (kVar == null) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (this.f1599b.g() == e.OK_HTTP_RESPONSE) {
                this.f1599b.b(kVar);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            } else if (kVar.a() >= 400) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(kVar), this.f1599b, kVar.a()));
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            } else {
                com.meizu.cloud.pushsdk.a.a.c a2 = this.f1599b.a(kVar);
                if (!a2.b()) {
                    a(this.f1599b, a2.c());
                    com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
                    return;
                }
                a2.a(kVar);
                this.f1599b.a(a2);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            }
        } catch (Exception e) {
            a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        } finally {
            com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
        }
    }

    private void c() {
        try {
            k b2 = b.b(this.f1599b);
            if (b2 == null) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (b2.a() >= 400) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(b2), this.f1599b, b2.a()));
            } else {
                this.f1599b.j();
            }
        } catch (Exception e) {
            a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        }
    }

    private void d() {
        k kVar = null;
        try {
            kVar = b.c(this.f1599b);
            if (kVar == null) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            } else if (this.f1599b.g() == e.OK_HTTP_RESPONSE) {
                this.f1599b.b(kVar);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            } else if (kVar.a() >= 400) {
                a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(kVar), this.f1599b, kVar.a()));
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            } else {
                com.meizu.cloud.pushsdk.a.a.c a2 = this.f1599b.a(kVar);
                if (!a2.b()) {
                    a(this.f1599b, a2.c());
                    com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
                    return;
                }
                a2.a(kVar);
                this.f1599b.a(a2);
                com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
            }
        } catch (Exception e) {
            a(this.f1599b, com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        } finally {
            com.meizu.cloud.pushsdk.a.i.a.a(kVar, this.f1599b);
        }
    }

    public d a() {
        return this.c;
    }

    public void run() {
        com.meizu.cloud.pushsdk.a.a.a.a("execution started : " + this.f1599b.toString());
        switch (this.f1599b.h()) {
            case 0:
                b();
                break;
            case 1:
                c();
                break;
            case 2:
                d();
                break;
        }
        com.meizu.cloud.pushsdk.a.a.a.a("execution done : " + this.f1599b.toString());
    }
}
