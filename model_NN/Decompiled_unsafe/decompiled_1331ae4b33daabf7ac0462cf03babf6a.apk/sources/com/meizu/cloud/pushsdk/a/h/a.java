package com.meizu.cloud.pushsdk.a.h;

import android.support.v4.media.session.PlaybackStateCompat;
import com.igexin.download.Downloads;
import java.io.EOFException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public final class a implements b, c, Cloneable {
    private static final byte[] c = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: a  reason: collision with root package name */
    i f1609a;

    /* renamed from: b  reason: collision with root package name */
    long f1610b;

    public int a(byte[] bArr, int i, int i2) {
        n.a((long) bArr.length, (long) i, (long) i2);
        i iVar = this.f1609a;
        if (iVar == null) {
            return -1;
        }
        int min = Math.min(i2, iVar.c - iVar.f1626b);
        System.arraycopy(iVar.f1625a, iVar.f1626b, bArr, i, min);
        iVar.f1626b += min;
        this.f1610b -= (long) min;
        if (iVar.f1626b != iVar.c) {
            return min;
        }
        this.f1609a = iVar.a();
        j.a(iVar);
        return min;
    }

    public long a() {
        return this.f1610b;
    }

    public long a(l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("source == null");
        }
        long j = 0;
        while (true) {
            long b2 = lVar.b(this, PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH);
            if (b2 == -1) {
                return j;
            }
            j += b2;
        }
    }

    public a a(int i) {
        if (i < 128) {
            b(i);
        } else if (i < 2048) {
            b((i >> 6) | Downloads.STATUS_RUNNING);
            b((i & 63) | 128);
        } else if (i < 65536) {
            if (i < 55296 || i > 57343) {
                b((i >> 12) | 224);
                b(((i >> 6) & 63) | 128);
                b((i & 63) | 128);
            } else {
                throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
            }
        } else if (i <= 1114111) {
            b((i >> 18) | 240);
            b(((i >> 12) & 63) | 128);
            b(((i >> 6) & 63) | 128);
            b((i & 63) | 128);
        } else {
            throw new IllegalArgumentException("Unexpected code point: " + Integer.toHexString(i));
        }
        return this;
    }

    /* renamed from: a */
    public a b(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("byteString == null");
        }
        dVar.a(this);
        return this;
    }

    /* renamed from: a */
    public a b(String str) {
        return a(str, 0, str.length());
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 152 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.meizu.cloud.pushsdk.a.h.a a(java.lang.String r10, int r11, int r12) {
        /*
            r9 = this;
            r8 = 57343(0xdfff, float:8.0355E-41)
            r7 = 128(0x80, float:1.794E-43)
            if (r10 != 0) goto L_0x000f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "string == null"
            r0.<init>(r1)
            throw r0
        L_0x000f:
            if (r11 >= 0) goto L_0x002a
            java.lang.IllegalAccessError r0 = new java.lang.IllegalAccessError
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "beginIndex < 0: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x002a:
            if (r12 >= r11) goto L_0x004f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex < beginIndex: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " < "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004f:
            int r0 = r10.length()
            if (r12 <= r0) goto L_0x0090
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "endIndex > string.length: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r2 = " > "
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = r10.length()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007c:
            r0 = 0
        L_0x007d:
            r2 = 56319(0xdbff, float:7.892E-41)
            if (r1 > r2) goto L_0x0089
            r2 = 56320(0xdc00, float:7.8921E-41)
            if (r0 < r2) goto L_0x0089
            if (r0 <= r8) goto L_0x0114
        L_0x0089:
            r0 = 63
            r9.b(r0)
            int r11 = r11 + 1
        L_0x0090:
            if (r11 >= r12) goto L_0x0145
            char r1 = r10.charAt(r11)
            if (r1 >= r7) goto L_0x00d2
            r0 = 1
            com.meizu.cloud.pushsdk.a.h.i r2 = r9.c(r0)
            byte[] r3 = r2.f1625a
            int r0 = r2.c
            int r4 = r0 - r11
            int r0 = 2048 - r4
            int r5 = java.lang.Math.min(r12, r0)
            int r0 = r11 + 1
            int r6 = r4 + r11
            byte r1 = (byte) r1
            r3[r6] = r1
        L_0x00b0:
            if (r0 >= r5) goto L_0x00b8
            char r6 = r10.charAt(r0)
            if (r6 < r7) goto L_0x00ca
        L_0x00b8:
            int r1 = r0 + r4
            int r3 = r2.c
            int r1 = r1 - r3
            int r3 = r2.c
            int r3 = r3 + r1
            r2.c = r3
            long r2 = r9.f1610b
            long r4 = (long) r1
            long r2 = r2 + r4
            r9.f1610b = r2
        L_0x00c8:
            r11 = r0
            goto L_0x0090
        L_0x00ca:
            int r1 = r0 + 1
            int r0 = r0 + r4
            byte r6 = (byte) r6
            r3[r0] = r6
            r0 = r1
            goto L_0x00b0
        L_0x00d2:
            r0 = 2048(0x800, float:2.87E-42)
            if (r1 >= r0) goto L_0x00e7
            int r0 = r1 >> 6
            r0 = r0 | 192(0xc0, float:2.69E-43)
            r9.b(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.b(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x00e7:
            r0 = 55296(0xd800, float:7.7486E-41)
            if (r1 < r0) goto L_0x00ee
            if (r1 <= r8) goto L_0x0108
        L_0x00ee:
            int r0 = r1 >> 12
            r0 = r0 | 224(0xe0, float:3.14E-43)
            r9.b(r0)
            int r0 = r1 >> 6
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.b(r0)
            r0 = r1 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.b(r0)
            int r0 = r11 + 1
            goto L_0x00c8
        L_0x0108:
            int r0 = r11 + 1
            if (r0 >= r12) goto L_0x007c
            int r0 = r11 + 1
            char r0 = r10.charAt(r0)
            goto L_0x007d
        L_0x0114:
            r2 = 65536(0x10000, float:9.18355E-41)
            r3 = -55297(0xffffffffffff27ff, float:NaN)
            r1 = r1 & r3
            int r1 = r1 << 10
            r3 = -56321(0xffffffffffff23ff, float:NaN)
            r0 = r0 & r3
            r0 = r0 | r1
            int r0 = r0 + r2
            int r1 = r0 >> 18
            r1 = r1 | 240(0xf0, float:3.36E-43)
            r9.b(r1)
            int r1 = r0 >> 12
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.b(r1)
            int r1 = r0 >> 6
            r1 = r1 & 63
            r1 = r1 | 128(0x80, float:1.794E-43)
            r9.b(r1)
            r0 = r0 & 63
            r0 = r0 | 128(0x80, float:1.794E-43)
            r9.b(r0)
            int r0 = r11 + 2
            goto L_0x00c8
        L_0x0145:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.h.a.a(java.lang.String, int, int):com.meizu.cloud.pushsdk.a.h.a");
    }

    public String a(long j, Charset charset) {
        n.a(this.f1610b, 0, j);
        if (charset == null) {
            throw new IllegalArgumentException("charset == null");
        } else if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        } else if (j == 0) {
            return "";
        } else {
            i iVar = this.f1609a;
            if (((long) iVar.f1626b) + j > ((long) iVar.c)) {
                return new String(a(j), charset);
            }
            String str = new String(iVar.f1625a, iVar.f1626b, (int) j, charset);
            iVar.f1626b = (int) (((long) iVar.f1626b) + j);
            this.f1610b -= j;
            if (iVar.f1626b != iVar.c) {
                return str;
            }
            this.f1609a = iVar.a();
            j.a(iVar);
            return str;
        }
    }

    public void a(a aVar, long j) {
        if (aVar == null) {
            throw new IllegalArgumentException("source == null");
        } else if (aVar == this) {
            throw new IllegalArgumentException("source == this");
        } else {
            n.a(aVar.f1610b, 0, j);
            while (j > 0) {
                if (j < ((long) (aVar.f1609a.c - aVar.f1609a.f1626b))) {
                    i iVar = this.f1609a != null ? this.f1609a.g : null;
                    if (iVar != null && iVar.e) {
                        if ((((long) iVar.c) + j) - ((long) (iVar.d ? 0 : iVar.f1626b)) <= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH) {
                            aVar.f1609a.a(iVar, (int) j);
                            aVar.f1610b -= j;
                            this.f1610b += j;
                            return;
                        }
                    }
                    aVar.f1609a = aVar.f1609a.a((int) j);
                }
                i iVar2 = aVar.f1609a;
                long j2 = (long) (iVar2.c - iVar2.f1626b);
                aVar.f1609a = iVar2.a();
                if (this.f1609a == null) {
                    this.f1609a = iVar2;
                    i iVar3 = this.f1609a;
                    i iVar4 = this.f1609a;
                    i iVar5 = this.f1609a;
                    iVar4.g = iVar5;
                    iVar3.f = iVar5;
                } else {
                    this.f1609a.g.a(iVar2).b();
                }
                aVar.f1610b -= j2;
                this.f1610b += j2;
                j -= j2;
            }
        }
    }

    public void a(byte[] bArr) {
        int i = 0;
        while (i < bArr.length) {
            int a2 = a(bArr, i, bArr.length - i);
            if (a2 == -1) {
                throw new EOFException();
            }
            i += a2;
        }
    }

    public byte[] a(long j) {
        n.a(this.f1610b, 0, j);
        if (j > 2147483647L) {
            throw new IllegalArgumentException("byteCount > Integer.MAX_VALUE: " + j);
        }
        byte[] bArr = new byte[((int) j)];
        a(bArr);
        return bArr;
    }

    public long b(a aVar, long j) {
        if (aVar == null) {
            throw new IllegalArgumentException("sink == null");
        } else if (j < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + j);
        } else if (this.f1610b == 0) {
            return -1;
        } else {
            if (j > this.f1610b) {
                j = this.f1610b;
            }
            aVar.a(this, j);
            return j;
        }
    }

    public a b() {
        return this;
    }

    public a b(int i) {
        i c2 = c(1);
        byte[] bArr = c2.f1625a;
        int i2 = c2.c;
        c2.c = i2 + 1;
        bArr[i2] = (byte) i;
        this.f1610b++;
        return this;
    }

    /* renamed from: b */
    public a c(byte[] bArr) {
        if (bArr != null) {
            return c(bArr, 0, bArr.length);
        }
        throw new IllegalArgumentException("source == null");
    }

    /* renamed from: b */
    public a c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            throw new IllegalArgumentException("source == null");
        }
        n.a((long) bArr.length, (long) i, (long) i2);
        int i3 = i + i2;
        while (i < i3) {
            i c2 = c(1);
            int min = Math.min(i3 - i, 2048 - c2.c);
            System.arraycopy(bArr, i, c2.f1625a, c2.c, min);
            i += min;
            c2.c = min + c2.c;
        }
        this.f1610b += (long) i2;
        return this;
    }

    public void b(long j) {
        while (j > 0) {
            if (this.f1609a == null) {
                throw new EOFException();
            }
            int min = (int) Math.min(j, (long) (this.f1609a.c - this.f1609a.f1626b));
            this.f1610b -= (long) min;
            j -= (long) min;
            i iVar = this.f1609a;
            iVar.f1626b = min + iVar.f1626b;
            if (this.f1609a.f1626b == this.f1609a.c) {
                i iVar2 = this.f1609a;
                this.f1609a = iVar2.a();
                j.a(iVar2);
            }
        }
    }

    /* renamed from: c */
    public a e(long j) {
        boolean z;
        long j2;
        if (j == 0) {
            return b(48);
        }
        if (j < 0) {
            j2 = -j;
            if (j2 < 0) {
                return b("-9223372036854775808");
            }
            z = true;
        } else {
            z = false;
            j2 = j;
        }
        int i = j2 < 100000000 ? j2 < 10000 ? j2 < 100 ? j2 < 10 ? 1 : 2 : j2 < 1000 ? 3 : 4 : j2 < 1000000 ? j2 < 100000 ? 5 : 6 : j2 < 10000000 ? 7 : 8 : j2 < 1000000000000L ? j2 < 10000000000L ? j2 < 1000000000 ? 9 : 10 : j2 < 100000000000L ? 11 : 12 : j2 < 1000000000000000L ? j2 < 10000000000000L ? 13 : j2 < 100000000000000L ? 14 : 15 : j2 < 100000000000000000L ? j2 < 10000000000000000L ? 16 : 17 : j2 < 1000000000000000000L ? 18 : 19;
        if (z) {
            i++;
        }
        i c2 = c(i);
        byte[] bArr = c2.f1625a;
        int i2 = c2.c + i;
        while (j2 != 0) {
            i2--;
            bArr[i2] = c[(int) (j2 % 10)];
            j2 /= 10;
        }
        if (z) {
            bArr[i2 - 1] = 45;
        }
        c2.c += i;
        this.f1610b = ((long) i) + this.f1610b;
        return this;
    }

    /* access modifiers changed from: package-private */
    public i c(int i) {
        if (i < 1 || i > 2048) {
            throw new IllegalArgumentException();
        } else if (this.f1609a == null) {
            this.f1609a = j.a();
            i iVar = this.f1609a;
            i iVar2 = this.f1609a;
            i iVar3 = this.f1609a;
            iVar2.g = iVar3;
            iVar.f = iVar3;
            return iVar3;
        } else {
            i iVar4 = this.f1609a.g;
            return (iVar4.c + i > 2048 || !iVar4.e) ? iVar4.a(j.a()) : iVar4;
        }
    }

    public boolean c() {
        return this.f1610b == 0;
    }

    public void close() {
    }

    public a d(long j) {
        if (j == 0) {
            return b(48);
        }
        int numberOfTrailingZeros = (Long.numberOfTrailingZeros(Long.highestOneBit(j)) / 4) + 1;
        i c2 = c(numberOfTrailingZeros);
        byte[] bArr = c2.f1625a;
        int i = c2.c;
        for (int i2 = (c2.c + numberOfTrailingZeros) - 1; i2 >= i; i2--) {
            bArr[i2] = c[(int) (15 & j)];
            j >>>= 4;
        }
        c2.c += numberOfTrailingZeros;
        this.f1610b = ((long) numberOfTrailingZeros) + this.f1610b;
        return this;
    }

    public InputStream d() {
        return new InputStream() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ClspMth{java.lang.Math.min(long, long):long}
             arg types: [long, int]
             candidates:
              ClspMth{java.lang.Math.min(double, double):double}
              ClspMth{java.lang.Math.min(float, float):float}
              ClspMth{java.lang.Math.min(int, int):int}
              ClspMth{java.lang.Math.min(long, long):long} */
            public int available() {
                return (int) Math.min(a.this.f1610b, 2147483647L);
            }

            public void close() {
            }

            public int read() {
                if (a.this.f1610b > 0) {
                    return a.this.f() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                }
                return -1;
            }

            public int read(byte[] bArr, int i, int i2) {
                return a.this.a(bArr, i, i2);
            }

            public String toString() {
                return a.this + ".inputStream()";
            }
        };
    }

    public long e() {
        long j = this.f1610b;
        if (j == 0) {
            return 0;
        }
        i iVar = this.f1609a.g;
        return (iVar.c >= 2048 || !iVar.e) ? j : j - ((long) (iVar.c - iVar.f1626b));
    }

    public boolean equals(Object obj) {
        long j = 0;
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a)) {
            return false;
        }
        a aVar = (a) obj;
        if (this.f1610b != aVar.f1610b) {
            return false;
        }
        if (this.f1610b == 0) {
            return true;
        }
        i iVar = this.f1609a;
        i iVar2 = aVar.f1609a;
        int i = iVar.f1626b;
        int i2 = iVar2.f1626b;
        while (j < this.f1610b) {
            long min = (long) Math.min(iVar.c - i, iVar2.c - i2);
            int i3 = 0;
            while (((long) i3) < min) {
                int i4 = i + 1;
                byte b2 = iVar.f1625a[i];
                int i5 = i2 + 1;
                if (b2 != iVar2.f1625a[i2]) {
                    return false;
                }
                i3++;
                i2 = i5;
                i = i4;
            }
            if (i == iVar.c) {
                iVar = iVar.f;
                i = iVar.f1626b;
            }
            if (i2 == iVar2.c) {
                iVar2 = iVar2.f;
                i2 = iVar2.f1626b;
            }
            j += min;
        }
        return true;
    }

    public byte f() {
        if (this.f1610b == 0) {
            throw new IllegalStateException("size == 0");
        }
        i iVar = this.f1609a;
        int i = iVar.f1626b;
        int i2 = iVar.c;
        int i3 = i + 1;
        byte b2 = iVar.f1625a[i];
        this.f1610b--;
        if (i3 == i2) {
            this.f1609a = iVar.a();
            j.a(iVar);
        } else {
            iVar.f1626b = i3;
        }
        return b2;
    }

    public void flush() {
    }

    public d g() {
        return new d(i());
    }

    public String h() {
        try {
            return a(this.f1610b, n.f1631a);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public int hashCode() {
        i iVar = this.f1609a;
        if (iVar == null) {
            return 0;
        }
        int i = 1;
        do {
            int i2 = iVar.f1626b;
            int i3 = iVar.c;
            while (i2 < i3) {
                i2++;
                i = iVar.f1625a[i2] + (i * 31);
            }
            iVar = iVar.f;
        } while (iVar != this.f1609a);
        return i;
    }

    public byte[] i() {
        try {
            return a(this.f1610b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    public void j() {
        try {
            b(this.f1610b);
        } catch (EOFException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: k */
    public a clone() {
        a aVar = new a();
        if (this.f1610b == 0) {
            return aVar;
        }
        aVar.f1609a = new i(this.f1609a);
        i iVar = aVar.f1609a;
        i iVar2 = aVar.f1609a;
        i iVar3 = aVar.f1609a;
        iVar2.g = iVar3;
        iVar.f = iVar3;
        for (i iVar4 = this.f1609a.f; iVar4 != this.f1609a; iVar4 = iVar4.f) {
            aVar.f1609a.g.a(new i(iVar4));
        }
        aVar.f1610b = this.f1610b;
        return aVar;
    }

    public String toString() {
        if (this.f1610b == 0) {
            return "Buffer[size=0]";
        }
        if (this.f1610b <= 16) {
            return String.format("Buffer[size=%s data=%s]", Long.valueOf(this.f1610b), clone().g().c());
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(this.f1609a.f1625a, this.f1609a.f1626b, this.f1609a.c - this.f1609a.f1626b);
            for (i iVar = this.f1609a.f; iVar != this.f1609a; iVar = iVar.f) {
                instance.update(iVar.f1625a, iVar.f1626b, iVar.c - iVar.f1626b);
            }
            return String.format("Buffer[size=%s md5=%s]", Long.valueOf(this.f1610b), d.a(instance.digest()).c());
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError();
        }
    }
}
