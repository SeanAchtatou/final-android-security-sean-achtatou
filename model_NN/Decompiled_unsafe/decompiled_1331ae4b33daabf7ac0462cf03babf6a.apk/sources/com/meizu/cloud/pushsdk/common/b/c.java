package com.meizu.cloud.pushsdk.common.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.meizu.cloud.pushsdk.common.b.g;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f1636a = new e(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static a f1637b;
    /* access modifiers changed from: private */
    public static LinkedList<b> c = new LinkedList<>();
    private static a.C0023a d = a.C0023a.DEBUG;
    private static a.C0023a e = a.C0023a.DEBUG;
    private static C0024c f = new C0024c();

    public interface a {

        /* renamed from: com.meizu.cloud.pushsdk.common.b.c$a$a  reason: collision with other inner class name */
        public enum C0023a {
            DEBUG,
            INFO,
            WARN,
            ERROR,
            NULL
        }

        void a(C0023a aVar, String str, String str2);
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        static SimpleDateFormat f1640a = new SimpleDateFormat("MM-dd HH:mm:ss ");

        /* renamed from: b  reason: collision with root package name */
        static String f1641b = String.valueOf(Process.myPid());
        a.C0023a c;
        String d;
        String e;

        b(a.C0023a aVar, String str, String str2) {
            this.c = aVar;
            this.d = f1640a.format(new Date()) + f1641b + "-" + String.valueOf(Thread.currentThread().getId()) + " " + str;
            this.e = str2;
        }
    }

    /* renamed from: com.meizu.cloud.pushsdk.common.b.c$c  reason: collision with other inner class name */
    public class C0024c {

        /* renamed from: a  reason: collision with root package name */
        int f1642a = 100;

        /* renamed from: b  reason: collision with root package name */
        int f1643b = 120000;
    }

    public enum d {
        CONSOLE,
        FILE
    }

    class e extends Handler {
        e(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message message) {
            if (c.f1637b != null) {
                g.a(new g.a() {
                    public void a() {
                        LinkedList linkedList;
                        synchronized (c.c) {
                            linkedList = new LinkedList(c.c);
                            c.c.clear();
                        }
                        Iterator it = linkedList.iterator();
                        while (it.hasNext()) {
                            b bVar = (b) it.next();
                            c.f1637b.a(bVar.c, bVar.d, bVar.e);
                        }
                    }
                });
            }
        }
    }

    public static void a() {
        synchronized (c) {
            f1636a.removeMessages(1);
            f1636a.obtainMessage(1).sendToTarget();
        }
    }

    private static void a(a.C0023a aVar, String str, String str2) {
        if (f1637b != null && e.ordinal() <= aVar.ordinal()) {
            synchronized (c) {
                c.addLast(new b(aVar, str, str2));
                if (c.size() >= f.f1642a || f.f1643b <= 0) {
                    a();
                } else if (!f1636a.hasMessages(1)) {
                    f1636a.sendMessageDelayed(f1636a.obtainMessage(1), (long) f.f1643b);
                }
            }
        }
    }

    public static void a(a aVar) {
        f1637b = aVar;
    }

    public static void a(d dVar, a.C0023a aVar) {
        if (dVar == d.CONSOLE) {
            d = aVar;
        } else if (dVar == d.FILE) {
            e = aVar;
        }
    }

    public static void a(String str, String str2) {
        if (d.ordinal() <= a.C0023a.DEBUG.ordinal()) {
            Log.d(str, str2);
        }
        a(a.C0023a.DEBUG, str, str2);
    }

    public static void a(String str, Throwable th) {
        d(str, Log.getStackTraceString(th));
    }

    public static void b(String str, String str2) {
        if (d.ordinal() <= a.C0023a.INFO.ordinal()) {
            Log.i(str, str2);
        }
        a(a.C0023a.INFO, str, str2);
    }

    public static void c(String str, String str2) {
        if (d.ordinal() <= a.C0023a.WARN.ordinal()) {
            Log.w(str, str2);
        }
        a(a.C0023a.WARN, str, str2);
    }

    public static void d(String str, String str2) {
        if (d.ordinal() <= a.C0023a.ERROR.ordinal()) {
            Log.e(str, str2);
        }
        a(a.C0023a.ERROR, str, str2);
    }
}
