package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.d.g;
import com.meizu.cloud.pushsdk.a.d.j;
import com.meizu.cloud.pushsdk.a.e.o;
import com.meizu.cloud.pushsdk.a.h.a;
import com.meizu.cloud.pushsdk.a.h.b;
import com.meizu.cloud.pushsdk.a.h.e;
import com.meizu.cloud.pushsdk.a.h.f;
import com.meizu.cloud.pushsdk.a.h.k;

public class d extends j {

    /* renamed from: a  reason: collision with root package name */
    private final j f1602a;

    /* renamed from: b  reason: collision with root package name */
    private b f1603b;
    /* access modifiers changed from: private */
    public f c;

    public d(j jVar, o oVar) {
        this.f1602a = jVar;
        if (oVar != null) {
            this.c = new f(oVar);
        }
    }

    private k a(k kVar) {
        return new e(kVar) {

            /* renamed from: a  reason: collision with root package name */
            long f1604a = 0;

            /* renamed from: b  reason: collision with root package name */
            long f1605b = 0;

            public void a(a aVar, long j) {
                super.a(aVar, j);
                if (this.f1605b == 0) {
                    this.f1605b = d.this.b();
                }
                this.f1604a += j;
                if (d.this.c != null) {
                    d.this.c.obtainMessage(1, new com.meizu.cloud.pushsdk.a.g.a(this.f1604a, this.f1605b)).sendToTarget();
                }
            }
        };
    }

    public g a() {
        return this.f1602a.a();
    }

    public void a(b bVar) {
        if (this.f1603b == null) {
            this.f1603b = f.a(a((k) bVar));
        }
        this.f1602a.a(this.f1603b);
        this.f1603b.flush();
    }

    public long b() {
        return this.f1602a.b();
    }
}
