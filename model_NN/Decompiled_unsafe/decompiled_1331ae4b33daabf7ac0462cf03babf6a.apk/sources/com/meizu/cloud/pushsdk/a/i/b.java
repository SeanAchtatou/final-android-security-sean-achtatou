package com.meizu.cloud.pushsdk.a.i;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.NetworkOnMainThreadException;
import android.widget.ImageView;
import com.meizu.cloud.pushsdk.a.a.c;
import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.k;
import com.meizu.cloud.pushsdk.a.h.f;
import java.io.IOException;
import java.net.URLConnection;

public class b {
    public static int a(int i, int i2, int i3, int i4) {
        float f = 1.0f;
        while (((double) (f * 2.0f)) <= Math.min(((double) i) / ((double) i3), ((double) i2) / ((double) i4))) {
            f *= 2.0f;
        }
        return (int) f;
    }

    private static int a(int i, int i2, int i3, int i4, ImageView.ScaleType scaleType) {
        if (i == 0 && i2 == 0) {
            return i3;
        }
        if (scaleType == ImageView.ScaleType.FIT_XY) {
            return i == 0 ? i3 : i;
        }
        if (i == 0) {
            return (int) ((((double) i2) / ((double) i4)) * ((double) i3));
        }
        if (i2 == 0) {
            return i;
        }
        double d = ((double) i4) / ((double) i3);
        return scaleType == ImageView.ScaleType.CENTER_CROP ? ((double) i) * d < ((double) i2) ? (int) (((double) i2) / d) : i : ((double) i) * d > ((double) i2) ? (int) (((double) i2) / d) : i;
    }

    public static c<Bitmap> a(k kVar, int i, int i2, Bitmap.Config config, ImageView.ScaleType scaleType) {
        Bitmap bitmap;
        byte[] bArr = new byte[0];
        try {
            bArr = f.a(kVar.b().a()).i();
        } catch (IOException e) {
            e.printStackTrace();
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        if (i == 0 && i2 == 0) {
            options.inPreferredConfig = config;
            bitmap = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        } else {
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i3 = options.outWidth;
            int i4 = options.outHeight;
            int a2 = a(i, i2, i3, i4, scaleType);
            int a3 = a(i2, i, i4, i3, scaleType);
            options.inJustDecodeBounds = false;
            options.inSampleSize = a(i3, i4, a2, a3);
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            if (decodeByteArray == null || (decodeByteArray.getWidth() <= a2 && decodeByteArray.getHeight() <= a3)) {
                bitmap = decodeByteArray;
            } else {
                bitmap = Bitmap.createScaledBitmap(decodeByteArray, a2, a3, true);
                decodeByteArray.recycle();
            }
        }
        return bitmap == null ? c.a(b(new a(kVar))) : c.a(bitmap);
    }

    public static a a(a aVar) {
        aVar.a("connectionError");
        aVar.a(0);
        return aVar;
    }

    public static a a(a aVar, com.meizu.cloud.pushsdk.a.a.b bVar, int i) {
        a a2 = bVar.a(aVar);
        a2.a(i);
        a2.a("responseFromServerError");
        return a2;
    }

    public static a a(Exception exc) {
        a aVar = new a(exc);
        if (Build.VERSION.SDK_INT < 11 || !(exc instanceof NetworkOnMainThreadException)) {
            aVar.a("connectionError");
        } else {
            aVar.a("networkOnMainThreadError");
        }
        aVar.a(0);
        return aVar;
    }

    public static String a(String str) {
        String contentTypeFor = URLConnection.getFileNameMap().getContentTypeFor(str);
        return contentTypeFor == null ? "application/octet-stream" : contentTypeFor;
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036 A[SYNTHETIC, Splitter:B:15:0x0036] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003b A[SYNTHETIC, Splitter:B:18:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.meizu.cloud.pushsdk.a.d.k r5, java.lang.String r6, java.lang.String r7) {
        /*
            r1 = 0
            r0 = 2048(0x800, float:2.87E-42)
            byte[] r0 = new byte[r0]
            com.meizu.cloud.pushsdk.a.d.l r2 = r5.b()     // Catch:{ all -> 0x0061 }
            java.io.InputStream r3 = r2.b()     // Catch:{ all -> 0x0061 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x0064 }
            r2.<init>(r6)     // Catch:{ all -> 0x0064 }
            boolean r4 = r2.exists()     // Catch:{ all -> 0x0064 }
            if (r4 != 0) goto L_0x001b
            r2.mkdirs()     // Catch:{ all -> 0x0064 }
        L_0x001b:
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x0064 }
            r4.<init>(r2, r7)     // Catch:{ all -> 0x0064 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x0064 }
            r2.<init>(r4)     // Catch:{ all -> 0x0064 }
        L_0x0025:
            int r1 = r3.read(r0)     // Catch:{ all -> 0x0031 }
            r4 = -1
            if (r1 == r4) goto L_0x003f
            r4 = 0
            r2.write(r0, r4, r1)     // Catch:{ all -> 0x0031 }
            goto L_0x0025
        L_0x0031:
            r0 = move-exception
            r1 = r2
            r2 = r3
        L_0x0034:
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0039:
            if (r1 == 0) goto L_0x003e
            r1.close()     // Catch:{ IOException -> 0x005c }
        L_0x003e:
            throw r0
        L_0x003f:
            r2.flush()     // Catch:{ all -> 0x0031 }
            if (r3 == 0) goto L_0x0047
            r3.close()     // Catch:{ IOException -> 0x004d }
        L_0x0047:
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ IOException -> 0x0052 }
        L_0x004c:
            return
        L_0x004d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0047
        L_0x0052:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004c
        L_0x0057:
            r2 = move-exception
            r2.printStackTrace()
            goto L_0x0039
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x003e
        L_0x0061:
            r0 = move-exception
            r2 = r1
            goto L_0x0034
        L_0x0064:
            r0 = move-exception
            r2 = r3
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.i.b.a(com.meizu.cloud.pushsdk.a.d.k, java.lang.String, java.lang.String):void");
    }

    public static a b(a aVar) {
        aVar.a(0);
        aVar.a("parseError");
        return aVar;
    }
}
