package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.a.b;
import com.meizu.cloud.pushsdk.a.a.c;
import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.k;

public final class e {
    public static <T> c<T> a(b bVar) {
        switch (bVar.h()) {
            case 0:
                return b(bVar);
            case 1:
                return c(bVar);
            case 2:
                return d(bVar);
            default:
                return new c<>(new a());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x006e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006f, code lost:
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x007e, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0071=Splitter:B:28:0x0071, B:23:0x005c=Splitter:B:23:0x005c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> com.meizu.cloud.pushsdk.a.a.c<T> b(com.meizu.cloud.pushsdk.a.a.b r4) {
        /*
            r1 = 0
            com.meizu.cloud.pushsdk.a.d.k r1 = com.meizu.cloud.pushsdk.a.f.b.a(r4)     // Catch:{ a -> 0x0059, Exception -> 0x006e, all -> 0x007e }
            if (r1 != 0) goto L_0x0019
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r2 = new com.meizu.cloud.pushsdk.a.c.a     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r2.<init>()     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r2 = com.meizu.cloud.pushsdk.a.i.b.a(r2)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.<init>(r2)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
        L_0x0018:
            return r0
        L_0x0019:
            com.meizu.cloud.pushsdk.a.a.e r0 = r4.g()     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.a.e r2 = com.meizu.cloud.pushsdk.a.a.e.OK_HTTP_RESPONSE     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            if (r0 != r2) goto L_0x002d
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.<init>(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.a(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x002d:
            int r0 = r1.a()     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r2 = 400(0x190, float:5.6E-43)
            if (r0 < r2) goto L_0x004e
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r2 = new com.meizu.cloud.pushsdk.a.c.a     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r2.<init>(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            int r3 = r1.a()     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r2 = com.meizu.cloud.pushsdk.a.i.b.a(r2, r4, r3)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.<init>(r2)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.a(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x004e:
            com.meizu.cloud.pushsdk.a.a.c r0 = r4.a(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            r0.a(r1)     // Catch:{ a -> 0x008a, Exception -> 0x0086, all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x0059:
            r0 = move-exception
            r2 = r1
            r1 = r0
        L_0x005c:
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ all -> 0x0083 }
            com.meizu.cloud.pushsdk.a.c.a r3 = new com.meizu.cloud.pushsdk.a.c.a     // Catch:{ all -> 0x0083 }
            r3.<init>(r1)     // Catch:{ all -> 0x0083 }
            com.meizu.cloud.pushsdk.a.c.a r1 = com.meizu.cloud.pushsdk.a.i.b.a(r3)     // Catch:{ all -> 0x0083 }
            r0.<init>(r1)     // Catch:{ all -> 0x0083 }
            com.meizu.cloud.pushsdk.a.i.a.a(r2, r4)
            goto L_0x0018
        L_0x006e:
            r0 = move-exception
            r2 = r1
            r1 = r0
        L_0x0071:
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ all -> 0x0083 }
            com.meizu.cloud.pushsdk.a.c.a r1 = com.meizu.cloud.pushsdk.a.i.b.a(r1)     // Catch:{ all -> 0x0083 }
            r0.<init>(r1)     // Catch:{ all -> 0x0083 }
            com.meizu.cloud.pushsdk.a.i.a.a(r2, r4)
            goto L_0x0018
        L_0x007e:
            r0 = move-exception
        L_0x007f:
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            throw r0
        L_0x0083:
            r0 = move-exception
            r1 = r2
            goto L_0x007f
        L_0x0086:
            r0 = move-exception
            r2 = r1
            r1 = r0
            goto L_0x0071
        L_0x008a:
            r0 = move-exception
            r2 = r1
            r1 = r0
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.f.e.b(com.meizu.cloud.pushsdk.a.a.b):com.meizu.cloud.pushsdk.a.a.c");
    }

    private static <T> c<T> c(b bVar) {
        try {
            k b2 = b.b(bVar);
            if (b2 == null) {
                return new c<>(com.meizu.cloud.pushsdk.a.i.b.a(new a()));
            }
            if (b2.a() >= 400) {
                c<T> cVar = new c<>(com.meizu.cloud.pushsdk.a.i.b.a(new a(b2), bVar, b2.a()));
                cVar.a(b2);
                return cVar;
            }
            c<T> cVar2 = new c<>("success");
            cVar2.a(b2);
            return cVar2;
        } catch (a e) {
            return new c<>(com.meizu.cloud.pushsdk.a.i.b.a(new a(e)));
        } catch (Exception e2) {
            return new c<>(com.meizu.cloud.pushsdk.a.i.b.a(e2));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0059, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005a, code lost:
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0069, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006a, code lost:
        r2 = null;
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0079, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0079 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0001] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x005c=Splitter:B:23:0x005c, B:28:0x006c=Splitter:B:28:0x006c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static <T> com.meizu.cloud.pushsdk.a.a.c<T> d(com.meizu.cloud.pushsdk.a.a.b r4) {
        /*
            r1 = 0
            com.meizu.cloud.pushsdk.a.d.k r1 = com.meizu.cloud.pushsdk.a.f.b.c(r4)     // Catch:{ a -> 0x0059, Exception -> 0x0069, all -> 0x0079 }
            if (r1 != 0) goto L_0x0019
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.c.a r2 = new com.meizu.cloud.pushsdk.a.c.a     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r2.<init>()     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.c.a r2 = com.meizu.cloud.pushsdk.a.i.b.a(r2)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.<init>(r2)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
        L_0x0018:
            return r0
        L_0x0019:
            com.meizu.cloud.pushsdk.a.a.e r0 = r4.g()     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.a.e r2 = com.meizu.cloud.pushsdk.a.a.e.OK_HTTP_RESPONSE     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            if (r0 != r2) goto L_0x002d
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.<init>(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.a(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x002d:
            int r0 = r1.a()     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r2 = 400(0x190, float:5.6E-43)
            if (r0 < r2) goto L_0x004e
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.c.a r2 = new com.meizu.cloud.pushsdk.a.c.a     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r2.<init>(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            int r3 = r1.a()     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.c.a r2 = com.meizu.cloud.pushsdk.a.i.b.a(r2, r4, r3)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.<init>(r2)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.a(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x004e:
            com.meizu.cloud.pushsdk.a.a.c r0 = r4.a(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            r0.a(r1)     // Catch:{ a -> 0x0085, Exception -> 0x0081, all -> 0x0079 }
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            goto L_0x0018
        L_0x0059:
            r0 = move-exception
            r2 = r1
            r1 = r0
        L_0x005c:
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r1 = com.meizu.cloud.pushsdk.a.i.b.a(r1)     // Catch:{ all -> 0x007e }
            r0.<init>(r1)     // Catch:{ all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r2, r4)
            goto L_0x0018
        L_0x0069:
            r0 = move-exception
            r2 = r1
            r1 = r0
        L_0x006c:
            com.meizu.cloud.pushsdk.a.a.c r0 = new com.meizu.cloud.pushsdk.a.a.c     // Catch:{ all -> 0x007e }
            com.meizu.cloud.pushsdk.a.c.a r1 = com.meizu.cloud.pushsdk.a.i.b.a(r1)     // Catch:{ all -> 0x007e }
            r0.<init>(r1)     // Catch:{ all -> 0x007e }
            com.meizu.cloud.pushsdk.a.i.a.a(r2, r4)
            goto L_0x0018
        L_0x0079:
            r0 = move-exception
        L_0x007a:
            com.meizu.cloud.pushsdk.a.i.a.a(r1, r4)
            throw r0
        L_0x007e:
            r0 = move-exception
            r1 = r2
            goto L_0x007a
        L_0x0081:
            r0 = move-exception
            r2 = r1
            r1 = r0
            goto L_0x006c
        L_0x0085:
            r0 = move-exception
            r2 = r1
            r1 = r0
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.f.e.d(com.meizu.cloud.pushsdk.a.a.b):com.meizu.cloud.pushsdk.a.a.c");
    }
}
