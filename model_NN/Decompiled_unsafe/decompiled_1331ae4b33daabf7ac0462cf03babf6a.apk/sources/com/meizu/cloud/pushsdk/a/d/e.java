package com.meizu.cloud.pushsdk.a.d;

import com.meizu.cloud.pushsdk.a.a.a;
import com.meizu.cloud.pushsdk.a.d.k;
import com.meizu.cloud.pushsdk.a.h.b;
import com.meizu.cloud.pushsdk.a.h.c;
import com.meizu.cloud.pushsdk.a.h.f;
import com.meizu.cloud.pushsdk.platform.SSLCertificateValidation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.net.ssl.HttpsURLConnection;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class e implements a {

    /* renamed from: a  reason: collision with root package name */
    i f1563a;

    public e(i iVar) {
        this.f1563a = iVar;
    }

    private static l a(final URLConnection uRLConnection) {
        if (!uRLConnection.getDoInput()) {
            return null;
        }
        final c a2 = f.a(f.a(uRLConnection.getInputStream()));
        return new l() {
            public c a() {
                return a2;
            }
        };
    }

    private HttpURLConnection a(i iVar) {
        String fVar = iVar.a().toString();
        HttpURLConnection a2 = a(new URL(fVar));
        a2.setConnectTimeout(60000);
        a2.setReadTimeout(60000);
        a2.setUseCaches(false);
        a2.setDoInput(true);
        if (iVar.f() && fVar.startsWith("https://push.statics")) {
            ((HttpsURLConnection) a2).setSSLSocketFactory(SSLCertificateValidation.getSSLSocketFactory());
            ((HttpsURLConnection) a2).setHostnameVerifier(SSLCertificateValidation.getHostnameVerifier());
        }
        return a2;
    }

    static void a(HttpURLConnection httpURLConnection, i iVar) {
        switch (iVar.c()) {
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                b(httpURLConnection, iVar);
                return;
            case 2:
                httpURLConnection.setRequestMethod(HttpProxyConstants.PUT);
                b(httpURLConnection, iVar);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("PATCH");
                b(httpURLConnection, iVar);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }

    private static void b(HttpURLConnection httpURLConnection, i iVar) {
        j e = iVar.e();
        if (e != null) {
            httpURLConnection.setDoOutput(true);
            httpURLConnection.addRequestProperty("Content-Type", e.a().toString());
            b a2 = f.a(f.a(httpURLConnection.getOutputStream()));
            e.a(a2);
            a2.close();
        }
    }

    public k a() {
        HttpURLConnection a2 = a(this.f1563a);
        for (String next : this.f1563a.d().b()) {
            String a3 = this.f1563a.a(next);
            a.b("current header name " + next + " value " + a3);
            a2.addRequestProperty(next, a3);
        }
        a(a2, this.f1563a);
        int responseCode = a2.getResponseCode();
        return new k.a().a(responseCode).a(this.f1563a.d()).a(a2.getResponseMessage()).a(this.f1563a).a(a(a2)).a();
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(URL url) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }
}
