package com.meizu.cloud.pushsdk.common.base;

import com.meizu.cloud.pushsdk.common.b.c;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class a<T> {

    /* renamed from: a  reason: collision with root package name */
    protected T f1656a;

    /* renamed from: b  reason: collision with root package name */
    protected String f1657b;
    private AtomicInteger c = new AtomicInteger(0);

    public a(String str) {
        this.f1657b = str;
    }

    /* access modifiers changed from: protected */
    public abstract void a();

    public final void a(Object obj) {
        synchronized (this.c) {
            if (this.c.incrementAndGet() == 1) {
                c.b(this.f1657b, "call onInit");
                this.f1656a = obj;
                a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    public final void c() {
        synchronized (this.c) {
            if (this.c.decrementAndGet() == 0) {
                c.b(this.f1657b, "call onDestroy");
                b();
            }
        }
    }
}
