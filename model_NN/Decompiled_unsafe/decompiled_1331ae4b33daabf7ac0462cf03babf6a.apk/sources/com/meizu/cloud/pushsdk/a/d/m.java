package com.meizu.cloud.pushsdk.a.d;

import java.io.Closeable;
import java.net.IDN;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;

public class m {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f1593a = new byte[0];

    /* renamed from: b  reason: collision with root package name */
    public static final String[] f1594b = new String[0];
    public static final Charset c = Charset.forName("UTF-8");
    public static final TimeZone d = TimeZone.getTimeZone("GMT");
    private static final Pattern e = Pattern.compile("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(java.lang.String r2, int r3, int r4) {
        /*
            r0 = r3
        L_0x0001:
            if (r0 >= r4) goto L_0x000b
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000c;
                case 10: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 32: goto L_0x000c;
                default: goto L_0x000a;
            }
        L_0x000a:
            r4 = r0
        L_0x000b:
            return r4
        L_0x000c:
            int r0 = r0 + 1
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int):int");
    }

    public static int a(String str, int i, int i2, char c2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str.charAt(i3) == c2) {
                return i3;
            }
        }
        return i2;
    }

    public static int a(String str, int i, int i2, String str2) {
        for (int i3 = i; i3 < i2; i3++) {
            if (str2.indexOf(str.charAt(i3)) != -1) {
                return i3;
            }
        }
        return i2;
    }

    public static String a(String str) {
        try {
            String lowerCase = IDN.toASCII(str).toLowerCase(Locale.US);
            if (!lowerCase.isEmpty() && !b(lowerCase)) {
                return lowerCase;
            }
            return null;
        } catch (IllegalArgumentException e2) {
            return null;
        }
    }

    public static <T> List<T> a(List list) {
        return Collections.unmodifiableList(new ArrayList(list));
    }

    public static void a(long j, long j2, long j3) {
        if ((j2 | j3) < 0 || j2 > j || j - j2 < j3) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception e3) {
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:2:0x0004  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int b(java.lang.String r2, int r3, int r4) {
        /*
            int r0 = r4 + -1
        L_0x0002:
            if (r0 < r3) goto L_0x000d
            char r1 = r2.charAt(r0)
            switch(r1) {
                case 9: goto L_0x000e;
                case 10: goto L_0x000e;
                case 12: goto L_0x000e;
                case 13: goto L_0x000e;
                case 32: goto L_0x000e;
                default: goto L_0x000b;
            }
        L_0x000b:
            int r3 = r0 + 1
        L_0x000d:
            return r3
        L_0x000e:
            int r0 = r0 + -1
            goto L_0x0002
        */
        throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.d.m.b(java.lang.String, int, int):int");
    }

    private static boolean b(String str) {
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return true;
            }
            if (" #%/:?@[\\]".indexOf(charAt) != -1) {
                return true;
            }
        }
        return false;
    }
}
