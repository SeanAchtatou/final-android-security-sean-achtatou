package com.meizu.cloud.pushsdk.a.h;

import android.support.v4.media.session.PlaybackStateCompat;

final class j {

    /* renamed from: a  reason: collision with root package name */
    static i f1627a;

    /* renamed from: b  reason: collision with root package name */
    static long f1628b;

    private j() {
    }

    static i a() {
        synchronized (j.class) {
            if (f1627a == null) {
                return new i();
            }
            i iVar = f1627a;
            f1627a = iVar.f;
            iVar.f = null;
            f1628b -= PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
            return iVar;
        }
    }

    static void a(i iVar) {
        if (iVar.f != null || iVar.g != null) {
            throw new IllegalArgumentException();
        } else if (!iVar.d) {
            synchronized (j.class) {
                if (f1628b + PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH <= PlaybackStateCompat.ACTION_PREPARE_FROM_SEARCH) {
                    f1628b += PlaybackStateCompat.ACTION_PLAY_FROM_SEARCH;
                    iVar.f = f1627a;
                    iVar.c = 0;
                    iVar.f1626b = 0;
                    f1627a = iVar;
                }
            }
        }
    }
}
