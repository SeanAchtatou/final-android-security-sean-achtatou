package com.meizu.cloud.pushsdk.common.b;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static ExecutorService f1653a;

    public abstract class a implements Runnable {
        public abstract void a();

        public final void run() {
            a();
        }
    }

    private static synchronized Executor a() {
        ExecutorService executorService;
        synchronized (g.class) {
            if (f1653a == null) {
                f1653a = new ThreadPoolExecutor(0, 5, 180, TimeUnit.SECONDS, new ArrayBlockingQueue(100, true));
            }
            executorService = f1653a;
        }
        return executorService;
    }

    public static void a(a aVar) {
        a();
        try {
            f1653a.execute(aVar);
        } catch (RejectedExecutionException e) {
            new Thread(aVar).start();
        }
    }
}
