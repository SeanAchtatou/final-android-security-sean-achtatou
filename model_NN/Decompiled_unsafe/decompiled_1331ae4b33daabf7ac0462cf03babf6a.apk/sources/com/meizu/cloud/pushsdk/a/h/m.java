package com.meizu.cloud.pushsdk.a.h;

import java.io.InterruptedIOException;

public class m {

    /* renamed from: a  reason: collision with root package name */
    public static final m f1629a = new m() {
        public void a() {
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private boolean f1630b;
    private long c;

    public void a() {
        if (Thread.interrupted()) {
            throw new InterruptedIOException("thread interrupted");
        } else if (this.f1630b && this.c - System.nanoTime() <= 0) {
            throw new InterruptedIOException("deadline reached");
        }
    }
}
