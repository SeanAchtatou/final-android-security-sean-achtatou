package com.meizu.cloud.pushsdk.common.base;

import android.text.TextUtils;
import android.util.Log;
import com.meizu.cloud.pushsdk.common.b.c;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class c implements c.a {

    /* renamed from: a  reason: collision with root package name */
    private String f1660a;

    /* renamed from: b  reason: collision with root package name */
    private BufferedWriter f1661b;
    private b c = new b("lo");

    public c(String str) {
        this.f1660a = str;
    }

    private synchronized void a() {
        boolean z = false;
        synchronized (this) {
            if (!TextUtils.isEmpty(this.f1660a)) {
                File file = new File(this.f1660a);
                if (file.exists() || file.mkdirs()) {
                    File file2 = new File(file, "logs_v2.txt");
                    if (file2.exists() || file2.createNewFile()) {
                        if (file2 != null) {
                            if (file2.length() >= 31457280) {
                                String parent = file2.getParent();
                                File file3 = new File(parent, "logs_v2_old.txt");
                                if (!file3.exists()) {
                                    this.f1661b = new BufferedWriter(new FileWriter(file2, z));
                                } else {
                                    this.f1661b = new BufferedWriter(new FileWriter(file2, z));
                                }
                            }
                        }
                        z = true;
                        this.f1661b = new BufferedWriter(new FileWriter(file2, z));
                    } else {
                        Log.e("EncryptLogger", "create new file " + "logs_v2.txt" + " failed!");
                    }
                } else {
                    Log.e("EncryptLogger", "create dir " + this.f1660a + " failed!");
                }
            }
        }
    }

    private synchronized void b() {
        if (this.f1661b != null) {
            try {
                this.f1661b.close();
            } catch (IOException e) {
            }
        }
    }

    public void a(c.a.C0023a aVar, String str, String str2) {
        try {
            a();
            if (this.f1661b != null) {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("/");
                sb.append(aVar == c.a.C0023a.DEBUG ? "D" : aVar == c.a.C0023a.INFO ? "I" : aVar == c.a.C0023a.WARN ? "W" : "E");
                sb.append(": ");
                sb.append(str2);
                this.f1661b.append((CharSequence) this.c.a(sb.toString().getBytes(Charset.forName("UTF-8"))));
                this.f1661b.append((CharSequence) HttpProxyConstants.CRLF);
                this.f1661b.flush();
            }
        } catch (Exception e) {
        } finally {
            b();
        }
    }
}
