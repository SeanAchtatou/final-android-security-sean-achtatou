package com.meizu.cloud.pushsdk.common.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.TextUtils;
import com.meizu.cloud.pushsdk.common.b.e;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static String f1634a = "";

    /* renamed from: b  reason: collision with root package name */
    private static String f1635b = "";

    public static String a(Context context) {
        if (TextUtils.isEmpty(f1635b)) {
            if (a()) {
                f1635b = h.a(context);
            } else if (TextUtils.isEmpty(f1635b)) {
                StringBuilder sb = new StringBuilder();
                String str = Build.SERIAL;
                c.b("DeviceUtils", "device serial " + str);
                if (TextUtils.isEmpty(str)) {
                    return null;
                }
                sb.append(str);
                String b2 = b(context);
                c.d("DeviceUtils", "mac address " + b2);
                if (TextUtils.isEmpty(b2)) {
                    return null;
                }
                sb.append(b2.replace(":", "").toUpperCase());
                f1635b = sb.toString();
            }
        }
        return f1635b;
    }

    private static String a(String str) {
        String str2 = null;
        try {
            FileInputStream fileInputStream = new FileInputStream("/sys/class/net/" + str + "/address");
            Scanner scanner = new Scanner(fileInputStream);
            if (scanner.hasNextLine()) {
                str2 = scanner.nextLine().trim();
            }
            fileInputStream.close();
        } catch (FileNotFoundException e) {
            c.d("DeviceUtils", "getMacAddressWithIfName File not found Exception");
        } catch (IOException e2) {
            c.d("DeviceUtils", "getMacAddressWithIfName IOException");
        }
        return str2;
    }

    public static boolean a() {
        e.c a2 = f.a("ro.target.product");
        if (!a2.f1650a || TextUtils.isEmpty((CharSequence) a2.f1651b)) {
            c.b("DeviceUtils", "current product is phone");
            return true;
        }
        c.b("DeviceUtils", "current product is " + ((String) a2.f1651b));
        return false;
    }

    public static String b(Context context) {
        WifiInfo connectionInfo;
        String str;
        String str2 = null;
        if (!TextUtils.isEmpty(f1634a)) {
            return f1634a;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo == null) {
                    str = a("wlan0");
                    if (TextUtils.isEmpty(str)) {
                        str = a("eth0");
                    }
                } else if (activeNetworkInfo.getType() == 1) {
                    str = a("wlan0");
                } else if (activeNetworkInfo.getType() == 9) {
                    str = a("eth0");
                }
                str2 = str;
            }
            str = null;
            str2 = str;
        } else {
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (!(wifiManager == null || (connectionInfo = wifiManager.getConnectionInfo()) == null)) {
                str2 = connectionInfo.getMacAddress();
            }
        }
        f1634a = str2;
        return f1634a;
    }
}
