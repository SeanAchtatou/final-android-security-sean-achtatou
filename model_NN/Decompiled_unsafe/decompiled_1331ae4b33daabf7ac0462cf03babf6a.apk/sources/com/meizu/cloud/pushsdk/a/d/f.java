package com.meizu.cloud.pushsdk.a.d;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f1566a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public final String f1567b;
    private final String c;
    private final String d;
    /* access modifiers changed from: private */
    public final String e;
    /* access modifiers changed from: private */
    public final int f;
    private final List<String> g;
    private final List<String> h;
    private final String i;
    private final String j;

    /* renamed from: com.meizu.cloud.pushsdk.a.d.f$1  reason: invalid class name */
    /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1568a = new int[a.C0022a.values().length];

        static {
            try {
                f1568a[a.C0022a.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1568a[a.C0022a.INVALID_HOST.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1568a[a.C0022a.UNSUPPORTED_SCHEME.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1568a[a.C0022a.MISSING_SCHEME.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1568a[a.C0022a.INVALID_PORT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public final class a {

        /* renamed from: a  reason: collision with root package name */
        String f1569a;

        /* renamed from: b  reason: collision with root package name */
        String f1570b = "";
        String c = "";
        String d;
        int e = -1;
        final List<String> f = new ArrayList();
        List<String> g;
        String h;

        /* renamed from: com.meizu.cloud.pushsdk.a.d.f$a$a  reason: collision with other inner class name */
        enum C0022a {
            SUCCESS,
            MISSING_SCHEME,
            UNSUPPORTED_SCHEME,
            INVALID_PORT,
            INVALID_HOST
        }

        public a() {
            this.f.add("");
        }

        private static String a(byte[] bArr) {
            int i = 0;
            int i2 = 0;
            int i3 = -1;
            int i4 = 0;
            while (i4 < bArr.length) {
                int i5 = i4;
                while (i5 < 16 && bArr[i5] == 0 && bArr[i5 + 1] == 0) {
                    i5 += 2;
                }
                int i6 = i5 - i4;
                if (i6 > i2) {
                    i2 = i6;
                    i3 = i4;
                }
                i4 = i5 + 2;
            }
            com.meizu.cloud.pushsdk.a.h.a aVar = new com.meizu.cloud.pushsdk.a.h.a();
            while (i < bArr.length) {
                if (i == i3) {
                    aVar.b(58);
                    i += i2;
                    if (i == 16) {
                        aVar.b(58);
                    }
                } else {
                    if (i > 0) {
                        aVar.b(58);
                    }
                    aVar.d((long) (((bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8) | (bArr[i + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)));
                    i += 2;
                }
            }
            return aVar.h();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.meizu.cloud.pushsdk.a.d.f.a.a(java.lang.String, int, int, boolean, boolean):void
         arg types: [java.lang.String, int, int, boolean, int]
         candidates:
          com.meizu.cloud.pushsdk.a.d.f.a.a(java.lang.String, int, int, byte[], int):boolean
          com.meizu.cloud.pushsdk.a.d.f.a.a(java.lang.String, int, int, boolean, boolean):void */
        private void a(String str, int i, int i2) {
            if (i != i2) {
                char charAt = str.charAt(i);
                if (charAt == '/' || charAt == '\\') {
                    this.f.clear();
                    this.f.add("");
                    i++;
                } else {
                    this.f.set(this.f.size() - 1, "");
                }
                int i3 = i;
                while (i3 < i2) {
                    int a2 = m.a(str, i3, i2, "/\\");
                    boolean z = a2 < i2;
                    a(str, i3, a2, z, true);
                    if (z) {
                        a2++;
                    }
                    i3 = a2;
                }
            }
        }

        private void a(String str, int i, int i2, boolean z, boolean z2) {
            String a2 = f.a(str, i, i2, " \"<>^`{}|/\\?#", z2, false, false, true);
            if (!b(a2)) {
                if (c(a2)) {
                    c();
                    return;
                }
                if (this.f.get(this.f.size() - 1).isEmpty()) {
                    this.f.set(this.f.size() - 1, a2);
                } else {
                    this.f.add(a2);
                }
                if (z) {
                    this.f.add("");
                }
            }
        }

        private static boolean a(String str, int i, int i2, byte[] bArr, int i3) {
            int i4 = i;
            int i5 = i3;
            while (i4 < i2) {
                if (i5 == bArr.length) {
                    return false;
                }
                if (i5 != i3) {
                    if (str.charAt(i4) != '.') {
                        return false;
                    }
                    i4++;
                }
                int i6 = 0;
                int i7 = i4;
                while (i7 < i2) {
                    char charAt = str.charAt(i7);
                    if (charAt < '0' || charAt > '9') {
                        break;
                    } else if (i6 == 0 && i4 != i7) {
                        return false;
                    } else {
                        i6 = ((i6 * 10) + charAt) - 48;
                        if (i6 > 255) {
                            return false;
                        }
                        i7++;
                    }
                }
                if (i7 - i4 == 0) {
                    return false;
                }
                bArr[i5] = (byte) i6;
                i5++;
                i4 = i7;
            }
            return i5 == i3 + 4;
        }

        private static int b(String str, int i, int i2) {
            if (i2 - i < 2) {
                return -1;
            }
            char charAt = str.charAt(i);
            if ((charAt < 'a' || charAt > 'z') && (charAt < 'A' || charAt > 'Z')) {
                return -1;
            }
            int i3 = i + 1;
            while (i3 < i2) {
                char charAt2 = str.charAt(i3);
                if ((charAt2 >= 'a' && charAt2 <= 'z') || ((charAt2 >= 'A' && charAt2 <= 'Z') || ((charAt2 >= '0' && charAt2 <= '9') || charAt2 == '+' || charAt2 == '-' || charAt2 == '.'))) {
                    i3++;
                } else if (charAt2 == ':') {
                    return i3;
                } else {
                    return -1;
                }
            }
            return -1;
        }

        private boolean b(String str) {
            return str.equals(".") || str.equalsIgnoreCase("%2e");
        }

        private static int c(String str, int i, int i2) {
            int i3 = 0;
            while (i < i2) {
                char charAt = str.charAt(i);
                if (charAt != '\\' && charAt != '/') {
                    break;
                }
                i3++;
                i++;
            }
            return i3;
        }

        private void c() {
            if (!this.f.remove(this.f.size() - 1).isEmpty() || this.f.isEmpty()) {
                this.f.add("");
            } else {
                this.f.set(this.f.size() - 1, "");
            }
        }

        private boolean c(String str) {
            return str.equals("..") || str.equalsIgnoreCase("%2e.") || str.equalsIgnoreCase(".%2e") || str.equalsIgnoreCase("%2e%2e");
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private static int d(String str, int i, int i2) {
            int i3 = i;
            while (i3 < i2) {
                switch (str.charAt(i3)) {
                    case ':':
                        return i3;
                    case '[':
                        break;
                    default:
                        i3++;
                }
                do {
                    i3++;
                    if (i3 < i2) {
                    }
                    i3++;
                } while (str.charAt(i3) != ']');
                i3++;
            }
            return i2;
        }

        private static String e(String str, int i, int i2) {
            String a2 = f.a(str, i, i2, false);
            if (!a2.contains(":")) {
                return m.a(a2);
            }
            InetAddress f2 = (!a2.startsWith("[") || !a2.endsWith("]")) ? f(a2, 0, a2.length()) : f(a2, 1, a2.length() - 1);
            if (f2 == null) {
                return null;
            }
            byte[] address = f2.getAddress();
            if (address.length == 16) {
                return a(address);
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
            return null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static java.net.InetAddress f(java.lang.String r12, int r13, int r14) {
            /*
                r11 = 1
                r7 = -1
                r3 = 0
                r5 = 0
                r0 = 16
                byte[] r8 = new byte[r0]
                r0 = r13
                r4 = r7
                r1 = r7
                r2 = r5
            L_0x000c:
                if (r0 >= r14) goto L_0x002b
                int r6 = r8.length
                if (r2 != r6) goto L_0x0013
                r0 = r3
            L_0x0012:
                return r0
            L_0x0013:
                int r6 = r0 + 2
                if (r6 > r14) goto L_0x0032
                java.lang.String r6 = "::"
                r9 = 2
                boolean r6 = r12.regionMatches(r0, r6, r5, r9)
                if (r6 == 0) goto L_0x0032
                if (r1 == r7) goto L_0x0024
                r0 = r3
                goto L_0x0012
            L_0x0024:
                int r0 = r0 + 2
                int r1 = r2 + 2
                if (r0 != r14) goto L_0x00a1
                r2 = r1
            L_0x002b:
                int r0 = r8.length
                if (r2 == r0) goto L_0x0094
                if (r1 != r7) goto L_0x0085
                r0 = r3
                goto L_0x0012
            L_0x0032:
                if (r2 == 0) goto L_0x003e
                java.lang.String r6 = ":"
                boolean r6 = r12.regionMatches(r0, r6, r5, r11)
                if (r6 == 0) goto L_0x0055
                int r0 = r0 + 1
            L_0x003e:
                r4 = r5
                r6 = r0
            L_0x0040:
                if (r6 >= r14) goto L_0x004c
                char r9 = r12.charAt(r6)
                int r9 = com.meizu.cloud.pushsdk.a.d.f.a(r9)
                if (r9 != r7) goto L_0x006c
            L_0x004c:
                int r9 = r6 - r0
                if (r9 == 0) goto L_0x0053
                r10 = 4
                if (r9 <= r10) goto L_0x0072
            L_0x0053:
                r0 = r3
                goto L_0x0012
            L_0x0055:
                java.lang.String r6 = "."
                boolean r0 = r12.regionMatches(r0, r6, r5, r11)
                if (r0 == 0) goto L_0x006a
                int r0 = r2 + -2
                boolean r0 = a(r12, r4, r14, r8, r0)
                if (r0 != 0) goto L_0x0067
                r0 = r3
                goto L_0x0012
            L_0x0067:
                int r2 = r2 + 2
                goto L_0x002b
            L_0x006a:
                r0 = r3
                goto L_0x0012
            L_0x006c:
                int r4 = r4 << 4
                int r4 = r4 + r9
                int r6 = r6 + 1
                goto L_0x0040
            L_0x0072:
                int r9 = r2 + 1
                int r10 = r4 >>> 8
                r10 = r10 & 255(0xff, float:3.57E-43)
                byte r10 = (byte) r10
                r8[r2] = r10
                int r2 = r9 + 1
                r4 = r4 & 255(0xff, float:3.57E-43)
                byte r4 = (byte) r4
                r8[r9] = r4
                r4 = r0
                r0 = r6
                goto L_0x000c
            L_0x0085:
                int r0 = r8.length
                int r3 = r2 - r1
                int r0 = r0 - r3
                int r3 = r2 - r1
                java.lang.System.arraycopy(r8, r1, r8, r0, r3)
                int r0 = r8.length
                int r0 = r0 - r2
                int r0 = r0 + r1
                java.util.Arrays.fill(r8, r1, r0, r5)
            L_0x0094:
                java.net.InetAddress r0 = java.net.InetAddress.getByAddress(r8)     // Catch:{ UnknownHostException -> 0x009a }
                goto L_0x0012
            L_0x009a:
                r0 = move-exception
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x00a1:
                r2 = r1
                goto L_0x003e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.meizu.cloud.pushsdk.a.d.f.a.f(java.lang.String, int, int):java.net.InetAddress");
        }

        private static int g(String str, int i, int i2) {
            try {
                int parseInt = Integer.parseInt(f.a(str, i, i2, "", false, false, false, true));
                if (parseInt <= 0 || parseInt > 65535) {
                    return -1;
                }
                return parseInt;
            } catch (NumberFormatException e2) {
                return -1;
            }
        }

        /* access modifiers changed from: package-private */
        public int a() {
            return this.e != -1 ? this.e : f.a(this.f1569a);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int
         arg types: [java.lang.String, int, int, int]
         candidates:
          com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, java.lang.String):int
          com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int */
        /* access modifiers changed from: package-private */
        public C0022a a(f fVar, String str) {
            int i;
            int a2 = m.a(str, 0, str.length());
            int b2 = m.b(str, a2, str.length());
            if (b(str, a2, b2) != -1) {
                if (str.regionMatches(true, a2, "https:", 0, 6)) {
                    this.f1569a = "https";
                    a2 += "https:".length();
                } else if (!str.regionMatches(true, a2, "http:", 0, 5)) {
                    return C0022a.UNSUPPORTED_SCHEME;
                } else {
                    this.f1569a = "http";
                    a2 += "http:".length();
                }
            } else if (fVar == null) {
                return C0022a.MISSING_SCHEME;
            } else {
                this.f1569a = fVar.f1567b;
            }
            boolean z = false;
            boolean z2 = false;
            int c2 = c(str, a2, b2);
            if (c2 >= 2 || fVar == null || !fVar.f1567b.equals(this.f1569a)) {
                int i2 = a2 + c2;
                while (true) {
                    boolean z3 = z2;
                    boolean z4 = z;
                    int i3 = i2;
                    int a3 = m.a(str, i3, b2, "@/\\?#");
                    switch (a3 != b2 ? str.charAt(a3) : 65535) {
                        case 65535:
                        case '#':
                        case '/':
                        case '?':
                        case '\\':
                            int d2 = d(str, i3, a3);
                            if (d2 + 1 < a3) {
                                this.d = e(str, i3, d2);
                                this.e = g(str, d2 + 1, a3);
                                if (this.e == -1) {
                                    return C0022a.INVALID_PORT;
                                }
                            } else {
                                this.d = e(str, i3, d2);
                                this.e = f.a(this.f1569a);
                            }
                            if (this.d != null) {
                                a2 = a3;
                                break;
                            } else {
                                return C0022a.INVALID_HOST;
                            }
                        case '@':
                            if (!z3) {
                                int a4 = m.a(str, i3, a3, ':');
                                String a5 = f.a(str, i3, a4, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                if (z4) {
                                    a5 = this.f1570b + "%40" + a5;
                                }
                                this.f1570b = a5;
                                if (a4 != a3) {
                                    z3 = true;
                                    this.c = f.a(str, a4 + 1, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                                }
                                z4 = true;
                            } else {
                                this.c += "%40" + f.a(str, i3, a3, " \"':;<=>@[]^`{}|/\\?#", true, false, false, true);
                            }
                            i2 = a3 + 1;
                            z2 = z3;
                            break;
                        default:
                            z2 = z3;
                            i2 = i3;
                            break;
                    }
                    z = z4;
                }
            } else {
                this.f1570b = fVar.b();
                this.c = fVar.c();
                this.d = fVar.e;
                this.e = fVar.f;
                this.f.clear();
                this.f.addAll(fVar.d());
                if (a2 == b2 || str.charAt(a2) == '#') {
                    a(fVar.e());
                }
            }
            int a6 = m.a(str, a2, b2, "?#");
            a(str, a2, a6);
            if (a6 >= b2 || str.charAt(a6) != '?') {
                i = a6;
            } else {
                i = m.a(str, a6, b2, '#');
                this.g = f.b(f.a(str, a6 + 1, i, " \"'<>#", true, false, true, true));
            }
            if (i < b2 && str.charAt(i) == '#') {
                this.h = f.a(str, i + 1, b2, "", true, false, false, false);
            }
            return C0022a.SUCCESS;
        }

        public a a(String str) {
            this.g = str != null ? f.b(f.a(str, " \"'<>#", true, false, true, true)) : null;
            return this;
        }

        public a a(String str, String str2) {
            if (str == null) {
                throw new IllegalArgumentException("name == null");
            }
            if (this.g == null) {
                this.g = new ArrayList();
            }
            this.g.add(f.a(str, " \"'<>#&=", false, false, true, true));
            this.g.add(str2 != null ? f.a(str2, " \"'<>#&=", false, false, true, true) : null);
            return this;
        }

        public f b() {
            if (this.f1569a == null) {
                throw new IllegalStateException("scheme == null");
            } else if (this.d != null) {
                return new f(this, null);
            } else {
                throw new IllegalStateException("host == null");
            }
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(this.f1569a);
            sb.append("://");
            if (!this.f1570b.isEmpty() || !this.c.isEmpty()) {
                sb.append(this.f1570b);
                if (!this.c.isEmpty()) {
                    sb.append(':');
                    sb.append(this.c);
                }
                sb.append('@');
            }
            if (this.d.indexOf(58) != -1) {
                sb.append('[');
                sb.append(this.d);
                sb.append(']');
            } else {
                sb.append(this.d);
            }
            int a2 = a();
            if (a2 != f.a(this.f1569a)) {
                sb.append(':');
                sb.append(a2);
            }
            f.a(sb, this.f);
            if (this.g != null) {
                sb.append('?');
                f.b(sb, this.g);
            }
            if (this.h != null) {
                sb.append('#');
                sb.append(this.h);
            }
            return sb.toString();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.f.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.f.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
      com.meizu.cloud.pushsdk.a.d.f.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      com.meizu.cloud.pushsdk.a.d.f.a(java.lang.String, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.f.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String>
     arg types: [java.util.List<java.lang.String>, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.f.a(java.lang.String, boolean):java.lang.String
      com.meizu.cloud.pushsdk.a.d.f.a(java.lang.StringBuilder, java.util.List<java.lang.String>):void
      com.meizu.cloud.pushsdk.a.d.f.a(java.util.List<java.lang.String>, boolean):java.util.List<java.lang.String> */
    private f(a aVar) {
        String str = null;
        this.f1567b = aVar.f1569a;
        this.c = a(aVar.f1570b, false);
        this.d = a(aVar.c, false);
        this.e = aVar.d;
        this.f = aVar.a();
        this.g = a(aVar.f, false);
        this.h = aVar.g != null ? a(aVar.g, true) : null;
        this.i = aVar.h != null ? a(aVar.h, false) : str;
        this.j = aVar.toString();
    }

    /* synthetic */ f(a aVar, AnonymousClass1 r2) {
        this(aVar);
    }

    static int a(char c2) {
        if (c2 >= '0' && c2 <= '9') {
            return c2 - '0';
        }
        if (c2 >= 'a' && c2 <= 'f') {
            return (c2 - 'a') + 10;
        }
        if (c2 < 'A' || c2 > 'F') {
            return -1;
        }
        return (c2 - 'A') + 10;
    }

    public static int a(String str) {
        if (str.equals("http")) {
            return 80;
        }
        return str.equals("https") ? 443 : -1;
    }

    static String a(String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || ((codePointAt == 37 && (!z || (z2 && !a(str, i4, i3)))) || (codePointAt == 43 && z3)))) {
                com.meizu.cloud.pushsdk.a.h.a aVar = new com.meizu.cloud.pushsdk.a.h.a();
                aVar.a(str, i2, i4);
                a(aVar, str, i4, i3, str2, z, z2, z3, z4);
                return aVar.h();
            }
            i4 += Character.charCount(codePointAt);
        }
        return str.substring(i2, i3);
    }

    static String a(String str, int i2, int i3, boolean z) {
        for (int i4 = i2; i4 < i3; i4++) {
            char charAt = str.charAt(i4);
            if (charAt == '%' || (charAt == '+' && z)) {
                com.meizu.cloud.pushsdk.a.h.a aVar = new com.meizu.cloud.pushsdk.a.h.a();
                aVar.a(str, i2, i4);
                a(aVar, str, i4, i3, z);
                return aVar.h();
            }
        }
        return str.substring(i2, i3);
    }

    static String a(String str, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        return a(str, 0, str.length(), str2, z, z2, z3, z4);
    }

    static String a(String str, boolean z) {
        return a(str, 0, str.length(), z);
    }

    private List<String> a(List<String> list, boolean z) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            String next = it.next();
            arrayList.add(next != null ? a(next, z) : null);
        }
        return Collections.unmodifiableList(arrayList);
    }

    static void a(com.meizu.cloud.pushsdk.a.h.a aVar, String str, int i2, int i3, String str2, boolean z, boolean z2, boolean z3, boolean z4) {
        com.meizu.cloud.pushsdk.a.h.a aVar2 = null;
        while (i2 < i3) {
            int codePointAt = str.codePointAt(i2);
            if (!z || !(codePointAt == 9 || codePointAt == 10 || codePointAt == 12 || codePointAt == 13)) {
                if (codePointAt == 43 && z3) {
                    aVar.b(z ? "+" : "%2B");
                } else if (codePointAt < 32 || codePointAt == 127 || ((codePointAt >= 128 && z4) || str2.indexOf(codePointAt) != -1 || (codePointAt == 37 && (!z || (z2 && !a(str, i2, i3)))))) {
                    if (aVar2 == null) {
                        aVar2 = new com.meizu.cloud.pushsdk.a.h.a();
                    }
                    aVar2.a(codePointAt);
                    while (!aVar2.c()) {
                        byte f2 = aVar2.f() & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                        aVar.b(37);
                        aVar.b((int) f1566a[(f2 >> 4) & 15]);
                        aVar.b((int) f1566a[f2 & 15]);
                    }
                } else {
                    aVar.a(codePointAt);
                }
            }
            i2 += Character.charCount(codePointAt);
        }
    }

    static void a(com.meizu.cloud.pushsdk.a.h.a aVar, String str, int i2, int i3, boolean z) {
        int i4 = i2;
        while (i4 < i3) {
            int codePointAt = str.codePointAt(i4);
            if (codePointAt != 37 || i4 + 2 >= i3) {
                if (codePointAt == 43 && z) {
                    aVar.b(32);
                }
                aVar.a(codePointAt);
            } else {
                int a2 = a(str.charAt(i4 + 1));
                int a3 = a(str.charAt(i4 + 2));
                if (!(a2 == -1 || a3 == -1)) {
                    aVar.b((a2 << 4) + a3);
                    i4 += 2;
                }
                aVar.a(codePointAt);
            }
            i4 += Character.charCount(codePointAt);
        }
    }

    static void a(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            sb.append('/');
            sb.append(list.get(i2));
        }
    }

    static boolean a(String str, int i2, int i3) {
        return i2 + 2 < i3 && str.charAt(i2) == '%' && a(str.charAt(i2 + 1)) != -1 && a(str.charAt(i2 + 2)) != -1;
    }

    static List<String> b(String str) {
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 <= str.length()) {
            int indexOf = str.indexOf(38, i2);
            if (indexOf == -1) {
                indexOf = str.length();
            }
            int indexOf2 = str.indexOf(61, i2);
            if (indexOf2 == -1 || indexOf2 > indexOf) {
                arrayList.add(str.substring(i2, indexOf));
                arrayList.add(null);
            } else {
                arrayList.add(str.substring(i2, indexOf2));
                arrayList.add(str.substring(indexOf2 + 1, indexOf));
            }
            i2 = indexOf + 1;
        }
        return arrayList;
    }

    static void b(StringBuilder sb, List<String> list) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2 += 2) {
            String str = list.get(i2);
            String str2 = list.get(i2 + 1);
            if (i2 > 0) {
                sb.append('&');
            }
            sb.append(str);
            if (str2 != null) {
                sb.append('=');
                sb.append(str2);
            }
        }
    }

    public static f c(String str) {
        a aVar = new a();
        if (aVar.a((f) null, str) == a.C0022a.SUCCESS) {
            return aVar.b();
        }
        return null;
    }

    public boolean a() {
        return this.f1567b.equals("https");
    }

    public String b() {
        if (this.c.isEmpty()) {
            return "";
        }
        int length = this.f1567b.length() + 3;
        return this.j.substring(length, m.a(this.j, length, this.j.length(), ":@"));
    }

    public String c() {
        if (this.d.isEmpty()) {
            return "";
        }
        int indexOf = this.j.indexOf(64);
        return this.j.substring(this.j.indexOf(58, this.f1567b.length() + 3) + 1, indexOf);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, java.lang.String):int
      com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int */
    public List<String> d() {
        int indexOf = this.j.indexOf(47, this.f1567b.length() + 3);
        int a2 = m.a(this.j, indexOf, this.j.length(), "?#");
        ArrayList arrayList = new ArrayList();
        while (indexOf < a2) {
            int i2 = indexOf + 1;
            indexOf = m.a(this.j, i2, a2, '/');
            arrayList.add(this.j.substring(i2, indexOf));
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int
     arg types: [java.lang.String, int, int, int]
     candidates:
      com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, java.lang.String):int
      com.meizu.cloud.pushsdk.a.d.m.a(java.lang.String, int, int, char):int */
    public String e() {
        if (this.h == null) {
            return null;
        }
        int indexOf = this.j.indexOf(63) + 1;
        return this.j.substring(indexOf, m.a(this.j, indexOf + 1, this.j.length(), '#'));
    }

    public boolean equals(Object obj) {
        return (obj instanceof f) && ((f) obj).j.equals(this.j);
    }

    public String f() {
        if (this.i == null) {
            return null;
        }
        return this.j.substring(this.j.indexOf(35) + 1);
    }

    public a g() {
        a aVar = new a();
        aVar.f1569a = this.f1567b;
        aVar.f1570b = b();
        aVar.c = c();
        aVar.d = this.e;
        aVar.e = this.f != a(this.f1567b) ? this.f : -1;
        aVar.f.clear();
        aVar.f.addAll(d());
        aVar.a(e());
        aVar.h = f();
        return aVar;
    }

    public int hashCode() {
        return this.j.hashCode();
    }

    public String toString() {
        return this.j;
    }
}
