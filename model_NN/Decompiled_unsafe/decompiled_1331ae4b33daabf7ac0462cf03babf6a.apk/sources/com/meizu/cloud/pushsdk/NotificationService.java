package com.meizu.cloud.pushsdk;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.text.TextUtils;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;
import com.meizu.cloud.pushsdk.util.UxIPUtils;
import java.lang.reflect.Method;
import java.util.List;

public class NotificationService extends IntentService {
    public NotificationService() {
        super("NotificationService");
        if (!MzSystemUtils.isBrandMeizu()) {
        }
    }

    public NotificationService(String str) {
        super(str);
        if (!MzSystemUtils.isBrandMeizu()) {
        }
    }

    public String a(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || !MzSystemUtils.isBrandMeizu()) {
            return null;
        }
        Intent intent = new Intent(str2);
        intent.setPackage(str);
        List<ResolveInfo> queryBroadcastReceivers = getPackageManager().queryBroadcastReceivers(intent, 0);
        if (queryBroadcastReceivers == null || queryBroadcastReceivers.size() <= 0) {
            return null;
        }
        return queryBroadcastReceivers.get(0).activityInfo.name;
    }

    public void a(Intent intent) {
        if (MzSystemUtils.isBrandMeizu()) {
            String a2 = a(getPackageName(), intent.getAction());
            if (TextUtils.isEmpty(a2)) {
                UxIPUtils.notificationEvent(this, intent, "reflectReceiver sendbroadcast", PushConstants.NOTIFICATIONSERVICE_SEND_MESSAGE_BROADCAST);
                DebugLogger.i("NotificationService", " reflectReceiver error: receiver for: " + intent.getAction() + " not found, package: " + getPackageName());
                intent.setPackage(getPackageName());
                sendBroadcast(intent);
                return;
            }
            try {
                UxIPUtils.notificationEvent(this, intent, "reflectReceiver startservice", PushConstants.NOTIFICATIONSERVICE_SEND_MESSAGE);
                Class<?> cls = Class.forName(a2);
                Object newInstance = cls.getConstructor(null).newInstance(null);
                Method method = cls.getMethod("onReceive", Context.class, Intent.class);
                intent.setClassName(getPackageName(), a2);
                method.invoke(newInstance, getApplicationContext(), intent);
            } catch (Exception e) {
                DebugLogger.i("NotificationService", "reflect e: " + e);
                UxIPUtils.notificationEvent(this, intent, e.getMessage(), PushConstants.NOTIFICATIONSERVICE_SEND_MESSAGE_ERROR);
            }
        }
    }

    public void onDestroy() {
        DebugLogger.i("NotificationService", "NotificationService destroy");
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        if (intent != null) {
            try {
                if (MzSystemUtils.isBrandMeizu()) {
                    DebugLogger.i("NotificationService", "onHandleIntentaction " + intent.getAction());
                    String stringExtra = intent.getStringExtra("command_type");
                    DebugLogger.d("NotificationService", "-- command_type -- " + stringExtra);
                    if (!TextUtils.isEmpty(stringExtra) && stringExtra.equals("reflect_receiver")) {
                        a(intent);
                    }
                }
            } catch (Exception e) {
                DebugLogger.e("NotificationService", "onHandleIntent error " + e.getMessage());
            }
        }
    }

    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }
}
