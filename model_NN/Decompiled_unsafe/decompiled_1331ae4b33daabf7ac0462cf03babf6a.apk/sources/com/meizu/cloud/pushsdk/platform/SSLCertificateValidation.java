package com.meizu.cloud.pushsdk.platform;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public final class SSLCertificateValidation {

    class NullHostnameVerifier implements HostnameVerifier {
        private NullHostnameVerifier() {
        }

        public boolean verify(String str, SSLSession sSLSession) {
            return true;
        }
    }

    class NullX509TrustManager implements X509TrustManager {
        private NullX509TrustManager() {
        }

        public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    public static HostnameVerifier getHostnameVerifier() {
        return new NullHostnameVerifier();
    }

    public static SSLSocketFactory getSSLSocketFactory() {
        KeyManagementException e;
        SSLContext sSLContext;
        NoSuchAlgorithmException e2;
        try {
            sSLContext = SSLContext.getInstance("TLS");
            try {
                sSLContext.init(null, new TrustManager[]{new NullX509TrustManager()}, new SecureRandom());
            } catch (NoSuchAlgorithmException e3) {
                e2 = e3;
                e2.printStackTrace();
                return sSLContext.getSocketFactory();
            } catch (KeyManagementException e4) {
                e = e4;
                e.printStackTrace();
                return sSLContext.getSocketFactory();
            }
        } catch (NoSuchAlgorithmException e5) {
            NoSuchAlgorithmException noSuchAlgorithmException = e5;
            sSLContext = null;
            e2 = noSuchAlgorithmException;
            e2.printStackTrace();
            return sSLContext.getSocketFactory();
        } catch (KeyManagementException e6) {
            KeyManagementException keyManagementException = e6;
            sSLContext = null;
            e = keyManagementException;
            e.printStackTrace();
            return sSLContext.getSocketFactory();
        }
        return sSLContext.getSocketFactory();
    }
}
