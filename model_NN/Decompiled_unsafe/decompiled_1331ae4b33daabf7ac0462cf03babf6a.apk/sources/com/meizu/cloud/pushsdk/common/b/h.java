package com.meizu.cloud.pushsdk.common.b;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.meizu.cloud.pushsdk.common.b.e;

public class h {
    public static String a(Context context) {
        if (d.a().f1650a) {
            return (String) d.a().f1651b;
        }
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getDeviceId();
        }
        return null;
    }

    public static boolean a() {
        e.c a2 = f.a("ro.meizu.product.model");
        return (a2.f1650a && !TextUtils.isEmpty((CharSequence) a2.f1651b)) || "meizu".equalsIgnoreCase(Build.BRAND) || "22c4185e".equalsIgnoreCase(Build.BRAND);
    }

    public static boolean a(int i) {
        return Build.VERSION.SDK_INT >= i;
    }

    public static boolean b() {
        if (a.a().f1650a) {
            return ((Boolean) a.a().f1651b).booleanValue();
        }
        return false;
    }

    public static boolean b(Context context) {
        try {
            return (context.getApplicationInfo().flags & 2) != 0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean c() {
        e.c a2 = f.a("ro.meizu.locale.region");
        if (a2.f1650a) {
            return "india".equals(a2.f1651b);
        }
        return false;
    }
}
