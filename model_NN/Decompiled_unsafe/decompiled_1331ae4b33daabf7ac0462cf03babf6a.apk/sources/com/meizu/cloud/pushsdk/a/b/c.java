package com.meizu.cloud.pushsdk.a.b;

import java.util.concurrent.Executor;

public class c implements d {

    /* renamed from: a  reason: collision with root package name */
    public static final int f1549a = ((Runtime.getRuntime().availableProcessors() * 2) + 1);

    /* renamed from: b  reason: collision with root package name */
    private final a f1550b;
    private final a c;
    private final Executor d = new e();

    public c() {
        f fVar = new f(10);
        this.f1550b = new a(f1549a, fVar);
        this.c = new a(2, fVar);
    }

    public a a() {
        return this.f1550b;
    }

    public a b() {
        return this.c;
    }

    public Executor c() {
        return this.d;
    }
}
