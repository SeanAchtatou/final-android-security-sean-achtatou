package com.meizu.cloud.pushsdk.a.d;

import com.meizu.cloud.pushsdk.a.d.c;

public class k {

    /* renamed from: a  reason: collision with root package name */
    private final i f1589a;

    /* renamed from: b  reason: collision with root package name */
    private final int f1590b;
    private final String c;
    private final c d;
    private final l e;
    private k f;
    private k g;
    private final k h;

    public class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public i f1591a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public int f1592b = -1;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public c.a d = new c.a();
        /* access modifiers changed from: private */
        public l e;
        /* access modifiers changed from: private */
        public k f;
        /* access modifiers changed from: private */
        public k g;
        /* access modifiers changed from: private */
        public k h;

        public a a(int i) {
            this.f1592b = i;
            return this;
        }

        public a a(c cVar) {
            this.d = cVar.c();
            return this;
        }

        public a a(i iVar) {
            this.f1591a = iVar;
            return this;
        }

        public a a(l lVar) {
            this.e = lVar;
            return this;
        }

        public a a(String str) {
            this.c = str;
            return this;
        }

        public k a() {
            if (this.f1591a == null) {
                throw new IllegalStateException("request == null");
            } else if (this.f1592b >= 0) {
                return new k(this);
            } else {
                throw new IllegalStateException("code < 0: " + this.f1592b);
            }
        }
    }

    private k(a aVar) {
        this.f1589a = aVar.f1591a;
        this.f1590b = aVar.f1592b;
        this.c = aVar.c;
        this.d = aVar.d.a();
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
    }

    public int a() {
        return this.f1590b;
    }

    public l b() {
        return this.e;
    }

    public String toString() {
        return "Response{protocol=, code=" + this.f1590b + ", message=" + this.c + ", url=" + this.f1589a.a() + '}';
    }
}
