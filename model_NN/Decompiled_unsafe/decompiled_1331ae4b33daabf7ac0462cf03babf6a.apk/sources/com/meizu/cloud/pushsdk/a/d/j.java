package com.meizu.cloud.pushsdk.a.d;

import com.meizu.cloud.pushsdk.a.h.b;
import com.meizu.cloud.pushsdk.a.h.f;
import com.meizu.cloud.pushsdk.a.h.l;
import java.io.File;
import java.nio.charset.Charset;

public abstract class j {
    public static j a(final g gVar, final File file) {
        if (file != null) {
            return new j() {
                public g a() {
                    return g.this;
                }

                public void a(b bVar) {
                    l lVar = null;
                    try {
                        lVar = f.a(file);
                        bVar.a(lVar);
                    } finally {
                        m.a(lVar);
                    }
                }

                public long b() {
                    return file.length();
                }
            };
        }
        throw new NullPointerException("content == null");
    }

    public static j a(g gVar, String str) {
        Charset charset = m.c;
        if (gVar != null && (charset = gVar.b()) == null) {
            charset = m.c;
            gVar = g.a(gVar + "; charset=utf-8");
        }
        return a(gVar, str.getBytes(charset));
    }

    public static j a(g gVar, byte[] bArr) {
        return a(gVar, bArr, 0, bArr.length);
    }

    public static j a(final g gVar, final byte[] bArr, final int i, final int i2) {
        if (bArr == null) {
            throw new NullPointerException("content == null");
        }
        m.a((long) bArr.length, (long) i, (long) i2);
        return new j() {
            public g a() {
                return g.this;
            }

            public void a(b bVar) {
                bVar.c(bArr, i, i2);
            }

            public long b() {
                return (long) i2;
            }
        };
    }

    public abstract g a();

    public abstract void a(b bVar);

    public long b() {
        return -1;
    }
}
