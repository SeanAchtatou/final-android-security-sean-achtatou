package com.meizu.cloud.pushsdk.common.b;

import com.meizu.cloud.pushsdk.common.b.e;
import com.meizu.cloud.pushsdk.constants.MeizuConstants;
import java.util.HashMap;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, e.c> f1652a = new HashMap<>();

    public static <T> e.c<T> a(String str) {
        if (f1652a.containsKey(str)) {
            return f1652a.get(str);
        }
        e.c<T> a2 = e.a(MeizuConstants.CLS_NAME_SYSTEM_PROPERTIES).b("get").a(new Object[]{str}).a();
        if (!a2.f1650a) {
            return a2;
        }
        f1652a.put(str, a2);
        return a2;
    }
}
