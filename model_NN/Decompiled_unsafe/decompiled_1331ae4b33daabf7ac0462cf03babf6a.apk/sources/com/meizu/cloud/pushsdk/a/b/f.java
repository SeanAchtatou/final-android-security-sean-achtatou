package com.meizu.cloud.pushsdk.a.b;

import android.os.Process;
import java.util.concurrent.ThreadFactory;

public class f implements ThreadFactory {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final int f1552a;

    public f(int i) {
        this.f1552a = i;
    }

    public Thread newThread(final Runnable runnable) {
        return new Thread(new Runnable() {
            public void run() {
                try {
                    Process.setThreadPriority(f.this.f1552a);
                } catch (Throwable th) {
                }
                runnable.run();
            }
        });
    }
}
