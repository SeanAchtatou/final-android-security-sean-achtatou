package com.meizu.cloud.pushsdk.a.h;

import java.io.EOFException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class d implements Serializable, Comparable<d> {

    /* renamed from: a  reason: collision with root package name */
    static final char[] f1612a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /* renamed from: b  reason: collision with root package name */
    public static final d f1613b = a(new byte[0]);
    private static final long serialVersionUID = 1;
    final byte[] c;
    transient int d;
    transient String e;

    d(byte[] bArr) {
        this.c = bArr;
    }

    public static d a(InputStream inputStream, int i) {
        if (inputStream == null) {
            throw new IllegalArgumentException("in == null");
        } else if (i < 0) {
            throw new IllegalArgumentException("byteCount < 0: " + i);
        } else {
            byte[] bArr = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    throw new EOFException();
                }
                i2 += read;
            }
            return new d(bArr);
        }
    }

    public static d a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("s == null");
        }
        d dVar = new d(str.getBytes(n.f1631a));
        dVar.e = str;
        return dVar;
    }

    public static d a(byte... bArr) {
        if (bArr != null) {
            return new d((byte[]) bArr.clone());
        }
        throw new IllegalArgumentException("data == null");
    }

    private d b(String str) {
        try {
            return a(MessageDigest.getInstance(str).digest(this.c));
        } catch (NoSuchAlgorithmException e2) {
            throw new AssertionError(e2);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) {
        d a2 = a(objectInputStream, objectInputStream.readInt());
        try {
            Field declaredField = d.class.getDeclaredField("c");
            declaredField.setAccessible(true);
            declaredField.set(this, a2.c);
        } catch (NoSuchFieldException e2) {
            throw new AssertionError();
        } catch (IllegalAccessException e3) {
            throw new AssertionError();
        }
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeInt(this.c.length);
        objectOutputStream.write(this.c);
    }

    public byte a(int i) {
        return this.c[i];
    }

    /* renamed from: a */
    public int compareTo(d dVar) {
        int d2 = d();
        int d3 = dVar.d();
        int min = Math.min(d2, d3);
        for (int i = 0; i < min; i++) {
            byte a2 = a(i) & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            byte a3 = dVar.a(i) & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (a2 != a3) {
                return a2 < a3 ? -1 : 1;
            }
        }
        if (d2 == d3) {
            return 0;
        }
        return d2 >= d3 ? 1 : -1;
    }

    public String a() {
        String str = this.e;
        if (str != null) {
            return str;
        }
        String str2 = new String(this.c, n.f1631a);
        this.e = str2;
        return str2;
    }

    /* access modifiers changed from: package-private */
    public void a(a aVar) {
        aVar.c(this.c, 0, this.c.length);
    }

    public boolean a(int i, byte[] bArr, int i2, int i3) {
        return i <= this.c.length - i3 && i2 <= bArr.length - i3 && n.a(this.c, i, bArr, i2, i3);
    }

    public d b() {
        return b("MD5");
    }

    public String c() {
        char[] cArr = new char[(this.c.length * 2)];
        int i = 0;
        for (byte b2 : this.c) {
            int i2 = i + 1;
            cArr[i] = f1612a[(b2 >> 4) & 15];
            i = i2 + 1;
            cArr[i2] = f1612a[b2 & 15];
        }
        return new String(cArr);
    }

    public int d() {
        return this.c.length;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        return (obj instanceof d) && ((d) obj).d() == this.c.length && ((d) obj).a(0, this.c, 0, this.c.length);
    }

    public int hashCode() {
        int i = this.d;
        if (i != 0) {
            return i;
        }
        int hashCode = Arrays.hashCode(this.c);
        this.d = hashCode;
        return hashCode;
    }

    public String toString() {
        if (this.c.length == 0) {
            return "ByteString[size=0]";
        }
        if (this.c.length <= 16) {
            return String.format("ByteString[size=%s data=%s]", Integer.valueOf(this.c.length), c());
        }
        return String.format("ByteString[size=%s md5=%s]", Integer.valueOf(this.c.length), b().c());
    }
}
