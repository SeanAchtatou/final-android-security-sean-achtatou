package com.meizu.cloud.pushsdk.common.b;

import com.meizu.cloud.pushsdk.common.b.e;

class a {

    /* renamed from: a  reason: collision with root package name */
    private static String f1632a = "android.os.BuildExt";

    /* renamed from: b  reason: collision with root package name */
    private static e.c<Boolean> f1633b;

    public static synchronized e.c<Boolean> a() {
        e.c<Boolean> cVar;
        synchronized (a.class) {
            if (f1633b == null) {
                f1633b = new e.c<>();
            }
            if (!f1633b.f1650a) {
                f1633b = e.a(f1632a).b("isProductInternational").a();
            }
            cVar = f1633b;
        }
        return cVar;
    }
}
