package com.meizu.cloud.pushsdk.a.b;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f1547a = null;

    /* renamed from: b  reason: collision with root package name */
    private final d f1548b = new c();

    private b() {
    }

    public static b a() {
        if (f1547a == null) {
            synchronized (b.class) {
                if (f1547a == null) {
                    f1547a = new b();
                }
            }
        }
        return f1547a;
    }

    public d b() {
        return this.f1548b;
    }
}
