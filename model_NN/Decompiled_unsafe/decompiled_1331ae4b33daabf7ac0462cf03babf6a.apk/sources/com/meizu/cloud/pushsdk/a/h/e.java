package com.meizu.cloud.pushsdk.a.h;

public abstract class e implements k {

    /* renamed from: a  reason: collision with root package name */
    private final k f1614a;

    public e(k kVar) {
        if (kVar == null) {
            throw new IllegalArgumentException("delegate == null");
        }
        this.f1614a = kVar;
    }

    public void a(a aVar, long j) {
        this.f1614a.a(aVar, j);
    }

    public void close() {
        this.f1614a.close();
    }

    public void flush() {
        this.f1614a.flush();
    }

    public String toString() {
        return getClass().getSimpleName() + "(" + this.f1614a.toString() + ")";
    }
}
