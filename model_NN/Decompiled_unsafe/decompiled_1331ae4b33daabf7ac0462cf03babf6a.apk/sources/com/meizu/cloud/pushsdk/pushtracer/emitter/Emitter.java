package com.meizu.cloud.pushsdk.pushtracer.emitter;

import android.content.Context;
import android.net.Uri;
import com.meizu.cloud.pushsdk.a.d.e;
import com.meizu.cloud.pushsdk.a.d.g;
import com.meizu.cloud.pushsdk.a.d.i;
import com.meizu.cloud.pushsdk.a.d.j;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.meizu.cloud.pushsdk.pushtracer.constant.TrackerConstants;
import com.meizu.cloud.pushsdk.pushtracer.dataload.DataLoad;
import com.meizu.cloud.pushsdk.pushtracer.storage.EventStore;
import com.meizu.cloud.pushsdk.pushtracer.utils.Logger;
import com.meizu.cloud.pushsdk.pushtracer.utils.Util;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;

public abstract class Emitter {
    protected final g JSON = g.a(TrackerConstants.POST_CONTENT_TYPE);
    protected int POST_STM_BYTES = 22;
    protected int POST_WRAPPER_BYTES = 88;
    private final String TAG = Emitter.class.getSimpleName();
    protected BufferOption bufferOption;
    protected long byteLimitGet;
    protected long byteLimitPost;
    protected Context context;
    protected int emitterTick;
    protected int emptyLimit;
    protected HostnameVerifier hostnameVerifier;
    protected HttpMethod httpMethod;
    protected AtomicBoolean isRunning = new AtomicBoolean(false);
    protected RequestCallback requestCallback;
    protected RequestSecurity requestSecurity;
    protected int sendLimit;
    protected SSLSocketFactory sslSocketFactory;
    protected TimeUnit timeUnit;
    protected String uri;
    protected Uri.Builder uriBuilder;

    public class EmitterBuilder {
        protected static Class<? extends Emitter> defaultEmitterClass;
        protected BufferOption bufferOption;
        protected long byteLimitGet;
        protected long byteLimitPost;
        protected final Context context;
        private Class<? extends Emitter> emitterClass;
        protected int emitterTick;
        protected int emptyLimit;
        protected HostnameVerifier hostnameVerifier;
        protected HttpMethod httpMethod;
        protected RequestCallback requestCallback;
        protected RequestSecurity requestSecurity;
        protected int sendLimit;
        protected SSLSocketFactory sslSocketFactory;
        protected TimeUnit timeUnit;
        protected final String uri;

        static {
            try {
                defaultEmitterClass = Class.forName("com.meizu.cloud.pushsdk.pushtracer.rx.Emitter");
            } catch (ClassNotFoundException e) {
                try {
                    defaultEmitterClass = Class.forName("com.meizu.cloud.pushsdk.pushtracer.emitter.classic.Emitter");
                } catch (ClassNotFoundException e2) {
                    defaultEmitterClass = null;
                }
            }
        }

        public EmitterBuilder(String str, Context context2) {
            this(str, context2, defaultEmitterClass);
        }

        public EmitterBuilder(String str, Context context2, Class<? extends Emitter> cls) {
            this.requestCallback = null;
            this.httpMethod = HttpMethod.POST;
            this.bufferOption = BufferOption.Single;
            this.requestSecurity = RequestSecurity.HTTPS;
            this.emitterTick = 5;
            this.sendLimit = 250;
            this.emptyLimit = 5;
            this.byteLimitGet = 40000;
            this.byteLimitPost = 40000;
            this.timeUnit = TimeUnit.SECONDS;
            this.uri = str;
            this.context = context2;
            this.emitterClass = cls;
        }

        public Emitter build() {
            if (this.emitterClass == null) {
                throw new IllegalStateException("No emitter class found or defined");
            }
            try {
                return (Emitter) this.emitterClass.getDeclaredConstructor(EmitterBuilder.class).newInstance(this);
            } catch (NoSuchMethodException e) {
                throw new IllegalStateException("Can’t create emitter", e);
            } catch (InvocationTargetException e2) {
                throw new IllegalStateException("Can’t create emitter", e2);
            } catch (InstantiationException e3) {
                throw new IllegalStateException("Can’t create emitter", e3);
            } catch (IllegalAccessException e4) {
                throw new IllegalStateException("Can’t create emitter", e4);
            }
        }

        public EmitterBuilder byteLimitGet(long j) {
            this.byteLimitGet = j;
            return this;
        }

        public EmitterBuilder byteLimitPost(long j) {
            this.byteLimitPost = j;
            return this;
        }

        public EmitterBuilder callback(RequestCallback requestCallback2) {
            this.requestCallback = requestCallback2;
            return this;
        }

        public EmitterBuilder emptyLimit(int i) {
            this.emptyLimit = i;
            return this;
        }

        public EmitterBuilder hostnameVerifier(HostnameVerifier hostnameVerifier2) {
            this.hostnameVerifier = hostnameVerifier2;
            return this;
        }

        public EmitterBuilder method(HttpMethod httpMethod2) {
            this.httpMethod = httpMethod2;
            return this;
        }

        public EmitterBuilder option(BufferOption bufferOption2) {
            this.bufferOption = bufferOption2;
            return this;
        }

        public EmitterBuilder security(RequestSecurity requestSecurity2) {
            this.requestSecurity = requestSecurity2;
            return this;
        }

        public EmitterBuilder sendLimit(int i) {
            this.sendLimit = i;
            return this;
        }

        public EmitterBuilder sslSocketFactory(SSLSocketFactory sSLSocketFactory) {
            this.sslSocketFactory = sSLSocketFactory;
            return this;
        }

        public EmitterBuilder tick(int i) {
            this.emitterTick = i;
            return this;
        }

        public EmitterBuilder timeUnit(TimeUnit timeUnit2) {
            this.timeUnit = timeUnit2;
            return this;
        }
    }

    public Emitter(EmitterBuilder emitterBuilder) {
        this.httpMethod = emitterBuilder.httpMethod;
        this.requestCallback = emitterBuilder.requestCallback;
        this.context = emitterBuilder.context;
        this.bufferOption = emitterBuilder.bufferOption;
        this.requestSecurity = emitterBuilder.requestSecurity;
        this.sslSocketFactory = emitterBuilder.sslSocketFactory;
        this.hostnameVerifier = emitterBuilder.hostnameVerifier;
        this.emitterTick = emitterBuilder.emitterTick;
        this.emptyLimit = emitterBuilder.emptyLimit;
        this.sendLimit = emitterBuilder.sendLimit;
        this.byteLimitGet = emitterBuilder.byteLimitGet;
        this.byteLimitPost = emitterBuilder.byteLimitPost;
        this.uri = emitterBuilder.uri;
        this.timeUnit = emitterBuilder.timeUnit;
        buildEmitterUri();
        Logger.i(this.TAG, "Emitter created successfully!", new Object[0]);
    }

    private void addStmToEvent(DataLoad dataLoad, String str) {
        if (str.equals("")) {
            str = Util.getTimestamp();
        }
        dataLoad.add(Parameters.SENT_TIMESTAMP, str);
    }

    private void buildEmitterUri() {
        Logger.e(this.TAG, "security " + this.requestSecurity, new Object[0]);
        if (this.requestSecurity == RequestSecurity.HTTP) {
            this.uriBuilder = Uri.parse("http://" + this.uri).buildUpon();
        } else {
            this.uriBuilder = Uri.parse("https://" + this.uri).buildUpon();
        }
        if (this.httpMethod == HttpMethod.GET) {
            this.uriBuilder.appendPath("i");
        } else {
            this.uriBuilder.appendEncodedPath("push_data_report/mobile");
        }
    }

    private void buildHttpsSecurity() {
        if (this.requestSecurity == RequestSecurity.HTTPS) {
            if (this.sslSocketFactory == null) {
                Logger.e(this.TAG, "Https Ensure you have set SSLSocketFactory", new Object[0]);
            }
            if (this.hostnameVerifier == null) {
                Logger.e(this.TAG, "Https Ensure you have set HostnameVerifier", new Object[0]);
            }
        }
    }

    private i requestBuilderGet(DataLoad dataLoad) {
        addStmToEvent(dataLoad, "");
        this.uriBuilder.clearQuery();
        HashMap hashMap = (HashMap) dataLoad.getMap();
        for (String str : hashMap.keySet()) {
            this.uriBuilder.appendQueryParameter(str, (String) hashMap.get(str));
        }
        return new i.a().a(this.uriBuilder.build().toString()).a().c();
    }

    private i requestBuilderPost(ArrayList<DataLoad> arrayList) {
        new ArrayList();
        StringBuffer stringBuffer = new StringBuffer();
        Iterator<DataLoad> it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next().toString());
        }
        String uri2 = this.uriBuilder.build().toString();
        String stringBuffer2 = stringBuffer.toString();
        Logger.d(this.TAG, "post final String " + stringBuffer2, new Object[0]);
        return new i.a().a(uri2).a(j.a(this.JSON, stringBuffer2)).c();
    }

    public abstract void add(DataLoad dataLoad);

    /* access modifiers changed from: protected */
    public LinkedList<ReadyRequest> buildRequests(EmittableEvents emittableEvents) {
        int size = emittableEvents.getEvents().size();
        LinkedList<Long> eventIds = emittableEvents.getEventIds();
        LinkedList<ReadyRequest> linkedList = new LinkedList<>();
        if (this.httpMethod == HttpMethod.GET) {
            for (int i = 0; i < size; i++) {
                LinkedList linkedList2 = new LinkedList();
                linkedList2.add(eventIds.get(i));
                DataLoad dataLoad = emittableEvents.getEvents().get(i);
                linkedList.add(new ReadyRequest(dataLoad.getByteSize() + ((long) this.POST_STM_BYTES) > this.byteLimitGet, requestBuilderGet(dataLoad), linkedList2));
            }
        } else {
            for (int i2 = 0; i2 < size; i2 += this.bufferOption.getCode()) {
                LinkedList linkedList3 = new LinkedList();
                ArrayList arrayList = new ArrayList();
                LinkedList linkedList4 = linkedList3;
                long j = 0;
                int i3 = i2;
                while (i3 < this.bufferOption.getCode() + i2 && i3 < size) {
                    DataLoad dataLoad2 = emittableEvents.getEvents().get(i3);
                    long byteSize = dataLoad2.getByteSize() + ((long) this.POST_STM_BYTES);
                    if (((long) this.POST_WRAPPER_BYTES) + byteSize > this.byteLimitPost) {
                        ArrayList arrayList2 = new ArrayList();
                        LinkedList linkedList5 = new LinkedList();
                        arrayList2.add(dataLoad2);
                        linkedList5.add(eventIds.get(i3));
                        linkedList.add(new ReadyRequest(true, requestBuilderPost(arrayList2), linkedList5));
                    } else if (j + byteSize + ((long) this.POST_WRAPPER_BYTES) + ((long) (arrayList.size() - 1)) > this.byteLimitPost) {
                        linkedList.add(new ReadyRequest(false, requestBuilderPost(arrayList), linkedList4));
                        arrayList = new ArrayList();
                        linkedList4 = new LinkedList();
                        arrayList.add(dataLoad2);
                        linkedList4.add(eventIds.get(i3));
                        j = byteSize;
                    } else {
                        j += byteSize;
                        arrayList.add(dataLoad2);
                        linkedList4.add(eventIds.get(i3));
                    }
                    i3++;
                }
                if (!arrayList.isEmpty()) {
                    linkedList.add(new ReadyRequest(false, requestBuilderPost(arrayList), linkedList4));
                }
            }
        }
        return linkedList;
    }

    public abstract void flush();

    public BufferOption getBufferOption() {
        return this.bufferOption;
    }

    public long getByteLimitGet() {
        return this.byteLimitGet;
    }

    public long getByteLimitPost() {
        return this.byteLimitPost;
    }

    public abstract boolean getEmitterStatus();

    public int getEmitterTick() {
        return this.emitterTick;
    }

    public String getEmitterUri() {
        return this.uriBuilder.clearQuery().build().toString();
    }

    public int getEmptyLimit() {
        return this.emptyLimit;
    }

    public abstract EventStore getEventStore();

    public HttpMethod getHttpMethod() {
        return this.httpMethod;
    }

    public RequestCallback getRequestCallback() {
        return this.requestCallback;
    }

    public RequestSecurity getRequestSecurity() {
        return this.requestSecurity;
    }

    public int getSendLimit() {
        return this.sendLimit;
    }

    /* access modifiers changed from: protected */
    public boolean isSuccessfulSend(int i) {
        return i >= 200 && i < 300;
    }

    /* access modifiers changed from: protected */
    public LinkedList<RequestResult> performSyncEmit(LinkedList<ReadyRequest> linkedList) {
        LinkedList<RequestResult> linkedList2 = new LinkedList<>();
        Iterator<ReadyRequest> it = linkedList.iterator();
        while (it.hasNext()) {
            ReadyRequest next = it.next();
            int requestSender = requestSender(next.getRequest());
            if (next.isOversize()) {
                linkedList2.add(new RequestResult(true, next.getEventIds()));
            } else {
                linkedList2.add(new RequestResult(isSuccessfulSend(requestSender), next.getEventIds()));
            }
        }
        return linkedList2;
    }

    /* access modifiers changed from: protected */
    public int requestSender(i iVar) {
        try {
            Logger.d(this.TAG, "Sending request: %s", iVar);
            return new e(iVar).a().a();
        } catch (IOException e) {
            Logger.e(this.TAG, "Request sending failed: %s", e.toString());
            return -1;
        }
    }

    public void setBufferOption(BufferOption bufferOption2) {
        if (!this.isRunning.get()) {
            this.bufferOption = bufferOption2;
        }
    }

    public void setEmitterUri(String str) {
        if (!this.isRunning.get()) {
            this.uri = str;
            buildEmitterUri();
        }
    }

    public void setHttpMethod(HttpMethod httpMethod2) {
        if (!this.isRunning.get()) {
            this.httpMethod = httpMethod2;
            buildEmitterUri();
        }
    }

    public void setRequestSecurity(RequestSecurity requestSecurity2) {
        if (!this.isRunning.get()) {
            this.requestSecurity = requestSecurity2;
            buildEmitterUri();
        }
    }

    public abstract void shutdown();
}
