package com.meizu.cloud.pushsdk.a.a;

import com.meizu.cloud.pushinternal.DebugLogger;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1518a = false;

    /* renamed from: b  reason: collision with root package name */
    private static String f1519b = "AndroidNetworking";

    public static void a() {
        f1518a = true;
    }

    public static void a(String str) {
        if (f1518a) {
            DebugLogger.d(f1519b, str);
        }
    }

    public static void b(String str) {
        if (f1518a) {
            DebugLogger.i(f1519b, str);
        }
    }
}
