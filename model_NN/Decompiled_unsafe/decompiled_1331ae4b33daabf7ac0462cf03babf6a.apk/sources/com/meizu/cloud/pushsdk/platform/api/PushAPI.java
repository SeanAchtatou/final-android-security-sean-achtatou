package com.meizu.cloud.pushsdk.platform.api;

import android.content.Context;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.a.a;
import com.meizu.cloud.pushsdk.a.e.k;
import com.meizu.cloud.pushsdk.common.b.h;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.meizu.cloud.pushsdk.platform.SignUtils;
import com.sina.weibo.sdk.constant.WBConstants;
import java.util.LinkedHashMap;

public class PushAPI {
    public static String API_SERVER = "https://api-push.meizu.com/garcia/api/client/";
    private static final String CHANGE_REGISTER_SWITCH_STATUS_URL_PRIX = (API_SERVER + "message/changeRegisterSwitch");
    private static final String CHECK_REGISTER_SWITCH_STATUS_URL_PRIX = (API_SERVER + "message/getRegisterSwitch");
    private static final String CHECK_SUBSCRIBE_ALIAS_URL_PRIX = (API_SERVER + "message/getSubAlias");
    private static final String CHECK_SUBSCRIBE_TAGS_URL_PRIX = (API_SERVER + "message/getSubTags");
    private static final String REGISTER_URL_PRIX = (API_SERVER + "message/registerPush");
    private static final String SUBSCRIBE_ALIAS_URL_PRIX = (API_SERVER + "message/subscribeAlias");
    private static final String SUBSCRIBE_TAGS_URL_PRIX = (API_SERVER + "message/subscribeTags");
    public static final String TAG = "PushAPI";
    private static final String UNREGISTER_URL_ADVANCE_PRIX = (API_SERVER + "advance/unRegisterPush");
    private static final String UNREGISTER_URL_PRIX = (API_SERVER + "message/unRegisterPush");
    private static final String UNSUBSCRIBE_ALIAS_URL_PRIX = (API_SERVER + "message/unSubscribeAlias");
    private static final String UNSUBSCRIBE_TAGS_URL_PRIX = (API_SERVER + "message/unSubscribeTags");

    public PushAPI(Context context) {
        a.a();
        if (h.b() || h.c()) {
            API_SERVER = "https://api-push.in.meizu.com/garcia/api/client";
        }
    }

    public void checkPush(String str, String str2, String str3, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "checkPush post map " + linkedHashMap2);
        a.a(CHECK_REGISTER_SWITCH_STATUS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void checkSubScribeAlias(String str, String str2, String str3, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "checkPush post map " + linkedHashMap2);
        a.a(CHECK_SUBSCRIBE_ALIAS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void checkSubScribeTags(String str, String str2, String str3, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "checkPush post map " + linkedHashMap2);
        a.a(CHECK_SUBSCRIBE_TAGS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void register(String str, String str2, String str3, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put("deviceId", str3);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "register post map " + linkedHashMap2);
        a.b(REGISTER_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void subScribeAlias(String str, String str2, String str3, String str4, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(WBConstants.SSO_APP_KEY, str2);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        linkedHashMap.put("alias", str4);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "subScribeTags post map " + linkedHashMap2);
        a.b(SUBSCRIBE_ALIAS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void subScribeTags(String str, String str2, String str3, String str4, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        linkedHashMap.put("tags", str4);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "subScribeTags post map " + linkedHashMap2);
        a.b(SUBSCRIBE_TAGS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void switchPush(String str, String str2, String str3, int i, boolean z, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        linkedHashMap.put("msgType", String.valueOf(i));
        linkedHashMap.put("subSwitch", z ? "1" : "0");
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "swithPush post map " + linkedHashMap2);
        a.b(CHANGE_REGISTER_SWITCH_STATUS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void unRegister(String str, String str2, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("packageName", str);
        linkedHashMap.put("deviceId", str2);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, "4a2ca769d79f4856bb3bd982d30de790"));
        DebugLogger.i(TAG, "advance unregister post map " + linkedHashMap2);
        a.b(UNREGISTER_URL_ADVANCE_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void unRegister(String str, String str2, String str3, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put("deviceId", str3);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "unregister post map " + linkedHashMap2);
        a.a(UNREGISTER_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void unSubScribeAlias(String str, String str2, String str3, String str4, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        linkedHashMap.put("alias", str4);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "subScribeTags post map " + linkedHashMap2);
        a.b(UNSUBSCRIBE_ALIAS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }

    public void unSubScribeTags(String str, String str2, String str3, String str4, k kVar) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("appId", str);
        linkedHashMap.put(PushConstants.KEY_PUSH_ID, str3);
        linkedHashMap.put("tags", str4);
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        linkedHashMap2.putAll(linkedHashMap);
        linkedHashMap2.put("sign", SignUtils.getSignature(linkedHashMap, str2));
        DebugLogger.i(TAG, "subScribeTags post map " + linkedHashMap2);
        a.b(UNSUBSCRIBE_TAGS_URL_PRIX).a(linkedHashMap2).a().a(kVar);
    }
}
