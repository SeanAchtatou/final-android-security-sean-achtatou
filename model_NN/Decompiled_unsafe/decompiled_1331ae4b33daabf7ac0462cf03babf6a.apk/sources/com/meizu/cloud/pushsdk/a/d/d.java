package com.meizu.cloud.pushsdk.a.d;

import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class d {
    public static boolean a(String str) {
        return str.equals("POST") || str.equals(HttpProxyConstants.PUT) || str.equals("PATCH") || str.equals("PROPPATCH") || str.equals("REPORT");
    }

    public static boolean b(String str) {
        return a(str) || str.equals("OPTIONS") || str.equals("DELETE") || str.equals("PROPFIND") || str.equals("MKCOL") || str.equals("LOCK");
    }
}
