package com.meizu.cloud.pushsdk.a.h;

final class i {

    /* renamed from: a  reason: collision with root package name */
    final byte[] f1625a;

    /* renamed from: b  reason: collision with root package name */
    int f1626b;
    int c;
    boolean d;
    boolean e;
    i f;
    i g;

    i() {
        this.f1625a = new byte[2048];
        this.e = true;
        this.d = false;
    }

    i(i iVar) {
        this(iVar.f1625a, iVar.f1626b, iVar.c);
        iVar.d = true;
    }

    i(byte[] bArr, int i, int i2) {
        this.f1625a = bArr;
        this.f1626b = i;
        this.c = i2;
        this.e = false;
        this.d = true;
    }

    public i a() {
        i iVar = this.f != this ? this.f : null;
        this.g.f = this.f;
        this.f.g = this.g;
        this.f = null;
        this.g = null;
        return iVar;
    }

    public i a(int i) {
        if (i <= 0 || i > this.c - this.f1626b) {
            throw new IllegalArgumentException();
        }
        i iVar = new i(this);
        iVar.c = iVar.f1626b + i;
        this.f1626b += i;
        this.g.a(iVar);
        return iVar;
    }

    public i a(i iVar) {
        iVar.g = this;
        iVar.f = this.f;
        this.f.g = iVar;
        this.f = iVar;
        return iVar;
    }

    public void a(i iVar, int i) {
        if (!iVar.e) {
            throw new IllegalArgumentException();
        }
        if (iVar.c + i > 2048) {
            if (iVar.d) {
                throw new IllegalArgumentException();
            } else if ((iVar.c + i) - iVar.f1626b > 2048) {
                throw new IllegalArgumentException();
            } else {
                System.arraycopy(iVar.f1625a, iVar.f1626b, iVar.f1625a, 0, iVar.c - iVar.f1626b);
                iVar.c -= iVar.f1626b;
                iVar.f1626b = 0;
            }
        }
        System.arraycopy(this.f1625a, this.f1626b, iVar.f1625a, iVar.c, i);
        iVar.c += i;
        this.f1626b += i;
    }

    public void b() {
        if (this.g == this) {
            throw new IllegalStateException();
        } else if (this.g.e) {
            int i = this.c - this.f1626b;
            if (i <= (this.g.d ? 0 : this.g.f1626b) + (2048 - this.g.c)) {
                a(this.g, i);
                a();
                j.a(this);
            }
        }
    }
}
