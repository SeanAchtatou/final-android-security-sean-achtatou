package com.meizu.cloud.pushsdk.common.b;

import android.text.TextUtils;
import com.meizu.cloud.pushsdk.common.b.e;

class d {

    /* renamed from: a  reason: collision with root package name */
    private static e.c<String> f1647a;

    public static synchronized e.c<String> a() {
        e.c<String> cVar;
        synchronized (d.class) {
            if (f1647a == null) {
                f1647a = new e.c<>();
            }
            if (!f1647a.f1650a || TextUtils.isEmpty((CharSequence) f1647a.f1651b)) {
                f1647a = e.a("android.telephony.MzTelephonyManager").b("getDeviceId").a();
            }
            cVar = f1647a;
        }
        return cVar;
    }
}
