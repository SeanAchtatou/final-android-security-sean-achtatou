package com.meizu.cloud.pushsdk.notification;

import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.meizu.cloud.pushinternal.R;
import com.meizu.cloud.pushsdk.handler.MessageV3;
import com.meizu.cloud.pushsdk.util.MinSdkChecker;

public class b extends f {
    public b(Context context, PushNotificationBuilder pushNotificationBuilder) {
        super(context, pushNotificationBuilder);
    }

    private void b(RemoteViews remoteViews, MessageV3 messageV3) {
        if (messageV3.getmNotificationStyle() != null && !a()) {
            if (!TextUtils.isEmpty(messageV3.getmNotificationStyle().getExpandableImageUrl())) {
                Bitmap a2 = a(messageV3.getmNotificationStyle().getExpandableImageUrl());
                if (a2 != null) {
                    remoteViews.setViewVisibility(R.id.push_big_bigview_defaultView, 0);
                    remoteViews.setImageViewBitmap(R.id.push_big_bigview_defaultView, a2);
                    return;
                }
                remoteViews.setViewVisibility(R.id.push_big_bigview_defaultView, 8);
                return;
            }
            remoteViews.setViewVisibility(R.id.push_big_bigview_defaultView, 8);
        }
    }

    /* access modifiers changed from: protected */
    public void b(Notification notification, MessageV3 messageV3) {
        if (MinSdkChecker.isSupportNotificationBuild()) {
            RemoteViews remoteViews = new RemoteViews(this.f1664a.getPackageName(), R.layout.push_expandable_big_image_notification);
            remoteViews.setTextViewText(R.id.push_big_notification_title, messageV3.getTitle());
            remoteViews.setTextViewText(R.id.push_big_notification_content, messageV3.getContent());
            remoteViews.setLong(R.id.push_big_notification_date, "setTime", System.currentTimeMillis());
            a(remoteViews, messageV3);
            b(remoteViews, messageV3);
            notification.bigContentView = remoteViews;
        }
    }
}
