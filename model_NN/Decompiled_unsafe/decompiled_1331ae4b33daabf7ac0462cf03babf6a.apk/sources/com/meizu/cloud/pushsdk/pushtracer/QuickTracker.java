package com.meizu.cloud.pushsdk.pushtracer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.igexin.sdk.PushConsts;
import com.meizu.cloud.pushinternal.DebugLogger;
import com.meizu.cloud.pushsdk.common.b.h;
import com.meizu.cloud.pushsdk.platform.SSLCertificateValidation;
import com.meizu.cloud.pushsdk.pushtracer.emitter.Emitter;
import com.meizu.cloud.pushsdk.pushtracer.emitter.RequestCallback;
import com.meizu.cloud.pushsdk.pushtracer.tracker.Subject;
import com.meizu.cloud.pushsdk.pushtracer.tracker.Tracker;
import com.meizu.cloud.pushsdk.pushtracer.utils.LogLevel;
import com.meizu.cloud.pushsdk.pushtracer.utils.Logger;
import com.meizu.cloud.pushsdk.pushtracer.utils.Util;

public class QuickTracker {
    public static final String namespace = "PushAndroidTracker";
    private static Tracker tracker;

    public static synchronized Tracker getAndroidTrackerClassic(Context context, RequestCallback requestCallback) {
        Tracker tracker2;
        synchronized (QuickTracker.class) {
            Subject subject = getSubject(context);
            if (tracker == null) {
                tracker = getTrackerClassic(getEmitterClassic(context, requestCallback), subject, context);
                registerNetworkReceiver(context, tracker);
            }
            tracker.setSubject(subject);
            tracker2 = tracker;
        }
        return tracker2;
    }

    private static Emitter getEmitterClassic(Context context, RequestCallback requestCallback) {
        return new Emitter.EmitterBuilder(getStaticsDomain(), context, com.meizu.cloud.pushsdk.pushtracer.emitter.classic.Emitter.class).sslSocketFactory(SSLCertificateValidation.getSSLSocketFactory()).hostnameVerifier(SSLCertificateValidation.getHostnameVerifier()).callback(requestCallback).tick(1).emptyLimit(2).build();
    }

    private static String getStaticsDomain() {
        if (h.b() || h.c()) {
            return "push.statics.in.meizu.com";
        }
        DebugLogger.e("QuickTracker", "current statics domain is " + "push.statics.meizu.com");
        return "push.statics.meizu.com";
    }

    private static Subject getSubject(Context context) {
        return new Subject.SubjectBuilder().context(context).build();
    }

    private static Tracker getTrackerClassic(Emitter emitter, Subject subject, Context context) {
        return new Tracker.TrackerBuilder(emitter, namespace, context.getPackageCodePath(), context, com.meizu.cloud.pushsdk.pushtracer.tracker.classic.Tracker.class).level(LogLevel.VERBOSE).base64(false).subject(subject).threadCount(2).build();
    }

    private static void registerNetworkReceiver(Context context, final Tracker tracker2) {
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                if (Util.isOnline(context)) {
                    Logger.e("QuickTracker", "restart track event: %s", "online true");
                    Tracker.this.restartEventTracking();
                }
            }
        }, new IntentFilter(PushConsts.ACTION_BROADCAST_NETWORK_CHANGE));
    }
}
