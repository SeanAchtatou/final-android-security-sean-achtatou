package com.meizu.cloud.pushsdk.platform;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class SignUtils {
    public static String getSignature(Map<String, String> map, String str) {
        Set<Map.Entry> entrySet = new TreeMap(map).entrySet();
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : entrySet) {
            sb.append((String) entry.getKey()).append("=").append((String) entry.getValue());
        }
        sb.append(str);
        return parseStrToMd5L32(sb.toString());
    }

    public static String parseStrToMd5L32(String str) {
        try {
            byte[] digest = MessageDigest.getInstance("MD5").digest(str.getBytes());
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b2 : digest) {
                byte b3 = b2 & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
                if (b3 < 16) {
                    stringBuffer.append(0);
                }
                stringBuffer.append(Integer.toHexString(b3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
