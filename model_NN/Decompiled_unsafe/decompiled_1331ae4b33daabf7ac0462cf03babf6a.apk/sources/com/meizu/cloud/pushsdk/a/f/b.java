package com.meizu.cloud.pushsdk.a.f;

import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.c;
import com.meizu.cloud.pushsdk.a.d.e;
import com.meizu.cloud.pushsdk.a.d.i;
import com.meizu.cloud.pushsdk.a.d.k;
import java.io.File;
import java.io.IOException;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public static String f1597a = null;

    public static k a(com.meizu.cloud.pushsdk.a.a.b bVar) {
        try {
            i.a a2 = new i.a().a(bVar.e());
            a(a2, bVar);
            switch (bVar.c()) {
                case 0:
                    a2 = a2.a();
                    break;
                case 1:
                    a2 = a2.a(bVar.q());
                    break;
                case 2:
                    a2 = a2.c(bVar.q());
                    break;
                case 3:
                    a2 = a2.b(bVar.q());
                    break;
                case 4:
                    a2 = a2.b();
                    break;
                case 5:
                    a2 = a2.d(bVar.q());
                    break;
            }
            bVar.a(new e(a2.c()));
            return bVar.n().a();
        } catch (IOException e) {
            throw new a(e);
        }
    }

    public static void a(i.a aVar, com.meizu.cloud.pushsdk.a.a.b bVar) {
        if (bVar.i() != null) {
            aVar.a("User-Agent", bVar.i());
        } else if (f1597a != null) {
            bVar.a(f1597a);
            aVar.a("User-Agent", f1597a);
        }
        c s = bVar.s();
        if (s != null) {
            aVar.a(s);
            if (bVar.i() != null && !s.b().contains("User-Agent")) {
                aVar.a("User-Agent", bVar.i());
            }
        }
    }

    public static k b(com.meizu.cloud.pushsdk.a.a.b bVar) {
        try {
            i.a a2 = new i.a().a(bVar.e());
            a(a2, bVar);
            bVar.a(new e(a2.a().c()));
            k a3 = bVar.n().a();
            com.meizu.cloud.pushsdk.a.i.b.a(a3, bVar.l(), bVar.m());
            return a3;
        } catch (IOException e) {
            try {
                File file = new File(bVar.l() + File.separator + bVar.m());
                if (file.exists()) {
                    file.delete();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            throw new a(e);
        }
    }

    public static k c(com.meizu.cloud.pushsdk.a.a.b bVar) {
        try {
            i.a a2 = new i.a().a(bVar.e());
            a(a2, bVar);
            bVar.a(new e(a2.a(new d(bVar.r(), bVar.k())).c()));
            return bVar.n().a();
        } catch (IOException e) {
            throw new a(e);
        }
    }
}
