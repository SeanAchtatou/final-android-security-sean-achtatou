package com.meizu.cloud.pushsdk.a.a;

import com.meizu.cloud.pushsdk.a.c.a;
import com.meizu.cloud.pushsdk.a.d.k;

public class c<T> {

    /* renamed from: a  reason: collision with root package name */
    private final T f1540a;

    /* renamed from: b  reason: collision with root package name */
    private final a f1541b;
    private k c;

    public c(a aVar) {
        this.f1540a = null;
        this.f1541b = aVar;
    }

    public c(T t) {
        this.f1540a = t;
        this.f1541b = null;
    }

    public static <T> c<T> a(a aVar) {
        return new c<>(aVar);
    }

    public static <T> c<T> a(Object obj) {
        return new c<>(obj);
    }

    public T a() {
        return this.f1540a;
    }

    public void a(k kVar) {
        this.c = kVar;
    }

    public boolean b() {
        return this.f1541b == null;
    }

    public a c() {
        return this.f1541b;
    }

    public k d() {
        return this.c;
    }
}
