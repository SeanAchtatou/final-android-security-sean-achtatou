package com.a.a.c;

import com.a.a.b.b;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/* compiled from: TypeToken */
public class a<T> {

    /* renamed from: a  reason: collision with root package name */
    final Class<? super T> f696a;

    /* renamed from: b  reason: collision with root package name */
    final Type f697b;
    final int c;

    protected a() {
        this.f697b = a(getClass());
        this.f696a = b.e(this.f697b);
        this.c = this.f697b.hashCode();
    }

    a(Type type) {
        this.f697b = b.d((Type) com.a.a.b.a.a(type));
        this.f696a = b.e(this.f697b);
        this.c = this.f697b.hashCode();
    }

    static Type a(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return b.d(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    public final Class<? super T> a() {
        return this.f696a;
    }

    public final Type b() {
        return this.f697b;
    }

    public final int hashCode() {
        return this.c;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof a) && b.a(this.f697b, ((a) obj).f697b);
    }

    public final String toString() {
        return b.f(this.f697b);
    }

    public static a<?> a(Type type) {
        return new a<>(type);
    }

    public static <T> a<T> b(Class<T> cls) {
        return new a<>(cls);
    }
}
