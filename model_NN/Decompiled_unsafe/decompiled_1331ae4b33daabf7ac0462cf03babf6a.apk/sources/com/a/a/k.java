package com.a.a;

/* compiled from: JsonNull */
public final class k extends i {

    /* renamed from: a  reason: collision with root package name */
    public static final k f714a = new k();

    public int hashCode() {
        return k.class.hashCode();
    }

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof k);
    }
}
