package com.a.a;

import com.a.a.b.a;
import com.a.a.b.f;
import java.math.BigInteger;

/* compiled from: JsonPrimitive */
public final class n extends i {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?>[] f716a = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};

    /* renamed from: b  reason: collision with root package name */
    private Object f717b;

    public n(Boolean bool) {
        a(bool);
    }

    public n(Number number) {
        a(number);
    }

    public n(String str) {
        a(str);
    }

    /* access modifiers changed from: package-private */
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.f717b = String.valueOf(((Character) obj).charValue());
            return;
        }
        a.a((obj instanceof Number) || b(obj));
        this.f717b = obj;
    }

    public boolean o() {
        return this.f717b instanceof Boolean;
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        return (Boolean) this.f717b;
    }

    public boolean f() {
        if (o()) {
            return n().booleanValue();
        }
        return Boolean.parseBoolean(b());
    }

    public boolean p() {
        return this.f717b instanceof Number;
    }

    public Number a() {
        return this.f717b instanceof String ? new f((String) this.f717b) : (Number) this.f717b;
    }

    public boolean q() {
        return this.f717b instanceof String;
    }

    public String b() {
        if (p()) {
            return a().toString();
        }
        if (o()) {
            return n().toString();
        }
        return (String) this.f717b;
    }

    public double c() {
        return p() ? a().doubleValue() : Double.parseDouble(b());
    }

    public long d() {
        return p() ? a().longValue() : Long.parseLong(b());
    }

    public int e() {
        return p() ? a().intValue() : Integer.parseInt(b());
    }

    private static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : f716a) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        if (this.f717b == null) {
            return 31;
        }
        if (a(this)) {
            long longValue = a().longValue();
            return (int) (longValue ^ (longValue >>> 32));
        } else if (!(this.f717b instanceof Number)) {
            return this.f717b.hashCode();
        } else {
            long doubleToLongBits = Double.doubleToLongBits(a().doubleValue());
            return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        }
    }

    public boolean equals(Object obj) {
        boolean z = false;
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        n nVar = (n) obj;
        if (this.f717b == null) {
            if (nVar.f717b != null) {
                return false;
            }
            return true;
        } else if (!a(this) || !a(nVar)) {
            if (!(this.f717b instanceof Number) || !(nVar.f717b instanceof Number)) {
                return this.f717b.equals(nVar.f717b);
            }
            double doubleValue = a().doubleValue();
            double doubleValue2 = nVar.a().doubleValue();
            if (doubleValue == doubleValue2 || (Double.isNaN(doubleValue) && Double.isNaN(doubleValue2))) {
                z = true;
            }
            return z;
        } else if (a().longValue() != nVar.a().longValue()) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean a(n nVar) {
        if (!(nVar.f717b instanceof Number)) {
            return false;
        }
        Number number = (Number) nVar.f717b;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }
}
