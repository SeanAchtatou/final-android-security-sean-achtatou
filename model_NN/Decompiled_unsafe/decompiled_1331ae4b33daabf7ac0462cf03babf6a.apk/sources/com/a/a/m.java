package com.a.a;

/* compiled from: JsonParseException */
public class m extends RuntimeException {
    public m(String str) {
        super(str);
    }

    public m(String str, Throwable th) {
        super(str, th);
    }

    public m(Throwable th) {
        super(th);
    }
}
