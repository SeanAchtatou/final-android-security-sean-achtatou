package com.a.a.b.a;

import com.a.a.c.a;
import com.a.a.d.b;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: TimeTypeAdapter */
public final class k extends r<Time> {

    /* renamed from: a  reason: collision with root package name */
    public static final s f625a = new s() {
        public <T> r<T> a(e eVar, a<T> aVar) {
            if (aVar.a() == Time.class) {
                return new k();
            }
            return null;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private final DateFormat f626b = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: a */
    public synchronized Time b(com.a.a.d.a aVar) throws IOException {
        Time time;
        if (aVar.f() == b.NULL) {
            aVar.j();
            time = null;
        } else {
            try {
                time = new Time(this.f626b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new p(e);
            }
        }
        return time;
    }

    public synchronized void a(c cVar, Time time) throws IOException {
        cVar.b(time == null ? null : this.f626b.format(time));
    }
}
