package com.a.a.b;

import com.a.a.a;
import com.a.a.b;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;

/* compiled from: Excluder */
public final class d implements s, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static final d f672a = new d();

    /* renamed from: b  reason: collision with root package name */
    private double f673b = -1.0d;
    private int c = 136;
    private boolean d = true;
    private boolean e;
    private List<a> f = Collections.emptyList();
    private List<a> g = Collections.emptyList();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public d clone() {
        try {
            return (d) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.b.d.a(java.lang.Class<?>, boolean):boolean
     arg types: [java.lang.Class<? super T>, int]
     candidates:
      com.a.a.b.d.a(com.a.a.a.d, com.a.a.a.e):boolean
      com.a.a.b.d.a(com.a.a.e, com.a.a.c.a):com.a.a.r<T>
      com.a.a.b.d.a(java.lang.reflect.Field, boolean):boolean
      com.a.a.s.a(com.a.a.e, com.a.a.c.a):com.a.a.r<T>
      com.a.a.b.d.a(java.lang.Class<?>, boolean):boolean */
    public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
        Class<? super T> a2 = aVar.a();
        final boolean a3 = a((Class<?>) a2, true);
        final boolean a4 = a((Class<?>) a2, false);
        if (!a3 && !a4) {
            return null;
        }
        final e eVar2 = eVar;
        final com.a.a.c.a<T> aVar2 = aVar;
        return new r<T>() {
            private r<T> f;

            public T b(com.a.a.d.a aVar) throws IOException {
                if (!a4) {
                    return b().b(aVar);
                }
                aVar.n();
                return null;
            }

            public void a(c cVar, T t) throws IOException {
                if (a3) {
                    cVar.f();
                } else {
                    b().a(cVar, t);
                }
            }

            private r<T> b() {
                r<T> rVar = this.f;
                if (rVar != null) {
                    return rVar;
                }
                r<T> a2 = eVar2.a(d.this, aVar2);
                this.f = a2;
                return a2;
            }
        };
    }

    public boolean a(Field field, boolean z) {
        com.a.a.a.a aVar;
        if ((this.c & field.getModifiers()) != 0) {
            return true;
        }
        if (this.f673b != -1.0d && !a((com.a.a.a.d) field.getAnnotation(com.a.a.a.d.class), (com.a.a.a.e) field.getAnnotation(com.a.a.a.e.class))) {
            return true;
        }
        if (field.isSynthetic()) {
            return true;
        }
        if (this.e && ((aVar = (com.a.a.a.a) field.getAnnotation(com.a.a.a.a.class)) == null || (!z ? !aVar.b() : !aVar.a()))) {
            return true;
        }
        if (!this.d && b(field.getType())) {
            return true;
        }
        if (a(field.getType())) {
            return true;
        }
        List<a> list = z ? this.f : this.g;
        if (!list.isEmpty()) {
            b bVar = new b(field);
            for (a a2 : list) {
                if (a2.a(bVar)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean a(Class<?> cls, boolean z) {
        if (this.f673b != -1.0d && !a((com.a.a.a.d) cls.getAnnotation(com.a.a.a.d.class), (com.a.a.a.e) cls.getAnnotation(com.a.a.a.e.class))) {
            return true;
        }
        if (!this.d && b(cls)) {
            return true;
        }
        if (a(cls)) {
            return true;
        }
        for (a a2 : z ? this.f : this.g) {
            if (a2.a(cls)) {
                return true;
            }
        }
        return false;
    }

    private boolean a(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    private boolean b(Class<?> cls) {
        return cls.isMemberClass() && !c(cls);
    }

    private boolean c(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    private boolean a(com.a.a.a.d dVar, com.a.a.a.e eVar) {
        return a(dVar) && a(eVar);
    }

    private boolean a(com.a.a.a.d dVar) {
        if (dVar == null || dVar.a() <= this.f673b) {
            return true;
        }
        return false;
    }

    private boolean a(com.a.a.a.e eVar) {
        if (eVar == null || eVar.a() > this.f673b) {
            return true;
        }
        return false;
    }
}
