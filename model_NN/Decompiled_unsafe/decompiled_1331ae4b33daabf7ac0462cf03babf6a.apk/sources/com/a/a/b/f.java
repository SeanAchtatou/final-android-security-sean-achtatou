package com.a.a.b;

import java.math.BigDecimal;

/* compiled from: LazilyParsedNumber */
public final class f extends Number {

    /* renamed from: a  reason: collision with root package name */
    private final String f677a;

    public f(String str) {
        this.f677a = str;
    }

    public int intValue() {
        try {
            return Integer.parseInt(this.f677a);
        } catch (NumberFormatException e) {
            try {
                return (int) Long.parseLong(this.f677a);
            } catch (NumberFormatException e2) {
                return new BigDecimal(this.f677a).intValue();
            }
        }
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f677a);
        } catch (NumberFormatException e) {
            return new BigDecimal(this.f677a).longValue();
        }
    }

    public float floatValue() {
        return Float.parseFloat(this.f677a);
    }

    public double doubleValue() {
        return Double.parseDouble(this.f677a);
    }

    public String toString() {
        return this.f677a;
    }

    public int hashCode() {
        return this.f677a.hashCode();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (this.f677a == fVar.f677a || this.f677a.equals(fVar.f677a)) {
            return true;
        }
        return false;
    }
}
