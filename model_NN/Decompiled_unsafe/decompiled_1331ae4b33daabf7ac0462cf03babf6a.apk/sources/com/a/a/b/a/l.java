package com.a.a.b.a;

import com.a.a.b.a.i;
import com.a.a.d.a;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.r;
import java.io.IOException;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;

/* compiled from: TypeAdapterRuntimeTypeWrapper */
final class l<T> extends r<T> {

    /* renamed from: a  reason: collision with root package name */
    private final e f627a;

    /* renamed from: b  reason: collision with root package name */
    private final r<T> f628b;
    private final Type c;

    l(e eVar, r<T> rVar, Type type) {
        this.f627a = eVar;
        this.f628b = rVar;
        this.c = type;
    }

    public T b(a aVar) throws IOException {
        return this.f628b.b(aVar);
    }

    public void a(c cVar, T t) throws IOException {
        r<T> rVar = this.f628b;
        Type a2 = a(this.c, t);
        if (a2 != this.c) {
            rVar = this.f627a.a(com.a.a.c.a.a(a2));
            if ((rVar instanceof i.a) && !(this.f628b instanceof i.a)) {
                rVar = this.f628b;
            }
        }
        rVar.a(cVar, t);
    }

    private Type a(Type type, Object obj) {
        if (obj == null) {
            return type;
        }
        if (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) {
            return obj.getClass();
        }
        return type;
    }
}
