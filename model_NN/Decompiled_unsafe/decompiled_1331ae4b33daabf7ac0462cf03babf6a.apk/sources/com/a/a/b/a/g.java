package com.a.a.b.a;

import com.a.a.b.b;
import com.a.a.b.c;
import com.a.a.b.h;
import com.a.a.b.j;
import com.a.a.e;
import com.a.a.i;
import com.a.a.n;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;

/* compiled from: MapTypeAdapterFactory */
public final class g implements s {

    /* renamed from: a  reason: collision with root package name */
    final boolean f610a;

    /* renamed from: b  reason: collision with root package name */
    private final c f611b;

    public g(c cVar, boolean z) {
        this.f611b = cVar;
        this.f610a = z;
    }

    public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
        Type b2 = aVar.b();
        if (!Map.class.isAssignableFrom(aVar.a())) {
            return null;
        }
        Type[] b3 = b.b(b2, b.e(b2));
        return new a(eVar, b3[0], a(eVar, b3[0]), b3[1], eVar.a(com.a.a.c.a.a(b3[1])), this.f611b.a(aVar));
    }

    private r<?> a(e eVar, Type type) {
        if (type == Boolean.TYPE || type == Boolean.class) {
            return m.f;
        }
        return eVar.a(com.a.a.c.a.a(type));
    }

    /* compiled from: MapTypeAdapterFactory */
    private final class a<K, V> extends r<Map<K, V>> {

        /* renamed from: b  reason: collision with root package name */
        private final r<K> f613b;
        private final r<V> c;
        private final h<? extends Map<K, V>> d;

        public a(e eVar, Type type, r<K> rVar, Type type2, r<V> rVar2, h<? extends Map<K, V>> hVar) {
            this.f613b = new l(eVar, rVar, type);
            this.c = new l(eVar, rVar2, type2);
            this.d = hVar;
        }

        /* renamed from: a */
        public Map<K, V> b(com.a.a.d.a aVar) throws IOException {
            com.a.a.d.b f = aVar.f();
            if (f == com.a.a.d.b.NULL) {
                aVar.j();
                return null;
            }
            Map<K, V> map = (Map) this.d.a();
            if (f == com.a.a.d.b.BEGIN_ARRAY) {
                aVar.a();
                while (aVar.e()) {
                    aVar.a();
                    K b2 = this.f613b.b(aVar);
                    if (map.put(b2, this.c.b(aVar)) != null) {
                        throw new p("duplicate key: " + ((Object) b2));
                    }
                    aVar.b();
                }
                aVar.b();
                return map;
            }
            aVar.c();
            while (aVar.e()) {
                com.a.a.b.e.f676a.a(aVar);
                K b3 = this.f613b.b(aVar);
                if (map.put(b3, this.c.b(aVar)) != null) {
                    throw new p("duplicate key: " + ((Object) b3));
                }
            }
            aVar.d();
            return map;
        }

        public void a(com.a.a.d.c cVar, Map<K, V> map) throws IOException {
            boolean z;
            int i = 0;
            if (map == null) {
                cVar.f();
            } else if (!g.this.f610a) {
                cVar.d();
                for (Map.Entry next : map.entrySet()) {
                    cVar.a(String.valueOf(next.getKey()));
                    this.c.a(cVar, next.getValue());
                }
                cVar.e();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                boolean z2 = false;
                for (Map.Entry next2 : map.entrySet()) {
                    i a2 = this.f613b.a(next2.getKey());
                    arrayList.add(a2);
                    arrayList2.add(next2.getValue());
                    if (a2.g() || a2.h()) {
                        z = true;
                    } else {
                        z = false;
                    }
                    z2 = z | z2;
                }
                if (z2) {
                    cVar.b();
                    while (i < arrayList.size()) {
                        cVar.b();
                        j.a((i) arrayList.get(i), cVar);
                        this.c.a(cVar, arrayList2.get(i));
                        cVar.c();
                        i++;
                    }
                    cVar.c();
                    return;
                }
                cVar.d();
                while (i < arrayList.size()) {
                    cVar.a(a((i) arrayList.get(i)));
                    this.c.a(cVar, arrayList2.get(i));
                    i++;
                }
                cVar.e();
            }
        }

        private String a(i iVar) {
            if (iVar.i()) {
                n m = iVar.m();
                if (m.p()) {
                    return String.valueOf(m.a());
                }
                if (m.o()) {
                    return Boolean.toString(m.f());
                }
                if (m.q()) {
                    return m.b();
                }
                throw new AssertionError();
            } else if (iVar.j()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }
    }
}
