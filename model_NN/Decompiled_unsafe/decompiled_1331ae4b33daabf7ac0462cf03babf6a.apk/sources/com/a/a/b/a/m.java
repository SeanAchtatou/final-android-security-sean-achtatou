package com.a.a.b.a;

import com.a.a.b.f;
import com.a.a.d.b;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.g;
import com.a.a.i;
import com.a.a.j;
import com.a.a.k;
import com.a.a.l;
import com.a.a.n;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;

/* compiled from: TypeAdapters */
public final class m {
    public static final r<String> A = new r<String>() {
        /* renamed from: a */
        public String b(com.a.a.d.a aVar) throws IOException {
            b f = aVar.f();
            if (f == b.NULL) {
                aVar.j();
                return null;
            } else if (f == b.BOOLEAN) {
                return Boolean.toString(aVar.i());
            } else {
                return aVar.h();
            }
        }

        public void a(c cVar, String str) throws IOException {
            cVar.b(str);
        }
    };
    public static final r<BigDecimal> B = new r<BigDecimal>() {
        /* renamed from: a */
        public BigDecimal b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return new BigDecimal(aVar.h());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, BigDecimal bigDecimal) throws IOException {
            cVar.a(bigDecimal);
        }
    };
    public static final r<BigInteger> C = new r<BigInteger>() {
        /* renamed from: a */
        public BigInteger b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return new BigInteger(aVar.h());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, BigInteger bigInteger) throws IOException {
            cVar.a(bigInteger);
        }
    };
    public static final s D = a(String.class, A);
    public static final r<StringBuilder> E = new r<StringBuilder>() {
        /* renamed from: a */
        public StringBuilder b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return new StringBuilder(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, StringBuilder sb) throws IOException {
            cVar.b(sb == null ? null : sb.toString());
        }
    };
    public static final s F = a(StringBuilder.class, E);
    public static final r<StringBuffer> G = new r<StringBuffer>() {
        /* renamed from: a */
        public StringBuffer b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return new StringBuffer(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, StringBuffer stringBuffer) throws IOException {
            cVar.b(stringBuffer == null ? null : stringBuffer.toString());
        }
    };
    public static final s H = a(StringBuffer.class, G);
    public static final r<URL> I = new r<URL>() {
        /* renamed from: a */
        public URL b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            String h = aVar.h();
            if (!"null".equals(h)) {
                return new URL(h);
            }
            return null;
        }

        public void a(c cVar, URL url) throws IOException {
            cVar.b(url == null ? null : url.toExternalForm());
        }
    };
    public static final s J = a(URL.class, I);
    public static final r<URI> K = new r<URI>() {
        /* renamed from: a */
        public URI b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                String h = aVar.h();
                if (!"null".equals(h)) {
                    return new URI(h);
                }
                return null;
            } catch (URISyntaxException e) {
                throw new j(e);
            }
        }

        public void a(c cVar, URI uri) throws IOException {
            cVar.b(uri == null ? null : uri.toASCIIString());
        }
    };
    public static final s L = a(URI.class, K);
    public static final r<InetAddress> M = new r<InetAddress>() {
        /* renamed from: a */
        public InetAddress b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return InetAddress.getByName(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, InetAddress inetAddress) throws IOException {
            cVar.b(inetAddress == null ? null : inetAddress.getHostAddress());
        }
    };
    public static final s N = b(InetAddress.class, M);
    public static final r<UUID> O = new r<UUID>() {
        /* renamed from: a */
        public UUID b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return UUID.fromString(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, UUID uuid) throws IOException {
            cVar.b(uuid == null ? null : uuid.toString());
        }
    };
    public static final s P = a(UUID.class, O);
    public static final r<Currency> Q = new r<Currency>() {
        /* renamed from: a */
        public Currency b(com.a.a.d.a aVar) throws IOException {
            return Currency.getInstance(aVar.h());
        }

        public void a(c cVar, Currency currency) throws IOException {
            cVar.b(currency.getCurrencyCode());
        }
    }.a();
    public static final s R = a(Currency.class, Q);
    public static final s S = new s() {
        public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
            if (aVar.a() != Timestamp.class) {
                return null;
            }
            final r a2 = eVar.a(Date.class);
            return new r<Timestamp>() {
                /* renamed from: a */
                public Timestamp b(com.a.a.d.a aVar) throws IOException {
                    Date date = (Date) a2.b(aVar);
                    if (date != null) {
                        return new Timestamp(date.getTime());
                    }
                    return null;
                }

                public void a(c cVar, Timestamp timestamp) throws IOException {
                    a2.a(cVar, timestamp);
                }
            };
        }
    };
    public static final r<Calendar> T = new r<Calendar>() {
        /* renamed from: a */
        public Calendar b(com.a.a.d.a aVar) throws IOException {
            int i = 0;
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            aVar.c();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (aVar.f() != b.END_OBJECT) {
                String g = aVar.g();
                int m = aVar.m();
                if ("year".equals(g)) {
                    i6 = m;
                } else if ("month".equals(g)) {
                    i5 = m;
                } else if ("dayOfMonth".equals(g)) {
                    i4 = m;
                } else if ("hourOfDay".equals(g)) {
                    i3 = m;
                } else if ("minute".equals(g)) {
                    i2 = m;
                } else if ("second".equals(g)) {
                    i = m;
                }
            }
            aVar.d();
            return new GregorianCalendar(i6, i5, i4, i3, i2, i);
        }

        public void a(c cVar, Calendar calendar) throws IOException {
            if (calendar == null) {
                cVar.f();
                return;
            }
            cVar.d();
            cVar.a("year");
            cVar.a((long) calendar.get(1));
            cVar.a("month");
            cVar.a((long) calendar.get(2));
            cVar.a("dayOfMonth");
            cVar.a((long) calendar.get(5));
            cVar.a("hourOfDay");
            cVar.a((long) calendar.get(11));
            cVar.a("minute");
            cVar.a((long) calendar.get(12));
            cVar.a("second");
            cVar.a((long) calendar.get(13));
            cVar.e();
        }
    };
    public static final s U = b(Calendar.class, GregorianCalendar.class, T);
    public static final r<Locale> V = new r<Locale>() {
        /* renamed from: a */
        public Locale b(com.a.a.d.a aVar) throws IOException {
            String str;
            String str2;
            String str3;
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(aVar.h(), "_");
            if (stringTokenizer.hasMoreElements()) {
                str = stringTokenizer.nextToken();
            } else {
                str = null;
            }
            if (stringTokenizer.hasMoreElements()) {
                str2 = stringTokenizer.nextToken();
            } else {
                str2 = null;
            }
            if (stringTokenizer.hasMoreElements()) {
                str3 = stringTokenizer.nextToken();
            } else {
                str3 = null;
            }
            if (str2 == null && str3 == null) {
                return new Locale(str);
            }
            if (str3 == null) {
                return new Locale(str, str2);
            }
            return new Locale(str, str2, str3);
        }

        public void a(c cVar, Locale locale) throws IOException {
            cVar.b(locale == null ? null : locale.toString());
        }
    };
    public static final s W = a(Locale.class, V);
    public static final r<i> X = new r<i>() {
        /* renamed from: a */
        public i b(com.a.a.d.a aVar) throws IOException {
            switch (AnonymousClass29.f643a[aVar.f().ordinal()]) {
                case 1:
                    return new n(new f(aVar.h()));
                case 2:
                    return new n(Boolean.valueOf(aVar.i()));
                case 3:
                    return new n(aVar.h());
                case 4:
                    aVar.j();
                    return k.f714a;
                case 5:
                    g gVar = new g();
                    aVar.a();
                    while (aVar.e()) {
                        gVar.a(b(aVar));
                    }
                    aVar.b();
                    return gVar;
                case 6:
                    l lVar = new l();
                    aVar.c();
                    while (aVar.e()) {
                        lVar.a(aVar.g(), b(aVar));
                    }
                    aVar.d();
                    return lVar;
                default:
                    throw new IllegalArgumentException();
            }
        }

        public void a(c cVar, i iVar) throws IOException {
            if (iVar == null || iVar.j()) {
                cVar.f();
            } else if (iVar.i()) {
                n m = iVar.m();
                if (m.p()) {
                    cVar.a(m.a());
                } else if (m.o()) {
                    cVar.a(m.f());
                } else {
                    cVar.b(m.b());
                }
            } else if (iVar.g()) {
                cVar.b();
                Iterator<i> it = iVar.l().iterator();
                while (it.hasNext()) {
                    a(cVar, it.next());
                }
                cVar.c();
            } else if (iVar.h()) {
                cVar.d();
                for (Map.Entry next : iVar.k().o()) {
                    cVar.a((String) next.getKey());
                    a(cVar, (i) next.getValue());
                }
                cVar.e();
            } else {
                throw new IllegalArgumentException("Couldn't write " + iVar.getClass());
            }
        }
    };
    public static final s Y = b(i.class, X);
    public static final s Z = new s() {
        public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
            Class<? super T> a2 = aVar.a();
            if (!Enum.class.isAssignableFrom(a2) || a2 == Enum.class) {
                return null;
            }
            if (!a2.isEnum()) {
                a2 = a2.getSuperclass();
            }
            return new a(a2);
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public static final r<Class> f629a = new r<Class>() {
        public void a(c cVar, Class cls) throws IOException {
            if (cls == null) {
                cVar.f();
                return;
            }
            throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
        }

        /* renamed from: a */
        public Class b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    };

    /* renamed from: b  reason: collision with root package name */
    public static final s f630b = a(Class.class, f629a);
    public static final r<BitSet> c = new r<BitSet>() {
        /* renamed from: a */
        public BitSet b(com.a.a.d.a aVar) throws IOException {
            boolean z;
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            BitSet bitSet = new BitSet();
            aVar.a();
            b f = aVar.f();
            int i = 0;
            while (f != b.END_ARRAY) {
                switch (AnonymousClass29.f643a[f.ordinal()]) {
                    case 1:
                        if (aVar.m() == 0) {
                            z = false;
                            break;
                        } else {
                            z = true;
                            break;
                        }
                    case 2:
                        z = aVar.i();
                        break;
                    case 3:
                        String h = aVar.h();
                        try {
                            if (Integer.parseInt(h) == 0) {
                                z = false;
                                break;
                            } else {
                                z = true;
                                break;
                            }
                        } catch (NumberFormatException e) {
                            throw new p("Error: Expecting: bitset number value (1, 0), Found: " + h);
                        }
                    default:
                        throw new p("Invalid bitset value type: " + f);
                }
                if (z) {
                    bitSet.set(i);
                }
                i++;
                f = aVar.f();
            }
            aVar.b();
            return bitSet;
        }

        public void a(c cVar, BitSet bitSet) throws IOException {
            int i;
            if (bitSet == null) {
                cVar.f();
                return;
            }
            cVar.b();
            for (int i2 = 0; i2 < bitSet.length(); i2++) {
                if (bitSet.get(i2)) {
                    i = 1;
                } else {
                    i = 0;
                }
                cVar.a((long) i);
            }
            cVar.c();
        }
    };
    public static final s d = a(BitSet.class, c);
    public static final r<Boolean> e = new r<Boolean>() {
        /* renamed from: a */
        public Boolean b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            } else if (aVar.f() == b.STRING) {
                return Boolean.valueOf(Boolean.parseBoolean(aVar.h()));
            } else {
                return Boolean.valueOf(aVar.i());
            }
        }

        public void a(c cVar, Boolean bool) throws IOException {
            if (bool == null) {
                cVar.f();
            } else {
                cVar.a(bool.booleanValue());
            }
        }
    };
    public static final r<Boolean> f = new r<Boolean>() {
        /* renamed from: a */
        public Boolean b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Boolean.valueOf(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Boolean bool) throws IOException {
            cVar.b(bool == null ? "null" : bool.toString());
        }
    };
    public static final s g = a(Boolean.TYPE, Boolean.class, e);
    public static final r<Number> h = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Byte.valueOf((byte) aVar.m());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final s i = a(Byte.TYPE, Byte.class, h);
    public static final r<Number> j = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Short.valueOf((short) aVar.m());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final s k = a(Short.TYPE, Short.class, j);
    public static final r<Number> l = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Integer.valueOf(aVar.m());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final s m = a(Integer.TYPE, Integer.class, l);
    public static final r<AtomicInteger> n = new r<AtomicInteger>() {
        /* renamed from: a */
        public AtomicInteger b(com.a.a.d.a aVar) throws IOException {
            try {
                return new AtomicInteger(aVar.m());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, AtomicInteger atomicInteger) throws IOException {
            cVar.a((long) atomicInteger.get());
        }
    }.a();
    public static final s o = a(AtomicInteger.class, n);
    public static final r<AtomicBoolean> p = new r<AtomicBoolean>() {
        /* renamed from: a */
        public AtomicBoolean b(com.a.a.d.a aVar) throws IOException {
            return new AtomicBoolean(aVar.i());
        }

        public void a(c cVar, AtomicBoolean atomicBoolean) throws IOException {
            cVar.a(atomicBoolean.get());
        }
    }.a();
    public static final s q = a(AtomicBoolean.class, p);
    public static final r<AtomicIntegerArray> r = new r<AtomicIntegerArray>() {
        /* renamed from: a */
        public AtomicIntegerArray b(com.a.a.d.a aVar) throws IOException {
            ArrayList arrayList = new ArrayList();
            aVar.a();
            while (aVar.e()) {
                try {
                    arrayList.add(Integer.valueOf(aVar.m()));
                } catch (NumberFormatException e) {
                    throw new p(e);
                }
            }
            aVar.b();
            int size = arrayList.size();
            AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
            for (int i = 0; i < size; i++) {
                atomicIntegerArray.set(i, ((Integer) arrayList.get(i)).intValue());
            }
            return atomicIntegerArray;
        }

        public void a(c cVar, AtomicIntegerArray atomicIntegerArray) throws IOException {
            cVar.b();
            int length = atomicIntegerArray.length();
            for (int i = 0; i < length; i++) {
                cVar.a((long) atomicIntegerArray.get(i));
            }
            cVar.c();
        }
    }.a();
    public static final s s = a(AtomicIntegerArray.class, r);
    public static final r<Number> t = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            try {
                return Long.valueOf(aVar.l());
            } catch (NumberFormatException e) {
                throw new p(e);
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final r<Number> u = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Float.valueOf((float) aVar.k());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final r<Number> v = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return Double.valueOf(aVar.k());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final r<Number> w = new r<Number>() {
        /* renamed from: a */
        public Number b(com.a.a.d.a aVar) throws IOException {
            b f = aVar.f();
            switch (f) {
                case NUMBER:
                    return new f(aVar.h());
                case BOOLEAN:
                case STRING:
                default:
                    throw new p("Expecting number, got: " + f);
                case NULL:
                    aVar.j();
                    return null;
            }
        }

        public void a(c cVar, Number number) throws IOException {
            cVar.a(number);
        }
    };
    public static final s x = a(Number.class, w);
    public static final r<Character> y = new r<Character>() {
        /* renamed from: a */
        public Character b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() == b.NULL) {
                aVar.j();
                return null;
            }
            String h = aVar.h();
            if (h.length() == 1) {
                return Character.valueOf(h.charAt(0));
            }
            throw new p("Expecting character, got: " + h);
        }

        public void a(c cVar, Character ch) throws IOException {
            cVar.b(ch == null ? null : String.valueOf(ch));
        }
    };
    public static final s z = a(Character.TYPE, Character.class, y);

    /* compiled from: TypeAdapters */
    private static final class a<T extends Enum<T>> extends r<T> {

        /* renamed from: a  reason: collision with root package name */
        private final Map<String, T> f644a = new HashMap();

        /* renamed from: b  reason: collision with root package name */
        private final Map<T, String> f645b = new HashMap();

        public a(Class<T> cls) {
            try {
                for (Enum enumR : (Enum[]) cls.getEnumConstants()) {
                    String name = enumR.name();
                    com.a.a.a.c cVar = (com.a.a.a.c) cls.getField(name).getAnnotation(com.a.a.a.c.class);
                    if (cVar != null) {
                        name = cVar.a();
                        for (String put : cVar.b()) {
                            this.f644a.put(put, enumR);
                        }
                    }
                    String str = name;
                    this.f644a.put(str, enumR);
                    this.f645b.put(enumR, str);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError("Missing field in " + cls.getName(), e);
            }
        }

        /* renamed from: a */
        public T b(com.a.a.d.a aVar) throws IOException {
            if (aVar.f() != b.NULL) {
                return (Enum) this.f644a.get(aVar.h());
            }
            aVar.j();
            return null;
        }

        public void a(c cVar, T t) throws IOException {
            cVar.b(t == null ? null : this.f645b.get(t));
        }
    }

    public static <TT> s a(final Class<TT> cls, final r<TT> rVar) {
        return new s() {
            public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
                if (aVar.a() == cls) {
                    return rVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls.getName() + ",adapter=" + rVar + "]";
            }
        };
    }

    public static <TT> s a(final Class<TT> cls, final Class<TT> cls2, final r<? super TT> rVar) {
        return new s() {
            public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
                Class<? super T> a2 = aVar.a();
                if (a2 == cls || a2 == cls2) {
                    return rVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls2.getName() + "+" + cls.getName() + ",adapter=" + rVar + "]";
            }
        };
    }

    public static <TT> s b(final Class<TT> cls, final Class<? extends TT> cls2, final r<? super TT> rVar) {
        return new s() {
            public <T> r<T> a(e eVar, com.a.a.c.a<T> aVar) {
                Class<? super T> a2 = aVar.a();
                if (a2 == cls || a2 == cls2) {
                    return rVar;
                }
                return null;
            }

            public String toString() {
                return "Factory[type=" + cls.getName() + "+" + cls2.getName() + ",adapter=" + rVar + "]";
            }
        };
    }

    public static <T1> s b(final Class<T1> cls, final r<T1> rVar) {
        return new s() {
            public <T2> r<T2> a(e eVar, com.a.a.c.a<T2> aVar) {
                final Class<? super T2> a2 = aVar.a();
                if (!cls.isAssignableFrom(a2)) {
                    return null;
                }
                return new r<T1>() {
                    public void a(c cVar, T1 t1) throws IOException {
                        rVar.a(cVar, t1);
                    }

                    public T1 b(com.a.a.d.a aVar) throws IOException {
                        T1 b2 = rVar.b(aVar);
                        if (b2 == null || a2.isInstance(b2)) {
                            return b2;
                        }
                        throw new p("Expected a " + a2.getName() + " but was " + b2.getClass().getName());
                    }
                };
            }

            public String toString() {
                return "Factory[typeHierarchy=" + cls.getName() + ",adapter=" + rVar + "]";
            }
        };
    }
}
