package com.a.a.b.a;

import com.a.a.c.a;
import com.a.a.d.b;
import com.a.a.d.c;
import com.a.a.e;
import com.a.a.p;
import com.a.a.r;
import com.a.a.s;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/* compiled from: SqlDateTypeAdapter */
public final class j extends r<Date> {

    /* renamed from: a  reason: collision with root package name */
    public static final s f623a = new s() {
        public <T> r<T> a(e eVar, a<T> aVar) {
            if (aVar.a() == Date.class) {
                return new j();
            }
            return null;
        }
    };

    /* renamed from: b  reason: collision with root package name */
    private final DateFormat f624b = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: a */
    public synchronized Date b(com.a.a.d.a aVar) throws IOException {
        Date date;
        if (aVar.f() == b.NULL) {
            aVar.j();
            date = null;
        } else {
            try {
                date = new Date(this.f624b.parse(aVar.h()).getTime());
            } catch (ParseException e) {
                throw new p(e);
            }
        }
        return date;
    }

    public synchronized void a(c cVar, Date date) throws IOException {
        cVar.b(date == null ? null : this.f624b.format(date));
    }
}
