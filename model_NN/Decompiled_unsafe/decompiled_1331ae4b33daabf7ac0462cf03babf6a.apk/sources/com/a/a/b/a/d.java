package com.a.a.b.a;

import com.a.a.a.b;
import com.a.a.b.c;
import com.a.a.c.a;
import com.a.a.e;
import com.a.a.r;
import com.a.a.s;

/* compiled from: JsonAdapterAnnotationTypeAdapterFactory */
public final class d implements s {

    /* renamed from: a  reason: collision with root package name */
    private final c f606a;

    public d(c cVar) {
        this.f606a = cVar;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [com.a.a.c.a, com.a.a.c.a<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> com.a.a.r<T> a(com.a.a.e r3, com.a.a.c.a<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.a()
            java.lang.Class<com.a.a.a.b> r1 = com.a.a.a.b.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            com.a.a.a.b r0 = (com.a.a.a.b) r0
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            com.a.a.b.c r1 = r2.f606a
            com.a.a.r r0 = a(r1, r3, r4, r0)
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.a.d.a(com.a.a.e, com.a.a.c.a):com.a.a.r");
    }

    static r<?> a(c cVar, e eVar, a<?> aVar, b bVar) {
        r<?> a2;
        Class<?> a3 = bVar.a();
        if (r.class.isAssignableFrom(a3)) {
            a2 = (r) cVar.a(a.b(a3)).a();
        } else if (s.class.isAssignableFrom(a3)) {
            a2 = ((s) cVar.a(a.b(a3)).a()).a(eVar, aVar);
        } else {
            throw new IllegalArgumentException("@JsonAdapter value must be TypeAdapter or TypeAdapterFactory reference.");
        }
        if (a2 != null) {
            return a2.a();
        }
        return a2;
    }
}
