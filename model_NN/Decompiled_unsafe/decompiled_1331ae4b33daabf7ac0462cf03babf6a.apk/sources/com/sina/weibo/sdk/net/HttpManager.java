package com.sina.weibo.sdk.net;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.webkit.URLUtil;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.exception.WeiboException;
import com.sina.weibo.sdk.exception.WeiboHttpException;
import com.sina.weibo.sdk.utils.LogUtil;
import com.sina.weibo.sdk.utils.NetworkHelper;
import com.sina.weibo.sdk.utils.Utility;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.ProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HttpContext;
import org.apache.mina.proxy.handlers.http.HttpProxyConstants;

public class HttpManager {
    private static final String BOUNDARY = getBoundry();
    private static final int BUFFER_SIZE = 8192;
    private static final int CONNECTION_TIMEOUT = 25000;
    private static final String END_MP_BOUNDARY = ("--" + BOUNDARY + "--");
    private static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_POST = "POST";
    private static final String MP_BOUNDARY = ("--" + BOUNDARY);
    private static final String MULTIPART_FORM_DATA = "multipart/form-data";
    private static final int SOCKET_TIMEOUT = 20000;
    private static final String TAG = "HttpManager";
    private static SSLSocketFactory sSSLSocketFactory;

    private static native String calcOauthSignNative(Context context, String str, String str2);

    static {
        System.loadLibrary("weibosdkcore");
    }

    public static String openUrl(Context context, String str, String str2, WeiboParameters weiboParameters) throws WeiboException {
        String readRsponse = readRsponse(requestHttpExecute(context, str, str2, weiboParameters));
        LogUtil.d(TAG, "Response : " + readRsponse);
        return readRsponse;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x007a A[SYNTHETIC, Splitter:B:19:0x007a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static org.apache.http.HttpResponse requestHttpExecute(android.content.Context r7, java.lang.String r8, java.lang.String r9, com.sina.weibo.sdk.net.WeiboParameters r10) {
        /*
            r1 = 0
            org.apache.http.client.HttpClient r3 = getNewHttpClient()     // Catch:{ IOException -> 0x0145, all -> 0x0141 }
            org.apache.http.params.HttpParams r0 = r3.getParams()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = "http.route.default-proxy"
            org.apache.http.HttpHost r4 = com.sina.weibo.sdk.net.NetStateManager.getAPN()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r0.setParameter(r2, r4)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            setHttpCommonParam(r7, r10)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r0 = "GET"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            if (r0 == 0) goto L_0x0081
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = "?"
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = r10.encodeUrl()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = r0.toString()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            org.apache.http.client.methods.HttpGet r0 = new org.apache.http.client.methods.HttpGet     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r4 = "HttpManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r6 = "requestHttpExecute GET Url : "
            r5.<init>(r6)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.StringBuilder r2 = r5.append(r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            com.sina.weibo.sdk.utils.LogUtil.d(r4, r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
        L_0x0051:
            org.apache.http.HttpResponse r0 = r3.execute(r0)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            int r2 = r2.getStatusCode()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 == r4) goto L_0x0133
            java.lang.String r0 = readRsponse(r0)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            com.sina.weibo.sdk.exception.WeiboHttpException r4 = new com.sina.weibo.sdk.exception.WeiboHttpException     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r4.<init>(r0, r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            throw r4     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
        L_0x006b:
            r0 = move-exception
            r2 = r3
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x0076 }
            com.sina.weibo.sdk.exception.WeiboException r3 = new com.sina.weibo.sdk.exception.WeiboException     // Catch:{ all -> 0x0076 }
            r3.<init>(r0)     // Catch:{ all -> 0x0076 }
            throw r3     // Catch:{ all -> 0x0076 }
        L_0x0076:
            r0 = move-exception
            r3 = r2
        L_0x0078:
            if (r1 == 0) goto L_0x007d
            r1.close()     // Catch:{ IOException -> 0x013c }
        L_0x007d:
            shutdownHttpClient(r3)
            throw r0
        L_0x0081:
            java.lang.String r0 = "POST"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            if (r0 == 0) goto L_0x0121
            java.lang.String r0 = "HttpManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r4 = "requestHttpExecute POST Url : "
            r2.<init>(r4)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.StringBuilder r2 = r2.append(r8)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            com.sina.weibo.sdk.utils.LogUtil.d(r0, r2)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r4.<init>(r8)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r2.<init>()     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            boolean r0 = r10.hasBinaryData()     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            if (r0 == 0) goto L_0x00d6
            java.lang.String r0 = "Content-Type"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r5 = "multipart/form-data; boundary="
            r1.<init>(r5)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r5 = com.sina.weibo.sdk.net.HttpManager.BOUNDARY     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.StringBuilder r1 = r1.append(r5)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r4.setHeader(r0, r1)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            buildParams(r2, r10)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
        L_0x00c6:
            org.apache.http.entity.ByteArrayEntity r0 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            byte[] r1 = r2.toByteArray()     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r0.<init>(r1)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r4.setEntity(r0)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r0 = r4
            r1 = r2
            goto L_0x0051
        L_0x00d6:
            java.lang.String r0 = "content-type"
            java.lang.Object r0 = r10.get(r0)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            if (r0 == 0) goto L_0x0115
            boolean r1 = r0 instanceof java.lang.String     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            if (r1 == 0) goto L_0x0115
            java.lang.String r1 = "content-type"
            r10.remove(r1)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r1 = "Content-Type"
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r4.setHeader(r1, r0)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
        L_0x00ee:
            java.lang.String r0 = r10.encodeUrl()     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r1 = "HttpManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r6 = "requestHttpExecute POST postParam : "
            r5.<init>(r6)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.StringBuilder r5 = r5.append(r0)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            com.sina.weibo.sdk.utils.LogUtil.d(r1, r5)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            java.lang.String r1 = "UTF-8"
            byte[] r0 = r0.getBytes(r1)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            r2.write(r0)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            goto L_0x00c6
        L_0x0110:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x006d
        L_0x0115:
            java.lang.String r0 = "Content-Type"
            java.lang.String r1 = "application/x-www-form-urlencoded"
            r4.setHeader(r0, r1)     // Catch:{ IOException -> 0x0110, all -> 0x011d }
            goto L_0x00ee
        L_0x011d:
            r0 = move-exception
            r1 = r2
            goto L_0x0078
        L_0x0121:
            java.lang.String r0 = "DELETE"
            boolean r0 = r9.equals(r0)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            if (r0 == 0) goto L_0x0149
            org.apache.http.client.methods.HttpDelete r0 = new org.apache.http.client.methods.HttpDelete     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            r0.<init>(r8)     // Catch:{ IOException -> 0x006b, all -> 0x0130 }
            goto L_0x0051
        L_0x0130:
            r0 = move-exception
            goto L_0x0078
        L_0x0133:
            if (r1 == 0) goto L_0x0138
            r1.close()     // Catch:{ IOException -> 0x013f }
        L_0x0138:
            shutdownHttpClient(r3)
            return r0
        L_0x013c:
            r1 = move-exception
            goto L_0x007d
        L_0x013f:
            r1 = move-exception
            goto L_0x0138
        L_0x0141:
            r0 = move-exception
            r3 = r1
            goto L_0x0078
        L_0x0145:
            r0 = move-exception
            r2 = r1
            goto L_0x006d
        L_0x0149:
            r0 = r1
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.net.HttpManager.requestHttpExecute(android.content.Context, java.lang.String, java.lang.String, com.sina.weibo.sdk.net.WeiboParameters):org.apache.http.HttpResponse");
    }

    private static void setHttpCommonParam(Context context, WeiboParameters weiboParameters) {
        String str;
        String str2 = "";
        if (!TextUtils.isEmpty(weiboParameters.getAppKey())) {
            str2 = Utility.getAid(context, weiboParameters.getAppKey());
            if (!TextUtils.isEmpty(str2)) {
                weiboParameters.put("aid", str2);
            }
        }
        String str3 = str2;
        String timestamp = getTimestamp();
        weiboParameters.put("oauth_timestamp", timestamp);
        Object obj = weiboParameters.get("access_token");
        Object obj2 = weiboParameters.get(Oauth2AccessToken.KEY_REFRESH_TOKEN);
        Object obj3 = weiboParameters.get("phone");
        if (obj != null && (obj instanceof String)) {
            str = (String) obj;
        } else if (obj2 != null && (obj2 instanceof String)) {
            str = (String) obj2;
        } else if (obj3 == null || !(obj3 instanceof String)) {
            str = "";
        } else {
            str = (String) obj3;
        }
        weiboParameters.put("oauth_sign", getOauthSign(context, str3, str, weiboParameters.getAppKey(), timestamp));
    }

    public static void shutdownHttpClient(HttpClient httpClient) {
        if (httpClient != null) {
            try {
                httpClient.getConnectionManager().closeExpiredConnections();
            } catch (Exception e) {
            }
        }
    }

    public static String openUrl4RdirectURL(Context context, String str, String str2, WeiboParameters weiboParameters) throws WeiboException {
        HttpGet httpGet = null;
        try {
            HttpGet httpGet2 = (DefaultHttpClient) getNewHttpClient();
            try {
                httpGet2.setRedirectHandler(new RedirectHandler() {
                    public boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
                        LogUtil.d(HttpManager.TAG, "openUrl4RdirectURL isRedirectRequested method");
                        return false;
                    }

                    public URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) throws ProtocolException {
                        LogUtil.d(HttpManager.TAG, "openUrl4RdirectURL getLocationURI method");
                        return null;
                    }
                });
                setHttpCommonParam(context, weiboParameters);
                httpGet2.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
                if (str2.equals("GET")) {
                    String str3 = String.valueOf(str) + "?" + weiboParameters.encodeUrl();
                    LogUtil.d(TAG, "openUrl4RdirectURL GET url : " + str3);
                    httpGet = new HttpGet(str3);
                } else if (str2.equals("POST")) {
                    httpGet = new HttpPost(str);
                    LogUtil.d(TAG, "openUrl4RdirectURL POST url : " + str);
                }
                HttpResponse execute = httpGet2.execute(httpGet);
                int statusCode = execute.getStatusLine().getStatusCode();
                if (statusCode == 301 || statusCode == 302) {
                    String value = execute.getFirstHeader("Location").getValue();
                    LogUtil.d(TAG, "RedirectURL = " + value);
                    shutdownHttpClient(httpGet2);
                    return value;
                } else if (statusCode == 200) {
                    String readRsponse = readRsponse(execute);
                    shutdownHttpClient(httpGet2);
                    return readRsponse;
                } else {
                    throw new WeiboHttpException(readRsponse(execute), statusCode);
                }
            } catch (IOException e) {
                IOException iOException = e;
                httpGet = httpGet2;
                e = iOException;
                try {
                    throw new WeiboException(e);
                } catch (Throwable th) {
                    th = th;
                    shutdownHttpClient(httpGet);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                httpGet = httpGet2;
                th = th3;
                shutdownHttpClient(httpGet);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            throw new WeiboException(e);
        }
    }

    public static String openRedirectUrl4LocationUri(Context context, String str, String str2, WeiboParameters weiboParameters) {
        HttpGet httpGet = null;
        try {
            AnonymousClass2 r2 = new CustomRedirectHandler() {
                public boolean shouldRedirectUrl(String str) {
                    return true;
                }

                public void onReceivedException() {
                }
            };
            HttpGet httpGet2 = (DefaultHttpClient) getNewHttpClient();
            try {
                httpGet2.setRedirectHandler(r2);
                setHttpCommonParam(context, weiboParameters);
                httpGet2.getParams().setParameter("http.route.default-proxy", NetStateManager.getAPN());
                if (str2.equals("GET")) {
                    httpGet = new HttpGet(String.valueOf(str) + "?" + weiboParameters.encodeUrl());
                } else if (str2.equals("POST")) {
                    httpGet = new HttpPost(str);
                }
                httpGet.setHeader("User-Agent", NetworkHelper.generateUA(context));
                httpGet2.execute(httpGet);
                String redirectUrl = r2.getRedirectUrl();
                shutdownHttpClient(httpGet2);
                return redirectUrl;
            } catch (IOException e) {
                IOException iOException = e;
                httpGet = httpGet2;
                e = iOException;
                try {
                    throw new WeiboException(e);
                } catch (Throwable th) {
                    th = th;
                    shutdownHttpClient(httpGet);
                    throw th;
                }
            } catch (Throwable th2) {
                Throwable th3 = th2;
                httpGet = httpGet2;
                th = th3;
                shutdownHttpClient(httpGet);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            throw new WeiboException(e);
        }
    }

    public static synchronized String downloadFile(Context context, String str, String str2, String str3) throws WeiboException {
        String str4;
        long j;
        long j2;
        InputStream content;
        synchronized (HttpManager.class) {
            File file = new File(str2);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(file, str3);
            if (file2.exists()) {
                str4 = file2.getPath();
            } else if (!URLUtil.isValidUrl(str)) {
                str4 = "";
            } else {
                HttpClient newHttpClient = getNewHttpClient();
                long j3 = 0;
                File file3 = new File(str2, String.valueOf(str3) + "_temp");
                try {
                    if (file3.exists()) {
                        j3 = file3.length();
                    } else {
                        file3.createNewFile();
                    }
                    HttpGet httpGet = new HttpGet(str);
                    httpGet.setHeader("RANGE", "bytes=" + j3 + "-");
                    HttpResponse execute = newHttpClient.execute(httpGet);
                    int statusCode = execute.getStatusLine().getStatusCode();
                    if (statusCode == 206) {
                        Header[] headers = execute.getHeaders("Content-Range");
                        if (!(headers == null || headers.length == 0)) {
                            String value = headers[0].getValue();
                            j = Long.parseLong(value.substring(value.indexOf(47) + 1));
                            j2 = j3;
                        }
                        j = 0;
                        j2 = j3;
                    } else if (statusCode == 200) {
                        j3 = 0;
                        Header firstHeader = execute.getFirstHeader("Content-Length");
                        if (firstHeader != null) {
                            j = (long) Integer.valueOf(firstHeader.getValue()).intValue();
                            j2 = 0;
                        }
                        j = 0;
                        j2 = j3;
                    } else {
                        throw new WeiboHttpException(readRsponse(execute), statusCode);
                    }
                    HttpEntity entity = execute.getEntity();
                    Header firstHeader2 = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader2 == null || firstHeader2.getValue().toLowerCase().indexOf("gzip") <= -1) {
                        content = entity.getContent();
                    } else {
                        content = new GZIPInputStream(entity.getContent());
                    }
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file3, "rw");
                    randomAccessFile.seek(j2);
                    byte[] bArr = new byte[1024];
                    while (true) {
                        int read = content.read(bArr);
                        if (read == -1) {
                            break;
                        }
                        randomAccessFile.write(bArr, 0, read);
                    }
                    randomAccessFile.close();
                    content.close();
                    if (j == 0 || file3.length() < j) {
                        file3.delete();
                        if (newHttpClient != null) {
                            newHttpClient.getConnectionManager().closeExpiredConnections();
                            newHttpClient.getConnectionManager().closeIdleConnections(300, TimeUnit.SECONDS);
                        }
                        str4 = "";
                    } else {
                        file3.renameTo(file2);
                        str4 = file2.getPath();
                        if (newHttpClient != null) {
                            newHttpClient.getConnectionManager().closeExpiredConnections();
                            newHttpClient.getConnectionManager().closeIdleConnections(300, TimeUnit.SECONDS);
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    file3.delete();
                    if (newHttpClient != null) {
                        newHttpClient.getConnectionManager().closeExpiredConnections();
                        newHttpClient.getConnectionManager().closeIdleConnections(300, TimeUnit.SECONDS);
                    }
                } catch (Throwable th) {
                    if (newHttpClient != null) {
                        newHttpClient.getConnectionManager().closeExpiredConnections();
                        newHttpClient.getConnectionManager().closeIdleConnections(300, TimeUnit.SECONDS);
                    }
                    throw th;
                }
            }
        }
        return str4;
    }

    public static HttpClient getNewHttpClient() {
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, "UTF-8");
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", getSSLSocketFactory(), 443));
            ThreadSafeClientConnManager threadSafeClientConnManager = new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(basicHttpParams, SOCKET_TIMEOUT);
            return new DefaultHttpClient(threadSafeClientConnManager, basicHttpParams);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    public static void buildParams(OutputStream outputStream, WeiboParameters weiboParameters) throws WeiboException {
        try {
            Set<String> keySet = weiboParameters.keySet();
            for (String next : keySet) {
                if (weiboParameters.get(next) instanceof String) {
                    StringBuilder sb = new StringBuilder(100);
                    sb.setLength(0);
                    sb.append(MP_BOUNDARY).append(HttpProxyConstants.CRLF);
                    sb.append("content-disposition: form-data; name=\"").append(next).append("\"\r\n\r\n");
                    sb.append(weiboParameters.get(next)).append(HttpProxyConstants.CRLF);
                    outputStream.write(sb.toString().getBytes());
                }
            }
            for (String next2 : keySet) {
                Object obj = weiboParameters.get(next2);
                if (obj instanceof Bitmap) {
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(MP_BOUNDARY).append(HttpProxyConstants.CRLF);
                    sb2.append("content-disposition: form-data; name=\"").append(next2).append("\"; filename=\"file\"\r\n");
                    sb2.append("Content-Type: application/octet-stream; charset=utf-8\r\n\r\n");
                    outputStream.write(sb2.toString().getBytes());
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    ((Bitmap) obj).compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    outputStream.write(byteArrayOutputStream.toByteArray());
                    outputStream.write(HttpProxyConstants.CRLF.getBytes());
                } else if (obj instanceof ByteArrayOutputStream) {
                    StringBuilder sb3 = new StringBuilder();
                    sb3.append(MP_BOUNDARY).append(HttpProxyConstants.CRLF);
                    sb3.append("content-disposition: form-data; name=\"").append(next2).append("\"; filename=\"file\"\r\n");
                    sb3.append("Content-Type: application/octet-stream; charset=utf-8\r\n\r\n");
                    outputStream.write(sb3.toString().getBytes());
                    ByteArrayOutputStream byteArrayOutputStream2 = (ByteArrayOutputStream) obj;
                    outputStream.write(byteArrayOutputStream2.toByteArray());
                    outputStream.write(HttpProxyConstants.CRLF.getBytes());
                    byteArrayOutputStream2.close();
                }
            }
            outputStream.write((HttpProxyConstants.CRLF + END_MP_BOUNDARY).getBytes());
        } catch (IOException e) {
            throw new WeiboException(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x0078 A[SYNTHETIC, Splitter:B:34:0x0078] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007d A[SYNTHETIC, Splitter:B:37:0x007d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readRsponse(org.apache.http.HttpResponse r7) throws com.sina.weibo.sdk.exception.WeiboException {
        /*
            r0 = 0
            r5 = -1
            if (r7 != 0) goto L_0x0005
        L_0x0004:
            return r0
        L_0x0005:
            org.apache.http.HttpEntity r1 = r7.getEntity()
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            r2.<init>()
            java.io.InputStream r1 = r1.getContent()     // Catch:{ IOException -> 0x0095, all -> 0x0090 }
            java.lang.String r0 = "Content-Encoding"
            org.apache.http.Header r0 = r7.getFirstHeader(r0)     // Catch:{ IOException -> 0x006e }
            if (r0 == 0) goto L_0x0030
            java.lang.String r0 = r0.getValue()     // Catch:{ IOException -> 0x006e }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ IOException -> 0x006e }
            java.lang.String r3 = "gzip"
            int r0 = r0.indexOf(r3)     // Catch:{ IOException -> 0x006e }
            if (r0 <= r5) goto L_0x0030
            java.util.zip.GZIPInputStream r0 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x006e }
            r0.<init>(r1)     // Catch:{ IOException -> 0x006e }
            r1 = r0
        L_0x0030:
            r0 = 8192(0x2000, float:1.14794E-41)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x006e }
        L_0x0034:
            int r3 = r1.read(r0)     // Catch:{ IOException -> 0x006e }
            if (r3 != r5) goto L_0x0069
            java.lang.String r0 = new java.lang.String     // Catch:{ IOException -> 0x006e }
            byte[] r3 = r2.toByteArray()     // Catch:{ IOException -> 0x006e }
            java.lang.String r4 = "UTF-8"
            r0.<init>(r3, r4)     // Catch:{ IOException -> 0x006e }
            java.lang.String r3 = "HttpManager"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x006e }
            java.lang.String r5 = "readRsponse result : "
            r4.<init>(r5)     // Catch:{ IOException -> 0x006e }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ IOException -> 0x006e }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x006e }
            com.sina.weibo.sdk.utils.LogUtil.d(r3, r4)     // Catch:{ IOException -> 0x006e }
            if (r1 == 0) goto L_0x005e
            r1.close()     // Catch:{ IOException -> 0x0081 }
        L_0x005e:
            if (r2 == 0) goto L_0x0004
            r2.close()     // Catch:{ IOException -> 0x0064 }
            goto L_0x0004
        L_0x0064:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0004
        L_0x0069:
            r4 = 0
            r2.write(r0, r4, r3)     // Catch:{ IOException -> 0x006e }
            goto L_0x0034
        L_0x006e:
            r0 = move-exception
        L_0x006f:
            com.sina.weibo.sdk.exception.WeiboException r3 = new com.sina.weibo.sdk.exception.WeiboException     // Catch:{ all -> 0x0075 }
            r3.<init>(r0)     // Catch:{ all -> 0x0075 }
            throw r3     // Catch:{ all -> 0x0075 }
        L_0x0075:
            r0 = move-exception
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.close()     // Catch:{ IOException -> 0x0086 }
        L_0x007b:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ IOException -> 0x008b }
        L_0x0080:
            throw r0
        L_0x0081:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x005e
        L_0x0086:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x007b
        L_0x008b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0080
        L_0x0090:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x0076
        L_0x0095:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.net.HttpManager.readRsponse(org.apache.http.HttpResponse):java.lang.String");
    }

    public static String getBoundry() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 1; i < 12; i++) {
            long currentTimeMillis = System.currentTimeMillis() + ((long) i);
            if (currentTimeMillis % 3 == 0) {
                stringBuffer.append(((char) ((int) currentTimeMillis)) % 9);
            } else if (currentTimeMillis % 3 == 1) {
                stringBuffer.append((char) ((int) ((currentTimeMillis % 26) + 65)));
            } else {
                stringBuffer.append((char) ((int) ((currentTimeMillis % 26) + 97)));
            }
        }
        return stringBuffer.toString();
    }

    private static SSLSocketFactory getSSLSocketFactory() {
        if (sSSLSocketFactory == null) {
            try {
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                instance.load(null, null);
                Certificate certificate = getCertificate("cacert_cn.cer");
                Certificate certificate2 = getCertificate("cacert_com.cer");
                instance.setCertificateEntry("cnca", certificate);
                instance.setCertificateEntry("comca", certificate2);
                sSSLSocketFactory = new SSLSocketFactoryEx(instance);
                LogUtil.d(TAG, "getSSLSocketFactory noraml !!!!!");
            } catch (Exception e) {
                e.printStackTrace();
                sSSLSocketFactory = SSLSocketFactory.getSocketFactory();
                LogUtil.d(TAG, "getSSLSocketFactory error default !!!!!");
            }
        }
        return sSSLSocketFactory;
    }

    private static Certificate getCertificate(String str) throws CertificateException, IOException {
        CertificateFactory instance = CertificateFactory.getInstance("X.509");
        InputStream resourceAsStream = HttpManager.class.getResourceAsStream(str);
        try {
            return instance.generateCertificate(resourceAsStream);
        } finally {
            if (resourceAsStream != null) {
                resourceAsStream.close();
            }
        }
    }

    private static String getTimestamp() {
        return String.valueOf(System.currentTimeMillis() / 1000);
    }

    private static String getOauthSign(Context context, String str, String str2, String str3, String str4) {
        StringBuilder sb = new StringBuilder("");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        if (!TextUtils.isEmpty(str2)) {
            sb.append(str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            sb.append(str3);
        }
        return calcOauthSignNative(context, sb.toString(), str4);
    }
}
