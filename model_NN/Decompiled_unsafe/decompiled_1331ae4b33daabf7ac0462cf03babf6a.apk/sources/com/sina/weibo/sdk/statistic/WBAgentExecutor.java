package com.sina.weibo.sdk.statistic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class WBAgentExecutor {
    private static long TIMEOUT = 5;
    private static ExecutorService mExecutor = Executors.newSingleThreadExecutor();

    WBAgentExecutor() {
    }

    public static synchronized void execute(Runnable runnable) {
        synchronized (WBAgentExecutor.class) {
            if (mExecutor.isShutdown()) {
                mExecutor = Executors.newSingleThreadExecutor();
            }
            mExecutor.execute(runnable);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void shutDownExecutor() {
        /*
            java.lang.Class<com.sina.weibo.sdk.statistic.WBAgentExecutor> r1 = com.sina.weibo.sdk.statistic.WBAgentExecutor.class
            monitor-enter(r1)
            java.util.concurrent.ExecutorService r0 = com.sina.weibo.sdk.statistic.WBAgentExecutor.mExecutor     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            boolean r0 = r0.isShutdown()     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            if (r0 != 0) goto L_0x0010
            java.util.concurrent.ExecutorService r0 = com.sina.weibo.sdk.statistic.WBAgentExecutor.mExecutor     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            r0.shutdown()     // Catch:{ Exception -> 0x001e, all -> 0x001b }
        L_0x0010:
            java.util.concurrent.ExecutorService r0 = com.sina.weibo.sdk.statistic.WBAgentExecutor.mExecutor     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            long r2 = com.sina.weibo.sdk.statistic.WBAgentExecutor.TIMEOUT     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ Exception -> 0x001e, all -> 0x001b }
            r0.awaitTermination(r2, r4)     // Catch:{ Exception -> 0x001e, all -> 0x001b }
        L_0x0019:
            monitor-exit(r1)
            return
        L_0x001b:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x001e:
            r0 = move-exception
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.sina.weibo.sdk.statistic.WBAgentExecutor.shutDownExecutor():void");
    }
}
