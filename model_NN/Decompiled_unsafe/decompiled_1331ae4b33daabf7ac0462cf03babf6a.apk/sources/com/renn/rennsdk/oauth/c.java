package com.renn.rennsdk.oauth;

/* compiled from: RRException */
public class c extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private int f1784a;

    /* renamed from: b  reason: collision with root package name */
    private String f1785b;
    private String c;

    public c(String str) {
        super(str);
        this.f1785b = str;
    }

    public String toString() {
        return "RRException [mExceptionCode=" + this.f1784a + ", mExceptionMsg=" + this.f1785b + ", mExceptionDescription=" + this.c + "]";
    }
}
