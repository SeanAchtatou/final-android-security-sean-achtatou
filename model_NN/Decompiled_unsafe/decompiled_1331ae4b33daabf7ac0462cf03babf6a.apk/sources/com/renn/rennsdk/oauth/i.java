package com.renn.rennsdk.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import com.renn.rennsdk.a;

/* compiled from: ValueStorage */
public class i {
    private static i c;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f1806a;

    /* renamed from: b  reason: collision with root package name */
    private SharedPreferences.Editor f1807b = this.f1806a.edit();

    public i(Context context) {
        this.f1806a = context.getSharedPreferences(context.getPackageName(), 0);
    }

    public static synchronized i a(Context context) {
        i iVar;
        synchronized (i.class) {
            if (c == null) {
                c = new i(context);
            }
            iVar = c;
        }
        return iVar;
    }

    public void a(String str, String str2) {
        this.f1807b.putString(str, str2);
        this.f1807b.commit();
    }

    public String a(String str) {
        return this.f1806a.getString(str, "");
    }

    public void a(String str, Long l) {
        this.f1807b.putLong(str, l.longValue());
        this.f1807b.commit();
    }

    public Long b(String str) {
        return Long.valueOf(this.f1806a.getLong(str, 0));
    }

    public void a(String str, a.C0028a aVar) {
        if (aVar == a.C0028a.Bearer) {
            this.f1807b.putInt(str, 0);
        } else if (aVar == a.C0028a.MAC) {
            this.f1807b.putInt(str, 1);
        }
        this.f1807b.commit();
    }

    public a.C0028a c(String str) {
        if (this.f1806a.getInt(str, 0) == 1) {
            return a.C0028a.MAC;
        }
        return a.C0028a.Bearer;
    }
}
