package com.renn.rennsdk.oauth;

import android.content.Context;

/* compiled from: ResourcesUtils */
public class g {
    public static int a(Context context, String str) {
        int identifier = context.getResources().getIdentifier(str, "string", context.getPackageName());
        if (identifier != 0) {
            return identifier;
        }
        try {
            throw new c("no such resources [" + str + "] was found.");
        } catch (c e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static int b(Context context, String str) {
        int identifier = context.getResources().getIdentifier(str, "drawable", context.getPackageName());
        if (identifier != 0) {
            return identifier;
        }
        try {
            throw new c("no such resources [" + str + "] was found.");
        } catch (c e) {
            e.printStackTrace();
            return -1;
        }
    }
}
