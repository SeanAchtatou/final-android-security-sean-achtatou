package com.jaeger.library;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class StatusBarView extends View {
    public StatusBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public StatusBarView(Context context) {
        super(context);
    }
}
