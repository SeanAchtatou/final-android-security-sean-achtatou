package com.pocketmusic.kshare.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import cn.a.a.a;
import eu.inmite.android.lib.dialogs.BaseDialogFragment;
import eu.inmite.android.lib.dialogs.b;
import eu.inmite.android.lib.dialogs.c;

public class MyDialogFragment extends BaseDialogFragment {

    /* renamed from: a  reason: collision with root package name */
    protected static String f1674a = "message";

    /* renamed from: b  reason: collision with root package name */
    protected static String f1675b = "title";
    protected static String c = "layout_id";
    protected static String d = "positive_button";
    protected static String e = "negative_button";
    protected int f;
    private c g;
    private b h;

    public static a a(Context context, FragmentManager fragmentManager) {
        return new a(context, fragmentManager, MyDialogFragment.class);
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (getTargetFragment() != null) {
            this.f = getTargetRequestCode();
            return;
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.f = arguments.getInt(eu.inmite.android.lib.dialogs.a.f4178a, 0);
        }
    }

    /* access modifiers changed from: protected */
    public BaseDialogFragment.a a(BaseDialogFragment.a aVar) {
        String c2 = c();
        if (!TextUtils.isEmpty(c2)) {
            aVar.a(c2);
        }
        int b2 = b();
        if (b2 > 0) {
            aVar.a(LayoutInflater.from(getActivity()).inflate(b2, (ViewGroup) null));
        }
        CharSequence a2 = a();
        if (!TextUtils.isEmpty(a2)) {
            aVar.b(a2);
        }
        String d2 = d();
        if (!TextUtils.isEmpty(d2)) {
            aVar.a(d2, new View.OnClickListener() {
                public void onClick(View view) {
                    c f = MyDialogFragment.this.f();
                    if (f != null) {
                        f.onPositiveButtonClicked(MyDialogFragment.this.f);
                    }
                    MyDialogFragment.this.dismiss();
                }
            });
        }
        String e2 = e();
        if (!TextUtils.isEmpty(e2)) {
            aVar.b(e2, new View.OnClickListener() {
                public void onClick(View view) {
                    c f = MyDialogFragment.this.f();
                    if (f != null) {
                        f.onNegativeButtonClicked(MyDialogFragment.this.f);
                    }
                    MyDialogFragment.this.dismiss();
                }
            });
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    public CharSequence a() {
        return getArguments().getCharSequence(f1674a);
    }

    /* access modifiers changed from: protected */
    public int b() {
        return getArguments().getInt(c);
    }

    /* access modifiers changed from: protected */
    public String c() {
        return getArguments().getString(f1675b);
    }

    /* access modifiers changed from: protected */
    public String d() {
        return getArguments().getString(d);
    }

    /* access modifiers changed from: protected */
    public String e() {
        return getArguments().getString(e);
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        b g2 = g();
        if (g2 != null) {
            g2.onCancelled(this.f);
        }
    }

    public void onStart() {
        super.onStart();
    }

    public void a(c cVar) {
        this.g = cVar;
    }

    public void a(b bVar) {
        this.h = bVar;
    }

    /* access modifiers changed from: protected */
    public c f() {
        if (this.g != null) {
            return this.g;
        }
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            if (targetFragment instanceof c) {
                return (c) targetFragment;
            }
        } else if (getParentFragment() != null && (getParentFragment() instanceof c)) {
            return (c) getParentFragment();
        } else {
            if (getActivity() instanceof c) {
                return (c) getActivity();
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public b g() {
        if (this.h != null) {
            return this.h;
        }
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null) {
            if (targetFragment instanceof b) {
                return (b) targetFragment;
            }
        } else if (getParentFragment() != null && (getParentFragment() instanceof b)) {
            return (b) getParentFragment();
        } else {
            if (getActivity() instanceof b) {
                return (b) getActivity();
            }
        }
        return null;
    }

    public static class a extends eu.inmite.android.lib.dialogs.a<a> {
        private String h;
        private CharSequence i;
        private String j;
        private String k;
        private boolean l = true;
        private int m = -1;

        protected a(Context context, FragmentManager fragmentManager, Class<? extends MyDialogFragment> cls) {
            super(context, fragmentManager, cls);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public a c() {
            return this;
        }

        public a a(int i2) {
            this.h = this.e.getString(i2);
            return this;
        }

        public a a(String str) {
            this.h = str;
            return this;
        }

        public a b(int i2) {
            this.i = this.e.getText(i2);
            return this;
        }

        public a a(CharSequence charSequence) {
            this.i = charSequence;
            return this;
        }

        public a c(int i2) {
            this.m = i2;
            return this;
        }

        public a d(int i2) {
            this.j = this.e.getString(i2);
            return this;
        }

        public a b(String str) {
            this.j = str;
            return this;
        }

        public a e(int i2) {
            this.k = this.e.getString(i2);
            return this;
        }

        public a c(String str) {
            this.k = str;
            return this;
        }

        /* access modifiers changed from: protected */
        public Bundle b() {
            if (this.l && this.j == null && this.k == null) {
                this.j = this.e.getString(a.h.dialog_close);
            }
            Bundle bundle = new Bundle();
            bundle.putCharSequence(MyDialogFragment.f1674a, this.i);
            bundle.putString(MyDialogFragment.f1675b, this.h);
            bundle.putString(MyDialogFragment.d, this.j);
            bundle.putString(MyDialogFragment.e, this.k);
            bundle.putInt(MyDialogFragment.c, this.m);
            return bundle;
        }
    }
}
