package com.pocketmusic.kshare.requestobjs;

import android.content.Context;
import java.util.HashMap;

/* compiled from: NotifyConfig */
public class k extends n {

    /* renamed from: a  reason: collision with root package name */
    public static HashMap<String, a> f1724a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    public static a f1725b = new a();
    public static boolean c = false;

    /* compiled from: NotifyConfig */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f1726a = "爱唱-音质最好伴奏最全的K歌神器";

        /* renamed from: b  reason: collision with root package name */
        public String f1727b = "更多精彩好玩内容，快免费安装最新爱唱";
        public String c = "立即安装";
        public String d = "以后再说";
    }

    /* compiled from: NotifyConfig */
    public enum b {
        ROOM_CHAT("room_chat"),
        ROOM_GIFT("room_gift"),
        ROOM_SING("room_sing"),
        ROOM_MICS("room_mics"),
        ROOM_INVITE("room_invite"),
        ROOM_HEAD("room_head"),
        PLAYER_REPLY("player_reply"),
        PLAYER_GIFT("player_gift"),
        PLAYER_SHARE("player_share"),
        PLAYER_SING("player_sing"),
        PLAYER_FIX("player_fix"),
        PLAYER_HEAD("player_head"),
        DEFAULT("default");
        
        private static /* synthetic */ int[] o;
        private String n = "default";

        private b(String str) {
            this.n = str;
        }

        public String a() {
            return this.n;
        }

        public static String a(b bVar) {
            if (bVar == null) {
                return "爱唱-音质最好伴奏最全的K歌神器";
            }
            switch (b()[bVar.ordinal()]) {
                case 1:
                    return "与主播聊天亲密互动，快免费安装爱唱！";
                case 2:
                    return "领取礼物送给TA，快免费安装爱唱！";
                case 3:
                    return "排麦唱歌展才华，快免费安装爱唱！";
                case 4:
                    return "看谁在这个房间里，快免费安装爱唱！";
                case 5:
                    return "免费安装爱唱，邀请好友一起唱！";
                case 6:
                    return "关注主播每天唱什么，快免费安装爱唱！";
                case 7:
                    return "免费安装爱唱与TA聊天互动";
                case 8:
                    return "领取礼物送给TA，快免费安装爱唱!";
                case 9:
                    return "分享歌曲给好友，快免费安装爱唱";
                case 10:
                    return "免费安装新爱唱，比比谁是K歌王!";
                case 11:
                    return "免费安装爱唱，随时收听TA的歌声";
                case 12:
                    return "免费安装爱唱，随时收听TA的歌声";
                default:
                    return "爱唱-音质最好伴奏最全的K歌神器";
            }
        }
    }

    public static a a(Context context, b bVar) {
        a aVar = f1724a.get(bVar.a());
        if (aVar != null) {
            return aVar;
        }
        a aVar2 = new a();
        aVar2.f1727b = b.a(bVar);
        f1724a.put(bVar.a(), aVar2);
        return aVar2;
    }
}
