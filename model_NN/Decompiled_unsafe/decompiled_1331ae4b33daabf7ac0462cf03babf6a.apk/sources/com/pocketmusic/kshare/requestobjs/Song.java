package com.pocketmusic.kshare.requestobjs;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import cn.banshenggua.aichang.utils.CommonUtil;
import java.io.Serializable;
import java.util.UUID;

public class Song extends n implements Parcelable, Serializable, Cloneable {
    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        /* renamed from: a */
        public Song createFromParcel(Parcel parcel) {
            Song song = new Song();
            song.f1679b = parcel.readString();
            song.c = parcel.readString();
            song.d = parcel.readString();
            song.f = parcel.readString();
            song.g = parcel.readString();
            song.h = parcel.readString();
            song.i = parcel.readString();
            song.j = parcel.readString();
            song.k = parcel.readString();
            song.l = parcel.readString();
            song.n = parcel.readString();
            song.o = parcel.readString();
            song.u = parcel.readString();
            song.v = parcel.readString();
            song.w = parcel.readString();
            if (parcel.readInt() == 0) {
                song.m = false;
            } else {
                song.m = true;
            }
            song.q = parcel.readString();
            song.r = parcel.readString();
            if (parcel.readInt() == 0) {
                song.p = false;
            } else {
                song.p = true;
            }
            int readInt = parcel.readInt();
            int readInt2 = parcel.readInt();
            int readInt3 = parcel.readInt();
            int readInt4 = parcel.readInt();
            int readInt5 = parcel.readInt();
            if (readInt == 0) {
                song.B = false;
            } else {
                song.B = true;
            }
            if (readInt2 == 0) {
                song.C = false;
            } else {
                song.C = true;
            }
            if (readInt3 == 0) {
                song.D = false;
            } else {
                song.D = true;
            }
            if (readInt4 == 0) {
                song.E = false;
            } else {
                song.E = true;
            }
            if (readInt5 == 0) {
                song.I = false;
            } else {
                song.I = true;
            }
            song.z = parcel.readString();
            if (parcel.readInt() == 0) {
                song.x = false;
            } else {
                song.x = true;
            }
            song.J = parcel.readLong();
            int readInt6 = parcel.readInt();
            int readInt7 = parcel.readInt();
            int readInt8 = parcel.readInt();
            int readInt9 = parcel.readInt();
            if (readInt6 == 0) {
                song.F = false;
            } else {
                song.F = true;
            }
            if (readInt7 == 0) {
                song.G = false;
            } else {
                song.G = true;
            }
            if (readInt8 == 0) {
                song.H = false;
            } else {
                song.H = true;
            }
            if (readInt9 == 0) {
                song.N = false;
            } else {
                song.N = true;
            }
            song.O = parcel.readString();
            song.s = parcel.readString();
            song.R = parcel.readLong();
            return song;
        }

        /* renamed from: a */
        public Song[] newArray(int i) {
            return new Song[i];
        }
    };
    public v A = null;
    public boolean B = false;
    public boolean C = false;
    public boolean D = false;
    public boolean E = false;
    public boolean F = false;
    public boolean G = false;
    public boolean H = false;
    public boolean I = false;
    public long J = 0;
    public boolean K = false;
    public int L = 0;
    public int M = 1;
    public boolean N = false;
    public String O = "0";
    public String P = UUID.randomUUID().toString();
    public String Q = "";
    public long R = 0;
    public a S = a.Refresh;
    public b T = b.Default;
    private String U = null;
    private String V = null;

    /* renamed from: a  reason: collision with root package name */
    public String f1678a = (String.valueOf(CommonUtil.getKshareRootPath()) + "/recording.mp4");

    /* renamed from: b  reason: collision with root package name */
    public String f1679b = null;
    public String c = null;
    public String d = null;
    public String e = "";
    public String f = null;
    public String g = null;
    public String h = null;
    public String i = null;
    public String j = null;
    public String k = null;
    public String l = null;
    public boolean m = false;
    public String n = null;
    public String o = null;
    public boolean p = false;
    public String q = null;
    public String r = null;
    public String s = null;
    public boolean t = false;
    public String u = null;
    public String v = null;
    public String w = null;
    public boolean x = false;
    public boolean y = false;
    public String z = null;

    public enum a {
        Refresh,
        Download,
        Upload
    }

    public enum b {
        Default(0),
        Loading(1),
        Downloaded(2),
        Uploaded(3),
        Paused(4),
        Mixed(5),
        DownloadError(6);
        
        public static b[] h = {Default, Loading, Downloaded, Uploaded, Paused, Mixed, DownloadError};
        private int i = 0;

        private b(int i2) {
            this.i = i2;
        }
    }

    public Object clone() throws CloneNotSupportedException {
        Song song = (Song) super.clone();
        song.P = UUID.randomUUID().toString();
        song.f = song.P;
        return song;
    }

    public String toString() {
        return "uid=" + this.d + " name=" + this.l + "; local: " + this.N;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13 = 1;
        parcel.writeString(this.f1679b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.i);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeString(this.l);
        parcel.writeString(this.n);
        parcel.writeString(this.o);
        parcel.writeString(this.u);
        parcel.writeString(this.v);
        parcel.writeString(this.w);
        parcel.writeInt(this.m ? 1 : 0);
        parcel.writeString(this.q);
        parcel.writeString(this.r);
        if (this.p) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        parcel.writeInt(i3);
        if (this.B) {
            i4 = 1;
        } else {
            i4 = 0;
        }
        if (this.C) {
            i5 = 1;
        } else {
            i5 = 0;
        }
        if (this.D) {
            i6 = 1;
        } else {
            i6 = 0;
        }
        if (this.E) {
            i7 = 1;
        } else {
            i7 = 0;
        }
        if (this.I) {
            i8 = 1;
        } else {
            i8 = 0;
        }
        parcel.writeInt(i4);
        parcel.writeInt(i5);
        parcel.writeInt(i6);
        parcel.writeInt(i7);
        parcel.writeInt(i8);
        parcel.writeString(this.z);
        if (this.x) {
            i9 = 1;
        } else {
            i9 = 0;
        }
        parcel.writeInt(i9);
        parcel.writeLong(this.J);
        if (this.F) {
            i10 = 1;
        } else {
            i10 = 0;
        }
        if (this.G) {
            i11 = 1;
        } else {
            i11 = 0;
        }
        if (this.H) {
            i12 = 1;
        } else {
            i12 = 0;
        }
        if (!this.N) {
            i13 = 0;
        }
        parcel.writeInt(i10);
        parcel.writeInt(i11);
        parcel.writeInt(i12);
        parcel.writeInt(i13);
        parcel.writeString(this.O);
        if (TextUtils.isEmpty(this.s)) {
            this.s = "a";
        }
        parcel.writeString(this.s);
        parcel.writeLong(this.R);
    }
}
