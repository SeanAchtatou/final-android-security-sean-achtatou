package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.utils.Toaster;
import com.android.volley.a;
import com.android.volley.j;
import com.android.volley.k;
import com.android.volley.n;
import com.android.volley.q;
import com.android.volley.s;
import java.io.Serializable;
import org.json.JSONObject;

/* compiled from: SimpleRequestListener */
public class r implements n.a, n.b<JSONObject>, Serializable {
    private static final long serialVersionUID = 14807546712L;

    public void onErrorResponse(s sVar) {
        try {
            if (sVar instanceof j) {
                Toaster.showLongToast("手机未连接到网络");
            } else if (sVar instanceof q) {
                Toaster.showLongToast("服务器内部错误，错误码:" + sVar.f751a.f737a);
            } else if (!(sVar instanceof a) && !(sVar instanceof k)) {
                if (sVar instanceof com.android.volley.r) {
                    Toaster.showLongToast("网路连接超时，请稍后再试");
                } else {
                    Toaster.showLongToast(sVar.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResponse(JSONObject jSONObject) {
    }
}
