package com.pocketmusic.kshare.requestobjs;

import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import java.util.HashMap;
import org.json.JSONObject;

/* compiled from: LibCheck */
public class i extends n {

    /* renamed from: a  reason: collision with root package name */
    public String f1720a;

    /* renamed from: b  reason: collision with root package name */
    public long f1721b;
    public boolean c = false;
    public long d;

    public void a() {
        KURL kurl = new KURL();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        a(kurl, this.ar, APIKey.APIKey_LibCheck);
    }

    public void a(String str, String str2) {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        hashMap.put("cmd", "notifyclick");
        hashMap.put("type", str);
        hashMap.put("click", str2);
        kurl.getParameter.putAll(hashMap);
        a(kurl, null, APIKey.APIKey_LibCheck);
    }

    public void a(String str) {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        hashMap.put("cmd", "callinstall");
        hashMap.put("step", str);
        kurl.getParameter.putAll(hashMap);
        a(kurl, null, APIKey.APIKey_LibCheck);
    }

    public void b(String str, String str2) {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        hashMap.put("cmd", "callinstall");
        hashMap.put("step", str);
        hashMap.put("apkinfo", str2);
        kurl.getParameter.putAll(hashMap);
        a(kurl, null, APIKey.APIKey_LibCheck);
    }

    public void b(String str) {
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        kurl.baseURL = s.a(APIKey.APIKey_LibCheck);
        hashMap.put("cmd", SocketMessage.MSG_ERROR_KEY);
        hashMap.put("log", str);
        kurl.getParameter.putAll(hashMap);
        a(kurl, null, APIKey.APIKey_LibCheck);
    }

    public void a(JSONObject jSONObject) {
        boolean z = false;
        if (jSONObject != null) {
            this.f1720a = jSONObject.optString("file");
            this.f1721b = jSONObject.optLong("size", 0);
            this.d = jSONObject.optLong("osize", 0);
            if (jSONObject.optInt("zip", 0) != 0) {
                z = true;
            }
            this.c = z;
        }
    }
}
