package com.pocketmusic.kshare.requestobjs;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.utils.ULog;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: GiftList */
public class f extends n implements Serializable {
    private static /* synthetic */ int[] p;

    /* renamed from: a  reason: collision with root package name */
    public int f1708a = 0;

    /* renamed from: b  reason: collision with root package name */
    public int f1709b = 0;
    public int c = 1;
    public int d = 0;
    public int e = 20;
    public String f = null;
    public String g = null;
    public boolean h = true;
    public String i = "";
    public List<e> j = new ArrayList();
    public String k = "";
    public String l = "";
    public String m = "";
    public String n = "";
    public a o = a.GiftList;

    /* compiled from: GiftList */
    public enum a {
        GiftList,
        UserGiftList,
        UserGiftListByUser,
        UserGiftListGroup,
        FanChangGiftTopUser,
        RoomVip,
        RoomGiftSpendTop
    }

    static /* synthetic */ int[] h() {
        int[] iArr = p;
        if (iArr == null) {
            iArr = new int[a.values().length];
            try {
                iArr[a.FanChangGiftTopUser.ordinal()] = 5;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[a.GiftList.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[a.RoomGiftSpendTop.ordinal()] = 7;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[a.RoomVip.ordinal()] = 6;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[a.UserGiftList.ordinal()] = 2;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[a.UserGiftListByUser.ordinal()] = 3;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[a.UserGiftListGroup.ordinal()] = 4;
            } catch (NoSuchFieldError e8) {
            }
            p = iArr;
        }
        return iArr;
    }

    public f(a aVar) {
        this.o = aVar;
    }

    public f(a aVar, String str) {
        this.o = aVar;
        this.l = str;
    }

    public void a() {
        e();
        KURL kurl = new KURL();
        HashMap hashMap = new HashMap();
        switch (h()[this.o.ordinal()]) {
            case 1:
                kurl.baseURL = s.a(APIKey.APIKey_GiftList);
                hashMap.put("cmd", "giftlist");
                hashMap.put(WBPageConstants.ParamKey.PAGE, String.valueOf(this.c));
                hashMap.put(SocketMessage.MSG_FINGER_KEY, String.valueOf(this.k));
                if (!TextUtils.isEmpty(this.l)) {
                    hashMap.put("rid", this.l);
                }
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_GiftList);
                return;
            case 2:
            case 3:
            case 4:
            case 6:
            default:
                return;
            case 5:
                kurl.baseURL = s.a(APIKey.APIKey_Get_Super_Gift_Fans);
                hashMap.put("cmd", "fctopgift");
                if (!TextUtils.isEmpty(this.i)) {
                    hashMap.put("uid", this.i);
                }
                if (!TextUtils.isEmpty(this.m)) {
                    hashMap.put("fcid", this.m);
                }
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_Get_Super_Gift_Fans);
                return;
            case 7:
                kurl.baseURL = s.a(APIKey.APIKey_RoomGiftSpendTop);
                hashMap.put("rid", this.n);
                hashMap.put(WBPageConstants.ParamKey.PAGE, String.valueOf(this.c));
                kurl.getParameter.putAll(hashMap);
                a(kurl, this.ar, APIKey.APIKey_RoomGiftSpendTop);
                return;
        }
    }

    public void b() {
        this.h = true;
        a();
    }

    public void c() {
        this.h = false;
        this.c = 1;
        a();
    }

    public boolean d() {
        ULog.d("luolei", "total: " + this.f1708a + "; limit: " + this.e + "; page: " + this.f1709b + "; nextPage: " + this.c);
        if (this.f1708a <= this.e || this.f1709b >= this.c) {
            return false;
        }
        return true;
    }

    public void a(e eVar) {
        this.j.add(eVar);
    }

    public void e() {
        this.j.clear();
    }

    public List<e> f() {
        return this.j;
    }

    public e a(int i2) {
        return this.j.get(i2);
    }

    public int g() {
        return this.j.size();
    }

    public void a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        if (jSONObject != null && !b(jSONObject) && (optJSONObject = jSONObject.optJSONObject(SocketMessage.MSG_RESULE_KEY)) != null && optJSONObject.length() != 0 && optJSONObject != null) {
            JSONArray optJSONArray = optJSONObject.optJSONArray("gifts");
            this.c = optJSONObject.optInt("page_next");
            this.f1709b = optJSONObject.optInt(WBPageConstants.ParamKey.PAGE);
            this.d = optJSONObject.optInt("page_count");
            this.k = optJSONObject.optString(SocketMessage.MSG_FINGER_KEY);
            this.e = optJSONObject.optInt("limit", this.e);
            if (this.h) {
                this.f1708a = optJSONObject.optInt("total");
            } else if (this.f1708a > 0) {
                this.f1708a = this.e + 1;
            } else {
                this.f1708a = optJSONObject.optInt("total");
            }
            if (optJSONArray != null) {
                try {
                    if (optJSONArray.length() > 0) {
                        for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                            if (!(optJSONArray.get(i2) == null || optJSONArray.get(i2) == JSONObject.NULL)) {
                                e eVar = new e();
                                eVar.a(optJSONArray.getJSONObject(i2));
                                a(eVar);
                            }
                        }
                    }
                } catch (JSONException e2) {
                }
            }
        }
    }
}
