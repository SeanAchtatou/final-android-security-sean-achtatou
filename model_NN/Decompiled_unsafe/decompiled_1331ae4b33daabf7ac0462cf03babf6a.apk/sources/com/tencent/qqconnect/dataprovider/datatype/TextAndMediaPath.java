package com.tencent.qqconnect.dataprovider.datatype;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class TextAndMediaPath implements Parcelable {
    public static final Parcelable.Creator<TextAndMediaPath> CREATOR = new Parcelable.Creator<TextAndMediaPath>() {
        public TextAndMediaPath createFromParcel(Parcel parcel) {
            return new TextAndMediaPath(parcel);
        }

        public TextAndMediaPath[] newArray(int i) {
            return new TextAndMediaPath[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private String f3601a;

    /* renamed from: b  reason: collision with root package name */
    private String f3602b;

    public TextAndMediaPath(String str, String str2) {
        this.f3601a = str;
        this.f3602b = str2;
    }

    public String getText() {
        return this.f3601a;
    }

    public String getMediaPath() {
        return this.f3602b;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3601a);
        parcel.writeString(this.f3602b);
    }

    private TextAndMediaPath(Parcel parcel) {
        this.f3601a = parcel.readString();
        this.f3602b = parcel.readString();
    }
}
