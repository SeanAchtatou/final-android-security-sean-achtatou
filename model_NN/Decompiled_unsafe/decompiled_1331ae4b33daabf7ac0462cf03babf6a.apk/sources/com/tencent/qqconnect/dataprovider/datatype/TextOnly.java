package com.tencent.qqconnect.dataprovider.datatype;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class TextOnly implements Parcelable {
    public static final Parcelable.Creator<TextOnly> CREATOR = new Parcelable.Creator<TextOnly>() {
        public TextOnly createFromParcel(Parcel parcel) {
            return new TextOnly(parcel);
        }

        public TextOnly[] newArray(int i) {
            return new TextOnly[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private String f3603a;

    public TextOnly(String str) {
        this.f3603a = str;
    }

    public String getText() {
        return this.f3603a;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3603a);
    }

    private TextOnly(Parcel parcel) {
        this.f3603a = parcel.readString();
    }
}
