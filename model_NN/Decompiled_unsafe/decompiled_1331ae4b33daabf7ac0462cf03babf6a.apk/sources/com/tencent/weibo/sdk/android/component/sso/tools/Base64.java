package com.tencent.weibo.sdk.android.component.sso.tools;

public class Base64 {
    public static String encode(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < bArr.length; i += 3) {
            stringBuffer.append(encodeBlock(bArr, i));
        }
        return stringBuffer.toString();
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:21:0x002a */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:20:0x002a */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v6, resolved type: int} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=byte, code=int, for r3v2, types: [int, byte] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static char[] encodeBlock(byte[] r10, int r11) {
        /*
            r9 = 61
            r8 = 4
            r4 = 0
            r1 = 2
            int r0 = r10.length
            int r0 = r0 - r11
            int r2 = r0 + -1
            if (r2 < r1) goto L_0x0020
            r0 = r1
        L_0x000c:
            r5 = r4
            r6 = r4
        L_0x000e:
            if (r5 <= r0) goto L_0x0022
            char[] r3 = new char[r8]
            r0 = r4
        L_0x0013:
            if (r0 < r8) goto L_0x0034
            r0 = 1
            if (r2 >= r0) goto L_0x001a
            r3[r1] = r9
        L_0x001a:
            if (r2 >= r1) goto L_0x001f
            r0 = 3
            r3[r0] = r9
        L_0x001f:
            return r3
        L_0x0020:
            r0 = r2
            goto L_0x000c
        L_0x0022:
            int r3 = r11 + r5
            byte r3 = r10[r3]
            if (r3 >= 0) goto L_0x002a
            int r3 = r3 + 256
        L_0x002a:
            int r7 = 2 - r5
            int r7 = r7 * 8
            int r3 = r3 << r7
            int r6 = r6 + r3
            int r3 = r5 + 1
            r5 = r3
            goto L_0x000e
        L_0x0034:
            int r4 = 3 - r0
            int r4 = r4 * 6
            int r4 = r6 >>> r4
            r4 = r4 & 63
            char r4 = getChar(r4)
            r3[r0] = r4
            int r0 = r0 + 1
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.weibo.sdk.android.component.sso.tools.Base64.encodeBlock(byte[], int):char[]");
    }

    protected static char getChar(int i) {
        if (i >= 0 && i <= 25) {
            return (char) (i + 65);
        }
        if (i >= 26 && i <= 51) {
            return (char) ((i - 26) + 97);
        }
        if (i >= 52 && i <= 61) {
            return (char) ((i - 52) + 48);
        }
        if (i == 62) {
            return '+';
        }
        if (i == 63) {
            return '/';
        }
        return '?';
    }

    public static byte[] decode(String str) {
        int i = 0;
        for (int length = str.length() - 1; str.charAt(length) == '='; length--) {
            i++;
        }
        byte[] bArr = new byte[(((str.length() * 6) / 8) - i)];
        int i2 = 0;
        for (int i3 = 0; i3 < str.length(); i3 += 4) {
            int value = getValue(str.charAt(i3 + 3)) + (getValue(str.charAt(i3)) << 18) + (getValue(str.charAt(i3 + 1)) << 12) + (getValue(str.charAt(i3 + 2)) << 6);
            int i4 = 0;
            while (i4 < 3 && i2 + i4 < bArr.length) {
                bArr[i2 + i4] = (byte) ((value >> ((2 - i4) * 8)) & 255);
                i4++;
            }
            i2 += 3;
        }
        return bArr;
    }

    protected static int getValue(char c) {
        if (c >= 'A' && c <= 'Z') {
            return c - 'A';
        }
        if (c >= 'a' && c <= 'z') {
            return (c - 'a') + 26;
        }
        if (c >= '0' && c <= '9') {
            return (c - '0') + 52;
        }
        if (c == '+') {
            return 62;
        }
        if (c == '/') {
            return 63;
        }
        if (c == '=') {
            return 0;
        }
        return -1;
    }
}
