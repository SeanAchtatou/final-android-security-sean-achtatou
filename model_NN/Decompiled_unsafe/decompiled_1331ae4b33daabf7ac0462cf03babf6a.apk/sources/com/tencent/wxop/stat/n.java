package com.tencent.wxop.stat;

import com.tencent.wxop.stat.a.c;
import java.lang.Thread;

final class n implements Thread.UncaughtExceptionHandler {
    n() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int
     arg types: [android.content.Context, int, ?[OBJECT, ARRAY]]
     candidates:
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, com.tencent.wxop.stat.f):void
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int */
    public final void uncaughtException(Thread thread, Throwable th) {
        if (c.l() && e.aY != null) {
            if (c.x()) {
                t.s(e.aY).b(new c(e.aY, e.a(e.aY, false, (f) null), th, thread), null, false, true);
                e.aV.debug("MTA has caught the following uncaught exception:");
                e.aV.a(th);
            }
            e.p(e.aY);
            if (e.aW != null) {
                e.aV.e("Call the original uncaught exception handler.");
                if (!(e.aW instanceof n)) {
                    e.aW.uncaughtException(thread, th);
                }
            }
        }
    }
}
