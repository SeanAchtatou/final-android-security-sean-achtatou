package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.g;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;

final class o implements Runnable {
    private f bM = null;
    private Map<String, Integer> bO = null;
    private Context e = null;

    public o(Context context) {
        this.e = context;
        this.bM = null;
    }

    private static b a(String str, int i) {
        b bVar = new b();
        Socket socket = new Socket();
        int i2 = 0;
        try {
            bVar.setDomain(str);
            bVar.setPort(i);
            long currentTimeMillis = System.currentTimeMillis();
            InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
            socket.connect(inetSocketAddress, 30000);
            bVar.a(System.currentTimeMillis() - currentTimeMillis);
            bVar.k(inetSocketAddress.getAddress().getHostAddress());
            socket.close();
            try {
                socket.close();
            } catch (Throwable th) {
                e.aV.b(th);
            }
        } catch (IOException e2) {
            IOException iOException = e2;
            i2 = -1;
            e.aV.b((Throwable) iOException);
            socket.close();
        } catch (Throwable th2) {
            e.aV.b(th2);
        }
        bVar.setStatusCode(i2);
        return bVar;
    }

    private static Map<String, Integer> ag() {
        String str;
        HashMap hashMap = new HashMap();
        String l = c.l("__MTA_TEST_SPEED__");
        if (!(l == null || l.trim().length() == 0)) {
            for (String split : l.split(";")) {
                String[] split2 = split.split(MiPushClient.ACCEPT_TIME_SEPARATOR);
                if (!(split2 == null || split2.length != 2 || (str = split2[0]) == null || str.trim().length() == 0)) {
                    try {
                        hashMap.put(str, Integer.valueOf(Integer.valueOf(split2[1]).intValue()));
                    } catch (NumberFormatException e2) {
                        e.aV.b((Throwable) e2);
                    }
                }
            }
        }
        return hashMap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int
     arg types: [android.content.Context, int, com.tencent.wxop.stat.f]
     candidates:
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, com.tencent.wxop.stat.f):void
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int */
    public final void run() {
        try {
            if (this.bO == null) {
                this.bO = ag();
            }
            if (this.bO == null || this.bO.size() == 0) {
                e.aV.b("empty domain list.");
                return;
            }
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry next : this.bO.entrySet()) {
                String str = (String) next.getKey();
                if (str == null || str.length() == 0) {
                    e.aV.c("empty domain name.");
                } else if (((Integer) next.getValue()) == null) {
                    e.aV.c("port is null for " + str);
                } else {
                    jSONArray.put(a((String) next.getKey(), ((Integer) next.getValue()).intValue()).i());
                }
            }
            if (jSONArray.length() != 0) {
                g gVar = new g(this.e, e.a(this.e, false, this.bM), this.bM);
                gVar.b(jSONArray.toString());
                new p(gVar).ah();
            }
        } catch (Throwable th) {
            e.aV.b(th);
        }
    }
}
