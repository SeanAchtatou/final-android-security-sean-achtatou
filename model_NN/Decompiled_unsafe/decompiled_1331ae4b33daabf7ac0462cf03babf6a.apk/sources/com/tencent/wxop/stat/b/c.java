package com.tencent.wxop.stat.b;

import com.tencent.stat.DeviceInfo;
import org.json.JSONException;
import org.json.JSONObject;

public final class c {
    private String W = "0";

    /* renamed from: a  reason: collision with root package name */
    private String f3698a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f3699b = null;
    private int bf = 0;
    private String c = null;
    private int cu;
    private long cv = 0;

    public c() {
    }

    public c(String str, String str2, int i) {
        this.f3698a = str;
        this.f3699b = str2;
        this.cu = i;
    }

    private JSONObject aq() {
        JSONObject jSONObject = new JSONObject();
        try {
            r.a(jSONObject, DeviceInfo.TAG_IMEI, this.f3698a);
            r.a(jSONObject, DeviceInfo.TAG_MAC, this.f3699b);
            r.a(jSONObject, DeviceInfo.TAG_MID, this.W);
            r.a(jSONObject, "aid", this.c);
            jSONObject.put("ts", this.cv);
            jSONObject.put(DeviceInfo.TAG_VERSION, this.bf);
        } catch (JSONException e) {
        }
        return jSONObject;
    }

    public final String ar() {
        return this.f3699b;
    }

    public final int as() {
        return this.cu;
    }

    public final String b() {
        return this.f3698a;
    }

    public final String toString() {
        return aq().toString();
    }

    public final void z() {
        this.cu = 1;
    }
}
