package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.d;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.f;
import com.tencent.wxop.stat.b.l;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

class ak {
    private static b cx = l.av();
    private static ak dj = null;
    private static Context dk = null;
    private long cv = 0;
    DefaultHttpClient dg = null;
    f dh = null;
    StringBuilder di = new StringBuilder(4096);

    private ak(Context context) {
        try {
            dk = context.getApplicationContext();
            this.cv = System.currentTimeMillis() / 1000;
            this.dh = new f();
            if (c.k()) {
                try {
                    Logger.getLogger("org.apache.http.wire").setLevel(Level.FINER);
                    Logger.getLogger("org.apache.http.headers").setLevel(Level.FINER);
                    System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
                    System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
                    System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", "debug");
                    System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", "debug");
                } catch (Throwable th) {
                }
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            this.dg = new DefaultHttpClient(basicHttpParams);
            this.dg.setKeepAliveStrategy(new al(this));
        } catch (Throwable th2) {
            cx.b(th2);
        }
    }

    static ak Z(Context context) {
        if (dj == null) {
            synchronized (ak.class) {
                if (dj == null) {
                    dj = new ak(context);
                }
            }
        }
        return dj;
    }

    static Context aB() {
        return dk;
    }

    static void j(Context context) {
        dk = context.getApplicationContext();
    }

    /* access modifiers changed from: package-private */
    public final void a(d dVar, aj ajVar) {
        b(Arrays.asList(dVar.af()), ajVar);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x01be, code lost:
        r0 = th;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.List<?> r13, com.tencent.wxop.stat.aj r14) {
        /*
            r12 = this;
            r10 = 0
            r1 = 0
            r2 = 0
            if (r13 == 0) goto L_0x000c
            boolean r0 = r13.isEmpty()
            if (r0 == 0) goto L_0x000d
        L_0x000c:
            return
        L_0x000d:
            int r3 = r13.size()
            r13.get(r2)
            java.lang.StringBuilder r0 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r4 = 0
            java.lang.StringBuilder r5 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r5 = r5.length()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r0.delete(r4, r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r0 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r4 = "["
            r0.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r4 = "rc4"
            r0 = r2
        L_0x002a:
            if (r0 >= r3) goto L_0x0047
            java.lang.StringBuilder r5 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.Object r6 = r13.get(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.append(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r5 = r3 + -1
            if (r0 == r5) goto L_0x0044
            java.lang.StringBuilder r5 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = ","
            r5.append(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0044:
            int r0 = r0 + 1
            goto L_0x002a
        L_0x0047:
            java.lang.StringBuilder r0 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = "]"
            r0.append(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r0 = r12.di     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r3 = r0.length()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.<init>()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = com.tencent.wxop.stat.c.y()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "/?index="
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            long r6 = r12.cv     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = r5.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            long r6 = r12.cv     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r8 = 1
            long r6 = r6 + r8
            r12.cv = r6     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            boolean r6 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r6 == 0) goto L_0x00aa
            com.tencent.wxop.stat.b.b r6 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "["
            r7.<init>(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r7 = r7.append(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "]Send request("
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r7 = r7.append(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "bytes), content:"
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r7 = r7.append(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r7 = r7.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r6.b(r7)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x00aa:
            org.apache.http.client.methods.HttpPost r6 = new org.apache.http.client.methods.HttpPost     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r6.<init>(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "Accept-Encoding"
            java.lang.String r7 = "gzip"
            r6.addHeader(r5, r7)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "Connection"
            java.lang.String r7 = "Keep-Alive"
            r6.setHeader(r5, r7)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "Cache-Control"
            r6.removeHeaders(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            android.content.Context r5 = com.tencent.wxop.stat.ak.dk     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            com.tencent.wxop.stat.g r5 = com.tencent.wxop.stat.g.r(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.HttpHost r5 = r5.V()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r7 = "Content-Encoding"
            r6.addHeader(r7, r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 != 0) goto L_0x01e8
            org.apache.http.impl.client.DefaultHttpClient r7 = r12.dg     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.params.HttpParams r7 = r7.getParams()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "http.route.default-proxy"
            r7.removeParameter(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x00de:
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r7.<init>(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "UTF-8"
            byte[] r0 = r0.getBytes(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r8 = r0.length     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r9 = com.tencent.wxop.stat.c.aA     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r3 <= r9) goto L_0x00ef
            r2 = 1
        L_0x00ef:
            if (r2 == 0) goto L_0x0163
            java.lang.String r2 = "Content-Encoding"
            r6.removeHeaders(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.<init>()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = ",gzip"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r2 = r2.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = "Content-Encoding"
            r6.addHeader(r3, r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 == 0) goto L_0x011a
            java.lang.String r3 = "X-Content-Encoding"
            r6.removeHeaders(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = "X-Content-Encoding"
            r6.addHeader(r3, r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x011a:
            r2 = 4
            byte[] r2 = new byte[r2]     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r7.write(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.<init>(r7)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.write(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.close()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            byte[] r0 = r7.toByteArray()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2 = 0
            r3 = 4
            java.nio.ByteBuffer r2 = java.nio.ByteBuffer.wrap(r0, r2, r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.putInt(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            boolean r2 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r2 == 0) goto L_0x0163
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r4 = "before Gzip:"
            r3.<init>(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r3 = r3.append(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r4 = " bytes, after Gzip:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r4 = r0.length     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r4 = " bytes"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.e(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0163:
            byte[] r0 = com.tencent.wxop.stat.b.g.b(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.entity.ByteArrayEntity r2 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r6.setEntity(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.impl.client.DefaultHttpClient r0 = r12.dg     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.HttpResponse r2 = r0.execute(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.HttpEntity r0 = r2.getEntity()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.StatusLine r3 = r2.getStatusLine()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r3 = r3.getStatusCode()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            long r4 = r0.getContentLength()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            boolean r6 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r6 == 0) goto L_0x01a9
            com.tencent.wxop.stat.b.b r6 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r9 = "http recv response status code:"
            r8.<init>(r9)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r8 = r8.append(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r9 = ", content length:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r8 = r8.append(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = r8.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r6.b(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x01a9:
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 > 0) goto L_0x022f
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r3 = "Server response no data."
            r2.d(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r14 == 0) goto L_0x01b9
            r14.B()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x01b9:
            org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x000c
        L_0x01be:
            r0 = move-exception
        L_0x01bf:
            if (r0 == 0) goto L_0x000c
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx
            r2.a(r0)
            if (r14 == 0) goto L_0x01cb
            r14.B()     // Catch:{ Throwable -> 0x03ab }
        L_0x01cb:
            boolean r0 = r0 instanceof java.lang.OutOfMemoryError
            if (r0 == 0) goto L_0x01dd
            java.lang.System.gc()
            r12.di = r1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r1 = 2048(0x800, float:2.87E-42)
            r0.<init>(r1)
            r12.di = r0
        L_0x01dd:
            android.content.Context r0 = com.tencent.wxop.stat.ak.dk
            com.tencent.wxop.stat.g r0 = com.tencent.wxop.stat.g.r(r0)
            r0.I()
            goto L_0x000c
        L_0x01e8:
            boolean r7 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r7 == 0) goto L_0x0206
            com.tencent.wxop.stat.b.b r7 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r9 = "proxy:"
            r8.<init>(r9)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r9 = r5.toHostString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = r8.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r7.e(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0206:
            java.lang.String r7 = "X-Content-Encoding"
            r6.addHeader(r7, r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.impl.client.DefaultHttpClient r7 = r12.dg     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            org.apache.http.params.HttpParams r7 = r7.getParams()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "http.route.default-proxy"
            r7.setParameter(r8, r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r7 = "X-Online-Host"
            java.lang.String r8 = com.tencent.wxop.stat.c.al     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r6.addHeader(r7, r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r7 = "Accept"
            java.lang.String r8 = "*/*"
            r6.addHeader(r7, r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r7 = "Content-Type"
            java.lang.String r8 = "json"
            r6.addHeader(r7, r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x00de
        L_0x022d:
            r0 = move-exception
            throw r0
        L_0x022f:
            int r4 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r4 <= 0) goto L_0x03a7
            java.io.InputStream r4 = r0.getContent()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.<init>(r4)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            long r8 = r0.getContentLength()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            int r0 = (int) r8     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            byte[] r0 = new byte[r0]     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.readFully(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r4.close()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.close()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "Content-Encoding"
            org.apache.http.Header r2 = r2.getFirstHeader(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r2 == 0) goto L_0x0268
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "gzip,rc4"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 == 0) goto L_0x0330
            byte[] r0 = com.tencent.wxop.stat.b.l.b(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            byte[] r0 = com.tencent.wxop.stat.b.g.c(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0268:
            java.lang.String r2 = new java.lang.String     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "UTF-8"
            r2.<init>(r0, r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            boolean r5 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 == 0) goto L_0x0289
            com.tencent.wxop.stat.b.b r5 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r8 = "http get response data:"
            r6.<init>(r8)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r6 = r6.append(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = r6.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.b(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0289:
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r5.<init>(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2 = 200(0xc8, float:2.8E-43)
            if (r3 != r2) goto L_0x037c
            java.lang.String r0 = "mid"
            java.lang.String r0 = r5.optString(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            boolean r2 = com.tencent.a.a.a.a.h.e(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            if (r2 == 0) goto L_0x02c1
            boolean r2 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            if (r2 == 0) goto L_0x02b8
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.String r6 = "update mid:"
            r3.<init>(r6)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.String r3 = r3.toString()     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            r2.b(r3)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
        L_0x02b8:
            android.content.Context r2 = com.tencent.wxop.stat.ak.dk     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            com.tencent.a.a.a.a.g r2 = com.tencent.a.a.a.a.g.a(r2)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            r2.b(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
        L_0x02c1:
            java.lang.String r0 = "cfg"
            boolean r0 = r5.isNull(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            if (r0 != 0) goto L_0x02d4
            java.lang.String r0 = "cfg"
            org.json.JSONObject r0 = r5.getJSONObject(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            android.content.Context r2 = com.tencent.wxop.stat.ak.dk     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            com.tencent.wxop.stat.c.a(r2, r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
        L_0x02d4:
            java.lang.String r0 = "ncts"
            boolean r0 = r5.isNull(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            if (r0 != 0) goto L_0x031a
            java.lang.String r0 = "ncts"
            int r0 = r5.getInt(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            long r2 = (long) r0     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            long r8 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            r10 = 1000(0x3e8, double:4.94E-321)
            long r8 = r8 / r10
            long r2 = r2 - r8
            int r2 = (int) r2     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            boolean r3 = com.tencent.wxop.stat.c.k()     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            if (r3 == 0) goto L_0x0310
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.String r8 = "server time:"
            r6.<init>(r8)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.StringBuilder r0 = r6.append(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.String r6 = ", diff time:"
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            r3.b(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
        L_0x0310:
            android.content.Context r0 = com.tencent.wxop.stat.ak.dk     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            com.tencent.wxop.stat.b.l.Q(r0)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            android.content.Context r0 = com.tencent.wxop.stat.ak.dk     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
            com.tencent.wxop.stat.b.l.a(r0, r2)     // Catch:{ Throwable -> 0x036a, all -> 0x022d }
        L_0x031a:
            if (r14 == 0) goto L_0x0327
            java.lang.String r0 = "ret"
            int r0 = r5.optInt(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r0 != 0) goto L_0x0371
            r14.ah()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x0327:
            r4.close()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
        L_0x032a:
            r7.close()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r0 = r1
            goto L_0x01bf
        L_0x0330:
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "rc4,gzip"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 == 0) goto L_0x0346
            byte[] r0 = com.tencent.wxop.stat.b.g.c(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            byte[] r0 = com.tencent.wxop.stat.b.l.b(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x0268
        L_0x0346:
            java.lang.String r5 = r2.getValue()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "gzip"
            boolean r5 = r5.equalsIgnoreCase(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r5 == 0) goto L_0x0358
            byte[] r0 = com.tencent.wxop.stat.b.l.b(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x0268
        L_0x0358:
            java.lang.String r2 = r2.getValue()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = "rc4"
            boolean r2 = r2.equalsIgnoreCase(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r2 == 0) goto L_0x0268
            byte[] r0 = com.tencent.wxop.stat.b.g.c(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x0268
        L_0x036a:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.c(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x031a
        L_0x0371:
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r2 = "response error data."
            r0.error(r2)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r14.B()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x0327
        L_0x037c:
            com.tencent.wxop.stat.b.b r2 = com.tencent.wxop.stat.ak.cx     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "Server response error code:"
            r5.<init>(r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = ", error:"
            java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r5 = new java.lang.String     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r0, r6)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.StringBuilder r0 = r3.append(r5)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            r2.error(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            if (r14 == 0) goto L_0x0327
            r14.B()     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x0327
        L_0x03a7:
            org.apache.http.util.EntityUtils.toString(r0)     // Catch:{ Throwable -> 0x01be, all -> 0x022d }
            goto L_0x032a
        L_0x03ab:
            r2 = move-exception
            com.tencent.wxop.stat.b.b r3 = com.tencent.wxop.stat.ak.cx
            r3.b(r2)
            goto L_0x01cb
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.ak.a(java.util.List, com.tencent.wxop.stat.aj):void");
    }

    /* access modifiers changed from: package-private */
    public final void b(List<?> list, aj ajVar) {
        if (this.dh != null) {
            this.dh.a(new am(this, list, ajVar));
        }
    }
}
