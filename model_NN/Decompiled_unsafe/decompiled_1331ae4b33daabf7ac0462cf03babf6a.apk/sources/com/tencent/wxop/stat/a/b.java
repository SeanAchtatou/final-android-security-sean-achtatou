package com.tencent.wxop.stat.a;

import com.xiaomi.mipush.sdk.MiPushClient;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public String f3689a;
    public JSONArray bl;
    public JSONObject bm = null;

    public b() {
    }

    public b(String str) {
        this.f3689a = str;
        this.bm = new JSONObject();
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj instanceof b) {
            return toString().equals(((b) obj).toString());
        }
        return false;
    }

    public final int hashCode() {
        return toString().hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder(32);
        sb.append(this.f3689a).append(MiPushClient.ACCEPT_TIME_SEPARATOR);
        if (this.bl != null) {
            sb.append(this.bl.toString());
        }
        if (this.bm != null) {
            sb.append(this.bm.toString());
        }
        return sb.toString();
    }
}
