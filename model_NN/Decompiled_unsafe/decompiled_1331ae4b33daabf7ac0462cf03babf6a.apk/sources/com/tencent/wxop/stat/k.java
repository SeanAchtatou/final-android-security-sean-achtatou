package com.tencent.wxop.stat;

import android.content.Context;

final class k implements Runnable {

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f3707b;
    final /* synthetic */ f bM;
    final /* synthetic */ Context e;

    k(Context context, String str, f fVar) {
        this.e = context;
        this.f3707b = str;
        this.bM = fVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int
     arg types: [android.content.Context, int, com.tencent.wxop.stat.f]
     candidates:
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, com.tencent.wxop.stat.f):void
      com.tencent.wxop.stat.e.a(android.content.Context, java.lang.String, java.lang.String):boolean
      com.tencent.wxop.stat.e.a(android.content.Context, boolean, com.tencent.wxop.stat.f):int */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0085, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0086, code lost:
        com.tencent.wxop.stat.e.aV.b(r0);
        com.tencent.wxop.stat.e.a(r8.e, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r8 = this;
            android.content.Context r0 = r8.e     // Catch:{ Throwable -> 0x0085 }
            com.tencent.wxop.stat.e.p(r0)     // Catch:{ Throwable -> 0x0085 }
            java.util.Map r1 = com.tencent.wxop.stat.e.aT     // Catch:{ Throwable -> 0x0085 }
            monitor-enter(r1)     // Catch:{ Throwable -> 0x0085 }
            java.util.Map r0 = com.tencent.wxop.stat.e.aT     // Catch:{ all -> 0x0082 }
            java.lang.String r2 = r8.f3707b     // Catch:{ all -> 0x0082 }
            java.lang.Object r0 = r0.remove(r2)     // Catch:{ all -> 0x0082 }
            java.lang.Long r0 = (java.lang.Long) r0     // Catch:{ all -> 0x0082 }
            monitor-exit(r1)     // Catch:{ all -> 0x0082 }
            if (r0 == 0) goto L_0x0093
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Throwable -> 0x0085 }
            long r0 = r0.longValue()     // Catch:{ Throwable -> 0x0085 }
            long r0 = r2 - r0
            r2 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r2
            java.lang.Long r5 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x0085 }
            long r0 = r5.longValue()     // Catch:{ Throwable -> 0x0085 }
            r2 = 0
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 > 0) goto L_0x003a
            r0 = 1
            java.lang.Long r5 = java.lang.Long.valueOf(r0)     // Catch:{ Throwable -> 0x0085 }
        L_0x003a:
            java.lang.String r2 = com.tencent.wxop.stat.e.aS     // Catch:{ Throwable -> 0x0085 }
            if (r2 == 0) goto L_0x004b
            java.lang.String r0 = r8.f3707b     // Catch:{ Throwable -> 0x0085 }
            boolean r0 = r2.equals(r0)     // Catch:{ Throwable -> 0x0085 }
            r1 = 1
            if (r0 != r1) goto L_0x004b
            java.lang.String r2 = "-"
        L_0x004b:
            com.tencent.wxop.stat.a.h r0 = new com.tencent.wxop.stat.a.h     // Catch:{ Throwable -> 0x0085 }
            android.content.Context r1 = r8.e     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r3 = r8.f3707b     // Catch:{ Throwable -> 0x0085 }
            android.content.Context r4 = r8.e     // Catch:{ Throwable -> 0x0085 }
            r6 = 0
            com.tencent.wxop.stat.f r7 = r8.bM     // Catch:{ Throwable -> 0x0085 }
            int r4 = com.tencent.wxop.stat.e.a(r4, r6, r7)     // Catch:{ Throwable -> 0x0085 }
            com.tencent.wxop.stat.f r6 = r8.bM     // Catch:{ Throwable -> 0x0085 }
            r0.<init>(r1, r2, r3, r4, r5, r6)     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r1 = r8.f3707b     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r2 = com.tencent.wxop.stat.e.aR     // Catch:{ Throwable -> 0x0085 }
            boolean r1 = r1.equals(r2)     // Catch:{ Throwable -> 0x0085 }
            if (r1 != 0) goto L_0x0074
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.e.aV     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r2 = "Invalid invocation since previous onResume on diff page."
            r1.warn(r2)     // Catch:{ Throwable -> 0x0085 }
        L_0x0074:
            com.tencent.wxop.stat.p r1 = new com.tencent.wxop.stat.p     // Catch:{ Throwable -> 0x0085 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0085 }
            r1.ah()     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r0 = r8.f3707b     // Catch:{ Throwable -> 0x0085 }
            java.lang.String unused = com.tencent.wxop.stat.e.aS = r0     // Catch:{ Throwable -> 0x0085 }
        L_0x0081:
            return
        L_0x0082:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ Throwable -> 0x0085 }
            throw r0     // Catch:{ Throwable -> 0x0085 }
        L_0x0085:
            r0 = move-exception
            com.tencent.wxop.stat.b.b r1 = com.tencent.wxop.stat.e.aV
            r1.b(r0)
            android.content.Context r1 = r8.e
            com.tencent.wxop.stat.e.a(r1, r0)
            goto L_0x0081
        L_0x0093:
            com.tencent.wxop.stat.b.b r0 = com.tencent.wxop.stat.e.aV     // Catch:{ Throwable -> 0x0085 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r2 = "Starttime for PageID:"
            r1.<init>(r2)     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r2 = r8.f3707b     // Catch:{ Throwable -> 0x0085 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r2 = " not found, lost onResume()?"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x0085 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x0085 }
            r0.d(r1)     // Catch:{ Throwable -> 0x0085 }
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.wxop.stat.k.run():void");
    }
}
