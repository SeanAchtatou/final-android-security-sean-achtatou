package com.tencent.wxop.stat;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private String f3705a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f3706b = null;
    private boolean ba = false;
    private boolean bb = false;
    private String c = null;

    public final boolean R() {
        return this.ba;
    }

    public final String S() {
        return this.f3705a;
    }

    public final String T() {
        return this.f3706b;
    }

    public final boolean U() {
        return this.bb;
    }

    public final String getVersion() {
        return this.c;
    }

    public final void s(String str) {
        this.f3705a = str;
    }

    public final String toString() {
        return "StatSpecifyReportedInfo [appKey=" + this.f3705a + ", installChannel=" + this.f3706b + ", version=" + this.c + ", sendImmediately=" + this.ba + ", isImportant=" + this.bb + "]";
    }
}
