package com.tencent.stat.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private static SharedPreferences f3655a = null;

    public static int a(Context context, String str, int i) {
        return a(context).getInt(k.b(context, "" + str), i);
    }

    public static long a(Context context, String str, long j) {
        return a(context).getLong(k.b(context, "" + str), j);
    }

    static synchronized SharedPreferences a(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (p.class) {
            if (f3655a == null) {
                f3655a = PreferenceManager.getDefaultSharedPreferences(context);
            }
            sharedPreferences = f3655a;
        }
        return sharedPreferences;
    }

    public static String a(Context context, String str, String str2) {
        return a(context).getString(k.b(context, "" + str), str2);
    }

    public static void b(Context context, String str, int i) {
        String b2 = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putInt(b2, i);
        edit.commit();
    }

    public static void b(Context context, String str, long j) {
        String b2 = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putLong(b2, j);
        edit.commit();
    }

    public static void b(Context context, String str, String str2) {
        String b2 = k.b(context, "" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putString(b2, str2);
        edit.commit();
    }
}
