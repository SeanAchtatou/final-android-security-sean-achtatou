package com.tencent.stat.common;

import java.io.File;

class o {

    /* renamed from: a  reason: collision with root package name */
    private static int f3654a = -1;

    public static boolean a() {
        if (f3654a == 1) {
            return true;
        }
        if (f3654a == 0) {
            return false;
        }
        String[] strArr = {"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
        int i = 0;
        while (i < strArr.length) {
            try {
                File file = new File(strArr[i] + "su");
                if (file == null || !file.exists()) {
                    i++;
                } else {
                    f3654a = 1;
                    return true;
                }
            } catch (Exception e) {
            }
        }
        f3654a = 0;
        return false;
    }
}
