package com.tencent.stat.common;

import android.content.Context;
import com.meizu.cloud.pushsdk.notification.model.AdvanceSetting;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    static c f3643a;
    private static StatLogger d = k.b();
    private static JSONObject e = null;

    /* renamed from: b  reason: collision with root package name */
    Integer f3644b = null;
    String c = null;

    public a(Context context) {
        try {
            a(context);
            this.f3644b = k.q(context.getApplicationContext());
            this.c = k.p(context);
        } catch (Throwable th) {
            d.e(th);
        }
    }

    static synchronized c a(Context context) {
        c cVar;
        synchronized (a.class) {
            if (f3643a == null) {
                f3643a = new c(context.getApplicationContext());
            }
            cVar = f3643a;
        }
        return cVar;
    }

    public static void a(Context context, Map<String, String> map) {
        if (map != null) {
            HashMap hashMap = new HashMap(map);
            if (e == null) {
                e = new JSONObject();
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                e.put((String) entry.getKey(), entry.getValue());
            }
        }
    }

    public void a(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (f3643a != null) {
                f3643a.a(jSONObject2);
            }
            k.a(jSONObject2, AdvanceSetting.CLEAR_NOTIFICATION, this.c);
            if (this.f3644b != null) {
                jSONObject2.put("tn", this.f3644b);
            }
            jSONObject.put("ev", jSONObject2);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.e(th);
        }
    }
}
