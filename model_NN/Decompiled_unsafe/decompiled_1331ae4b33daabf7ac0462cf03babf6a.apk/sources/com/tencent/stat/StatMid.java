package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.k;

public class StatMid {

    /* renamed from: a  reason: collision with root package name */
    private static StatLogger f3614a = k.b();

    /* renamed from: b  reason: collision with root package name */
    private static DeviceInfo f3615b = null;

    static synchronized DeviceInfo a(Context context) {
        DeviceInfo deviceInfo;
        synchronized (StatMid.class) {
            try {
                a a2 = a.a(context);
                DeviceInfo a3 = a(a2.d(DeviceInfo.TAG_FLAG, null));
                f3614a.d("get device info from internal storage:" + a3);
                DeviceInfo a4 = a(a2.f(DeviceInfo.TAG_FLAG, null));
                f3614a.d("get device info from setting.system:" + a4);
                DeviceInfo a5 = a(a2.b(DeviceInfo.TAG_FLAG, null));
                f3614a.d("get device info from SharedPreference:" + a5);
                f3615b = a(a5, a4, a3);
                if (f3615b == null) {
                    f3615b = new DeviceInfo();
                }
                DeviceInfo b2 = n.a(context).b(context);
                if (b2 != null) {
                    f3615b.d(b2.getImei());
                    f3615b.e(b2.getMac());
                    f3615b.b(b2.getUserType());
                }
            } catch (Throwable th) {
                f3614a.e(th);
            }
            deviceInfo = f3615b;
        }
        return deviceInfo;
    }

    static DeviceInfo a(DeviceInfo deviceInfo, DeviceInfo deviceInfo2) {
        if (deviceInfo != null && deviceInfo2 != null) {
            return deviceInfo.a(deviceInfo2) >= 0 ? deviceInfo : deviceInfo2;
        }
        if (deviceInfo != null) {
            return deviceInfo;
        }
        if (deviceInfo2 != null) {
            return deviceInfo2;
        }
        return null;
    }

    static DeviceInfo a(DeviceInfo deviceInfo, DeviceInfo deviceInfo2, DeviceInfo deviceInfo3) {
        return a(a(deviceInfo, deviceInfo2), a(deviceInfo2, deviceInfo3));
    }

    private static DeviceInfo a(String str) {
        if (str != null) {
            return DeviceInfo.a(k.d(str));
        }
        return null;
    }

    public static DeviceInfo getDeviceInfo(Context context) {
        if (context == null) {
            f3614a.error("Context for StatConfig.getDeviceInfo is null.");
            return null;
        }
        if (f3615b == null) {
            a(context);
        }
        return f3615b;
    }

    public static String getMid(Context context) {
        if (f3615b == null) {
            getDeviceInfo(context);
        }
        return f3615b.getMid();
    }

    public static void updateDeviceInfo(Context context, String str) {
        try {
            getDeviceInfo(context);
            f3615b.c(str);
            f3615b.a(f3615b.a() + 1);
            f3615b.a(System.currentTimeMillis());
            String jSONObject = f3615b.c().toString();
            f3614a.d("save DeviceInfo:" + jSONObject);
            String replace = k.c(jSONObject).replace("\n", "");
            a a2 = a.a(context);
            a2.c(DeviceInfo.TAG_FLAG, replace);
            a2.e(DeviceInfo.TAG_FLAG, replace);
            a2.a(DeviceInfo.TAG_FLAG, replace);
        } catch (Throwable th) {
            f3614a.e(th);
        }
    }
}
