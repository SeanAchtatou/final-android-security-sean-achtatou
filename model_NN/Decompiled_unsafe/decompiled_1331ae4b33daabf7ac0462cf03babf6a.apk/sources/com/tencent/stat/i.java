package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.a.d;
import java.io.File;
import java.util.Iterator;

class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private Context f3663a = null;

    public i(Context context) {
        this.f3663a = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatService.a(android.content.Context, boolean):int
     arg types: [android.content.Context, int]
     candidates:
      com.tencent.stat.StatService.a(android.content.Context, java.lang.Throwable):void
      com.tencent.stat.StatService.a(android.content.Context, java.util.Map<java.lang.String, ?>):void
      com.tencent.stat.StatService.a(android.content.Context, boolean):int */
    public void run() {
        Iterator<File> it = StatNativeCrashReport.a(this.f3663a).iterator();
        while (it.hasNext()) {
            File next = it.next();
            d dVar = new d(this.f3663a, StatService.a(this.f3663a, false), StatNativeCrashReport.a(next), 3, 10240);
            dVar.a(StatNativeCrashReport.b(next));
            if (StatService.c(this.f3663a) != null) {
                StatService.c(this.f3663a).post(new k(dVar));
            }
            next.delete();
            StatService.i.d("delete tombstone file:" + next.getAbsolutePath().toString());
        }
    }
}
