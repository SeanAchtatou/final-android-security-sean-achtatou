package com.tencent.stat;

public class StatGameUser implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private String f3612a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f3613b = "";
    private String c = "";

    public StatGameUser clone() {
        try {
            return (StatGameUser) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    public String getAccount() {
        return this.f3613b;
    }

    public String getLevel() {
        return this.c;
    }

    public String getWorldName() {
        return this.f3612a;
    }

    public void setAccount(String str) {
        this.f3613b = str;
    }

    public void setLevel(String str) {
        this.c = str;
    }

    public void setWorldName(String str) {
        this.f3612a = str;
    }
}
