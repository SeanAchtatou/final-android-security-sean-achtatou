package com.tencent.stat.a;

import android.content.Context;
import com.tencent.stat.StatGameUser;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class g extends e {

    /* renamed from: a  reason: collision with root package name */
    private StatGameUser f3633a = null;

    public g(Context context, int i, StatGameUser statGameUser) {
        super(context, i);
        this.f3633a = statGameUser.clone();
    }

    public f a() {
        return f.MTA_GAME_USER;
    }

    public boolean a(JSONObject jSONObject) {
        if (this.f3633a == null) {
            return false;
        }
        k.a(jSONObject, "wod", this.f3633a.getWorldName());
        k.a(jSONObject, "gid", this.f3633a.getAccount());
        k.a(jSONObject, "lev", this.f3633a.getLevel());
        return true;
    }
}
