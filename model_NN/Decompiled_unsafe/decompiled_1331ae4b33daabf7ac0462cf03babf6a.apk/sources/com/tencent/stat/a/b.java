package com.tencent.stat.a;

import android.content.Context;
import com.meizu.cloud.pushsdk.notification.model.NotificationStyle;
import java.util.Map;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

public class b extends e {

    /* renamed from: a  reason: collision with root package name */
    protected c f3625a = new c();
    private long l = -1;

    public b(Context context, int i, String str) {
        super(context, i);
        this.f3625a.f3626a = str;
    }

    public f a() {
        return f.CUSTOM;
    }

    public void a(long j) {
        this.l = j;
    }

    public void a(Properties properties) {
        if (properties != null) {
            this.f3625a.c = (Properties) properties.clone();
        }
    }

    public void a(String[] strArr) {
        if (strArr != null) {
            this.f3625a.f3627b = (String[]) strArr.clone();
        }
    }

    public boolean a(JSONObject jSONObject) {
        JSONObject jSONObject2;
        jSONObject.put(NotificationStyle.EXPANDABLE_IMAGE_URL, this.f3625a.f3626a);
        if (this.l > 0) {
            jSONObject.put("du", this.l);
        }
        if (this.f3625a.c == null && this.f3625a.f3627b == null) {
            jSONObject.put("kv", new JSONObject());
        }
        if (this.f3625a.f3627b != null) {
            JSONArray jSONArray = new JSONArray();
            for (String put : this.f3625a.f3627b) {
                jSONArray.put(put);
            }
            jSONObject.put("ar", jSONArray);
        }
        if (this.f3625a.c == null) {
            return true;
        }
        JSONObject jSONObject3 = new JSONObject();
        try {
            for (Map.Entry entry : this.f3625a.c.entrySet()) {
                jSONObject3.put(entry.getKey().toString(), entry.getValue().toString());
            }
            jSONObject2 = jSONObject3;
        } catch (Exception e) {
            jSONObject2 = new JSONObject(this.f3625a.c);
        }
        jSONObject.put("kv", jSONObject2);
        return true;
    }
}
