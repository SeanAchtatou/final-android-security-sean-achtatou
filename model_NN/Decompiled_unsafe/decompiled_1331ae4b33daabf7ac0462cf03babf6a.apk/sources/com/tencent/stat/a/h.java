package com.tencent.stat.a;

import android.content.Context;
import com.meizu.cloud.pushsdk.notification.model.AdvanceSetting;
import com.tencent.stat.StatAppMonitor;
import com.tencent.stat.common.k;
import org.json.JSONObject;

public class h extends e {
    private static String l = null;
    private static String m = null;

    /* renamed from: a  reason: collision with root package name */
    private StatAppMonitor f3634a = null;

    public h(Context context, int i, StatAppMonitor statAppMonitor) {
        super(context, i);
        this.f3634a = statAppMonitor.clone();
    }

    public f a() {
        return f.MONITOR_STAT;
    }

    public boolean a(JSONObject jSONObject) {
        if (this.f3634a == null) {
            return false;
        }
        jSONObject.put("na", this.f3634a.getInterfaceName());
        jSONObject.put("rq", this.f3634a.getReqSize());
        jSONObject.put("rp", this.f3634a.getRespSize());
        jSONObject.put("rt", this.f3634a.getResultType());
        jSONObject.put("tm", this.f3634a.getMillisecondsConsume());
        jSONObject.put("rc", this.f3634a.getReturnCode());
        jSONObject.put("sp", this.f3634a.getSampling());
        if (m == null) {
            m = k.r(this.k);
        }
        k.a(jSONObject, "av", m);
        if (l == null) {
            l = k.m(this.k);
        }
        k.a(jSONObject, "op", l);
        jSONObject.put(AdvanceSetting.CLEAR_NOTIFICATION, k.p(this.k));
        return true;
    }
}
