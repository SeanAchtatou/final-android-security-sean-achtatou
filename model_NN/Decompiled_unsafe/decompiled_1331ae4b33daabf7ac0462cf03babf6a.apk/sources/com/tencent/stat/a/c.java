package com.tencent.stat.a;

import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.Arrays;
import java.util.Properties;

public class c {

    /* renamed from: a  reason: collision with root package name */
    String f3626a;

    /* renamed from: b  reason: collision with root package name */
    String[] f3627b;
    Properties c = null;

    public c() {
    }

    public c(String str, String[] strArr, Properties properties) {
        this.f3626a = str;
        this.f3627b = strArr;
        this.c = properties;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        c cVar = (c) obj;
        boolean z = this.f3626a.equals(cVar.f3626a) && Arrays.equals(this.f3627b, cVar.f3627b);
        return this.c != null ? z && this.c.equals(cVar.c) : z && cVar.c == null;
    }

    public int hashCode() {
        int i = 0;
        if (this.f3626a != null) {
            i = this.f3626a.hashCode();
        }
        if (this.f3627b != null) {
            i ^= Arrays.hashCode(this.f3627b);
        }
        return this.c != null ? i ^ this.c.hashCode() : i;
    }

    public String toString() {
        String str = this.f3626a;
        String str2 = "";
        if (this.f3627b != null) {
            String str3 = this.f3627b[0];
            for (int i = 1; i < this.f3627b.length; i++) {
                str3 = str3 + MiPushClient.ACCEPT_TIME_SEPARATOR + this.f3627b[i];
            }
            str2 = "[" + str3 + "]";
        }
        if (this.c != null) {
            str2 = str2 + this.c.toString();
        }
        return str + str2;
    }
}
