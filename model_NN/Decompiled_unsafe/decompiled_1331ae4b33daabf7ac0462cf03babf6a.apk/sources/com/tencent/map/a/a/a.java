package com.tencent.map.a.a;

import android.content.Context;
import com.tencent.map.b.f;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static f f3383a = f.a();

    /* renamed from: b  reason: collision with root package name */
    private static a f3384b;

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3384b == null) {
                f3384b = new a();
            }
            aVar = f3384b;
        }
        return aVar;
    }

    public boolean a(Context context, b bVar) {
        return f3383a.a(context, bVar);
    }

    public boolean a(String str, String str2) {
        return f3383a.a(str, str2);
    }

    public void b() {
        f3383a.b();
    }
}
