package com.tencent.map.b;

import android.location.Location;
import org.json.JSONException;
import org.json.JSONObject;

public class b {

    /* renamed from: b  reason: collision with root package name */
    private static b f3393b;

    /* renamed from: a  reason: collision with root package name */
    public String f3394a = "";
    private double c = 0.0d;
    private double d = 0.0d;
    private double e = 0.0d;
    private double f = 0.0d;
    private double g = 0.0d;
    private double h = 0.0d;
    /* access modifiers changed from: private */
    public a i;
    private C0048b j = null;
    /* access modifiers changed from: private */
    public boolean k = false;

    public interface a {
        void a(double d, double d2);
    }

    /* renamed from: com.tencent.map.b.b$b  reason: collision with other inner class name */
    public class C0048b extends Thread {
        public C0048b() {
        }

        public final void run() {
            try {
                byte[] a2 = j.a(b.this.f3394a.getBytes());
                boolean unused = b.this.k = true;
                n a3 = b.a("http://ls.map.soso.com/deflect?c=1", "SOSO MAP LBS SDK", a2);
                boolean unused2 = b.this.k = false;
                b.a(b.this, j.b(a3.f3439a), a3.f3440b);
            } catch (Exception e) {
                int i = 0;
                while (true) {
                    i++;
                    if (i <= 3) {
                        try {
                            sleep(2000);
                            n a4 = b.a("http://ls.map.soso.com/deflect?c=1", "SOSO MAP LBS SDK", j.a(b.this.f3394a.getBytes()));
                            boolean unused3 = b.this.k = false;
                            b.a(b.this, j.b(a4.f3439a), a4.f3440b);
                            return;
                        } catch (Exception e2) {
                        }
                    } else {
                        boolean unused4 = b.this.k = false;
                        if (b.this.i != null) {
                            b.this.i.a(360.0d, 360.0d);
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    public static b a() {
        if (f3393b == null) {
            f3393b = new b();
        }
        return f3393b;
    }

    static /* synthetic */ void a(b bVar, byte[] bArr, String str) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            stringBuffer.append(new String(bArr, str));
        } catch (Exception e2) {
            if (bVar.i != null) {
                bVar.i.a(360.0d, 360.0d);
            }
        }
        try {
            JSONObject jSONObject = new JSONObject(stringBuffer.toString()).getJSONObject("location");
            double d2 = jSONObject.getDouble("latitude");
            double d3 = jSONObject.getDouble("longitude");
            bVar.g = d2 - bVar.e;
            bVar.h = d3 - bVar.f;
            bVar.c = bVar.e;
            bVar.d = bVar.f;
            if (bVar.i != null) {
                bVar.i.a(d2, d3);
            }
        } catch (JSONException e3) {
            if (bVar.i != null) {
                bVar.i.a(360.0d, 360.0d);
            }
        }
    }

    public final void a(double d2, double d3, a aVar) {
        this.i = aVar;
        if (!(this.g == 0.0d || this.h == 0.0d)) {
            float[] fArr = new float[10];
            Location.distanceBetween(d2, d3, this.c, this.d, fArr);
            if (fArr[0] < 1500.0f) {
                this.i.a(this.g + d2, this.h + d3);
                return;
            }
        }
        if (!this.k) {
            this.f3394a = "{\"source\":101,\"access_token\":\"160e7bd42dec9428721034e0146fc6dd\",\"location\":{\"latitude\":" + d2 + ",\"longitude\":" + d3 + "}\t}";
            this.e = d2;
            this.f = d3;
            this.j = new C0048b();
            this.j.start();
        }
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static n a(String str, String str2, byte[] bArr) throws o, r, Exception {
        boolean z = true;
        if (l.b() == null) {
            z = false;
        }
        if (!z) {
            throw new o();
        }
        try {
            return q.a(false, str, str2, null, bArr, false, true);
        } catch (Exception e2) {
            throw e2;
        }
    }
}
