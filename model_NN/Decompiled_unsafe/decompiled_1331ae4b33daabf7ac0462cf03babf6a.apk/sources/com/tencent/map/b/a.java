package com.tencent.map.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f3391a = null;

    /* renamed from: com.tencent.map.b.a$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ a f3392a;

        private AnonymousClass1(a aVar) {
            this.f3392a = aVar;
        }

        /* synthetic */ AnonymousClass1(a aVar, byte b2) {
            this(aVar);
        }

        public boolean a(String str, String str2) {
            int a2 = a.a(this.f3392a, str);
            if (str2.charAt(4) != i.f3429a.charAt(((((a2 * 9) + 10) / 3) + 36) & 31)) {
                return false;
            }
            if (str2.charAt(7) != i.f3429a.charAt((((a2 * 5) + 11) / 5) & 31)) {
                return false;
            }
            if (str2.charAt(12) != i.f3429a.charAt((((a2 + 10) / 3) << 3) & 31)) {
                return false;
            }
            if (str2.charAt(14) != i.f3429a.charAt((((a2 * 3) + 19) / 9) & 31)) {
                return false;
            }
            if (str2.charAt(19) != i.f3429a.charAt((((a2 * 3) + 39) / 8) & 31)) {
                return false;
            }
            if (str2.charAt(21) != i.f3429a.charAt((((a2 / 23) + 67) / 7) & 31)) {
                return false;
            }
            if (str2.charAt(26) != i.f3429a.charAt(((((a2 + 23) / 6) + 3) * 7) & 31)) {
                return false;
            }
            int i = 0;
            for (int i2 = 0; i2 < str.length(); i2++) {
                i = i.f3430b[(i ^ i.a(str.charAt(i2))) & 255] ^ ((i >> 8) & 255);
            }
            if (str2.charAt(0) != i.f3429a.charAt(i & 31)) {
                return false;
            }
            return str2.charAt(1) == i.f3429a.charAt((i >> 5) & 31);
        }
    }

    private a() {
    }

    static /* synthetic */ int a(a aVar, String str) {
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            i += i.a(str.charAt(i2));
        }
        return ((length << 7) + length) ^ i;
    }

    public static synchronized a a() {
        a aVar;
        synchronized (a.class) {
            if (f3391a == null) {
                f3391a = new a();
            }
            aVar = f3391a;
        }
        return aVar;
    }

    public final boolean a(String str, String str2) {
        boolean z;
        if (!i.a(str) || !i.b(str2) || !new AnonymousClass1(this, (byte) 0).a(str, str2)) {
            return false;
        }
        int i = 0;
        for (int i2 = 0; i2 < 27; i2++) {
            i = i.f3430b[(i ^ i.a(str2.charAt(i2))) & 255] ^ ((i >> 8) & 255);
        }
        if (str2.charAt(27) != i.f3429a.charAt(i & 31)) {
            z = false;
        } else {
            z = str2.charAt(28) == i.f3429a.charAt((i >> 5) & 31);
        }
        return z;
    }
}
