package com.tencent.map.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/* compiled from: ProGuard */
public final class l {

    /* renamed from: b  reason: collision with root package name */
    private static l f3435b;

    /* renamed from: a  reason: collision with root package name */
    private Context f3436a;

    public static l a() {
        if (f3435b == null) {
            f3435b = new l();
        }
        return f3435b;
    }

    private l() {
    }

    public final void a(Context context) {
        if (this.f3436a == null) {
            this.f3436a = context.getApplicationContext();
        }
    }

    public static Context b() {
        return a().f3436a;
    }

    public static boolean c() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) a().f3436a.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.getType() == 1) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean d() {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) a().f3436a.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo != null) {
                return activeNetworkInfo.isAvailable();
            }
        } catch (Exception e) {
        }
        return false;
    }
}
