package com.tencent.map.b;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: ProGuard */
public final class k {

    /* renamed from: a  reason: collision with root package name */
    private static int f3431a;

    /* renamed from: b  reason: collision with root package name */
    private static int f3432b;
    private static int c;
    private static int d;
    private static int e;
    private static int f;
    private static ArrayList<a> g;
    private static long h;
    private static long i;
    private static long j;
    private static long k;
    private static long l;
    private static long m;
    private static long n;
    private static long o;
    private static long p;
    private static long q;
    private static int r;
    private static int s;
    private static int t;
    private static int u;

    /* compiled from: ProGuard */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public long f3433a;

        /* renamed from: b  reason: collision with root package name */
        public long f3434b;
        public long c;
        public long d;
        public int e;
        public long f;
        public int g;
        public int h;
    }

    static {
        NetworkInfo activeNetworkInfo;
        String subscriberId;
        f3431a = 10000;
        f3432b = 15000;
        c = 5000;
        d = 20000;
        e = 25000;
        f = 15000;
        f3431a = 12000;
        f3432b = 20000;
        c = 8000;
        d = 20000;
        e = 25000;
        f = 15000;
        ConnectivityManager connectivityManager = (ConnectivityManager) l.b().getSystemService("connectivity");
        if (connectivityManager != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null) {
            int type = activeNetworkInfo.getType();
            if (activeNetworkInfo.isConnected() && type == 0 && (subscriberId = ((TelephonyManager) l.b().getSystemService("phone")).getSubscriberId()) != null && subscriberId.length() > 3 && !subscriberId.startsWith("46000") && !subscriberId.startsWith("46002")) {
                f3431a = 15000;
                f3432b = 25000;
                c = 10000;
                d = 25000;
                e = 35000;
                f = 15000;
            }
        }
    }

    public static int a() {
        int i2;
        NetworkInfo activeNetworkInfo;
        int i3 = f3431a;
        if (j <= 0 || k <= 0) {
            i2 = i3;
        } else {
            i2 = (int) ((Math.max(m, j) + k) - l);
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) l.b().getSystemService("connectivity");
        if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
            if (!activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable()) {
                i2 = f3432b;
            } else if (k > 0 && k < ((long) c)) {
                i2 = c;
            }
        }
        int i4 = (u * c) + i2;
        if (i4 <= c) {
            i4 = c;
        }
        if (((long) i4) <= k) {
            i4 = (int) (k + ((long) c));
        }
        if (i4 >= f3432b) {
            i4 = f3432b;
        }
        a b2 = b(Thread.currentThread().getId());
        if (b2 == null) {
            b2 = a(Thread.currentThread().getId());
        }
        if (i4 < b2.g + c) {
            i4 = b2.g + c;
        }
        b2.g = i4;
        return i4;
    }

    public static int b() {
        int i2;
        NetworkInfo activeNetworkInfo;
        int i3 = d;
        if (n <= 0 || o <= 0) {
            i2 = i3;
        } else {
            i2 = (int) ((Math.max(q, n) + o) - p);
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) l.b().getSystemService("connectivity");
        if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
            if (!activeNetworkInfo.isConnected() && activeNetworkInfo.isAvailable()) {
                i2 = e;
            } else if (o > 0 && o < ((long) f)) {
                i2 = f;
            }
        }
        int i4 = (u * c) + i2;
        if (i4 <= f) {
            i4 = f;
        }
        if (((long) i4) <= o) {
            i4 = (int) (o + ((long) f));
        }
        if (i4 >= e) {
            i4 = e;
        }
        a b2 = b(Thread.currentThread().getId());
        if (b2 != null) {
            if (i4 < b2.h + f) {
                i4 = b2.h + f;
            }
            if (i4 < b2.g + f) {
                i4 = b2.g + f;
            }
            b2.h = i4;
        }
        return i4;
    }

    public static void a(boolean z) {
        if (!z) {
            u++;
        }
        a c2 = c(Thread.currentThread().getId());
        if (c2 != null) {
            long j2 = c2.f3434b;
        }
    }

    public static void a(HttpURLConnection httpURLConnection) {
        a b2 = b(Thread.currentThread().getId());
        if (b2 == null) {
            b2 = a(Thread.currentThread().getId());
        }
        if (b2 != null) {
            b2.f3434b = System.currentTimeMillis();
        }
    }

    public static void c() {
        long j2;
        a b2 = b(Thread.currentThread().getId());
        if (b2 != null) {
            b2.c = System.currentTimeMillis() - b2.f3434b;
            b2.f3434b = System.currentTimeMillis();
            m = b2.c;
            k = b2.c > k ? b2.c : k;
            if (b2.c < l) {
                j2 = b2.c;
            } else {
                j2 = l == 0 ? b2.c : l;
            }
            l = j2;
            if (g != null) {
                synchronized (g) {
                    Iterator<a> it = g.iterator();
                    int i2 = 0;
                    while (it.hasNext()) {
                        a next = it.next();
                        if (next.c > 0) {
                            j += next.c;
                            i2++;
                        }
                    }
                    if (i2 > 0) {
                        j /= (long) i2;
                    }
                }
            }
        }
    }

    public static void d() {
        long j2;
        long j3;
        a b2 = b(Thread.currentThread().getId());
        if (b2 != null) {
            b2.d = System.currentTimeMillis() - b2.f3434b;
            b2.f3434b = System.currentTimeMillis();
            q = b2.d;
            if (b2.d > o) {
                j2 = b2.d;
            } else {
                j2 = o;
            }
            o = j2;
            if (b2.d < p) {
                j3 = b2.d;
            } else {
                j3 = p == 0 ? b2.d : p;
            }
            p = j3;
            if (g != null) {
                synchronized (g) {
                    Iterator<a> it = g.iterator();
                    int i2 = 0;
                    while (it.hasNext()) {
                        a next = it.next();
                        if (next.d > 0) {
                            n += next.d;
                            i2++;
                        }
                    }
                    if (i2 > 0) {
                        n /= (long) i2;
                    }
                }
            }
        }
    }

    public static void a(int i2) {
        int i3;
        a b2 = b(Thread.currentThread().getId());
        if (b2 != null) {
            b2.f = System.currentTimeMillis() - b2.f3434b;
            b2.f3434b = System.currentTimeMillis();
            b2.e = i2;
            int i4 = (int) (((long) (i2 * 1000)) / (b2.f == 0 ? 1 : b2.f));
            t = i4;
            r = i4 > r ? t : r;
            if (t < s) {
                i3 = t;
            } else {
                i3 = s == 0 ? t : s;
            }
            s = i3;
            if (g != null) {
                synchronized (g) {
                    Iterator<a> it = g.iterator();
                    while (it.hasNext()) {
                        a next = it.next();
                        int i5 = next.e;
                        long j2 = next.f;
                    }
                }
            }
            if (u > 0 && b2.c < ((long) c) && b2.d < ((long) f)) {
                u--;
            }
            b2.g = (int) b2.c;
        }
    }

    private static a a(long j2) {
        a aVar;
        int i2;
        boolean z;
        if (g == null) {
            g = new ArrayList<>();
        }
        synchronized (g) {
            if (g.size() > 20) {
                int size = g.size();
                int i3 = 0;
                boolean z2 = false;
                int i4 = 0;
                while (i3 < size / 2) {
                    if (g.get(i4).f > 0 || System.currentTimeMillis() - g.get(i4).f3434b > 600000) {
                        g.remove(i4);
                        z = true;
                        i2 = i4;
                    } else {
                        boolean z3 = z2;
                        i2 = i4 + 1;
                        z = z3;
                    }
                    i3++;
                    i4 = i2;
                    z2 = z;
                }
                if (z2) {
                    g.get(0);
                    h = 0;
                    g.get(0);
                    i = 0;
                    k = g.get(0).c;
                    l = g.get(0).c;
                    o = g.get(0).d;
                    p = g.get(0).d;
                    if (g.get(0).f > 0) {
                        r = (int) (((long) (g.get(0).e * 1000)) / g.get(0).f);
                    }
                    s = r;
                    Iterator<a> it = g.iterator();
                    while (it.hasNext()) {
                        a next = it.next();
                        if (0 > h) {
                            h = 0;
                        }
                        if (0 < i) {
                            i = 0;
                        }
                        if (next.c > k) {
                            k = next.c;
                        }
                        if (next.c < l) {
                            l = next.c;
                        }
                        if (next.d > o) {
                            o = next.d;
                        }
                        if (next.d < p) {
                            p = next.d;
                        }
                        if (next.f > 0) {
                            int i5 = (int) (((long) (next.e * 1000)) / next.f);
                            if (i5 > r) {
                                r = i5;
                            }
                            if (i5 < s) {
                                s = i5;
                            }
                        }
                    }
                }
            }
            aVar = new a();
            aVar.f3433a = j2;
            g.add(aVar);
        }
        return aVar;
    }

    private static a b(long j2) {
        if (g == null) {
            return null;
        }
        synchronized (g) {
            Iterator<a> it = g.iterator();
            while (it.hasNext()) {
                a next = it.next();
                if (next.f3433a == j2) {
                    return next;
                }
            }
            return null;
        }
    }

    private static a c(long j2) {
        if (g != null) {
            synchronized (g) {
                for (int size = g.size() - 1; size >= 0; size--) {
                    if (g.get(size).f3433a == j2) {
                        a remove = g.remove(size);
                        return remove;
                    }
                }
            }
        }
        return null;
    }
}
