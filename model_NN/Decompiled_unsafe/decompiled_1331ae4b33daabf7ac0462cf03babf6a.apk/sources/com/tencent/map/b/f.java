package com.tencent.map.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.map.b.b;
import com.tencent.map.b.d;
import com.tencent.map.b.e;
import com.tencent.map.b.g;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

public final class f implements b.a, d.c, e.c, g.c {
    /* access modifiers changed from: private */
    public static boolean t = false;
    private static f u = null;
    private com.tencent.map.a.a.d A;
    /* access modifiers changed from: private */
    public int B;
    private int C;
    /* access modifiers changed from: private */
    public int D;
    /* access modifiers changed from: private */
    public String E;
    /* access modifiers changed from: private */
    public String F;
    /* access modifiers changed from: private */
    public String G;
    /* access modifiers changed from: private */
    public String H;
    /* access modifiers changed from: private */
    public String I;
    /* access modifiers changed from: private */
    public String J;
    /* access modifiers changed from: private */
    public boolean K;
    private boolean L;
    /* access modifiers changed from: private */
    public long M;
    private Handler N;
    private Runnable O;
    private final BroadcastReceiver P;

    /* renamed from: a  reason: collision with root package name */
    private long f3413a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Context f3414b;
    private e c;
    /* access modifiers changed from: private */
    public d d;
    /* access modifiers changed from: private */
    public g e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private c h;
    private b i;
    private com.tencent.map.a.a.b j;
    private int k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public int m;
    private byte[] n;
    private byte[] o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public c q;
    private b r;
    /* access modifiers changed from: private */
    public a s;
    private long v;
    /* access modifiers changed from: private */
    public e.b w;
    private d.b x;
    private g.b y;
    private com.tencent.map.a.a.d z;

    class a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private e.b f3417a = null;

        /* renamed from: b  reason: collision with root package name */
        private d.b f3418b = null;
        private g.b c = null;

        a(e.b bVar, d.b bVar2, g.b bVar3) {
            if (bVar != null) {
                this.f3417a = (e.b) bVar.clone();
            }
            if (bVar2 != null) {
                this.f3418b = (d.b) bVar2.clone();
            }
            if (bVar3 != null) {
                this.c = (g.b) bVar3.clone();
            }
        }

        public final void run() {
            if (!f.t) {
                try {
                    TelephonyManager telephonyManager = (TelephonyManager) f.this.f3414b.getSystemService("phone");
                    String unused = f.this.E = telephonyManager.getDeviceId();
                    String unused2 = f.this.F = telephonyManager.getSubscriberId();
                    String unused3 = f.this.G = telephonyManager.getLine1Number();
                    Pattern compile = Pattern.compile("[0-9a-zA-Z+-]*");
                    String unused4 = f.this.E = f.this.E == null ? "" : f.this.E;
                    if (compile.matcher(f.this.E).matches()) {
                        String unused5 = f.this.E = f.this.E == null ? "" : f.this.E;
                    } else {
                        String unused6 = f.this.E = "";
                    }
                    String unused7 = f.this.F = f.this.F == null ? "" : f.this.F;
                    if (compile.matcher(f.this.F).matches()) {
                        String unused8 = f.this.F = f.this.F == null ? "" : f.this.F;
                    } else {
                        String unused9 = f.this.F = "";
                    }
                    String unused10 = f.this.G = f.this.G == null ? "" : f.this.G;
                    if (compile.matcher(f.this.G).matches()) {
                        String unused11 = f.this.G = f.this.G == null ? "" : f.this.G;
                    } else {
                        String unused12 = f.this.G = "";
                    }
                } catch (Exception e) {
                }
                boolean unused13 = f.t = true;
                String unused14 = f.this.E = f.this.E == null ? "" : f.this.E;
                String unused15 = f.this.F = f.this.F == null ? "" : f.this.F;
                String unused16 = f.this.G = f.this.G == null ? "" : f.this.G;
                String unused17 = f.this.I = j.a(f.this.E == null ? "0123456789ABCDEF" : f.this.E);
            }
            f.this.q.sendMessage(f.this.q.obtainMessage(16, (("{\"version\":\"1.1.8\",\"address\":" + f.this.l) + ",\"source\":203,\"access_token\":\"" + f.this.I + "\",\"app_name\":" + "\"" + f.this.J + "\",\"bearing\":1") + ",\"attribute\":" + i.a(f.this.E, f.this.F, f.this.G, f.this.H, f.this.K) + ",\"location\":" + ((this.f3417a == null || !this.f3417a.a()) ? "{}" : i.a(this.f3417a)) + ",\"cells\":" + i.a(this.f3418b, f.this.d.b()) + ",\"wifis\":" + (f.this.g == 4 ? i.a(this.c) : "[]") + "}"));
        }
    }

    class b extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private String f3419a = null;

        /* renamed from: b  reason: collision with root package name */
        private String f3420b = null;
        private String c = null;

        public b(String str) {
            this.f3419a = str;
            this.f3420b = (f.this.D == 0 ? "http://lstest.map.soso.com/loc?c=1" : "http://lbs.map.qq.com/loc?c=1") + "&mars=" + f.this.m;
        }

        private String a(byte[] bArr, String str) {
            long unused = f.this.M = System.currentTimeMillis();
            StringBuffer stringBuffer = new StringBuffer();
            try {
                stringBuffer.append(new String(bArr, str));
                return stringBuffer.toString();
            } catch (Exception e) {
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.map.b.f.a(com.tencent.map.b.f, boolean):boolean
         arg types: [com.tencent.map.b.f, int]
         candidates:
          com.tencent.map.b.f.a(com.tencent.map.b.f, long):long
          com.tencent.map.b.f.a(com.tencent.map.b.f, com.tencent.map.b.f$a):com.tencent.map.b.f$a
          com.tencent.map.b.f.a(com.tencent.map.b.f, int):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, android.location.Location):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, com.tencent.map.b.d$b):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, com.tencent.map.b.e$b):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, com.tencent.map.b.g$b):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, java.lang.String):void
          com.tencent.map.b.f.a(double, double):void
          com.tencent.map.b.f.a(android.content.Context, com.tencent.map.a.a.b):boolean
          com.tencent.map.b.f.a(java.lang.String, java.lang.String):boolean
          com.tencent.map.b.b.a.a(double, double):void
          com.tencent.map.b.f.a(com.tencent.map.b.f, boolean):boolean */
        public final void run() {
            Message message = new Message();
            message.what = 8;
            try {
                byte[] a2 = j.a(this.f3419a.getBytes());
                boolean unused = f.this.p = true;
                n a3 = b.a(this.f3420b, "SOSO MAP LBS SDK", a2);
                boolean unused2 = f.this.p = false;
                this.c = a(j.b(a3.f3439a), a3.f3440b);
                if (this.c != null) {
                    message.arg1 = 0;
                    message.obj = this.c;
                } else {
                    message.arg1 = 1;
                }
            } catch (Exception e) {
                int i = 0;
                while (true) {
                    i++;
                    if (i > 3) {
                        break;
                    }
                    try {
                        sleep(1000);
                        byte[] a4 = j.a(this.f3419a.getBytes());
                        boolean unused3 = f.this.p = true;
                        n a5 = b.a(this.f3420b, "SOSO MAP LBS SDK", a4);
                        boolean unused4 = f.this.p = false;
                        this.c = a(j.b(a5.f3439a), a5.f3440b);
                        if (this.c != null) {
                            message.arg1 = 0;
                            message.obj = this.c;
                        } else {
                            message.arg1 = 1;
                        }
                    } catch (Exception e2) {
                    }
                }
                boolean unused5 = f.this.p = false;
                message.arg1 = 1;
            }
            f.j(f.this);
            f.this.q.sendMessage(message);
        }
    }

    class c extends Handler {
        public c() {
            super(Looper.getMainLooper());
        }

        public final void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    f.a(f.this, (e.b) message.obj);
                    return;
                case 2:
                    f.a(f.this, (d.b) message.obj);
                    return;
                case 3:
                    f.a(f.this, (g.b) message.obj);
                    return;
                case 4:
                    f.a(f.this, message.arg1);
                    return;
                case 5:
                    f.b(f.this, message.arg1);
                    return;
                case 6:
                    f.a(f.this, (Location) message.obj);
                    return;
                case 8:
                    if (message.arg1 == 0) {
                        f.this.a((String) message.obj);
                        return;
                    } else if (f.this.w == null || !f.this.w.a()) {
                        f.this.e();
                        return;
                    } else {
                        return;
                    }
                case 16:
                    if (message.obj != null) {
                        f.a(f.this, (String) message.obj);
                        a unused = f.this.s = null;
                        return;
                    }
                    return;
                case 256:
                    if (f.this.B == 1) {
                        f.this.d();
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    private f() {
        this.f3413a = 5000;
        this.f3414b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = 1024;
        this.g = 4;
        this.h = null;
        this.i = null;
        this.j = null;
        this.n = new byte[0];
        this.o = new byte[0];
        this.p = false;
        this.q = null;
        this.r = null;
        this.s = null;
        this.v = -1;
        this.w = null;
        this.x = null;
        this.y = null;
        this.z = null;
        this.A = null;
        this.B = 0;
        this.C = 0;
        this.D = 1;
        this.E = "";
        this.F = "";
        this.G = "";
        this.H = "";
        this.I = "";
        this.J = "";
        this.K = false;
        this.L = false;
        this.M = 0;
        this.N = null;
        this.O = new Runnable() {
            public final void run() {
                if (System.currentTimeMillis() - f.this.M >= 8000) {
                    if (!f.this.e.b() || !f.this.e.c()) {
                        f.this.d();
                    } else {
                        f.this.e.a(0);
                    }
                }
            }
        };
        this.P = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                if (!intent.getBooleanExtra("noConnectivity", false) && f.this.q != null) {
                    f.this.q.sendEmptyMessage(256);
                }
            }
        };
        this.c = new e();
        this.d = new d();
        this.e = new g();
    }

    public static synchronized f a() {
        f fVar;
        synchronized (f.class) {
            if (u == null) {
                u = new f();
            }
            fVar = u;
        }
        return fVar;
    }

    private static ArrayList<com.tencent.map.a.a.c> a(JSONArray jSONArray) throws Exception {
        int length = jSONArray.length();
        ArrayList<com.tencent.map.a.a.c> arrayList = new ArrayList<>();
        for (int i2 = 0; i2 < length; i2++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i2);
            arrayList.add(new com.tencent.map.a.a.c(jSONObject.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME), jSONObject.getString("addr"), jSONObject.getString("catalog"), jSONObject.getDouble("dist"), Double.parseDouble(jSONObject.getString("latitude")), Double.parseDouble(jSONObject.getString("longitude"))));
        }
        return arrayList;
    }

    static /* synthetic */ void a(f fVar, int i2) {
        if (i2 == 0) {
            fVar.w = null;
        }
        fVar.f = i2 == 0 ? 1 : 2;
        if (fVar.j != null) {
            fVar.j.a(fVar.f);
        }
    }

    static /* synthetic */ void a(f fVar, Location location) {
        if (location == null || location.getLatitude() > 359.0d || location.getLongitude() > 359.0d) {
            if (fVar.w == null || !fVar.w.a()) {
                fVar.e();
            } else {
                fVar.b(true);
            }
        }
        fVar.z = new com.tencent.map.a.a.d();
        fVar.z.z = 0;
        fVar.z.y = 0;
        fVar.z.f3390b = i.a(location.getLatitude(), 6);
        fVar.z.c = i.a(location.getLongitude(), 6);
        if (fVar.w != null && fVar.w.a()) {
            fVar.z.e = i.a((double) fVar.w.b().getAccuracy(), 1);
            fVar.z.d = i.a(fVar.w.b().getAltitude(), 1);
            fVar.z.f = i.a((double) fVar.w.b().getSpeed(), 1);
            fVar.z.g = i.a((double) fVar.w.b().getBearing(), 1);
            fVar.z.f3389a = 0;
        }
        fVar.z.x = true;
        if (!(fVar.l == 0 || fVar.A == null || fVar.B != 0)) {
            if ((fVar.l == 3 || fVar.l == 4) && fVar.l == fVar.A.z) {
                fVar.z.i = fVar.A.i;
                fVar.z.j = fVar.A.j;
                fVar.z.k = fVar.A.k;
                fVar.z.l = fVar.A.l;
                fVar.z.m = fVar.A.m;
                fVar.z.n = fVar.A.n;
                fVar.z.o = fVar.A.o;
                fVar.z.p = fVar.A.p;
                fVar.z.z = 3;
            }
            if (fVar.l == 4 && fVar.l == fVar.A.z && fVar.A.w != null) {
                fVar.z.w = new ArrayList<>();
                Iterator<com.tencent.map.a.a.c> it = fVar.A.w.iterator();
                while (it.hasNext()) {
                    fVar.z.w.add(new com.tencent.map.a.a.c(it.next()));
                }
                fVar.z.z = 4;
            }
            if (fVar.l == 7 && fVar.l == fVar.A.z) {
                fVar.z.z = 7;
                fVar.z.h = fVar.A.h;
                fVar.z.i = fVar.A.i;
                if (fVar.A.h == 0) {
                    fVar.z.j = fVar.A.j;
                    fVar.z.k = fVar.A.k;
                    fVar.z.l = fVar.A.l;
                    fVar.z.m = fVar.A.m;
                    fVar.z.n = fVar.A.n;
                    fVar.z.o = fVar.A.o;
                    fVar.z.p = fVar.A.p;
                } else {
                    fVar.z.q = fVar.A.q;
                    fVar.z.r = fVar.A.r;
                    fVar.z.s = fVar.A.s;
                    fVar.z.t = fVar.A.t;
                    fVar.z.u = fVar.A.u;
                    fVar.z.v = fVar.A.v;
                }
            }
        }
        if (fVar.B != 0 || fVar.A != null) {
            if (fVar.B != 0) {
                fVar.z.y = fVar.B;
            }
            if (System.currentTimeMillis() - fVar.v >= fVar.f3413a && fVar.j != null && fVar.k == 1) {
                fVar.j.a(fVar.z);
                fVar.v = System.currentTimeMillis();
            }
        }
    }

    static /* synthetic */ void a(f fVar, d.b bVar) {
        fVar.x = bVar;
        if (fVar.e == null || !fVar.e.b() || !fVar.e.c()) {
            if (fVar.C > 0 && !i.a(bVar.f3406a, bVar.f3407b, bVar.c, bVar.d, bVar.e)) {
                fVar.C--;
            }
            fVar.d();
            return;
        }
        fVar.e.a(0);
    }

    static /* synthetic */ void a(f fVar, e.b bVar) {
        if (bVar != null) {
            fVar.w = bVar;
            if (fVar.k == 1 && fVar.w != null && fVar.w.a()) {
                if (fVar.m == 0) {
                    fVar.b(false);
                } else if (fVar.m == 1 && fVar.i != null) {
                    b bVar2 = fVar.i;
                    double latitude = fVar.w.b().getLatitude();
                    double longitude = fVar.w.b().getLongitude();
                    Context context = fVar.f3414b;
                    bVar2.a(latitude, longitude, fVar);
                }
            }
        }
    }

    static /* synthetic */ void a(f fVar, g.b bVar) {
        if (bVar != null) {
            fVar.y = bVar;
            fVar.d();
        }
    }

    static /* synthetic */ void a(f fVar, String str) {
        byte[] bArr;
        if (!i.c(str)) {
            if (fVar.C > 0) {
                fVar.C--;
            } else if (fVar.k == 0 && fVar.j != null) {
                fVar.j.a(null, -1);
            } else if (fVar.k == 1 && fVar.j != null) {
                fVar.z = new com.tencent.map.a.a.d();
                fVar.B = 3;
                fVar.z.y = 3;
                fVar.z.z = -1;
                fVar.j.a(fVar.z);
            }
        } else if (fVar.k != 0 || fVar.j == null) {
            String b2 = fVar.h == null ? null : (fVar.x == null || fVar.y == null) ? null : fVar.h.b(fVar.x.f3407b, fVar.x.c, fVar.x.d, fVar.x.e, fVar.y.a());
            if (b2 != null) {
                fVar.a(b2);
                return;
            }
            if (!(fVar.h == null || fVar.x == null || fVar.y == null)) {
                fVar.h.a(fVar.x.f3407b, fVar.x.c, fVar.x.d, fVar.x.e, fVar.y.a());
            }
            if (!fVar.p) {
                if (fVar.r != null) {
                    fVar.r.interrupt();
                }
                fVar.r = null;
                fVar.r = new b(str);
                fVar.r.start();
            }
        } else {
            try {
                bArr = str.getBytes();
            } catch (Exception e2) {
                bArr = null;
            }
            fVar.j.a(bArr, 0);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        double d2;
        int i2 = 0;
        try {
            this.z = new com.tencent.map.a.a.d();
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = jSONObject.getJSONObject("location");
            this.z.f3389a = 1;
            this.z.f3390b = i.a(jSONObject2.getDouble("latitude"), 6);
            this.z.c = i.a(jSONObject2.getDouble("longitude"), 6);
            this.z.d = i.a(jSONObject2.getDouble(Parameters.ALTITUDE), 1);
            this.z.e = i.a(jSONObject2.getDouble("accuracy"), 1);
            this.z.x = this.m == 1;
            String string = jSONObject.getString(Parameters.BEARING);
            int i3 = -100;
            if (string != null && string.split(MiPushClient.ACCEPT_TIME_SEPARATOR).length > 1) {
                i2 = Integer.parseInt(string.split(MiPushClient.ACCEPT_TIME_SEPARATOR)[1]);
            }
            if (this.x != null) {
                i3 = this.x.f;
            }
            com.tencent.map.a.a.d dVar = this.z;
            double d3 = this.z.e;
            if (i2 >= 6) {
                d2 = 40.0d;
            } else if (i2 == 5) {
                d2 = 60.0d;
            } else if (i2 == 4) {
                d2 = 70.0d;
            } else if (i2 == 3) {
                d2 = 90.0d;
            } else if (i2 == 2) {
                d2 = 110.0d;
            } else {
                d2 = (double) ((i3 < -72 || i2 != 0) ? d3 <= 100.0d ? ((int) (((d3 - 1.0d) / 10.0d) + 1.0d)) * 10 : (d3 <= 100.0d || d3 > 800.0d) ? ((int) ((0.8d * d3) / 10.0d)) * 10 : ((int) ((0.85d * d3) / 10.0d)) * 10 : ((int) ((0.45d * d3) / 10.0d)) * 10);
            }
            dVar.e = d2;
            this.z.z = 0;
            if ((this.l == 3 || this.l == 4) && this.m == 1) {
                JSONObject jSONObject3 = jSONObject.getJSONObject("details").getJSONObject("subnation");
                this.z.a(jSONObject3.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME));
                this.z.m = jSONObject3.getString("town");
                this.z.n = jSONObject3.getString("village");
                this.z.o = jSONObject3.getString("street");
                this.z.p = jSONObject3.getString("street_no");
                this.z.z = 3;
                this.z.h = 0;
            }
            if (this.l == 4 && this.m == 1) {
                this.z.w = a(jSONObject.getJSONObject("details").getJSONArray("poilist"));
                this.z.z = 4;
            }
            if (this.l == 7 && this.m == 1) {
                JSONObject jSONObject4 = jSONObject.getJSONObject("details");
                int i4 = jSONObject4.getInt("stat");
                JSONObject jSONObject5 = jSONObject4.getJSONObject("subnation");
                if (i4 == 0) {
                    this.z.a(jSONObject5.getString(SelectCountryActivity.EXTRA_COUNTRY_NAME));
                    this.z.m = jSONObject5.getString("town");
                    this.z.n = jSONObject5.getString("village");
                    this.z.o = jSONObject5.getString("street");
                    this.z.p = jSONObject5.getString("street_no");
                } else if (i4 == 1) {
                    this.z.i = jSONObject5.getString("nation");
                    this.z.q = jSONObject5.getString("admin_level_1");
                    this.z.r = jSONObject5.getString("admin_level_2");
                    this.z.s = jSONObject5.getString("admin_level_3");
                    this.z.t = jSONObject5.getString("locality");
                    this.z.u = jSONObject5.getString("sublocality");
                    this.z.v = jSONObject5.getString("route");
                }
                this.z.h = i4;
                this.z.z = 7;
            }
            this.z.y = 0;
            this.A = new com.tencent.map.a.a.d(this.z);
            this.B = 0;
            if (this.h != null) {
                this.h.a(str);
            }
        } catch (Exception e2) {
            this.z = new com.tencent.map.a.a.d();
            this.z.z = -1;
            this.z.y = 2;
            this.B = 2;
        }
        if (this.j != null && this.k == 1) {
            if (this.w == null || !this.w.a()) {
                this.j.a(this.z);
                this.v = System.currentTimeMillis();
            }
        }
    }

    static /* synthetic */ void b(f fVar, int i2) {
        int i3 = 3;
        if (i2 == 3) {
            i3 = 4;
        }
        fVar.g = i3;
        if (fVar.j != null) {
            fVar.j.a(fVar.g);
        }
    }

    private void b(boolean z2) {
        if (this.w != null && this.w.a()) {
            Location b2 = this.w.b();
            this.z = new com.tencent.map.a.a.d();
            this.z.f3390b = i.a(b2.getLatitude(), 6);
            this.z.c = i.a(b2.getLongitude(), 6);
            this.z.d = i.a(b2.getAltitude(), 1);
            this.z.e = i.a((double) b2.getAccuracy(), 1);
            this.z.f = i.a((double) b2.getSpeed(), 1);
            this.z.g = i.a((double) b2.getBearing(), 1);
            this.z.f3389a = 0;
            this.z.x = false;
            if (!z2) {
                this.z.y = 0;
            } else {
                this.z.y = 1;
            }
            this.z.z = 0;
            this.A = new com.tencent.map.a.a.d(this.z);
            this.B = 0;
            if (System.currentTimeMillis() - this.v >= this.f3413a && this.j != null && this.k == 1) {
                this.j.a(this.z);
                this.v = System.currentTimeMillis();
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.s == null) {
            this.s = new a(this.w, this.x, this.y);
            this.s.start();
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.z = new com.tencent.map.a.a.d();
        this.B = 1;
        this.z.y = 1;
        this.z.z = -1;
        this.z.f3389a = 1;
        if (this.j != null && this.k == 1) {
            this.j.a(this.z);
        }
    }

    static /* synthetic */ void j(f fVar) {
    }

    public final void a(double d2, double d3) {
        synchronized (this.o) {
            Message obtainMessage = this.q.obtainMessage(6);
            Location location = new Location("Deflect");
            location.setLatitude(d2);
            location.setLongitude(d3);
            obtainMessage.obj = location;
            this.q.sendMessage(obtainMessage);
        }
    }

    public final void a(int i2) {
        synchronized (this.o) {
            this.q.sendMessage(this.q.obtainMessage(4, i2, 0));
        }
    }

    public final void a(d.b bVar) {
        synchronized (this.o) {
            this.q.sendMessage(this.q.obtainMessage(2, bVar));
        }
    }

    public final void a(e.b bVar) {
        synchronized (this.o) {
            this.q.sendMessage(this.q.obtainMessage(1, bVar));
        }
    }

    public final void a(g.b bVar) {
        synchronized (this.o) {
            this.q.sendMessage(this.q.obtainMessage(3, bVar));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:?, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Context r9, com.tencent.map.a.a.b r10) {
        /*
            r8 = this;
            r2 = 1
            r1 = 0
            byte[] r3 = r8.n
            monitor-enter(r3)
            if (r9 == 0) goto L_0x0009
            if (r10 != 0) goto L_0x000c
        L_0x0009:
            monitor-exit(r3)
            r0 = r1
        L_0x000b:
            return r0
        L_0x000c:
            java.lang.String r0 = r8.J     // Catch:{ all -> 0x00e5 }
            boolean r0 = com.tencent.map.b.i.a(r0)     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x0017
            monitor-exit(r3)     // Catch:{ all -> 0x00e5 }
            r0 = r1
            goto L_0x000b
        L_0x0017:
            com.tencent.map.b.f$c r0 = new com.tencent.map.b.f$c     // Catch:{ all -> 0x00e5 }
            r0.<init>()     // Catch:{ all -> 0x00e5 }
            r8.q = r0     // Catch:{ all -> 0x00e5 }
            android.os.Handler r0 = new android.os.Handler     // Catch:{ all -> 0x00e5 }
            android.os.Looper r4 = android.os.Looper.getMainLooper()     // Catch:{ all -> 0x00e5 }
            r0.<init>(r4)     // Catch:{ all -> 0x00e5 }
            r8.N = r0     // Catch:{ all -> 0x00e5 }
            r8.f3414b = r9     // Catch:{ all -> 0x00e5 }
            r8.j = r10     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.l r0 = com.tencent.map.b.l.a()     // Catch:{ all -> 0x00e5 }
            android.content.Context r4 = r8.f3414b     // Catch:{ all -> 0x00e5 }
            android.content.Context r4 = r4.getApplicationContext()     // Catch:{ all -> 0x00e5 }
            r0.a(r4)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "connectivity"
            java.lang.Object r0 = r9.getSystemService(r0)     // Catch:{ Exception -> 0x00e8 }
            android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x0054
            android.net.NetworkInfo r4 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x00e8 }
            if (r4 == 0) goto L_0x0054
            android.net.NetworkInfo r0 = r0.getActiveNetworkInfo()     // Catch:{ Exception -> 0x00e8 }
            boolean r0 = r0.isRoaming()     // Catch:{ Exception -> 0x00e8 }
            r8.K = r0     // Catch:{ Exception -> 0x00e8 }
        L_0x0054:
            android.content.Context r0 = r8.f3414b     // Catch:{ Exception -> 0x00e8 }
            android.content.BroadcastReceiver r4 = r8.P     // Catch:{ Exception -> 0x00e8 }
            android.content.IntentFilter r5 = new android.content.IntentFilter     // Catch:{ Exception -> 0x00e8 }
            java.lang.String r6 = "android.net.conn.CONNECTIVITY_CHANGE"
            r5.<init>(r6)     // Catch:{ Exception -> 0x00e8 }
            r0.registerReceiver(r4, r5)     // Catch:{ Exception -> 0x00e8 }
        L_0x0062:
            com.tencent.map.a.a.b r0 = r8.j     // Catch:{ all -> 0x00e5 }
            int r0 = r0.a()     // Catch:{ all -> 0x00e5 }
            r8.k = r0     // Catch:{ all -> 0x00e5 }
            com.tencent.map.a.a.b r0 = r8.j     // Catch:{ all -> 0x00e5 }
            int r0 = r0.b()     // Catch:{ all -> 0x00e5 }
            r8.l = r0     // Catch:{ all -> 0x00e5 }
            com.tencent.map.a.a.b r0 = r8.j     // Catch:{ all -> 0x00e5 }
            int r0 = r0.c()     // Catch:{ all -> 0x00e5 }
            r8.m = r0     // Catch:{ all -> 0x00e5 }
            r4 = -1
            r8.v = r4     // Catch:{ all -> 0x00e5 }
            int r0 = r8.l     // Catch:{ all -> 0x00e5 }
            r4 = 7
            if (r0 != r4) goto L_0x0086
            r0 = 0
            r8.l = r0     // Catch:{ all -> 0x00e5 }
        L_0x0086:
            r0 = 0
            r8.L = r0     // Catch:{ all -> 0x00e5 }
            r0 = 1
            r8.D = r0     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.e r0 = r8.c     // Catch:{ all -> 0x00e5 }
            android.content.Context r4 = r8.f3414b     // Catch:{ all -> 0x00e5 }
            boolean r0 = r0.a(r8, r4)     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.d r4 = r8.d     // Catch:{ all -> 0x00e5 }
            android.content.Context r5 = r8.f3414b     // Catch:{ all -> 0x00e5 }
            boolean r4 = r4.a(r5, r8)     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.g r5 = r8.e     // Catch:{ all -> 0x00e5 }
            android.content.Context r6 = r8.f3414b     // Catch:{ all -> 0x00e5 }
            r7 = 1
            boolean r5 = r5.a(r6, r8, r7)     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.c r6 = com.tencent.map.b.c.a()     // Catch:{ all -> 0x00e5 }
            r8.h = r6     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.b r6 = com.tencent.map.b.b.a()     // Catch:{ all -> 0x00e5 }
            r8.i = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.w = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.x = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.y = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.z = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.A = r6     // Catch:{ all -> 0x00e5 }
            r6 = 0
            r8.B = r6     // Catch:{ all -> 0x00e5 }
            com.tencent.map.b.c r6 = r8.h     // Catch:{ all -> 0x00e5 }
            if (r6 == 0) goto L_0x00cc
            com.tencent.map.b.c r6 = r8.h     // Catch:{ all -> 0x00e5 }
            r6.b()     // Catch:{ all -> 0x00e5 }
        L_0x00cc:
            r6 = 1
            r8.C = r6     // Catch:{ all -> 0x00e5 }
            if (r0 == 0) goto L_0x00d9
            int r0 = r8.m     // Catch:{ all -> 0x00e5 }
            if (r0 != 0) goto L_0x00d9
            monitor-exit(r3)     // Catch:{ all -> 0x00e5 }
            r0 = r2
            goto L_0x000b
        L_0x00d9:
            if (r4 != 0) goto L_0x00dd
            if (r5 == 0) goto L_0x00e1
        L_0x00dd:
            monitor-exit(r3)
            r0 = r2
            goto L_0x000b
        L_0x00e1:
            monitor-exit(r3)
            r0 = r1
            goto L_0x000b
        L_0x00e5:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x00e8:
            r0 = move-exception
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.map.b.f.a(android.content.Context, com.tencent.map.a.a.b):boolean");
    }

    public final boolean a(String str, String str2) {
        boolean z2;
        synchronized (this.n) {
            if (a.a().a(str, str2)) {
                this.J = str;
                z2 = true;
            } else {
                z2 = false;
            }
        }
        return z2;
    }

    public final void b() {
        synchronized (this.n) {
            try {
                if (this.j != null) {
                    this.j = null;
                    this.N.removeCallbacks(this.O);
                    this.f3414b.unregisterReceiver(this.P);
                    this.c.a();
                    this.d.a();
                    this.e.a();
                }
            } catch (Exception e2) {
            }
        }
    }

    public final void b(int i2) {
        synchronized (this.o) {
            this.q.sendMessage(this.q.obtainMessage(5, i2, 0));
        }
    }
}
