package com.tencent.open.yyb;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public class ShareModel implements Parcelable {
    public static final Parcelable.Creator<ShareModel> CREATOR = new Parcelable.Creator<ShareModel>() {
        /* renamed from: a */
        public ShareModel createFromParcel(Parcel parcel) {
            ShareModel shareModel = new ShareModel();
            shareModel.f3597a = parcel.readString();
            shareModel.f3598b = parcel.readString();
            shareModel.c = parcel.readString();
            shareModel.d = parcel.readString();
            return shareModel;
        }

        /* renamed from: a */
        public ShareModel[] newArray(int i) {
            return null;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f3597a;

    /* renamed from: b  reason: collision with root package name */
    public String f3598b;
    public String c;
    public String d;

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3597a);
        parcel.writeString(this.f3598b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
    }
}
