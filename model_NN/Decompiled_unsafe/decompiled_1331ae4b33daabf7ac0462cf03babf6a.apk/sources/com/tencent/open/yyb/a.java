package com.tencent.open.yyb;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.tencent.connect.common.Constants;

/* compiled from: ProGuard */
public class a {

    /* renamed from: com.tencent.open.yyb.a$a  reason: collision with other inner class name */
    /* compiled from: ProGuard */
    public static class C0056a {

        /* renamed from: a  reason: collision with root package name */
        public String f3599a;

        /* renamed from: b  reason: collision with root package name */
        public String f3600b;
        public long c;
    }

    public static void a(Context context, String str, String str2, String str3, String str4) {
        if (!TextUtils.isEmpty(str)) {
            CookieSyncManager.createInstance(context);
            CookieManager instance = CookieManager.getInstance();
            instance.setAcceptCookie(true);
            String str5 = null;
            if (Uri.parse(str).getHost().toLowerCase().endsWith(".qq.com")) {
                str5 = ".qq.com";
            }
            instance.setCookie(str, b("logintype", "MOBILEQ", str5));
            instance.setCookie(str, b("qopenid", str2, str5));
            instance.setCookie(str, b("qaccesstoken", str3, str5));
            instance.setCookie(str, b("openappid", str4, str5));
            CookieSyncManager.getInstance().sync();
        }
    }

    private static String b(String str, String str2, String str3) {
        String str4 = str + "=" + str2;
        if (str3 == null) {
            return str4;
        }
        return (str4 + "; path=/") + "; domain=" + str3;
    }

    public static Drawable a(String str, Context context) {
        return a(str, context, new Rect(0, 0, 0, 0));
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0096 A[SYNTHETIC, Splitter:B:44:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00ac A[SYNTHETIC, Splitter:B:52:0x00ac] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.drawable.Drawable a(java.lang.String r8, android.content.Context r9, android.graphics.Rect r10) {
        /*
            r6 = 0
            android.content.Context r1 = r9.getApplicationContext()
            android.content.res.AssetManager r0 = r1.getAssets()
            java.io.InputStream r7 = r0.open(r8)     // Catch:{ OutOfMemoryError -> 0x0068, IOException -> 0x0088, all -> 0x00a8 }
            if (r7 != 0) goto L_0x0021
            if (r7 == 0) goto L_0x0014
            r7.close()     // Catch:{ IOException -> 0x0015 }
        L_0x0014:
            return r6
        L_0x0015:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "openSDK_LOG.AppbarUtil"
            java.lang.String r2 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r1, r2, r0)
            goto L_0x0014
        L_0x0021:
            java.lang.String r0 = ".9.png"
            boolean r0 = r8.endsWith(r0)     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            if (r0 == 0) goto L_0x0057
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r7)     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            if (r2 == 0) goto L_0x0045
            byte[] r3 = r2.getNinePatchChunk()     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            android.graphics.drawable.NinePatchDrawable r0 = new android.graphics.drawable.NinePatchDrawable     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            r5 = 0
            r4 = r10
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
        L_0x003e:
            if (r7 == 0) goto L_0x0043
            r7.close()     // Catch:{ IOException -> 0x005c }
        L_0x0043:
            r6 = r0
            goto L_0x0014
        L_0x0045:
            if (r7 == 0) goto L_0x0014
            r7.close()     // Catch:{ IOException -> 0x004b }
            goto L_0x0014
        L_0x004b:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "openSDK_LOG.AppbarUtil"
            java.lang.String r2 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r1, r2, r0)
            goto L_0x0014
        L_0x0057:
            android.graphics.drawable.Drawable r0 = android.graphics.drawable.Drawable.createFromStream(r7, r8)     // Catch:{ OutOfMemoryError -> 0x00c3, IOException -> 0x00c1 }
            goto L_0x003e
        L_0x005c:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r2 = "openSDK_LOG.AppbarUtil"
            java.lang.String r3 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r2, r3, r1)
            goto L_0x0043
        L_0x0068:
            r0 = move-exception
            r1 = r6
        L_0x006a:
            r0.printStackTrace()     // Catch:{ all -> 0x00be }
            java.lang.String r2 = "openSDK_LOG.AppbarUtil"
            java.lang.String r3 = "-->(AppbarUtil)getDrawable : OutOfMemoryError"
            com.tencent.open.a.f.b(r2, r3, r0)     // Catch:{ all -> 0x00be }
            if (r1 == 0) goto L_0x00c6
            r1.close()     // Catch:{ IOException -> 0x007b }
            r0 = r6
            goto L_0x0043
        L_0x007b:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "openSDK_LOG.AppbarUtil"
            java.lang.String r2 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r1, r2, r0)
            r0 = r6
            goto L_0x0043
        L_0x0088:
            r0 = move-exception
            r7 = r6
        L_0x008a:
            r0.printStackTrace()     // Catch:{ all -> 0x00bc }
            java.lang.String r1 = "openSDK_LOG.AppbarUtil"
            java.lang.String r2 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r1, r2, r0)     // Catch:{ all -> 0x00bc }
            if (r7 == 0) goto L_0x00c6
            r7.close()     // Catch:{ IOException -> 0x009b }
            r0 = r6
            goto L_0x0043
        L_0x009b:
            r0 = move-exception
            r0.printStackTrace()
            java.lang.String r1 = "openSDK_LOG.AppbarUtil"
            java.lang.String r2 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r1, r2, r0)
            r0 = r6
            goto L_0x0043
        L_0x00a8:
            r0 = move-exception
            r7 = r6
        L_0x00aa:
            if (r7 == 0) goto L_0x00af
            r7.close()     // Catch:{ IOException -> 0x00b0 }
        L_0x00af:
            throw r0
        L_0x00b0:
            r1 = move-exception
            r1.printStackTrace()
            java.lang.String r2 = "openSDK_LOG.AppbarUtil"
            java.lang.String r3 = "-->(AppbarUtil)getDrawable : IOException"
            com.tencent.open.a.f.b(r2, r3, r1)
            goto L_0x00af
        L_0x00bc:
            r0 = move-exception
            goto L_0x00aa
        L_0x00be:
            r0 = move-exception
            r7 = r1
            goto L_0x00aa
        L_0x00c1:
            r0 = move-exception
            goto L_0x008a
        L_0x00c3:
            r0 = move-exception
            r1 = r7
            goto L_0x006a
        L_0x00c6:
            r0 = r6
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.yyb.a.a(java.lang.String, android.content.Context, android.graphics.Rect):android.graphics.drawable.Drawable");
    }

    public static void a(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        bundle.putString("uin", Constants.DEFAULT_UIN);
        bundle.putString("action", str2);
        bundle.putString("appid", str);
        bundle.putString("via", str3);
        new b().execute(bundle);
    }

    /* compiled from: ProGuard */
    private static class b extends AsyncTask<Bundle, Void, Void> {
        private b() {
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0019;
         */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void doInBackground(android.os.Bundle... r6) {
            /*
                r5 = this;
                r4 = 0
                if (r6 != 0) goto L_0x0004
            L_0x0003:
                return r4
            L_0x0004:
                java.lang.String r1 = "http://analy.qq.com/cgi-bin/mapp_apptrace"
                int r0 = r6.length
                r2 = 2
                if (r0 != r2) goto L_0x0054
                r0 = 1
                r0 = r6[r0]
                java.lang.String r2 = "uri"
                java.lang.String r0 = r0.getString(r2)
                boolean r2 = android.text.TextUtils.isEmpty(r0)
                if (r2 != 0) goto L_0x0054
            L_0x0019:
                r1 = 0
                java.lang.String r2 = "GET"
                r3 = 0
                r3 = r6[r3]     // Catch:{ Exception -> 0x0048 }
                com.tencent.open.utils.Util$Statistic r0 = com.tencent.open.utils.HttpUtils.openUrl2(r1, r0, r2, r3)     // Catch:{ Exception -> 0x0048 }
                java.lang.String r0 = r0.response     // Catch:{ Exception -> 0x0048 }
                org.json.JSONObject r0 = com.tencent.open.utils.Util.parseJson(r0)     // Catch:{ Exception -> 0x0048 }
                java.lang.String r1 = "ret"
                int r0 = r0.getInt(r1)     // Catch:{ Exception -> 0x0048 }
                java.lang.String r1 = "openSDK_LOG.AppbarUtil"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0048 }
                r2.<init>()     // Catch:{ Exception -> 0x0048 }
                java.lang.String r3 = "-->(ViaAsyncTask)doInBackground : ret = "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ Exception -> 0x0048 }
                java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ Exception -> 0x0048 }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0048 }
                com.tencent.open.a.f.b(r1, r0)     // Catch:{ Exception -> 0x0048 }
                goto L_0x0003
            L_0x0048:
                r0 = move-exception
                java.lang.String r1 = "openSDK_LOG.AppbarUtil"
                java.lang.String r2 = "-->(ViaAsyncTask)doInBackground : Exception = "
                com.tencent.open.a.f.b(r1, r2, r0)
                r0.printStackTrace()
                goto L_0x0003
            L_0x0054:
                r0 = r1
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.yyb.a.b.doInBackground(android.os.Bundle[]):java.lang.Void");
        }
    }
}
