package com.tencent.open.a;

import com.tencent.open.a.d;

/* compiled from: ProGuard */
public abstract class i {

    /* renamed from: a  reason: collision with root package name */
    private volatile int f3517a;

    /* renamed from: b  reason: collision with root package name */
    private volatile boolean f3518b;
    private h c;

    /* access modifiers changed from: protected */
    public abstract void a(int i, Thread thread, long j, String str, String str2, Throwable th);

    public i() {
        this(c.f3507a, true, h.f3516a);
    }

    public i(int i, boolean z, h hVar) {
        this.f3517a = c.f3507a;
        this.f3518b = true;
        this.c = h.f3516a;
        a(i);
        a(z);
        a(hVar);
    }

    public void b(int i, Thread thread, long j, String str, String str2, Throwable th) {
        if (d() && d.a.a(this.f3517a, i)) {
            a(i, thread, j, str, str2, th);
        }
    }

    public void a(int i) {
        this.f3517a = i;
    }

    public boolean d() {
        return this.f3518b;
    }

    public void a(boolean z) {
        this.f3518b = z;
    }

    public h e() {
        return this.c;
    }

    public void a(h hVar) {
        this.c = hVar;
    }
}
