package com.tencent.open.utils;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: ProGuard */
public final class ThreadManager {
    public static final Executor NETWORK_EXECUTOR = a();

    /* renamed from: a  reason: collision with root package name */
    private static Handler f3567a;

    /* renamed from: b  reason: collision with root package name */
    private static Object f3568b = new Object();
    private static Handler c;
    private static HandlerThread d;
    private static Handler e;
    private static HandlerThread f;

    private static Executor a() {
        Executor threadPoolExecutor;
        Executor executor;
        if (Build.VERSION.SDK_INT >= 11) {
            executor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue());
        } else {
            try {
                Field declaredField = AsyncTask.class.getDeclaredField("sExecutor");
                declaredField.setAccessible(true);
                threadPoolExecutor = (Executor) declaredField.get(null);
            } catch (Exception e2) {
                threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, new LinkedBlockingQueue());
            }
            executor = threadPoolExecutor;
        }
        if (executor instanceof ThreadPoolExecutor) {
            ((ThreadPoolExecutor) executor).setCorePoolSize(3);
        }
        return executor;
    }

    public static void init() {
    }

    public static void executeOnNetWorkThread(Runnable runnable) {
        try {
            NETWORK_EXECUTOR.execute(runnable);
        } catch (RejectedExecutionException e2) {
        }
    }

    public static Handler getMainHandler() {
        if (f3567a == null) {
            synchronized (f3568b) {
                if (f3567a == null) {
                    f3567a = new Handler(Looper.getMainLooper());
                }
            }
        }
        return f3567a;
    }

    public static Handler getFileThreadHandler() {
        if (e == null) {
            synchronized (ThreadManager.class) {
                f = new HandlerThread("SDK_FILE_RW");
                f.start();
                e = new Handler(f.getLooper());
            }
        }
        return e;
    }

    public static Looper getFileThreadLooper() {
        return getFileThreadHandler().getLooper();
    }

    public static Thread getSubThread() {
        if (d == null) {
            getSubThreadHandler();
        }
        return d;
    }

    public static Handler getSubThreadHandler() {
        if (c == null) {
            synchronized (ThreadManager.class) {
                d = new HandlerThread("SDK_SUB");
                d.start();
                c = new Handler(d.getLooper());
            }
        }
        return c;
    }

    public static Looper getSubThreadLooper() {
        return getSubThreadHandler().getLooper();
    }

    public static void executeOnSubThread(Runnable runnable) {
        getSubThreadHandler().post(runnable);
    }

    public static void executeOnFileThread(Runnable runnable) {
        getFileThreadHandler().post(runnable);
    }

    public static Executor newSerialExecutor() {
        return new SerialExecutor();
    }

    /* compiled from: ProGuard */
    private static class SerialExecutor implements Executor {

        /* renamed from: a  reason: collision with root package name */
        final Queue<Runnable> f3569a;

        /* renamed from: b  reason: collision with root package name */
        Runnable f3570b;

        private SerialExecutor() {
            this.f3569a = new LinkedList();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f3569a.offer(new Runnable() {
                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        SerialExecutor.this.a();
                    }
                }
            });
            if (this.f3570b == null) {
                a();
            }
        }

        /* access modifiers changed from: protected */
        public synchronized void a() {
            Runnable poll = this.f3569a.poll();
            this.f3570b = poll;
            if (poll != null) {
                ThreadManager.NETWORK_EXECUTOR.execute(this.f3570b);
            }
        }
    }
}
