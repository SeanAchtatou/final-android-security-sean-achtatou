package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: ProGuard */
public final class ZipShort implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private int f3579a;

    public ZipShort(byte[] bArr) {
        this(bArr, 0);
    }

    public ZipShort(byte[] bArr, int i) {
        this.f3579a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f3579a += bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
    }

    public ZipShort(int i) {
        this.f3579a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ZipShort) || this.f3579a != ((ZipShort) obj).getValue()) {
            return false;
        }
        return true;
    }

    public byte[] getBytes() {
        return new byte[]{(byte) (this.f3579a & 255), (byte) ((this.f3579a & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    public int getValue() {
        return this.f3579a;
    }

    public int hashCode() {
        return this.f3579a;
    }
}
