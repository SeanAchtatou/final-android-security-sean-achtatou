package com.tencent.connect.a;

import android.content.Context;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.utils.OpenConfig;
import java.lang.reflect.Method;

/* compiled from: ProGuard */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Class<?> f3300a = null;

    /* renamed from: b  reason: collision with root package name */
    private static Class<?> f3301b = null;
    private static Method c = null;
    private static Method d = null;
    private static Method e = null;
    private static Method f = null;
    private static boolean g = false;

    public static boolean a(Context context, QQToken qQToken) {
        return OpenConfig.getInstance(context, qQToken.getAppId()).getBoolean("Common_ta_enable");
    }

    public static void b(Context context, QQToken qQToken) {
        try {
            if (a(context, qQToken)) {
                f.invoke(f3300a, true);
                return;
            }
            f.invoke(f3300a, false);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void c(Context context, QQToken qQToken) {
        String str = "Aqc" + qQToken.getAppId();
        try {
            f3300a = Class.forName("com.tencent.stat.StatConfig");
            f3301b = Class.forName("com.tencent.stat.StatService");
            c = f3301b.getMethod("reportQQ", Context.class, String.class);
            d = f3301b.getMethod("trackCustomEvent", Context.class, String.class, String[].class);
            e = f3301b.getMethod("commitEvents", Context.class, Integer.TYPE);
            f = f3300a.getMethod("setEnableStatService", Boolean.TYPE);
            b(context, qQToken);
            f3300a.getMethod("setAutoExceptionCaught", Boolean.TYPE).invoke(f3300a, false);
            f3300a.getMethod("setEnableSmartReporting", Boolean.TYPE).invoke(f3300a, true);
            f3300a.getMethod("setSendPeriodMinutes", Integer.TYPE).invoke(f3300a, 1440);
            Class<?> cls = Class.forName("com.tencent.stat.StatReportStrategy");
            f3300a.getMethod("setStatSendStrategy", cls).invoke(f3300a, cls.getField("PERIOD").get(null));
            f3301b.getMethod("startStatService", Context.class, String.class, String.class).invoke(f3301b, context, str, Class.forName("com.tencent.stat.common.StatConstants").getField("VERSION").get(null));
            g = true;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public static void d(Context context, QQToken qQToken) {
        if (g) {
            b(context, qQToken);
            if (qQToken.getOpenId() != null) {
                try {
                    c.invoke(f3301b, context, qQToken.getOpenId());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public static void a(Context context, QQToken qQToken, String str, String... strArr) {
        if (g) {
            b(context, qQToken);
            try {
                d.invoke(f3301b, context, str, strArr);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
