package com.tencent.connect.auth;

/* compiled from: ProGuard */
public class QQToken {
    public static final int AUTH_QQ = 2;
    public static final int AUTH_QZONE = 3;
    public static final int AUTH_WEB = 1;

    /* renamed from: a  reason: collision with root package name */
    private String f3337a;

    /* renamed from: b  reason: collision with root package name */
    private String f3338b;
    private String c;
    private int d = 1;
    private long e = -1;

    public QQToken(String str) {
        this.f3337a = str;
    }

    public boolean isSessionValid() {
        return this.f3338b != null && System.currentTimeMillis() < this.e;
    }

    public String getAppId() {
        return this.f3337a;
    }

    public void setAppId(String str) {
        this.f3337a = str;
    }

    public String getAccessToken() {
        return this.f3338b;
    }

    public void setAccessToken(String str, String str2) throws NumberFormatException {
        this.f3338b = str;
        this.e = 0;
        if (str2 != null) {
            this.e = System.currentTimeMillis() + (Long.parseLong(str2) * 1000);
        }
    }

    public String getOpenId() {
        return this.c;
    }

    public void setOpenId(String str) {
        this.c = str;
    }

    public int getAuthSource() {
        return this.d;
    }

    public void setAuthSource(int i) {
        this.d = i;
    }

    public long getExpireTimeInSecond() {
        return this.e;
    }
}
