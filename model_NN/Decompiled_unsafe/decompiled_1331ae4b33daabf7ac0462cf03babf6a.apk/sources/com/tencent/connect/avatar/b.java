package com.tencent.connect.avatar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/* compiled from: ProGuard */
public class b extends View {

    /* renamed from: a  reason: collision with root package name */
    private Rect f3354a;

    /* renamed from: b  reason: collision with root package name */
    private Paint f3355b;

    public b(Context context) {
        super(context);
        b();
    }

    private void b() {
        this.f3355b = new Paint();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect a2 = a();
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        this.f3355b.setStyle(Paint.Style.FILL);
        this.f3355b.setColor(Color.argb(100, 0, 0, 0));
        canvas.drawRect(0.0f, 0.0f, (float) measuredWidth, (float) a2.top, this.f3355b);
        canvas.drawRect(0.0f, (float) a2.bottom, (float) measuredWidth, (float) measuredHeight, this.f3355b);
        canvas.drawRect(0.0f, (float) a2.top, (float) a2.left, (float) a2.bottom, this.f3355b);
        canvas.drawRect((float) a2.right, (float) a2.top, (float) measuredWidth, (float) a2.bottom, this.f3355b);
        canvas.drawColor(Color.argb(100, 0, 0, 0));
        this.f3355b.setStyle(Paint.Style.STROKE);
        this.f3355b.setColor(-1);
        canvas.drawRect((float) a2.left, (float) a2.top, (float) (a2.right - 1), (float) a2.bottom, this.f3355b);
    }

    public Rect a() {
        if (this.f3354a == null) {
            this.f3354a = new Rect();
            int measuredWidth = getMeasuredWidth();
            int measuredHeight = getMeasuredHeight();
            int min = Math.min(Math.min((measuredHeight - 60) - 80, measuredWidth), 640);
            int i = (measuredWidth - min) / 2;
            int i2 = (measuredHeight - min) / 2;
            this.f3354a.set(i, i2, i + min, min + i2);
        }
        return this.f3354a;
    }
}
