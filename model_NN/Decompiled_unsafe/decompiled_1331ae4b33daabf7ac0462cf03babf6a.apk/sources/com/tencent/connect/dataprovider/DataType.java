package com.tencent.connect.dataprovider;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: ProGuard */
public final class DataType {
    public static final int CONTENT_AND_IMAGE_PATH = 1;
    public static final int CONTENT_AND_VIDEO_PATH = 2;
    public static final int CONTENT_ONLY = 4;

    /* compiled from: ProGuard */
    public static class TextAndMediaPath implements Parcelable {
        public static final Parcelable.Creator<TextAndMediaPath> CREATOR = new Parcelable.Creator<TextAndMediaPath>() {
            public TextAndMediaPath createFromParcel(Parcel parcel) {
                return new TextAndMediaPath(parcel);
            }

            public TextAndMediaPath[] newArray(int i) {
                return new TextAndMediaPath[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f3362a;

        /* renamed from: b  reason: collision with root package name */
        private String f3363b;

        public TextAndMediaPath(String str, String str2) {
            this.f3362a = str;
            this.f3363b = str2;
        }

        public String getText() {
            return this.f3362a;
        }

        public String getMediaPath() {
            return this.f3363b;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f3362a);
            parcel.writeString(this.f3363b);
        }

        private TextAndMediaPath(Parcel parcel) {
            this.f3362a = parcel.readString();
            this.f3363b = parcel.readString();
        }
    }

    /* compiled from: ProGuard */
    public static class TextOnly implements Parcelable {
        public static final Parcelable.Creator<TextOnly> CREATOR = new Parcelable.Creator<TextOnly>() {
            public TextOnly createFromParcel(Parcel parcel) {
                return new TextOnly(parcel);
            }

            public TextOnly[] newArray(int i) {
                return new TextOnly[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        private String f3364a;

        public TextOnly(String str) {
            this.f3364a = str;
        }

        public String getText() {
            return this.f3364a;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f3364a);
        }

        private TextOnly(Parcel parcel) {
            this.f3364a = parcel.readString();
        }
    }
}
