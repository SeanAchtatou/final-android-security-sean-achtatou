package com.tencent.a.a.a.a;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class g {
    private static g i = null;
    private Map<Integer, f> f = null;
    private int g = 0;
    private Context h = null;

    private g(Context context) {
        this.h = context.getApplicationContext();
        this.f = new HashMap(3);
        this.f.put(1, new e(context));
        this.f.put(2, new b(context));
        this.f.put(4, new d(context));
    }

    private c a(List<Integer> list) {
        c e;
        if (list != null && list.size() >= 0) {
            for (Integer num : list) {
                f fVar = this.f.get(num);
                if (fVar != null && (e = fVar.e()) != null && h.e(e.c)) {
                    return e;
                }
            }
        }
        return new c();
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (i == null) {
                i = new g(context);
            }
            gVar = i;
        }
        return gVar;
    }

    public final void b(String str) {
        c f2 = f();
        f2.c = str;
        if (!h.d(f2.f3298a)) {
            f2.f3298a = h.b(this.h);
        }
        if (!h.d(f2.f3299b)) {
            f2.f3299b = h.c(this.h);
        }
        f2.d = System.currentTimeMillis();
        for (Map.Entry<Integer, f> value : this.f.entrySet()) {
            ((f) value.getValue()).a(f2);
        }
    }

    public final c f() {
        return a(new ArrayList(Arrays.asList(1, 2, 4)));
    }
}
