package com.umeng.a;

import android.util.Log;

/* compiled from: OnlineConfigLog */
public class b {

    /* renamed from: a  reason: collision with root package name */
    public static boolean f3723a = false;

    public static void a(String str, String str2) {
        if (f3723a) {
            Log.i(str, str2);
        }
    }

    public static void a(String str, String str2, Exception exc) {
        if (f3723a) {
            Log.i(str, exc.toString() + ":  [" + str2 + "]");
        }
    }

    public static void b(String str, String str2) {
        if (f3723a) {
            Log.e(str, str2);
        }
    }

    public static void b(String str, String str2, Exception exc) {
        if (f3723a) {
            Log.e(str, exc.toString() + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                Log.e(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }

    public static void c(String str, String str2) {
        if (f3723a) {
            Log.d(str, str2);
        }
    }

    public static void c(String str, String str2, Exception exc) {
        if (f3723a) {
            Log.d(str, exc.toString() + ":  [" + str2 + "]");
        }
    }

    public static void d(String str, String str2) {
        if (f3723a) {
            Log.w(str, str2);
        }
    }

    public static void d(String str, String str2, Exception exc) {
        if (f3723a) {
            Log.w(str, exc.toString() + ":  [" + str2 + "]");
            StackTraceElement[] stackTrace = exc.getStackTrace();
            int length = stackTrace.length;
            for (int i = 0; i < length; i++) {
                Log.w(str, "        at\t " + stackTrace[i].toString());
            }
        }
    }
}
