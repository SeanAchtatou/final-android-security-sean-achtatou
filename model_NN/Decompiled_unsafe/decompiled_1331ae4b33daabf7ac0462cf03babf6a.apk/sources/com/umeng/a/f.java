package com.umeng.a;

import android.content.Context;
import android.content.SharedPreferences;

/* compiled from: OnlineConfigStoreHelper */
public class f {

    /* renamed from: a  reason: collision with root package name */
    private static f f3726a = null;

    /* renamed from: b  reason: collision with root package name */
    private static Context f3727b;
    private static String c;

    public f(Context context) {
        f3727b = context.getApplicationContext();
        c = context.getPackageName();
    }

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f3726a == null) {
                f3726a = new f(context);
            }
            fVar = f3726a;
        }
        return fVar;
    }

    public SharedPreferences a() {
        return f3727b.getSharedPreferences("onlineconfig_agent_online_setting_" + c, 0);
    }
}
