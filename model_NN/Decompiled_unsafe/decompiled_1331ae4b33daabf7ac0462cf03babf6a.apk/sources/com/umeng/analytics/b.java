package com.umeng.analytics;

import android.content.Context;
import b.a.ao;
import java.util.HashMap;
import java.util.Map;

/* compiled from: MobclickAgent */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final g f3735a = new g();

    public static g a() {
        return f3735a;
    }

    public static void a(Context context) {
        f3735a.b(context);
    }

    public static void b(Context context) {
        if (context == null) {
            ao.b("MobclickAgent", "unexpected null context in onResume");
        } else {
            f3735a.a(context);
        }
    }

    public static void a(Context context, String str) {
        f3735a.a(context, str);
    }

    public static void a(Context context, Throwable th) {
        f3735a.a(context, th);
    }

    public static void b(Context context, String str) {
        f3735a.a(context, str, null, -1, 1);
    }

    public static void a(Context context, String str, Map<String, String> map) {
        if (map == null) {
            ao.b("MobclickAgent", "input map is null");
            return;
        }
        f3735a.a(context, str, new HashMap(map), -1);
    }

    public static void a(Context context, String str, Map<String, String> map, int i) {
        HashMap hashMap;
        if (map == null) {
            hashMap = new HashMap();
        } else {
            hashMap = new HashMap(map);
        }
        hashMap.put("__ct__", Integer.valueOf(i));
        f3735a.a(context, str, hashMap, -1);
    }

    public static void c(Context context) {
        f3735a.c(context);
    }
}
