package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;
import b.a.ao;
import b.a.cj;
import b.a.cl;
import b.a.cm;
import b.a.cs;
import b.a.cu;
import b.a.cw;
import b.a.e;
import com.umeng.analytics.a.b;
import java.util.Map;

/* compiled from: InternalAgent */
public class g implements cs {

    /* renamed from: a  reason: collision with root package name */
    private final b f3748a = new b();

    /* renamed from: b  reason: collision with root package name */
    private Context f3749b = null;
    private f c;
    private cl d = new cl();
    private e e = new e();
    private cw f = new cw();
    private cm g;
    private cj h;
    private boolean i = false;

    g() {
        this.d.a(this);
    }

    private void d(Context context) {
        if (!this.i) {
            this.f3749b = context.getApplicationContext();
            this.g = new cm(this.f3749b);
            this.h = cj.a(this.f3749b);
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final Context context) {
        if (context == null) {
            ao.b("MobclickAgent", "unexpected null context in onResume");
            return;
        }
        if (a.h) {
            this.e.a(context.getClass().getName());
        }
        try {
            if (!this.i) {
                d(context);
            }
            i.a(new j() {
                public void a() {
                    g.this.e(context.getApplicationContext());
                }
            });
        } catch (Exception e2) {
            ao.b("MobclickAgent", "Exception occurred in Mobclick.onResume(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(final Context context) {
        if (context == null) {
            ao.b("MobclickAgent", "unexpected null context in onPause");
            return;
        }
        if (a.h) {
            this.e.b(context.getClass().getName());
        }
        try {
            if (!this.i) {
                d(context);
            }
            i.a(new j() {
                public void a() {
                    g.this.f(context.getApplicationContext());
                }
            });
        } catch (Exception e2) {
            ao.b("MobclickAgent", "Exception occurred in Mobclick.onRause(). ", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (context == null) {
                ao.b("MobclickAgent", "unexpected null context in reportError");
                return;
            }
            try {
                if (!this.i) {
                    d(context);
                }
                this.h.a(new b.a.g(str).a(false));
            } catch (Exception e2) {
                ao.b("MobclickAgent", "", e2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, Throwable th) {
        if (context != null && th != null) {
            try {
                if (!this.i) {
                    d(context);
                }
                this.h.a(new b.a.g(th).a(false));
            } catch (Exception e2) {
                ao.b("MobclickAgent", "", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(Context context) {
        this.f.c(context);
        if (this.c != null) {
            this.c.a();
        }
    }

    /* access modifiers changed from: private */
    public void f(Context context) {
        this.f.d(context);
        this.e.a(context);
        if (this.c != null) {
            this.c.b();
        }
        this.h.b();
    }

    public void a(Context context, String str, String str2, long j, int i2) {
        try {
            if (!this.i) {
                d(context);
            }
            this.g.a(str, str2, j, i2);
        } catch (Exception e2) {
            ao.b("MobclickAgent", "", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, String str, Map<String, Object> map, long j) {
        try {
            if (!this.i) {
                d(context);
            }
            this.g.a(str, map, j);
        } catch (Exception e2) {
            ao.b("MobclickAgent", "", e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void c(Context context) {
        try {
            this.e.a();
            f(context);
            cu.a(context).edit().commit();
            i.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a(Throwable th) {
        try {
            this.e.a();
            if (this.f3749b != null) {
                if (!(th == null || this.h == null)) {
                    this.h.b(new b.a.g(th));
                }
                f(this.f3749b);
                cu.a(this.f3749b).edit().commit();
            }
            i.a();
        } catch (Exception e2) {
            ao.a("MobclickAgent", "Exception in onAppCrash", e2);
        }
    }
}
