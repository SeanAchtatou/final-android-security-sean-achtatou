package com.umeng.analytics;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: InternalConfig */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f3754a = new String[2];

    public static String[] a(Context context) {
        String[] a2;
        if (!TextUtils.isEmpty(f3754a[0]) && !TextUtils.isEmpty(f3754a[1])) {
            return f3754a;
        }
        if (context == null || (a2 = k.a(context).a()) == null) {
            return null;
        }
        f3754a[0] = a2[0];
        f3754a[1] = a2[1];
        return f3754a;
    }
}
