package com.umeng.analytics;

import android.content.Context;
import b.a.an;

/* compiled from: AnalyticsConfig */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static String f3731a = null;

    /* renamed from: b  reason: collision with root package name */
    public static String f3732b = null;
    public static int c;
    public static String d = "";
    public static String e = "";
    public static boolean f = false;
    public static int g;
    public static boolean h = true;
    public static boolean i = true;
    public static boolean j = true;
    public static boolean k = true;
    public static long l = StatisticConfig.MIN_UPLOAD_INTERVAL;
    private static String m = null;
    private static String n = null;
    private static double[] o = null;

    public static String a(Context context) {
        if (m == null) {
            m = an.j(context);
        }
        return m;
    }

    public static String b(Context context) {
        if (n == null) {
            n = an.n(context);
        }
        return n;
    }

    public static String a() {
        if (c == 1) {
            return "5.5.3.0";
        }
        return "5.5.3";
    }

    public static double[] b() {
        return o;
    }
}
