package com.umeng.analytics;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/* compiled from: QueuedWork */
public class i {

    /* renamed from: a  reason: collision with root package name */
    private static List<WeakReference<ScheduledFuture<?>>> f3755a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private static ExecutorService f3756b = Executors.newSingleThreadExecutor();
    private static long c = 5;
    private static ScheduledExecutorService d = Executors.newSingleThreadScheduledExecutor();

    public static void a(Runnable runnable) {
        if (f3756b.isShutdown()) {
            f3756b = Executors.newSingleThreadExecutor();
        }
        f3756b.execute(runnable);
    }

    public static void a() {
        try {
            for (WeakReference<ScheduledFuture<?>> weakReference : f3755a) {
                ScheduledFuture scheduledFuture = (ScheduledFuture) weakReference.get();
                if (scheduledFuture != null) {
                    scheduledFuture.cancel(false);
                }
            }
            f3755a.clear();
            if (!f3756b.isShutdown()) {
                f3756b.shutdown();
            }
            if (!d.isShutdown()) {
                d.shutdown();
            }
            f3756b.awaitTermination(c, TimeUnit.SECONDS);
            d.awaitTermination(c, TimeUnit.SECONDS);
        } catch (Exception e) {
        }
    }

    public static synchronized void b(Runnable runnable) {
        synchronized (i.class) {
            if (d.isShutdown()) {
                d = Executors.newSingleThreadScheduledExecutor();
            }
            d.execute(runnable);
        }
    }

    public static synchronized void a(Runnable runnable, long j) {
        synchronized (i.class) {
            if (d.isShutdown()) {
                d = Executors.newSingleThreadScheduledExecutor();
            }
            f3755a.add(new WeakReference(d.schedule(runnable, j, TimeUnit.MILLISECONDS)));
        }
    }
}
