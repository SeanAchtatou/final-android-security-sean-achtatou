package com.umeng.analytics;

import android.content.Context;
import b.a.an;
import b.a.j;

/* compiled from: ReportPolicy */
public class c {

    /* compiled from: ReportPolicy */
    public static class f {
        public boolean a(boolean z) {
            return true;
        }

        public boolean a() {
            return true;
        }
    }

    /* renamed from: com.umeng.analytics.c$c  reason: collision with other inner class name */
    /* compiled from: ReportPolicy */
    public static class C0058c extends f {
        public boolean a(boolean z) {
            return z;
        }
    }

    /* compiled from: ReportPolicy */
    public static class d extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f3740a = 90000;

        /* renamed from: b  reason: collision with root package name */
        private long f3741b;
        private b.a.b c;

        public d(b.a.b bVar, long j) {
            this.c = bVar;
            this.f3741b = j < this.f3740a ? this.f3740a : j;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.c.c >= this.f3741b) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class e extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f3742a = LogBuilder.MAX_INTERVAL;

        /* renamed from: b  reason: collision with root package name */
        private b.a.b f3743b;

        public e(b.a.b bVar) {
            this.f3743b = bVar;
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.f3743b.c >= this.f3742a) {
                return true;
            }
            return false;
        }
    }

    /* compiled from: ReportPolicy */
    public static class g extends f {

        /* renamed from: a  reason: collision with root package name */
        private Context f3744a = null;

        public g(Context context) {
            this.f3744a = context;
        }

        public boolean a(boolean z) {
            return an.f(this.f3744a);
        }
    }

    /* compiled from: ReportPolicy */
    public static class a extends f {

        /* renamed from: a  reason: collision with root package name */
        private j f3736a;

        /* renamed from: b  reason: collision with root package name */
        private b.a.b f3737b;

        public a(b.a.b bVar, j jVar) {
            this.f3737b = bVar;
            this.f3736a = jVar;
        }

        public boolean a(boolean z) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.f3737b.c >= this.f3736a.a()) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return this.f3736a.b();
        }
    }

    /* compiled from: ReportPolicy */
    public static class b extends f {

        /* renamed from: a  reason: collision with root package name */
        private long f3738a;

        /* renamed from: b  reason: collision with root package name */
        private long f3739b = 0;

        public b(int i) {
            this.f3738a = (long) i;
            this.f3739b = System.currentTimeMillis();
        }

        public boolean a(boolean z) {
            if (System.currentTimeMillis() - this.f3739b >= this.f3738a) {
                return true;
            }
            return false;
        }

        public boolean a() {
            return System.currentTimeMillis() - this.f3739b < this.f3738a;
        }
    }
}
