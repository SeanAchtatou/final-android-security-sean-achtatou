package com.umeng.socialize.shareboard.a;

import android.view.View;
import android.widget.Toast;
import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.d;

/* compiled from: SNSPlatformAdapter */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f3954a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f3955b;

    b(a aVar, a aVar2) {
        this.f3955b = aVar;
        this.f3954a = aVar2;
    }

    public void onClick(View view) {
        this.f3955b.c.dismiss();
        com.umeng.socialize.c.a aVar = this.f3954a.f;
        if (d.d(this.f3955b.f3953b) || aVar == com.umeng.socialize.c.a.SMS) {
            this.f3955b.a(this.f3954a, aVar);
        } else {
            Toast.makeText(this.f3955b.f3953b, "您的网络不可用,请检查网络连接...", 0).show();
        }
    }
}
