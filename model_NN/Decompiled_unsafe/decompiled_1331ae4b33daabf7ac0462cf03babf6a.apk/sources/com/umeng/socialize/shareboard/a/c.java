package com.umeng.socialize.shareboard.a;

import android.view.MotionEvent;
import android.view.View;

/* compiled from: SNSPlatformAdapter */
class c implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ View f3956a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f3957b;

    c(a aVar, View view) {
        this.f3957b = aVar;
        this.f3956a = view;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.f3956a.setBackgroundColor(-3355444);
            return false;
        } else if (motionEvent.getAction() != 1) {
            return false;
        } else {
            this.f3956a.setBackgroundColor(-1);
            return false;
        }
    }
}
