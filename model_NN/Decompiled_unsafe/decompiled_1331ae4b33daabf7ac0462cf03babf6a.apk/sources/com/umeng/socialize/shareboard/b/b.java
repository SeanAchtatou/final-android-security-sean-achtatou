package com.umeng.socialize.shareboard.b;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import com.umeng.socialize.common.g;
import java.lang.reflect.Array;

/* compiled from: UMActionFrame */
public class b extends ViewGroup {

    /* renamed from: a  reason: collision with root package name */
    private int f3960a = 4;

    /* renamed from: b  reason: collision with root package name */
    private int f3961b = 0;
    private int[][] c = null;
    private a d;
    private int e;
    private int f;
    private int g = 0;
    private int h = -1;
    private int i = 2;
    private Context j = null;

    public b(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = context.getResources().getColor(g.a(context).e("umeng_socialize_grid_divider_line"));
        this.j = context;
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = context.getResources().getColor(g.a(context).e("umeng_socialize_grid_divider_line"));
        this.j = context;
    }

    public b(Context context) {
        super(context);
        this.g = context.getResources().getColor(g.a(context).e("umeng_socialize_grid_divider_line"));
        this.j = context;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        if (this.d != null) {
            Context context = getContext();
            a(this.d.a());
            removeAllViews();
            int length = this.c.length;
            int length2 = this.c[0].length;
            int i8 = (this.f - ((length - 1) * this.i)) / length;
            int i9 = (this.e - ((length2 - 1) * this.i)) / length2;
            int i10 = 0;
            int i11 = 0;
            int i12 = 0;
            int i13 = 0;
            while (i11 < length2) {
                int i14 = 0;
                int i15 = i10;
                while (i14 < length) {
                    if (this.c[i14][i11] == 1) {
                        int i16 = i15 + 1;
                        View a2 = this.d.a(i15, this);
                        ViewGroup.LayoutParams layoutParams = a2.getLayoutParams();
                        if (layoutParams == null) {
                            a2.setLayoutParams(new ViewGroup.LayoutParams(i8, i9));
                        } else {
                            layoutParams.height = i9;
                            layoutParams.width = i8;
                        }
                        boolean z2 = i14 == length + -1;
                        int i17 = (i14 * i8) + i13;
                        int i18 = i17 + i8;
                        int i19 = (i11 * i9) + i12;
                        int i20 = i19 + i9;
                        addView(a2);
                        measureChild(a2, i8, i9);
                        a2.layout(i17, i19, i18, i20);
                        if (!z2 && this.c[i14 + 1][i11] == 2) {
                            View view = new View(context);
                            view.setBackgroundColor(this.h);
                            addView(view);
                            view.layout(i17 + i8, i19, i4, i20);
                        }
                        View view2 = new View(context);
                        if (!z2) {
                            view2.setBackgroundColor(this.g);
                            i7 = this.i + i13;
                        } else {
                            view2.setBackgroundColor(this.h);
                            i7 = 0;
                        }
                        addView(view2);
                        view2.layout(i17 + i8, i19, this.i + i18, i20);
                        int i21 = i16;
                        i6 = i7;
                        i15 = i21;
                    } else {
                        i6 = i13;
                    }
                    i14++;
                    i13 = i6;
                }
                if (i11 > 0 ? this.c[0][i11 + -1] == 1 : false) {
                    View view3 = new View(context);
                    view3.setBackgroundColor(this.g);
                    addView(view3);
                    int i22 = (i11 * i9) + i12;
                    view3.layout(i2, i22 - this.i, i4, i22);
                }
                i11++;
                i12 += this.i;
                i10 = i15;
                i13 = 0;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        this.e = View.MeasureSpec.getSize(i3);
        this.f = View.MeasureSpec.getSize(i2);
        setMeasuredDimension(this.f, this.e);
    }

    public void a(int i2) {
        int length = this.c.length;
        int length2 = this.c[0].length;
        int i3 = length * length2;
        if (i2 > i3) {
            i2 = i3;
        }
        int i4 = i2 % length;
        int i5 = (i3 - i2) - (i4 > 0 ? length - i4 : 0);
        int i6 = i5 + i2;
        int i7 = 0;
        int i8 = 0;
        while (i7 < length2) {
            int i9 = i8;
            for (int i10 = 0; i10 < length; i10++) {
                if (i9 >= i5 && i9 < i6) {
                    this.c[i10][i7] = 1;
                } else if (i9 >= i6) {
                    this.c[i10][i7] = 2;
                } else {
                    this.c[i10][i7] = 3;
                }
                i9++;
            }
            i7++;
            i8 = i9;
        }
    }

    private void a() {
        if (this.j == null || this.d == null) {
            this.c = (int[][]) Array.newInstance(Integer.TYPE, 4, 2);
            return;
        }
        if (this.j.getResources().getConfiguration().orientation == 2) {
            this.f3960a = 6;
        }
        int a2 = this.d.a();
        this.f3961b = this.d.a() / this.f3960a;
        if (a2 % this.f3960a > 0) {
            this.f3961b++;
        }
        com.umeng.socialize.utils.g.c("", "###### row = " + this.f3961b + ", column = " + this.f3960a);
        this.c = (int[][]) Array.newInstance(Integer.TYPE, this.f3960a, this.f3961b);
    }

    public void a(a aVar) {
        this.d = aVar;
        a();
        requestLayout();
    }

    public int b(int i2) {
        return (((i2 - ((this.f3960a - 1) * this.i)) / this.f3960a) * this.f3961b) + ((this.f3961b - 1) * this.i);
    }
}
