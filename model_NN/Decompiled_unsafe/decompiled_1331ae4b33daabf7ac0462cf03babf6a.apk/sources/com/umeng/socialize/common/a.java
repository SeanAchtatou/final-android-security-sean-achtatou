package com.umeng.socialize.common;

/* compiled from: ImageFormat */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final String[] f3794a = {"jpeg", "gif", "png", "bmp", "pcx", "iff", "ras", "pbm", "pgm", "ppm", "psd", "swf"};

    /* JADX WARNING: Removed duplicated region for block: B:132:0x013f A[SYNTHETIC, Splitter:B:132:0x013f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(byte[] r9) {
        /*
            r7 = 66
            r6 = 10
            r5 = 1
            r4 = 6
            r0 = 0
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0139, all -> 0x014a }
            r1.<init>(r9)     // Catch:{ Exception -> 0x0139, all -> 0x014a }
            int r0 = r1.read()     // Catch:{ Exception -> 0x015b }
            int r2 = r1.read()     // Catch:{ Exception -> 0x015b }
            r3 = 71
            if (r0 != r3) goto L_0x002c
            r3 = 73
            if (r2 != r3) goto L_0x002c
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 1
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0027 }
        L_0x0026:
            return r0
        L_0x0027:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x002c:
            r3 = 137(0x89, float:1.92E-43)
            if (r0 != r3) goto L_0x0044
            r3 = 80
            if (r2 != r3) goto L_0x0044
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 2
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0026
        L_0x003f:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x0044:
            r3 = 255(0xff, float:3.57E-43)
            if (r0 != r3) goto L_0x005c
            r3 = 216(0xd8, float:3.03E-43)
            if (r2 != r3) goto L_0x005c
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 0
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x0026
        L_0x0057:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x005c:
            if (r0 != r7) goto L_0x0072
            r3 = 77
            if (r2 != r3) goto L_0x0072
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 3
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0026
        L_0x006d:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x0072:
            if (r0 != r6) goto L_0x0086
            if (r2 >= r4) goto L_0x0086
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 4
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0081 }
            goto L_0x0026
        L_0x0081:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x0086:
            r3 = 70
            if (r0 != r3) goto L_0x009e
            r3 = 79
            if (r2 != r3) goto L_0x009e
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 5
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x0026
        L_0x0099:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x009e:
            r3 = 89
            if (r0 != r3) goto L_0x00b8
            r3 = 166(0xa6, float:2.33E-43)
            if (r2 != r3) goto L_0x00b8
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 6
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x00b2 }
            goto L_0x0026
        L_0x00b2:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x00b8:
            r3 = 80
            if (r0 != r3) goto L_0x00f6
            r3 = 49
            if (r2 < r3) goto L_0x00f6
            r3 = 54
            if (r2 > r3) goto L_0x00f6
            int r0 = r2 + -48
            if (r0 < r5) goto L_0x00ca
            if (r0 <= r4) goto L_0x00d9
        L_0x00ca:
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x00d3 }
            goto L_0x0026
        L_0x00d3:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x00d9:
            r2 = 3
            int[] r2 = new int[r2]     // Catch:{ Exception -> 0x015b }
            r2 = {7, 8, 9} // fill-array     // Catch:{ Exception -> 0x015b }
            int r0 = r0 + -1
            int r0 = r0 % 3
            r0 = r2[r0]     // Catch:{ Exception -> 0x015b }
            java.lang.String[] r2 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r0 = r2[r0]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x00f0 }
            goto L_0x0026
        L_0x00f0:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x00f6:
            r3 = 56
            if (r0 != r3) goto L_0x010f
            if (r2 != r7) goto L_0x010f
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 10
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0109 }
            goto L_0x0026
        L_0x0109:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x010f:
            r3 = 70
            if (r0 != r3) goto L_0x012a
            r0 = 87
            if (r2 != r0) goto L_0x012a
            java.lang.String[] r0 = com.umeng.socialize.common.a.f3794a     // Catch:{ Exception -> 0x015b }
            r2 = 11
            r0 = r0[r2]     // Catch:{ Exception -> 0x015b }
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0124 }
            goto L_0x0026
        L_0x0124:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x012a:
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0133 }
            goto L_0x0026
        L_0x0133:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x0139:
            r1 = move-exception
            r1 = r0
        L_0x013b:
            java.lang.String r0 = ""
            if (r1 == 0) goto L_0x0026
            r1.close()     // Catch:{ IOException -> 0x0144 }
            goto L_0x0026
        L_0x0144:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0026
        L_0x014a:
            r1 = move-exception
            r8 = r1
            r1 = r0
            r0 = r8
        L_0x014e:
            if (r1 == 0) goto L_0x0153
            r1.close()     // Catch:{ IOException -> 0x0154 }
        L_0x0153:
            throw r0
        L_0x0154:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0153
        L_0x0159:
            r0 = move-exception
            goto L_0x014e
        L_0x015b:
            r0 = move-exception
            goto L_0x013b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.common.a.a(byte[]):java.lang.String");
    }
}
