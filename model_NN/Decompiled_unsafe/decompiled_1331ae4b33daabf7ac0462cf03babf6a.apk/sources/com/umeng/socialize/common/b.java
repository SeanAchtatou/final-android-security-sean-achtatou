package com.umeng.socialize.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.umeng.socialize.Config;
import com.umeng.socialize.utils.h;

/* compiled from: QueuedWork */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static Handler f3795a = new Handler(Looper.getMainLooper());

    public static void a(Runnable runnable) {
        f3795a.post(runnable);
    }

    public static void b(Runnable runnable) {
        HandlerThread handlerThread = new HandlerThread("umengsocial", 10);
        handlerThread.start();
        new Handler(handlerThread.getLooper()).post(runnable);
    }

    public static void c(Runnable runnable) {
    }

    /* compiled from: QueuedWork */
    public static abstract class a<T> extends C0060b {

        /* renamed from: a  reason: collision with root package name */
        Dialog f3796a = null;

        public a(Context context) {
            if ((context instanceof Activity) && Config.dialogSwitch) {
                if (Config.dialog != null) {
                    this.f3796a = Config.dialog;
                } else {
                    this.f3796a = new ProgressDialog(context);
                }
                this.f3796a.setOwnerActivity((Activity) context);
                this.f3796a.setOnKeyListener(new c(this));
            }
        }

        /* access modifiers changed from: protected */
        public void a(Object obj) {
            super.a(obj);
            h.a(this.f3796a);
        }

        /* access modifiers changed from: protected */
        public void d_() {
            super.d_();
            h.b(this.f3796a);
        }
    }

    /* renamed from: com.umeng.socialize.common.b$b  reason: collision with other inner class name */
    /* compiled from: QueuedWork */
    public static abstract class C0060b<Result> {

        /* renamed from: b  reason: collision with root package name */
        protected Runnable f3797b;

        /* access modifiers changed from: protected */
        public abstract Result c();

        /* access modifiers changed from: protected */
        public void d_() {
        }

        /* access modifiers changed from: protected */
        public void a(Result result) {
        }

        public final C0060b<Result> d() {
            this.f3797b = new d(this);
            b.a(new f(this));
            b.b(this.f3797b);
            return this;
        }
    }
}
