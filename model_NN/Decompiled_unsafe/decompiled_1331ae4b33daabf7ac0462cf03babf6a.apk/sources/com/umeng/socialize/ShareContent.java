package com.umeng.socialize;

import com.umeng.socialize.c.c;
import com.umeng.socialize.media.UMediaObject;

public class ShareContent {
    public UMediaObject mExtra;
    public String mFollow;
    public c mLocation;
    public UMediaObject mMedia;
    public String mTargetUrl;
    public String mText;
    public String mTitle;
}
