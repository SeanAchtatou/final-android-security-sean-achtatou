package com.umeng.socialize.c;

import android.location.Location;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.Serializable;

/* compiled from: UMLocation */
public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private double f3791a;

    /* renamed from: b  reason: collision with root package name */
    private double f3792b;

    public c(double d, double d2) {
        this.f3791a = d;
        this.f3792b = d2;
    }

    public String toString() {
        return "(" + this.f3792b + MiPushClient.ACCEPT_TIME_SEPARATOR + this.f3791a + ")";
    }

    public static c a(Location location) {
        try {
            if (!(location.getLatitude() == 0.0d || location.getLongitude() == 0.0d)) {
                return new c(location.getLatitude(), location.getLongitude());
            }
        } catch (Exception e) {
        }
        return null;
    }
}
