package com.umeng.socialize.c;

import com.tencent.connect.common.Constants;
import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.umeng.socialize.PlatformConfig;

/* compiled from: SHARE_MEDIA */
public enum a {
    GOOGLEPLUS,
    GENERIC,
    SMS,
    EMAIL,
    SINA,
    QZONE,
    QQ,
    RENREN,
    WEIXIN,
    WEIXIN_CIRCLE,
    WEIXIN_FAVORITE,
    TENCENT,
    DOUBAN,
    FACEBOOK,
    TWITTER,
    LAIWANG,
    LAIWANG_DYNAMIC,
    YIXIN,
    YIXIN_CIRCLE,
    INSTAGRAM,
    PINTEREST,
    EVERNOTE,
    POCKET,
    LINKEDIN,
    FOURSQUARE,
    YNOTE,
    WHATSAPP,
    LINE,
    FLICKR,
    TUMBLR,
    ALIPAY,
    KAKAO;

    public static com.umeng.socialize.shareboard.a a(String str, String str2, String str3, String str4, int i) {
        com.umeng.socialize.shareboard.a aVar = new com.umeng.socialize.shareboard.a();
        aVar.f3951b = str;
        aVar.c = str3;
        aVar.d = str4;
        aVar.e = i;
        aVar.f3950a = str2;
        return aVar;
    }

    public com.umeng.socialize.shareboard.a a() {
        com.umeng.socialize.shareboard.a aVar = new com.umeng.socialize.shareboard.a();
        if (toString().equals(Constants.SOURCE_QQ)) {
            aVar.f3951b = "umeng_socialize_text_qq_key";
            aVar.c = "umeng_socialize_qq_on";
            aVar.d = "umeng_socialize_qq_off";
            aVar.e = 0;
            aVar.f3950a = "qq";
        } else if (toString().equals("SMS")) {
            aVar.f3951b = "umeng_socialize_sms";
            aVar.c = "umeng_socialize_sms_on";
            aVar.d = "umeng_socialize_sms_off";
            aVar.e = 1;
            aVar.f3950a = "sms";
        } else if (toString().equals("GOOGLEPLUS")) {
            aVar.f3951b = "umeng_socialize_text_googleplus_key";
            aVar.c = "umeng_socialize_google";
            aVar.d = "umeng_socialize_google";
            aVar.e = 0;
            aVar.f3950a = "gooleplus";
        } else if (!toString().equals("GENERIC")) {
            if (toString().equals("EMAIL")) {
                aVar.f3951b = "umeng_socialize_mail";
                aVar.c = "umeng_socialize_gmail_on";
                aVar.d = "umeng_socialize_gmail_off";
                aVar.e = 2;
                aVar.f3950a = "email";
            } else if (toString().equals("SINA")) {
                aVar.f3951b = "umeng_socialize_sina";
                aVar.c = "umeng_socialize_sina_on";
                aVar.d = "umeng_socialize_sina_off";
                aVar.e = 0;
                aVar.f3950a = "sina";
            } else if (toString().equals("QZONE")) {
                aVar.f3951b = "umeng_socialize_text_qq_zone_key";
                aVar.c = "umeng_socialize_qzone_on";
                aVar.d = "umeng_socialize_qzone_off";
                aVar.e = 0;
                aVar.f3950a = Constants.SOURCE_QZONE;
            } else if (toString().equals("RENREN")) {
                aVar.f3951b = "umeng_socialize_text_renren_key";
                aVar.c = "umeng_socialize_renren_on";
                aVar.d = "umeng_socialize_renren_off";
                aVar.e = 0;
                aVar.f3950a = PlatformConfig.Renren.Name;
            } else if (toString().equals("WEIXIN")) {
                aVar.f3951b = "umeng_socialize_text_weixin_key";
                aVar.c = "umeng_socialize_wechat";
                aVar.d = "umeng_socialize_weichat_gray";
                aVar.e = 0;
                aVar.f3950a = ConstantsAPI.Token.WX_TOKEN_PLATFORMID_VALUE;
            } else if (toString().equals("WEIXIN_CIRCLE")) {
                aVar.f3951b = "umeng_socialize_text_weixin_circle_key";
                aVar.c = "umeng_socialize_wxcircle";
                aVar.d = "umeng_socialize_wxcircle_gray";
                aVar.e = 0;
                aVar.f3950a = "wxcircle";
            } else if (toString().equals("WEIXIN_FAVORITE")) {
                aVar.f3951b = "umeng_socialize_text_weixin_fav_key";
                aVar.c = "wechat_fav";
                aVar.d = "wechat_fav";
                aVar.e = 0;
                aVar.f3950a = "wechatfavorite";
            } else if (toString().equals("TENCENT")) {
                aVar.f3951b = "umeng_socialize_text_tencent_key";
                aVar.c = "umeng_socialize_tx_on";
                aVar.d = "umeng_socialize_tx_off";
                aVar.e = 0;
                aVar.f3950a = PlatformConfig.TencentWeibo.Name;
            } else if (toString().equals("FACEBOOK")) {
                aVar.f3951b = "umeng_socialize_text_facebook_key";
                aVar.c = "umeng_socialize_facebook";
                aVar.d = "umeng_socialize_facebook";
                aVar.e = 0;
                aVar.f3950a = "facebook";
            } else if (toString().equals("YIXIN")) {
                aVar.f3951b = "umeng_socialize_text_yixin_key";
                aVar.c = "umeng_socialize_yixin";
                aVar.d = "umeng_socialize_yixin_gray";
                aVar.e = 0;
                aVar.f3950a = "yinxin";
            } else if (toString().equals("TWITTER")) {
                aVar.f3951b = "umeng_socialize_text_twitter_key";
                aVar.c = "umeng_socialize_twitter";
                aVar.d = "umeng_socialize_twitter";
                aVar.e = 0;
                aVar.f3950a = "twitter";
            } else if (toString().equals("LAIWANG")) {
                aVar.f3951b = "umeng_socialize_text_laiwang_key";
                aVar.c = "umeng_socialize_laiwang";
                aVar.d = "umeng_socialize_laiwang_gray";
                aVar.e = 0;
                aVar.f3950a = "laiwang";
            } else if (toString().equals("LAIWANG_DYNAMIC")) {
                aVar.f3951b = "umeng_socialize_text_laiwangdynamic_key";
                aVar.c = "umeng_socialize_laiwang_dynamic";
                aVar.d = "umeng_socialize_laiwang_dynamic_gray";
                aVar.e = 0;
                aVar.f3950a = "laiwang_dynamic";
            } else if (toString().equals("INSTAGRAM")) {
                aVar.f3951b = "umeng_socialize_text_instagram_key";
                aVar.c = "umeng_socialize_instagram_on";
                aVar.d = "umeng_socialize_instagram_off";
                aVar.e = 0;
                aVar.f3950a = "qq";
            } else if (toString().equals("YIXIN_CIRCLE")) {
                aVar.f3951b = "umeng_socialize_text_yixincircle_key";
                aVar.c = "umeng_socialize_yixin_circle";
                aVar.d = "umeng_socialize_yixin_circle_gray";
                aVar.e = 0;
                aVar.f3950a = "yinxincircle";
            } else if (toString().equals("PINTEREST")) {
                aVar.f3951b = "umeng_socialize_text_pinterest_key";
                aVar.c = "umeng_socialize_pinterest";
                aVar.d = "umeng_socialize_pinterest_gray";
                aVar.e = 0;
                aVar.f3950a = "pinterest";
            } else if (toString().equals("EVERNOTE")) {
                aVar.f3951b = "umeng_socialize_text_evernote_key";
                aVar.c = "umeng_socialize_evernote";
                aVar.d = "umeng_socialize_evernote_gray";
                aVar.e = 0;
                aVar.f3950a = "evernote";
            } else if (toString().equals("POCKET")) {
                aVar.f3951b = "umeng_socialize_text_pocket_key";
                aVar.c = "umeng_socialize_pocket";
                aVar.d = "umeng_socialize_pocket_gray";
                aVar.e = 0;
                aVar.f3950a = "pocket";
            } else if (toString().equals("LINKEDIN")) {
                aVar.f3951b = "umeng_socialize_text_linkedin_key";
                aVar.c = "umeng_socialize_linkedin";
                aVar.d = "umeng_socialize_linkedin_gray";
                aVar.e = 0;
                aVar.f3950a = "linkedin";
            } else if (toString().equals("FOURSQUARE")) {
                aVar.f3951b = "umeng_socialize_text_foursquare_key";
                aVar.c = "umeng_socialize_foursquare";
                aVar.d = "umeng_socialize_foursquare_gray";
                aVar.e = 0;
                aVar.f3950a = "foursquare";
            } else if (toString().equals("YNOTE")) {
                aVar.f3951b = "umeng_socialize_text_ydnote_key";
                aVar.c = "umeng_socialize_ynote";
                aVar.d = "umeng_socialize_ynote_gray";
                aVar.e = 0;
                aVar.f3950a = "ynote";
            } else if (toString().equals("WHATSAPP")) {
                aVar.f3951b = "umeng_socialize_text_whatsapp_key";
                aVar.c = "umeng_socialize_whatsapp";
                aVar.d = "umeng_socialize_whatsapp_gray";
                aVar.e = 0;
                aVar.f3950a = "whatsapp";
            } else if (toString().equals("LINE")) {
                aVar.f3951b = "umeng_socialize_text_line_key";
                aVar.c = "umeng_socialize_line";
                aVar.d = "umeng_socialize_line_gray";
                aVar.e = 0;
                aVar.f3950a = "line";
            } else if (toString().equals("FLICKR")) {
                aVar.f3951b = "umeng_socialize_text_flickr_key";
                aVar.c = "umeng_socialize_flickr";
                aVar.d = "umeng_socialize_flickr_gray";
                aVar.e = 0;
                aVar.f3950a = "flickr";
            } else if (toString().equals("TUMBLR")) {
                aVar.f3951b = "umeng_socialize_text_tumblr_key";
                aVar.c = "umeng_socialize_tumblr";
                aVar.d = "umeng_socialize_tumblr_gray";
                aVar.e = 0;
                aVar.f3950a = "tumblr";
            } else if (toString().equals("KAKAO")) {
                aVar.f3951b = "umeng_socialize_text_kakao_key";
                aVar.c = "umeng_socialize_kakao";
                aVar.d = "umeng_socialize_kakao_gray";
                aVar.e = 0;
                aVar.f3950a = "kakao";
            } else if (toString().equals("DOUBAN")) {
                aVar.f3951b = "umeng_socialize_text_douban_key";
                aVar.c = "umeng_socialize_douban_on";
                aVar.d = "umeng_socialize_douban_off";
                aVar.e = 0;
                aVar.f3950a = "douban";
            } else if (toString().equals("ALIPAY")) {
                aVar.f3951b = "umeng_socialize_text_alipay_key";
                aVar.c = PlatformConfig.Alipay.Name;
                aVar.d = PlatformConfig.Alipay.Name;
                aVar.e = 0;
                aVar.f3950a = PlatformConfig.Alipay.Name;
            }
        }
        aVar.f = this;
        return aVar;
    }

    public String toString() {
        return super.toString();
    }
}
