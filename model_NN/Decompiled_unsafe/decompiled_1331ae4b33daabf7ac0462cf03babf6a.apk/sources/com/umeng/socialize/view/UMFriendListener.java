package com.umeng.socialize.view;

import com.umeng.socialize.c.a;
import java.util.Map;

public interface UMFriendListener {

    /* renamed from: b  reason: collision with root package name */
    public static final UMFriendListener f3975b = new b();

    void a(a aVar, int i, Throwable th);

    void a(a aVar, int i, Map<String, Object> map);
}
