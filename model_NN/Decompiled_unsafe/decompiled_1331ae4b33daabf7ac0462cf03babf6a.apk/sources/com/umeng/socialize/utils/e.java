package com.umeng.socialize.utils;

import java.lang.reflect.Proxy;

/* compiled from: Dummy */
public class e {
    public static <T> T a(Class<T> cls, T t) {
        if (t != null) {
            return t;
        }
        if (!cls.isInterface()) {
            try {
                return cls.newInstance();
            } catch (Exception e) {
                return null;
            }
        } else {
            return Proxy.newProxyInstance(cls.getClassLoader(), new Class[]{cls}, new f());
        }
    }
}
