package com.umeng.socialize.editorpage.location;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.text.TextUtils;
import com.umeng.socialize.utils.d;
import com.umeng.socialize.utils.g;

/* compiled from: DefaultLocationProvider */
public class a implements SocializeLocationProvider {

    /* renamed from: a  reason: collision with root package name */
    private Location f3845a;

    /* renamed from: b  reason: collision with root package name */
    private Context f3846b;
    private d c;
    private c d = null;
    private String e;

    public void a(Context context) {
        this.f3846b = context;
        this.d = new c();
        b();
    }

    public void a() {
        if (this.c != null && this.d != null) {
            this.c.a(this.d);
        }
    }

    public Location b() {
        if (this.f3845a == null) {
            if (d.a(this.f3846b, "android.permission.ACCESS_FINE_LOCATION")) {
                a(this.f3846b, 1);
            } else if (d.a(this.f3846b, "android.permission.ACCESS_COARSE_LOCATION")) {
                a(this.f3846b, 2);
            }
        }
        return this.f3845a;
    }

    private void a(Context context, int i) {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(i);
        String a2 = this.c.a(criteria, true);
        if (a2 != null) {
            this.e = a2;
        }
        g.c("DefaultLocationProvider", "Get location from " + this.e);
        try {
            if (!TextUtils.isEmpty(this.e)) {
                Location a3 = this.c.a(this.e);
                if (a3 != null) {
                    this.f3845a = a3;
                } else if (this.c.b(this.e) && this.d != null && (context instanceof Activity)) {
                    this.c.a((Activity) context, this.e, 1, 0.0f, this.d);
                }
            }
        } catch (IllegalArgumentException e2) {
        }
    }

    public void a(d dVar) {
        this.c = dVar;
    }

    /* access modifiers changed from: protected */
    public d c() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(Location location) {
        this.f3845a = location;
    }

    public void a(String str) {
        this.e = str;
    }
}
