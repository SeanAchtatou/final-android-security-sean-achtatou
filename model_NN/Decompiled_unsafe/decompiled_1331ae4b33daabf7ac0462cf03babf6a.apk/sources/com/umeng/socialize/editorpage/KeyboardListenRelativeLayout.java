package com.umeng.socialize.editorpage;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class KeyboardListenRelativeLayout extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3834a = false;

    /* renamed from: b  reason: collision with root package name */
    private boolean f3835b = false;
    private int c;
    private IOnKeyboardStateChangedListener d;

    public interface IOnKeyboardStateChangedListener {
        void a(int i);
    }

    public KeyboardListenRelativeLayout(Context context) {
        super(context);
    }

    public KeyboardListenRelativeLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public KeyboardListenRelativeLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void a(IOnKeyboardStateChangedListener iOnKeyboardStateChangedListener) {
        this.d = iOnKeyboardStateChangedListener;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (!this.f3834a) {
            this.f3834a = true;
            this.c = i4;
            if (this.d != null) {
                this.d.a(-1);
            }
        } else {
            this.c = this.c < i4 ? i4 : this.c;
        }
        if (this.f3834a && this.c > i4) {
            this.f3835b = true;
            if (this.d != null) {
                this.d.a(-3);
            }
        }
        if (this.f3834a && this.f3835b && this.c == i4) {
            this.f3835b = false;
            if (this.d != null) {
                this.d.a(-2);
            }
        }
    }
}
