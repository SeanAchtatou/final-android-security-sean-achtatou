package com.umeng.socialize.editorpage.location;

import android.location.Location;
import android.os.AsyncTask;
import com.umeng.socialize.utils.g;

/* compiled from: GetLocationTask */
public class b extends AsyncTask<Void, Void, Location> {

    /* renamed from: a  reason: collision with root package name */
    private a f3847a;

    public b(a aVar) {
        this.f3847a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Location doInBackground(Void... voidArr) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 20) {
                return null;
            }
            try {
                if (isCancelled()) {
                    return null;
                }
                Location a2 = a();
                if (a2 != null) {
                    return a2;
                }
                Thread.sleep(500);
                i = i2 + 1;
            } catch (InterruptedException e) {
                return null;
            }
        }
    }

    private Location a() {
        Location b2 = this.f3847a.b();
        if (b2 != null) {
            return b2;
        }
        g.c("Location", "Fetch gps info from default failed,then fetch form network..");
        this.f3847a.a("network");
        Location b3 = this.f3847a.b();
        this.f3847a.a((String) null);
        return b3;
    }
}
