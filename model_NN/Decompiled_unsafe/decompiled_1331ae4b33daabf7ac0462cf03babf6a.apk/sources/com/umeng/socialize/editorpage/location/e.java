package com.umeng.socialize.editorpage.location;

import android.location.LocationListener;

/* compiled from: SocializeLocationManager */
class e implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f3850a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ long f3851b;
    final /* synthetic */ float c;
    final /* synthetic */ LocationListener d;
    final /* synthetic */ d e;

    e(d dVar, String str, long j, float f, LocationListener locationListener) {
        this.e = dVar;
        this.f3850a = str;
        this.f3851b = j;
        this.c = f;
        this.d = locationListener;
    }

    public void run() {
        this.e.f3849a.requestLocationUpdates(this.f3850a, this.f3851b, this.c, this.d);
    }
}
