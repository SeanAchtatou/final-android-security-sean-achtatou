package com.umeng.socialize.media;

import com.umeng.socialize.ShareContent;

/* compiled from: SimpleShareContent */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private g f3936a;

    /* renamed from: b  reason: collision with root package name */
    private String f3937b;
    private String c;
    private String d;
    private h e;
    private p f;

    public d(ShareContent shareContent) {
        this.f3937b = shareContent.mText;
        this.c = shareContent.mTitle;
        this.d = shareContent.mTargetUrl;
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof g)) {
            this.f3936a = (g) shareContent.mMedia;
        }
    }

    public void a(String str) {
        this.c = str;
    }

    public String f() {
        return this.c;
    }

    public String g() {
        return this.f3937b;
    }

    public g h() {
        return this.f3936a;
    }

    public void b(String str) {
        this.d = str;
    }

    public String i() {
        return this.d;
    }

    public void a(p pVar) {
        this.f = pVar;
    }

    public p j() {
        return this.f;
    }

    public void a(h hVar) {
        this.e = hVar;
    }

    public h k() {
        return this.e;
    }
}
