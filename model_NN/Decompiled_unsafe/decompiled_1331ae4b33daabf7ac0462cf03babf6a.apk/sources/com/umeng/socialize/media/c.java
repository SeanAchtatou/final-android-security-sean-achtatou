package com.umeng.socialize.media;

import android.os.Bundle;
import android.text.TextUtils;
import com.umeng.socialize.ShareContent;
import java.util.ArrayList;

/* compiled from: QZoneShareContent */
public class c extends b {
    private UMediaObject c;

    public c(ShareContent shareContent) {
        super(shareContent);
        this.c = shareContent.mMedia;
    }

    public UMediaObject d() {
        return this.c;
    }

    public Bundle e() {
        Bundle bundle = new Bundle();
        String g = g();
        int i = 1;
        UMediaObject uMediaObject = this.c;
        if ((uMediaObject instanceof g) && TextUtils.isEmpty(g())) {
            i = 5;
            c(bundle);
        } else if ((uMediaObject instanceof h) || (uMediaObject instanceof p)) {
            i = 2;
            b(bundle);
        } else {
            a(bundle);
        }
        bundle.putString("summary", g);
        ArrayList arrayList = new ArrayList();
        String string = bundle.getString("imageUrl");
        bundle.remove("imageUrl");
        if (!TextUtils.isEmpty(string)) {
            arrayList.add(string);
        }
        bundle.putStringArrayList("imageUrl", arrayList);
        bundle.putInt("req_type", i);
        if (TextUtils.isEmpty(bundle.getString("title"))) {
            bundle.putString("title", "分享到QQ空间");
        }
        if (TextUtils.isEmpty(bundle.getString("targetUrl"))) {
            bundle.putString("targetUrl", "http://dev.umeng.com/");
        }
        this.f3935b.clear();
        return bundle;
    }

    private void a(Bundle bundle) {
        c(bundle);
    }

    private void b(Bundle bundle) {
        UMediaObject k;
        if (j() != null || k() != null) {
            if (j() != null) {
                b();
            } else if (k() != null) {
                c();
            }
            String str = (String) this.f3935b.get("image_path_local");
            if (TextUtils.isEmpty(str)) {
                str = (String) this.f3935b.get("image_path_url");
            }
            if (j() != null) {
                k = j();
            } else {
                k = k();
            }
            bundle.putString("imageUrl", str);
            bundle.putString("targetUrl", i());
            bundle.putString("audio_url", k.b());
            bundle.putString("title", f());
        }
    }

    private void c(Bundle bundle) {
        a(h());
        String str = (String) this.f3935b.get("image_path_local");
        if (TextUtils.isEmpty(str)) {
            str = (String) this.f3935b.get("image_path_url");
        }
        bundle.putString("imageUrl", str);
        if (TextUtils.isEmpty(i())) {
            b((String) this.f3935b.get("image_target_url"));
        }
        if (TextUtils.isEmpty(i())) {
            b("http://dev.umeng.com/");
        }
        bundle.putString("targetUrl", i());
        bundle.putString("title", f());
    }
}
