package com.umeng.socialize.media;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.sina.weibo.sdk.api.share.BaseResponse;
import com.sina.weibo.sdk.api.share.IWeiboHandler;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.c.a;
import com.umeng.socialize.handler.SinaSsoHandler;
import com.umeng.socialize.utils.g;

public class WBShareCallBackActivity extends Activity implements IWeiboHandler.Response {

    /* renamed from: a  reason: collision with root package name */
    protected SinaSsoHandler f3931a = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a("create wx callback activity");
        this.f3931a = (SinaSsoHandler) UMShareAPI.get(getApplicationContext()).getHandler(a.SINA);
        this.f3931a.a(this, PlatformConfig.getPlatform(a.SINA));
        if (getIntent() != null) {
            this.f3931a.c().handleWeiboResponse(getIntent(), this);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        this.f3931a = (SinaSsoHandler) UMShareAPI.get(getApplicationContext()).getHandler(a.SINA);
        this.f3931a.a(this, PlatformConfig.getPlatform(a.SINA));
        this.f3931a.c().handleWeiboResponse(intent, this);
    }

    public void onResponse(BaseResponse baseResponse) {
        if (this.f3931a != null) {
            this.f3931a.a(baseResponse);
        }
        finish();
    }
}
