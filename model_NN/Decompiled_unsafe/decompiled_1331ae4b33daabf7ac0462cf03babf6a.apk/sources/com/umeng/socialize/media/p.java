package com.umeng.socialize.media;

import android.text.TextUtils;
import com.umeng.socialize.d.b.e;
import com.umeng.socialize.media.UMediaObject;
import java.util.HashMap;
import java.util.Map;

/* compiled from: UMusic */
public class p extends a {
    private String j = "未知";
    private String k = "未知";
    private g l;
    private String m;
    private String n;
    private String o;
    private String p;
    private int q;

    public int j() {
        return this.q;
    }

    public String k() {
        return this.p;
    }

    public p(String str) {
        super(str);
    }

    public String l() {
        return this.n;
    }

    public String m() {
        return this.o;
    }

    public UMediaObject.a g() {
        return UMediaObject.a.MUSIC;
    }

    public final Map<String, Object> h() {
        HashMap hashMap = new HashMap();
        if (c()) {
            hashMap.put(e.e, this.f3933b);
            hashMap.put(e.f, g());
            hashMap.put(e.g, this.j);
            hashMap.put(e.i, this.k);
        }
        return hashMap;
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.j = str;
        }
    }

    public String d() {
        return this.j;
    }

    public void a(g gVar) {
        this.l = gVar;
    }

    public byte[] i() {
        if (this.l != null) {
            return this.l.i();
        }
        return null;
    }

    public String toString() {
        return "UMusic [title=" + this.j + ", author=" + this.k + "media_url=" + this.f3933b + ", qzone_title=" + this.c + ", qzone_thumb=" + this.d + "]";
    }

    public g n() {
        return this.l;
    }

    public String o() {
        return this.m;
    }
}
