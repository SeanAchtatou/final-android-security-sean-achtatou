package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.tencent.connect.UserInfo;
import com.tencent.connect.common.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.d.e;
import com.umeng.socialize.media.b;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public class UMQQSsoHandler extends UMTencentSSOHandler {
    /* access modifiers changed from: private */
    public Activity i;
    private boolean j = false;
    /* access modifiers changed from: private */
    public IUiListener k;
    /* access modifiers changed from: private */
    public b l;
    /* access modifiers changed from: private */
    public Bundle m;
    /* access modifiers changed from: private */
    public QQPreferences n;

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
        if (context != null) {
            this.n = new QQPreferences(context, a.QQ.toString());
        }
    }

    public boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        if (activity == null) {
            g.c("UMError", "qq share activity is null");
        } else {
            this.m = null;
            this.i = activity;
            this.k = a(uMShareListener);
            if (this.k == null) {
                g.c("listen", "listener is null");
            }
            this.l = new b(shareContent);
            c(activity);
        }
        return false;
    }

    private IUiListener a(final UMShareListener uMShareListener) {
        return new IUiListener() {
            public void onError(UiError uiError) {
                if (uiError != null) {
                    g.b("xxxx qqerror" + uiError.errorDetail + uiError.errorMessage + uiError.errorCode);
                }
                uMShareListener.onError(a.QQ, null);
            }

            public void onCancel() {
                g.b("xxxx qqcancle");
                uMShareListener.onCancel(a.QQ);
            }

            public void onComplete(Object obj) {
                g.b("xxxx qqcomplete");
                uMShareListener.onResult(a.QQ);
            }
        };
    }

    public boolean b(Context context) {
        if (context == null || this.n == null) {
            return false;
        }
        return this.n.e();
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
        if (activity == null) {
            g.c("UMError", "qq authorize activity is null");
            return;
        }
        this.i = activity;
        this.f = uMAuthListener;
        e();
    }

    private IUiListener a(UMAuthListener uMAuthListener) {
        return new IUiListener() {
            public void onError(UiError uiError) {
                if (uiError != null) {
                    g.c("UMQQSsoHandler", "授权失败! ==> errorCode = " + uiError.errorCode + ", errorMsg = " + uiError.errorMessage + ", detail = " + uiError.errorDetail);
                }
                UMQQSsoHandler.this.f.onError(a.QQ, 0, new Throwable("授权失败! ==> errorCode = " + uiError.errorCode + ", errorMsg = " + uiError.errorMessage + ", detail = " + uiError.errorDetail));
            }

            public void onCancel() {
                if (UMQQSsoHandler.this.f != null) {
                    UMQQSsoHandler.this.f.onCancel(a.QQ, 0);
                }
            }

            public void onComplete(Object obj) {
                h.a(UMQQSsoHandler.this.f3908b);
                Bundle a2 = UMQQSsoHandler.this.a(obj);
                if (UMQQSsoHandler.this.n == null && UMQQSsoHandler.this.i != null) {
                    QQPreferences unused = UMQQSsoHandler.this.n = new QQPreferences(UMQQSsoHandler.this.i, a.QQ.toString());
                }
                if (UMQQSsoHandler.this.n != null) {
                    UMQQSsoHandler.this.n.a(a2).f();
                }
                UMQQSsoHandler.this.a((JSONObject) obj);
                if (UMQQSsoHandler.this.f != null) {
                    UMQQSsoHandler.this.f.onComplete(a.QQ, 0, UMQQSsoHandler.this.b(a2));
                }
                UMQQSsoHandler.this.a(a2);
                if (a2 == null || Integer.valueOf(a2.getString("ret")).intValue() == 0) {
                }
            }
        };
    }

    public boolean a(Context context) {
        if (!this.g.isSupportSSOLogin((Activity) context)) {
            return false;
        }
        return true;
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
        this.g.logout(context);
        if (this.n != null) {
            this.n.g();
        }
        uMAuthListener.onComplete(a.QQ, 1, null);
    }

    public boolean a() {
        return true;
    }

    private void e() {
        g.a("UMQQSsoHandler", "QQ oauth login...");
        if (a((Context) this.i)) {
            g.c("qq", "installed qq");
            this.g.login(this.i, "all", a(this.f));
            return;
        }
        g.c("qq", "didn't install qq");
        this.g.loginServerSide(this.i, "all", a(this.f));
    }

    public void a(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("access_token");
            String string2 = jSONObject.getString("expires_in");
            String string3 = jSONObject.getString("openid");
            if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                this.g.setAccessToken(string, string2);
                this.g.setOpenId(string3);
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void a(final Bundle bundle) throws SocializeException {
        new Thread(new Runnable() {
            public void run() {
                if (UMQQSsoHandler.this.i() != null && bundle != null && UMQQSsoHandler.this.e != null) {
                    e eVar = new e(UMQQSsoHandler.this.i());
                    eVar.a("to", "qq");
                    eVar.a("usid", bundle.getString("uid"));
                    eVar.a("access_token", bundle.getString("access_token"));
                    eVar.a(Oauth2AccessToken.KEY_REFRESH_TOKEN, bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN));
                    eVar.a("expires_in", bundle.getString("expires_in"));
                    eVar.a("app_id", UMQQSsoHandler.this.e.appId);
                    eVar.a("app_secret", UMQQSsoHandler.this.e.appKey);
                    g.b("upload token resp = " + com.umeng.socialize.d.g.a(eVar));
                }
            }
        }).start();
    }

    /* access modifiers changed from: private */
    public Map<String, String> b(Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return null;
        }
        Set<String> keySet = bundle.keySet();
        HashMap hashMap = new HashMap();
        for (String next : keySet) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    public void c(Context context) {
        if (d()) {
            String str = this.l.f3935b.get("image_path_local");
            if (e(context)) {
                a(context, this.l.f3935b.get("image_path_url"));
            } else if (a(str, this.l.f3934a, context)) {
                com.umeng.socialize.media.g gVar = new com.umeng.socialize.media.g(context, new File(str));
                g.e("UMQQSsoHandler", "未安装QQ客户端的情况下，QQ不支持音频，图文是为本地图片的分享。此时将上传本地图片到相册，请确保在QQ互联申请了upload_pic权限.");
                a(this.i, a(gVar));
            } else {
                g.c("shareQQ", "share to qq");
                a(this.l);
            }
        } else {
            g.c("UMQQSsoHandler", "QQ平台还没有授权");
            a(this.i, (UMAuthListener) null);
        }
    }

    private UMAuthListener a(final com.umeng.socialize.media.g gVar) {
        return new UMAuthListener() {
            public void onComplete(a aVar, int i, Map<String, String> map) {
                if (map != null && map.containsKey("uid") && !TextUtils.isEmpty(gVar.k())) {
                    UMQQSsoHandler.this.m.putString("imageUrl", gVar.k());
                    UMQQSsoHandler.this.m.remove("imageLocalUrl");
                    UMQQSsoHandler.this.a(UMQQSsoHandler.this.l);
                }
            }

            public void onError(a aVar, int i, Throwable th) {
            }

            public void onCancel(a aVar, int i) {
            }
        };
    }

    /* access modifiers changed from: protected */
    public boolean a(String str, int i2, Context context) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        a name = this.e.getName();
        boolean d = d(context);
        boolean z = !str.startsWith("http://") && !str.startsWith("https://");
        if (d || !z) {
            return false;
        }
        if (name == a.QQ && (i2 == 2 || i2 == 1)) {
            return true;
        }
        if (name != a.QZONE) {
            return false;
        }
        if (i2 == 1 || i2 == 2) {
            return true;
        }
        return false;
    }

    private boolean e(Context context) {
        String str = this.l.f3935b.get("image_path_url");
        String str2 = this.l.f3935b.get("image_path_local");
        if (this.l.f3934a != 5 || !d(context) || TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2)) {
            return false;
        }
        return true;
    }

    private void a(Context context, String str) {
        com.umeng.socialize.utils.a.c(str);
        h.a(this.f3908b);
        this.m.putString("imageLocalUrl", com.umeng.socialize.utils.a.d(str));
        this.m.remove("imageUrl");
        a(this.l);
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        this.m = this.l.a();
        this.m.putString("appName", c());
        if (this.m != null) {
            com.umeng.socialize.common.b.a(new Runnable() {
                public void run() {
                    UMQQSsoHandler.this.g.shareToQQ(UMQQSsoHandler.this.i, UMQQSsoHandler.this.m, UMQQSsoHandler.this.k);
                }
            });
        }
    }

    public int b() {
        return Constants.REQUEST_QQ_SHARE;
    }

    public void a(int i2, int i3, Intent intent) {
        if (i2 == 10103) {
            Tencent.onActivityResultData(i2, i3, intent, this.k);
        }
        if (i2 == 11101) {
            Tencent.onActivityResultData(i2, i3, intent, a(this.f));
        }
    }

    public void b(Activity activity, final UMAuthListener uMAuthListener) {
        if (b(activity)) {
            try {
                String a2 = this.n.a();
                QQPreferences qQPreferences = this.n;
                String b2 = QQPreferences.b();
                String c = this.n.c();
                if (!(activity == null || this.n == null)) {
                    a2 = this.n.a();
                    QQPreferences qQPreferences2 = this.n;
                    b2 = QQPreferences.b();
                    c = this.n.c();
                }
                if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c)) {
                    this.g.setAccessToken(a2, b2);
                    this.g.setOpenId(c);
                }
            } catch (Exception e) {
            }
        }
        if (activity == null) {
            g.c("UMError", "QQ getPlatFormInfo activity is null");
        } else {
            new UserInfo(activity, this.g.getQQToken()).getUserInfo(new IUiListener() {
                public void onCancel() {
                }

                public void onComplete(Object obj) {
                    try {
                        JSONObject jSONObject = new JSONObject(obj.toString());
                        HashMap hashMap = new HashMap();
                        hashMap.put("screen_name", jSONObject.optString("nickname"));
                        hashMap.put("gender", jSONObject.optString("gender"));
                        hashMap.put("profile_image_url", jSONObject.optString("figureurl_qq_2"));
                        hashMap.put("is_yellow_year_vip", jSONObject.optString("is_yellow_year_vip"));
                        hashMap.put("yellow_vip_level", jSONObject.optString("yellow_vip_level"));
                        hashMap.put("msg", jSONObject.optString("msg"));
                        hashMap.put("city", jSONObject.optString("city"));
                        hashMap.put("vip", jSONObject.optString("vip"));
                        hashMap.put("level", jSONObject.optString("level"));
                        hashMap.put("province", jSONObject.optString("province"));
                        hashMap.put("is_yellow_vip", jSONObject.optString("is_yellow_vip"));
                        if (UMQQSsoHandler.this.n != null) {
                            hashMap.put("openid", UMQQSsoHandler.this.n.d());
                        }
                        uMAuthListener.onComplete(a.QQ, 2, hashMap);
                    } catch (JSONException e) {
                        uMAuthListener.onComplete(a.QQ, 2, null);
                    }
                }

                public void onError(UiError uiError) {
                    uMAuthListener.onError(a.QQ, 2, new Throwable(uiError.toString()));
                }
            });
        }
    }
}
