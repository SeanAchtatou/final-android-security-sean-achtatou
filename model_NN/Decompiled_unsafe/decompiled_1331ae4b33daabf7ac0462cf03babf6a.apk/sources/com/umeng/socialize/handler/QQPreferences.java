package com.umeng.socialize.handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.umeng.socialize.utils.g;

public class QQPreferences {

    /* renamed from: b  reason: collision with root package name */
    private static String f3852b = null;

    /* renamed from: a  reason: collision with root package name */
    private String f3853a = null;
    private String c = null;
    private SharedPreferences d = null;

    public QQPreferences(Context context, String str) {
        this.d = context.getSharedPreferences(str, 0);
        this.f3853a = this.d.getString("access_token", null);
        this.c = this.d.getString("uid", null);
        f3852b = this.d.getString("expires_in", null);
    }

    public String a() {
        return this.f3853a;
    }

    public static String b() {
        return f3852b;
    }

    public String c() {
        return this.c;
    }

    public QQPreferences a(Bundle bundle) {
        this.f3853a = bundle.getString("access_token");
        f3852b = bundle.getString("expires_in");
        this.c = bundle.getString("uid");
        return this;
    }

    public String d() {
        return this.c;
    }

    public boolean e() {
        return this.f3853a != null;
    }

    public void f() {
        this.d.edit().putString("access_token", this.f3853a).putString("expires_in", f3852b).putString("uid", this.c).commit();
        g.a("save auth succeed");
    }

    public void g() {
        this.d.edit().clear().commit();
    }
}
