package com.umeng.socialize.handler;

import android.os.Bundle;
import com.tencent.open.SocialConstants;
import com.umeng.socialize.handler.UMAPIShareHandler;
import com.umeng.socialize.utils.g;

/* compiled from: UMAPIShareHandler */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ UMAPIShareHandler.a f3924a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Bundle f3925b;
    final /* synthetic */ UMAPIShareHandler c;

    a(UMAPIShareHandler uMAPIShareHandler, UMAPIShareHandler.a aVar, Bundle bundle) {
        this.c = uMAPIShareHandler;
        this.f3924a = aVar;
        this.f3925b = bundle;
    }

    public void run() {
        this.c.a(this.c.a(this.f3924a.f3894a, this.f3925b), this.f3924a.f3895b);
        g.c(SocialConstants.PARAM_ACT, "sent share request");
    }
}
