package com.umeng.socialize.handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import java.util.HashMap;
import java.util.Map;

public class WeixinPreferences {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f3922a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f3923b;
    private String c;
    private long d;
    private String e;
    private long f;
    private long g;

    public WeixinPreferences(Context context, String str) {
        this.f3922a = context.getSharedPreferences(str, 0);
        this.f3923b = this.f3922a.getString("openid", null);
        this.c = this.f3922a.getString("access_token", null);
        this.d = this.f3922a.getLong("expires_in", 0);
        this.e = this.f3922a.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN, null);
        this.f = this.f3922a.getLong("rt_expires_in", 0);
        this.g = this.f3922a.getLong("expires_in", 0);
    }

    public WeixinPreferences a(Bundle bundle) {
        this.f3923b = bundle.getString("openid");
        this.c = bundle.getString("access_token");
        this.e = bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN);
        String string = bundle.getString("expires_in");
        if (!TextUtils.isEmpty(string)) {
            this.g = (Long.valueOf(string).longValue() * 1000) + System.currentTimeMillis();
        }
        String string2 = bundle.getString("expires_in");
        if (!TextUtils.isEmpty(string2)) {
            this.d = (Long.valueOf(string2).longValue() * 1000) + System.currentTimeMillis();
        }
        String string3 = bundle.getString("rt_expires_in");
        if (!TextUtils.isEmpty(string3)) {
            this.f = (Long.valueOf(string3).longValue() * 1000) + System.currentTimeMillis();
        }
        i();
        return this;
    }

    public String a() {
        return this.f3923b;
    }

    public String b() {
        return this.e;
    }

    public Map<String, String> c() {
        HashMap hashMap = new HashMap();
        hashMap.put("access_token", this.c);
        hashMap.put("openid", this.f3923b);
        hashMap.put(Oauth2AccessToken.KEY_REFRESH_TOKEN, this.e);
        return hashMap;
    }

    public boolean d() {
        boolean z;
        boolean isEmpty = TextUtils.isEmpty(this.c);
        if (this.g - System.currentTimeMillis() <= 0) {
            z = true;
        } else {
            z = false;
        }
        if (isEmpty || z) {
            return false;
        }
        return true;
    }

    public String e() {
        return this.c;
    }

    public boolean f() {
        boolean z;
        boolean isEmpty = TextUtils.isEmpty(this.e);
        if (this.f - System.currentTimeMillis() <= 0) {
            z = true;
        } else {
            z = false;
        }
        if (isEmpty || z) {
            return false;
        }
        return true;
    }

    public boolean g() {
        return !TextUtils.isEmpty(e());
    }

    public void h() {
        this.f3922a.edit().clear().commit();
    }

    public void i() {
        this.f3922a.edit().putString("openid", this.f3923b).putString("access_token", this.c).putLong("expires_in", this.d).putString(Oauth2AccessToken.KEY_REFRESH_TOKEN, this.e).putLong("rt_expires_in", this.f).putLong("expires_in", this.g).commit();
    }
}
