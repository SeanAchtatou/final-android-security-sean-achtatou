package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.tencent.open.t.Weibo;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.d.e;
import com.umeng.socialize.d.f;
import com.umeng.socialize.utils.d;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

public class QQwbHandler extends UMTencentSSOHandler {
    private Weibo i = null;
    /* access modifiers changed from: private */
    public Activity j;
    /* access modifiers changed from: private */
    public QQPreferences k;

    public void a(Context context, PlatformConfig.Platform platform) {
        super.a(context, platform);
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
        if (activity == null) {
            g.c("UMError", "QQwb authorize activity is null");
            return;
        }
        this.j = activity;
        if (b(activity, j())) {
            this.f = uMAuthListener;
            e();
        }
    }

    private boolean b(Context context, PlatformConfig.Platform platform) {
        if (d.a("com.tencent.mobileqq", context)) {
            return true;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("请安装");
        sb.append("qq");
        sb.append("客户端");
        g.d(sb.toString());
        if (Config.IsToastTip) {
            Toast.makeText(context, sb, 1).show();
        }
        return false;
    }

    public boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        String str;
        if (activity == null) {
            g.c("UMError", "QQwb share activity is null");
            return false;
        }
        this.k = new QQPreferences(activity, a.QQ.toString());
        if (this.k.c() == null) {
            g.b("QQ没有授权");
            if (uMShareListener == null) {
                return false;
            }
            uMShareListener.onError(a.TENCENT, new Throwable("QQ is not  auth"));
            return false;
        }
        String a2 = this.k.a();
        QQPreferences qQPreferences = this.k;
        String b2 = QQPreferences.b();
        String c = this.k.c();
        if (!TextUtils.isEmpty(a2) && !TextUtils.isEmpty(b2) && !TextUtils.isEmpty(c)) {
            this.g.setAccessToken(a2, b2);
            this.g.setOpenId(c);
        }
        this.i = new Weibo(activity, this.g.getQQToken());
        if (shareContent.mMedia != null && (shareContent.mMedia instanceof com.umeng.socialize.media.g)) {
            if (((com.umeng.socialize.media.g) shareContent.mMedia).j() != null) {
                str = ((com.umeng.socialize.media.g) shareContent.mMedia).j().getPath();
            } else {
                str = null;
            }
            g.b("xxxxx", "filePath=" + str);
            if (str == null) {
                this.i.sendText(shareContent.mText, b(uMShareListener));
            } else {
                this.i.sendPicText(shareContent.mText, str, b(uMShareListener));
            }
        } else if (!TextUtils.isEmpty(shareContent.mText)) {
            this.i.sendText(shareContent.mText, b(uMShareListener));
        }
        return true;
    }

    private IUiListener b(final UMShareListener uMShareListener) {
        return new IUiListener() {
            public void onError(UiError uiError) {
                g.b("xxxx qqerror");
                uMShareListener.onError(a.TENCENT, null);
            }

            public void onCancel() {
                g.b("xxxx qqcancle");
                uMShareListener.onCancel(a.TENCENT);
            }

            public void onComplete(Object obj) {
                g.b("xxxx qqcomplete=" + obj);
                uMShareListener.onResult(a.TENCENT);
            }
        };
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Bundle bundle) {
        if (bundle == null || bundle.isEmpty()) {
            return null;
        }
        Set<String> keySet = bundle.keySet();
        HashMap hashMap = new HashMap();
        for (String next : keySet) {
            hashMap.put(next, bundle.getString(next));
        }
        return hashMap;
    }

    private void e() {
        this.g.login(this.j, "all", a(this.f));
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
        this.g.logout(context);
        if (this.k != null) {
            this.k.g();
        }
        uMAuthListener.onComplete(a.TENCENT, 1, null);
    }

    private IUiListener a(UMAuthListener uMAuthListener) {
        return new IUiListener() {
            public void onError(UiError uiError) {
                if (uiError != null) {
                    g.c("授权失败! ==> errorCode = " + uiError.errorCode + ", errorMsg = " + uiError.errorMessage + ", detail = " + uiError.errorDetail);
                }
            }

            public void onCancel() {
            }

            public void onComplete(Object obj) {
                h.a(QQwbHandler.this.f3908b);
                Bundle a2 = QQwbHandler.this.a(obj);
                QQPreferences unused = QQwbHandler.this.k = new QQPreferences(QQwbHandler.this.j, a.QQ.toString());
                QQwbHandler.this.k.a(a2).f();
                QQwbHandler.this.b(a2);
                QQwbHandler.this.a((JSONObject) obj);
                if (QQwbHandler.this.f != null) {
                    QQwbHandler.this.f.onComplete(a.QQ, 0, QQwbHandler.this.a(a2));
                }
                if (a2 == null || TextUtils.isEmpty(a2.getString("ret"))) {
                }
            }
        };
    }

    public boolean a() {
        return true;
    }

    public void a(int i2, int i3, Intent intent) {
        if (i2 == 10104) {
            Tencent.onActivityResultData(i2, i3, intent, a(this.h));
        }
        if (i2 == 11101) {
            Tencent.onActivityResultData(i2, i3, intent, a(this.f));
        }
    }

    public IUiListener a(final UMShareListener uMShareListener) {
        return new IUiListener() {
            public void onComplete(Object obj) {
                uMShareListener.onResult(a.QZONE);
            }

            public void onError(UiError uiError) {
                uMShareListener.onError(a.QZONE, null);
            }

            public void onCancel() {
                uMShareListener.onCancel(a.QZONE);
            }
        };
    }

    public void a(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString("access_token");
            String string2 = jSONObject.getString("expires_in");
            String string3 = jSONObject.getString("openid");
            if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                this.g.setAccessToken(string, string2);
                this.g.setOpenId(string3);
                g.b("xxxx write mTencent.getQQToken().getOpenId()=");
            }
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void b(final Bundle bundle) throws SocializeException {
        new Thread(new Runnable() {
            public void run() {
                e eVar = new e(QQwbHandler.this.i());
                eVar.a("to", "qq");
                eVar.a("usid", bundle.getString("uid"));
                eVar.a("access_token", bundle.getString("access_token"));
                eVar.a(Oauth2AccessToken.KEY_REFRESH_TOKEN, bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN));
                eVar.a("expires_in", bundle.getString("expires_in"));
                eVar.a("app_id", QQwbHandler.this.e.appId);
                eVar.a("app_secret", QQwbHandler.this.e.appKey);
                f a2 = com.umeng.socialize.d.g.a(eVar);
                if (a2 == null) {
                    g.b("fail to upload sina token");
                } else if (!a2.b()) {
                    g.b("fail to upload sina token = " + a2.k);
                }
            }
        }).start();
    }
}
