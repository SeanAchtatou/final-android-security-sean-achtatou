package com.umeng.socialize.handler;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.igexin.sdk.PushBuildConfig;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelmsg.SendAuth;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.open.GameAppOperation;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import com.umeng.socialize.common.b;
import com.umeng.socialize.media.q;
import com.umeng.socialize.utils.e;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UMWXHandler extends UMSSOHandler {

    /* renamed from: b  reason: collision with root package name */
    private static String f3909b = "snsapi_userinfo,snsapi_friend,snsapi_message";
    private q c;
    /* access modifiers changed from: private */
    public PlatformConfig.Weixin d;
    /* access modifiers changed from: private */
    public WeixinPreferences e;
    private UMAuthListener f;
    private UMShareListener g;
    private a h = a.WEIXIN;
    private boolean i = false;
    /* access modifiers changed from: private */
    public Context j;
    private IWXAPI k;
    private IWXAPIEventHandler l = new IWXAPIEventHandler() {
        public void onResp(BaseResp baseResp) {
            switch (baseResp.getType()) {
                case 1:
                    UMWXHandler.this.a((SendAuth.Resp) baseResp);
                    return;
                case 2:
                    UMWXHandler.this.a((SendMessageToWX.Resp) baseResp);
                    return;
                default:
                    return;
            }
        }

        public void onReq(BaseReq baseReq) {
        }
    };

    public void a(Context context, PlatformConfig.Platform platform) {
        this.e = new WeixinPreferences(context.getApplicationContext(), "weixin");
        this.d = (PlatformConfig.Weixin) platform;
        this.k = WXAPIFactory.createWXAPI(context.getApplicationContext(), this.d.appId);
        this.k.registerApp(this.d.appId);
        if (!c()) {
            if (Config.IsToastTip) {
                Toast.makeText(context, "请安装" + this.h + "客户端", 0).show();
            }
        }
        this.j = context;
    }

    public void a(Activity activity, UMAuthListener uMAuthListener) {
        this.f = uMAuthListener;
        this.h = this.d.getName();
        if (this.e.f()) {
            if (this.e.d()) {
                b("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + this.d.appId + "&grant_type=refresh_token&refresh_token=" + this.e.b());
            }
            this.f.onComplete(a.WEIXIN, 0, d(this.e.b()));
            return;
        }
        SendAuth.Req req = new SendAuth.Req();
        req.scope = f3909b;
        req.state = PushBuildConfig.sdk_conf_debug_level;
        this.k.sendReq(req);
    }

    public boolean b(Context context) {
        return this.e.g();
    }

    private void b(String str) {
        this.e.a(c(com.umeng.socialize.weixin.a.a.a(str)));
        this.f.onComplete(this.d.getName(), 0, null);
    }

    /* access modifiers changed from: private */
    public Bundle c(String str) {
        Bundle bundle = new Bundle();
        if (TextUtils.isEmpty(str)) {
            return bundle;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                bundle.putString(next, jSONObject.optString(next));
            }
            bundle.putString("uid", bundle.getString("openid"));
            bundle.putLong("refresh_token_expires", 604800);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return bundle;
    }

    private Map<String, String> d(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("https://api.weixin.qq.com/sns/oauth2/refresh_token?");
        sb.append("appid=").append(this.d.appId);
        sb.append("&grant_type=refresh_token");
        sb.append("&refresh_token=").append(str);
        try {
            return h.b(com.umeng.socialize.weixin.a.a.a(sb.toString()));
        } catch (Exception e2) {
            return null;
        }
    }

    private void a(String str, final UMAuthListener uMAuthListener) {
        final StringBuilder sb = new StringBuilder();
        sb.append("https://api.weixin.qq.com/sns/oauth2/access_token?");
        sb.append("appid=").append(this.d.appId);
        sb.append("&secret=").append(this.d.appSecret);
        sb.append("&code=").append(str);
        sb.append("&grant_type=authorization_code");
        new Thread(new Runnable() {
            public void run() {
                String a2 = com.umeng.socialize.weixin.a.a.a(sb.toString());
                try {
                    final Map<String, String> b2 = h.b(a2);
                    if (b2 == null || b2.size() == 0) {
                        b2 = UMWXHandler.this.e.c();
                    }
                    UMWXHandler.this.e.a(UMWXHandler.this.c(a2));
                    b.a(new Runnable() {
                        public void run() {
                            UMWXHandler.this.a(b2);
                            uMAuthListener.onComplete(a.WEIXIN, 0, b2);
                        }
                    });
                } catch (Exception e) {
                }
            }
        }).start();
    }

    public boolean a(Context context) {
        return c();
    }

    /* access modifiers changed from: protected */
    public void a(SendAuth.Resp resp) {
        UMAuthListener uMAuthListener = (UMAuthListener) e.a(UMAuthListener.class, this.f);
        if (resp.errCode == 0) {
            a(resp.code, uMAuthListener);
        } else if (resp.errCode != -2) {
            SocializeException socializeException = new SocializeException(TextUtils.concat("weixin auth error (", String.valueOf(resp.errCode), "):", resp.errStr).toString());
            if (uMAuthListener != null) {
                uMAuthListener.onError(a.WEIXIN, 0, socializeException);
            }
        } else if (uMAuthListener != null) {
            uMAuthListener.onCancel(a.WEIXIN, 0);
        }
    }

    public void a(Context context, UMAuthListener uMAuthListener) {
        if (a(context)) {
            this.e.h();
            uMAuthListener.onComplete(a.WEIXIN, 1, null);
        }
    }

    public boolean a() {
        return true;
    }

    public int b() {
        if (this.i) {
            return 10085;
        }
        return 10086;
    }

    private void f() {
        if (this.e == null) {
            return;
        }
        if (this.e.f()) {
            g.c("refresh", "requesting access token with refresh");
            this.e.a(c(com.umeng.socialize.weixin.a.a.a("https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=" + this.d.appId + "&grant_type=refresh_token&refresh_token=" + this.e.b())));
            return;
        }
        g.b("refresh", "weixin refresh token is expired");
    }

    public void b(Activity activity, final UMAuthListener uMAuthListener) {
        String a2 = this.e.a();
        String e2 = this.e.e();
        if (TextUtils.isEmpty(a2) || TextUtils.isEmpty(e2)) {
            g.b("UMWXHandler", "please check had authed...");
            b.a(new Runnable() {
                public void run() {
                    uMAuthListener.onComplete(a.WEIXIN, 2, null);
                }
            });
            return;
        }
        if (!this.e.d()) {
            g.c("refresh", "getting auth with refresh token");
            f();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("https://api.weixin.qq.com/sns/userinfo?access_token=");
        sb.append(e2).append("&openid=").append(a2);
        sb.append("&lang=zh_CN");
        final Map<String, String> e3 = e(com.umeng.socialize.weixin.a.a.a(sb.toString()));
        b.a(new Runnable() {
            public void run() {
                uMAuthListener.onComplete(a.WEIXIN, 2, e3);
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(final Map<String, String> map) throws SocializeException {
        new Thread(new Runnable() {
            public void run() {
                if (UMWXHandler.this.j != null) {
                    com.umeng.socialize.d.e eVar = new com.umeng.socialize.d.e(UMWXHandler.this.j);
                    eVar.a("to", "wxsession");
                    eVar.a("usid", (String) map.get("uid"));
                    eVar.a("access_token", (String) map.get("access_token"));
                    eVar.a(Oauth2AccessToken.KEY_REFRESH_TOKEN, (String) map.get(Oauth2AccessToken.KEY_REFRESH_TOKEN));
                    eVar.a("expires_in", (String) map.get("expires_in"));
                    eVar.a("app_id", UMWXHandler.this.d.appId);
                    eVar.a("app_secret", UMWXHandler.this.d.appSecret);
                    g.b("upload token resp = " + com.umeng.socialize.d.g.a(eVar));
                }
            }
        }).start();
    }

    private Map<String, String> e(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.has("errcode")) {
                g.b("UMWXHandler", str + "");
                hashMap.put("errcode", jSONObject.getString("errcode"));
                return hashMap;
            }
            hashMap.put("openid", jSONObject.opt("openid").toString());
            hashMap.put("nickname", jSONObject.opt("nickname").toString());
            hashMap.put("language", jSONObject.opt("language").toString());
            hashMap.put("city", jSONObject.opt("city").toString());
            hashMap.put("province", jSONObject.opt("province").toString());
            hashMap.put("country", jSONObject.opt("country").toString());
            hashMap.put("headimgurl", jSONObject.opt("headimgurl").toString());
            hashMap.put(GameAppOperation.GAME_UNION_ID, jSONObject.opt(GameAppOperation.GAME_UNION_ID).toString());
            hashMap.put("sex", jSONObject.opt("sex").toString());
            JSONArray jSONArray = jSONObject.getJSONArray("privilege");
            int length = jSONArray.length();
            if (length <= 0) {
                return hashMap;
            }
            String[] strArr = new String[length];
            for (int i2 = 0; i2 < length; i2++) {
                strArr[i2] = jSONArray.get(i2).toString();
            }
            hashMap.put("privilege", strArr.toString());
            return hashMap;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return Collections.emptyMap();
        }
    }

    public boolean a(Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        if (activity == null) {
            g.c("UMError", "Weixin share activity is null");
            return false;
        }
        this.h = this.d.getName();
        if (c()) {
            this.c = new q(shareContent);
            if (this.c != null) {
                this.c.a();
                String str = this.c.f3948a;
                q qVar = this.c;
                if (str == "emoji" && this.i) {
                    Toast.makeText(activity, "微信朋友圈不支持表情分享...", 0).show();
                    return false;
                }
            }
            this.g = uMShareListener;
            return a(new q(shareContent));
        } else if (!Config.IsToastTip) {
            return false;
        } else {
            Toast.makeText(activity, "你还没有安装微信", 0).show();
            return false;
        }
    }

    public boolean c() {
        return this.k.isWXAppInstalled();
    }

    public boolean a(q qVar) {
        SendMessageToWX.Req req = new SendMessageToWX.Req();
        g.c("weixin", "share type:" + this.c.f3948a);
        req.transaction = f(this.c.f3948a);
        req.message = qVar.b();
        switch (this.h) {
            case WEIXIN:
                req.scene = 0;
                break;
            case WEIXIN_CIRCLE:
                req.scene = 1;
                break;
            case WEIXIN_FAVORITE:
                req.scene = 2;
                break;
            default:
                req.scene = 2;
                break;
        }
        return this.k.sendReq(req);
    }

    /* access modifiers changed from: protected */
    public void a(SendMessageToWX.Resp resp) {
        switch (resp.errCode) {
            case -3:
            case -1:
                if (this.g != null) {
                    this.g.onError(this.h, new SocializeException(resp.errCode, resp.errStr));
                    return;
                }
                return;
            case -2:
                if (this.g != null) {
                    this.g.onCancel(this.h);
                    return;
                }
                return;
            case 0:
                if (this.g != null) {
                    this.g.onResult(this.h);
                    return;
                }
                return;
            default:
                g.c("UMWXHandler", "微信发送 -- 未知错误.");
                return;
        }
    }

    public IWXAPIEventHandler d() {
        return this.l;
    }

    public IWXAPI e() {
        return this.k;
    }

    private String f(String str) {
        if (str == null) {
            return String.valueOf(System.currentTimeMillis());
        }
        return str + System.currentTimeMillis();
    }
}
