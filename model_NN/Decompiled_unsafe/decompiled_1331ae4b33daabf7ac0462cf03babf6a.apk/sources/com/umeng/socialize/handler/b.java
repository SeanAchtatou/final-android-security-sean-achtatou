package com.umeng.socialize.handler;

import android.app.Activity;
import com.umeng.socialize.ShareContent;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.c.a;
import java.util.Map;

/* compiled from: UMAPIShareHandler */
class b implements UMAuthListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f3926a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ShareContent f3927b;
    final /* synthetic */ UMShareListener c;
    final /* synthetic */ UMAPIShareHandler d;

    b(UMAPIShareHandler uMAPIShareHandler, Activity activity, ShareContent shareContent, UMShareListener uMShareListener) {
        this.d = uMAPIShareHandler;
        this.f3926a = activity;
        this.f3927b = shareContent;
        this.c = uMShareListener;
    }

    public void onComplete(a aVar, int i, Map<String, String> map) {
        com.umeng.socialize.common.b.b(new c(this));
    }

    public void onError(a aVar, int i, Throwable th) {
        this.c.onError(aVar, th);
    }

    public void onCancel(a aVar, int i) {
        this.c.onCancel(aVar);
    }
}
