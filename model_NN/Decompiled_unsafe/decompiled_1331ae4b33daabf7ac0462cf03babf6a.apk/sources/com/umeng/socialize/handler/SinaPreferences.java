package com.umeng.socialize.handler;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.umeng.socialize.utils.g;

public class SinaPreferences {

    /* renamed from: a  reason: collision with root package name */
    private String f3872a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f3873b = null;
    private String c = null;
    private long d = 0;
    private String e = null;
    private String f = null;
    private String g = null;
    private boolean h = false;
    private SharedPreferences i = null;

    public SinaPreferences(Context context, String str) {
        this.i = context.getSharedPreferences(str, 0);
        this.f3872a = this.i.getString("access_key", null);
        this.f = this.i.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN, null);
        this.f3873b = this.i.getString("access_secret", null);
        this.e = this.i.getString("access_token", null);
        this.c = this.i.getString("uid", null);
        this.d = this.i.getLong("expires_in", 0);
        this.h = this.i.getBoolean("isfollow", false);
    }

    public String a() {
        return this.e;
    }

    public SinaPreferences a(Bundle bundle) {
        this.e = bundle.getString("access_token");
        this.f = bundle.getString(Oauth2AccessToken.KEY_REFRESH_TOKEN);
        this.c = bundle.getString("uid");
        g.d("xxxx authend = " + this.e);
        if (!TextUtils.isEmpty(bundle.getString("expires_in"))) {
            this.d = (Long.valueOf(bundle.getString("expires_in")).longValue() * 1000) + System.currentTimeMillis();
        }
        return this;
    }

    public String b() {
        return this.c;
    }

    public boolean c() {
        boolean z;
        StringBuilder append = new StringBuilder().append("xxxx auth = ").append(this.e).append("   ");
        if (!TextUtils.isEmpty(this.e)) {
            z = true;
        } else {
            z = false;
        }
        g.d(append.append(z).toString());
        return !TextUtils.isEmpty(this.e);
    }

    public boolean d() {
        return this.h;
    }

    public void a(boolean z) {
        this.i.edit().putBoolean("isfollow", z).commit();
    }

    public void e() {
        this.i.edit().putString("access_key", this.f3872a).putString("access_secret", this.f3873b).putString("access_token", this.e).putString(Oauth2AccessToken.KEY_REFRESH_TOKEN, this.f).putString("uid", this.c).putLong("expires_in", this.d).commit();
        g.a("save auth succeed");
    }

    public void f() {
        this.f3872a = null;
        this.f3873b = null;
        this.e = null;
        this.c = null;
        this.d = 0;
        this.i.edit().clear().commit();
        g.d("xxxx dele = " + this.e);
    }
}
