package com.umeng.socialize.weixin.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.c.a;
import com.umeng.socialize.handler.UMWXHandler;
import com.umeng.socialize.utils.g;

public abstract class WXCallbackActivity extends Activity implements IWXAPIEventHandler {

    /* renamed from: a  reason: collision with root package name */
    protected UMWXHandler f3981a = null;

    /* renamed from: b  reason: collision with root package name */
    private final String f3982b = WXCallbackActivity.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g.a("create wx callback activity");
        this.f3981a = (UMWXHandler) UMShareAPI.get(getApplicationContext()).getHandler(a.WEIXIN);
        g.b("xxxx wxhandler=" + this.f3981a);
        this.f3981a.a(getApplicationContext(), PlatformConfig.getPlatform(a.WEIXIN));
        this.f3981a.e().handleIntent(getIntent(), this);
    }

    /* access modifiers changed from: protected */
    public final void onNewIntent(Intent intent) {
        g.c(this.f3982b, "### WXCallbackActivity   onNewIntent");
        super.onNewIntent(intent);
        setIntent(intent);
        this.f3981a = (UMWXHandler) UMShareAPI.get(getApplicationContext()).getHandler(a.WEIXIN);
        this.f3981a.a(getApplicationContext(), PlatformConfig.getPlatform(a.WEIXIN));
        this.f3981a.e().handleIntent(intent, this);
    }

    public void onResp(BaseResp baseResp) {
        if (!(this.f3981a == null || baseResp == null)) {
            try {
                this.f3981a.d().onResp(baseResp);
            } catch (Exception e) {
            }
        }
        finish();
    }

    public void onReq(BaseReq baseReq) {
        if (this.f3981a != null) {
            this.f3981a.d().onReq(baseReq);
        }
        finish();
    }
}
