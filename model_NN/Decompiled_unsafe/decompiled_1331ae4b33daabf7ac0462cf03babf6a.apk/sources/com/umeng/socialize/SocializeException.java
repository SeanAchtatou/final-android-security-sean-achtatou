package com.umeng.socialize;

public class SocializeException extends RuntimeException {

    /* renamed from: b  reason: collision with root package name */
    private static final long f3766b = 1;

    /* renamed from: a  reason: collision with root package name */
    protected int f3767a = 5000;
    private String c = "";

    public int getErrorCode() {
        return this.f3767a;
    }

    public SocializeException(int i, String str) {
        super(str);
        this.f3767a = i;
        this.c = str;
    }

    public SocializeException(String str, Throwable th) {
        super(str, th);
        this.c = str;
    }

    public SocializeException(String str) {
        super(str);
        this.c = str;
    }

    public String getMessage() {
        return this.c;
    }
}
