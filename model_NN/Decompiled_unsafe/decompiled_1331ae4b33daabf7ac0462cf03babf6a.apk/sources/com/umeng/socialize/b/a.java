package com.umeng.socialize.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Pair;
import com.tencent.connect.common.Constants;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.a.c;
import com.umeng.socialize.d.o;
import com.umeng.socialize.d.p;
import com.umeng.socialize.handler.UMSSOHandler;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.view.UMFriendListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* compiled from: SocialRouter */
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private int f3777a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final Map<com.umeng.socialize.c.a, UMSSOHandler> f3778b = new HashMap();
    private final List<Pair<com.umeng.socialize.c.a, String>> c = new ArrayList();
    private C0059a d;
    private Context e;

    public a(Context context) {
        List<Pair<com.umeng.socialize.c.a, String>> list = this.c;
        list.add(new Pair(com.umeng.socialize.c.a.LAIWANG, "com.umeng.socialize.handler.UMLWHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.LAIWANG_DYNAMIC, "com.umeng.socialize.handler.UMLWHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.SINA, "com.umeng.socialize.handler.SinaSsoHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.PINTEREST, "com.umeng.socialize.handler.UMPinterestHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.QZONE, "com.umeng.socialize.handler.QZoneSsoHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.QQ, "com.umeng.socialize.handler.UMQQSsoHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.RENREN, "com.umeng.socialize.handler.RenrenSsoHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.TENCENT, "com.umeng.socialize.handler.QQwbHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.WEIXIN, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.WEIXIN_CIRCLE, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.WEIXIN_FAVORITE, "com.umeng.socialize.handler.UMWXHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.YIXIN, "com.umeng.socialize.handler.UMYXHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.YIXIN_CIRCLE, "com.umeng.socialize.handler.UMYXHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.EMAIL, "com.umeng.socialize.handler.EmailHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.EVERNOTE, "com.umeng.socialize.handler.UMEvernoteHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.FACEBOOK, "com.umeng.socialize.handler.UMFacebookHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.FLICKR, "com.umeng.socialize.handler.UMFlickrHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.FOURSQUARE, "com.umeng.socialize.handler.UMFourSquareHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.GOOGLEPLUS, "com.umeng.socialize.handler.UMGooglePlusHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.INSTAGRAM, "com.umeng.socialize.handler.UMInstagramHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.KAKAO, "com.umeng.socialize.handler.UMKakaoHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.LINE, "com.umeng.socialize.handler.UMLineHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.LINKEDIN, "com.umeng.socialize.handler.UMLinkedInHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.POCKET, "com.umeng.socialize.handler.UMPocketHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.WHATSAPP, "com.umeng.socialize.handler.UMWhatsAppHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.YNOTE, "com.umeng.socialize.handler.UMYNoteHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.SMS, "com.umeng.socialize.handler.SmsHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.DOUBAN, "com.umeng.socialize.handler.DoubanHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.TUMBLR, "com.umeng.socialize.handler.UMTumblrHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.TWITTER, "com.umeng.socialize.handler.TwitterHandler"));
        list.add(new Pair(com.umeng.socialize.c.a.ALIPAY, "com.umeng.socialize.handler.AlipayHandler"));
        this.d = new C0059a(this.f3778b);
        this.e = null;
        this.e = context;
        a();
    }

    private void a() {
        UMSSOHandler uMSSOHandler;
        for (Pair next : this.c) {
            if (next.first == com.umeng.socialize.c.a.WEIXIN_CIRCLE || next.first == com.umeng.socialize.c.a.WEIXIN_FAVORITE) {
                uMSSOHandler = this.f3778b.get(com.umeng.socialize.c.a.WEIXIN);
            } else if (next.first == com.umeng.socialize.c.a.YIXIN_CIRCLE) {
                uMSSOHandler = this.f3778b.get(com.umeng.socialize.c.a.YIXIN);
            } else if (next.first == com.umeng.socialize.c.a.LAIWANG_DYNAMIC) {
                uMSSOHandler = this.f3778b.get(com.umeng.socialize.c.a.LAIWANG);
            } else if (next.first != com.umeng.socialize.c.a.TENCENT) {
                uMSSOHandler = a((String) next.second);
            } else if (!Config.WBBYQQ) {
                uMSSOHandler = a("com.umeng.socialize.handler.TencentWBSsoHandler");
            } else {
                uMSSOHandler = a((String) next.second);
            }
            this.f3778b.put(next.first, uMSSOHandler);
        }
    }

    private UMSSOHandler a(String str) {
        try {
            return (UMSSOHandler) Class.forName(str).newInstance();
        } catch (Exception e2) {
            g.d("xxxx", "ignore=" + e2);
            return null;
        }
    }

    public UMSSOHandler a(com.umeng.socialize.c.a aVar) {
        UMSSOHandler uMSSOHandler = this.f3778b.get(aVar);
        if (uMSSOHandler != null) {
            uMSSOHandler.a(this.e, PlatformConfig.getPlatform(aVar));
        }
        return uMSSOHandler;
    }

    public void a(int i, int i2, Intent intent) {
        UMSSOHandler a2 = a(i);
        if (a2 != null) {
            a2.a(i, i2, intent);
        }
    }

    private UMSSOHandler a(int i) {
        int i2;
        int i3 = Constants.REQUEST_QQ_SHARE;
        if (!(i == 10103 || i == 11101)) {
            i3 = i;
        }
        if (i == 64207 || i == 64206) {
            i3 = 64206;
        }
        if (i == 32973 || i == 765) {
            i2 = 5659;
        } else {
            i2 = i3;
        }
        for (UMSSOHandler next : this.f3778b.values()) {
            if (next != null && i2 == next.b()) {
                return next;
            }
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void
     arg types: [android.app.Activity, com.umeng.socialize.UMAuthListener]
     candidates:
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.UMAuthListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.app.Activity, com.umeng.socialize.view.UMFriendListener):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.PlatformConfig$Platform):void
      com.umeng.socialize.handler.UMSSOHandler.a(android.content.Context, com.umeng.socialize.UMAuthListener):void */
    public void a(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, aVar)) {
            if (uMAuthListener == null) {
                uMAuthListener = new b(this);
            }
            this.f3778b.get(aVar).a(activity, PlatformConfig.getPlatform(aVar));
            this.f3778b.get(aVar).a((Context) activity, uMAuthListener);
        }
    }

    public void b(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, aVar)) {
            if (uMAuthListener == null) {
                uMAuthListener = new c(this);
            }
            this.f3778b.get(aVar).a(activity, PlatformConfig.getPlatform(aVar));
            this.f3778b.get(aVar).b(activity, uMAuthListener);
        }
    }

    public void a(Activity activity, com.umeng.socialize.c.a aVar, UMFriendListener uMFriendListener) {
        if (this.d.a(activity, aVar)) {
            if (uMFriendListener == null) {
                uMFriendListener = new d(this);
            }
            this.f3778b.get(aVar).a(activity, PlatformConfig.getPlatform(aVar));
            this.f3778b.get(aVar).a(activity, uMFriendListener);
        }
    }

    public boolean a(Activity activity, com.umeng.socialize.c.a aVar) {
        if (!this.d.a(activity, aVar)) {
            return false;
        }
        this.f3778b.get(aVar).a(activity, PlatformConfig.getPlatform(aVar));
        return this.f3778b.get(aVar).a(activity);
    }

    public boolean b(Activity activity, com.umeng.socialize.c.a aVar) {
        if (!this.d.a(activity, aVar)) {
            return false;
        }
        this.f3778b.get(aVar).a(activity, PlatformConfig.getPlatform(aVar));
        return this.f3778b.get(aVar).b(activity);
    }

    public void c(Activity activity, com.umeng.socialize.c.a aVar, UMAuthListener uMAuthListener) {
        if (this.d.a(activity, aVar)) {
            UMSSOHandler uMSSOHandler = this.f3778b.get(aVar);
            uMSSOHandler.a(activity, PlatformConfig.getPlatform(aVar));
            uMSSOHandler.a(activity, uMAuthListener);
        }
    }

    public void a(Activity activity, ShareAction shareAction, UMShareListener uMShareListener) {
        if (this.d.a(activity, shareAction)) {
            if (uMShareListener == null) {
                uMShareListener = new e(this);
            }
            com.umeng.socialize.c.a platform = shareAction.getPlatform();
            UMSSOHandler uMSSOHandler = this.f3778b.get(platform);
            uMSSOHandler.a(shareAction.getFrom());
            uMSSOHandler.a(activity, PlatformConfig.getPlatform(platform));
            if (!platform.toString().equals("TENCENT") && !platform.toString().equals("RENREN") && !platform.toString().equals("DOUBAN")) {
                if (platform.toString().equals("WEIXIN")) {
                    c.a(activity, "wxsession", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else if (platform.toString().equals("WEIXIN_CIRCLE")) {
                    c.a(activity, "wxtimeline", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else if (platform.toString().equals("WEIXIN_FAVORITE")) {
                    c.a(activity, "wxfavorite", shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                } else {
                    c.a(activity, platform.toString().toLowerCase(), shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
                }
            }
            if (platform.toString().equals("TENCENT") && Config.WBBYQQ) {
                c.a(activity, platform.toString().toLowerCase(), shareAction.getShareContent().mText, shareAction.getShareContent().mMedia);
            }
            if (Config.isloadUrl) {
                a(activity, shareAction);
            }
            uMSSOHandler.a(activity, shareAction.getShareContent(), uMShareListener);
        }
    }

    private void a(Activity activity, ShareAction shareAction) {
        String lowerCase;
        String str = shareAction.getShareContent().mTargetUrl;
        if (!TextUtils.isEmpty(str)) {
            if (shareAction.getPlatform().toString().equals("WEIXIN")) {
                lowerCase = "wxsession";
            } else if (shareAction.getPlatform().toString().equals("")) {
                lowerCase = "wxtimeline";
            } else {
                lowerCase = shareAction.getPlatform().toString().toLowerCase();
            }
            p a2 = com.umeng.socialize.d.g.a(new o(activity, lowerCase, str));
            g.b("xxxxxx resp" + a2);
            if (a2 == null || a2.l != 200) {
                g.b("upload url fail ");
            } else {
                shareAction.withTargetUrl(a2.f3833a);
            }
        }
    }

    /* renamed from: com.umeng.socialize.b.a$a  reason: collision with other inner class name */
    /* compiled from: SocialRouter */
    static class C0059a {

        /* renamed from: a  reason: collision with root package name */
        private Map<com.umeng.socialize.c.a, UMSSOHandler> f3779a;

        public C0059a(Map<com.umeng.socialize.c.a, UMSSOHandler> map) {
            this.f3779a = map;
        }

        public boolean a(Context context, com.umeng.socialize.c.a aVar) {
            if (!a(context)) {
                return false;
            }
            if (!a(aVar)) {
                return false;
            }
            if (this.f3779a.get(aVar).a()) {
                return true;
            }
            g.e(aVar.toString() + "平台不支持授权,无法完成操作");
            return false;
        }

        public boolean a(Activity activity, ShareAction shareAction) {
            com.umeng.socialize.c.a platform;
            if (a(activity) && (platform = shareAction.getPlatform()) != null && a(platform)) {
                return true;
            }
            return false;
        }

        private boolean a(Context context) {
            if (context != null) {
                return true;
            }
            g.b("Context is null");
            return false;
        }

        private boolean a(com.umeng.socialize.c.a aVar) {
            PlatformConfig.Platform platform = PlatformConfig.configs.get(aVar);
            if (platform != null && !platform.isConfigured()) {
                g.b(aVar + ": 没有配置相关的Appkey、Secret");
                return false;
            } else if (this.f3779a.get(aVar) != null) {
                return true;
            } else {
                g.b("没有配置 " + aVar + " 的jar包");
                return false;
            }
        }
    }
}
