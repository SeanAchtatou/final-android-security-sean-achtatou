package com.umeng.socialize.a;

import com.umeng.socialize.c.a;
import com.umeng.socialize.d.a.f;
import java.util.Map;
import org.json.JSONObject;

/* compiled from: AnalyticsResponse */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public Map<a, Integer> f3773a;

    /* renamed from: b  reason: collision with root package name */
    public String f3774b;

    public b(JSONObject jSONObject) {
        super(jSONObject);
    }

    public String toString() {
        return "ShareMultiResponse [mInfoMap=" + this.f3773a + ", mWeiboId=" + this.f3774b + ", mMsg=" + this.k + ", mStCode=" + this.l + "]";
    }
}
