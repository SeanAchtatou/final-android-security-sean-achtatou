package com.umeng.socialize.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.utils.g;
import com.umeng.socialize.utils.h;

/* compiled from: AnalyticsReqeust */
public class a extends b {
    private String g;
    private String h;
    private String i;
    private String j;
    private UMediaObject k;

    public a(Context context, String str, String str2) {
        super(context, "", b.class, 9, b.C0061b.POST);
        this.f3808b = context;
        this.g = str;
        this.j = str2;
        a(1);
    }

    public void a(String str) {
        this.i = str;
    }

    public void a(UMediaObject uMediaObject) {
        this.k = uMediaObject;
    }

    public void a() {
        super.a();
        Object[] objArr = new Object[2];
        objArr[0] = this.g;
        objArr[1] = this.h == null ? "" : this.h;
        String format = String.format("{\"%s\":\"%s\"}", objArr);
        String a2 = h.a(this.f3808b);
        a("to", format);
        a("sns", format);
        a("ak", a2);
        a("type", this.i);
        a("ct", this.j);
        g.c("para", "parameter" + format + " " + h.a(this.f3808b) + " " + this.i + " " + this.j);
        b(this.k);
        if (!TextUtils.isEmpty(Config.getAdapterSDK())) {
            a(Config.getAdapterSDK(), Config.getAdapterSDKVersion());
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        StringBuilder sb = new StringBuilder();
        sb.append("/share/multi_add/");
        sb.append(h.a(this.f3808b));
        sb.append("/").append(Config.EntityKey).append("/");
        return sb.toString();
    }
}
