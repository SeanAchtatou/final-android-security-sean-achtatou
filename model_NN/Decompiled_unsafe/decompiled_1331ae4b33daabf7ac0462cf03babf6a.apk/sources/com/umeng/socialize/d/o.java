package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.d.a.b;
import com.umeng.socialize.utils.h;

/* compiled from: UrlRequest */
public class o extends b {
    private String g;
    private String h;

    public o(Context context, String str, String str2) {
        super(context, "", p.class, 26, b.C0061b.POST);
        this.f3808b = context;
        this.g = str2;
        this.h = str;
    }

    public void a() {
        super.a();
        a("url", this.g);
        a("to", this.h);
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/link/add/" + h.a(this.f3808b) + "/";
    }
}
