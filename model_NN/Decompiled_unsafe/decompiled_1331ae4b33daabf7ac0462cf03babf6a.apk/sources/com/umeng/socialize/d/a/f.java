package com.umeng.socialize.d.a;

import android.text.TextUtils;
import com.meizu.cloud.pushsdk.notification.model.TimeDisplaySetting;
import com.umeng.socialize.d.b.h;
import com.umeng.socialize.utils.g;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: SocializeReseponse */
public class f extends h {
    protected JSONObject j;
    public String k;
    public int l = -103;

    public f(JSONObject jSONObject) {
        super(jSONObject);
        this.j = a(jSONObject);
        a();
    }

    public boolean b() {
        boolean z;
        StringBuilder append = new StringBuilder().append("is http 200:");
        if (this.l == 200) {
            z = true;
        } else {
            z = false;
        }
        g.c("umeng_share_response", append.append(z).toString());
        return this.l == 200;
    }

    public JSONObject c() {
        return this.j;
    }

    private JSONObject a(JSONObject jSONObject) {
        if (jSONObject == null) {
            g.b("SocializeReseponse", "failed requesting");
            return null;
        }
        try {
            this.l = jSONObject.optInt(TimeDisplaySetting.START_SHOW_TIME, 1998);
            if (this.l == 0) {
                g.b("SocializeReseponse", "no status code in response.");
                return null;
            }
            this.k = jSONObject.optString("msg", "");
            String optString = jSONObject.optString("data", null);
            if (TextUtils.isEmpty(optString)) {
                return null;
            }
            if (this.l != 200) {
                a(optString);
            }
            return new JSONObject(optString);
        } catch (JSONException e) {
            e.printStackTrace();
            g.b("SocializeReseponse", "Data body can`t convert to json ");
            return null;
        }
    }

    public void a() {
    }

    private void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                JSONObject jSONObject2 = jSONObject.getJSONObject(next);
                String string = jSONObject2.getString("msg");
                if (!TextUtils.isEmpty(string)) {
                    a(next, string);
                } else {
                    a(next, jSONObject2.getJSONObject("data").getString("platform_error"));
                }
            }
        } catch (Exception e) {
        }
    }

    private void a(String str, String str2) {
        g.b("SocializeReseponse", "error message -> " + str + " : " + str2);
    }
}
