package com.umeng.socialize.d;

import android.content.Context;
import com.umeng.socialize.c.a;
import com.umeng.socialize.d.a.b;

/* compiled from: ShareFriendsRequest */
public class h extends b {
    private String g;
    private a h;

    public h(Context context, a aVar, String str) {
        super(context, "", i.class, 14, b.C0061b.GET);
        this.f3808b = context;
        this.g = str;
        this.h = aVar;
    }

    public void a() {
        a("to", this.h.toString().toLowerCase());
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "/share/friends/" + com.umeng.socialize.utils.h.a(this.f3808b) + "/" + this.g + "/";
    }
}
