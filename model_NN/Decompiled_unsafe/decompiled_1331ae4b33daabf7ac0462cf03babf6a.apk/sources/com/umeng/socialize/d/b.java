package com.umeng.socialize.d;

import com.umeng.socialize.d.a.f;
import com.umeng.socialize.utils.g;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ActionBarResponse */
public class b extends f {

    /* renamed from: a  reason: collision with root package name */
    public int f3814a;

    /* renamed from: b  reason: collision with root package name */
    public int f3815b;
    public int c;
    public String d;
    public String e;
    public int f;
    public int g;
    public String h;
    public int i;

    public b(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        JSONObject jSONObject = this.j;
        if (jSONObject == null) {
            g.b("SocializeReseponse", "data json is null....");
            return;
        }
        try {
            if (jSONObject.has("cm")) {
                this.f3815b = jSONObject.getInt("cm");
            }
            if (jSONObject.has("ek")) {
                this.e = jSONObject.getString("ek");
            }
            if (jSONObject.has("ft")) {
                this.f = jSONObject.getInt("ft");
            }
            if (jSONObject.has("fr")) {
                this.g = jSONObject.optInt("fr", 0);
            }
            if (jSONObject.has("lk")) {
                this.c = jSONObject.getInt("lk");
            }
            if (jSONObject.has("pv")) {
                this.f3814a = jSONObject.getInt("pv");
            }
            if (jSONObject.has("sid")) {
                this.d = jSONObject.getString("sid");
            }
            if (jSONObject.has("uid")) {
                this.h = jSONObject.getString("uid");
            }
            if (jSONObject.has("sn")) {
                this.i = jSONObject.getInt("sn");
            }
        } catch (JSONException e2) {
            g.b("SocializeReseponse", "Parse json error[ " + jSONObject.toString() + " ]", e2);
        }
    }
}
