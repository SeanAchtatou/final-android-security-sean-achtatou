package com.umeng.socialize.d;

import android.text.TextUtils;
import com.umeng.socialize.PlatformConfig;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: PlatformTokenUploadResponse */
public class f extends com.umeng.socialize.d.a.f {

    /* renamed from: a  reason: collision with root package name */
    public String f3827a;

    public f(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        super.a();
        d();
        e();
    }

    private void d() {
        if (this.j != null) {
            try {
                JSONObject jSONObject = this.j.getJSONObject(PlatformConfig.TencentWeibo.Name);
                if (jSONObject != null) {
                    String optString = jSONObject.optString("user_id");
                    if (!TextUtils.isEmpty(optString)) {
                        this.f3827a = optString;
                    }
                }
            } catch (JSONException e) {
            }
        }
    }

    private void e() {
    }
}
