package com.umeng.socialize.d.a;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.d.b.d;
import com.umeng.socialize.d.b.e;
import com.umeng.socialize.d.b.g;
import com.umeng.socialize.media.UMediaObject;
import com.umeng.socialize.utils.h;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

/* compiled from: SocializeRequest */
public abstract class b extends g {

    /* renamed from: a  reason: collision with root package name */
    protected Class<? extends f> f3807a;

    /* renamed from: b  reason: collision with root package name */
    protected Context f3808b;
    protected int c;
    private C0061b g;
    private boolean h = true;
    private Map<String, g.a> i = new HashMap();
    private Map<String, String> j = new HashMap();
    private int k = 1;

    /* compiled from: SocializeRequest */
    protected enum a {
        IMAGE,
        VEDIO
    }

    /* renamed from: com.umeng.socialize.d.a.b$b  reason: collision with other inner class name */
    /* compiled from: SocializeRequest */
    protected enum C0061b {
        GET,
        POST
    }

    /* access modifiers changed from: protected */
    public abstract String b();

    public b(Context context, String str, Class<? extends f> cls, int i2, C0061b bVar) {
        super("");
        this.f3807a = cls;
        this.c = i2;
        this.f3808b = context;
        this.g = bVar;
        com.umeng.socialize.d.b.a.a(h.a(context));
    }

    public void a(int i2) {
        this.k = i2;
    }

    public void a(String str, String str2) {
        if (!TextUtils.isEmpty(str2)) {
            this.j.put(str, str2);
        }
    }

    public void a(byte[] bArr, a aVar, String str) {
        if (a.IMAGE == aVar) {
            String a2 = com.umeng.socialize.common.a.a(bArr);
            if (TextUtils.isEmpty(a2)) {
                a2 = "png";
            }
            if (TextUtils.isEmpty(str)) {
                str = System.currentTimeMillis() + "";
            }
            this.i.put(e.f3822b, new g.a(str + "" + a2, bArr));
        }
    }

    public void b(UMediaObject uMediaObject) {
        if (uMediaObject != null) {
            if (uMediaObject.c()) {
                for (Map.Entry next : uMediaObject.h().entrySet()) {
                    a((String) next.getKey(), next.getValue().toString());
                }
            } else {
                byte[] i2 = uMediaObject.i();
                if (i2 != null) {
                    a(i2, a.IMAGE, null);
                }
            }
            try {
                if (uMediaObject instanceof com.umeng.socialize.media.a) {
                    com.umeng.socialize.media.a aVar = (com.umeng.socialize.media.a) uMediaObject;
                    String d = aVar.d();
                    String e = aVar.e();
                    if (!TextUtils.isEmpty(d) || !TextUtils.isEmpty(e)) {
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put(e.g, d);
                        jSONObject.put(e.h, e);
                        a("ext", jSONObject.toString());
                    }
                }
            } catch (Exception e2) {
                com.umeng.socialize.utils.g.b("can`t add qzone title & thumb. " + e2.getMessage());
            }
        }
    }

    public Map<String, Object> d() {
        Map<String, Object> a2 = d.a(this.f3808b);
        if (!TextUtils.isEmpty(Config.EntityKey)) {
            a2.put("ek", Config.EntityKey);
        }
        if (!TextUtils.isEmpty(Config.SessionId)) {
            a2.put("sid", Config.SessionId);
        }
        a2.put("tp", Integer.valueOf(this.k));
        a2.put("opid", Integer.valueOf(this.c));
        a2.put("uid", Config.UID);
        a2.putAll(this.j);
        String a3 = a(a2);
        com.umeng.socialize.utils.g.b("xxxxx", "raw=" + a3);
        com.umeng.socialize.utils.g.a("--->", "unencrypt string: " + a3);
        if (a3 != null) {
            try {
                String a4 = com.umeng.socialize.d.b.a.a(a3, "UTF-8");
                a2.clear();
                a2.put("ud_post", a4);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        com.umeng.socialize.utils.g.b("xxxxx send~~=" + a2);
        return a2;
    }

    public Map<String, g.a> c() {
        return this.i;
    }

    public JSONObject e() {
        return null;
    }

    public String f() {
        Map<String, Object> a2 = d.a(this.f3808b);
        if (!TextUtils.isEmpty(Config.EntityKey)) {
            a2.put("ek", Config.EntityKey);
        }
        if (!TextUtils.isEmpty(Config.SessionId)) {
            a2.put("sid", Config.SessionId);
        }
        a2.put("tp", Integer.valueOf(this.k));
        a2.put("opid", Integer.valueOf(this.c));
        a2.put("uid", Config.UID);
        a2.putAll(this.j);
        return d.a(j(), a2);
    }

    public void b(String str) {
        try {
            super.b(new URL(new URL(str), b()).toString());
        } catch (Exception e) {
            throw new SocializeException("Can not generate correct url in SocializeRequest [" + j() + "]", e);
        }
    }

    public void a() {
        a("pcv", "2.0");
        String a2 = com.umeng.socialize.utils.d.a(this.f3808b);
        a("imei", a2);
        a("md5imei", com.umeng.socialize.d.b.a.c(a2));
        a("de", Build.MODEL);
        a("mac", com.umeng.socialize.utils.d.f(this.f3808b));
        a("android_id", com.umeng.socialize.utils.d.e(this.f3808b));
        a("sn", com.umeng.socialize.utils.d.a());
        a("os", "Android");
        a("en", com.umeng.socialize.utils.d.b(this.f3808b)[0]);
        a("uid", null);
        a("sdkv", "5.1.4");
        a("dt", String.valueOf(System.currentTimeMillis()));
        if (g() == d && this.j != null && !this.j.isEmpty()) {
            Set<String> keySet = this.j.keySet();
            Uri.Builder builder = new Uri.Builder();
            for (String next : keySet) {
                if (this.j.get(next) != null) {
                    builder.appendQueryParameter(next, this.j.get(next));
                }
            }
            this.f += "?" + builder.build().getEncodedQuery();
        }
    }

    private String a(Map<String, Object> map) {
        if (this.j.isEmpty()) {
            return null;
        }
        try {
            return new JSONObject(map).toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public String g() {
        switch (c.f3813a[this.g.ordinal()]) {
            case 1:
                return d;
            default:
                return e;
        }
    }
}
