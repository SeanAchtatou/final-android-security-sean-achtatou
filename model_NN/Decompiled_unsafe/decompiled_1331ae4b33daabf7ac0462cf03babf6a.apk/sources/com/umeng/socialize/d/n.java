package com.umeng.socialize.d;

import com.umeng.socialize.d.a.f;
import org.json.JSONObject;

/* compiled from: UploadImageResponse */
public class n extends f {

    /* renamed from: a  reason: collision with root package name */
    public String f3831a;

    /* renamed from: b  reason: collision with root package name */
    public String f3832b;

    public n(JSONObject jSONObject) {
        super(jSONObject);
    }

    public void a() {
        super.a();
        JSONObject jSONObject = this.j;
        if (jSONObject != null) {
            this.f3831a = jSONObject.optString("large_url");
            this.f3832b = jSONObject.optString("small_url");
        }
    }
}
