package com.umeng.socialize.d.b;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.socialize.Config;
import com.umeng.socialize.SocializeException;
import com.umeng.socialize.common.h;
import com.umeng.socialize.utils.g;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/* compiled from: SocializeNetUtils */
public class d {
    public static String a(String str, Map<String, Object> map) {
        if (TextUtils.isEmpty(str) || map == null || map.size() == 0) {
            return str;
        }
        if (!str.endsWith("?")) {
            str = str + "?";
        }
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = sb;
        for (String next : map.keySet()) {
            if (map.get(next) != null) {
                sb2 = sb2.append(next + "=" + URLEncoder.encode(map.get(next).toString()) + "&");
            }
        }
        StringBuilder sb3 = new StringBuilder(str);
        try {
            String str2 = sb2.substring(0, sb2.length() - 1).toString();
            g.c("SocializeNetUtils", "##### 未加密参数 : " + str2);
            sb3.append("ud_get=" + a.a(str2, "UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        g.c("SocializeNetUtils", "#### 完整请求链接 : " + sb3.toString());
        return sb3.toString();
    }

    public static Map<String, Object> a(Context context) {
        HashMap hashMap = new HashMap();
        String a2 = com.umeng.socialize.utils.d.a(context);
        if (!TextUtils.isEmpty(a2)) {
            hashMap.put("imei", a2);
            hashMap.put("md5imei", a.c(a2));
        }
        String f = com.umeng.socialize.utils.d.f(context);
        if (TextUtils.isEmpty(f)) {
            g.e("SocializeNetUtils", "Get MacAddress failed. Check permission android.permission.ACCESS_WIFI_STATE [" + com.umeng.socialize.utils.d.a(context, "android.permission.ACCESS_WIFI_STATE") + "]");
        } else {
            hashMap.put("mac", f);
        }
        if (!TextUtils.isEmpty(h.f3805a)) {
            hashMap.put("uid", h.f3805a);
        }
        try {
            hashMap.put("en", com.umeng.socialize.utils.d.b(context)[0]);
        } catch (Exception e) {
            hashMap.put("en", "Unknown");
        }
        hashMap.put("de", Build.MODEL);
        hashMap.put("sdkv", "5.1.4");
        hashMap.put("os", "Android");
        hashMap.put("dt", Long.valueOf(System.currentTimeMillis()));
        String a3 = com.umeng.socialize.utils.h.a(context);
        if (TextUtils.isEmpty(a3)) {
            throw new SocializeException("No found appkey.");
        }
        hashMap.put("ak", a3);
        hashMap.put(e.j, "2.0");
        if (!TextUtils.isEmpty(Config.EntityKey)) {
            hashMap.put("ek", Config.EntityKey);
            g.d("10.13", "ek = " + Config.EntityKey);
        }
        if (!TextUtils.isEmpty(Config.SessionId)) {
            hashMap.put("sid", Config.SessionId);
        }
        try {
            hashMap.put("tp", 0);
        } catch (Exception e2) {
        }
        return hashMap;
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061 A[SYNTHETIC, Splitter:B:18:0x0061] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x009f A[SYNTHETIC, Splitter:B:56:0x009f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.String r7) {
        /*
            r1 = 0
            boolean r0 = android.text.TextUtils.isEmpty(r7)
            if (r0 == 0) goto L_0x0009
            r0 = r1
        L_0x0008:
            return r0
        L_0x0009:
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00ce, all -> 0x009b }
            r3.<init>()     // Catch:{ Exception -> 0x00ce, all -> 0x009b }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00d3, all -> 0x00c4 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x00d3, all -> 0x00c4 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00d3, all -> 0x00c4 }
            java.lang.Object r0 = r0.getContent()     // Catch:{ Exception -> 0x00d3, all -> 0x00c4 }
            java.io.InputStream r0 = (java.io.InputStream) r0     // Catch:{ Exception -> 0x00d3, all -> 0x00c4 }
            java.lang.String r2 = "image"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            r4.<init>()     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            java.lang.String r5 = "getting image from url"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            com.umeng.socialize.utils.g.c(r2, r4)     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
        L_0x0039:
            int r4 = r0.read(r2)     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            r5 = -1
            if (r4 == r5) goto L_0x006b
            r5 = 0
            r3.write(r2, r5, r4)     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            goto L_0x0039
        L_0x0045:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
        L_0x0049:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cb }
            r4.<init>()     // Catch:{ all -> 0x00cb }
            java.lang.String r5 = "xxxxx e="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x00cb }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ all -> 0x00cb }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00cb }
            com.umeng.socialize.utils.g.b(r0)     // Catch:{ all -> 0x00cb }
            if (r2 == 0) goto L_0x0069
            r2.close()     // Catch:{ IOException -> 0x008b, all -> 0x0094 }
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ IOException -> 0x00bc }
        L_0x0069:
            r0 = r1
            goto L_0x0008
        L_0x006b:
            byte[] r1 = r3.toByteArray()     // Catch:{ Exception -> 0x0045, all -> 0x00c6 }
            if (r0 == 0) goto L_0x0079
            r0.close()     // Catch:{ IOException -> 0x007b, all -> 0x0084 }
            if (r3 == 0) goto L_0x0079
            r3.close()     // Catch:{ IOException -> 0x00b8 }
        L_0x0079:
            r0 = r1
            goto L_0x0008
        L_0x007b:
            r0 = move-exception
            if (r3 == 0) goto L_0x0079
            r3.close()     // Catch:{ IOException -> 0x0082 }
            goto L_0x0079
        L_0x0082:
            r0 = move-exception
            goto L_0x0079
        L_0x0084:
            r0 = move-exception
            if (r3 == 0) goto L_0x008a
            r3.close()     // Catch:{ IOException -> 0x00ba }
        L_0x008a:
            throw r0
        L_0x008b:
            r0 = move-exception
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ IOException -> 0x0092 }
            goto L_0x0069
        L_0x0092:
            r0 = move-exception
            goto L_0x0069
        L_0x0094:
            r0 = move-exception
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ IOException -> 0x00be }
        L_0x009a:
            throw r0
        L_0x009b:
            r0 = move-exception
            r3 = r1
        L_0x009d:
            if (r1 == 0) goto L_0x00a7
            r1.close()     // Catch:{ IOException -> 0x00a8, all -> 0x00b1 }
            if (r3 == 0) goto L_0x00a7
            r3.close()     // Catch:{ IOException -> 0x00c0 }
        L_0x00a7:
            throw r0
        L_0x00a8:
            r1 = move-exception
            if (r3 == 0) goto L_0x00a7
            r3.close()     // Catch:{ IOException -> 0x00af }
            goto L_0x00a7
        L_0x00af:
            r1 = move-exception
            goto L_0x00a7
        L_0x00b1:
            r0 = move-exception
            if (r3 == 0) goto L_0x00b7
            r3.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00b7:
            throw r0
        L_0x00b8:
            r0 = move-exception
            goto L_0x0079
        L_0x00ba:
            r1 = move-exception
            goto L_0x008a
        L_0x00bc:
            r0 = move-exception
            goto L_0x0069
        L_0x00be:
            r1 = move-exception
            goto L_0x009a
        L_0x00c0:
            r1 = move-exception
            goto L_0x00a7
        L_0x00c2:
            r1 = move-exception
            goto L_0x00b7
        L_0x00c4:
            r0 = move-exception
            goto L_0x009d
        L_0x00c6:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
            goto L_0x009d
        L_0x00cb:
            r0 = move-exception
            r1 = r2
            goto L_0x009d
        L_0x00ce:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x0049
        L_0x00d3:
            r0 = move-exception
            r2 = r1
            goto L_0x0049
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.socialize.d.b.d.a(java.lang.String):byte[]");
    }
}
