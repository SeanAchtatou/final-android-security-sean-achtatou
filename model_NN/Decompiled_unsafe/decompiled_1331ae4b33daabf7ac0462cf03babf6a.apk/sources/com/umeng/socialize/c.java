package com.umeng.socialize;

import com.umeng.socialize.shareboard.a;
import com.umeng.socialize.utils.ShareBoardlistener;

/* compiled from: ShareAction */
class c implements ShareBoardlistener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ShareAction f3784a;

    c(ShareAction shareAction) {
        this.f3784a = shareAction;
    }

    public void a(a aVar, com.umeng.socialize.c.a aVar2) {
        ShareContent shareContent;
        int indexOf = this.f3784a.g.indexOf(aVar2);
        int size = this.f3784a.i.size();
        if (size != 0) {
            if (indexOf < size) {
                shareContent = (ShareContent) this.f3784a.i.get(indexOf);
            } else {
                shareContent = (ShareContent) this.f3784a.i.get(size - 1);
            }
            ShareContent unused = this.f3784a.f3764a = shareContent;
        }
        int size2 = this.f3784a.j.size();
        if (size2 != 0) {
            if (indexOf < size2) {
                UMShareListener unused2 = this.f3784a.d = (UMShareListener) this.f3784a.j.get(indexOf);
            } else {
                UMShareListener unused3 = this.f3784a.d = (UMShareListener) this.f3784a.j.get(size2 - 1);
            }
        }
        this.f3784a.setPlatform(aVar2);
        this.f3784a.share();
    }
}
