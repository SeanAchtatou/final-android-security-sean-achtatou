package com.cmsc.cmmusic;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static int ic_launcher = com.shoujiduoduo.ringtone.R.drawable.abc_ab_share_pack_mtrl_alpha;
    }

    public static final class string {
        public static int app_name = com.shoujiduoduo.ringtone.R.layout.abc_action_bar_title_item;
        public static int hello_world = com.shoujiduoduo.ringtone.R.layout.abc_action_bar_up_container;
        public static int menu_settings = com.shoujiduoduo.ringtone.R.layout.abc_action_bar_view_list_nav_layout;
    }

    public static final class style {
        public static int AppBaseTheme = com.shoujiduoduo.ringtone.R.anim.abc_fade_in;
        public static int AppTheme = com.shoujiduoduo.ringtone.R.anim.abc_fade_out;
    }
}
