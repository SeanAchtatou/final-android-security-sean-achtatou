package com.cmsc.cmmusic.common.data;

public class RegistResult extends Result {
    private String identityID;

    public RegistResult() {
    }

    public RegistResult(Result result) {
        if (result != null) {
            setResCode(result.getResCode());
            setResMsg(result.getResMsg());
        }
    }

    public String getIdentityID() {
        return this.identityID;
    }

    public void setIdentityID(String str) {
        this.identityID = str;
    }

    public String toString() {
        return "RegistResult [identityID=" + this.identityID + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
    }
}
