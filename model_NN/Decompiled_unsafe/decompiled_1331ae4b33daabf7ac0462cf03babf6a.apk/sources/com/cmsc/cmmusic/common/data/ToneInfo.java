package com.cmsc.cmmusic.common.data;

public class ToneInfo {
    private String info;
    private String price;
    private String singerName;
    private String singerNameLetter;
    private String toneID;
    private String toneName;
    private String toneNameLetter;
    private String tonePreListenAddress;
    private String toneType;
    private String toneValidDay;

    public String getToneID() {
        return this.toneID;
    }

    public void setToneID(String str) {
        this.toneID = str;
    }

    public String getToneName() {
        return this.toneName;
    }

    public void setToneName(String str) {
        this.toneName = str;
    }

    public String getToneNameLetter() {
        return this.toneNameLetter;
    }

    public void setToneNameLetter(String str) {
        this.toneNameLetter = str;
    }

    public String getSingerName() {
        return this.singerName;
    }

    public void setSingerName(String str) {
        this.singerName = str;
    }

    public String getSingerNameLetter() {
        return this.singerNameLetter;
    }

    public void setSingerNameLetter(String str) {
        this.singerNameLetter = str;
    }

    public String getToneValidDay() {
        return this.toneValidDay;
    }

    public void setToneValidDay(String str) {
        this.toneValidDay = str;
    }

    public String getInfo() {
        return this.info;
    }

    public void setInfo(String str) {
        this.info = str;
    }

    public String getToneType() {
        return this.toneType;
    }

    public void setToneType(String str) {
        this.toneType = str;
    }

    public String getPrice() {
        return this.price;
    }

    public void setPrice(String str) {
        this.price = str;
    }

    public String getTonePreListenAddress() {
        return this.tonePreListenAddress;
    }

    public void setTonePreListenAddress(String str) {
        this.tonePreListenAddress = str;
    }

    public String toString() {
        return "ToneInfo [toneID=" + this.toneID + ", toneName=" + this.toneName + ", toneNameLetter=" + this.toneNameLetter + ", singerName=" + this.singerName + ", singerNameLetter=" + this.singerNameLetter + ", price=" + this.price + ", toneValidDay=" + this.toneValidDay + ", info=" + this.info + ", tonePreListenAddress=" + this.tonePreListenAddress + ", toneType=" + this.toneType + "]";
    }
}
