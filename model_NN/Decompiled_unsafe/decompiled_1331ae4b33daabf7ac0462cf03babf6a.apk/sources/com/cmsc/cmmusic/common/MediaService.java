package com.cmsc.cmmusic.common;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import java.util.Timer;
import java.util.TimerTask;

public class MediaService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnSeekCompleteListener {
    public static final String ACTION = "com.android.mediaService";
    public static final String CONTIUNING = "com.android.ttpod.musicservicecommand.CONTIUNING";
    public static final String PAUSE = "com.android.ttpod.musicservicecommand.PAUSE";
    public static final String PLAY = "com.android.ttpod.musicservicecommand.PLAY";
    public static final String PREV = "com.android.ttpod.musicservicecommand.PREVIOUS";
    public static final String SEEK = "com.android.ttpod.musicservicecommand.SEEK";
    public static final String STOP = "com.android.ttpod.musicservicecommand.STOP";
    protected static final String TAG = "MediaService";
    private static ErrorListener errorListen;
    /* access modifiers changed from: private */
    public TimerTask mTask;
    /* access modifiers changed from: private */
    public MediaPlayer player;
    public BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String stringExtra = intent.getStringExtra("mean");
            if (stringExtra.equals(MediaService.PLAY)) {
                String stringExtra2 = intent.getStringExtra("path");
                Log.i(MediaService.TAG, "paly path = " + stringExtra2);
                if (!(stringExtra2 == null || stringExtra2.trim() == null)) {
                    MediaService.this.play(stringExtra2);
                }
                int duration = MediaService.this.player.getDuration();
                Bundle bundle = new Bundle();
                bundle.putInt("duration", duration);
                Message message = new Message();
                message.what = -1;
                message.setData(bundle);
                OrderOwnRingView.seekBarHandle.sendMessage(message);
                if (MediaService.this.mTask != null) {
                    MediaService.this.mTask.cancel();
                }
                MediaService.this.mTask = new TimerTask() {
                    public void run() {
                        int currentPosition = MediaService.this.player.getCurrentPosition();
                        Log.i(MediaService.TAG, "pos==" + currentPosition);
                        Bundle bundle = new Bundle();
                        bundle.putInt("posiztion", currentPosition);
                        Message message = new Message();
                        message.what = -1;
                        message.setData(bundle);
                        OrderOwnRingView.seekBarHandle.sendMessage(message);
                    }
                };
                MediaService.this.timer.schedule(MediaService.this.mTask, 5, 500);
            }
            if (stringExtra.equals(MediaService.PAUSE)) {
                MediaService.this.pause();
            }
            if (stringExtra.equals(MediaService.CONTIUNING)) {
                MediaService.this.player.start();
            }
            if (stringExtra.equals(MediaService.SEEK)) {
                MediaService.this.playerToPosiztion(intent.getIntExtra("posiztion", 0));
            }
            if (stringExtra.equals(MediaService.STOP)) {
                if (MediaService.this.mTask != null) {
                    MediaService.this.mTask.cancel();
                }
                if (MediaService.this.player != null) {
                    MediaService.this.player.reset();
                }
            }
        }
    };
    /* access modifiers changed from: private */
    public Timer timer = new Timer();

    public interface ErrorListener {
        boolean onError(MediaPlayer mediaPlayer, int i, int i2);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        Log.i(TAG, "MediaService启动了");
        this.player = new MediaPlayer();
        this.player.setOnSeekCompleteListener(this);
        this.player.setOnCompletionListener(this);
        this.player.setOnErrorListener(this);
        registerReceiver(this.receiver, new IntentFilter(ACTION));
    }

    public void onDestroy() {
        super.onDestroy();
        this.timer.cancel();
        stop();
    }

    /* access modifiers changed from: private */
    public void play(String str) {
        if (this.player == null) {
            this.player = new MediaPlayer();
        }
        try {
            this.player.reset();
            this.player.setDataSource(str);
            this.player.prepare();
            this.player.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void pause() {
        if (this.player != null && this.player.isPlaying()) {
            this.player.pause();
        }
    }

    private void stop() {
        if (this.player != null) {
            this.player.stop();
            this.player.release();
        }
    }

    /* access modifiers changed from: private */
    public void playerToPosiztion(int i) {
        if (i > 0 && i < this.player.getDuration()) {
            this.player.seekTo(i);
        }
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        if (this.mTask != null) {
            this.mTask.cancel();
        }
        sendBroadcast(new Intent(OrderOwnRingView.ONPLAYCOMPLETED));
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        if (this.player.isPlaying()) {
            this.player.start();
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (errorListen == null) {
            return false;
        }
        errorListen.onError(mediaPlayer, i, i2);
        return false;
    }

    public static void setErrorListener(ErrorListener errorListener) {
        errorListen = errorListener;
    }
}
