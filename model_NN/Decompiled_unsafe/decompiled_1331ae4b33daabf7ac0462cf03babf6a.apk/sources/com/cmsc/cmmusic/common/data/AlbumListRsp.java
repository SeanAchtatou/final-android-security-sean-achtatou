package com.cmsc.cmmusic.common.data;

import java.util.List;

public class AlbumListRsp extends Result {
    private List<AlbumInfo> albumInfos;
    private String resCounter;

    public String getResCounter() {
        return this.resCounter;
    }

    public void setResCounter(String str) {
        this.resCounter = str;
    }

    public List<AlbumInfo> getAlbumInfos() {
        return this.albumInfos;
    }

    public void setAlbumInfos(List<AlbumInfo> list) {
        this.albumInfos = list;
    }

    public String toString() {
        return "AlbumListRsp [resCounter=" + this.resCounter + ", albumInfos=" + this.albumInfos + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
