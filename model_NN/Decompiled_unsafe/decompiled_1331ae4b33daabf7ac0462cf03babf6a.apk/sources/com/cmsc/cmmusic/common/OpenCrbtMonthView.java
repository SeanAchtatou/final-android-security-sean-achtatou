package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;

public class OpenCrbtMonthView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "OpenCrbtMonthView";
    private static final String Month_DEFAULT_PRICE = "10";
    private String definedseq;
    private TextView monthPriceTxt;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public OpenCrbtMonthView(Context context, Bundle bundle) {
        super(context, bundle);
        this.definedseq = bundle.getString("definedseq");
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.monthPriceTxt = new TextView(this.mCurActivity);
        this.monthPriceTxt.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.monthPriceTxt);
    }

    private String getMonthPrice() {
        return EnablerInterface.getPriceString(EnablerInterface.getPrice(this.policyObj, "10"));
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    EnablerInterface.openRingbackMonth(this.mCurActivity, this.curMusicID, EnablerInterface.getPrice(this.policyObj, "10"), this.definedseq, new CMMusicCallback<OrderResult>() {
                        public void operationResult(OrderResult orderResult) {
                            OpenCrbtMonthView.this.mCurActivity.closeActivity(orderResult);
                        }
                    });
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mCurActivity.showToast("开通失败");
        }
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        this.baseView.setVisibility(8);
        setUserTip("您尚未开通“" + this.policyObj.getBizInfos().get(0).getDescription() + "”彩铃包月订购功能，请问您是否需要开通？\n\n");
        this.monthPriceTxt.setText("彩铃包月开通价格：" + getMonthPrice() + "/月");
    }
}
