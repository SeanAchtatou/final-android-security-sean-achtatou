package com.cmsc.cmmusic.common.data;

import java.util.List;

public class TagListRsp extends Result {
    private String resCounter;
    private List<TagInfo> tagInfos;

    public String getResCounter() {
        return this.resCounter;
    }

    public void setResCounter(String str) {
        this.resCounter = str;
    }

    public List<TagInfo> getTagInfos() {
        return this.tagInfos;
    }

    public void setTagInfos(List<TagInfo> list) {
        this.tagInfos = list;
    }

    public String toString() {
        return "TagListRsp [resCounter=" + this.resCounter + ", tagInfos=" + this.tagInfos + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
