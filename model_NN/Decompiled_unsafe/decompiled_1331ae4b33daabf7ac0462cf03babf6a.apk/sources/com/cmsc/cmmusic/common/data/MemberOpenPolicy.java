package com.cmsc.cmmusic.common.data;

import java.util.List;

public class MemberOpenPolicy extends Result {
    private List<ClubUserInfo> clubUserInfos;
    private String mobile;

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public List<ClubUserInfo> getClubUserInfos() {
        return this.clubUserInfos;
    }

    public void setClubUserInfos(List<ClubUserInfo> list) {
        this.clubUserInfos = list;
    }

    public String toString() {
        return "MemberOpenPolicy [mobile=" + this.mobile + ", clubUserInfos=" + this.clubUserInfos + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
