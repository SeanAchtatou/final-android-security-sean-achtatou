package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.SongMonthInfo;
import java.util.Iterator;
import java.util.List;

final class OpenSongMonthView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "OpenMemberView";
    static final int ORDINARY_MEM = 100;
    static final int SENIOR_MEM = 101;
    private RadioGroup monthGroup = new RadioGroup(this.mCurActivity);

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public OpenSongMonthView(Context context, Bundle bundle) {
        super(context, bundle);
        RadioButton radioButton = new RadioButton(this.mCurActivity);
        radioButton.setText("10元包月（200首）");
        radioButton.setId(2);
        RadioButton radioButton2 = new RadioButton(this.mCurActivity);
        radioButton2.setText("5元包月（50首）");
        radioButton2.setId(1);
        RadioButton radioButton3 = new RadioButton(this.mCurActivity);
        radioButton3.setText("3元包月（20首）");
        radioButton3.setId(0);
        this.monthGroup.addView(radioButton);
        this.monthGroup.addView(radioButton2);
        this.monthGroup.addView(radioButton3);
        radioButton.setChecked(true);
        this.smsView.addView(this.monthGroup);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                default:
                    return;
                case 2:
                    Log.d(LOG_TAG, "open songmonth by sms");
                    this.mCurActivity.popView(null);
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        this.mCurActivity.popView(null);
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        this.baseView.setVisibility(8);
        List<SongMonthInfo> songMonthInfos = this.policyObj.getSongMonthInfos();
        if (songMonthInfos == null || songMonthInfos.size() <= 0) {
            Result result = new Result();
            result.setResCode(this.policyObj.getResCode());
            result.setResMsg(this.policyObj.getResMsg());
            this.mCurActivity.closeActivity(result);
            return;
        }
        setUserTip("点击确认将开通全曲包月下载功能\n");
        this.monthGroup = new RadioGroup(this.mCurActivity);
        int i = 0;
        Iterator<SongMonthInfo> it = songMonthInfos.iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                this.rootView.addView(this.monthGroup);
                return;
            }
            SongMonthInfo next = it.next();
            RadioButton radioButton = new RadioButton(this.mCurActivity);
            radioButton.setText(next.getTypeDesc());
            radioButton.setId(Integer.parseInt(next.getType()));
            if (i2 == 0) {
                radioButton.setChecked(true);
            }
            this.monthGroup.addView(radioButton);
            i = i2 + 1;
        }
    }
}
