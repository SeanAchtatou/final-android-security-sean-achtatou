package com.cmsc.cmmusic.common.data;

public class RechargeResult extends Result {
    private String redirectUrl;

    public String getRedirectUrl() {
        return this.redirectUrl;
    }

    public void setRedirectUrl(String str) {
        this.redirectUrl = str;
    }

    public String toString() {
        return "RechargeResult [redirectUrl=" + this.redirectUrl + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
