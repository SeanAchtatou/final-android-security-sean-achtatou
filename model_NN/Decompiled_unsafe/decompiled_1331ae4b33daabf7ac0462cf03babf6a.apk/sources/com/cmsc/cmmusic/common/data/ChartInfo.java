package com.cmsc.cmmusic.common.data;

public class ChartInfo {
    private String chartCode;
    private String chartName;

    public String getChartCode() {
        return this.chartCode;
    }

    public void setChartCode(String str) {
        this.chartCode = str;
    }

    public String getChartName() {
        return this.chartName;
    }

    public void setChartName(String str) {
        this.chartName = str;
    }

    public String toString() {
        return "ChartInfo [chartCode=" + this.chartCode + ", chartName=" + this.chartName + "]";
    }
}
