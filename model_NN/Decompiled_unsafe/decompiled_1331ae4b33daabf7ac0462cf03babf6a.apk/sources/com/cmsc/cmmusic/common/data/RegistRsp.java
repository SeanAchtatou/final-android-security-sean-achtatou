package com.cmsc.cmmusic.common.data;

public class RegistRsp extends Result {
    private String isExistent;
    private String isVerified;
    private String mobile;
    private String registerMode;

    public String getIsExistent() {
        return this.isExistent;
    }

    public void setIsExistent(String str) {
        this.isExistent = str;
    }

    public String getIsVerified() {
        return this.isVerified;
    }

    public void setIsVerified(String str) {
        this.isVerified = str;
    }

    public String getRegisterMode() {
        return this.registerMode;
    }

    public void setRegisterMode(String str) {
        this.registerMode = str;
    }

    public String getMobile() {
        return this.mobile;
    }

    public void setMobile(String str) {
        this.mobile = str;
    }

    public String toString() {
        return "RegistRsp [isExistent=" + this.isExistent + ", isVerified=" + this.isVerified + ", registerMode=" + this.registerMode + ", mobile=" + this.mobile + "]";
    }
}
