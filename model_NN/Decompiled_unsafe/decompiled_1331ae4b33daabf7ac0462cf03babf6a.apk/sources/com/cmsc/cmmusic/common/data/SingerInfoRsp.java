package com.cmsc.cmmusic.common.data;

public class SingerInfoRsp extends Result {
    private SingerInfo singerInfo;

    public SingerInfo getSingerInfo() {
        return this.singerInfo;
    }

    public void setSingerInfo(SingerInfo singerInfo2) {
        this.singerInfo = singerInfo2;
    }

    public String toString() {
        return "SingerInfoRsp [singerInfo=" + this.singerInfo + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
