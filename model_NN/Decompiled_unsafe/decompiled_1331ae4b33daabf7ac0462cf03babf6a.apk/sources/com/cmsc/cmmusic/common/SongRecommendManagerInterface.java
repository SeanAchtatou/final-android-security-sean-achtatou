package com.cmsc.cmmusic.common;

import android.content.Context;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.SongRecommendResult;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.xmlpull.v1.XmlPullParserException;

public class SongRecommendManagerInterface {
    public static Result associateSongRecommend(Context context, String str, CMMusicCallback<SongRecommendResult> cMMusicCallback) {
        try {
            return EnablerInterface.getSongRecommendResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/recommond/music", EnablerInterface.buildRequsetXml("<contentId>" + str + "</contentId>")));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Result associateSingerSongRecommend(Context context, String str, CMMusicCallback<SongRecommendResult> cMMusicCallback) {
        try {
            return EnablerInterface.getSongRecommendResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/recommond/music", EnablerInterface.buildRequsetXml("<singerName>" + URLEncoder.encode(str, "UTF-8") + "</singerName>")));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (XmlPullParserException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static SongRecommendResult likeSongRecommend(Context context, CMMusicCallback<SongRecommendResult> cMMusicCallback) {
        try {
            return EnablerInterface.getSongRecommendResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/recommond/msisdn", ""));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static SongRecommendResult dealSongRecommend(Context context, CMMusicCallback<SongRecommendResult> cMMusicCallback) {
        try {
            return EnablerInterface.getSongRecommendResult(HttpPostCore.httpConnection(context, "http://218.200.227.123:95/sdkServer/1.0/recommond/coop", ""));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
