package com.cmsc.cmmusic.common.data;

public class UserInfo {
    public static final String NON_MEM = "0";
    public static final String ORDINARY_MEM = "1";
    public static final String SENIOR_MEM = "2";
    public static final String SPECIAL_MEM = "3";
    private String email;
    private String imageUrl;
    private String imageUrl2;
    private String imageUrl3;
    private String mbUrl;
    private String memLevel;
    private String spaceUrl;
    private String uid;
    private String username;

    public String getUid() {
        return this.uid;
    }

    public void setUid(String str) {
        this.uid = str;
    }

    public String getUsername() {
        return this.username;
    }

    public void setUsername(String str) {
        this.username = str;
    }

    public String getMemLevel() {
        return this.memLevel;
    }

    public void setMemLevel(String str) {
        this.memLevel = str;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String str) {
        this.email = str;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String str) {
        this.imageUrl = str;
    }

    public String getImageUrl2() {
        return this.imageUrl2;
    }

    public void setImageUrl2(String str) {
        this.imageUrl2 = str;
    }

    public String getImageUrl3() {
        return this.imageUrl3;
    }

    public void setImageUrl3(String str) {
        this.imageUrl3 = str;
    }

    public String getSpaceUrl() {
        return this.spaceUrl;
    }

    public void setSpaceUrl(String str) {
        this.spaceUrl = str;
    }

    public String getMbUrl() {
        return this.mbUrl;
    }

    public void setMbUrl(String str) {
        this.mbUrl = str;
    }

    public String toString() {
        return "UserInfo [uid=" + this.uid + ", username=" + this.username + ", memLevel=" + this.memLevel + ", email=" + this.email + ", imageUrl=" + this.imageUrl + ", imageUrl2=" + this.imageUrl2 + ", imageUrl3=" + this.imageUrl3 + ", spaceUrl=" + this.spaceUrl + ", mbUrl=" + this.mbUrl + "]";
    }
}
