package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.init.Utils;
import java.io.InputStream;

final class LoginView extends LinearLayout {
    private static final String KEY = "sdk2l@31";
    protected Button btnCancel = null;
    protected Button btnSure = null;
    protected LinearLayout btnView = null;
    protected Bundle curExtraInfo = null;
    private EditText edtName;
    private EditText edtPwd;
    protected CMMusicActivity mCurActivity;
    protected Handler mHandler = null;
    protected OrderPolicy policyObj;
    protected LinearLayout rootView = null;
    private CheckBox tipCb;

    public LoginView(Context context, Bundle bundle) {
        super(context);
        this.mCurActivity = (CMMusicActivity) context;
        this.mHandler = new Handler();
        this.curExtraInfo = bundle;
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        this.rootView = new LinearLayout(context);
        this.rootView.setOrientation(1);
        this.rootView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.rootView.setPadding(10, 5, 10, 20);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        scrollView.addView(this.rootView);
        addView(scrollView);
        initLogoView();
        initContentView(context);
        initBtnView(context);
    }

    private void initLogoView() {
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        try {
            InputStream open = this.mCurActivity.getAssets().open("logo.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            ImageView imageView = new ImageView(this.mCurActivity);
            imageView.setImageDrawable(bitmapDrawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 100));
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.rootView.addView(linearLayout);
    }

    private void initContentView(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        textView.setTextAppearance(context, 16973892);
        textView.setText("账户名：");
        textView.setGravity(7);
        linearLayout.addView(textView);
        this.edtName = new EditText(this.mCurActivity);
        this.edtName.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        this.edtName.setInputType(3);
        this.edtName.setKeyListener(new DigitsKeyListener(false, false));
        linearLayout.addView(this.edtName);
        this.rootView.addView(linearLayout);
        LinearLayout linearLayout2 = new LinearLayout(context);
        linearLayout2.setOrientation(0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout2.setLayoutParams(layoutParams2);
        TextView textView2 = new TextView(context);
        textView2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        textView2.setTextAppearance(context, 16973892);
        textView2.setText("密码：");
        textView2.setGravity(7);
        linearLayout2.addView(textView2);
        this.edtPwd = new EditText(this.mCurActivity);
        this.edtPwd.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1.0f));
        linearLayout2.addView(this.edtPwd);
        this.rootView.addView(linearLayout2);
        LinearLayout linearLayout3 = new LinearLayout(context);
        linearLayout3.setOrientation(0);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams3.setMargins(dip2px(20.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout3.setLayoutParams(layoutParams3);
        this.tipCb = new CheckBox(context);
        this.tipCb.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.tipCb.setText("记住账户名");
        this.tipCb.setGravity(16);
        linearLayout3.addView(this.tipCb);
        this.tipCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (PreferenceUtil.getFirstUse(LoginView.this.mCurActivity)) {
                    PreferenceUtil.saveFirstUse(LoginView.this.mCurActivity, false);
                }
            }
        });
        this.rootView.addView(linearLayout3);
        if (PreferenceUtil.getFirstUse(this.mCurActivity)) {
            this.tipCb.setChecked(true);
        }
        if (PreferenceUtil.getCheckState(this.mCurActivity)) {
            this.tipCb.setChecked(true);
            this.edtName.setText(PreferenceUtil.getName(this.mCurActivity));
        }
    }

    private void initBtnView(Context context) {
        this.btnView = new LinearLayout(context);
        this.btnView.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        this.btnView.setLayoutParams(layoutParams);
        this.btnSure = new Button(context);
        this.btnSure.setText("登陆");
        this.btnSure.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.btnView.addView(this.btnSure);
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                LoginView.this.sureClicked();
            }
        });
        addView(this.btnView);
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        String editable = this.edtName.getText().toString();
        if (this.tipCb.isChecked()) {
            PreferenceUtil.saveCheckState(this.mCurActivity, true);
            PreferenceUtil.saveName(this.mCurActivity, editable);
        } else {
            PreferenceUtil.saveCheckState(this.mCurActivity, false);
            PreferenceUtil.saveName(this.mCurActivity, editable);
        }
        this.mCurActivity.closeActivity(null);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        final String editable = this.edtName.getText().toString();
        final String editable2 = this.edtPwd.getText().toString();
        if (editable == null || !Utils.validatePhoneNumber(editable)) {
            setErrorText(this.edtName, "请正确输入手机号码");
        } else if (editable2 == null || "".equals(editable2)) {
            setErrorText(this.edtPwd, "密码不能为空");
        } else {
            if (PreferenceUtil.getFirstUse(this.mCurActivity)) {
                PreferenceUtil.saveFirstUse(this.mCurActivity, false);
            }
            String editable3 = this.edtName.getText().toString();
            if (this.tipCb.isChecked()) {
                PreferenceUtil.saveCheckState(this.mCurActivity, true);
                PreferenceUtil.saveName(this.mCurActivity, editable3);
            } else {
                PreferenceUtil.saveCheckState(this.mCurActivity, false);
                PreferenceUtil.saveName(this.mCurActivity, editable3);
            }
            this.mCurActivity.showProgressBar("请稍候...");
            new Thread(new Runnable() {
                public void run() {
                    try {
                        LoginView.this.mCurActivity.closeActivity(EnablerInterface.getLoginResult(HttpPostCore.httpConnection(LoginView.this.mCurActivity, "http://218.200.227.123:95/sdkServer/1.0/pay/member/login", EnablerInterface.buildRequsetXml("<accountName>" + editable + "</accountName><password>" + DES.encryptDES(editable2, LoginView.KEY) + "</password>"))));
                    } catch (Exception e) {
                        e.printStackTrace();
                        LoginView.this.mCurActivity.showToast("登陆失败，请重试");
                    } finally {
                        LoginView.this.mCurActivity.hideProgressBar();
                    }
                }
            }).start();
        }
    }

    private void setErrorText(EditText editText, String str) {
        Spanned fromHtml = Html.fromHtml("<font color='blue'>" + str + "</font>");
        editText.requestFocus();
        editText.setError(fromHtml);
    }

    private int dip2px(float f) {
        return (int) ((this.mCurActivity.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightPx() {
        int i = (int) ((((float) this.mCurActivity.getResources().getDisplayMetrics().heightPixels) * this.mCurActivity.getResources().getDisplayMetrics().density) + 0.5f);
        Log.d("getScreenHeightPx=", new StringBuilder().append(i).toString());
        return i;
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightDip() {
        int i = this.mCurActivity.getResources().getDisplayMetrics().heightPixels;
        Log.d("getScreenHeightDip=", new StringBuilder().append(i).toString());
        return i;
    }
}
