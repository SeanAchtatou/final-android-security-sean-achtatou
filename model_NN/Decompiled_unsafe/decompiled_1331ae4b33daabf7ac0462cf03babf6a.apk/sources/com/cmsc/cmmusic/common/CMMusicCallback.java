package com.cmsc.cmmusic.common;

import com.cmsc.cmmusic.common.data.Result;

public interface CMMusicCallback<T extends Result> {
    void operationResult(Result result);
}
