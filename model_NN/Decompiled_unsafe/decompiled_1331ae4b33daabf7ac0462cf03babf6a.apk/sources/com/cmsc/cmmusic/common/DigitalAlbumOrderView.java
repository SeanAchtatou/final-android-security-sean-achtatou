package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.AlbumInfo;
import com.cmsc.cmmusic.common.data.BizInfo;
import com.cmsc.cmmusic.common.data.OrderResult;
import java.util.ArrayList;
import java.util.Iterator;

class DigitalAlbumOrderView extends OrderView {
    private static final String LOG_TAG = "DigitalAlbumOrderView";
    private TextView priceTextView;
    private TextView sumTextView;

    public DigitalAlbumOrderView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.sumTextView = new TextView(this.mCurActivity);
        this.sumTextView.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.sumTextView);
        this.priceTextView = new TextView(this.mCurActivity);
        this.priceTextView.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.priceTextView);
        linearLayout.addView(getOrderCountView());
        TextView textView = new TextView(this.mCurActivity);
        textView.setTextAppearance(this.mCurActivity, 16973892);
        textView.setText("\n提示：\n具备彩铃功能的中国移动用户，购买数字专辑内的彩铃才有效。");
        linearLayout.addView(textView);
    }

    private LinearLayout getOrderCountView() {
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(0);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout.setGravity(16);
        TextView textView = new TextView(this.mCurActivity);
        textView.setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        textView.setTextAppearance(this.mCurActivity, 16973892);
        textView.setText("数量：");
        linearLayout.addView(textView);
        EditText editText = new EditText(this.mCurActivity);
        editText.setText("1");
        editText.setGravity(17);
        editText.setEnabled(false);
        editText.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        linearLayout.addView(editText);
        return linearLayout;
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        setUserTip("点击“确认”将订购该专辑");
        String sum = this.policyObj.getSum();
        if (sum != null) {
            this.sumTextView.setText("\n已        售：" + sum + "张");
        }
        AlbumInfo albumInfo = this.policyObj.getAlbumInfo();
        if (albumInfo != null) {
            this.curSongName = "专   辑:   " + albumInfo.getName();
            this.curSingerName = "歌   手:   " + albumInfo.getSingerName();
        }
        BizInfo phonePayTypeBizInfo = getPhonePayTypeBizInfo();
        if (phonePayTypeBizInfo != null) {
            this.priceTextView.setText("专辑价格：" + EnablerInterface.getPriceString(phonePayTypeBizInfo.getSalePrice()) + "/张");
        }
    }

    /* access modifiers changed from: protected */
    public BizInfo getPhonePayTypeBizInfo() {
        ArrayList<BizInfo> bizInfos = this.policyObj.getBizInfos();
        if (bizInfos == null) {
            return null;
        }
        Iterator<BizInfo> it = bizInfos.iterator();
        while (it.hasNext()) {
            BizInfo next = it.next();
            if ("00".equals(next.getPayType())) {
                return next;
            }
        }
        return bizInfos.get(0);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        Log.d(LOG_TAG, "sure button clicked");
        BizInfo phonePayTypeBizInfo = getPhonePayTypeBizInfo();
        if (phonePayTypeBizInfo != null) {
            EnablerInterface.orderDigitalAlbum(this.mCurActivity, this.curExtraInfo.getString("MusicId"), phonePayTypeBizInfo.getBizCode(), phonePayTypeBizInfo.getSalePrice(), this.policyObj.getMonLevel(), phonePayTypeBizInfo.getHold2(), new CMMusicCallback<OrderResult>() {
                public void operationResult(OrderResult orderResult) {
                    DigitalAlbumOrderView.this.mCurActivity.closeActivity(orderResult);
                }
            });
        }
    }
}
