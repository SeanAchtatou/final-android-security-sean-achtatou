package com.cmsc.cmmusic.common;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;

public class OpenBjhyView extends OrderView {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = null;
    private static final String LOG_TAG = "OpenBjhyView";
    private TextView memPriceTxt;

    static /* synthetic */ int[] $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType() {
        int[] iArr = $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType;
        if (iArr == null) {
            iArr = new int[OrderPolicy.OrderType.values().length];
            try {
                iArr[OrderPolicy.OrderType.net.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[OrderPolicy.OrderType.sms.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[OrderPolicy.OrderType.verifyCode.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType = iArr;
        }
        return iArr;
    }

    public OpenBjhyView(Context context, Bundle bundle) {
        super(context, bundle);
    }

    /* access modifiers changed from: protected */
    public void initContentView(LinearLayout linearLayout) {
        this.memPriceTxt = new TextView(this.mCurActivity);
        this.memPriceTxt.setTextAppearance(this.mCurActivity, 16973892);
        linearLayout.addView(this.memPriceTxt);
    }

    private String getBjhyPrice() {
        return EnablerInterface.getPriceString(EnablerInterface.getPrice(this.policyObj, ""));
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        try {
            switch ($SWITCH_TABLE$com$cmsc$cmmusic$common$data$OrderPolicy$OrderType()[this.orderType.ordinal()]) {
                case 1:
                    EnablerInterface.openBjhy(this.mCurActivity, EnablerInterface.getBizCode(this.policyObj), EnablerInterface.getPrice(this.policyObj, ""), new CMMusicCallback<OrderResult>() {
                        public void operationResult(OrderResult orderResult) {
                            OpenBjhyView.this.mCurActivity.closeActivity(orderResult);
                        }
                    });
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.mCurActivity.showToast("开通失败");
        }
    }

    /* access modifiers changed from: protected */
    public void updateNetView() {
        this.baseView.setVisibility(8);
        setUserTip("点击“确认”将开通咪咕超清白金会员功能。\n\n开通即可获尊享权益：\n1、振铃、歌曲无限量下载；\n2、MV无限量观看；\n3、彩铃及数字专辑7折购。\n\n");
        this.memPriceTxt.setText("会员开通价格：" + getBjhyPrice() + "/月");
    }
}
