package com.cmsc.cmmusic.common.data;

import java.util.List;

public class MusicListRsp extends Result {
    private List<MusicInfo> musics;
    private String resCounter;

    public String getResCounter() {
        return this.resCounter;
    }

    public void setResCounter(String str) {
        this.resCounter = str;
    }

    public List<MusicInfo> getMusics() {
        return this.musics;
    }

    public void setMusics(List<MusicInfo> list) {
        this.musics = list;
    }

    public String toString() {
        return "MusicListRsp [resCounter=" + this.resCounter + ", musics=" + this.musics + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
