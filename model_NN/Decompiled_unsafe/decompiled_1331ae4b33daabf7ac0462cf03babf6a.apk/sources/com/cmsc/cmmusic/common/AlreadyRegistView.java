package com.cmsc.cmmusic.common;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import java.io.InputStream;

final class AlreadyRegistView extends RelativeLayout {
    protected Button btnSure = null;
    protected LinearLayout btnView = null;
    protected Bundle curExtraInfo = null;
    protected CMMusicActivity mCurActivity;
    protected Handler mHandler = null;
    protected OrderPolicy policyObj;
    protected LinearLayout rootView = null;

    public AlreadyRegistView(Context context, Bundle bundle) {
        super(context);
        this.mCurActivity = (CMMusicActivity) context;
        this.mHandler = new Handler();
        this.curExtraInfo = bundle;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.rootView = new LinearLayout(context);
        this.rootView.setOrientation(1);
        this.rootView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        this.rootView.setPadding(10, 5, 10, 20);
        ScrollView scrollView = new ScrollView(context);
        scrollView.setLayoutParams(new FrameLayout.LayoutParams(-1, getScreenHeightDip() - 100));
        scrollView.addView(this.rootView);
        addView(scrollView);
        initLogoView();
        initContentView(context);
        initBtnView(context);
    }

    private void initLogoView() {
        LinearLayout linearLayout = new LinearLayout(this.mCurActivity);
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        try {
            InputStream open = this.mCurActivity.getAssets().open("logo.png");
            BitmapDrawable bitmapDrawable = new BitmapDrawable(BitmapFactory.decodeStream(open));
            open.close();
            ImageView imageView = new ImageView(this.mCurActivity);
            imageView.setImageDrawable(bitmapDrawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setLayoutParams(new LinearLayout.LayoutParams(-1, 100));
            linearLayout.addView(imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.rootView.addView(linearLayout);
    }

    private void initContentView(Context context) {
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        linearLayout.setLayoutParams(layoutParams);
        TextView textView = new TextView(context);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 2.0f));
        textView.setTextAppearance(context, 16973892);
        textView.setText("尊敬的手机用户" + this.curExtraInfo.getString("mobile") + "您已经成功注册过，可以直接通过此手机号进行登陆。");
        textView.setGravity(7);
        linearLayout.addView(textView);
        this.rootView.addView(linearLayout);
    }

    private void initBtnView(Context context) {
        this.btnView = new LinearLayout(context);
        this.btnView.setOrientation(0);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        layoutParams.setMargins(dip2px(10.0f), dip2px(10.0f), dip2px(10.0f), 0);
        this.btnView.setLayoutParams(layoutParams);
        this.btnSure = new Button(context);
        this.btnSure.setText("确定");
        this.btnSure.setLayoutParams(new LinearLayout.LayoutParams(-2, -2, 1.0f));
        this.btnView.addView(this.btnSure);
        this.btnSure.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AlreadyRegistView.this.sureClicked();
            }
        });
        addView(this.btnView);
    }

    /* access modifiers changed from: protected */
    public void cancelClicked() {
        this.mCurActivity.closeActivity(null);
    }

    /* access modifiers changed from: protected */
    public void sureClicked() {
        this.mCurActivity.closeActivity(null);
    }

    private int dip2px(float f) {
        return (int) ((this.mCurActivity.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightPx() {
        int i = (int) ((((float) this.mCurActivity.getResources().getDisplayMetrics().heightPixels) * this.mCurActivity.getResources().getDisplayMetrics().density) + 0.5f);
        Log.d("getScreenHeightPx=", new StringBuilder().append(i).toString());
        return i;
    }

    /* access modifiers changed from: protected */
    public int getScreenHeightDip() {
        int i = this.mCurActivity.getResources().getDisplayMetrics().heightPixels;
        Log.d("getScreenHeightDip=", new StringBuilder().append(i).toString());
        return i;
    }
}
