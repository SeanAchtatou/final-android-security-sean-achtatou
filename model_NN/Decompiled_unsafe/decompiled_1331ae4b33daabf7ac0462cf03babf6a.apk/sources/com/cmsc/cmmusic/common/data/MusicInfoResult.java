package com.cmsc.cmmusic.common.data;

public class MusicInfoResult extends Result {
    private MusicInfo musicInfo;

    public MusicInfo getMusicInfo() {
        return this.musicInfo;
    }

    public void setMusicInfo(MusicInfo musicInfo2) {
        this.musicInfo = musicInfo2;
    }

    public String toString() {
        return "MusicInfoResult [musicInfo=" + this.musicInfo + ", getResCode()=" + getResCode() + ", getResMsg()=" + getResMsg() + "]";
    }
}
