package com.cmsc.cmmusic.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.cmsc.cmmusic.common.data.GetUserInfoRsp;
import com.cmsc.cmmusic.common.data.MVMonthPolicy;
import com.cmsc.cmmusic.common.data.MusicInfo;
import com.cmsc.cmmusic.common.data.MusicInfoResult;
import com.cmsc.cmmusic.common.data.OrderPolicy;
import com.cmsc.cmmusic.common.data.OrderResult;
import com.cmsc.cmmusic.common.data.RegistRsp;
import com.cmsc.cmmusic.common.data.Result;
import com.cmsc.cmmusic.common.data.ServiceExInfo;
import com.cmsc.cmmusic.common.data.UserInfo;
import com.cmsc.cmmusic.init.NetMode;
import java.io.IOException;
import java.util.Stack;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.xmlpull.v1.XmlPullParserException;

public final class CMMusicActivity extends Activity {
    static final int DB = 102;
    static final int HD = 101;
    private static final String LOG_TAG = "CMMusicActivity";
    static final int PAYMODE_DEFAULT = 0;
    static final int PAYMODE_SMS = 1;
    static final int SD = 100;
    static final int TYPE_CP_FULL_SONG = 14;
    static final int TYPE_CP_VIBRATE_RING = 15;
    static final int TYPE_CRBT_CHECK_OPEN = 9;
    static final int TYPE_DIGITAL_ALBUM_AUTO_OPEN_MUMBER = 27;
    static final int TYPE_DIGITAL_ALBUM_AUTO_OPEN_RINGBACK = 26;
    static final int TYPE_EXCLUSIVE = 19;
    static final int TYPE_FULL_SONG = 1;
    static final int TYPE_GIVE_DIGITAL_ALBUM = 25;
    static final int TYPE_GIVE_RINGBACK = 5;
    static final int TYPE_KEY_ORDER_OWN_RING_MONTH = 20;
    static final int TYPE_LOGIN = 12;
    static final int TYPE_MV_DOWNLOAD = 18;
    static final int TYPE_MV_MONTH = 17;
    static final int TYPE_OPEN_BJHY = 28;
    static final int TYPE_OPEN_CPMONTH = 10;
    static final int TYPE_OPEN_MEMBER = 6;
    static final int TYPE_OPEN_RINGBACK = 4;
    static final int TYPE_OPEN_RINGBACK_MONTH = 29;
    static final int TYPE_OPEN_SONGMONTH = 7;
    static final int TYPE_ORDER_DIGITAL_ALBUM = 24;
    static final int TYPE_OWN_RINGBACK = 16;
    static final int TYPE_OWN_RING_MONTH = 21;
    static final int TYPE_PHONE_NO_LOGIN = 8;
    static final int TYPE_REGIST = 11;
    static final int TYPE_RINGBACK_AUTO_OPEN_MUMBER = 22;
    static final int TYPE_RINGBACK_AUTO_OPEN_RINGBACK = 23;
    static final int TYPE_RING_BACK = 2;
    static final int TYPE_TRANSFER = 13;
    static final int TYPE_VIBRATE_RING = 3;
    public static CMMusicActivity mCurActivity;
    /* access modifiers changed from: private */
    public static volatile CMMusicCallback mCurCallback = null;
    /* access modifiers changed from: private */
    public Result isOwnRingOrderMonthUserResult;
    private volatile Stack<LinearLayout> loginViewStack;
    /* access modifiers changed from: private */
    public ProgressDialog mProgress = null;
    /* access modifiers changed from: private */
    public Handler mUIHandler = new Handler();
    private volatile Stack<ViewGroup> ownRingViewStack;
    /* access modifiers changed from: private */
    public Dialog resultDialog;
    private volatile Stack<BaseView> viewStack;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setTitle("中国移动无线音乐平台");
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        if (defaultDisplay.getWidth() <= defaultDisplay.getHeight()) {
            setRequestedOrientation(1);
            getWindow().setSoftInputMode(32);
            start();
            mCurActivity = this;
        } else if (getIntent().getIntExtra("ReqType", -1) == 18) {
            final MyVideoView myVideoView = new MyVideoView(this);
            myVideoView.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
            setContentView(myVideoView);
            myVideoView.setMediaController(new MediaController(this));
            myVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    CMMusicActivity.this.hideProgressBar();
                    myVideoView.start();
                }
            });
            myVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    CMMusicActivity.this.finish();
                    CMMusicActivity.this.showToast("播放异常啦！");
                    CMMusicActivity.this.hideProgressBar();
                    return true;
                }
            });
            myVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                }
            });
            String playUri = PreferenceUtil.getPlayUri(this);
            if (playUri == null || playUri.trim().length() <= 0) {
                showToast("播放地址异常！");
                finish();
                return;
            }
            myVideoView.setVideoPath(playUri);
            showProgressBar("正在加载视频...");
        }
    }

    /* access modifiers changed from: package-private */
    public void start() {
        this.viewStack = new Stack<>();
        this.loginViewStack = new Stack<>();
        this.ownRingViewStack = new Stack<>();
        int intExtra = getIntent().getIntExtra("payMode", 0);
        final Bundle bundleExtra = getIntent().getBundleExtra("ExtraInfo");
        final String string = bundleExtra.getString("MusicId");
        final boolean z = bundleExtra.getBoolean("PrioritySMS");
        switch (getIntent().getIntExtra("ReqType", -1)) {
            case 1:
                updateView(intExtra, OrderPolicy.OrderPolicyType.fullSong, string, z, new FullSongView(this, bundleExtra), false);
                return;
            case 2:
                updateView(intExtra, OrderPolicy.OrderPolicyType.ringback, string, z, new RingbackOrderView(this, bundleExtra), false);
                return;
            case 3:
                updateView(intExtra, OrderPolicy.OrderPolicyType.vibrateRing, string, z, new VibrateRingOrderView(this, bundleExtra), false);
                return;
            case 4:
                showProgressBar("数据加载中...");
                final String str = string;
                final boolean z2 = z;
                final Bundle bundle = bundleExtra;
                final int i = intExtra;
                new Thread() {
                    public void run() {
                        try {
                            OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, str, OrderPolicy.OrderPolicyType.checkRingbackOpen, z2);
                            if ("000000".equals(orderPolicy.getResCode()) || "100100".equals(orderPolicy.getResCode())) {
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundle;
                                final int i = i;
                                final String str = str;
                                final boolean z = z2;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.updateView(i, OrderPolicy.OrderPolicyType.openRingback, str, z, new RingbackOpenView(CMMusicActivity.this, bundle), false);
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                                return;
                            }
                            OrderResult orderResult = new OrderResult();
                            orderResult.setResCode(orderPolicy.getResCode());
                            orderResult.setResMsg(orderPolicy.getResMsg());
                            CMMusicActivity.this.closeActivity(orderResult);
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 5:
                updateView(intExtra, OrderPolicy.OrderPolicyType.ringback, string, z, new GiveRingbackView(this, bundleExtra), false);
                return;
            case 6:
                updateView(intExtra, OrderPolicy.OrderPolicyType.openMember, null, z, new OpenMemberView(this, bundleExtra), true);
                return;
            case 7:
                updateView(intExtra, OrderPolicy.OrderPolicyType.openSongMonth, null, z, new OpenSongMonthView(this, bundleExtra), true);
                return;
            case 8:
                showPhoneNoLoginAuthView();
                return;
            case 9:
                showCrbtOpenCheckView();
                return;
            case 10:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        try {
                            String string = bundleExtra.getString("serviceId");
                            ServiceExInfo serviceEx = EnablerInterface.getServiceEx(CMMusicActivity.this, string);
                            if (serviceEx == null) {
                                CMMusicActivity.this.finish();
                            } else if (serviceEx.getResCode() != null) {
                                CMMusicActivity.this.hideProgressBar();
                                CMMusicActivity.this.showToast(serviceEx.getResMsg());
                                CMMusicActivity.this.finish();
                                CMMusicActivity.this.hideProgressBar();
                            } else {
                                OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, string, OrderPolicy.OrderPolicyType.openCPMonth, z);
                                if (orderPolicy == null) {
                                    CMMusicActivity.this.finish();
                                    CMMusicActivity.this.hideProgressBar();
                                } else if (!"000000".equals(orderPolicy.getResCode())) {
                                    CMMusicActivity.this.hideProgressBar();
                                    CMMusicActivity.this.showToast(orderPolicy.getResMsg());
                                    CMMusicActivity.this.finish();
                                    CMMusicActivity.this.hideProgressBar();
                                } else {
                                    serviceEx.setSalePrice(EnablerInterface.getPrice(orderPolicy, serviceEx.getPrice()));
                                    serviceEx.setBizCode(EnablerInterface.getBizCode(orderPolicy));
                                    Log.d("serviceExInfo", serviceEx.toString());
                                    bundleExtra.putSerializable("serviceExInfo", serviceEx);
                                    Handler access$0 = CMMusicActivity.this.mUIHandler;
                                    final Bundle bundle = bundleExtra;
                                    access$0.post(new Runnable() {
                                        public void run() {
                                            CMMusicActivity.this.setContentView(new OpenCPMonthView(CMMusicActivity.this, bundle));
                                        }
                                    });
                                    CMMusicActivity.this.hideProgressBar();
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 11:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        try {
                            final RegistRsp registResult = EnablerInterface.getRegistResult(HttpPostCore.httpConnection(CMMusicActivity.this, "http://218.200.227.123:95/sdkServer/1.0/pay/member/check", ""));
                            if ("000000".equals(registResult.getResCode())) {
                                String trim = registResult.getIsExistent().trim();
                                if ("0".equals(trim)) {
                                    CMMusicActivity.this.mUIHandler.post(new Runnable() {
                                        public void run() {
                                            CMMusicActivity.this.showRegistView();
                                        }
                                    });
                                    return;
                                } else if ("1".equals(trim)) {
                                    CMMusicActivity.this.mUIHandler.post(new Runnable() {
                                        public void run() {
                                            CMMusicActivity.this.showAleradayRegistView(registResult);
                                        }
                                    });
                                    CMMusicActivity.this.hideProgressBar();
                                    return;
                                }
                            } else {
                                OrderResult orderResult = new OrderResult();
                                orderResult.setResCode(registResult.getResCode());
                                orderResult.setResMsg(registResult.getResMsg());
                                CMMusicActivity.this.closeActivity(orderResult);
                            }
                            CMMusicActivity.this.hideProgressBar();
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.closeActivity(null);
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 12:
                showLoginView();
                return;
            case 13:
                showTransferView(bundleExtra);
                return;
            case 14:
                updateView(intExtra, OrderPolicy.OrderPolicyType.cpFullSong, string, z, new CPFullSongView(this, bundleExtra), false);
                return;
            case 15:
                updateView(intExtra, OrderPolicy.OrderPolicyType.cpVibrateRing, string, z, new CPVibrateRingOrderView(this, bundleExtra), false);
                return;
            case 16:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        try {
                            final OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, string, OrderPolicy.OrderPolicyType.checkRingbackOpen, z);
                            if (!"000000".equals(orderPolicy.getResCode()) && !"100100".equals(orderPolicy.getResCode())) {
                                OrderResult orderResult = new OrderResult();
                                orderResult.setResCode(orderPolicy.getResCode());
                                orderResult.setResMsg(orderPolicy.getResMsg());
                                CMMusicActivity.this.closeActivity(orderResult);
                            } else if (OrderPolicy.OrderType.verifyCode == orderPolicy.getOrderType()) {
                                CMMusicActivity.this.mUIHandler.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.showPhoneNoLoginAuthView();
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                            } else {
                                bundleExtra.putString("phoneNum", orderPolicy.getMobile());
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.setContentView(new OrderOwnRingView(CMMusicActivity.this, bundle, 0, orderPolicy));
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 17:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        UserInfo userInfo;
                        try {
                            final MVMonthPolicy mvMonthPolicy = EnablerInterface.getMvMonthPolicy(CMMusicActivity.this, bundleExtra.getStringArray("serviceIds"), z);
                            if (!"000000".equals(mvMonthPolicy.getResCode()) && !"300002".equals(mvMonthPolicy.getResCode())) {
                                OrderResult orderResult = new OrderResult();
                                orderResult.setResCode(mvMonthPolicy.getResCode());
                                orderResult.setResMsg(mvMonthPolicy.getResMsg());
                                CMMusicActivity.this.closeActivity(orderResult);
                            } else if (MVMonthPolicy.OrderType.verifyCode == mvMonthPolicy.getOrderType()) {
                                CMMusicActivity.this.mUIHandler.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.showPhoneNoLoginAuthView();
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                            } else if (MVMonthPolicy.OrderType.sms == mvMonthPolicy.getOrderType()) {
                                CMMusicActivity.this.closeActivity(null);
                                CMMusicActivity.this.hideProgressBar();
                            } else if (mvMonthPolicy.getmVOrderInfos() == null) {
                                OrderResult orderResult2 = new OrderResult();
                                orderResult2.setResCode(mvMonthPolicy.getResCode());
                                orderResult2.setResMsg("未找到指定的包月类型！");
                                CMMusicActivity.this.closeActivity(orderResult2);
                                CMMusicActivity.this.hideProgressBar();
                            } else if (mvMonthPolicy.isAllOpen()) {
                                OrderResult orderResult3 = new OrderResult();
                                orderResult3.setResCode(mvMonthPolicy.getResCode());
                                orderResult3.setResMsg("指定的包月类型已开通！");
                                CMMusicActivity.this.closeActivity(orderResult3);
                                CMMusicActivity.this.hideProgressBar();
                            } else {
                                try {
                                    GetUserInfoRsp userInfo2 = EnablerInterface.getUserInfo(CMMusicActivity.this);
                                    if (userInfo2 != null) {
                                        if (GetUserInfoRsp.NON_MEM_ERROR_CODE.equals(userInfo2.getResCode())) {
                                            userInfo = new UserInfo();
                                            userInfo.setMemLevel("0");
                                        } else {
                                            userInfo = userInfo2.getUserInfo();
                                        }
                                        mvMonthPolicy.setUserInfo(userInfo);
                                    }
                                } catch (Exception e) {
                                    e.fillInStackTrace();
                                }
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.setContentView(new OpenMvMonthView(CMMusicActivity.this, bundle, mvMonthPolicy));
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            CMMusicActivity.this.closeActivity(null);
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 18:
                updateView(intExtra, OrderPolicy.OrderPolicyType.mvDownLoad, bundleExtra.getString("mvId"), z, new MvDownLoadView(this, bundleExtra), true);
                return;
            case 19:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        try {
                            ServiceExInfo serviceEx = EnablerInterface.getServiceEx(CMMusicActivity.this, bundleExtra.getString("serviceId"));
                            if (serviceEx == null) {
                                CMMusicActivity.this.finish();
                            } else if (serviceEx.getResCode() != null) {
                                CMMusicActivity.this.hideProgressBar();
                                CMMusicActivity.this.showToast(serviceEx.getResMsg());
                                CMMusicActivity.this.finish();
                            } else {
                                Log.d("serviceExInfo", serviceEx.toString());
                                bundleExtra.putSerializable("serviceExInfo", serviceEx);
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.setContentView(new ExclusiveOrderView(CMMusicActivity.this, bundle));
                                    }
                                });
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 20:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        UserInfo userInfo;
                        try {
                            CMMusicActivity.this.isOwnRingOrderMonthUserResult = EnablerInterface.getIsOwnRingOrderMonthUserRsp(CMMusicActivity.mCurActivity);
                            if (CMMusicActivity.this.isOwnRingOrderMonthUserResult != null) {
                                "000000".equals(CMMusicActivity.this.isOwnRingOrderMonthUserResult.getResCode());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (XmlPullParserException e2) {
                            e2.printStackTrace();
                        }
                        try {
                            final OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, string, OrderPolicy.OrderPolicyType.checkRingbackOpen, z);
                            if ("000000".equals(orderPolicy.getResCode()) || "100100".equals(orderPolicy.getResCode())) {
                                try {
                                    GetUserInfoRsp userInfo2 = EnablerInterface.getUserInfo(CMMusicActivity.this);
                                    if (userInfo2 != null) {
                                        if (GetUserInfoRsp.NON_MEM_ERROR_CODE.equals(userInfo2.getResCode())) {
                                            userInfo = new UserInfo();
                                            userInfo.setMemLevel("0");
                                        } else {
                                            userInfo = userInfo2.getUserInfo();
                                        }
                                        orderPolicy.setUserInfo(userInfo);
                                    }
                                } catch (Exception e3) {
                                    e3.fillInStackTrace();
                                }
                                bundleExtra.putString("phoneNum", orderPolicy.getMobile());
                                bundleExtra.putString("isOwnRingOrderMonthUser", CMMusicActivity.this.isOwnRingOrderMonthUserResult.getResMsg());
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.setContentView(new KeyOrderOwnRingMonthView(CMMusicActivity.this, bundle, 1, orderPolicy));
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                                return;
                            }
                            OrderResult orderResult = new OrderResult();
                            orderResult.setResCode(orderPolicy.getResCode());
                            orderResult.setResMsg(orderPolicy.getResMsg());
                            CMMusicActivity.this.closeActivity(orderResult);
                        } catch (Exception e4) {
                            e4.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 21:
                showProgressBar("数据加载中...");
                new Thread() {
                    public void run() {
                        UserInfo userInfo;
                        try {
                            CMMusicActivity.this.isOwnRingOrderMonthUserResult = EnablerInterface.getIsOwnRingOrderMonthUserRsp(CMMusicActivity.mCurActivity);
                            if (CMMusicActivity.this.isOwnRingOrderMonthUserResult != null) {
                                "000000".equals(CMMusicActivity.this.isOwnRingOrderMonthUserResult.getResCode());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (XmlPullParserException e2) {
                            e2.printStackTrace();
                        }
                        try {
                            final OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, string, OrderPolicy.OrderPolicyType.checkRingbackOpen, z);
                            if ("000000".equals(orderPolicy.getResCode()) || "100100".equals(orderPolicy.getResCode())) {
                                try {
                                    GetUserInfoRsp userInfo2 = EnablerInterface.getUserInfo(CMMusicActivity.this);
                                    if (userInfo2 != null) {
                                        if (GetUserInfoRsp.NON_MEM_ERROR_CODE.equals(userInfo2.getResCode())) {
                                            userInfo = new UserInfo();
                                            userInfo.setMemLevel("0");
                                        } else {
                                            userInfo = userInfo2.getUserInfo();
                                        }
                                        orderPolicy.setUserInfo(userInfo);
                                    }
                                } catch (Exception e3) {
                                    e3.fillInStackTrace();
                                }
                                bundleExtra.putString("phoneNum", orderPolicy.getMobile());
                                bundleExtra.putString("isOwnRingOrderMonthUser", CMMusicActivity.this.isOwnRingOrderMonthUserResult.getResMsg());
                                CMMusicActivity.this.setOwnRingbackOpenPolicy(orderPolicy);
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.setContentView(new KeyOrderOwnRingMonthView(CMMusicActivity.this, bundle, 2, orderPolicy));
                                    }
                                });
                                CMMusicActivity.this.hideProgressBar();
                                return;
                            }
                            OrderResult orderResult = new OrderResult();
                            orderResult.setResCode(orderPolicy.getResCode());
                            orderResult.setResMsg(orderPolicy.getResMsg());
                            CMMusicActivity.this.closeActivity(orderResult);
                        } catch (Exception e4) {
                            e4.printStackTrace();
                            CMMusicActivity.this.finish();
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 22:
                updateView(intExtra, OrderPolicy.OrderPolicyType.ringback, string, z, new AutoOpenMemberOrderRingbackView(this, bundleExtra), false);
                return;
            case 23:
                updateView(intExtra, OrderPolicy.OrderPolicyType.ringback, string, z, new AutoOpenRingbackOrderRingbackView(this, bundleExtra), false);
                return;
            case 24:
                updateView(intExtra, OrderPolicy.OrderPolicyType.digitalAlbum, string, z, new DigitalAlbumOrderView(this, bundleExtra), false);
                return;
            case 25:
                updateView(intExtra, OrderPolicy.OrderPolicyType.digitalAlbum, string, z, new DigitalAlbumGiveView(this, bundleExtra), false);
                return;
            case 26:
                updateView(intExtra, OrderPolicy.OrderPolicyType.digitalAlbum, string, z, new AutoOpenRingbackOrderDigitalAlbumView(this, bundleExtra), false);
                return;
            case 27:
                updateView(intExtra, OrderPolicy.OrderPolicyType.digitalAlbum, string, z, new AutoOpenMemberOrderDigitalAlbumView(this, bundleExtra), false);
                return;
            case 28:
                updateView(intExtra, OrderPolicy.OrderPolicyType.openBjhy, null, z, new OpenBjhyView(this, bundleExtra), false);
                return;
            case 29:
                showProgressBar("数据加载中...");
                final String str2 = string;
                final int i2 = intExtra;
                final boolean z3 = z;
                new Thread() {
                    public void run() {
                        try {
                            Result queryCrbtMonth = EnablerInterface.queryCrbtMonth(CMMusicActivity.this, str2);
                            if (queryCrbtMonth == null) {
                                CMMusicActivity.this.closeActivity(null);
                            } else if ("300002".equals(queryCrbtMonth.getResCode())) {
                                Handler access$0 = CMMusicActivity.this.mUIHandler;
                                final Bundle bundle = bundleExtra;
                                final int i = i2;
                                final String str = str2;
                                final boolean z = z3;
                                access$0.post(new Runnable() {
                                    public void run() {
                                        CMMusicActivity.this.updateView(i, OrderPolicy.OrderPolicyType.openRingbackMonth, str, z, new OpenCrbtMonthView(CMMusicActivity.this, bundle), false);
                                    }
                                });
                            } else {
                                OrderResult orderResult = new OrderResult();
                                orderResult.setResCode(queryCrbtMonth.getResCode());
                                orderResult.setResMsg(queryCrbtMonth.getResMsg());
                                CMMusicActivity.this.closeActivity(orderResult);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CMMusicActivity.this.closeActivity(null);
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            default:
                return;
        }
    }

    public void setContentView(View view) {
        super.setContentView(view);
        if (getIntent().getIntExtra("ReqType", -1) == 11) {
            return;
        }
        if (getIntent().getIntExtra("ReqType", -1) == 12) {
            this.loginViewStack.push((LinearLayout) view);
        } else if (getIntent().getIntExtra("ReqType", -1) == 13) {
        } else {
            if (getIntent().getIntExtra("ReqType", -1) == 16) {
                this.ownRingViewStack.push((ViewGroup) view);
            } else if (getIntent().getIntExtra("ReqType", -1) != 18 && getIntent().getIntExtra("ReqType", -1) != 19 && getIntent().getIntExtra("ReqType", -1) != 10 && getIntent().getIntExtra("ReqType", -1) != 20 && getIntent().getIntExtra("ReqType", -1) != 21) {
                this.viewStack.push((BaseView) view);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public BaseView popView(Result result) {
        this.viewStack.pop();
        if (this.viewStack.size() == 0) {
            closeActivity(result);
            return null;
        }
        final BaseView baseView = this.viewStack.get(0);
        this.mUIHandler.post(new Runnable() {
            public void run() {
                CMMusicActivity.super.setContentView(baseView);
            }
        });
        return baseView;
    }

    /* access modifiers changed from: package-private */
    public ViewGroup popOwnRingView(Result result) {
        this.ownRingViewStack.pop();
        if (this.ownRingViewStack.size() == 0) {
            closeActivity(result);
            return null;
        }
        final ViewGroup viewGroup = this.ownRingViewStack.get(0);
        this.mUIHandler.post(new Runnable() {
            public void run() {
                CMMusicActivity.super.setContentView(viewGroup);
            }
        });
        return viewGroup;
    }

    /* access modifiers changed from: package-private */
    public void updateView(int i, OrderPolicy.OrderPolicyType orderPolicyType, String str, boolean z, OrderView orderView, boolean z2) {
        setContentView(orderView);
        switch (i) {
            case 0:
                showProgressBar("数据加载中...");
                final String str2 = str;
                final OrderPolicy.OrderPolicyType orderPolicyType2 = orderPolicyType;
                final boolean z3 = z;
                final OrderView orderView2 = orderView;
                final boolean z4 = z2;
                new Thread() {
                    public void run() {
                        UserInfo userInfo;
                        try {
                            OrderPolicy orderPolicy = EnablerInterface.getOrderPolicy(CMMusicActivity.this, str2, orderPolicyType2, z3);
                            if (orderPolicy == null) {
                                CMMusicActivity.this.closeActivity(null);
                            }
                            int intExtra = CMMusicActivity.this.getIntent().getIntExtra("ReqType", -1);
                            if ("000000".equals(orderPolicy.getResCode())) {
                                if (intExtra == 14 || intExtra == 15) {
                                    MusicInfoResult musicInfoByMusicId = MusicQueryInterface.getMusicInfoByMusicId(CMMusicActivity.this, str2);
                                    if ("000000".equals(musicInfoByMusicId.getResCode())) {
                                        MusicInfo musicInfo = musicInfoByMusicId.getMusicInfo();
                                        if (musicInfo != null) {
                                            orderPolicy.setMusicInfo(musicInfo);
                                        }
                                    } else {
                                        OrderResult orderResult = new OrderResult();
                                        orderResult.setResCode(musicInfoByMusicId.getResCode());
                                        orderResult.setResMsg(musicInfoByMusicId.getResMsg());
                                        CMMusicActivity.this.closeActivity(orderResult);
                                        CMMusicActivity.this.hideProgressBar();
                                        return;
                                    }
                                }
                                if (OrderPolicy.OrderType.verifyCode == orderPolicy.getOrderType()) {
                                    CMMusicActivity.this.mUIHandler.post(new Runnable() {
                                        public void run() {
                                            CMMusicActivity.this.showPhoneNoLoginAuthView();
                                        }
                                    });
                                    CMMusicActivity.this.hideProgressBar();
                                    return;
                                }
                                if (z4) {
                                    try {
                                        GetUserInfoRsp userInfo2 = EnablerInterface.getUserInfo(CMMusicActivity.this);
                                        if (userInfo2 != null) {
                                            if (GetUserInfoRsp.NON_MEM_ERROR_CODE.equals(userInfo2.getResCode())) {
                                                userInfo = new UserInfo();
                                                userInfo.setMemLevel("0");
                                            } else {
                                                userInfo = userInfo2.getUserInfo();
                                            }
                                            orderPolicy.setUserInfo(userInfo);
                                        }
                                    } catch (Exception e) {
                                        e.fillInStackTrace();
                                    }
                                }
                                orderView2.updateView(orderPolicy);
                                CMMusicActivity.this.hideProgressBar();
                            } else if (intExtra != 4 || !"100100".equals(orderPolicy.getResCode())) {
                                if (intExtra == 10) {
                                    if ("300002".equals(orderPolicy.getResCode())) {
                                        orderView2.updateView(orderPolicy);
                                        CMMusicActivity.this.hideProgressBar();
                                        return;
                                    }
                                }
                                OrderResult orderResult2 = new OrderResult();
                                orderResult2.setResCode(orderPolicy.getResCode());
                                orderResult2.setResMsg(orderPolicy.getResMsg());
                                CMMusicActivity.this.closeActivity(orderResult2);
                                CMMusicActivity.this.hideProgressBar();
                            } else {
                                orderView2.updateView(orderPolicy);
                            }
                        } catch (Exception e2) {
                            e2.printStackTrace();
                            OrderPolicy orderPolicy2 = new OrderPolicy();
                            orderPolicy2.setOrderType(OrderPolicy.OrderType.sms);
                            orderView2.updateView(orderPolicy2);
                        } finally {
                            CMMusicActivity.this.hideProgressBar();
                        }
                    }
                }.start();
                return;
            case 1:
                OrderPolicy orderPolicy = new OrderPolicy();
                orderPolicy.setOrderType(OrderPolicy.OrderType.sms);
                orderView.updateView(orderPolicy);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void showPhoneNoLoginAuthView() {
        setContentView(new PhoneNoLoginAuthView(this, null));
    }

    private void showCrbtOpenCheckView() {
        setContentView(new CrbtOpenCheckView(this, null));
    }

    /* access modifiers changed from: private */
    public void showRegistView() {
        setContentView(new RegistView(this, null));
    }

    /* access modifiers changed from: private */
    public void showAleradayRegistView(RegistRsp registRsp) {
        Bundle bundle = new Bundle();
        bundle.putString("mobile", registRsp.getMobile());
        setContentView(new AlreadyRegistView(this, bundle));
    }

    private void showLoginView() {
        setContentView(new LoginView(this, null));
    }

    private void showTransferView(Bundle bundle) {
        setContentView(new TransferView(this, bundle));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (this.mProgress != null) {
            this.mProgress.dismiss();
        }
    }

    static void showActivitySMS(Context context, Bundle bundle, int i) {
        Intent intent = new Intent(context, CMMusicActivity.class);
        intent.putExtra("payMode", 1);
        intent.putExtra("ReqType", i);
        intent.putExtra("ExtraInfo", bundle);
        context.startActivity(intent);
    }

    static void showActivityDefault(Context context, Bundle bundle, int i, CMMusicCallback cMMusicCallback) {
        mCurCallback = cMMusicCallback;
        if (context instanceof Activity) {
            Intent intent = new Intent(context, CMMusicActivity.class);
            intent.putExtra("payMode", 0);
            intent.putExtra("ReqType", i);
            intent.putExtra("ExtraInfo", bundle);
            context.startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(context, CMMusicActivity.class);
        intent2.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent2.putExtra("payMode", 0);
        intent2.putExtra("ReqType", i);
        intent2.putExtra("ExtraInfo", bundle);
        context.startActivity(intent2);
    }

    /* access modifiers changed from: private */
    public void setOwnRingbackOpenPolicy(OrderPolicy orderPolicy) throws IOException, XmlPullParserException {
        OrderPolicy orderPolicy2 = EnablerInterface.getOrderPolicy(this, null, OrderPolicy.OrderPolicyType.openOwnRingback, false);
        if (orderPolicy2 == null) {
            closeActivity(null);
        } else if (!"000000".equals(orderPolicy2.getResCode()) || orderPolicy2.getBizInfos() == null) {
            OrderResult orderResult = new OrderResult();
            orderResult.setResCode(orderPolicy2.getResCode());
            orderResult.setResMsg(orderPolicy2.getResMsg());
            closeActivity(orderResult);
        } else {
            orderPolicy.setBizInfos(orderPolicy2.getBizInfos());
        }
    }

    /* access modifiers changed from: package-private */
    public void showToast(final String str) {
        Log.d(LOG_TAG, "showToast：" + str);
        this.mUIHandler.post(new Runnable() {
            public void run() {
                Toast.makeText(CMMusicActivity.this, str, 1).show();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void showDialog(final String str, final String str2) {
        Log.d(LOG_TAG, "title：" + str + "content：" + str2);
        this.mUIHandler.post(new Runnable() {
            public void run() {
                CMMusicActivity.this.resultDialog = new AlertDialog.Builder(CMMusicActivity.this).setTitle(str).setMessage(str2).setNegativeButton("确认", (DialogInterface.OnClickListener) null).show();
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void dismissDialog() {
        this.resultDialog.dismiss();
    }

    /* access modifiers changed from: package-private */
    public void showProgressBar(final String str) {
        Log.d(LOG_TAG, "showProgressBar invoked!");
        this.mUIHandler.post(new Runnable() {
            public void run() {
                if (CMMusicActivity.this.mProgress == null) {
                    CMMusicActivity.this.mProgress = new ProgressDialog(CMMusicActivity.this);
                    CMMusicActivity.this.mProgress.setMessage(str);
                    CMMusicActivity.this.mProgress.setIndeterminate(false);
                    CMMusicActivity.this.mProgress.setCancelable(false);
                    CMMusicActivity.this.mProgress.show();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void hideProgressBar() {
        Log.d(LOG_TAG, "hideProgressBar invoked!");
        this.mUIHandler.post(new Runnable() {
            public void run() {
                if (CMMusicActivity.this.mProgress != null) {
                    CMMusicActivity.this.mProgress.dismiss();
                    CMMusicActivity.this.mProgress = null;
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void closeActivity(final Result result) {
        this.mUIHandler.post(new Runnable() {
            public void run() {
                CMMusicActivity.this.hideProgressBar();
                CMMusicActivity.this.finish();
                if (CMMusicActivity.mCurCallback != null) {
                    CMMusicActivity.mCurCallback.operationResult(result);
                    CMMusicActivity.mCurCallback = null;
                }
            }
        });
    }

    public int getViewStackSize() {
        return this.viewStack.size();
    }

    public void onBackPressed() {
        if (getIntent().getIntExtra("ReqType", -1) == 12) {
            ((LoginView) this.loginViewStack.pop()).cancelClicked();
        }
        if (getIntent().getIntExtra("ReqType", -1) == 18) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        Log.i(LOG_TAG, "NetMode.isConnected = " + NetMode.isConnected(mCurActivity));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (!NetMode.isConnected(mCurActivity)) {
            new AlertDialog.Builder(this).setMessage("请连接网络").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    CMMusicActivity.mCurActivity.closeActivity(null);
                }
            }).show();
        }
    }
}
