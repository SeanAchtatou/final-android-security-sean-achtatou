package com.cmsc.cmmusic.init;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

public class GetAppInfo {
    static native boolean equals(Context context, String str, String str2);

    static native String getIMSI(Context context, int i) throws Throwable;

    static String getChannelCode(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            String string = applicationInfo.metaData.getString("miguopen_chcode");
            if (string == null || string.length() <= 0) {
                return string;
            }
            Log.d("SDK_LW_CMM", string);
            return string.substring(7, 14);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
            return "";
        }
    }

    static String getAppid(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getString("miguopen_appid").substring(6, 24);
            }
            return "";
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            String str = "";
            Log.e("SDK_LW_CMM", nameNotFoundException.getMessage());
            return str;
        }
    }

    public static String getMacAddress(Context context) {
        return ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
    }

    public static String getexCode(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            String string = applicationInfo.metaData.getString("miguopen_excode");
            if (string == null || string.length() <= 0) {
                return string;
            }
            Log.d("SDK_LW_CMM", string);
            return string.substring(7, 11);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
            return "";
        }
    }

    static String getSign(Context context) {
        byte[] bArr = null;
        try {
            bArr = getSignInfo(context).getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
        }
        return MD5.bytes2hex(MD5.md5(bArr));
    }

    static String getSignInfo(Context context) {
        String str = "";
        try {
            str = parseSignature(context.getPackageManager().getPackageInfo(getPackageName(context), 64).signatures[0].toByteArray());
            return str.toLowerCase();
        } catch (PackageManager.NameNotFoundException e) {
            PackageManager.NameNotFoundException nameNotFoundException = e;
            String str2 = str;
            Log.e("SDK_LW_CMM", nameNotFoundException.getMessage());
            return str2;
        }
    }

    static String parseSignature(byte[] bArr) {
        try {
            String lowerCase = Utils.subString(((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey().toString()).replace(MiPushClient.ACCEPT_TIME_SEPARATOR, "").toLowerCase();
            return lowerCase.substring(lowerCase.indexOf("modulus") + 8, lowerCase.indexOf("publicexponent"));
        } catch (CertificateException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
            return "";
        }
    }

    static String getPackageName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).packageName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
            return "";
        }
    }

    static String getSDKVersion() {
        return "I1.2";
    }

    static String getIMSI(Context context, String str, int i) {
        byte[] bArr = null;
        try {
            if (!isValidityImsi(str)) {
                return "";
            }
            bArr = str.getBytes("UTF-8");
            return MD5.bytes2hex(MD5.md5(bArr));
        } catch (UnsupportedEncodingException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
        }
    }

    static boolean isValidityImsi(String str) {
        return str != null && str.matches("^\\d{15}$") && !str.matches("^(\\d)\\1{14}$");
    }

    static boolean isCmCard(String str) {
        String substring = str.substring(3, 5);
        if ("00".equals(substring) || "02".equals(substring) || "07".equals(substring)) {
            return true;
        }
        return false;
    }

    static String getIMSI1(Context context) throws Throwable {
        String imsi = getIMSI(context, InitCmm1.SINGLE_CARD);
        return isValidityImsi(imsi) ? imsi : "";
    }

    public static String getIMSI(Context context) {
        String str = "";
        try {
            str = getIMSI(context, InitCmmInterface.simWhichConnected(context));
        } catch (Throwable th) {
        }
        if (str != null && !"".equals(str.trim())) {
            return str;
        }
        try {
            return getIMSI1(context);
        } catch (Throwable th2) {
            return str;
        }
    }

    static String getIMSIbyFile(Context context) {
        int simWhichConnected = InitCmmInterface.simWhichConnected(context);
        Log.e("SDK_LW_CMM", "simWhichConnected=" + simWhichConnected);
        String fromZIP = XZip.fromZIP(context, simWhichConnected);
        if (fromZIP != null && fromZIP.trim().length() > 0) {
            return fromZIP;
        }
        String fromZIP2 = XZip.fromZIP(context);
        return (fromZIP2 == null || fromZIP2.trim().length() <= 0) ? "" : fromZIP2;
    }

    static String getToken(Context context) {
        return PreferenceUtil.getToken(context);
    }

    public static String getIMEI(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    static boolean isTokenExist(Context context) {
        return PreferenceUtil.getToken(context).length() > 0;
    }

    static boolean needSmsInit(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null) {
                return applicationInfo.metaData.getBoolean("smsinit", true);
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("SDK_LW_CMM", e.getMessage());
            return true;
        }
    }
}
