package com.cmsc.cmmusic.init;

import android.content.Context;

public class GetAppInfoInterface {
    public static String getChannelCode(Context context) {
        return GetAppInfo.getChannelCode(context);
    }

    public static String getAppid(Context context) {
        return GetAppInfo.getAppid(context);
    }

    public static String getSign(Context context) {
        return GetAppInfo.getSign(context);
    }

    public static String getPackageName(Context context) {
        return GetAppInfo.getPackageName(context);
    }

    public static String getSDKVersion() {
        return GetAppInfo.getSDKVersion();
    }

    public static String getNetMode(Context context) {
        return NetMode.WIFIorMOBILE(context);
    }

    public static String getIMSI(Context context) {
        return GetAppInfo.getIMSIbyFile(context);
    }

    public static String getToken(Context context) {
        return GetAppInfo.getToken(context);
    }

    public static boolean isTokenExist(Context context) {
        return GetAppInfo.isTokenExist(context);
    }

    public static boolean isImsiExist(Context context) {
        return getIMSI(context).trim().length() != 0;
    }
}
