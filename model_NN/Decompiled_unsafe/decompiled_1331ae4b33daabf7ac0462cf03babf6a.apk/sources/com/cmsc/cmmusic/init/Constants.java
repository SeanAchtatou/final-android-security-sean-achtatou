package com.cmsc.cmmusic.init;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Constants {
    static final String CHECK_SMSAUTH_LOGIN_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/smsAuthLoginValidate";
    static final String CHECK_VALIDATE_CODE_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/smsLoginAuth";
    static final String CMNET = "CMNET";
    static final String CMWAP = "CMWAP";
    static final long CYCLE = 86400000;
    static final boolean DEBUG = false;
    static final String ENABLER_URL_DOMAIN = "http://218.200.227.123:95/sdkServer/1.0";
    static final String ENCODE = "UTF-8";
    static final String FN = "scmsc.si";
    static final String FN0 = "scmsc0.si";
    static final String FN1 = "scmsc1.si";
    static final String GET_VALIDATE_CODE_URL = "http://218.200.227.123:95/sdkServer/1.0/crbt/getValidateCode";
    static final String INITCOUNT = "initCount";
    static final String NIISNULL = "NIISNUll";
    static final String NOWM = "NOWM";
    static final String OTHER = "OTHER";
    static final String ROOT = "/data/data/";
    static final String SDK_VERSION = "I1.2";
    static final String TAG = "SDK_LW_CMM";
    static final String URL_SMSCHECK = "http://218.200.227.123:95/sdkServer/checksmsinitreturn";
    static final String WIFI = "WIFI";
    static Map<String, Integer> countMap = Collections.synchronizedMap(new HashMap());
    public static SmsStage smsStage = SmsStage.Original;

    public enum SmsStage {
        Original,
        Send
    }

    static {
        countMap.put(INITCOUNT, 0);
    }
}
