package com.cmsc.cmmusic.init;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.cmsc.cmmusic.init.Constants;

public class PhoneUtil {
    public static final String DELIVERED_SMS_ACTION = "DELIVERED_SMS_ACTION";
    public static final String SENT_SMS_ACTION = "SENT_SMS_ACTION";
    private static PhoneUtil instrance;
    /* access modifiers changed from: private */
    public Context context;
    private BroadcastReceiver deliveredSMSReceiver;
    private Thread mThread;
    private BroadcastReceiver sentSMSReceiver;
    private TelephonyManager telephonyManager;

    public interface SmsSendBackCall {
        void received();

        void sendFail();

        void sendSuccess();
    }

    private PhoneUtil(Context context2) {
        this.context = context2.getApplicationContext();
        this.telephonyManager = (TelephonyManager) context2.getSystemService("phone");
        Context context3 = this.context;
        AnonymousClass1 r1 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.i("SDK_LW_CMM", "sentSMSReceiver onReceive");
                Constants.smsStage = Constants.SmsStage.Original;
                switch (getResultCode()) {
                }
            }
        };
        this.sentSMSReceiver = r1;
        context3.registerReceiver(r1, new IntentFilter(SENT_SMS_ACTION));
        Context context4 = this.context;
        AnonymousClass2 r12 = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
            }
        };
        this.deliveredSMSReceiver = r12;
        context4.registerReceiver(r12, new IntentFilter(DELIVERED_SMS_ACTION));
    }

    public static PhoneUtil getInstrance(Context context2) {
        if (instrance == null) {
            instrance = new PhoneUtil(context2);
        }
        return instrance;
    }

    public void call(String str) {
        Uri parse = Uri.parse("tel:" + str);
        Intent intent = new Intent();
        intent.setAction("android.intent.action.CALL");
        intent.setData(parse);
        this.context.startActivity(intent);
    }

    public void unregisterSMSReceiver() {
        if (this.sentSMSReceiver != null) {
            this.context.unregisterReceiver(this.sentSMSReceiver);
        }
        if (this.deliveredSMSReceiver != null) {
            this.context.unregisterReceiver(this.deliveredSMSReceiver);
        }
    }

    public void sendSMS(final String str, final String str2, final int i) {
        this.mThread = new Thread() {
            public void run() {
                super.run();
                int i = 0;
                while (Constants.smsStage != Constants.SmsStage.Original && i < 80) {
                    Log.d(getClass().getSimpleName(), "sms not alerday .." + i);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
                PendingIntent.getBroadcast(PhoneUtil.this.context, 0, new Intent(PhoneUtil.SENT_SMS_ACTION), 0);
                PendingIntent.getBroadcast(PhoneUtil.this.context, 0, new Intent(PhoneUtil.DELIVERED_SMS_ACTION), 0);
                try {
                    DualSimUtils.sendTextMessage(str, null, str2, null, null, i);
                } catch (NoSuchMethodException e2) {
                    e2.printStackTrace();
                }
                Constants.smsStage = Constants.SmsStage.Send;
            }
        };
        this.mThread.start();
    }

    public boolean isAbsentSim() {
        return 1 == this.telephonyManager.getSimState();
    }
}
