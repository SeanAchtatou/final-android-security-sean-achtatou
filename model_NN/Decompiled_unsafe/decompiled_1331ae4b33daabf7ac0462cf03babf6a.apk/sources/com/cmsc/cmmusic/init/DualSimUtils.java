package com.cmsc.cmmusic.init;

import android.app.PendingIntent;
import android.content.Context;
import android.net.ConnectivityManager;
import android.provider.Settings;

public class DualSimUtils {
    public static int getDefaultSim(Context context) throws NoSuchMethodException {
        try {
            return ((Integer) Class.forName("android.net.NetworkInfo").getMethod("getSimId", null).invoke(((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0), null)).intValue();
        } catch (Error | Exception e) {
            try {
                Class<?> cls = Class.forName("android.telephony.MSimTelephonyManager");
                return ((Integer) cls.getMethod("getPreferredDataSubscription", null).invoke(cls.getMethod("getDefault", null).invoke(null, null), null)).intValue();
            } catch (Error | Exception e2) {
                try {
                    Class<?> cls2 = Class.forName("android.telephony.MSimTelephonyManager");
                    return ((Integer) cls2.getMethod("getDefaultSubscription", null).invoke(cls2.getMethod("getDefault", null).invoke(null, null), null)).intValue();
                } catch (Error | Exception e3) {
                    try {
                        Class<?> cls3 = Class.forName("android.telephony.TelephonyManager");
                        Object invoke = cls3.getMethod("getDefault", new Class[0]).invoke(null, null);
                        return ((Integer) cls3.getMethod("getDefaultSim", Context.class, Integer.TYPE).invoke(invoke, context, 1)).intValue();
                    } catch (Error | Exception e4) {
                        try {
                            Class<?> cls4 = Class.forName("android.telephony.TelephonyManager");
                            return ((Integer) cls4.getMethod("getSmsDefaultSim", null).invoke(cls4.getMethod("getDefault", new Class[0]).invoke(null, null), null)).intValue();
                        } catch (Error | Exception e5) {
                            try {
                                int i = Settings.System.getInt(context.getContentResolver(), "gprs_connection_setting", -4);
                                if (i < 0 || i > 2) {
                                    return 0;
                                }
                                return i;
                            } catch (Error | Exception e6) {
                                throw new NoSuchMethodException();
                            }
                        }
                    }
                }
            }
        }
    }

    static void sendTextMessage(String str, String str2, int i) throws NoSuchMethodException {
        sendTextMessage(str, null, str2, null, null, i);
    }

    static void sendTextMessage(String str, String str2, String str3, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i) throws NoSuchMethodException {
        try {
            Class.forName("android.telephony.gemini.GeminiSmsManager").getMethod("sendTextMessageGemini", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(null, str, str2, str3, Integer.valueOf(i), pendingIntent, pendingIntent2);
        } catch (Error | Exception e) {
            try {
                Class<?> cls = Class.forName("android.telephony.MSimSmsManager");
                Object invoke = cls.getMethod("getDefault", null).invoke(null, null);
                cls.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(invoke, str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
            } catch (Error | Exception e2) {
                try {
                    Class<?> cls2 = Class.forName("android.telephony.MSimSmsManager");
                    cls2.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(cls2.newInstance(), str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
                } catch (Error | Exception e3) {
                    try {
                        Class<?> cls3 = Class.forName("android.telephony.SmsManager");
                        Object invoke2 = cls3.getDeclaredMethod("getDefault", Integer.TYPE).invoke(null, Integer.valueOf(i));
                        cls3.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke2, str, str2, str3, pendingIntent, pendingIntent2);
                    } catch (Error | Exception e4) {
                        try {
                            Class<?> cls4 = Class.forName("com.mediatek.telephony.SmsManager");
                            cls4.getMethod("sendTextMessage", String.class, String.class, String.class, Integer.TYPE, PendingIntent.class, PendingIntent.class).invoke(cls4.newInstance(), str, str2, str3, Integer.valueOf(i), pendingIntent, pendingIntent2);
                        } catch (Error | Exception e5) {
                            try {
                                Class<?> cls5 = Class.forName("com.mediatek.telephony.SmsManagerEx");
                                Object invoke3 = cls5.getMethod("getDefault", null).invoke(null, null);
                                cls5.getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class, Integer.TYPE).invoke(invoke3, str, str2, str3, pendingIntent, pendingIntent2, Integer.valueOf(i));
                            } catch (Error | Exception e6) {
                                try {
                                    Object invoke4 = Class.forName("android.telephony.MultiSimSmsManager").getMethod("getDefault", Integer.TYPE).invoke(null, Integer.valueOf(i));
                                    invoke4.getClass().getMethod("sendTextMessage", String.class, String.class, String.class, PendingIntent.class, PendingIntent.class).invoke(invoke4, str, str2, str3, pendingIntent, pendingIntent2);
                                } catch (Error | Exception e7) {
                                    throw new NoSuchMethodException();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
