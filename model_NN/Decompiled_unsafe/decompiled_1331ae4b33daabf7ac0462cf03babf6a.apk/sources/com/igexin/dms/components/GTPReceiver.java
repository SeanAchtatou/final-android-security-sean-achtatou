package com.igexin.dms.components;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;

public class GTPReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final String f1160a = ("GDaemon_" + GTPReceiver.class.getSimpleName());

    public void onReceive(Context context, Intent intent) {
        try {
            Class a2 = a.a(context);
            if (a2 != null) {
                ComponentName componentName = new ComponentName(context.getPackageName(), a2.getCanonicalName());
                Intent intent2 = new Intent();
                intent2.putExtra("last_dm_time", intent.getLongExtra("last_dm_time", 0));
                intent2.setComponent(componentName);
                context.startService(intent2);
            }
        } catch (Throwable th) {
            f.a(this.f1160a, th.toString());
        }
    }
}
