package com.igexin.dms.components;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import com.igexin.dms.a.f;

public class GTAReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private final String f1159a = ("GDaemon_" + GTAReceiver.class.getSimpleName());

    public void onReceive(Context context, Intent intent) {
        try {
            ComponentName componentName = new ComponentName(context.getPackageName(), GTAService.class.getCanonicalName());
            Intent intent2 = new Intent();
            intent2.putExtra("last_dm_time", intent.getLongExtra("last_dm_time", 0));
            intent2.setComponent(componentName);
            context.startService(intent2);
        } catch (Throwable th) {
            f.a(this.f1159a, th.toString());
        }
    }
}
