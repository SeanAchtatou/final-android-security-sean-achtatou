package com.igexin.dms.core;

import android.content.Context;
import com.igexin.dms.a.f;
import java.io.File;

class d extends Thread {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1166a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f1167b;

    d(a aVar, Context context) {
        this.f1167b = aVar;
        this.f1166a = context;
    }

    public void run() {
        try {
            File dir = this.f1166a.getDir("dms", 0);
            new NativeCaller().doDaemon(new File(dir, "lock_dm").getAbsolutePath(), new File(dir, "lock_gt").getAbsolutePath(), new File(dir, "observer_dm").getAbsolutePath(), new File(dir, "observer_gt").getAbsolutePath());
        } catch (Throwable th) {
            f.a(this.f1167b.f1162b, th.toString());
        }
    }
}
