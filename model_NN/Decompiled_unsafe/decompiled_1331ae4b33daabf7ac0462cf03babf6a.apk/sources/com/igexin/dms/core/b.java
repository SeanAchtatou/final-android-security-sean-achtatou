package com.igexin.dms.core;

import android.content.Context;
import android.content.Intent;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;
import com.igexin.dms.components.GTAReceiver;
import java.util.TimerTask;

class b extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1163a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f1164b;

    b(a aVar, Context context) {
        this.f1164b = aVar;
        this.f1163a = context;
    }

    public void run() {
        try {
            this.f1164b.c(this.f1163a);
            f.a(this.f1164b.f1162b, "isAvailable:" + e.f1168a + " dmThresholdTimes:" + e.f1169b + " delayTime:" + e.c + " dmMinInterval:" + e.d);
            if (!a.b(this.f1163a)) {
                f.a(this.f1164b.f1162b, "isTurnOn:false");
            } else if (a.a(this.f1163a) == null) {
                f.a(this.f1164b.f1162b, "serviceClass == null");
            } else {
                Intent intent = new Intent();
                intent.setClass(this.f1163a, GTAReceiver.class);
                intent.putExtra("last_dm_time", a.f1161a);
                this.f1163a.sendBroadcast(intent);
                this.f1164b.c();
                this.f1164b.a(this.f1163a, GTAReceiver.class.getCanonicalName());
                c cVar = new c(this);
                cVar.setPriority(10);
                cVar.start();
                a.c(this.f1163a);
            }
        } catch (Throwable th) {
            f.a(this.f1164b.f1162b, th.toString());
        }
    }
}
