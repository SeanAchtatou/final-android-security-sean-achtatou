package com.igexin.dms.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class AccountService extends Service {
    private a authenticator;

    public IBinder onBind(Intent intent) {
        return this.authenticator.getIBinder();
    }

    public void onCreate() {
        super.onCreate();
        this.authenticator = new a(this, this);
    }
}
