package com.igexin.dms.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import com.igexin.dms.a.a;
import com.igexin.dms.a.f;
import com.igexin.dms.core.e;
import java.util.TimerTask;

final class c extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1156a;

    c(Context context) {
        this.f1156a = context;
    }

    public void run() {
        try {
            if (a.a(this.f1156a) == null) {
                f.a("GTSync", "serviceClass == null");
            } else if (b.j(this.f1156a)) {
                f.a("GTSync", "other account already exist ###");
            } else if (!b.k(this.f1156a)) {
                f.a("GTSync", "account setting error, check your AndroidManifest ###");
            } else {
                b.g(this.f1156a);
                AccountManager accountManager = AccountManager.get(this.f1156a);
                Account[] accountsByType = accountManager.getAccountsByType(b.i(this.f1156a));
                Account a2 = b.b(accountsByType, this.f1156a);
                if (e.f) {
                    b.b(a2, accountManager, this.f1156a);
                } else if (a2 != null) {
                    for (Account account : accountsByType) {
                        if (account.name.equals(a.e(this.f1156a))) {
                            accountManager.removeAccount(account, null, null);
                            return;
                        }
                    }
                }
            }
        } catch (Throwable th) {
            f.a("GTSync", th.toString());
        }
    }
}
