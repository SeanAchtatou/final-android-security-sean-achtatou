package com.igexin.dms.account;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import com.igexin.dms.a.f;

class d extends AbstractThreadedSyncAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SyncService f1157a;

    /* renamed from: b  reason: collision with root package name */
    private Context f1158b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(SyncService syncService, Context context, boolean z) {
        super(context, z);
        this.f1157a = syncService;
        this.f1158b = context;
    }

    public void onPerformSync(Account account, Bundle bundle, String str, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        f.a("GTSync", "onPerformSync");
        b.b(this.f1158b);
    }
}
