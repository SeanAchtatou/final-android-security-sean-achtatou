package com.igexin.b.a.d;

import com.igexin.b.a.c.a;
import com.igexin.b.a.d.d;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class c<E extends d> {
    static final /* synthetic */ boolean i = (!c.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    public String f1135a = getClass().getName();

    /* renamed from: b  reason: collision with root package name */
    final transient ReentrantLock f1136b = new ReentrantLock();
    final transient Condition c = this.f1136b.newCondition();
    final TreeSet<E> d;
    final AtomicInteger e = new AtomicInteger(0);
    int f;
    e g;
    public final AtomicLong h = new AtomicLong(-1);

    public c(Comparator<? super E> comparator, e eVar) {
        this.d = new TreeSet<>(comparator);
        this.g = eVar;
    }

    private E e() {
        E a2 = a();
        if (a2 != null && this.d.remove(a2)) {
            return a2;
        }
        return null;
    }

    public final int a(E e2, long j, TimeUnit timeUnit) {
        ReentrantLock reentrantLock = this.f1136b;
        reentrantLock.lock();
        try {
            if (!this.d.contains(e2)) {
                return -1;
            }
            this.d.remove(e2);
            e2.u = System.currentTimeMillis() + TimeUnit.MILLISECONDS.convert(j, timeUnit);
            int i2 = a(e2) ? 1 : -2;
            reentrantLock.unlock();
            return i2;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public E a() {
        try {
            return (d) this.d.first();
        } catch (NoSuchElementException e2) {
            return null;
        }
    }

    public final boolean a(d dVar) {
        if (dVar == null) {
            return false;
        }
        ReentrantLock reentrantLock = this.f1136b;
        reentrantLock.lock();
        try {
            d a2 = a();
            int i2 = this.f + 1;
            this.f = i2;
            dVar.v = i2;
            if (!this.d.add(dVar)) {
                dVar.v--;
                return false;
            }
            dVar.n();
            if (a2 == null || this.d.comparator().compare(dVar, a2) < 0) {
                this.c.signalAll();
            }
            reentrantLock.unlock();
            return true;
        } catch (Exception e2) {
            a.b("ScheduleQueue|offer|error");
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    public final boolean a(Class cls) {
        if (cls == null) {
            return false;
        }
        ReentrantLock reentrantLock = this.f1136b;
        reentrantLock.lock();
        try {
            ArrayList arrayList = new ArrayList();
            Iterator<E> it = this.d.iterator();
            while (it.hasNext()) {
                d dVar = (d) it.next();
                if (dVar.getClass() == cls) {
                    arrayList.add(dVar);
                }
            }
            this.d.removeAll(arrayList);
            return true;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean b() {
        ReentrantLock reentrantLock = this.f1136b;
        reentrantLock.lock();
        try {
            return this.d.isEmpty();
        } finally {
            reentrantLock.unlock();
        }
    }

    public final E c() {
        ReentrantLock reentrantLock = this.f1136b;
        reentrantLock.lockInterruptibly();
        while (true) {
            try {
                d a2 = a();
                if (a2 == null) {
                    this.e.set(1);
                    this.f = 0;
                    this.c.await();
                } else {
                    long a3 = a2.a(TimeUnit.NANOSECONDS);
                    boolean z = a2.k || a2.m;
                    if (a3 <= 0 || z) {
                        E e2 = e();
                    } else {
                        this.h.set(a2.u);
                        a.b("schedule take|needAlarm = " + this.g.t + "|" + a2.getClass().getName() + "@" + a2.hashCode());
                        if (this.g.t) {
                            this.g.a(a2.u);
                        }
                        this.c.awaitNanos(a3);
                    }
                }
            } finally {
                reentrantLock.unlock();
            }
        }
        E e22 = e();
        if (i || e22 != null) {
            if (!b()) {
                this.c.signalAll();
            }
            this.h.set(-1);
            return e22;
        }
        throw new AssertionError();
    }

    public final void d() {
        this.d.clear();
    }
}
