package com.igexin.b.a.b;

import com.igexin.b.a.d.d;

public abstract class e extends d {

    /* renamed from: a  reason: collision with root package name */
    public String f1127a;

    /* renamed from: b  reason: collision with root package name */
    public b f1128b;
    public Object c;
    public d d;

    public e(int i, String str, b bVar) {
        super(i);
        if (str != null) {
            this.f1127a = a(str);
        }
        this.f1128b = bVar;
    }

    public e(String str, b bVar) {
        this(0, str, bVar);
    }

    private String a(String str) {
        return f.a(f.a(str));
    }

    public void f() {
        if (this.f1128b != null) {
            this.f1128b.a(false);
        }
        this.f1128b = null;
        this.d = null;
        this.c = null;
        this.f1127a = null;
        super.f();
    }
}
