package com.igexin.b.a.d;

import android.os.Process;
import com.igexin.b.a.c.a;

final class i extends Thread {

    /* renamed from: a  reason: collision with root package name */
    volatile boolean f1145a = true;

    /* renamed from: b  reason: collision with root package name */
    f f1146b;
    final /* synthetic */ e c;

    public i(e eVar) {
        this.c = eVar;
        setName("taskService-processor");
    }

    public final void run() {
        Process.setThreadPriority(-2);
        c<d> cVar = this.c.k;
        char c2 = 1;
        d dVar = null;
        while (this.f1145a) {
            switch (c2) {
                case 65535:
                    try {
                        dVar.d();
                        a.b("TaskService|TASK_INIT|initTask =" + dVar.getClass().getName() + "@" + dVar.hashCode() + "|isBlock = " + dVar.q() + "|isCycle = " + dVar.o + "|doTime = " + dVar.u);
                        if (!dVar.q()) {
                            if (dVar.o && dVar.u == 0) {
                                a.b("TaskService|" + dVar + "|isBlock = false|cycyle = true|doTime = 0, " + "invalid ###########");
                                c2 = 1;
                                break;
                            }
                        } else {
                            if (this.f1146b == null) {
                                this.f1146b = new f(this.c);
                            }
                            a.b(dVar.getClass().getName() + " is a block task!");
                            this.f1146b.a(dVar);
                            c2 = 1;
                            dVar = null;
                            break;
                        }
                    } catch (Exception e) {
                        a.b("TaskService|TASK_INIT|error|" + e.toString());
                        c2 = 1;
                        break;
                    }
                case 0:
                    try {
                        dVar.a_();
                        dVar.t();
                        dVar.v();
                        this.c.g();
                        a.b("TaskService|SERVICE_PROCESSING|finally|hasError = " + dVar.t + "|isDone = " + dVar.s() + "|isPending = " + dVar.p + "|isCycle = " + dVar.o + "|isBlcok = " + dVar.q());
                        if (!dVar.t) {
                            dVar.c();
                        }
                        if (!dVar.k && !dVar.p) {
                            a.b("TaskService|SERVICE_PROCESSING|finally|mainQueue.offer task = " + dVar);
                            dVar.A = 0;
                            cVar.a(dVar);
                        }
                        c2 = 1;
                        dVar = null;
                    } catch (Exception e2) {
                        a.b("TaskService|SERVICE_PROCESSING|error|" + e2.toString());
                        dVar.t = true;
                        dVar.B = e2;
                        dVar.w();
                        dVar.p();
                        this.c.j.offer(dVar);
                        this.c.g();
                        a.b("TaskService|SERVICE_PROCESSING|finally|hasError = " + dVar.t + "|isDone = " + dVar.s() + "|isPending = " + dVar.p + "|isCycle = " + dVar.o + "|isBlcok = " + dVar.q());
                        if (!dVar.t) {
                            dVar.c();
                        }
                        if (!dVar.k && !dVar.p) {
                            a.b("TaskService|SERVICE_PROCESSING|finally|mainQueue.offer task = " + dVar);
                            dVar.A = 0;
                            cVar.a(dVar);
                        }
                        c2 = 1;
                        dVar = null;
                    } catch (Throwable th) {
                        this.c.g();
                        a.b("TaskService|SERVICE_PROCESSING|finally|hasError = " + dVar.t + "|isDone = " + dVar.s() + "|isPending = " + dVar.p + "|isCycle = " + dVar.o + "|isBlcok = " + dVar.q());
                        if (!dVar.t) {
                            dVar.c();
                        }
                        if (!dVar.k && !dVar.p) {
                            a.b("TaskService|SERVICE_PROCESSING|finally|mainQueue.offer task = " + dVar);
                            dVar.A = 0;
                            cVar.a(dVar);
                        }
                        throw th;
                    }
                case 1:
                    try {
                        dVar = cVar.c();
                    } catch (InterruptedException e3) {
                    }
                    if (dVar != null) {
                        if (!dVar.k && !dVar.m) {
                            c2 = 65535;
                            break;
                        } else {
                            dVar = null;
                            break;
                        }
                    }
                    break;
                case 2:
                    this.c.g();
                    dVar = dVar;
                    c2 = 1;
                    break;
            }
        }
        cVar.d();
    }
}
