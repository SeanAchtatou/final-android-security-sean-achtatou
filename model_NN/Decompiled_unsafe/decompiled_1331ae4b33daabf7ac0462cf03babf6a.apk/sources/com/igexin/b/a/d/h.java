package com.igexin.b.a.d;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class h implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    final AtomicInteger f1143a = new AtomicInteger(0);

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ f f1144b;

    public h(f fVar) {
        this.f1144b = fVar;
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "TaskService-pool-" + this.f1143a.incrementAndGet());
    }
}
