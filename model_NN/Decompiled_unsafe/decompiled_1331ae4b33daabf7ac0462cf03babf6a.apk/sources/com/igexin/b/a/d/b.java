package com.igexin.b.a.d;

import com.igexin.b.a.d.a.f;
import java.util.concurrent.TimeUnit;

public abstract class b implements f {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1134a = true;

    public void a() {
        this.f1134a = false;
    }

    public boolean a(long j, d dVar) {
        return TimeUnit.SECONDS.toMillis((long) dVar.y) < j - dVar.w;
    }

    public long b(long j, d dVar) {
        return (TimeUnit.SECONDS.toMillis((long) dVar.y) + dVar.w) - j;
    }
}
