package com.igexin.b.a.d;

import com.igexin.b.a.c.a;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

final class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final BlockingQueue<d> f1141a = new LinkedBlockingQueue();

    /* renamed from: b  reason: collision with root package name */
    d f1142b;
    d c;
    volatile int d;
    final /* synthetic */ f e;

    public g(f fVar, d dVar) {
        this.e = fVar;
        this.f1142b = dVar;
    }

    public final void a() {
        this.f1141a.clear();
        this.c = null;
    }

    public final void a(d dVar) {
        if (this.d == 0) {
            this.d = dVar.z;
        }
        boolean z = true;
        while (z) {
            try {
                dVar.a_();
                dVar.t();
                dVar.v();
                if (!dVar.t) {
                    dVar.c();
                }
                a.b("TaskService|Worker|runTask = " + dVar + "|isDone = " + dVar.k + "|isCycle = " + dVar.o + "|doTime = " + dVar.u);
                if (!dVar.k && dVar.o && dVar.u != 0) {
                }
            } catch (Exception e2) {
                a.b("TaskService" + e2.toString());
                dVar.t = true;
                dVar.B = e2;
                dVar.w();
                dVar.p();
                this.e.i.a(dVar);
                this.e.i.f();
                if (!dVar.t) {
                    dVar.c();
                }
                a.b("TaskService|Worker|runTask = " + dVar + "|isDone = " + dVar.k + "|isCycle = " + dVar.o + "|doTime = " + dVar.u);
                if (!dVar.k && dVar.o && dVar.u != 0) {
                }
            } catch (Throwable th) {
                if (!dVar.t) {
                    dVar.c();
                }
                a.b("TaskService|Worker|runTask = " + dVar + "|isDone = " + dVar.k + "|isCycle = " + dVar.o + "|doTime = " + dVar.u);
                if (dVar.k || !dVar.o || dVar.u == 0) {
                    throw th;
                }
            }
            z = false;
            dVar = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.igexin.b.a.d.d b() {
        /*
            r5 = this;
            r1 = 0
        L_0x0001:
            int r0 = r5.d
            if (r0 == 0) goto L_0x0052
            java.util.concurrent.BlockingQueue<com.igexin.b.a.d.d> r0 = r5.f1141a     // Catch:{ InterruptedException -> 0x0031 }
            com.igexin.b.a.d.f r2 = r5.e     // Catch:{ InterruptedException -> 0x0031 }
            long r2 = r2.e     // Catch:{ InterruptedException -> 0x0031 }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.NANOSECONDS     // Catch:{ InterruptedException -> 0x0031 }
            java.lang.Object r0 = r0.poll(r2, r4)     // Catch:{ InterruptedException -> 0x0031 }
            com.igexin.b.a.d.d r0 = (com.igexin.b.a.d.d) r0     // Catch:{ InterruptedException -> 0x0031 }
            if (r0 == 0) goto L_0x0016
        L_0x0015:
            return r0
        L_0x0016:
            java.util.concurrent.BlockingQueue<com.igexin.b.a.d.d> r0 = r5.f1141a     // Catch:{ InterruptedException -> 0x0031 }
            boolean r0 = r0.isEmpty()     // Catch:{ InterruptedException -> 0x0031 }
            if (r0 == 0) goto L_0x0001
            com.igexin.b.a.d.f r0 = r5.e     // Catch:{ InterruptedException -> 0x0031 }
            java.util.concurrent.locks.ReentrantLock r2 = r0.c     // Catch:{ InterruptedException -> 0x0031 }
            r2.lock()     // Catch:{ InterruptedException -> 0x0031 }
            java.util.concurrent.BlockingQueue<com.igexin.b.a.d.d> r0 = r5.f1141a     // Catch:{ all -> 0x004d }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x004d }
            if (r0 != 0) goto L_0x0033
            r2.unlock()     // Catch:{ InterruptedException -> 0x0031 }
            goto L_0x0001
        L_0x0031:
            r0 = move-exception
            goto L_0x0001
        L_0x0033:
            com.igexin.b.a.d.f r0 = r5.e     // Catch:{ all -> 0x004d }
            java.util.HashMap<java.lang.Integer, com.igexin.b.a.d.g> r0 = r0.f1140b     // Catch:{ all -> 0x004d }
            int r3 = r5.d     // Catch:{ all -> 0x004d }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x004d }
            r0.remove(r3)     // Catch:{ all -> 0x004d }
            com.igexin.b.a.d.d r0 = r5.c     // Catch:{ all -> 0x004d }
            r0.e()     // Catch:{ all -> 0x004d }
            r0 = 0
            r5.d = r0     // Catch:{ all -> 0x004d }
            r2.unlock()     // Catch:{ InterruptedException -> 0x0031 }
            r0 = r1
            goto L_0x0015
        L_0x004d:
            r0 = move-exception
            r2.unlock()     // Catch:{ InterruptedException -> 0x0031 }
            throw r0     // Catch:{ InterruptedException -> 0x0031 }
        L_0x0052:
            r0 = r1
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.b.a.d.g.b():com.igexin.b.a.d.d");
    }

    public final void run() {
        boolean z = true;
        while (z) {
            try {
                d dVar = this.f1142b;
                this.f1142b = null;
                while (true) {
                    if (dVar == null) {
                        dVar = b();
                        if (dVar == null && (dVar = this.e.a()) == null) {
                            break;
                        }
                    }
                    this.c = null;
                    a(dVar);
                    this.c = dVar;
                    dVar = null;
                }
                z = this.e.a(this);
                if (!z) {
                    a();
                }
            } catch (Exception e2) {
                a.b("TaskService|Worker|run()|error" + e2.toString());
                z = this.e.a(this);
                if (!z) {
                    a();
                }
            } catch (Throwable th) {
                if (!this.e.a(this)) {
                    a();
                }
                throw th;
            }
        }
    }
}
