package com.igexin.b.a.b.a.a;

import android.text.TextUtils;
import com.igexin.b.a.b.a.a.a.c;
import com.igexin.b.a.b.b;
import com.igexin.b.a.b.d;
import com.igexin.b.a.c.a;

public final class l extends a {
    private static final String L = l.class.getName();
    private c M;
    private d N;
    public b i;
    n j;

    public l(n nVar, b bVar, d dVar) {
        super(-2036, null, bVar);
        this.i = bVar;
        this.N = dVar;
        this.j = nVar;
    }

    public void a(c cVar) {
        this.M = cVar;
    }

    public void a_() {
        super.a_();
        Thread currentThread = Thread.currentThread();
        a.b(L + "|" + currentThread + " running");
        d a2 = d.a();
        while (this.h && !currentThread.isInterrupted() && !this.e) {
            try {
                a2.f1116a.lock();
                if (a2.c.isEmpty()) {
                    a2.f1117b.await();
                }
                k poll = a2.c.poll();
                if (poll != null) {
                    poll.d = this.N;
                    if (!(this.N == null || this.j == null || this.e)) {
                        this.f = b.NORMAL;
                        this.j.a((byte[]) this.i.d(null, this.N, poll.c));
                        a.b(L + "|" + poll.toString() + " --> " + poll.c.getClass().getName() + "-- send success");
                        if (this.M != null && !this.e) {
                            this.M.a(poll);
                        }
                    }
                }
                try {
                    a2.f1116a.unlock();
                } catch (Exception e) {
                }
            } catch (Throwable th) {
                try {
                    a2.f1116a.unlock();
                } catch (Exception e2) {
                }
                throw th;
            }
        }
        this.e = true;
        a.b(L + "|finish ~~~~~~");
    }

    public final int b() {
        return -2036;
    }

    public void f() {
        super.f();
        a.b(L + "|dispose");
        if (this.M != null) {
            if (this.f == b.EXCEPTION) {
                if (!TextUtils.isEmpty(this.g)) {
                    this.M.a(new Exception(this.g));
                }
            } else if (this.f == b.INTERRUPT) {
                this.M.a(this);
            }
        }
        this.M = null;
    }

    public void h() {
        this.h = false;
        this.f = b.INTERRUPT;
        d a2 = d.a();
        try {
            if (!this.e) {
                a2.f1116a.lock();
                d.a().f1117b.signalAll();
            }
            try {
                a2.f1116a.unlock();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            try {
                a2.f1116a.unlock();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                a2.f1116a.unlock();
            } catch (Exception e4) {
            }
            throw th;
        }
    }
}
