package com.igexin.b.a.b.a.a;

import android.os.Message;
import com.igexin.b.a.b.a.a.a.d;
import com.igexin.b.a.c.a;
import java.net.Socket;

class e implements d {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1118a;

    e(d dVar) {
        this.f1118a = dVar;
    }

    public void a(com.igexin.b.a.b.e eVar) {
        this.f1118a.o.sendEmptyMessage(3);
    }

    public void a(Exception exc) {
        a.b(d.e + "|c ex = " + exc.toString());
        this.f1118a.b();
    }

    public void a(String str) {
        this.f1118a.o.sendEmptyMessage(1);
    }

    public void a(Socket socket) {
        Message obtain = Message.obtain();
        obtain.obj = socket;
        obtain.what = 2;
        this.f1118a.o.sendMessage(obtain);
    }
}
