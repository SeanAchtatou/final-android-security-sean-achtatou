package com.igexin.b.a.d;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.PowerManager;
import com.igexin.b.a.b.a.a.d;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.a.b;
import com.igexin.push.d.c.p;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class e extends BroadcastReceiver implements Comparator<d> {
    public static final String g = e.class.getName();
    public static final long u = TimeUnit.SECONDS.toMillis(2);

    /* renamed from: a  reason: collision with root package name */
    private boolean f1138a = false;
    final i h = new i(this);
    final HashMap<Long, b> i = new HashMap<>(7);
    final ConcurrentLinkedQueue<com.igexin.b.a.d.a.e> j = new ConcurrentLinkedQueue<>();
    final c<d> k = new c<>(this, this);
    final ReentrantLock l = new ReentrantLock();
    PowerManager m;
    AlarmManager n;
    Intent o;
    PendingIntent p;
    Intent q;
    PendingIntent r;
    String s;
    volatile boolean t;

    protected e() {
        d.E = this;
    }

    /* renamed from: a */
    public final int compare(d dVar, d dVar2) {
        int i2 = dVar.A > dVar2.A ? -1 : dVar.A < dVar2.A ? 1 : dVar.v < dVar2.v ? -1 : dVar.v > dVar2.v ? 1 : 0;
        if (dVar.u != dVar2.u) {
            i2 = dVar.u < dVar2.u ? -1 : 1;
        }
        return i2 == 0 ? dVar.hashCode() - dVar2.hashCode() : i2;
    }

    @TargetApi(19)
    public final void a(long j2) {
        if (this.t) {
            a.b("setnioalarm|" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date(j2)));
            if (j2 < 0) {
                j2 = System.currentTimeMillis() + u;
            }
            try {
                if (this.p == null) {
                    return;
                }
                if (Build.VERSION.SDK_INT < 19) {
                    this.n.set(0, j2, this.p);
                } else {
                    this.n.setExact(0, j2, this.p);
                }
            } catch (Throwable th) {
                a.b("TaskService" + th.toString());
            }
        }
    }

    public final void a(Context context) {
        if (!this.f1138a) {
            this.m = (PowerManager) context.getSystemService("power");
            this.t = true;
            this.n = (AlarmManager) context.getSystemService("alarm");
            context.registerReceiver(this, new IntentFilter("AlarmTaskSchedule." + context.getPackageName()));
            context.registerReceiver(this, new IntentFilter("AlarmTaskScheduleBak." + context.getPackageName()));
            context.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_OFF"));
            context.registerReceiver(this, new IntentFilter("android.intent.action.SCREEN_ON"));
            this.s = "AlarmNioTaskSchedule." + context.getPackageName();
            context.registerReceiver(this, new IntentFilter(this.s));
            this.o = new Intent("AlarmTaskSchedule." + context.getPackageName());
            this.p = PendingIntent.getBroadcast(context, hashCode(), this.o, NTLMConstants.FLAG_UNIDENTIFIED_10);
            this.q = new Intent(this.s);
            this.r = PendingIntent.getBroadcast(context, hashCode() + 2, this.q, NTLMConstants.FLAG_UNIDENTIFIED_10);
            this.h.start();
            try {
                Thread.yield();
            } catch (Throwable th) {
            }
            this.f1138a = true;
        }
    }

    public final boolean a(b bVar) {
        if (bVar == null) {
            throw new NullPointerException();
        }
        ReentrantLock reentrantLock = this.l;
        if (!reentrantLock.tryLock()) {
            return false;
        }
        try {
            if (this.i.keySet().contains(Long.valueOf(bVar.o()))) {
                return false;
            }
            this.i.put(Long.valueOf(bVar.o()), bVar);
            reentrantLock.unlock();
            return true;
        } catch (Throwable th) {
            a.b("TaskService|" + th.toString());
            return false;
        } finally {
            reentrantLock.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(com.igexin.b.a.d.a.e eVar, b bVar) {
        int b2 = eVar.b();
        if (b2 > Integer.MIN_VALUE && b2 < 0) {
            d dVar = (d) eVar;
            boolean a2 = dVar.t ? bVar.a(dVar, this) : bVar.a(eVar, this);
            if (a2) {
                dVar.c();
            }
            return a2;
        } else if (b2 < 0 || b2 >= Integer.MAX_VALUE) {
            return false;
        } else {
            return bVar.a(eVar, this);
        }
    }

    public final boolean a(d dVar, boolean z) {
        int i2 = 0;
        if (dVar == null) {
            throw new NullPointerException();
        } else if (dVar.p || dVar.k) {
            return false;
        } else {
            c<d> cVar = this.k;
            if (z) {
                i2 = cVar.e.incrementAndGet();
            }
            dVar.A = i2;
            return cVar.a(dVar);
        }
    }

    public final boolean a(d dVar, boolean z, boolean z2) {
        boolean z3 = true;
        if (dVar == null) {
            throw new NullPointerException();
        } else if (dVar.m) {
            return false;
        } else {
            if (!z || z2) {
                if (!z2 || !z) {
                    z3 = false;
                }
                return a(dVar, z3);
            }
            dVar.d();
            try {
                dVar.a_();
                dVar.t();
                dVar.v();
                if (!dVar.t) {
                    dVar.c();
                }
                return true;
            } catch (Exception e) {
                dVar.t = true;
                dVar.B = e;
                dVar.p();
                dVar.w();
                a(dVar);
                f();
                if (dVar.t) {
                    return false;
                }
                dVar.c();
                return false;
            } catch (Throwable th) {
                if (!dVar.t) {
                    dVar.c();
                }
                throw th;
            }
        }
    }

    public final boolean a(Class cls) {
        c<d> cVar = this.k;
        return cVar != null && cVar.a(cls);
    }

    public final boolean a(Object obj) {
        if (obj == null) {
            return false;
        }
        try {
            if (obj instanceof p) {
                a.a("TaskService|responseTask|" + obj.getClass().getName() + "|" + obj.hashCode() + "|" + ((String) ((p) obj).e));
            }
        } catch (Exception e) {
        }
        a.b("TaskService|responseQueue ++ task = " + obj.getClass().getName() + "@" + obj.hashCode());
        if (!(obj instanceof com.igexin.b.a.d.a.e)) {
            throw new ClassCastException("response Obj is not a TaskResult ");
        }
        com.igexin.b.a.d.a.e eVar = (com.igexin.b.a.d.a.e) obj;
        if (eVar.l()) {
            return false;
        }
        eVar.c(false);
        this.j.offer(eVar);
        return true;
    }

    @TargetApi(19)
    public final void b(long j2) {
        a.b("setnioalarm|" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date(j2)));
        if (j2 < 0) {
            j2 = System.currentTimeMillis() + u;
        }
        try {
            if (Build.VERSION.SDK_INT < 19) {
                this.n.set(0, j2, this.r);
            } else {
                this.n.setExact(0, j2, this.r);
            }
        } catch (Exception e) {
            this.n.set(0, j2, this.r);
        } catch (Throwable th) {
        }
    }

    public final void e() {
        try {
            if (this.r != null) {
                this.n.cancel(this.r);
            }
        } catch (Throwable th) {
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.h != null && !this.h.isInterrupted()) {
            this.h.interrupt();
        }
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        int b2;
        int b3;
        int b4;
        a.b("TaskService|call notifyObserver ~~~~");
        while (!this.j.isEmpty()) {
            com.igexin.b.a.d.a.e poll = this.j.poll();
            a.b("TaskService|notifyObserver responseQueue -- task = " + poll);
            poll.c(true);
            boolean z = false;
            ReentrantLock reentrantLock = this.l;
            reentrantLock.lock();
            try {
                if (!this.i.isEmpty()) {
                    long m2 = poll.m();
                    if (m2 == 0) {
                        for (b next : this.i.values()) {
                            if (next.n() && (z = a(poll, next))) {
                                break;
                            }
                        }
                    } else {
                        b bVar = this.i.get(Long.valueOf(m2));
                        z = (bVar == null || !bVar.n()) ? false : a(poll, bVar);
                    }
                }
                if (!z && (b4 = poll.b()) > Integer.MIN_VALUE && b4 < 0) {
                    ((d) poll).c();
                }
                reentrantLock.unlock();
            } catch (Throwable th) {
                if (0 == 0 && (b2 = poll.b()) > Integer.MIN_VALUE && b2 < 0) {
                    ((d) poll).c();
                }
                reentrantLock.unlock();
                throw th;
            }
        }
    }

    public final void onReceive(Context context, Intent intent) {
        if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
            this.t = true;
            a.b("screenoff");
            if (this.k.h.get() > 0) {
                a(this.k.h.get());
            }
        } else if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
            this.t = false;
            a.b("screenon");
        } else if (intent.getAction().startsWith("AlarmTaskSchedule.") || intent.getAction().startsWith("AlarmTaskScheduleBak.")) {
            a.b("receivealarm|" + this.t);
            f();
        } else if (this.s.equals(intent.getAction())) {
            a.b("receive nioalarm");
            try {
                a.b("TaskService|alarm time out #######");
                d.a().d();
            } catch (Exception e) {
            }
        }
    }
}
