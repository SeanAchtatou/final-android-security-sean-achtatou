package com.igexin.b.a.b.a.a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.igexin.b.a.c.a;
import com.igexin.push.core.a.e;
import java.net.Socket;

class i extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1122a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i(d dVar) {
        super(Looper.getMainLooper());
        this.f1122a = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, boolean):boolean
     arg types: [com.igexin.b.a.b.a.a.d, int]
     candidates:
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, com.igexin.b.a.b.a.a.k):void
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, java.net.Socket):void
      com.igexin.b.a.b.a.a.d.a(com.igexin.b.a.b.a.a.d, boolean):boolean */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                if (!this.f1122a.k()) {
                    this.f1122a.f();
                    return;
                }
                return;
            case 1:
                boolean unused = this.f1122a.l = false;
                return;
            case 2:
                this.f1122a.a((Socket) message.obj);
                return;
            case 3:
                if (this.f1122a.j()) {
                    this.f1122a.h();
                    return;
                }
                return;
            case 4:
                this.f1122a.l();
                if (this.f1122a.k == null && this.f1122a.j == null && this.f1122a.i == null) {
                    e.a().e(false);
                    return;
                } else if (this.f1122a.j()) {
                    this.f1122a.h();
                    return;
                } else {
                    this.f1122a.g();
                    return;
                }
            case 5:
                if (this.f1122a.j()) {
                    a.b(d.e + "|tcp already close reconnect immediately");
                    e.a().e(((Boolean) message.obj).booleanValue());
                    return;
                }
                a.b(d.e + "|reconnect will run after close");
                return;
            default:
                return;
        }
    }
}
