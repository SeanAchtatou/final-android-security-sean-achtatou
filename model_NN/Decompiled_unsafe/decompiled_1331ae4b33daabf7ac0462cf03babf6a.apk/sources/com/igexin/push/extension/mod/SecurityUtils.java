package com.igexin.push.extension.mod;

import com.igexin.a.c;
import com.igexin.b.a.c.a;
import com.igexin.sdk.GTServiceManager;

public class SecurityUtils {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1456a = SecurityUtils.class.getName();

    /* renamed from: b  reason: collision with root package name */
    public static boolean f1457b;
    public static String c;

    static {
        c = "";
        try {
            a.b(f1456a + "|load so by system start #######");
            System.loadLibrary("getuiext2");
            f1457b = true;
            a.b(f1456a + "|load so by system success ^_^");
        } catch (UnsatisfiedLinkError e) {
            a.b(f1456a + "|load so by system error = " + e.toString());
            c = e.getMessage() + " + ";
            a.b(f1456a + "|load so by new start !!");
            if (GTServiceManager.context == null) {
                a.b(f1456a + "|load so by new context = null ~~~~");
                f1457b = false;
                c = e.getMessage();
                return;
            }
            c.a(null).a().b().a(GTServiceManager.context, "getuiext2", null, new a());
        } catch (Throwable th) {
            a.b(f1456a + "|load so error not UnsatisfiedLinkError");
            a.b(f1456a + "|load so error e = " + th.toString());
            f1457b = false;
            c += th.toString() + " + " + th.getMessage();
        }
    }

    public static native byte[] a();

    public static native byte[] b(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] c(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] d(byte[] bArr);

    public static native byte[] e();

    public static native byte[] f(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] g(byte[] bArr, byte[] bArr2, byte[] bArr3);

    public static native byte[] h(byte[] bArr);

    public static native byte[] i(byte[] bArr);

    public static native byte[] j();

    public static native byte[] k();

    public static native byte[] l(byte[] bArr, byte[] bArr2);

    public static native byte[] m(byte[] bArr, byte[] bArr2);
}
