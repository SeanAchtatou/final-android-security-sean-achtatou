package com.igexin.push.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.b.a.c.a;
import com.igexin.sdk.PushBuildConfig;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class b {
    public static void a(d dVar, Context context) {
        new Thread(new c(context, dVar)).start();
    }

    /* access modifiers changed from: private */
    public static boolean c(Context context) {
        try {
            byte[] a2 = e.a(context.getFilesDir().getPath() + "/" + "init_er.pid");
            return a2 == null || System.currentTimeMillis() - Long.valueOf(new String(a2)).longValue() > LogBuilder.MAX_INTERVAL;
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public static String d(Context context) {
        String packageName = context.getPackageName();
        String str = null;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(packageName, 128);
            if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                str = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPID);
            }
        } catch (Exception e) {
        }
        String str2 = Build.MODEL;
        String str3 = Build.VERSION.SDK;
        String str4 = Build.VERSION.RELEASE;
        File file = new File(context.getApplicationInfo().nativeLibraryDir + File.separator + "libgetuiext2.so");
        StringBuilder sb = new StringBuilder();
        sb.append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date()));
        sb.append("|");
        sb.append(str);
        sb.append("|");
        sb.append(PushBuildConfig.sdk_conf_version);
        sb.append("|");
        sb.append(file.exists());
        sb.append("|");
        sb.append(m.a(context));
        sb.append("|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        sb.append("|");
        sb.append(str4);
        sb.append("|");
        sb.append(m.b(context));
        sb.append("|");
        sb.append(m.a());
        sb.append("|");
        sb.append(packageName);
        if (EncryptUtils.errorMsg != null) {
            sb.append("|");
            sb.append(EncryptUtils.errorMsg);
        }
        a.b("ErrorReport|" + sb.toString());
        return sb.toString();
    }
}
