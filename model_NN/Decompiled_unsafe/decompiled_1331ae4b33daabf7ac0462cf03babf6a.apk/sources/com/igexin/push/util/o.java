package com.igexin.push.util;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

final class o implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    boolean f1485a;

    /* renamed from: b  reason: collision with root package name */
    private final LinkedBlockingQueue<IBinder> f1486b;

    private o() {
        this.f1485a = false;
        this.f1486b = new LinkedBlockingQueue<>(1);
    }

    public IBinder a() {
        if (this.f1485a) {
            throw new IllegalStateException();
        }
        this.f1485a = true;
        return this.f1486b.take();
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.f1486b.put(iBinder);
        } catch (Exception e) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
