package com.igexin.push.util;

import java.io.UnsupportedEncodingException;

public class i {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f1482a = (!i.class.desiredAssertionStatus());

    private i() {
    }

    public static byte[] a(String str, int i) {
        return a(str.getBytes(), i);
    }

    public static byte[] a(byte[] bArr, int i) {
        return a(bArr, 0, bArr.length, i);
    }

    public static byte[] a(byte[] bArr, int i, int i2, int i3) {
        k kVar = new k(i3, new byte[((i2 * 3) / 4)]);
        if (!kVar.a(bArr, i, i2, true)) {
            throw new IllegalArgumentException("bad base-64");
        } else if (kVar.f1484b == kVar.f1483a.length) {
            return kVar.f1483a;
        } else {
            byte[] bArr2 = new byte[kVar.f1484b];
            System.arraycopy(kVar.f1483a, 0, bArr2, 0, kVar.f1484b);
            return bArr2;
        }
    }

    public static String b(byte[] bArr, int i) {
        try {
            return new String(c(bArr, i), "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public static byte[] b(byte[] bArr, int i, int i2, int i3) {
        l lVar = new l(i3, null);
        int i4 = (i2 / 3) * 4;
        if (!lVar.d) {
            switch (i2 % 3) {
                case 1:
                    i4 += 2;
                    break;
                case 2:
                    i4 += 3;
                    break;
            }
        } else if (i2 % 3 > 0) {
            i4 += 4;
        }
        if (lVar.e && i2 > 0) {
            i4 += (lVar.f ? 2 : 1) * (((i2 - 1) / 57) + 1);
        }
        lVar.f1483a = new byte[i4];
        lVar.a(bArr, i, i2, true);
        if (f1482a || lVar.f1484b == i4) {
            return lVar.f1483a;
        }
        throw new AssertionError();
    }

    public static byte[] c(byte[] bArr, int i) {
        return b(bArr, 0, bArr.length, i);
    }
}
