package com.igexin.push.util;

import android.content.Context;
import com.igexin.b.a.b.f;
import com.igexin.b.b.a;
import com.igexin.push.config.SDKUrlConfig;
import org.json.JSONObject;

final class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f1477a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ d f1478b;

    c(Context context, d dVar) {
        this.f1477a = context;
        this.f1478b = dVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void
     arg types: [byte[], java.lang.String, int]
     candidates:
      com.igexin.push.util.e.a(java.io.File, java.io.File, java.lang.String):boolean
      com.igexin.push.util.e.a(java.lang.String, java.lang.String, boolean):boolean
      com.igexin.push.util.e.a(byte[], java.lang.String, boolean):void */
    public void run() {
        boolean z = false;
        try {
            if (b.c(this.f1477a)) {
                e.a(String.valueOf(System.currentTimeMillis()).getBytes(), this.f1477a.getFilesDir().getPath() + "/" + "init_er.pid", false);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("action", "upload_BI");
                jSONObject.put("BIType", "25");
                jSONObject.put("cid", "0");
                jSONObject.put("BIData", new String(f.f(b.d(this.f1477a).getBytes(), 0), "UTF-8"));
                byte[] a2 = s.a(SDKUrlConfig.getBiUploadServiceUrl(), a.b(jSONObject.toString().getBytes()), 10000, 10000);
                if (a2 != null) {
                    new String(a2);
                }
                z = true;
            }
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("ErrorReport|report 25 ex = " + th.toString());
        }
        if (this.f1478b != null) {
            this.f1478b.a(z);
        }
    }
}
