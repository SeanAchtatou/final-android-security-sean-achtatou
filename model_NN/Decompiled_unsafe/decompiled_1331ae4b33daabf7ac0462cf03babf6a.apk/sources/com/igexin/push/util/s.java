package com.igexin.push.util;

import android.text.TextUtils;
import cn.banshenggua.aichang.utils.StringUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class s {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1489a = s.class.getName();

    private static String a(InputStream inputStream, String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, str));
            StringWriter stringWriter = new StringWriter();
            char[] cArr = new char[256];
            while (true) {
                int read = bufferedReader.read(cArr);
                if (read <= 0) {
                    break;
                }
                stringWriter.write(cArr, 0, read);
            }
            return stringWriter.toString();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    private static String a(String str) {
        if (TextUtils.isEmpty(str)) {
            return StringUtil.Encoding;
        }
        for (String trim : str.split(";")) {
            String trim2 = trim.trim();
            if (trim2.startsWith("charset")) {
                String[] split = trim2.split("=", 2);
                return (split.length != 2 || TextUtils.isEmpty(split[1])) ? StringUtil.Encoding : split[1].trim();
            }
        }
        return StringUtil.Encoding;
    }

    private static HttpURLConnection a(URL url, String str, String str2) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(str);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(30000);
        httpURLConnection.setRequestProperty("User-Agent", "GETUI");
        httpURLConnection.setRequestProperty("Content-Type", str2);
        httpURLConnection.setRequestProperty("HOST", url.getHost() + ":" + url.getPort());
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0036 A[SYNTHETIC, Splitter:B:22:0x0036] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(java.io.InputStream r7) {
        /*
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0040, all -> 0x0030 }
            r1.<init>(r7)     // Catch:{ Exception -> 0x0040, all -> 0x0030 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x001d, all -> 0x003e }
            r3 = 1024(0x400, float:1.435E-42)
            r2.<init>(r3)     // Catch:{ Exception -> 0x001d, all -> 0x003e }
            r3 = 1024(0x400, float:1.435E-42)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x001d, all -> 0x003e }
        L_0x0011:
            int r4 = r1.read(r3)     // Catch:{ Exception -> 0x001d, all -> 0x003e }
            r5 = -1
            if (r4 == r5) goto L_0x0024
            r5 = 0
            r2.write(r3, r5, r4)     // Catch:{ Exception -> 0x001d, all -> 0x003e }
            goto L_0x0011
        L_0x001d:
            r2 = move-exception
        L_0x001e:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x003a }
        L_0x0023:
            return r0
        L_0x0024:
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x001d, all -> 0x003e }
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x002e }
            goto L_0x0023
        L_0x002e:
            r1 = move-exception
            goto L_0x0023
        L_0x0030:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()     // Catch:{ IOException -> 0x003c }
        L_0x0039:
            throw r0
        L_0x003a:
            r1 = move-exception
            goto L_0x0023
        L_0x003c:
            r1 = move-exception
            goto L_0x0039
        L_0x003e:
            r0 = move-exception
            goto L_0x0034
        L_0x0040:
            r1 = move-exception
            r1 = r0
            goto L_0x001e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.s.a(java.io.InputStream):byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002b, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0037, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r0;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x0012, B:14:0x002a] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] a(java.lang.String r3, java.lang.String r4, byte[] r5, int r6, int r7) {
        /*
            r2 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x0028, all -> 0x0039 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0028, all -> 0x0039 }
            java.lang.String r1 = "POST"
            java.net.HttpURLConnection r1 = a(r0, r1, r4)     // Catch:{ IOException -> 0x0028, all -> 0x0039 }
            r1.setConnectTimeout(r6)     // Catch:{ IOException -> 0x003c }
            r1.setReadTimeout(r7)     // Catch:{ IOException -> 0x003c }
            java.io.OutputStream r2 = r1.getOutputStream()     // Catch:{ Exception -> 0x0037 }
            r2.write(r5)     // Catch:{ Exception -> 0x0037 }
            byte[] r0 = a(r1)     // Catch:{ Exception -> 0x0037 }
            if (r2 == 0) goto L_0x0022
            r2.close()
        L_0x0022:
            if (r1 == 0) goto L_0x0027
            r1.disconnect()
        L_0x0027:
            return r0
        L_0x0028:
            r0 = move-exception
            r1 = r2
        L_0x002a:
            throw r0     // Catch:{ all -> 0x002b }
        L_0x002b:
            r0 = move-exception
        L_0x002c:
            if (r2 == 0) goto L_0x0031
            r2.close()
        L_0x0031:
            if (r1 == 0) goto L_0x0036
            r1.disconnect()
        L_0x0036:
            throw r0
        L_0x0037:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x002b }
        L_0x0039:
            r0 = move-exception
            r1 = r2
            goto L_0x002c
        L_0x003c:
            r0 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.util.s.a(java.lang.String, java.lang.String, byte[], int, int):byte[]");
    }

    public static byte[] a(String str, byte[] bArr, int i, int i2) {
        return a(str, "application/octet-stream", bArr, i, i2);
    }

    private static byte[] a(HttpURLConnection httpURLConnection) {
        return httpURLConnection.getErrorStream() == null ? a(httpURLConnection.getInputStream()) : b(httpURLConnection).getBytes();
    }

    private static String b(HttpURLConnection httpURLConnection) {
        String a2 = a(httpURLConnection.getErrorStream(), a(httpURLConnection.getContentType()));
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        throw new IOException(httpURLConnection.getResponseCode() + ":" + httpURLConnection.getResponseMessage());
    }
}
