package com.igexin.push.d.a;

import com.igexin.b.a.b.a.a.m;
import com.igexin.b.a.b.c;
import com.igexin.b.a.b.d;
import com.igexin.b.a.b.e;
import com.igexin.b.a.b.f;
import com.igexin.b.a.c.a;
import com.igexin.push.d.c.g;
import com.igexin.push.util.EncryptUtils;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class b extends com.igexin.b.a.b.b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1400a = b.class.getName();

    /* renamed from: b  reason: collision with root package name */
    public static int f1401b = -1;
    private byte[] g;

    b(String str) {
        super(str, true);
    }

    private byte a(m mVar) {
        return (byte) b(mVar, 1);
    }

    public static com.igexin.b.a.b.b a() {
        b bVar = new b("socketProtocol");
        new a("command", bVar);
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00ea, code lost:
        if (r13.g == 0) goto L_0x00a2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.igexin.b.a.d.a.e a(com.igexin.b.a.b.e r10, com.igexin.b.a.b.d r11, com.igexin.b.a.b.a.a.m r12, com.igexin.push.d.c.g r13) {
        /*
            r9 = this;
            r8 = 48
            r7 = 0
            byte r0 = r13.h
            if (r0 != r8) goto L_0x0008
        L_0x0007:
            return r7
        L_0x0008:
            byte r0 = r9.a(r12)
            if (r0 <= 0) goto L_0x0011
            r9.a(r12, r0)
        L_0x0011:
            byte r0 = r9.a(r12)
            r13.f = r0
            byte r0 = r9.a(r12)
            r13.o = r0
            int r0 = r13.o
            if (r0 <= 0) goto L_0x0029
            int r0 = r13.o
            byte[] r0 = r9.a(r12, r0)
            r13.n = r0
        L_0x0029:
            int r0 = r13.e
            if (r0 != 0) goto L_0x0041
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            com.igexin.push.d.c.h r1 = new com.igexin.push.d.c.h
            r1.<init>()
            r0.a(r1)
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            r0.c()
            goto L_0x0007
        L_0x0041:
            r0 = 11
            byte[] r0 = r9.a(r12, r0)
            r1 = 0
            int r1 = com.igexin.b.a.b.f.d(r0, r1)
            int r2 = com.igexin.push.d.a.b.f1401b
            if (r1 > r2) goto L_0x005b
            r0 = -1
            com.igexin.push.d.a.b.f1401b = r0
            java.lang.Exception r0 = new java.lang.Exception
            java.lang.String r1 = "server packetId can't be less than previous"
            r0.<init>(r1)
            throw r0
        L_0x005b:
            com.igexin.push.d.a.b.f1401b = r1
            r2 = 4
            int r2 = com.igexin.b.a.b.f.d(r0, r2)
            r3 = 8
            short r3 = com.igexin.b.a.b.f.b(r0, r3)
            r4 = 10
            int r4 = com.igexin.b.a.b.f.a(r0, r4)
            com.igexin.push.d.c.b r5 = new com.igexin.push.d.c.b
            r5.<init>()
            r5.f1408a = r3
            byte r0 = (byte) r4
            r5.f1409b = r0
            int r0 = r13.c
            r5.f = r0
            byte r0 = r13.h
            r5.g = r0
            if (r3 <= 0) goto L_0x00ed
            byte[] r0 = r9.a(r12, r3)
            byte r3 = r13.h
            r6 = 16
            if (r3 != r6) goto L_0x00cb
            byte[] r3 = com.igexin.b.a.b.f.b(r2)
            byte[] r3 = com.igexin.push.util.EncryptUtils.getIV(r3)
            byte[] r0 = com.igexin.push.util.EncryptUtils.aesDecSocket(r0, r3)
        L_0x0098:
            byte r3 = r13.g
            r4 = -128(0xffffffffffffff80, float:NaN)
            if (r3 != r4) goto L_0x00e8
            byte[] r0 = com.igexin.b.a.b.f.d(r0)
        L_0x00a2:
            r5.a(r0)
            byte[] r0 = r13.n
            byte[] r1 = com.igexin.push.util.EncryptUtils.getSocketSignature(r5, r1, r2)
            boolean r0 = java.util.Arrays.equals(r0, r1)
            if (r0 != 0) goto L_0x010b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.igexin.push.d.a.b.f1400a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "|decode signature error!!!!"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x0007
        L_0x00cb:
            byte r3 = r13.h
            r6 = 32
            if (r3 != r6) goto L_0x00de
            r3 = 26
            if (r4 != r3) goto L_0x0007
            byte[] r3 = com.igexin.b.a.b.f.b(r2)
            byte[] r0 = com.igexin.push.util.EncryptUtils.altAesDecSocket(r0, r3)
            goto L_0x0098
        L_0x00de:
            byte r3 = r13.h
            if (r3 == 0) goto L_0x0098
            byte r0 = r13.h
            if (r0 != r8) goto L_0x0007
            goto L_0x0007
        L_0x00e8:
            byte r3 = r13.g
            if (r3 != 0) goto L_0x0007
            goto L_0x00a2
        L_0x00ed:
            int r0 = r5.f1408a
            if (r0 >= 0) goto L_0x010b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.igexin.push.d.a.b.f1400a
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "|data len < 0, error"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x0007
        L_0x010b:
            com.igexin.b.a.b.b r0 = r9.d
            if (r0 == 0) goto L_0x011c
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            com.igexin.b.a.b.b r1 = r9.d
            java.lang.Object r1 = r1.c(r10, r11, r5)
            r0.a(r1)
        L_0x011c:
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            r0.c()
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.d.a.b.a(com.igexin.b.a.b.e, com.igexin.b.a.b.d, com.igexin.b.a.b.a.a.m, com.igexin.push.d.c.g):com.igexin.b.a.d.a.e");
    }

    static g a(com.igexin.push.d.c.b bVar) {
        g gVar = new g();
        gVar.f1416a = 1944742139;
        gVar.a(bVar.c);
        gVar.e = bVar.f1409b > 0 ? 1 : 0;
        gVar.c = 7;
        gVar.f1417b = 11;
        gVar.f = bVar.d;
        gVar.f1417b += EncryptUtils.getRSAKeyId().length;
        if (bVar.f1408a > 0) {
            gVar.p = EncryptUtils.getPacketId();
            gVar.q = (int) (System.currentTimeMillis() / 1000);
            gVar.n = EncryptUtils.getSocketSignature(bVar, gVar.p, gVar.q);
            gVar.o = gVar.n.length;
            gVar.f1417b += gVar.o;
        } else if (gVar.h == 0) {
            gVar.o = 0;
            gVar.f1417b += gVar.o;
        }
        c.d();
        return gVar;
    }

    private byte[] a(m mVar, int i) {
        byte[] bArr = new byte[i];
        mVar.a(bArr);
        return bArr;
    }

    private int b(m mVar, int i) {
        byte[] a2 = a(mVar, i);
        if (i == 1) {
            return f.a(a2, 0);
        }
        if (i == 2) {
            return f.b(a2, 0);
        }
        if (i == 4) {
            return f.d(a2, 0);
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x009d, code lost:
        if (r10.g == 0) goto L_0x0072;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.igexin.b.a.d.a.e b(com.igexin.b.a.b.e r7, com.igexin.b.a.b.d r8, com.igexin.b.a.b.a.a.m r9, com.igexin.push.d.c.g r10) {
        /*
            r6 = this;
            r5 = 48
            r4 = 0
            byte r0 = r10.h
            if (r0 != r5) goto L_0x0013
            byte r0 = r6.a(r9)
            if (r0 <= 0) goto L_0x0013
            byte[] r0 = r6.a(r9, r0)
            r6.g = r0
        L_0x0013:
            int r0 = r10.e
            if (r0 != 0) goto L_0x002b
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            com.igexin.push.d.c.h r1 = new com.igexin.push.d.c.h
            r1.<init>()
            r0.a(r1)
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            r0.c()
        L_0x002a:
            return r4
        L_0x002b:
            r0 = 3
            byte[] r0 = r6.a(r9, r0)
            r1 = 0
            short r1 = com.igexin.b.a.b.f.b(r0, r1)
            r2 = 2
            int r0 = com.igexin.b.a.b.f.a(r0, r2)
            com.igexin.push.d.c.b r2 = new com.igexin.push.d.c.b
            r2.<init>()
            r2.f1408a = r1
            byte r3 = (byte) r0
            r2.f1409b = r3
            int r3 = r10.c
            r2.f = r3
            r3 = 26
            if (r0 != r3) goto L_0x002a
            int r0 = r2.f1408a
            if (r0 <= 0) goto L_0x0075
            byte[] r1 = r6.a(r9, r1)
            byte r0 = r10.h
            if (r0 != r5) goto L_0x0095
            byte[] r0 = r6.g
            if (r0 != 0) goto L_0x008e
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            byte[] r0 = r0.a()
        L_0x0064:
            byte[] r0 = com.igexin.b.a.a.a.a(r1, r0)
        L_0x0068:
            byte r1 = r10.g
            r3 = -128(0xffffffffffffff80, float:NaN)
            if (r1 != r3) goto L_0x009b
            byte[] r0 = com.igexin.b.a.b.f.d(r0)
        L_0x0072:
            r2.a(r0)
        L_0x0075:
            com.igexin.b.a.b.b r0 = r6.d
            if (r0 == 0) goto L_0x0086
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            com.igexin.b.a.b.b r1 = r6.d
            java.lang.Object r1 = r1.c(r7, r8, r2)
            r0.a(r1)
        L_0x0086:
            com.igexin.b.a.b.c r0 = com.igexin.b.a.b.c.b()
            r0.c()
            goto L_0x002a
        L_0x008e:
            byte[] r0 = r6.g
            byte[] r0 = com.igexin.b.b.a.a(r0)
            goto L_0x0064
        L_0x0095:
            byte r0 = r10.h
            if (r0 != 0) goto L_0x0099
        L_0x0099:
            r0 = r1
            goto L_0x0068
        L_0x009b:
            byte r1 = r10.g
            if (r1 != 0) goto L_0x002a
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.d.a.b.b(com.igexin.b.a.b.e, com.igexin.b.a.b.d, com.igexin.b.a.b.a.a.m, com.igexin.push.d.c.g):com.igexin.b.a.d.a.e");
    }

    public Object a(e eVar, d dVar, Object obj) {
        int c;
        if (!(obj instanceof com.igexin.push.d.c.b)) {
            return null;
        }
        com.igexin.push.d.c.b bVar = (com.igexin.push.d.c.b) obj;
        g a2 = a(bVar);
        if (bVar.f1409b > 0 && bVar.f1408a > 0) {
            if ((a2.g & 192) == 128) {
                bVar.a(f.c(bVar.e));
            }
            if ((a2.h & 48) == 16) {
                byte[] iv = EncryptUtils.getIV(f.b(a2.q));
                if ((a2.f & 16) != 16) {
                    bVar.a(EncryptUtils.aesEncSocket(bVar.e, iv));
                }
            } else if ((a2.h & 48) != 0) {
                if ((a2.h & 48) == 48) {
                    a.b(f1400a + "|encry type = 0x30 not support");
                    return null;
                } else if ((a2.h & 48) == 32) {
                    a.b(f1400a + "|encry type = 0x20 reserved");
                } else {
                    a.b(f1400a + "|encry type = " + ((int) (a2.h & 48)) + " not support");
                    return null;
                }
            }
        }
        byte[] bArr = new byte[((bVar.f1409b > 0 ? bVar.f1408a + 11 : 0) + a2.f1417b)];
        int a3 = f.a(1944742139, bArr, 0);
        int c2 = a3 + f.c(a2.f1417b, bArr, a3);
        int c3 = c2 + f.c(a2.c, bArr, c2);
        int c4 = c3 + f.c(a2.a(), bArr, c3);
        int c5 = c4 + f.c(a2.e, bArr, c4);
        byte[] rSAKeyId = EncryptUtils.getRSAKeyId();
        int c6 = c5 + f.c(rSAKeyId.length, bArr, c5);
        int a4 = c6 + f.a(rSAKeyId, 0, bArr, c6, rSAKeyId.length);
        int c7 = a4 + f.c(a2.b(), bArr, a4);
        if (bVar.f1408a > 0) {
            int c8 = c7 + f.c(a2.o, bArr, c7);
            c = c8 + f.a(a2.n, 0, bArr, c8, a2.o);
        } else {
            c = c7 + f.c(0, bArr, c7);
        }
        if (bVar.f1409b > 0) {
            int a5 = c + f.a(a2.p, bArr, c);
            int a6 = a5 + f.a(a2.q, bArr, a5);
            int b2 = a6 + f.b(bVar.f1408a, bArr, a6);
            int c9 = b2 + f.c(bVar.f1409b, bArr, b2);
            if (bVar.f1408a > 0) {
                int a7 = c9 + f.a(bVar.e, 0, bArr, c9, bVar.f1408a);
                return bArr;
            }
        }
        return bArr;
    }

    /* renamed from: b */
    public com.igexin.b.a.d.a.e c(e eVar, d dVar, Object obj) {
        m mVar = obj instanceof m ? (m) obj : null;
        if (mVar == null) {
            a.b(f1400a + "|syncIns is null");
            return null;
        }
        byte[] a2 = a(mVar, 8);
        if (f.d(a2, 0) != 1944742139) {
            return null;
        }
        g gVar = new g();
        gVar.f1417b = a2[4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        gVar.c = a2[5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        gVar.a(a2[6]);
        gVar.e = a2[7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        if (gVar.c == 7) {
            return a(eVar, dVar, mVar, gVar);
        }
        if (gVar.c == 1) {
            return b(eVar, dVar, mVar, gVar);
        }
        a.b(f1400a + "|server socket resp version = " + gVar.c + ", not support !!!");
        return null;
    }
}
