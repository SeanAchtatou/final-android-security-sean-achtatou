package com.igexin.push.d.c;

import cn.banshenggua.aichang.utils.StringUtil;

public class f extends e {

    /* renamed from: a  reason: collision with root package name */
    String f1414a;

    /* renamed from: b  reason: collision with root package name */
    String f1415b;
    String c;
    String d;

    public f() {
        this.i = 6;
        this.j = 20;
        this.f1414a = "";
        this.f1415b = "";
        this.c = "";
        this.d = "";
    }

    public f(String str, String str2, String str3, String str4) {
        this.i = 6;
        this.j = 20;
        this.f1414a = str == null ? "" : str;
        this.f1415b = str2 == null ? "" : str2;
        this.c = str3 == null ? "" : str3;
        this.d = str4 == null ? "" : str4;
    }

    public String a() {
        return this.c;
    }

    public void a(byte[] bArr) {
        try {
            int a2 = com.igexin.b.a.b.f.a(bArr, 0);
            this.f1414a = new String(bArr, 1, a2, StringUtil.Encoding);
            int i = a2 + 1;
            int a3 = com.igexin.b.a.b.f.a(bArr, i);
            int i2 = i + 1;
            this.f1415b = new String(bArr, i2, a3, StringUtil.Encoding);
            int i3 = i2 + a3;
            int a4 = com.igexin.b.a.b.f.a(bArr, i3);
            int i4 = i3 + 1;
            this.c = new String(bArr, i4, a4, StringUtil.Encoding);
            int i5 = i4 + a4;
            int a5 = com.igexin.b.a.b.f.a(bArr, i5);
            int i6 = i5 + 1;
            this.d = new String(bArr, i6, a5, StringUtil.Encoding);
            int i7 = i6 + a5;
        } catch (Exception e) {
        }
    }

    public byte[] d() {
        byte[] bytes = this.f1415b.getBytes();
        byte[] bytes2 = this.f1414a.getBytes();
        byte[] bytes3 = this.c.getBytes();
        byte[] bytes4 = this.d.getBytes();
        byte[] bArr = new byte[(bytes.length + bytes2.length + bytes3.length + bytes4.length + 4)];
        com.igexin.b.a.b.f.c(bytes.length, bArr, 0);
        System.arraycopy(bytes, 0, bArr, 1, bytes.length);
        int length = bytes.length + 1;
        int i = length + 1;
        com.igexin.b.a.b.f.c(bytes2.length, bArr, length);
        System.arraycopy(bytes2, 0, bArr, i, bytes2.length);
        int length2 = bytes2.length + i;
        int i2 = length2 + 1;
        com.igexin.b.a.b.f.c(bytes3.length, bArr, length2);
        System.arraycopy(bytes3, 0, bArr, i2, bytes3.length);
        int length3 = bytes3.length + i2;
        int i3 = length3 + 1;
        com.igexin.b.a.b.f.c(bytes4.length, bArr, length3);
        System.arraycopy(bytes4, 0, bArr, i3, bytes4.length);
        int length4 = bytes4.length + i3;
        return bArr;
    }
}
