package com.igexin.push.d.c;

import android.text.TextUtils;
import com.igexin.b.a.b.f;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class q extends e {

    /* renamed from: a  reason: collision with root package name */
    public long f1433a;

    /* renamed from: b  reason: collision with root package name */
    public String f1434b = "";
    public String c = "";
    public String d = "";

    public q() {
        this.i = 9;
    }

    private String a(byte[] bArr, int i, int i2) {
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }

    public void a(byte[] bArr) {
        int i;
        this.f1433a = f.e(bArr, 0);
        if (bArr.length > 8) {
            i = 9;
            byte b2 = bArr[8] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (b2 > 0) {
                this.f1434b = a(bArr, 9, b2);
                i = b2 + 9;
            }
        } else {
            i = 8;
        }
        if (bArr.length > i) {
            int i2 = i + 1;
            byte b3 = bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (b3 > 0) {
                this.c = a(bArr, i2, b3);
                i = b3 + i2;
            } else {
                i = i2;
            }
        }
        if (bArr.length > i) {
            int i3 = i + 1;
            byte b4 = bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (b4 > 0) {
                this.d = a(bArr, i3, b4);
                int i4 = b4 + i3;
            }
        }
    }

    public byte[] d() {
        if (TextUtils.isEmpty(this.c) || TextUtils.isEmpty(this.d)) {
            byte[] bytes = this.f1434b.getBytes();
            byte[] bArr = new byte[(bytes.length + 8 + 1)];
            f.a(this.f1433a, bArr, 0);
            f.c(bytes.length, bArr, 8);
            System.arraycopy(bytes, 0, bArr, 9, bytes.length);
            return bArr;
        }
        byte[] bytes2 = this.f1434b.getBytes();
        byte[] bytes3 = this.c.getBytes();
        byte[] bytes4 = this.d.getBytes();
        byte[] bArr2 = new byte[(bytes2.length + 8 + bytes3.length + bytes4.length + 3)];
        f.a(this.f1433a, bArr2, 0);
        f.c(bytes2.length, bArr2, 8);
        System.arraycopy(bytes2, 0, bArr2, 9, bytes2.length);
        int length = bytes2.length + 9;
        int i = length + 1;
        f.c(bytes3.length, bArr2, length);
        System.arraycopy(bytes3, 0, bArr2, i, bytes3.length);
        int length2 = bytes3.length + i;
        f.c(bytes4.length, bArr2, length2);
        System.arraycopy(bytes4, 0, bArr2, length2 + 1, bytes4.length);
        return bArr2;
    }
}
