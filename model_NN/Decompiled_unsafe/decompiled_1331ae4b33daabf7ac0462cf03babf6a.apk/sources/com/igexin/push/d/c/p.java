package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class p extends e {
    private static final String m = p.class.getName();

    /* renamed from: a  reason: collision with root package name */
    public int f1431a;

    /* renamed from: b  reason: collision with root package name */
    public int f1432b;
    public long c;
    public String d;
    public Object e;
    public Object f;
    public String g;
    public String h = "UTF-8";

    public p() {
        this.i = 26;
    }

    public void a(byte[] bArr) {
        int i;
        int i2;
        this.f1431a = f.c(bArr, 0);
        this.f1432b = bArr[2] & 192;
        this.h = a(bArr[2]);
        this.c = f.e(bArr, 3);
        byte b2 = bArr[11] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
        try {
            this.d = new String(bArr, 12, b2, this.h);
        } catch (Exception e2) {
            this.d = "";
        }
        int i3 = b2 + 12;
        int i4 = 0;
        while (true) {
            i = i4 | (bArr[i3] & Byte.MAX_VALUE);
            if ((bArr[i3] & 128) == 0) {
                break;
            }
            i4 = i << 7;
            i3++;
        }
        int i5 = i3 + 1;
        if (i > 0) {
            if (this.f1432b == 192) {
                this.e = new byte[i];
                System.arraycopy(bArr, i5, this.e, 0, i);
            } else {
                try {
                    this.e = new String(bArr, i5, i, this.h);
                } catch (Exception e3) {
                }
            }
        }
        int i6 = i + i5;
        int i7 = 0;
        while (true) {
            i2 = i7 | (bArr[i6] & Byte.MAX_VALUE);
            if ((bArr[i6] & 128) == 0) {
                break;
            }
            i7 = i2 << 7;
            i6++;
        }
        int i8 = i6 + 1;
        if (i2 > 0) {
            this.f = new byte[i2];
            System.arraycopy(bArr, i8, this.f, 0, i2);
        }
        int i9 = i2 + i8;
        if (bArr.length > i9) {
            int i10 = i9 + 1;
            byte b3 = bArr[i9] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            try {
                this.g = new String(bArr, i10, b3, this.h);
            } catch (Exception e4) {
            }
            int i11 = b3 + i10;
        }
    }

    public final boolean a() {
        return this.f1432b == 128;
    }

    public byte[] d() {
        int i = 0;
        try {
            byte[] bytes = this.d.getBytes(this.h);
            byte[] bytes2 = this.g.getBytes(this.h);
            byte[] bytes3 = !"".equals(this.e) ? this.f1432b == 192 ? (byte[]) this.e : ((String) this.e).getBytes(this.h) : null;
            byte[] bArr = this.f != null ? (byte[]) this.f : null;
            int length = bytes3 == null ? 0 : bytes3.length;
            if (bArr != null) {
                i = bArr.length;
            }
            byte[] a2 = f.a(length);
            byte[] a3 = f.a(i);
            byte[] bArr2 = new byte[(bytes.length + 13 + a2.length + length + a3.length + i + bytes2.length)];
            try {
                int b2 = f.b(this.f1431a, bArr2, 0);
                int c2 = b2 + f.c(this.f1432b | a(this.h), bArr2, b2);
                int a4 = c2 + f.a(this.c, bArr2, c2);
                int c3 = a4 + f.c(bytes.length, bArr2, a4);
                int a5 = c3 + f.a(bytes, 0, bArr2, c3, bytes.length);
                int a6 = a5 + f.a(a2, 0, bArr2, a5, a2.length);
                if (length > 0) {
                    a6 += f.a(bytes3, 0, bArr2, a6, length);
                }
                int a7 = a6 + f.a(a3, 0, bArr2, a6, a3.length);
                if (i > 0) {
                    a7 += f.a(bArr, 0, bArr2, a7, i);
                }
                int c4 = a7 + f.c(bytes2.length, bArr2, a7);
                int a8 = c4 + f.a(bytes2, 0, bArr2, c4, bytes2.length);
                return bArr2;
            } catch (Exception e2) {
                return bArr2;
            }
        } catch (Exception e3) {
            return null;
        }
    }
}
