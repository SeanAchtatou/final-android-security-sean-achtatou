package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import com.igexin.push.util.EncryptUtils;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class i extends e {

    /* renamed from: a  reason: collision with root package name */
    public String f1418a;

    /* renamed from: b  reason: collision with root package name */
    public byte[] f1419b;
    public byte c;
    public String d;

    public i() {
        this.i = 96;
        this.j = 4;
        this.k = (byte) (this.k | 16);
    }

    private String a(byte[] bArr, int i, int i2) {
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (Exception e) {
            return "";
        }
    }

    public void a(byte[] bArr) {
        try {
            this.c = bArr[0];
            byte b2 = bArr[1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            this.f1418a = a(bArr, 2, b2);
            int i = b2 + 2;
            int i2 = i + 1;
            int i3 = bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            this.f1419b = new byte[i3];
            System.arraycopy(bArr, i2, this.f1419b, 0, i3);
            int i4 = i3 + i2;
            this.d = a(bArr, i4 + 1, bArr[i4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        } catch (Exception e) {
        }
    }

    public byte[] d() {
        byte[] bytes = this.f1418a.getBytes();
        byte[] iv = EncryptUtils.getIV(f.b((int) (System.currentTimeMillis() / 1000)));
        byte[] socketAESKey = EncryptUtils.getSocketAESKey();
        byte[] bArr = new byte[(bytes.length + 2 + 2 + socketAESKey.length + 1 + iv.length)];
        int c2 = f.c(0, bArr, 0);
        int c3 = c2 + f.c((byte) bytes.length, bArr, c2);
        int a2 = f.a(bytes, 0, bArr, c3, bytes.length) + c3;
        int b2 = a2 + f.b((short) socketAESKey.length, bArr, a2);
        int a3 = b2 + f.a(socketAESKey, 0, bArr, b2, socketAESKey.length);
        int c4 = a3 + f.c((byte) iv.length, bArr, a3);
        int a4 = c4 + f.a(iv, 0, bArr, c4, iv.length);
        return bArr;
    }
}
