package com.igexin.push.d;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.igexin.b.a.b.a.a.k;
import com.igexin.b.a.b.b;
import com.igexin.b.a.b.e;

public class a implements com.igexin.b.a.d.a.a<String, Integer, b, e> {

    /* renamed from: a  reason: collision with root package name */
    public ConnectivityManager f1398a;

    /* renamed from: b  reason: collision with root package name */
    public Context f1399b;

    public a(Context context, ConnectivityManager connectivityManager) {
        this.f1398a = connectivityManager;
        this.f1399b = context;
    }

    public e a(String str, Integer num, b bVar) {
        NetworkInfo activeNetworkInfo;
        if (!str.startsWith("socket") || this.f1398a == null || (activeNetworkInfo = this.f1398a.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) {
            return null;
        }
        return new k(str, bVar);
    }
}
