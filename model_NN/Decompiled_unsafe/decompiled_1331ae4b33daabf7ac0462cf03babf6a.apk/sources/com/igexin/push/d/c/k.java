package com.igexin.push.d.c;

import com.igexin.b.a.b.f;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class k extends e {

    /* renamed from: a  reason: collision with root package name */
    public long f1422a;

    /* renamed from: b  reason: collision with root package name */
    public byte f1423b;
    public int c;
    public String d;
    public List<l> e;

    public k() {
        this.i = 4;
        this.j = 20;
    }

    private String a(byte[] bArr, int i, int i2) {
        try {
            return new String(bArr, i, i2, "UTF-8");
        } catch (Exception e2) {
            return "";
        }
    }

    public void a(byte[] bArr) {
        int i;
        this.f1422a = f.e(bArr, 0);
        this.f1423b = bArr[8];
        this.c = f.d(bArr, 9) & -1;
        if (bArr.length > 13) {
            i = 14;
            byte b2 = bArr[13] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD;
            if (b2 > 0) {
                this.e = new ArrayList();
                int i2 = b2 + 14;
                while (i < i2) {
                    l lVar = new l();
                    this.e.add(lVar);
                    int i3 = i + 1;
                    int a2 = f.a(bArr, i) & 255;
                    int i4 = i3 + 1;
                    int a3 = f.a(bArr, i3) & 255;
                    lVar.f1424a = (byte) a2;
                    if ((a2 == 1 || a2 == 4) && a3 > 0) {
                        try {
                            lVar.f1425b = new String(bArr, i4, a3, "UTF-8");
                        } catch (Exception e2) {
                        }
                    }
                    i = i4 + a3;
                }
            }
        } else {
            i = 13;
        }
        if (bArr.length > i) {
            this.d = a(bArr, i + 1, bArr[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD);
        }
    }

    public byte[] d() {
        byte[] bArr;
        int i;
        int i2;
        byte[] bArr2;
        byte[] bArr3 = null;
        if (this.e == null || this.e.size() <= 0) {
            bArr = null;
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            for (l d2 : this.e) {
                try {
                    byteArrayOutputStream.write(d2.d());
                    bArr2 = byteArrayOutputStream.toByteArray();
                } catch (IOException e2) {
                    bArr2 = bArr3;
                }
                bArr3 = bArr2;
            }
            try {
                byteArrayOutputStream.close();
                bArr = bArr3;
            } catch (IOException e3) {
                bArr = bArr3;
            }
        }
        if (bArr != null) {
            i2 = bArr.length;
            i = i2 + 1;
        } else {
            i = 1;
            i2 = 0;
        }
        byte[] bArr4 = new byte[(i + 12 + this.d.getBytes().length + 1)];
        int a2 = f.a(this.f1422a, bArr4, 0);
        int a3 = a2 + f.a(((this.f1423b & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | this.c, bArr4, a2);
        int c2 = a3 + f.c(i2, bArr4, a3);
        int a4 = i2 > 0 ? f.a(bArr, 0, bArr4, c2, i2) + c2 : c2;
        byte[] bytes = this.d.getBytes();
        int i3 = a4 + 1;
        f.c(bytes.length, bArr4, a4);
        System.arraycopy(bytes, 0, bArr4, i3, bytes.length);
        int length = bytes.length + i3;
        return bArr4;
    }
}
