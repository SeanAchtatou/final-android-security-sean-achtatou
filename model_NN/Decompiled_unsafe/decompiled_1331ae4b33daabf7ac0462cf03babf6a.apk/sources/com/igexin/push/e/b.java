package com.igexin.push.e;

import android.content.ServiceConnection;
import com.igexin.sdk.aidl.IGexinMsgService;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f1439a;

    /* renamed from: b  reason: collision with root package name */
    private String f1440b;
    private IGexinMsgService c;
    private ServiceConnection d;
    private String e;

    public String a() {
        return this.f1439a;
    }

    public void a(ServiceConnection serviceConnection) {
        this.d = serviceConnection;
    }

    public void a(IGexinMsgService iGexinMsgService) {
        this.c = iGexinMsgService;
    }

    public void a(String str) {
        this.f1439a = str;
    }

    public String b() {
        return this.f1440b;
    }

    public void b(String str) {
        this.f1440b = str;
    }

    public IGexinMsgService c() {
        return this.c;
    }

    public void c(String str) {
        this.e = str;
    }

    public ServiceConnection d() {
        return this.d;
    }

    public String e() {
        return this.e;
    }
}
