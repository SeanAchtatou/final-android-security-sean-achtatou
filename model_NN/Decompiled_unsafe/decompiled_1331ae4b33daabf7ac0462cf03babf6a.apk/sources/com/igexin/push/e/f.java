package com.igexin.push.e;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.igexin.push.core.c;
import com.igexin.push.core.d;
import com.igexin.sdk.aidl.a;

class f implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1445a;

    f(c cVar) {
        this.f1445a = cVar;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (this.f1445a.f1442b == d.prepare) {
            this.f1445a.e.a(a.a(iBinder));
            a aVar = new a();
            aVar.a(c.connectASNL);
            this.f1445a.a(aVar);
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (this.f1445a.f1442b == d.passive) {
            com.igexin.push.core.f.a().g().b(true);
            this.f1445a.c();
        }
    }
}
