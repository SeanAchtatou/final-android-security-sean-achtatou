package com.igexin.push.f.b;

import com.igexin.b.a.b.c;
import com.igexin.push.c.i;
import com.igexin.push.core.a.e;
import com.igexin.push.core.f;
import com.igexin.push.util.a;
import java.util.concurrent.TimeUnit;

public class g extends h {

    /* renamed from: a  reason: collision with root package name */
    private static g f1473a;

    private g() {
        super(3600000);
        this.o = true;
    }

    public static synchronized g g() {
        g gVar;
        synchronized (g.class) {
            if (f1473a == null) {
                f1473a = new g();
            }
            gVar = f1473a;
        }
        return gVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        e.a().A();
        boolean a2 = a.a(System.currentTimeMillis());
        boolean b2 = a.b();
        com.igexin.b.a.c.a.b("ReconnectTimerTask|networkAvailable = " + com.igexin.push.core.g.h + "|,sdkOnline = " + com.igexin.push.core.g.l + ", " + "sdkOn= " + com.igexin.push.core.g.i + ", pushOn =" + com.igexin.push.core.g.j + ", isSilentTime= " + a2 + ", blockEndTime= " + b2);
        if (!com.igexin.push.core.g.h || !com.igexin.push.core.g.i || !com.igexin.push.core.g.j || com.igexin.push.core.g.l || a2 || !b2) {
            com.igexin.b.a.c.a.b("ReconnectTimerTask reconnect timer task stop, connect interval= 1h #######");
            a(3600000, TimeUnit.MILLISECONDS);
            return;
        }
        com.igexin.b.a.c.a.b("ReconnectTimerTask reconnect timer task isOnline = false, try login...");
        int d = f.a().i().d();
        if (d != 1 && d == 0) {
            c.b().a(new com.igexin.push.d.b.a());
            c.b().c();
        }
        a(1800000, TimeUnit.MILLISECONDS);
    }

    public final int b() {
        return -2147483641;
    }

    public void c() {
        super.c();
    }

    public void d() {
    }

    public void h() {
        long j = com.igexin.push.core.g.E;
        com.igexin.b.a.c.a.b("ReconnectTimerTask|refreshDelayTime, delay = " + j);
        if (!i.a().e().j()) {
            com.igexin.b.a.c.a.b("ReconnectTimerTask|refreshDelayTime, already enter backup or trynormal #####");
            a(j, TimeUnit.MILLISECONDS);
        } else if (i.a().g() != null) {
            a(j, TimeUnit.MILLISECONDS);
        } else {
            com.igexin.b.a.c.a.b("ReconnectTimerTask|detect ip = null and not enter backup,  wait for detect, delay = 3600000");
            a(3600000, TimeUnit.MILLISECONDS);
        }
    }
}
