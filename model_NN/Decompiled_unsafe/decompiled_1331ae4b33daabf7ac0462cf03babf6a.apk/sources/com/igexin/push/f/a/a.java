package com.igexin.push.f.a;

import android.os.Process;
import com.igexin.b.a.b.c;
import com.igexin.b.a.d.d;
import com.igexin.push.config.m;
import java.net.HttpURLConnection;

public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1458a = a.class.getName();

    /* renamed from: b  reason: collision with root package name */
    public b f1459b;
    private HttpURLConnection c;

    public a(b bVar) {
        super(0);
        this.f1459b = bVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x0084 A[SYNTHETIC, Splitter:B:41:0x0084] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0089 A[SYNTHETIC, Splitter:B:44:0x0089] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(java.lang.String r7) {
        /*
            r6 = this;
            r1 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            r6.c = r0     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            r2 = 20000(0x4e20, float:2.8026E-41)
            r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            r2 = 20000(0x4e20, float:2.8026E-41)
            r0.setReadTimeout(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.lang.String r2 = "GET"
            r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            r2 = 1
            r0.setDoInput(r2)     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x00a5, all -> 0x0080 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x00a9, all -> 0x00a0 }
            r2.<init>()     // Catch:{ Exception -> 0x00a9, all -> 0x00a0 }
            java.net.HttpURLConnection r0 = r6.c     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r0 != r4) goto L_0x0072
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
        L_0x0042:
            int r4 = r3.read(r0)     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
            r5 = -1
            if (r4 == r5) goto L_0x0060
            r5 = 0
            r2.write(r0, r5, r4)     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
            goto L_0x0042
        L_0x004e:
            r0 = move-exception
            r0 = r2
            r2 = r3
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ Exception -> 0x0098 }
        L_0x0056:
            if (r0 == 0) goto L_0x005b
            r0.close()     // Catch:{ Exception -> 0x009a }
        L_0x005b:
            r6.g()
        L_0x005e:
            r0 = r1
        L_0x005f:
            return r0
        L_0x0060:
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x004e, all -> 0x00a2 }
            if (r3 == 0) goto L_0x0069
            r3.close()     // Catch:{ Exception -> 0x0090 }
        L_0x0069:
            if (r2 == 0) goto L_0x006e
            r2.close()     // Catch:{ Exception -> 0x0092 }
        L_0x006e:
            r6.g()
            goto L_0x005f
        L_0x0072:
            if (r3 == 0) goto L_0x0077
            r3.close()     // Catch:{ Exception -> 0x0094 }
        L_0x0077:
            if (r2 == 0) goto L_0x007c
            r2.close()     // Catch:{ Exception -> 0x0096 }
        L_0x007c:
            r6.g()
            goto L_0x005e
        L_0x0080:
            r0 = move-exception
            r3 = r1
        L_0x0082:
            if (r3 == 0) goto L_0x0087
            r3.close()     // Catch:{ Exception -> 0x009c }
        L_0x0087:
            if (r1 == 0) goto L_0x008c
            r1.close()     // Catch:{ Exception -> 0x009e }
        L_0x008c:
            r6.g()
            throw r0
        L_0x0090:
            r1 = move-exception
            goto L_0x0069
        L_0x0092:
            r1 = move-exception
            goto L_0x006e
        L_0x0094:
            r0 = move-exception
            goto L_0x0077
        L_0x0096:
            r0 = move-exception
            goto L_0x007c
        L_0x0098:
            r2 = move-exception
            goto L_0x0056
        L_0x009a:
            r0 = move-exception
            goto L_0x005b
        L_0x009c:
            r2 = move-exception
            goto L_0x0087
        L_0x009e:
            r1 = move-exception
            goto L_0x008c
        L_0x00a0:
            r0 = move-exception
            goto L_0x0082
        L_0x00a2:
            r0 = move-exception
            r1 = r2
            goto L_0x0082
        L_0x00a5:
            r0 = move-exception
            r0 = r1
            r2 = r1
            goto L_0x0051
        L_0x00a9:
            r0 = move-exception
            r0 = r1
            r2 = r3
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.f.a.a.a(java.lang.String):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0089 A[SYNTHETIC, Splitter:B:19:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x008e A[SYNTHETIC, Splitter:B:22:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0093 A[SYNTHETIC, Splitter:B:25:0x0093] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ca A[SYNTHETIC, Splitter:B:54:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00cf A[SYNTHETIC, Splitter:B:57:0x00cf] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00d4 A[SYNTHETIC, Splitter:B:60:0x00d4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(java.lang.String r8, byte[] r9) {
        /*
            r7 = this;
            r1 = 0
            r2 = 0
            r3 = 0
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r0.<init>(r8)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r7.c = r0     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 1
            r0.setDoInput(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 1
            r0.setDoOutput(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.lang.String r4 = "POST"
            r0.setRequestMethod(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 0
            r0.setUseCaches(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 1
            r0.setInstanceFollowRedirects(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/octet-stream"
            r0.setRequestProperty(r4, r5)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 20000(0x4e20, float:2.8026E-41)
            r0.setConnectTimeout(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4 = 20000(0x4e20, float:2.8026E-41)
            r0.setReadTimeout(r4)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r0.connect()     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.io.DataOutputStream r4 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x00fb, all -> 0x00c5 }
            r0 = 0
            int r5 = r9.length     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            r4.write(r9, r0, r5)     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            r4.flush()     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            int r0 = r0.getResponseCode()     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r0 != r5) goto L_0x00b2
            java.net.HttpURLConnection r0 = r7.c     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ Exception -> 0x0100, all -> 0x00f3 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0105, all -> 0x00f6 }
            r2.<init>()     // Catch:{ Exception -> 0x0105, all -> 0x00f6 }
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0083, all -> 0x00f8 }
        L_0x0077:
            int r5 = r3.read(r0)     // Catch:{ Exception -> 0x0083, all -> 0x00f8 }
            r6 = -1
            if (r5 == r6) goto L_0x009b
            r6 = 0
            r2.write(r0, r6, r5)     // Catch:{ Exception -> 0x0083, all -> 0x00f8 }
            goto L_0x0077
        L_0x0083:
            r0 = move-exception
            r0 = r2
            r2 = r3
            r3 = r4
        L_0x0087:
            if (r3 == 0) goto L_0x008c
            r3.close()     // Catch:{ Exception -> 0x00e7 }
        L_0x008c:
            if (r2 == 0) goto L_0x0091
            r2.close()     // Catch:{ Exception -> 0x00e9 }
        L_0x0091:
            if (r0 == 0) goto L_0x0096
            r0.close()     // Catch:{ Exception -> 0x00eb }
        L_0x0096:
            r7.g()
        L_0x0099:
            r0 = r1
        L_0x009a:
            return r0
        L_0x009b:
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x0083, all -> 0x00f8 }
            if (r4 == 0) goto L_0x00a4
            r4.close()     // Catch:{ Exception -> 0x00db }
        L_0x00a4:
            if (r3 == 0) goto L_0x00a9
            r3.close()     // Catch:{ Exception -> 0x00dd }
        L_0x00a9:
            if (r2 == 0) goto L_0x00ae
            r2.close()     // Catch:{ Exception -> 0x00df }
        L_0x00ae:
            r7.g()
            goto L_0x009a
        L_0x00b2:
            if (r4 == 0) goto L_0x00b7
            r4.close()     // Catch:{ Exception -> 0x00e1 }
        L_0x00b7:
            if (r1 == 0) goto L_0x00bc
            r2.close()     // Catch:{ Exception -> 0x00e3 }
        L_0x00bc:
            if (r1 == 0) goto L_0x00c1
            r3.close()     // Catch:{ Exception -> 0x00e5 }
        L_0x00c1:
            r7.g()
            goto L_0x0099
        L_0x00c5:
            r0 = move-exception
            r3 = r1
            r4 = r1
        L_0x00c8:
            if (r4 == 0) goto L_0x00cd
            r4.close()     // Catch:{ Exception -> 0x00ed }
        L_0x00cd:
            if (r3 == 0) goto L_0x00d2
            r3.close()     // Catch:{ Exception -> 0x00ef }
        L_0x00d2:
            if (r1 == 0) goto L_0x00d7
            r1.close()     // Catch:{ Exception -> 0x00f1 }
        L_0x00d7:
            r7.g()
            throw r0
        L_0x00db:
            r1 = move-exception
            goto L_0x00a4
        L_0x00dd:
            r1 = move-exception
            goto L_0x00a9
        L_0x00df:
            r1 = move-exception
            goto L_0x00ae
        L_0x00e1:
            r0 = move-exception
            goto L_0x00b7
        L_0x00e3:
            r0 = move-exception
            goto L_0x00bc
        L_0x00e5:
            r0 = move-exception
            goto L_0x00c1
        L_0x00e7:
            r3 = move-exception
            goto L_0x008c
        L_0x00e9:
            r2 = move-exception
            goto L_0x0091
        L_0x00eb:
            r0 = move-exception
            goto L_0x0096
        L_0x00ed:
            r2 = move-exception
            goto L_0x00cd
        L_0x00ef:
            r2 = move-exception
            goto L_0x00d2
        L_0x00f1:
            r1 = move-exception
            goto L_0x00d7
        L_0x00f3:
            r0 = move-exception
            r3 = r1
            goto L_0x00c8
        L_0x00f6:
            r0 = move-exception
            goto L_0x00c8
        L_0x00f8:
            r0 = move-exception
            r1 = r2
            goto L_0x00c8
        L_0x00fb:
            r0 = move-exception
            r0 = r1
            r2 = r1
            r3 = r1
            goto L_0x0087
        L_0x0100:
            r0 = move-exception
            r0 = r1
            r2 = r1
            r3 = r4
            goto L_0x0087
        L_0x0105:
            r0 = move-exception
            r0 = r1
            r2 = r3
            r3 = r4
            goto L_0x0087
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.f.a.a.a(java.lang.String, byte[]):byte[]");
    }

    private void g() {
        if (this.c != null) {
            try {
                this.c.disconnect();
                this.c = null;
            } catch (Exception e) {
            }
        }
    }

    public final void a_() {
        super.a_();
        Process.setThreadPriority(10);
        if (this.f1459b == null || this.f1459b.f1460b == null || (this.f1459b.c != null && this.f1459b.c.length > m.K * 1024)) {
            p();
            com.igexin.b.a.c.a.b(f1458a + "|run return ###");
            return;
        }
        try {
            byte[] a2 = this.f1459b.c == null ? a(this.f1459b.f1460b) : a(this.f1459b.f1460b, this.f1459b.c);
            if (a2 != null) {
                try {
                    this.f1459b.a(a2);
                    c.b().a(this.f1459b);
                    c.b().c();
                } catch (Exception e) {
                    this.f1459b.a(e);
                    throw e;
                }
            } else {
                Exception exc = new Exception("Http response ＝＝ null");
                this.f1459b.a(exc);
                throw exc;
            }
        } catch (Exception e2) {
            this.f1459b.a(e2);
            throw e2;
        }
    }

    public final int b() {
        return -2147483639;
    }

    public void d() {
        this.n = true;
    }

    /* access modifiers changed from: protected */
    public void e() {
    }

    public void f() {
        super.f();
        g();
    }
}
