package com.igexin.push.f.b;

import com.igexin.b.a.b.c;
import com.igexin.push.core.a.e;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class a extends h {

    /* renamed from: b  reason: collision with root package name */
    private static a f1465b;

    /* renamed from: a  reason: collision with root package name */
    private List<d> f1466a = new ArrayList();

    private a() {
        super(360000);
        this.o = true;
    }

    public static a g() {
        if (f1465b == null) {
            f1465b = new a();
        }
        return f1465b;
    }

    private void h() {
        a(360000, TimeUnit.MILLISECONDS);
    }

    /* access modifiers changed from: protected */
    public void a() {
        e.a().A();
        for (d next : this.f1466a) {
            if (next.b()) {
                next.a();
                next.a(System.currentTimeMillis());
            }
        }
        h();
        c.b().a(this);
    }

    public boolean a(d dVar) {
        return this.f1466a != null && !this.f1466a.contains(dVar) && this.f1466a.add(dVar);
    }

    public int b() {
        return 0;
    }
}
