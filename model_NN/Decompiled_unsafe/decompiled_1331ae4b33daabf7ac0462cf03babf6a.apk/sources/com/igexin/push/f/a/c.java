package com.igexin.push.f.a;

import android.os.Process;
import com.igexin.b.a.b.f;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.d;
import com.igexin.push.config.m;
import com.igexin.push.util.EncryptUtils;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class c extends d {

    /* renamed from: a  reason: collision with root package name */
    public b f1461a;

    /* renamed from: b  reason: collision with root package name */
    private HttpURLConnection f1462b;

    public c(b bVar) {
        super(0);
        this.f1461a = bVar;
    }

    private d a(String str) {
        try {
            this.f1462b = b(str);
            byte[] a2 = a(this.f1462b);
            if (a2 != null) {
                return b(this.f1462b, a2);
            }
            g();
            return new d(this, false, null);
        } catch (Throwable th) {
        } finally {
            g();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0062 A[SYNTHETIC, Splitter:B:28:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x006c A[SYNTHETIC, Splitter:B:33:0x006c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.igexin.push.f.a.d a(java.lang.String r7, byte[] r8) {
        /*
            r6 = this;
            r5 = 0
            r1 = 0
            r2 = 0
            java.net.HttpURLConnection r0 = r6.b(r7, r8)     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            r6.f1462b = r0     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            java.net.HttpURLConnection r0 = r6.f1462b     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            byte[] r0 = r6.a(r8, r0)     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            if (r0 != 0) goto L_0x0021
            com.igexin.push.f.a.d r0 = new com.igexin.push.f.a.d     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            r3 = 1
            r4 = 0
            r0.<init>(r6, r3, r4)     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            if (r1 == 0) goto L_0x001d
            r2.close()     // Catch:{ Exception -> 0x0073 }
        L_0x001d:
            r6.g()
        L_0x0020:
            return r0
        L_0x0021:
            java.net.HttpURLConnection r2 = r6.f1462b     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            r2.connect()     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            java.net.HttpURLConnection r3 = r6.f1462b     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            java.io.OutputStream r3 = r3.getOutputStream()     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            r2.<init>(r3)     // Catch:{ Throwable -> 0x005e, all -> 0x0069 }
            r3 = 0
            int r4 = r0.length     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            r2.write(r0, r3, r4)     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            r2.flush()     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            java.net.HttpURLConnection r0 = r6.f1462b     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            byte[] r0 = r6.a(r0)     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            if (r0 == 0) goto L_0x0050
            java.net.HttpURLConnection r3 = r6.f1462b     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            com.igexin.push.f.a.d r0 = r6.b(r3, r0)     // Catch:{ Throwable -> 0x0080, all -> 0x007d }
            if (r2 == 0) goto L_0x004c
            r2.close()     // Catch:{ Exception -> 0x0075 }
        L_0x004c:
            r6.g()
            goto L_0x0020
        L_0x0050:
            if (r2 == 0) goto L_0x0055
            r2.close()     // Catch:{ Exception -> 0x0077 }
        L_0x0055:
            r6.g()
        L_0x0058:
            com.igexin.push.f.a.d r0 = new com.igexin.push.f.a.d
            r0.<init>(r6, r5, r1)
            goto L_0x0020
        L_0x005e:
            r0 = move-exception
            r0 = r1
        L_0x0060:
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ Exception -> 0x0079 }
        L_0x0065:
            r6.g()
            goto L_0x0058
        L_0x0069:
            r0 = move-exception
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()     // Catch:{ Exception -> 0x007b }
        L_0x006f:
            r6.g()
            throw r0
        L_0x0073:
            r1 = move-exception
            goto L_0x001d
        L_0x0075:
            r1 = move-exception
            goto L_0x004c
        L_0x0077:
            r0 = move-exception
            goto L_0x0055
        L_0x0079:
            r0 = move-exception
            goto L_0x0065
        L_0x007b:
            r1 = move-exception
            goto L_0x006f
        L_0x007d:
            r0 = move-exception
            r1 = r2
            goto L_0x006a
        L_0x0080:
            r0 = move-exception
            r0 = r2
            goto L_0x0060
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.f.a.c.a(java.lang.String, byte[]):com.igexin.push.f.a.d");
    }

    private void a(HttpURLConnection httpURLConnection, byte[] bArr) {
        if (httpURLConnection != null) {
            byte[] bArr2 = new byte[0];
            if (bArr == null) {
                bArr = bArr2;
            }
            httpURLConnection.addRequestProperty("GT_C_T", String.valueOf(1));
            httpURLConnection.addRequestProperty("GT_C_K", new String(EncryptUtils.getRSAKeyId()));
            httpURLConnection.addRequestProperty("GT_C_V", EncryptUtils.getHttpGTCV());
            String valueOf = String.valueOf(System.currentTimeMillis());
            String httpSignature = EncryptUtils.getHttpSignature(valueOf, bArr);
            httpURLConnection.addRequestProperty("GT_T", valueOf);
            httpURLConnection.addRequestProperty("GT_C_S", httpSignature);
        }
    }

    private void a(byte[] bArr) {
        this.f1461a.a(bArr);
        com.igexin.b.a.b.c.b().a(this.f1461a);
        com.igexin.b.a.b.c.b().c();
    }

    private byte[] a(HttpURLConnection httpURLConnection) {
        InputStream inputStream;
        Exception e;
        Throwable th;
        byte[] bArr = null;
        try {
            inputStream = httpURLConnection.getInputStream();
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                if (httpURLConnection.getResponseCode() == 200) {
                    byte[] bArr2 = new byte[1024];
                    while (true) {
                        int read = inputStream.read(bArr2);
                        if (read == -1) {
                            break;
                        }
                        byteArrayOutputStream.write(bArr2, 0, read);
                    }
                    bArr = byteArrayOutputStream.toByteArray();
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (Exception e2) {
                        }
                    }
                } else if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (Exception e3) {
                    }
                }
                return bArr;
            } catch (Exception e4) {
                e = e4;
            }
        } catch (Exception e5) {
            Exception exc = e5;
            inputStream = null;
            e = exc;
        } catch (Throwable th2) {
            Throwable th3 = th2;
            inputStream = null;
            th = th3;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e6) {
                }
            }
            throw th;
        }
        try {
            throw e;
        } catch (Throwable th4) {
            th = th4;
        }
    }

    private byte[] a(byte[] bArr, HttpURLConnection httpURLConnection) {
        String requestProperty;
        try {
            if (httpURLConnection.getRequestProperties().containsKey("GT_C_S") && (requestProperty = httpURLConnection.getRequestProperty("GT_C_S")) != null) {
                return EncryptUtils.aesEncHttp(bArr, EncryptUtils.md5(requestProperty.getBytes()));
            }
        } catch (Throwable th) {
            a.b("_HttpTask|" + th.toString());
        }
        return null;
    }

    private d b(HttpURLConnection httpURLConnection, byte[] bArr) {
        try {
            String headerField = httpURLConnection.getHeaderField("GT_ERR");
            a.b("_HttpTask|GT_ERR = " + headerField);
            if (headerField == null || !headerField.equals("0")) {
                return new d(this, true, null);
            }
            String headerField2 = httpURLConnection.getHeaderField("GT_T");
            if (headerField2 == null) {
                a.b("_HttpTask|GT_T = null");
                return new d(this, true, null);
            }
            String headerField3 = httpURLConnection.getHeaderField("GT_C_S");
            if (headerField3 == null) {
                a.b("_HttpTask|GT_C_S = null");
                return new d(this, true, null);
            }
            byte[] aesDecHttp = EncryptUtils.aesDecHttp(bArr, EncryptUtils.md5(headerField2.getBytes()));
            String httpSignature = EncryptUtils.getHttpSignature(headerField2, aesDecHttp);
            if (httpSignature != null && httpSignature.equals(headerField3)) {
                return new d(this, false, aesDecHttp);
            }
            a.b("_HttpTask|signature = null or error");
            return new d(this, true, null);
        } catch (Throwable th) {
            a.b("_HttpTask|" + th.toString());
            return new d(this, true, null);
        }
    }

    private HttpURLConnection b(String str) {
        this.f1462b = (HttpURLConnection) new URL(str).openConnection();
        this.f1462b.setConnectTimeout(20000);
        this.f1462b.setReadTimeout(20000);
        this.f1462b.setRequestMethod("GET");
        this.f1462b.setDoInput(true);
        a(this.f1462b, (byte[]) null);
        return this.f1462b;
    }

    private HttpURLConnection b(String str, byte[] bArr) {
        this.f1462b = (HttpURLConnection) new URL(str).openConnection();
        this.f1462b.setDoInput(true);
        this.f1462b.setDoOutput(true);
        this.f1462b.setRequestMethod("POST");
        this.f1462b.setUseCaches(false);
        this.f1462b.setInstanceFollowRedirects(true);
        this.f1462b.setRequestProperty("Content-Type", "application/octet-stream");
        this.f1462b.setConnectTimeout(20000);
        this.f1462b.setReadTimeout(20000);
        a(this.f1462b, bArr);
        return this.f1462b;
    }

    private void g() {
        if (this.f1462b != null) {
            try {
                this.f1462b.disconnect();
                this.f1462b = null;
            } catch (Exception e) {
            }
        }
    }

    public final void a_() {
        super.a_();
        Process.setThreadPriority(10);
        if (this.f1461a == null || this.f1461a.f1460b == null || (this.f1461a.c != null && this.f1461a.c.length > m.K * 1024)) {
            p();
            a.b("_HttpTask|run return ###");
            return;
        }
        if (this.f1461a.c != null && this.f1461a.c.length > 0) {
            this.f1461a.c = f.c(this.f1461a.c);
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 3) {
                d a2 = this.f1461a.c == null ? a(this.f1461a.f1460b) : a(this.f1461a.f1460b, this.f1461a.c);
                if (a2.f1463a) {
                    throw new Exception("http server resp decode header error");
                } else if (a2.f1464b != null) {
                    a(a2.f1464b);
                    return;
                } else if (i2 == 2) {
                    this.f1461a.a(new Exception("try up to limit"));
                    throw new Exception("http request exception, try times = " + (i2 + 1));
                } else {
                    i = i2 + 1;
                }
            } else {
                return;
            }
        }
    }

    public final int b() {
        return -2147483638;
    }

    public void d() {
        this.n = true;
    }

    /* access modifiers changed from: protected */
    public void e() {
    }

    public void f() {
        super.f();
        g();
    }
}
