package com.igexin.push.a.a;

import com.igexin.push.config.m;
import com.igexin.push.core.f;
import com.igexin.push.f.b.d;
import com.igexin.push.util.a;
import java.util.Calendar;

public class c implements d {
    private static c c;

    /* renamed from: a  reason: collision with root package name */
    private long f1197a = 0;

    /* renamed from: b  reason: collision with root package name */
    private long f1198b = 0;
    private boolean d = false;

    private c() {
    }

    public static c c() {
        if (c == null) {
            c = new c();
        }
        return c;
    }

    public void a() {
        d();
    }

    public void a(long j) {
        this.f1197a = j;
    }

    public boolean b() {
        return System.currentTimeMillis() - this.f1197a > this.f1198b;
    }

    public void d() {
        this.f1198b = 3600000;
        long currentTimeMillis = System.currentTimeMillis();
        if (m.f1251b != 0) {
            Calendar instance = Calendar.getInstance();
            if (a.a(currentTimeMillis)) {
                if (!this.d) {
                    this.d = true;
                    com.igexin.push.e.a aVar = new com.igexin.push.e.a();
                    aVar.a(com.igexin.push.core.c.g);
                    f.a().h().a(aVar);
                }
                if (m.f1250a + m.f1251b > 24) {
                    instance.set(11, (m.f1250a + m.f1251b) - 24);
                } else {
                    instance.set(11, m.f1250a + m.f1251b);
                }
                instance.set(12, 0);
                instance.set(13, 0);
                if (instance.getTimeInMillis() < currentTimeMillis) {
                    instance.add(5, 1);
                }
            } else {
                if (this.d) {
                    this.d = false;
                    com.igexin.push.e.a aVar2 = new com.igexin.push.e.a();
                    aVar2.a(com.igexin.push.core.c.start);
                    f.a().h().a(aVar2);
                }
                instance.set(11, m.f1250a);
                instance.set(12, 0);
                instance.set(13, 0);
                if (instance.getTimeInMillis() < currentTimeMillis) {
                    instance.add(5, 1);
                }
            }
            this.f1198b = instance.getTimeInMillis() - currentTimeMillis;
        } else if (this.d) {
            this.d = false;
            com.igexin.push.e.a aVar3 = new com.igexin.push.e.a();
            aVar3.a(com.igexin.push.core.c.start);
            f.a().h().a(aVar3);
        }
        if (m.c > this.f1198b + currentTimeMillis) {
            this.f1198b = m.c - currentTimeMillis;
            if (!this.d) {
                this.d = true;
                com.igexin.push.e.a aVar4 = new com.igexin.push.e.a();
                aVar4.a(com.igexin.push.core.c.g);
                f.a().h().a(aVar4);
            }
        }
    }
}
