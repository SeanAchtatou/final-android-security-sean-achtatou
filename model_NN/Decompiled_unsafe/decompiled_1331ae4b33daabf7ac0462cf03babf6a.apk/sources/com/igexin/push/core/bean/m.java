package com.igexin.push.core.bean;

import com.igexin.push.core.f;
import com.igexin.push.core.g;

public class m extends BaseAction {

    /* renamed from: a  reason: collision with root package name */
    private String f1353a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1354b;
    private boolean c;
    private String d;

    public String a() {
        return this.f1353a;
    }

    public void a(String str) {
        this.f1353a = str;
    }

    public void a(boolean z) {
        this.f1354b = z;
    }

    public String b() {
        return this.d;
    }

    public void b(String str) {
        this.d = str;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public String c() {
        String m;
        String str = this.f1353a;
        if (this.f1354b) {
            str = str.indexOf("?") > 0 ? str + "&cid=" + g.r : str + "?cid=" + g.r;
        }
        return (!this.c || (m = f.a().m()) == null) ? str : str.indexOf("?") > 0 ? str + "&nettype=" + m : str + "?nettype=" + m;
    }
}
