package com.igexin.push.core.stub;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import com.igexin.push.core.d.a;
import com.igexin.push.core.d.b;
import com.igexin.push.core.f;
import com.igexin.push.core.o;
import com.igexin.sdk.IPushCore;
import com.tencent.open.GameAppOperation;
import java.util.HashMap;
import java.util.Map;

public class PushCore implements IPushCore {

    /* renamed from: a  reason: collision with root package name */
    private f f1396a;

    /* renamed from: b  reason: collision with root package name */
    private Map<Activity, a> f1397b = new HashMap();

    public void onActivityConfigurationChanged(Activity activity, Configuration configuration) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.a(configuration);
        }
    }

    public boolean onActivityCreateOptionsMenu(Activity activity, Menu menu) {
        a aVar = this.f1397b.get(activity);
        return aVar != null && aVar.a(menu);
    }

    public void onActivityDestroy(Activity activity) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.h();
            this.f1397b.remove(activity);
            b.a().c(aVar);
        }
    }

    public boolean onActivityKeyDown(Activity activity, int i, KeyEvent keyEvent) {
        a aVar = this.f1397b.get(activity);
        return aVar != null && aVar.a(i, keyEvent);
    }

    public void onActivityNewIntent(Activity activity, Intent intent) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.a(intent);
        }
    }

    public void onActivityPause(Activity activity) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.f();
        }
    }

    public void onActivityRestart(Activity activity) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.d();
        }
    }

    public void onActivityResume(Activity activity) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.e();
        }
    }

    public void onActivityStart(Activity activity, Intent intent) {
        if (activity != null && intent != null && intent.hasExtra(GameAppOperation.SHARE_PRIZE_ACTIVITY_ID)) {
            a a2 = b.a().a(Long.valueOf(intent.getLongExtra(GameAppOperation.SHARE_PRIZE_ACTIVITY_ID, 0)));
            if (a2 != null) {
                a2.a(activity);
                this.f1397b.put(activity, a2);
                a2.c();
                return;
            }
            activity.finish();
        }
    }

    public void onActivityStop(Activity activity) {
        a aVar = this.f1397b.get(activity);
        if (aVar != null) {
            aVar.g();
        }
    }

    public IBinder onServiceBind(Intent intent) {
        return o.a();
    }

    public void onServiceDestroy() {
        if (this.f1396a != null) {
            this.f1396a.l();
        }
    }

    public int onServiceStartCommand(Intent intent, int i, int i2) {
        if (this.f1396a == null) {
            return 1;
        }
        Message obtain = Message.obtain();
        obtain.what = com.igexin.push.core.a.c;
        obtain.obj = intent;
        this.f1396a.a(obtain);
        return 1;
    }

    public boolean start(Context context) {
        this.f1396a = f.a();
        this.f1396a.a(context);
        return true;
    }
}
