package com.igexin.push.core.a;

import android.content.Intent;
import com.igexin.b.a.c.a;
import com.igexin.push.core.g;
import com.igexin.push.f.b.h;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

class i extends h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1270a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f1271b;
    final /* synthetic */ e c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    i(e eVar, long j, String str, String str2) {
        super(j);
        this.c = eVar;
        this.f1270a = str;
        this.f1271b = str2;
    }

    /* access modifiers changed from: protected */
    public void a() {
        boolean z = false;
        try {
            Intent intent = new Intent();
            intent.setClassName(this.f1270a, "com.igexin.sdk.GActivity");
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            g.f.startActivity(intent);
            z = true;
            a.b(this.l + "|startActivity success pkg = " + this.f1270a + " activityName = " + "com.igexin.sdk.GActivity");
        } catch (Exception e) {
            a.b(this.l + "|startActivity exception pkg = " + this.f1270a + " activityName = " + "com.igexin.sdk.GActivity" + " " + e.toString());
        }
        if (!z) {
            this.c.e(this.f1270a, this.f1271b);
        }
    }

    public int b() {
        return 0;
    }
}
