package com.igexin.push.core;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.b.a.d.a.b;
import com.igexin.b.a.d.d;
import com.igexin.push.c.i;
import com.igexin.push.config.k;
import com.igexin.push.config.m;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b.ae;
import com.igexin.push.core.b.g;
import com.igexin.push.e.j;
import com.igexin.push.f.b.h;
import com.igexin.sdk.PushConsts;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;

public class f implements b {
    private static f l;

    /* renamed from: a  reason: collision with root package name */
    private Context f1380a;

    /* renamed from: b  reason: collision with root package name */
    private h f1381b = new h();
    private Handler c;
    private ConcurrentLinkedQueue<Message> d = new ConcurrentLinkedQueue<>();
    private e e;
    private ConnectivityManager f;
    private c g;
    private com.igexin.b.a.b.b h;
    private j i;
    private com.igexin.push.e.c j;
    private com.igexin.push.b.b k;
    private final int m = 100;
    private final int n = 30;
    private final AtomicBoolean o = new AtomicBoolean(false);

    private f() {
    }

    public static f a() {
        if (l == null) {
            l = new f();
        }
        return l;
    }

    private Method a(String str, Class<?>... clsArr) {
        try {
            return Class.forName("com.igexin.dms.DMSManager").getMethod(str, clsArr);
        } catch (Exception e2) {
            a.b("CoreLogicinvokeMethod error");
            return null;
        }
    }

    private void q() {
        ServiceInfo[] serviceInfoArr;
        try {
            if (m.p && p()) {
                String packageName = this.f1380a.getPackageName();
                List<PackageInfo> installedPackages = this.f1380a.getPackageManager().getInstalledPackages(4);
                if (installedPackages != null && !installedPackages.isEmpty()) {
                    for (PackageInfo next : installedPackages) {
                        if (((next.applicationInfo.flags & 1) == 0 || (next.applicationInfo.flags & 128) != 0) && (serviceInfoArr = next.services) != null && serviceInfoArr.length != 0) {
                            int length = serviceInfoArr.length;
                            int i2 = 0;
                            while (true) {
                                if (i2 >= length) {
                                    break;
                                }
                                ServiceInfo serviceInfo = serviceInfoArr[i2];
                                if (!com.igexin.push.util.a.a(serviceInfo, next)) {
                                    i2++;
                                } else if (!packageName.equals(next.packageName)) {
                                    g.a().d().put(next.packageName, serviceInfo.name);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    private boolean r() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PushConsts.ACTION_BROADCAST_NETWORK_CHANGE);
        intentFilter.addAction("com.igexin.sdk.action.snlrefresh");
        intentFilter.addAction("com.igexin.sdk.action.snlretire");
        intentFilter.addAction(g.V);
        intentFilter.addAction("com.igexin.sdk.action.execute");
        intentFilter.addAction("com.igexin.sdk.action.doaction");
        intentFilter.addAction("android.intent.action.TIME_SET");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        if (com.igexin.push.util.a.c()) {
            intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        }
        if (this.f1380a.registerReceiver(n.a(), intentFilter) == null) {
            a.b("CoreLogic|InternalPublicReceiver|Failed");
        }
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addDataScheme("package");
        intentFilter2.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter2.addAction("android.intent.action.PACKAGE_REMOVED");
        if (this.f1380a.registerReceiver(m.a(), intentFilter2) != null) {
            return true;
        }
        a.b("CoreLogic|InternalPackageReceiver|Failed");
        return true;
    }

    public void a(e eVar) {
        this.c = eVar;
    }

    public boolean a(Context context) {
        this.f1380a = context;
        if (this.f1381b != null && this.f1381b.isAlive()) {
            a.b("CoreLogic|coreThread is alive +++++");
        } else if (!this.o.getAndSet(true)) {
            a.b("CoreLogic|start coreThread +++++");
            this.f1381b.start();
        }
        return true;
    }

    public boolean a(Message message) {
        if (g.g.get()) {
            this.c.sendMessage(message);
            return true;
        }
        this.d.add(message);
        return true;
    }

    public boolean a(com.igexin.b.a.d.a.e eVar, com.igexin.b.a.d.e eVar2) {
        return this.e != null && this.e.a(eVar);
    }

    public boolean a(d dVar, com.igexin.b.a.d.e eVar) {
        return this.e != null && this.e.a(dVar);
    }

    public boolean a(h hVar) {
        return hVar != null && c.b().a(hVar, false, true);
    }

    public boolean a(String str) {
        String d2 = e.a().d("ss");
        if (!(g.f == null || this.j == null)) {
            new com.igexin.sdk.a.d(g.f).b();
            g.i = false;
            g.m = false;
            com.igexin.push.e.a aVar = new com.igexin.push.e.a();
            aVar.a(c.g);
            this.j.a(aVar);
            if (d2 != null && "1".equals(d2)) {
                try {
                    InputStream inputStream = Runtime.getRuntime().exec("ps").getInputStream();
                    if (inputStream != null) {
                        String packageName = g.f.getPackageName();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                        while (true) {
                            String readLine = bufferedReader.readLine();
                            if (readLine == null) {
                                break;
                            }
                            String[] split = readLine.split("\\s+");
                            if (readLine.contains(packageName + "/files/gdaemon") && split.length > 0) {
                                Process.killProcess(Integer.valueOf(split[1]).intValue());
                                break;
                            }
                        }
                        bufferedReader.close();
                        inputStream.close();
                    }
                } catch (Exception e2) {
                }
                e();
            }
        }
        return true;
    }

    public boolean a(boolean z) {
        a.b("CoreLogic|start sdkSwitch isSlave = " + z);
        if (!(g.f == null || this.j == null)) {
            new com.igexin.sdk.a.d(g.f).a();
            g.i = true;
            if (!new com.igexin.sdk.a.b(g.f).b()) {
                new com.igexin.sdk.a.c(g.f).a();
                g.j = true;
                new com.igexin.sdk.a.b(g.f).a();
            }
            if (z) {
                new com.igexin.sdk.a.c(g.f).a();
                g.j = true;
            }
            a.b("CoreLogic|snlCoordinator.doEvent start ++++");
            com.igexin.push.e.a aVar = new com.igexin.push.e.a();
            aVar.a(c.start);
            this.j.a(aVar);
        }
        return true;
    }

    public void b() {
        try {
            this.f = (ConnectivityManager) this.f1380a.getSystemService("connectivity");
            g.a(this.f1380a);
            this.k = new com.igexin.push.b.b(this.f1380a);
            k.a().b();
            r();
            this.g = c.b();
            this.g.a((com.igexin.b.a.d.a.a<String, Integer, com.igexin.b.a.b.b, com.igexin.b.a.b.e>) new com.igexin.push.d.a(this.f1380a, j()));
            this.g.a((b) this);
            this.g.a(this.f1380a);
            com.igexin.push.b.a aVar = new com.igexin.push.b.a();
            aVar.a(g.a());
            aVar.a(com.igexin.push.core.b.d.a());
            aVar.a(com.igexin.push.core.b.b.a());
            aVar.a(com.igexin.push.config.a.a());
            aVar.a(ae.a());
            this.g.a(aVar, true, false);
            c.b().a(com.igexin.b.b.a.a(g.B.getBytes()));
            g.af = this.g.a(com.igexin.push.f.b.c.g(), false, true);
            g.ag = this.g.a(com.igexin.push.f.b.g.g(), true, true);
            i.a().c();
            c();
            this.e = e.a();
            d();
            this.i = new j();
            this.i.a(this.f1380a, this.g, this.e);
            this.j = new com.igexin.push.e.c();
            this.j.a(this.f1380a);
            com.igexin.push.e.a aVar2 = new com.igexin.push.e.a();
            aVar2.a(c.start);
            this.j.a(aVar2);
            com.igexin.push.a.a.c.c().d();
            g.g.set(true);
            Iterator<Message> it = this.d.iterator();
            while (it.hasNext()) {
                Message next = it.next();
                if (this.c != null) {
                    this.c.sendMessage(next);
                }
            }
            this.e.a(Process.myPid());
            q();
            com.igexin.push.extension.a.a().a(this.f1380a);
            e.a().s();
            try {
                com.igexin.assist.sdk.b.a().a(g.f);
                com.igexin.assist.sdk.b.a().b(g.f);
            } catch (Throwable th) {
            }
            Object invoke = a("getInstance", new Class[0]).invoke(null, new Object[0]);
            a("start", Context.class).invoke(invoke, this.f1380a);
        } catch (Throwable th2) {
            a.b("CoreLogic|init|failed|" + th2.toString());
        }
    }

    public boolean b(String str) {
        if (g.f == null || this.j == null) {
            return true;
        }
        new com.igexin.sdk.a.c(g.f).b();
        g.j = false;
        g.m = false;
        com.igexin.push.e.a aVar = new com.igexin.push.e.a();
        aVar.a(c.g);
        this.j.a(aVar);
        return true;
    }

    public com.igexin.push.f.b.a c() {
        com.igexin.push.f.b.a g2 = com.igexin.push.f.b.a.g();
        g2.a(new com.igexin.push.a.a.a());
        g2.a(new com.igexin.push.a.a.b());
        g2.a(new com.igexin.push.a.a.d());
        g2.a(com.igexin.push.a.a.c.c());
        g.ah = this.g.a(g2, false, true);
        this.f1380a.sendBroadcast(new Intent());
        return g2;
    }

    public void d() {
        if (TextUtils.isEmpty(g.w)) {
            try {
                if (com.igexin.push.util.a.a()) {
                    WifiInfo connectionInfo = ((WifiManager) this.f1380a.getSystemService("wifi")).getConnectionInfo();
                    if (connectionInfo != null) {
                        g.a().a(connectionInfo.getMacAddress());
                    }
                    a.b("CoreLogic mac:" + g.w);
                }
            } catch (Throwable th) {
            }
        }
    }

    public void e() {
        this.f1380a.stopService(new Intent(this.f1380a, e.a().a(g.f)));
    }

    public com.igexin.b.a.b.b f() {
        if (this.h == null) {
            this.h = com.igexin.push.d.a.b.a();
        }
        return this.h;
    }

    public j g() {
        return this.i;
    }

    public com.igexin.push.e.c h() {
        return this.j;
    }

    public e i() {
        return this.e;
    }

    public ConnectivityManager j() {
        return this.f;
    }

    public com.igexin.push.b.b k() {
        return this.k;
    }

    public void l() {
        try {
            this.f1380a.unregisterReceiver(c.b());
        } catch (Exception e2) {
        }
        try {
            this.f1380a.unregisterReceiver(m.a());
        } catch (Exception e3) {
        }
        try {
            this.f1380a.unregisterReceiver(n.a());
        } catch (Exception e4) {
        }
        try {
            com.igexin.push.extension.a.a().b();
        } catch (Throwable th) {
        }
    }

    public String m() {
        NetworkInfo activeNetworkInfo;
        if (this.f == null || (activeNetworkInfo = this.f.getActiveNetworkInfo()) == null) {
            return null;
        }
        if (activeNetworkInfo.getType() == 1) {
            return "wifi";
        }
        if (activeNetworkInfo.getType() == 0) {
            return "mobile";
        }
        return null;
    }

    public boolean n() {
        return true;
    }

    public long o() {
        return 94808;
    }

    public boolean p() {
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) g.f.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getMemoryInfo(memoryInfo);
            long j2 = (memoryInfo.availMem / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID;
            if (memoryInfo.lowMemory) {
                a.b("CoreLogic", "system in lowMemory, available menmory = " + j2 + "M");
                return false;
            } else if (j2 < 100) {
                return false;
            } else {
                return (((Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()) + Runtime.getRuntime().freeMemory()) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID) / PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID > 30;
            }
        } catch (Throwable th) {
            return false;
        }
    }
}
