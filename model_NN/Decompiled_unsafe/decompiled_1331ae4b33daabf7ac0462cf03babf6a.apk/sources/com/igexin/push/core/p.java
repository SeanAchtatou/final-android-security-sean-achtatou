package com.igexin.push.core;

import com.igexin.sdk.aidl.a;

final class p extends a {
    p() {
    }

    public byte[] extFunction(byte[] bArr) {
        return null;
    }

    public int isStarted(String str) {
        return 1;
    }

    public int onASNLConnected(String str, String str2, String str3, long j) {
        if (f.a() != null) {
            return f.a().h().a(str3);
        }
        return -1;
    }

    public int onASNLNetworkConnected() {
        if (f.a().g().a()) {
            return -1;
        }
        f.a().g().b();
        return 0;
    }

    public int onASNLNetworkDisconnected() {
        if (f.a().g().a()) {
            return -1;
        }
        f.a().g().b(false);
        return 0;
    }

    public int onPSNLConnected(String str, String str2, String str3, long j) {
        if (f.a() == null || str.equals("") || str2.equals("")) {
            return -1;
        }
        return f.a().h().a(str, str2);
    }

    public int receiveToPSNL(String str, String str2, byte[] bArr) {
        if (str2 == null || bArr == null || f.a().g().a()) {
            return -1;
        }
        return f.a().h().b(str, str2, bArr);
    }

    public int sendByASNL(String str, String str2, byte[] bArr) {
        if (str2 == null || bArr == null || !f.a().g().a()) {
            return -1;
        }
        return f.a().h().a(str, str2, bArr);
    }

    public int setSilentTime(int i, int i2, String str) {
        return 1;
    }

    public int startService(String str) {
        return 1;
    }

    public int stopService(String str) {
        return 1;
    }
}
