package com.igexin.push.core.c;

import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import com.igexin.push.d.c.a;
import java.util.TimerTask;

class e extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PushTaskBean f1363a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ a f1364b;
    final /* synthetic */ c c;

    e(c cVar, PushTaskBean pushTaskBean, a aVar) {
        this.c = cVar;
        this.f1363a = pushTaskBean;
        this.f1364b = aVar;
    }

    public void run() {
        if (g.ak.containsKey(this.f1363a.getTaskId())) {
            g.ak.get(this.f1363a.getTaskId()).cancel();
            g.ak.remove(this.f1363a.getTaskId());
        }
        this.c.a(this.f1363a, this.f1364b);
        this.f1364b.b(this.f1364b.c() + 1);
    }
}
