package com.igexin.push.core.a;

import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import com.igexin.push.d.c.a;
import com.igexin.push.d.c.c;
import com.igexin.push.f.b.b;

public class d extends a {
    private void a(String str, a aVar) {
        if (str != null) {
            String substring = str.substring("CDN".length(), str.length());
            if (substring.contains("@")) {
                String[] split = substring.split("\\@");
                String str2 = split[0];
                if (split[1].contains("|")) {
                    String[] split2 = split[1].split("\\|");
                    String str3 = split2[0];
                    String str4 = split2[1];
                    if (str2 != null && str3 != null && str4 != null) {
                        PushTaskBean pushTaskBean = new PushTaskBean();
                        pushTaskBean.setAppid(g.f1382a);
                        pushTaskBean.setMessageId(str2);
                        pushTaskBean.setTaskId(str3);
                        pushTaskBean.setId(str2);
                        pushTaskBean.setAppKey(g.f1383b);
                        pushTaskBean.setCurrentActionid(1);
                        e.a().a(pushTaskBean);
                        e.a().a(str4, aVar, pushTaskBean);
                    }
                }
            }
        }
    }

    public boolean a(com.igexin.b.a.d.d dVar) {
        return super.a(dVar);
    }

    public boolean a(Object obj) {
        b e;
        if (!(obj instanceof a)) {
            return true;
        }
        a aVar = (a) obj;
        if (aVar.c == null) {
            return true;
        }
        String str = (String) aVar.c;
        com.igexin.b.a.c.a.b("cdnpushmessage|" + str);
        if (str.startsWith("RCV")) {
            String substring = str.substring("RCV".length(), str.length());
            if (!g.al.containsKey(substring)) {
                return true;
            }
            c cVar = g.al.get(substring);
            g.al.remove(substring);
            if (cVar == null || (e = cVar.e()) == null) {
                return true;
            }
            e.u();
            return true;
        } else if (!str.contains("CDN")) {
            return true;
        } else {
            a(str, aVar);
            return true;
        }
    }
}
