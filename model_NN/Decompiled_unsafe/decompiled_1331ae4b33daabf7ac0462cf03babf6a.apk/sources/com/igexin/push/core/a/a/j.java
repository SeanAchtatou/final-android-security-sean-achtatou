package com.igexin.push.core.a.a;

import android.content.Intent;
import android.net.Uri;
import com.igexin.push.config.l;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.m;
import com.igexin.push.core.g;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class j implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1259a = l.f1249a;

    private void a(m mVar, String str) {
        int indexOf;
        String substring;
        String a2 = mVar.a();
        if (a2 != null && (indexOf = a2.indexOf(str)) != -1) {
            String str2 = "";
            String str3 = null;
            int indexOf2 = a2.indexOf("&");
            if (indexOf2 == -1) {
                str2 = a2.substring(0, indexOf - 1);
                String substring2 = a2.substring(indexOf);
                if (substring2.contains("=")) {
                    str3 = substring2.substring(substring2.indexOf("=") + 1);
                }
            } else if (a2.charAt(indexOf - 1) == '?') {
                str2 = a2.substring(0, indexOf) + a2.substring(indexOf2 + 1);
                String substring3 = a2.substring(indexOf, indexOf2);
                if (substring3.contains("=")) {
                    str3 = substring3.substring(substring3.indexOf("=") + 1);
                }
            } else if (a2.charAt(indexOf - 1) == '&') {
                String substring4 = a2.substring(0, indexOf - 1);
                String substring5 = a2.substring(indexOf);
                String str4 = "";
                int indexOf3 = substring5.indexOf("&");
                if (indexOf3 != -1) {
                    str4 = substring5.substring(indexOf3);
                    String substring6 = substring5.substring(0, indexOf3);
                    substring = substring6.substring(substring6.indexOf("=") + 1);
                } else {
                    substring = substring5.substring(substring5.indexOf("=") + 1);
                }
                String str5 = substring;
                str2 = substring4 + str4;
                str3 = str5;
            }
            mVar.a(str2);
            mVar.b(str3);
        }
    }

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            if (jSONObject.has("url") && jSONObject.has("do") && jSONObject.has("actionid")) {
                String string = jSONObject.getString("url");
                if (!string.equals("")) {
                    m mVar = new m();
                    mVar.setType("startweb");
                    mVar.setActionId(jSONObject.getString("actionid"));
                    mVar.setDoActionId(jSONObject.getString("do"));
                    mVar.a(string);
                    if (jSONObject.has("is_withcid") && jSONObject.getString("is_withcid").equals("true")) {
                        mVar.a(true);
                    }
                    if (!jSONObject.has("is_withnettype") || !jSONObject.getString("is_withnettype").equals("true")) {
                        return mVar;
                    }
                    mVar.b(true);
                    return mVar;
                }
            }
        } catch (JSONException e) {
        }
        return null;
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        m mVar = (m) baseAction;
        a(mVar, "targetpkgname");
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent.setPackage(mVar.b());
        intent.setData(Uri.parse(mVar.c()));
        try {
            g.f.startActivity(intent);
        } catch (Exception e) {
        }
        if (baseAction.getDoActionId().equals("")) {
            return true;
        }
        e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), baseAction.getDoActionId());
        return true;
    }
}
