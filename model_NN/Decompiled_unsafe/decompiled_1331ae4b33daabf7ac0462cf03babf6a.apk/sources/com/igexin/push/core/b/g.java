package com.igexin.push.core.b;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.push.util.EncryptUtils;
import com.igexin.push.util.e;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class g implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1301a = g.class.getName();

    /* renamed from: b  reason: collision with root package name */
    private static g f1302b;
    private Map<String, String> c = new TreeMap();
    private boolean d;

    private g() {
    }

    public static g a() {
        if (f1302b == null) {
            f1302b = new g();
        }
        return f1302b;
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, int i, String str) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Integer.valueOf(i));
        contentValues.put("value", str);
        sQLiteDatabase.replace("runtime", null, contentValues);
    }

    /* access modifiers changed from: private */
    public void a(SQLiteDatabase sQLiteDatabase, int i, byte[] bArr) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", Integer.valueOf(i));
        contentValues.put("value", bArr);
        sQLiteDatabase.replace("runtime", null, contentValues);
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(android.database.sqlite.SQLiteDatabase r6, int r7) {
        /*
            r5 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            r1.<init>()     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            java.lang.String r2 = "select value from runtime where id="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            r2 = 0
            android.database.Cursor r1 = r6.rawQuery(r1, r2)     // Catch:{ Exception -> 0x003d, all -> 0x0045 }
            if (r1 == 0) goto L_0x0037
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            if (r2 == 0) goto L_0x0037
            java.lang.String r2 = "value"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            byte[] r2 = r1.getBlob(r2)     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            java.lang.String r3 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            byte[] r0 = com.igexin.b.a.a.a.c(r2, r3)     // Catch:{ Exception -> 0x0051, all -> 0x004f }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return r0
        L_0x0037:
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x003d:
            r1 = move-exception
            r1 = r0
        L_0x003f:
            if (r1 == 0) goto L_0x0036
            r1.close()
            goto L_0x0036
        L_0x0045:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()
        L_0x004e:
            throw r0
        L_0x004f:
            r0 = move-exception
            goto L_0x0049
        L_0x0051:
            r2 = move-exception
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.g.a(android.database.sqlite.SQLiteDatabase, int):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String b(android.database.sqlite.SQLiteDatabase r5, int r6) {
        /*
            r4 = this;
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            r1.<init>()     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            java.lang.String r2 = "select value from runtime where id="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            r2 = 0
            android.database.Cursor r1 = r5.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0037, all -> 0x003f }
            if (r1 == 0) goto L_0x0031
            boolean r2 = r1.moveToFirst()     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            if (r2 == 0) goto L_0x0031
            java.lang.String r2 = "value"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            java.lang.String r0 = r1.getString(r2)     // Catch:{ Exception -> 0x004b, all -> 0x0049 }
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            return r0
        L_0x0031:
            if (r1 == 0) goto L_0x0030
            r1.close()
            goto L_0x0030
        L_0x0037:
            r1 = move-exception
            r1 = r0
        L_0x0039:
            if (r1 == 0) goto L_0x0030
            r1.close()
            goto L_0x0030
        L_0x003f:
            r1 = move-exception
            r3 = r1
            r1 = r0
            r0 = r3
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()
        L_0x0048:
            throw r0
        L_0x0049:
            r0 = move-exception
            goto L_0x0043
        L_0x004b:
            r2 = move-exception
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.g.b(android.database.sqlite.SQLiteDatabase, int):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0046, code lost:
        if (r0 != null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0052, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0059, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x005a, code lost:
        r4 = r1;
        r1 = r0;
        r0 = r4;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0045 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:1:0x0004] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0052  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void e(android.database.sqlite.SQLiteDatabase r6) {
        /*
            r5 = this;
            r0 = 0
            java.lang.String r1 = "select value from runtime where id=25"
            r2 = 0
            android.database.Cursor r0 = r6.rawQuery(r1, r2)     // Catch:{ Exception -> 0x0045, all -> 0x004c }
            if (r0 == 0) goto L_0x002f
            boolean r1 = r0.moveToFirst()     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            if (r1 == 0) goto L_0x002f
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            java.lang.String r2 = "value"
            int r2 = r0.getColumnIndex(r2)     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            byte[] r2 = r0.getBlob(r2)     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            android.content.Context r3 = com.igexin.push.core.g.f     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            java.lang.String r3 = r3.getPackageName()     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            java.lang.String r3 = com.igexin.b.b.a.a(r3)     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            byte[] r2 = com.igexin.b.a.a.a.c(r2, r3)     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
            com.igexin.push.core.g.C = r1     // Catch:{ Exception -> 0x0045, all -> 0x0059 }
        L_0x002f:
            if (r0 == 0) goto L_0x0034
            r0.close()
        L_0x0034:
            java.lang.String r0 = com.igexin.push.core.g.C
            if (r0 != 0) goto L_0x0044
            java.lang.String r0 = com.igexin.push.core.g.t
            if (r0 != 0) goto L_0x0056
            java.lang.String r0 = "cantgetimei"
        L_0x003e:
            java.lang.String r0 = com.igexin.b.b.a.a(r0)
            com.igexin.push.core.g.C = r0
        L_0x0044:
            return
        L_0x0045:
            r1 = move-exception
            if (r0 == 0) goto L_0x0034
            r0.close()
            goto L_0x0034
        L_0x004c:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0050:
            if (r1 == 0) goto L_0x0055
            r1.close()
        L_0x0055:
            throw r0
        L_0x0056:
            java.lang.String r0 = com.igexin.push.core.g.t
            goto L_0x003e
        L_0x0059:
            r1 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.g.e(android.database.sqlite.SQLiteDatabase):void");
    }

    private boolean e() {
        return c.b().a(new x(this), false, true);
    }

    /* access modifiers changed from: private */
    public void f() {
        e.a();
        String c2 = e.c();
        if (c2 == null || c2.length() <= 5) {
            e.e();
        }
    }

    private void f(SQLiteDatabase sQLiteDatabase) {
        String b2 = b(sQLiteDatabase, 2);
        if (!TextUtils.isEmpty(b2)) {
            if (b2.equals("null")) {
                b2 = null;
            }
            com.igexin.push.core.g.y = b2;
        }
    }

    private String g() {
        String str = "";
        Random random = new Random(Math.abs(new Random().nextLong()));
        for (int i = 0; i < 15; i++) {
            str = str + random.nextInt(10);
        }
        return str;
    }

    private void g(SQLiteDatabase sQLiteDatabase) {
        String b2 = b(sQLiteDatabase, 46);
        if (!TextUtils.isEmpty(b2)) {
            if (b2.equals("null")) {
                b2 = null;
            }
            com.igexin.push.core.g.z = b2;
        }
    }

    /* access modifiers changed from: private */
    public byte[] g(String str) {
        return EncryptUtils.getBytesEncrypted(str.getBytes());
    }

    private void h(SQLiteDatabase sQLiteDatabase) {
        String b2 = b(sQLiteDatabase, 48);
        if (!TextUtils.isEmpty(b2)) {
            if (b2.equals("null")) {
                b2 = null;
            }
            com.igexin.push.core.g.A = b2;
        }
    }

    private void i(SQLiteDatabase sQLiteDatabase) {
        String b2 = b(sQLiteDatabase, 3);
        if (!TextUtils.isEmpty(b2)) {
            if (b2.equals("null")) {
                b2 = null;
            }
            com.igexin.push.core.g.B = b2;
        }
    }

    private void j(SQLiteDatabase sQLiteDatabase) {
        byte[] a2 = a(sQLiteDatabase, 1);
        if (a2 != null) {
            try {
                String str = new String(a2);
                com.igexin.push.core.g.q = str.equals("null") ? 0 : Long.parseLong(str);
            } catch (Exception e) {
            }
            a.b(f1301a + "|db version changed, save session = " + com.igexin.push.core.g.q);
        }
    }

    private void k(SQLiteDatabase sQLiteDatabase) {
        byte[] a2 = a(sQLiteDatabase, 20);
        if (a2 != null) {
            String str = new String(a2);
            if (str.equals("null")) {
                str = null;
            }
            com.igexin.push.core.g.s = str;
            com.igexin.push.core.g.r = str;
            a.b(f1301a + "|db version changed, save cid = " + str);
        }
    }

    public void a(SQLiteDatabase sQLiteDatabase) {
    }

    public boolean a(int i) {
        com.igexin.push.core.g.T = i;
        return c.b().a(new o(this), false, true);
    }

    public boolean a(long j) {
        if (j == com.igexin.push.core.g.H) {
            return false;
        }
        com.igexin.push.core.g.H = j;
        return c.b().a(new s(this), false, true);
    }

    public boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        com.igexin.push.core.g.w = str;
        return c.b().a(new y(this), false, true);
    }

    public boolean a(String str, String str2, long j) {
        com.igexin.push.core.g.q = j;
        if (TextUtils.isEmpty(com.igexin.push.core.g.y)) {
            com.igexin.push.core.g.y = str2;
        }
        com.igexin.push.core.g.r = str;
        return e();
    }

    public boolean a(String str, boolean z) {
        String str2 = null;
        if (str == null) {
            return false;
        }
        if (z) {
            if (!str.equals(com.igexin.push.core.g.az)) {
                if (!str.equals("null")) {
                    str2 = str;
                }
                com.igexin.push.core.g.az = str2;
                return c.b().a(new p(this, str), false, true);
            }
        } else if (!str.equals(com.igexin.push.core.g.aA)) {
            if (!str.equals("null")) {
                str2 = str;
            }
            com.igexin.push.core.g.aA = str2;
            return c.b().a(new q(this, str), false, true);
        }
        return false;
    }

    public boolean a(boolean z) {
        if (com.igexin.push.core.g.O == z) {
            return false;
        }
        com.igexin.push.core.g.O = z;
        return c.b().a(new l(this), false, true);
    }

    public void b() {
        c.b().a(new h(this), false, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:122:0x0241, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x0242, code lost:
        r10 = r1;
        r1 = r0;
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0247, code lost:
        r1.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0241 A[ExcHandler: all (r1v23 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:4:0x0013] */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x0247  */
    /* JADX WARNING: Removed duplicated region for block: B:234:0x0484  */
    /* JADX WARNING: Removed duplicated region for block: B:271:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x018e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(android.database.sqlite.SQLiteDatabase r12) {
        /*
            r11 = this;
            r9 = 20
            r8 = 1
            r3 = 0
            r6 = 0
            r1 = 0
            r11.e(r12)
            java.lang.String r0 = "select id, value from runtime order by id"
            r2 = 0
            android.database.Cursor r0 = r12.rawQuery(r0, r2)     // Catch:{ Exception -> 0x0491, all -> 0x048e }
            if (r0 == 0) goto L_0x047d
        L_0x0013:
            boolean r2 = r0.moveToNext()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r2 == 0) goto L_0x047d
            r2 = 0
            r4 = 1
            int r5 = r0.getInt(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r5 == r8) goto L_0x003f
            r2 = 14
            if (r5 == r2) goto L_0x003f
            r2 = 19
            if (r5 == r2) goto L_0x003f
            if (r5 == r9) goto L_0x003f
            r2 = 23
            if (r5 == r2) goto L_0x003f
            r2 = 25
            if (r5 == r2) goto L_0x003f
            r2 = 22
            if (r5 == r2) goto L_0x003f
            r2 = 31
            if (r5 == r2) goto L_0x003f
            r2 = 30
            if (r5 != r2) goto L_0x0221
        L_0x003f:
            byte[] r2 = r0.getBlob(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r4 = r2
            r2 = r1
        L_0x0045:
            if (r4 != 0) goto L_0x0049
            if (r2 == 0) goto L_0x0013
        L_0x0049:
            switch(r5) {
                case 1: goto L_0x004d;
                case 2: goto L_0x024d;
                case 3: goto L_0x0298;
                case 4: goto L_0x022e;
                case 5: goto L_0x004c;
                case 6: goto L_0x0286;
                case 7: goto L_0x004c;
                case 8: goto L_0x0274;
                case 9: goto L_0x004c;
                case 10: goto L_0x004c;
                case 11: goto L_0x02a5;
                case 12: goto L_0x02b7;
                case 13: goto L_0x02c9;
                case 14: goto L_0x02d6;
                case 15: goto L_0x0317;
                case 16: goto L_0x0327;
                case 17: goto L_0x0339;
                case 18: goto L_0x0346;
                case 19: goto L_0x02e5;
                case 20: goto L_0x02fd;
                case 21: goto L_0x0358;
                case 22: goto L_0x036a;
                case 23: goto L_0x03a0;
                case 24: goto L_0x004c;
                case 25: goto L_0x004c;
                case 26: goto L_0x004c;
                case 27: goto L_0x004c;
                case 28: goto L_0x004c;
                case 29: goto L_0x004c;
                case 30: goto L_0x03d6;
                case 31: goto L_0x040c;
                case 32: goto L_0x0442;
                case 33: goto L_0x004c;
                case 34: goto L_0x004c;
                case 35: goto L_0x004c;
                case 36: goto L_0x004c;
                case 37: goto L_0x004c;
                case 38: goto L_0x004c;
                case 39: goto L_0x004c;
                case 40: goto L_0x0454;
                case 41: goto L_0x004c;
                case 42: goto L_0x004c;
                case 43: goto L_0x004c;
                case 44: goto L_0x004c;
                case 45: goto L_0x004c;
                case 46: goto L_0x025a;
                case 47: goto L_0x046b;
                case 48: goto L_0x0267;
                default: goto L_0x004c;
            }     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
        L_0x004c:
            goto L_0x0013
        L_0x004d:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x0064, all -> 0x0241 }
            if (r4 == 0) goto L_0x0228
            r4 = r6
        L_0x0061:
            com.igexin.push.core.g.q = r4     // Catch:{ Exception -> 0x0064, all -> 0x0241 }
            goto L_0x0013
        L_0x0064:
            r2 = move-exception
            r4 = 0
            com.igexin.push.core.g.q = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x006a:
            r2 = move-exception
        L_0x006b:
            if (r0 == 0) goto L_0x0070
            r0.close()
        L_0x0070:
            long r4 = com.igexin.push.core.g.q
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x008f
            long r4 = com.igexin.push.util.e.d()
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x008f
            com.igexin.push.core.g.q = r4
            java.lang.String r0 = java.lang.String.valueOf(r4)
            byte[] r0 = r0.getBytes()
            byte[] r0 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r0)
            r11.a(r12, r8, r0)
        L_0x008f:
            java.lang.String r0 = com.igexin.push.core.g.r
            if (r0 != 0) goto L_0x00aa
            java.lang.String r0 = com.igexin.push.util.e.b()
            if (r0 == 0) goto L_0x00aa
            com.igexin.push.core.g.s = r0
            com.igexin.push.core.g.r = r0
            java.lang.String r0 = com.igexin.push.core.g.r
            byte[] r0 = r0.getBytes()
            byte[] r0 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r0)
            r11.a(r12, r9, r0)
        L_0x00aa:
            java.lang.String r0 = com.igexin.push.core.g.r
            if (r0 != 0) goto L_0x00d2
            long r4 = com.igexin.push.core.g.q
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x00d2
            long r4 = com.igexin.push.core.g.q
            java.lang.String r0 = java.lang.String.valueOf(r4)
            java.lang.String r0 = com.igexin.b.b.a.a(r0)
            com.igexin.push.core.g.s = r0
            long r4 = com.igexin.push.core.g.q
            com.igexin.push.core.g.a(r4)
            java.lang.String r0 = com.igexin.push.core.g.r
            byte[] r0 = r0.getBytes()
            byte[] r0 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r0)
            r11.a(r12, r9, r0)
        L_0x00d2:
            java.lang.String r0 = "cfcd208495d565ef66e7dff9f98764da"
            java.lang.String r2 = com.igexin.push.core.g.r
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x00ea
            java.lang.String r0 = com.igexin.push.core.g.r
            if (r0 == 0) goto L_0x0100
            java.lang.String r0 = com.igexin.push.core.g.r
            java.lang.String r2 = "([a-f]|[0-9]){32}"
            boolean r0 = r0.matches(r2)
            if (r0 != 0) goto L_0x0100
        L_0x00ea:
            long r4 = com.igexin.push.core.g.q
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x0484
            com.igexin.push.core.b.g r0 = a()
            long r4 = com.igexin.push.core.g.q
            r0.b(r4)
            java.lang.String r0 = com.igexin.push.core.g.r
            com.igexin.push.core.g.s = r0
            com.igexin.push.util.e.f()
        L_0x0100:
            java.lang.String r0 = com.igexin.push.core.g.au
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0112
            java.lang.String r0 = "null"
            java.lang.String r1 = com.igexin.push.core.g.au
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0129
        L_0x0112:
            r0 = 32
            java.lang.String r0 = com.igexin.b.b.a.a(r0)
            com.igexin.push.core.g.au = r0
            r0 = 14
            java.lang.String r1 = com.igexin.push.core.g.au
            byte[] r1 = r1.getBytes()
            byte[] r1 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r1)
            r11.a(r12, r0, r1)
        L_0x0129:
            java.lang.String r0 = com.igexin.push.util.e.c()
            java.lang.String r1 = com.igexin.push.core.g.y
            if (r1 != 0) goto L_0x0142
            if (r0 == 0) goto L_0x0142
            int r1 = r0.length()
            r2 = 5
            if (r1 <= r2) goto L_0x0142
            com.igexin.push.core.g.y = r0
            r0 = 2
            java.lang.String r1 = com.igexin.push.core.g.y
            r11.a(r12, r0, r1)
        L_0x0142:
            java.lang.String r0 = com.igexin.push.core.g.B
            if (r0 != 0) goto L_0x018a
            java.lang.String r0 = com.igexin.push.core.g.t
            if (r0 != 0) goto L_0x0161
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "V"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = r11.g()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0161:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "A-"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r1 = "-"
            java.lang.StringBuilder r0 = r0.append(r1)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            com.igexin.push.core.g.B = r0
            r0 = 3
            java.lang.String r1 = com.igexin.push.core.g.B
            r11.a(r12, r0, r1)
        L_0x018a:
            boolean r0 = r11.d
            if (r0 == 0) goto L_0x0220
            r11.d = r3
            java.lang.String r0 = com.igexin.push.core.g.C
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x01b1
            r0 = 25
            java.lang.String r1 = com.igexin.push.core.g.C
            byte[] r1 = r1.getBytes()
            android.content.Context r2 = com.igexin.push.core.g.f
            java.lang.String r2 = r2.getPackageName()
            java.lang.String r2 = com.igexin.b.b.a.a(r2)
            byte[] r1 = com.igexin.b.a.a.a.d(r1, r2)
            r11.a(r12, r0, r1)
        L_0x01b1:
            long r0 = com.igexin.push.core.g.q
            int r0 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r0 == 0) goto L_0x01c8
            long r0 = com.igexin.push.core.g.q
            java.lang.String r0 = java.lang.String.valueOf(r0)
            byte[] r0 = r0.getBytes()
            byte[] r0 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r0)
            r11.a(r12, r8, r0)
        L_0x01c8:
            java.lang.String r0 = com.igexin.push.core.g.r
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x01dd
            java.lang.String r0 = com.igexin.push.core.g.r
            byte[] r0 = r0.getBytes()
            byte[] r0 = com.igexin.push.util.EncryptUtils.getBytesEncrypted(r0)
            r11.a(r12, r9, r0)
        L_0x01dd:
            java.lang.String r0 = com.igexin.push.core.g.y
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x01f4
            java.lang.String r0 = com.igexin.push.core.g.y
            int r0 = r0.length()
            r1 = 5
            if (r0 <= r1) goto L_0x01f4
            r0 = 2
            java.lang.String r1 = com.igexin.push.core.g.y
            r11.a(r12, r0, r1)
        L_0x01f4:
            java.lang.String r0 = com.igexin.push.core.g.B
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0202
            r0 = 3
            java.lang.String r1 = com.igexin.push.core.g.B
            r11.a(r12, r0, r1)
        L_0x0202:
            java.lang.String r0 = com.igexin.push.core.g.z
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0211
            r0 = 46
            java.lang.String r1 = com.igexin.push.core.g.z
            r11.a(r12, r0, r1)
        L_0x0211:
            java.lang.String r0 = com.igexin.push.core.g.A
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x0220
            r0 = 48
            java.lang.String r1 = com.igexin.push.core.g.A
            r11.a(r12, r0, r1)
        L_0x0220:
            return
        L_0x0221:
            java.lang.String r2 = r0.getString(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r4 = r1
            goto L_0x0045
        L_0x0228:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x0064, all -> 0x0241 }
            goto L_0x0061
        L_0x022e:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 != 0) goto L_0x023c
            boolean r2 = java.lang.Boolean.parseBoolean(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r2 == 0) goto L_0x024b
        L_0x023c:
            r2 = r8
        L_0x023d:
            com.igexin.push.core.g.k = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0241:
            r1 = move-exception
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0245:
            if (r1 == 0) goto L_0x024a
            r1.close()
        L_0x024a:
            throw r0
        L_0x024b:
            r2 = r3
            goto L_0x023d
        L_0x024d:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0256
            r2 = r1
        L_0x0256:
            com.igexin.push.core.g.y = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x025a:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0263
            r2 = r1
        L_0x0263:
            com.igexin.push.core.g.z = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0267:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0270
            r2 = r1
        L_0x0270:
            com.igexin.push.core.g.A = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0274:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0281
            r4 = r6
        L_0x027d:
            com.igexin.push.core.g.H = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0281:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x027d
        L_0x0286:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0293
            r4 = r6
        L_0x028f:
            com.igexin.push.core.g.G = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0293:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x028f
        L_0x0298:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x02a1
            r2 = r1
        L_0x02a1:
            com.igexin.push.core.g.B = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02a5:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x02b2
            r4 = r6
        L_0x02ae:
            com.igexin.push.core.g.K = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02b2:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x02ae
        L_0x02b7:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x02c4
            r4 = r6
        L_0x02c0:
            com.igexin.push.core.g.L = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02c4:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x02c0
        L_0x02c9:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x02d2
            r2 = r1
        L_0x02d2:
            com.igexin.push.core.g.N = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02d6:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.push.core.g.au = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02e5:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x02f9
            r2 = r1
        L_0x02f9:
            com.igexin.push.core.g.w = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x02fd:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0311
            r2 = r1
        L_0x0311:
            com.igexin.push.core.g.s = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.push.core.g.r = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0317:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 != 0) goto L_0x0013
            boolean r2 = java.lang.Boolean.parseBoolean(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.push.core.g.O = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0327:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0334
            r4 = r6
        L_0x0330:
            com.igexin.push.core.g.P = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0334:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0330
        L_0x0339:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0342
            r2 = r1
        L_0x0342:
            com.igexin.push.core.g.R = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0346:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0353
            r2 = r3
        L_0x034f:
            com.igexin.push.core.g.T = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0353:
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x034f
        L_0x0358:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0365
            r4 = r6
        L_0x0361:
            com.igexin.push.core.g.aw = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0365:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0361
        L_0x036a:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x037e
            r2 = r1
        L_0x037e:
            com.igexin.push.core.g.ay = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.b.g.f1301a     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "|read last detectWifiLastResult = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.g.ay     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x03a0:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x03b4
            r2 = r1
        L_0x03b4:
            com.igexin.push.core.g.ax = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.b.g.f1301a     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "|read last detectMobileLastResult = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.g.ax     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x03d6:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x03ea
            r2 = r1
        L_0x03ea:
            com.igexin.push.core.g.aA = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.b.g.f1301a     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "|read last domainWifiStatus = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.g.aA     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x040c:
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r5 = com.igexin.push.core.g.C     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            byte[] r4 = com.igexin.b.a.a.a.c(r4, r5)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0420
            r2 = r1
        L_0x0420:
            com.igexin.push.core.g.az = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r2.<init>()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.b.g.f1301a     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = "|read last domainMobileStatus = "
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r4 = com.igexin.push.core.g.az     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            com.igexin.b.a.c.a.b(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0442:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x044f
            r4 = r6
        L_0x044b:
            com.igexin.push.core.g.M = r4     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x044f:
            long r4 = java.lang.Long.parseLong(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x044b
        L_0x0454:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0466
            r2 = r3
        L_0x045d:
            com.igexin.push.d.b r4 = com.igexin.push.d.b.a()     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            r4.a(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0466:
            boolean r2 = java.lang.Boolean.parseBoolean(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x045d
        L_0x046b:
            java.lang.String r4 = "null"
            boolean r4 = r2.equals(r4)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            if (r4 == 0) goto L_0x0478
            r2 = r3
        L_0x0474:
            com.igexin.push.core.g.aC = r2     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0013
        L_0x0478:
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x006a, all -> 0x0241 }
            goto L_0x0474
        L_0x047d:
            if (r0 == 0) goto L_0x0070
            r0.close()
            goto L_0x0070
        L_0x0484:
            com.igexin.push.core.g.s = r1
            java.lang.String r0 = "null"
            com.igexin.push.core.g.r = r0
            com.igexin.push.core.g.q = r6
            goto L_0x0100
        L_0x048e:
            r0 = move-exception
            goto L_0x0245
        L_0x0491:
            r0 = move-exception
            r0 = r1
            goto L_0x006b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.b.g.b(android.database.sqlite.SQLiteDatabase):void");
    }

    public boolean b(int i) {
        if (com.igexin.push.core.g.aC == i) {
            return false;
        }
        com.igexin.push.core.g.aC = i;
        return c.b().a(new v(this), false, true);
    }

    public boolean b(long j) {
        com.igexin.push.core.g.a(j);
        return c.b().a(new z(this), false, true);
    }

    public boolean b(String str) {
        com.igexin.push.core.g.y = str;
        return c.b().a(new aa(this), false, true);
    }

    public boolean b(String str, boolean z) {
        String str2 = null;
        if (str == null) {
            return false;
        }
        if (z) {
            if (!str.equals(com.igexin.push.core.g.ax)) {
                if (!str.equals("null")) {
                    str2 = str;
                }
                com.igexin.push.core.g.ax = str2;
                return c.b().a(new r(this, str), false, true);
            }
        } else if (!str.equals(com.igexin.push.core.g.ay)) {
            if (!str.equals("null")) {
                str2 = str;
            }
            com.igexin.push.core.g.ay = str2;
            return c.b().a(new t(this, str), false, true);
        }
        return false;
    }

    public boolean b(boolean z) {
        return c.b().a(new w(this, z), false, true);
    }

    public void c(SQLiteDatabase sQLiteDatabase) {
        a(sQLiteDatabase, 1, com.igexin.b.a.a.a.d(String.valueOf(com.igexin.push.core.g.q).getBytes(), com.igexin.push.core.g.C));
        a(sQLiteDatabase, 4, String.valueOf(com.igexin.push.core.g.k));
        a(sQLiteDatabase, 8, String.valueOf(com.igexin.push.core.g.H));
        a(sQLiteDatabase, 6, String.valueOf(com.igexin.push.core.g.G));
        a(sQLiteDatabase, 32, String.valueOf(com.igexin.push.core.g.M));
        a(sQLiteDatabase, 3, com.igexin.push.core.g.B);
        a(sQLiteDatabase, 11, String.valueOf(com.igexin.push.core.g.K));
        a(sQLiteDatabase, 12, String.valueOf(com.igexin.push.core.g.L));
        a(sQLiteDatabase, 20, com.igexin.b.a.a.a.d(com.igexin.push.core.g.r.getBytes(), com.igexin.push.core.g.C));
        a(sQLiteDatabase, 2, com.igexin.push.core.g.y);
        a(sQLiteDatabase, 25, com.igexin.b.a.a.a.d(com.igexin.push.core.g.C.getBytes(), com.igexin.b.b.a.a(com.igexin.push.core.g.f.getPackageName())));
    }

    public boolean c() {
        com.igexin.push.core.g.q = 0;
        com.igexin.push.core.g.r = "null";
        return e();
    }

    public boolean c(long j) {
        if (com.igexin.push.core.g.L == j) {
            return false;
        }
        com.igexin.push.core.g.L = j;
        c.b().a(new ad(this), false, true);
        return true;
    }

    public boolean c(String str) {
        com.igexin.push.core.g.z = str;
        return c.b().a(new ab(this), false, true);
    }

    public Map<String, String> d() {
        return this.c;
    }

    public void d(SQLiteDatabase sQLiteDatabase) {
        this.d = true;
        e(sQLiteDatabase);
        j(sQLiteDatabase);
        k(sQLiteDatabase);
        i(sQLiteDatabase);
        f(sQLiteDatabase);
        g(sQLiteDatabase);
        h(sQLiteDatabase);
    }

    public boolean d(long j) {
        com.igexin.push.core.g.aw = j;
        a.b(f1301a + "|save idc config failed time : " + j);
        return c.b().a(new i(this, j), false, true);
    }

    public boolean d(String str) {
        com.igexin.push.core.g.A = str;
        return c.b().a(new ac(this), false, true);
    }

    public boolean e(long j) {
        if (com.igexin.push.core.g.K == j) {
            return false;
        }
        com.igexin.push.core.g.K = j;
        return c.b().a(new j(this), false, true);
    }

    public boolean e(String str) {
        if (str == null || str.equals(com.igexin.push.core.g.N)) {
            return false;
        }
        com.igexin.push.core.g.N = str;
        c.b().a(new k(this), false, true);
        return true;
    }

    public boolean f(long j) {
        if (com.igexin.push.core.g.P == j) {
            return false;
        }
        com.igexin.push.core.g.P = j;
        return c.b().a(new m(this), false, true);
    }

    public boolean f(String str) {
        if (str.equals(com.igexin.push.core.g.R)) {
            return false;
        }
        com.igexin.push.core.g.R = str;
        return c.b().a(new n(this), false, true);
    }

    public boolean g(long j) {
        if (com.igexin.push.core.g.M == j) {
            return false;
        }
        com.igexin.push.core.g.M = j;
        return c.b().a(new u(this), false, true);
    }
}
