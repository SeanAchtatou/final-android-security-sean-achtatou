package com.igexin.push.core.bean;

import android.os.Build;
import com.igexin.push.core.g;
import com.igexin.push.util.m;
import com.igexin.sdk.GTServiceManager;
import com.igexin.sdk.PushBuildConfig;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public String f1332a;

    /* renamed from: b  reason: collision with root package name */
    public String f1333b;
    public String c;
    public String d;
    public String e;
    public String f = "open";
    public String g;
    public String h;
    public String i;
    public String j;
    public String k;
    public String l;
    public String m;
    public long n;

    public a() {
        if (g.e != null) {
            this.f += ":" + g.e;
        }
        this.e = PushBuildConfig.sdk_conf_version;
        this.f1333b = g.u;
        this.c = g.t;
        this.d = g.w;
        this.i = g.x;
        this.f1332a = g.v;
        this.h = "ANDROID";
        this.j = "android" + Build.VERSION.RELEASE;
        this.k = "MDP";
        this.g = g.y;
        this.n = System.currentTimeMillis();
        this.l = g.z;
        this.m = Build.BRAND;
    }

    public static String a(a aVar) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("model", aVar.f1332a == null ? "" : aVar.f1332a);
        jSONObject.put("sim", aVar.f1333b == null ? "" : aVar.f1333b);
        jSONObject.put("imei", aVar.c == null ? "" : aVar.c);
        jSONObject.put("mac", aVar.d == null ? "" : aVar.d);
        jSONObject.put("version", aVar.e == null ? "" : aVar.e);
        jSONObject.put("channelid", aVar.f == null ? "" : aVar.f);
        jSONObject.put("type", "ANDROID");
        jSONObject.put(PushConstants.EXTRA_APPLICATION_PENDING_INTENT, aVar.k == null ? "" : aVar.k);
        jSONObject.put("deviceid", "ANDROID-" + (aVar.g == null ? "" : aVar.g));
        jSONObject.put("device_token", aVar.l == null ? "" : aVar.l);
        jSONObject.put("brand", aVar.m == null ? "" : aVar.m);
        jSONObject.put("system_version", aVar.j == null ? "" : aVar.j);
        jSONObject.put("cell", aVar.i == null ? "" : aVar.i);
        jSONObject.put("aid", m.b());
        jSONObject.put("adid", m.c());
        String name = GTServiceManager.getInstance().getUserPushService(g.f).getName();
        if (!com.igexin.push.core.a.n.equals(name)) {
            jSONObject.put("us", name);
        }
        JSONObject jSONObject2 = new JSONObject();
        jSONObject2.put("action", "addphoneinfo");
        jSONObject2.put("id", String.valueOf(aVar.n));
        jSONObject2.put("info", jSONObject);
        return jSONObject2.toString();
    }
}
