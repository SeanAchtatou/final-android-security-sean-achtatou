package com.igexin.push.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.igexin.b.a.c.a;
import com.igexin.sdk.PushConsts;

public class n extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private static n f1393a;

    private n() {
    }

    public static n a() {
        if (f1393a == null) {
            f1393a = new n();
        }
        return f1393a;
    }

    private void a(Intent intent) {
        try {
            a.b("----------------------------------------------------------------------------------");
            a.b("InternalPublicReceiver|action = " + intent.getAction() + ", component = " + intent.getComponent());
            Bundle extras = intent.getExtras();
            if (extras != null) {
                for (String next : extras.keySet()) {
                    a.b("InternalPublicReceiver|key [" + next + "]: " + extras.get(next));
                }
                return;
            }
            a.b("InternalPublicReceiver|no extras");
        } catch (Exception e) {
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (!(intent == null || intent.getAction() == null || !intent.getAction().equals(PushConsts.ACTION_BROADCAST_NETWORK_CHANGE))) {
            a(intent);
        }
        if (f.a() != null) {
            Message message = new Message();
            message.what = a.d;
            message.obj = intent;
            f.a().a(message);
        }
    }
}
