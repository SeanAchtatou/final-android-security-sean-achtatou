package com.igexin.push.core.a;

import com.igexin.b.a.c.a;
import com.igexin.push.config.l;
import org.json.JSONObject;

public class w extends b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1282a = (l.f1249a + "_SetTagResultAction");

    public boolean a(Object obj, JSONObject jSONObject) {
        a.b(f1282a + "|set tag result resp data = " + jSONObject);
        if (jSONObject == null) {
            return true;
        }
        try {
            if (!jSONObject.has("action") || !jSONObject.getString("action").equals("settag_result")) {
                return true;
            }
            e.a().b(jSONObject.getString("sn"), jSONObject.getString("error_code"));
            return true;
        } catch (Exception e) {
            a.b(f1282a + "|" + e.toString());
            return true;
        }
    }
}
