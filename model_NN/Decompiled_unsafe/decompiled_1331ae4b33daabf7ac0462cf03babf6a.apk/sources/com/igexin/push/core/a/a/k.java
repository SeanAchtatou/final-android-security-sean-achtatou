package com.igexin.push.core.a.a;

import android.content.Intent;
import android.content.pm.PackageManager;
import com.igexin.b.a.c.a;
import com.igexin.push.config.l;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.g;
import org.json.JSONException;
import org.json.JSONObject;

public class k implements a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1260a = l.f1249a;

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            com.igexin.push.core.bean.l lVar = new com.igexin.push.core.bean.l();
            lVar.setType("startapp");
            lVar.setActionId(jSONObject.getString("actionid"));
            lVar.setDoActionId(jSONObject.getString("do"));
            if (jSONObject.has("appstartupid")) {
                lVar.a(jSONObject.getJSONObject("appstartupid").getString("android"));
            }
            if (jSONObject.has("is_autostart")) {
                lVar.d(jSONObject.getString("is_autostart"));
            }
            if (jSONObject.has("appid")) {
                lVar.b(jSONObject.getString("appid"));
            }
            if (!jSONObject.has("noinstall_action")) {
                return lVar;
            }
            lVar.c(jSONObject.getString("noinstall_action"));
            return lVar;
        } catch (JSONException e) {
            return null;
        }
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        boolean z;
        boolean z2 = false;
        if (!(pushTaskBean == null || baseAction == null)) {
            com.igexin.push.core.bean.l lVar = (com.igexin.push.core.bean.l) baseAction;
            PackageManager packageManager = g.f.getPackageManager();
            String b2 = lVar.b();
            if (b2.equals("")) {
                b2 = g.f1382a;
                z = true;
            } else {
                z = g.f1382a.equals(lVar.b());
            }
            a.b("doStartApp|" + z + "|" + b2);
            if (z) {
                try {
                    e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), b2, (String) null);
                    if (((com.igexin.push.core.bean.l) baseAction).d().equals("true")) {
                        Intent launchIntentForPackage = packageManager.getLaunchIntentForPackage(g.e);
                        if (launchIntentForPackage == null) {
                            return false;
                        }
                        g.f.startActivity(launchIntentForPackage);
                    }
                    if (lVar.getDoActionId() != null) {
                        e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), lVar.getDoActionId());
                    }
                } catch (Exception e) {
                }
            } else {
                e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), b2, (String) null);
                if (!lVar.d().equals("true")) {
                    z2 = true;
                } else if (com.igexin.push.util.a.a(lVar.a())) {
                    Intent launchIntentForPackage2 = packageManager.getLaunchIntentForPackage(((com.igexin.push.core.bean.l) baseAction).a());
                    if (launchIntentForPackage2 == null) {
                        return false;
                    }
                    g.f.startActivity(launchIntentForPackage2);
                    z2 = true;
                }
                if (z2) {
                    if (lVar.getDoActionId() != null) {
                        e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), lVar.getDoActionId());
                    }
                } else if (lVar.c() != null) {
                    e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), lVar.c());
                }
            }
        }
        return true;
    }
}
