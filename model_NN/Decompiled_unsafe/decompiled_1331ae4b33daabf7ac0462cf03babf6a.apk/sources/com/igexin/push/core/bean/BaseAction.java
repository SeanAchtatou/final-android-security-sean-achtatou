package com.igexin.push.core.bean;

public class BaseAction {

    /* renamed from: a  reason: collision with root package name */
    private String f1328a;

    /* renamed from: b  reason: collision with root package name */
    private String f1329b;
    private String c;
    private boolean d = true;

    public String getActionId() {
        return this.f1328a;
    }

    public String getDoActionId() {
        return this.c;
    }

    public String getType() {
        return this.f1329b;
    }

    public boolean isSupportExt() {
        return this.d;
    }

    public void setActionId(String str) {
        this.f1328a = str;
    }

    public void setDoActionId(String str) {
        this.c = str;
    }

    public void setSupportExt(boolean z) {
        this.d = z;
    }

    public void setType(String str) {
        this.f1329b = str;
    }
}
