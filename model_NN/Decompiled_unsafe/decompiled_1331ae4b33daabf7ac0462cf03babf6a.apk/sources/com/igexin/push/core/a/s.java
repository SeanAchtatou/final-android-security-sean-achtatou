package com.igexin.push.core.a;

import android.content.Intent;
import android.text.TextUtils;
import com.igexin.b.a.b.c;
import com.igexin.b.a.c.a;
import com.igexin.push.c.i;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.config.l;
import com.igexin.push.config.m;
import com.igexin.push.core.c.f;
import com.igexin.push.core.g;
import com.igexin.push.f.a.e;
import com.igexin.push.util.EncryptUtils;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;

public class s extends b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1279a = (l.f1249a + "_RedirectServerAction");

    private void a(String str, String str2) {
        if (!TextUtils.isEmpty(str) && !str2.equals(str)) {
            a.b(f1279a + "|new location = " + str + "; pre location = " + str2 + ", send snl retire broadcast");
            Intent intent = new Intent();
            intent.setAction("com.igexin.sdk.action.snlretire");
            intent.putExtra("groupid", str2);
            intent.putExtra("branch", "open");
            g.f.sendBroadcast(intent);
        }
    }

    private void a(String str, JSONArray jSONArray) {
        try {
            a.b(f1279a + "|start fetch idc config, url : " + str);
            c.b().a(new e(new f(str, jSONArray)), false, true);
        } catch (Exception e) {
            a.b(f1279a + e.toString());
        }
    }

    public static String[] a(JSONArray jSONArray) {
        String[] strArr;
        Exception e;
        try {
            strArr = new String[jSONArray.length()];
            int i = 0;
            while (i < jSONArray.length()) {
                try {
                    strArr[i] = "http://" + jSONArray.getString(i);
                    i++;
                } catch (Exception e2) {
                    e = e2;
                    a.b(f1279a + "|parseIDCConfigURL exception" + e.toString());
                    return strArr;
                }
            }
        } catch (Exception e3) {
            Exception exc = e3;
            strArr = null;
            e = exc;
            a.b(f1279a + "|parseIDCConfigURL exception" + e.toString());
            return strArr;
        }
        return strArr;
    }

    public boolean a(Object obj, JSONObject jSONObject) {
        long j;
        a.b(f1279a + "|redirect server resp data : " + jSONObject);
        try {
            if (!jSONObject.has("action") || !jSONObject.getString("action").equals("redirect_server")) {
                return true;
            }
            String string = jSONObject.getString("delay");
            ArrayList arrayList = new ArrayList();
            JSONArray jSONArray = jSONObject.getJSONArray("address_list");
            a.b("redirect|" + string + "|" + jSONArray.toString());
            for (int i = 0; i < jSONArray.length(); i++) {
                String string2 = jSONArray.getString(i);
                int indexOf = string2.indexOf(44);
                if (indexOf > 0) {
                    String substring = string2.substring(0, indexOf);
                    String substring2 = string2.substring(indexOf + 1);
                    long currentTimeMillis = System.currentTimeMillis();
                    try {
                        long parseLong = Long.parseLong(substring2);
                        com.igexin.push.c.e eVar = new com.igexin.push.c.e();
                        eVar.f1213a = "socket://" + substring;
                        eVar.f1214b = (parseLong * 1000) + currentTimeMillis;
                        arrayList.add(eVar);
                    } catch (NumberFormatException e) {
                    }
                }
            }
            try {
                j = Long.parseLong(string) * 1000;
            } catch (Exception e2) {
                j = 0;
            }
            if (j >= 0) {
                g.E = j;
            }
            if (jSONObject.has("loc") && jSONObject.has("conf")) {
                try {
                    String string3 = jSONObject.getString("loc");
                    String str = g.d;
                    SDKUrlConfig.setLocation(string3);
                    if (m.n) {
                        a(string3, str);
                    }
                    a.b(f1279a + " set group id : " + g.d);
                    JSONArray jSONArray2 = jSONObject.getJSONArray("conf");
                    String[] a2 = a(jSONArray2);
                    if (a2 != null && a2.length > 0) {
                        String[] idcConfigUrl = SDKUrlConfig.getIdcConfigUrl();
                        if (idcConfigUrl != null && a2[0].equals(idcConfigUrl[0])) {
                            a.b(f1279a + "|current idc config url == new idc config url, return");
                        } else if (g.aw == 0) {
                            a(a2[0], jSONArray2);
                        } else if (System.currentTimeMillis() - g.aw > 7200000) {
                            a(a2[0], jSONArray2);
                        } else {
                            a.b(f1279a + "|get idc cfg last time less than 2 hours return");
                        }
                    }
                } catch (Exception e3) {
                    a.b(f1279a + e3.toString());
                }
            }
            i.a().e().a(arrayList);
            e.a().c(true);
            if (!EncryptUtils.isLoadSuccess()) {
                return true;
            }
            a.b(f1279a + "|redirect reInit so ~~~~~");
            EncryptUtils.reset();
            return true;
        } catch (Exception e4) {
            a.b(f1279a + e4.toString());
            return true;
        }
    }
}
