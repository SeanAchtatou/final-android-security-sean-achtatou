package com.igexin.push.core.a.a;

import android.app.PendingIntent;
import android.content.Intent;
import android.text.TextUtils;
import com.igexin.b.a.c.a;
import com.igexin.push.config.l;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.i;
import com.igexin.push.core.g;
import java.util.HashMap;
import java.util.Random;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class h implements a {

    /* renamed from: a  reason: collision with root package name */
    public static HashMap<String, String> f1256a = new HashMap<>();

    /* renamed from: b  reason: collision with root package name */
    private static final String f1257b = l.f1249a;

    private int a(i iVar) {
        int identifier = g.f.getResources().getIdentifier("push", "drawable", g.e);
        if (iVar.f() == null) {
            if (identifier != 0) {
                return identifier;
            }
            return 17301651;
        } else if ("null".equals(iVar.f())) {
            return 17301651;
        } else {
            if (iVar.f().startsWith("@")) {
                String f = iVar.f();
                return f.substring(1, f.length()).endsWith("email") ? 17301647 : 17301651;
            }
            int identifier2 = g.f.getResources().getIdentifier(iVar.f(), "drawable", g.e);
            if (identifier2 == 0) {
                identifier2 = g.f.getResources().getIdentifier(iVar.f(), "mipmap", g.e);
            }
            if (identifier2 != 0) {
                return identifier2;
            }
            return 17301651;
        }
    }

    private PendingIntent a(String str, String str2, String str3, int i) {
        Intent intent = new Intent("com.igexin.sdk.action.doaction");
        intent.putExtra("taskid", str);
        intent.putExtra("messageid", str2);
        intent.putExtra("appid", g.f1382a);
        intent.putExtra("actionid", str3);
        intent.putExtra("accesstoken", g.au);
        intent.putExtra("notifID", i);
        return PendingIntent.getBroadcast(g.f, new Random().nextInt(1000), intent, NTLMConstants.FLAG_UNIDENTIFIED_10);
    }

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            i iVar = new i();
            iVar.setType("notification");
            iVar.setActionId(jSONObject.getString("actionid"));
            iVar.setDoActionId(jSONObject.getString("do"));
            String string = jSONObject.getString("title");
            String string2 = jSONObject.getString("text");
            iVar.a(string);
            iVar.b(string2);
            if (jSONObject.has("logo") && !"".equals(jSONObject.getString("logo"))) {
                String string3 = jSONObject.getString("logo");
                if (string3.lastIndexOf(".png") == -1 && string3.lastIndexOf(".jpeg") == -1) {
                    string3 = "null";
                } else {
                    int indexOf = string3.indexOf(".png");
                    if (indexOf == -1) {
                        indexOf = string3.indexOf(".jpeg");
                    }
                    if (indexOf != -1) {
                        string3 = string3.substring(0, indexOf);
                    }
                }
                iVar.c(string3);
            }
            if (jSONObject.has("is_noclear")) {
                iVar.a(jSONObject.getBoolean("is_noclear"));
            }
            if (jSONObject.has("is_novibrate")) {
                iVar.b(jSONObject.getBoolean("is_novibrate"));
            }
            if (jSONObject.has("is_noring")) {
                iVar.c(jSONObject.getBoolean("is_noring"));
            }
            if (jSONObject.has("is_chklayout")) {
                iVar.d(jSONObject.getBoolean("is_chklayout"));
            }
            if (jSONObject.has("logo_url")) {
                iVar.d(jSONObject.getString("logo_url"));
            }
            if (jSONObject.has("banner_url")) {
                iVar.e(jSONObject.getString("banner_url"));
            }
            return iVar;
        } catch (JSONException e) {
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0155  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0094  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r13, java.lang.String r14, com.igexin.push.core.bean.i r15) {
        /*
            r12 = this;
            r10 = 4
            r3 = 0
            r9 = 1
            long r0 = java.lang.System.currentTimeMillis()
            int r4 = (int) r0
            java.util.Map<java.lang.String, java.lang.Integer> r0 = com.igexin.push.core.g.aj
            java.lang.Integer r1 = java.lang.Integer.valueOf(r4)
            r0.put(r13, r1)
            java.lang.String r0 = r15.getDoActionId()
            android.app.PendingIntent r5 = r12.a(r13, r14, r0, r4)
            android.content.Context r0 = com.igexin.push.core.g.f
            java.lang.String r1 = "notification"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.app.NotificationManager r0 = (android.app.NotificationManager) r0
            int r2 = r12.a(r15)
            int r1 = android.os.Build.VERSION.SDK_INT
            r6 = 11
            if (r1 >= r6) goto L_0x00df
            android.app.Notification r1 = new android.app.Notification
            r1.<init>()
            r1.icon = r2
            java.lang.String r2 = "android.app.Notification"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x00c5 }
            java.lang.String r3 = "setLatestEventInfo"
            r6 = 4
            java.lang.Class[] r6 = new java.lang.Class[r6]     // Catch:{ Exception -> 0x00c5 }
            r7 = 0
            java.lang.Class<android.content.Context> r8 = android.content.Context.class
            r6[r7] = r8     // Catch:{ Exception -> 0x00c5 }
            r7 = 1
            java.lang.Class<java.lang.CharSequence> r8 = java.lang.CharSequence.class
            r6[r7] = r8     // Catch:{ Exception -> 0x00c5 }
            r7 = 2
            java.lang.Class<java.lang.CharSequence> r8 = java.lang.CharSequence.class
            r6[r7] = r8     // Catch:{ Exception -> 0x00c5 }
            r7 = 3
            java.lang.Class<android.app.PendingIntent> r8 = android.app.PendingIntent.class
            r6[r7] = r8     // Catch:{ Exception -> 0x00c5 }
            java.lang.reflect.Method r2 = r2.getMethod(r3, r6)     // Catch:{ Exception -> 0x00c5 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Exception -> 0x00c5 }
            r3 = 4
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00c5 }
            r6 = 0
            android.content.Context r7 = com.igexin.push.core.g.f     // Catch:{ Exception -> 0x00c5 }
            r3[r6] = r7     // Catch:{ Exception -> 0x00c5 }
            r6 = 1
            java.lang.String r7 = r15.a()     // Catch:{ Exception -> 0x00c5 }
            r3[r6] = r7     // Catch:{ Exception -> 0x00c5 }
            r6 = 2
            java.lang.String r7 = r15.b()     // Catch:{ Exception -> 0x00c5 }
            r3[r6] = r7     // Catch:{ Exception -> 0x00c5 }
            r6 = 3
            r3[r6] = r5     // Catch:{ Exception -> 0x00c5 }
            r2.invoke(r1, r3)     // Catch:{ Exception -> 0x00c5 }
        L_0x0077:
            java.lang.String r2 = r15.b()
            r1.tickerText = r2
            r1.defaults = r10
            r2 = -16711936(0xffffffffff00ff00, float:-1.7146522E38)
            r1.ledARGB = r2
            r2 = 1000(0x3e8, float:1.401E-42)
            r1.ledOnMS = r2
            r2 = 3000(0xbb8, float:4.204E-42)
            r1.ledOffMS = r2
            r1.flags = r9
            boolean r2 = r15.c()
            if (r2 != 0) goto L_0x019a
            int r2 = r1.flags
            r2 = r2 | 16
            r1.flags = r2
        L_0x009a:
            boolean r2 = r15.e()
            if (r2 != 0) goto L_0x00a6
            int r2 = r1.defaults
            r2 = r2 | 1
            r1.defaults = r2
        L_0x00a6:
            boolean r2 = r15.d()
            if (r2 != 0) goto L_0x00b2
            int r2 = r1.defaults
            r2 = r2 | 2
            r1.defaults = r2
        L_0x00b2:
            java.lang.String r2 = r15.h()
            if (r2 != 0) goto L_0x00be
            java.lang.String r2 = r15.g()
            if (r2 == 0) goto L_0x01a2
        L_0x00be:
            boolean r2 = r15.i()
            if (r2 == 0) goto L_0x01a2
        L_0x00c4:
            return
        L_0x00c5:
            r0 = move-exception
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.igexin.push.core.a.a.h.f1257b
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "reflect invoke setLatestEventInfo failed!"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x00c4
        L_0x00df:
            android.content.Context r1 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x0176 }
            android.content.res.Resources r1 = r1.getResources()     // Catch:{ Throwable -> 0x0176 }
            java.lang.String r6 = "push_small"
            java.lang.String r7 = "drawable"
            java.lang.String r8 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x0176 }
            int r1 = r1.getIdentifier(r6, r7, r8)     // Catch:{ Throwable -> 0x0176 }
            if (r1 != 0) goto L_0x0101
            android.content.Context r3 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x01a7 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ Throwable -> 0x01a7 }
            java.lang.String r6 = "push_small"
            java.lang.String r7 = "mipmap"
            java.lang.String r8 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x01a7 }
            int r1 = r3.getIdentifier(r6, r7, r8)     // Catch:{ Throwable -> 0x01a7 }
        L_0x0101:
            if (r1 != 0) goto L_0x011b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = com.igexin.push.core.a.a.h.f1257b
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r6 = "|push_small.png is missing"
            java.lang.StringBuilder r3 = r3.append(r6)
            java.lang.String r3 = r3.toString()
            com.igexin.b.a.c.a.b(r3)
        L_0x011b:
            android.app.Notification$Builder r3 = new android.app.Notification$Builder
            android.content.Context r6 = com.igexin.push.core.g.f
            r3.<init>(r6)
            java.lang.String r6 = r15.a()
            android.app.Notification$Builder r3 = r3.setContentTitle(r6)
            java.lang.String r6 = r15.b()
            android.app.Notification$Builder r3 = r3.setContentText(r6)
            if (r1 != 0) goto L_0x0135
            r1 = r2
        L_0x0135:
            android.app.Notification$Builder r1 = r3.setSmallIcon(r1)
            android.content.Context r3 = com.igexin.push.core.g.f
            android.content.res.Resources r3 = r3.getResources()
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeResource(r3, r2)
            android.app.Notification$Builder r1 = r1.setLargeIcon(r2)
            android.app.Notification$Builder r1 = r1.setContentIntent(r5)
            android.app.Notification r1 = r1.getNotification()
            boolean r2 = com.igexin.push.util.a.f()
            if (r2 == 0) goto L_0x0077
            java.lang.String r2 = "com.android.internal.R$id"
            java.lang.Class r2 = java.lang.Class.forName(r2)     // Catch:{ Exception -> 0x0173 }
            java.lang.String r3 = "right_icon"
            java.lang.reflect.Field r2 = r2.getField(r3)     // Catch:{ Exception -> 0x0173 }
            r3 = 1
            r2.setAccessible(r3)     // Catch:{ Exception -> 0x0173 }
            r3 = 0
            int r2 = r2.getInt(r3)     // Catch:{ Exception -> 0x0173 }
            android.widget.RemoteViews r3 = r1.contentView     // Catch:{ Exception -> 0x0173 }
            r5 = 8
            r3.setViewVisibility(r2, r5)     // Catch:{ Exception -> 0x0173 }
            goto L_0x0077
        L_0x0173:
            r2 = move-exception
            goto L_0x0077
        L_0x0176:
            r1 = move-exception
        L_0x0177:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = com.igexin.push.core.a.a.h.f1257b
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = "|"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r1 = r1.toString()
            java.lang.StringBuilder r1 = r6.append(r1)
            java.lang.String r1 = r1.toString()
            com.igexin.b.a.c.a.b(r1)
            r1 = r3
            goto L_0x0101
        L_0x019a:
            int r2 = r1.flags
            r2 = r2 | 32
            r1.flags = r2
            goto L_0x009a
        L_0x01a2:
            r0.notify(r4, r1)
            goto L_0x00c4
        L_0x01a7:
            r3 = move-exception
            r11 = r3
            r3 = r1
            r1 = r11
            goto L_0x0177
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.a.h.a(java.lang.String, java.lang.String, com.igexin.push.core.bean.i):void");
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        if (pushTaskBean == null || baseAction == null || !(baseAction instanceof i)) {
            return true;
        }
        i iVar = (i) baseAction;
        if (TextUtils.isEmpty(iVar.a()) || TextUtils.isEmpty(iVar.b())) {
            a.a(f1257b, "title = " + iVar.a() + ", content = " + iVar.b() + ", is invalid, don't show");
            a.b(f1257b + " title = " + iVar.a() + ", content = " + iVar.b() + ", is invalid, don't show");
            return true;
        }
        a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), iVar);
        return true;
    }
}
