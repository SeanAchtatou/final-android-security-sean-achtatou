package com.igexin.push.core;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.b.b.a;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.config.m;
import com.igexin.push.config.q;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.d.c.c;
import com.igexin.push.f.b.h;
import com.igexin.sdk.a.d;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicBoolean;

public class g {
    public static String A;
    public static String B;
    public static String C;
    public static String D = "";
    public static long E = -1;
    public static long F = -1;
    public static long G = 0;
    public static long H = 0;
    public static long I = 0;
    public static long J = 0;
    public static long K = 0;
    public static long L = 0;
    public static long M = 0;
    public static String N = null;
    public static boolean O = q.f1253a.equals("debug");
    public static long P = 0;
    public static long Q = 0;
    public static String R;
    public static long S = 0;
    public static int T = 0;
    public static long U;
    public static String V;
    public static String W;
    public static String X;
    public static String Y;
    public static String Z;

    /* renamed from: a  reason: collision with root package name */
    public static String f1382a = "";
    public static String aA;
    public static boolean aB;
    public static int aC;
    private static HashMap<String, String> aD = new HashMap<>();
    private static Map<String, Integer> aE;
    public static String aa;
    public static String ab;
    public static String ac;
    public static String ad;
    public static byte[] ae;
    public static boolean af;
    public static boolean ag;
    public static boolean ah;
    public static Map<String, PushTaskBean> ai;
    public static Map<String, Integer> aj;
    public static HashMap<String, Timer> ak;
    public static HashMap<String, c> al;
    public static HashMap<String, Long> am;
    public static int an = 0;
    public static Map<String, String> ao;
    public static int ap = 0;
    public static int aq = 0;
    public static int ar = 0;
    public static com.igexin.push.core.bean.g as;
    public static boolean at;
    public static String au;
    public static h av;
    public static long aw;
    public static String ax;
    public static String ay;
    public static String az;

    /* renamed from: b  reason: collision with root package name */
    public static String f1383b = "";
    public static String c = "";
    public static String d = "";
    public static String e = "";
    public static Context f;
    public static AtomicBoolean g = new AtomicBoolean(false);
    public static boolean h = true;
    public static volatile boolean i;
    public static volatile boolean j;
    public static boolean k = true;
    public static volatile boolean l;
    public static volatile boolean m;
    public static boolean n = true;
    public static int o = 0;
    public static int p = 0;
    public static long q = 0;
    public static String r;
    public static String s;
    public static String t;
    public static String u;
    public static String v;
    public static String w;
    public static String x;
    public static String y;
    public static String z;

    public static int a(String str, boolean z2) {
        int intValue;
        synchronized (g.class) {
            if (aE.get(str) == null) {
                aE.put(str, 0);
            }
            intValue = aE.get(str).intValue();
            if (z2) {
                intValue--;
                aE.put(str, Integer.valueOf(intValue));
                if (intValue == 0) {
                    aE.remove(str);
                }
            }
        }
        return intValue;
    }

    public static String a() {
        return SDKUrlConfig.getConfigServiceUrl();
    }

    public static void a(long j2) {
        q = j2;
        r = a.a(String.valueOf(q));
    }

    public static boolean a(Context context) {
        boolean z2 = false;
        f = context;
        e = context.getPackageName();
        if (!f()) {
            com.igexin.b.a.c.a.b("CoreRuntimeInfo|parseManifests failed");
            return false;
        }
        ae = a.a(f1382a + c + f1383b + context.getPackageName()).getBytes();
        e();
        d();
        g();
        ConnectivityManager j2 = f.a().j();
        if (j2 != null) {
            NetworkInfo activeNetworkInfo = j2.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                z2 = true;
            }
            h = z2;
        } else {
            h = false;
        }
        ai = new HashMap();
        aj = new HashMap();
        ak = new HashMap<>();
        al = new HashMap<>();
        am = new HashMap<>();
        ao = new HashMap();
        U = System.currentTimeMillis();
        V = "com.igexin.sdk.action.snlresponse." + e;
        i = new d(context).c();
        j = new com.igexin.sdk.a.c(context).c();
        aE = new HashMap();
        aB = true;
        com.igexin.b.a.c.a.b("CoreRuntimeInfo", "getui sdk init success");
        com.igexin.b.a.c.a.b("CoreRuntimeInfo|getui sdk init success ##########");
        return true;
    }

    public static boolean a(String str, Integer num, boolean z2) {
        boolean z3;
        synchronized (g.class) {
            int intValue = num.intValue();
            if (!z2 || aE.get(str) == null || (intValue = aE.get(str).intValue() + num.intValue()) != 0) {
                aE.put(str, Integer.valueOf(intValue));
                z3 = true;
            } else {
                aE.remove(str);
                z3 = false;
            }
        }
        return z3;
    }

    public static HashMap<String, String> b() {
        return aD;
    }

    private static HashMap<String, String> c() {
        if (!new File(aa).exists()) {
            return null;
        }
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(aa));
            HashMap<String, String> hashMap = (HashMap) objectInputStream.readObject();
            try {
                objectInputStream.close();
                return hashMap;
            } catch (Exception e2) {
                return hashMap;
            }
        } catch (Exception e3) {
            return null;
        }
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void d() {
        /*
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = "/sdcard/libs/"
            r0.<init>(r1)     // Catch:{ Throwable -> 0x00ed }
            boolean r1 = r0.exists()     // Catch:{ Throwable -> 0x00ed }
            if (r1 == 0) goto L_0x001b
            boolean r1 = r0.isFile()     // Catch:{ Throwable -> 0x00ed }
            if (r1 == 0) goto L_0x001b
            java.lang.String r1 = "CoreRuntimeInfo|libs is file not directory, delete libs file ++++"
            com.igexin.b.a.c.a.b(r1)     // Catch:{ Throwable -> 0x00ed }
            r0.delete()     // Catch:{ Throwable -> 0x00ed }
        L_0x001b:
            boolean r1 = r0.exists()     // Catch:{ Throwable -> 0x00ed }
            if (r1 != 0) goto L_0x002c
            boolean r0 = r0.mkdir()     // Catch:{ Throwable -> 0x00ed }
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = "CoreRuntimeInfo|create libs directory failed ++++++"
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x00ed }
        L_0x002c:
            android.content.Context r0 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x00ed }
            r0.getFilesDir()     // Catch:{ Throwable -> 0x00ed }
            java.io.File r0 = new java.io.File     // Catch:{ Throwable -> 0x00d0 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00d0 }
            r1.<init>()     // Catch:{ Throwable -> 0x00d0 }
            java.io.File r2 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Throwable -> 0x00d0 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Throwable -> 0x00d0 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00d0 }
            java.lang.String r2 = "/system/tmp/local"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00d0 }
            java.lang.String r1 = r1.toString()     // Catch:{ Throwable -> 0x00d0 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x00d0 }
            boolean r1 = r0.exists()     // Catch:{ Throwable -> 0x00d0 }
            if (r1 != 0) goto L_0x005a
            r0.mkdirs()     // Catch:{ Throwable -> 0x00d0 }
        L_0x005a:
            java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ Throwable -> 0x00d0 }
            com.igexin.push.core.g.ab = r0     // Catch:{ Throwable -> 0x00d0 }
        L_0x0060:
            android.content.Context r0 = com.igexin.push.core.g.f     // Catch:{ Throwable -> 0x00ed }
            java.io.File r0 = r0.getFilesDir()     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.getPath()     // Catch:{ Throwable -> 0x00ed }
            com.igexin.push.core.g.ac = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ed }
            r0.<init>()     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = "/sdcard/libs//"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = ".db"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ed }
            com.igexin.push.core.g.X = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = "/sdcard/libs//com.igexin.sdk.deviceId.db"
            com.igexin.push.core.g.Y = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = "/sdcard/libs//app.db"
            com.igexin.push.core.g.Z = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = "/sdcard/libs//imsi.db"
            com.igexin.push.core.g.aa = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ed }
            r0.<init>()     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = "/sdcard/libs//"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = ".properties"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ed }
            com.igexin.push.core.g.W = r0     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ed }
            r0.<init>()     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = "/sdcard/libs//"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = com.igexin.push.core.g.e     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r1 = ".bin"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ed }
            com.igexin.push.core.g.ad = r0     // Catch:{ Throwable -> 0x00ed }
        L_0x00cf:
            return
        L_0x00d0:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x00ed }
            r1.<init>()     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r2 = "CoreRuntimeInfo|"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ed }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Throwable -> 0x00ed }
            java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x00ed }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Throwable -> 0x00ed }
            goto L_0x0060
        L_0x00ed:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "CoreRuntimeInfo|initFile exception = "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.g.d():void");
    }

    private static void e() {
        try {
            PackageInfo packageInfo = f.getPackageManager().getPackageInfo(e, 4096);
            if (packageInfo != null && packageInfo.requestedPermissions != null) {
                for (String str : packageInfo.requestedPermissions) {
                }
            }
        } catch (Exception e2) {
            com.igexin.b.a.c.a.b("CoreRuntimeInfo|init exception : " + e2.toString());
        }
    }

    private static boolean f() {
        try {
            ApplicationInfo applicationInfo = f.getPackageManager().getApplicationInfo(e, 128);
            if (applicationInfo == null || applicationInfo.metaData == null) {
                return false;
            }
            String string = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPID);
            String string2 = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPSECRET);
            String string3 = applicationInfo.metaData.getString(AssistPushConsts.GETUI_APPKEY);
            if (string != null) {
                string = string.trim();
            }
            if (string2 != null) {
                string2 = string2.trim();
            }
            if (string3 != null) {
                string3 = string3.trim();
            }
            if (TextUtils.isEmpty(string) || TextUtils.isEmpty(string2) || TextUtils.isEmpty(string3)) {
                com.igexin.b.a.c.a.b("CoreRuntimeInfo|getui sdk init error, missing parm #####");
                return false;
            }
            f1382a = string;
            f1383b = string3;
            c = string2;
            d = SDKUrlConfig.getLocation();
            return true;
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("CoreRuntimeInfo|get ApplicationInfo meta data exception :" + th.toString());
            return false;
        }
    }

    private static void g() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) f.getSystemService("phone");
            t = telephonyManager.getDeviceId();
            u = telephonyManager.getSubscriberId();
            if (TextUtils.isEmpty(x)) {
                if (m.g) {
                    HashMap<String, String> c2 = c();
                    if (c2 != null && !TextUtils.isEmpty(u)) {
                        x = c2.get(u);
                    }
                    if (x == null || !x.equals("")) {
                        x = null;
                    }
                } else {
                    x = null;
                }
            }
            v = Build.MODEL;
        } catch (Throwable th) {
        }
    }
}
