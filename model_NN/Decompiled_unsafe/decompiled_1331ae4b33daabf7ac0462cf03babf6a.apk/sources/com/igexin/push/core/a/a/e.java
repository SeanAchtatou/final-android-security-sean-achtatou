package com.igexin.push.core.a.a;

import com.igexin.push.core.b;
import com.igexin.push.core.b.g;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import org.json.JSONException;
import org.json.JSONObject;

public class e implements a {
    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            if (jSONObject.has("do") && jSONObject.has("actionid") && jSONObject.has("duration")) {
                com.igexin.push.core.bean.e eVar = new com.igexin.push.core.bean.e();
                eVar.setType(jSONObject.getString("type"));
                eVar.setActionId(jSONObject.getString("actionid"));
                eVar.setDoActionId(jSONObject.getString("do"));
                if (!jSONObject.has("duration")) {
                    return eVar;
                }
                eVar.a(Long.valueOf(jSONObject.getString("duration")).longValue());
                return eVar;
            }
        } catch (JSONException e) {
        }
        return null;
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        long currentTimeMillis = System.currentTimeMillis() + (((com.igexin.push.core.bean.e) baseAction).a() * 1000);
        g.a().a(true);
        g.a().f(currentTimeMillis);
        return true;
    }
}
