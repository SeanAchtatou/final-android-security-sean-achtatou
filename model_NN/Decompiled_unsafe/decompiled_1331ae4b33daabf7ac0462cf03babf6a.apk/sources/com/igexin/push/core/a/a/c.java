package com.igexin.push.core.a.a;

import android.os.Process;
import com.igexin.push.config.a;
import com.igexin.push.config.m;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.d;
import com.igexin.push.core.bean.f;
import com.igexin.push.util.e;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class c implements a {
    private boolean a(f fVar) {
        String c = fVar.c();
        return c != null && e.b(c);
    }

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        if (jSONObject.has("ids")) {
            try {
                JSONArray jSONArray = new JSONArray(jSONObject.getString("ids"));
                if (jSONArray.length() > 0) {
                    int[] iArr = new int[jSONArray.length()];
                    for (int i = 0; i < jSONArray.length(); i++) {
                        iArr[i] = jSONArray.getInt(i);
                    }
                    d dVar = new d();
                    dVar.setType("cleanext");
                    dVar.a(iArr);
                    dVar.setActionId(jSONObject.getString("actionid"));
                    dVar.setDoActionId(jSONObject.getString("do"));
                    return dVar;
                }
            } catch (Exception e) {
            }
        }
        return null;
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        if (m.s == null || m.s.b() == null || m.s.b().size() == 0) {
            return false;
        }
        if (!(pushTaskBean == null || baseAction == null)) {
            d dVar = (d) baseAction;
            Map<Integer, f> b2 = m.s.b();
            int[] a2 = dVar.a();
            if (a2 == null || a2.length <= 0) {
                z = false;
            } else {
                int i = 0;
                z = false;
                while (i < dVar.a().length) {
                    if (b2.containsKey(Integer.valueOf(a2[i]))) {
                        a(b2.get(Integer.valueOf(a2[i])));
                        b2.remove(Integer.valueOf(a2[i]));
                        z3 = true;
                        z2 = true;
                    } else {
                        z2 = z;
                        z3 = z4;
                    }
                    i++;
                    z4 = z3;
                    z = z2;
                }
                if (z4) {
                    a.a().g();
                }
            }
            if (z) {
                Process.killProcess(Process.myPid());
            }
        }
        if (baseAction.getDoActionId().equals("")) {
            return true;
        }
        com.igexin.push.core.a.e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), baseAction.getDoActionId());
        return true;
    }
}
