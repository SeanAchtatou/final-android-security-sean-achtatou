package com.igexin.push.core.a;

import com.igexin.b.a.c.a;
import com.igexin.push.core.b.c;
import com.igexin.push.core.g;
import com.igexin.push.d.c.p;
import org.json.JSONObject;

public class q extends b {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean
     arg types: [org.json.JSONObject, byte[], int]
     candidates:
      com.igexin.push.core.a.e.a(int, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.a.e, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(int, int, java.lang.String):void
      com.igexin.push.core.a.e.a(com.igexin.push.core.bean.PushTaskBean, java.lang.String, java.lang.String):void
      com.igexin.push.core.a.e.a(java.lang.String, com.igexin.push.d.c.a, com.igexin.push.core.bean.PushTaskBean):void
      com.igexin.push.core.a.e.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.igexin.push.core.a.e.a(org.json.JSONObject, byte[], boolean):boolean */
    public boolean a(Object obj, JSONObject jSONObject) {
        try {
            p pVar = (p) obj;
            if (jSONObject.has("action") && jSONObject.getString("action").equals("pushmessage")) {
                byte[] bArr = (pVar.f == null || !(pVar.f instanceof byte[])) ? null : (byte[]) pVar.f;
                String string = jSONObject.getString("taskid");
                if (g.ak.containsKey(string)) {
                    g.ak.get(string).cancel();
                    g.ak.remove(string);
                }
                a.b("getui receive message : " + jSONObject.toString());
                if (com.igexin.assist.sdk.a.c(g.f) || com.igexin.assist.sdk.a.b(g.f) || com.igexin.assist.sdk.a.d(g.f)) {
                    c cVar = new c(g.f);
                    if (!cVar.a(string)) {
                        cVar.b(string);
                        cVar.a();
                        e.a().a(jSONObject, bArr, true);
                    }
                } else {
                    e.a().a(jSONObject, bArr, true);
                }
            }
        } catch (Exception e) {
            a.b("PushmessageAction|" + e.toString());
        }
        return true;
    }
}
