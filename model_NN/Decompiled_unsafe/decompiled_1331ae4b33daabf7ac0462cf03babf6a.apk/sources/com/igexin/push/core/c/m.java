package com.igexin.push.core.c;

import cn.banshenggua.aichang.room.message.SocketMessage;
import com.igexin.b.a.b.f;
import com.igexin.push.config.SDKUrlConfig;
import com.igexin.push.core.b.ae;
import com.igexin.push.core.bean.k;
import com.igexin.push.core.g;
import com.igexin.push.f.a.b;
import java.util.ArrayList;
import org.json.JSONObject;

public class m extends b {

    /* renamed from: a  reason: collision with root package name */
    public static final String f1372a = m.class.getName();
    private String g;
    private ArrayList<k> h;

    public m(byte[] bArr, String str, ArrayList<k> arrayList) {
        super(SDKUrlConfig.getBiUploadServiceUrl());
        a(bArr, str, arrayList);
    }

    private void a(byte[] bArr, String str, ArrayList<k> arrayList) {
        this.g = str;
        this.h = arrayList;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("action", "upload_BI");
            jSONObject.put("BIType", str);
            jSONObject.put("cid", g.r);
            jSONObject.put("BIData", new String(f.f(bArr, 0), "UTF-8"));
            b(jSONObject.toString().getBytes());
        } catch (Exception e) {
        }
    }

    public void a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject(new String(bArr));
        if (jSONObject.has(SocketMessage.MSG_RESULE_KEY) && "ok".equals(jSONObject.getString(SocketMessage.MSG_RESULE_KEY))) {
            ae.a().a(this.g, this.h);
        }
    }

    public int b() {
        return 0;
    }
}
