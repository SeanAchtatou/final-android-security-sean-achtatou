package com.igexin.push.core.a.a;

import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.ServiceInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.push.config.m;
import com.igexin.push.core.a;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b;
import com.igexin.push.core.bean.BaseAction;
import com.igexin.push.core.bean.PushTaskBean;
import com.igexin.push.core.bean.o;
import com.igexin.push.core.f;
import com.igexin.push.core.g;
import com.igexin.sdk.PushConsts;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class l implements a {

    /* renamed from: b  reason: collision with root package name */
    private static final String f1261b = a.n;
    private static final String c = a.p;
    private static final String d = a.o;

    /* renamed from: a  reason: collision with root package name */
    private PackageManager f1262a;

    private String a(String str) {
        try {
            List<PackageInfo> installedPackages = g.f.getPackageManager().getInstalledPackages(4);
            if (installedPackages != null) {
                for (PackageInfo next : installedPackages) {
                    if (str.equals(next.packageName)) {
                        for (ServiceInfo serviceInfo : next.services) {
                            if (f1261b.equals(serviceInfo.name) || d.equals(serviceInfo.name) || c.equals(serviceInfo.name)) {
                                return serviceInfo.name;
                            }
                        }
                        continue;
                    }
                }
            }
        } catch (Exception e) {
            com.igexin.b.a.c.a.b(e.toString());
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x008e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x008f, code lost:
        r4 = r5;
        r11 = r0;
        r0 = r1;
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x018a, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x00b7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00af A[SYNTHETIC, Splitter:B:31:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00b4 A[SYNTHETIC, Splitter:B:34:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x018a A[ExcHandler: all (th java.lang.Throwable), Splitter:B:22:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x018d A[SYNTHETIC, Splitter:B:74:0x018d] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0192 A[SYNTHETIC, Splitter:B:77:0x0192] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<java.lang.String> a(int r14, java.lang.String r15) {
        /*
            r13 = this;
            java.io.File r6 = new java.io.File
            java.lang.String r0 = "/sdcard/libs/"
            r6.<init>(r0)
            boolean r0 = r6.exists()
            if (r0 != 0) goto L_0x000f
            r1 = 0
        L_0x000e:
            return r1
        L_0x000f:
            java.lang.String[] r7 = r6.list()
            if (r7 != 0) goto L_0x0017
            r1 = 0
            goto L_0x000e
        L_0x0017:
            r1 = 0
            r0 = 0
            r2 = r0
        L_0x001a:
            int r0 = r7.length
            if (r2 >= r0) goto L_0x0201
            r0 = r7[r2]
            java.lang.String r3 = ".db"
            int r0 = r0.indexOf(r3)
            if (r0 <= 0) goto L_0x024b
            r0 = r7[r2]
            java.lang.String r3 = "app.db"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x024b
            r0 = r7[r2]
            java.lang.String r3 = "imsi.db"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x024b
            r0 = r7[r2]
            java.lang.String r3 = "com.igexin.sdk.deviceId.db"
            boolean r0 = r0.equals(r3)
            if (r0 != 0) goto L_0x024b
            r3 = 0
            r4 = 0
            r0 = r7[r2]     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r5 = 0
            r8 = r7[r2]     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            int r8 = r8.length()     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            int r8 = r8 + -3
            java.lang.String r8 = r0.substring(r5, r8)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r5.<init>()     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.lang.String r9 = "/"
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r9 = r7[r2]     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.lang.StringBuilder r5 = r5.append(r9)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r0.<init>(r5)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r5 = 1024(0x400, float:1.435E-42)
            byte[] r9 = new byte[r5]     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            r5.<init>(r0)     // Catch:{ Exception -> 0x0236, all -> 0x022d }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x023f, all -> 0x0232 }
            r3.<init>()     // Catch:{ Exception -> 0x023f, all -> 0x0232 }
        L_0x0082:
            int r0 = r5.read(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r4 = -1
            if (r0 == r4) goto L_0x00bd
            r4 = 0
            r3.write(r9, r4, r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            goto L_0x0082
        L_0x008e:
            r0 = move-exception
            r4 = r5
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x0093:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0229 }
            r5.<init>()     // Catch:{ all -> 0x0229 }
            java.lang.String r8 = "WakeupAction"
            java.lang.StringBuilder r5 = r5.append(r8)     // Catch:{ all -> 0x0229 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0229 }
            java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ all -> 0x0229 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0229 }
            com.igexin.b.a.c.a.b(r1)     // Catch:{ all -> 0x0229 }
            if (r4 == 0) goto L_0x00b2
            r4.close()     // Catch:{ IOException -> 0x01eb }
        L_0x00b2:
            if (r3 == 0) goto L_0x00b7
            r3.close()     // Catch:{ IOException -> 0x01f1 }
        L_0x00b7:
            int r1 = r2 + 1
            r2 = r1
            r1 = r0
            goto L_0x001a
        L_0x00bd:
            byte[] r4 = r3.toByteArray()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r0 = com.igexin.push.core.g.t     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 != 0) goto L_0x0179
            java.lang.String r0 = "cantgetimei"
        L_0x00c7:
            java.lang.String r0 = com.igexin.b.b.a.a(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = new java.lang.String     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            byte[] r0 = com.igexin.b.a.a.a.c(r4, r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9.<init>(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r0 = "\\|"
            java.lang.String[] r4 = r9.split(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.io.PrintStream r0 = java.lang.System.out     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r10 = "length="
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            int r10 = r4.length     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r0.println(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r0 = 0
            r0 = r4[r0]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = "v"
            boolean r0 = r0.startsWith(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 == 0) goto L_0x0114
            r0 = 0
            r0 = r4[r0]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = "null"
            boolean r0 = r0.contains(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 == 0) goto L_0x017d
            r0 = 0
            r9 = 0
            r9 = r4[r9]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r10 = 7
            java.lang.String r9 = r9.substring(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r4[r0] = r9     // Catch:{ Exception -> 0x008e, all -> 0x018a }
        L_0x0114:
            r0 = 0
            int r9 = r4.length     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r10 = 2
            if (r9 <= r10) goto L_0x013d
            r0 = 2
            r0 = r4[r0]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 == 0) goto L_0x013d
            java.lang.String r9 = "null"
            boolean r9 = r0.equals(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r9 == 0) goto L_0x0127
            r0 = 0
        L_0x0127:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r10 = "WakeupAction get check form db file : "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            com.igexin.b.a.c.a.b(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
        L_0x013d:
            if (r0 != 0) goto L_0x015c
            r0 = 0
            r0 = r4[r0]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r0 = com.igexin.b.b.a.a(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r10 = "WakeupAction check cid form md5 session : "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            com.igexin.b.a.c.a.b(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
        L_0x015c:
            if (r14 != 0) goto L_0x01a0
            boolean r0 = r15.equals(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 == 0) goto L_0x01d3
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r0.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r0.add(r8)     // Catch:{ Exception -> 0x0247, all -> 0x018a }
            if (r5 == 0) goto L_0x0171
            r5.close()     // Catch:{ IOException -> 0x0196 }
        L_0x0171:
            if (r3 == 0) goto L_0x0176
            r3.close()     // Catch:{ IOException -> 0x019b }
        L_0x0176:
            r1 = r0
            goto L_0x000e
        L_0x0179:
            java.lang.String r0 = com.igexin.push.core.g.t     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            goto L_0x00c7
        L_0x017d:
            r0 = 0
            r9 = 0
            r9 = r4[r9]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r10 = 20
            java.lang.String r9 = r9.substring(r10)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r4[r0] = r9     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            goto L_0x0114
        L_0x018a:
            r0 = move-exception
        L_0x018b:
            if (r5 == 0) goto L_0x0190
            r5.close()     // Catch:{ IOException -> 0x01f7 }
        L_0x0190:
            if (r3 == 0) goto L_0x0195
            r3.close()     // Catch:{ IOException -> 0x01fc }
        L_0x0195:
            throw r0
        L_0x0196:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0171
        L_0x019b:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0176
        L_0x01a0:
            int r0 = r4.length     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9 = 1
            if (r0 <= r9) goto L_0x01d3
            r0 = 1
            r0 = r4[r0]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            boolean r0 = r15.equals(r0)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            if (r0 == 0) goto L_0x01b8
            if (r1 != 0) goto L_0x01b5
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r0.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r1 = r0
        L_0x01b5:
            r1.add(r8)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
        L_0x01b8:
            java.lang.String r0 = "WakeupAction"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r8.<init>()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r9 = "check from appid="
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            r9 = 1
            r4 = r4[r9]     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.StringBuilder r4 = r8.append(r4)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x008e, all -> 0x018a }
            com.igexin.b.a.c.a.a(r0, r4)     // Catch:{ Exception -> 0x008e, all -> 0x018a }
        L_0x01d3:
            r0 = r1
            if (r5 == 0) goto L_0x01d9
            r5.close()     // Catch:{ IOException -> 0x01e6 }
        L_0x01d9:
            if (r3 == 0) goto L_0x00b7
            r3.close()     // Catch:{ IOException -> 0x01e0 }
            goto L_0x00b7
        L_0x01e0:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b7
        L_0x01e6:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x01d9
        L_0x01eb:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b2
        L_0x01f1:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x00b7
        L_0x01f7:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0190
        L_0x01fc:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0195
        L_0x0201:
            if (r1 == 0) goto L_0x000e
            int r0 = r1.size()
            r2 = 1
            if (r0 != r2) goto L_0x000e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "WakeupAction check finished, final pkg is  : "
            java.lang.StringBuilder r2 = r0.append(r2)
            r0 = 0
            java.lang.Object r0 = r1.get(r0)
            java.lang.String r0 = (java.lang.String) r0
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            com.igexin.b.a.c.a.b(r0)
            goto L_0x000e
        L_0x0229:
            r0 = move-exception
            r5 = r4
            goto L_0x018b
        L_0x022d:
            r0 = move-exception
            r5 = r3
            r3 = r4
            goto L_0x018b
        L_0x0232:
            r0 = move-exception
            r3 = r4
            goto L_0x018b
        L_0x0236:
            r0 = move-exception
            r11 = r0
            r0 = r1
            r1 = r11
            r12 = r3
            r3 = r4
            r4 = r12
            goto L_0x0093
        L_0x023f:
            r0 = move-exception
            r3 = r4
            r4 = r5
            r11 = r0
            r0 = r1
            r1 = r11
            goto L_0x0093
        L_0x0247:
            r1 = move-exception
            r4 = r5
            goto L_0x0093
        L_0x024b:
            r0 = r1
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.core.a.a.l.a(int, java.lang.String):java.util.List");
    }

    private void a(String str, String str2, String str3, String str4, String str5) {
        StringBuilder sb = new StringBuilder();
        sb.append(g.f.getPackageName());
        sb.append("#");
        sb.append(str4);
        sb.append("#");
        sb.append(str5);
        sb.append("#");
        sb.append(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
        b("30025", sb.toString(), str, str2, str3);
        com.igexin.b.a.c.a.b("feedback actionId=30025 result=" + sb.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    private void a(String str, boolean z, PushTaskBean pushTaskBean, BaseAction baseAction) {
        String messageId;
        String taskId;
        String a2;
        StringBuffer stringBuffer;
        try {
            String a3 = a(str);
            messageId = pushTaskBean.getMessageId();
            taskId = pushTaskBean.getTaskId();
            a2 = ((o) baseAction).a();
            b(str);
            if (a3 != null) {
                HashMap hashMap = new HashMap();
                hashMap.put("messageId", messageId);
                hashMap.put("taskId", taskId);
                hashMap.put("id", a2);
                hashMap.put("pkgName", str);
                stringBuffer = new StringBuffer();
                stringBuffer.append(g.f.getPackageName());
                stringBuffer.append("#");
                stringBuffer.append(d(str));
                stringBuffer.append("#");
                stringBuffer.append(str);
                stringBuffer.append("/");
                if (a3.equals(f1261b)) {
                    stringBuffer.append(f1261b);
                    stringBuffer.append("#");
                    if (a(str, f1261b)) {
                        stringBuffer.append("0");
                    } else {
                        if (z) {
                            Intent intent = new Intent();
                            intent.setClassName(str, a3);
                            intent.putExtra("action", PushConsts.ACTION_SERVICE_INITIALIZE_SLAVE);
                            intent.putExtra("op_app", g.e);
                            intent.putExtra("isSlave", true);
                            g.f.startService(intent);
                        } else if (!b(str, a3)) {
                            a(stringBuffer, messageId, taskId, a2);
                            return;
                        }
                        hashMap.put("serviceName", f1261b);
                        a(hashMap);
                        stringBuffer.append("1");
                    }
                } else if (a3.equals(d)) {
                    stringBuffer.append(d);
                    stringBuffer.append("#");
                    if (a(str, d)) {
                        stringBuffer.append("0");
                    } else if (!b(str, a3)) {
                        a(stringBuffer, messageId, taskId, a2);
                        return;
                    } else {
                        hashMap.put("serviceName", d);
                        a(hashMap);
                        stringBuffer.append("1");
                    }
                } else if (a3.equals(c)) {
                    stringBuffer.append(c);
                    stringBuffer.append("#");
                    if (a(str, c)) {
                        stringBuffer.append("0");
                    } else if (!b(str, a3)) {
                        a(stringBuffer, messageId, taskId, a2);
                        return;
                    } else {
                        hashMap.put("serviceName", c);
                        a(hashMap);
                        stringBuffer.append("1");
                    }
                }
                b("30025", stringBuffer.toString(), messageId, taskId, a2);
                com.igexin.b.a.c.a.b("feedback actionId=30025 result=" + stringBuffer.toString());
                return;
            }
            a(messageId, taskId, a2, ((o) baseAction).d() != null ? ((o) baseAction).d() : "", ((o) baseAction).c() != null ? ((o) baseAction).c() : "");
        } catch (Exception e) {
            com.igexin.b.a.c.a.b(e.toString());
            a(stringBuffer, messageId, taskId, a2);
        } catch (Throwable th) {
            com.igexin.b.a.c.a.b("WakeupAction|" + th.toString());
        }
    }

    private void a(StringBuffer stringBuffer, String str, String str2, String str3) {
        stringBuffer.append(WeiboAuthException.DEFAULT_AUTH_ERROR_CODE);
        b("30025", stringBuffer.toString(), str, str2, str3);
        com.igexin.b.a.c.a.b("feedback actionId=30025 result=" + stringBuffer.toString());
    }

    private void a(Map<String, String> map) {
        f.a().a(new m(this, 180000, map));
    }

    public static boolean a(String str, String str2) {
        List<ActivityManager.RunningServiceInfo> runningServices = ((ActivityManager) g.f.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getRunningServices(2000);
        if (runningServices.size() <= 0) {
            return false;
        }
        for (int i = 0; i < runningServices.size(); i++) {
            if (runningServices.get(i).service.getClassName().equals(str2) && runningServices.get(i).service.getPackageName().equals(str)) {
                return true;
            }
        }
        return false;
    }

    private void b(String str) {
        if (c(str)) {
            try {
                Cursor query = g.f.getContentResolver().query(Uri.parse("content://downloads." + str + "/download"), null, null, null, null);
                if (query != null) {
                    query.close();
                }
            } catch (Exception e) {
                com.igexin.b.a.c.a.b(e.toString());
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str, String str2, String str3, String str4, String str5) {
        PushTaskBean pushTaskBean = new PushTaskBean();
        pushTaskBean.setAppid(g.f1382a);
        pushTaskBean.setMessageId(str3);
        pushTaskBean.setTaskId(str4);
        pushTaskBean.setId(str5);
        pushTaskBean.setAppKey(g.f1383b);
        e.a().a(pushTaskBean, str, str2);
    }

    private boolean b(String str, String str2) {
        try {
            Intent intent = new Intent();
            intent.setClassName(str, str2);
            g.f.startService(intent);
            return true;
        } catch (Exception e) {
            com.igexin.b.a.c.a.b("WakeupAction|" + e.toString());
            return false;
        }
    }

    private boolean c(String str) {
        ProviderInfo[] providerInfoArr;
        try {
            this.f1262a = g.f.getPackageManager();
            PackageInfo packageInfo = this.f1262a.getPackageInfo(str, 8);
            if (packageInfo == null || (providerInfoArr = packageInfo.providers) == null || providerInfoArr.length == 0) {
                return false;
            }
            for (ProviderInfo providerInfo : providerInfoArr) {
                if (providerInfo.name.equals("com.igexin.download.DownloadProvider") && providerInfo.authority.equals("downloads." + str)) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    public String d(String str) {
        try {
            this.f1262a = g.f.getPackageManager();
            Bundle bundle = this.f1262a.getApplicationInfo(str, 128).metaData;
            if (bundle != null) {
                for (String next : bundle.keySet()) {
                    if (next.equals(AssistPushConsts.GETUI_APPID)) {
                        return bundle.get(next).toString();
                    }
                }
            }
        } catch (Exception e) {
        }
        return "";
    }

    public b a(PushTaskBean pushTaskBean, BaseAction baseAction) {
        return b.success;
    }

    public BaseAction a(JSONObject jSONObject) {
        try {
            if (m.q && jSONObject.has("do") && jSONObject.has("actionid") && jSONObject.has("type") && (jSONObject.has("pkgname") || jSONObject.has("appid") || jSONObject.has("cid"))) {
                o oVar = new o();
                oVar.setType("wakeupsdk");
                oVar.setActionId(jSONObject.getString("actionid"));
                oVar.setDoActionId(jSONObject.getString("do"));
                if (jSONObject.has("pkgname")) {
                    oVar.b(jSONObject.getString("pkgname"));
                } else if (jSONObject.has("cid")) {
                    oVar.d(jSONObject.getString("cid"));
                } else if (jSONObject.has("appid")) {
                    oVar.c(jSONObject.getString("appid"));
                }
                if (jSONObject.has("is_forcestart")) {
                    oVar.a(jSONObject.getBoolean("is_forcestart"));
                }
                if (!jSONObject.has("id")) {
                    return oVar;
                }
                oVar.a(jSONObject.getString("id"));
                return oVar;
            }
        } catch (JSONException e) {
            com.igexin.b.a.c.a.b(e.toString());
        }
        return null;
    }

    public boolean b(PushTaskBean pushTaskBean, BaseAction baseAction) {
        boolean z;
        boolean z2;
        if (!(pushTaskBean == null || baseAction == null)) {
            o oVar = (o) baseAction;
            String c2 = oVar.c();
            if (c2 != null || oVar.e() == null) {
                z = true;
            } else {
                List<String> a2 = a(0, oVar.e());
                if (a2 == null || a2.size() != 1) {
                    z = false;
                } else {
                    c2 = a2.get(0);
                    z = true;
                }
            }
            if (c2 != null) {
                a(c2, oVar.b(), pushTaskBean, baseAction);
                z2 = z;
            } else if (oVar.d() != null) {
                List<String> a3 = a(1, oVar.d());
                if (a3 == null || a3.size() <= 0) {
                    z2 = false;
                } else {
                    for (String a4 : a3) {
                        a(a4, oVar.b(), pushTaskBean, baseAction);
                    }
                    z2 = z;
                }
            } else {
                z2 = z;
            }
            if (!z2) {
                a(pushTaskBean.getMessageId(), pushTaskBean.getTaskId(), ((o) baseAction).a(), ((o) baseAction).d() != null ? ((o) baseAction).d() : "", ((o) baseAction).c() != null ? ((o) baseAction).c() : "");
            }
            if (!baseAction.getDoActionId().equals("")) {
                e.a().a(pushTaskBean.getTaskId(), pushTaskBean.getMessageId(), baseAction.getDoActionId());
            }
        }
        return true;
    }
}
