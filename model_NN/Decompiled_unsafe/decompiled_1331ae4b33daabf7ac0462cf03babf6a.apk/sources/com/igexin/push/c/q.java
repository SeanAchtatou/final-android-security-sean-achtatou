package com.igexin.push.c;

class q implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f1232a;

    q(p pVar) {
        this.f1232a = pVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x018f A[Catch:{ all -> 0x01ca }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
        L_0x0000:
            com.igexin.push.c.p r0 = r8.f1232a
            boolean r0 = r0.L
            if (r0 == 0) goto L_0x0058
            com.igexin.push.c.p r0 = r8.f1232a
            java.lang.Thread r0 = r0.e
            boolean r0 = r0.isInterrupted()
            if (r0 != 0) goto L_0x0058
            r1 = 0
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            java.util.concurrent.locks.Lock r0 = r0.h     // Catch:{ Exception -> 0x0154 }
            r0.lock()     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            java.util.List r0 = r0.j     // Catch:{ Exception -> 0x0154 }
            boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x0154 }
            if (r0 == 0) goto L_0x0033
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            java.util.concurrent.locks.Condition r0 = r0.i     // Catch:{ Exception -> 0x0154 }
            r0.await()     // Catch:{ Exception -> 0x0154 }
        L_0x0033:
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            java.util.List r0 = r0.j     // Catch:{ Exception -> 0x0154 }
            r0.clear()     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            boolean r0 = r0.M     // Catch:{ Exception -> 0x0154 }
            if (r0 == 0) goto L_0x0059
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01f3 }
            java.util.concurrent.locks.Lock r0 = r0.h     // Catch:{ Exception -> 0x01f3 }
            r0.unlock()     // Catch:{ Exception -> 0x01f3 }
        L_0x004d:
            if (r1 == 0) goto L_0x0058
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x01e0 }
        L_0x0058:
            return
        L_0x0059:
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ Exception -> 0x0154 }
            if (r0 == 0) goto L_0x0070
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.p r2 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.j r2 = r2.c     // Catch:{ Exception -> 0x0154 }
            r0.a(r2)     // Catch:{ Exception -> 0x0154 }
        L_0x0070:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x0154 }
            com.igexin.push.c.j r0 = r0.c     // Catch:{ Exception -> 0x0154 }
            java.lang.String r0 = r0.a()     // Catch:{ Exception -> 0x0154 }
            java.lang.String[] r0 = com.igexin.b.a.b.f.a(r0)     // Catch:{ Exception -> 0x0154 }
            java.net.Socket r6 = new java.net.Socket     // Catch:{ Exception -> 0x0154 }
            r6.<init>()     // Catch:{ Exception -> 0x0154 }
            java.net.InetSocketAddress r1 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r4 = 1
            r0 = r0[r4]     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r4 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.j r4 = r4.c     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            int r4 = r4.d()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r1.<init>(r0, r4)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r6.connect(r1, r0)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.j r0 = r0.c     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r1.<init>()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r7 = "socket://"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.net.InetAddress r7 = r6.getInetAddress()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r7 = r7.getHostAddress()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r7 = ":"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r7 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.j r7 = r7.c     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            int r7 = r7.d()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            long r2 = r4 - r2
            r0.a(r1, r2, r4)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r0.<init>()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = com.igexin.push.c.p.f1231b     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = "|detect "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r1 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = r1.z()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = "ok, time: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r1 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.j r1 = r1.c     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            long r2 = r1.e()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r1 = " ######"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            if (r0 == 0) goto L_0x013b
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            boolean r0 = r0.M     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            if (r0 != 0) goto L_0x013b
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.g r1 = com.igexin.push.c.g.SUCCESS     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.p r2 = r8.f1232a     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            com.igexin.push.c.j r2 = r2.c     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
            r0.a(r1, r2)     // Catch:{ Exception -> 0x01ec, all -> 0x01e7 }
        L_0x013b:
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01f0 }
            java.util.concurrent.locks.Lock r0 = r0.h     // Catch:{ Exception -> 0x01f0 }
            r0.unlock()     // Catch:{ Exception -> 0x01f0 }
        L_0x0144:
            if (r6 == 0) goto L_0x0000
            boolean r0 = r6.isClosed()
            if (r0 != 0) goto L_0x0000
            r6.close()     // Catch:{ Exception -> 0x0151 }
            goto L_0x0000
        L_0x0151:
            r0 = move-exception
            goto L_0x0000
        L_0x0154:
            r0 = move-exception
        L_0x0155:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01ca }
            r2.<init>()     // Catch:{ all -> 0x01ca }
            java.lang.String r3 = com.igexin.push.c.p.f1231b     // Catch:{ all -> 0x01ca }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x01ca }
            java.lang.String r3 = "|detect "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.p r3 = r8.f1232a     // Catch:{ all -> 0x01ca }
            java.lang.String r3 = r3.z()     // Catch:{ all -> 0x01ca }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x01ca }
            java.lang.String r3 = "thread -->"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x01ca }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01ca }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x01ca }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01ca }
            com.igexin.b.a.c.a.b(r0)     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ all -> 0x01ca }
            boolean r0 = r0.M     // Catch:{ all -> 0x01ca }
            if (r0 != 0) goto L_0x01b1
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.j r0 = r0.c     // Catch:{ all -> 0x01ca }
            r0.b()     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ all -> 0x01ca }
            if (r0 == 0) goto L_0x01b1
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.o r0 = r0.f     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.g r2 = com.igexin.push.c.g.EXCEPTION     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.p r3 = r8.f1232a     // Catch:{ all -> 0x01ca }
            com.igexin.push.c.j r3 = r3.c     // Catch:{ all -> 0x01ca }
            r0.a(r2, r3)     // Catch:{ all -> 0x01ca }
        L_0x01b1:
            com.igexin.push.c.p r0 = r8.f1232a     // Catch:{ Exception -> 0x01ea }
            java.util.concurrent.locks.Lock r0 = r0.h     // Catch:{ Exception -> 0x01ea }
            r0.unlock()     // Catch:{ Exception -> 0x01ea }
        L_0x01ba:
            if (r1 == 0) goto L_0x0000
            boolean r0 = r1.isClosed()
            if (r0 != 0) goto L_0x0000
            r1.close()     // Catch:{ Exception -> 0x01c7 }
            goto L_0x0000
        L_0x01c7:
            r0 = move-exception
            goto L_0x0000
        L_0x01ca:
            r0 = move-exception
        L_0x01cb:
            com.igexin.push.c.p r2 = r8.f1232a     // Catch:{ Exception -> 0x01e5 }
            java.util.concurrent.locks.Lock r2 = r2.h     // Catch:{ Exception -> 0x01e5 }
            r2.unlock()     // Catch:{ Exception -> 0x01e5 }
        L_0x01d4:
            if (r1 == 0) goto L_0x01df
            boolean r2 = r1.isClosed()
            if (r2 != 0) goto L_0x01df
            r1.close()     // Catch:{ Exception -> 0x01e3 }
        L_0x01df:
            throw r0
        L_0x01e0:
            r0 = move-exception
            goto L_0x0058
        L_0x01e3:
            r1 = move-exception
            goto L_0x01df
        L_0x01e5:
            r2 = move-exception
            goto L_0x01d4
        L_0x01e7:
            r0 = move-exception
            r1 = r6
            goto L_0x01cb
        L_0x01ea:
            r0 = move-exception
            goto L_0x01ba
        L_0x01ec:
            r0 = move-exception
            r1 = r6
            goto L_0x0155
        L_0x01f0:
            r0 = move-exception
            goto L_0x0144
        L_0x01f3:
            r0 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.c.q.run():void");
    }
}
