package com.igexin.push.config;

import android.content.Context;
import com.igexin.b.a.c.a;
import com.igexin.push.util.e;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private static String f1252a = "FileConfig";

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r1v6, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v11 */
    /* JADX WARN: Type inference failed for: r1v19 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0047 A[SYNTHETIC, Splitter:B:18:0x0047] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x004b A[SYNTHETIC, Splitter:B:21:0x004b] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009c A[SYNTHETIC, Splitter:B:38:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a1 A[SYNTHETIC, Splitter:B:41:0x00a1] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00ba A[SYNTHETIC, Splitter:B:54:0x00ba] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00bf A[SYNTHETIC, Splitter:B:57:0x00bf] */
    /* JADX WARNING: Removed duplicated region for block: B:81:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a() {
        /*
            r1 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = com.igexin.push.core.g.e
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r2 = ".properties"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            android.content.Context r2 = com.igexin.push.core.g.f
            android.content.res.Resources r2 = r2.getResources()
            android.content.res.AssetManager r2 = r2.getAssets()
            java.io.InputStream r0 = r2.open(r0)     // Catch:{ Exception -> 0x003a, all -> 0x0044 }
            a(r0)     // Catch:{ Exception -> 0x00e2, all -> 0x00dc }
            if (r0 == 0) goto L_0x002c
            r0.close()     // Catch:{ Exception -> 0x00c3 }
        L_0x002c:
            java.io.File r2 = new java.io.File
            java.lang.String r3 = com.igexin.push.core.g.W
            r2.<init>(r3)
            boolean r2 = r2.exists()
            if (r2 != 0) goto L_0x004b
        L_0x0039:
            return
        L_0x003a:
            r0 = move-exception
            r0 = r1
        L_0x003c:
            if (r0 == 0) goto L_0x002c
            r0.close()     // Catch:{ Exception -> 0x0042 }
            goto L_0x002c
        L_0x0042:
            r2 = move-exception
            goto L_0x002c
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            if (r1 == 0) goto L_0x004a
            r1.close()     // Catch:{ Exception -> 0x00c6 }
        L_0x004a:
            throw r0
        L_0x004b:
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00d7, all -> 0x00b4 }
            java.lang.String r3 = com.igexin.push.core.g.W     // Catch:{ Exception -> 0x00d7, all -> 0x00b4 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00d7, all -> 0x00b4 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x00d9, all -> 0x00d0 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x00d9, all -> 0x00d0 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r2, r4)     // Catch:{ Exception -> 0x00d9, all -> 0x00d0 }
            r0.<init>(r3)     // Catch:{ Exception -> 0x00d9, all -> 0x00d0 }
        L_0x005e:
            java.lang.String r1 = r0.readLine()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r1 == 0) goto L_0x00a7
            java.lang.String r3 = "#"
            boolean r3 = r1.startsWith(r3)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r3 != 0) goto L_0x005e
            java.lang.String r3 = "="
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            int r3 = r1.length     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r4 = 2
            if (r3 < r4) goto L_0x005e
            r3 = 0
            r3 = r1[r3]     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            r4 = 1
            r1 = r1[r4]     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            java.lang.String r4 = "sdk.debug"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            if (r3 == 0) goto L_0x005e
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            com.igexin.b.a.c.a.f1129a = r1     // Catch:{ Exception -> 0x0097, all -> 0x00d2 }
            goto L_0x005e
        L_0x0097:
            r1 = move-exception
            r1 = r0
            r0 = r2
        L_0x009a:
            if (r1 == 0) goto L_0x009f
            r1.close()     // Catch:{ IOException -> 0x00ca }
        L_0x009f:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ Exception -> 0x00a5 }
            goto L_0x0039
        L_0x00a5:
            r0 = move-exception
            goto L_0x0039
        L_0x00a7:
            if (r0 == 0) goto L_0x00ac
            r0.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x00ac:
            if (r2 == 0) goto L_0x0039
            r2.close()     // Catch:{ Exception -> 0x00b2 }
            goto L_0x0039
        L_0x00b2:
            r0 = move-exception
            goto L_0x0039
        L_0x00b4:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.close()     // Catch:{ IOException -> 0x00cc }
        L_0x00bd:
            if (r2 == 0) goto L_0x00c2
            r2.close()     // Catch:{ Exception -> 0x00ce }
        L_0x00c2:
            throw r0
        L_0x00c3:
            r2 = move-exception
            goto L_0x002c
        L_0x00c6:
            r1 = move-exception
            goto L_0x004a
        L_0x00c8:
            r0 = move-exception
            goto L_0x00ac
        L_0x00ca:
            r1 = move-exception
            goto L_0x009f
        L_0x00cc:
            r1 = move-exception
            goto L_0x00bd
        L_0x00ce:
            r1 = move-exception
            goto L_0x00c2
        L_0x00d0:
            r0 = move-exception
            goto L_0x00b8
        L_0x00d2:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00b8
        L_0x00d7:
            r2 = move-exception
            goto L_0x009a
        L_0x00d9:
            r0 = move-exception
            r0 = r2
            goto L_0x009a
        L_0x00dc:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x0045
        L_0x00e2:
            r2 = move-exception
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.config.n.a():void");
    }

    /* JADX WARN: Type inference failed for: r1v0 */
    /* JADX WARN: Type inference failed for: r1v1, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r1v3, types: [java.io.InputStreamReader] */
    /* JADX WARN: Type inference failed for: r1v5 */
    /* JADX WARN: Type inference failed for: r1v7 */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0043 A[SYNTHETIC, Splitter:B:14:0x0043] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0048 A[SYNTHETIC, Splitter:B:17:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x005d A[SYNTHETIC, Splitter:B:29:0x005d] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0062 A[SYNTHETIC, Splitter:B:32:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r6) {
        /*
            r1 = 0
            android.content.res.Resources r0 = r6.getResources()
            android.content.res.AssetManager r0 = r0.getAssets()
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Throwable -> 0x0077, all -> 0x0059 }
            java.lang.String r3 = "green.cfg"
            java.io.InputStream r0 = r0.open(r3)     // Catch:{ Throwable -> 0x0077, all -> 0x0059 }
            r2.<init>(r0)     // Catch:{ Throwable -> 0x0077, all -> 0x0059 }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Throwable -> 0x007a, all -> 0x0070 }
            r0.<init>(r2)     // Catch:{ Throwable -> 0x007a, all -> 0x0070 }
        L_0x0019:
            java.lang.String r1 = r0.readLine()     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            if (r1 == 0) goto L_0x004c
            java.lang.String r3 = "="
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            int r3 = r1.length     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            r4 = 2
            if (r3 != r4) goto L_0x0019
            r3 = 0
            r3 = r1[r3]     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            java.lang.String r3 = r3.trim()     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            r4 = 1
            r1 = r1[r4]     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            java.lang.String r1 = r1.trim()     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            java.util.HashMap r4 = com.igexin.push.core.g.b()     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            r4.put(r3, r1)     // Catch:{ Throwable -> 0x003f, all -> 0x0072 }
            goto L_0x0019
        L_0x003f:
            r1 = move-exception
            r1 = r2
        L_0x0041:
            if (r1 == 0) goto L_0x0046
            r1.close()     // Catch:{ Exception -> 0x0068 }
        L_0x0046:
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ Exception -> 0x006a }
        L_0x004b:
            return
        L_0x004c:
            if (r2 == 0) goto L_0x0051
            r2.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0051:
            if (r0 == 0) goto L_0x004b
            r0.close()     // Catch:{ Exception -> 0x0057 }
            goto L_0x004b
        L_0x0057:
            r0 = move-exception
            goto L_0x004b
        L_0x0059:
            r0 = move-exception
            r2 = r1
        L_0x005b:
            if (r2 == 0) goto L_0x0060
            r2.close()     // Catch:{ Exception -> 0x006c }
        L_0x0060:
            if (r1 == 0) goto L_0x0065
            r1.close()     // Catch:{ Exception -> 0x006e }
        L_0x0065:
            throw r0
        L_0x0066:
            r1 = move-exception
            goto L_0x0051
        L_0x0068:
            r1 = move-exception
            goto L_0x0046
        L_0x006a:
            r0 = move-exception
            goto L_0x004b
        L_0x006c:
            r2 = move-exception
            goto L_0x0060
        L_0x006e:
            r1 = move-exception
            goto L_0x0065
        L_0x0070:
            r0 = move-exception
            goto L_0x005b
        L_0x0072:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x005b
        L_0x0077:
            r0 = move-exception
            r0 = r1
            goto L_0x0041
        L_0x007a:
            r0 = move-exception
            r0 = r1
            r1 = r2
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.config.n.a(android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:146:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x004a A[SYNTHETIC, Splitter:B:15:0x004a] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0065 A[SYNTHETIC, Splitter:B:25:0x0065] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.io.InputStream r7) {
        /*
            r4 = 1000(0x3e8, double:4.94E-321)
            r1 = 0
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0259, all -> 0x0256 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0259, all -> 0x0256 }
            java.lang.String r3 = "UTF-8"
            r2.<init>(r7, r3)     // Catch:{ Exception -> 0x0259, all -> 0x0256 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0259, all -> 0x0256 }
        L_0x000f:
            java.lang.String r1 = r0.readLine()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r1 == 0) goto L_0x023d
            java.lang.String r2 = "#"
            boolean r2 = r1.startsWith(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r2 != 0) goto L_0x000f
            java.lang.String r2 = "="
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            int r2 = r1.length     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            r3 = 2
            if (r2 < r3) goto L_0x000f
            r2 = 0
            r2 = r1[r2]     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            r3 = 1
            r1 = r1[r3]     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            java.lang.String r1 = r1.trim()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            java.lang.String r3 = "sdk.cm_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x004e
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.setXfrAddressIps(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0047:
            r1 = move-exception
        L_0x0048:
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ Exception -> 0x024a }
        L_0x004d:
            return
        L_0x004e:
            java.lang.String r3 = "sdk.config_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0069
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.CONFIG_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x005f:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            r1.close()     // Catch:{ Exception -> 0x0250 }
        L_0x0068:
            throw r0
        L_0x0069:
            java.lang.String r3 = "sdk.bi_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x007a
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.BI_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x007a:
            java.lang.String r3 = "sdk.state_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x008b
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.STATE_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x008b:
            java.lang.String r3 = "sdk.log_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x009d
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.LOG_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x009d:
            java.lang.String r3 = "sdk.amp_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x00af
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.AMP_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x00af:
            java.lang.String r3 = "sdk.lbs_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x00c1
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.LBS_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x00c1:
            java.lang.String r3 = "sdk.cm_address_backup"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x00d3
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.XFR_ADDRESS_BAK = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x00d3:
            java.lang.String r3 = "sdk.inc_address"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x00e5
            java.lang.String r2 = ","
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.SDKUrlConfig.INC_ADDRESS_IPS = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x00e5:
            java.lang.String r3 = "sdk.debug"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x00f9
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.b.a.c.a.f1129a = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x00f9:
            java.lang.String r3 = "sdk.domainbackup.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x010d
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.f = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x010d:
            java.lang.String r3 = "sdk.readlocalcell.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0121
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.g = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0121:
            java.lang.String r3 = "sdk.uploadapplist.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0135
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.h = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0135:
            java.lang.String r3 = "sdk.feature.sendmessage.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0149
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.i = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0149:
            java.lang.String r3 = "sdk.feature.settag.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x015d
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.j = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x015d:
            java.lang.String r3 = "sdk.feature.setsilenttime.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0171
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.k = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0171:
            java.lang.String r3 = "sdk.snl.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0185
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.n = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0185:
            java.lang.String r3 = "sdk.snl.maxactiveflow"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0199
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r1.longValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.o = r2     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0199:
            java.lang.String r3 = "sdk.feature.setheartbeatinterval.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x01ad
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.l = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x01ad:
            java.lang.String r3 = "sdk.feature.setsockettimeout.enable"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x01c1
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.m = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x01c1:
            java.lang.String r3 = "sdk.stay.backup.time"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x01d6
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r1.longValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r2 * r4
            com.igexin.push.config.m.z = r2     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x01d6:
            java.lang.String r3 = "sdk.enter.backup.detect.failed.cnt"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x01ea
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.A = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x01ea:
            java.lang.String r3 = "sdk.login.failed.cnt"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x01fe
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            com.igexin.push.config.m.B = r1     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x01fe:
            java.lang.String r3 = "sdk.detect.ip.expired.time"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0213
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r1.longValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r2 * r4
            com.igexin.push.config.m.C = r2     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0213:
            java.lang.String r3 = "sdk.detect.interval.time"
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r3 == 0) goto L_0x0228
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r1.longValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r2 * r4
            com.igexin.push.config.m.D = r2     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x0228:
            java.lang.String r3 = "sdk.reset.reconnect.delay"
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            if (r2 == 0) goto L_0x000f
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r1.longValue()     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            long r2 = r2 * r4
            com.igexin.push.config.m.y = r2     // Catch:{ Exception -> 0x0047, all -> 0x005f }
            goto L_0x000f
        L_0x023d:
            if (r0 == 0) goto L_0x004d
            r0.close()     // Catch:{ Exception -> 0x0244 }
            goto L_0x004d
        L_0x0244:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004d
        L_0x024a:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x004d
        L_0x0250:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0068
        L_0x0256:
            r0 = move-exception
            goto L_0x0063
        L_0x0259:
            r0 = move-exception
            r0 = r1
            goto L_0x0048
        */
        throw new UnsupportedOperationException("Method not decompiled: com.igexin.push.config.n.a(java.io.InputStream):void");
    }

    public static void b(Context context) {
        try {
            byte[] a2 = e.a(context.getFilesDir().getPath() + "/" + "conf_n.pid");
            if (a2 != null) {
                m.w = Boolean.valueOf(new String(a2)).booleanValue();
            }
        } catch (Throwable th) {
            a.b(f1252a + "|load need confgi error = " + th.toString());
        }
    }
}
