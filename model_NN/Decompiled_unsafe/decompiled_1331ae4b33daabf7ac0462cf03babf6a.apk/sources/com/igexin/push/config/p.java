package com.igexin.push.config;

import android.os.Bundle;
import com.igexin.b.a.c.a;
import com.igexin.push.core.g;

public class p {
    public static void a() {
        try {
            Bundle bundle = g.f.getPackageManager().getApplicationInfo(g.f.getPackageName(), 128).metaData;
            if (bundle != null) {
                for (String next : bundle.keySet()) {
                    if (next.equals("PUSH_DOMAIN")) {
                        a.b("PUSH_DOMAIN:" + bundle.getString(next));
                        a(bundle.getString(next));
                        return;
                    }
                }
            }
        } catch (Exception e) {
            a.b(e.toString());
        }
    }

    private static void a(String str) {
        SDKUrlConfig.setXfrAddressIps(new String[]{"socket://xfr." + str + ":5224"});
        a.b("XFR_ADDRESS_IPS:" + SDKUrlConfig.getXfrAddress()[0]);
        SDKUrlConfig.XFR_ADDRESS_BAK = new String[]{"socket://xfr_bak." + str + ":5224"};
        a.b("XFR_ADDRESS_IPS_BAK:" + SDKUrlConfig.XFR_ADDRESS_BAK[0]);
        SDKUrlConfig.BI_ADDRESS_IPS = new String[]{"http://bi." + str + "/api.php"};
        a.b("BI_ADDRESS_IPS:" + SDKUrlConfig.BI_ADDRESS_IPS[0]);
        SDKUrlConfig.CONFIG_ADDRESS_IPS = new String[]{"http://config." + str + "/api.php"};
        a.b("CONFIG_ADDRESS_IPS:" + SDKUrlConfig.CONFIG_ADDRESS_IPS[0]);
        SDKUrlConfig.STATE_ADDRESS_IPS = new String[]{"http://stat." + str + "/api.php"};
        a.b("STATE_ADDRESS_IPS:" + SDKUrlConfig.STATE_ADDRESS_IPS[0]);
        SDKUrlConfig.LOG_ADDRESS_IPS = new String[]{"http://log." + str + "/api.php"};
        a.b("LOG_ADDRESS_IPS:" + SDKUrlConfig.LOG_ADDRESS_IPS[0]);
        SDKUrlConfig.AMP_ADDRESS_IPS = new String[]{"http://amp." + str + "/api.htm"};
        a.b("AMP_ADDRESS_IPS:" + SDKUrlConfig.AMP_ADDRESS_IPS[0]);
        SDKUrlConfig.LBS_ADDRESS_IPS = new String[]{"http://lbs." + str + "/api.htm"};
        a.b("LBS_ADDRESS_IPS:" + SDKUrlConfig.LBS_ADDRESS_IPS[0]);
        SDKUrlConfig.INC_ADDRESS_IPS = new String[]{"http://inc." + str + "/api.php"};
        a.b("INC_ADDRESS_IPS:" + SDKUrlConfig.INC_ADDRESS_IPS[0]);
    }
}
