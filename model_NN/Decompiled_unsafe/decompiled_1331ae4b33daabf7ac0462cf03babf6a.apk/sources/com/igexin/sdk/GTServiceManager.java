package com.igexin.sdk;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import com.igexin.b.a.c.a;
import com.igexin.push.config.m;
import com.igexin.push.config.n;
import com.igexin.push.core.f;
import com.igexin.push.core.g;
import com.igexin.push.util.EncryptUtils;
import com.igexin.push.util.b;
import com.igexin.push.util.e;
import com.igexin.push.util.q;
import com.igexin.sdk.a.d;
import java.lang.reflect.Method;
import java.util.concurrent.atomic.AtomicBoolean;

public class GTServiceManager {
    public static final String TAG = GTServiceManager.class.getName();
    public static Context context;

    /* renamed from: a  reason: collision with root package name */
    private IPushCore f1490a;

    /* renamed from: b  reason: collision with root package name */
    private final AtomicBoolean f1491b;

    private GTServiceManager() {
        this.f1491b = new AtomicBoolean(false);
    }

    /* synthetic */ GTServiceManager(a aVar) {
        this();
    }

    private int a(Service service) {
        a.b(TAG + "|start by system ####");
        if (a((Context) service)) {
            a.b(TAG + "|intent = null");
            if (this.f1491b.getAndSet(true)) {
                return 1;
            }
            a(service, (Intent) null);
            return 1;
        }
        a.b(TAG + "|start by system, needLook = " + m.w + ", firstInit = true or (ss = 1 switchOn = false), stop");
        service.stopSelf();
        return 2;
    }

    private int a(Service service, Intent intent, int i, int i2) {
        a.b(TAG + "|start from initialize...");
        a(service, intent);
        if (this.f1490a != null) {
            return this.f1490a.onServiceStartCommand(intent, i, i2);
        }
        return 1;
    }

    private int a(Intent intent, int i, int i2) {
        if (this.f1490a == null) {
            return 1;
        }
        a.b(TAG + "|inInit = true, call onServiceStartCommand...");
        return this.f1490a.onServiceStartCommand(intent, i, i2);
    }

    private Method a(String str, Class<?>... clsArr) {
        try {
            return Class.forName("com.igexin.dms.DMSManager").getMethod(str, clsArr);
        } catch (Exception e) {
            a.b(TAG + "invokeMethod error");
            return null;
        }
    }

    private void a(Service service, Intent intent) {
        a.b(TAG + "|startPushCore ++++");
        if (EncryptUtils.isLoadSuccess()) {
            com.igexin.sdk.a.a.a().a(service);
            this.f1490a = com.igexin.sdk.a.a.a().b();
            if (this.f1490a != null) {
                this.f1490a.start(service);
            }
            try {
                Object invoke = a("getInstance", new Class[0]).invoke(null, new Object[0]);
                a("init", Context.class, Intent.class).invoke(invoke, service, intent);
            } catch (Throwable th) {
                a.b(TAG + "|" + th.toString());
            }
        } else {
            b.a(new a(this, service), service);
        }
    }

    private void a(Intent intent) {
        try {
            if (!TextUtils.isEmpty(intent.getStringExtra("from"))) {
                Message obtain = Message.obtain();
                obtain.what = com.igexin.push.core.a.j;
                obtain.obj = intent;
                f.a().a(obtain);
            }
        } catch (Throwable th) {
            a.b(TAG + "|" + th.toString());
        }
    }

    private boolean a(Context context2) {
        if (e.a(context2)) {
            return false;
        }
        n.b(context2);
        if (!m.w) {
            return false;
        }
        n.a(context2);
        return !"1".equals(g.b().get("ss")) || new d(context2).c();
    }

    private int b(Service service, Intent intent, int i, int i2) {
        if (a((Context) service)) {
            a(intent);
            a(service, intent);
            if (this.f1490a != null) {
                return this.f1490a.onServiceStartCommand(intent, i, i2);
            }
        } else {
            a.b(TAG + "|start by guard, needLook = " + m.w + ", firstInit = true or (ss = 1 switchOn = false), stop");
            service.stopSelf();
        }
        return 2;
    }

    public static GTServiceManager getInstance() {
        return b.f1506a;
    }

    public Class getUserIntentService(Context context2) {
        try {
            String str = (String) q.b(context2, "uis", "");
            if (!TextUtils.isEmpty(str)) {
                return Class.forName(str);
            }
        } catch (Throwable th) {
            a.b(TAG + "|" + th.toString());
        }
        return null;
    }

    public Class getUserPushService(Context context2) {
        try {
            String str = (String) q.b(context2, "us", "");
            return TextUtils.isEmpty(str) ? PushService.class : Class.forName(str);
        } catch (Throwable th) {
            a.b(TAG + "|" + th.toString());
            return PushService.class;
        }
    }

    public boolean isUserPushServiceSet(Context context2) {
        try {
            String str = (String) q.b(context2, "us", "");
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            Class.forName(str);
            return true;
        } catch (Exception e) {
            a.b(TAG + "|" + e.toString());
            return false;
        }
    }

    public IBinder onBind(Intent intent) {
        a.b(TAG + "|onBind...");
        if (this.f1490a != null) {
            return this.f1490a.onServiceBind(intent);
        }
        return null;
    }

    public void onCreate(Context context2) {
        context = context2;
    }

    public void onDestroy() {
        a.b(TAG + "|onDestroy...");
        if (this.f1490a != null) {
            this.f1490a.onServiceDestroy();
        }
        Process.killProcess(Process.myPid());
    }

    public void onLowMemory() {
        a.b(TAG + "|onLowMemory...");
    }

    public int onStartCommand(Service service, Intent intent, int i, int i2) {
        if (intent == null) {
            try {
                return a(service);
            } catch (Throwable th) {
                a.b(TAG + "|" + th.toString());
                return 2;
            }
        } else {
            String stringExtra = intent.getStringExtra("action");
            if (PushConsts.ACTION_SERVICE_INITIALIZE.equals(stringExtra)) {
                e.b(service);
            }
            if (this.f1491b.get()) {
                return a(intent, i, i2);
            }
            this.f1491b.set(true);
            return PushConsts.ACTION_SERVICE_INITIALIZE.equals(stringExtra) ? a(service, intent, i, i2) : b(service, intent, i, i2);
        }
    }
}
