package com.igexin.sdk;

import android.content.Context;
import android.util.Log;
import com.igexin.assist.MessageBean;
import com.igexin.assist.a.b;
import com.igexin.assist.action.MessageManger;
import com.igexin.assist.sdk.AssistPushConsts;
import com.xiaomi.mipush.sdk.MiPushClient;
import com.xiaomi.mipush.sdk.MiPushCommandMessage;
import com.xiaomi.mipush.sdk.MiPushMessage;
import com.xiaomi.mipush.sdk.PushMessageReceiver;
import java.util.List;

public class MiuiPushReceiver extends PushMessageReceiver {
    public static final String TAG = "Assist_MiuiPushReceiver";

    public void onCommandResult(Context context, MiPushCommandMessage miPushCommandMessage) {
    }

    public void onNotificationMessageArrived(Context context, MiPushMessage miPushMessage) {
        Log.d(TAG, "onNotificationArrived receive message ...");
    }

    public void onNotificationMessageClicked(Context context, MiPushMessage miPushMessage) {
        try {
            Log.d(TAG, "onNotificationMessageClicked receive message ...");
            if (!(context == null || miPushMessage == null)) {
                MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, miPushMessage.getContent());
                messageBean.setMessageSource(AssistPushConsts.XM_PREFIX);
                MessageManger.getInstance().addMessage(messageBean);
            }
            b.a(context);
        } catch (Throwable th) {
        }
    }

    public void onReceivePassThroughMessage(Context context, MiPushMessage miPushMessage) {
        try {
            Log.d(TAG, "onReceivePassThroughMessage receive meaasge ...");
            if (!(context == null || miPushMessage == null)) {
                MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, miPushMessage.getContent());
                messageBean.setMessageSource(AssistPushConsts.XM_PREFIX);
                MessageManger.getInstance().addMessage(messageBean);
            }
            b.a(context);
        } catch (Throwable th) {
        }
    }

    public void onReceiveRegisterResult(Context context, MiPushCommandMessage miPushCommandMessage) {
        try {
            Log.d(TAG, "onReceiveRegisterResult receiver message ...");
            if (context != null && miPushCommandMessage != null) {
                String command = miPushCommandMessage.getCommand();
                List<String> commandArguments = miPushCommandMessage.getCommandArguments();
                String str = (commandArguments == null || commandArguments.size() <= 0) ? null : commandArguments.get(0);
                if (MiPushClient.COMMAND_REGISTER.equals(command) && miPushCommandMessage.getResultCode() == 0) {
                    MessageManger.getInstance().addMessage(new MessageBean(context, AssistPushConsts.MSG_TYPE_TOKEN, AssistPushConsts.XM_PREFIX + str));
                }
            }
        } catch (Throwable th) {
        }
    }
}
