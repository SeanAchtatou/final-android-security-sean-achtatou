package com.igexin.sdk.message;

public class SetTagCmdMessage extends GTCmdMessage {

    /* renamed from: a  reason: collision with root package name */
    private String f1515a;

    /* renamed from: b  reason: collision with root package name */
    private String f1516b;

    public SetTagCmdMessage() {
    }

    public SetTagCmdMessage(String str, String str2, int i) {
        super(i);
        this.f1515a = str;
        this.f1516b = str2;
    }

    public String getCode() {
        return this.f1516b;
    }

    public String getSn() {
        return this.f1515a;
    }

    public void setCode(String str) {
        this.f1516b = str;
    }

    public void setSn(String str) {
        this.f1515a = str;
    }
}
