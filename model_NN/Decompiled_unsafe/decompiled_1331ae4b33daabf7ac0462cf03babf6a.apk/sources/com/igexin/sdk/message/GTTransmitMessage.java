package com.igexin.sdk.message;

public class GTTransmitMessage extends GTPushMessage {

    /* renamed from: a  reason: collision with root package name */
    private String f1513a;

    /* renamed from: b  reason: collision with root package name */
    private String f1514b;
    private String c;
    private byte[] d;

    public GTTransmitMessage() {
    }

    public GTTransmitMessage(String str, String str2, String str3, byte[] bArr) {
        this.f1513a = str;
        this.f1514b = str2;
        this.c = str3;
        this.d = bArr;
    }

    public String getMessageId() {
        return this.f1514b;
    }

    public byte[] getPayload() {
        return this.d;
    }

    public String getPayloadId() {
        return this.c;
    }

    public String getTaskId() {
        return this.f1513a;
    }

    public void setMessageId(String str) {
        this.f1514b = str;
    }

    public void setPayload(byte[] bArr) {
        this.d = bArr;
    }

    public void setPayloadId(String str) {
        this.c = str;
    }

    public void setTaskId(String str) {
        this.f1513a = str;
    }
}
