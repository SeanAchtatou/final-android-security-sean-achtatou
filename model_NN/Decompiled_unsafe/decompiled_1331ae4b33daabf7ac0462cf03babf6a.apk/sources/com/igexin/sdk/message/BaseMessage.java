package com.igexin.sdk.message;

import com.igexin.push.core.g;
import java.io.Serializable;

public class BaseMessage implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f1508a = g.f1382a;

    /* renamed from: b  reason: collision with root package name */
    private String f1509b = g.e;
    private String c = g.r;

    public String getAppid() {
        return this.f1508a;
    }

    public String getClientId() {
        return this.c;
    }

    public String getPkgName() {
        return this.f1509b;
    }

    public void setAppid(String str) {
        this.f1508a = str;
    }

    public void setClientId(String str) {
        this.c = str;
    }

    public void setPkgName(String str) {
        this.f1509b = str;
    }
}
