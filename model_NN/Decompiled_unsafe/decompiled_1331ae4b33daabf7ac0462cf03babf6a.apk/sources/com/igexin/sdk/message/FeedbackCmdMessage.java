package com.igexin.sdk.message;

public class FeedbackCmdMessage extends GTCmdMessage {

    /* renamed from: a  reason: collision with root package name */
    private String f1510a;

    /* renamed from: b  reason: collision with root package name */
    private String f1511b;
    private String c;
    private long d;

    public FeedbackCmdMessage() {
    }

    public FeedbackCmdMessage(String str, String str2, String str3, long j, int i) {
        super(i);
        this.f1510a = str;
        this.f1511b = str2;
        this.c = str3;
        this.d = j;
    }

    public String getActionId() {
        return this.f1511b;
    }

    public String getResult() {
        return this.c;
    }

    public String getTaskId() {
        return this.f1510a;
    }

    public long getTimeStamp() {
        return this.d;
    }

    public void setActionId(String str) {
        this.f1511b = str;
    }

    public void setResult(String str) {
        this.c = str;
    }

    public void setTaskId(String str) {
        this.f1510a = str;
    }

    public void setTimeStamp(long j) {
        this.d = j;
    }
}
