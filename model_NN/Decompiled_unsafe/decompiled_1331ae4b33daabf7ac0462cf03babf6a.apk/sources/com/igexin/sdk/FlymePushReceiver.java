package com.igexin.sdk;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.assist.MessageBean;
import com.igexin.assist.a.b;
import com.igexin.assist.action.MessageManger;
import com.igexin.assist.sdk.AssistPushConsts;
import com.meizu.cloud.pushsdk.MzPushMessageReceiver;
import com.meizu.cloud.pushsdk.platform.message.PushSwitchStatus;
import com.meizu.cloud.pushsdk.platform.message.RegisterStatus;
import com.meizu.cloud.pushsdk.platform.message.SubAliasStatus;
import com.meizu.cloud.pushsdk.platform.message.SubTagsStatus;
import com.meizu.cloud.pushsdk.platform.message.UnRegisterStatus;
import org.json.JSONObject;

public class FlymePushReceiver extends MzPushMessageReceiver {
    public static final String CONTENT = "content";
    public static final String MSG_KEY_PAYLOAD = "gt_payload";
    public static final String TAG = "Assist_MzPushReceiver";

    public void onMessage(Context context, Intent intent) {
        if (context != null && intent != null) {
            try {
                Log.d(TAG, "onMessage intent msg...");
                String stringExtra = intent.getStringExtra("content");
                if (!TextUtils.isEmpty(stringExtra)) {
                    MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, stringExtra);
                    messageBean.setMessageSource(AssistPushConsts.MZ_PREFIX);
                    MessageManger.getInstance().addMessage(messageBean);
                }
            } catch (Throwable th) {
            }
        }
    }

    public void onMessage(Context context, String str) {
        try {
            Log.d(TAG, "onMessage receive msg ...");
            if (context != null && !TextUtils.isEmpty(str)) {
                MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, str);
                messageBean.setMessageSource(AssistPushConsts.MZ_PREFIX);
                MessageManger.getInstance().addMessage(messageBean);
            }
        } catch (Throwable th) {
        }
    }

    public void onNotificationArrived(Context context, String str, String str2, String str3) {
        Log.d(TAG, "onNotificationArrived receive msg ...");
    }

    public void onNotificationClicked(Context context, String str, String str2, String str3) {
        try {
            Log.d(TAG, "onNotificationClicked receive msg ...");
            if (context != null && !TextUtils.isEmpty(str3)) {
                try {
                    JSONObject jSONObject = new JSONObject(str3);
                    if (jSONObject.has("gt_payload")) {
                        str3 = jSONObject.getString("gt_payload");
                    }
                } catch (Throwable th) {
                }
                if (!TextUtils.isEmpty(str3)) {
                    MessageBean messageBean = new MessageBean(context, AssistPushConsts.MSG_TYPE_PAYLOAD, str3);
                    messageBean.setMessageSource(AssistPushConsts.MZ_PREFIX);
                    MessageManger.getInstance().addMessage(messageBean);
                }
            }
            b.a(context);
        } catch (Throwable th2) {
        }
    }

    public void onPushStatus(Context context, PushSwitchStatus pushSwitchStatus) {
    }

    public void onRegister(Context context, String str) {
        try {
            Log.d(TAG, "onRegister :" + str);
            if (context != null && !TextUtils.isEmpty(str)) {
                MessageManger.getInstance().addMessage(new MessageBean(context, AssistPushConsts.MSG_TYPE_TOKEN, AssistPushConsts.MZ_PREFIX + str));
            }
        } catch (Throwable th) {
        }
    }

    public void onRegisterStatus(Context context, RegisterStatus registerStatus) {
        try {
            Log.d(TAG, "onRegisterStatus :" + registerStatus);
            String pushId = registerStatus.getPushId();
            if (context != null && !TextUtils.isEmpty(pushId)) {
                MessageManger.getInstance().addMessage(new MessageBean(context, AssistPushConsts.MSG_TYPE_TOKEN, AssistPushConsts.MZ_PREFIX + pushId));
            }
        } catch (Throwable th) {
        }
    }

    public void onSubAliasStatus(Context context, SubAliasStatus subAliasStatus) {
    }

    public void onSubTagsStatus(Context context, SubTagsStatus subTagsStatus) {
    }

    public void onUnRegister(Context context, boolean z) {
    }

    public void onUnRegisterStatus(Context context, UnRegisterStatus unRegisterStatus) {
    }
}
