package com.igexin.sdk.a;

import android.content.Context;
import com.igexin.b.a.c.a;
import java.io.File;
import java.io.IOException;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private String f1502a;

    public c(Context context) {
        if (context != null) {
            this.f1502a = context.getFilesDir().getPath() + "/" + "push.pid";
        }
    }

    public void a() {
        if (!c() && this.f1502a != null) {
            try {
                new File(this.f1502a).createNewFile();
            } catch (IOException e) {
                a.b("SdkPushSwitch | switchOn, create file = " + this.f1502a + " exception, " + e.toString());
            }
        }
    }

    public void b() {
        if (c() && this.f1502a != null && !new File(this.f1502a).delete()) {
            a.b("SdkPushSwitch | switchOff, delete file = " + this.f1502a + " failed !!!!!!!!!!!!");
        }
    }

    public boolean c() {
        if (this.f1502a != null) {
            return new File(this.f1502a).exists();
        }
        return false;
    }
}
