package com.igexin.sdk.a;

import android.content.Context;
import java.io.File;
import java.io.IOException;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private String f1503a;

    /* renamed from: b  reason: collision with root package name */
    private String f1504b;
    private Context c;

    public d(Context context) {
        if (context != null) {
            this.c = context;
            this.f1503a = context.getFilesDir().getPath() + "/" + "run.pid";
            this.f1504b = context.getFilesDir().getPath() + "/" + "stop.lock";
        }
    }

    public void a() {
        if (!c() && this.f1503a != null) {
            try {
                new File(this.f1503a).createNewFile();
            } catch (IOException e) {
            }
        }
    }

    public void b() {
        if (c() && this.f1503a != null) {
            new File(this.f1503a).delete();
        }
    }

    public boolean c() {
        File file = null;
        File file2 = this.f1503a != null ? new File(this.f1503a) : null;
        if (this.f1504b != null) {
            file = new File(this.f1504b);
        }
        if (file2 != null && file2.exists()) {
            if (file != null && file.exists()) {
                file.delete();
            }
            return true;
        } else if (file == null || !file.exists() || !file.renameTo(new File(this.f1503a))) {
            return false;
        } else {
            new c(this.c).a();
            return true;
        }
    }
}
