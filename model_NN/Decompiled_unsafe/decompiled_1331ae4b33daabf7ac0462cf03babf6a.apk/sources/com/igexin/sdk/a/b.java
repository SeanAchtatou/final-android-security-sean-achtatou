package com.igexin.sdk.a;

import android.content.Context;
import java.io.File;
import java.io.IOException;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f1501a;

    public b(Context context) {
        if (context != null) {
            this.f1501a = context.getFilesDir().getPath() + "/" + "init.pid";
        }
    }

    public void a() {
        if (!b() && this.f1501a != null) {
            try {
                new File(this.f1501a).createNewFile();
            } catch (IOException e) {
            }
        }
    }

    public boolean b() {
        if (this.f1501a != null) {
            return new File(this.f1501a).exists();
        }
        return false;
    }
}
