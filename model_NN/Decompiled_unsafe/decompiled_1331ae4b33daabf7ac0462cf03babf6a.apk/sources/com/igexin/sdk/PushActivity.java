package com.igexin.sdk;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import com.igexin.sdk.a.a;

public class PushActivity extends Activity {
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (a.a().b() != null) {
            a.a().b().onActivityConfigurationChanged(this, configuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        if (a.a().b() != null) {
            return a.a().b().onActivityCreateOptionsMenu(this, menu);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (a.a().b() != null) {
            a.a().b().onActivityDestroy(this);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (a.a().b() == null || !a.a().b().onActivityKeyDown(this, i, keyEvent)) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (a.a().b() != null) {
            a.a().b().onActivityNewIntent(this, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (a.a().b() != null) {
            a.a().b().onActivityPause(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (a.a().b() != null) {
            a.a().b().onActivityRestart(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (a.a().b() != null) {
            a.a().b().onActivityResume(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (a.a().b() != null) {
            a.a().b().onActivityStart(this, getIntent());
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (a.a().b() != null) {
            a.a().b().onActivityStop(this);
        }
    }
}
