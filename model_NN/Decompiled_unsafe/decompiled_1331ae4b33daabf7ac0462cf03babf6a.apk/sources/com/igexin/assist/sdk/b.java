package com.igexin.assist.sdk;

import android.content.Context;
import com.igexin.assist.control.AbstractPushManager;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private AbstractPushManager f1110a;

    private b() {
    }

    public static b a() {
        return d.f1111a;
    }

    public void a(Context context) {
        this.f1110a = a.a(context);
    }

    public void a(Context context, int i, int i2) {
        if (this.f1110a != null) {
            this.f1110a.setSilentTime(context, i, i2);
        }
    }

    public void b(Context context) {
        if (this.f1110a != null) {
            this.f1110a.register(context);
        }
    }

    public void c(Context context) {
        if (this.f1110a != null) {
            this.f1110a.turnOnPush(context);
        }
    }

    public void d(Context context) {
        if (this.f1110a != null) {
            this.f1110a.turnOffPush(context);
        }
    }
}
