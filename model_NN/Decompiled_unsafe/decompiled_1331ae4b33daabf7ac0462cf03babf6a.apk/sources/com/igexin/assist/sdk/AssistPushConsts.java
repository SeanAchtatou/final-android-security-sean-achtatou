package com.igexin.assist.sdk;

public class AssistPushConsts {
    public static final String GETUI_APPID = "PUSH_APPID";
    public static final String GETUI_APPKEY = "PUSH_APPKEY";
    public static final String GETUI_APPSECRET = "PUSH_APPSECRET";
    public static final String HW_PREFIX = "HW_";
    public static final String LOG_TAG = "Assist_";
    public static final String MEIZUPUSH_APPID = "MEIZUPUSH_APPID";
    public static final String MEIZUPUSH_APPKEY = "MEIZUPUSH_APPKEY";
    public static final String MIPUSH_APPID = "MIPUSH_APPID";
    public static final String MIPUSH_APPKEY = "MIPUSH_APPKEY";
    public static final String MSG_KEY_ACTION = "AC";
    public static final String MSG_KEY_CONTENT = "CT";
    public static final String MSG_KEY_TASKID = "TI";
    public static final String MSG_TYPE_PAYLOAD = "payload";
    public static final String MSG_TYPE_TOKEN = "token";
    public static final String MSG_VALUE_PAYLOAD = "PT";
    public static final String MZ_PREFIX = "MZ_";
    public static final String XM_PREFIX = "XM_";
}
