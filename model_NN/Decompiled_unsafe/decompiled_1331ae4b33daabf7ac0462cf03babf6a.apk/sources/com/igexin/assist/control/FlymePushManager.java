package com.igexin.assist.control;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.assist.sdk.AssistPushConsts;
import com.meizu.cloud.pushsdk.PushManager;
import com.meizu.cloud.pushsdk.util.MzSystemUtils;

public class FlymePushManager implements AbstractPushManager {
    public static final String TAG = "Assist_FlymePushManager";

    /* renamed from: a  reason: collision with root package name */
    private String f1103a = "";

    /* renamed from: b  reason: collision with root package name */
    private String f1104b = "";

    public FlymePushManager(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            this.f1103a = (String) applicationInfo.metaData.get(AssistPushConsts.MEIZUPUSH_APPID);
            this.f1103a = this.f1103a.replace(AssistPushConsts.MZ_PREFIX, "");
            this.f1104b = (String) applicationInfo.metaData.get(AssistPushConsts.MEIZUPUSH_APPKEY);
            this.f1104b = this.f1104b.replace(AssistPushConsts.MZ_PREFIX, "");
        } catch (Throwable th) {
        }
    }

    public static boolean checkMZDevice(Context context) {
        try {
            if (!MzSystemUtils.isBrandMeizu()) {
                return false;
            }
            String str = Build.DISPLAY;
            if (TextUtils.isEmpty(str)) {
                return true;
            }
            if (!str.startsWith("Flyme OS") && !str.startsWith("Flyme")) {
                return true;
            }
            try {
                return Integer.valueOf(str.replaceAll("Flyme OS", "").replaceAll("Flyme", "").trim().split("\\.")[0]).intValue() >= 5;
            } catch (Throwable th) {
                return true;
            }
        } catch (Throwable th2) {
            return false;
        }
    }

    public String getToken(Context context) {
        if (context == null) {
            return null;
        }
        return PushManager.getPushId(context);
    }

    public void register(Context context) {
        try {
            if (TextUtils.isEmpty(this.f1103a) || TextUtils.isEmpty(this.f1104b)) {
                Log.d(TAG, "Register meizupush appId not find");
                return;
            }
            Log.d(TAG, "Register meizupush, pkg = " + context.getPackageName());
            PushManager.register(context, this.f1103a, this.f1104b);
        } catch (Throwable th) {
        }
    }

    public void setSilentTime(Context context, int i, int i2) {
    }

    public void turnOffPush(Context context) {
        if (context != null) {
            String pushId = PushManager.getPushId(context);
            if (!TextUtils.isEmpty(this.f1103a) && !TextUtils.isEmpty(this.f1104b) && !TextUtils.isEmpty(pushId)) {
                PushManager.switchPush(context, this.f1103a, this.f1104b, pushId, 0, false);
                PushManager.switchPush(context, this.f1103a, this.f1104b, pushId, 1, false);
            }
        }
    }

    public void turnOnPush(Context context) {
        if (context != null) {
            String pushId = PushManager.getPushId(context);
            if (!TextUtils.isEmpty(this.f1103a) && !TextUtils.isEmpty(this.f1104b) && !TextUtils.isEmpty(pushId)) {
                PushManager.switchPush(context, this.f1103a, this.f1104b, pushId, 0, true);
                PushManager.switchPush(context, this.f1103a, this.f1104b, pushId, 1, true);
            }
        }
    }

    public void unregister(Context context) {
        try {
            if (TextUtils.isEmpty(this.f1103a) || TextUtils.isEmpty(this.f1104b)) {
                Log.d(TAG, "|Unregister meizupush appId not find");
                return;
            }
            Log.d(TAG, "|Unregister meizupush");
            PushManager.unRegister(context, this.f1103a, this.f1104b);
        } catch (Throwable th) {
        }
    }
}
