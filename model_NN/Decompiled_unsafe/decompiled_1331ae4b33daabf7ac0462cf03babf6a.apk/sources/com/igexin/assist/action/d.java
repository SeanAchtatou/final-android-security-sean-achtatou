package com.igexin.assist.action;

import android.content.Context;
import android.text.TextUtils;
import com.igexin.assist.MessageBean;
import com.igexin.assist.a.a;
import com.igexin.assist.sdk.AssistPushConsts;
import java.util.UUID;
import org.json.JSONObject;

class d {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f1101a;

    /* renamed from: b  reason: collision with root package name */
    private String f1102b;
    private String c;
    private String d;
    private String e;
    private String f;

    d() {
    }

    public void a(MessageBean messageBean) {
        try {
            Context context = messageBean.getContext();
            String stringMessage = messageBean.getStringMessage();
            if (!TextUtils.isEmpty(stringMessage) && context != null) {
                this.d = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(AssistPushConsts.GETUI_APPID);
                if (!TextUtils.isEmpty(this.d)) {
                    this.e = context.getPackageName();
                    this.c = (TextUtils.isEmpty(messageBean.getMessageSource()) ? "" : messageBean.getMessageSource()) + UUID.randomUUID().toString();
                    String a2 = a.a(stringMessage, this.d);
                    if (!TextUtils.isEmpty(a2)) {
                        JSONObject jSONObject = new JSONObject(a2);
                        if (jSONObject.has(AssistPushConsts.MSG_KEY_TASKID)) {
                            this.f1102b = jSONObject.getString(AssistPushConsts.MSG_KEY_TASKID);
                        }
                        if (jSONObject.has(AssistPushConsts.MSG_KEY_ACTION)) {
                            this.f = jSONObject.getString(AssistPushConsts.MSG_KEY_ACTION);
                        }
                        if (jSONObject.has(AssistPushConsts.MSG_KEY_CONTENT) && !TextUtils.isEmpty(jSONObject.getString(AssistPushConsts.MSG_KEY_CONTENT))) {
                            this.f1101a = jSONObject.getString(AssistPushConsts.MSG_KEY_CONTENT).getBytes();
                        }
                    }
                }
            }
        } catch (Throwable th) {
        }
    }

    public boolean a() {
        return this.f1101a != null && !TextUtils.isEmpty(this.f1102b) && !TextUtils.isEmpty(this.e) && !TextUtils.isEmpty(this.d) && !TextUtils.isEmpty(this.f) && !TextUtils.isEmpty(this.c);
    }

    public byte[] b() {
        return this.f1101a;
    }

    public String c() {
        return this.f1102b;
    }

    public String d() {
        return this.c;
    }

    public String e() {
        return this.d;
    }

    public String f() {
        return this.f;
    }

    public String g() {
        return this.e;
    }
}
