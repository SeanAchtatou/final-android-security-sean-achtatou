package com.igexin.assist.action;

import android.text.TextUtils;
import com.igexin.assist.MessageBean;
import com.igexin.assist.sdk.AssistPushConsts;

class b extends Thread {

    /* renamed from: a  reason: collision with root package name */
    MessageBean f1098a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ MessageManger f1099b;

    b(MessageManger messageManger, MessageBean messageBean) {
        this.f1099b = messageManger;
        this.f1098a = messageBean;
    }

    public void run() {
        try {
            if (this.f1098a == null) {
                return;
            }
            if (this.f1098a.getMessageType().equals(AssistPushConsts.MSG_TYPE_TOKEN)) {
                this.f1099b.a(this.f1098a.getStringMessage());
            } else if (this.f1098a.getMessageType().equals(AssistPushConsts.MSG_TYPE_PAYLOAD) && !TextUtils.isEmpty(this.f1098a.getStringMessage())) {
                d dVar = new d();
                dVar.a(this.f1098a);
                if (dVar.a() && dVar.f().equals(AssistPushConsts.MSG_VALUE_PAYLOAD)) {
                    this.f1099b.a(dVar, this.f1098a.getContext());
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
