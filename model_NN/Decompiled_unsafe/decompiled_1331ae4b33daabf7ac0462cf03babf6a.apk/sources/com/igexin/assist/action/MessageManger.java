package com.igexin.assist.action;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.igexin.assist.MessageBean;
import com.igexin.assist.sdk.AssistPushConsts;
import com.igexin.push.core.a.e;
import com.igexin.push.core.b.c;
import com.igexin.push.core.g;
import com.igexin.push.util.q;
import com.igexin.sdk.PushConsts;
import com.igexin.sdk.message.GTTransmitMessage;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MessageManger {

    /* renamed from: a  reason: collision with root package name */
    private ThreadPoolExecutor f1097a;

    private MessageManger() {
        this.f1097a = new ThreadPoolExecutor(1, 2, 20, TimeUnit.SECONDS, new LinkedBlockingDeque());
    }

    private Class a(Context context) {
        try {
            String str = (String) q.b(context, "uis", "");
            if (!TextUtils.isEmpty(str)) {
                return Class.forName(str);
            }
        } catch (Throwable th) {
        }
        return null;
    }

    /* access modifiers changed from: private */
    public void a(d dVar, Context context) {
        if (dVar != null) {
            try {
                c cVar = new c(context);
                if (!cVar.a(dVar.c())) {
                    cVar.b(dVar.c());
                    cVar.a();
                    Class a2 = a(context);
                    if (a2 != null) {
                        Intent intent = new Intent(context, a2);
                        Bundle bundle = new Bundle();
                        bundle.putInt("action", 10001);
                        bundle.putSerializable(PushConsts.KEY_MESSAGE_DATA, new GTTransmitMessage(dVar.c(), dVar.d(), dVar.d() + ":" + dVar.c(), dVar.b()));
                        intent.putExtras(bundle);
                        context.startService(intent);
                        return;
                    }
                    Intent intent2 = new Intent();
                    if (Build.VERSION.SDK_INT >= 12) {
                        intent2.addFlags(32);
                    }
                    intent2.setAction("com.igexin.sdk.action." + dVar.e());
                    Bundle bundle2 = new Bundle();
                    bundle2.putInt("action", 10001);
                    bundle2.putString("taskid", dVar.c());
                    bundle2.putString("messageid", dVar.d());
                    bundle2.putString("appid", dVar.e());
                    bundle2.putString("payloadid", dVar.d() + ":" + dVar.c());
                    bundle2.putString("packagename", dVar.g());
                    bundle2.putByteArray(AssistPushConsts.MSG_TYPE_PAYLOAD, dVar.b());
                    intent2.putExtras(bundle2);
                    context.sendBroadcast(intent2);
                }
            } catch (Throwable th) {
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        try {
            if (!TextUtils.isEmpty(str) && g.g.get() && !str.equals(g.z)) {
                Log.e("Assist_MessageManger", "other token = " + str);
                com.igexin.push.core.b.g.a().c(str);
                if (g.l) {
                    e.a().j();
                }
            }
        } catch (Throwable th) {
        }
    }

    public static MessageManger getInstance() {
        return c.f1100a;
    }

    public void addMessage(MessageBean messageBean) {
        if (this.f1097a != null) {
            this.f1097a.execute(new b(this, messageBean));
        }
    }
}
