package com.igexin.download;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class j extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SdkDownLoader f1193a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    j(SdkDownLoader sdkDownLoader, Looper looper) {
        super(looper);
        this.f1193a = sdkDownLoader;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 2:
                synchronized (this.f1193a.h) {
                    if (this.f1193a.g.size() > 0 && this.f1193a.updateData.size() > 0) {
                        for (DownloadInfo downloadInfo : this.f1193a.updateData.values()) {
                            IDownloadCallback a2 = this.f1193a.a(downloadInfo.mData8);
                            if (a2 != null) {
                                a2.update(downloadInfo);
                            }
                        }
                    }
                }
                return;
            default:
                return;
        }
    }
}
