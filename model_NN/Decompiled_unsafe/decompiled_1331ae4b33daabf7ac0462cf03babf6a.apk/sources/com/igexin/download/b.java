package com.igexin.download;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

final class b extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadProvider f1181a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(DownloadProvider downloadProvider, Context context) {
        super(context, DownloadProvider.f1171a, (SQLiteDatabase.CursorFactory) null, 101);
        this.f1181a = downloadProvider;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        this.f1181a.a(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i != 31 || i2 != 100) {
            this.f1181a.b(sQLiteDatabase);
            this.f1181a.a(sQLiteDatabase);
        }
    }
}
