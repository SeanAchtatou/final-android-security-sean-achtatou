package com.igexin.a.a;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class k extends f {
    public k(j jVar, e eVar, long j) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(eVar.f1086a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = eVar.c + (((long) eVar.e) * j);
        this.f1088a = jVar.c(allocate, j2);
        this.f1089b = jVar.c(allocate, 4 + j2);
        this.c = jVar.c(allocate, 8 + j2);
        this.d = jVar.c(allocate, j2 + 20);
    }
}
