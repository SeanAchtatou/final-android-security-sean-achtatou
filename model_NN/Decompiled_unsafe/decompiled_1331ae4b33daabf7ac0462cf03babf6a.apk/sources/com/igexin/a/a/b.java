package com.igexin.a.a;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class b extends d {
    public b(j jVar, e eVar, long j, int i) {
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.order(eVar.f1086a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = ((long) (i * 16)) + j;
        this.f1084a = jVar.b(allocate, j2);
        this.f1085b = jVar.b(allocate, j2 + 8);
    }
}
