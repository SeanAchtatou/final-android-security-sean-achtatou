package com.igexin.a.a;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class l extends f {
    public l(j jVar, e eVar, long j) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(eVar.f1086a ? ByteOrder.BIG_ENDIAN : ByteOrder.LITTLE_ENDIAN);
        long j2 = eVar.c + (((long) eVar.e) * j);
        this.f1088a = jVar.c(allocate, j2);
        this.f1089b = jVar.b(allocate, 8 + j2);
        this.c = jVar.b(allocate, 16 + j2);
        this.d = jVar.b(allocate, j2 + 40);
    }
}
