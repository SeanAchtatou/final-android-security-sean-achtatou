package com.igexin.a;

import java.io.File;
import java.io.FilenameFilter;

class i implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f1095a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ h f1096b;

    i(h hVar, String str) {
        this.f1096b = hVar;
        this.f1095a = str;
    }

    public boolean accept(File file, String str) {
        return str.startsWith(this.f1095a);
    }
}
