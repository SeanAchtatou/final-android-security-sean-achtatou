package cn.banshenggua.aichang.app;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import cn.a.a.a;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.utils.CommonUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.Toaster;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.dialog.MyDialogFragment;
import com.pocketmusic.kshare.requestobjs.i;
import java.io.File;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class DownloadService extends Service {
    public static final String ACTION_ADD_TO_DOWNLOAD = "com.pocketmusic.kshare.action.add_to_download";
    public static final String ACTION_DWNLOAD_PROGRESS = "cn.banshenggua.aichang.downloadprogress";
    public static final String ACTION_START_DOWNLOAD = "cn.banshenggua.aichang.startdownload";
    public static final String ACTOIN_DWNLOAD_COMP = "cn.banshenggua.aichang.downloadcomplite";
    public static final String AC_APK = (String.valueOf(CommonUtil.getDownloadDir()) + "aichang.apk");
    public static final String DOWNLOAD_URL = "aichang_apk";
    private static final String DTAG = "ACDownload";
    public static final String F_HANMAI = "hanmai";
    public static final String F_JINGXUAN = "jingxuan";
    public static final String F_ZHIBO = "zhibo";
    public static boolean isDonwloading = false;
    public static String mFrom = "";
    public static int mProgress = 0;
    /* access modifiers changed from: private */
    public String downloadurl = null;
    private int installcount = 0;
    /* access modifiers changed from: private */
    public boolean isErrorWaiting = false;
    private KShareApplication.DownloadListener mDefaultDownload = new KShareApplication.DownloadListener() {
        MyDialogFragment dialog = null;
        private int lastReportProcess = -1;

        public void onStart() {
            ULog.d(DownloadService.DTAG, "onStart");
            Toaster.showLongToast("开始下载安装包......");
            DownloadService.this.sendBroadcast(new Intent(DownloadService.ACTION_START_DOWNLOAD));
        }

        public void onLoading(int i) {
            if (i % 5 == 0 && this.lastReportProcess != i) {
                Intent intent = new Intent(DownloadService.ACTION_DWNLOAD_PROGRESS);
                intent.putExtra(DownloadService.ACTION_DWNLOAD_PROGRESS, i);
                DownloadService.this.sendBroadcast(intent);
                ULog.d(DownloadService.DTAG, "broadcast: " + i);
                this.lastReportProcess = i;
            }
        }

        public void onError(String str) {
            ULog.d(DownloadService.DTAG, "error: " + str);
            DownloadService.isDonwloading = false;
            Toaster.showLongToast("网络异常，重新下载");
            new i().b(str);
            if (!DownloadService.this.isErrorWaiting) {
                DownloadService.this.isErrorWaiting = true;
                DownloadService.this.mHandler.postDelayed(new Runnable() {
                    public void run() {
                        DownloadService.this.isErrorWaiting = false;
                        DownloadService.this.downloadFile(DownloadService.this.downloadurl, DownloadService.AC_APK, null);
                    }
                }, 5000);
            }
        }

        public void onDownloaded(String str) {
            ULog.d(DownloadService.DTAG, "save file: " + str);
            DownloadService.this.sendBroadcast(new Intent(DownloadService.ACTOIN_DWNLOAD_COMP));
            DownloadService.isDonwloading = false;
            Toaster.showLongToast("下载安装包完成，请安装。");
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
            intent.setDataAndType(Uri.fromFile(new File(DownloadService.AC_APK)), "application/vnd.android.package-archive");
            DownloadService.this.startActivity(intent);
            String str2 = "ok";
            if (DownloadService.this.getPackageManager().getPackageArchiveInfo(DownloadService.AC_APK, 1) == null) {
                str2 = SocketMessage.MSG_ERROR_KEY;
            }
            new i().b("install", str2);
            DownloadService.this.sendBroadcast(new Intent(SimpleLiveRoomActivity.FINISH_ROOM_ACTION));
            DownloadService.this.startEntryAcTask();
        }
    };
    /* access modifiers changed from: private */
    public Handler mHandler = new Handler() {
    };

    public void onCreate() {
        super.onCreate();
        ULog.i(KShareApplication.TAG, "DownloadService.onCreate");
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        if (intent != null) {
            String action = intent.getAction();
            Log.i(KShareApplication.TAG, "DownloadService.onStart - " + action);
            if (!TextUtils.isEmpty(action) && action.equals(ACTION_ADD_TO_DOWNLOAD)) {
                try {
                    downloadFile(intent.getStringExtra(DOWNLOAD_URL), AC_APK, null);
                } catch (Exception e) {
                }
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i(KShareApplication.TAG, "DownloadService.onDestroy");
    }

    public void downloadFile(String str, String str2, KShareApplication.DownloadListener downloadListener) {
        KShareApplication.DownloadListener downloadListener2;
        if (isDonwloading) {
            Toaster.showLongToast("已经开始下载了......");
            return;
        }
        this.downloadurl = str;
        ULog.d(DTAG, "url: " + str);
        if (downloadListener == null) {
            downloadListener2 = this.mDefaultDownload;
        } else {
            downloadListener2 = downloadListener;
        }
        new DownloadTask(str2, str, downloadListener2, false).execute(new Void[0]);
        isDonwloading = true;
    }

    private class DownloadTask extends AsyncTask<Void, Integer, String> {
        private boolean isGzip = false;
        private KShareApplication.DownloadListener mListener;
        private String mSave;
        private String mTmpFile;
        private String mUrl;
        private String mZipFile;

        public DownloadTask(String str, String str2, KShareApplication.DownloadListener downloadListener, boolean z) {
            this.mSave = str;
            this.mUrl = str2;
            this.mListener = downloadListener;
            this.isGzip = z;
            this.mZipFile = String.valueOf(str) + ".z";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int
         arg types: [java.lang.String, java.util.zip.GZIPInputStream, int]
         candidates:
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.io.File, java.io.InputStream, boolean):int
          cn.banshenggua.aichang.utils.FileUtils.streamToFile(java.lang.String, java.io.InputStream, boolean):int */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x0116, code lost:
            if (r0.isGzip == false) goto L_0x01a1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0118, code lost:
            if (r4 == null) goto L_0x011d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x011a, code lost:
            r4.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x011d, code lost:
            cn.banshenggua.aichang.utils.FileUtils.streamToFile(r0.mSave, (java.io.InputStream) new java.util.zip.GZIPInputStream(new java.io.FileInputStream(r8)), false);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x012f, code lost:
            if (r4 == null) goto L_0x0134;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
            r4.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:50:0x0134, code lost:
            if (r5 == null) goto L_0x0139;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x0136, code lost:
            r5.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x0139, code lost:
            if (r2 != null) goto L_0x013b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x013b, code lost:
            r2.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x013e, code lost:
            return null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x0186, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x0187, code lost:
            r18 = r3;
            r3 = r2;
            r2 = r18;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:?, code lost:
            r8.renameTo(new java.io.File(r0.mSave));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x01ae, code lost:
            r3 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x01af, code lost:
            r18 = r3;
            r3 = r2;
            r2 = r18;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
            r4.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x01bb, code lost:
            r5.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x01c0, code lost:
            r3.disconnect();
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:110:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x019c  */
        /* JADX WARNING: Removed duplicated region for block: B:87:0x01ae A[ExcHandler: all (r3v3 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r4 r5 
          PHI: (r4v5 java.io.RandomAccessFile) = (r4v0 java.io.RandomAccessFile), (r4v0 java.io.RandomAccessFile), (r4v0 java.io.RandomAccessFile), (r4v0 java.io.RandomAccessFile), (r4v0 java.io.RandomAccessFile), (r4v8 java.io.RandomAccessFile), (r4v8 java.io.RandomAccessFile), (r4v8 java.io.RandomAccessFile), (r4v0 java.io.RandomAccessFile) binds: [B:3:0x0032, B:17:0x0077, B:18:?, B:35:0x00ee, B:55:0x0141, B:85:0x01a1, B:86:?, B:71:0x016a, B:20:0x008c] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r5v3 java.io.InputStream) = (r5v0 java.io.InputStream), (r5v0 java.io.InputStream), (r5v0 java.io.InputStream), (r5v0 java.io.InputStream), (r5v5 java.io.InputStream), (r5v5 java.io.InputStream), (r5v5 java.io.InputStream), (r5v5 java.io.InputStream), (r5v0 java.io.InputStream) binds: [B:3:0x0032, B:17:0x0077, B:18:?, B:35:0x00ee, B:55:0x0141, B:85:0x01a1, B:86:?, B:71:0x016a, B:20:0x008c] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0032] */
        /* JADX WARNING: Removed duplicated region for block: B:90:0x01b6 A[SYNTHETIC, Splitter:B:90:0x01b6] */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x01bb A[Catch:{ IOException -> 0x01c7 }] */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x01c0  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.String doInBackground(java.lang.Void... r20) {
            /*
                r19 = this;
                r5 = 0
                r4 = 0
                r3 = 0
                java.io.File r8 = new java.io.File     // Catch:{ Exception -> 0x01cd }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01cd }
                r0 = r19
                java.lang.String r6 = r0.mSave     // Catch:{ Exception -> 0x01cd }
                java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x01cd }
                r2.<init>(r6)     // Catch:{ Exception -> 0x01cd }
                java.lang.String r6 = ".tmp"
                java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x01cd }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x01cd }
                r8.<init>(r2)     // Catch:{ Exception -> 0x01cd }
                r6 = 0
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x01cd }
                r0 = r19
                java.lang.String r7 = r0.mUrl     // Catch:{ Exception -> 0x01cd }
                r2.<init>(r7)     // Catch:{ Exception -> 0x01cd }
                r8.deleteOnExit()     // Catch:{ Exception -> 0x01cd }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x01cd }
                java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x01cd }
                r2.connect()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                int r3 = r2.getResponseCode()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r7 = 200(0xc8, float:2.8E-43)
                if (r3 == r7) goto L_0x0077
                int r3 = r2.getResponseCode()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r7 = 206(0xce, float:2.89E-43)
                if (r3 == r7) goto L_0x0077
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r6 = "Server returned HTTP "
                r3.<init>(r6)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                int r6 = r2.getResponseCode()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r6 = " "
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r6 = r2.getResponseMessage()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r4 == 0) goto L_0x006b
                r4.close()     // Catch:{ IOException -> 0x01d4 }
            L_0x006b:
                if (r5 == 0) goto L_0x0070
                r5.close()     // Catch:{ IOException -> 0x01d4 }
            L_0x0070:
                if (r2 == 0) goto L_0x0075
                r2.disconnect()
            L_0x0075:
                r2 = r3
            L_0x0076:
                return r2
            L_0x0077:
                int r3 = r2.getContentLength()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                int r9 = r3 + r6
                java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r0 = r19
                java.lang.String r7 = r0.mSave     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r3.<init>(r7)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                boolean r3 = r3.exists()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r3 == 0) goto L_0x00ee
                java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r0 = r19
                java.lang.String r7 = r0.mSave     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r3.<init>(r7)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r3.hashCode()     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r0 = r19
                cn.banshenggua.aichang.app.DownloadService r3 = cn.banshenggua.aichang.app.DownloadService.this     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                android.content.pm.PackageManager r3 = r3.getPackageManager()     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r0 = r19
                java.lang.String r7 = r0.mSave     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r10 = 1
                android.content.pm.PackageInfo r3 = r3.getPackageArchiveInfo(r7, r10)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                if (r3 == 0) goto L_0x00ee
                java.lang.String r3 = r3.packageName     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                java.lang.String r7 = "cn.banshenggua.aichang"
                boolean r3 = r3.equals(r7)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                if (r3 == 0) goto L_0x00ee
                r3 = 1
                java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r7 = 0
                r10 = 90
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r3[r7] = r10     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r0 = r19
                r0.publishProgress(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r10 = 1500(0x5dc, double:7.41E-321)
                java.lang.Thread.sleep(r10)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r3 = 1
                java.lang.Integer[] r3 = new java.lang.Integer[r3]     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r7 = 0
                r10 = 100
                java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r3[r7] = r10     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                r0 = r19
                r0.publishProgress(r3)     // Catch:{ Exception -> 0x00ed, all -> 0x01ae }
                if (r4 == 0) goto L_0x00e1
                r4.close()     // Catch:{ IOException -> 0x01d1 }
            L_0x00e1:
                if (r5 == 0) goto L_0x00e6
                r5.close()     // Catch:{ IOException -> 0x01d1 }
            L_0x00e6:
                if (r2 == 0) goto L_0x00eb
                r2.disconnect()
            L_0x00eb:
                r2 = 0
                goto L_0x0076
            L_0x00ed:
                r3 = move-exception
            L_0x00ee:
                java.io.InputStream r5 = r2.getInputStream()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r0 = r19
                boolean r3 = r0.isGzip     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r3 == 0) goto L_0x0141
                java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r7 = "rw"
                r3.<init>(r8, r7)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r4 = r3
            L_0x0100:
                r3 = 4096(0x1000, float:5.74E-42)
                byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                long r10 = (long) r6     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r4.seek(r10)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                int r10 = r6 + r9
                long r6 = (long) r6     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
            L_0x010b:
                int r11 = r5.read(r3)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r12 = -1
                if (r11 != r12) goto L_0x014a
                r0 = r19
                boolean r3 = r0.isGzip     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r3 == 0) goto L_0x01a1
                if (r4 == 0) goto L_0x011d
                r4.close()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
            L_0x011d:
                java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r3.<init>(r8)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.util.zip.GZIPInputStream r6 = new java.util.zip.GZIPInputStream     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r6.<init>(r3)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r0 = r19
                java.lang.String r3 = r0.mSave     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r7 = 0
                cn.banshenggua.aichang.utils.FileUtils.streamToFile(r3, r6, r7)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
            L_0x012f:
                if (r4 == 0) goto L_0x0134
                r4.close()     // Catch:{ IOException -> 0x01c4 }
            L_0x0134:
                if (r5 == 0) goto L_0x0139
                r5.close()     // Catch:{ IOException -> 0x01c4 }
            L_0x0139:
                if (r2 == 0) goto L_0x013e
                r2.disconnect()
            L_0x013e:
                r2 = 0
                goto L_0x0076
            L_0x0141:
                java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.String r7 = "rw"
                r3.<init>(r8, r7)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r4 = r3
                goto L_0x0100
            L_0x014a:
                boolean r12 = r19.isCancelled()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r12 == 0) goto L_0x0165
                r5.close()     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                if (r4 == 0) goto L_0x0158
                r4.close()     // Catch:{ IOException -> 0x01cf }
            L_0x0158:
                if (r5 == 0) goto L_0x015d
                r5.close()     // Catch:{ IOException -> 0x01cf }
            L_0x015d:
                if (r2 == 0) goto L_0x0162
                r2.disconnect()
            L_0x0162:
                r2 = 0
                goto L_0x0076
            L_0x0165:
                long r12 = (long) r11
                long r6 = r6 + r12
                if (r9 <= 0) goto L_0x0181
                r12 = 1
                java.lang.Integer[] r12 = new java.lang.Integer[r12]     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r13 = 0
                r14 = 100
                long r14 = r14 * r6
                long r0 = (long) r10     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r16 = r0
                long r14 = r14 / r16
                int r14 = (int) r14     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                java.lang.Integer r14 = java.lang.Integer.valueOf(r14)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r12[r13] = r14     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r0 = r19
                r0.publishProgress(r12)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
            L_0x0181:
                r12 = 0
                r4.write(r3, r12, r11)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                goto L_0x010b
            L_0x0186:
                r3 = move-exception
                r18 = r3
                r3 = r2
                r2 = r18
            L_0x018c:
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x01c9 }
                if (r4 == 0) goto L_0x0195
                r4.close()     // Catch:{ IOException -> 0x01cb }
            L_0x0195:
                if (r5 == 0) goto L_0x019a
                r5.close()     // Catch:{ IOException -> 0x01cb }
            L_0x019a:
                if (r3 == 0) goto L_0x0076
                r3.disconnect()
                goto L_0x0076
            L_0x01a1:
                java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r0 = r19
                java.lang.String r6 = r0.mSave     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r3.<init>(r6)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                r8.renameTo(r3)     // Catch:{ Exception -> 0x0186, all -> 0x01ae }
                goto L_0x012f
            L_0x01ae:
                r3 = move-exception
                r18 = r3
                r3 = r2
                r2 = r18
            L_0x01b4:
                if (r4 == 0) goto L_0x01b9
                r4.close()     // Catch:{ IOException -> 0x01c7 }
            L_0x01b9:
                if (r5 == 0) goto L_0x01be
                r5.close()     // Catch:{ IOException -> 0x01c7 }
            L_0x01be:
                if (r3 == 0) goto L_0x01c3
                r3.disconnect()
            L_0x01c3:
                throw r2
            L_0x01c4:
                r3 = move-exception
                goto L_0x0139
            L_0x01c7:
                r4 = move-exception
                goto L_0x01be
            L_0x01c9:
                r2 = move-exception
                goto L_0x01b4
            L_0x01cb:
                r4 = move-exception
                goto L_0x019a
            L_0x01cd:
                r2 = move-exception
                goto L_0x018c
            L_0x01cf:
                r3 = move-exception
                goto L_0x015d
            L_0x01d1:
                r3 = move-exception
                goto L_0x00e6
            L_0x01d4:
                r4 = move-exception
                goto L_0x0070
            */
            throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.app.DownloadService.DownloadTask.doInBackground(java.lang.Void[]):java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String str) {
            super.onPostExecute((Object) str);
            if (str == null && this.mListener != null) {
                this.mListener.onDownloaded(this.mSave);
            } else if (this.mListener != null) {
                this.mListener.onError(str);
            }
        }

        /* access modifiers changed from: protected */
        public void onProgressUpdate(Integer... numArr) {
            super.onProgressUpdate((Object[]) numArr);
            if (this.mListener != null) {
                this.mListener.onLoading(numArr[0].intValue());
            }
            DownloadService.mProgress = numArr[0].intValue();
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
            super.onPreExecute();
            if (this.mListener != null) {
                this.mListener.onStart();
            }
            DownloadService.mProgress = 0;
        }
    }

    /* access modifiers changed from: private */
    public void entryAc() {
        Intent intent = new Intent();
        intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        intent.setComponent(new ComponentName(KShareUtil.AC_PACK, KShareUtil.AC_MAIN));
        startActivity(intent);
        new i().a("startac");
        sendBroadcast(new Intent(SimpleLiveRoomActivity.FINISH_ROOM_ACTION));
        Toaster.showLongToast(a.h.entry_ac_toast);
    }

    /* access modifiers changed from: private */
    public void startEntryAcTask() {
        this.mHandler.post(new Runnable() {
            public void run() {
                try {
                    if (KShareApplication.getInstance().hasInstallAc()) {
                        DownloadService.this.entryAc();
                    } else {
                        DownloadService.this.mHandler.postDelayed(this, 2000);
                    }
                } catch (ACException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public IBinder onBind(Intent intent) {
        return null;
    }
}
