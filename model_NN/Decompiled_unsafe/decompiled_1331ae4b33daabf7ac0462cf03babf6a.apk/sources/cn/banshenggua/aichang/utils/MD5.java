package cn.banshenggua.aichang.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class MD5 {
    static final String HEXES = "0123456789abcdef";

    public static String getHex(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b2 : bArr) {
            sb.append(HEXES.charAt((b2 & 240) >> 4)).append(HEXES.charAt(b2 & 15));
        }
        return sb.toString();
    }

    public static String md5sum(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        byte[] digest = messageDigest.digest();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if (Integer.toHexString(digest[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD).length() == 1) {
                stringBuffer.append("0").append(Integer.toHexString(digest[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
            } else {
                stringBuffer.append(Integer.toHexString(digest[i] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD));
            }
        }
        return stringBuffer.toString();
    }
}
