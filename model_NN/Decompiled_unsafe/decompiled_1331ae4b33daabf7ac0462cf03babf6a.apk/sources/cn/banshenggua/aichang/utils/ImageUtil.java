package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.View;
import cn.a.a.a;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import cn.banshenggua.aichang.widget.OvaledBitmapDisplayer;
import cn.banshenggua.aichang.widget.OvaledCornerBitmapDisplayer;
import com.d.a.a.a.a.b;
import com.d.a.a.a.a.d;
import com.d.a.a.a.b.a;
import com.d.a.a.b.a.c;
import com.d.a.b.a.j;
import com.d.a.b.c;
import com.d.a.b.e;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;

public class ImageUtil {
    private static final int CPU_NUMBERS = (SystemDevice.getNumCores() * 2);
    private static boolean DEBUG = true;
    private static final int DISC_IMAGE_MODIFIED = 604800;
    private static final int DISC_ONE_DAY_IMAGE_MODIFIED = 432000;
    private static final int IMAGE_MAX_SIZE = 2046;
    private static final int MAX_IMAGE_CACHE_COUNT = 10;
    private static final int MEMORY_LIMIT = 6291456;
    private static String TAG = "ImageUtil";
    public static final int USER_PHOTO_HEIGHT = 96;
    public static final int USER_PHOTO_WIDTH = 96;

    public enum ScalingLogic {
        CROP,
        FIT
    }

    public static Bitmap decodeResource(Resources resources, int i, int i2, int i3, ScalingLogic scalingLogic) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, i, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, i2, i3, scalingLogic);
        return BitmapFactory.decodeResource(resources, i, options);
    }

    public static Bitmap decodeFile(String str, int i, int i2, ScalingLogic scalingLogic) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(str, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, i, i2, scalingLogic);
        return BitmapFactory.decodeFile(str, options);
    }

    public static Bitmap decodeFile(byte[] bArr, int i, int i2, ScalingLogic scalingLogic) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, i, i2, scalingLogic);
        return BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
    }

    public static Bitmap createScaledBitmap(Bitmap bitmap, int i, int i2, ScalingLogic scalingLogic) {
        Rect calculateSrcRect = calculateSrcRect(bitmap.getWidth(), bitmap.getHeight(), i, i2, scalingLogic);
        Rect calculateDstRect = calculateDstRect(bitmap.getWidth(), bitmap.getHeight(), i, i2, scalingLogic);
        Bitmap createBitmap = Bitmap.createBitmap(calculateDstRect.width(), calculateDstRect.height(), Bitmap.Config.ARGB_8888);
        new Canvas(createBitmap).drawBitmap(bitmap, calculateSrcRect, calculateDstRect, new Paint(2));
        return createBitmap;
    }

    public static Bitmap loadFromFile(String str) {
        try {
            if (!new File(str).exists()) {
                return null;
            }
            return BitmapFactory.decodeFile(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static int calculateSampleSize(int i, int i2, int i3, int i4, ScalingLogic scalingLogic) {
        if (scalingLogic == ScalingLogic.FIT) {
            if (((float) i) / ((float) i2) > ((float) i3) / ((float) i4)) {
                return i / i3;
            }
            return i2 / i4;
        } else if (((float) i) / ((float) i2) > ((float) i3) / ((float) i4)) {
            return i2 / i4;
        } else {
            return i / i3;
        }
    }

    public static Rect calculateSrcRect(int i, int i2, int i3, int i4, ScalingLogic scalingLogic) {
        if (scalingLogic != ScalingLogic.CROP) {
            return new Rect(0, 0, i, i2);
        }
        float f = ((float) i3) / ((float) i4);
        if (((float) i) / ((float) i2) > f) {
            int i5 = (int) (((float) i2) * f);
            int i6 = (i - i5) / 2;
            return new Rect(i6, 0, i5 + i6, i2);
        }
        int i7 = (int) (((float) i) / f);
        int i8 = (i2 - i7) / 2;
        return new Rect(0, i8, i, i7 + i8);
    }

    public static Rect calculateDstRect(int i, int i2, int i3, int i4, ScalingLogic scalingLogic) {
        if (scalingLogic != ScalingLogic.FIT) {
            return new Rect(0, 0, i3, i4);
        }
        float f = ((float) i) / ((float) i2);
        if (f > ((float) i3) / ((float) i4)) {
            return new Rect(0, 0, i3, (int) (((float) i3) / f));
        }
        return new Rect(0, 0, (int) (f * ((float) i4)), i4);
    }

    public static Bitmap blurImage(Bitmap bitmap) {
        System.currentTimeMillis();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int[][] iArr = (int[][]) Array.newInstance(Integer.TYPE, 9, 3);
        int i4 = width - 1;
        for (int i5 = 1; i5 < i4; i5++) {
            int i6 = 1;
            int i7 = height - 1;
            while (i6 < i7) {
                int i8 = 0;
                while (true) {
                    int i9 = i8;
                    if (i9 >= 9) {
                        int i10 = i3;
                        int i11 = i2;
                        int i12 = i;
                        for (int i13 = 0; i13 < 9; i13++) {
                            i12 += iArr[i13][0];
                            i11 += iArr[i13][1];
                            i10 += iArr[i13][2];
                        }
                        createBitmap.setPixel(i5, i6, Color.argb(255, Math.min(255, Math.max(0, (int) (((float) i12) / 9.0f))), Math.min(255, Math.max(0, (int) (((float) i11) / 9.0f))), Math.min(255, Math.max(0, (int) (((float) i10) / 9.0f)))));
                        i = 0;
                        i2 = 0;
                        i3 = 0;
                        i6++;
                    } else {
                        int i14 = 0;
                        int i15 = 0;
                        switch (i9) {
                            case 0:
                                i14 = i5 - 1;
                                i15 = i6 - 1;
                                break;
                            case 1:
                                i15 = i6 - 1;
                                i14 = i5;
                                break;
                            case 2:
                                i14 = i5 + 1;
                                i15 = i6 - 1;
                                break;
                            case 3:
                                i14 = i5 + 1;
                                i15 = i6;
                                break;
                            case 4:
                                i14 = i5 + 1;
                                i15 = i6 + 1;
                                break;
                            case 5:
                                i15 = i6 + 1;
                                i14 = i5;
                                break;
                            case 6:
                                i14 = i5 - 1;
                                i15 = i6 + 1;
                                break;
                            case 7:
                                i14 = i5 - 1;
                                i15 = i6;
                                break;
                            case 8:
                                i15 = i6;
                                i14 = i5;
                                break;
                        }
                        int pixel = bitmap.getPixel(i14, i15);
                        iArr[i9][0] = Color.red(pixel);
                        iArr[i9][1] = Color.green(pixel);
                        iArr[i9][2] = Color.blue(pixel);
                        i8 = i9 + 1;
                    }
                }
            }
        }
        System.currentTimeMillis();
        return createBitmap;
    }

    public static Bitmap blurImageAmeliorate(Bitmap bitmap, int i) {
        System.currentTimeMillis();
        int[] iArr = {1, 2, 1, 2, 4, 2, 1, 2, 1};
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        int[] iArr2 = new int[(width * height)];
        bitmap.getPixels(iArr2, 0, width, 0, 0, width, height);
        int i2 = height - 1;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        for (int i6 = 1; i6 < i2; i6++) {
            int i7 = 1;
            int i8 = width - 1;
            while (true) {
                int i9 = i7;
                if (i9 >= i8) {
                    break;
                }
                int i10 = 0;
                int i11 = -1;
                while (true) {
                    int i12 = i11;
                    if (i12 > 1) {
                        break;
                    }
                    for (int i13 = -1; i13 <= 1; i13++) {
                        int i14 = iArr2[((i6 + i12) * width) + i9 + i13];
                        int red = Color.red(i14);
                        int green = Color.green(i14);
                        int blue = Color.blue(i14);
                        i5 += red * iArr[i10];
                        i4 += iArr[i10] * green;
                        i3 += blue * iArr[i10];
                        i10++;
                    }
                    i11 = i12 + 1;
                }
                iArr2[(i6 * width) + i9] = Color.argb(255, Math.min(255, Math.max(0, i5 / 16)), Math.min(255, Math.max(0, i4 / 16)), Math.min(255, Math.max(0, i3 / 16)));
                i5 = 0;
                i4 = 0;
                i3 = 0;
                i7 = i9 + 1;
            }
        }
        createBitmap.setPixels(iArr2, 0, width, 0, 0, width, height);
        System.currentTimeMillis();
        return createBitmap;
    }

    public static Bitmap cutReflectImage(Bitmap bitmap, int i) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        return Bitmap.createBitmap(bitmap, 0, ((height / i) / 2) + 0, width, height / i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void}
     arg types: [int, float, int, float, int, ?, android.graphics.Shader$TileMode]
     candidates:
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long, long, android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, long[], float[], android.graphics.Shader$TileMode):void}
      ClspMth{android.graphics.LinearGradient.<init>(float, float, float, float, int, int, android.graphics.Shader$TileMode):void} */
    public static Bitmap getReflectedImage(Bitmap bitmap, int i) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.preScale(1.0f, -1.0f);
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, height - (height / i), width, height / i, matrix, false);
        Bitmap createBitmap2 = Bitmap.createBitmap(width, (height / i) + height + 0, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap2);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        canvas.drawRect(0.0f, (float) height, (float) width, (float) (height + 0), new Paint());
        canvas.drawBitmap(createBitmap, 0.0f, (float) (height + 0), (Paint) null);
        Paint paint = new Paint();
        paint.setShader(new LinearGradient(0.0f, (float) bitmap.getHeight(), 0.0f, (float) createBitmap2.getHeight(), 1895825407, (int) ViewCompat.MEASURED_SIZE_MASK, Shader.TileMode.MIRROR));
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        canvas.drawRect(0.0f, (float) height, (float) width, (float) createBitmap2.getHeight(), paint);
        return createBitmap2;
    }

    public static BitmapDrawable toRoundCorner(BitmapDrawable bitmapDrawable, int i) {
        return new BitmapDrawable(getRoundedCornerBitmap(bitmapDrawable.getBitmap(), (float) i));
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float f) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        RectF rectF = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(-1);
        canvas.drawRoundRect(rectF, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return createBitmap;
    }

    public static Bitmap getOvaledCornerBitmap(Bitmap bitmap, int i, int i2, int i3) {
        boolean z;
        int density = (int) (((float) i) * UIUtil.getInstance().getDensity());
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            width = height;
        } else {
            height = width;
        }
        if (i2 <= 0 || width >= i2) {
            i2 = height;
            z = false;
        } else {
            z = true;
            width = i2;
        }
        Bitmap createBitmap = Bitmap.createBitmap(width + density, i2 + density, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, width, i2);
        Rect rect2 = new Rect(density, density, width, i2);
        RectF rectF = new RectF(rect2);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        canvas.drawOval(rectF, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        if (z) {
            canvas.drawBitmap(Bitmap.createScaledBitmap(bitmap, width, i2, true), rect, rect2, paint);
        } else {
            canvas.drawBitmap(bitmap, rect, rect2, paint);
        }
        return createBitmap;
    }

    public static Bitmap getOvaledCornerBitmap(Bitmap bitmap, int i, int i2) {
        return getOvaledCornerBitmap(bitmap, 5, i, i2);
    }

    public static Bitmap decodeFile(File file, int i) {
        int i2 = 1;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            int i3 = options.outWidth;
            int i4 = options.outHeight;
            while (i3 / 2 >= i) {
                i3 /= 2;
                i4 /= 2;
                i2 *= 2;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i2;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, options2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Bitmap decodeFile(File file) {
        int i = 1;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(file), null, options);
            int i2 = options.outWidth;
            int i3 = options.outHeight;
            while (i2 / 2 >= IMAGE_MAX_SIZE) {
                i2 /= 2;
                i3 /= 2;
                i *= 2;
            }
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            options2.inSampleSize = i;
            return BitmapFactory.decodeStream(new FileInputStream(file), null, options2);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] pngCompress(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 70, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static byte[] jpegCompress(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap cropAndScale(Bitmap bitmap) {
        int i;
        int i2;
        int i3 = 0;
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width > height) {
            i2 = (width - height) >> 1;
            i = height;
        } else if (width < height) {
            height = width;
            i = width;
            i2 = 0;
            i3 = (height - width) >> 1;
        } else {
            i = width;
            i2 = 0;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(96.0f / ((float) i), 96.0f / ((float) height));
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, i2, i3, i, height, matrix, true);
        bitmap.recycle();
        return createBitmap;
    }

    public static Bitmap createWaterBitmap(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int width2 = bitmap2.getWidth();
        int height2 = bitmap2.getHeight();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        Rect rect = new Rect();
        rect.left = 0;
        rect.right = width2;
        rect.top = 0;
        rect.bottom = height2;
        canvas.drawBitmap(bitmap2, (Rect) null, rect, paint);
        canvas.save(31);
        canvas.restore();
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap resizeImage(Bitmap bitmap, int i, int i2) {
        if (bitmap == null) {
            return null;
        }
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static final Bitmap scaleImg(Bitmap bitmap, int i) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int i2 = width;
        while (i2 > i) {
            i2--;
        }
        int i3 = height;
        while (i3 > i) {
            i3--;
        }
        Matrix matrix = new Matrix();
        matrix.postScale(((float) i2) / ((float) width), ((float) i3) / ((float) height));
        Bitmap createBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        if (!(bitmap == null || createBitmap == bitmap)) {
            bitmap.recycle();
        }
        return createBitmap;
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap.Config config;
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (drawable.getOpacity() != -1) {
            config = Bitmap.Config.ARGB_8888;
        } else {
            config = Bitmap.Config.RGB_565;
        }
        Bitmap createBitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, config);
        Canvas canvas = new Canvas(createBitmap);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(canvas);
        return createBitmap;
    }

    public static Bitmap convertViewToBitmap(View view) {
        Bitmap createBitmap = Bitmap.createBitmap(320, 320, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawColor((int) ViewCompat.MEASURED_STATE_MASK);
        view.setDrawingCacheEnabled(false);
        view.layout(0, 0, view.getLayoutParams().width, view.getLayoutParams().height);
        view.draw(canvas);
        return createBitmap;
    }

    public static boolean saveBitmap(Bitmap bitmap, String str) {
        if (bitmap == null || bitmap.isRecycled()) {
            return false;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
            if (bitmap.compress(Bitmap.CompressFormat.JPEG, 70, fileOutputStream)) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e2) {
            e2.printStackTrace();
            return false;
        } catch (IllegalStateException e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public static File getSavePath() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        File file = new File(CommonUtil.getSavePicPath());
        if (file.isDirectory() || file.mkdirs()) {
            return file;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044 A[SYNTHETIC, Splitter:B:12:0x0044] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004d A[SYNTHETIC, Splitter:B:18:0x004d] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getResizedImageData(android.content.Context r6, android.net.Uri r7, int r8, int r9) {
        /*
            r1 = 0
            android.graphics.BitmapFactory$Options r0 = decodeBitmapOptionsInfo(r6, r7)
            r2 = 0
            r0.inJustDecodeBounds = r2
            int r2 = r0.outWidth
            int r3 = r0.outHeight
            cn.banshenggua.aichang.utils.ImageUtil$ScalingLogic r4 = cn.banshenggua.aichang.utils.ImageUtil.ScalingLogic.CROP
            int r2 = calculateSampleSize(r2, r3, r8, r9, r4)
            r0.inSampleSize = r2
            android.content.ContentResolver r2 = r6.getContentResolver()     // Catch:{ Exception -> 0x0040, all -> 0x0049 }
            java.io.InputStream r2 = r2.openInputStream(r7)     // Catch:{ Exception -> 0x0040, all -> 0x0049 }
            r3 = 0
            android.graphics.Bitmap r3 = android.graphics.BitmapFactory.decodeStream(r2, r3, r0)     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            cn.banshenggua.aichang.utils.UIUtil r0 = cn.banshenggua.aichang.utils.UIUtil.getInstance()     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            int r0 = r0.getmScreenWidth()     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            cn.banshenggua.aichang.utils.UIUtil r4 = cn.banshenggua.aichang.utils.UIUtil.getInstance()     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            int r4 = r4.getmScreenWidth()     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            cn.banshenggua.aichang.utils.ImageUtil$ScalingLogic r5 = cn.banshenggua.aichang.utils.ImageUtil.ScalingLogic.CROP     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            android.graphics.Bitmap r0 = createScaledBitmap(r3, r0, r4, r5)     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            r3.recycle()     // Catch:{ Exception -> 0x0059, all -> 0x0057 }
            if (r2 == 0) goto L_0x003f
            r2.close()     // Catch:{ IOException -> 0x0051 }
        L_0x003f:
            return r0
        L_0x0040:
            r0 = move-exception
            r0 = r1
        L_0x0042:
            if (r0 == 0) goto L_0x0047
            r0.close()     // Catch:{ IOException -> 0x0053 }
        L_0x0047:
            r0 = r1
            goto L_0x003f
        L_0x0049:
            r0 = move-exception
            r2 = r1
        L_0x004b:
            if (r2 == 0) goto L_0x0050
            r2.close()     // Catch:{ IOException -> 0x0055 }
        L_0x0050:
            throw r0
        L_0x0051:
            r1 = move-exception
            goto L_0x003f
        L_0x0053:
            r0 = move-exception
            goto L_0x0047
        L_0x0055:
            r1 = move-exception
            goto L_0x0050
        L_0x0057:
            r0 = move-exception
            goto L_0x004b
        L_0x0059:
            r0 = move-exception
            r0 = r2
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.utils.ImageUtil.getResizedImageData(android.content.Context, android.net.Uri, int, int):android.graphics.Bitmap");
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0061 A[SYNTHETIC, Splitter:B:19:0x0061] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] getResizedImageData(android.content.Context r7, android.net.Uri r8, int r9, int r10, int r11) {
        /*
            r0 = 0
            android.graphics.BitmapFactory$Options r2 = decodeBitmapOptionsInfo(r7, r8)
            r1 = 0
            r2.inJustDecodeBounds = r1
            int r1 = r2.outWidth
            int r3 = r2.outHeight
            cn.banshenggua.aichang.utils.ImageUtil$ScalingLogic r4 = cn.banshenggua.aichang.utils.ImageUtil.ScalingLogic.CROP
            int r1 = calculateSampleSize(r1, r3, r10, r11, r4)
            r2.inSampleSize = r1
            android.content.ContentResolver r1 = r7.getContentResolver()     // Catch:{ Exception -> 0x0051, all -> 0x005b }
            java.io.InputStream r1 = r1.openInputStream(r8)     // Catch:{ Exception -> 0x0051, all -> 0x005b }
            r3 = 0
            android.graphics.Bitmap r2 = android.graphics.BitmapFactory.decodeStream(r1, r3, r2)     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            cn.banshenggua.aichang.utils.UIUtil r3 = cn.banshenggua.aichang.utils.UIUtil.getInstance()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            int r3 = r3.getmScreenWidth()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            cn.banshenggua.aichang.utils.UIUtil r4 = cn.banshenggua.aichang.utils.UIUtil.getInstance()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            int r4 = r4.getmScreenWidth()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            cn.banshenggua.aichang.utils.ImageUtil$ScalingLogic r5 = cn.banshenggua.aichang.utils.ImageUtil.ScalingLogic.CROP     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            android.graphics.Bitmap r3 = createScaledBitmap(r2, r3, r4, r5)     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            r2.recycle()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            r2.<init>()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            android.graphics.Bitmap$CompressFormat r4 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            r3.compress(r4, r9, r2)     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            r3.recycle()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            byte[] r0 = r2.toByteArray()     // Catch:{ Exception -> 0x006b, all -> 0x0069 }
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0050:
            return r0
        L_0x0051:
            r1 = move-exception
            r1 = r0
        L_0x0053:
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x0050
        L_0x0059:
            r1 = move-exception
            goto L_0x0050
        L_0x005b:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x005f:
            if (r1 == 0) goto L_0x0064
            r1.close()     // Catch:{ IOException -> 0x0067 }
        L_0x0064:
            throw r0
        L_0x0065:
            r1 = move-exception
            goto L_0x0050
        L_0x0067:
            r1 = move-exception
            goto L_0x0064
        L_0x0069:
            r0 = move-exception
            goto L_0x005f
        L_0x006b:
            r2 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.utils.ImageUtil.getResizedImageData(android.content.Context, android.net.Uri, int, int, int):byte[]");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0023 A[SYNTHETIC, Splitter:B:12:0x0023] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002c A[SYNTHETIC, Splitter:B:18:0x002c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.BitmapFactory.Options decodeBitmapOptionsInfo(android.content.Context r4, android.net.Uri r5) {
        /*
            r1 = 0
            android.graphics.BitmapFactory$Options r0 = new android.graphics.BitmapFactory$Options
            r0.<init>()
            android.content.ContentResolver r2 = r4.getContentResolver()     // Catch:{ FileNotFoundException -> 0x001f, all -> 0x0028 }
            java.io.InputStream r2 = r2.openInputStream(r5)     // Catch:{ FileNotFoundException -> 0x001f, all -> 0x0028 }
            r3 = 1
            r0.inJustDecodeBounds = r3     // Catch:{ FileNotFoundException -> 0x0038, all -> 0x0036 }
            android.graphics.Bitmap$Config r3 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ FileNotFoundException -> 0x0038, all -> 0x0036 }
            r0.inPreferredConfig = r3     // Catch:{ FileNotFoundException -> 0x0038, all -> 0x0036 }
            r3 = 0
            android.graphics.BitmapFactory.decodeStream(r2, r3, r0)     // Catch:{ FileNotFoundException -> 0x0038, all -> 0x0036 }
            if (r2 == 0) goto L_0x001e
            r2.close()     // Catch:{ IOException -> 0x0030 }
        L_0x001e:
            return r0
        L_0x001f:
            r0 = move-exception
            r0 = r1
        L_0x0021:
            if (r0 == 0) goto L_0x0026
            r0.close()     // Catch:{ IOException -> 0x0032 }
        L_0x0026:
            r0 = r1
            goto L_0x001e
        L_0x0028:
            r0 = move-exception
            r2 = r1
        L_0x002a:
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x0034 }
        L_0x002f:
            throw r0
        L_0x0030:
            r1 = move-exception
            goto L_0x001e
        L_0x0032:
            r0 = move-exception
            goto L_0x0026
        L_0x0034:
            r1 = move-exception
            goto L_0x002f
        L_0x0036:
            r0 = move-exception
            goto L_0x002a
        L_0x0038:
            r0 = move-exception
            r0 = r2
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.utils.ImageUtil.decodeBitmapOptionsInfo(android.content.Context, android.net.Uri):android.graphics.BitmapFactory$Options");
    }

    public static Bitmap resizeBitmap(Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() < i && bitmap.getHeight() < i2) {
            return bitmap;
        }
        if (bitmap.getWidth() > bitmap.getHeight()) {
            i2 = (bitmap.getHeight() * i) / bitmap.getWidth();
        } else {
            i = (bitmap.getWidth() * i2) / bitmap.getHeight();
        }
        return Bitmap.createScaledBitmap(bitmap, i, i2, false);
    }

    public static e get200DipConfig() {
        e.a aVar;
        try {
            aVar = new e.a(KShareApplication.getInstance().getApp()).a((int) (UIUtil.getInstance().getDensity() * 200.0f), (int) (UIUtil.getInstance().getDensity() * 200.0f)).a((int) (UIUtil.getInstance().getDensity() * 200.0f), (int) (UIUtil.getInstance().getDensity() * 200.0f), Bitmap.CompressFormat.JPEG, 75, null).a(CPU_NUMBERS).b(4).a().a(new c()).a(new d(new File(CommonUtil.getImageCacheDir()), new ImageSaveNameGenerator())).a(com.d.a.b.c.t());
        } catch (ACException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar == null) {
            return null;
        }
        if (ULog.debug) {
            aVar.b();
        }
        return aVar.c();
    }

    public static class ImageSaveNameGenerator implements a {
        public String generate(String str) {
            return getFileName(str);
        }

        public static String getFileName(String str) {
            int hashCode = str.hashCode();
            return String.valueOf(String.valueOf(Math.abs(hashCode) % 100)) + File.separator + String.valueOf(Math.abs(hashCode / 100) % 100) + File.separator + String.valueOf(hashCode);
        }
    }

    public static e getScreenDipConfig() {
        e.a aVar;
        try {
            aVar = new e.a(KShareApplication.getInstance().getApp()).a(CPU_NUMBERS).b(4).a().a(new c()).a(UIUtil.getInstance().getmScreenWidth(), UIUtil.getInstance().getmScreenHeight()).a(new d(new File(CommonUtil.getImageCacheDir()), new ImageSaveNameGenerator())).a(com.d.a.b.c.t());
        } catch (ACException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar == null) {
            return null;
        }
        if (ULog.debug) {
            aVar.b();
        }
        return aVar.c();
    }

    public static e getOneDayLimitConfig() {
        e.a aVar;
        try {
            aVar = new e.a(KShareApplication.getInstance().getApp()).a(CPU_NUMBERS).b(4).a().a(new c()).a(UIUtil.getInstance().getmScreenWidth(), UIUtil.getInstance().getmScreenHeight()).a(new b(new File(CommonUtil.getImageCacheDir()), new ImageSaveNameGenerator(), 432000)).a(com.d.a.b.c.t());
        } catch (ACException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar == null) {
            return null;
        }
        if (ULog.debug) {
            aVar.b();
        }
        return aVar.c();
    }

    public static boolean removeImageCacheFromMemoryDisk(String str) {
        j.a(str, com.d.a.b.d.a().c());
        com.d.a.b.a.b.a(str, com.d.a.b.d.a().d());
        return false;
    }

    public static e get106DipConfig() {
        e.a aVar;
        try {
            aVar = new e.a(KShareApplication.getInstance().getApp()).a(CPU_NUMBERS).b(4).a().a(new c()).a(new d(new File(CommonUtil.getImageCacheDir()), new ImageSaveNameGenerator())).a(com.d.a.b.c.t());
        } catch (ACException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar == null) {
            return null;
        }
        if (ULog.debug) {
            aVar.b();
        }
        return aVar.c();
    }

    public static e getDefaultDipConfig() {
        e.a aVar;
        try {
            aVar = new e.a(KShareApplication.getInstance().getApp()).a(CPU_NUMBERS).b(4).a().a(new c()).a(new d(new File(CommonUtil.getImageCacheDir()), new ImageSaveNameGenerator())).a(com.d.a.b.c.t());
        } catch (ACException e) {
            e.printStackTrace();
            aVar = null;
        }
        if (aVar == null) {
            return null;
        }
        if (ULog.debug) {
            aVar.b();
        }
        return aVar.c();
    }

    public static com.d.a.b.c getOvaledCornerDefaultOption(int i) {
        return new c.a().a(a.e.default_ovaled_corner).c(a.e.default_ovaled_corner).a().b().a(new OvaledCornerBitmapDisplayer(i)).c();
    }

    public static com.d.a.b.c getOvaledCornerDefaultOption() {
        return new c.a().c(a.e.default_ovaled_corner).a().b().a(new OvaledCornerBitmapDisplayer()).c();
    }

    public static com.d.a.b.c getOvalDefaultOption() {
        return new c.a().a(a.e.default_ovaled).c(a.e.default_ovaled).a().b().a(new OvaledBitmapDisplayer()).c();
    }

    public static com.d.a.b.c getRoundDefaultOption(int i) {
        return new c.a().a(a.e.default_my).c(a.e.default_my).a().b().a(new com.d.a.b.c.c(i)).c();
    }

    public static com.d.a.b.c getDefaultScaleAnimationOption() {
        return new c.a().a(a.e.default_my).c(a.e.default_my).a().b().c();
    }

    public static com.d.a.b.c getDefaultPlayerImageAminOption(int i) {
        return new c.a().a().b().a(new PlayerImageAminDisplayer(i)).c();
    }

    public static com.d.a.b.c getNoDefaultOption() {
        return new c.a().a().b().c();
    }

    public static com.d.a.b.c getNoDefaultPlayerImageAminOption(int i) {
        return new c.a().b().a().a(new PlayerImageAminDisplayer(i)).c();
    }

    public static com.d.a.b.c getDefaultOptionForGift() {
        return new c.a().a(a.e.gift_default).c(a.e.gift_default).a().b().c();
    }

    public static com.d.a.b.c getDefaultOptionForBanner() {
        return new c.a().a(a.e.banner_default).c(a.e.banner_default).a().b().c();
    }

    public static com.d.a.b.c getDefaultOption() {
        return new c.a().a(a.e.default_my).c(a.e.default_my).a().b().c();
    }

    public static com.d.a.b.c getDefaultLevelOption() {
        return new c.a().a().b().c();
    }

    public static void cropImage(Object obj, int i, Uri uri, Uri uri2) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 600);
        intent.putExtra("outputY", 600);
        intent.putExtra("output", uri2);
        try {
            if (obj instanceof Activity) {
                ((Activity) obj).startActivityForResult(intent, i);
            } else if (obj instanceof Fragment) {
                ((Fragment) obj).startActivityForResult(intent, i);
            }
        } catch (Exception e) {
            if (obj instanceof Activity) {
                ToastUtils.show((Activity) obj, "系统图片程序异常");
            } else if (obj instanceof Fragment) {
                ToastUtils.show(((Fragment) obj).getActivity(), "系统图片程序异常");
            }
        }
    }

    public static final int getBitmapByteCount(Bitmap bitmap) {
        if (bitmap != null) {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
        return 0;
    }
}
