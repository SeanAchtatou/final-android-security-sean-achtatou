package cn.banshenggua.aichang.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.text.TextUtils;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import java.io.File;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

public class CommonUtil {
    public static final long CACHE_SIZE = 10;
    public static final int MB = 1048576;
    public static final int SD_CARD_AVAILABLE = 1;
    public static final int SD_CARD_NOT_AVAILABLE = 0;
    public static final int SD_CARD_SPACE_NOT_ENOUGH = 2;
    static final float scale = Resources.getSystem().getDisplayMetrics().density;

    public static boolean isAppInstalled(String str) {
        PackageInfo packageInfo;
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            packageInfo = KShareApplication.getInstance().getApp().getPackageManager().getPackageInfo(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
        } catch (ACException e2) {
            e2.printStackTrace();
            packageInfo = null;
        }
        if (packageInfo != null) {
            return true;
        }
        return false;
    }

    public static boolean stringIsNotNull(String str) {
        return str != null && str.trim().length() > 0;
    }

    public static boolean stringIsNull(String str) {
        return str == null || str.trim().length() <= 0;
    }

    public static String decodeUnicode(String str) {
        int length = str.length();
        StringBuffer stringBuffer = new StringBuffer(length);
        int i = 0;
        while (i < length) {
            int i2 = i + 1;
            char charAt = str.charAt(i);
            if (charAt == '\\') {
                int i3 = i2 + 1;
                char charAt2 = str.charAt(i2);
                if (charAt2 == 'u') {
                    int i4 = 0;
                    i = i3;
                    int i5 = 0;
                    while (i5 < 4) {
                        int i6 = i + 1;
                        char charAt3 = str.charAt(i);
                        switch (charAt3) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                i4 = ((i4 << 4) + charAt3) - 48;
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                i4 = (((i4 << 4) + 10) + charAt3) - 65;
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                i4 = (((i4 << 4) + 10) + charAt3) - 97;
                                break;
                            default:
                                throw new IllegalArgumentException("Malformed   \\uxxxx   encoding.");
                        }
                        i5++;
                        i = i6;
                    }
                    stringBuffer.append((char) i4);
                } else {
                    if (charAt2 == 't') {
                        charAt2 = 9;
                    } else if (charAt2 == 'r') {
                        charAt2 = 13;
                    } else if (charAt2 == 'n') {
                        charAt2 = 10;
                    } else if (charAt2 == 'f') {
                        charAt2 = 12;
                    }
                    stringBuffer.append(charAt2);
                    i = i3;
                }
            } else {
                stringBuffer.append(charAt);
                i = i2;
            }
        }
        return stringBuffer.toString();
    }

    public static boolean checkUsername(String str) {
        if (str.trim().length() < 4 || str.trim().length() >= 16) {
            return false;
        }
        return true;
    }

    public static boolean stringIsChinese(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            if ((str.substring(i).getBytes()[0] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) > 128) {
                return true;
            }
        }
        return false;
    }

    public static int getSDCardStatus() {
        if (!Environment.getExternalStorageState().equals("mounted")) {
            return 0;
        }
        try {
            StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
            if ((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1048576 >= 10) {
                return 1;
            }
            return 2;
        } catch (Exception e) {
            return 1;
        }
    }

    public static int dipToPixel(int i) {
        return (int) ((((float) i) * scale) + 0.5f);
    }

    public static int getTextLength(String str) {
        UnsupportedEncodingException e;
        int i;
        try {
            int length = new String(str.getBytes("GBK"), "ISO8859-1").length();
            i = length / 2;
            try {
                if (length % 2 == 1) {
                    return i + 1;
                }
                return i;
            } catch (UnsupportedEncodingException e2) {
                e = e2;
                e.printStackTrace();
                return i;
            }
        } catch (UnsupportedEncodingException e3) {
            UnsupportedEncodingException unsupportedEncodingException = e3;
            i = 0;
            e = unsupportedEncodingException;
            e.printStackTrace();
            return i;
        }
    }

    public static void removeImageCache(String str) {
        File file = new File(String.valueOf(getKshareRootPath()) + "/cache", MD5.md5sum(str));
        if (file.exists()) {
            file.delete();
        }
    }

    public static String getKshareRootPath() {
        String str = "/sdcard/kshare";
        if (checkSdCard()) {
            str = String.valueOf(Environment.getExternalStorageDirectory().getPath()) + "/kshare";
        } else {
            try {
                str = String.valueOf(KShareApplication.getInstance().getApp().getCacheDir().getPath()) + "/kshare";
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
        ULog.d("path", "path=" + str);
        return str;
    }

    public static boolean checkSdCard() {
        if ("mounted".equals(Environment.getExternalStorageState())) {
            return true;
        }
        return false;
    }

    public static String getCacheDir() {
        return String.valueOf(getKshareRootPath()) + "/cache/";
    }

    public static String getSongLyricPath() {
        return String.valueOf(getKshareRootPath()) + "/music/lyric/";
    }

    public static String getSavePicPath() {
        return String.valueOf(getKshareRootPath()) + "/picture/";
    }

    public static String getImageCacheDir() {
        return String.valueOf(getKshareRootPath()) + "/cache/";
    }

    public static String getRecordSongDir() {
        return String.valueOf(getKshareRootPath()) + "/records/";
    }

    public static String getSongsDir() {
        return String.valueOf(getKshareRootPath()) + "/songs/";
    }

    public static String getFanchangsDir() {
        return String.valueOf(getKshareRootPath()) + "/fanchangs/";
    }

    public static String getMessageDir() {
        return String.valueOf(getKshareRootPath()) + "/msgobj/";
    }

    public static String getLocalSongDir() {
        return String.valueOf(getKshareRootPath()) + "/localsong/";
    }

    public static String getDownloadDir() {
        return String.valueOf(getKshareRootPath()) + "/download/";
    }

    public static void killProcess(Context context) {
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    public static boolean initKsharePath() {
        File file = new File(getRecordSongDir());
        if (!file.isDirectory()) {
            if (file.isFile()) {
                file.delete();
                file.mkdirs();
            } else {
                file.mkdirs();
            }
        }
        File file2 = new File(getCacheDir());
        if (!file2.isDirectory()) {
            if (file2.isFile()) {
                file2.delete();
                file2.mkdirs();
            } else {
                file2.mkdirs();
            }
        }
        File file3 = new File(getSongLyricPath());
        if (!file3.isDirectory()) {
            if (file3.isFile()) {
                file3.delete();
                file3.mkdirs();
            } else {
                file3.mkdirs();
            }
        }
        File file4 = new File(getSavePicPath());
        if (!file4.isDirectory()) {
            if (file4.isFile()) {
                file4.delete();
                file4.mkdirs();
            } else {
                file4.mkdirs();
            }
        }
        File file5 = new File(getImageCacheDir());
        if (!file5.isDirectory()) {
            if (file5.isFile()) {
                file5.delete();
                file5.mkdirs();
            } else {
                file5.mkdirs();
            }
        }
        File file6 = new File(getSongsDir());
        if (!file6.isDirectory()) {
            if (file6.isFile()) {
                file6.delete();
                file6.mkdirs();
            } else {
                file6.mkdirs();
            }
        }
        File file7 = new File(getMessageDir());
        if (!file7.isDirectory()) {
            if (file7.isFile()) {
                file7.delete();
                file7.mkdirs();
            } else {
                file7.mkdirs();
            }
        }
        File file8 = new File(getLocalSongDir());
        if (!file8.isDirectory()) {
            if (file8.isFile()) {
                file8.delete();
                file8.mkdirs();
            } else {
                file8.mkdirs();
            }
        }
        File file9 = new File(getDownloadDir());
        if (file9.isDirectory()) {
            return false;
        }
        if (file9.isFile()) {
            file9.delete();
            file9.mkdirs();
            return false;
        }
        file9.mkdirs();
        return false;
    }

    public static byte[] readFromFile(String str, int i, int i2) {
        byte[] bArr = null;
        if (str == null) {
            return null;
        }
        File file = new File(str);
        if (!file.exists()) {
            ULog.i("CommonUtil", "readFromFile: file not found");
            return null;
        }
        if (i2 == -1) {
            i2 = (int) file.length();
        }
        ULog.d("CommonUtil", "readFromFile : offset = " + i + " len = " + i2 + " offset + len = " + (i + i2));
        if (i < 0) {
            ULog.e("CommonUtil", "readFromFile invalid offset:" + i);
            return null;
        } else if (i2 <= 0) {
            ULog.e("CommonUtil", "readFromFile invalid len:" + i2);
            return null;
        } else if (i + i2 > ((int) file.length())) {
            ULog.e("CommonUtil", "readFromFile invalid file len:" + file.length());
            return null;
        } else {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(str, "r");
                bArr = new byte[i2];
                randomAccessFile.seek((long) i);
                randomAccessFile.readFully(bArr);
                randomAccessFile.close();
                return bArr;
            } catch (Exception e) {
                ULog.e("CommonUtil", "readFromFile : errMsg = " + e.getMessage());
                e.printStackTrace();
                return bArr;
            }
        }
    }
}
