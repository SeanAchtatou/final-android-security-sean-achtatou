package cn.banshenggua.aichang.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class StringUtil {
    public static final String Encoding = "utf-8";

    public static String optStringFromJson(JSONObject jSONObject, String str, String str2) {
        String str3;
        if (jSONObject == null) {
            return str2;
        }
        String str4 = "";
        try {
            str4 = jSONObject.optString(str, str2);
            return URLDecoder.decode(str4, Encoding);
        } catch (UnsupportedEncodingException e) {
            UnsupportedEncodingException unsupportedEncodingException = e;
            str3 = str4;
            unsupportedEncodingException.printStackTrace();
        } catch (Exception e2) {
            Exception exc = e2;
            str3 = str4;
            exc.printStackTrace();
        }
        return str3;
    }

    public static int getChineseCount(String str) {
        Matcher matcher = Pattern.compile("[\\u4e00-\\u9fa5]").matcher(str);
        int i = 0;
        while (matcher.find()) {
            int i2 = 0;
            while (i2 <= matcher.groupCount()) {
                i2++;
                i++;
            }
        }
        return i;
    }
}
