package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.text.TextUtils;

public class ToastUtils {
    public static void show(Activity activity, String str) {
        Toaster.showLong(activity, str);
    }

    public static void show(Activity activity, int i) {
        if (activity != null) {
            show(activity, activity.getString(i));
        }
    }

    public static void show(Activity activity, Exception exc, int i) {
        if (activity != null) {
            String str = exc.getMessage().toString();
            if (TextUtils.isEmpty(str)) {
                str = activity.getString(i);
            }
            show(activity, str);
        }
    }
}
