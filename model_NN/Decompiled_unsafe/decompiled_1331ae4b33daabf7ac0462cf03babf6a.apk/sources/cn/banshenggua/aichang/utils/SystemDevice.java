package cn.banshenggua.aichang.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.format.Formatter;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

public class SystemDevice {
    private static final String INST_SRC_SUFFIX = "_kshare.apk";
    private static SystemDevice instance = null;
    public String BOOTLOADER = Build.BOOTLOADER;
    public String BRAND = Build.BRAND;
    public String CPU_ABI = Build.CPU_ABI;
    public String CPU_ABI2 = Build.CPU_ABI2;
    public String DEVICE_ID;
    public String FINGERPRINT = Build.FINGERPRINT;
    public String HARDWARE = Build.HARDWARE;
    public String ID = Build.ID;
    public String INSTALL_SOURCE;
    public String MANUFACTURER = Build.MANUFACTURER;
    public String MODEL = Build.MODEL;
    public String PRODUCT = Build.PRODUCT;
    public String RADIO = Build.RADIO;
    public String SDK_RELEASE = Build.VERSION.RELEASE;
    public String SDK_VERSION = Build.VERSION.SDK;
    public String SERIAL;
    public String VERSION_CODE;
    public String VERSION_NAME;
    public boolean isMeizu3 = false;
    public boolean isVivoX5 = false;

    public enum MachineLever {
        Low,
        Mid,
        High
    }

    public static SystemDevice getInstance() {
        if (instance == null) {
            try {
                instance = new SystemDevice(KShareApplication.getInstance().getApp());
            } catch (ACException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    public String getSystemMachine() {
        return String.valueOf(this.MODEL) + "-" + this.DEVICE_ID + "-" + this.SDK_VERSION + "-" + this.SDK_RELEASE;
    }

    public String getSystemModel() {
        return this.MODEL;
    }

    public String getVivoWeiboTail() {
        if (!this.isVivoX5) {
            return "";
        }
        if (this.MODEL.indexOf("vivo X5Max") >= 0) {
            return " (通过#纤薄王者X5Max#录制)";
        }
        return " (通过#K歌之王X5#录制)";
    }

    private SystemDevice(Context context) {
        if (!TextUtils.isEmpty(this.BRAND) && this.BRAND.equalsIgnoreCase("meizu")) {
            this.isMeizu3 = false;
        }
        if (!TextUtils.isEmpty(this.BRAND) && this.BRAND.equalsIgnoreCase("vivo") && !TextUtils.isEmpty(this.MODEL) && this.MODEL.indexOf("vivo X5") >= 0) {
            this.isVivoX5 = true;
        }
        try {
            this.VERSION_CODE = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            this.VERSION_NAME = "歌神_" + this.VERSION_CODE;
            this.INSTALL_SOURCE = String.valueOf(this.VERSION_NAME) + INST_SRC_SUFFIX;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        if (TextUtils.isEmpty(deviceId)) {
            deviceId = readMACID(context);
            if (TextUtils.isEmpty(deviceId)) {
                deviceId = readDeviceId();
            }
        }
        this.DEVICE_ID = deviceId;
    }

    private String readMACID(Context context) {
        String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        if (TextUtils.isEmpty(macAddress)) {
            return null;
        }
        return MD5.md5sum(macAddress);
    }

    private String readDeviceId() {
        return MD5.md5sum(String.valueOf(this.BRAND) + this.MANUFACTURER + this.HARDWARE + this.CPU_ABI + this.CPU_ABI2 + this.PRODUCT + this.SDK_VERSION + this.RADIO + this.ID);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{\"DEVICE_ID\":\"").append(this.DEVICE_ID).append("\",\"").append("BRAND\":\"").append(this.BRAND).append("\",\"").append("MANUFACTURER\":\"").append(this.MANUFACTURER).append("\",\"").append("HARDWARE\":\"").append(this.HARDWARE).append("\",\"").append("CPU_ABI\":\"").append(this.CPU_ABI).append("\",\"").append("CPU_ABI2\":\"").append(this.CPU_ABI2).append("\",\"").append("PRODUCT\":\"").append(this.PRODUCT).append("\",\"").append("ID\":\"").append(this.ID).append("\",\"").append("FINGERPRINT\":\"").append(this.FINGERPRINT).append("\",\"").append("RADIO\":\"").append(this.RADIO).append("\",\"").append("BOOTLOADER\":\"").append(this.BOOTLOADER).append("\",\"").append("SDK_VERSION\":\"").append(this.SDK_VERSION).append("\",\"").append("VERSION_CODE\":\"").append(this.VERSION_CODE).append("\",\"").append("VERSION_NAME\":\"").append(this.VERSION_NAME).append("\",\"").append("BOOTLOADER\":\"").append(this.BOOTLOADER).append("\",\"").append("INSTALL_SOURCE\":\"").append(this.INSTALL_SOURCE).append("\",\"MODEL\":\"").append(this.MODEL).append("\"}");
        return sb.toString();
    }

    public static int getNumCores() {
        try {
            File[] listFiles = new File("/sys/devices/system/cpu/").listFiles(new FileFilter() {
                public boolean accept(File file) {
                    if (Pattern.matches("cpu[0-9]", file.getName())) {
                        return true;
                    }
                    return false;
                }
            });
            ULog.d("cpuinfo", "cores: " + listFiles.length);
            return listFiles.length;
        } catch (Exception e) {
            e.printStackTrace();
            ULog.d("cpuinfo", "cores: 1");
            return 1;
        }
    }

    private static String getAvailMemory(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService(PushConstants.INTENT_ACTIVITY_NAME)).getMemoryInfo(memoryInfo);
        return Formatter.formatFileSize(context, memoryInfo.availMem);
    }
}
