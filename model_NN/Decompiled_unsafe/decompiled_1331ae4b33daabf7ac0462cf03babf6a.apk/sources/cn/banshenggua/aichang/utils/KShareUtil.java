package cn.banshenggua.aichang.utils;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import cn.a.a.a;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.app.DownloadService;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.room.message.SocketMessage;
import cn.banshenggua.aichang.sdk.ACException;
import com.pocketmusic.kshare.a.a;
import com.pocketmusic.kshare.dialog.MyDialogFragment;
import com.pocketmusic.kshare.requestobjs.i;
import com.pocketmusic.kshare.requestobjs.k;
import com.pocketmusic.kshare.requestobjs.n;
import com.pocketmusic.kshare.requestobjs.s;
import eu.inmite.android.lib.dialogs.b;
import eu.inmite.android.lib.dialogs.c;
import java.io.File;
import java.util.HashMap;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class KShareUtil {
    public static final String AC_APK = (String.valueOf(CommonUtil.getDownloadDir()) + "aichang.apk");
    public static final String AC_MAIN = "com.pocketmusic.kshare.WelcomeActivity";
    public static final String AC_PACK = "cn.banshenggua.aichang";
    private static final String anzhi = "cn.goapk.market";
    private static final String baidu = "com.baidu.appsearch";
    private static final String gdt = "com.tencent.android.qqdownloader";
    private static final String huawei = "com.huawei.appmarket";
    private static final String jinli = "com.gionee.aora.market";
    private static final String lenovo = "com.lenovo.leos.appstore";
    private static final String letv = "com.letv.app.appstore";
    public static k.b mCurrentNotifyKey = k.b.DEFAULT;
    private static final String meizu = "com.meizu.mstore";
    private static final String oppo = "com.oppo.market";
    private static final String pp = "com.pp.assistant";
    private static final String qh360 = "com.qihoo.appstore";
    private static final String vivo = "com.bbk.appstore";
    private static final String wdj = "com.wandoujia.phoenix2";
    private static final String xiaomi = "com.xiaomi.market";

    public static boolean marketDownAndInstall(Context context, String str) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("market://details?id=" + str));
            HashMap hashMap = new HashMap();
            if (CommonUtil.isAppInstalled(oppo)) {
                hashMap.put("market", "oppo");
                intent.setPackage(oppo);
                z = true;
            } else if (CommonUtil.isAppInstalled(vivo)) {
                hashMap.put("market", "vivo");
                intent.setPackage(vivo);
                z = true;
            } else if (CommonUtil.isAppInstalled(huawei)) {
                hashMap.put("market", "huawei");
                intent.setPackage(huawei);
                z = true;
            } else if (CommonUtil.isAppInstalled(xiaomi)) {
                hashMap.put("market", "xiaomi");
                intent.setPackage(xiaomi);
                z = true;
            } else if (CommonUtil.isAppInstalled(lenovo)) {
                hashMap.put("market", "lenovo");
                intent.setPackage(lenovo);
                z = true;
            } else if (CommonUtil.isAppInstalled(letv)) {
                hashMap.put("market", "letv");
                intent.setPackage(letv);
                z = true;
            } else if (CommonUtil.isAppInstalled(jinli)) {
                hashMap.put("market", "jinli");
                intent.setPackage(jinli);
                z = true;
            } else if (CommonUtil.isAppInstalled(meizu)) {
                hashMap.put("market", "meizu");
                intent.setPackage(meizu);
                z = true;
            } else if (CommonUtil.isAppInstalled(gdt)) {
                hashMap.put("market", "gdt");
                intent.setPackage(gdt);
                z = true;
            } else {
                hashMap.put("market", "others");
                new HashMap().put("brand", Build.BRAND);
                HashMap hashMap2 = new HashMap();
                if (CommonUtil.isAppInstalled(baidu)) {
                    hashMap2.put("market", "baidu");
                    intent.setPackage(baidu);
                    z = true;
                } else if (CommonUtil.isAppInstalled(qh360)) {
                    hashMap2.put("market", "qh360");
                    intent.setPackage(qh360);
                    z = true;
                } else if (CommonUtil.isAppInstalled(wdj)) {
                    hashMap2.put("market", "wdj");
                    intent.setPackage(wdj);
                    z = true;
                } else if (CommonUtil.isAppInstalled(pp)) {
                    hashMap2.put("market", "pp");
                    intent.setPackage(pp);
                    z = true;
                } else if (CommonUtil.isAppInstalled(anzhi)) {
                    hashMap2.put("market", "anzhi");
                    intent.setPackage(anzhi);
                    z = true;
                } else {
                    hashMap2.put("market", "other");
                    z = false;
                }
            }
            ULog.d("luolei", "log: " + hashMap);
            if (!z) {
                return false;
            }
            context.startActivity(intent);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void showToast(Context context, int i) {
        if (context != null) {
            Toast.makeText(context, i, 0).show();
        } else {
            ULog.d(SocketMessage.MSG_ERROR_KEY, "Toast IS ERROR");
        }
    }

    public static void showToast(Context context, String str) {
        if (TextUtils.isEmpty(str) || context == null) {
            ULog.d(SocketMessage.MSG_ERROR_KEY, "Toast IS ERROR");
        } else {
            Toast.makeText(context, str, 0).show();
        }
    }

    public static void showToastNetWorkInfo(Context context) {
    }

    public static void showToastJsonStatus(Context context, n nVar) {
        if (context != null && nVar != null && nVar.i() != -1000) {
            showToast(context, a.a(nVar.i(), nVar.j()));
        }
    }

    public static void showToastJsonStatus(Context context, n nVar, int i) {
        if (context != null) {
            if (nVar.i() != -1000) {
                showToast(context, a.a(nVar.i(), nVar.j()));
            } else {
                showToast(context, context.getString(i));
            }
        }
    }

    public static int sendMailByIntent(Context context, String str) {
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("plain/text");
        intent.putExtra("android.intent.extra.EMAIL", new String[]{str});
        intent.putExtra("android.intent.extra.CC", "cc");
        intent.putExtra("android.intent.extra.TEXT", "android问题或建议：");
        context.startActivity(Intent.createChooser(intent, "感谢您的反馈"));
        return 1;
    }

    public static void showNewUpIcon(String str, Button button) {
        if (button != null) {
            if (TextUtils.isEmpty(str)) {
                button.setVisibility(8);
                return;
            }
            button.setVisibility(0);
            button.setText(str);
        }
    }

    public static void setNumUpIcon(int i, TextView textView) {
        if (textView != null) {
            if (i > 0) {
                textView.setVisibility(0);
                if (i < 100) {
                    textView.setText(new StringBuilder(String.valueOf(i)).toString());
                } else {
                    textView.setText("99+");
                }
            } else {
                textView.setVisibility(8);
            }
        }
    }

    public static void setNumUpIcon(int i, Button button) {
        if (button != null) {
            if (i > 0) {
                button.setVisibility(0);
                if (i < 100) {
                    button.setText(new StringBuilder(String.valueOf(i)).toString());
                } else {
                    button.setText("99+");
                }
            } else {
                button.setVisibility(8);
            }
        }
    }

    public static void setNewIcon(int i, TextView textView) {
        if (textView != null) {
            if (i > 0) {
                textView.setVisibility(0);
                textView.setText("new");
                return;
            }
            textView.setVisibility(8);
        }
    }

    public static void setNewIcon(int i, Button button) {
        if (button != null) {
            if (i > 0) {
                button.setVisibility(0);
                button.setText("new");
                return;
            }
            button.setVisibility(8);
        }
    }

    public static boolean processAnonymous(FragmentActivity fragmentActivity, String str, String str2) {
        if (Session.getCurrentAccount().d()) {
            Session.getSharedSession().updateAccount();
            if (Session.getCurrentAccount().d()) {
                tipInstallAC(fragmentActivity, str, str2);
                return true;
            }
        }
        return false;
    }

    public static void tipLoginDialog(FragmentActivity fragmentActivity) {
        tipInstallAC(fragmentActivity, null, null);
    }

    public static void installAC(FragmentActivity fragmentActivity) throws ACException {
        HashMap hashMap = new HashMap();
        if (!TextUtils.isEmpty(DownloadService.mFrom)) {
            hashMap.put("from", DownloadService.mFrom);
        }
        String urlEncode = KURL.urlEncode(s.a(APIKey.APIKey_GetAPK), hashMap, true);
        ULog.d("luolei", "apkurl: " + urlEncode);
        Intent intent = new Intent(fragmentActivity, DownloadService.class);
        intent.setAction(DownloadService.ACTION_ADD_TO_DOWNLOAD);
        intent.putExtra(DownloadService.DOWNLOAD_URL, urlEncode);
        fragmentActivity.startService(intent);
    }

    public static void tipEntryAc(final FragmentActivity fragmentActivity, final String str, final String str2) {
        final MyDialogFragment myDialogFragment = (MyDialogFragment) MyDialogFragment.a(fragmentActivity, fragmentActivity.getSupportFragmentManager()).a(k.a(fragmentActivity, mCurrentNotifyKey).f1726a).b(a.h.tip_for_entry_ac).e(a.h.cancel).d(a.h.ok).d();
        myDialogFragment.a(new c() {
            public void onPositiveButtonClicked(int i) {
                KShareUtil.entryAc(FragmentActivity.this, str, str2);
                myDialogFragment.dismiss();
            }

            public void onNegativeButtonClicked(int i) {
                myDialogFragment.dismiss();
            }
        });
        myDialogFragment.a(new b() {
            public void onCancelled(int i) {
                MyDialogFragment.this.dismiss();
            }
        });
    }

    public static void entryAc(Activity activity, String str, String str2) {
        Intent intent = new Intent();
        Uri uri = null;
        intent.addFlags(NTLMConstants.FLAG_UNIDENTIFIED_11);
        if (!TextUtils.isEmpty(str)) {
            uri = Uri.parse("aichang://webview?access_type=2&data_type=12&url=http%3A%2F%2Fweibo.aichang.cn%2Fapiv5%2Fuser%2Fuserinfo.php%3Fcmd%3Droominfo%26uid%3D" + str);
        }
        if (!TextUtils.isEmpty(str2)) {
            uri = Uri.parse("aichang://cn.banshenggua.aichang/roominfo?rid=" + str2);
        }
        intent.setComponent(new ComponentName(AC_PACK, AC_MAIN));
        if (uri != null) {
            intent.setData(uri);
        }
        activity.startActivity(intent);
        Toaster.showLongToast(a.h.entry_ac_toast);
        activity.finish();
    }

    public static void tipInstallAC(final FragmentActivity fragmentActivity, String str, String str2) {
        try {
            if (KShareApplication.getInstance().hasInstallAc()) {
                tipEntryAc(fragmentActivity, str, str2);
                return;
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
        k.a a2 = k.a(fragmentActivity, mCurrentNotifyKey);
        final MyDialogFragment myDialogFragment = (MyDialogFragment) MyDialogFragment.a(fragmentActivity, fragmentActivity.getSupportFragmentManager()).a(a2.f1726a).a((CharSequence) a2.f1727b).c(a2.d).b(a2.c).d();
        myDialogFragment.a(new c() {
            public void onPositiveButtonClicked(int i) {
                if (PreferencesUtils.loadPrefBoolean(FragmentActivity.this, PreferencesUtils.Entry_Market_Flag, false)) {
                    if (!KShareUtil.marketDownAndInstall(FragmentActivity.this, KShareUtil.AC_PACK)) {
                        KShareUtil.entryMarketForAc(FragmentActivity.this);
                    }
                    KShareUtil.setMarketToSD(FragmentActivity.this);
                    return;
                }
                try {
                    KShareUtil.installAC(FragmentActivity.this);
                    new i().a("download");
                    new i().a(KShareUtil.mCurrentNotifyKey.a(), "ok");
                } catch (ACException e) {
                    Toaster.showLongToast("安装爱唱失败");
                }
                myDialogFragment.dismiss();
            }

            public void onNegativeButtonClicked(int i) {
                myDialogFragment.dismiss();
                new i().a(KShareUtil.mCurrentNotifyKey.a(), "cancel");
            }
        });
        myDialogFragment.a(new b() {
            public void onCancelled(int i) {
                MyDialogFragment.this.dismiss();
            }
        });
    }

    public static void entryMarketForAc(Context context) {
        if (context != null) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse("market://details?id=cn.banshenggua.aichang"));
            context.startActivity(intent);
        }
    }

    public static void setMarketToSD(Context context) {
        FileUtils.stringToFile(new File(CommonUtil.getDownloadDir(), "sdkconfig"), "dd_new_zhibo");
    }

    public static void pushFromBottom(FragmentActivity fragmentActivity, Fragment fragment, int i) {
        FragmentTransaction beginTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
        beginTransaction.replace(i, fragment);
        beginTransaction.addToBackStack(null);
        beginTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        beginTransaction.commitAllowingStateLoss();
        fragment.getFragmentManager().executePendingTransactions();
    }

    public static void pop(Fragment fragment) {
        FragmentManager supportFragmentManager;
        if (fragment != null) {
            try {
                if (!fragment.isRemoving() && fragment.getActivity() != null && (supportFragmentManager = fragment.getActivity().getSupportFragmentManager()) != null) {
                    supportFragmentManager.beginTransaction().commitAllowingStateLoss();
                    supportFragmentManager.popBackStackImmediate();
                }
            } catch (Exception e) {
            }
        }
    }
}
