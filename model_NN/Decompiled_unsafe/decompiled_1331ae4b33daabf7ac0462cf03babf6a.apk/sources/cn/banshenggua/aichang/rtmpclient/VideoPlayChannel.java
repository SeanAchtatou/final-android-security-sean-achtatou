package cn.banshenggua.aichang.rtmpclient;

import android.text.TextUtils;
import cn.banshenggua.aichang.rtmpclient.AudioChannel;
import cn.banshenggua.aichang.utils.ULog;

public class VideoPlayChannel extends Channel implements Runnable {
    boolean isRunning = false;
    private AudioChannel mAudio = null;
    private int mHeight = 0;
    public OnSubscriberListener mListener = null;
    private VideoPlayerInterface mPlayView = null;
    Thread mThread;
    private int mUpdateHeight = 0;
    private int mViewHeight = 0;
    private int mViewWidth = 0;
    private int mWidth = 0;

    public void setListener(OnSubscriberListener onSubscriberListener) {
        this.mListener = onSubscriberListener;
    }

    public VideoPlayChannel(String str, String[] strArr) {
        super(str, strArr);
    }

    public VideoPlayChannel(String str, String[] strArr, int i, int i2, VideoPlayerInterface videoPlayerInterface) {
        super(str, strArr);
        setSid(i);
        setCid(i2);
        attachPlayView(videoPlayerInterface);
    }

    public void attachPlayView(VideoPlayerInterface videoPlayerInterface) {
        if (this.mPlayView == null) {
            this.mPlayView = videoPlayerInterface;
        }
    }

    public void setAudioChannel(AudioChannel audioChannel) {
        this.mAudio = audioChannel;
    }

    private byte[] processYuvData(int i, int i2, int i3, int i4, byte[] bArr) {
        if (i3 == i && i4 == i2) {
            return bArr;
        }
        if (i3 > i) {
            i3 = i;
        }
        if (i4 > i2) {
            i4 = i2;
        }
        int i5 = i3 * i4;
        byte[] bArr2 = new byte[((int) (((double) i5) * 1.5d))];
        System.arraycopy(bArr, 0, bArr2, 0, i5);
        System.arraycopy(bArr, i * i2, bArr2, i5, (int) (((double) i5) * 0.5d));
        return bArr2;
    }

    private int updateNewHeight(int i, int i2) {
        if (this.mPlayView == null) {
            return i2;
        }
        int i3 = this.mPlayView.getwidth();
        int i4 = this.mPlayView.getheight();
        if (this.mUpdateHeight != 0 && this.mViewHeight == i4 && this.mViewWidth == i3 && this.mHeight == i2 && this.mWidth == i) {
            return this.mUpdateHeight;
        }
        this.mViewHeight = i4;
        this.mViewWidth = i3;
        this.mHeight = i2;
        this.mWidth = i;
        if (i3 > 0 && i4 > 0) {
            float f = ((float) i4) / ((float) i3);
            if (f != ((float) i2) / ((float) i2)) {
                i2 = (int) (f * ((float) i));
            }
        }
        this.mUpdateHeight = i2;
        return this.mUpdateHeight;
    }

    private void drawFrame(int i, int i2, byte[] bArr) {
        if (this.mPlayView != null) {
            int updateNewHeight = updateNewHeight(i, i2);
            if (updateNewHeight > i2) {
                updateNewHeight = i2;
            }
            this.mPlayView.drawFrame(i, updateNewHeight, processYuvData(i, i2, i, updateNewHeight, bArr));
        }
    }

    public void beginShow() {
    }

    public void run() {
        byte[] bArr;
        int i;
        int i2;
        this.isRunning = true;
        int i3 = 0;
        int i4 = 0;
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                i3 = Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
            }
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("cid"))) {
            try {
                i4 = Integer.parseInt((String) this.mFinger.get("cid"));
            } catch (Exception e2) {
            }
        }
        AudioChannel.Status status = AudioChannel.Status.NORMAL;
        int i5 = 0;
        if (!(this.mListener == null || status == AudioChannel.Status.Load)) {
            this.mListener.OnLoading();
        }
        AudioChannel.Status status2 = AudioChannel.Status.Load;
        try {
            Thread.sleep(500);
        } catch (Exception e3) {
        }
        int i6 = -1;
        while (true) {
            int i7 = i6;
            int i8 = i5;
            AudioChannel.Status status3 = status2;
            if (this.shouldStop) {
                break;
            }
            long currentTimeMillis = System.currentTimeMillis();
            byte[] bArr2 = null;
            if (i8 == 0) {
                ULog.d("luolei", "video channel run data null " + this.shouldStop + "; cid = " + i4 + "; sid: " + i3);
            }
            if (this.manager != null) {
                bArr2 = this.manager.getdata(i3);
            }
            VideoSize videoSize = new VideoSize();
            long j = 0;
            if (this.manager2 != null) {
                if (this.mAudio != null) {
                    j = this.mAudio.getPlayTime();
                }
                bArr = this.manager2.getdata(i4, i3, false, j, videoSize);
            } else {
                bArr = bArr2;
            }
            if (i8 == 0) {
                if (bArr == null) {
                    ULog.d("luolei", "video channel run data null");
                } else {
                    ULog.d("luolei", "video channel run datasize: " + bArr.length);
                }
            }
            i5 = i8 + 1;
            if (i8 > 120) {
                i5 = 0;
            }
            if (bArr == null || bArr.length <= 0) {
                i6 = i7 + 1;
                if (i6 > 5 || i6 == -1) {
                    if (!(this.mListener == null || status3 == AudioChannel.Status.Load)) {
                        this.mListener.OnLoading();
                    }
                    status2 = AudioChannel.Status.Load;
                } else {
                    status2 = status3;
                }
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e4) {
                    e4.printStackTrace();
                }
            } else {
                if (!(this.mListener == null || status3 == AudioChannel.Status.PLAY)) {
                    this.mListener.OnPlaying();
                }
                i6 = 0;
                status2 = AudioChannel.Status.PLAY;
                if (i5 == 0) {
                    ULog.d("luolei", "video channel run datasize: " + bArr.length + "; " + videoSize.getWidth() + " x " + videoSize.getHeight() + "; cid: " + i4 + "; sid: " + i3);
                }
                if (videoSize.getWidth() <= 0 || videoSize.getHeight() <= 0) {
                    i = 320;
                    i2 = 240;
                } else {
                    int width = videoSize.getWidth();
                    i2 = videoSize.getHeight();
                    i = width;
                }
                drawFrame(i, i2, bArr);
                long currentTimeMillis2 = 30 - (System.currentTimeMillis() - currentTimeMillis);
                if (currentTimeMillis2 > 0) {
                    try {
                        Thread.sleep(currentTimeMillis2);
                    } catch (InterruptedException e5) {
                        e5.printStackTrace();
                    }
                }
            }
        }
        if (this.mListener != null) {
            this.mListener.OnFinish();
        }
        this.isRunning = false;
    }

    public void stopChannel() {
        ULog.d("luolei", "stop channel: " + this.shouldStop);
        this.shouldStop = true;
    }

    public void startChannel() {
        this.mThread = new Thread(this);
        this.mThread.setPriority(10);
        this.mThread.start();
    }
}
