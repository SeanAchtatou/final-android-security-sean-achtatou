package cn.banshenggua.aichang.rtmpclient;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;

public abstract class Channel {
    public int bitrate = 300000;
    public int height = 0;
    public int mBufferTime = -1;
    public String mChannelName = "";
    public String[] mChannelParams = null;
    public Map<String, String> mFinger = new HashMap();
    public RtmpClientManager manager;
    public RtmpClientManager2 manager2;
    public int oheight = 0;
    public int owidth = 0;
    public int quality = 0;
    public boolean shouldStop = false;
    public int width = 0;

    public abstract void startChannel();

    public abstract void stopChannel();

    public Channel(String str, String[] strArr) {
        this.mChannelName = str;
        this.mChannelParams = strArr;
    }

    public void setInOutSize(int i, int i2, int i3, int i4) {
        this.width = i;
        this.height = i2;
        this.owidth = i3;
        this.oheight = i4;
    }

    public void setOutQuality(int i, int i2) {
        this.bitrate = i;
        this.quality = i2;
    }

    public boolean isSetInOutSize() {
        return this.width > 0 && this.height > 0 && this.owidth > 0 && this.oheight > 0;
    }

    public void setBuffer(int i) {
        int i2;
        int i3 = 0;
        this.mBufferTime = i;
        if (!TextUtils.isEmpty(this.mFinger.get("sid"))) {
            try {
                i2 = Integer.parseInt(this.mFinger.get("sid"));
            } catch (Exception e) {
                i2 = 0;
            }
        } else {
            i2 = 0;
        }
        if (!TextUtils.isEmpty(this.mFinger.get("cid"))) {
            try {
                i3 = Integer.parseInt(this.mFinger.get("cid"));
            } catch (Exception e2) {
            }
        }
        if (this.manager2 != null) {
            this.manager2.setBuffer(i3, i2, true, i);
        }
    }

    public void setSid(int i) {
        this.mFinger.put("sid", new StringBuilder().append(i).toString());
    }

    public void setCid(int i) {
        this.mFinger.put("cid", new StringBuilder().append(i).toString());
    }

    public String getCid() {
        return this.mFinger.get("cid");
    }

    public String getSid() {
        return this.mFinger.get("sid");
    }

    public boolean equal(Channel channel) {
        if (channel == null || TextUtils.isEmpty(channel.mChannelName) || TextUtils.isEmpty(this.mChannelName)) {
            return false;
        }
        return this.mChannelName.equals(channel.mChannelName);
    }

    public void updateManager(RtmpClientManager rtmpClientManager) {
        this.manager = rtmpClientManager;
    }

    public void updateManager(RtmpClientManager2 rtmpClientManager2) {
        this.manager2 = rtmpClientManager2;
    }
}
