package cn.banshenggua.aichang.rtmpclient;

import android.text.TextUtils;

public class VideoPubChannel extends Channel implements PubInterface {
    public VideoPubChannel(String str, String[] strArr) {
        super(str, strArr);
    }

    public void pushdata(byte[] bArr, int i) {
    }

    public void pushdata(byte[] bArr, int i, long j, boolean z) {
        int i2;
        int i3;
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                i2 = Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
                i2 = 0;
            }
        } else {
            i2 = 0;
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("cid"))) {
            try {
                i3 = Integer.parseInt((String) this.mFinger.get("cid"));
            } catch (Exception e2) {
                i3 = 0;
            }
        } else {
            i3 = 0;
        }
        if (this.manager2 != null && !this.shouldStop) {
            this.manager2.pushdata(i3, i2, false, bArr, i, j, z);
        }
    }

    public void stop() {
        stopChannel();
    }

    public void start() {
        startChannel();
    }

    public void startChannel() {
    }

    public void stopChannel() {
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
            }
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("cid"))) {
            try {
                Integer.parseInt((String) this.mFinger.get("cid"));
            } catch (Exception e2) {
            }
        }
        this.shouldStop = true;
    }

    public boolean isConnected() {
        int i;
        if (this.manager2 == null) {
            return false;
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("cid"))) {
            try {
                i = Integer.parseInt((String) this.mFinger.get("cid"));
            } catch (Exception e) {
                return false;
            }
        } else {
            i = 0;
        }
        return this.manager2.isConnected(i);
    }

    public void updateVideoSize(VideoSize videoSize, VideoSize videoSize2, int i, int i2) {
        int i3;
        int i4;
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("sid"))) {
            try {
                i3 = Integer.parseInt((String) this.mFinger.get("sid"));
            } catch (Exception e) {
                i3 = 0;
            }
        } else {
            i3 = 0;
        }
        if (!TextUtils.isEmpty((CharSequence) this.mFinger.get("cid"))) {
            try {
                i4 = Integer.parseInt((String) this.mFinger.get("cid"));
            } catch (Exception e2) {
                i4 = 0;
            }
        } else {
            i4 = 0;
        }
        if (this.manager2 != null && !this.shouldStop) {
            this.manager2.changeInputConfig(i4, i3, false, videoSize.width, videoSize.height, videoSize2.width, videoSize2.height, i, i2);
        }
    }
}
