package cn.banshenggua.aichang.rtmpclient;

import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import cn.banshenggua.aichang.utils.ULog;
import java.nio.Buffer;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class TextureRenderer implements GLSurfaceView.Renderer {
    private static final String TAG = "TextureRenderer";
    private Context mActivityContext;
    private ByteBuffer mBuffer;
    private final int mBytesPerFloat = 4;
    private final FloatBuffer mColor;
    private final int mColorDataSize = 4;
    private int mColorHandle;
    private int mHeight;
    private Bitmap mImage;
    private final int mPositionDataSize = 3;
    private int mPositionHandle;
    private int mProgramHandle;
    private ByteBuffer mReadBuffer;
    private final FloatBuffer mScreenPosition;
    private final int mTexCoordinateDataSize = 2;
    private int mTexCoordinateHandle;
    private int mTexture0Id;
    private int mTexture1Id;
    private final FloatBuffer mTextureCoordinate;
    private int mTextureUniformHandle0;
    private int mTextureUniformHandle1;
    private int mWidth;

    TextureRenderer(Context context) {
        this.mActivityContext = context;
        float[] fArr = {-1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f};
        float[] fArr2 = {1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f};
        float[] fArr3 = {0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f};
        this.mScreenPosition = ByteBuffer.allocateDirect(fArr.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.mTextureCoordinate = ByteBuffer.allocateDirect(fArr3.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.mColor = ByteBuffer.allocateDirect(fArr2.length * 4).order(ByteOrder.nativeOrder()).asFloatBuffer();
        this.mScreenPosition.put(fArr).position(0);
        this.mTextureCoordinate.put(fArr3).position(0);
        this.mColor.put(fArr2).position(0);
    }

    public void onSurfaceChanged(GL10 gl10, int i, int i2) {
        ULog.d(TAG, "onSurfaceChanged");
        GLES20.glViewport(0, 0, i, i2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:6:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSurfaceCreated(javax.microedition.khronos.opengles.GL10 r10, javax.microedition.khronos.egl.EGLConfig r11) {
        /*
            r9 = this;
            r8 = 33071(0x812f, float:4.6342E-41)
            r7 = 1175979008(0x46180400, float:9729.0)
            r6 = 1
            r5 = 3553(0xde1, float:4.979E-42)
            r1 = 0
            java.lang.String r0 = "TextureRenderer"
            java.lang.String r2 = "onSurfaceCreated"
            cn.banshenggua.aichang.utils.ULog.d(r0, r2)
            java.lang.String r0 = "attribute vec4 a_Position;            \nattribute vec4 a_Color;               \nattribute vec2 a_TexCoordinate;       \n                                      \nvarying vec4 v_Color;                 \nvarying vec2 v_TexCoordinate;         \n                                      \nvoid main()                           \n{                                     \n   v_Color = a_Color;                 \n \tv_TexCoordinate = a_TexCoordinate; \n   gl_Position = a_Position;          \n                                  \t   \n}                                     \n"
            java.lang.String r0 = "precision mediump float;\t\t\t\t\t\t\t\t  \n                                                         \nuniform sampler2D u_Texture0;                            \nuniform sampler2D u_Texture1;                            \nvarying vec2 v_TexCoordinate;                            \n                                                         \nconst vec3 offset = vec3(0.0625, 0.5, 0.5);              \nconst mat3 coeffs = mat3(              \t\t\t\t  \n\t1.164,  1.164,  1.164,              \t\t\t\t  \n\t1.596, -0.813,  0.0,              \t\t\t\t\t  \n\t0.0  , -0.391,  2.018 );              \t\t\t\t  \n              \t\t\t\t\t\t\t\t\t\t\t  \nvec3 texture2Dsmart(vec2 uv)              \t\t\t\t  \n  {\t\t\t\t\t\t\t\t\t\t\t\t\t  \n\t\treturn coeffs*(vec3(texture2D(u_Texture0, uv).r, texture2D(u_Texture1, uv).ra) - offset);  \n  }              \t\t\t\t\t\t\t\t\t\t  \n              \t\t\t\t\t\t\t\t\t\t\t  \nvoid main()                                              \n{                                                         \n  gl_FragColor = vec4(texture2Dsmart(v_TexCoordinate), 1.0);\n}                                                         \n"
            r0 = 35633(0x8b31, float:4.9932E-41)
            int r0 = android.opengl.GLES20.glCreateShader(r0)
            if (r0 == 0) goto L_0x0139
            java.lang.String r2 = "attribute vec4 a_Position;            \nattribute vec4 a_Color;               \nattribute vec2 a_TexCoordinate;       \n                                      \nvarying vec4 v_Color;                 \nvarying vec2 v_TexCoordinate;         \n                                      \nvoid main()                           \n{                                     \n   v_Color = a_Color;                 \n \tv_TexCoordinate = a_TexCoordinate; \n   gl_Position = a_Position;          \n                                  \t   \n}                                     \n"
            android.opengl.GLES20.glShaderSource(r0, r2)
            android.opengl.GLES20.glCompileShader(r0)
            int[] r2 = new int[r6]
            r3 = 35713(0x8b81, float:5.0045E-41)
            android.opengl.GLES20.glGetShaderiv(r0, r3, r2, r1)
            r2 = r2[r1]
            if (r2 != 0) goto L_0x0139
            android.opengl.GLES20.glDeleteShader(r0)
            r2 = r1
        L_0x0036:
            if (r2 != 0) goto L_0x0040
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Error creating vertex shader."
            r0.<init>(r1)
            throw r0
        L_0x0040:
            r0 = 35632(0x8b30, float:4.9931E-41)
            int r0 = android.opengl.GLES20.glCreateShader(r0)
            if (r0 == 0) goto L_0x0061
            java.lang.String r3 = "precision mediump float;\t\t\t\t\t\t\t\t  \n                                                         \nuniform sampler2D u_Texture0;                            \nuniform sampler2D u_Texture1;                            \nvarying vec2 v_TexCoordinate;                            \n                                                         \nconst vec3 offset = vec3(0.0625, 0.5, 0.5);              \nconst mat3 coeffs = mat3(              \t\t\t\t  \n\t1.164,  1.164,  1.164,              \t\t\t\t  \n\t1.596, -0.813,  0.0,              \t\t\t\t\t  \n\t0.0  , -0.391,  2.018 );              \t\t\t\t  \n              \t\t\t\t\t\t\t\t\t\t\t  \nvec3 texture2Dsmart(vec2 uv)              \t\t\t\t  \n  {\t\t\t\t\t\t\t\t\t\t\t\t\t  \n\t\treturn coeffs*(vec3(texture2D(u_Texture0, uv).r, texture2D(u_Texture1, uv).ra) - offset);  \n  }              \t\t\t\t\t\t\t\t\t\t  \n              \t\t\t\t\t\t\t\t\t\t\t  \nvoid main()                                              \n{                                                         \n  gl_FragColor = vec4(texture2Dsmart(v_TexCoordinate), 1.0);\n}                                                         \n"
            android.opengl.GLES20.glShaderSource(r0, r3)
            android.opengl.GLES20.glCompileShader(r0)
            int[] r3 = new int[r6]
            r4 = 35713(0x8b81, float:5.0045E-41)
            android.opengl.GLES20.glGetShaderiv(r0, r4, r3, r1)
            r3 = r3[r1]
            if (r3 != 0) goto L_0x0061
            android.opengl.GLES20.glDeleteShader(r0)
            r0 = r1
        L_0x0061:
            if (r0 != 0) goto L_0x006b
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Error creating fragment shader."
            r0.<init>(r1)
            throw r0
        L_0x006b:
            int r3 = android.opengl.GLES20.glCreateProgram()
            r9.mProgramHandle = r3
            int r3 = r9.mProgramHandle
            if (r3 == 0) goto L_0x00a0
            int r3 = r9.mProgramHandle
            android.opengl.GLES20.glAttachShader(r3, r2)
            int r2 = r9.mProgramHandle
            android.opengl.GLES20.glAttachShader(r2, r0)
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "a_Position"
            android.opengl.GLES20.glBindAttribLocation(r0, r1, r2)
            int r0 = r9.mProgramHandle
            android.opengl.GLES20.glLinkProgram(r0)
            int[] r0 = new int[r6]
            int r2 = r9.mProgramHandle
            r3 = 35714(0x8b82, float:5.0046E-41)
            android.opengl.GLES20.glGetProgramiv(r2, r3, r0, r1)
            r0 = r0[r1]
            if (r0 != 0) goto L_0x00a0
            int r0 = r9.mProgramHandle
            android.opengl.GLES20.glDeleteProgram(r0)
            r9.mProgramHandle = r1
        L_0x00a0:
            int r0 = r9.mProgramHandle
            if (r0 != 0) goto L_0x00ac
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Error creating program."
            r0.<init>(r1)
            throw r0
        L_0x00ac:
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "a_Position"
            int r0 = android.opengl.GLES20.glGetAttribLocation(r0, r2)
            r9.mPositionHandle = r0
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "a_Color"
            int r0 = android.opengl.GLES20.glGetAttribLocation(r0, r2)
            r9.mColorHandle = r0
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "a_TexCoordinate"
            int r0 = android.opengl.GLES20.glGetAttribLocation(r0, r2)
            r9.mTexCoordinateHandle = r0
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "u_Texture0"
            int r0 = android.opengl.GLES20.glGetUniformLocation(r0, r2)
            r9.mTextureUniformHandle0 = r0
            int r0 = r9.mProgramHandle
            java.lang.String r2 = "u_Texture1"
            int r0 = android.opengl.GLES20.glGetUniformLocation(r0, r2)
            r9.mTextureUniformHandle1 = r0
            int[] r0 = new int[r6]
            android.opengl.GLES20.glGenTextures(r6, r0, r1)
            r2 = r0[r1]
            if (r2 == 0) goto L_0x0129
            r2 = r0[r1]
            r9.mTexture0Id = r2
            int r2 = r9.mTexture0Id
            android.opengl.GLES20.glBindTexture(r5, r2)
            r2 = 10241(0x2801, float:1.435E-41)
            android.opengl.GLES20.glTexParameterf(r5, r2, r7)
            r2 = 10240(0x2800, float:1.4349E-41)
            android.opengl.GLES20.glTexParameterf(r5, r2, r7)
            r2 = 10242(0x2802, float:1.4352E-41)
            android.opengl.GLES20.glTexParameteri(r5, r2, r8)
            r2 = 10243(0x2803, float:1.4354E-41)
            android.opengl.GLES20.glTexParameteri(r5, r2, r8)
            android.opengl.GLES20.glGenTextures(r6, r0, r1)
            r2 = r0[r1]
            if (r2 == 0) goto L_0x0131
            r0 = r0[r1]
            r9.mTexture1Id = r0
            int r0 = r9.mTexture1Id
            android.opengl.GLES20.glBindTexture(r5, r0)
            r0 = 10241(0x2801, float:1.435E-41)
            android.opengl.GLES20.glTexParameterf(r5, r0, r7)
            r0 = 10240(0x2800, float:1.4349E-41)
            android.opengl.GLES20.glTexParameterf(r5, r0, r7)
            r0 = 10242(0x2802, float:1.4352E-41)
            android.opengl.GLES20.glTexParameteri(r5, r0, r8)
            r0 = 10243(0x2803, float:1.4354E-41)
            android.opengl.GLES20.glTexParameteri(r5, r0, r8)
            return
        L_0x0129:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Error loading texture."
            r0.<init>(r1)
            throw r0
        L_0x0131:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Error loading texture."
            r0.<init>(r1)
            throw r0
        L_0x0139:
            r2 = r0
            goto L_0x0036
        */
        throw new UnsupportedOperationException("Method not decompiled: cn.banshenggua.aichang.rtmpclient.TextureRenderer.onSurfaceCreated(javax.microedition.khronos.opengles.GL10, javax.microedition.khronos.egl.EGLConfig):void");
    }

    public synchronized void drawFrame(int i, int i2, byte[] bArr) {
        if (this.mBuffer == null) {
            this.mBuffer = ByteBuffer.allocateDirect(bArr.length);
        }
        if (this.mBuffer.limit() < bArr.length) {
            this.mBuffer = ByteBuffer.allocateDirect(bArr.length);
        }
        this.mBuffer.clear();
        try {
            this.mBuffer.put(bArr);
        } catch (BufferOverflowException e) {
        }
        this.mBuffer.position(0);
        this.mWidth = i;
        this.mHeight = i2;
    }

    public void loadTexture(int i, int i2, Bitmap bitmap) {
        setImage(bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, java.nio.Buffer):void}
     arg types: [int, int, int, int, int, java.nio.FloatBuffer]
     candidates:
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, int):void}
      ClspMth{android.opengl.GLES20.glVertexAttribPointer(int, int, int, boolean, int, java.nio.Buffer):void} */
    public void onDrawFrame(GL10 gl10) {
        GLES20.glClear(16640);
        GLES20.glUseProgram(this.mProgramHandle);
        this.mScreenPosition.position(0);
        GLES20.glVertexAttribPointer(this.mPositionHandle, 3, 5126, false, 0, (Buffer) this.mScreenPosition);
        GLES20.glEnableVertexAttribArray(this.mPositionHandle);
        this.mTextureCoordinate.position(0);
        GLES20.glVertexAttribPointer(this.mTexCoordinateHandle, 2, 5126, false, 0, (Buffer) this.mTextureCoordinate);
        GLES20.glEnableVertexAttribArray(this.mTexCoordinateHandle);
        loadAndDrawTextureFromBuffer();
        GLES20.glDrawArrays(4, 0, 6);
    }

    private void loadAndDrawTexture() {
        Bitmap image = getImage();
        if (image != null) {
            GLES20.glActiveTexture(33984);
            GLES20.glBindTexture(3553, this.mTexture0Id);
            GLES20.glUniform1i(this.mTextureUniformHandle0, 0);
            GLUtils.texImage2D(3553, 0, image, 0);
            image.recycle();
        }
    }

    private void loadAndDrawTextureFromBuffer() {
        ByteBuffer buffer = getBuffer();
        if (buffer != null) {
            GLES20.glActiveTexture(33984);
            buffer.position(0);
            GLES20.glBindTexture(3553, this.mTexture0Id);
            GLES20.glTexImage2D(3553, 0, 6409, this.mWidth, this.mHeight, 0, 6409, 5121, buffer);
            GLES20.glUniform1i(this.mTextureUniformHandle0, 0);
            GLES20.glActiveTexture(33985);
            buffer.position(this.mWidth * this.mHeight);
            buffer.position();
            buffer.remaining();
            GLES20.glBindTexture(3553, this.mTexture1Id);
            GLES20.glTexImage2D(3553, 0, 6410, this.mWidth / 2, this.mHeight / 2, 0, 6410, 5121, buffer);
            buffer.position(0);
            GLES20.glUniform1i(this.mTextureUniformHandle1, 1);
        }
    }

    private synchronized Bitmap getImage() {
        Bitmap bitmap;
        bitmap = this.mImage;
        this.mImage = null;
        return bitmap;
    }

    private synchronized ByteBuffer getBuffer() {
        return this.mBuffer;
    }

    private synchronized void setImage(Bitmap bitmap) {
        this.mImage = bitmap;
    }
}
