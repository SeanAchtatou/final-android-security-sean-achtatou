package cn.banshenggua.aichang.entry;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import cn.banshenggua.aichang.app.KShareApplication;
import cn.banshenggua.aichang.sdk.ACException;

public class BaseFragment extends Fragment {
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            if (!KShareApplication.getInstance().isInitData) {
                KShareApplication.getInstance().initData(false);
            }
        } catch (ACException e) {
            e.printStackTrace();
        }
    }
}
