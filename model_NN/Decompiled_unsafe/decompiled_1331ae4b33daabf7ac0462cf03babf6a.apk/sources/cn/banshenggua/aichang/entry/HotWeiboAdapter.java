package cn.banshenggua.aichang.entry;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.app.DownloadService;
import cn.banshenggua.aichang.player.PlayerActivity;
import cn.banshenggua.aichang.room.SimpleLiveRoomActivity;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.UIUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.h;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.v;

public class HotWeiboAdapter extends ArrayListAdapter<v> implements View.OnClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType = null;
    private static final int ITEMS = 2;
    private static final int ITEM_MARGE = 20;
    private static final String TAG = "HotWeiboAdapter";
    private Activity activity;
    private d imgLoader;
    private int item_height = -1;
    c levelOptions = ImageUtil.getDefaultLevelOption();
    public String mFrom = "";
    c options = ImageUtil.getDefaultPlayerImageAminOption(1);
    private boolean showback = false;

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType;
        if (iArr == null) {
            iArr = new int[v.a.values().length];
            try {
                iArr[v.a.HECHANG.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[v.a.INVITE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[v.a.NONE.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[v.a.NORMARE.ordinal()] = 1;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType = iArr;
        }
        return iArr;
    }

    public HotWeiboAdapter(Activity activity2, String str) {
        super(activity2);
        this.activity = activity2;
        this.imgLoader = d.a();
        this.mFrom = str;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null || view.getTag() == null) {
            view = this.mInflater.inflate(a.g.item_hot_weibo, (ViewGroup) null);
            viewHolder = createHolder(view, true);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.footer.setVisibility(8);
        if (i == getCount() - 1) {
            viewHolder.footer.setVisibility(0);
        }
        showWeibos(viewHolder, i);
        return view;
    }

    public int getCount() {
        ULog.d(TAG, "mList.size() = " + this.mList.size());
        return getListSize() / 2;
    }

    public int getListSize() {
        if (this.mList.size() == 0) {
            return 0;
        }
        return this.mList.size() - 1;
    }

    public v getWeibo(int i) {
        if (i + 1 < this.mList.size()) {
            return (v) this.mList.get(i + 1);
        }
        return null;
    }

    private void showWeibos(ViewHolder viewHolder, int i) {
        ULog.d(TAG, "showWeibos 001 ; position: " + i);
        View[] viewArr = {viewHolder.item1, viewHolder.item2};
        ImageView[] imageViewArr = {viewHolder.image01, viewHolder.image02};
        ImageView[] imageViewArr2 = {viewHolder.sex01, viewHolder.sex02};
        ImageView[] imageViewArr3 = {viewHolder.grade01, viewHolder.grade02};
        ImageView[] imageViewArr4 = {viewHolder.authIcon1, viewHolder.authIcon2};
        ImageView[] imageViewArr5 = {viewHolder.videoIcon1, viewHolder.videoIcon2};
        TextView[] textViewArr = {viewHolder.title01, viewHolder.title02};
        TextView[] textViewArr2 = {viewHolder.peopleNum1, viewHolder.peopleNum2};
        ULog.d(TAG, "showWeibos 002 ; position: " + i);
        for (int i2 = 0; i2 < imageViewArr.length; i2++) {
            viewArr[i2].setVisibility(0);
            if (!this.showback || i != 0) {
                imageViewArr[i2].setImageBitmap(null);
                imageViewArr[i2].setBackgroundResource(a.e.default_my);
                textViewArr[i2].setVisibility(8);
                imageViewArr2[i2].setVisibility(8);
                imageViewArr3[i2].setVisibility(8);
            }
        }
        ULog.d(TAG, "showWeibos 003 ; position: " + i);
        v[] vVarArr = new v[2];
        int listSize = getListSize();
        for (int i3 = 0; i3 < vVarArr.length; i3++) {
            if (i3 == 0 && this.showback && i == 0) {
                vVarArr[0] = null;
            } else if (this.showback) {
                if (((i * 2) + i3) - 1 < listSize) {
                    vVarArr[i3] = getWeibo(((i * 2) + i3) - 1);
                } else {
                    vVarArr[i3] = null;
                }
            } else if ((i * 2) + i3 < listSize) {
                vVarArr[i3] = getWeibo((i * 2) + i3);
            } else {
                vVarArr[i3] = null;
            }
        }
        ULog.d(TAG, "showWeibos 004 ; position: " + i);
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 >= vVarArr.length) {
                ULog.d(TAG, "showWeibos 005 ; position: " + i);
                return;
            }
            v vVar = vVarArr[i5];
            if (vVar == null) {
                viewArr[i5].setVisibility(4);
            } else {
                textViewArr[i5].setVisibility(0);
                imageViewArr2[i5].setVisibility(8);
                imageViewArr3[i5].setVisibility(0);
                setSex(imageViewArr2[i5], vVar);
                this.imgLoader.a(h.a(h.a.SIM, Integer.valueOf(vVar.p).intValue()), imageViewArr3[i5]);
                this.imgLoader.a(vVar.n, imageViewArr[i5], this.options);
                String str = vVar.A;
                switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType()[vVar.d().ordinal()]) {
                    case 2:
                        str = String.valueOf(this.mContext.getResources().getString(a.h.hot_invate)) + str;
                        break;
                    case 3:
                        str = String.valueOf(this.mContext.getResources().getString(a.h.hot_hechang)) + str;
                        break;
                }
                imageViewArr[i5].setTag(vVar);
                textViewArr[i5].setText(vVar.b());
                imageViewArr[i5].setOnClickListener(this);
                textViewArr2[i5].setText(str);
                if (!TextUtils.isEmpty(vVar.W)) {
                    ImageLoaderUtil.displayImageBg(imageViewArr4[i5], vVar.Y, this.levelOptions);
                    imageViewArr4[i5].setVisibility(0);
                } else {
                    imageViewArr4[i5].setVisibility(8);
                }
                if (vVar.a() == v.b.Video) {
                    imageViewArr5[i5].setVisibility(0);
                } else {
                    imageViewArr5[i5].setVisibility(8);
                }
            }
            i4 = i5 + 1;
        }
    }

    private void setSex(ImageView imageView, v vVar) {
        if (vVar.w == 1) {
            imageView.setImageResource(a.e.zone_image_boy);
        } else {
            imageView.setImageResource(a.e.zone_image_girl);
        }
    }

    public void showWeibo(ViewHolder viewHolder, v vVar) {
        if (vVar != null) {
            viewHolder.title01.setVisibility(0);
            viewHolder.sex01.setVisibility(8);
            setSex(viewHolder.sex01, vVar);
            this.imgLoader.a(h.a(h.a.SIM, Integer.valueOf(vVar.p).intValue()), viewHolder.grade01, this.levelOptions);
            this.imgLoader.a(vVar.n, viewHolder.image01, this.options);
            String str = vVar.A;
            switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$WeiBo$FanchangType()[vVar.d().ordinal()]) {
                case 2:
                    str = String.valueOf(this.mContext.getResources().getString(a.h.hot_invate)) + str;
                    break;
                case 3:
                    str = String.valueOf(this.mContext.getResources().getString(a.h.hot_hechang)) + str;
                    break;
            }
            viewHolder.image01.setTag(vVar);
            viewHolder.title01.setText(vVar.b());
            viewHolder.peopleNum1.setText(str);
            viewHolder.image01.setOnClickListener(this);
            viewHolder.footer.setVisibility(8);
            if (!TextUtils.isEmpty(vVar.W)) {
                ImageLoaderUtil.displayImageBg(viewHolder.authIcon1, vVar.Y, this.levelOptions);
                viewHolder.authIcon1.setVisibility(0);
            } else {
                viewHolder.authIcon1.setVisibility(8);
            }
            if (vVar.a() == v.b.Video) {
                viewHolder.videoIcon1.setVisibility(0);
            } else {
                viewHolder.videoIcon1.setVisibility(8);
            }
        }
    }

    private void initHolderHeight(View view) {
        if (this.item_height < 0) {
            this.item_height = (UIUtil.getInstance().getmScreenWidth() * 188) / 480;
        }
        View findViewById = view.findViewById(a.f.weibo_item_layout_top);
        ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
        layoutParams.height = this.item_height;
        findViewById.setLayoutParams(layoutParams);
        ULog.d(TAG, "item_height: " + this.item_height);
    }

    public ViewHolder createHolder(View view, Boolean bool) {
        ViewHolder viewHolder = new ViewHolder();
        if (bool.booleanValue()) {
            initHolderHeight(view);
        }
        viewHolder.item1 = view.findViewById(a.f.weibo_item_01);
        viewHolder.item2 = view.findViewById(a.f.weibo_item_02);
        viewHolder.image01 = (ImageView) view.findViewById(a.f.weibo_item_image_01);
        viewHolder.image02 = (ImageView) view.findViewById(a.f.weibo_item_image_02);
        viewHolder.grade01 = (ImageView) view.findViewById(a.f.weibo_item_grade_btn_01);
        viewHolder.grade02 = (ImageView) view.findViewById(a.f.weibo_item_grade_btn_02);
        viewHolder.authIcon1 = (ImageView) view.findViewById(a.f.weibo_item_auth_btn_01);
        viewHolder.authIcon2 = (ImageView) view.findViewById(a.f.weibo_item_auth_btn_02);
        viewHolder.sex01 = (ImageView) view.findViewById(a.f.weibo_item_sex_btn_01);
        viewHolder.sex02 = (ImageView) view.findViewById(a.f.weibo_item_sex_btn_02);
        viewHolder.sex01.setVisibility(8);
        viewHolder.sex02.setVisibility(8);
        viewHolder.title01 = (TextView) view.findViewById(a.f.weibo_item_text_01);
        viewHolder.title02 = (TextView) view.findViewById(a.f.weibo_item_text_02);
        viewHolder.peopleNum1 = (TextView) view.findViewById(a.f.weibo_item_num_01);
        viewHolder.peopleNum2 = (TextView) view.findViewById(a.f.weibo_item_num_02);
        viewHolder.videoIcon1 = (ImageView) view.findViewById(a.f.weibo_video_icon_01);
        viewHolder.videoIcon2 = (ImageView) view.findViewById(a.f.weibo_video_icon_02);
        viewHolder.footer = view.findViewById(a.f.listview_item_footer);
        return viewHolder;
    }

    class ViewHolder {
        public ImageView authIcon1;
        public ImageView authIcon2;
        View footer;
        ImageView grade01;
        ImageView grade02;
        ImageView image01;
        ImageView image02;
        View item1;
        View item2;
        public TextView peopleNum1;
        public TextView peopleNum2;
        ImageView sex01;
        ImageView sex02;
        TextView title01;
        TextView title02;
        public ImageView videoIcon1;
        public ImageView videoIcon2;

        ViewHolder() {
        }
    }

    public void onClick(View view) {
        int id = view.getId();
        ULog.d(TAG, "onClick: " + id + "; hot_item_01: " + a.f.weibo_item_image_01 + "; hot_item_02: " + a.f.weibo_item_image_02);
        if (id == a.f.weibo_item_image_01 || id == a.f.weibo_item_image_02) {
            Object tag = view.getTag();
            ULog.d(TAG, "obj: " + tag);
            if (tag instanceof v) {
                PlayerActivity.launch(this.activity, (v) tag, this.mFrom);
            }
            boolean z = tag instanceof com.pocketmusic.kshare.requestobjs.a;
            if (tag instanceof o) {
                int i = -1;
                if (DownloadService.isDonwloading) {
                    i = DownloadService.mProgress;
                }
                SimpleLiveRoomActivity.launch(this.activity, (o) tag, i);
            }
        }
    }
}
