package cn.banshenggua.aichang.entry;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import cn.a.a.a;
import com.android.volley.s;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.r;
import java.util.List;
import org.json.JSONObject;

public class RoomsEntryFragment extends BaseFragment {
    private View container;
    /* access modifiers changed from: private */
    public MainFragmentAdapter mAdapter;
    /* access modifiers changed from: private */
    public List<q.a> mEntrys = null;
    private ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        public void onPageSelected(int i) {
            switch (i) {
                case 0:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_1);
                    return;
                case 1:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_2);
                    return;
                case 2:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_3);
                    return;
                case 3:
                    RoomsEntryFragment.this.mRadioGroup.check(a.f.adbtn_4);
                    return;
                default:
                    return;
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
        }

        public void onPageScrollStateChanged(int i) {
        }
    };
    /* access modifiers changed from: private */
    public ViewPager mPager;
    /* access modifiers changed from: private */
    public RadioGroup mRadioGroup = null;
    public q mSdkConfig = null;

    public static RoomsEntryFragment newInstance() {
        return new RoomsEntryFragment();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    private void initData() {
        this.mSdkConfig = new q();
        this.mSdkConfig.a(new r() {
            public void onResponse(JSONObject jSONObject) {
                RoomsEntryFragment.this.mSdkConfig.a(jSONObject);
                RoomsEntryFragment.this.mEntrys = RoomsEntryFragment.this.mSdkConfig.f1745a;
                RoomsEntryFragment.this.mAdapter = new MainFragmentAdapter(RoomsEntryFragment.this.getChildFragmentManager(), RoomsEntryFragment.this.mEntrys);
                RoomsEntryFragment.this.initUI();
            }

            public void onErrorResponse(s sVar) {
                super.onErrorResponse(sVar);
                RoomsEntryFragment.this.mAdapter = new MainFragmentAdapter(RoomsEntryFragment.this.getChildFragmentManager(), RoomsEntryFragment.this.mEntrys);
                RoomsEntryFragment.this.initUI();
            }
        });
        this.mSdkConfig.a();
    }

    /* access modifiers changed from: private */
    public void initUI() {
        this.mPager = (ViewPager) this.container.findViewById(a.f.pager);
        this.mPager.setOffscreenPageLimit(this.mAdapter.getCount());
        this.mPager.setAdapter(this.mAdapter);
        this.mPager.setOnPageChangeListener(this.mOnPageChangeListener);
        this.mRadioGroup = (RadioGroup) this.container.findViewById(a.f.radiogroup_ad_type);
        if (this.mEntrys != null && this.mEntrys.size() >= 4) {
            ((RadioButton) this.container.findViewById(a.f.adbtn_1)).setText(this.mEntrys.get(0).f1747b);
            ((RadioButton) this.container.findViewById(a.f.adbtn_2)).setText(this.mEntrys.get(1).f1747b);
            ((RadioButton) this.container.findViewById(a.f.adbtn_3)).setText(this.mEntrys.get(2).f1747b);
            ((RadioButton) this.container.findViewById(a.f.adbtn_4)).setText(this.mEntrys.get(3).f1747b);
        }
        this.mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == a.f.adbtn_1) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(0);
                }
                if (i == a.f.adbtn_2) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(1);
                }
                if (i == a.f.adbtn_3) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(2);
                }
                if (i == a.f.adbtn_4) {
                    RoomsEntryFragment.this.mPager.setCurrentItem(3);
                }
            }
        });
        this.mRadioGroup.check(a.f.adbtn_1);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.container = (ViewGroup) layoutInflater.inflate(a.g.fra_main_tab, (ViewGroup) null);
        initData();
        return this.container;
    }

    public void onResume() {
        super.onResume();
    }
}
