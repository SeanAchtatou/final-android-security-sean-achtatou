package cn.banshenggua.aichang.entry;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.pocketmusic.kshare.requestobjs.p;
import com.pocketmusic.kshare.requestobjs.q;
import com.pocketmusic.kshare.requestobjs.w;
import java.util.ArrayList;
import java.util.List;

class MainFragmentAdapter extends FragmentPagerAdapter {
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$SDKConfig$MainEntryType = null;
    private static final String[] CONTENT = {"直播房", "精选", "喊麦", "新人"};
    public static final String TAG = "DynamicFragment";
    private HotWeiBoFragment hmWeibosFragment;
    List<q.a> mEntrys = null;
    private ArrayList<Fragment> mFragments = new ArrayList<>(CONTENT.length);
    private HotWeiBoFragment newSongFragment;
    private HotRoomFragment roomsFragment;
    private HotWeiBoFragment weibosFragment;

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$SDKConfig$MainEntryType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$SDKConfig$MainEntryType;
        if (iArr == null) {
            iArr = new int[q.b.values().length];
            try {
                iArr[q.b.Rooms.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[q.b.WeiBos.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[q.b.__NO_SUPPORT.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$SDKConfig$MainEntryType = iArr;
        }
        return iArr;
    }

    public MainFragmentAdapter(FragmentManager fragmentManager, List<q.a> list) {
        super(fragmentManager);
        this.mEntrys = list;
        for (int i = 0; i < CONTENT.length; i++) {
            this.mFragments.add(null);
        }
    }

    public Fragment getItem(int i) {
        Fragment newInstance;
        if (i < 0 || i >= CONTENT.length) {
            return null;
        }
        Fragment fragment = this.mFragments.get(i);
        if (fragment != null) {
            return fragment;
        }
        if (this.mEntrys == null || this.mEntrys.size() < CONTENT.length) {
            switch (i) {
                case 0:
                    if (this.roomsFragment == null) {
                        this.roomsFragment = HotRoomFragment.newInstance();
                    }
                    return this.roomsFragment;
                case 1:
                    if (this.weibosFragment == null) {
                        this.weibosFragment = HotWeiBoFragment.newInstance(w.b.TodaySelected, null);
                    }
                    return this.weibosFragment;
                case 2:
                    if (this.hmWeibosFragment == null) {
                        this.hmWeibosFragment = HotWeiBoFragment.newInstance(w.b.HMSelected, null);
                    }
                    return this.hmWeibosFragment;
                case 3:
                    if (this.newSongFragment == null) {
                        this.newSongFragment = HotWeiBoFragment.newInstance(w.b.NEWPeopleSong, null);
                    }
                    return this.newSongFragment;
                default:
                    return null;
            }
        } else {
            q.a aVar = this.mEntrys.get(i);
            switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$SDKConfig$MainEntryType()[aVar.f1746a.ordinal()]) {
                case 1:
                    newInstance = HotRoomFragment.newInstance(p.a.HotUrl, aVar.c);
                    break;
                case 2:
                    newInstance = HotWeiBoFragment.newInstance(w.b.HotUrl, aVar.c);
                    break;
                default:
                    newInstance = null;
                    break;
            }
            this.mFragments.set(i, newInstance);
            return newInstance;
        }
    }

    public CharSequence getPageTitle(int i) {
        return CONTENT[i % CONTENT.length].toUpperCase();
    }

    public int getCount() {
        return CONTENT.length;
    }
}
