package cn.banshenggua.aichang.room.message;

import com.pocketmusic.kshare.requestobjs.o;
import com.sina.weibo.sdk.constant.WBPageConstants;
import org.json.JSONObject;

public class UserMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType;
    public boolean isMore = false;
    public UserMessageType mType = UserMessageType.User_List;
    public UserList mUsers = null;

    public enum UserMessageType {
        User_List,
        Kick_User,
        Mute_User
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType;
        if (iArr == null) {
            iArr = new int[UserMessageType.values().length];
            try {
                iArr[UserMessageType.Kick_User.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[UserMessageType.Mute_User.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[UserMessageType.User_List.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType = iArr;
        }
        return iArr;
    }

    public UserMessage(UserMessageType userMessageType, o oVar) {
        super(oVar);
        this.mType = userMessageType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType()[userMessageType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_GetUsers;
                break;
            case 2:
                this.mKey = MessageKey.Message_KickUser;
                break;
            case 3:
                this.mKey = MessageKey.Message_MuteUser;
                break;
        }
        this.isMore = false;
    }

    public UserMessage(UserMessageType userMessageType, o oVar, boolean z) {
        super(oVar);
        this.mType = userMessageType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType()[userMessageType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_GetUsers;
                break;
            case 2:
                this.mKey = MessageKey.Message_KickUser;
                break;
            case 3:
                this.mKey = MessageKey.Message_MuteUser;
                break;
        }
        this.isMore = z;
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType()[this.mType.ordinal()]) {
            case 1:
                if (this.mUsers != null) {
                    socketMessage.pushCommend(WBPageConstants.ParamKey.PAGE, this.mUsers.page_next);
                    socketMessage.pushCommend("limit", this.mUsers.limit);
                    break;
                }
                break;
        }
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserMessage$UserMessageType()[this.mType.ordinal()]) {
            case 1:
                if (jSONObject.has("users")) {
                    if (this.mUsers == null) {
                        this.mUsers = null;
                    }
                    this.mUsers = new UserList();
                    this.mUsers.parseUsers(jSONObject.optJSONObject("users"));
                    return;
                }
                return;
            default:
                return;
        }
    }
}
