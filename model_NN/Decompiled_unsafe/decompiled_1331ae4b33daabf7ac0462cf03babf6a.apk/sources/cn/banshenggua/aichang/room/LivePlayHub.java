package cn.banshenggua.aichang.room;

import java.util.ArrayList;
import java.util.HashMap;

public class LivePlayHub {
    private static LivePlayHub mInstance = null;
    private HashMap<String, HubClientObject> mRunClients = new HashMap<>();

    public enum StreamStat {
        Running,
        Stop,
        Pause
    }

    public static LivePlayHub getInstance() {
        if (mInstance == null) {
            mInstance = new LivePlayHub();
        }
        return mInstance;
    }

    public class HubStreamObject {
        public int mCid;
        public int mSid;
        public StreamStat mStat;

        public HubStreamObject() {
        }
    }

    public class HubClientObject {
        private static final int MAX_STREMS = 4;
        public int mCid;
        public ArrayList<HubStreamObject> mStreams = new ArrayList<>(4);

        public HubClientObject() {
        }
    }

    public class HubStreamID {
        public int mCid;
        public int mSid;

        public HubStreamID(int i, int i2) {
            this.mCid = i;
            this.mSid = i2;
        }
    }

    public HubStreamID getHubStreamID(String str) {
        return null;
    }

    public void removeHubStream(String str) {
    }
}
