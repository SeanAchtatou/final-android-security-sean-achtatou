package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.KURL;
import cn.banshenggua.aichang.room.SocketRouter;
import cn.banshenggua.aichang.utils.ULog;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.http.client.utils.URLEncodedUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class SocketMessage {
    public static final String MSG_ACK_SDAT = "sdat";
    public static final String MSG_ERROR_KEY = "error";
    public static final String MSG_FINGER_KEY = "tag";
    public static final String MSG_RESULE_KEY = "result";
    public long mBeginTime = 0;
    public MessageCommend mCommend;
    public MessageError mError;
    public String mFinger;
    public MessageResult mResult;
    public String mSdat;

    public void pushCommend(String str, int i) {
        pushCommend(str, new StringBuilder().append(i).toString());
    }

    public void pushCommend(String str, String str2) {
        if (this.mCommend == null) {
            this.mCommend = new MessageCommend();
        }
        this.mCommend.parameter.put(str, str2);
    }

    public void parseError(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.mError = null;
            this.mError = new MessageError();
            try {
                this.mError.setError(Integer.parseInt(str));
            } catch (Exception e) {
                this.mError.setErrorString(str);
            }
        }
    }

    public void parseResult(String str) {
        if (!TextUtils.isEmpty(str)) {
            if (this.mResult == null) {
                this.mResult = new MessageResult();
            }
            this.mResult.mResult = str;
            try {
                JSONObject jSONObject = new JSONObject(str);
                this.mFinger = jSONObject.optString(MSG_FINGER_KEY, null);
                this.mSdat = jSONObject.optString(MSG_ACK_SDAT, null);
                parseError(jSONObject.optString(MSG_ERROR_KEY));
                this.mResult.mParseResult = LiveMessage.parseResult(jSONObject);
                ULog.d(SocketRouter.TAG, "parseResult: " + this.mResult.mParseResult);
            } catch (JSONException e) {
                e.printStackTrace();
                parseError(MSG_ERROR_KEY);
                this.mResult.mParseResult = null;
            }
        }
    }

    public String getSocketMessageValue() {
        if (this.mResult != null) {
            return this.mResult.getResultValue();
        }
        return null;
    }

    public static SocketMessage createMessage(String str, String str2) {
        SocketMessage socketMessage = new SocketMessage();
        socketMessage.parseResult(str);
        socketMessage.parseError(str2);
        return socketMessage;
    }

    public static class MessageCommend {
        public static final String Encoding = "UTF-8";
        public MessageKey mType = MessageKey.Message_Null;
        public Map<String, String> parameter = new LinkedHashMap();

        public String messageEncode() {
            if (this.parameter.size() > 0) {
                return URLEncodedUtils.format(KURL.getParamsList(this.parameter), "UTF-8");
            }
            return "1";
        }
    }

    public static class MessageResult {
        public LiveMessage mParseResult = null;
        public String mResult = null;

        public String getResultValue() {
            return this.mResult;
        }
    }

    public static class MessageError implements Serializable {
        private static final long serialVersionUID = 1;
        private int mError = 0;
        private String mErrorString = "";

        public int getError() {
            return this.mError;
        }

        public void setError(int i) {
            this.mError = i;
        }

        public String getErrorString() {
            if (TextUtils.isEmpty(this.mErrorString)) {
                return ContextError.getErrorString(this.mError);
            }
            return this.mErrorString;
        }

        public void setErrorString(String str) {
            this.mErrorString = str;
        }
    }
}
