package cn.banshenggua.aichang.room;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import cn.a.a.a;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.entry.ArrayListAdapter;
import cn.banshenggua.aichang.room.message.ChatMessage;
import cn.banshenggua.aichang.room.message.ClubMessage;
import cn.banshenggua.aichang.room.message.GiftMessage;
import cn.banshenggua.aichang.room.message.LiveMessage;
import cn.banshenggua.aichang.room.message.MessageKey;
import cn.banshenggua.aichang.room.message.MicMessage;
import cn.banshenggua.aichang.room.message.SimpleMessage;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ImageLoaderUtil;
import cn.banshenggua.aichang.utils.ImageUtil;
import cn.banshenggua.aichang.utils.KShareUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.d.a.b.c;
import com.d.a.b.d;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.k;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.s;

public class RoomMessgeAdapter extends ArrayListAdapter<LiveMessage> implements View.OnClickListener, AdapterView.OnItemClickListener {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey;
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason;
    private static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;
    private FragmentActivity context;
    private Handler handle = new Handler();
    private d loader = d.a();
    private int mFlag = 0;
    private ITEM_TYPE mItemType;
    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            if (view.getTag() != null && (view.getTag() instanceof User)) {
                KShareUtil.mCurrentNotifyKey = k.b.ROOM_HEAD;
                if (!(RoomMessgeAdapter.this.mContext instanceof SimpleLiveRoomActivity) || KShareUtil.processAnonymous((SimpleLiveRoomActivity) RoomMessgeAdapter.this.mContext, null, null)) {
                }
            }
        }
    };
    public o.a mRoomClass = o.a.Normal;
    private String mRoomUid = "";
    private int messageDefault = -13355202;
    private int messagesys = -5872731;
    private int nickDefault = -8816263;
    private int nicksys = -5872731;
    private OnRoomMessageClickListener onListener;
    private c options = ImageUtil.getOvalDefaultOption();
    private c optionsGift = ImageUtil.getDefaultOptionForGift();
    private c optionsLevel = ImageUtil.getDefaultLevelOption();
    private c optionsNormal = ImageUtil.getDefaultOption();
    private o room;

    public enum ITEM_TYPE {
        ALL,
        SEC,
        GIFT,
        NORMAL
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType;
        if (iArr == null) {
            iArr = new int[ClubMessage.ClubMessageType.values().length];
            try {
                iArr[ClubMessage.ClubMessageType.APPLY.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.APPLY_AGREE.ordinal()] = 7;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.APPLY_DISAGREE.ordinal()] = 6;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.APPLY_RESULT.ordinal()] = 5;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.DISSOLVE.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.JOIN.ordinal()] = 1;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.LEAVE.ordinal()] = 2;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.LEAVE_By_Admin.ordinal()] = 9;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.LEAVE_By_User.ordinal()] = 8;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[ClubMessage.ClubMessageType.NO_SURPORT.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey;
        if (iArr == null) {
            iArr = new int[MessageKey.values().length];
            try {
                iArr[MessageKey.MessageCenter_Private.ordinal()] = 32;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MessageKey.Message_ACK.ordinal()] = 33;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MessageKey.Message_ALL.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[MessageKey.Message_AdjustMic.ordinal()] = 24;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[MessageKey.Message_CancelMic.ordinal()] = 14;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[MessageKey.Message_ChangeBanzou.ordinal()] = 25;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[MessageKey.Message_ChangeMic.ordinal()] = 19;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[MessageKey.Message_CloseMic.ordinal()] = 18;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[MessageKey.Message_FAMILY_DISSOLVE.ordinal()] = 28;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[MessageKey.Message_Family.ordinal()] = 27;
            } catch (NoSuchFieldError e10) {
            }
            try {
                iArr[MessageKey.Message_GetMics.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                iArr[MessageKey.Message_GetUsers.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                iArr[MessageKey.Message_Gift.ordinal()] = 9;
            } catch (NoSuchFieldError e13) {
            }
            try {
                iArr[MessageKey.Message_GlobalGift.ordinal()] = 44;
            } catch (NoSuchFieldError e14) {
            }
            try {
                iArr[MessageKey.Message_HeartBeat.ordinal()] = 12;
            } catch (NoSuchFieldError e15) {
            }
            try {
                iArr[MessageKey.Message_Join.ordinal()] = 5;
            } catch (NoSuchFieldError e16) {
            }
            try {
                iArr[MessageKey.Message_KickUser.ordinal()] = 15;
            } catch (NoSuchFieldError e17) {
            }
            try {
                iArr[MessageKey.Message_Leave.ordinal()] = 6;
            } catch (NoSuchFieldError e18) {
            }
            try {
                iArr[MessageKey.Message_Login.ordinal()] = 3;
            } catch (NoSuchFieldError e19) {
            }
            try {
                iArr[MessageKey.Message_Logout.ordinal()] = 4;
            } catch (NoSuchFieldError e20) {
            }
            try {
                iArr[MessageKey.Message_Media.ordinal()] = 20;
            } catch (NoSuchFieldError e21) {
            }
            try {
                iArr[MessageKey.Message_MultiCancelMic.ordinal()] = 43;
            } catch (NoSuchFieldError e22) {
            }
            try {
                iArr[MessageKey.Message_MultiChangeMic.ordinal()] = 40;
            } catch (NoSuchFieldError e23) {
            }
            try {
                iArr[MessageKey.Message_MultiCloseMic.ordinal()] = 37;
            } catch (NoSuchFieldError e24) {
            }
            try {
                iArr[MessageKey.Message_MultiConfirmMic.ordinal()] = 38;
            } catch (NoSuchFieldError e25) {
            }
            try {
                iArr[MessageKey.Message_MultiDelMic.ordinal()] = 35;
            } catch (NoSuchFieldError e26) {
            }
            try {
                iArr[MessageKey.Message_MultiInviteMic.ordinal()] = 36;
            } catch (NoSuchFieldError e27) {
            }
            try {
                iArr[MessageKey.Message_MultiMedia.ordinal()] = 39;
            } catch (NoSuchFieldError e28) {
            }
            try {
                iArr[MessageKey.Message_MultiOpenMic.ordinal()] = 41;
            } catch (NoSuchFieldError e29) {
            }
            try {
                iArr[MessageKey.Message_MultiReqMic.ordinal()] = 34;
            } catch (NoSuchFieldError e30) {
            }
            try {
                iArr[MessageKey.Message_MuteRoom.ordinal()] = 21;
            } catch (NoSuchFieldError e31) {
            }
            try {
                iArr[MessageKey.Message_MuteUser.ordinal()] = 16;
            } catch (NoSuchFieldError e32) {
            }
            try {
                iArr[MessageKey.Message_Null.ordinal()] = 1;
            } catch (NoSuchFieldError e33) {
            }
            try {
                iArr[MessageKey.Message_OpenMic.ordinal()] = 17;
            } catch (NoSuchFieldError e34) {
            }
            try {
                iArr[MessageKey.Message_ReqMic.ordinal()] = 13;
            } catch (NoSuchFieldError e35) {
            }
            try {
                iArr[MessageKey.Message_RoomMod.ordinal()] = 29;
            } catch (NoSuchFieldError e36) {
            }
            try {
                iArr[MessageKey.Message_RoomParam.ordinal()] = 31;
            } catch (NoSuchFieldError e37) {
            }
            try {
                iArr[MessageKey.Message_RoomUpdate.ordinal()] = 26;
            } catch (NoSuchFieldError e38) {
            }
            try {
                iArr[MessageKey.Message_RtmpProp.ordinal()] = 42;
            } catch (NoSuchFieldError e39) {
            }
            try {
                iArr[MessageKey.Message_STalk.ordinal()] = 8;
            } catch (NoSuchFieldError e40) {
            }
            try {
                iArr[MessageKey.Message_ServerError.ordinal()] = 22;
            } catch (NoSuchFieldError e41) {
            }
            try {
                iArr[MessageKey.Message_ServerSys.ordinal()] = 23;
            } catch (NoSuchFieldError e42) {
            }
            try {
                iArr[MessageKey.Message_Talk.ordinal()] = 7;
            } catch (NoSuchFieldError e43) {
            }
            try {
                iArr[MessageKey.Message_Toast.ordinal()] = 30;
            } catch (NoSuchFieldError e44) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason;
        if (iArr == null) {
            iArr = new int[MicMessage.SwitchMicReason.values().length];
            try {
                iArr[MicMessage.SwitchMicReason.ADMIN_CANCEL_MIC_QUEUE.ordinal()] = 10;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.ADMIN_CANEL_MIC.ordinal()] = 4;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.NO_USER_ON_MIC.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_HB_DIE.ordinal()] = 6;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_NO_HB.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.SERVER_CANCEL_USERLEAVE.ordinal()] = 7;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.SERVER_TIME_END.ordinal()] = 8;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.USER_CANCEL_MIC.ordinal()] = 2;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.USER_CANCEL_MIC_QUEUE.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                iArr[MicMessage.SwitchMicReason.USER_CANCEL_WEAK_NET.ordinal()] = 3;
            } catch (NoSuchFieldError e10) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType() {
        int[] iArr = $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType;
        if (iArr == null) {
            iArr = new int[e.c.values().length];
            try {
                iArr[e.c.GuiBin.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[e.c.HanHua.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[e.c.Normal.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[e.c.SaQian.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            $SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType = iArr;
        }
        return iArr;
    }

    public RoomMessgeAdapter(FragmentActivity fragmentActivity) {
        super(fragmentActivity);
        this.context = fragmentActivity;
    }

    public void setRoomClass(o.a aVar) {
        if (aVar != null) {
            this.mRoomClass = aVar;
            this.room.R = aVar;
        }
    }

    public RoomMessgeAdapter(FragmentActivity fragmentActivity, int i, o oVar) {
        super(fragmentActivity);
        this.mFlag = i;
        switch (this.mFlag) {
            case 0:
                this.mItemType = ITEM_TYPE.ALL;
                break;
            case 1:
                this.mItemType = ITEM_TYPE.SEC;
                break;
            case 2:
                this.mItemType = ITEM_TYPE.GIFT;
                break;
            default:
                this.mItemType = ITEM_TYPE.NORMAL;
                break;
        }
        this.context = fragmentActivity;
        this.room = oVar;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (view == null || view.getTag() == null) {
            view = this.mInflater.inflate(a.g.item_room_message, (ViewGroup) null);
            ViewHolder createHolder = createHolder(view);
            view.setTag(createHolder);
            viewHolder = createHolder;
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        initView(viewHolder);
        viewHolder.mUserHead.setTag(Integer.valueOf(i));
        viewHolder.mUserHead.setOnClickListener(this);
        setMessage((LiveMessage) getItem(i), viewHolder);
        return view;
    }

    private void showUserIcon(ViewHolder viewHolder, User user) {
        if (user == null) {
            return;
        }
        if (TextUtils.isEmpty(user.mVip)) {
            showUserIcon(viewHolder, user.mUid);
        } else if (user.mVip.equalsIgnoreCase("vip")) {
            viewHolder.mVipIcon.setVisibility(0);
            viewHolder.mVipIcon.setImageResource(a.e.vip_icon);
        }
    }

    private void showUserIcon(ViewHolder viewHolder, com.pocketmusic.kshare.requestobjs.a aVar) {
        if (aVar == null) {
            return;
        }
        if (TextUtils.isEmpty(aVar.ag)) {
            showUserIcon(viewHolder, aVar.f1685b);
        } else if (aVar.ag.equalsIgnoreCase("vip")) {
            viewHolder.mVipIcon.setVisibility(0);
            viewHolder.mVipIcon.setImageResource(a.e.vip_icon);
        }
    }

    private void showUserIcon(ViewHolder viewHolder, String str) {
        if (SimpleLiveRoomActivity.isVipUser(str)) {
            viewHolder.mVipIcon.setImageResource(a.e.room_indentity_online);
            viewHolder.mVipIcon.setVisibility(0);
        }
    }

    private void showAuthIcon(ViewHolder viewHolder, User user) {
        if (user == null) {
            return;
        }
        if (!TextUtils.isEmpty(user.auth_info)) {
            ImageLoaderUtil.displayImageBg(viewHolder.authIcon, user.authIcon, this.optionsLevel);
            viewHolder.authIcon.setVisibility(0);
            return;
        }
        viewHolder.authIcon.setVisibility(8);
    }

    private void showAuthIcon(ViewHolder viewHolder, com.pocketmusic.kshare.requestobjs.a aVar) {
        if (aVar == null) {
            return;
        }
        if (!TextUtils.isEmpty(aVar.ab)) {
            ImageLoaderUtil.displayImageBg(viewHolder.authIcon, aVar.ac, this.optionsLevel);
            viewHolder.authIcon.setVisibility(0);
            return;
        }
        viewHolder.authIcon.setVisibility(8);
    }

    private boolean isAdminUser(User user) {
        if (user == null || TextUtils.isEmpty(this.mRoomUid) || !this.mRoomUid.equalsIgnoreCase(user.mUid)) {
            return false;
        }
        return true;
    }

    private boolean isMe(User user) {
        if (user == null || !user.mUid.equalsIgnoreCase(Session.getCurrentAccount().f1685b)) {
            return false;
        }
        return true;
    }

    private void initSysText(boolean z, ViewHolder viewHolder) {
        if (z) {
            viewHolder.mNickname.setTextColor(this.nicksys);
            viewHolder.mMessage.setTextColor(this.messagesys);
            return;
        }
        viewHolder.mNickname.setTextColor(this.nickDefault);
        viewHolder.mMessage.setTextColor(this.messageDefault);
    }

    private void setMessage(LiveMessage liveMessage, ViewHolder viewHolder) {
        String fullName;
        String format;
        String fullName2;
        String string;
        String str;
        if (liveMessage != null) {
            viewHolder.mUserHead.setOnClickListener(this.mOnClickListener);
            viewHolder.mUserLevelAnima.setVisibility(8);
            viewHolder.mUserLevelAnima.setImageDrawable(null);
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[liveMessage.mKey.ordinal()]) {
                case 5:
                    if (liveMessage.mUser != null) {
                        showUserIcon(viewHolder, liveMessage.mUser);
                        if (!TextUtils.isEmpty(liveMessage.mUser.getFace(User.FACE_SIZE.SIM))) {
                            this.loader.a(liveMessage.mUser.getFace(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.options);
                        }
                        if (isMe(liveMessage.mUser)) {
                            viewHolder.mNickname.setText(this.mContext.getString(a.h.room_me));
                        } else {
                            viewHolder.mNickname.setText(liveMessage.mUser.getFullName());
                        }
                        if (isAdminUser(liveMessage.mUser)) {
                            viewHolder.mMessage.setText("管理员进入了房间");
                        } else if (liveMessage.mUser.mLevel <= 0 || TextUtils.isEmpty(liveMessage.mUser.mLevelName)) {
                            viewHolder.mMessage.setText("已进入了房间");
                        } else {
                            viewHolder.mMessage.setText(String.valueOf(liveMessage.mUser.mLevelName) + " 已进入了房间");
                        }
                        ULog.d("luolei", "level img: " + liveMessage.mUser.mLevelImage + "; level: " + liveMessage.mUser.mLevel);
                        if (liveMessage.mUser.mLevel > 0) {
                            viewHolder.mLevelImage.setVisibility(0);
                            this.loader.a(liveMessage.mUser.mLevelImage, viewHolder.mLevelImage, this.optionsLevel);
                        } else {
                            viewHolder.mLevelImage.setVisibility(8);
                        }
                        showAuthIcon(viewHolder, liveMessage.mUser);
                        viewHolder.mUserHead.setTag(liveMessage.mUser);
                        return;
                    }
                    return;
                case 6:
                    if (liveMessage.mUser != null) {
                        this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                        viewHolder.mNickname.setText(this.mContext.getString(a.h.room_sys_message_nick));
                        initSysText(true, viewHolder);
                        if (isAdminUser(liveMessage.mUser)) {
                            viewHolder.mMessage.setText("管理员离开了房间");
                        } else {
                            viewHolder.mMessage.setText(String.valueOf(liveMessage.mUser.getFullName()) + " 离开了房间");
                        }
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 7:
                case 8:
                    if (liveMessage instanceof ChatMessage) {
                        ChatMessage chatMessage = (ChatMessage) liveMessage;
                        String.format("%s", chatMessage.mMessage);
                        if (chatMessage.mFrom != null) {
                            showUserIcon(viewHolder, chatMessage.mFrom);
                        }
                        if (chatMessage.mSource == ChatMessage.ChatSource.In_Hall) {
                            initSysText(true, viewHolder);
                            viewHolder.mMessage.setText(String.format("%s 发来私信：\n%s ", chatMessage.mFrom.getFullName(), chatMessage.mMessage));
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            viewHolder.mNickname.setText(this.mContext.getString(a.h.room_sys_message_nick));
                            viewHolder.mUserHead.setTag(null);
                            return;
                        }
                        viewHolder.mTextEnd.setVisibility(0);
                        viewHolder.mTextMid.setVisibility(0);
                        viewHolder.mTextMid.setText(a.h.room_message_nickname_mid);
                        viewHolder.mNicknameRight.setVisibility(0);
                        if (chatMessage.mFrom != null) {
                            this.loader.a(chatMessage.mFrom.getFace(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.options);
                            if (isMe(chatMessage.mFrom)) {
                                string = this.mContext.getString(a.h.room_me);
                            } else {
                                string = chatMessage.mFrom.getFullName();
                            }
                            String.format("%s", chatMessage.mMessage);
                            showAuthIcon(viewHolder, chatMessage.mFrom);
                            showUserIcon(viewHolder, chatMessage.mFrom);
                        } else {
                            this.loader.a(Session.getCurrentAccount().k, viewHolder.mUserHead, this.options);
                            string = this.mContext.getString(a.h.room_me);
                            showUserIcon(viewHolder, Session.getCurrentAccount());
                            showAuthIcon(viewHolder, Session.getCurrentAccount());
                        }
                        if (chatMessage.mTo != null) {
                            str = chatMessage.mTo.getFullName();
                            if (isMe(chatMessage.mTo)) {
                                str = this.mContext.getString(a.h.room_me);
                            }
                        } else {
                            viewHolder.mTextMid.setVisibility(8);
                            viewHolder.mNicknameRight.setVisibility(8);
                            viewHolder.mTextEnd.setVisibility(8);
                            str = "";
                        }
                        String format2 = String.format("%s", chatMessage.mMessage);
                        if (liveMessage.mKey == MessageKey.Message_Talk) {
                            viewHolder.mTextEnd.setText(a.h.room_message_nickname_end);
                        } else {
                            viewHolder.mTextEnd.setText(a.h.room_message_nickname_end_sec);
                        }
                        viewHolder.mNicknameRight.setText(str);
                        viewHolder.mMessage.setText(format2);
                        viewHolder.mNickname.setText(string);
                        viewHolder.mUserHead.setTag(chatMessage.mFrom);
                        return;
                    }
                    return;
                case 9:
                    if (liveMessage instanceof GiftMessage) {
                        GiftMessage giftMessage = (GiftMessage) liveMessage;
                        String str2 = giftMessage.uid;
                        String format3 = String.format("送礼 %s: %s", giftMessage.toUid, giftMessage.gift);
                        e gift = giftMessage.getGift();
                        if (giftMessage.mFrom != null) {
                            showUserIcon(viewHolder, giftMessage.mFrom);
                            showAuthIcon(viewHolder, giftMessage.mFrom);
                            if (isMe(giftMessage.mFrom)) {
                                str2 = this.mContext.getResources().getString(a.h.room_me);
                            } else {
                                str2 = giftMessage.mFrom.getFullName();
                            }
                            this.loader.a(giftMessage.mFrom.getFace(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.options);
                            String str3 = "送给";
                            if (giftMessage.mTo != null) {
                                if (isMe(giftMessage.mTo)) {
                                    str3 = String.format("%s %s", str3, this.mContext.getResources().getString(a.h.room_me));
                                } else {
                                    str3 = String.format("%s %s", str3, giftMessage.mTo.getFullName());
                                }
                            }
                            if (gift != null) {
                                switch ($SWITCH_TABLE$com$pocketmusic$kshare$requestobjs$Gift$GiftType()[gift.O.ordinal()]) {
                                    case 2:
                                        viewHolder.mRightImage.setVisibility(0);
                                        this.loader.a(gift.e, viewHolder.mRightImage, this.optionsGift);
                                        format3 = giftMessage.mText;
                                        break;
                                    case 3:
                                        viewHolder.mRightImage.setVisibility(8);
                                        viewHolder.mNicknameRight.setVisibility(0);
                                        viewHolder.mNicknameRight.setText("  发出一个喊话");
                                        format3 = giftMessage.mText;
                                        break;
                                    case 4:
                                        viewHolder.mRightImage.setVisibility(8);
                                        format3 = giftMessage.mText;
                                        break;
                                    default:
                                        viewHolder.mRightImage.setVisibility(0);
                                        if (this.mItemType == ITEM_TYPE.ALL) {
                                            format3 = String.format("%s %s", format3, this.mContext.getResources().getString(a.h.gift_tail));
                                        } else {
                                            format3 = String.format("%s %s %s %s %s %s", format3, this.mContext.getResources().getString(a.h.gift_tail), giftMessage.mText, this.mContext.getResources().getString(a.h.gift_exp_begin), giftMessage.content, this.mContext.getResources().getString(a.h.gift_exp_end));
                                        }
                                        this.loader.a(gift.e, viewHolder.mRightImage, this.optionsGift);
                                        break;
                                }
                            }
                        }
                        viewHolder.mMessage.setText(format3);
                        viewHolder.mNickname.setText(str2);
                        viewHolder.mUserHead.setTag(giftMessage.mFrom);
                        return;
                    }
                    return;
                case 10:
                case 11:
                case 12:
                case 16:
                case 17:
                case 18:
                case 20:
                case 21:
                case 22:
                case 25:
                case 26:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 37:
                case 38:
                default:
                    return;
                case 13:
                    if (liveMessage instanceof MicMessage) {
                        MicMessage micMessage = (MicMessage) liveMessage;
                        String str4 = "";
                        String str5 = "排麦";
                        if (micMessage.mUser != null) {
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            str4 = this.mContext.getString(a.h.room_sys_message_nick);
                            initSysText(true, viewHolder);
                            if (this.mRoomClass != o.a.Show) {
                                if (isMe(micMessage.mUser)) {
                                    str5 = String.format("%s 点了", this.mContext.getResources().getString(a.h.room_me));
                                } else {
                                    str5 = String.format("%s 点了", micMessage.mUser.getFullName());
                                }
                                if (micMessage.mBanzou != null) {
                                    str5 = String.format("%s《%s》", str5, micMessage.mBanzou.name);
                                }
                            } else if (isMe(micMessage.mUser)) {
                                str5 = String.format("%s 开始排麦", this.mContext.getResources().getString(a.h.room_me));
                            } else {
                                str5 = String.format("%s 开始排麦", micMessage.mUser.getFullName());
                            }
                        }
                        viewHolder.mMessage.setText(str5);
                        viewHolder.mNickname.setText(str4);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 14:
                    if (liveMessage instanceof MicMessage) {
                        MicMessage micMessage2 = (MicMessage) liveMessage;
                        String str6 = "";
                        String str7 = "下麦";
                        if (micMessage2.mUser != null) {
                            if (isMe(micMessage2.mUser)) {
                                fullName2 = this.mContext.getResources().getString(a.h.room_me);
                            } else {
                                fullName2 = micMessage2.mUser.getFullName();
                            }
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            str6 = this.mContext.getString(a.h.room_sys_message_nick);
                            initSysText(true, viewHolder);
                            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason()[micMessage2.mReason.ordinal()]) {
                                case 1:
                                    if (this.mRoomClass != o.a.Show) {
                                        str7 = String.format("%s结束了自己的演唱", fullName2);
                                        break;
                                    } else {
                                        str7 = String.format("%s下麦了", fullName2);
                                        break;
                                    }
                                case 2:
                                    if (this.mRoomClass != o.a.Show) {
                                        str7 = String.format("%s结束了自己的演唱", fullName2);
                                        break;
                                    } else {
                                        str7 = String.format("%s下麦了", fullName2);
                                        break;
                                    }
                                case 3:
                                case 7:
                                    if (this.mRoomClass != o.a.Show) {
                                        if (!isMe(micMessage2.mUser)) {
                                            str7 = String.format("演唱者连接异常，已结束演唱", new Object[0]);
                                            break;
                                        } else {
                                            str7 = String.format("网络不好，已结束你的演唱", new Object[0]);
                                            break;
                                        }
                                    } else if (!isMe(micMessage2.mUser)) {
                                        str7 = String.format("演唱者连接异常，已下麦", new Object[0]);
                                        break;
                                    } else {
                                        str7 = String.format("网络不好，你已下麦", new Object[0]);
                                        break;
                                    }
                                case 4:
                                    if (this.mRoomClass != o.a.Show) {
                                        str7 = String.format("管理员结束了%s的演唱", fullName2);
                                        break;
                                    } else {
                                        str7 = String.format("管理员让%s下麦了", fullName2);
                                        break;
                                    }
                                case 5:
                                    str7 = String.format("%s上麦失败", fullName2);
                                    break;
                                case 6:
                                    if (this.mRoomClass != o.a.Show) {
                                        str7 = String.format("表演者连接异常，已结束演唱", fullName2);
                                        break;
                                    } else {
                                        str7 = String.format("表演者连接异常，已下麦", fullName2);
                                        break;
                                    }
                                case 8:
                                    if (this.mRoomClass != o.a.Show) {
                                        str7 = String.format("%s完成了演唱", fullName2);
                                        break;
                                    } else {
                                        str7 = String.format("%s已下麦", fullName2);
                                        break;
                                    }
                                case 9:
                                    str7 = String.format("%s取消了自己的排麦", fullName2);
                                    break;
                                case 10:
                                    str7 = String.format("管理员取消了%s的排麦", fullName2);
                                    break;
                                default:
                                    str7 = String.format("%s取消了自己的排麦", fullName2);
                                    break;
                            }
                        }
                        viewHolder.mMessage.setText(str7);
                        viewHolder.mNickname.setText(str6);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 15:
                    if (liveMessage instanceof SimpleMessage) {
                        SimpleMessage simpleMessage = (SimpleMessage) liveMessage;
                        String str8 = "";
                        String str9 = "";
                        if (simpleMessage.mUser != null) {
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            str8 = this.mContext.getString(a.h.room_sys_message_nick);
                            initSysText(true, viewHolder);
                            if (isMe(simpleMessage.mUser)) {
                                str9 = String.format("%s 被管理员踢出房间", this.mContext.getResources().getString(a.h.room_me));
                            } else {
                                str9 = String.format("%s 被管理员踢出房间", simpleMessage.mUser.getFullName());
                            }
                        }
                        viewHolder.mMessage.setText(str9);
                        viewHolder.mNickname.setText(str8);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 19:
                case 40:
                    if (liveMessage instanceof MicMessage) {
                        MicMessage micMessage3 = (MicMessage) liveMessage;
                        String str10 = "";
                        String str11 = "";
                        if (micMessage3.mDownUser != null) {
                            if (isMe(micMessage3.mDownUser)) {
                                fullName = this.mContext.getResources().getString(a.h.room_me);
                            } else {
                                fullName = micMessage3.mDownUser.getFullName();
                            }
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            String string2 = this.mContext.getString(a.h.room_sys_message_nick);
                            initSysText(true, viewHolder);
                            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MicMessage$SwitchMicReason()[micMessage3.mReason.ordinal()]) {
                                case 1:
                                    if (this.mRoomClass != o.a.Show) {
                                        format = String.format("%s结束了自己的演唱", fullName);
                                        break;
                                    } else {
                                        format = String.format("%s下麦了", fullName);
                                        break;
                                    }
                                case 2:
                                    if (this.mRoomClass != o.a.Show) {
                                        format = String.format("%s结束了自己的演唱", fullName);
                                        break;
                                    } else {
                                        format = String.format("%s下麦了", fullName);
                                        break;
                                    }
                                case 3:
                                case 7:
                                    if (this.mRoomClass != o.a.Show) {
                                        if (!isMe(micMessage3.mUser)) {
                                            format = String.format("表演者连接异常，已结束演唱", new Object[0]);
                                            break;
                                        } else {
                                            format = String.format("网络不好，已结束你的演唱", new Object[0]);
                                            break;
                                        }
                                    } else if (!isMe(micMessage3.mUser)) {
                                        format = String.format("表演者连接异常，已下麦", new Object[0]);
                                        break;
                                    } else {
                                        format = String.format("网络不好，你已下麦", new Object[0]);
                                        break;
                                    }
                                case 4:
                                    if (this.mRoomClass != o.a.Show) {
                                        format = String.format("管理员结束了%s的演唱", fullName);
                                        break;
                                    } else {
                                        format = String.format("管理员让%s下麦了", fullName);
                                        break;
                                    }
                                case 5:
                                    format = String.format("%s 上麦失败", fullName);
                                    break;
                                case 6:
                                    if (this.mRoomClass != o.a.Show) {
                                        format = String.format("表演者连接异常，已结束演唱", fullName);
                                        break;
                                    } else {
                                        format = String.format("表演者连接异常，已下麦", fullName);
                                        break;
                                    }
                                case 8:
                                    if (this.mRoomClass != o.a.Show) {
                                        format = String.format("%s完成了演唱", fullName);
                                        break;
                                    } else {
                                        format = String.format("%s已下麦", fullName);
                                        break;
                                    }
                                case 9:
                                    format = String.format("%s取消了自己的排麦", fullName);
                                    break;
                                case 10:
                                    format = String.format("管理员取消了%s的排麦", fullName);
                                    break;
                                default:
                                    format = String.format("%s取消了自己的排麦", fullName);
                                    break;
                            }
                            if (!TextUtils.isEmpty(micMessage3.mDownUser.mExperienceMsg)) {
                                String str12 = string2;
                                str10 = String.format("%s, %s", format, micMessage3.mDownUser.mExperienceMsg);
                                str11 = str12;
                            } else {
                                String str13 = string2;
                                str10 = format;
                                str11 = str13;
                            }
                        }
                        viewHolder.mMessage.setText(str10);
                        viewHolder.mNickname.setText(str11);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 23:
                    if (liveMessage instanceof SimpleMessage) {
                        SimpleMessage simpleMessage2 = (SimpleMessage) liveMessage;
                        if (simpleMessage2.mFlag != LiveMessage.Message_Flag.Message_Vehicle || simpleMessage2.mUser == null || simpleMessage2.mUser.isAnonymous()) {
                            this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                            String string3 = this.mContext.getString(a.h.room_sys_message_nick);
                            if (!TextUtils.isEmpty(simpleMessage2.mMsgType)) {
                                string3 = simpleMessage2.mMsgType;
                            }
                            initSysText(true, viewHolder);
                            viewHolder.mMessage.setText(simpleMessage2.mMsg);
                            viewHolder.mNickname.setText(string3);
                            viewHolder.mUserHead.setTag(null);
                            return;
                        }
                        showUserIcon(viewHolder, simpleMessage2.mUser);
                        if (!TextUtils.isEmpty(simpleMessage2.mUser.getFace(User.FACE_SIZE.SIM))) {
                            this.loader.a(simpleMessage2.mUser.getFace(User.FACE_SIZE.SIM), viewHolder.mUserHead, this.options);
                        }
                        if (simpleMessage2.mUser.mLevel > 0) {
                            viewHolder.mLevelImage.setVisibility(0);
                            this.loader.a(simpleMessage2.mUser.mLevelImage, viewHolder.mLevelImage, this.optionsLevel);
                        } else {
                            viewHolder.mLevelImage.setVisibility(8);
                        }
                        showAuthIcon(viewHolder, simpleMessage2.mUser);
                        viewHolder.mUserHead.setTag(simpleMessage2.mUser);
                        String fullName3 = simpleMessage2.mUser.getFullName();
                        viewHolder.mMessage.setText(simpleMessage2.mMsg);
                        viewHolder.mNickname.setText(fullName3);
                        if (simpleMessage2.mUser.gift != null) {
                            viewHolder.mRightImage.setVisibility(0);
                            this.loader.a(simpleMessage2.mUser.gift.e, viewHolder.mRightImage, this.optionsGift);
                            return;
                        }
                        return;
                    }
                    return;
                case 24:
                case 36:
                    if (liveMessage instanceof MicMessage) {
                        this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                        String string4 = this.mContext.getString(a.h.room_sys_message_nick);
                        initSysText(true, viewHolder);
                        viewHolder.mMessage.setText(((MicMessage) liveMessage).mMsg);
                        viewHolder.mNickname.setText(string4);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 27:
                    if (liveMessage instanceof ClubMessage) {
                        ClubMessage clubMessage = (ClubMessage) liveMessage;
                        this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                        String string5 = this.mContext.getString(a.h.room_sys_message_nick);
                        initSysText(true, viewHolder);
                        String str14 = clubMessage.mUid;
                        if (TextUtils.isEmpty(clubMessage.mMsg)) {
                            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$ClubMessage$ClubMessageType()[clubMessage.mType.ordinal()]) {
                                case 1:
                                case 7:
                                    if (!isMe(clubMessage.mClubUser)) {
                                        str14 = String.format("欢迎 %s 加入 %s", clubMessage.mClubUser.getFullName(), clubMessage.mClub.f1689b);
                                        break;
                                    } else {
                                        str14 = String.format("恭喜您已经通过家族长审核，成功加入 %s", clubMessage.mClub.f1689b);
                                        break;
                                    }
                                case 3:
                                    str14 = "该家族被家族长解散";
                                    break;
                                case 4:
                                    str14 = String.format("%s 申请加入 %s 家族", clubMessage.mClubUser.getFullName(), clubMessage.mClub.f1689b);
                                    break;
                                case 6:
                                    if (!isMe(clubMessage.mClubUser)) {
                                        str14 = String.format("%s 没有被管理员允许加入 %s ", clubMessage.mClubUser.getFullName(), clubMessage.mClub.f1689b);
                                        break;
                                    } else {
                                        str14 = String.format("很抱歉，您未被允许加入 %s 。再去别的家族试试吧", clubMessage.mClub.f1689b);
                                        break;
                                    }
                                case 8:
                                    str14 = String.format("%s 已退出家族", clubMessage.mClubUser.getFullName());
                                    break;
                                case 9:
                                    if (!isMe(clubMessage.mClubUser)) {
                                        str14 = String.format("%s 被 %s 家族解除了家族关系", clubMessage.mClubUser.getFullName(), clubMessage.mClub.f1689b);
                                        break;
                                    } else {
                                        str14 = String.format("您被 %s 家族解除了家族关系", clubMessage.mClub.f1689b);
                                        break;
                                    }
                            }
                        } else {
                            str14 = clubMessage.mMsg;
                        }
                        viewHolder.mMessage.setText(str14);
                        viewHolder.mNickname.setText(string5);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
                case 39:
                    if (liveMessage instanceof SimpleMessage) {
                        SimpleMessage simpleMessage3 = (SimpleMessage) liveMessage;
                        this.loader.a(s.a(APIKey.APIKey_GET_FACE_SYSTEM), viewHolder.mUserHead, this.options);
                        String string6 = this.mContext.getString(a.h.room_sys_message_nick);
                        if (!TextUtils.isEmpty(simpleMessage3.mMsgType)) {
                            string6 = simpleMessage3.mMsgType;
                        }
                        initSysText(true, viewHolder);
                        viewHolder.mMessage.setText(simpleMessage3.mMessage);
                        viewHolder.mNickname.setText(string6);
                        viewHolder.mUserHead.setTag(null);
                        return;
                    }
                    return;
            }
        }
    }

    public void showDialog(User user) {
    }

    private ViewHolder createHolder(View view) {
        ViewHolder viewHolder = new ViewHolder(null);
        viewHolder.mUserHead = (ImageView) view.findViewById(a.f.room_img_usericon);
        viewHolder.mNickname = (TextView) view.findViewById(a.f.room_message_nickname);
        viewHolder.mNicknameRight = (TextView) view.findViewById(a.f.room_message_nickname_right);
        viewHolder.mTextMid = (TextView) view.findViewById(a.f.room_message_nickname_mid);
        viewHolder.mTextEnd = (TextView) view.findViewById(a.f.room_message_nickname_end);
        viewHolder.mMessage = (TextView) view.findViewById(a.f.room_message_message);
        viewHolder.mRightImage = (ImageView) view.findViewById(a.f.room_item_right);
        viewHolder.mLevelImage = (ImageView) view.findViewById(a.f.room_message_img_level);
        viewHolder.authIcon = (ImageView) view.findViewById(a.f.room_message_img_auth);
        viewHolder.mTestMessageId = (TextView) view.findViewById(a.f.room_message_testid);
        viewHolder.mUserLevelAnima = (ImageView) view.findViewById(a.f.room_level_animation);
        viewHolder.mVipIcon = (ImageView) view.findViewById(a.f.room_vip_icon);
        return viewHolder;
    }

    private void initView(ViewHolder viewHolder) {
        if (viewHolder != null) {
            try {
                viewHolder.mUserHead.setImageResource(a.e.default_ovaled);
            } catch (OutOfMemoryError e) {
                System.gc();
            }
            viewHolder.mUserHead.setTag(null);
            viewHolder.mNickname.setText("");
            viewHolder.mNicknameRight.setVisibility(8);
            viewHolder.mNicknameRight.setText("");
            viewHolder.mTextMid.setVisibility(8);
            viewHolder.mTextMid.setText("");
            viewHolder.mTextEnd.setVisibility(8);
            viewHolder.mTextEnd.setText("");
            viewHolder.mMessage.setText("");
            viewHolder.mRightImage.setVisibility(8);
            viewHolder.mLevelImage.setVisibility(8);
            viewHolder.authIcon.setVisibility(8);
            viewHolder.mTestMessageId.setVisibility(8);
            viewHolder.mVipIcon.setVisibility(8);
            initSysText(false, viewHolder);
        }
    }

    private static class ViewHolder {
        public ImageView authIcon;
        public ImageView mLevelImage;
        public TextView mMessage;
        public TextView mNickname;
        public TextView mNicknameRight;
        public ImageView mRightImage;
        public TextView mTestMessageId;
        public TextView mTextEnd;
        public TextView mTextMid;
        public ImageView mUserHead;
        public ImageView mUserLevelAnima;
        public ImageView mVipIcon;

        private ViewHolder() {
        }

        /* synthetic */ ViewHolder(ViewHolder viewHolder) {
            this();
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (i >= 0 && i < this.mList.size() && this.onListener != null) {
            LiveMessage liveMessage = (LiveMessage) this.mList.get(i);
            switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$MessageKey()[liveMessage.mKey.ordinal()]) {
                case 5:
                case 7:
                case 8:
                case 9:
                    this.onListener.OnItemClick(liveMessage, this.mFlag);
                    return;
                case 6:
                default:
                    return;
            }
        }
    }

    public OnRoomMessageClickListener getListener() {
        return this.onListener;
    }

    public void setListener(OnRoomMessageClickListener onRoomMessageClickListener) {
        this.onListener = onRoomMessageClickListener;
    }

    public void onClick(View view) {
        int intValue;
        if (view.getTag() != null && this.onListener != null && (intValue = ((Integer) view.getTag()).intValue()) < this.mList.size()) {
            this.onListener.OnItemIconClick((LiveMessage) this.mList.get(intValue), this.mFlag);
        }
    }

    public String getRoomUid() {
        return this.mRoomUid;
    }

    public void setRoomUid(String str) {
        this.mRoomUid = str;
    }
}
