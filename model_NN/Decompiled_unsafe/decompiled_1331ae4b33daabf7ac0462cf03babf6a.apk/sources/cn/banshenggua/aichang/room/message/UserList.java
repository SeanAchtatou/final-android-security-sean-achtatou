package cn.banshenggua.aichang.room.message;

import cn.banshenggua.aichang.utils.Constants;
import com.sina.weibo.sdk.constant.WBPageConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class UserList {
    public int count = 20;
    public int limit = Constants.textWatch_result;
    public List<User> mUsers = new ArrayList();
    public int offset = 0;
    public int page = 0;
    public int page_count = 1;
    public int page_next = 0;
    public int page_previous = 0;
    public int total = 0;

    public void parseUsers(JSONArray jSONArray) {
        if (jSONArray != null) {
            for (int i = 0; i < jSONArray.length(); i++) {
                User user = new User();
                try {
                    user.parseUser(jSONArray.getJSONObject(i));
                    this.mUsers.add(user);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void parseUsers(JSONObject jSONObject) {
        if (jSONObject != null) {
            this.total = jSONObject.optInt("total", 0);
            this.page = jSONObject.optInt(WBPageConstants.ParamKey.PAGE, 1);
            this.count = jSONObject.optInt(WBPageConstants.ParamKey.COUNT, 0);
            this.limit = jSONObject.optInt("limit", 20);
            this.page_next = this.page + 1;
            JSONArray optJSONArray = jSONObject.optJSONArray("users");
            if (optJSONArray != null) {
                for (int i = 0; i < optJSONArray.length(); i++) {
                    User user = new User();
                    try {
                        user.parseUser(optJSONArray.getJSONObject(i));
                        this.mUsers.add(user);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
