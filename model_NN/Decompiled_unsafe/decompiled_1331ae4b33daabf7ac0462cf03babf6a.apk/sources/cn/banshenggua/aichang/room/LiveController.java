package cn.banshenggua.aichang.room;

import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;
import cn.banshenggua.aichang.room.message.Banzou;
import cn.banshenggua.aichang.room.message.User;
import cn.banshenggua.aichang.utils.ULog;
import com.pocketmusic.kshare.requestobjs.o;

public class LiveController {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType = null;
    private static final String TAG = "LiveController";
    private FragmentActivity mContext = null;
    private LivePlayController mPlayController = null;
    private ViewGroup mRootView = null;
    private LiveControllerType mType = null;

    public interface ControllerClick {
        void onBackClick();

        void onMoreClick();
    }

    public enum LiveControllerType {
        AudioPlay,
        VideoPlay,
        None
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType;
        if (iArr == null) {
            iArr = new int[LiveControllerType.values().length];
            try {
                iArr[LiveControllerType.AudioPlay.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[LiveControllerType.None.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[LiveControllerType.VideoPlay.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType = iArr;
        }
        return iArr;
    }

    public LiveController(FragmentActivity fragmentActivity, ViewGroup viewGroup) {
        this.mContext = fragmentActivity;
        this.mRootView = viewGroup;
    }

    public void setControllerType(LiveControllerType liveControllerType) {
        ULog.d(TAG, "setControllerType type: " + liveControllerType);
        if (liveControllerType != null && liveControllerType != this.mType) {
            this.mType = liveControllerType;
            initView();
            if (this.mPlayController != null) {
                this.mPlayController.setControllerType(this.mType);
            }
        }
    }

    public void showRoomInfo(o oVar) {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.ShowRoomInfo(oVar);
        }
    }

    public void showPlayInfo(User user, boolean z) {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.ShowUserInfo(user);
        }
    }

    public void stopTimer() {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.stopTimer();
        }
    }

    public void beginTimer(long j, long j2, long j3) {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.beginTimer(j, j2, j3);
        }
    }

    public void Pause() {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.Pause();
        }
    }

    public void Play() {
        if (isPlayController() && this.mPlayController != null) {
            this.mPlayController.Play();
        }
    }

    public void ClosePlayLyric() {
    }

    public void OpenPlayLyric() {
    }

    public void showRecordInfo() {
    }

    public void changeBanzou(Banzou banzou) {
        if (this.mPlayController != null) {
            this.mPlayController.ChangeBanzou(banzou);
        }
    }

    public boolean isPlayController() {
        if (this.mType == LiveControllerType.AudioPlay || this.mType == LiveControllerType.VideoPlay || this.mType == LiveControllerType.None) {
            return true;
        }
        return false;
    }

    public boolean isPlayVideo() {
        return this.mType == LiveControllerType.VideoPlay;
    }

    public boolean isVideoOpen() {
        if (isPlayController() && this.mPlayController != null) {
            return this.mPlayController.isVideoOpen();
        }
        if (isRecordController()) {
            return true;
        }
        return false;
    }

    public boolean isRecordController() {
        return false;
    }

    private void initView() {
        ULog.d(TAG, "initView type: " + this.mType);
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$LiveController$LiveControllerType()[this.mType.ordinal()]) {
            case 1:
                initAudioPlayView();
                return;
            case 2:
                initVideoPlayView();
                return;
            case 3:
                initNoneView();
                return;
            default:
                return;
        }
    }

    private void initPlayView() {
        if (this.mRootView != null) {
            this.mRootView.removeAllViews();
            if (this.mPlayController == null) {
                this.mPlayController = new LivePlayController(this.mContext);
            } else {
                this.mPlayController.Play();
            }
            this.mRootView.addView(this.mPlayController.getView(), new ViewGroup.LayoutParams(-1, -1));
        }
    }

    private void initVideoPlayView() {
        initPlayView();
    }

    private void initNoneView() {
        initPlayView();
    }

    private void initAudioPlayView() {
        initPlayView();
    }

    public void clean() {
        if (this.mRootView != null) {
            this.mRootView.removeAllViews();
        }
        this.mRootView = null;
    }
}
