package cn.banshenggua.aichang.room.message;

import com.pocketmusic.kshare.requestobjs.o;
import org.json.JSONObject;

public class UserListMessage extends LiveMessage {
    private static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserListMessage$UserType;
    public String mBzid = null;
    public UserType mType = UserType.UserList;
    public UserList mUsers = null;

    public enum UserType {
        UserList
    }

    static /* synthetic */ int[] $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserListMessage$UserType() {
        int[] iArr = $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserListMessage$UserType;
        if (iArr == null) {
            iArr = new int[UserType.values().length];
            try {
                iArr[UserType.UserList.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            $SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserListMessage$UserType = iArr;
        }
        return iArr;
    }

    public UserListMessage(UserType userType, o oVar) {
        super(oVar);
        this.mType = userType;
        switch ($SWITCH_TABLE$cn$banshenggua$aichang$room$message$UserListMessage$UserType()[userType.ordinal()]) {
            case 1:
                this.mKey = MessageKey.Message_GetUsers;
                return;
            default:
                return;
        }
    }

    public SocketMessage getSocketMessage() {
        return super.getSocketMessage();
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (this.mUsers != null) {
            this.mUsers = null;
        }
        if (jSONObject != null && jSONObject.has("users")) {
            this.mUsers = new UserList();
            this.mUsers.parseUsers(jSONObject.optJSONObject("users"));
        }
    }
}
