package cn.banshenggua.aichang.room.message;

import cn.banshenggua.aichang.utils.StringUtil;
import com.pocketmusic.kshare.requestobjs.o;
import org.json.JSONObject;

public class PrivateMessage extends LiveMessage {
    public User mFrom;
    public String mFromNickname = "";
    public String mFromUid = "";
    public String mMessage = "";
    public User mTo;
    public String mToNickname = "";
    public String mToUid = "";

    public PrivateMessage(o oVar) {
        super(oVar);
        this.mKey = MessageKey.MessageCenter_Private;
    }

    public SocketMessage getSocketMessage() {
        SocketMessage socketMessage = super.getSocketMessage();
        socketMessage.pushCommend("touid", this.mToUid);
        socketMessage.pushCommend("msg", this.mMessage);
        return socketMessage;
    }

    public void parseOut(JSONObject jSONObject) {
        super.parseOut(jSONObject);
        if (jSONObject != null) {
            this.mMessage = StringUtil.optStringFromJson(jSONObject, "text", "");
            this.mFromUid = jSONObject.optString("uid", "");
            this.mToUid = jSONObject.optString("touid", "");
            if (jSONObject.has("from")) {
                this.mFrom = new User();
                this.mFrom.parseUser(jSONObject.optJSONObject("from"));
            }
            if (jSONObject.has("to")) {
                this.mTo = new User();
                this.mTo.parseUser(jSONObject.optJSONObject("to"));
            }
        }
    }
}
