package cn.banshenggua.aichang.room;

public class EventMessage {
    public Object mObject;
    public EventMessageType mType;
    public int mViewId = -1;

    public enum EventMessageType {
        ViewMessage,
        ObjectMessage
    }

    public EventMessage(EventMessageType eventMessageType, Object obj) {
        this.mType = eventMessageType;
        this.mObject = obj;
    }

    public EventMessage(EventMessageType eventMessageType, int i) {
        this.mType = eventMessageType;
        this.mViewId = i;
    }

    public EventMessage(EventMessageType eventMessageType, int i, Object obj) {
        this.mType = eventMessageType;
        this.mViewId = i;
        this.mObject = obj;
    }

    public String toString() {
        return this.mType + "; viewid: " + this.mViewId;
    }
}
