package cn.banshenggua.aichang.room.message;

import android.text.TextUtils;
import cn.banshenggua.aichang.api.APIKey;
import cn.banshenggua.aichang.app.Session;
import cn.banshenggua.aichang.utils.StringUtil;
import cn.banshenggua.aichang.utils.ULog;
import com.meizu.cloud.pushsdk.notification.model.NotifyType;
import com.pocketmusic.kshare.requestobjs.a;
import com.pocketmusic.kshare.requestobjs.b;
import com.pocketmusic.kshare.requestobjs.e;
import com.pocketmusic.kshare.requestobjs.h;
import com.pocketmusic.kshare.requestobjs.o;
import com.pocketmusic.kshare.requestobjs.s;
import com.sina.weibo.sdk.constant.WBConstants;
import com.sina.weibo.sdk.exception.WeiboAuthException;
import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import com.tencent.stat.DeviceInfo;
import java.io.Serializable;
import org.json.JSONObject;

public class User implements Serializable {
    private static final long serialVersionUID = 2684030268962542023L;
    public int applyStatu = 0;
    public String authIcon = "";
    public String auth_info = "";
    public e gift;
    public boolean isAdmin = false;
    public boolean isMixStreams = false;
    public boolean isVice = false;
    public boolean isVip = false;
    public Banzou mBanzou = null;
    public String mBzid = "";
    public String mBzname = "";
    public b mClub = null;
    public int mExperience = 0;
    public String mExperienceMsg = "";
    public String mFace = "";
    private String mFullName = "";
    public int mGender = 0;
    public int mGiftCount = -1;
    public int mGiftValue = -1;
    public int mHighScore = -1;
    public boolean mInFamily = false;
    public boolean mIsMultiMic = false;
    public int mLevel = 0;
    public String mLevelImage = "";
    public String mLevelName = "";
    public String mMedia = "";
    public String mMicId = "";
    public String mNickname = "";
    public int mRank = -1;
    public String mRankString = "";
    public o mRoom = null;
    public int mScore = -1;
    public String mSongName = "";
    public String mUid = "";
    public String mVip = null;
    public int micorder = 0;
    public boolean onLine = false;
    public boolean onMic = false;
    public String signature = "";

    public static User getCurrentUser(o oVar) {
        a currentAccount = Session.getCurrentAccount();
        User user = new User();
        if (currentAccount != null) {
            user.mUid = currentAccount.f1685b;
            user.mNickname = currentAccount.d;
            user.mFullName = currentAccount.a();
            user.mGender = currentAccount.f.intValue();
            ULog.d("SimpleMessage", "getcurrent: " + currentAccount.f + "; " + currentAccount.f1685b + "; " + currentAccount.d);
            user.mFace = currentAccount.k;
            user.mRoom = oVar;
            user.signature = currentAccount.t;
            user.auth_info = currentAccount.ab;
            user.authIcon = currentAccount.ac;
            user.mVip = currentAccount.ag;
        }
        return user;
    }

    public static User getUser(a aVar) {
        User user = new User();
        if (aVar != null) {
            user.mUid = aVar.f1685b;
            user.mNickname = aVar.d;
            user.mFullName = aVar.a();
            user.mGender = aVar.f.intValue();
            user.mFace = aVar.k;
            user.mRoom = aVar.U;
            user.signature = aVar.t;
            user.auth_info = aVar.ab;
            user.authIcon = aVar.ac;
            user.mVip = aVar.ag;
        }
        return user;
    }

    public a getAccount() {
        a aVar = new a();
        aVar.f1685b = this.mUid;
        aVar.d = this.mNickname;
        aVar.a(this.mFullName);
        aVar.f = Integer.valueOf(this.mGender);
        aVar.k = this.mFace;
        aVar.U = this.mRoom;
        aVar.t = this.signature;
        aVar.ab = this.auth_info;
        aVar.ac = this.authIcon;
        aVar.ag = this.mVip;
        return aVar;
    }

    public String toString() {
        return String.valueOf(this.mUid) + "; " + this.mNickname;
    }

    public void getFace() {
    }

    public boolean isAnonymous() {
        return WeiboAuthException.DEFAULT_AUTH_ERROR_CODE.equals(this.mUid);
    }

    public enum FACE_SIZE {
        SIM(NotifyType.SOUND),
        MID("m"),
        LARGE("o");
        
        private String mValue = "m";

        private FACE_SIZE(String str) {
            this.mValue = str;
        }

        public String getValue() {
            return this.mValue;
        }
    }

    public String getFace(FACE_SIZE face_size) {
        if (TextUtils.isEmpty(this.mFace)) {
            this.mFace = getFace(this.mUid, face_size);
        }
        return this.mFace;
    }

    public static String getFace(String str, FACE_SIZE face_size) {
        if (face_size == null) {
            face_size = FACE_SIZE.MID;
        }
        String a2 = s.a(APIKey.APIKey_GetUserFace);
        if (a2.indexOf("?") > 0) {
            return String.valueOf(a2) + "&" + "uid=" + str + "&size=" + face_size.getValue();
        }
        return String.valueOf(a2) + "?" + "uid=" + str + "&size=" + face_size.getValue();
    }

    public void parseUser(JSONObject jSONObject) {
        boolean z;
        if (jSONObject != null) {
            this.mUid = jSONObject.optString("uid", "");
            this.mNickname = StringUtil.optStringFromJson(jSONObject, SelectCountryActivity.EXTRA_COUNTRY_NAME, "");
            this.mGender = jSONObject.optInt("gender", 0);
            this.mFace = StringUtil.optStringFromJson(jSONObject, "face", "");
            if (TextUtils.isEmpty(this.mFace)) {
                this.mFace = getFace(FACE_SIZE.MID);
            }
            this.onMic = jSONObject.optBoolean("onmic", false);
            this.micorder = jSONObject.optInt("micorder", -1);
            this.isAdmin = jSONObject.optBoolean("admin", false);
            if (jSONObject.has("banzou")) {
                this.mBanzou = new Banzou();
                this.mBanzou.parseBanzou(jSONObject.optJSONObject("banzou"));
            }
            if (jSONObject.has("nickname")) {
                this.mNickname = StringUtil.optStringFromJson(jSONObject, "nickname", "");
            }
            this.mMedia = jSONObject.optString("media", "");
            if (jSONObject.has("mt")) {
                this.mMedia = jSONObject.optString("mt", "");
            }
            this.mMicId = jSONObject.optString(DeviceInfo.TAG_MID, "");
            this.mLevel = jSONObject.optInt("level", 0);
            this.mLevelName = jSONObject.optString("levelname", "未知");
            if (this.mLevel > 0) {
                this.mLevelImage = h.a(h.a.SIM, this.mLevel);
            }
            this.mExperience = jSONObject.optInt("exp", 0);
            this.mExperienceMsg = jSONObject.optString("msg", "");
            if (jSONObject.has("room")) {
                this.mRoom = new o();
                this.mRoom.c(jSONObject.optJSONObject("room"));
            }
            if (jSONObject.has("family")) {
                if (this.mClub != null) {
                    this.mClub = null;
                }
                this.mClub = new b();
                this.mClub.a(jSONObject.optJSONObject("family"));
            }
            this.mScore = jSONObject.optInt(WBConstants.GAME_PARAMS_SCORE, -1);
            this.mRank = jSONObject.optInt("rank", -1);
            this.mHighScore = jSONObject.optInt("highscore", -1);
            this.mGiftCount = jSONObject.optInt("giftcount", -1);
            this.mGiftValue = jSONObject.optInt("giftvalue", -1);
            this.mSongName = jSONObject.optString("songname", "");
            this.mRankString = jSONObject.optString("rank_str", "");
            this.auth_info = jSONObject.optString("auth", "");
            this.mFullName = jSONObject.optString("full", "");
            if (TextUtils.isEmpty(this.mFullName)) {
                this.mFullName = this.mNickname;
            }
            this.mInFamily = jSONObject.optString("infamily", "0").equalsIgnoreCase("1");
            if (jSONObject.optString("isonline", "0").equalsIgnoreCase("1")) {
                z = true;
            } else {
                z = false;
            }
            this.onLine = z;
            this.authIcon = jSONObject.optString("icon", this.authIcon);
            if (jSONObject.optInt("multi_mic", 0) == 1) {
                this.mIsMultiMic = true;
            } else {
                this.mIsMultiMic = false;
            }
            this.mVip = jSONObject.optString("vip", null);
            this.isMixStreams = "mix".equals(jSONObject.optString("streams"));
        }
    }

    public String getFullName() {
        if (TextUtils.isEmpty(this.mFullName)) {
            return this.mNickname;
        }
        return this.mFullName;
    }

    public void setFullName(String str) {
        this.mFullName = str;
    }
}
