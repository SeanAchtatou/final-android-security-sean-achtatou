package cn.banshenggua.aichang.room;

import cn.banshenggua.aichang.room.message.LiveMessage;

public interface OnRoomMessageClickListener {
    void OnItemClick(LiveMessage liveMessage, int i);

    void OnItemIconClick(LiveMessage liveMessage, int i);
}
