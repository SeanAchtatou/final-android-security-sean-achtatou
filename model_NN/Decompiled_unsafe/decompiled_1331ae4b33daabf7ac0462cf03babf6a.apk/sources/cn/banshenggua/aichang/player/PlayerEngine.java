package cn.banshenggua.aichang.player;

import android.media.MediaPlayer;
import android.view.SurfaceHolder;
import cn.banshenggua.aichang.player.Playlist;

public interface PlayerEngine {
    void forward(int i);

    int getDuration();

    MediaPlayer getMyCurrentMedia();

    Playlist.PlaylistPlaybackMode getPlaybackMode();

    Playlist getPlaylist();

    boolean isPausing();

    boolean isPlaying();

    void next();

    void openPlaylist(Playlist playlist);

    void pause();

    void play();

    void prev();

    void prevList();

    void rewind(int i);

    void seekToPosition(int i);

    void setListener(PlayerEngineListener playerEngineListener);

    void setPlaybackMode(Playlist.PlaylistPlaybackMode playlistPlaybackMode);

    void setPlayerSurfaceHolder(SurfaceHolder surfaceHolder);

    void skipTo(int i);

    void stop();
}
