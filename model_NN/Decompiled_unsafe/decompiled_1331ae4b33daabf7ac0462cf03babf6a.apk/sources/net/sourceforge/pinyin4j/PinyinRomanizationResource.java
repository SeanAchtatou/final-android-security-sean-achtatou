package net.sourceforge.pinyin4j;

import com.b.a.a.d;
import com.b.a.a.m;
import com.b.a.a.q;
import java.io.FileNotFoundException;
import java.io.IOException;

class PinyinRomanizationResource {
    private d pinyinMappingDoc;

    /* renamed from: net.sourceforge.pinyin4j.PinyinRomanizationResource$1  reason: invalid class name */
    static class AnonymousClass1 {
    }

    private static class PinyinRomanizationSystemResourceHolder {
        static final PinyinRomanizationResource theInstance = new PinyinRomanizationResource(null);

        private PinyinRomanizationSystemResourceHolder() {
        }
    }

    private PinyinRomanizationResource() {
        initializeResource();
    }

    PinyinRomanizationResource(AnonymousClass1 r1) {
        this();
    }

    static PinyinRomanizationResource getInstance() {
        return PinyinRomanizationSystemResourceHolder.theInstance;
    }

    private void initializeResource() {
        try {
            setPinyinMappingDoc(q.a("", ResourceHelper.getResourceInputStream("/pinyindb/pinyin_mapping.xml")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        } catch (m e3) {
            e3.printStackTrace();
        }
    }

    private void setPinyinMappingDoc(d dVar) {
        this.pinyinMappingDoc = dVar;
    }

    /* access modifiers changed from: package-private */
    public d getPinyinMappingDoc() {
        return this.pinyinMappingDoc;
    }
}
