package net.sourceforge.pinyin4j;

import com.b.a.a.f;
import com.b.a.a.m;

class PinyinRomanizationTranslator {
    PinyinRomanizationTranslator() {
    }

    static String convertRomanizationSystem(String str, PinyinRomanizationType pinyinRomanizationType, PinyinRomanizationType pinyinRomanizationType2) {
        String extractPinyinString = TextHelper.extractPinyinString(str);
        String extractToneNumber = TextHelper.extractToneNumber(str);
        try {
            f b2 = PinyinRomanizationResource.getInstance().getPinyinMappingDoc().b(new StringBuffer().append("//").append(pinyinRomanizationType.getTagName()).append("[text()='").append(extractPinyinString).append("']").toString());
            if (b2 == null) {
                return null;
            }
            return new StringBuffer().append(b2.c(new StringBuffer().append("../").append(pinyinRomanizationType2.getTagName()).append("/text()").toString())).append(extractToneNumber).toString();
        } catch (m e) {
            e.printStackTrace();
            return null;
        }
    }
}
