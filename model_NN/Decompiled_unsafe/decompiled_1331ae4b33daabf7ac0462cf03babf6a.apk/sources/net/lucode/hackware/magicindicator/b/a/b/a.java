package net.lucode.hackware.magicindicator.b.a.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import java.util.Arrays;
import java.util.List;
import net.lucode.hackware.magicindicator.b.a.a.c;
import net.lucode.hackware.magicindicator.b.b;

/* compiled from: LinePagerIndicator */
public class a extends View implements c {

    /* renamed from: a  reason: collision with root package name */
    private int f4187a;

    /* renamed from: b  reason: collision with root package name */
    private Interpolator f4188b = new LinearInterpolator();
    private Interpolator c = new LinearInterpolator();
    private float d;
    private float e;
    private float f;
    private float g;
    private float h;
    private Paint i;
    private List<net.lucode.hackware.magicindicator.b.a.c.a> j;
    private List<Integer> k;
    private RectF l = new RectF();

    public a(Context context) {
        super(context);
        a(context);
    }

    private void a(Context context) {
        this.i = new Paint(1);
        this.i.setStyle(Paint.Style.FILL);
        this.e = (float) b.a(context, 3.0d);
        this.g = (float) b.a(context, 10.0d);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        canvas.drawRoundRect(this.l, this.h, this.h, this.i);
    }

    public void a(int i2, float f2, int i3) {
        float a2;
        float a3;
        float a4;
        float a5;
        if (this.j != null && !this.j.isEmpty()) {
            if (this.k != null && this.k.size() > 0) {
                this.i.setColor(net.lucode.hackware.magicindicator.b.a.a(f2, this.k.get(Math.abs(i2) % this.k.size()).intValue(), this.k.get(Math.abs(i2 + 1) % this.k.size()).intValue()));
            }
            net.lucode.hackware.magicindicator.b.a.c.a a6 = net.lucode.hackware.magicindicator.a.a(this.j, i2);
            net.lucode.hackware.magicindicator.b.a.c.a a7 = net.lucode.hackware.magicindicator.a.a(this.j, i2 + 1);
            if (this.f4187a == 0) {
                a2 = ((float) a6.f4189a) + this.f;
                a3 = this.f + ((float) a7.f4189a);
                a4 = ((float) a6.c) - this.f;
                a5 = ((float) a7.c) - this.f;
            } else if (this.f4187a == 1) {
                a2 = ((float) a6.e) + this.f;
                a3 = this.f + ((float) a7.e);
                a4 = ((float) a6.g) - this.f;
                a5 = ((float) a7.g) - this.f;
            } else {
                a2 = ((float) a6.f4189a) + ((((float) a6.a()) - this.g) / 2.0f);
                a3 = ((((float) a7.a()) - this.g) / 2.0f) + ((float) a7.f4189a);
                a4 = ((float) a6.f4189a) + ((((float) a6.a()) + this.g) / 2.0f);
                a5 = ((float) a7.f4189a) + ((((float) a7.a()) + this.g) / 2.0f);
            }
            this.l.left = ((a3 - a2) * this.f4188b.getInterpolation(f2)) + a2;
            this.l.right = ((a5 - a4) * this.c.getInterpolation(f2)) + a4;
            this.l.top = (((float) getHeight()) - this.e) - this.d;
            this.l.bottom = ((float) getHeight()) - this.d;
            invalidate();
        }
    }

    public void a(int i2) {
    }

    public void b(int i2) {
    }

    public void a(List<net.lucode.hackware.magicindicator.b.a.c.a> list) {
        this.j = list;
    }

    public float getYOffset() {
        return this.d;
    }

    public void setYOffset(float f2) {
        this.d = f2;
    }

    public float getXOffset() {
        return this.f;
    }

    public void setXOffset(float f2) {
        this.f = f2;
    }

    public float getLineHeight() {
        return this.e;
    }

    public void setLineHeight(float f2) {
        this.e = f2;
    }

    public float getLineWidth() {
        return this.g;
    }

    public void setLineWidth(float f2) {
        this.g = f2;
    }

    public float getRoundRadius() {
        return this.h;
    }

    public void setRoundRadius(float f2) {
        this.h = f2;
    }

    public int getMode() {
        return this.f4187a;
    }

    public void setMode(int i2) {
        if (i2 == 2 || i2 == 0 || i2 == 1) {
            this.f4187a = i2;
            return;
        }
        throw new IllegalArgumentException("mode " + i2 + " not supported.");
    }

    public Paint getPaint() {
        return this.i;
    }

    public List<Integer> getColors() {
        return this.k;
    }

    public void setColors(Integer... numArr) {
        this.k = Arrays.asList(numArr);
    }

    public Interpolator getStartInterpolator() {
        return this.f4188b;
    }

    public void setStartInterpolator(Interpolator interpolator) {
        this.f4188b = interpolator;
        if (this.f4188b == null) {
            this.f4188b = new LinearInterpolator();
        }
    }

    public Interpolator getEndInterpolator() {
        return this.c;
    }

    public void setEndInterpolator(Interpolator interpolator) {
        this.c = interpolator;
        if (this.c == null) {
            this.c = new LinearInterpolator();
        }
    }
}
