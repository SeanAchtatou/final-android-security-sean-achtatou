package net.lucode.hackware.magicindicator.b.a.d;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextUtils;
import android.widget.TextView;

/* compiled from: SimplePagerTitleView */
public class b extends TextView implements net.lucode.hackware.magicindicator.b.a.a.b {

    /* renamed from: a  reason: collision with root package name */
    protected int f4191a;

    /* renamed from: b  reason: collision with root package name */
    protected int f4192b;

    public b(Context context) {
        super(context, null);
        a(context);
    }

    private void a(Context context) {
        setGravity(17);
        int a2 = net.lucode.hackware.magicindicator.b.b.a(context, 10.0d);
        setPadding(a2, 0, a2, 0);
        setSingleLine();
        setEllipsize(TextUtils.TruncateAt.END);
    }

    public void a(int i, int i2) {
        setTextColor(this.f4191a);
    }

    public void b(int i, int i2) {
        setTextColor(this.f4192b);
    }

    public void b(int i, int i2, float f, boolean z) {
    }

    public void a(int i, int i2, float f, boolean z) {
    }

    public int getContentLeft() {
        Rect rect = new Rect();
        getPaint().getTextBounds(getText().toString(), 0, getText().length(), rect);
        return (getLeft() + (getWidth() / 2)) - (rect.width() / 2);
    }

    public int getContentTop() {
        Paint.FontMetrics fontMetrics = getPaint().getFontMetrics();
        return (int) (((float) (getHeight() / 2)) - ((fontMetrics.bottom - fontMetrics.top) / 2.0f));
    }

    public int getContentRight() {
        Rect rect = new Rect();
        getPaint().getTextBounds(getText().toString(), 0, getText().length(), rect);
        return (rect.width() / 2) + getLeft() + (getWidth() / 2);
    }

    public int getContentBottom() {
        Paint.FontMetrics fontMetrics = getPaint().getFontMetrics();
        return (int) (((fontMetrics.bottom - fontMetrics.top) / 2.0f) + ((float) (getHeight() / 2)));
    }

    public int getSelectedColor() {
        return this.f4191a;
    }

    public void setSelectedColor(int i) {
        this.f4191a = i;
    }

    public int getNormalColor() {
        return this.f4192b;
    }

    public void setNormalColor(int i) {
        this.f4192b = i;
    }
}
