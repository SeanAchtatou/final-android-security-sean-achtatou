package net.lucode.hackware.magicindicator;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import net.lucode.hackware.magicindicator.a.a;

public class MagicIndicator extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private a f4180a;

    public MagicIndicator(Context context) {
        super(context);
    }

    public MagicIndicator(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void a(int i, float f, int i2) {
        if (this.f4180a != null) {
            this.f4180a.a(i, f, i2);
        }
    }

    public void a(int i) {
        if (this.f4180a != null) {
            this.f4180a.a(i);
        }
    }

    public void b(int i) {
        if (this.f4180a != null) {
            this.f4180a.b(i);
        }
    }

    public a getNavigator() {
        return this.f4180a;
    }

    public void setNavigator(a aVar) {
        if (this.f4180a != aVar) {
            if (this.f4180a != null) {
                this.f4180a.b();
            }
            this.f4180a = aVar;
            removeAllViews();
            if (this.f4180a instanceof View) {
                addView((View) this.f4180a, new FrameLayout.LayoutParams(-1, -1));
                this.f4180a.a();
            }
        }
    }
}
