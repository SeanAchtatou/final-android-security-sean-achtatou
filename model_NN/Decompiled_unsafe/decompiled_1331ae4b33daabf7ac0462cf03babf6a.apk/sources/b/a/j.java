package b.a;

import android.content.Context;
import java.util.Arrays;
import java.util.List;

/* compiled from: Defcon */
public class j {

    /* renamed from: a  reason: collision with root package name */
    private int f538a = 0;

    /* renamed from: b  reason: collision with root package name */
    private final long f539b = 60000;

    public al a(Context context, al alVar) {
        if (alVar == null) {
            return null;
        }
        if (this.f538a == 1) {
            alVar.a((List<aa>) null);
            return alVar;
        } else if (this.f538a == 2) {
            alVar.b(Arrays.asList(a(context)));
            alVar.a((List<aa>) null);
            return alVar;
        } else if (this.f538a != 3) {
            return alVar;
        } else {
            alVar.b((List<aj>) null);
            alVar.a((List<aa>) null);
            return alVar;
        }
    }

    public aj a(Context context) {
        long currentTimeMillis = System.currentTimeMillis();
        aj ajVar = new aj();
        ajVar.a(cw.e(context));
        ajVar.a(currentTimeMillis);
        ajVar.b(currentTimeMillis + 60000);
        ajVar.c(60000);
        return ajVar;
    }

    public long a() {
        switch (this.f538a) {
            case 1:
                return 14400000;
            case 2:
                return 28800000;
            case 3:
                return LogBuilder.MAX_INTERVAL;
            default:
                return 0;
        }
    }

    public void a(int i) {
        if (i >= 0 && i <= 3) {
            this.f538a = i;
        }
    }

    public boolean b() {
        return this.f538a != 0;
    }
}
