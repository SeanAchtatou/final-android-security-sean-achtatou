package b.a;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;

/* compiled from: UError */
public class g extends r implements co {
    public g() {
        a(System.currentTimeMillis());
        a(s.LEGIT);
    }

    public g(String str) {
        this();
        a(str);
    }

    public g(Throwable th) {
        this();
        a(a(th));
    }

    public g a(boolean z) {
        a(z ? s.LEGIT : s.ALIEN);
        return this;
    }

    private String a(Throwable th) {
        if (th == null) {
            return null;
        }
        try {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            th.printStackTrace(printWriter);
            for (Throwable cause = th.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            String obj = stringWriter.toString();
            printWriter.close();
            stringWriter.close();
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void a(al alVar, String str) {
        aa aaVar;
        if (alVar.b() > 0) {
            Iterator<aa> it = alVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                aaVar = it.next();
                if (str.equals(aaVar.a())) {
                    break;
                }
            }
        }
        aaVar = null;
        if (aaVar == null) {
            aaVar = new aa();
            aaVar.a(str);
            alVar.a(aaVar);
        }
        aaVar.a(this);
    }
}
