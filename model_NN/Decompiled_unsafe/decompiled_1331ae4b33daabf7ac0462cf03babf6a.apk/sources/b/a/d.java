package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.lang.reflect.Method;

/* compiled from: TrafficTracker */
public class d {
    public static ak a(Context context) {
        try {
            ak akVar = new ak();
            long[] b2 = b(context);
            if (b2[0] <= 0 || b2[1] <= 0) {
                return null;
            }
            SharedPreferences a2 = cu.a(context);
            long j = a2.getLong("uptr", -1);
            long j2 = a2.getLong("dntr", -1);
            a2.edit().putLong("uptr", b2[1]).putLong("dntr", b2[0]).commit();
            if (j <= 0 || j2 <= 0) {
                return null;
            }
            b2[0] = b2[0] - j2;
            b2[1] = b2[1] - j;
            if (b2[0] <= 0 || b2[1] <= 0) {
                return null;
            }
            akVar.b((int) b2[0]);
            akVar.a((int) b2[1]);
            return akVar;
        } catch (Exception e) {
            ao.d("MobclickAgent", "sdk less than 2.2 has get no traffic");
            return null;
        }
    }

    private static long[] b(Context context) throws Exception {
        Class<?> cls = Class.forName("android.net.TrafficStats");
        Method method = cls.getMethod("getUidRxBytes", Integer.TYPE);
        Method method2 = cls.getMethod("getUidTxBytes", Integer.TYPE);
        int i = context.getApplicationInfo().uid;
        if (i == -1) {
            return null;
        }
        return new long[]{((Long) method.invoke(null, Integer.valueOf(i))).longValue(), ((Long) method2.invoke(null, Integer.valueOf(i))).longValue()};
    }
}
