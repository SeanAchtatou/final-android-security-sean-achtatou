package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ImprintValue */
public class z implements au<z, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("ImprintValue");
    /* access modifiers changed from: private */
    public static final bk f = new bk("value", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("guid", (byte) 11, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f593a;

    /* renamed from: b  reason: collision with root package name */
    public long f594b;
    public String c;
    private byte j = 0;
    private e[] k = {e.VALUE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.z$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VALUE, (Object) new bc("value", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.GUID, (Object) new bc("guid", (byte) 1, new bd((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(z.class, d);
    }

    /* compiled from: ImprintValue */
    public enum e implements ay {
        VALUE(1, "value"),
        TS(2, "ts"),
        GUID(3, "guid");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public String a() {
        return this.f593a;
    }

    public boolean b() {
        return this.f593a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f593a = null;
        }
    }

    public long c() {
        return this.f594b;
    }

    public boolean d() {
        return as.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ImprintValue(");
        boolean z = true;
        if (b()) {
            sb.append("value:");
            if (this.f593a == null) {
                sb.append("null");
            } else {
                sb.append(this.f593a);
            }
            z = false;
        }
        if (!z) {
            sb.append(", ");
        }
        sb.append("ts:");
        sb.append(this.f594b);
        sb.append(", ");
        sb.append("guid:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ax {
        if (this.c == null) {
            throw new bo("Required field 'guid' was not present! Struct: " + toString());
        }
    }

    /* compiled from: ImprintValue */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ImprintValue */
    private static class a extends bw<z> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, z zVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!zVar.d()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    zVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            zVar.f593a = bnVar.v();
                            zVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            zVar.f594b = bnVar.t();
                            zVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            zVar.c = bnVar.v();
                            zVar.c(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, z zVar) throws ax {
            zVar.f();
            bnVar.a(z.e);
            if (zVar.f593a != null && zVar.b()) {
                bnVar.a(z.f);
                bnVar.a(zVar.f593a);
                bnVar.b();
            }
            bnVar.a(z.g);
            bnVar.a(zVar.f594b);
            bnVar.b();
            if (zVar.c != null) {
                bnVar.a(z.h);
                bnVar.a(zVar.c);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: ImprintValue */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ImprintValue */
    private static class c extends bx<z> {
        private c() {
        }

        public void a(bn bnVar, z zVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(zVar.f594b);
            btVar.a(zVar.c);
            BitSet bitSet = new BitSet();
            if (zVar.b()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (zVar.b()) {
                btVar.a(zVar.f593a);
            }
        }

        public void b(bn bnVar, z zVar) throws ax {
            bt btVar = (bt) bnVar;
            zVar.f594b = btVar.t();
            zVar.b(true);
            zVar.c = btVar.v();
            zVar.c(true);
            if (btVar.b(1).get(0)) {
                zVar.f593a = btVar.v();
                zVar.a(true);
            }
        }
    }
}
