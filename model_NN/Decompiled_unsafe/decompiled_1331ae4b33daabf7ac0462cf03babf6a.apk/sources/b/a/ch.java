package b.a;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* compiled from: UTDIdTracker */
public class ch extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f509a = Pattern.compile("UTDID\">([^<]+)");

    /* renamed from: b  reason: collision with root package name */
    private Context f510b;

    public ch(Context context) {
        super("utdid");
        this.f510b = context;
    }

    public String f() {
        return g();
    }

    private String g() {
        FileInputStream fileInputStream;
        File h = h();
        if (h == null || !h.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(h);
            String b2 = b(ar.a(fileInputStream));
            ar.c(fileInputStream);
            return b2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } catch (Throwable th) {
            ar.c(fileInputStream);
            throw th;
        }
    }

    private String b(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = f509a.matcher(str);
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    private File h() {
        if (!an.a(this.f510b, "android.permission.WRITE_EXTERNAL_STORAGE") || !Environment.getExternalStorageState().equals("mounted")) {
            return null;
        }
        try {
            return new File(Environment.getExternalStorageDirectory().getCanonicalPath(), ".UTSystemConfig/Global/Alvin2.xml");
        } catch (Exception e) {
            return null;
        }
    }
}
