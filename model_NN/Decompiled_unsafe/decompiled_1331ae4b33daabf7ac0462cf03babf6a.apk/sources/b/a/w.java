package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: IdTracking */
public class w implements au<w, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("IdTracking");
    /* access modifiers changed from: private */
    public static final bk f = new bk("snapshots", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("journals", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("checksum", (byte) 11, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, v> f584a;

    /* renamed from: b  reason: collision with root package name */
    public List<u> f585b;
    public String c;
    private e[] j = {e.JOURNALS, e.CHECKSUM};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.w$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.SNAPSHOTS, (Object) new bc("snapshots", (byte) 1, new bf((byte) 13, new bd((byte) 11), new bg((byte) 12, v.class))));
        enumMap.put((Object) e.JOURNALS, (Object) new bc("journals", (byte) 2, new be((byte) 15, new bg((byte) 12, u.class))));
        enumMap.put((Object) e.CHECKSUM, (Object) new bc("checksum", (byte) 2, new bd((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(w.class, d);
    }

    /* compiled from: IdTracking */
    public enum e implements ay {
        SNAPSHOTS(1, "snapshots"),
        JOURNALS(2, "journals"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public Map<String, v> a() {
        return this.f584a;
    }

    public w a(Map<String, v> map) {
        this.f584a = map;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f584a = null;
        }
    }

    public List<u> b() {
        return this.f585b;
    }

    public w a(List<u> list) {
        this.f585b = list;
        return this;
    }

    public boolean c() {
        return this.f585b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f585b = null;
        }
    }

    public boolean d() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdTracking(");
        sb.append("snapshots:");
        if (this.f584a == null) {
            sb.append("null");
        } else {
            sb.append(this.f584a);
        }
        if (c()) {
            sb.append(", ");
            sb.append("journals:");
            if (this.f585b == null) {
                sb.append("null");
            } else {
                sb.append(this.f585b);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("checksum:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ax {
        if (this.f584a == null) {
            throw new bo("Required field 'snapshots' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdTracking */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdTracking */
    private static class a extends bw<w> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, w wVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    wVar.e();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 13) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bm j = bnVar.j();
                            wVar.f584a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = bnVar.v();
                                v vVar = new v();
                                vVar.a(bnVar);
                                wVar.f584a.put(v, vVar);
                            }
                            bnVar.k();
                            wVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l = bnVar.l();
                            wVar.f585b = new ArrayList(l.f489b);
                            for (int i2 = 0; i2 < l.f489b; i2++) {
                                u uVar = new u();
                                uVar.a(bnVar);
                                wVar.f585b.add(uVar);
                            }
                            bnVar.m();
                            wVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            wVar.c = bnVar.v();
                            wVar.c(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, w wVar) throws ax {
            wVar.e();
            bnVar.a(w.e);
            if (wVar.f584a != null) {
                bnVar.a(w.f);
                bnVar.a(new bm((byte) 11, (byte) 12, wVar.f584a.size()));
                for (Map.Entry next : wVar.f584a.entrySet()) {
                    bnVar.a((String) next.getKey());
                    ((v) next.getValue()).b(bnVar);
                }
                bnVar.d();
                bnVar.b();
            }
            if (wVar.f585b != null && wVar.c()) {
                bnVar.a(w.g);
                bnVar.a(new bl((byte) 12, wVar.f585b.size()));
                for (u b2 : wVar.f585b) {
                    b2.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (wVar.c != null && wVar.d()) {
                bnVar.a(w.h);
                bnVar.a(wVar.c);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: IdTracking */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdTracking */
    private static class c extends bx<w> {
        private c() {
        }

        public void a(bn bnVar, w wVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(wVar.f584a.size());
            for (Map.Entry next : wVar.f584a.entrySet()) {
                btVar.a((String) next.getKey());
                ((v) next.getValue()).b(btVar);
            }
            BitSet bitSet = new BitSet();
            if (wVar.c()) {
                bitSet.set(0);
            }
            if (wVar.d()) {
                bitSet.set(1);
            }
            btVar.a(bitSet, 2);
            if (wVar.c()) {
                btVar.a(wVar.f585b.size());
                for (u b2 : wVar.f585b) {
                    b2.b(btVar);
                }
            }
            if (wVar.d()) {
                btVar.a(wVar.c);
            }
        }

        public void b(bn bnVar, w wVar) throws ax {
            bt btVar = (bt) bnVar;
            bm bmVar = new bm((byte) 11, (byte) 12, btVar.s());
            wVar.f584a = new HashMap(bmVar.c * 2);
            for (int i = 0; i < bmVar.c; i++) {
                String v = btVar.v();
                v vVar = new v();
                vVar.a(btVar);
                wVar.f584a.put(v, vVar);
            }
            wVar.a(true);
            BitSet b2 = btVar.b(2);
            if (b2.get(0)) {
                bl blVar = new bl((byte) 12, btVar.s());
                wVar.f585b = new ArrayList(blVar.f489b);
                for (int i2 = 0; i2 < blVar.f489b; i2++) {
                    u uVar = new u();
                    uVar.a(btVar);
                    wVar.f585b.add(uVar);
                }
                wVar.b(true);
            }
            if (b2.get(1)) {
                wVar.c = btVar.v();
                wVar.c(true);
            }
        }
    }
}
