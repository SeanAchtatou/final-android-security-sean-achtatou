package b.a;

import android.content.Context;
import android.text.TextUtils;
import com.xiaomi.mipush.sdk.MiPushClient;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* compiled from: IdTracker */
public class bi {

    /* renamed from: a  reason: collision with root package name */
    public static bi f479a;

    /* renamed from: b  reason: collision with root package name */
    private final String f480b = "umeng_it.cache";
    private File c;
    private w d = null;
    private long e;
    private long f;
    private Set<a> g = new HashSet();
    private a h = null;

    bi(Context context) {
        this.c = new File(context.getFilesDir(), "umeng_it.cache");
        this.f = LogBuilder.MAX_INTERVAL;
        this.h = new a(context);
        this.h.b();
    }

    public static synchronized bi a(Context context) {
        bi biVar;
        synchronized (bi.class) {
            if (f479a == null) {
                f479a = new bi(context);
                f479a.a(new cc(context));
                f479a.a(new ce(context));
                f479a.a(new x(context));
                f479a.a(new ch(context));
                f479a.a(new cg(context));
                f479a.a(new cf());
                f479a.d();
            }
            biVar = f479a;
        }
        return biVar;
    }

    public boolean a(a aVar) {
        if (this.h.a(aVar.b())) {
            return this.g.add(aVar);
        }
        return false;
    }

    public void a() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - this.e >= this.f) {
            boolean z = false;
            for (a next : this.g) {
                if (next.c()) {
                    if (next.a()) {
                        z = true;
                        if (!next.c()) {
                            this.h.b(next.b());
                        }
                    }
                    z = z;
                }
            }
            if (z) {
                f();
                this.h.a();
                e();
            }
            this.e = currentTimeMillis;
        }
    }

    public w b() {
        return this.d;
    }

    private void f() {
        w wVar = new w();
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        for (a next : this.g) {
            if (next.c()) {
                if (next.d() != null) {
                    hashMap.put(next.b(), next.d());
                }
                if (next.e() != null && !next.e().isEmpty()) {
                    arrayList.addAll(next.e());
                }
            }
        }
        wVar.a(arrayList);
        wVar.a(hashMap);
        synchronized (this) {
            this.d = wVar;
        }
    }

    public void c() {
        boolean z = false;
        for (a next : this.g) {
            if (next.c()) {
                if (next.e() != null && !next.e().isEmpty()) {
                    next.a((List<u>) null);
                    z = true;
                }
                z = z;
            }
        }
        if (z) {
            this.d.b(false);
            e();
        }
    }

    public void d() {
        w g2 = g();
        if (g2 != null) {
            ArrayList<a> arrayList = new ArrayList<>(this.g.size());
            synchronized (this) {
                this.d = g2;
                for (a next : this.g) {
                    next.a(this.d);
                    if (!next.c()) {
                        arrayList.add(next);
                    }
                }
                for (a remove : arrayList) {
                    this.g.remove(remove);
                }
            }
            f();
        }
    }

    public void e() {
        if (this.d != null) {
            a(this.d);
        }
    }

    private w g() {
        FileInputStream fileInputStream;
        Throwable th;
        if (!this.c.exists()) {
            return null;
        }
        try {
            fileInputStream = new FileInputStream(this.c);
            try {
                byte[] b2 = ar.b(fileInputStream);
                w wVar = new w();
                new aw().a(wVar, b2);
                ar.c(fileInputStream);
                return wVar;
            } catch (Exception e2) {
                e = e2;
                try {
                    e.printStackTrace();
                    ar.c(fileInputStream);
                    return null;
                } catch (Throwable th2) {
                    th = th2;
                    ar.c(fileInputStream);
                    throw th;
                }
            }
        } catch (Exception e3) {
            e = e3;
            fileInputStream = null;
            e.printStackTrace();
            ar.c(fileInputStream);
            return null;
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            ar.c(fileInputStream);
            throw th;
        }
    }

    private void a(w wVar) {
        byte[] a2;
        if (wVar != null) {
            try {
                synchronized (this) {
                    a2 = new az().a(wVar);
                }
                if (a2 != null) {
                    ar.a(this.c, a2);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /* compiled from: IdTracker */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private Context f481a;

        /* renamed from: b  reason: collision with root package name */
        private Set<String> f482b = new HashSet();

        public a(Context context) {
            this.f481a = context;
        }

        public boolean a(String str) {
            return !this.f482b.contains(str);
        }

        public void b(String str) {
            this.f482b.add(str);
        }

        public void a() {
            if (!this.f482b.isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (String append : this.f482b) {
                    sb.append(append);
                    sb.append(',');
                }
                sb.deleteCharAt(sb.length() - 1);
                cu.a(this.f481a).edit().putString("invld_id", sb.toString()).commit();
            }
        }

        public void b() {
            String[] split;
            String string = cu.a(this.f481a).getString("invld_id", null);
            if (!TextUtils.isEmpty(string) && (split = string.split(MiPushClient.ACCEPT_TIME_SEPARATOR)) != null) {
                for (String str : split) {
                    if (!TextUtils.isEmpty(str)) {
                        this.f482b.add(str);
                    }
                }
            }
        }
    }
}
