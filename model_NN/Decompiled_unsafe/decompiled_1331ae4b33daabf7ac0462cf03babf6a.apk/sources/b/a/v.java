package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdSnapshot */
public class v implements au<v, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("IdSnapshot");
    /* access modifiers changed from: private */
    public static final bk f = new bk("identity", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("ts", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("version", (byte) 8, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f580a;

    /* renamed from: b  reason: collision with root package name */
    public long f581b;
    public int c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.v$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.IDENTITY, (Object) new bc("identity", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.VERSION, (Object) new bc("version", (byte) 1, new bd((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(v.class, d);
    }

    /* compiled from: IdSnapshot */
    public enum e implements ay {
        IDENTITY(1, "identity"),
        TS(2, "ts"),
        VERSION(3, "version");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public String a() {
        return this.f580a;
    }

    public v a(String str) {
        this.f580a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f580a = null;
        }
    }

    public long b() {
        return this.f581b;
    }

    public v a(long j2) {
        this.f581b = j2;
        b(true);
        return this;
    }

    public boolean c() {
        return as.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public int d() {
        return this.c;
    }

    public v a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean e() {
        return as.a(this.j, 1);
    }

    public void c(boolean z) {
        this.j = as.a(this.j, 1, z);
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdSnapshot(");
        sb.append("identity:");
        if (this.f580a == null) {
            sb.append("null");
        } else {
            sb.append(this.f580a);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.f581b);
        sb.append(", ");
        sb.append("version:");
        sb.append(this.c);
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ax {
        if (this.f580a == null) {
            throw new bo("Required field 'identity' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdSnapshot */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class a extends bw<v> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, v vVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!vVar.c()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    } else if (!vVar.e()) {
                        throw new bo("Required field 'version' was not found in serialized data! Struct: " + toString());
                    } else {
                        vVar.f();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                vVar.f580a = bnVar.v();
                                vVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                vVar.f581b = bnVar.t();
                                vVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                vVar.c = bnVar.s();
                                vVar.c(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, v vVar) throws ax {
            vVar.f();
            bnVar.a(v.e);
            if (vVar.f580a != null) {
                bnVar.a(v.f);
                bnVar.a(vVar.f580a);
                bnVar.b();
            }
            bnVar.a(v.g);
            bnVar.a(vVar.f581b);
            bnVar.b();
            bnVar.a(v.h);
            bnVar.a(vVar.c);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: IdSnapshot */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdSnapshot */
    private static class c extends bx<v> {
        private c() {
        }

        public void a(bn bnVar, v vVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(vVar.f580a);
            btVar.a(vVar.f581b);
            btVar.a(vVar.c);
        }

        public void b(bn bnVar, v vVar) throws ax {
            bt btVar = (bt) bnVar;
            vVar.f580a = btVar.v();
            vVar.a(true);
            vVar.f581b = btVar.t();
            vVar.b(true);
            vVar.c = btVar.s();
            vVar.c(true);
        }
    }
}
