package b.a;

import android.content.Context;
import android.provider.Settings;

/* compiled from: AndroidIdTracker */
public class x extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f588a;

    public x(Context context) {
        super("android_id");
        this.f588a = context;
    }

    public String f() {
        try {
            return Settings.Secure.getString(this.f588a.getContentResolver(), "android_id");
        } catch (Exception e) {
            return null;
        }
    }
}
