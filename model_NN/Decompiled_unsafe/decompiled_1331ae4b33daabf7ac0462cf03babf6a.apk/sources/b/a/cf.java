package b.a;

import android.os.Build;

/* compiled from: SerialTracker */
public class cf extends a {
    public cf() {
        super("serial");
    }

    public String f() {
        if (Build.VERSION.SDK_INT >= 9) {
            return Build.SERIAL;
        }
        return null;
    }
}
