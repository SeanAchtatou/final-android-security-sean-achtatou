package b.a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: TIOStreamTransport */
public class by extends ca {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f497a = null;

    /* renamed from: b  reason: collision with root package name */
    protected OutputStream f498b = null;

    protected by() {
    }

    public by(OutputStream outputStream) {
        this.f498b = outputStream;
    }

    public int a(byte[] bArr, int i, int i2) throws cb {
        if (this.f497a == null) {
            throw new cb(1, "Cannot read from null inputStream");
        }
        try {
            int read = this.f497a.read(bArr, i, i2);
            if (read >= 0) {
                return read;
            }
            throw new cb(4);
        } catch (IOException e) {
            throw new cb(0, e);
        }
    }

    public void b(byte[] bArr, int i, int i2) throws cb {
        if (this.f498b == null) {
            throw new cb(1, "Cannot write to null outputStream");
        }
        try {
            this.f498b.write(bArr, i, i2);
        } catch (IOException e) {
            throw new cb(0, e);
        }
    }
}
