package b.a;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import org.apache.mina.proxy.handlers.socks.SocksProxyConstants;

/* compiled from: TBinaryProtocol */
public class bh extends bn {
    private static final bs f = new bs();

    /* renamed from: a  reason: collision with root package name */
    protected boolean f475a = false;

    /* renamed from: b  reason: collision with root package name */
    protected boolean f476b = true;
    protected int c;
    protected boolean d = false;
    private byte[] g = new byte[1];
    private byte[] h = new byte[2];
    private byte[] i = new byte[4];
    private byte[] j = new byte[8];
    private byte[] k = new byte[1];
    private byte[] l = new byte[2];
    private byte[] m = new byte[4];
    private byte[] n = new byte[8];

    /* compiled from: TBinaryProtocol */
    public static class a implements bp {

        /* renamed from: a  reason: collision with root package name */
        protected boolean f477a;

        /* renamed from: b  reason: collision with root package name */
        protected boolean f478b;
        protected int c;

        public a() {
            this(false, true);
        }

        public a(boolean z, boolean z2) {
            this(z, z2, 0);
        }

        public a(boolean z, boolean z2, int i) {
            this.f477a = false;
            this.f478b = true;
            this.f477a = z;
            this.f478b = z2;
            this.c = i;
        }

        public bn a(ca caVar) {
            bh bhVar = new bh(caVar, this.f477a, this.f478b);
            if (this.c != 0) {
                bhVar.c(this.c);
            }
            return bhVar;
        }
    }

    public bh(ca caVar, boolean z, boolean z2) {
        super(caVar);
        this.f475a = z;
        this.f476b = z2;
    }

    public void a(bs bsVar) {
    }

    public void a() {
    }

    public void a(bk bkVar) throws ax {
        a(bkVar.f487b);
        a(bkVar.c);
    }

    public void b() {
    }

    public void c() throws ax {
        a((byte) 0);
    }

    public void a(bm bmVar) throws ax {
        a(bmVar.f490a);
        a(bmVar.f491b);
        a(bmVar.c);
    }

    public void d() {
    }

    public void a(bl blVar) throws ax {
        a(blVar.f488a);
        a(blVar.f489b);
    }

    public void e() {
    }

    public void a(boolean z) throws ax {
        a(z ? (byte) 1 : 0);
    }

    public void a(byte b2) throws ax {
        this.g[0] = b2;
        this.e.b(this.g, 0, 1);
    }

    public void a(short s) throws ax {
        this.h[0] = (byte) ((s >> 8) & 255);
        this.h[1] = (byte) (s & 255);
        this.e.b(this.h, 0, 2);
    }

    public void a(int i2) throws ax {
        this.i[0] = (byte) ((i2 >> 24) & 255);
        this.i[1] = (byte) ((i2 >> 16) & 255);
        this.i[2] = (byte) ((i2 >> 8) & 255);
        this.i[3] = (byte) (i2 & 255);
        this.e.b(this.i, 0, 4);
    }

    public void a(long j2) throws ax {
        this.j[0] = (byte) ((int) ((j2 >> 56) & 255));
        this.j[1] = (byte) ((int) ((j2 >> 48) & 255));
        this.j[2] = (byte) ((int) ((j2 >> 40) & 255));
        this.j[3] = (byte) ((int) ((j2 >> 32) & 255));
        this.j[4] = (byte) ((int) ((j2 >> 24) & 255));
        this.j[5] = (byte) ((int) ((j2 >> 16) & 255));
        this.j[6] = (byte) ((int) ((j2 >> 8) & 255));
        this.j[7] = (byte) ((int) (255 & j2));
        this.e.b(this.j, 0, 8);
    }

    public void a(double d2) throws ax {
        a(Double.doubleToLongBits(d2));
    }

    public void a(String str) throws ax {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            a(bytes.length);
            this.e.b(bytes, 0, bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new ax("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public void a(ByteBuffer byteBuffer) throws ax {
        int limit = byteBuffer.limit() - byteBuffer.position();
        a(limit);
        this.e.b(byteBuffer.array(), byteBuffer.position() + byteBuffer.arrayOffset(), limit);
    }

    public bs f() {
        return f;
    }

    public void g() {
    }

    public bk h() throws ax {
        byte q = q();
        return new bk("", q, q == 0 ? 0 : r());
    }

    public void i() {
    }

    public bm j() throws ax {
        return new bm(q(), q(), s());
    }

    public void k() {
    }

    public bl l() throws ax {
        return new bl(q(), s());
    }

    public void m() {
    }

    public br n() throws ax {
        return new br(q(), s());
    }

    public void o() {
    }

    public boolean p() throws ax {
        return q() == 1;
    }

    public byte q() throws ax {
        if (this.e.d() >= 1) {
            byte b2 = this.e.b()[this.e.c()];
            this.e.a(1);
            return b2;
        }
        a(this.k, 0, 1);
        return this.k[0];
    }

    public short r() throws ax {
        int i2 = 0;
        byte[] bArr = this.l;
        if (this.e.d() >= 2) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(2);
        } else {
            a(this.l, 0, 2);
        }
        return (short) ((bArr[i2 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr[i2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8));
    }

    public int s() throws ax {
        int i2 = 0;
        byte[] bArr = this.m;
        if (this.e.d() >= 4) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(4);
        } else {
            a(this.m, 0, 4);
        }
        return (bArr[i2 + 3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) | ((bArr[i2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 24) | ((bArr[i2 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 16) | ((bArr[i2 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD) << 8);
    }

    public long t() throws ax {
        int i2 = 0;
        byte[] bArr = this.n;
        if (this.e.d() >= 8) {
            bArr = this.e.b();
            i2 = this.e.c();
            this.e.a(8);
        } else {
            a(this.n, 0, 8);
        }
        return ((long) (bArr[i2 + 7] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) | (((long) (bArr[i2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 56) | (((long) (bArr[i2 + 1] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 48) | (((long) (bArr[i2 + 2] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 40) | (((long) (bArr[i2 + 3] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 32) | (((long) (bArr[i2 + 4] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 24) | (((long) (bArr[i2 + 5] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 16) | (((long) (bArr[i2 + 6] & SocksProxyConstants.NO_ACCEPTABLE_AUTH_METHOD)) << 8);
    }

    public double u() throws ax {
        return Double.longBitsToDouble(t());
    }

    public String v() throws ax {
        int s = s();
        if (this.e.d() < s) {
            return b(s);
        }
        try {
            String str = new String(this.e.b(), this.e.c(), s, "UTF-8");
            this.e.a(s);
            return str;
        } catch (UnsupportedEncodingException e) {
            throw new ax("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public String b(int i2) throws ax {
        try {
            d(i2);
            byte[] bArr = new byte[i2];
            this.e.d(bArr, 0, i2);
            return new String(bArr, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ax("JVM DOES NOT SUPPORT UTF-8");
        }
    }

    public ByteBuffer w() throws ax {
        int s = s();
        d(s);
        if (this.e.d() >= s) {
            ByteBuffer wrap = ByteBuffer.wrap(this.e.b(), this.e.c(), s);
            this.e.a(s);
            return wrap;
        }
        byte[] bArr = new byte[s];
        this.e.d(bArr, 0, s);
        return ByteBuffer.wrap(bArr);
    }

    private int a(byte[] bArr, int i2, int i3) throws ax {
        d(i3);
        return this.e.d(bArr, i2, i3);
    }

    public void c(int i2) {
        this.c = i2;
        this.d = true;
    }

    /* access modifiers changed from: protected */
    public void d(int i2) throws ax {
        if (i2 < 0) {
            throw new bo("Negative length: " + i2);
        } else if (this.d) {
            this.c -= i2;
            if (this.c < 0) {
                throw new bo("Message length exceeded: " + i2);
            }
        }
    }
}
