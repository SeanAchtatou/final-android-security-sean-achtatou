package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Resolution */
public class ag implements au<ag, e>, Serializable, Cloneable {
    public static final Map<e, bc> c;
    /* access modifiers changed from: private */
    public static final bs d = new bs("Resolution");
    /* access modifiers changed from: private */
    public static final bk e = new bk("height", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk f = new bk("width", (byte) 8, 2);
    private static final Map<Class<? extends bu>, bv> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f424a;

    /* renamed from: b  reason: collision with root package name */
    public int f425b;
    private byte h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ag$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(bw.class, new b());
        g.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.HEIGHT, (Object) new bc("height", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.WIDTH, (Object) new bc("width", (byte) 1, new bd((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        bc.a(ag.class, c);
    }

    /* compiled from: Resolution */
    public enum e implements ay {
        HEIGHT(1, "height"),
        WIDTH(2, "width");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public ag() {
        this.h = 0;
    }

    public ag(int i, int i2) {
        this();
        this.f424a = i;
        a(true);
        this.f425b = i2;
        b(true);
    }

    public boolean a() {
        return as.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = as.a(this.h, 0, z);
    }

    public boolean b() {
        return as.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = as.a(this.h, 1, z);
    }

    public void a(bn bnVar) throws ax {
        g.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        g.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        return "Resolution(" + "height:" + this.f424a + ", " + "width:" + this.f425b + ")";
    }

    public void c() throws ax {
    }

    /* compiled from: Resolution */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Resolution */
    private static class a extends bw<ag> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ag agVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!agVar.a()) {
                        throw new bo("Required field 'height' was not found in serialized data! Struct: " + toString());
                    } else if (!agVar.b()) {
                        throw new bo("Required field 'width' was not found in serialized data! Struct: " + toString());
                    } else {
                        agVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                agVar.f424a = bnVar.s();
                                agVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                agVar.f425b = bnVar.s();
                                agVar.b(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ag agVar) throws ax {
            agVar.c();
            bnVar.a(ag.d);
            bnVar.a(ag.e);
            bnVar.a(agVar.f424a);
            bnVar.b();
            bnVar.a(ag.f);
            bnVar.a(agVar.f425b);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Resolution */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Resolution */
    private static class c extends bx<ag> {
        private c() {
        }

        public void a(bn bnVar, ag agVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(agVar.f424a);
            btVar.a(agVar.f425b);
        }

        public void b(bn bnVar, ag agVar) throws ax {
            bt btVar = (bt) bnVar;
            agVar.f424a = btVar.s();
            agVar.a(true);
            agVar.f425b = btVar.s();
            agVar.b(true);
        }
    }
}
