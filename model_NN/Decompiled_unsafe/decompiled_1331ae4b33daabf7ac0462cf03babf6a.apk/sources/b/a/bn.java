package b.a;

import java.nio.ByteBuffer;

/* compiled from: TProtocol */
public abstract class bn {
    protected ca e;

    public abstract void a() throws ax;

    public abstract void a(double d) throws ax;

    public abstract void a(int i) throws ax;

    public abstract void a(long j) throws ax;

    public abstract void a(bk bkVar) throws ax;

    public abstract void a(bl blVar) throws ax;

    public abstract void a(bm bmVar) throws ax;

    public abstract void a(bs bsVar) throws ax;

    public abstract void a(String str) throws ax;

    public abstract void a(ByteBuffer byteBuffer) throws ax;

    public abstract void a(short s) throws ax;

    public abstract void a(boolean z) throws ax;

    public abstract void b() throws ax;

    public abstract void c() throws ax;

    public abstract void d() throws ax;

    public abstract void e() throws ax;

    public abstract bs f() throws ax;

    public abstract void g() throws ax;

    public abstract bk h() throws ax;

    public abstract void i() throws ax;

    public abstract bm j() throws ax;

    public abstract void k() throws ax;

    public abstract bl l() throws ax;

    public abstract void m() throws ax;

    public abstract br n() throws ax;

    public abstract void o() throws ax;

    public abstract boolean p() throws ax;

    public abstract byte q() throws ax;

    public abstract short r() throws ax;

    public abstract int s() throws ax;

    public abstract long t() throws ax;

    public abstract double u() throws ax;

    public abstract String v() throws ax;

    public abstract ByteBuffer w() throws ax;

    protected bn(ca caVar) {
        this.e = caVar;
    }

    public void x() {
    }

    public Class<? extends bu> y() {
        return bw.class;
    }
}
