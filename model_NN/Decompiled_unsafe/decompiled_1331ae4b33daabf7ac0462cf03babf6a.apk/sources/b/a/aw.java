package b.a;

import b.a.bj;

/* compiled from: TDeserializer */
public class aw {

    /* renamed from: a  reason: collision with root package name */
    private final bn f459a;

    /* renamed from: b  reason: collision with root package name */
    private final bz f460b;

    public aw() {
        this(new bj.a());
    }

    public aw(bp bpVar) {
        this.f460b = new bz();
        this.f459a = bpVar.a(this.f460b);
    }

    public void a(au auVar, byte[] bArr) throws ax {
        try {
            this.f460b.a(bArr);
            auVar.a(this.f459a);
        } finally {
            this.f460b.a();
            this.f459a.x();
        }
    }
}
