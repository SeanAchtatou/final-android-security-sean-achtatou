package b.a;

import android.content.Context;
import android.telephony.TelephonyManager;

/* compiled from: ImeiTracker */
public class cc extends a {

    /* renamed from: a  reason: collision with root package name */
    private Context f504a;

    public cc(Context context) {
        super("imei");
        this.f504a = context;
    }

    public String f() {
        TelephonyManager telephonyManager = (TelephonyManager) this.f504a.getSystemService("phone");
        if (telephonyManager == null) {
        }
        try {
            if (an.a(this.f504a, "android.permission.READ_PHONE_STATE")) {
                return telephonyManager.getDeviceId();
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }
}
