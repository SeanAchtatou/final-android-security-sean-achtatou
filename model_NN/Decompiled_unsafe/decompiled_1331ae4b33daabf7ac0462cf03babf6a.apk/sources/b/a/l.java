package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ActivateMsg */
public class l implements au<l, e>, Serializable, Cloneable {

    /* renamed from: b  reason: collision with root package name */
    public static final Map<e, bc> f542b;
    /* access modifiers changed from: private */
    public static final bs c = new bs("ActivateMsg");
    /* access modifiers changed from: private */
    public static final bk d = new bk("ts", (byte) 10, 1);
    private static final Map<Class<? extends bu>, bv> e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f543a;
    private byte f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.l$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        e.put(bw.class, new b());
        e.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        f542b = Collections.unmodifiableMap(enumMap);
        bc.a(l.class, f542b);
    }

    /* compiled from: ActivateMsg */
    public enum e implements ay {
        TS(1, "ts");
        

        /* renamed from: b  reason: collision with root package name */
        private static final Map<String, e> f545b = new HashMap();
        private final short c;
        private final String d;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                f545b.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.c = s;
            this.d = str;
        }

        public short a() {
            return this.c;
        }

        public String b() {
            return this.d;
        }
    }

    public l() {
        this.f = 0;
    }

    public l(long j) {
        this();
        this.f543a = j;
        a(true);
    }

    public boolean a() {
        return as.a(this.f, 0);
    }

    public void a(boolean z) {
        this.f = as.a(this.f, 0, z);
    }

    public void a(bn bnVar) throws ax {
        e.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        e.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        return "ActivateMsg(" + "ts:" + this.f543a + ")";
    }

    public void b() throws ax {
    }

    /* compiled from: ActivateMsg */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ActivateMsg */
    private static class a extends bw<l> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, l lVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!lVar.a()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    lVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            lVar.f543a = bnVar.t();
                            lVar.a(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, l lVar) throws ax {
            lVar.b();
            bnVar.a(l.c);
            bnVar.a(l.d);
            bnVar.a(lVar.f543a);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: ActivateMsg */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ActivateMsg */
    private static class c extends bx<l> {
        private c() {
        }

        public void a(bn bnVar, l lVar) throws ax {
            ((bt) bnVar).a(lVar.f543a);
        }

        public void b(bn bnVar, l lVar) throws ax {
            lVar.f543a = ((bt) bnVar).t();
            lVar.a(true);
        }
    }
}
