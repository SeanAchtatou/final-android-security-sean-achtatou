package b.a;

import android.content.Context;
import com.umeng.analytics.i;
import com.umeng.analytics.j;

/* compiled from: CacheService */
public final class cj implements cn {
    private static cj c;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public cn f516a = new ci(this.f517b);

    /* renamed from: b  reason: collision with root package name */
    private Context f517b;

    private cj(Context context) {
        this.f517b = context.getApplicationContext();
    }

    public static synchronized cj a(Context context) {
        cj cjVar;
        synchronized (cj.class) {
            if (c == null && context != null) {
                c = new cj(context);
            }
            cjVar = c;
        }
        return cjVar;
    }

    public void a(final co coVar) {
        i.b(new j() {
            public void a() {
                cj.this.f516a.a(coVar);
            }
        });
    }

    public void b(co coVar) {
        this.f516a.b(coVar);
    }

    public void b() {
        i.b(new j() {
            public void a() {
                cj.this.f516a.b();
            }
        });
    }
}
