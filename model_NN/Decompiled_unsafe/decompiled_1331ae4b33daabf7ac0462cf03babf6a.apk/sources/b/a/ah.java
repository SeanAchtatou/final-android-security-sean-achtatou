package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Response */
public class ah implements au<ah, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("Response");
    /* access modifiers changed from: private */
    public static final bk f = new bk("resp_code", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("msg", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("imprint", (byte) 12, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f428a;

    /* renamed from: b  reason: collision with root package name */
    public String f429b;
    public y c;
    private byte j = 0;
    private e[] k = {e.MSG, e.IMPRINT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ah$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.RESP_CODE, (Object) new bc("resp_code", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.MSG, (Object) new bc("msg", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.IMPRINT, (Object) new bc("imprint", (byte) 2, new bg((byte) 12, y.class)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(ah.class, d);
    }

    /* compiled from: Response */
    public enum e implements ay {
        RESP_CODE(1, "resp_code"),
        MSG(2, "msg"),
        IMPRINT(3, "imprint");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public boolean a() {
        return as.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public String b() {
        return this.f429b;
    }

    public boolean c() {
        return this.f429b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f429b = null;
        }
    }

    public y d() {
        return this.c;
    }

    public boolean e() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Response(");
        sb.append("resp_code:");
        sb.append(this.f428a);
        if (c()) {
            sb.append(", ");
            sb.append("msg:");
            if (this.f429b == null) {
                sb.append("null");
            } else {
                sb.append(this.f429b);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ax {
        if (this.c != null) {
            this.c.f();
        }
    }

    /* compiled from: Response */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Response */
    private static class a extends bw<ah> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ah ahVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!ahVar.a()) {
                        throw new bo("Required field 'resp_code' was not found in serialized data! Struct: " + toString());
                    }
                    ahVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            ahVar.f428a = bnVar.s();
                            ahVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            ahVar.f429b = bnVar.v();
                            ahVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            ahVar.c = new y();
                            ahVar.c.a(bnVar);
                            ahVar.c(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ah ahVar) throws ax {
            ahVar.f();
            bnVar.a(ah.e);
            bnVar.a(ah.f);
            bnVar.a(ahVar.f428a);
            bnVar.b();
            if (ahVar.f429b != null && ahVar.c()) {
                bnVar.a(ah.g);
                bnVar.a(ahVar.f429b);
                bnVar.b();
            }
            if (ahVar.c != null && ahVar.e()) {
                bnVar.a(ah.h);
                ahVar.c.b(bnVar);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Response */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Response */
    private static class c extends bx<ah> {
        private c() {
        }

        public void a(bn bnVar, ah ahVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(ahVar.f428a);
            BitSet bitSet = new BitSet();
            if (ahVar.c()) {
                bitSet.set(0);
            }
            if (ahVar.e()) {
                bitSet.set(1);
            }
            btVar.a(bitSet, 2);
            if (ahVar.c()) {
                btVar.a(ahVar.f429b);
            }
            if (ahVar.e()) {
                ahVar.c.b(btVar);
            }
        }

        public void b(bn bnVar, ah ahVar) throws ax {
            bt btVar = (bt) bnVar;
            ahVar.f428a = btVar.s();
            ahVar.a(true);
            BitSet b2 = btVar.b(2);
            if (b2.get(0)) {
                ahVar.f429b = btVar.v();
                ahVar.b(true);
            }
            if (b2.get(1)) {
                ahVar.c = new y();
                ahVar.c.a(btVar);
                ahVar.c(true);
            }
        }
    }
}
