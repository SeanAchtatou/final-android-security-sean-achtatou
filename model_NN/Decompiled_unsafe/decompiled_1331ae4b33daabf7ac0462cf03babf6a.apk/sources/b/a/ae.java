package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Page */
public class ae implements au<ae, e>, Serializable, Cloneable {
    public static final Map<e, bc> c;
    /* access modifiers changed from: private */
    public static final bs d = new bs("Page");
    /* access modifiers changed from: private */
    public static final bk e = new bk("page_name", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk f = new bk("duration", (byte) 10, 2);
    private static final Map<Class<? extends bu>, bv> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f416a;

    /* renamed from: b  reason: collision with root package name */
    public long f417b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ae$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(bw.class, new b());
        g.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PAGE_NAME, (Object) new bc("page_name", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.DURATION, (Object) new bc("duration", (byte) 1, new bd((byte) 10)));
        c = Collections.unmodifiableMap(enumMap);
        bc.a(ae.class, c);
    }

    /* compiled from: Page */
    public enum e implements ay {
        PAGE_NAME(1, "page_name"),
        DURATION(2, "duration");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public ae a(String str) {
        this.f416a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f416a = null;
        }
    }

    public ae a(long j) {
        this.f417b = j;
        b(true);
        return this;
    }

    public boolean a() {
        return as.a(this.h, 0);
    }

    public void b(boolean z) {
        this.h = as.a(this.h, 0, z);
    }

    public void a(bn bnVar) throws ax {
        g.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        g.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Page(");
        sb.append("page_name:");
        if (this.f416a == null) {
            sb.append("null");
        } else {
            sb.append(this.f416a);
        }
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.f417b);
        sb.append(")");
        return sb.toString();
    }

    public void b() throws ax {
        if (this.f416a == null) {
            throw new bo("Required field 'page_name' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Page */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Page */
    private static class a extends bw<ae> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ae aeVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!aeVar.a()) {
                        throw new bo("Required field 'duration' was not found in serialized data! Struct: " + toString());
                    }
                    aeVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            aeVar.f416a = bnVar.v();
                            aeVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            aeVar.f417b = bnVar.t();
                            aeVar.b(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ae aeVar) throws ax {
            aeVar.b();
            bnVar.a(ae.d);
            if (aeVar.f416a != null) {
                bnVar.a(ae.e);
                bnVar.a(aeVar.f416a);
                bnVar.b();
            }
            bnVar.a(ae.f);
            bnVar.a(aeVar.f417b);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Page */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Page */
    private static class c extends bx<ae> {
        private c() {
        }

        public void a(bn bnVar, ae aeVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(aeVar.f416a);
            btVar.a(aeVar.f417b);
        }

        public void b(bn bnVar, ae aeVar) throws ax {
            bt btVar = (bt) bnVar;
            aeVar.f416a = btVar.v();
            aeVar.a(true);
            aeVar.f417b = btVar.t();
            aeVar.b(true);
        }
    }
}
