package b.a;

import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: PropertyValue */
public class af extends ba<af, a> {

    /* renamed from: a  reason: collision with root package name */
    public static final Map<a, bc> f420a;
    private static final bs d = new bs("PropertyValue");
    private static final bk e = new bk("string_value", (byte) 11, 1);
    private static final bk f = new bk("long_value", (byte) 10, 2);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.af$a, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        EnumMap enumMap = new EnumMap(a.class);
        enumMap.put((Object) a.STRING_VALUE, (Object) new bc("string_value", (byte) 3, new bd((byte) 11)));
        enumMap.put((Object) a.LONG_VALUE, (Object) new bc("long_value", (byte) 3, new bd((byte) 10)));
        f420a = Collections.unmodifiableMap(enumMap);
        bc.a(af.class, f420a);
    }

    /* compiled from: PropertyValue */
    public enum a implements ay {
        STRING_VALUE(1, "string_value"),
        LONG_VALUE(2, "long_value");
        
        private static final Map<String, a> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(a.class).iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                c.put(aVar.b(), aVar);
            }
        }

        public static a a(int i) {
            switch (i) {
                case 1:
                    return STRING_VALUE;
                case 2:
                    return LONG_VALUE;
                default:
                    return null;
            }
        }

        public static a b(int i) {
            a a2 = a(i);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException("Field " + i + " doesn't exist!");
        }

        private a(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    /* access modifiers changed from: protected */
    public Object a(bn bnVar, bk bkVar) throws ax {
        a a2 = a.a(bkVar.c);
        if (a2 == null) {
            return null;
        }
        switch (a2) {
            case STRING_VALUE:
                if (bkVar.f487b == e.f487b) {
                    return bnVar.v();
                }
                bq.a(bnVar, bkVar.f487b);
                return null;
            case LONG_VALUE:
                if (bkVar.f487b == f.f487b) {
                    return Long.valueOf(bnVar.t());
                }
                bq.a(bnVar, bkVar.f487b);
                return null;
            default:
                throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
        }
    }

    /* access modifiers changed from: protected */
    public void c(bn bnVar) throws ax {
        switch ((a) this.c) {
            case STRING_VALUE:
                bnVar.a((String) this.f466b);
                return;
            case LONG_VALUE:
                bnVar.a(((Long) this.f466b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    /* access modifiers changed from: protected */
    public Object a(bn bnVar, short s) throws ax {
        a a2 = a.a(s);
        if (a2 != null) {
            switch (a2) {
                case STRING_VALUE:
                    return bnVar.v();
                case LONG_VALUE:
                    return Long.valueOf(bnVar.t());
                default:
                    throw new IllegalStateException("setField wasn't null, but didn't match any of the case statements!");
            }
        } else {
            throw new bo("Couldn't find a field with field id " + ((int) s));
        }
    }

    /* access modifiers changed from: protected */
    public void d(bn bnVar) throws ax {
        switch ((a) this.c) {
            case STRING_VALUE:
                bnVar.a((String) this.f466b);
                return;
            case LONG_VALUE:
                bnVar.a(((Long) this.f466b).longValue());
                return;
            default:
                throw new IllegalStateException("Cannot write union with unknown field " + this.c);
        }
    }

    /* access modifiers changed from: protected */
    public bk a(a aVar) {
        switch (aVar) {
            case STRING_VALUE:
                return e;
            case LONG_VALUE:
                return f;
            default:
                throw new IllegalArgumentException("Unknown field id " + aVar);
        }
    }

    /* access modifiers changed from: protected */
    public bs a() {
        return d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public a b(short s) {
        return a.b(s);
    }

    public void a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.c = a.STRING_VALUE;
        this.f466b = str;
    }

    public void a(long j) {
        this.c = a.LONG_VALUE;
        this.f466b = Long.valueOf(j);
    }

    public boolean equals(Object obj) {
        if (obj instanceof af) {
            return a((af) obj);
        }
        return false;
    }

    public boolean a(af afVar) {
        return afVar != null && b() == afVar.b() && c().equals(afVar.c());
    }

    public int hashCode() {
        return 0;
    }
}
