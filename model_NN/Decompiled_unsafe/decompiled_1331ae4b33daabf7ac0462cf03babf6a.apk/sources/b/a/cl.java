package b.a;

import com.umeng.analytics.a;
import java.lang.Thread;

/* compiled from: CrashHandler */
public class cl implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private Thread.UncaughtExceptionHandler f523a;

    /* renamed from: b  reason: collision with root package name */
    private cs f524b;

    public cl() {
        if (Thread.getDefaultUncaughtExceptionHandler() != this) {
            this.f523a = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void a(cs csVar) {
        this.f524b = csVar;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        a(th);
        if (this.f523a != null && this.f523a != Thread.getDefaultUncaughtExceptionHandler()) {
            this.f523a.uncaughtException(thread, th);
        }
    }

    private void a(Throwable th) {
        if (a.k) {
            this.f524b.a(th);
        } else {
            this.f524b.a(null);
        }
    }
}
