package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Location */
public class ac implements au<ac, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("Location");
    /* access modifiers changed from: private */
    public static final bk f = new bk("lat", (byte) 4, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("lng", (byte) 4, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("ts", (byte) 10, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public double f408a;

    /* renamed from: b  reason: collision with root package name */
    public double f409b;
    public long c;
    private byte j;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ac$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LAT, (Object) new bc("lat", (byte) 1, new bd((byte) 4)));
        enumMap.put((Object) e.LNG, (Object) new bc("lng", (byte) 1, new bd((byte) 4)));
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(ac.class, d);
    }

    /* compiled from: Location */
    public enum e implements ay {
        LAT(1, "lat"),
        LNG(2, "lng"),
        TS(3, "ts");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public ac() {
        this.j = 0;
    }

    public ac(double d2, double d3, long j2) {
        this();
        this.f408a = d2;
        a(true);
        this.f409b = d3;
        b(true);
        this.c = j2;
        c(true);
    }

    public boolean a() {
        return as.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public boolean b() {
        return as.a(this.j, 1);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 1, z);
    }

    public boolean c() {
        return as.a(this.j, 2);
    }

    public void c(boolean z) {
        this.j = as.a(this.j, 2, z);
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        return "Location(" + "lat:" + this.f408a + ", " + "lng:" + this.f409b + ", " + "ts:" + this.c + ")";
    }

    public void d() throws ax {
    }

    /* compiled from: Location */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Location */
    private static class a extends bw<ac> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ac acVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!acVar.a()) {
                        throw new bo("Required field 'lat' was not found in serialized data! Struct: " + toString());
                    } else if (!acVar.b()) {
                        throw new bo("Required field 'lng' was not found in serialized data! Struct: " + toString());
                    } else if (!acVar.c()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    } else {
                        acVar.d();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 4) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                acVar.f408a = bnVar.u();
                                acVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 4) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                acVar.f409b = bnVar.u();
                                acVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                acVar.c = bnVar.t();
                                acVar.c(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ac acVar) throws ax {
            acVar.d();
            bnVar.a(ac.e);
            bnVar.a(ac.f);
            bnVar.a(acVar.f408a);
            bnVar.b();
            bnVar.a(ac.g);
            bnVar.a(acVar.f409b);
            bnVar.b();
            bnVar.a(ac.h);
            bnVar.a(acVar.c);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Location */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Location */
    private static class c extends bx<ac> {
        private c() {
        }

        public void a(bn bnVar, ac acVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(acVar.f408a);
            btVar.a(acVar.f409b);
            btVar.a(acVar.c);
        }

        public void b(bn bnVar, ac acVar) throws ax {
            bt btVar = (bt) bnVar;
            acVar.f408a = btVar.u();
            acVar.a(true);
            acVar.f409b = btVar.u();
            acVar.b(true);
            acVar.c = btVar.t();
            acVar.c(true);
        }
    }
}
