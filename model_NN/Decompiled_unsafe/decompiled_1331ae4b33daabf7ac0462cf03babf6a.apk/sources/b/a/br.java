package b.a;

/* compiled from: TSet */
public final class br {

    /* renamed from: a  reason: collision with root package name */
    public final byte f494a;

    /* renamed from: b  reason: collision with root package name */
    public final int f495b;

    public br() {
        this((byte) 0, 0);
    }

    public br(byte b2, int i) {
        this.f494a = b2;
        this.f495b = i;
    }

    public br(bl blVar) {
        this(blVar.f488a, blVar.f489b);
    }
}
