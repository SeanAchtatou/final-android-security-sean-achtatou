package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Traffic */
public class ak implements au<ak, e>, Serializable, Cloneable {
    public static final Map<e, bc> c;
    /* access modifiers changed from: private */
    public static final bs d = new bs("Traffic");
    /* access modifiers changed from: private */
    public static final bk e = new bk("upload_traffic", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk f = new bk("download_traffic", (byte) 8, 2);
    private static final Map<Class<? extends bu>, bv> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f438a;

    /* renamed from: b  reason: collision with root package name */
    public int f439b;
    private byte h = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ak$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(bw.class, new b());
        g.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.UPLOAD_TRAFFIC, (Object) new bc("upload_traffic", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.DOWNLOAD_TRAFFIC, (Object) new bc("download_traffic", (byte) 1, new bd((byte) 8)));
        c = Collections.unmodifiableMap(enumMap);
        bc.a(ak.class, c);
    }

    /* compiled from: Traffic */
    public enum e implements ay {
        UPLOAD_TRAFFIC(1, "upload_traffic"),
        DOWNLOAD_TRAFFIC(2, "download_traffic");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public ak a(int i) {
        this.f438a = i;
        a(true);
        return this;
    }

    public boolean a() {
        return as.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = as.a(this.h, 0, z);
    }

    public ak b(int i) {
        this.f439b = i;
        b(true);
        return this;
    }

    public boolean b() {
        return as.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = as.a(this.h, 1, z);
    }

    public void a(bn bnVar) throws ax {
        g.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        g.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        return "Traffic(" + "upload_traffic:" + this.f438a + ", " + "download_traffic:" + this.f439b + ")";
    }

    public void c() throws ax {
    }

    /* compiled from: Traffic */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Traffic */
    private static class a extends bw<ak> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ak akVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!akVar.a()) {
                        throw new bo("Required field 'upload_traffic' was not found in serialized data! Struct: " + toString());
                    } else if (!akVar.b()) {
                        throw new bo("Required field 'download_traffic' was not found in serialized data! Struct: " + toString());
                    } else {
                        akVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                akVar.f438a = bnVar.s();
                                akVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                akVar.f439b = bnVar.s();
                                akVar.b(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ak akVar) throws ax {
            akVar.c();
            bnVar.a(ak.d);
            bnVar.a(ak.e);
            bnVar.a(akVar.f438a);
            bnVar.b();
            bnVar.a(ak.f);
            bnVar.a(akVar.f439b);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Traffic */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Traffic */
    private static class c extends bx<ak> {
        private c() {
        }

        public void a(bn bnVar, ak akVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(akVar.f438a);
            btVar.a(akVar.f439b);
        }

        public void b(bn bnVar, ak akVar) throws ax {
            bt btVar = (bt) bnVar;
            akVar.f438a = btVar.s();
            akVar.a(true);
            akVar.f439b = btVar.s();
            akVar.b(true);
        }
    }
}
