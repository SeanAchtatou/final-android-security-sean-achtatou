package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: IdJournal */
public class u implements au<u, e>, Serializable, Cloneable {
    public static final Map<e, bc> e;
    /* access modifiers changed from: private */
    public static final bs f = new bs("IdJournal");
    /* access modifiers changed from: private */
    public static final bk g = new bk("domain", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk h = new bk("old_id", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk i = new bk("new_id", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final bk j = new bk("ts", (byte) 10, 4);
    private static final Map<Class<? extends bu>, bv> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f576a;

    /* renamed from: b  reason: collision with root package name */
    public String f577b;
    public String c;
    public long d;
    private byte l = 0;
    private e[] m = {e.OLD_ID};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.u$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(bw.class, new b());
        k.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.DOMAIN, (Object) new bc("domain", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.OLD_ID, (Object) new bc("old_id", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.NEW_ID, (Object) new bc("new_id", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        e = Collections.unmodifiableMap(enumMap);
        bc.a(u.class, e);
    }

    /* compiled from: IdJournal */
    public enum e implements ay {
        DOMAIN(1, "domain"),
        OLD_ID(2, "old_id"),
        NEW_ID(3, "new_id"),
        TS(4, "ts");
        
        private static final Map<String, e> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                e.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public short a() {
            return this.f;
        }

        public String b() {
            return this.g;
        }
    }

    public u a(String str) {
        this.f576a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f576a = null;
        }
    }

    public u b(String str) {
        this.f577b = str;
        return this;
    }

    public boolean a() {
        return this.f577b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f577b = null;
        }
    }

    public u c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public u a(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public boolean b() {
        return as.a(this.l, 0);
    }

    public void d(boolean z) {
        this.l = as.a(this.l, 0, z);
    }

    public void a(bn bnVar) throws ax {
        k.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        k.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("IdJournal(");
        sb.append("domain:");
        if (this.f576a == null) {
            sb.append("null");
        } else {
            sb.append(this.f576a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("old_id:");
            if (this.f577b == null) {
                sb.append("null");
            } else {
                sb.append(this.f577b);
            }
        }
        sb.append(", ");
        sb.append("new_id:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.d);
        sb.append(")");
        return sb.toString();
    }

    public void c() throws ax {
        if (this.f576a == null) {
            throw new bo("Required field 'domain' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bo("Required field 'new_id' was not present! Struct: " + toString());
        }
    }

    /* compiled from: IdJournal */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: IdJournal */
    private static class a extends bw<u> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, u uVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!uVar.b()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    uVar.c();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            uVar.f576a = bnVar.v();
                            uVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            uVar.f577b = bnVar.v();
                            uVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            uVar.c = bnVar.v();
                            uVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            uVar.d = bnVar.t();
                            uVar.d(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, u uVar) throws ax {
            uVar.c();
            bnVar.a(u.f);
            if (uVar.f576a != null) {
                bnVar.a(u.g);
                bnVar.a(uVar.f576a);
                bnVar.b();
            }
            if (uVar.f577b != null && uVar.a()) {
                bnVar.a(u.h);
                bnVar.a(uVar.f577b);
                bnVar.b();
            }
            if (uVar.c != null) {
                bnVar.a(u.i);
                bnVar.a(uVar.c);
                bnVar.b();
            }
            bnVar.a(u.j);
            bnVar.a(uVar.d);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: IdJournal */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: IdJournal */
    private static class c extends bx<u> {
        private c() {
        }

        public void a(bn bnVar, u uVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(uVar.f576a);
            btVar.a(uVar.c);
            btVar.a(uVar.d);
            BitSet bitSet = new BitSet();
            if (uVar.a()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (uVar.a()) {
                btVar.a(uVar.f577b);
            }
        }

        public void b(bn bnVar, u uVar) throws ax {
            bt btVar = (bt) bnVar;
            uVar.f576a = btVar.v();
            uVar.a(true);
            uVar.c = btVar.v();
            uVar.c(true);
            uVar.d = btVar.t();
            uVar.d(true);
            if (btVar.b(1).get(0)) {
                uVar.f577b = btVar.v();
                uVar.b(true);
            }
        }
    }
}
