package b.a;

/* compiled from: TProtocolUtil */
public class bq {

    /* renamed from: a  reason: collision with root package name */
    private static int f493a = Integer.MAX_VALUE;

    public static void a(bn bnVar, byte b2) throws ax {
        a(bnVar, b2, f493a);
    }

    public static void a(bn bnVar, byte b2, int i) throws ax {
        int i2 = 0;
        if (i <= 0) {
            throw new ax("Maximum skip depth exceeded");
        }
        switch (b2) {
            case 2:
                bnVar.p();
                return;
            case 3:
                bnVar.q();
                return;
            case 4:
                bnVar.u();
                return;
            case 5:
            case 7:
            case 9:
            default:
                return;
            case 6:
                bnVar.r();
                return;
            case 8:
                bnVar.s();
                return;
            case 10:
                bnVar.t();
                return;
            case 11:
                bnVar.w();
                return;
            case 12:
                bnVar.f();
                while (true) {
                    bk h = bnVar.h();
                    if (h.f487b == 0) {
                        bnVar.g();
                        return;
                    } else {
                        a(bnVar, h.f487b, i - 1);
                        bnVar.i();
                    }
                }
            case 13:
                bm j = bnVar.j();
                while (i2 < j.c) {
                    a(bnVar, j.f490a, i - 1);
                    a(bnVar, j.f491b, i - 1);
                    i2++;
                }
                bnVar.k();
                return;
            case 14:
                br n = bnVar.n();
                while (i2 < n.f495b) {
                    a(bnVar, n.f494a, i - 1);
                    i2++;
                }
                bnVar.o();
                return;
            case 15:
                bl l = bnVar.l();
                while (i2 < l.f489b) {
                    a(bnVar, l.f488a, i - 1);
                    i2++;
                }
                bnVar.m();
                return;
        }
    }
}
