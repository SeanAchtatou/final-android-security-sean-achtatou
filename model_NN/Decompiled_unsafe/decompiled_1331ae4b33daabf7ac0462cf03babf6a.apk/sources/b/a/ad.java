package b.a;

import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: MiscInfo */
public class ad implements au<ad, e>, Serializable, Cloneable {
    public static final Map<e, bc> k;
    /* access modifiers changed from: private */
    public static final bs l = new bs("MiscInfo");
    /* access modifiers changed from: private */
    public static final bk m = new bk("time_zone", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk n = new bk("language", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk o = new bk("country", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final bk p = new bk("latitude", (byte) 4, 4);
    /* access modifiers changed from: private */
    public static final bk q = new bk("longitude", (byte) 4, 5);
    /* access modifiers changed from: private */
    public static final bk r = new bk(Parameters.CARRIER, (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final bk s = new bk("latency", (byte) 8, 7);
    /* access modifiers changed from: private */
    public static final bk t = new bk("display_name", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final bk u = new bk("access_type", (byte) 8, 9);
    /* access modifiers changed from: private */
    public static final bk v = new bk("access_subtype", (byte) 11, 10);
    private static final Map<Class<? extends bu>, bv> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f412a;

    /* renamed from: b  reason: collision with root package name */
    public String f413b;
    public String c;
    public double d;
    public double e;
    public String f;
    public int g;
    public String h;
    public k i;
    public String j;
    private byte x = 0;
    private e[] y = {e.TIME_ZONE, e.LANGUAGE, e.COUNTRY, e.LATITUDE, e.LONGITUDE, e.CARRIER, e.LATENCY, e.DISPLAY_NAME, e.ACCESS_TYPE, e.ACCESS_SUBTYPE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ad$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(bw.class, new b());
        w.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TIME_ZONE, (Object) new bc("time_zone", (byte) 2, new bd((byte) 8)));
        enumMap.put((Object) e.LANGUAGE, (Object) new bc("language", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.COUNTRY, (Object) new bc("country", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.LATITUDE, (Object) new bc("latitude", (byte) 2, new bd((byte) 4)));
        enumMap.put((Object) e.LONGITUDE, (Object) new bc("longitude", (byte) 2, new bd((byte) 4)));
        enumMap.put((Object) e.CARRIER, (Object) new bc(Parameters.CARRIER, (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.LATENCY, (Object) new bc("latency", (byte) 2, new bd((byte) 8)));
        enumMap.put((Object) e.DISPLAY_NAME, (Object) new bc("display_name", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.ACCESS_TYPE, (Object) new bc("access_type", (byte) 2, new bb((byte) 16, k.class)));
        enumMap.put((Object) e.ACCESS_SUBTYPE, (Object) new bc("access_subtype", (byte) 2, new bd((byte) 11)));
        k = Collections.unmodifiableMap(enumMap);
        bc.a(ad.class, k);
    }

    /* compiled from: MiscInfo */
    public enum e implements ay {
        TIME_ZONE(1, "time_zone"),
        LANGUAGE(2, "language"),
        COUNTRY(3, "country"),
        LATITUDE(4, "latitude"),
        LONGITUDE(5, "longitude"),
        CARRIER(6, Parameters.CARRIER),
        LATENCY(7, "latency"),
        DISPLAY_NAME(8, "display_name"),
        ACCESS_TYPE(9, "access_type"),
        ACCESS_SUBTYPE(10, "access_subtype");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public ad a(int i2) {
        this.f412a = i2;
        a(true);
        return this;
    }

    public boolean a() {
        return as.a(this.x, 0);
    }

    public void a(boolean z) {
        this.x = as.a(this.x, 0, z);
    }

    public ad a(String str) {
        this.f413b = str;
        return this;
    }

    public boolean b() {
        return this.f413b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f413b = null;
        }
    }

    public ad b(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean d() {
        return as.a(this.x, 1);
    }

    public void d(boolean z) {
        this.x = as.a(this.x, 1, z);
    }

    public boolean e() {
        return as.a(this.x, 2);
    }

    public void e(boolean z) {
        this.x = as.a(this.x, 2, z);
    }

    public ad c(String str) {
        this.f = str;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public boolean g() {
        return as.a(this.x, 3);
    }

    public void g(boolean z) {
        this.x = as.a(this.x, 3, z);
    }

    public boolean h() {
        return this.h != null;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public ad a(k kVar) {
        this.i = kVar;
        return this;
    }

    public boolean i() {
        return this.i != null;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public ad d(String str) {
        this.j = str;
        return this;
    }

    public boolean j() {
        return this.j != null;
    }

    public void j(boolean z) {
        if (!z) {
            this.j = null;
        }
    }

    public void a(bn bnVar) throws ax {
        w.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        w.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        boolean z = false;
        StringBuilder sb = new StringBuilder("MiscInfo(");
        boolean z2 = true;
        if (a()) {
            sb.append("time_zone:");
            sb.append(this.f412a);
            z2 = false;
        }
        if (b()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("language:");
            if (this.f413b == null) {
                sb.append("null");
            } else {
                sb.append(this.f413b);
            }
            z2 = false;
        }
        if (c()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("country:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z2 = false;
        }
        if (d()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("latitude:");
            sb.append(this.d);
            z2 = false;
        }
        if (e()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("longitude:");
            sb.append(this.e);
            z2 = false;
        }
        if (f()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("carrier:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z2 = false;
        }
        if (g()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("latency:");
            sb.append(this.g);
            z2 = false;
        }
        if (h()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("display_name:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z2 = false;
        }
        if (i()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("access_type:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        } else {
            z = z2;
        }
        if (j()) {
            if (!z) {
                sb.append(", ");
            }
            sb.append("access_subtype:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void k() throws ax {
    }

    /* compiled from: MiscInfo */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: MiscInfo */
    private static class a extends bw<ad> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ad adVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    adVar.k();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.f412a = bnVar.s();
                            adVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.f413b = bnVar.v();
                            adVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.c = bnVar.v();
                            adVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 4) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.d = bnVar.u();
                            adVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f487b != 4) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.e = bnVar.u();
                            adVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.f = bnVar.v();
                            adVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.g = bnVar.s();
                            adVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.h = bnVar.v();
                            adVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.i = k.a(bnVar.s());
                            adVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            adVar.j = bnVar.v();
                            adVar.j(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ad adVar) throws ax {
            adVar.k();
            bnVar.a(ad.l);
            if (adVar.a()) {
                bnVar.a(ad.m);
                bnVar.a(adVar.f412a);
                bnVar.b();
            }
            if (adVar.f413b != null && adVar.b()) {
                bnVar.a(ad.n);
                bnVar.a(adVar.f413b);
                bnVar.b();
            }
            if (adVar.c != null && adVar.c()) {
                bnVar.a(ad.o);
                bnVar.a(adVar.c);
                bnVar.b();
            }
            if (adVar.d()) {
                bnVar.a(ad.p);
                bnVar.a(adVar.d);
                bnVar.b();
            }
            if (adVar.e()) {
                bnVar.a(ad.q);
                bnVar.a(adVar.e);
                bnVar.b();
            }
            if (adVar.f != null && adVar.f()) {
                bnVar.a(ad.r);
                bnVar.a(adVar.f);
                bnVar.b();
            }
            if (adVar.g()) {
                bnVar.a(ad.s);
                bnVar.a(adVar.g);
                bnVar.b();
            }
            if (adVar.h != null && adVar.h()) {
                bnVar.a(ad.t);
                bnVar.a(adVar.h);
                bnVar.b();
            }
            if (adVar.i != null && adVar.i()) {
                bnVar.a(ad.u);
                bnVar.a(adVar.i.a());
                bnVar.b();
            }
            if (adVar.j != null && adVar.j()) {
                bnVar.a(ad.v);
                bnVar.a(adVar.j);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: MiscInfo */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: MiscInfo */
    private static class c extends bx<ad> {
        private c() {
        }

        public void a(bn bnVar, ad adVar) throws ax {
            bt btVar = (bt) bnVar;
            BitSet bitSet = new BitSet();
            if (adVar.a()) {
                bitSet.set(0);
            }
            if (adVar.b()) {
                bitSet.set(1);
            }
            if (adVar.c()) {
                bitSet.set(2);
            }
            if (adVar.d()) {
                bitSet.set(3);
            }
            if (adVar.e()) {
                bitSet.set(4);
            }
            if (adVar.f()) {
                bitSet.set(5);
            }
            if (adVar.g()) {
                bitSet.set(6);
            }
            if (adVar.h()) {
                bitSet.set(7);
            }
            if (adVar.i()) {
                bitSet.set(8);
            }
            if (adVar.j()) {
                bitSet.set(9);
            }
            btVar.a(bitSet, 10);
            if (adVar.a()) {
                btVar.a(adVar.f412a);
            }
            if (adVar.b()) {
                btVar.a(adVar.f413b);
            }
            if (adVar.c()) {
                btVar.a(adVar.c);
            }
            if (adVar.d()) {
                btVar.a(adVar.d);
            }
            if (adVar.e()) {
                btVar.a(adVar.e);
            }
            if (adVar.f()) {
                btVar.a(adVar.f);
            }
            if (adVar.g()) {
                btVar.a(adVar.g);
            }
            if (adVar.h()) {
                btVar.a(adVar.h);
            }
            if (adVar.i()) {
                btVar.a(adVar.i.a());
            }
            if (adVar.j()) {
                btVar.a(adVar.j);
            }
        }

        public void b(bn bnVar, ad adVar) throws ax {
            bt btVar = (bt) bnVar;
            BitSet b2 = btVar.b(10);
            if (b2.get(0)) {
                adVar.f412a = btVar.s();
                adVar.a(true);
            }
            if (b2.get(1)) {
                adVar.f413b = btVar.v();
                adVar.b(true);
            }
            if (b2.get(2)) {
                adVar.c = btVar.v();
                adVar.c(true);
            }
            if (b2.get(3)) {
                adVar.d = btVar.u();
                adVar.d(true);
            }
            if (b2.get(4)) {
                adVar.e = btVar.u();
                adVar.e(true);
            }
            if (b2.get(5)) {
                adVar.f = btVar.v();
                adVar.f(true);
            }
            if (b2.get(6)) {
                adVar.g = btVar.s();
                adVar.g(true);
            }
            if (b2.get(7)) {
                adVar.h = btVar.v();
                adVar.h(true);
            }
            if (b2.get(8)) {
                adVar.i = k.a(btVar.s());
                adVar.i(true);
            }
            if (b2.get(9)) {
                adVar.j = btVar.v();
                adVar.j(true);
            }
        }
    }
}
