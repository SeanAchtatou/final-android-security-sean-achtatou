package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.umeng.analytics.k;

/* compiled from: StatTracer */
public class b implements cp {

    /* renamed from: a  reason: collision with root package name */
    public int f463a;

    /* renamed from: b  reason: collision with root package name */
    public int f464b;
    public long c;
    private final int d = 3600000;
    private int e;
    private long f = 0;
    private long g = 0;
    private Context h;

    public b(Context context) {
        b(context);
    }

    private void b(Context context) {
        this.h = context.getApplicationContext();
        SharedPreferences a2 = cu.a(context);
        this.f463a = a2.getInt("successful_request", 0);
        this.f464b = a2.getInt("failed_requests ", 0);
        this.e = a2.getInt("last_request_spent_ms", 0);
        this.c = a2.getLong("last_request_time", 0);
        this.f = a2.getLong("last_req", 0);
    }

    public boolean a() {
        boolean z;
        boolean z2;
        if (this.c == 0) {
            z = true;
        } else {
            z = false;
        }
        if (!k.a(this.h).g()) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    public void b() {
        this.f463a++;
        this.c = this.f;
    }

    public void c() {
        this.f464b++;
    }

    public void d() {
        this.f = System.currentTimeMillis();
    }

    public void e() {
        this.e = (int) (System.currentTimeMillis() - this.f);
    }

    public void f() {
        cu.a(this.h).edit().putInt("successful_request", this.f463a).putInt("failed_requests ", this.f464b).putInt("last_request_spent_ms", this.e).putLong("last_request_time", this.c).putLong("last_req", this.f).commit();
    }

    public void g() {
        cu.a(this.h).edit().putLong("first_activate_time", System.currentTimeMillis()).commit();
    }

    public boolean h() {
        if (this.g == 0) {
            this.g = cu.a(this.h).getLong("first_activate_time", 0);
        }
        return this.g == 0;
    }

    public long i() {
        return h() ? System.currentTimeMillis() : this.g;
    }

    public long j() {
        return this.f;
    }

    public static o a(Context context) {
        SharedPreferences a2 = cu.a(context);
        o oVar = new o();
        oVar.b(a2.getInt("failed_requests ", 0));
        oVar.c(a2.getInt("last_request_spent_ms", 0));
        oVar.a(a2.getInt("successful_request", 0));
        return oVar;
    }

    public void k() {
        d();
    }

    public void l() {
        e();
    }

    public void m() {
        b();
    }

    public void n() {
        c();
    }
}
