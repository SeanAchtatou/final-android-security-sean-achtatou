package b.a;

/* compiled from: TField */
public class bk {

    /* renamed from: a  reason: collision with root package name */
    public final String f486a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f487b;
    public final short c;

    public bk() {
        this("", (byte) 0, 0);
    }

    public bk(String str, byte b2, short s) {
        this.f486a = str;
        this.f487b = b2;
        this.c = s;
    }

    public String toString() {
        return "<TField name:'" + this.f486a + "' type:" + ((int) this.f487b) + " field-id:" + ((int) this.c) + ">";
    }
}
