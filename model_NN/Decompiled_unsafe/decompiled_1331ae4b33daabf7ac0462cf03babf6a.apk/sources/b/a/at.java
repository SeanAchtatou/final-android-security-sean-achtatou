package b.a;

/* compiled from: ShortStack */
public class at {

    /* renamed from: a  reason: collision with root package name */
    private short[] f456a;

    /* renamed from: b  reason: collision with root package name */
    private int f457b = -1;

    public at(int i) {
        this.f456a = new short[i];
    }

    public short a() {
        short[] sArr = this.f456a;
        int i = this.f457b;
        this.f457b = i - 1;
        return sArr[i];
    }

    public void a(short s) {
        if (this.f456a.length == this.f457b + 1) {
            c();
        }
        short[] sArr = this.f456a;
        int i = this.f457b + 1;
        this.f457b = i;
        sArr[i] = s;
    }

    private void c() {
        short[] sArr = new short[(this.f456a.length * 2)];
        System.arraycopy(this.f456a, 0, sArr, 0, this.f456a.length);
        this.f456a = sArr;
    }

    public void b() {
        this.f457b = -1;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<ShortStack vector:[");
        for (int i = 0; i < this.f456a.length; i++) {
            if (i != 0) {
                sb.append(" ");
            }
            if (i == this.f457b) {
                sb.append(">>");
            }
            sb.append((int) this.f456a[i]);
            if (i == this.f457b) {
                sb.append("<<");
            }
        }
        sb.append("]>");
        return sb.toString();
    }
}
