package b.a;

import android.content.Context;
import b.a.bh;
import com.umeng.analytics.a;
import com.umeng.analytics.k;
import java.io.File;
import java.io.FileInputStream;

/* compiled from: Sender */
public class cv {

    /* renamed from: a  reason: collision with root package name */
    private bi f531a;

    /* renamed from: b  reason: collision with root package name */
    private cd f532b;
    private final int c = 1;
    private Context d;
    /* access modifiers changed from: private */
    public b e;
    /* access modifiers changed from: private */
    public cr f;
    private al g;
    private boolean h = false;
    /* access modifiers changed from: private */
    public boolean i;

    public cv(Context context, b bVar) {
        this.f531a = bi.a(context);
        this.f532b = cd.a(context);
        this.d = context;
        this.e = bVar;
        this.f = new cr(context);
        this.f.a(this.e);
    }

    public void a(al alVar) {
        this.g = alVar;
    }

    public void a(boolean z) {
        this.h = z;
    }

    public void b(boolean z) {
        this.i = z;
    }

    public void a(ct ctVar) {
        this.f532b.a(ctVar);
    }

    public void a() {
        if (this.g != null) {
            c();
        } else {
            b();
        }
    }

    private void b() {
        k.a(this.d).h().a(new k.b() {
            public void a(File file) {
            }

            public boolean b(File file) {
                FileInputStream fileInputStream;
                int a2;
                try {
                    fileInputStream = new FileInputStream(file);
                    try {
                        byte[] b2 = ar.b(fileInputStream);
                        try {
                            ar.c(fileInputStream);
                            byte[] a3 = cv.this.f.a(b2);
                            if (a3 == null) {
                                a2 = 1;
                            } else {
                                a2 = cv.this.a(a3);
                            }
                            if (a2 == 2 && cv.this.e.h()) {
                                cv.this.e.g();
                            }
                            if (!cv.this.i && a2 == 1) {
                                return false;
                            }
                            return true;
                        } catch (Exception e) {
                            return false;
                        }
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    fileInputStream = null;
                    ar.c(fileInputStream);
                    throw th;
                }
            }

            public void c(File file) {
                cv.this.e.f();
            }
        });
    }

    private void c() {
        ap b2;
        int a2;
        this.f531a.a();
        al alVar = this.g;
        alVar.a(this.f531a.b());
        byte[] b3 = b(alVar);
        if (b3 == null) {
            ao.d("MobclickAgent", "message is null");
            return;
        }
        if (!this.h) {
            b2 = ap.a(this.d, a.a(this.d), b3);
        } else {
            b2 = ap.b(this.d, a.a(this.d), b3);
        }
        byte[] c2 = b2.c();
        k.a(this.d).f();
        byte[] a3 = this.f.a(c2);
        if (a3 == null) {
            a2 = 1;
        } else {
            a2 = a(a3);
        }
        switch (a2) {
            case 1:
                if (!this.i) {
                    k.a(this.d).b(c2);
                }
                ao.b("MobclickAgent", "connection error");
                return;
            case 2:
                if (this.e.h()) {
                    this.e.g();
                }
                this.f531a.c();
                this.e.f();
                return;
            case 3:
                this.e.f();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public int a(byte[] bArr) {
        ah ahVar = new ah();
        try {
            new aw(new bh.a()).a(ahVar, bArr);
            if (ahVar.f428a == 1) {
                this.f532b.b(ahVar.d());
                this.f532b.c();
            }
            ao.a("MobclickAgent", "send log:" + ahVar.b());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (ahVar.f428a == 1) {
            return 2;
        }
        return 3;
    }

    private byte[] b(al alVar) {
        if (alVar == null) {
            return null;
        }
        try {
            byte[] a2 = new az().a(alVar);
            if (ao.f451a) {
                ao.c("MobclickAgent", alVar.toString());
            }
            return a2;
        } catch (Exception e2) {
            ao.b("MobclickAgent", "Fail to serialize log ...", e2);
            return null;
        }
    }
}
