package b.a;

/* compiled from: ErrorSource */
public enum s {
    LEGIT(1),
    ALIEN(2);
    
    private final int c;

    private s(int i) {
        this.c = i;
    }

    public int a() {
        return this.c;
    }

    public static s a(int i) {
        switch (i) {
            case 1:
                return LEGIT;
            case 2:
                return ALIEN;
            default:
                return null;
        }
    }
}
