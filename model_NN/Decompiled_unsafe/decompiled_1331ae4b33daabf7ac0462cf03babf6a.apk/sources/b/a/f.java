package b.a;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: UEKV */
public class f extends t implements co {
    public f(String str, Map<String, Object> map, long j, int i) {
        a(str);
        b(System.currentTimeMillis());
        if (map.size() > 0) {
            a(b(map));
        }
        a(i <= 0 ? 1 : i);
        if (j > 0) {
            a(j);
        }
    }

    private HashMap<String, af> b(Map<String, Object> map) {
        Iterator<Map.Entry<String, Object>> it = map.entrySet().iterator();
        HashMap<String, af> hashMap = new HashMap<>();
        int i = 0;
        while (i < 10 && it.hasNext()) {
            Map.Entry next = it.next();
            af afVar = new af();
            Object value = next.getValue();
            if (value instanceof String) {
                afVar.a((String) value);
            } else if (value instanceof Long) {
                afVar.a(((Long) value).longValue());
            } else if (value instanceof Integer) {
                afVar.a(((Integer) value).longValue());
            } else if (value instanceof Float) {
                afVar.a(((Float) value).longValue());
            } else if (value instanceof Double) {
                afVar.a(((Double) value).longValue());
            }
            if (afVar.d()) {
                hashMap.put(next.getKey(), afVar);
                i++;
            }
        }
        return hashMap;
    }

    public void a(al alVar, String str) {
        aa aaVar;
        if (alVar.b() > 0) {
            Iterator<aa> it = alVar.c().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                aaVar = it.next();
                if (str.equals(aaVar.a())) {
                    break;
                }
            }
        }
        aaVar = null;
        if (aaVar == null) {
            aaVar = new aa();
            aaVar.a(str);
            alVar.a(aaVar);
        }
        aaVar.a(this);
    }
}
