package b.a;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.umeng.analytics.a;
import com.umeng.analytics.h;
import java.util.ArrayList;
import java.util.List;

/* compiled from: MemoCache */
public class cq {

    /* renamed from: a  reason: collision with root package name */
    private List<co> f527a = new ArrayList();

    /* renamed from: b  reason: collision with root package name */
    private l f528b = null;
    private n c = null;
    private q d = null;
    private ad e = null;
    private Context f = null;

    public cq(Context context) {
        this.f = context;
    }

    public synchronized int a() {
        int size;
        size = this.f527a.size();
        if (this.f528b != null) {
            size++;
        }
        return size;
    }

    public synchronized void a(co coVar) {
        this.f527a.add(coVar);
    }

    public void a(al alVar) {
        String e2 = cw.e(this.f);
        if (e2 != null) {
            synchronized (this) {
                if (this.f528b != null && new b(this.f).a()) {
                    alVar.a(this.f528b);
                    this.f528b = null;
                }
                for (co a2 : this.f527a) {
                    a2.a(alVar, e2);
                }
                this.f527a.clear();
            }
            alVar.a(b());
            alVar.a(c());
            alVar.a(d());
            alVar.a(g());
            alVar.a(e());
            alVar.a(f());
            alVar.a(h());
        }
    }

    public synchronized void a(l lVar) {
        this.f528b = lVar;
    }

    public synchronized n b() {
        if (this.c == null) {
            this.c = new n();
            a(this.f);
        }
        return this.c;
    }

    public synchronized q c() {
        if (this.d == null) {
            this.d = new q();
            b(this.f);
        }
        return this.d;
    }

    public synchronized ad d() {
        if (this.e == null) {
            this.e = new ad();
            c(this.f);
        }
        return this.e;
    }

    public y e() {
        try {
            return cd.a(this.f).a();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public w f() {
        try {
            return bi.a(this.f).b();
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public o g() {
        try {
            return b.a(this.f);
        } catch (Exception e2) {
            e2.printStackTrace();
            return new o();
        }
    }

    public m h() {
        String[] a2 = h.a(this.f);
        if (a2 == null || TextUtils.isEmpty(a2[0]) || TextUtils.isEmpty(a2[1])) {
            return null;
        }
        return new m(a2[0], a2[1]);
    }

    private void a(Context context) {
        try {
            this.c.a(a.a(context));
            this.c.e(a.b(context));
            if (!(a.f3731a == null || a.f3732b == null)) {
                this.c.f(a.f3731a);
                this.c.g(a.f3732b);
            }
            this.c.c(an.o(context));
            this.c.a(ai.ANDROID);
            this.c.d("5.5.3");
            this.c.b(an.b(context));
            this.c.a(Integer.parseInt(an.a(context)));
            this.c.b(a.c);
            this.c.d(a.a());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(Context context) {
        try {
            this.d.e(an.a());
            this.d.a(an.c(context));
            this.d.b(an.d(context));
            this.d.c(an.k(context));
            this.d.d(Build.MODEL);
            this.d.f("Android");
            this.d.g(Build.VERSION.RELEASE);
            int[] l = an.l(context);
            if (l != null) {
                this.d.a(new ag(l[1], l[0]));
            }
            if (a.e == null || a.d != null) {
            }
            this.d.h(Build.BOARD);
            this.d.i(Build.BRAND);
            this.d.a(Build.TIME);
            this.d.j(Build.MANUFACTURER);
            this.d.k(Build.ID);
            this.d.l(Build.DEVICE);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void c(Context context) {
        try {
            String[] e2 = an.e(context);
            if ("Wi-Fi".equals(e2[0])) {
                this.e.a(k.ACCESS_TYPE_WIFI);
            } else if ("2G/3G".equals(e2[0])) {
                this.e.a(k.ACCESS_TYPE_2G_3G);
            } else {
                this.e.a(k.ACCESS_TYPE_UNKNOWN);
            }
            if (!"".equals(e2[1])) {
                this.e.d(e2[1]);
            }
            this.e.c(an.m(context));
            String[] i = an.i(context);
            this.e.b(i[0]);
            this.e.a(i[1]);
            this.e.a(an.h(context));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
