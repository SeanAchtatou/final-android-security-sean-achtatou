package b.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.umeng.analytics.a;
import com.umeng.analytics.d;
import java.net.URLEncoder;

/* compiled from: NetworkHelper */
public class cr {

    /* renamed from: a  reason: collision with root package name */
    private String f529a;

    /* renamed from: b  reason: collision with root package name */
    private String f530b = "10.0.0.172";
    private int c = 80;
    private Context d;
    private cp e;

    public cr(Context context) {
        this.d = context;
        this.f529a = a(context);
    }

    public void a(cp cpVar) {
        this.e = cpVar;
    }

    public byte[] a(byte[] bArr) {
        byte[] bArr2 = null;
        int i = 0;
        while (true) {
            if (i >= d.f3745a.length) {
                break;
            }
            bArr2 = a(bArr, d.f3745a[i]);
            if (bArr2 == null) {
                if (this.e != null) {
                    this.e.n();
                }
                i++;
            } else if (this.e != null) {
                this.e.m();
            }
        }
        return bArr2;
    }

    private boolean a() {
        String extraInfo;
        if (this.d.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", this.d.getPackageName()) != 0) {
            return false;
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.d.getSystemService("connectivity")).getActiveNetworkInfo();
            if (!(activeNetworkInfo == null || activeNetworkInfo.getType() == 1 || (extraInfo = activeNetworkInfo.getExtraInfo()) == null || (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap") && !extraInfo.equals("uniwap")))) {
                return true;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return false;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private byte[] a(byte[] r9, java.lang.String r10) {
        /*
            r8 = this;
            r1 = 0
            org.apache.http.client.methods.HttpPost r0 = new org.apache.http.client.methods.HttpPost
            r0.<init>(r10)
            org.apache.http.params.BasicHttpParams r2 = new org.apache.http.params.BasicHttpParams
            r2.<init>()
            r3 = 10000(0x2710, float:1.4013E-41)
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r2, r3)
            r3 = 30000(0x7530, float:4.2039E-41)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r2, r3)
            org.apache.http.impl.client.DefaultHttpClient r3 = new org.apache.http.impl.client.DefaultHttpClient
            r3.<init>(r2)
            java.lang.String r2 = "X-Umeng-UTC"
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            r0.addHeader(r2, r4)
            java.lang.String r2 = "X-Umeng-Sdk"
            java.lang.String r4 = r8.f529a
            r0.addHeader(r2, r4)
            java.lang.String r2 = "Msg-Type"
            java.lang.String r4 = "envelope"
            r0.addHeader(r2, r4)
            boolean r2 = r8.a()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            if (r2 == 0) goto L_0x004d
            org.apache.http.HttpHost r2 = new org.apache.http.HttpHost     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r4 = r8.f530b     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            int r5 = r8.c     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r2.<init>(r4, r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            org.apache.http.params.HttpParams r4 = r3.getParams()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r5 = "http.route.default-proxy"
            r4.setParameter(r5, r2)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
        L_0x004d:
            org.apache.http.entity.InputStreamEntity r2 = new org.apache.http.entity.InputStreamEntity     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.io.ByteArrayInputStream r4 = new java.io.ByteArrayInputStream     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r4.<init>(r9)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            int r5 = r9.length     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            long r6 = (long) r5     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r2.<init>(r4, r6)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r0.setEntity(r2)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            b.a.cp r2 = r8.e     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            if (r2 == 0) goto L_0x0065
            b.a.cp r2 = r8.e     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r2.k()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
        L_0x0065:
            org.apache.http.HttpResponse r0 = r3.execute(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            b.a.cp r2 = r8.e     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            if (r2 == 0) goto L_0x0072
            b.a.cp r2 = r8.e     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r2.l()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
        L_0x0072:
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            int r2 = r2.getStatusCode()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r3 = "Content-Type"
            org.apache.http.Header r3 = r0.getFirstHeader(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r4 = "application/thrift"
            boolean r3 = b.a.ar.a(r3, r4)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r4 = "MobclickAgent"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r6 = "status code : "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            b.a.ao.c(r4, r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 != r4) goto L_0x00df
            if (r3 == 0) goto L_0x00df
            java.lang.String r2 = "MobclickAgent"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r4 = "Send message to "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            java.lang.String r3 = r3.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            b.a.ao.a(r2, r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            org.apache.http.HttpEntity r0 = r0.getEntity()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            if (r0 == 0) goto L_0x00dd
            java.io.InputStream r2 = r0.getContent()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            byte[] r0 = b.a.ar.b(r2)     // Catch:{ all -> 0x00ce }
            b.a.ar.c(r2)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
        L_0x00cd:
            return r0
        L_0x00ce:
            r0 = move-exception
            b.a.ar.c(r2)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
            throw r0     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00e1 }
        L_0x00d3:
            r0 = move-exception
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "ClientProtocolException,Failed to send message."
            b.a.ao.b(r2, r3, r0)
            r0 = r1
            goto L_0x00cd
        L_0x00dd:
            r0 = r1
            goto L_0x00cd
        L_0x00df:
            r0 = r1
            goto L_0x00cd
        L_0x00e1:
            r0 = move-exception
            java.lang.String r2 = "MobclickAgent"
            java.lang.String r3 = "IOException,Failed to send message."
            b.a.ao.b(r2, r3, r0)
            r0 = r1
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.cr.a(byte[], java.lang.String):byte[]");
    }

    private String a(Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Android");
        stringBuffer.append("/");
        stringBuffer.append("5.5.3");
        stringBuffer.append(" ");
        try {
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(an.p(context));
            stringBuffer2.append("/");
            stringBuffer2.append(an.b(context));
            stringBuffer2.append(" ");
            stringBuffer2.append(Build.MODEL);
            stringBuffer2.append("/");
            stringBuffer2.append(Build.VERSION.RELEASE);
            stringBuffer2.append(" ");
            stringBuffer2.append(ar.a(a.a(context)));
            stringBuffer.append(URLEncoder.encode(stringBuffer2.toString(), "UTF-8"));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return stringBuffer.toString();
    }
}
