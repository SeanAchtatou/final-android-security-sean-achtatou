package b.a;

import com.sina.weibo.sdk.register.mobile.SelectCountryActivity;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Event */
public class t implements au<t, e>, Serializable, Cloneable {
    public static final Map<e, bc> f;
    /* access modifiers changed from: private */
    public static final bs g = new bs("Event");
    /* access modifiers changed from: private */
    public static final bk h = new bk(SelectCountryActivity.EXTRA_COUNTRY_NAME, (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk i = new bk("properties", (byte) 13, 2);
    /* access modifiers changed from: private */
    public static final bk j = new bk("duration", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final bk k = new bk("acc", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final bk l = new bk("ts", (byte) 10, 5);
    private static final Map<Class<? extends bu>, bv> m = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f572a;

    /* renamed from: b  reason: collision with root package name */
    public Map<String, af> f573b;
    public long c;
    public int d;
    public long e;
    private byte n = 0;
    private e[] o = {e.DURATION, e.ACC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.t$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        m.put(bw.class, new b());
        m.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.NAME, (Object) new bc(SelectCountryActivity.EXTRA_COUNTRY_NAME, (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.PROPERTIES, (Object) new bc("properties", (byte) 1, new bf((byte) 13, new bd((byte) 11), new bg((byte) 12, af.class))));
        enumMap.put((Object) e.DURATION, (Object) new bc("duration", (byte) 2, new bd((byte) 10)));
        enumMap.put((Object) e.ACC, (Object) new bc("acc", (byte) 2, new bd((byte) 8)));
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        f = Collections.unmodifiableMap(enumMap);
        bc.a(t.class, f);
    }

    /* compiled from: Event */
    public enum e implements ay {
        NAME(1, SelectCountryActivity.EXTRA_COUNTRY_NAME),
        PROPERTIES(2, "properties"),
        DURATION(3, "duration"),
        ACC(4, "acc"),
        TS(5, "ts");
        
        private static final Map<String, e> f = new HashMap();
        private final short g;
        private final String h;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                f.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.g = s;
            this.h = str;
        }

        public short a() {
            return this.g;
        }

        public String b() {
            return this.h;
        }
    }

    public t a(String str) {
        this.f572a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f572a = null;
        }
    }

    public t a(Map<String, af> map) {
        this.f573b = map;
        return this;
    }

    public void b(boolean z) {
        if (!z) {
            this.f573b = null;
        }
    }

    public t a(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public boolean a() {
        return as.a(this.n, 0);
    }

    public void c(boolean z) {
        this.n = as.a(this.n, 0, z);
    }

    public t a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public boolean b() {
        return as.a(this.n, 1);
    }

    public void d(boolean z) {
        this.n = as.a(this.n, 1, z);
    }

    public t b(long j2) {
        this.e = j2;
        e(true);
        return this;
    }

    public boolean c() {
        return as.a(this.n, 2);
    }

    public void e(boolean z) {
        this.n = as.a(this.n, 2, z);
    }

    public void a(bn bnVar) throws ax {
        m.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        m.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Event(");
        sb.append("name:");
        if (this.f572a == null) {
            sb.append("null");
        } else {
            sb.append(this.f572a);
        }
        sb.append(", ");
        sb.append("properties:");
        if (this.f573b == null) {
            sb.append("null");
        } else {
            sb.append(this.f573b);
        }
        if (a()) {
            sb.append(", ");
            sb.append("duration:");
            sb.append(this.c);
        }
        if (b()) {
            sb.append(", ");
            sb.append("acc:");
            sb.append(this.d);
        }
        sb.append(", ");
        sb.append("ts:");
        sb.append(this.e);
        sb.append(")");
        return sb.toString();
    }

    public void d() throws ax {
        if (this.f572a == null) {
            throw new bo("Required field 'name' was not present! Struct: " + toString());
        } else if (this.f573b == null) {
            throw new bo("Required field 'properties' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Event */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Event */
    private static class a extends bw<t> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, t tVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!tVar.c()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    tVar.d();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            tVar.f572a = bnVar.v();
                            tVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 13) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bm j = bnVar.j();
                            tVar.f573b = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = bnVar.v();
                                af afVar = new af();
                                afVar.a(bnVar);
                                tVar.f573b.put(v, afVar);
                            }
                            bnVar.k();
                            tVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            tVar.c = bnVar.t();
                            tVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            tVar.d = bnVar.s();
                            tVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            tVar.e = bnVar.t();
                            tVar.e(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, t tVar) throws ax {
            tVar.d();
            bnVar.a(t.g);
            if (tVar.f572a != null) {
                bnVar.a(t.h);
                bnVar.a(tVar.f572a);
                bnVar.b();
            }
            if (tVar.f573b != null) {
                bnVar.a(t.i);
                bnVar.a(new bm((byte) 11, (byte) 12, tVar.f573b.size()));
                for (Map.Entry next : tVar.f573b.entrySet()) {
                    bnVar.a((String) next.getKey());
                    ((af) next.getValue()).b(bnVar);
                }
                bnVar.d();
                bnVar.b();
            }
            if (tVar.a()) {
                bnVar.a(t.j);
                bnVar.a(tVar.c);
                bnVar.b();
            }
            if (tVar.b()) {
                bnVar.a(t.k);
                bnVar.a(tVar.d);
                bnVar.b();
            }
            bnVar.a(t.l);
            bnVar.a(tVar.e);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Event */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Event */
    private static class c extends bx<t> {
        private c() {
        }

        public void a(bn bnVar, t tVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(tVar.f572a);
            btVar.a(tVar.f573b.size());
            for (Map.Entry next : tVar.f573b.entrySet()) {
                btVar.a((String) next.getKey());
                ((af) next.getValue()).b(btVar);
            }
            btVar.a(tVar.e);
            BitSet bitSet = new BitSet();
            if (tVar.a()) {
                bitSet.set(0);
            }
            if (tVar.b()) {
                bitSet.set(1);
            }
            btVar.a(bitSet, 2);
            if (tVar.a()) {
                btVar.a(tVar.c);
            }
            if (tVar.b()) {
                btVar.a(tVar.d);
            }
        }

        public void b(bn bnVar, t tVar) throws ax {
            bt btVar = (bt) bnVar;
            tVar.f572a = btVar.v();
            tVar.a(true);
            bm bmVar = new bm((byte) 11, (byte) 12, btVar.s());
            tVar.f573b = new HashMap(bmVar.c * 2);
            for (int i = 0; i < bmVar.c; i++) {
                String v = btVar.v();
                af afVar = new af();
                afVar.a(btVar);
                tVar.f573b.put(v, afVar);
            }
            tVar.b(true);
            tVar.e = btVar.t();
            tVar.e(true);
            BitSet b2 = btVar.b(2);
            if (b2.get(0)) {
                tVar.c = btVar.t();
                tVar.c(true);
            }
            if (b2.get(1)) {
                tVar.d = btVar.s();
                tVar.d(true);
            }
        }
    }
}
