package b.a;

import b.a.bj;
import java.io.ByteArrayOutputStream;

/* compiled from: TSerializer */
public class az {

    /* renamed from: a  reason: collision with root package name */
    private final ByteArrayOutputStream f461a;

    /* renamed from: b  reason: collision with root package name */
    private final by f462b;
    private bn c;

    public az() {
        this(new bj.a());
    }

    public az(bp bpVar) {
        this.f461a = new ByteArrayOutputStream();
        this.f462b = new by(this.f461a);
        this.c = bpVar.a(this.f462b);
    }

    public byte[] a(au auVar) throws ax {
        this.f461a.reset();
        auVar.b(this.c);
        return this.f461a.toByteArray();
    }
}
