package b.a;

import b.a.ay;
import b.a.ba;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* compiled from: TUnion */
public abstract class ba<T extends ba<?, ?>, F extends ay> implements au<T, F> {

    /* renamed from: a  reason: collision with root package name */
    private static final Map<Class<? extends bu>, bv> f465a = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    protected Object f466b = null;
    protected F c = null;

    /* access modifiers changed from: protected */
    public abstract bk a(ay ayVar);

    /* access modifiers changed from: protected */
    public abstract bs a();

    /* access modifiers changed from: protected */
    public abstract Object a(bn bnVar, bk bkVar) throws ax;

    /* access modifiers changed from: protected */
    public abstract Object a(bn bnVar, short s) throws ax;

    /* access modifiers changed from: protected */
    public abstract F b(short s);

    /* access modifiers changed from: protected */
    public abstract void c(bn bnVar) throws ax;

    /* access modifiers changed from: protected */
    public abstract void d(bn bnVar) throws ax;

    protected ba() {
    }

    static {
        f465a.put(bw.class, new b());
        f465a.put(bx.class, new d());
    }

    public F b() {
        return this.c;
    }

    public Object c() {
        return this.f466b;
    }

    public boolean d() {
        return this.c != null;
    }

    public void a(bn bnVar) throws ax {
        f465a.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        f465a.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("<");
        sb.append(getClass().getSimpleName());
        sb.append(" ");
        if (b() != null) {
            Object c2 = c();
            sb.append(a(b()).f486a);
            sb.append(":");
            if (c2 instanceof ByteBuffer) {
                av.a((ByteBuffer) c2, sb);
            } else {
                sb.append(c2.toString());
            }
        }
        sb.append(">");
        return sb.toString();
    }

    /* compiled from: TUnion */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: TUnion */
    private static class a extends bw<ba> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ba baVar) throws ax {
            baVar.c = null;
            baVar.f466b = null;
            bnVar.f();
            bk h = bnVar.h();
            baVar.f466b = baVar.a(bnVar, h);
            if (baVar.f466b != null) {
                baVar.c = baVar.b(h.c);
            }
            bnVar.i();
            bnVar.h();
            bnVar.g();
        }

        /* renamed from: b */
        public void a(bn bnVar, ba baVar) throws ax {
            if (baVar.b() == null || baVar.c() == null) {
                throw new bo("Cannot write a TUnion with no set value!");
            }
            bnVar.a(baVar.a());
            bnVar.a(baVar.a((ay) baVar.c));
            baVar.c(bnVar);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: TUnion */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: TUnion */
    private static class c extends bx<ba> {
        private c() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ba baVar) throws ax {
            baVar.c = null;
            baVar.f466b = null;
            short r = bnVar.r();
            baVar.f466b = baVar.a(bnVar, r);
            if (baVar.f466b != null) {
                baVar.c = baVar.b(r);
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ba baVar) throws ax {
            if (baVar.b() == null || baVar.c() == null) {
                throw new bo("Cannot write a TUnion with no set value!");
            }
            bnVar.a(baVar.c.a());
            baVar.d(bnVar);
        }
    }
}
