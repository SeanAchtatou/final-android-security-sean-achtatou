package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ActiveUser */
public class m implements au<m, e>, Serializable, Cloneable {
    public static final Map<e, bc> c;
    /* access modifiers changed from: private */
    public static final bs d = new bs("ActiveUser");
    /* access modifiers changed from: private */
    public static final bk e = new bk("provider", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk f = new bk("puid", (byte) 11, 2);
    private static final Map<Class<? extends bu>, bv> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f546a;

    /* renamed from: b  reason: collision with root package name */
    public String f547b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.m$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(bw.class, new b());
        g.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PROVIDER, (Object) new bc("provider", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.PUID, (Object) new bc("puid", (byte) 1, new bd((byte) 11)));
        c = Collections.unmodifiableMap(enumMap);
        bc.a(m.class, c);
    }

    /* compiled from: ActiveUser */
    public enum e implements ay {
        PROVIDER(1, "provider"),
        PUID(2, "puid");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public m() {
    }

    public m(String str, String str2) {
        this();
        this.f546a = str;
        this.f547b = str2;
    }

    public void a(boolean z) {
        if (!z) {
            this.f546a = null;
        }
    }

    public void b(boolean z) {
        if (!z) {
            this.f547b = null;
        }
    }

    public void a(bn bnVar) throws ax {
        g.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        g.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ActiveUser(");
        sb.append("provider:");
        if (this.f546a == null) {
            sb.append("null");
        } else {
            sb.append(this.f546a);
        }
        sb.append(", ");
        sb.append("puid:");
        if (this.f547b == null) {
            sb.append("null");
        } else {
            sb.append(this.f547b);
        }
        sb.append(")");
        return sb.toString();
    }

    public void a() throws ax {
        if (this.f546a == null) {
            throw new bo("Required field 'provider' was not present! Struct: " + toString());
        } else if (this.f547b == null) {
            throw new bo("Required field 'puid' was not present! Struct: " + toString());
        }
    }

    /* compiled from: ActiveUser */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ActiveUser */
    private static class a extends bw<m> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, m mVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    mVar.a();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            mVar.f546a = bnVar.v();
                            mVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            mVar.f547b = bnVar.v();
                            mVar.b(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, m mVar) throws ax {
            mVar.a();
            bnVar.a(m.d);
            if (mVar.f546a != null) {
                bnVar.a(m.e);
                bnVar.a(mVar.f546a);
                bnVar.b();
            }
            if (mVar.f547b != null) {
                bnVar.a(m.f);
                bnVar.a(mVar.f547b);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: ActiveUser */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ActiveUser */
    private static class c extends bx<m> {
        private c() {
        }

        public void a(bn bnVar, m mVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(mVar.f546a);
            btVar.a(mVar.f547b);
        }

        public void b(bn bnVar, m mVar) throws ax {
            bt btVar = (bt) bnVar;
            mVar.f546a = btVar.v();
            mVar.a(true);
            mVar.f547b = btVar.v();
            mVar.b(true);
        }
    }
}
