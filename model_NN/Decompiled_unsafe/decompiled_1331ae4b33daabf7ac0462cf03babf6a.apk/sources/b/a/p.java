package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ControlPolicy */
public class p implements au<p, e>, Serializable, Cloneable {

    /* renamed from: b  reason: collision with root package name */
    public static final Map<e, bc> f558b;
    /* access modifiers changed from: private */
    public static final bs c = new bs("ControlPolicy");
    /* access modifiers changed from: private */
    public static final bk d = new bk("latent", (byte) 12, 1);
    private static final Map<Class<? extends bu>, bv> e = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public ab f559a;
    private e[] f = {e.LATENT};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.p$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        e.put(bw.class, new b());
        e.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LATENT, (Object) new bc("latent", (byte) 2, new bg((byte) 12, ab.class)));
        f558b = Collections.unmodifiableMap(enumMap);
        bc.a(p.class, f558b);
    }

    /* compiled from: ControlPolicy */
    public enum e implements ay {
        LATENT(1, "latent");
        

        /* renamed from: b  reason: collision with root package name */
        private static final Map<String, e> f561b = new HashMap();
        private final short c;
        private final String d;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                f561b.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.c = s;
            this.d = str;
        }

        public short a() {
            return this.c;
        }

        public String b() {
            return this.d;
        }
    }

    public p a(ab abVar) {
        this.f559a = abVar;
        return this;
    }

    public boolean a() {
        return this.f559a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f559a = null;
        }
    }

    public void a(bn bnVar) throws ax {
        e.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        e.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ControlPolicy(");
        if (a()) {
            sb.append("latent:");
            if (this.f559a == null) {
                sb.append("null");
            } else {
                sb.append(this.f559a);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void b() throws ax {
        if (this.f559a != null) {
            this.f559a.c();
        }
    }

    /* compiled from: ControlPolicy */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ControlPolicy */
    private static class a extends bw<p> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, p pVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    pVar.b();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            pVar.f559a = new ab();
                            pVar.f559a.a(bnVar);
                            pVar.a(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, p pVar) throws ax {
            pVar.b();
            bnVar.a(p.c);
            if (pVar.f559a != null && pVar.a()) {
                bnVar.a(p.d);
                pVar.f559a.b(bnVar);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: ControlPolicy */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ControlPolicy */
    private static class c extends bx<p> {
        private c() {
        }

        public void a(bn bnVar, p pVar) throws ax {
            bt btVar = (bt) bnVar;
            BitSet bitSet = new BitSet();
            if (pVar.a()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (pVar.a()) {
                pVar.f559a.b(btVar);
            }
        }

        public void b(bn bnVar, p pVar) throws ax {
            bt btVar = (bt) bnVar;
            if (btVar.b(1).get(0)) {
                pVar.f559a = new ab();
                pVar.f559a.a(btVar);
                pVar.a(true);
            }
        }
    }
}
