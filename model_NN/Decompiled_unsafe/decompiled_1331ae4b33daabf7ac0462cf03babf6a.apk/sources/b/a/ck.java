package b.a;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* compiled from: Caretaker */
public class ck {

    /* renamed from: a  reason: collision with root package name */
    private final String f521a = "umeng_event_snapshot";

    /* renamed from: b  reason: collision with root package name */
    private boolean f522b = false;
    private SharedPreferences c;
    private Map<String, ArrayList<Object>> d = new HashMap();

    public ck(Context context) {
        this.c = cu.a(context, "umeng_event_snapshot");
    }

    public void a(boolean z) {
        this.f522b = z;
    }
}
