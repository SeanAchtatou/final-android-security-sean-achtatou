package b.a;

import android.content.Context;
import com.umeng.analytics.a;
import java.util.HashMap;
import java.util.Map;

/* compiled from: EventTracker */
public class cm {

    /* renamed from: a  reason: collision with root package name */
    private final int f525a = 128;

    /* renamed from: b  reason: collision with root package name */
    private final int f526b = 256;
    private ck c;
    private Context d;
    private cj e;

    public cm(Context context) {
        if (context == null) {
            throw new RuntimeException("Context is null, can't track event");
        }
        this.d = context.getApplicationContext();
        this.c = new ck(this.d);
        this.c.a(!a.j);
        this.e = cj.a(this.d);
    }

    public void a(String str, Map<String, Object> map, long j) {
        try {
            if (a(str) && a(map)) {
                this.e.a(new f(str, map, j, -1));
            }
        } catch (Exception e2) {
            ao.b("MobclickAgent", "Exception occurred in Mobclick.onEvent(). ", e2);
        }
    }

    public void a(String str, String str2, long j, int i) {
        if (a(str) && b(str2)) {
            HashMap hashMap = new HashMap();
            if (str2 == null) {
                str2 = "";
            }
            hashMap.put(str, str2);
            this.e.a(new f(str, hashMap, j, i));
        }
    }

    private boolean a(String str) {
        int length;
        if (str != null && (length = str.trim().getBytes().length) > 0 && length <= 128) {
            return true;
        }
        ao.b("MobclickAgent", "Event id is empty or too long in tracking Event");
        return false;
    }

    private boolean b(String str) {
        if (str == null || str.trim().getBytes().length <= 256) {
            return true;
        }
        ao.b("MobclickAgent", "Event label or value is empty or too long in tracking Event");
        return false;
    }

    private boolean a(Map<String, Object> map) {
        if (map == null || map.isEmpty()) {
            ao.b("MobclickAgent", "map is null or empty in onEvent");
            return false;
        }
        for (Map.Entry next : map.entrySet()) {
            if (!a((String) next.getKey())) {
                return false;
            }
            if (next.getValue() == null) {
                return false;
            }
            if ((next.getValue() instanceof String) && !b(next.getValue().toString())) {
                return false;
            }
        }
        return true;
    }
}
