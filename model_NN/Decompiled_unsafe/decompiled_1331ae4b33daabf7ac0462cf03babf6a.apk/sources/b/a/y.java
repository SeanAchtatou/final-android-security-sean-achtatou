package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Imprint */
public class y implements au<y, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("Imprint");
    /* access modifiers changed from: private */
    public static final bk f = new bk("property", (byte) 13, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("version", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("checksum", (byte) 11, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public Map<String, z> f589a;

    /* renamed from: b  reason: collision with root package name */
    public int f590b;
    public String c;
    private byte j = 0;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.y$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.PROPERTY, (Object) new bc("property", (byte) 1, new bf((byte) 13, new bd((byte) 11), new bg((byte) 12, z.class))));
        enumMap.put((Object) e.VERSION, (Object) new bc("version", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.CHECKSUM, (Object) new bc("checksum", (byte) 1, new bd((byte) 11)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(y.class, d);
    }

    /* compiled from: Imprint */
    public enum e implements ay {
        PROPERTY(1, "property"),
        VERSION(2, "version"),
        CHECKSUM(3, "checksum");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public Map<String, z> a() {
        return this.f589a;
    }

    public boolean b() {
        return this.f589a != null;
    }

    public void a(boolean z) {
        if (!z) {
            this.f589a = null;
        }
    }

    public int c() {
        return this.f590b;
    }

    public y a(int i2) {
        this.f590b = i2;
        b(true);
        return this;
    }

    public boolean d() {
        return as.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public String e() {
        return this.c;
    }

    public y a(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Imprint(");
        sb.append("property:");
        if (this.f589a == null) {
            sb.append("null");
        } else {
            sb.append(this.f589a);
        }
        sb.append(", ");
        sb.append("version:");
        sb.append(this.f590b);
        sb.append(", ");
        sb.append("checksum:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void f() throws ax {
        if (this.f589a == null) {
            throw new bo("Required field 'property' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bo("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Imprint */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Imprint */
    private static class a extends bw<y> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, y yVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!yVar.d()) {
                        throw new bo("Required field 'version' was not found in serialized data! Struct: " + toString());
                    }
                    yVar.f();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 13) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bm j = bnVar.j();
                            yVar.f589a = new HashMap(j.c * 2);
                            for (int i = 0; i < j.c; i++) {
                                String v = bnVar.v();
                                z zVar = new z();
                                zVar.a(bnVar);
                                yVar.f589a.put(v, zVar);
                            }
                            bnVar.k();
                            yVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            yVar.f590b = bnVar.s();
                            yVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            yVar.c = bnVar.v();
                            yVar.c(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, y yVar) throws ax {
            yVar.f();
            bnVar.a(y.e);
            if (yVar.f589a != null) {
                bnVar.a(y.f);
                bnVar.a(new bm((byte) 11, (byte) 12, yVar.f589a.size()));
                for (Map.Entry next : yVar.f589a.entrySet()) {
                    bnVar.a((String) next.getKey());
                    ((z) next.getValue()).b(bnVar);
                }
                bnVar.d();
                bnVar.b();
            }
            bnVar.a(y.g);
            bnVar.a(yVar.f590b);
            bnVar.b();
            if (yVar.c != null) {
                bnVar.a(y.h);
                bnVar.a(yVar.c);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Imprint */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Imprint */
    private static class c extends bx<y> {
        private c() {
        }

        public void a(bn bnVar, y yVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(yVar.f589a.size());
            for (Map.Entry next : yVar.f589a.entrySet()) {
                btVar.a((String) next.getKey());
                ((z) next.getValue()).b(btVar);
            }
            btVar.a(yVar.f590b);
            btVar.a(yVar.c);
        }

        public void b(bn bnVar, y yVar) throws ax {
            bt btVar = (bt) bnVar;
            bm bmVar = new bm((byte) 11, (byte) 12, btVar.s());
            yVar.f589a = new HashMap(bmVar.c * 2);
            for (int i = 0; i < bmVar.c; i++) {
                String v = btVar.v();
                z zVar = new z();
                zVar.a(btVar);
                yVar.f589a.put(v, zVar);
            }
            yVar.a(true);
            yVar.f590b = btVar.s();
            yVar.b(true);
            yVar.c = btVar.v();
            yVar.c(true);
        }
    }
}
