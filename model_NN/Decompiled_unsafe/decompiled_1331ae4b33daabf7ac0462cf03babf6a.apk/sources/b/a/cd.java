package b.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.analytics.e;
import com.umeng.analytics.k;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

/* compiled from: ImprintHandler */
public class cd {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f505a = "pbl0".getBytes();
    private static cd d;

    /* renamed from: b  reason: collision with root package name */
    private ct f506b;
    private y c = null;
    private Context e;

    cd(Context context) {
        this.e = context;
    }

    public static synchronized cd a(Context context) {
        cd cdVar;
        synchronized (cd.class) {
            if (d == null) {
                d = new cd(context);
                d.b();
            }
            cdVar = d;
        }
        return cdVar;
    }

    public void a(ct ctVar) {
        this.f506b = ctVar;
    }

    public String a(y yVar) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : new TreeMap(yVar.a()).entrySet()) {
            sb.append((String) entry.getKey());
            sb.append(((z) entry.getValue()).a());
            sb.append(((z) entry.getValue()).c());
            sb.append(((z) entry.getValue()).e());
        }
        sb.append(yVar.f590b);
        return ar.a(sb.toString()).toLowerCase(Locale.US);
    }

    private boolean c(y yVar) {
        if (!yVar.e().equals(a(yVar))) {
            return false;
        }
        for (z next : yVar.a().values()) {
            byte[] a2 = e.a(next.e());
            byte[] a3 = a(next);
            int i = 0;
            while (true) {
                if (i < 4) {
                    if (a2[i] != a3[i]) {
                        return false;
                    }
                    i++;
                }
            }
        }
        return true;
    }

    public byte[] a(z zVar) {
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.order(null);
        allocate.putLong(zVar.c());
        byte[] array = allocate.array();
        byte[] bArr = f505a;
        byte[] bArr2 = new byte[4];
        for (int i = 0; i < 4; i++) {
            bArr2[i] = (byte) (array[i] ^ bArr[i]);
        }
        return bArr2;
    }

    public void b(y yVar) {
        if (yVar != null && c(yVar)) {
            synchronized (this) {
                y yVar2 = this.c;
                if (yVar2 != null) {
                    yVar = a(yVar2, yVar);
                }
                this.c = yVar;
            }
            if (this.c != null) {
                d();
            }
        }
    }

    private y a(y yVar, y yVar2) {
        if (yVar2 != null) {
            Map<String, z> a2 = yVar.a();
            for (Map.Entry next : yVar2.a().entrySet()) {
                if (((z) next.getValue()).b()) {
                    a2.put(next.getKey(), next.getValue());
                } else {
                    a2.remove(next.getKey());
                }
            }
            yVar.a(yVar2.c());
            yVar.a(a(yVar));
        }
        return yVar;
    }

    private void d() {
        if (this.f506b != null) {
            int a2 = a("defcon");
            if (a2 != -1) {
                this.f506b.a(a2);
                k.a(this.e).a(a2);
            }
            int a3 = a("latent");
            if (a3 != -1) {
                int i = a3 * 1000;
                this.f506b.b(i);
                k.a(this.e).b(i);
            }
            int a4 = a("codex");
            if (a4 == 0 || a4 == 1 || a4 == -1) {
                this.f506b.c(a4);
                k.a(this.e).c(a4);
            }
        }
    }

    private int a(String str) {
        y yVar = this.c;
        if (yVar == null || !yVar.b()) {
            return -1;
        }
        z zVar = yVar.a().get(str);
        if (zVar == null || TextUtils.isEmpty(zVar.a())) {
            return -1;
        }
        try {
            return Integer.parseInt(zVar.a().trim());
        } catch (Exception e2) {
            return -1;
        }
    }

    public synchronized y a() {
        return this.c;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r2v3 */
    /* JADX WARN: Type inference failed for: r2v4, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026 A[SYNTHETIC, Splitter:B:8:0x0026] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r4 = this;
            r2 = 0
            java.io.File r0 = new java.io.File
            android.content.Context r1 = r4.e
            java.io.File r1 = r1.getFilesDir()
            java.lang.String r3 = ".imprint"
            r0.<init>(r1, r3)
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0015
        L_0x0014:
            return
        L_0x0015:
            android.content.Context r0 = r4.e     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            java.lang.String r1 = ".imprint"
            java.io.FileInputStream r1 = r0.openFileInput(r1)     // Catch:{ Exception -> 0x003b, all -> 0x0044 }
            byte[] r2 = b.a.ar.b(r1)     // Catch:{ Exception -> 0x004c }
            b.a.ar.c(r1)
        L_0x0024:
            if (r2 == 0) goto L_0x0014
            b.a.y r0 = new b.a.y     // Catch:{ Exception -> 0x0036 }
            r0.<init>()     // Catch:{ Exception -> 0x0036 }
            b.a.aw r1 = new b.a.aw     // Catch:{ Exception -> 0x0036 }
            r1.<init>()     // Catch:{ Exception -> 0x0036 }
            r1.a(r0, r2)     // Catch:{ Exception -> 0x0036 }
            r4.c = r0     // Catch:{ Exception -> 0x0036 }
            goto L_0x0014
        L_0x0036:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0014
        L_0x003b:
            r0 = move-exception
            r1 = r2
        L_0x003d:
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
            b.a.ar.c(r1)
            goto L_0x0024
        L_0x0044:
            r0 = move-exception
        L_0x0045:
            b.a.ar.c(r2)
            throw r0
        L_0x0049:
            r0 = move-exception
            r2 = r1
            goto L_0x0045
        L_0x004c:
            r0 = move-exception
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: b.a.cd.b():void");
    }

    public void c() {
        if (this.c != null) {
            try {
                ar.a(new File(this.e.getFilesDir(), ".imprint"), new az().a(this.c));
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
