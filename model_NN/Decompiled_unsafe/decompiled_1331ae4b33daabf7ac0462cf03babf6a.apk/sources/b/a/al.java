package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: UALogEntry */
public class al implements au<al, e>, Serializable, Cloneable {
    public static final Map<e, bc> l;
    /* access modifiers changed from: private */
    public static final bs m = new bs("UALogEntry");
    /* access modifiers changed from: private */
    public static final bk n = new bk("client_stats", (byte) 12, 1);
    /* access modifiers changed from: private */
    public static final bk o = new bk("app_info", (byte) 12, 2);
    /* access modifiers changed from: private */
    public static final bk p = new bk("device_info", (byte) 12, 3);
    /* access modifiers changed from: private */
    public static final bk q = new bk("misc_info", (byte) 12, 4);
    /* access modifiers changed from: private */
    public static final bk r = new bk("activate_msg", (byte) 12, 5);
    /* access modifiers changed from: private */
    public static final bk s = new bk("instant_msgs", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final bk t = new bk("sessions", (byte) 15, 7);
    /* access modifiers changed from: private */
    public static final bk u = new bk("imprint", (byte) 12, 8);
    /* access modifiers changed from: private */
    public static final bk v = new bk("id_tracking", (byte) 12, 9);
    /* access modifiers changed from: private */
    public static final bk w = new bk("active_user", (byte) 12, 10);
    /* access modifiers changed from: private */
    public static final bk x = new bk("control_policy", (byte) 12, 11);
    private static final Map<Class<? extends bu>, bv> y = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public o f442a;

    /* renamed from: b  reason: collision with root package name */
    public n f443b;
    public q c;
    public ad d;
    public l e;
    public List<aa> f;
    public List<aj> g;
    public y h;
    public w i;
    public m j;
    public p k;
    private e[] z = {e.ACTIVATE_MSG, e.INSTANT_MSGS, e.SESSIONS, e.IMPRINT, e.ID_TRACKING, e.ACTIVE_USER, e.CONTROL_POLICY};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.al$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        y.put(bw.class, new b());
        y.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.CLIENT_STATS, (Object) new bc("client_stats", (byte) 1, new bg((byte) 12, o.class)));
        enumMap.put((Object) e.APP_INFO, (Object) new bc("app_info", (byte) 1, new bg((byte) 12, n.class)));
        enumMap.put((Object) e.DEVICE_INFO, (Object) new bc("device_info", (byte) 1, new bg((byte) 12, q.class)));
        enumMap.put((Object) e.MISC_INFO, (Object) new bc("misc_info", (byte) 1, new bg((byte) 12, ad.class)));
        enumMap.put((Object) e.ACTIVATE_MSG, (Object) new bc("activate_msg", (byte) 2, new bg((byte) 12, l.class)));
        enumMap.put((Object) e.INSTANT_MSGS, (Object) new bc("instant_msgs", (byte) 2, new be((byte) 15, new bg((byte) 12, aa.class))));
        enumMap.put((Object) e.SESSIONS, (Object) new bc("sessions", (byte) 2, new be((byte) 15, new bg((byte) 12, aj.class))));
        enumMap.put((Object) e.IMPRINT, (Object) new bc("imprint", (byte) 2, new bg((byte) 12, y.class)));
        enumMap.put((Object) e.ID_TRACKING, (Object) new bc("id_tracking", (byte) 2, new bg((byte) 12, w.class)));
        enumMap.put((Object) e.ACTIVE_USER, (Object) new bc("active_user", (byte) 2, new bg((byte) 12, m.class)));
        enumMap.put((Object) e.CONTROL_POLICY, (Object) new bc("control_policy", (byte) 2, new bg((byte) 12, p.class)));
        l = Collections.unmodifiableMap(enumMap);
        bc.a(al.class, l);
    }

    /* compiled from: UALogEntry */
    public enum e implements ay {
        CLIENT_STATS(1, "client_stats"),
        APP_INFO(2, "app_info"),
        DEVICE_INFO(3, "device_info"),
        MISC_INFO(4, "misc_info"),
        ACTIVATE_MSG(5, "activate_msg"),
        INSTANT_MSGS(6, "instant_msgs"),
        SESSIONS(7, "sessions"),
        IMPRINT(8, "imprint"),
        ID_TRACKING(9, "id_tracking"),
        ACTIVE_USER(10, "active_user"),
        CONTROL_POLICY(11, "control_policy");
        
        private static final Map<String, e> l = new HashMap();
        private final short m;
        private final String n;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                l.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.m = s;
            this.n = str;
        }

        public short a() {
            return this.m;
        }

        public String b() {
            return this.n;
        }
    }

    public al a(o oVar) {
        this.f442a = oVar;
        return this;
    }

    public void a(boolean z2) {
        if (!z2) {
            this.f442a = null;
        }
    }

    public al a(n nVar) {
        this.f443b = nVar;
        return this;
    }

    public void b(boolean z2) {
        if (!z2) {
            this.f443b = null;
        }
    }

    public al a(q qVar) {
        this.c = qVar;
        return this;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public al a(ad adVar) {
        this.d = adVar;
        return this;
    }

    public void d(boolean z2) {
        if (!z2) {
            this.d = null;
        }
    }

    public al a(l lVar) {
        this.e = lVar;
        return this;
    }

    public boolean a() {
        return this.e != null;
    }

    public void e(boolean z2) {
        if (!z2) {
            this.e = null;
        }
    }

    public int b() {
        if (this.f == null) {
            return 0;
        }
        return this.f.size();
    }

    public void a(aa aaVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(aaVar);
    }

    public List<aa> c() {
        return this.f;
    }

    public al a(List<aa> list) {
        this.f = list;
        return this;
    }

    public boolean d() {
        return this.f != null;
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public void a(aj ajVar) {
        if (this.g == null) {
            this.g = new ArrayList();
        }
        this.g.add(ajVar);
    }

    public List<aj> e() {
        return this.g;
    }

    public al b(List<aj> list) {
        this.g = list;
        return this;
    }

    public boolean f() {
        return this.g != null;
    }

    public void g(boolean z2) {
        if (!z2) {
            this.g = null;
        }
    }

    public al a(y yVar) {
        this.h = yVar;
        return this;
    }

    public boolean g() {
        return this.h != null;
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public al a(w wVar) {
        this.i = wVar;
        return this;
    }

    public boolean h() {
        return this.i != null;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public al a(m mVar) {
        this.j = mVar;
        return this;
    }

    public boolean i() {
        return this.j != null;
    }

    public void j(boolean z2) {
        if (!z2) {
            this.j = null;
        }
    }

    public al a(p pVar) {
        this.k = pVar;
        return this;
    }

    public boolean j() {
        return this.k != null;
    }

    public void k(boolean z2) {
        if (!z2) {
            this.k = null;
        }
    }

    public void a(bn bnVar) throws ax {
        y.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        y.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UALogEntry(");
        sb.append("client_stats:");
        if (this.f442a == null) {
            sb.append("null");
        } else {
            sb.append(this.f442a);
        }
        sb.append(", ");
        sb.append("app_info:");
        if (this.f443b == null) {
            sb.append("null");
        } else {
            sb.append(this.f443b);
        }
        sb.append(", ");
        sb.append("device_info:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("misc_info:");
        if (this.d == null) {
            sb.append("null");
        } else {
            sb.append(this.d);
        }
        if (a()) {
            sb.append(", ");
            sb.append("activate_msg:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("instant_msgs:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("sessions:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("imprint:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (h()) {
            sb.append(", ");
            sb.append("id_tracking:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (i()) {
            sb.append(", ");
            sb.append("active_user:");
            if (this.j == null) {
                sb.append("null");
            } else {
                sb.append(this.j);
            }
        }
        if (j()) {
            sb.append(", ");
            sb.append("control_policy:");
            if (this.k == null) {
                sb.append("null");
            } else {
                sb.append(this.k);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void k() throws ax {
        if (this.f442a == null) {
            throw new bo("Required field 'client_stats' was not present! Struct: " + toString());
        } else if (this.f443b == null) {
            throw new bo("Required field 'app_info' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bo("Required field 'device_info' was not present! Struct: " + toString());
        } else if (this.d == null) {
            throw new bo("Required field 'misc_info' was not present! Struct: " + toString());
        } else {
            if (this.f442a != null) {
                this.f442a.d();
            }
            if (this.f443b != null) {
                this.f443b.g();
            }
            if (this.c != null) {
                this.c.r();
            }
            if (this.d != null) {
                this.d.k();
            }
            if (this.e != null) {
                this.e.b();
            }
            if (this.h != null) {
                this.h.f();
            }
            if (this.i != null) {
                this.i.e();
            }
            if (this.j != null) {
                this.j.a();
            }
            if (this.k != null) {
                this.k.b();
            }
        }
    }

    /* compiled from: UALogEntry */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: UALogEntry */
    private static class a extends bw<al> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, al alVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    alVar.k();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.f442a = new o();
                            alVar.f442a.a(bnVar);
                            alVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.f443b = new n();
                            alVar.f443b.a(bnVar);
                            alVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.c = new q();
                            alVar.c.a(bnVar);
                            alVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.d = new ad();
                            alVar.d.a(bnVar);
                            alVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.e = new l();
                            alVar.e.a(bnVar);
                            alVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l = bnVar.l();
                            alVar.f = new ArrayList(l.f489b);
                            for (int i = 0; i < l.f489b; i++) {
                                aa aaVar = new aa();
                                aaVar.a(bnVar);
                                alVar.f.add(aaVar);
                            }
                            bnVar.m();
                            alVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l2 = bnVar.l();
                            alVar.g = new ArrayList(l2.f489b);
                            for (int i2 = 0; i2 < l2.f489b; i2++) {
                                aj ajVar = new aj();
                                ajVar.a(bnVar);
                                alVar.g.add(ajVar);
                            }
                            bnVar.m();
                            alVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.h = new y();
                            alVar.h.a(bnVar);
                            alVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.i = new w();
                            alVar.i.a(bnVar);
                            alVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.j = new m();
                            alVar.j.a(bnVar);
                            alVar.j(true);
                            break;
                        }
                    case 11:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            alVar.k = new p();
                            alVar.k.a(bnVar);
                            alVar.k(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, al alVar) throws ax {
            alVar.k();
            bnVar.a(al.m);
            if (alVar.f442a != null) {
                bnVar.a(al.n);
                alVar.f442a.b(bnVar);
                bnVar.b();
            }
            if (alVar.f443b != null) {
                bnVar.a(al.o);
                alVar.f443b.b(bnVar);
                bnVar.b();
            }
            if (alVar.c != null) {
                bnVar.a(al.p);
                alVar.c.b(bnVar);
                bnVar.b();
            }
            if (alVar.d != null) {
                bnVar.a(al.q);
                alVar.d.b(bnVar);
                bnVar.b();
            }
            if (alVar.e != null && alVar.a()) {
                bnVar.a(al.r);
                alVar.e.b(bnVar);
                bnVar.b();
            }
            if (alVar.f != null && alVar.d()) {
                bnVar.a(al.s);
                bnVar.a(new bl((byte) 12, alVar.f.size()));
                for (aa b2 : alVar.f) {
                    b2.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (alVar.g != null && alVar.f()) {
                bnVar.a(al.t);
                bnVar.a(new bl((byte) 12, alVar.g.size()));
                for (aj b3 : alVar.g) {
                    b3.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (alVar.h != null && alVar.g()) {
                bnVar.a(al.u);
                alVar.h.b(bnVar);
                bnVar.b();
            }
            if (alVar.i != null && alVar.h()) {
                bnVar.a(al.v);
                alVar.i.b(bnVar);
                bnVar.b();
            }
            if (alVar.j != null && alVar.i()) {
                bnVar.a(al.w);
                alVar.j.b(bnVar);
                bnVar.b();
            }
            if (alVar.k != null && alVar.j()) {
                bnVar.a(al.x);
                alVar.k.b(bnVar);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: UALogEntry */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: UALogEntry */
    private static class c extends bx<al> {
        private c() {
        }

        public void a(bn bnVar, al alVar) throws ax {
            bt btVar = (bt) bnVar;
            alVar.f442a.b(btVar);
            alVar.f443b.b(btVar);
            alVar.c.b(btVar);
            alVar.d.b(btVar);
            BitSet bitSet = new BitSet();
            if (alVar.a()) {
                bitSet.set(0);
            }
            if (alVar.d()) {
                bitSet.set(1);
            }
            if (alVar.f()) {
                bitSet.set(2);
            }
            if (alVar.g()) {
                bitSet.set(3);
            }
            if (alVar.h()) {
                bitSet.set(4);
            }
            if (alVar.i()) {
                bitSet.set(5);
            }
            if (alVar.j()) {
                bitSet.set(6);
            }
            btVar.a(bitSet, 7);
            if (alVar.a()) {
                alVar.e.b(btVar);
            }
            if (alVar.d()) {
                btVar.a(alVar.f.size());
                for (aa b2 : alVar.f) {
                    b2.b(btVar);
                }
            }
            if (alVar.f()) {
                btVar.a(alVar.g.size());
                for (aj b3 : alVar.g) {
                    b3.b(btVar);
                }
            }
            if (alVar.g()) {
                alVar.h.b(btVar);
            }
            if (alVar.h()) {
                alVar.i.b(btVar);
            }
            if (alVar.i()) {
                alVar.j.b(btVar);
            }
            if (alVar.j()) {
                alVar.k.b(btVar);
            }
        }

        public void b(bn bnVar, al alVar) throws ax {
            bt btVar = (bt) bnVar;
            alVar.f442a = new o();
            alVar.f442a.a(btVar);
            alVar.a(true);
            alVar.f443b = new n();
            alVar.f443b.a(btVar);
            alVar.b(true);
            alVar.c = new q();
            alVar.c.a(btVar);
            alVar.c(true);
            alVar.d = new ad();
            alVar.d.a(btVar);
            alVar.d(true);
            BitSet b2 = btVar.b(7);
            if (b2.get(0)) {
                alVar.e = new l();
                alVar.e.a(btVar);
                alVar.e(true);
            }
            if (b2.get(1)) {
                bl blVar = new bl((byte) 12, btVar.s());
                alVar.f = new ArrayList(blVar.f489b);
                for (int i = 0; i < blVar.f489b; i++) {
                    aa aaVar = new aa();
                    aaVar.a(btVar);
                    alVar.f.add(aaVar);
                }
                alVar.f(true);
            }
            if (b2.get(2)) {
                bl blVar2 = new bl((byte) 12, btVar.s());
                alVar.g = new ArrayList(blVar2.f489b);
                for (int i2 = 0; i2 < blVar2.f489b; i2++) {
                    aj ajVar = new aj();
                    ajVar.a(btVar);
                    alVar.g.add(ajVar);
                }
                alVar.g(true);
            }
            if (b2.get(3)) {
                alVar.h = new y();
                alVar.h.a(btVar);
                alVar.h(true);
            }
            if (b2.get(4)) {
                alVar.i = new w();
                alVar.i.a(btVar);
                alVar.i(true);
            }
            if (b2.get(5)) {
                alVar.j = new m();
                alVar.j.a(btVar);
                alVar.j(true);
            }
            if (b2.get(6)) {
                alVar.k = new p();
                alVar.k.a(btVar);
                alVar.k(true);
            }
        }
    }
}
