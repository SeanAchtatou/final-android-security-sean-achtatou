package b.a;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Latent */
public class ab implements au<ab, e>, Serializable, Cloneable {
    public static final Map<e, bc> c;
    /* access modifiers changed from: private */
    public static final bs d = new bs("Latent");
    /* access modifiers changed from: private */
    public static final bk e = new bk("latency", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk f = new bk("interval", (byte) 10, 2);
    private static final Map<Class<? extends bu>, bv> g = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f404a;

    /* renamed from: b  reason: collision with root package name */
    public long f405b;
    private byte h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.ab$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        g.put(bw.class, new b());
        g.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.LATENCY, (Object) new bc("latency", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.INTERVAL, (Object) new bc("interval", (byte) 1, new bd((byte) 10)));
        c = Collections.unmodifiableMap(enumMap);
        bc.a(ab.class, c);
    }

    /* compiled from: Latent */
    public enum e implements ay {
        LATENCY(1, "latency"),
        INTERVAL(2, "interval");
        
        private static final Map<String, e> c = new HashMap();
        private final short d;
        private final String e;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                c.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.d = s;
            this.e = str;
        }

        public short a() {
            return this.d;
        }

        public String b() {
            return this.e;
        }
    }

    public ab() {
        this.h = 0;
    }

    public ab(int i, long j) {
        this();
        this.f404a = i;
        a(true);
        this.f405b = j;
        b(true);
    }

    public boolean a() {
        return as.a(this.h, 0);
    }

    public void a(boolean z) {
        this.h = as.a(this.h, 0, z);
    }

    public boolean b() {
        return as.a(this.h, 1);
    }

    public void b(boolean z) {
        this.h = as.a(this.h, 1, z);
    }

    public void a(bn bnVar) throws ax {
        g.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        g.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        return "Latent(" + "latency:" + this.f404a + ", " + "interval:" + this.f405b + ")";
    }

    public void c() throws ax {
    }

    /* compiled from: Latent */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Latent */
    private static class a extends bw<ab> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, ab abVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!abVar.a()) {
                        throw new bo("Required field 'latency' was not found in serialized data! Struct: " + toString());
                    } else if (!abVar.b()) {
                        throw new bo("Required field 'interval' was not found in serialized data! Struct: " + toString());
                    } else {
                        abVar.c();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                abVar.f404a = bnVar.s();
                                abVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                abVar.f405b = bnVar.t();
                                abVar.b(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, ab abVar) throws ax {
            abVar.c();
            bnVar.a(ab.d);
            bnVar.a(ab.e);
            bnVar.a(abVar.f404a);
            bnVar.b();
            bnVar.a(ab.f);
            bnVar.a(abVar.f405b);
            bnVar.b();
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Latent */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Latent */
    private static class c extends bx<ab> {
        private c() {
        }

        public void a(bn bnVar, ab abVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(abVar.f404a);
            btVar.a(abVar.f405b);
        }

        public void b(bn bnVar, ab abVar) throws ax {
            bt btVar = (bt) bnVar;
            abVar.f404a = btVar.s();
            abVar.a(true);
            abVar.f405b = btVar.t();
            abVar.b(true);
        }
    }
}
