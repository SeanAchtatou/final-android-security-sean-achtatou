package b.a;

import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: AppInfo */
public class n implements au<n, e>, Serializable, Cloneable {
    public static final Map<e, bc> k;
    /* access modifiers changed from: private */
    public static final bs l = new bs("AppInfo");
    /* access modifiers changed from: private */
    public static final bk m = new bk("key", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk n = new bk("version", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk o = new bk("version_index", (byte) 8, 3);
    /* access modifiers changed from: private */
    public static final bk p = new bk(Parameters.PACKAGE_NAME, (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final bk q = new bk("sdk_type", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final bk r = new bk("sdk_version", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final bk s = new bk(LogBuilder.KEY_CHANNEL, (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final bk t = new bk("wrapper_type", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final bk u = new bk("wrapper_version", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final bk v = new bk("vertical_type", (byte) 8, 10);
    private static final Map<Class<? extends bu>, bv> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f550a;

    /* renamed from: b  reason: collision with root package name */
    public String f551b;
    public int c;
    public String d;
    public ai e;
    public String f;
    public String g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private e[] y = {e.VERSION, e.VERSION_INDEX, e.PACKAGE_NAME, e.WRAPPER_TYPE, e.WRAPPER_VERSION, e.VERTICAL_TYPE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.n$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(bw.class, new b());
        w.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.KEY, (Object) new bc("key", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.VERSION, (Object) new bc("version", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.VERSION_INDEX, (Object) new bc("version_index", (byte) 2, new bd((byte) 8)));
        enumMap.put((Object) e.PACKAGE_NAME, (Object) new bc(Parameters.PACKAGE_NAME, (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.SDK_TYPE, (Object) new bc("sdk_type", (byte) 1, new bb((byte) 16, ai.class)));
        enumMap.put((Object) e.SDK_VERSION, (Object) new bc("sdk_version", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.CHANNEL, (Object) new bc(LogBuilder.KEY_CHANNEL, (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.WRAPPER_TYPE, (Object) new bc("wrapper_type", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.WRAPPER_VERSION, (Object) new bc("wrapper_version", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.VERTICAL_TYPE, (Object) new bc("vertical_type", (byte) 2, new bd((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        bc.a(n.class, k);
    }

    /* compiled from: AppInfo */
    public enum e implements ay {
        KEY(1, "key"),
        VERSION(2, "version"),
        VERSION_INDEX(3, "version_index"),
        PACKAGE_NAME(4, Parameters.PACKAGE_NAME),
        SDK_TYPE(5, "sdk_type"),
        SDK_VERSION(6, "sdk_version"),
        CHANNEL(7, LogBuilder.KEY_CHANNEL),
        WRAPPER_TYPE(8, "wrapper_type"),
        WRAPPER_VERSION(9, "wrapper_version"),
        VERTICAL_TYPE(10, "vertical_type");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public n a(String str) {
        this.f550a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f550a = null;
        }
    }

    public n b(String str) {
        this.f551b = str;
        return this;
    }

    public boolean a() {
        return this.f551b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f551b = null;
        }
    }

    public n a(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean b() {
        return as.a(this.x, 0);
    }

    public void c(boolean z) {
        this.x = as.a(this.x, 0, z);
    }

    public n c(String str) {
        this.d = str;
        return this;
    }

    public boolean c() {
        return this.d != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public n a(ai aiVar) {
        this.e = aiVar;
        return this;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public n d(String str) {
        this.f = str;
        return this;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public n e(String str) {
        this.g = str;
        return this;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public n f(String str) {
        this.h = str;
        return this;
    }

    public boolean d() {
        return this.h != null;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public n g(String str) {
        this.i = str;
        return this;
    }

    public boolean e() {
        return this.i != null;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public n b(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public boolean f() {
        return as.a(this.x, 1);
    }

    public void j(boolean z) {
        this.x = as.a(this.x, 1, z);
    }

    public void a(bn bnVar) throws ax {
        w.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        w.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("AppInfo(");
        sb.append("key:");
        if (this.f550a == null) {
            sb.append("null");
        } else {
            sb.append(this.f550a);
        }
        if (a()) {
            sb.append(", ");
            sb.append("version:");
            if (this.f551b == null) {
                sb.append("null");
            } else {
                sb.append(this.f551b);
            }
        }
        if (b()) {
            sb.append(", ");
            sb.append("version_index:");
            sb.append(this.c);
        }
        if (c()) {
            sb.append(", ");
            sb.append("package_name:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(", ");
        sb.append("sdk_type:");
        if (this.e == null) {
            sb.append("null");
        } else {
            sb.append(this.e);
        }
        sb.append(", ");
        sb.append("sdk_version:");
        if (this.f == null) {
            sb.append("null");
        } else {
            sb.append(this.f);
        }
        sb.append(", ");
        sb.append("channel:");
        if (this.g == null) {
            sb.append("null");
        } else {
            sb.append(this.g);
        }
        if (d()) {
            sb.append(", ");
            sb.append("wrapper_type:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
        }
        if (e()) {
            sb.append(", ");
            sb.append("wrapper_version:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("vertical_type:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }

    public void g() throws ax {
        if (this.f550a == null) {
            throw new bo("Required field 'key' was not present! Struct: " + toString());
        } else if (this.e == null) {
            throw new bo("Required field 'sdk_type' was not present! Struct: " + toString());
        } else if (this.f == null) {
            throw new bo("Required field 'sdk_version' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new bo("Required field 'channel' was not present! Struct: " + toString());
        }
    }

    /* compiled from: AppInfo */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: AppInfo */
    private static class a extends bw<n> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, n nVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    nVar.g();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.f550a = bnVar.v();
                            nVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.f551b = bnVar.v();
                            nVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.c = bnVar.s();
                            nVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.d = bnVar.v();
                            nVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.e = ai.a(bnVar.s());
                            nVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.f = bnVar.v();
                            nVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.g = bnVar.v();
                            nVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.h = bnVar.v();
                            nVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.i = bnVar.v();
                            nVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            nVar.j = bnVar.s();
                            nVar.j(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, n nVar) throws ax {
            nVar.g();
            bnVar.a(n.l);
            if (nVar.f550a != null) {
                bnVar.a(n.m);
                bnVar.a(nVar.f550a);
                bnVar.b();
            }
            if (nVar.f551b != null && nVar.a()) {
                bnVar.a(n.n);
                bnVar.a(nVar.f551b);
                bnVar.b();
            }
            if (nVar.b()) {
                bnVar.a(n.o);
                bnVar.a(nVar.c);
                bnVar.b();
            }
            if (nVar.d != null && nVar.c()) {
                bnVar.a(n.p);
                bnVar.a(nVar.d);
                bnVar.b();
            }
            if (nVar.e != null) {
                bnVar.a(n.q);
                bnVar.a(nVar.e.a());
                bnVar.b();
            }
            if (nVar.f != null) {
                bnVar.a(n.r);
                bnVar.a(nVar.f);
                bnVar.b();
            }
            if (nVar.g != null) {
                bnVar.a(n.s);
                bnVar.a(nVar.g);
                bnVar.b();
            }
            if (nVar.h != null && nVar.d()) {
                bnVar.a(n.t);
                bnVar.a(nVar.h);
                bnVar.b();
            }
            if (nVar.i != null && nVar.e()) {
                bnVar.a(n.u);
                bnVar.a(nVar.i);
                bnVar.b();
            }
            if (nVar.f()) {
                bnVar.a(n.v);
                bnVar.a(nVar.j);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: AppInfo */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: AppInfo */
    private static class c extends bx<n> {
        private c() {
        }

        public void a(bn bnVar, n nVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(nVar.f550a);
            btVar.a(nVar.e.a());
            btVar.a(nVar.f);
            btVar.a(nVar.g);
            BitSet bitSet = new BitSet();
            if (nVar.a()) {
                bitSet.set(0);
            }
            if (nVar.b()) {
                bitSet.set(1);
            }
            if (nVar.c()) {
                bitSet.set(2);
            }
            if (nVar.d()) {
                bitSet.set(3);
            }
            if (nVar.e()) {
                bitSet.set(4);
            }
            if (nVar.f()) {
                bitSet.set(5);
            }
            btVar.a(bitSet, 6);
            if (nVar.a()) {
                btVar.a(nVar.f551b);
            }
            if (nVar.b()) {
                btVar.a(nVar.c);
            }
            if (nVar.c()) {
                btVar.a(nVar.d);
            }
            if (nVar.d()) {
                btVar.a(nVar.h);
            }
            if (nVar.e()) {
                btVar.a(nVar.i);
            }
            if (nVar.f()) {
                btVar.a(nVar.j);
            }
        }

        public void b(bn bnVar, n nVar) throws ax {
            bt btVar = (bt) bnVar;
            nVar.f550a = btVar.v();
            nVar.a(true);
            nVar.e = ai.a(btVar.s());
            nVar.e(true);
            nVar.f = btVar.v();
            nVar.f(true);
            nVar.g = btVar.v();
            nVar.g(true);
            BitSet b2 = btVar.b(6);
            if (b2.get(0)) {
                nVar.f551b = btVar.v();
                nVar.b(true);
            }
            if (b2.get(1)) {
                nVar.c = btVar.s();
                nVar.c(true);
            }
            if (b2.get(2)) {
                nVar.d = btVar.v();
                nVar.d(true);
            }
            if (b2.get(3)) {
                nVar.h = btVar.v();
                nVar.h(true);
            }
            if (b2.get(4)) {
                nVar.i = btVar.v();
                nVar.i(true);
            }
            if (b2.get(5)) {
                nVar.j = btVar.s();
                nVar.j(true);
            }
        }
    }
}
