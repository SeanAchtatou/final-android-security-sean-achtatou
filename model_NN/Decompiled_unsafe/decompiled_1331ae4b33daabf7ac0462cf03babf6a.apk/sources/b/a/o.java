package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: ClientStats */
public class o implements au<o, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("ClientStats");
    /* access modifiers changed from: private */
    public static final bk f = new bk("successful_requests", (byte) 8, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("failed_requests", (byte) 8, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("last_request_spent_ms", (byte) 8, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public int f554a = 0;

    /* renamed from: b  reason: collision with root package name */
    public int f555b = 0;
    public int c;
    private byte j = 0;
    private e[] k = {e.LAST_REQUEST_SPENT_MS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.o$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.SUCCESSFUL_REQUESTS, (Object) new bc("successful_requests", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.FAILED_REQUESTS, (Object) new bc("failed_requests", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.LAST_REQUEST_SPENT_MS, (Object) new bc("last_request_spent_ms", (byte) 2, new bd((byte) 8)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(o.class, d);
    }

    /* compiled from: ClientStats */
    public enum e implements ay {
        SUCCESSFUL_REQUESTS(1, "successful_requests"),
        FAILED_REQUESTS(2, "failed_requests"),
        LAST_REQUEST_SPENT_MS(3, "last_request_spent_ms");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public o a(int i2) {
        this.f554a = i2;
        a(true);
        return this;
    }

    public boolean a() {
        return as.a(this.j, 0);
    }

    public void a(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public o b(int i2) {
        this.f555b = i2;
        b(true);
        return this;
    }

    public boolean b() {
        return as.a(this.j, 1);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 1, z);
    }

    public o c(int i2) {
        this.c = i2;
        c(true);
        return this;
    }

    public boolean c() {
        return as.a(this.j, 2);
    }

    public void c(boolean z) {
        this.j = as.a(this.j, 2, z);
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("ClientStats(");
        sb.append("successful_requests:");
        sb.append(this.f554a);
        sb.append(", ");
        sb.append("failed_requests:");
        sb.append(this.f555b);
        if (c()) {
            sb.append(", ");
            sb.append("last_request_spent_ms:");
            sb.append(this.c);
        }
        sb.append(")");
        return sb.toString();
    }

    public void d() throws ax {
    }

    /* compiled from: ClientStats */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: ClientStats */
    private static class a extends bw<o> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, o oVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!oVar.a()) {
                        throw new bo("Required field 'successful_requests' was not found in serialized data! Struct: " + toString());
                    } else if (!oVar.b()) {
                        throw new bo("Required field 'failed_requests' was not found in serialized data! Struct: " + toString());
                    } else {
                        oVar.d();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                oVar.f554a = bnVar.s();
                                oVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                oVar.f555b = bnVar.s();
                                oVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                oVar.c = bnVar.s();
                                oVar.c(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, o oVar) throws ax {
            oVar.d();
            bnVar.a(o.e);
            bnVar.a(o.f);
            bnVar.a(oVar.f554a);
            bnVar.b();
            bnVar.a(o.g);
            bnVar.a(oVar.f555b);
            bnVar.b();
            if (oVar.c()) {
                bnVar.a(o.h);
                bnVar.a(oVar.c);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: ClientStats */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: ClientStats */
    private static class c extends bx<o> {
        private c() {
        }

        public void a(bn bnVar, o oVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(oVar.f554a);
            btVar.a(oVar.f555b);
            BitSet bitSet = new BitSet();
            if (oVar.c()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (oVar.c()) {
                btVar.a(oVar.c);
            }
        }

        public void b(bn bnVar, o oVar) throws ax {
            bt btVar = (bt) bnVar;
            oVar.f554a = btVar.s();
            oVar.a(true);
            oVar.f555b = btVar.s();
            oVar.b(true);
            if (btVar.b(1).get(0)) {
                oVar.c = btVar.s();
                oVar.c(true);
            }
        }
    }
}
