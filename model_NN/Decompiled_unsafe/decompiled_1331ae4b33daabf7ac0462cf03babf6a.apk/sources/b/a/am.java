package b.a;

import com.igexin.download.Downloads;
import com.tencent.open.GameAppOperation;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: UMEnvelope */
public class am implements au<am, e>, Serializable, Cloneable {
    public static final Map<e, bc> k;
    /* access modifiers changed from: private */
    public static final bs l = new bs("UMEnvelope");
    /* access modifiers changed from: private */
    public static final bk m = new bk("version", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk n = new bk("address", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk o = new bk(GameAppOperation.GAME_SIGNATURE, (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final bk p = new bk("serial_num", (byte) 8, 4);
    /* access modifiers changed from: private */
    public static final bk q = new bk("ts_secs", (byte) 8, 5);
    /* access modifiers changed from: private */
    public static final bk r = new bk("length", (byte) 8, 6);
    /* access modifiers changed from: private */
    public static final bk s = new bk(Downloads.COLUMN_APP_DATA, (byte) 11, 7);
    /* access modifiers changed from: private */
    public static final bk t = new bk("guid", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final bk u = new bk("checksum", (byte) 11, 9);
    /* access modifiers changed from: private */
    public static final bk v = new bk("codex", (byte) 8, 10);
    private static final Map<Class<? extends bu>, bv> w = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f446a;

    /* renamed from: b  reason: collision with root package name */
    public String f447b;
    public String c;
    public int d;
    public int e;
    public int f;
    public ByteBuffer g;
    public String h;
    public String i;
    public int j;
    private byte x = 0;
    private e[] y = {e.CODEX};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.am$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        w.put(bw.class, new b());
        w.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.VERSION, (Object) new bc("version", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.ADDRESS, (Object) new bc("address", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.SIGNATURE, (Object) new bc(GameAppOperation.GAME_SIGNATURE, (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.SERIAL_NUM, (Object) new bc("serial_num", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.TS_SECS, (Object) new bc("ts_secs", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.LENGTH, (Object) new bc("length", (byte) 1, new bd((byte) 8)));
        enumMap.put((Object) e.ENTITY, (Object) new bc(Downloads.COLUMN_APP_DATA, (byte) 1, new bd((byte) 11, true)));
        enumMap.put((Object) e.GUID, (Object) new bc("guid", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.CHECKSUM, (Object) new bc("checksum", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.CODEX, (Object) new bc("codex", (byte) 2, new bd((byte) 8)));
        k = Collections.unmodifiableMap(enumMap);
        bc.a(am.class, k);
    }

    /* compiled from: UMEnvelope */
    public enum e implements ay {
        VERSION(1, "version"),
        ADDRESS(2, "address"),
        SIGNATURE(3, GameAppOperation.GAME_SIGNATURE),
        SERIAL_NUM(4, "serial_num"),
        TS_SECS(5, "ts_secs"),
        LENGTH(6, "length"),
        ENTITY(7, Downloads.COLUMN_APP_DATA),
        GUID(8, "guid"),
        CHECKSUM(9, "checksum"),
        CODEX(10, "codex");
        
        private static final Map<String, e> k = new HashMap();
        private final short l;
        private final String m;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                k.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.l = s;
            this.m = str;
        }

        public short a() {
            return this.l;
        }

        public String b() {
            return this.m;
        }
    }

    public am a(String str) {
        this.f446a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f446a = null;
        }
    }

    public am b(String str) {
        this.f447b = str;
        return this;
    }

    public void b(boolean z) {
        if (!z) {
            this.f447b = null;
        }
    }

    public am c(String str) {
        this.c = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public am a(int i2) {
        this.d = i2;
        d(true);
        return this;
    }

    public boolean a() {
        return as.a(this.x, 0);
    }

    public void d(boolean z) {
        this.x = as.a(this.x, 0, z);
    }

    public am b(int i2) {
        this.e = i2;
        e(true);
        return this;
    }

    public boolean b() {
        return as.a(this.x, 1);
    }

    public void e(boolean z) {
        this.x = as.a(this.x, 1, z);
    }

    public am c(int i2) {
        this.f = i2;
        f(true);
        return this;
    }

    public boolean c() {
        return as.a(this.x, 2);
    }

    public void f(boolean z) {
        this.x = as.a(this.x, 2, z);
    }

    public am a(byte[] bArr) {
        a(bArr == null ? null : ByteBuffer.wrap(bArr));
        return this;
    }

    public am a(ByteBuffer byteBuffer) {
        this.g = byteBuffer;
        return this;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public am d(String str) {
        this.h = str;
        return this;
    }

    public void h(boolean z) {
        if (!z) {
            this.h = null;
        }
    }

    public am e(String str) {
        this.i = str;
        return this;
    }

    public void i(boolean z) {
        if (!z) {
            this.i = null;
        }
    }

    public am d(int i2) {
        this.j = i2;
        j(true);
        return this;
    }

    public boolean d() {
        return as.a(this.x, 3);
    }

    public void j(boolean z) {
        this.x = as.a(this.x, 3, z);
    }

    public void a(bn bnVar) throws ax {
        w.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        w.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("UMEnvelope(");
        sb.append("version:");
        if (this.f446a == null) {
            sb.append("null");
        } else {
            sb.append(this.f446a);
        }
        sb.append(", ");
        sb.append("address:");
        if (this.f447b == null) {
            sb.append("null");
        } else {
            sb.append(this.f447b);
        }
        sb.append(", ");
        sb.append("signature:");
        if (this.c == null) {
            sb.append("null");
        } else {
            sb.append(this.c);
        }
        sb.append(", ");
        sb.append("serial_num:");
        sb.append(this.d);
        sb.append(", ");
        sb.append("ts_secs:");
        sb.append(this.e);
        sb.append(", ");
        sb.append("length:");
        sb.append(this.f);
        sb.append(", ");
        sb.append("entity:");
        if (this.g == null) {
            sb.append("null");
        } else {
            av.a(this.g, sb);
        }
        sb.append(", ");
        sb.append("guid:");
        if (this.h == null) {
            sb.append("null");
        } else {
            sb.append(this.h);
        }
        sb.append(", ");
        sb.append("checksum:");
        if (this.i == null) {
            sb.append("null");
        } else {
            sb.append(this.i);
        }
        if (d()) {
            sb.append(", ");
            sb.append("codex:");
            sb.append(this.j);
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ax {
        if (this.f446a == null) {
            throw new bo("Required field 'version' was not present! Struct: " + toString());
        } else if (this.f447b == null) {
            throw new bo("Required field 'address' was not present! Struct: " + toString());
        } else if (this.c == null) {
            throw new bo("Required field 'signature' was not present! Struct: " + toString());
        } else if (this.g == null) {
            throw new bo("Required field 'entity' was not present! Struct: " + toString());
        } else if (this.h == null) {
            throw new bo("Required field 'guid' was not present! Struct: " + toString());
        } else if (this.i == null) {
            throw new bo("Required field 'checksum' was not present! Struct: " + toString());
        }
    }

    /* compiled from: UMEnvelope */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class a extends bw<am> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, am amVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!amVar.a()) {
                        throw new bo("Required field 'serial_num' was not found in serialized data! Struct: " + toString());
                    } else if (!amVar.b()) {
                        throw new bo("Required field 'ts_secs' was not found in serialized data! Struct: " + toString());
                    } else if (!amVar.c()) {
                        throw new bo("Required field 'length' was not found in serialized data! Struct: " + toString());
                    } else {
                        amVar.e();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.f446a = bnVar.v();
                                amVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.f447b = bnVar.v();
                                amVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.c = bnVar.v();
                                amVar.c(true);
                                break;
                            }
                        case 4:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.d = bnVar.s();
                                amVar.d(true);
                                break;
                            }
                        case 5:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.e = bnVar.s();
                                amVar.e(true);
                                break;
                            }
                        case 6:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.f = bnVar.s();
                                amVar.f(true);
                                break;
                            }
                        case 7:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.g = bnVar.w();
                                amVar.g(true);
                                break;
                            }
                        case 8:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.h = bnVar.v();
                                amVar.h(true);
                                break;
                            }
                        case 9:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.i = bnVar.v();
                                amVar.i(true);
                                break;
                            }
                        case 10:
                            if (h.f487b != 8) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                amVar.j = bnVar.s();
                                amVar.j(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, am amVar) throws ax {
            amVar.e();
            bnVar.a(am.l);
            if (amVar.f446a != null) {
                bnVar.a(am.m);
                bnVar.a(amVar.f446a);
                bnVar.b();
            }
            if (amVar.f447b != null) {
                bnVar.a(am.n);
                bnVar.a(amVar.f447b);
                bnVar.b();
            }
            if (amVar.c != null) {
                bnVar.a(am.o);
                bnVar.a(amVar.c);
                bnVar.b();
            }
            bnVar.a(am.p);
            bnVar.a(amVar.d);
            bnVar.b();
            bnVar.a(am.q);
            bnVar.a(amVar.e);
            bnVar.b();
            bnVar.a(am.r);
            bnVar.a(amVar.f);
            bnVar.b();
            if (amVar.g != null) {
                bnVar.a(am.s);
                bnVar.a(amVar.g);
                bnVar.b();
            }
            if (amVar.h != null) {
                bnVar.a(am.t);
                bnVar.a(amVar.h);
                bnVar.b();
            }
            if (amVar.i != null) {
                bnVar.a(am.u);
                bnVar.a(amVar.i);
                bnVar.b();
            }
            if (amVar.d()) {
                bnVar.a(am.v);
                bnVar.a(amVar.j);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: UMEnvelope */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: UMEnvelope */
    private static class c extends bx<am> {
        private c() {
        }

        public void a(bn bnVar, am amVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(amVar.f446a);
            btVar.a(amVar.f447b);
            btVar.a(amVar.c);
            btVar.a(amVar.d);
            btVar.a(amVar.e);
            btVar.a(amVar.f);
            btVar.a(amVar.g);
            btVar.a(amVar.h);
            btVar.a(amVar.i);
            BitSet bitSet = new BitSet();
            if (amVar.d()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (amVar.d()) {
                btVar.a(amVar.j);
            }
        }

        public void b(bn bnVar, am amVar) throws ax {
            bt btVar = (bt) bnVar;
            amVar.f446a = btVar.v();
            amVar.a(true);
            amVar.f447b = btVar.v();
            amVar.b(true);
            amVar.c = btVar.v();
            amVar.c(true);
            amVar.d = btVar.s();
            amVar.d(true);
            amVar.e = btVar.s();
            amVar.e(true);
            amVar.f = btVar.s();
            amVar.f(true);
            amVar.g = btVar.w();
            amVar.g(true);
            amVar.h = btVar.v();
            amVar.h(true);
            amVar.i = btVar.v();
            amVar.i(true);
            if (btVar.b(1).get(0)) {
                amVar.j = btVar.s();
                amVar.j(true);
            }
        }
    }
}
