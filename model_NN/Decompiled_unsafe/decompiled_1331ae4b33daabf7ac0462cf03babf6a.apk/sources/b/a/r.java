package b.a;

import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: Error */
public class r implements au<r, e>, Serializable, Cloneable {
    public static final Map<e, bc> d;
    /* access modifiers changed from: private */
    public static final bs e = new bs("Error");
    /* access modifiers changed from: private */
    public static final bk f = new bk("ts", (byte) 10, 1);
    /* access modifiers changed from: private */
    public static final bk g = new bk("context", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk h = new bk("source", (byte) 8, 3);
    private static final Map<Class<? extends bu>, bv> i = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public long f566a;

    /* renamed from: b  reason: collision with root package name */
    public String f567b;
    public s c;
    private byte j = 0;
    private e[] k = {e.SOURCE};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.r$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        i.put(bw.class, new b());
        i.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.TS, (Object) new bc("ts", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.CONTEXT, (Object) new bc("context", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.SOURCE, (Object) new bc("source", (byte) 2, new bb((byte) 16, s.class)));
        d = Collections.unmodifiableMap(enumMap);
        bc.a(r.class, d);
    }

    /* compiled from: Error */
    public enum e implements ay {
        TS(1, "ts"),
        CONTEXT(2, "context"),
        SOURCE(3, "source");
        
        private static final Map<String, e> d = new HashMap();
        private final short e;
        private final String f;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                d.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.e = s;
            this.f = str;
        }

        public short a() {
            return this.e;
        }

        public String b() {
            return this.f;
        }
    }

    public r a(long j2) {
        this.f566a = j2;
        b(true);
        return this;
    }

    public boolean a() {
        return as.a(this.j, 0);
    }

    public void b(boolean z) {
        this.j = as.a(this.j, 0, z);
    }

    public r a(String str) {
        this.f567b = str;
        return this;
    }

    public void c(boolean z) {
        if (!z) {
            this.f567b = null;
        }
    }

    public r a(s sVar) {
        this.c = sVar;
        return this;
    }

    public boolean b() {
        return this.c != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public void a(bn bnVar) throws ax {
        i.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        i.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Error(");
        sb.append("ts:");
        sb.append(this.f566a);
        sb.append(", ");
        sb.append("context:");
        if (this.f567b == null) {
            sb.append("null");
        } else {
            sb.append(this.f567b);
        }
        if (b()) {
            sb.append(", ");
            sb.append("source:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void c() throws ax {
        if (this.f567b == null) {
            throw new bo("Required field 'context' was not present! Struct: " + toString());
        }
    }

    /* compiled from: Error */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Error */
    private static class a extends bw<r> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, r rVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!rVar.a()) {
                        throw new bo("Required field 'ts' was not found in serialized data! Struct: " + toString());
                    }
                    rVar.c();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            rVar.f566a = bnVar.t();
                            rVar.b(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            rVar.f567b = bnVar.v();
                            rVar.c(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 8) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            rVar.c = s.a(bnVar.s());
                            rVar.d(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, r rVar) throws ax {
            rVar.c();
            bnVar.a(r.e);
            bnVar.a(r.f);
            bnVar.a(rVar.f566a);
            bnVar.b();
            if (rVar.f567b != null) {
                bnVar.a(r.g);
                bnVar.a(rVar.f567b);
                bnVar.b();
            }
            if (rVar.c != null && rVar.b()) {
                bnVar.a(r.h);
                bnVar.a(rVar.c.a());
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Error */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Error */
    private static class c extends bx<r> {
        private c() {
        }

        public void a(bn bnVar, r rVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(rVar.f566a);
            btVar.a(rVar.f567b);
            BitSet bitSet = new BitSet();
            if (rVar.b()) {
                bitSet.set(0);
            }
            btVar.a(bitSet, 1);
            if (rVar.b()) {
                btVar.a(rVar.c.a());
            }
        }

        public void b(bn bnVar, r rVar) throws ax {
            bt btVar = (bt) bnVar;
            rVar.f566a = btVar.t();
            rVar.b(true);
            rVar.f567b = btVar.v();
            rVar.c(true);
            if (btVar.b(1).get(0)) {
                rVar.c = s.a(btVar.s());
                rVar.d(true);
            }
        }
    }
}
