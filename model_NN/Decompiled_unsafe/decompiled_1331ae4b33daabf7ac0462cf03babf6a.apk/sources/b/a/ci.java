package b.a;

import android.content.Context;
import android.text.TextUtils;
import com.umeng.analytics.a.c;
import com.umeng.analytics.c;
import com.umeng.analytics.e;
import com.umeng.analytics.i;
import com.umeng.analytics.j;
import com.umeng.analytics.k;

/* compiled from: CacheImpl */
public final class ci implements cn, ct, c {

    /* renamed from: a  reason: collision with root package name */
    private cq f511a = null;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public k f512b = null;
    /* access modifiers changed from: private */
    public b c = null;
    /* access modifiers changed from: private */
    public j d = new j();
    private a e = null;
    private int f = 10;
    private int g;
    /* access modifiers changed from: private */
    public Context h;

    public ci(Context context) {
        this.h = context;
        this.f511a = new cq(context);
        this.c = new b(context);
        this.f512b = k.a(context);
        this.d.a(this.f512b.c());
        this.e = new a();
        this.g = this.f512b.d(-1);
    }

    public void a() {
        if (an.g(this.h)) {
            d();
        } else if (ao.f451a) {
            ao.c("MobclickAgent", "network is unavailable");
        }
    }

    public void a(co coVar) {
        if (coVar != null) {
            this.f511a.a(coVar);
        }
        a(coVar instanceof aj);
    }

    public void b(co coVar) {
        this.f511a.a(coVar);
    }

    public void b() {
        if (this.f511a.a() > 0) {
            try {
                byte[] b2 = b(a(new int[0]));
                if (b2 != null) {
                    this.f512b.a(b2);
                }
            } catch (Throwable th) {
                if (th instanceof OutOfMemoryError) {
                    this.f512b.f();
                }
                if (th != null) {
                    th.printStackTrace();
                }
            }
        }
    }

    private void a(boolean z) {
        boolean a2 = this.c.a();
        if (a2) {
            this.f511a.a(new l(this.c.i()));
        }
        if (b(z)) {
            d();
        } else if (a2 || c()) {
            b();
        }
    }

    private void d(int i) {
        a(a(i, (int) (System.currentTimeMillis() - this.c.j())));
        i.a(new j() {
            public void a() {
                ci.this.a();
            }
        }, (long) i);
    }

    private void a(al alVar) {
        ap a2;
        if (alVar != null) {
            bi a3 = bi.a(this.h);
            a3.a();
            alVar.a(a3.b());
            byte[] b2 = b(alVar);
            if (b2 != null) {
                if (e()) {
                    a2 = ap.b(this.h, com.umeng.analytics.a.a(this.h), b2);
                } else {
                    a2 = ap.a(this.h, com.umeng.analytics.a.a(this.h), b2);
                }
                byte[] c2 = a2.c();
                k a4 = k.a(this.h);
                a4.f();
                a4.b(c2);
                a3.c();
            }
        }
    }

    /* access modifiers changed from: protected */
    public al a(int... iArr) {
        al alVar;
        boolean z;
        boolean z2 = false;
        try {
            if (TextUtils.isEmpty(com.umeng.analytics.a.a(this.h))) {
                ao.b("MobclickAgent", "Appkey is missing ,Please check AndroidManifest.xml");
                return null;
            }
            byte[] e2 = k.a(this.h).e();
            al a2 = e2 == null ? null : a(e2);
            if (a2 == null && this.f511a.a() == 0) {
                return null;
            }
            if (a2 == null) {
                alVar = new al();
            } else {
                alVar = a2;
            }
            this.f511a.a(alVar);
            if (ao.f451a && alVar.f()) {
                for (aj d2 : alVar.e()) {
                    if (d2.d() > 0) {
                        z = true;
                    } else {
                        z = z2;
                    }
                    z2 = z;
                }
                if (!z2) {
                    ao.d("MobclickAgent", "missing Activities or PageViews");
                }
            }
            al a3 = this.d.a(this.h, alVar);
            if (iArr == null || iArr.length != 2) {
                return a3;
            }
            p pVar = new p();
            pVar.a(new ab(iArr[0] / 1000, (long) iArr[1]));
            a3.a(pVar);
            return a3;
        } catch (Exception e3) {
            ao.b("MobclickAgent", "Fail to construct message ...", e3);
            k.a(this.h).f();
            return null;
        }
    }

    private al a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        try {
            al alVar = new al();
            new aw().a(alVar, bArr);
            return alVar;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private byte[] b(al alVar) {
        if (alVar == null) {
            return null;
        }
        try {
            byte[] a2 = new az().a(alVar);
            if (ao.f451a) {
                ao.c("MobclickAgent", alVar.toString());
            }
            return a2;
        } catch (Exception e2) {
            ao.b("MobclickAgent", "Fail to serialize log ...", e2);
            return null;
        }
    }

    private boolean b(boolean z) {
        if (!an.g(this.h)) {
            if (ao.f451a) {
                ao.c("MobclickAgent", "network is unavailable");
            }
            return false;
        } else if (this.c.a()) {
            return true;
        } else {
            if (!ao.f451a || !an.q(this.h)) {
                return this.e.c().a(z);
            }
            return true;
        }
    }

    private boolean c() {
        return this.f511a.a() > this.f;
    }

    private void d() {
        try {
            if (this.f512b.g()) {
                cv cvVar = new cv(this.h, this.c);
                cvVar.a(this);
                if (this.d.b()) {
                    cvVar.b(true);
                }
                cvVar.a();
                return;
            }
            al a2 = a(new int[0]);
            if (a2 == null) {
                ao.a("MobclickAgent", "No data to report");
                return;
            }
            cv cvVar2 = new cv(this.h, this.c);
            cvVar2.a(this);
            if (this.d.b()) {
                cvVar2.b(true);
            }
            cvVar2.a(a2);
            cvVar2.a(e());
            cvVar2.a();
        } catch (Throwable th) {
            if (th instanceof OutOfMemoryError) {
            }
            if (th != null) {
                th.printStackTrace();
            }
        }
    }

    private boolean e() {
        switch (this.g) {
            case -1:
                return com.umeng.analytics.a.f;
            case 0:
            default:
                return false;
            case 1:
                return true;
        }
    }

    /* access modifiers changed from: private */
    public void e(int i) {
        d(i);
    }

    public void a(int i) {
        if (i >= 0 && i <= 3) {
            this.d.a(i);
            this.e.b(i);
        }
    }

    public void b(int i) {
        if (i > 0) {
            this.e.a(i);
        }
    }

    public void c(int i) {
        this.g = i;
    }

    /* compiled from: CacheImpl */
    public class a {

        /* renamed from: b  reason: collision with root package name */
        private final long f515b = 1296000000;
        private final int c = 1800000;
        private final int d = 10000;
        private c.f e;
        private int f;
        private int g;
        private int h;
        private int i;
        private boolean j = false;

        public a() {
            this.f = ci.this.f512b.c();
            int d2 = ci.this.f512b.d();
            if (d2 > 0) {
                this.g = c(d2);
            } else if (com.umeng.analytics.a.g > 0) {
                this.g = c(com.umeng.analytics.a.g);
            } else {
                this.g = 10000;
            }
            int[] b2 = ci.this.f512b.b();
            this.h = b2[0];
            this.i = b2[1];
        }

        /* access modifiers changed from: protected */
        public void a() {
            c.f aVar;
            boolean z = true;
            if (this.f > 0) {
                if (!(this.e instanceof c.a) || !this.e.a()) {
                    z = false;
                }
                if (z) {
                    aVar = this.e;
                } else {
                    aVar = new c.a(ci.this.c, ci.this.d);
                }
                this.e = aVar;
            } else {
                if (!(this.e instanceof c.b) || !this.e.a()) {
                    z = false;
                }
                if (!z) {
                    if (b()) {
                        int a2 = e.a(this.g, ap.a(ci.this.h));
                        this.e = new c.b(a2);
                        ci.this.e(a2);
                    } else {
                        this.e = a(this.h, this.i);
                    }
                }
            }
            this.j = false;
        }

        /* access modifiers changed from: protected */
        public boolean b() {
            if (!ci.this.f512b.g() && !ci.this.c.a() && System.currentTimeMillis() - ci.this.c.j() > 1296000000) {
                return true;
            }
            return false;
        }

        public c.f c() {
            a();
            return this.e;
        }

        private c.f a(int i2, int i3) {
            switch (i2) {
                case 0:
                    return new c.f();
                case 1:
                    return new c.C0058c();
                case 2:
                case 3:
                default:
                    return new c.C0058c();
                case 4:
                    return new c.e(ci.this.c);
                case 5:
                    return new c.g(ci.this.h);
                case 6:
                    return new c.d(ci.this.c, (long) i3);
            }
        }

        private int c(int i2) {
            if (i2 > 1800000) {
                return 1800000;
            }
            return i2;
        }

        /* access modifiers changed from: protected */
        public void d() {
            this.j = true;
        }

        public void a(int i2) {
            this.g = c(i2);
            d();
        }

        public void b(int i2) {
            this.f = i2;
            d();
        }
    }
}
