package b.a;

import java.io.Serializable;

/* compiled from: FieldValueMetaData */
public class bd implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f470a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f471b;
    private final String c;
    private final boolean d;

    public bd(byte b2, boolean z) {
        this.f471b = b2;
        this.f470a = false;
        this.c = null;
        this.d = z;
    }

    public bd(byte b2) {
        this(b2, false);
    }
}
