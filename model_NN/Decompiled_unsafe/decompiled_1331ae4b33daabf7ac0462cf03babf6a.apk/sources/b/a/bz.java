package b.a;

/* compiled from: TMemoryInputTransport */
public final class bz extends ca {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f499a;

    /* renamed from: b  reason: collision with root package name */
    private int f500b;
    private int c;

    public void a(byte[] bArr) {
        c(bArr, 0, bArr.length);
    }

    public void c(byte[] bArr, int i, int i2) {
        this.f499a = bArr;
        this.f500b = i;
        this.c = i + i2;
    }

    public void a() {
        this.f499a = null;
    }

    public int a(byte[] bArr, int i, int i2) throws cb {
        int d = d();
        if (i2 > d) {
            i2 = d;
        }
        if (i2 > 0) {
            System.arraycopy(this.f499a, this.f500b, bArr, i, i2);
            a(i2);
        }
        return i2;
    }

    public void b(byte[] bArr, int i, int i2) throws cb {
        throw new UnsupportedOperationException("No writing allowed!");
    }

    public byte[] b() {
        return this.f499a;
    }

    public int c() {
        return this.f500b;
    }

    public int d() {
        return this.c - this.f500b;
    }

    public void a(int i) {
        this.f500b += i;
    }
}
