package b.a;

import com.meizu.cloud.pushsdk.pushtracer.storage.EventStoreHelper;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: InstantMsg */
public class aa implements au<aa, e>, Serializable, Cloneable {
    public static final Map<e, bc> e;
    /* access modifiers changed from: private */
    public static final bs f = new bs("InstantMsg");
    /* access modifiers changed from: private */
    public static final bk g = new bk("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk h = new bk("errors", (byte) 15, 2);
    /* access modifiers changed from: private */
    public static final bk i = new bk(EventStoreHelper.TABLE_EVENTS, (byte) 15, 3);
    /* access modifiers changed from: private */
    public static final bk j = new bk("game_events", (byte) 15, 4);
    private static final Map<Class<? extends bu>, bv> k = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f400a;

    /* renamed from: b  reason: collision with root package name */
    public List<r> f401b;
    public List<t> c;
    public List<t> d;
    private e[] l = {e.ERRORS, e.EVENTS, e.GAME_EVENTS};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.aa$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        k.put(bw.class, new b());
        k.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.ID, (Object) new bc("id", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.ERRORS, (Object) new bc("errors", (byte) 2, new be((byte) 15, new bg((byte) 12, r.class))));
        enumMap.put((Object) e.EVENTS, (Object) new bc(EventStoreHelper.TABLE_EVENTS, (byte) 2, new be((byte) 15, new bg((byte) 12, t.class))));
        enumMap.put((Object) e.GAME_EVENTS, (Object) new bc("game_events", (byte) 2, new be((byte) 15, new bg((byte) 12, t.class))));
        e = Collections.unmodifiableMap(enumMap);
        bc.a(aa.class, e);
    }

    /* compiled from: InstantMsg */
    public enum e implements ay {
        ID(1, "id"),
        ERRORS(2, "errors"),
        EVENTS(3, EventStoreHelper.TABLE_EVENTS),
        GAME_EVENTS(4, "game_events");
        
        private static final Map<String, e> e = new HashMap();
        private final short f;
        private final String g;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                e.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.f = s;
            this.g = str;
        }

        public short a() {
            return this.f;
        }

        public String b() {
            return this.g;
        }
    }

    public String a() {
        return this.f400a;
    }

    public aa a(String str) {
        this.f400a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f400a = null;
        }
    }

    public void a(r rVar) {
        if (this.f401b == null) {
            this.f401b = new ArrayList();
        }
        this.f401b.add(rVar);
    }

    public boolean b() {
        return this.f401b != null;
    }

    public void b(boolean z) {
        if (!z) {
            this.f401b = null;
        }
    }

    public void a(t tVar) {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        this.c.add(tVar);
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z) {
        if (!z) {
            this.c = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void d(boolean z) {
        if (!z) {
            this.d = null;
        }
    }

    public void a(bn bnVar) throws ax {
        k.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        k.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("InstantMsg(");
        sb.append("id:");
        if (this.f400a == null) {
            sb.append("null");
        } else {
            sb.append(this.f400a);
        }
        if (b()) {
            sb.append(", ");
            sb.append("errors:");
            if (this.f401b == null) {
                sb.append("null");
            } else {
                sb.append(this.f401b);
            }
        }
        if (c()) {
            sb.append(", ");
            sb.append("events:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
        }
        if (d()) {
            sb.append(", ");
            sb.append("game_events:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void e() throws ax {
        if (this.f400a == null) {
            throw new bo("Required field 'id' was not present! Struct: " + toString());
        }
    }

    /* compiled from: InstantMsg */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: InstantMsg */
    private static class a extends bw<aa> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, aa aaVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    aaVar.e();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            aaVar.f400a = bnVar.v();
                            aaVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l = bnVar.l();
                            aaVar.f401b = new ArrayList(l.f489b);
                            for (int i = 0; i < l.f489b; i++) {
                                r rVar = new r();
                                rVar.a(bnVar);
                                aaVar.f401b.add(rVar);
                            }
                            bnVar.m();
                            aaVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l2 = bnVar.l();
                            aaVar.c = new ArrayList(l2.f489b);
                            for (int i2 = 0; i2 < l2.f489b; i2++) {
                                t tVar = new t();
                                tVar.a(bnVar);
                                aaVar.c.add(tVar);
                            }
                            bnVar.m();
                            aaVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 15) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            bl l3 = bnVar.l();
                            aaVar.d = new ArrayList(l3.f489b);
                            for (int i3 = 0; i3 < l3.f489b; i3++) {
                                t tVar2 = new t();
                                tVar2.a(bnVar);
                                aaVar.d.add(tVar2);
                            }
                            bnVar.m();
                            aaVar.d(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, aa aaVar) throws ax {
            aaVar.e();
            bnVar.a(aa.f);
            if (aaVar.f400a != null) {
                bnVar.a(aa.g);
                bnVar.a(aaVar.f400a);
                bnVar.b();
            }
            if (aaVar.f401b != null && aaVar.b()) {
                bnVar.a(aa.h);
                bnVar.a(new bl((byte) 12, aaVar.f401b.size()));
                for (r b2 : aaVar.f401b) {
                    b2.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (aaVar.c != null && aaVar.c()) {
                bnVar.a(aa.i);
                bnVar.a(new bl((byte) 12, aaVar.c.size()));
                for (t b3 : aaVar.c) {
                    b3.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (aaVar.d != null && aaVar.d()) {
                bnVar.a(aa.j);
                bnVar.a(new bl((byte) 12, aaVar.d.size()));
                for (t b4 : aaVar.d) {
                    b4.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: InstantMsg */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: InstantMsg */
    private static class c extends bx<aa> {
        private c() {
        }

        public void a(bn bnVar, aa aaVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(aaVar.f400a);
            BitSet bitSet = new BitSet();
            if (aaVar.b()) {
                bitSet.set(0);
            }
            if (aaVar.c()) {
                bitSet.set(1);
            }
            if (aaVar.d()) {
                bitSet.set(2);
            }
            btVar.a(bitSet, 3);
            if (aaVar.b()) {
                btVar.a(aaVar.f401b.size());
                for (r b2 : aaVar.f401b) {
                    b2.b(btVar);
                }
            }
            if (aaVar.c()) {
                btVar.a(aaVar.c.size());
                for (t b3 : aaVar.c) {
                    b3.b(btVar);
                }
            }
            if (aaVar.d()) {
                btVar.a(aaVar.d.size());
                for (t b4 : aaVar.d) {
                    b4.b(btVar);
                }
            }
        }

        public void b(bn bnVar, aa aaVar) throws ax {
            bt btVar = (bt) bnVar;
            aaVar.f400a = btVar.v();
            aaVar.a(true);
            BitSet b2 = btVar.b(3);
            if (b2.get(0)) {
                bl blVar = new bl((byte) 12, btVar.s());
                aaVar.f401b = new ArrayList(blVar.f489b);
                for (int i = 0; i < blVar.f489b; i++) {
                    r rVar = new r();
                    rVar.a(btVar);
                    aaVar.f401b.add(rVar);
                }
                aaVar.b(true);
            }
            if (b2.get(1)) {
                bl blVar2 = new bl((byte) 12, btVar.s());
                aaVar.c = new ArrayList(blVar2.f489b);
                for (int i2 = 0; i2 < blVar2.f489b; i2++) {
                    t tVar = new t();
                    tVar.a(btVar);
                    aaVar.c.add(tVar);
                }
                aaVar.c(true);
            }
            if (b2.get(2)) {
                bl blVar3 = new bl((byte) 12, btVar.s());
                aaVar.d = new ArrayList(blVar3.f489b);
                for (int i3 = 0; i3 < blVar3.f489b; i3++) {
                    t tVar2 = new t();
                    tVar2.a(btVar);
                    aaVar.d.add(tVar2);
                }
                aaVar.d(true);
            }
        }
    }
}
