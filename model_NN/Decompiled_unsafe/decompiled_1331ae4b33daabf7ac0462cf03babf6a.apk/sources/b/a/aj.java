package b.a;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* compiled from: Session */
public class aj implements au<aj, e>, Serializable, Cloneable {
    public static final Map<e, bc> h;
    /* access modifiers changed from: private */
    public static final bs i = new bs("Session");
    /* access modifiers changed from: private */
    public static final bk j = new bk("id", (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk k = new bk("start_time", (byte) 10, 2);
    /* access modifiers changed from: private */
    public static final bk l = new bk("end_time", (byte) 10, 3);
    /* access modifiers changed from: private */
    public static final bk m = new bk("duration", (byte) 10, 4);
    /* access modifiers changed from: private */
    public static final bk n = new bk("pages", (byte) 15, 5);
    /* access modifiers changed from: private */
    public static final bk o = new bk("locations", (byte) 15, 6);
    /* access modifiers changed from: private */
    public static final bk p = new bk("traffic", (byte) 12, 7);
    private static final Map<Class<? extends bu>, bv> q = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public String f434a;

    /* renamed from: b  reason: collision with root package name */
    public long f435b;
    public long c;
    public long d;
    public List<ae> e;
    public List<ac> f;
    public ak g;
    private byte r = 0;
    private e[] s = {e.PAGES, e.LOCATIONS, e.TRAFFIC};

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.aj$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        q.put(bw.class, new b());
        q.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.ID, (Object) new bc("id", (byte) 1, new bd((byte) 11)));
        enumMap.put((Object) e.START_TIME, (Object) new bc("start_time", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.END_TIME, (Object) new bc("end_time", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.DURATION, (Object) new bc("duration", (byte) 1, new bd((byte) 10)));
        enumMap.put((Object) e.PAGES, (Object) new bc("pages", (byte) 2, new be((byte) 15, new bg((byte) 12, ae.class))));
        enumMap.put((Object) e.LOCATIONS, (Object) new bc("locations", (byte) 2, new be((byte) 15, new bg((byte) 12, ac.class))));
        enumMap.put((Object) e.TRAFFIC, (Object) new bc("traffic", (byte) 2, new bg((byte) 12, ak.class)));
        h = Collections.unmodifiableMap(enumMap);
        bc.a(aj.class, h);
    }

    /* compiled from: Session */
    public enum e implements ay {
        ID(1, "id"),
        START_TIME(2, "start_time"),
        END_TIME(3, "end_time"),
        DURATION(4, "duration"),
        PAGES(5, "pages"),
        LOCATIONS(6, "locations"),
        TRAFFIC(7, "traffic");
        
        private static final Map<String, e> h = new HashMap();
        private final short i;
        private final String j;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                h.put(eVar.b(), eVar);
            }
        }

        private e(short s, String str) {
            this.i = s;
            this.j = str;
        }

        public short a() {
            return this.i;
        }

        public String b() {
            return this.j;
        }
    }

    public aj a(String str) {
        this.f434a = str;
        return this;
    }

    public void a(boolean z) {
        if (!z) {
            this.f434a = null;
        }
    }

    public aj a(long j2) {
        this.f435b = j2;
        b(true);
        return this;
    }

    public boolean a() {
        return as.a(this.r, 0);
    }

    public void b(boolean z) {
        this.r = as.a(this.r, 0, z);
    }

    public aj b(long j2) {
        this.c = j2;
        c(true);
        return this;
    }

    public boolean b() {
        return as.a(this.r, 1);
    }

    public void c(boolean z) {
        this.r = as.a(this.r, 1, z);
    }

    public aj c(long j2) {
        this.d = j2;
        d(true);
        return this;
    }

    public boolean c() {
        return as.a(this.r, 2);
    }

    public void d(boolean z) {
        this.r = as.a(this.r, 2, z);
    }

    public int d() {
        if (this.e == null) {
            return 0;
        }
        return this.e.size();
    }

    public aj a(List<ae> list) {
        this.e = list;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public void e(boolean z) {
        if (!z) {
            this.e = null;
        }
    }

    public void a(ac acVar) {
        if (this.f == null) {
            this.f = new ArrayList();
        }
        this.f.add(acVar);
    }

    public aj b(List<ac> list) {
        this.f = list;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z) {
        if (!z) {
            this.f = null;
        }
    }

    public aj a(ak akVar) {
        this.g = akVar;
        return this;
    }

    public boolean g() {
        return this.g != null;
    }

    public void g(boolean z) {
        if (!z) {
            this.g = null;
        }
    }

    public void a(bn bnVar) throws ax {
        q.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        q.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("Session(");
        sb.append("id:");
        if (this.f434a == null) {
            sb.append("null");
        } else {
            sb.append(this.f434a);
        }
        sb.append(", ");
        sb.append("start_time:");
        sb.append(this.f435b);
        sb.append(", ");
        sb.append("end_time:");
        sb.append(this.c);
        sb.append(", ");
        sb.append("duration:");
        sb.append(this.d);
        if (e()) {
            sb.append(", ");
            sb.append("pages:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
        }
        if (f()) {
            sb.append(", ");
            sb.append("locations:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
        }
        if (g()) {
            sb.append(", ");
            sb.append("traffic:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void h() throws ax {
        if (this.f434a == null) {
            throw new bo("Required field 'id' was not present! Struct: " + toString());
        } else if (this.g != null) {
            this.g.c();
        }
    }

    /* compiled from: Session */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: Session */
    private static class a extends bw<aj> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, aj ajVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    if (!ajVar.a()) {
                        throw new bo("Required field 'start_time' was not found in serialized data! Struct: " + toString());
                    } else if (!ajVar.b()) {
                        throw new bo("Required field 'end_time' was not found in serialized data! Struct: " + toString());
                    } else if (!ajVar.c()) {
                        throw new bo("Required field 'duration' was not found in serialized data! Struct: " + toString());
                    } else {
                        ajVar.h();
                        return;
                    }
                } else {
                    switch (h.c) {
                        case 1:
                            if (h.f487b != 11) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                ajVar.f434a = bnVar.v();
                                ajVar.a(true);
                                break;
                            }
                        case 2:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                ajVar.f435b = bnVar.t();
                                ajVar.b(true);
                                break;
                            }
                        case 3:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                ajVar.c = bnVar.t();
                                ajVar.c(true);
                                break;
                            }
                        case 4:
                            if (h.f487b != 10) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                ajVar.d = bnVar.t();
                                ajVar.d(true);
                                break;
                            }
                        case 5:
                            if (h.f487b != 15) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                bl l = bnVar.l();
                                ajVar.e = new ArrayList(l.f489b);
                                for (int i = 0; i < l.f489b; i++) {
                                    ae aeVar = new ae();
                                    aeVar.a(bnVar);
                                    ajVar.e.add(aeVar);
                                }
                                bnVar.m();
                                ajVar.e(true);
                                break;
                            }
                        case 6:
                            if (h.f487b != 15) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                bl l2 = bnVar.l();
                                ajVar.f = new ArrayList(l2.f489b);
                                for (int i2 = 0; i2 < l2.f489b; i2++) {
                                    ac acVar = new ac();
                                    acVar.a(bnVar);
                                    ajVar.f.add(acVar);
                                }
                                bnVar.m();
                                ajVar.f(true);
                                break;
                            }
                        case 7:
                            if (h.f487b != 12) {
                                bq.a(bnVar, h.f487b);
                                break;
                            } else {
                                ajVar.g = new ak();
                                ajVar.g.a(bnVar);
                                ajVar.g(true);
                                break;
                            }
                        default:
                            bq.a(bnVar, h.f487b);
                            break;
                    }
                    bnVar.i();
                }
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, aj ajVar) throws ax {
            ajVar.h();
            bnVar.a(aj.i);
            if (ajVar.f434a != null) {
                bnVar.a(aj.j);
                bnVar.a(ajVar.f434a);
                bnVar.b();
            }
            bnVar.a(aj.k);
            bnVar.a(ajVar.f435b);
            bnVar.b();
            bnVar.a(aj.l);
            bnVar.a(ajVar.c);
            bnVar.b();
            bnVar.a(aj.m);
            bnVar.a(ajVar.d);
            bnVar.b();
            if (ajVar.e != null && ajVar.e()) {
                bnVar.a(aj.n);
                bnVar.a(new bl((byte) 12, ajVar.e.size()));
                for (ae b2 : ajVar.e) {
                    b2.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (ajVar.f != null && ajVar.f()) {
                bnVar.a(aj.o);
                bnVar.a(new bl((byte) 12, ajVar.f.size()));
                for (ac b3 : ajVar.f) {
                    b3.b(bnVar);
                }
                bnVar.e();
                bnVar.b();
            }
            if (ajVar.g != null && ajVar.g()) {
                bnVar.a(aj.p);
                ajVar.g.b(bnVar);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: Session */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: Session */
    private static class c extends bx<aj> {
        private c() {
        }

        public void a(bn bnVar, aj ajVar) throws ax {
            bt btVar = (bt) bnVar;
            btVar.a(ajVar.f434a);
            btVar.a(ajVar.f435b);
            btVar.a(ajVar.c);
            btVar.a(ajVar.d);
            BitSet bitSet = new BitSet();
            if (ajVar.e()) {
                bitSet.set(0);
            }
            if (ajVar.f()) {
                bitSet.set(1);
            }
            if (ajVar.g()) {
                bitSet.set(2);
            }
            btVar.a(bitSet, 3);
            if (ajVar.e()) {
                btVar.a(ajVar.e.size());
                for (ae b2 : ajVar.e) {
                    b2.b(btVar);
                }
            }
            if (ajVar.f()) {
                btVar.a(ajVar.f.size());
                for (ac b3 : ajVar.f) {
                    b3.b(btVar);
                }
            }
            if (ajVar.g()) {
                ajVar.g.b(btVar);
            }
        }

        public void b(bn bnVar, aj ajVar) throws ax {
            bt btVar = (bt) bnVar;
            ajVar.f434a = btVar.v();
            ajVar.a(true);
            ajVar.f435b = btVar.t();
            ajVar.b(true);
            ajVar.c = btVar.t();
            ajVar.c(true);
            ajVar.d = btVar.t();
            ajVar.d(true);
            BitSet b2 = btVar.b(3);
            if (b2.get(0)) {
                bl blVar = new bl((byte) 12, btVar.s());
                ajVar.e = new ArrayList(blVar.f489b);
                for (int i = 0; i < blVar.f489b; i++) {
                    ae aeVar = new ae();
                    aeVar.a(btVar);
                    ajVar.e.add(aeVar);
                }
                ajVar.e(true);
            }
            if (b2.get(1)) {
                bl blVar2 = new bl((byte) 12, btVar.s());
                ajVar.f = new ArrayList(blVar2.f489b);
                for (int i2 = 0; i2 < blVar2.f489b; i2++) {
                    ac acVar = new ac();
                    acVar.a(btVar);
                    ajVar.f.add(acVar);
                }
                ajVar.f(true);
            }
            if (b2.get(2)) {
                ajVar.g = new ak();
                ajVar.g.a(btVar);
                ajVar.g(true);
            }
        }
    }
}
