package b.a;

import android.support.v4.os.EnvironmentCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/* compiled from: AbstractIdTracker */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f398a = 10;

    /* renamed from: b  reason: collision with root package name */
    private final int f399b = 20;
    private final String c;
    private List<u> d;
    private v e;

    public abstract String f();

    public a(String str) {
        this.c = str;
    }

    public boolean a() {
        return g();
    }

    public String b() {
        return this.c;
    }

    public boolean c() {
        if (this.e == null || this.e.d() <= 20) {
            return true;
        }
        return false;
    }

    private boolean g() {
        v vVar = this.e;
        String a2 = vVar == null ? null : vVar.a();
        int d2 = vVar == null ? 0 : vVar.d();
        String a3 = a(f());
        if (a3 == null || a3.equals(a2)) {
            return false;
        }
        if (vVar == null) {
            vVar = new v();
        }
        vVar.a(a3);
        vVar.a(System.currentTimeMillis());
        vVar.a(d2 + 1);
        u uVar = new u();
        uVar.a(this.c);
        uVar.c(a3);
        uVar.b(a2);
        uVar.a(vVar.b());
        if (this.d == null) {
            this.d = new ArrayList(2);
        }
        this.d.add(uVar);
        if (this.d.size() > 10) {
            this.d.remove(0);
        }
        this.e = vVar;
        return true;
    }

    public v d() {
        return this.e;
    }

    public List<u> e() {
        return this.d;
    }

    public void a(List<u> list) {
        this.d = list;
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        String trim = str.trim();
        if (trim.length() == 0 || "0".equals(trim) || EnvironmentCompat.MEDIA_UNKNOWN.equals(trim.toLowerCase(Locale.US))) {
            return null;
        }
        return trim;
    }

    public void a(w wVar) {
        this.e = wVar.a().get(this.c);
        List<u> b2 = wVar.b();
        if (b2 != null && b2.size() > 0) {
            if (this.d == null) {
                this.d = new ArrayList();
            }
            for (u next : b2) {
                if (this.c.equals(next.f576a)) {
                    this.d.add(next);
                }
            }
        }
    }
}
