package b.a;

import com.meizu.cloud.pushsdk.pushtracer.constant.Parameters;
import java.io.Serializable;
import java.util.BitSet;
import java.util.Collections;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* compiled from: DeviceInfo */
public class q implements au<q, e>, Serializable, Cloneable {
    /* access modifiers changed from: private */
    public static final bk A = new bk("os_version", (byte) 11, 8);
    /* access modifiers changed from: private */
    public static final bk B = new bk("resolution", (byte) 12, 9);
    /* access modifiers changed from: private */
    public static final bk C = new bk("is_jailbroken", (byte) 2, 10);
    /* access modifiers changed from: private */
    public static final bk D = new bk("is_pirated", (byte) 2, 11);
    /* access modifiers changed from: private */
    public static final bk E = new bk("device_board", (byte) 11, 12);
    /* access modifiers changed from: private */
    public static final bk F = new bk("device_brand", (byte) 11, 13);
    /* access modifiers changed from: private */
    public static final bk G = new bk("device_manutime", (byte) 10, 14);
    /* access modifiers changed from: private */
    public static final bk H = new bk("device_manufacturer", (byte) 11, 15);
    /* access modifiers changed from: private */
    public static final bk I = new bk("device_manuid", (byte) 11, 16);
    /* access modifiers changed from: private */
    public static final bk J = new bk("device_name", (byte) 11, 17);
    private static final Map<Class<? extends bu>, bv> K = new HashMap();
    public static final Map<e, bc> r;
    /* access modifiers changed from: private */
    public static final bs s = new bs("DeviceInfo");
    /* access modifiers changed from: private */
    public static final bk t = new bk(Parameters.DEVICE_ID, (byte) 11, 1);
    /* access modifiers changed from: private */
    public static final bk u = new bk("idmd5", (byte) 11, 2);
    /* access modifiers changed from: private */
    public static final bk v = new bk("mac_address", (byte) 11, 3);
    /* access modifiers changed from: private */
    public static final bk w = new bk("open_udid", (byte) 11, 4);
    /* access modifiers changed from: private */
    public static final bk x = new bk("model", (byte) 11, 5);
    /* access modifiers changed from: private */
    public static final bk y = new bk("cpu", (byte) 11, 6);
    /* access modifiers changed from: private */
    public static final bk z = new bk("os", (byte) 11, 7);
    private byte L = 0;
    private e[] M = {e.DEVICE_ID, e.IDMD5, e.MAC_ADDRESS, e.OPEN_UDID, e.MODEL, e.CPU, e.OS, e.OS_VERSION, e.RESOLUTION, e.IS_JAILBROKEN, e.IS_PIRATED, e.DEVICE_BOARD, e.DEVICE_BRAND, e.DEVICE_MANUTIME, e.DEVICE_MANUFACTURER, e.DEVICE_MANUID, e.DEVICE_NAME};

    /* renamed from: a  reason: collision with root package name */
    public String f562a;

    /* renamed from: b  reason: collision with root package name */
    public String f563b;
    public String c;
    public String d;
    public String e;
    public String f;
    public String g;
    public String h;
    public ag i;
    public boolean j;
    public boolean k;
    public String l;
    public String m;
    public long n;
    public String o;
    public String p;
    public String q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
     arg types: [b.a.q$e, b.a.bc]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V} */
    static {
        K.put(bw.class, new b());
        K.put(bx.class, new d());
        EnumMap enumMap = new EnumMap(e.class);
        enumMap.put((Object) e.DEVICE_ID, (Object) new bc(Parameters.DEVICE_ID, (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.IDMD5, (Object) new bc("idmd5", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.MAC_ADDRESS, (Object) new bc("mac_address", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.OPEN_UDID, (Object) new bc("open_udid", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.MODEL, (Object) new bc("model", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.CPU, (Object) new bc("cpu", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.OS, (Object) new bc("os", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.OS_VERSION, (Object) new bc("os_version", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.RESOLUTION, (Object) new bc("resolution", (byte) 2, new bg((byte) 12, ag.class)));
        enumMap.put((Object) e.IS_JAILBROKEN, (Object) new bc("is_jailbroken", (byte) 2, new bd((byte) 2)));
        enumMap.put((Object) e.IS_PIRATED, (Object) new bc("is_pirated", (byte) 2, new bd((byte) 2)));
        enumMap.put((Object) e.DEVICE_BOARD, (Object) new bc("device_board", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.DEVICE_BRAND, (Object) new bc("device_brand", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.DEVICE_MANUTIME, (Object) new bc("device_manutime", (byte) 2, new bd((byte) 10)));
        enumMap.put((Object) e.DEVICE_MANUFACTURER, (Object) new bc("device_manufacturer", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.DEVICE_MANUID, (Object) new bc("device_manuid", (byte) 2, new bd((byte) 11)));
        enumMap.put((Object) e.DEVICE_NAME, (Object) new bc("device_name", (byte) 2, new bd((byte) 11)));
        r = Collections.unmodifiableMap(enumMap);
        bc.a(q.class, r);
    }

    /* compiled from: DeviceInfo */
    public enum e implements ay {
        DEVICE_ID(1, Parameters.DEVICE_ID),
        IDMD5(2, "idmd5"),
        MAC_ADDRESS(3, "mac_address"),
        OPEN_UDID(4, "open_udid"),
        MODEL(5, "model"),
        CPU(6, "cpu"),
        OS(7, "os"),
        OS_VERSION(8, "os_version"),
        RESOLUTION(9, "resolution"),
        IS_JAILBROKEN(10, "is_jailbroken"),
        IS_PIRATED(11, "is_pirated"),
        DEVICE_BOARD(12, "device_board"),
        DEVICE_BRAND(13, "device_brand"),
        DEVICE_MANUTIME(14, "device_manutime"),
        DEVICE_MANUFACTURER(15, "device_manufacturer"),
        DEVICE_MANUID(16, "device_manuid"),
        DEVICE_NAME(17, "device_name");
        
        private static final Map<String, e> r = new HashMap();
        private final short s;
        private final String t;

        static {
            Iterator it = EnumSet.allOf(e.class).iterator();
            while (it.hasNext()) {
                e eVar = (e) it.next();
                r.put(eVar.b(), eVar);
            }
        }

        private e(short s2, String str) {
            this.s = s2;
            this.t = str;
        }

        public short a() {
            return this.s;
        }

        public String b() {
            return this.t;
        }
    }

    public q a(String str) {
        this.f562a = str;
        return this;
    }

    public boolean a() {
        return this.f562a != null;
    }

    public void a(boolean z2) {
        if (!z2) {
            this.f562a = null;
        }
    }

    public q b(String str) {
        this.f563b = str;
        return this;
    }

    public boolean b() {
        return this.f563b != null;
    }

    public void b(boolean z2) {
        if (!z2) {
            this.f563b = null;
        }
    }

    public q c(String str) {
        this.c = str;
        return this;
    }

    public boolean c() {
        return this.c != null;
    }

    public void c(boolean z2) {
        if (!z2) {
            this.c = null;
        }
    }

    public boolean d() {
        return this.d != null;
    }

    public void d(boolean z2) {
        if (!z2) {
            this.d = null;
        }
    }

    public q d(String str) {
        this.e = str;
        return this;
    }

    public boolean e() {
        return this.e != null;
    }

    public void e(boolean z2) {
        if (!z2) {
            this.e = null;
        }
    }

    public q e(String str) {
        this.f = str;
        return this;
    }

    public boolean f() {
        return this.f != null;
    }

    public void f(boolean z2) {
        if (!z2) {
            this.f = null;
        }
    }

    public q f(String str) {
        this.g = str;
        return this;
    }

    public boolean g() {
        return this.g != null;
    }

    public void g(boolean z2) {
        if (!z2) {
            this.g = null;
        }
    }

    public q g(String str) {
        this.h = str;
        return this;
    }

    public boolean h() {
        return this.h != null;
    }

    public void h(boolean z2) {
        if (!z2) {
            this.h = null;
        }
    }

    public q a(ag agVar) {
        this.i = agVar;
        return this;
    }

    public boolean i() {
        return this.i != null;
    }

    public void i(boolean z2) {
        if (!z2) {
            this.i = null;
        }
    }

    public boolean j() {
        return as.a(this.L, 0);
    }

    public void j(boolean z2) {
        this.L = as.a(this.L, 0, z2);
    }

    public boolean k() {
        return as.a(this.L, 1);
    }

    public void k(boolean z2) {
        this.L = as.a(this.L, 1, z2);
    }

    public q h(String str) {
        this.l = str;
        return this;
    }

    public boolean l() {
        return this.l != null;
    }

    public void l(boolean z2) {
        if (!z2) {
            this.l = null;
        }
    }

    public q i(String str) {
        this.m = str;
        return this;
    }

    public boolean m() {
        return this.m != null;
    }

    public void m(boolean z2) {
        if (!z2) {
            this.m = null;
        }
    }

    public q a(long j2) {
        this.n = j2;
        n(true);
        return this;
    }

    public boolean n() {
        return as.a(this.L, 2);
    }

    public void n(boolean z2) {
        this.L = as.a(this.L, 2, z2);
    }

    public q j(String str) {
        this.o = str;
        return this;
    }

    public boolean o() {
        return this.o != null;
    }

    public void o(boolean z2) {
        if (!z2) {
            this.o = null;
        }
    }

    public q k(String str) {
        this.p = str;
        return this;
    }

    public boolean p() {
        return this.p != null;
    }

    public void p(boolean z2) {
        if (!z2) {
            this.p = null;
        }
    }

    public q l(String str) {
        this.q = str;
        return this;
    }

    public boolean q() {
        return this.q != null;
    }

    public void q(boolean z2) {
        if (!z2) {
            this.q = null;
        }
    }

    public void a(bn bnVar) throws ax {
        K.get(bnVar.y()).b().b(bnVar, this);
    }

    public void b(bn bnVar) throws ax {
        K.get(bnVar.y()).b().a(bnVar, this);
    }

    public String toString() {
        boolean z2 = false;
        StringBuilder sb = new StringBuilder("DeviceInfo(");
        boolean z3 = true;
        if (a()) {
            sb.append("device_id:");
            if (this.f562a == null) {
                sb.append("null");
            } else {
                sb.append(this.f562a);
            }
            z3 = false;
        }
        if (b()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("idmd5:");
            if (this.f563b == null) {
                sb.append("null");
            } else {
                sb.append(this.f563b);
            }
            z3 = false;
        }
        if (c()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("mac_address:");
            if (this.c == null) {
                sb.append("null");
            } else {
                sb.append(this.c);
            }
            z3 = false;
        }
        if (d()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("open_udid:");
            if (this.d == null) {
                sb.append("null");
            } else {
                sb.append(this.d);
            }
            z3 = false;
        }
        if (e()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("model:");
            if (this.e == null) {
                sb.append("null");
            } else {
                sb.append(this.e);
            }
            z3 = false;
        }
        if (f()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("cpu:");
            if (this.f == null) {
                sb.append("null");
            } else {
                sb.append(this.f);
            }
            z3 = false;
        }
        if (g()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os:");
            if (this.g == null) {
                sb.append("null");
            } else {
                sb.append(this.g);
            }
            z3 = false;
        }
        if (h()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("os_version:");
            if (this.h == null) {
                sb.append("null");
            } else {
                sb.append(this.h);
            }
            z3 = false;
        }
        if (i()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("resolution:");
            if (this.i == null) {
                sb.append("null");
            } else {
                sb.append(this.i);
            }
            z3 = false;
        }
        if (j()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_jailbroken:");
            sb.append(this.j);
            z3 = false;
        }
        if (k()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("is_pirated:");
            sb.append(this.k);
            z3 = false;
        }
        if (l()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_board:");
            if (this.l == null) {
                sb.append("null");
            } else {
                sb.append(this.l);
            }
            z3 = false;
        }
        if (m()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_brand:");
            if (this.m == null) {
                sb.append("null");
            } else {
                sb.append(this.m);
            }
            z3 = false;
        }
        if (n()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manutime:");
            sb.append(this.n);
            z3 = false;
        }
        if (o()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manufacturer:");
            if (this.o == null) {
                sb.append("null");
            } else {
                sb.append(this.o);
            }
            z3 = false;
        }
        if (p()) {
            if (!z3) {
                sb.append(", ");
            }
            sb.append("device_manuid:");
            if (this.p == null) {
                sb.append("null");
            } else {
                sb.append(this.p);
            }
        } else {
            z2 = z3;
        }
        if (q()) {
            if (!z2) {
                sb.append(", ");
            }
            sb.append("device_name:");
            if (this.q == null) {
                sb.append("null");
            } else {
                sb.append(this.q);
            }
        }
        sb.append(")");
        return sb.toString();
    }

    public void r() throws ax {
        if (this.i != null) {
            this.i.c();
        }
    }

    /* compiled from: DeviceInfo */
    private static class b implements bv {
        private b() {
        }

        /* renamed from: a */
        public a b() {
            return new a();
        }
    }

    /* compiled from: DeviceInfo */
    private static class a extends bw<q> {
        private a() {
        }

        /* renamed from: a */
        public void b(bn bnVar, q qVar) throws ax {
            bnVar.f();
            while (true) {
                bk h = bnVar.h();
                if (h.f487b == 0) {
                    bnVar.g();
                    qVar.r();
                    return;
                }
                switch (h.c) {
                    case 1:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.f562a = bnVar.v();
                            qVar.a(true);
                            break;
                        }
                    case 2:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.f563b = bnVar.v();
                            qVar.b(true);
                            break;
                        }
                    case 3:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.c = bnVar.v();
                            qVar.c(true);
                            break;
                        }
                    case 4:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.d = bnVar.v();
                            qVar.d(true);
                            break;
                        }
                    case 5:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.e = bnVar.v();
                            qVar.e(true);
                            break;
                        }
                    case 6:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.f = bnVar.v();
                            qVar.f(true);
                            break;
                        }
                    case 7:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.g = bnVar.v();
                            qVar.g(true);
                            break;
                        }
                    case 8:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.h = bnVar.v();
                            qVar.h(true);
                            break;
                        }
                    case 9:
                        if (h.f487b != 12) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.i = new ag();
                            qVar.i.a(bnVar);
                            qVar.i(true);
                            break;
                        }
                    case 10:
                        if (h.f487b != 2) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.j = bnVar.p();
                            qVar.j(true);
                            break;
                        }
                    case 11:
                        if (h.f487b != 2) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.k = bnVar.p();
                            qVar.k(true);
                            break;
                        }
                    case 12:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.l = bnVar.v();
                            qVar.l(true);
                            break;
                        }
                    case 13:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.m = bnVar.v();
                            qVar.m(true);
                            break;
                        }
                    case 14:
                        if (h.f487b != 10) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.n = bnVar.t();
                            qVar.n(true);
                            break;
                        }
                    case 15:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.o = bnVar.v();
                            qVar.o(true);
                            break;
                        }
                    case 16:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.p = bnVar.v();
                            qVar.p(true);
                            break;
                        }
                    case 17:
                        if (h.f487b != 11) {
                            bq.a(bnVar, h.f487b);
                            break;
                        } else {
                            qVar.q = bnVar.v();
                            qVar.q(true);
                            break;
                        }
                    default:
                        bq.a(bnVar, h.f487b);
                        break;
                }
                bnVar.i();
            }
        }

        /* renamed from: b */
        public void a(bn bnVar, q qVar) throws ax {
            qVar.r();
            bnVar.a(q.s);
            if (qVar.f562a != null && qVar.a()) {
                bnVar.a(q.t);
                bnVar.a(qVar.f562a);
                bnVar.b();
            }
            if (qVar.f563b != null && qVar.b()) {
                bnVar.a(q.u);
                bnVar.a(qVar.f563b);
                bnVar.b();
            }
            if (qVar.c != null && qVar.c()) {
                bnVar.a(q.v);
                bnVar.a(qVar.c);
                bnVar.b();
            }
            if (qVar.d != null && qVar.d()) {
                bnVar.a(q.w);
                bnVar.a(qVar.d);
                bnVar.b();
            }
            if (qVar.e != null && qVar.e()) {
                bnVar.a(q.x);
                bnVar.a(qVar.e);
                bnVar.b();
            }
            if (qVar.f != null && qVar.f()) {
                bnVar.a(q.y);
                bnVar.a(qVar.f);
                bnVar.b();
            }
            if (qVar.g != null && qVar.g()) {
                bnVar.a(q.z);
                bnVar.a(qVar.g);
                bnVar.b();
            }
            if (qVar.h != null && qVar.h()) {
                bnVar.a(q.A);
                bnVar.a(qVar.h);
                bnVar.b();
            }
            if (qVar.i != null && qVar.i()) {
                bnVar.a(q.B);
                qVar.i.b(bnVar);
                bnVar.b();
            }
            if (qVar.j()) {
                bnVar.a(q.C);
                bnVar.a(qVar.j);
                bnVar.b();
            }
            if (qVar.k()) {
                bnVar.a(q.D);
                bnVar.a(qVar.k);
                bnVar.b();
            }
            if (qVar.l != null && qVar.l()) {
                bnVar.a(q.E);
                bnVar.a(qVar.l);
                bnVar.b();
            }
            if (qVar.m != null && qVar.m()) {
                bnVar.a(q.F);
                bnVar.a(qVar.m);
                bnVar.b();
            }
            if (qVar.n()) {
                bnVar.a(q.G);
                bnVar.a(qVar.n);
                bnVar.b();
            }
            if (qVar.o != null && qVar.o()) {
                bnVar.a(q.H);
                bnVar.a(qVar.o);
                bnVar.b();
            }
            if (qVar.p != null && qVar.p()) {
                bnVar.a(q.I);
                bnVar.a(qVar.p);
                bnVar.b();
            }
            if (qVar.q != null && qVar.q()) {
                bnVar.a(q.J);
                bnVar.a(qVar.q);
                bnVar.b();
            }
            bnVar.c();
            bnVar.a();
        }
    }

    /* compiled from: DeviceInfo */
    private static class d implements bv {
        private d() {
        }

        /* renamed from: a */
        public c b() {
            return new c();
        }
    }

    /* compiled from: DeviceInfo */
    private static class c extends bx<q> {
        private c() {
        }

        public void a(bn bnVar, q qVar) throws ax {
            bt btVar = (bt) bnVar;
            BitSet bitSet = new BitSet();
            if (qVar.a()) {
                bitSet.set(0);
            }
            if (qVar.b()) {
                bitSet.set(1);
            }
            if (qVar.c()) {
                bitSet.set(2);
            }
            if (qVar.d()) {
                bitSet.set(3);
            }
            if (qVar.e()) {
                bitSet.set(4);
            }
            if (qVar.f()) {
                bitSet.set(5);
            }
            if (qVar.g()) {
                bitSet.set(6);
            }
            if (qVar.h()) {
                bitSet.set(7);
            }
            if (qVar.i()) {
                bitSet.set(8);
            }
            if (qVar.j()) {
                bitSet.set(9);
            }
            if (qVar.k()) {
                bitSet.set(10);
            }
            if (qVar.l()) {
                bitSet.set(11);
            }
            if (qVar.m()) {
                bitSet.set(12);
            }
            if (qVar.n()) {
                bitSet.set(13);
            }
            if (qVar.o()) {
                bitSet.set(14);
            }
            if (qVar.p()) {
                bitSet.set(15);
            }
            if (qVar.q()) {
                bitSet.set(16);
            }
            btVar.a(bitSet, 17);
            if (qVar.a()) {
                btVar.a(qVar.f562a);
            }
            if (qVar.b()) {
                btVar.a(qVar.f563b);
            }
            if (qVar.c()) {
                btVar.a(qVar.c);
            }
            if (qVar.d()) {
                btVar.a(qVar.d);
            }
            if (qVar.e()) {
                btVar.a(qVar.e);
            }
            if (qVar.f()) {
                btVar.a(qVar.f);
            }
            if (qVar.g()) {
                btVar.a(qVar.g);
            }
            if (qVar.h()) {
                btVar.a(qVar.h);
            }
            if (qVar.i()) {
                qVar.i.b(btVar);
            }
            if (qVar.j()) {
                btVar.a(qVar.j);
            }
            if (qVar.k()) {
                btVar.a(qVar.k);
            }
            if (qVar.l()) {
                btVar.a(qVar.l);
            }
            if (qVar.m()) {
                btVar.a(qVar.m);
            }
            if (qVar.n()) {
                btVar.a(qVar.n);
            }
            if (qVar.o()) {
                btVar.a(qVar.o);
            }
            if (qVar.p()) {
                btVar.a(qVar.p);
            }
            if (qVar.q()) {
                btVar.a(qVar.q);
            }
        }

        public void b(bn bnVar, q qVar) throws ax {
            bt btVar = (bt) bnVar;
            BitSet b2 = btVar.b(17);
            if (b2.get(0)) {
                qVar.f562a = btVar.v();
                qVar.a(true);
            }
            if (b2.get(1)) {
                qVar.f563b = btVar.v();
                qVar.b(true);
            }
            if (b2.get(2)) {
                qVar.c = btVar.v();
                qVar.c(true);
            }
            if (b2.get(3)) {
                qVar.d = btVar.v();
                qVar.d(true);
            }
            if (b2.get(4)) {
                qVar.e = btVar.v();
                qVar.e(true);
            }
            if (b2.get(5)) {
                qVar.f = btVar.v();
                qVar.f(true);
            }
            if (b2.get(6)) {
                qVar.g = btVar.v();
                qVar.g(true);
            }
            if (b2.get(7)) {
                qVar.h = btVar.v();
                qVar.h(true);
            }
            if (b2.get(8)) {
                qVar.i = new ag();
                qVar.i.a(btVar);
                qVar.i(true);
            }
            if (b2.get(9)) {
                qVar.j = btVar.p();
                qVar.j(true);
            }
            if (b2.get(10)) {
                qVar.k = btVar.p();
                qVar.k(true);
            }
            if (b2.get(11)) {
                qVar.l = btVar.v();
                qVar.l(true);
            }
            if (b2.get(12)) {
                qVar.m = btVar.v();
                qVar.m(true);
            }
            if (b2.get(13)) {
                qVar.n = btVar.t();
                qVar.n(true);
            }
            if (b2.get(14)) {
                qVar.o = btVar.v();
                qVar.o(true);
            }
            if (b2.get(15)) {
                qVar.p = btVar.v();
                qVar.p(true);
            }
            if (b2.get(16)) {
                qVar.q = btVar.v();
                qVar.q(true);
            }
        }
    }
}
