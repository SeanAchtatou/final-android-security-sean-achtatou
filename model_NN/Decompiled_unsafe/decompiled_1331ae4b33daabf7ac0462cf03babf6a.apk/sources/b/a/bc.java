package b.a;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/* compiled from: FieldMetaData */
public class bc implements Serializable {
    private static Map<Class<? extends au>, Map<? extends ay, bc>> d = new HashMap();

    /* renamed from: a  reason: collision with root package name */
    public final String f468a;

    /* renamed from: b  reason: collision with root package name */
    public final byte f469b;
    public final bd c;

    public bc(String str, byte b2, bd bdVar) {
        this.f468a = str;
        this.f469b = b2;
        this.c = bdVar;
    }

    public static void a(Class<? extends au> cls, Map<? extends ay, bc> map) {
        d.put(cls, map);
    }
}
