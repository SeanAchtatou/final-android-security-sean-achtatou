package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.menu.l;
import android.support.v7.view.menu.m;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

/* compiled from: BaseMenuPresenter */
public abstract class a implements l {

    /* renamed from: a  reason: collision with root package name */
    protected Context f159a;

    /* renamed from: b  reason: collision with root package name */
    protected Context f160b;
    protected MenuBuilder c;
    protected LayoutInflater d;
    protected LayoutInflater e;
    protected m f;
    private l.a g;
    private int h;
    private int i;
    private int j;

    public abstract void a(MenuItemImpl menuItemImpl, m.a aVar);

    public a(Context context, int i2, int i3) {
        this.f159a = context;
        this.d = LayoutInflater.from(context);
        this.h = i2;
        this.i = i3;
    }

    public void a(Context context, MenuBuilder menuBuilder) {
        this.f160b = context;
        this.e = LayoutInflater.from(this.f160b);
        this.c = menuBuilder;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public m a(ViewGroup viewGroup) {
        if (this.f == null) {
            this.f = (m) this.d.inflate(this.h, viewGroup, false);
            this.f.a(this.c);
            a(true);
        }
        return this.f;
    }

    public void a(boolean z) {
        int i2;
        int i3;
        ViewGroup viewGroup = (ViewGroup) this.f;
        if (viewGroup != null) {
            if (this.c != null) {
                this.c.j();
                ArrayList<MenuItemImpl> i4 = this.c.i();
                int size = i4.size();
                int i5 = 0;
                i2 = 0;
                while (i5 < size) {
                    MenuItemImpl menuItemImpl = i4.get(i5);
                    if (a(i2, menuItemImpl)) {
                        View childAt = viewGroup.getChildAt(i2);
                        MenuItemImpl itemData = childAt instanceof m.a ? ((m.a) childAt).getItemData() : null;
                        View a2 = a(menuItemImpl, childAt, viewGroup);
                        if (menuItemImpl != itemData) {
                            a2.setPressed(false);
                            ViewCompat.jumpDrawablesToCurrentState(a2);
                        }
                        if (a2 != childAt) {
                            a(a2, i2);
                        }
                        i3 = i2 + 1;
                    } else {
                        i3 = i2;
                    }
                    i5++;
                    i2 = i3;
                }
            } else {
                i2 = 0;
            }
            while (i2 < viewGroup.getChildCount()) {
                if (!a(viewGroup, i2)) {
                    i2++;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(View view, int i2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        if (viewGroup != null) {
            viewGroup.removeView(view);
        }
        ((ViewGroup) this.f).addView(view, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(ViewGroup viewGroup, int i2) {
        viewGroup.removeViewAt(i2);
        return true;
    }

    public void a(l.a aVar) {
        this.g = aVar;
    }

    public l.a a() {
        return this.g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public m.a b(ViewGroup viewGroup) {
        return (m.a) this.d.inflate(this.i, viewGroup, false);
    }

    public View a(MenuItemImpl menuItemImpl, View view, ViewGroup viewGroup) {
        m.a b2;
        if (view instanceof m.a) {
            b2 = (m.a) view;
        } else {
            b2 = b(viewGroup);
        }
        a(menuItemImpl, b2);
        return (View) b2;
    }

    public boolean a(int i2, MenuItemImpl menuItemImpl) {
        return true;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (this.g != null) {
            this.g.a(menuBuilder, z);
        }
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (this.g != null) {
            return this.g.a(subMenuBuilder);
        }
        return false;
    }

    public boolean b() {
        return false;
    }

    public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
        return false;
    }

    public void a(int i2) {
        this.j = i2;
    }
}
