package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.a.a;
import android.support.v7.view.menu.l;
import android.support.v7.widget.t;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

/* compiled from: StandardMenuPopup */
final class q extends j implements l, View.OnKeyListener, AdapterView.OnItemClickListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final t f188a;

    /* renamed from: b  reason: collision with root package name */
    View f189b;
    private final Context c;
    private final MenuBuilder d;
    private final f e;
    private final boolean f;
    private final int g;
    private final int h;
    private final int i;
    private final ViewTreeObserver.OnGlobalLayoutListener j = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (q.this.d() && !q.this.f188a.g()) {
                View view = q.this.f189b;
                if (view == null || !view.isShown()) {
                    q.this.c();
                } else {
                    q.this.f188a.a();
                }
            }
        }
    };
    private PopupWindow.OnDismissListener k;
    private View l;
    private l.a m;
    private ViewTreeObserver n;
    private boolean o;
    private boolean p;
    private int q;
    private int r = 0;
    private boolean s;

    public q(Context context, MenuBuilder menuBuilder, View view, int i2, int i3, boolean z) {
        this.c = context;
        this.d = menuBuilder;
        this.f = z;
        this.e = new f(menuBuilder, LayoutInflater.from(context), this.f);
        this.h = i2;
        this.i = i3;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(a.d.abc_config_prefDialogWidth));
        this.l = view;
        this.f188a = new t(this.c, null, this.h, this.i);
        menuBuilder.a(this, context);
    }

    public void b(boolean z) {
        this.e.a(z);
    }

    public void a(int i2) {
        this.r = i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private boolean h() {
        if (d()) {
            return true;
        }
        if (this.o || this.l == null) {
            return false;
        }
        this.f189b = this.l;
        this.f188a.a((PopupWindow.OnDismissListener) this);
        this.f188a.a((AdapterView.OnItemClickListener) this);
        this.f188a.a(true);
        View view = this.f189b;
        boolean z = this.n == null;
        this.n = view.getViewTreeObserver();
        if (z) {
            this.n.addOnGlobalLayoutListener(this.j);
        }
        this.f188a.b(view);
        this.f188a.e(this.r);
        if (!this.p) {
            this.q = a(this.e, null, this.c, this.g);
            this.p = true;
        }
        this.f188a.g(this.q);
        this.f188a.h(2);
        this.f188a.a(g());
        this.f188a.a();
        ListView e2 = this.f188a.e();
        e2.setOnKeyListener(this);
        if (this.s && this.d.m() != null) {
            FrameLayout frameLayout = (FrameLayout) LayoutInflater.from(this.c).inflate(a.h.abc_popup_menu_header_item_layout, (ViewGroup) e2, false);
            TextView textView = (TextView) frameLayout.findViewById(16908310);
            if (textView != null) {
                textView.setText(this.d.m());
            }
            frameLayout.setEnabled(false);
            e2.addHeaderView(frameLayout, null, false);
        }
        this.f188a.a((ListAdapter) this.e);
        this.f188a.a();
        return true;
    }

    public void a() {
        if (!h()) {
            throw new IllegalStateException("StandardMenuPopup cannot be used without an anchor");
        }
    }

    public void c() {
        if (d()) {
            this.f188a.c();
        }
    }

    public void a(MenuBuilder menuBuilder) {
    }

    public boolean d() {
        return !this.o && this.f188a.d();
    }

    public void onDismiss() {
        this.o = true;
        this.d.close();
        if (this.n != null) {
            if (!this.n.isAlive()) {
                this.n = this.f189b.getViewTreeObserver();
            }
            this.n.removeGlobalOnLayoutListener(this.j);
            this.n = null;
        }
        if (this.k != null) {
            this.k.onDismiss();
        }
    }

    public void a(boolean z) {
        this.p = false;
        if (this.e != null) {
            this.e.notifyDataSetChanged();
        }
    }

    public void a(l.a aVar) {
        this.m = aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        if (subMenuBuilder.hasVisibleItems()) {
            k kVar = new k(this.c, subMenuBuilder, this.f189b, this.f, this.h, this.i);
            kVar.a(this.m);
            kVar.a(j.b(subMenuBuilder));
            kVar.a(this.k);
            this.k = null;
            this.d.a(false);
            if (kVar.a(this.f188a.j(), this.f188a.k())) {
                if (this.m != null) {
                    this.m.a(subMenuBuilder);
                }
                return true;
            }
        }
        return false;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.d) {
            c();
            if (this.m != null) {
                this.m.a(menuBuilder, z);
            }
        }
    }

    public boolean b() {
        return false;
    }

    public void a(View view) {
        this.l = view;
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        c();
        return true;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.k = onDismissListener;
    }

    public ListView e() {
        return this.f188a.e();
    }

    public void b(int i2) {
        this.f188a.c(i2);
    }

    public void c(int i2) {
        this.f188a.d(i2);
    }

    public void c(boolean z) {
        this.s = z;
    }
}
