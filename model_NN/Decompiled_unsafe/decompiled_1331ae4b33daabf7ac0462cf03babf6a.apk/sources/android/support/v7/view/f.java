package android.support.v7.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.util.SimpleArrayMap;
import android.support.v7.view.b;
import android.support.v7.view.menu.n;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.util.ArrayList;

@TargetApi(11)
/* compiled from: SupportActionModeWrapper */
public class f extends ActionMode {

    /* renamed from: a  reason: collision with root package name */
    final Context f131a;

    /* renamed from: b  reason: collision with root package name */
    final b f132b;

    public f(Context context, b bVar) {
        this.f131a = context;
        this.f132b = bVar;
    }

    public Object getTag() {
        return this.f132b.j();
    }

    public void setTag(Object obj) {
        this.f132b.a(obj);
    }

    public void setTitle(CharSequence charSequence) {
        this.f132b.b(charSequence);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.f132b.a(charSequence);
    }

    public void invalidate() {
        this.f132b.d();
    }

    public void finish() {
        this.f132b.c();
    }

    public Menu getMenu() {
        return n.a(this.f131a, (SupportMenu) this.f132b.b());
    }

    public CharSequence getTitle() {
        return this.f132b.f();
    }

    public void setTitle(int i) {
        this.f132b.a(i);
    }

    public CharSequence getSubtitle() {
        return this.f132b.g();
    }

    public void setSubtitle(int i) {
        this.f132b.b(i);
    }

    public View getCustomView() {
        return this.f132b.i();
    }

    public void setCustomView(View view) {
        this.f132b.a(view);
    }

    public MenuInflater getMenuInflater() {
        return this.f132b.a();
    }

    public boolean getTitleOptionalHint() {
        return this.f132b.k();
    }

    public void setTitleOptionalHint(boolean z) {
        this.f132b.a(z);
    }

    public boolean isTitleOptional() {
        return this.f132b.h();
    }

    /* compiled from: SupportActionModeWrapper */
    public static class a implements b.a {

        /* renamed from: a  reason: collision with root package name */
        final ActionMode.Callback f133a;

        /* renamed from: b  reason: collision with root package name */
        final Context f134b;
        final ArrayList<f> c = new ArrayList<>();
        final SimpleArrayMap<Menu, Menu> d = new SimpleArrayMap<>();

        public a(Context context, ActionMode.Callback callback) {
            this.f134b = context;
            this.f133a = callback;
        }

        public boolean a(b bVar, Menu menu) {
            return this.f133a.onCreateActionMode(b(bVar), a(menu));
        }

        public boolean b(b bVar, Menu menu) {
            return this.f133a.onPrepareActionMode(b(bVar), a(menu));
        }

        public boolean a(b bVar, MenuItem menuItem) {
            return this.f133a.onActionItemClicked(b(bVar), n.a(this.f134b, (SupportMenuItem) menuItem));
        }

        public void a(b bVar) {
            this.f133a.onDestroyActionMode(b(bVar));
        }

        private Menu a(Menu menu) {
            Menu menu2 = this.d.get(menu);
            if (menu2 != null) {
                return menu2;
            }
            Menu a2 = n.a(this.f134b, (SupportMenu) menu);
            this.d.put(menu, a2);
            return a2;
        }

        public ActionMode b(b bVar) {
            int size = this.c.size();
            for (int i = 0; i < size; i++) {
                f fVar = this.c.get(i);
                if (fVar != null && fVar.f132b == bVar) {
                    return fVar;
                }
            }
            f fVar2 = new f(this.f134b, bVar);
            this.c.add(fVar2);
            return fVar2;
        }
    }
}
