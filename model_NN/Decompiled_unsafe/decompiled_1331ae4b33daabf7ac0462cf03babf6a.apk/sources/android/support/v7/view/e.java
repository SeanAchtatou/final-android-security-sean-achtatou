package android.support.v7.view;

import android.content.Context;
import android.support.v7.view.b;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContextView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.lang.ref.WeakReference;

/* compiled from: StandaloneActionMode */
public class e extends b implements MenuBuilder.a {

    /* renamed from: a  reason: collision with root package name */
    private Context f129a;

    /* renamed from: b  reason: collision with root package name */
    private ActionBarContextView f130b;
    private b.a c;
    private WeakReference<View> d;
    private boolean e;
    private boolean f;
    private MenuBuilder g;

    public e(Context context, ActionBarContextView actionBarContextView, b.a aVar, boolean z) {
        this.f129a = context;
        this.f130b = actionBarContextView;
        this.c = aVar;
        this.g = new MenuBuilder(actionBarContextView.getContext()).a(1);
        this.g.a(this);
        this.f = z;
    }

    public void b(CharSequence charSequence) {
        this.f130b.setTitle(charSequence);
    }

    public void a(CharSequence charSequence) {
        this.f130b.setSubtitle(charSequence);
    }

    public void a(int i) {
        b(this.f129a.getString(i));
    }

    public void b(int i) {
        a((CharSequence) this.f129a.getString(i));
    }

    public void a(boolean z) {
        super.a(z);
        this.f130b.setTitleOptional(z);
    }

    public boolean h() {
        return this.f130b.d();
    }

    public void a(View view) {
        this.f130b.setCustomView(view);
        this.d = view != null ? new WeakReference<>(view) : null;
    }

    public void d() {
        this.c.b(this, this.g);
    }

    public void c() {
        if (!this.e) {
            this.e = true;
            this.f130b.sendAccessibilityEvent(32);
            this.c.a(this);
        }
    }

    public Menu b() {
        return this.g;
    }

    public CharSequence f() {
        return this.f130b.getTitle();
    }

    public CharSequence g() {
        return this.f130b.getSubtitle();
    }

    public View i() {
        if (this.d != null) {
            return this.d.get();
        }
        return null;
    }

    public MenuInflater a() {
        return new g(this.f130b.getContext());
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.c.a(this, menuItem);
    }

    public void a(MenuBuilder menuBuilder) {
        d();
        this.f130b.a();
    }
}
