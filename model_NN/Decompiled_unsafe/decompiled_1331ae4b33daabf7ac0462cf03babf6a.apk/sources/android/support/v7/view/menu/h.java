package android.support.v7.view.menu;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import java.lang.reflect.Method;

@TargetApi(14)
/* compiled from: MenuItemWrapperICS */
public class h extends b<SupportMenuItem> implements MenuItem {
    private Method c;

    h(Context context, SupportMenuItem supportMenuItem) {
        super(context, supportMenuItem);
    }

    public int getItemId() {
        return ((SupportMenuItem) this.f162b).getItemId();
    }

    public int getGroupId() {
        return ((SupportMenuItem) this.f162b).getGroupId();
    }

    public int getOrder() {
        return ((SupportMenuItem) this.f162b).getOrder();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        ((SupportMenuItem) this.f162b).setTitle(charSequence);
        return this;
    }

    public MenuItem setTitle(int i) {
        ((SupportMenuItem) this.f162b).setTitle(i);
        return this;
    }

    public CharSequence getTitle() {
        return ((SupportMenuItem) this.f162b).getTitle();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        ((SupportMenuItem) this.f162b).setTitleCondensed(charSequence);
        return this;
    }

    public CharSequence getTitleCondensed() {
        return ((SupportMenuItem) this.f162b).getTitleCondensed();
    }

    public MenuItem setIcon(Drawable drawable) {
        ((SupportMenuItem) this.f162b).setIcon(drawable);
        return this;
    }

    public MenuItem setIcon(int i) {
        ((SupportMenuItem) this.f162b).setIcon(i);
        return this;
    }

    public Drawable getIcon() {
        return ((SupportMenuItem) this.f162b).getIcon();
    }

    public MenuItem setIntent(Intent intent) {
        ((SupportMenuItem) this.f162b).setIntent(intent);
        return this;
    }

    public Intent getIntent() {
        return ((SupportMenuItem) this.f162b).getIntent();
    }

    public MenuItem setShortcut(char c2, char c3) {
        ((SupportMenuItem) this.f162b).setShortcut(c2, c3);
        return this;
    }

    public MenuItem setNumericShortcut(char c2) {
        ((SupportMenuItem) this.f162b).setNumericShortcut(c2);
        return this;
    }

    public char getNumericShortcut() {
        return ((SupportMenuItem) this.f162b).getNumericShortcut();
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        ((SupportMenuItem) this.f162b).setAlphabeticShortcut(c2);
        return this;
    }

    public char getAlphabeticShortcut() {
        return ((SupportMenuItem) this.f162b).getAlphabeticShortcut();
    }

    public MenuItem setCheckable(boolean z) {
        ((SupportMenuItem) this.f162b).setCheckable(z);
        return this;
    }

    public boolean isCheckable() {
        return ((SupportMenuItem) this.f162b).isCheckable();
    }

    public MenuItem setChecked(boolean z) {
        ((SupportMenuItem) this.f162b).setChecked(z);
        return this;
    }

    public boolean isChecked() {
        return ((SupportMenuItem) this.f162b).isChecked();
    }

    public MenuItem setVisible(boolean z) {
        return ((SupportMenuItem) this.f162b).setVisible(z);
    }

    public boolean isVisible() {
        return ((SupportMenuItem) this.f162b).isVisible();
    }

    public MenuItem setEnabled(boolean z) {
        ((SupportMenuItem) this.f162b).setEnabled(z);
        return this;
    }

    public boolean isEnabled() {
        return ((SupportMenuItem) this.f162b).isEnabled();
    }

    public boolean hasSubMenu() {
        return ((SupportMenuItem) this.f162b).hasSubMenu();
    }

    public SubMenu getSubMenu() {
        return a(((SupportMenuItem) this.f162b).getSubMenu());
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        ((SupportMenuItem) this.f162b).setOnMenuItemClickListener(onMenuItemClickListener != null ? new d(onMenuItemClickListener) : null);
        return this;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return ((SupportMenuItem) this.f162b).getMenuInfo();
    }

    public void setShowAsAction(int i) {
        ((SupportMenuItem) this.f162b).setShowAsAction(i);
    }

    public MenuItem setShowAsActionFlags(int i) {
        ((SupportMenuItem) this.f162b).setShowAsActionFlags(i);
        return this;
    }

    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new b(view);
        }
        ((SupportMenuItem) this.f162b).setActionView(view);
        return this;
    }

    public MenuItem setActionView(int i) {
        ((SupportMenuItem) this.f162b).setActionView(i);
        View actionView = ((SupportMenuItem) this.f162b).getActionView();
        if (actionView instanceof CollapsibleActionView) {
            ((SupportMenuItem) this.f162b).setActionView(new b(actionView));
        }
        return this;
    }

    public View getActionView() {
        View actionView = ((SupportMenuItem) this.f162b).getActionView();
        if (actionView instanceof b) {
            return ((b) actionView).c();
        }
        return actionView;
    }

    public MenuItem setActionProvider(ActionProvider actionProvider) {
        ((SupportMenuItem) this.f162b).setSupportActionProvider(actionProvider != null ? a(actionProvider) : null);
        return this;
    }

    public ActionProvider getActionProvider() {
        android.support.v4.view.ActionProvider supportActionProvider = ((SupportMenuItem) this.f162b).getSupportActionProvider();
        if (supportActionProvider instanceof a) {
            return ((a) supportActionProvider).f179a;
        }
        return null;
    }

    public boolean expandActionView() {
        return ((SupportMenuItem) this.f162b).expandActionView();
    }

    public boolean collapseActionView() {
        return ((SupportMenuItem) this.f162b).collapseActionView();
    }

    public boolean isActionViewExpanded() {
        return ((SupportMenuItem) this.f162b).isActionViewExpanded();
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        ((SupportMenuItem) this.f162b).setSupportOnActionExpandListener(onActionExpandListener != null ? new c(onActionExpandListener) : null);
        return this;
    }

    public void a(boolean z) {
        try {
            if (this.c == null) {
                this.c = ((SupportMenuItem) this.f162b).getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
            }
            this.c.invoke(this.f162b, Boolean.valueOf(z));
        } catch (Exception e) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e);
        }
    }

    /* access modifiers changed from: package-private */
    public a a(ActionProvider actionProvider) {
        return new a(this.f161a, actionProvider);
    }

    /* compiled from: MenuItemWrapperICS */
    private class d extends c<MenuItem.OnMenuItemClickListener> implements MenuItem.OnMenuItemClickListener {
        d(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            super(onMenuItemClickListener);
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ((MenuItem.OnMenuItemClickListener) this.f162b).onMenuItemClick(h.this.a(menuItem));
        }
    }

    /* compiled from: MenuItemWrapperICS */
    private class c extends c<MenuItem.OnActionExpandListener> implements MenuItemCompat.OnActionExpandListener {
        c(MenuItem.OnActionExpandListener onActionExpandListener) {
            super(onActionExpandListener);
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f162b).onMenuItemActionExpand(h.this.a(menuItem));
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return ((MenuItem.OnActionExpandListener) this.f162b).onMenuItemActionCollapse(h.this.a(menuItem));
        }
    }

    /* compiled from: MenuItemWrapperICS */
    class a extends android.support.v4.view.ActionProvider {

        /* renamed from: a  reason: collision with root package name */
        final ActionProvider f179a;

        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.f179a = actionProvider;
        }

        public View onCreateActionView() {
            return this.f179a.onCreateActionView();
        }

        public boolean onPerformDefaultAction() {
            return this.f179a.onPerformDefaultAction();
        }

        public boolean hasSubMenu() {
            return this.f179a.hasSubMenu();
        }

        public void onPrepareSubMenu(SubMenu subMenu) {
            this.f179a.onPrepareSubMenu(h.this.a(subMenu));
        }
    }

    /* compiled from: MenuItemWrapperICS */
    static class b extends FrameLayout implements android.support.v7.view.c {

        /* renamed from: a  reason: collision with root package name */
        final CollapsibleActionView f181a;

        b(View view) {
            super(view.getContext());
            this.f181a = (CollapsibleActionView) view;
            addView(view);
        }

        public void a() {
            this.f181a.onActionViewExpanded();
        }

        public void b() {
            this.f181a.onActionViewCollapsed();
        }

        /* access modifiers changed from: package-private */
        public View c() {
            return (View) this.f181a;
        }
    }
}
