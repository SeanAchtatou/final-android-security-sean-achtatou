package android.support.v7.view.menu;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.b.a.b;
import android.support.v7.view.menu.m;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public final class MenuItemImpl implements SupportMenuItem {
    private static String w;
    private static String x;
    private static String y;
    private static String z;

    /* renamed from: a  reason: collision with root package name */
    MenuBuilder f156a;

    /* renamed from: b  reason: collision with root package name */
    private final int f157b;
    private final int c;
    private final int d;
    private final int e;
    private CharSequence f;
    private CharSequence g;
    private Intent h;
    private char i;
    private char j;
    private Drawable k;
    private int l = 0;
    private SubMenuBuilder m;
    private Runnable n;
    private MenuItem.OnMenuItemClickListener o;
    private int p = 16;
    private int q = 0;
    private View r;
    private ActionProvider s;
    private MenuItemCompat.OnActionExpandListener t;
    private boolean u = false;
    private ContextMenu.ContextMenuInfo v;

    MenuItemImpl(MenuBuilder menuBuilder, int i2, int i3, int i4, int i5, CharSequence charSequence, int i6) {
        this.f156a = menuBuilder;
        this.f157b = i3;
        this.c = i2;
        this.d = i4;
        this.e = i5;
        this.f = charSequence;
        this.q = i6;
    }

    public boolean a() {
        if ((this.o != null && this.o.onMenuItemClick(this)) || this.f156a.a(this.f156a.p(), this)) {
            return true;
        }
        if (this.n != null) {
            this.n.run();
            return true;
        }
        if (this.h != null) {
            try {
                this.f156a.e().startActivity(this.h);
                return true;
            } catch (ActivityNotFoundException e2) {
                Log.e("MenuItemImpl", "Can't find activity to handle intent; ignoring", e2);
            }
        }
        if (this.s == null || !this.s.onPerformDefaultAction()) {
            return false;
        }
        return true;
    }

    public boolean isEnabled() {
        return (this.p & 16) != 0;
    }

    public MenuItem setEnabled(boolean z2) {
        if (z2) {
            this.p |= 16;
        } else {
            this.p &= -17;
        }
        this.f156a.b(false);
        return this;
    }

    public int getGroupId() {
        return this.c;
    }

    @ViewDebug.CapturedViewProperty
    public int getItemId() {
        return this.f157b;
    }

    public int getOrder() {
        return this.d;
    }

    public int b() {
        return this.e;
    }

    public Intent getIntent() {
        return this.h;
    }

    public MenuItem setIntent(Intent intent) {
        this.h = intent;
        return this;
    }

    public char getAlphabeticShortcut() {
        return this.j;
    }

    public MenuItem setAlphabeticShortcut(char c2) {
        if (this.j != c2) {
            this.j = Character.toLowerCase(c2);
            this.f156a.b(false);
        }
        return this;
    }

    public char getNumericShortcut() {
        return this.i;
    }

    public MenuItem setNumericShortcut(char c2) {
        if (this.i != c2) {
            this.i = c2;
            this.f156a.b(false);
        }
        return this;
    }

    public MenuItem setShortcut(char c2, char c3) {
        this.i = c2;
        this.j = Character.toLowerCase(c3);
        this.f156a.b(false);
        return this;
    }

    /* access modifiers changed from: package-private */
    public char c() {
        return this.f156a.b() ? this.j : this.i;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        char c2 = c();
        if (c2 == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder(w);
        switch (c2) {
            case 8:
                sb.append(y);
                break;
            case 10:
                sb.append(x);
                break;
            case ' ':
                sb.append(z);
                break;
            default:
                sb.append(c2);
                break;
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.f156a.c() && c() != 0;
    }

    public SubMenu getSubMenu() {
        return this.m;
    }

    public boolean hasSubMenu() {
        return this.m != null;
    }

    public void a(SubMenuBuilder subMenuBuilder) {
        this.m = subMenuBuilder;
        subMenuBuilder.setHeaderTitle(getTitle());
    }

    @ViewDebug.CapturedViewProperty
    public CharSequence getTitle() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public CharSequence a(m.a aVar) {
        if (aVar == null || !aVar.a()) {
            return getTitle();
        }
        return getTitleCondensed();
    }

    public MenuItem setTitle(CharSequence charSequence) {
        this.f = charSequence;
        this.f156a.b(false);
        if (this.m != null) {
            this.m.setHeaderTitle(charSequence);
        }
        return this;
    }

    public MenuItem setTitle(int i2) {
        return setTitle(this.f156a.e().getString(i2));
    }

    public CharSequence getTitleCondensed() {
        CharSequence charSequence = this.g != null ? this.g : this.f;
        if (Build.VERSION.SDK_INT >= 18 || charSequence == null || (charSequence instanceof String)) {
            return charSequence;
        }
        return charSequence.toString();
    }

    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.g = charSequence;
        if (charSequence == null) {
            CharSequence charSequence2 = this.f;
        }
        this.f156a.b(false);
        return this;
    }

    public Drawable getIcon() {
        if (this.k != null) {
            return this.k;
        }
        if (this.l == 0) {
            return null;
        }
        Drawable b2 = b.b(this.f156a.e(), this.l);
        this.l = 0;
        this.k = b2;
        return b2;
    }

    public MenuItem setIcon(Drawable drawable) {
        this.l = 0;
        this.k = drawable;
        this.f156a.b(false);
        return this;
    }

    public MenuItem setIcon(int i2) {
        this.k = null;
        this.l = i2;
        this.f156a.b(false);
        return this;
    }

    public boolean isCheckable() {
        return (this.p & 1) == 1;
    }

    public MenuItem setCheckable(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 1 : 0) | (this.p & -2);
        if (i2 != this.p) {
            this.f156a.b(false);
        }
        return this;
    }

    public void a(boolean z2) {
        this.p = (z2 ? 4 : 0) | (this.p & -5);
    }

    public boolean f() {
        return (this.p & 4) != 0;
    }

    public boolean isChecked() {
        return (this.p & 2) == 2;
    }

    public MenuItem setChecked(boolean z2) {
        if ((this.p & 4) != 0) {
            this.f156a.a((MenuItem) this);
        } else {
            b(z2);
        }
        return this;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        int i2;
        int i3 = this.p;
        int i4 = this.p & -3;
        if (z2) {
            i2 = 2;
        } else {
            i2 = 0;
        }
        this.p = i2 | i4;
        if (i3 != this.p) {
            this.f156a.b(false);
        }
    }

    public boolean isVisible() {
        if (this.s == null || !this.s.overridesItemVisibility()) {
            if ((this.p & 8) != 0) {
                return false;
            }
            return true;
        } else if ((this.p & 8) != 0 || !this.s.isVisible()) {
            return false;
        } else {
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(boolean z2) {
        int i2 = this.p;
        this.p = (z2 ? 0 : 8) | (this.p & -9);
        if (i2 != this.p) {
            return true;
        }
        return false;
    }

    public MenuItem setVisible(boolean z2) {
        if (c(z2)) {
            this.f156a.a(this);
        }
        return this;
    }

    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.o = onMenuItemClickListener;
        return this;
    }

    public String toString() {
        if (this.f != null) {
            return this.f.toString();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(ContextMenu.ContextMenuInfo contextMenuInfo) {
        this.v = contextMenuInfo;
    }

    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.v;
    }

    public void g() {
        this.f156a.b(this);
    }

    public boolean h() {
        return this.f156a.q();
    }

    public boolean i() {
        return (this.p & 32) == 32;
    }

    public boolean j() {
        return (this.q & 1) == 1;
    }

    public boolean k() {
        return (this.q & 2) == 2;
    }

    public void d(boolean z2) {
        if (z2) {
            this.p |= 32;
        } else {
            this.p &= -33;
        }
    }

    public boolean l() {
        return (this.q & 4) == 4;
    }

    public void setShowAsAction(int i2) {
        switch (i2 & 3) {
            case 0:
            case 1:
            case 2:
                break;
            default:
                throw new IllegalArgumentException("SHOW_AS_ACTION_ALWAYS, SHOW_AS_ACTION_IF_ROOM, and SHOW_AS_ACTION_NEVER are mutually exclusive.");
        }
        this.q = i2;
        this.f156a.b(this);
    }

    /* renamed from: a */
    public SupportMenuItem setActionView(View view) {
        this.r = view;
        this.s = null;
        if (view != null && view.getId() == -1 && this.f157b > 0) {
            view.setId(this.f157b);
        }
        this.f156a.b(this);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.LinearLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* renamed from: a */
    public SupportMenuItem setActionView(int i2) {
        Context e2 = this.f156a.e();
        setActionView(LayoutInflater.from(e2).inflate(i2, (ViewGroup) new LinearLayout(e2), false));
        return this;
    }

    public View getActionView() {
        if (this.r != null) {
            return this.r;
        }
        if (this.s == null) {
            return null;
        }
        this.r = this.s.onCreateActionView(this);
        return this.r;
    }

    public MenuItem setActionProvider(android.view.ActionProvider actionProvider) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setActionProvider()");
    }

    public android.view.ActionProvider getActionProvider() {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.getActionProvider()");
    }

    public ActionProvider getSupportActionProvider() {
        return this.s;
    }

    public SupportMenuItem setSupportActionProvider(ActionProvider actionProvider) {
        if (this.s != null) {
            this.s.reset();
        }
        this.r = null;
        this.s = actionProvider;
        this.f156a.b(true);
        if (this.s != null) {
            this.s.setVisibilityListener(new ActionProvider.VisibilityListener() {
                public void onActionProviderVisibilityChanged(boolean z) {
                    MenuItemImpl.this.f156a.a(MenuItemImpl.this);
                }
            });
        }
        return this;
    }

    /* renamed from: b */
    public SupportMenuItem setShowAsActionFlags(int i2) {
        setShowAsAction(i2);
        return this;
    }

    public boolean expandActionView() {
        if (!m()) {
            return false;
        }
        if (this.t == null || this.t.onMenuItemActionExpand(this)) {
            return this.f156a.c(this);
        }
        return false;
    }

    public boolean collapseActionView() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null) {
            return true;
        }
        if (this.t == null || this.t.onMenuItemActionCollapse(this)) {
            return this.f156a.d(this);
        }
        return false;
    }

    public SupportMenuItem setSupportOnActionExpandListener(MenuItemCompat.OnActionExpandListener onActionExpandListener) {
        this.t = onActionExpandListener;
        return this;
    }

    public boolean m() {
        if ((this.q & 8) == 0) {
            return false;
        }
        if (this.r == null && this.s != null) {
            this.r = this.s.onCreateActionView(this);
        }
        if (this.r != null) {
            return true;
        }
        return false;
    }

    public void e(boolean z2) {
        this.u = z2;
        this.f156a.b(false);
    }

    public boolean isActionViewExpanded() {
        return this.u;
    }

    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        throw new UnsupportedOperationException("This is not supported, use MenuItemCompat.setOnActionExpandListener()");
    }
}
