package android.support.v7.view.menu;

/* compiled from: BaseWrapper */
class c<T> {

    /* renamed from: b  reason: collision with root package name */
    final T f162b;

    c(T t) {
        if (t == null) {
            throw new IllegalArgumentException("Wrapped Object can not be null.");
        }
        this.f162b = t;
    }
}
