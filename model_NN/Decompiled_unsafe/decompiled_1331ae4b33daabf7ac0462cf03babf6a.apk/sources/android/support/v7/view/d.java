package android.support.v7.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.support.v7.a.a;
import android.view.LayoutInflater;

/* compiled from: ContextThemeWrapper */
public class d extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private int f127a;

    /* renamed from: b  reason: collision with root package name */
    private Resources.Theme f128b;
    private LayoutInflater c;

    public d(Context context, int i) {
        super(context);
        this.f127a = i;
    }

    public d(Context context, Resources.Theme theme) {
        super(context);
        this.f128b = theme;
    }

    public void setTheme(int i) {
        if (this.f127a != i) {
            this.f127a = i;
            b();
        }
    }

    public int a() {
        return this.f127a;
    }

    public Resources.Theme getTheme() {
        if (this.f128b != null) {
            return this.f128b;
        }
        if (this.f127a == 0) {
            this.f127a = a.j.Theme_AppCompat_Light;
        }
        b();
        return this.f128b;
    }

    public Object getSystemService(String str) {
        if (!"layout_inflater".equals(str)) {
            return getBaseContext().getSystemService(str);
        }
        if (this.c == null) {
            this.c = LayoutInflater.from(getBaseContext()).cloneInContext(this);
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(Resources.Theme theme, int i, boolean z) {
        theme.applyStyle(i, true);
    }

    private void b() {
        boolean z = this.f128b == null;
        if (z) {
            this.f128b = getResources().newTheme();
            Resources.Theme theme = getBaseContext().getTheme();
            if (theme != null) {
                this.f128b.setTo(theme);
            }
        }
        a(this.f128b, this.f127a, z);
    }
}
