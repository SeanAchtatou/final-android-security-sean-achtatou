package android.support.v7.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.support.v7.view.menu.l;
import android.support.v7.widget.s;
import android.support.v7.widget.t;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/* compiled from: CascadingMenuPopup */
final class d extends j implements l, View.OnKeyListener, PopupWindow.OnDismissListener {

    /* renamed from: a  reason: collision with root package name */
    final Handler f163a;

    /* renamed from: b  reason: collision with root package name */
    final List<a> f164b = new ArrayList();
    View c;
    boolean d;
    private final Context e;
    private final int f;
    private final int g;
    private final int h;
    private final boolean i;
    private final List<MenuBuilder> j = new LinkedList();
    private final ViewTreeObserver.OnGlobalLayoutListener k = new ViewTreeObserver.OnGlobalLayoutListener() {
        public void onGlobalLayout() {
            if (d.this.d() && d.this.f164b.size() > 0 && !d.this.f164b.get(0).f169a.g()) {
                View view = d.this.c;
                if (view == null || !view.isShown()) {
                    d.this.c();
                    return;
                }
                for (a aVar : d.this.f164b) {
                    aVar.f169a.a();
                }
            }
        }
    };
    private final s l = new s() {
        public void a(MenuBuilder menuBuilder, MenuItem menuItem) {
            d.this.f163a.removeCallbacksAndMessages(menuBuilder);
        }

        public void b(final MenuBuilder menuBuilder, final MenuItem menuItem) {
            int i;
            final a aVar;
            d.this.f163a.removeCallbacksAndMessages(null);
            int i2 = 0;
            int size = d.this.f164b.size();
            while (true) {
                if (i2 >= size) {
                    i = -1;
                    break;
                } else if (menuBuilder == d.this.f164b.get(i2).f170b) {
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i != -1) {
                int i3 = i + 1;
                if (i3 < d.this.f164b.size()) {
                    aVar = d.this.f164b.get(i3);
                } else {
                    aVar = null;
                }
                d.this.f163a.postAtTime(new Runnable() {
                    public void run() {
                        if (aVar != null) {
                            d.this.d = true;
                            aVar.f170b.a(false);
                            d.this.d = false;
                        }
                        if (menuItem.isEnabled() && menuItem.hasSubMenu()) {
                            menuBuilder.a(menuItem, 0);
                        }
                    }
                }, menuBuilder, SystemClock.uptimeMillis() + 200);
            }
        }
    };
    private int m = 0;
    private int n = 0;
    private View o;
    private int p;
    private boolean q;
    private boolean r;
    private int s;
    private int t;
    private boolean u;
    private boolean v;
    private l.a w;
    private ViewTreeObserver x;
    private PopupWindow.OnDismissListener y;

    public d(Context context, View view, int i2, int i3, boolean z) {
        this.e = context;
        this.o = view;
        this.g = i2;
        this.h = i3;
        this.i = z;
        this.u = false;
        this.p = i();
        Resources resources = context.getResources();
        this.f = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(a.d.abc_config_prefDialogWidth));
        this.f163a = new Handler();
    }

    public void b(boolean z) {
        this.u = z;
    }

    private t h() {
        t tVar = new t(this.e, null, this.g, this.h);
        tVar.a(this.l);
        tVar.a((AdapterView.OnItemClickListener) this);
        tVar.a((PopupWindow.OnDismissListener) this);
        tVar.b(this.o);
        tVar.e(this.n);
        tVar.a(true);
        return tVar;
    }

    public void a() {
        if (!d()) {
            for (MenuBuilder c2 : this.j) {
                c(c2);
            }
            this.j.clear();
            this.c = this.o;
            if (this.c != null) {
                boolean z = this.x == null;
                this.x = this.c.getViewTreeObserver();
                if (z) {
                    this.x.addOnGlobalLayoutListener(this.k);
                }
            }
        }
    }

    public void c() {
        int size = this.f164b.size();
        if (size > 0) {
            a[] aVarArr = (a[]) this.f164b.toArray(new a[size]);
            for (int i2 = size - 1; i2 >= 0; i2--) {
                a aVar = aVarArr[i2];
                if (aVar.f169a.d()) {
                    aVar.f169a.c();
                }
            }
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        c();
        return true;
    }

    private int i() {
        if (ViewCompat.getLayoutDirection(this.o) == 1) {
            return 0;
        }
        return 1;
    }

    private int d(int i2) {
        ListView a2 = this.f164b.get(this.f164b.size() - 1).a();
        int[] iArr = new int[2];
        a2.getLocationOnScreen(iArr);
        Rect rect = new Rect();
        this.c.getWindowVisibleDisplayFrame(rect);
        if (this.p == 1) {
            if (a2.getWidth() + iArr[0] + i2 > rect.right) {
                return 0;
            }
            return 1;
        } else if (iArr[0] - i2 < 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public void a(MenuBuilder menuBuilder) {
        menuBuilder.a(this, this.e);
        if (d()) {
            c(menuBuilder);
        } else {
            this.j.add(menuBuilder);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void c(MenuBuilder menuBuilder) {
        View view;
        a aVar;
        boolean z;
        int i2;
        LayoutInflater from = LayoutInflater.from(this.e);
        f fVar = new f(menuBuilder, from, this.i);
        if (!d() && this.u) {
            fVar.a(true);
        } else if (d()) {
            fVar.a(j.b(menuBuilder));
        }
        int a2 = a(fVar, null, this.e, this.f);
        t h2 = h();
        h2.a((ListAdapter) fVar);
        h2.g(a2);
        h2.e(this.n);
        if (this.f164b.size() > 0) {
            a aVar2 = this.f164b.get(this.f164b.size() - 1);
            view = a(aVar2, menuBuilder);
            aVar = aVar2;
        } else {
            view = null;
            aVar = null;
        }
        if (view != null) {
            h2.b(false);
            h2.a((Object) null);
            int d2 = d(a2);
            if (d2 == 1) {
                z = true;
            } else {
                z = false;
            }
            this.p = d2;
            int[] iArr = new int[2];
            view.getLocationInWindow(iArr);
            int j2 = aVar.f169a.j() + iArr[0];
            int k2 = iArr[1] + aVar.f169a.k();
            if ((this.n & 5) == 5) {
                if (z) {
                    i2 = j2 + a2;
                } else {
                    i2 = j2 - view.getWidth();
                }
            } else if (z) {
                i2 = view.getWidth() + j2;
            } else {
                i2 = j2 - a2;
            }
            h2.c(i2);
            h2.d(k2);
        } else {
            if (this.q) {
                h2.c(this.s);
            }
            if (this.r) {
                h2.d(this.t);
            }
            h2.a(g());
        }
        this.f164b.add(new a(h2, menuBuilder, this.p));
        h2.a();
        if (aVar == null && this.v && menuBuilder.m() != null) {
            ListView e2 = h2.e();
            FrameLayout frameLayout = (FrameLayout) from.inflate(a.h.abc_popup_menu_header_item_layout, (ViewGroup) e2, false);
            frameLayout.setEnabled(false);
            ((TextView) frameLayout.findViewById(16908310)).setText(menuBuilder.m());
            e2.addHeaderView(frameLayout, null, false);
            h2.a();
        }
    }

    private MenuItem a(MenuBuilder menuBuilder, MenuBuilder menuBuilder2) {
        int size = menuBuilder.size();
        for (int i2 = 0; i2 < size; i2++) {
            MenuItem item = menuBuilder.getItem(i2);
            if (item.hasSubMenu() && menuBuilder2 == item.getSubMenu()) {
                return item;
            }
        }
        return null;
    }

    private View a(a aVar, MenuBuilder menuBuilder) {
        f fVar;
        int i2;
        int i3;
        int i4 = 0;
        MenuItem a2 = a(aVar.f170b, menuBuilder);
        if (a2 == null) {
            return null;
        }
        ListView a3 = aVar.a();
        ListAdapter adapter = a3.getAdapter();
        if (adapter instanceof HeaderViewListAdapter) {
            HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
            i2 = headerViewListAdapter.getHeadersCount();
            fVar = (f) headerViewListAdapter.getWrappedAdapter();
        } else {
            fVar = (f) adapter;
            i2 = 0;
        }
        int count = fVar.getCount();
        while (true) {
            if (i4 >= count) {
                i3 = -1;
                break;
            } else if (a2 == fVar.getItem(i4)) {
                i3 = i4;
                break;
            } else {
                i4++;
            }
        }
        if (i3 == -1) {
            return null;
        }
        int firstVisiblePosition = (i3 + i2) - a3.getFirstVisiblePosition();
        if (firstVisiblePosition < 0 || firstVisiblePosition >= a3.getChildCount()) {
            return null;
        }
        return a3.getChildAt(firstVisiblePosition);
    }

    public boolean d() {
        return this.f164b.size() > 0 && this.f164b.get(0).f169a.d();
    }

    public void onDismiss() {
        a aVar;
        int size = this.f164b.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                aVar = null;
                break;
            }
            aVar = this.f164b.get(i2);
            if (!aVar.f169a.d()) {
                break;
            }
            i2++;
        }
        if (aVar != null) {
            aVar.f170b.a(false);
        }
    }

    public void a(boolean z) {
        for (a a2 : this.f164b) {
            a(a2.a().getAdapter()).notifyDataSetChanged();
        }
    }

    public void a(l.a aVar) {
        this.w = aVar;
    }

    public boolean a(SubMenuBuilder subMenuBuilder) {
        for (a next : this.f164b) {
            if (subMenuBuilder == next.f170b) {
                next.a().requestFocus();
                return true;
            }
        }
        if (!subMenuBuilder.hasVisibleItems()) {
            return false;
        }
        a((MenuBuilder) subMenuBuilder);
        if (this.w != null) {
            this.w.a(subMenuBuilder);
        }
        return true;
    }

    private int d(MenuBuilder menuBuilder) {
        int size = this.f164b.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (menuBuilder == this.f164b.get(i2).f170b) {
                return i2;
            }
        }
        return -1;
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        int d2 = d(menuBuilder);
        if (d2 >= 0) {
            int i2 = d2 + 1;
            if (i2 < this.f164b.size()) {
                this.f164b.get(i2).f170b.a(false);
            }
            a remove = this.f164b.remove(d2);
            remove.f170b.b(this);
            if (this.d) {
                remove.f169a.b((Object) null);
                remove.f169a.b(0);
            }
            remove.f169a.c();
            int size = this.f164b.size();
            if (size > 0) {
                this.p = this.f164b.get(size - 1).c;
            } else {
                this.p = i();
            }
            if (size == 0) {
                c();
                if (this.w != null) {
                    this.w.a(menuBuilder, true);
                }
                if (this.x != null) {
                    if (this.x.isAlive()) {
                        this.x.removeGlobalOnLayoutListener(this.k);
                    }
                    this.x = null;
                }
                this.y.onDismiss();
            } else if (z) {
                this.f164b.get(0).f170b.a(false);
            }
        }
    }

    public boolean b() {
        return false;
    }

    public void a(int i2) {
        if (this.m != i2) {
            this.m = i2;
            this.n = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this.o));
        }
    }

    public void a(View view) {
        if (this.o != view) {
            this.o = view;
            this.n = GravityCompat.getAbsoluteGravity(this.m, ViewCompat.getLayoutDirection(this.o));
        }
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.y = onDismissListener;
    }

    public ListView e() {
        if (this.f164b.isEmpty()) {
            return null;
        }
        return this.f164b.get(this.f164b.size() - 1).a();
    }

    public void b(int i2) {
        this.q = true;
        this.s = i2;
    }

    public void c(int i2) {
        this.r = true;
        this.t = i2;
    }

    public void c(boolean z) {
        this.v = z;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* compiled from: CascadingMenuPopup */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        public final t f169a;

        /* renamed from: b  reason: collision with root package name */
        public final MenuBuilder f170b;
        public final int c;

        public a(t tVar, MenuBuilder menuBuilder, int i) {
            this.f169a = tVar;
            this.f170b = menuBuilder;
            this.c = i;
        }

        public ListView a() {
            return this.f169a.e();
        }
    }
}
