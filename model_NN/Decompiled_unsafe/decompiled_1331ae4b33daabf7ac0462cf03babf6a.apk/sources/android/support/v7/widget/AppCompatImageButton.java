package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.ImageButton;

public class AppCompatImageButton extends ImageButton implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    private e f229a;

    /* renamed from: b  reason: collision with root package name */
    private h f230b;

    public AppCompatImageButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.imageButtonStyle);
    }

    public AppCompatImageButton(Context context, AttributeSet attributeSet, int i) {
        super(z.a(context), attributeSet, i);
        this.f229a = new e(this);
        this.f229a.a(attributeSet, i);
        this.f230b = new h(this);
        this.f230b.a(attributeSet, i);
    }

    public void setImageResource(int i) {
        this.f230b.a(i);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f229a != null) {
            this.f229a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f229a != null) {
            this.f229a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f229a != null) {
            this.f229a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f229a != null) {
            return this.f229a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f229a != null) {
            this.f229a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f229a != null) {
            return this.f229a.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f229a != null) {
            this.f229a.c();
        }
    }

    public boolean hasOverlappingRendering() {
        return this.f230b.a() && super.hasOverlappingRendering();
    }
}
