package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class ScrollingTabContainerView extends HorizontalScrollView implements AdapterView.OnItemSelectedListener {
    private static final Interpolator k = new DecelerateInterpolator();

    /* renamed from: a  reason: collision with root package name */
    Runnable f277a;

    /* renamed from: b  reason: collision with root package name */
    LinearLayoutCompat f278b;
    int c;
    int d;
    protected ViewPropertyAnimatorCompat e;
    private b f;
    private Spinner g;
    private boolean h;
    private int i;
    private int j;

    public void onMeasure(int i2, int i3) {
        boolean z = true;
        int mode = View.MeasureSpec.getMode(i2);
        boolean z2 = mode == 1073741824;
        setFillViewport(z2);
        int childCount = this.f278b.getChildCount();
        if (childCount <= 1 || !(mode == 1073741824 || mode == Integer.MIN_VALUE)) {
            this.c = -1;
        } else {
            if (childCount > 2) {
                this.c = (int) (((float) View.MeasureSpec.getSize(i2)) * 0.4f);
            } else {
                this.c = View.MeasureSpec.getSize(i2) / 2;
            }
            this.c = Math.min(this.c, this.d);
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(this.i, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
        if (z2 || !this.h) {
            z = false;
        }
        if (z) {
            this.f278b.measure(0, makeMeasureSpec);
            if (this.f278b.getMeasuredWidth() > View.MeasureSpec.getSize(i2)) {
                b();
            } else {
                c();
            }
        } else {
            c();
        }
        int measuredWidth = getMeasuredWidth();
        super.onMeasure(i2, makeMeasureSpec);
        int measuredWidth2 = getMeasuredWidth();
        if (z2 && measuredWidth != measuredWidth2) {
            setTabSelected(this.j);
        }
    }

    private boolean a() {
        return this.g != null && this.g.getParent() == this;
    }

    public void setAllowCollapse(boolean z) {
        this.h = z;
    }

    private void b() {
        if (!a()) {
            if (this.g == null) {
                this.g = d();
            }
            removeView(this.f278b);
            addView(this.g, new ViewGroup.LayoutParams(-2, -1));
            if (this.g.getAdapter() == null) {
                this.g.setAdapter((SpinnerAdapter) new a());
            }
            if (this.f277a != null) {
                removeCallbacks(this.f277a);
                this.f277a = null;
            }
            this.g.setSelection(this.j);
        }
    }

    private boolean c() {
        if (a()) {
            removeView(this.g);
            addView(this.f278b, new ViewGroup.LayoutParams(-2, -1));
            setTabSelected(this.g.getSelectedItemPosition());
        }
        return false;
    }

    public void setTabSelected(int i2) {
        boolean z;
        this.j = i2;
        int childCount = this.f278b.getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = this.f278b.getChildAt(i3);
            if (i3 == i2) {
                z = true;
            } else {
                z = false;
            }
            childAt.setSelected(z);
            if (z) {
                a(i2);
            }
        }
        if (this.g != null && i2 >= 0) {
            this.g.setSelection(i2);
        }
    }

    public void setContentHeight(int i2) {
        this.i = i2;
        requestLayout();
    }

    private Spinner d() {
        AppCompatSpinner appCompatSpinner = new AppCompatSpinner(getContext(), null, a.C0002a.actionDropDownStyle);
        appCompatSpinner.setLayoutParams(new LinearLayoutCompat.LayoutParams(-2, -1));
        appCompatSpinner.setOnItemSelectedListener(this);
        return appCompatSpinner;
    }

    /* access modifiers changed from: protected */
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        android.support.v7.view.a a2 = android.support.v7.view.a.a(getContext());
        setContentHeight(a2.e());
        this.d = a2.g();
    }

    public void a(int i2) {
        final View childAt = this.f278b.getChildAt(i2);
        if (this.f277a != null) {
            removeCallbacks(this.f277a);
        }
        this.f277a = new Runnable() {
            public void run() {
                ScrollingTabContainerView.this.smoothScrollTo(childAt.getLeft() - ((ScrollingTabContainerView.this.getWidth() - childAt.getWidth()) / 2), 0);
                ScrollingTabContainerView.this.f277a = null;
            }
        };
        post(this.f277a);
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f277a != null) {
            post(this.f277a);
        }
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f277a != null) {
            removeCallbacks(this.f277a);
        }
    }

    /* access modifiers changed from: package-private */
    public c a(ActionBar.b bVar, boolean z) {
        c cVar = new c(getContext(), bVar, z);
        if (z) {
            cVar.setBackgroundDrawable(null);
            cVar.setLayoutParams(new AbsListView.LayoutParams(-1, this.i));
        } else {
            cVar.setFocusable(true);
            if (this.f == null) {
                this.f = new b();
            }
            cVar.setOnClickListener(this.f);
        }
        return cVar;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        ((c) view).b().d();
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }

    private class c extends LinearLayoutCompat implements View.OnLongClickListener {

        /* renamed from: b  reason: collision with root package name */
        private final int[] f286b = {16842964};
        private ActionBar.b c;
        private TextView d;
        private ImageView e;
        private View f;

        public c(Context context, ActionBar.b bVar, boolean z) {
            super(context, null, a.C0002a.actionBarTabStyle);
            this.c = bVar;
            ac a2 = ac.a(context, null, this.f286b, a.C0002a.actionBarTabStyle, 0);
            if (a2.g(0)) {
                setBackgroundDrawable(a2.a(0));
            }
            a2.a();
            if (z) {
                setGravity(8388627);
            }
            a();
        }

        public void a(ActionBar.b bVar) {
            this.c = bVar;
            a();
        }

        public void setSelected(boolean z) {
            boolean z2 = isSelected() != z;
            super.setSelected(z);
            if (z2 && z) {
                sendAccessibilityEvent(4);
            }
        }

        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            super.onInitializeAccessibilityEvent(accessibilityEvent);
            accessibilityEvent.setClassName(ActionBar.b.class.getName());
        }

        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
            if (Build.VERSION.SDK_INT >= 14) {
                accessibilityNodeInfo.setClassName(ActionBar.b.class.getName());
            }
        }

        public void onMeasure(int i, int i2) {
            super.onMeasure(i, i2);
            if (ScrollingTabContainerView.this.c > 0 && getMeasuredWidth() > ScrollingTabContainerView.this.c) {
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(ScrollingTabContainerView.this.c, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE), i2);
            }
        }

        public void a() {
            boolean z;
            ActionBar.b bVar = this.c;
            View c2 = bVar.c();
            if (c2 != null) {
                ViewParent parent = c2.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(c2);
                    }
                    addView(c2);
                }
                this.f = c2;
                if (this.d != null) {
                    this.d.setVisibility(8);
                }
                if (this.e != null) {
                    this.e.setVisibility(8);
                    this.e.setImageDrawable(null);
                    return;
                }
                return;
            }
            if (this.f != null) {
                removeView(this.f);
                this.f = null;
            }
            Drawable a2 = bVar.a();
            CharSequence b2 = bVar.b();
            if (a2 != null) {
                if (this.e == null) {
                    AppCompatImageView appCompatImageView = new AppCompatImageView(getContext());
                    LinearLayoutCompat.LayoutParams layoutParams = new LinearLayoutCompat.LayoutParams(-2, -2);
                    layoutParams.h = 16;
                    appCompatImageView.setLayoutParams(layoutParams);
                    addView(appCompatImageView, 0);
                    this.e = appCompatImageView;
                }
                this.e.setImageDrawable(a2);
                this.e.setVisibility(0);
            } else if (this.e != null) {
                this.e.setVisibility(8);
                this.e.setImageDrawable(null);
            }
            if (!TextUtils.isEmpty(b2)) {
                z = true;
            } else {
                z = false;
            }
            if (z) {
                if (this.d == null) {
                    AppCompatTextView appCompatTextView = new AppCompatTextView(getContext(), null, a.C0002a.actionBarTabTextStyle);
                    appCompatTextView.setEllipsize(TextUtils.TruncateAt.END);
                    LinearLayoutCompat.LayoutParams layoutParams2 = new LinearLayoutCompat.LayoutParams(-2, -2);
                    layoutParams2.h = 16;
                    appCompatTextView.setLayoutParams(layoutParams2);
                    addView(appCompatTextView);
                    this.d = appCompatTextView;
                }
                this.d.setText(b2);
                this.d.setVisibility(0);
            } else if (this.d != null) {
                this.d.setVisibility(8);
                this.d.setText((CharSequence) null);
            }
            if (this.e != null) {
                this.e.setContentDescription(bVar.e());
            }
            if (z || TextUtils.isEmpty(bVar.e())) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.c.e(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public ActionBar.b b() {
            return this.c;
        }
    }

    private class a extends BaseAdapter {
        a() {
        }

        public int getCount() {
            return ScrollingTabContainerView.this.f278b.getChildCount();
        }

        public Object getItem(int i) {
            return ((c) ScrollingTabContainerView.this.f278b.getChildAt(i)).b();
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                return ScrollingTabContainerView.this.a((ActionBar.b) getItem(i), true);
            }
            ((c) view).a((ActionBar.b) getItem(i));
            return view;
        }
    }

    private class b implements View.OnClickListener {
        b() {
        }

        public void onClick(View view) {
            boolean z;
            ((c) view).b().d();
            int childCount = ScrollingTabContainerView.this.f278b.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = ScrollingTabContainerView.this.f278b.getChildAt(i);
                if (childAt == view) {
                    z = true;
                } else {
                    z = false;
                }
                childAt.setSelected(z);
            }
        }
    }

    protected class VisibilityAnimListener implements ViewPropertyAnimatorListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ScrollingTabContainerView f281a;

        /* renamed from: b  reason: collision with root package name */
        private boolean f282b;
        private int c;

        public void onAnimationStart(View view) {
            this.f281a.setVisibility(0);
            this.f282b = false;
        }

        public void onAnimationEnd(View view) {
            if (!this.f282b) {
                this.f281a.e = null;
                this.f281a.setVisibility(this.c);
            }
        }

        public void onAnimationCancel(View view) {
            this.f282b = true;
        }
    }
}
