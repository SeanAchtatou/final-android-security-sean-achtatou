package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.TintableBackgroundView;
import android.util.AttributeSet;
import android.widget.ImageView;

public class AppCompatImageView extends ImageView implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    private e f231a;

    /* renamed from: b  reason: collision with root package name */
    private h f232b;

    public AppCompatImageView(Context context) {
        this(context, null);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AppCompatImageView(Context context, AttributeSet attributeSet, int i) {
        super(z.a(context), attributeSet, i);
        this.f231a = new e(this);
        this.f231a.a(attributeSet, i);
        this.f232b = new h(this);
        this.f232b.a(attributeSet, i);
    }

    public void setImageResource(int i) {
        this.f232b.a(i);
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f231a != null) {
            this.f231a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f231a != null) {
            this.f231a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f231a != null) {
            this.f231a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f231a != null) {
            return this.f231a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f231a != null) {
            this.f231a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f231a != null) {
            return this.f231a.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f231a != null) {
            this.f231a.c();
        }
    }

    public boolean hasOverlappingRendering() {
        return this.f232b.a() && super.hasOverlappingRendering();
    }
}
