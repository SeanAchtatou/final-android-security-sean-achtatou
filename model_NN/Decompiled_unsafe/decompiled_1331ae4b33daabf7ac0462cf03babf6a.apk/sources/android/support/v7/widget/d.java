package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.support.v4.os.AsyncTaskCompat;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import com.meizu.cloud.pushsdk.constants.PushConstants;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

/* compiled from: ActivityChooserModel */
class d extends DataSetObservable {

    /* renamed from: a  reason: collision with root package name */
    static final String f352a = d.class.getSimpleName();
    private static final Object e = new Object();
    private static final Map<String, d> f = new HashMap();

    /* renamed from: b  reason: collision with root package name */
    final Context f353b;
    final String c;
    boolean d = true;
    private final Object g = new Object();
    private final List<a> h = new ArrayList();
    private final List<C0006d> i = new ArrayList();
    private Intent j;
    private b k = new c();
    private int l = 50;
    private boolean m = false;
    private boolean n = true;
    private boolean o = false;
    private e p;

    /* compiled from: ActivityChooserModel */
    public interface b {
        void a(Intent intent, List<a> list, List<C0006d> list2);
    }

    /* compiled from: ActivityChooserModel */
    public interface e {
        boolean a(d dVar, Intent intent);
    }

    public static d a(Context context, String str) {
        d dVar;
        synchronized (e) {
            dVar = f.get(str);
            if (dVar == null) {
                dVar = new d(context, str);
                f.put(str, dVar);
            }
        }
        return dVar;
    }

    private d(Context context, String str) {
        this.f353b = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(".xml")) {
            this.c = str;
        } else {
            this.c = str + ".xml";
        }
    }

    public int a() {
        int size;
        synchronized (this.g) {
            e();
            size = this.h.size();
        }
        return size;
    }

    public ResolveInfo a(int i2) {
        ResolveInfo resolveInfo;
        synchronized (this.g) {
            e();
            resolveInfo = this.h.get(i2).f354a;
        }
        return resolveInfo;
    }

    public int a(ResolveInfo resolveInfo) {
        synchronized (this.g) {
            e();
            List<a> list = this.h;
            int size = list.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (list.get(i2).f354a == resolveInfo) {
                    return i2;
                }
            }
            return -1;
        }
    }

    public Intent b(int i2) {
        synchronized (this.g) {
            if (this.j == null) {
                return null;
            }
            e();
            a aVar = this.h.get(i2);
            ComponentName componentName = new ComponentName(aVar.f354a.activityInfo.packageName, aVar.f354a.activityInfo.name);
            Intent intent = new Intent(this.j);
            intent.setComponent(componentName);
            if (this.p != null) {
                if (this.p.a(this, new Intent(intent))) {
                    return null;
                }
            }
            a(new C0006d(componentName, System.currentTimeMillis(), 1.0f));
            return intent;
        }
    }

    public ResolveInfo b() {
        synchronized (this.g) {
            e();
            if (this.h.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.h.get(0).f354a;
            return resolveInfo;
        }
    }

    public void c(int i2) {
        float f2;
        synchronized (this.g) {
            e();
            a aVar = this.h.get(i2);
            a aVar2 = this.h.get(0);
            if (aVar2 != null) {
                f2 = (aVar2.f355b - aVar.f355b) + 5.0f;
            } else {
                f2 = 1.0f;
            }
            a(new C0006d(new ComponentName(aVar.f354a.activityInfo.packageName, aVar.f354a.activityInfo.name), System.currentTimeMillis(), f2));
        }
    }

    private void d() {
        if (!this.m) {
            throw new IllegalStateException("No preceding call to #readHistoricalData");
        } else if (this.n) {
            this.n = false;
            if (!TextUtils.isEmpty(this.c)) {
                AsyncTaskCompat.executeParallel(new f(), new ArrayList(this.i), this.c);
            }
        }
    }

    public int c() {
        int size;
        synchronized (this.g) {
            e();
            size = this.i.size();
        }
        return size;
    }

    private void e() {
        boolean g2 = g() | h();
        i();
        if (g2) {
            f();
            notifyChanged();
        }
    }

    private boolean f() {
        if (this.k == null || this.j == null || this.h.isEmpty() || this.i.isEmpty()) {
            return false;
        }
        this.k.a(this.j, this.h, Collections.unmodifiableList(this.i));
        return true;
    }

    private boolean g() {
        if (!this.o || this.j == null) {
            return false;
        }
        this.o = false;
        this.h.clear();
        List<ResolveInfo> queryIntentActivities = this.f353b.getPackageManager().queryIntentActivities(this.j, 0);
        int size = queryIntentActivities.size();
        for (int i2 = 0; i2 < size; i2++) {
            this.h.add(new a(queryIntentActivities.get(i2)));
        }
        return true;
    }

    private boolean h() {
        if (!this.d || !this.n || TextUtils.isEmpty(this.c)) {
            return false;
        }
        this.d = false;
        this.m = true;
        j();
        return true;
    }

    private boolean a(C0006d dVar) {
        boolean add = this.i.add(dVar);
        if (add) {
            this.n = true;
            i();
            d();
            f();
            notifyChanged();
        }
        return add;
    }

    private void i() {
        int size = this.i.size() - this.l;
        if (size > 0) {
            this.n = true;
            for (int i2 = 0; i2 < size; i2++) {
                C0006d remove = this.i.remove(0);
            }
        }
    }

    /* renamed from: android.support.v7.widget.d$d  reason: collision with other inner class name */
    /* compiled from: ActivityChooserModel */
    public static final class C0006d {

        /* renamed from: a  reason: collision with root package name */
        public final ComponentName f358a;

        /* renamed from: b  reason: collision with root package name */
        public final long f359b;
        public final float c;

        public C0006d(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public C0006d(ComponentName componentName, long j, float f) {
            this.f358a = componentName;
            this.f359b = j;
            this.c = f;
        }

        public int hashCode() {
            return (((((this.f358a == null ? 0 : this.f358a.hashCode()) + 31) * 31) + ((int) (this.f359b ^ (this.f359b >>> 32)))) * 31) + Float.floatToIntBits(this.c);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            C0006d dVar = (C0006d) obj;
            if (this.f358a == null) {
                if (dVar.f358a != null) {
                    return false;
                }
            } else if (!this.f358a.equals(dVar.f358a)) {
                return false;
            }
            if (this.f359b != dVar.f359b) {
                return false;
            }
            if (Float.floatToIntBits(this.c) != Float.floatToIntBits(dVar.c)) {
                return false;
            }
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:").append(this.f358a);
            sb.append("; time:").append(this.f359b);
            sb.append("; weight:").append(new BigDecimal((double) this.c));
            sb.append("]");
            return sb.toString();
        }
    }

    /* compiled from: ActivityChooserModel */
    public final class a implements Comparable<a> {

        /* renamed from: a  reason: collision with root package name */
        public final ResolveInfo f354a;

        /* renamed from: b  reason: collision with root package name */
        public float f355b;

        public a(ResolveInfo resolveInfo) {
            this.f354a = resolveInfo;
        }

        public int hashCode() {
            return Float.floatToIntBits(this.f355b) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (Float.floatToIntBits(this.f355b) != Float.floatToIntBits(((a) obj).f355b)) {
                return false;
            }
            return true;
        }

        /* renamed from: a */
        public int compareTo(a aVar) {
            return Float.floatToIntBits(aVar.f355b) - Float.floatToIntBits(this.f355b);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:").append(this.f354a.toString());
            sb.append("; weight:").append(new BigDecimal((double) this.f355b));
            sb.append("]");
            return sb.toString();
        }
    }

    /* compiled from: ActivityChooserModel */
    private final class c implements b {

        /* renamed from: b  reason: collision with root package name */
        private final Map<ComponentName, a> f357b = new HashMap();

        c() {
        }

        public void a(Intent intent, List<a> list, List<C0006d> list2) {
            float f;
            Map<ComponentName, a> map = this.f357b;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                a aVar = list.get(i);
                aVar.f355b = 0.0f;
                map.put(new ComponentName(aVar.f354a.activityInfo.packageName, aVar.f354a.activityInfo.name), aVar);
            }
            float f2 = 1.0f;
            int size2 = list2.size() - 1;
            while (size2 >= 0) {
                C0006d dVar = list2.get(size2);
                a aVar2 = map.get(dVar.f358a);
                if (aVar2 != null) {
                    aVar2.f355b = (dVar.c * f2) + aVar2.f355b;
                    f = 0.95f * f2;
                } else {
                    f = f2;
                }
                size2--;
                f2 = f;
            }
            Collections.sort(list);
        }
    }

    private void j() {
        try {
            FileInputStream openFileInput = this.f353b.openFileInput(this.c);
            try {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setInput(openFileInput, "UTF-8");
                int i2 = 0;
                while (i2 != 1 && i2 != 2) {
                    i2 = newPullParser.next();
                }
                if (!"historical-records".equals(newPullParser.getName())) {
                    throw new XmlPullParserException("Share records file does not start with historical-records tag.");
                }
                List<C0006d> list = this.i;
                list.clear();
                while (true) {
                    int next = newPullParser.next();
                    if (next == 1) {
                        if (openFileInput != null) {
                            try {
                                openFileInput.close();
                                return;
                            } catch (IOException e2) {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else if (!(next == 3 || next == 4)) {
                        if (!"historical-record".equals(newPullParser.getName())) {
                            throw new XmlPullParserException("Share records file not well-formed.");
                        }
                        list.add(new C0006d(newPullParser.getAttributeValue(null, PushConstants.INTENT_ACTIVITY_NAME), Long.parseLong(newPullParser.getAttributeValue(null, "time")), Float.parseFloat(newPullParser.getAttributeValue(null, "weight"))));
                    }
                }
            } catch (XmlPullParserException e3) {
                Log.e(f352a, "Error reading historical recrod file: " + this.c, e3);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e4) {
                    }
                }
            } catch (IOException e5) {
                Log.e(f352a, "Error reading historical recrod file: " + this.c, e5);
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e6) {
                    }
                }
            } catch (Throwable th) {
                if (openFileInput != null) {
                    try {
                        openFileInput.close();
                    } catch (IOException e7) {
                    }
                }
                throw th;
            }
        } catch (FileNotFoundException e8) {
        }
    }

    /* compiled from: ActivityChooserModel */
    private final class f extends AsyncTask<Object, Void, Void> {
        f() {
        }

        /* renamed from: a */
        public Void doInBackground(Object... objArr) {
            List list = (List) objArr[0];
            String str = (String) objArr[1];
            try {
                FileOutputStream openFileOutput = d.this.f353b.openFileOutput(str, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag(null, "historical-records");
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        C0006d dVar = (C0006d) list.remove(0);
                        newSerializer.startTag(null, "historical-record");
                        newSerializer.attribute(null, PushConstants.INTENT_ACTIVITY_NAME, dVar.f358a.flattenToString());
                        newSerializer.attribute(null, "time", String.valueOf(dVar.f359b));
                        newSerializer.attribute(null, "weight", String.valueOf(dVar.c));
                        newSerializer.endTag(null, "historical-record");
                    }
                    newSerializer.endTag(null, "historical-records");
                    newSerializer.endDocument();
                    d.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e(d.f352a, "Error writing historical record file: " + d.this.c, e2);
                    d.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (IllegalStateException e4) {
                    Log.e(d.f352a, "Error writing historical record file: " + d.this.c, e4);
                    d.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (IOException e6) {
                    Log.e(d.f352a, "Error writing historical record file: " + d.this.c, e6);
                    d.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e7) {
                        }
                    }
                } catch (Throwable th) {
                    d.this.d = true;
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e9) {
                Log.e(d.f352a, "Error writing historical record file: " + str, e9);
            }
            return null;
        }
    }
}
