package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.b.a.b;
import android.util.AttributeSet;

/* compiled from: TintTypedArray */
public class ac {

    /* renamed from: a  reason: collision with root package name */
    private final Context f332a;

    /* renamed from: b  reason: collision with root package name */
    private final TypedArray f333b;

    public static ac a(Context context, AttributeSet attributeSet, int[] iArr) {
        return new ac(context, context.obtainStyledAttributes(attributeSet, iArr));
    }

    public static ac a(Context context, AttributeSet attributeSet, int[] iArr, int i, int i2) {
        return new ac(context, context.obtainStyledAttributes(attributeSet, iArr, i, i2));
    }

    public static ac a(Context context, int i, int[] iArr) {
        return new ac(context, context.obtainStyledAttributes(i, iArr));
    }

    private ac(Context context, TypedArray typedArray) {
        this.f332a = context;
        this.f333b = typedArray;
    }

    public Drawable a(int i) {
        int resourceId;
        if (!this.f333b.hasValue(i) || (resourceId = this.f333b.getResourceId(i, 0)) == 0) {
            return this.f333b.getDrawable(i);
        }
        return b.b(this.f332a, resourceId);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable
     arg types: [android.content.Context, int, int]
     candidates:
      android.support.v7.widget.g.a(android.content.res.ColorStateList, android.graphics.PorterDuff$Mode, int[]):android.graphics.PorterDuffColorFilter
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, int, android.graphics.PorterDuff$Mode):void
      android.support.v7.widget.g.a(android.graphics.drawable.Drawable, android.support.v7.widget.aa, int[]):void
      android.support.v7.widget.g.a(android.content.Context, int, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, long, android.graphics.drawable.Drawable):boolean
      android.support.v7.widget.g.a(android.content.Context, int, android.content.res.ColorStateList):android.content.res.ColorStateList
      android.support.v7.widget.g.a(android.content.Context, android.support.v7.widget.ae, int):android.graphics.drawable.Drawable
      android.support.v7.widget.g.a(android.content.Context, int, boolean):android.graphics.drawable.Drawable */
    public Drawable b(int i) {
        int resourceId;
        if (!this.f333b.hasValue(i) || (resourceId = this.f333b.getResourceId(i, 0)) == 0) {
            return null;
        }
        return g.a().a(this.f332a, resourceId, true);
    }

    public CharSequence c(int i) {
        return this.f333b.getText(i);
    }

    public String d(int i) {
        return this.f333b.getString(i);
    }

    public boolean a(int i, boolean z) {
        return this.f333b.getBoolean(i, z);
    }

    public int a(int i, int i2) {
        return this.f333b.getInt(i, i2);
    }

    public float a(int i, float f) {
        return this.f333b.getFloat(i, f);
    }

    public int b(int i, int i2) {
        return this.f333b.getColor(i, i2);
    }

    public ColorStateList e(int i) {
        int resourceId;
        ColorStateList a2;
        return (!this.f333b.hasValue(i) || (resourceId = this.f333b.getResourceId(i, 0)) == 0 || (a2 = b.a(this.f332a, resourceId)) == null) ? this.f333b.getColorStateList(i) : a2;
    }

    public int c(int i, int i2) {
        return this.f333b.getInteger(i, i2);
    }

    public int d(int i, int i2) {
        return this.f333b.getDimensionPixelOffset(i, i2);
    }

    public int e(int i, int i2) {
        return this.f333b.getDimensionPixelSize(i, i2);
    }

    public int f(int i, int i2) {
        return this.f333b.getLayoutDimension(i, i2);
    }

    public int g(int i, int i2) {
        return this.f333b.getResourceId(i, i2);
    }

    public CharSequence[] f(int i) {
        return this.f333b.getTextArray(i);
    }

    public boolean g(int i) {
        return this.f333b.hasValue(i);
    }

    public void a() {
        this.f333b.recycle();
    }
}
