package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.l;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;

/* compiled from: DecorToolbar */
public interface n {
    ViewPropertyAnimatorCompat a(int i, long j);

    ViewGroup a();

    void a(int i);

    void a(Drawable drawable);

    void a(l.a aVar, MenuBuilder.a aVar2);

    void a(ScrollingTabContainerView scrollingTabContainerView);

    void a(Menu menu, l.a aVar);

    void a(Window.Callback callback);

    void a(CharSequence charSequence);

    void a(boolean z);

    Context b();

    void b(int i);

    void b(boolean z);

    void c(int i);

    boolean c();

    void d();

    void d(int i);

    CharSequence e();

    void f();

    void g();

    boolean h();

    boolean i();

    boolean j();

    boolean k();

    boolean l();

    void m();

    void n();

    int o();

    int p();

    int q();

    Menu r();
}
