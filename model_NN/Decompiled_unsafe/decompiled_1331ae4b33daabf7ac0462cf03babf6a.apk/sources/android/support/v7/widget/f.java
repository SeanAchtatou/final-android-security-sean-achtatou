package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.a.a;
import android.support.v7.b.a.b;
import android.util.AttributeSet;
import android.widget.CompoundButton;

/* compiled from: AppCompatCompoundButtonHelper */
class f {

    /* renamed from: a  reason: collision with root package name */
    private final CompoundButton f364a;

    /* renamed from: b  reason: collision with root package name */
    private ColorStateList f365b = null;
    private PorterDuff.Mode c = null;
    private boolean d = false;
    private boolean e = false;
    private boolean f;

    f(CompoundButton compoundButton) {
        this.f364a = compoundButton;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        int resourceId;
        TypedArray obtainStyledAttributes = this.f364a.getContext().obtainStyledAttributes(attributeSet, a.k.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_android_button) && (resourceId = obtainStyledAttributes.getResourceId(a.k.CompoundButton_android_button, 0)) != 0) {
                this.f364a.setButtonDrawable(b.b(this.f364a.getContext(), resourceId));
            }
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_buttonTint)) {
                CompoundButtonCompat.setButtonTintList(this.f364a, obtainStyledAttributes.getColorStateList(a.k.CompoundButton_buttonTint));
            }
            if (obtainStyledAttributes.hasValue(a.k.CompoundButton_buttonTintMode)) {
                CompoundButtonCompat.setButtonTintMode(this.f364a, o.a(obtainStyledAttributes.getInt(a.k.CompoundButton_buttonTintMode, -1), null));
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        this.f365b = colorStateList;
        this.d = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public ColorStateList a() {
        return this.f365b;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        d();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Drawable buttonDrawable = CompoundButtonCompat.getButtonDrawable(this.f364a);
        if (buttonDrawable == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = DrawableCompat.wrap(buttonDrawable).mutate();
            if (this.d) {
                DrawableCompat.setTintList(mutate, this.f365b);
            }
            if (this.e) {
                DrawableCompat.setTintMode(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.f364a.getDrawableState());
            }
            this.f364a.setButtonDrawable(mutate);
        }
    }

    /* access modifiers changed from: package-private */
    public int a(int i) {
        Drawable buttonDrawable;
        if (Build.VERSION.SDK_INT >= 17 || (buttonDrawable = CompoundButtonCompat.getButtonDrawable(this.f364a)) == null) {
            return i;
        }
        return i + buttonDrawable.getIntrinsicWidth();
    }
}
