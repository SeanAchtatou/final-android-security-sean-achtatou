package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.EditText;

public class AppCompatEditText extends EditText implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    private e f227a;

    /* renamed from: b  reason: collision with root package name */
    private k f228b;

    public AppCompatEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.editTextStyle);
    }

    public AppCompatEditText(Context context, AttributeSet attributeSet, int i) {
        super(z.a(context), attributeSet, i);
        this.f227a = new e(this);
        this.f227a.a(attributeSet, i);
        this.f228b = k.a(this);
        this.f228b.a(attributeSet, i);
        this.f228b.a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f227a != null) {
            this.f227a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f227a != null) {
            this.f227a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f227a != null) {
            this.f227a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f227a != null) {
            return this.f227a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f227a != null) {
            this.f227a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f227a != null) {
            return this.f227a.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f227a != null) {
            this.f227a.c();
        }
        if (this.f228b != null) {
            this.f228b.a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f228b != null) {
            this.f228b.a(context, i);
        }
    }
}
