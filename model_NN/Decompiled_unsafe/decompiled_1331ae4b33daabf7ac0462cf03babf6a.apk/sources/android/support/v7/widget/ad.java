package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v7.a.a;
import android.support.v7.b.a.b;
import android.support.v7.view.menu.ActionMenuItem;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.l;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* compiled from: ToolbarWidgetWrapper */
public class ad implements n {

    /* renamed from: a  reason: collision with root package name */
    Toolbar f334a;

    /* renamed from: b  reason: collision with root package name */
    CharSequence f335b;
    Window.Callback c;
    boolean d;
    private int e;
    private View f;
    private View g;
    private Drawable h;
    private Drawable i;
    private Drawable j;
    private boolean k;
    private CharSequence l;
    private CharSequence m;
    private c n;
    private int o;
    private int p;
    private Drawable q;

    public ad(Toolbar toolbar, boolean z) {
        this(toolbar, z, a.i.abc_action_bar_up_description, a.e.abc_ic_ab_back_material);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.v7.widget.Toolbar, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ad(Toolbar toolbar, boolean z, int i2, int i3) {
        this.o = 0;
        this.p = 0;
        this.f334a = toolbar;
        this.f335b = toolbar.getTitle();
        this.l = toolbar.getSubtitle();
        this.k = this.f335b != null;
        this.j = toolbar.getNavigationIcon();
        ac a2 = ac.a(toolbar.getContext(), null, a.k.ActionBar, a.C0002a.actionBarStyle, 0);
        this.q = a2.a(a.k.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence c2 = a2.c(a.k.ActionBar_title);
            if (!TextUtils.isEmpty(c2)) {
                b(c2);
            }
            CharSequence c3 = a2.c(a.k.ActionBar_subtitle);
            if (!TextUtils.isEmpty(c3)) {
                c(c3);
            }
            Drawable a3 = a2.a(a.k.ActionBar_logo);
            if (a3 != null) {
                b(a3);
            }
            Drawable a4 = a2.a(a.k.ActionBar_icon);
            if (a4 != null) {
                a(a4);
            }
            if (this.j == null && this.q != null) {
                c(this.q);
            }
            c(a2.a(a.k.ActionBar_displayOptions, 0));
            int g2 = a2.g(a.k.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.f334a.getContext()).inflate(g2, (ViewGroup) this.f334a, false));
                c(this.e | 16);
            }
            int f2 = a2.f(a.k.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.f334a.getLayoutParams();
                layoutParams.height = f2;
                this.f334a.setLayoutParams(layoutParams);
            }
            int d2 = a2.d(a.k.ActionBar_contentInsetStart, -1);
            int d3 = a2.d(a.k.ActionBar_contentInsetEnd, -1);
            if (d2 >= 0 || d3 >= 0) {
                this.f334a.a(Math.max(d2, 0), Math.max(d3, 0));
            }
            int g3 = a2.g(a.k.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                this.f334a.a(this.f334a.getContext(), g3);
            }
            int g4 = a2.g(a.k.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                this.f334a.b(this.f334a.getContext(), g4);
            }
            int g5 = a2.g(a.k.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.f334a.setPopupTheme(g5);
            }
        } else {
            this.e = s();
        }
        a2.a();
        e(i2);
        this.m = this.f334a.getNavigationContentDescription();
        this.f334a.setNavigationOnClickListener(new View.OnClickListener() {

            /* renamed from: a  reason: collision with root package name */
            final ActionMenuItem f336a = new ActionMenuItem(ad.this.f334a.getContext(), 0, 16908332, 0, 0, ad.this.f335b);

            public void onClick(View view) {
                if (ad.this.c != null && ad.this.d) {
                    ad.this.c.onMenuItemSelected(0, this.f336a);
                }
            }
        });
    }

    public void e(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.f334a.getNavigationContentDescription())) {
                f(this.p);
            }
        }
    }

    private int s() {
        if (this.f334a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.f334a.getNavigationIcon();
        return 15;
    }

    public ViewGroup a() {
        return this.f334a;
    }

    public Context b() {
        return this.f334a.getContext();
    }

    public boolean c() {
        return this.f334a.g();
    }

    public void d() {
        this.f334a.h();
    }

    public void a(Window.Callback callback) {
        this.c = callback;
    }

    public void a(CharSequence charSequence) {
        if (!this.k) {
            e(charSequence);
        }
    }

    public CharSequence e() {
        return this.f334a.getTitle();
    }

    public void b(CharSequence charSequence) {
        this.k = true;
        e(charSequence);
    }

    private void e(CharSequence charSequence) {
        this.f335b = charSequence;
        if ((this.e & 8) != 0) {
            this.f334a.setTitle(charSequence);
        }
    }

    public void c(CharSequence charSequence) {
        this.l = charSequence;
        if ((this.e & 8) != 0) {
            this.f334a.setSubtitle(charSequence);
        }
    }

    public void f() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void g() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    public void a(int i2) {
        a(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void a(Drawable drawable) {
        this.h = drawable;
        t();
    }

    public void b(int i2) {
        b(i2 != 0 ? b.b(b(), i2) : null);
    }

    public void b(Drawable drawable) {
        this.i = drawable;
        t();
    }

    private void t() {
        Drawable drawable = null;
        if ((this.e & 2) != 0) {
            if ((this.e & 1) != 0) {
                drawable = this.i != null ? this.i : this.h;
            } else {
                drawable = this.h;
            }
        }
        this.f334a.setLogo(drawable);
    }

    public boolean h() {
        return this.f334a.a();
    }

    public boolean i() {
        return this.f334a.b();
    }

    public boolean j() {
        return this.f334a.c();
    }

    public boolean k() {
        return this.f334a.d();
    }

    public boolean l() {
        return this.f334a.e();
    }

    public void m() {
        this.d = true;
    }

    public void a(Menu menu, l.a aVar) {
        if (this.n == null) {
            this.n = new c(this.f334a.getContext());
            this.n.a(a.f.action_menu_presenter);
        }
        this.n.a(aVar);
        this.f334a.a((MenuBuilder) menu, this.n);
    }

    public void n() {
        this.f334a.f();
    }

    public int o() {
        return this.e;
    }

    public void c(int i2) {
        int i3 = this.e ^ i2;
        this.e = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    v();
                }
                u();
            }
            if ((i3 & 3) != 0) {
                t();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.f334a.setTitle(this.f335b);
                    this.f334a.setSubtitle(this.l);
                } else {
                    this.f334a.setTitle((CharSequence) null);
                    this.f334a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && this.g != null) {
                if ((i2 & 16) != 0) {
                    this.f334a.addView(this.g);
                } else {
                    this.f334a.removeView(this.g);
                }
            }
        }
    }

    public void a(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.f != null && this.f.getParent() == this.f334a) {
            this.f334a.removeView(this.f);
        }
        this.f = scrollingTabContainerView;
        if (scrollingTabContainerView != null && this.o == 2) {
            this.f334a.addView(this.f, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.f.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.f35a = 8388691;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void a(boolean z) {
        this.f334a.setCollapsible(z);
    }

    public void b(boolean z) {
    }

    public int p() {
        return this.o;
    }

    public void a(View view) {
        if (!(this.g == null || (this.e & 16) == 0)) {
            this.f334a.removeView(this.g);
        }
        this.g = view;
        if (view != null && (this.e & 16) != 0) {
            this.f334a.addView(this.g);
        }
    }

    public ViewPropertyAnimatorCompat a(final int i2, long j2) {
        return ViewCompat.animate(this.f334a).alpha(i2 == 0 ? 1.0f : 0.0f).setDuration(j2).setListener(new ViewPropertyAnimatorListenerAdapter() {
            private boolean c = false;

            public void onAnimationStart(View view) {
                ad.this.f334a.setVisibility(0);
            }

            public void onAnimationEnd(View view) {
                if (!this.c) {
                    ad.this.f334a.setVisibility(i2);
                }
            }

            public void onAnimationCancel(View view) {
                this.c = true;
            }
        });
    }

    public void c(Drawable drawable) {
        this.j = drawable;
        u();
    }

    private void u() {
        if ((this.e & 4) != 0) {
            this.f334a.setNavigationIcon(this.j != null ? this.j : this.q);
        } else {
            this.f334a.setNavigationIcon((Drawable) null);
        }
    }

    public void d(CharSequence charSequence) {
        this.m = charSequence;
        v();
    }

    public void f(int i2) {
        d(i2 == 0 ? null : b().getString(i2));
    }

    private void v() {
        if ((this.e & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.m)) {
            this.f334a.setNavigationContentDescription(this.p);
        } else {
            this.f334a.setNavigationContentDescription(this.m);
        }
    }

    public void d(int i2) {
        this.f334a.setVisibility(i2);
    }

    public int q() {
        return this.f334a.getVisibility();
    }

    public void a(l.a aVar, MenuBuilder.a aVar2) {
        this.f334a.a(aVar, aVar2);
    }

    public Menu r() {
        return this.f334a.getMenu();
    }
}
