package android.support.v7.widget;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.v7.a.a;
import android.support.v7.b.a.b;
import android.util.AttributeSet;
import android.widget.ImageView;

/* compiled from: AppCompatImageHelper */
public class h {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f368a;

    public h(ImageView imageView) {
        this.f368a = imageView;
    }

    public void a(AttributeSet attributeSet, int i) {
        int g;
        ac acVar = null;
        try {
            Drawable drawable = this.f368a.getDrawable();
            if (!(drawable != null || (g = (acVar = ac.a(this.f368a.getContext(), attributeSet, a.k.AppCompatImageView, i, 0)).g(a.k.AppCompatImageView_srcCompat, -1)) == -1 || (drawable = b.b(this.f368a.getContext(), g)) == null)) {
                this.f368a.setImageDrawable(drawable);
            }
            if (drawable != null) {
                o.b(drawable);
            }
        } finally {
            if (acVar != null) {
                acVar.a();
            }
        }
    }

    public void a(int i) {
        if (i != 0) {
            Drawable b2 = b.b(this.f368a.getContext(), i);
            if (b2 != null) {
                o.b(b2);
            }
            this.f368a.setImageDrawable(b2);
            return;
        }
        this.f368a.setImageDrawable(null);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        Drawable background = this.f368a.getBackground();
        if (Build.VERSION.SDK_INT < 21 || !(background instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }
}
