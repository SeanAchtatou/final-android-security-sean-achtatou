package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.SeekBar;

/* compiled from: AppCompatSeekBarHelper */
class j extends i {

    /* renamed from: a  reason: collision with root package name */
    private final SeekBar f371a;

    /* renamed from: b  reason: collision with root package name */
    private Drawable f372b;
    private ColorStateList c = null;
    private PorterDuff.Mode d = null;
    private boolean e = false;
    private boolean f = false;

    j(SeekBar seekBar) {
        super(seekBar);
        this.f371a = seekBar;
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        ac a2 = ac.a(this.f371a.getContext(), attributeSet, a.k.AppCompatSeekBar, i, 0);
        Drawable b2 = a2.b(a.k.AppCompatSeekBar_android_thumb);
        if (b2 != null) {
            this.f371a.setThumb(b2);
        }
        a(a2.a(a.k.AppCompatSeekBar_tickMark));
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTintMode)) {
            this.d = o.a(a2.a(a.k.AppCompatSeekBar_tickMarkTintMode, -1), this.d);
            this.f = true;
        }
        if (a2.g(a.k.AppCompatSeekBar_tickMarkTint)) {
            this.c = a2.e(a.k.AppCompatSeekBar_tickMarkTint);
            this.e = true;
        }
        a2.a();
        d();
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        if (this.f372b != null) {
            this.f372b.setCallback(null);
        }
        this.f372b = drawable;
        if (drawable != null) {
            drawable.setCallback(this.f371a);
            DrawableCompat.setLayoutDirection(drawable, ViewCompat.getLayoutDirection(this.f371a));
            if (drawable.isStateful()) {
                drawable.setState(this.f371a.getDrawableState());
            }
            d();
        }
        this.f371a.invalidate();
    }

    private void d() {
        if (this.f372b == null) {
            return;
        }
        if (this.e || this.f) {
            this.f372b = DrawableCompat.wrap(this.f372b.mutate());
            if (this.e) {
                DrawableCompat.setTintList(this.f372b, this.c);
            }
            if (this.f) {
                DrawableCompat.setTintMode(this.f372b, this.d);
            }
            if (this.f372b.isStateful()) {
                this.f372b.setState(this.f371a.getDrawableState());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.f372b != null) {
            this.f372b.jumpToCurrentState();
        }
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable drawable = this.f372b;
        if (drawable != null && drawable.isStateful() && drawable.setState(this.f371a.getDrawableState())) {
            this.f371a.invalidateDrawable(drawable);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Canvas canvas) {
        int max;
        int i = 1;
        if (this.f372b != null && (max = this.f371a.getMax()) > 1) {
            int intrinsicWidth = this.f372b.getIntrinsicWidth();
            int intrinsicHeight = this.f372b.getIntrinsicHeight();
            int i2 = intrinsicWidth >= 0 ? intrinsicWidth / 2 : 1;
            if (intrinsicHeight >= 0) {
                i = intrinsicHeight / 2;
            }
            this.f372b.setBounds(-i2, -i, i2, i);
            float width = ((float) ((this.f371a.getWidth() - this.f371a.getPaddingLeft()) - this.f371a.getPaddingRight())) / ((float) max);
            int save = canvas.save();
            canvas.translate((float) this.f371a.getPaddingLeft(), (float) (this.f371a.getHeight() / 2));
            for (int i3 = 0; i3 <= max; i3++) {
                this.f372b.draw(canvas);
                canvas.translate(width, 0.0f);
            }
            canvas.restoreToCount(save);
        }
    }
}
