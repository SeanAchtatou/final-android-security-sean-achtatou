package android.support.v7.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;

/* compiled from: TintResources */
class ab extends u {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<Context> f331a;

    public ab(Context context, Resources resources) {
        super(resources);
        this.f331a = new WeakReference<>(context);
    }

    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = this.f331a.get();
        if (!(drawable == null || context == null)) {
            g.a();
            g.a(context, i, drawable);
        }
        return drawable;
    }
}
