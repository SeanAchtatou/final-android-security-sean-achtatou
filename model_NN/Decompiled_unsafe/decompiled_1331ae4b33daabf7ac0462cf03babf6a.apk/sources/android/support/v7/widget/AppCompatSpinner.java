package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.DataSetObserver;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.TintableBackgroundView;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.ThemedSpinnerAdapter;

public class AppCompatSpinner extends Spinner implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    static final boolean f242a;
    private static final boolean e;
    private static final int[] f = {16843505};

    /* renamed from: b  reason: collision with root package name */
    b f243b;
    int c;
    final Rect d;
    private e g;
    private Context h;
    private r i;
    private SpinnerAdapter j;
    private boolean k;

    static {
        boolean z;
        boolean z2;
        if (Build.VERSION.SDK_INT >= 23) {
            z = true;
        } else {
            z = false;
        }
        f242a = z;
        if (Build.VERSION.SDK_INT >= 16) {
            z2 = true;
        } else {
            z2 = false;
        }
        e = z2;
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.spinnerStyle);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, -1);
    }

    public AppCompatSpinner(Context context, AttributeSet attributeSet, int i2, int i3) {
        this(context, attributeSet, i2, i3, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatSpinner(android.content.Context r9, android.util.AttributeSet r10, int r11, int r12, android.content.res.Resources.Theme r13) {
        /*
            r8 = this;
            r1 = 0
            r3 = 1
            r7 = 0
            r8.<init>(r9, r10, r11)
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>()
            r8.d = r0
            int[] r0 = android.support.v7.a.a.k.Spinner
            android.support.v7.widget.ac r4 = android.support.v7.widget.ac.a(r9, r10, r0, r11, r7)
            android.support.v7.widget.e r0 = new android.support.v7.widget.e
            r0.<init>(r8)
            r8.g = r0
            if (r13 == 0) goto L_0x00af
            android.support.v7.view.d r0 = new android.support.v7.view.d
            r0.<init>(r9, r13)
            r8.h = r0
        L_0x0023:
            android.content.Context r0 = r8.h
            if (r0 == 0) goto L_0x0081
            r0 = -1
            if (r12 != r0) goto L_0x0049
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 11
            if (r0 < r2) goto L_0x00e3
            int[] r0 = android.support.v7.widget.AppCompatSpinner.f     // Catch:{ Exception -> 0x00cb, all -> 0x00db }
            r2 = 0
            android.content.res.TypedArray r2 = r9.obtainStyledAttributes(r10, r0, r11, r2)     // Catch:{ Exception -> 0x00cb, all -> 0x00db }
            r0 = 0
            boolean r0 = r2.hasValue(r0)     // Catch:{ Exception -> 0x00e8 }
            if (r0 == 0) goto L_0x0044
            r0 = 0
            r5 = 0
            int r12 = r2.getInt(r0, r5)     // Catch:{ Exception -> 0x00e8 }
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            r2.recycle()
        L_0x0049:
            if (r12 != r3) goto L_0x0081
            android.support.v7.widget.AppCompatSpinner$b r0 = new android.support.v7.widget.AppCompatSpinner$b
            android.content.Context r2 = r8.h
            r0.<init>(r2, r10, r11)
            android.content.Context r2 = r8.h
            int[] r5 = android.support.v7.a.a.k.Spinner
            android.support.v7.widget.ac r2 = android.support.v7.widget.ac.a(r2, r10, r5, r11, r7)
            int r5 = android.support.v7.a.a.k.Spinner_android_dropDownWidth
            r6 = -2
            int r5 = r2.f(r5, r6)
            r8.c = r5
            int r5 = android.support.v7.a.a.k.Spinner_android_popupBackground
            android.graphics.drawable.Drawable r5 = r2.a(r5)
            r0.a(r5)
            int r5 = android.support.v7.a.a.k.Spinner_android_prompt
            java.lang.String r5 = r4.d(r5)
            r0.a(r5)
            r2.a()
            r8.f243b = r0
            android.support.v7.widget.AppCompatSpinner$1 r2 = new android.support.v7.widget.AppCompatSpinner$1
            r2.<init>(r8, r0)
            r8.i = r2
        L_0x0081:
            int r0 = android.support.v7.a.a.k.Spinner_android_entries
            java.lang.CharSequence[] r0 = r4.f(r0)
            if (r0 == 0) goto L_0x0099
            android.widget.ArrayAdapter r2 = new android.widget.ArrayAdapter
            r5 = 17367048(0x1090008, float:2.5162948E-38)
            r2.<init>(r9, r5, r0)
            int r0 = android.support.v7.a.a.h.support_simple_spinner_dropdown_item
            r2.setDropDownViewResource(r0)
            r8.setAdapter(r2)
        L_0x0099:
            r4.a()
            r8.k = r3
            android.widget.SpinnerAdapter r0 = r8.j
            if (r0 == 0) goto L_0x00a9
            android.widget.SpinnerAdapter r0 = r8.j
            r8.setAdapter(r0)
            r8.j = r1
        L_0x00a9:
            android.support.v7.widget.e r0 = r8.g
            r0.a(r10, r11)
            return
        L_0x00af:
            int r0 = android.support.v7.a.a.k.Spinner_popupTheme
            int r0 = r4.g(r0, r7)
            if (r0 == 0) goto L_0x00c0
            android.support.v7.view.d r2 = new android.support.v7.view.d
            r2.<init>(r9, r0)
            r8.h = r2
            goto L_0x0023
        L_0x00c0:
            boolean r0 = android.support.v7.widget.AppCompatSpinner.f242a
            if (r0 != 0) goto L_0x00c9
            r0 = r9
        L_0x00c5:
            r8.h = r0
            goto L_0x0023
        L_0x00c9:
            r0 = r1
            goto L_0x00c5
        L_0x00cb:
            r0 = move-exception
            r2 = r1
        L_0x00cd:
            java.lang.String r5 = "AppCompatSpinner"
            java.lang.String r6 = "Could not read android:spinnerMode"
            android.util.Log.i(r5, r6, r0)     // Catch:{ all -> 0x00e6 }
            if (r2 == 0) goto L_0x0049
            r2.recycle()
            goto L_0x0049
        L_0x00db:
            r0 = move-exception
            r2 = r1
        L_0x00dd:
            if (r2 == 0) goto L_0x00e2
            r2.recycle()
        L_0x00e2:
            throw r0
        L_0x00e3:
            r12 = r3
            goto L_0x0049
        L_0x00e6:
            r0 = move-exception
            goto L_0x00dd
        L_0x00e8:
            r0 = move-exception
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatSpinner.<init>(android.content.Context, android.util.AttributeSet, int, int, android.content.res.Resources$Theme):void");
    }

    public Context getPopupContext() {
        if (this.f243b != null) {
            return this.h;
        }
        if (f242a) {
            return super.getPopupContext();
        }
        return null;
    }

    public void setPopupBackgroundDrawable(Drawable drawable) {
        if (this.f243b != null) {
            this.f243b.a(drawable);
        } else if (e) {
            super.setPopupBackgroundDrawable(drawable);
        }
    }

    public void setPopupBackgroundResource(int i2) {
        setPopupBackgroundDrawable(android.support.v7.b.a.b.b(getPopupContext(), i2));
    }

    public Drawable getPopupBackground() {
        if (this.f243b != null) {
            return this.f243b.h();
        }
        if (e) {
            return super.getPopupBackground();
        }
        return null;
    }

    public void setDropDownVerticalOffset(int i2) {
        if (this.f243b != null) {
            this.f243b.d(i2);
        } else if (e) {
            super.setDropDownVerticalOffset(i2);
        }
    }

    public int getDropDownVerticalOffset() {
        if (this.f243b != null) {
            return this.f243b.k();
        }
        if (e) {
            return super.getDropDownVerticalOffset();
        }
        return 0;
    }

    public void setDropDownHorizontalOffset(int i2) {
        if (this.f243b != null) {
            this.f243b.c(i2);
        } else if (e) {
            super.setDropDownHorizontalOffset(i2);
        }
    }

    public int getDropDownHorizontalOffset() {
        if (this.f243b != null) {
            return this.f243b.j();
        }
        if (e) {
            return super.getDropDownHorizontalOffset();
        }
        return 0;
    }

    public void setDropDownWidth(int i2) {
        if (this.f243b != null) {
            this.c = i2;
        } else if (e) {
            super.setDropDownWidth(i2);
        }
    }

    public int getDropDownWidth() {
        if (this.f243b != null) {
            return this.c;
        }
        if (e) {
            return super.getDropDownWidth();
        }
        return 0;
    }

    public void setAdapter(SpinnerAdapter spinnerAdapter) {
        if (!this.k) {
            this.j = spinnerAdapter;
            return;
        }
        super.setAdapter(spinnerAdapter);
        if (this.f243b != null) {
            this.f243b.a(new a(spinnerAdapter, (this.h == null ? getContext() : this.h).getTheme()));
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.f243b != null && this.f243b.d()) {
            this.f243b.c();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.i == null || !this.i.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.f243b != null && View.MeasureSpec.getMode(i2) == Integer.MIN_VALUE) {
            setMeasuredDimension(Math.min(Math.max(getMeasuredWidth(), a(getAdapter(), getBackground())), View.MeasureSpec.getSize(i2)), getMeasuredHeight());
        }
    }

    public boolean performClick() {
        if (this.f243b == null) {
            return super.performClick();
        }
        if (!this.f243b.d()) {
            this.f243b.a();
        }
        return true;
    }

    public void setPrompt(CharSequence charSequence) {
        if (this.f243b != null) {
            this.f243b.a(charSequence);
        } else {
            super.setPrompt(charSequence);
        }
    }

    public CharSequence getPrompt() {
        return this.f243b != null ? this.f243b.b() : super.getPrompt();
    }

    public void setBackgroundResource(int i2) {
        super.setBackgroundResource(i2);
        if (this.g != null) {
            this.g.a(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.g != null) {
            this.g.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.g != null) {
            this.g.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.g != null) {
            return this.g.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.g != null) {
            this.g.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.g != null) {
            return this.g.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.g != null) {
            this.g.c();
        }
    }

    /* access modifiers changed from: package-private */
    public int a(SpinnerAdapter spinnerAdapter, Drawable drawable) {
        View view;
        if (spinnerAdapter == null) {
            return 0;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 0);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 0);
        int max = Math.max(0, getSelectedItemPosition());
        int min = Math.min(spinnerAdapter.getCount(), max + 15);
        int max2 = Math.max(0, max - (15 - (min - max)));
        View view2 = null;
        int i2 = 0;
        int i3 = 0;
        while (max2 < min) {
            int itemViewType = spinnerAdapter.getItemViewType(max2);
            if (itemViewType != i3) {
                view = null;
            } else {
                itemViewType = i3;
                view = view2;
            }
            view2 = spinnerAdapter.getView(max2, view, this);
            if (view2.getLayoutParams() == null) {
                view2.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            }
            view2.measure(makeMeasureSpec, makeMeasureSpec2);
            i2 = Math.max(i2, view2.getMeasuredWidth());
            max2++;
            i3 = itemViewType;
        }
        if (drawable == null) {
            return i2;
        }
        drawable.getPadding(this.d);
        return this.d.left + this.d.right + i2;
    }

    private static class a implements ListAdapter, SpinnerAdapter {

        /* renamed from: a  reason: collision with root package name */
        private SpinnerAdapter f246a;

        /* renamed from: b  reason: collision with root package name */
        private ListAdapter f247b;

        public a(SpinnerAdapter spinnerAdapter, Resources.Theme theme) {
            this.f246a = spinnerAdapter;
            if (spinnerAdapter instanceof ListAdapter) {
                this.f247b = (ListAdapter) spinnerAdapter;
            }
            if (theme == null) {
                return;
            }
            if (AppCompatSpinner.f242a && (spinnerAdapter instanceof ThemedSpinnerAdapter)) {
                ThemedSpinnerAdapter themedSpinnerAdapter = (ThemedSpinnerAdapter) spinnerAdapter;
                if (themedSpinnerAdapter.getDropDownViewTheme() != theme) {
                    themedSpinnerAdapter.setDropDownViewTheme(theme);
                }
            } else if (spinnerAdapter instanceof y) {
                y yVar = (y) spinnerAdapter;
                if (yVar.a() == null) {
                    yVar.a(theme);
                }
            }
        }

        public int getCount() {
            if (this.f246a == null) {
                return 0;
            }
            return this.f246a.getCount();
        }

        public Object getItem(int i) {
            if (this.f246a == null) {
                return null;
            }
            return this.f246a.getItem(i);
        }

        public long getItemId(int i) {
            if (this.f246a == null) {
                return -1;
            }
            return this.f246a.getItemId(i);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return getDropDownView(i, view, viewGroup);
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            if (this.f246a == null) {
                return null;
            }
            return this.f246a.getDropDownView(i, view, viewGroup);
        }

        public boolean hasStableIds() {
            return this.f246a != null && this.f246a.hasStableIds();
        }

        public void registerDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f246a != null) {
                this.f246a.registerDataSetObserver(dataSetObserver);
            }
        }

        public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
            if (this.f246a != null) {
                this.f246a.unregisterDataSetObserver(dataSetObserver);
            }
        }

        public boolean areAllItemsEnabled() {
            ListAdapter listAdapter = this.f247b;
            if (listAdapter != null) {
                return listAdapter.areAllItemsEnabled();
            }
            return true;
        }

        public boolean isEnabled(int i) {
            ListAdapter listAdapter = this.f247b;
            if (listAdapter != null) {
                return listAdapter.isEnabled(i);
            }
            return true;
        }

        public int getItemViewType(int i) {
            return 0;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEmpty() {
            return getCount() == 0;
        }
    }

    private class b extends ListPopupWindow {

        /* renamed from: a  reason: collision with root package name */
        ListAdapter f248a;
        private CharSequence h;
        private final Rect i = new Rect();

        public b(Context context, AttributeSet attributeSet, int i2) {
            super(context, attributeSet, i2);
            b(AppCompatSpinner.this);
            a(true);
            a(0);
            a(new AdapterView.OnItemClickListener(AppCompatSpinner.this) {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    AppCompatSpinner.this.setSelection(i);
                    if (AppCompatSpinner.this.getOnItemClickListener() != null) {
                        AppCompatSpinner.this.performItemClick(view, i, b.this.f248a.getItemId(i));
                    }
                    b.this.c();
                }
            });
        }

        public void a(ListAdapter listAdapter) {
            super.a(listAdapter);
            this.f248a = listAdapter;
        }

        public CharSequence b() {
            return this.h;
        }

        public void a(CharSequence charSequence) {
            this.h = charSequence;
        }

        /* access modifiers changed from: package-private */
        public void f() {
            int i2;
            int i3;
            Drawable h2 = h();
            if (h2 != null) {
                h2.getPadding(AppCompatSpinner.this.d);
                i2 = af.a(AppCompatSpinner.this) ? AppCompatSpinner.this.d.right : -AppCompatSpinner.this.d.left;
            } else {
                Rect rect = AppCompatSpinner.this.d;
                AppCompatSpinner.this.d.right = 0;
                rect.left = 0;
                i2 = 0;
            }
            int paddingLeft = AppCompatSpinner.this.getPaddingLeft();
            int paddingRight = AppCompatSpinner.this.getPaddingRight();
            int width = AppCompatSpinner.this.getWidth();
            if (AppCompatSpinner.this.c == -2) {
                int a2 = AppCompatSpinner.this.a((SpinnerAdapter) this.f248a, h());
                int i4 = (AppCompatSpinner.this.getContext().getResources().getDisplayMetrics().widthPixels - AppCompatSpinner.this.d.left) - AppCompatSpinner.this.d.right;
                if (a2 <= i4) {
                    i4 = a2;
                }
                g(Math.max(i4, (width - paddingLeft) - paddingRight));
            } else if (AppCompatSpinner.this.c == -1) {
                g((width - paddingLeft) - paddingRight);
            } else {
                g(AppCompatSpinner.this.c);
            }
            if (af.a(AppCompatSpinner.this)) {
                i3 = ((width - paddingRight) - l()) + i2;
            } else {
                i3 = i2 + paddingLeft;
            }
            c(i3);
        }

        public void a() {
            ViewTreeObserver viewTreeObserver;
            boolean d = d();
            f();
            h(2);
            super.a();
            e().setChoiceMode(1);
            i(AppCompatSpinner.this.getSelectedItemPosition());
            if (!d && (viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver()) != null) {
                final AnonymousClass2 r1 = new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        if (!b.this.a(AppCompatSpinner.this)) {
                            b.this.c();
                            return;
                        }
                        b.this.f();
                        b.super.a();
                    }
                };
                viewTreeObserver.addOnGlobalLayoutListener(r1);
                a(new PopupWindow.OnDismissListener() {
                    public void onDismiss() {
                        ViewTreeObserver viewTreeObserver = AppCompatSpinner.this.getViewTreeObserver();
                        if (viewTreeObserver != null) {
                            viewTreeObserver.removeGlobalOnLayoutListener(r1);
                        }
                    }
                });
            }
        }

        /* access modifiers changed from: package-private */
        public boolean a(View view) {
            return ViewCompat.isAttachedToWindow(view) && view.getGlobalVisibleRect(this.i);
        }
    }
}
