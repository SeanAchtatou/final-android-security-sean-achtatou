package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;

public class AppCompatButton extends Button implements TintableBackgroundView {

    /* renamed from: a  reason: collision with root package name */
    private final e f222a;

    /* renamed from: b  reason: collision with root package name */
    private final k f223b;

    public AppCompatButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.buttonStyle);
    }

    public AppCompatButton(Context context, AttributeSet attributeSet, int i) {
        super(z.a(context), attributeSet, i);
        this.f222a = new e(this);
        this.f222a.a(attributeSet, i);
        this.f223b = k.a(this);
        this.f223b.a(attributeSet, i);
        this.f223b.a();
    }

    public void setBackgroundResource(int i) {
        super.setBackgroundResource(i);
        if (this.f222a != null) {
            this.f222a.a(i);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
        if (this.f222a != null) {
            this.f222a.a(drawable);
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (this.f222a != null) {
            this.f222a.a(colorStateList);
        }
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (this.f222a != null) {
            return this.f222a.a();
        }
        return null;
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (this.f222a != null) {
            this.f222a.a(mode);
        }
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (this.f222a != null) {
            return this.f222a.b();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.f222a != null) {
            this.f222a.c();
        }
        if (this.f223b != null) {
            this.f223b.a();
        }
    }

    public void setTextAppearance(Context context, int i) {
        super.setTextAppearance(context, i);
        if (this.f223b != null) {
            this.f223b.a(context, i);
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(Button.class.getName());
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(Button.class.getName());
    }

    public void setSupportAllCaps(boolean z) {
        if (this.f223b != null) {
            this.f223b.a(z);
        }
    }
}
