package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;

/* compiled from: TintInfo */
class aa {

    /* renamed from: b  reason: collision with root package name */
    public ColorStateList f330b;
    public PorterDuff.Mode c;
    public boolean d;
    public boolean e;

    aa() {
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f330b = null;
        this.e = false;
        this.c = null;
        this.d = false;
    }
}
