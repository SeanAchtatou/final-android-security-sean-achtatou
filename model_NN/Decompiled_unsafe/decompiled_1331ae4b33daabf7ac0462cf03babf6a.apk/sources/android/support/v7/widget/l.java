package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.widget.TextView;

/* compiled from: AppCompatTextHelperV17 */
class l extends k {

    /* renamed from: b  reason: collision with root package name */
    private aa f375b;
    private aa c;

    l(TextView textView) {
        super(textView);
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        super.a(attributeSet, i);
        Context context = this.f373a.getContext();
        g a2 = g.a();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.AppCompatTextHelper, i, 0);
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTextHelper_android_drawableStart)) {
            this.f375b = a(context, a2, obtainStyledAttributes.getResourceId(a.k.AppCompatTextHelper_android_drawableStart, 0));
        }
        if (obtainStyledAttributes.hasValue(a.k.AppCompatTextHelper_android_drawableEnd)) {
            this.c = a(context, a2, obtainStyledAttributes.getResourceId(a.k.AppCompatTextHelper_android_drawableEnd, 0));
        }
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: package-private */
    public void a() {
        super.a();
        if (this.f375b != null || this.c != null) {
            Drawable[] compoundDrawablesRelative = this.f373a.getCompoundDrawablesRelative();
            a(compoundDrawablesRelative[0], this.f375b);
            a(compoundDrawablesRelative[2], this.c);
        }
    }
}
