package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.AbsSavedState;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.view.c;
import android.support.v7.view.g;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.SubMenuBuilder;
import android.support.v7.view.menu.l;
import android.support.v7.widget.ActionMenuView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import org.apache.mina.proxy.handlers.http.ntlm.NTLMConstants;

public class Toolbar extends ViewGroup {
    private int A;
    private int B;
    private boolean C;
    private boolean D;
    private final ArrayList<View> E;
    private final ArrayList<View> F;
    private final int[] G;
    private final ActionMenuView.d H;
    private ad I;
    private c J;
    private a K;
    private l.a L;
    private MenuBuilder.a M;
    private boolean N;
    private final Runnable O;

    /* renamed from: a  reason: collision with root package name */
    ImageButton f317a;

    /* renamed from: b  reason: collision with root package name */
    View f318b;
    int c;
    b d;
    private ActionMenuView e;
    private TextView f;
    private TextView g;
    private ImageButton h;
    private ImageView i;
    private Drawable j;
    private CharSequence k;
    private Context l;
    private int m;
    private int n;
    private int o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;
    private v u;
    private int v;
    private int w;
    private int x;
    private CharSequence y;
    private CharSequence z;

    public interface b {
        boolean a(MenuItem menuItem);
    }

    public Toolbar(Context context) {
        this(context, null);
    }

    public Toolbar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.toolbarStyle);
    }

    public Toolbar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.x = 8388627;
        this.E = new ArrayList<>();
        this.F = new ArrayList<>();
        this.G = new int[2];
        this.H = new ActionMenuView.d() {
            public boolean a(MenuItem menuItem) {
                if (Toolbar.this.d != null) {
                    return Toolbar.this.d.a(menuItem);
                }
                return false;
            }
        };
        this.O = new Runnable() {
            public void run() {
                Toolbar.this.d();
            }
        };
        ac a2 = ac.a(getContext(), attributeSet, a.k.Toolbar, i2, 0);
        this.n = a2.g(a.k.Toolbar_titleTextAppearance, 0);
        this.o = a2.g(a.k.Toolbar_subtitleTextAppearance, 0);
        this.x = a2.c(a.k.Toolbar_android_gravity, this.x);
        this.c = a2.c(a.k.Toolbar_buttonGravity, 48);
        int d2 = a2.d(a.k.Toolbar_titleMargin, 0);
        d2 = a2.g(a.k.Toolbar_titleMargins) ? a2.d(a.k.Toolbar_titleMargins, d2) : d2;
        this.t = d2;
        this.s = d2;
        this.r = d2;
        this.q = d2;
        int d3 = a2.d(a.k.Toolbar_titleMarginStart, -1);
        if (d3 >= 0) {
            this.q = d3;
        }
        int d4 = a2.d(a.k.Toolbar_titleMarginEnd, -1);
        if (d4 >= 0) {
            this.r = d4;
        }
        int d5 = a2.d(a.k.Toolbar_titleMarginTop, -1);
        if (d5 >= 0) {
            this.s = d5;
        }
        int d6 = a2.d(a.k.Toolbar_titleMarginBottom, -1);
        if (d6 >= 0) {
            this.t = d6;
        }
        this.p = a2.e(a.k.Toolbar_maxButtonHeight, -1);
        int d7 = a2.d(a.k.Toolbar_contentInsetStart, Integer.MIN_VALUE);
        int d8 = a2.d(a.k.Toolbar_contentInsetEnd, Integer.MIN_VALUE);
        int e2 = a2.e(a.k.Toolbar_contentInsetLeft, 0);
        int e3 = a2.e(a.k.Toolbar_contentInsetRight, 0);
        s();
        this.u.b(e2, e3);
        if (!(d7 == Integer.MIN_VALUE && d8 == Integer.MIN_VALUE)) {
            this.u.a(d7, d8);
        }
        this.v = a2.d(a.k.Toolbar_contentInsetStartWithNavigation, Integer.MIN_VALUE);
        this.w = a2.d(a.k.Toolbar_contentInsetEndWithActions, Integer.MIN_VALUE);
        this.j = a2.a(a.k.Toolbar_collapseIcon);
        this.k = a2.c(a.k.Toolbar_collapseContentDescription);
        CharSequence c2 = a2.c(a.k.Toolbar_title);
        if (!TextUtils.isEmpty(c2)) {
            setTitle(c2);
        }
        CharSequence c3 = a2.c(a.k.Toolbar_subtitle);
        if (!TextUtils.isEmpty(c3)) {
            setSubtitle(c3);
        }
        this.l = getContext();
        setPopupTheme(a2.g(a.k.Toolbar_popupTheme, 0));
        Drawable a3 = a2.a(a.k.Toolbar_navigationIcon);
        if (a3 != null) {
            setNavigationIcon(a3);
        }
        CharSequence c4 = a2.c(a.k.Toolbar_navigationContentDescription);
        if (!TextUtils.isEmpty(c4)) {
            setNavigationContentDescription(c4);
        }
        Drawable a4 = a2.a(a.k.Toolbar_logo);
        if (a4 != null) {
            setLogo(a4);
        }
        CharSequence c5 = a2.c(a.k.Toolbar_logoDescription);
        if (!TextUtils.isEmpty(c5)) {
            setLogoDescription(c5);
        }
        if (a2.g(a.k.Toolbar_titleTextColor)) {
            setTitleTextColor(a2.b(a.k.Toolbar_titleTextColor, -1));
        }
        if (a2.g(a.k.Toolbar_subtitleTextColor)) {
            setSubtitleTextColor(a2.b(a.k.Toolbar_subtitleTextColor, -1));
        }
        a2.a();
    }

    public void setPopupTheme(int i2) {
        if (this.m != i2) {
            this.m = i2;
            if (i2 == 0) {
                this.l = getContext();
            } else {
                this.l = new ContextThemeWrapper(getContext(), i2);
            }
        }
    }

    public int getPopupTheme() {
        return this.m;
    }

    public int getTitleMarginStart() {
        return this.q;
    }

    public void setTitleMarginStart(int i2) {
        this.q = i2;
        requestLayout();
    }

    public int getTitleMarginTop() {
        return this.s;
    }

    public void setTitleMarginTop(int i2) {
        this.s = i2;
        requestLayout();
    }

    public int getTitleMarginEnd() {
        return this.r;
    }

    public void setTitleMarginEnd(int i2) {
        this.r = i2;
        requestLayout();
    }

    public int getTitleMarginBottom() {
        return this.t;
    }

    public void setTitleMarginBottom(int i2) {
        this.t = i2;
        requestLayout();
    }

    public void onRtlPropertiesChanged(int i2) {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT >= 17) {
            super.onRtlPropertiesChanged(i2);
        }
        s();
        v vVar = this.u;
        if (i2 != 1) {
            z2 = false;
        }
        vVar.a(z2);
    }

    public void setLogo(int i2) {
        setLogo(android.support.v7.b.a.b.b(getContext(), i2));
    }

    public boolean a() {
        return getVisibility() == 0 && this.e != null && this.e.a();
    }

    public boolean b() {
        return this.e != null && this.e.g();
    }

    public boolean c() {
        return this.e != null && this.e.h();
    }

    public boolean d() {
        return this.e != null && this.e.e();
    }

    public boolean e() {
        return this.e != null && this.e.f();
    }

    public void a(MenuBuilder menuBuilder, c cVar) {
        if (menuBuilder != null || this.e != null) {
            o();
            MenuBuilder d2 = this.e.d();
            if (d2 != menuBuilder) {
                if (d2 != null) {
                    d2.b(this.J);
                    d2.b(this.K);
                }
                if (this.K == null) {
                    this.K = new a();
                }
                cVar.c(true);
                if (menuBuilder != null) {
                    menuBuilder.a(cVar, this.l);
                    menuBuilder.a(this.K, this.l);
                } else {
                    cVar.a(this.l, (MenuBuilder) null);
                    this.K.a(this.l, (MenuBuilder) null);
                    cVar.a(true);
                    this.K.a(true);
                }
                this.e.setPopupTheme(this.m);
                this.e.setPresenter(cVar);
                this.J = cVar;
            }
        }
    }

    public void f() {
        if (this.e != null) {
            this.e.i();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.ImageView, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.widget.c):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.l$a, android.support.v7.view.menu.MenuBuilder$a):void
      android.support.v7.widget.Toolbar.a(android.view.View, boolean):void */
    public void setLogo(Drawable drawable) {
        if (drawable != null) {
            m();
            if (!d(this.i)) {
                a((View) this.i, true);
            }
        } else if (this.i != null && d(this.i)) {
            removeView(this.i);
            this.F.remove(this.i);
        }
        if (this.i != null) {
            this.i.setImageDrawable(drawable);
        }
    }

    public Drawable getLogo() {
        if (this.i != null) {
            return this.i.getDrawable();
        }
        return null;
    }

    public void setLogoDescription(int i2) {
        setLogoDescription(getContext().getText(i2));
    }

    public void setLogoDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            m();
        }
        if (this.i != null) {
            this.i.setContentDescription(charSequence);
        }
    }

    public CharSequence getLogoDescription() {
        if (this.i != null) {
            return this.i.getContentDescription();
        }
        return null;
    }

    private void m() {
        if (this.i == null) {
            this.i = new AppCompatImageView(getContext());
        }
    }

    public boolean g() {
        return (this.K == null || this.K.f326b == null) ? false : true;
    }

    public void h() {
        MenuItemImpl menuItemImpl = this.K == null ? null : this.K.f326b;
        if (menuItemImpl != null) {
            menuItemImpl.collapseActionView();
        }
    }

    public CharSequence getTitle() {
        return this.y;
    }

    public void setTitle(int i2) {
        setTitle(getContext().getText(i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.widget.c):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.l$a, android.support.v7.view.menu.MenuBuilder$a):void
      android.support.v7.widget.Toolbar.a(android.view.View, boolean):void */
    public void setTitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.f == null) {
                Context context = getContext();
                this.f = new AppCompatTextView(context);
                this.f.setSingleLine();
                this.f.setEllipsize(TextUtils.TruncateAt.END);
                if (this.n != 0) {
                    this.f.setTextAppearance(context, this.n);
                }
                if (this.A != 0) {
                    this.f.setTextColor(this.A);
                }
            }
            if (!d(this.f)) {
                a((View) this.f, true);
            }
        } else if (this.f != null && d(this.f)) {
            removeView(this.f);
            this.F.remove(this.f);
        }
        if (this.f != null) {
            this.f.setText(charSequence);
        }
        this.y = charSequence;
    }

    public CharSequence getSubtitle() {
        return this.z;
    }

    public void setSubtitle(int i2) {
        setSubtitle(getContext().getText(i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.TextView, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.widget.c):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.l$a, android.support.v7.view.menu.MenuBuilder$a):void
      android.support.v7.widget.Toolbar.a(android.view.View, boolean):void */
    public void setSubtitle(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            if (this.g == null) {
                Context context = getContext();
                this.g = new AppCompatTextView(context);
                this.g.setSingleLine();
                this.g.setEllipsize(TextUtils.TruncateAt.END);
                if (this.o != 0) {
                    this.g.setTextAppearance(context, this.o);
                }
                if (this.B != 0) {
                    this.g.setTextColor(this.B);
                }
            }
            if (!d(this.g)) {
                a((View) this.g, true);
            }
        } else if (this.g != null && d(this.g)) {
            removeView(this.g);
            this.F.remove(this.g);
        }
        if (this.g != null) {
            this.g.setText(charSequence);
        }
        this.z = charSequence;
    }

    public void a(Context context, int i2) {
        this.n = i2;
        if (this.f != null) {
            this.f.setTextAppearance(context, i2);
        }
    }

    public void b(Context context, int i2) {
        this.o = i2;
        if (this.g != null) {
            this.g.setTextAppearance(context, i2);
        }
    }

    public void setTitleTextColor(int i2) {
        this.A = i2;
        if (this.f != null) {
            this.f.setTextColor(i2);
        }
    }

    public void setSubtitleTextColor(int i2) {
        this.B = i2;
        if (this.g != null) {
            this.g.setTextColor(i2);
        }
    }

    public CharSequence getNavigationContentDescription() {
        if (this.h != null) {
            return this.h.getContentDescription();
        }
        return null;
    }

    public void setNavigationContentDescription(int i2) {
        setNavigationContentDescription(i2 != 0 ? getContext().getText(i2) : null);
    }

    public void setNavigationContentDescription(CharSequence charSequence) {
        if (!TextUtils.isEmpty(charSequence)) {
            p();
        }
        if (this.h != null) {
            this.h.setContentDescription(charSequence);
        }
    }

    public void setNavigationIcon(int i2) {
        setNavigationIcon(android.support.v7.b.a.b.b(getContext(), i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.widget.ImageButton, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.widget.c):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.l$a, android.support.v7.view.menu.MenuBuilder$a):void
      android.support.v7.widget.Toolbar.a(android.view.View, boolean):void */
    public void setNavigationIcon(Drawable drawable) {
        if (drawable != null) {
            p();
            if (!d(this.h)) {
                a((View) this.h, true);
            }
        } else if (this.h != null && d(this.h)) {
            removeView(this.h);
            this.F.remove(this.h);
        }
        if (this.h != null) {
            this.h.setImageDrawable(drawable);
        }
    }

    public Drawable getNavigationIcon() {
        if (this.h != null) {
            return this.h.getDrawable();
        }
        return null;
    }

    public void setNavigationOnClickListener(View.OnClickListener onClickListener) {
        p();
        this.h.setOnClickListener(onClickListener);
    }

    public Menu getMenu() {
        n();
        return this.e.getMenu();
    }

    public void setOverflowIcon(Drawable drawable) {
        n();
        this.e.setOverflowIcon(drawable);
    }

    public Drawable getOverflowIcon() {
        n();
        return this.e.getOverflowIcon();
    }

    private void n() {
        o();
        if (this.e.d() == null) {
            MenuBuilder menuBuilder = (MenuBuilder) this.e.getMenu();
            if (this.K == null) {
                this.K = new a();
            }
            this.e.setExpandedActionViewsExclusive(true);
            menuBuilder.a(this.K, this.l);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v7.widget.Toolbar.a(android.view.View, boolean):void
     arg types: [android.support.v7.widget.ActionMenuView, int]
     candidates:
      android.support.v7.widget.Toolbar.a(android.view.View, int):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int[]):int
      android.support.v7.widget.Toolbar.a(java.util.List<android.view.View>, int):void
      android.support.v7.widget.Toolbar.a(int, int):void
      android.support.v7.widget.Toolbar.a(android.content.Context, int):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.MenuBuilder, android.support.v7.widget.c):void
      android.support.v7.widget.Toolbar.a(android.support.v7.view.menu.l$a, android.support.v7.view.menu.MenuBuilder$a):void
      android.support.v7.widget.Toolbar.a(android.view.View, boolean):void */
    private void o() {
        if (this.e == null) {
            this.e = new ActionMenuView(getContext());
            this.e.setPopupTheme(this.m);
            this.e.setOnMenuItemClickListener(this.H);
            this.e.a(this.L, this.M);
            LayoutParams j2 = generateDefaultLayoutParams();
            j2.f35a = 8388613 | (this.c & 112);
            this.e.setLayoutParams(j2);
            a((View) this.e, false);
        }
    }

    private MenuInflater getMenuInflater() {
        return new g(getContext());
    }

    public void setOnMenuItemClickListener(b bVar) {
        this.d = bVar;
    }

    public void a(int i2, int i3) {
        s();
        this.u.a(i2, i3);
    }

    public int getContentInsetStart() {
        if (this.u != null) {
            return this.u.c();
        }
        return 0;
    }

    public int getContentInsetEnd() {
        if (this.u != null) {
            return this.u.d();
        }
        return 0;
    }

    public int getContentInsetLeft() {
        if (this.u != null) {
            return this.u.a();
        }
        return 0;
    }

    public int getContentInsetRight() {
        if (this.u != null) {
            return this.u.b();
        }
        return 0;
    }

    public int getContentInsetStartWithNavigation() {
        if (this.v != Integer.MIN_VALUE) {
            return this.v;
        }
        return getContentInsetStart();
    }

    public void setContentInsetStartWithNavigation(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.v) {
            this.v = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getContentInsetEndWithActions() {
        if (this.w != Integer.MIN_VALUE) {
            return this.w;
        }
        return getContentInsetEnd();
    }

    public void setContentInsetEndWithActions(int i2) {
        if (i2 < 0) {
            i2 = Integer.MIN_VALUE;
        }
        if (i2 != this.w) {
            this.w = i2;
            if (getNavigationIcon() != null) {
                requestLayout();
            }
        }
    }

    public int getCurrentContentInsetStart() {
        if (getNavigationIcon() != null) {
            return Math.max(getContentInsetStart(), Math.max(this.v, 0));
        }
        return getContentInsetStart();
    }

    public int getCurrentContentInsetEnd() {
        boolean z2;
        if (this.e != null) {
            MenuBuilder d2 = this.e.d();
            z2 = d2 != null && d2.hasVisibleItems();
        } else {
            z2 = false;
        }
        if (z2) {
            return Math.max(getContentInsetEnd(), Math.max(this.w, 0));
        }
        return getContentInsetEnd();
    }

    public int getCurrentContentInsetLeft() {
        if (ViewCompat.getLayoutDirection(this) == 1) {
            return getCurrentContentInsetEnd();
        }
        return getCurrentContentInsetStart();
    }

    public int getCurrentContentInsetRight() {
        if (ViewCompat.getLayoutDirection(this) == 1) {
            return getCurrentContentInsetStart();
        }
        return getCurrentContentInsetEnd();
    }

    private void p() {
        if (this.h == null) {
            this.h = new AppCompatImageButton(getContext(), null, a.C0002a.toolbarNavigationButtonStyle);
            LayoutParams j2 = generateDefaultLayoutParams();
            j2.f35a = 8388611 | (this.c & 112);
            this.h.setLayoutParams(j2);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.f317a == null) {
            this.f317a = new AppCompatImageButton(getContext(), null, a.C0002a.toolbarNavigationButtonStyle);
            this.f317a.setImageDrawable(this.j);
            this.f317a.setContentDescription(this.k);
            LayoutParams j2 = generateDefaultLayoutParams();
            j2.f35a = 8388611 | (this.c & 112);
            j2.f322b = 2;
            this.f317a.setLayoutParams(j2);
            this.f317a.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Toolbar.this.h();
                }
            });
        }
    }

    private void a(View view, boolean z2) {
        LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2 = view.getLayoutParams();
        if (layoutParams2 == null) {
            layoutParams = generateDefaultLayoutParams();
        } else if (!checkLayoutParams(layoutParams2)) {
            layoutParams = generateLayoutParams(layoutParams2);
        } else {
            layoutParams = (LayoutParams) layoutParams2;
        }
        layoutParams.f322b = 1;
        if (!z2 || this.f318b == null) {
            addView(view, layoutParams);
            return;
        }
        view.setLayoutParams(layoutParams);
        this.F.add(view);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.K == null || this.K.f326b == null)) {
            savedState.f323a = this.K.f326b.getItemId();
        }
        savedState.f324b = b();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        MenuBuilder d2 = this.e != null ? this.e.d() : null;
        if (!(savedState.f323a == 0 || this.K == null || d2 == null || (findItem = d2.findItem(savedState.f323a)) == null)) {
            MenuItemCompat.expandActionView(findItem);
        }
        if (savedState.f324b) {
            q();
        }
    }

    private void q() {
        removeCallbacks(this.O);
        post(this.O);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.O);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent);
        if (actionMasked == 0) {
            this.C = false;
        }
        if (!this.C) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.C = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.C = false;
        }
        return true;
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent);
        if (actionMasked == 9) {
            this.D = false;
        }
        if (!this.D) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.D = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.D = false;
        }
        return true;
    }

    private void a(View view, int i2, int i3, int i4, int i5, int i6) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int childMeasureSpec = getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + marginLayoutParams.leftMargin + marginLayoutParams.rightMargin + i3, marginLayoutParams.width);
        int childMeasureSpec2 = getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height);
        int mode = View.MeasureSpec.getMode(childMeasureSpec2);
        if (mode != 1073741824 && i6 >= 0) {
            if (mode != 0) {
                i6 = Math.min(View.MeasureSpec.getSize(childMeasureSpec2), i6);
            }
            childMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i6, NTLMConstants.FLAG_NEGOTIATE_KEY_EXCHANGE);
        }
        view.measure(childMeasureSpec, childMeasureSpec2);
    }

    private int a(View view, int i2, int i3, int i4, int i5, int[] iArr) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        int i6 = marginLayoutParams.leftMargin - iArr[0];
        int i7 = marginLayoutParams.rightMargin - iArr[1];
        int max = Math.max(0, i6) + Math.max(0, i7);
        iArr[0] = Math.max(0, -i6);
        iArr[1] = Math.max(0, -i7);
        view.measure(getChildMeasureSpec(i2, getPaddingLeft() + getPaddingRight() + max + i3, marginLayoutParams.width), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom() + marginLayoutParams.topMargin + marginLayoutParams.bottomMargin + i5, marginLayoutParams.height));
        return view.getMeasuredWidth() + max;
    }

    private boolean r() {
        if (!this.N) {
            return false;
        }
        int childCount = getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View childAt = getChildAt(i2);
            if (a(childAt) && childAt.getMeasuredWidth() > 0 && childAt.getMeasuredHeight() > 0) {
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        char c2;
        char c3;
        int i4;
        int i5;
        int i6 = 0;
        int i7 = 0;
        int[] iArr = this.G;
        if (af.a(this)) {
            c2 = 0;
            c3 = 1;
        } else {
            c2 = 1;
            c3 = 0;
        }
        int i8 = 0;
        if (a(this.h)) {
            a(this.h, i2, 0, i3, 0, this.p);
            i8 = this.h.getMeasuredWidth() + b(this.h);
            int max = Math.max(0, this.h.getMeasuredHeight() + c(this.h));
            i7 = af.a(0, ViewCompat.getMeasuredState(this.h));
            i6 = max;
        }
        if (a(this.f317a)) {
            a(this.f317a, i2, 0, i3, 0, this.p);
            i8 = this.f317a.getMeasuredWidth() + b(this.f317a);
            i6 = Math.max(i6, this.f317a.getMeasuredHeight() + c(this.f317a));
            i7 = af.a(i7, ViewCompat.getMeasuredState(this.f317a));
        }
        int currentContentInsetStart = getCurrentContentInsetStart();
        int max2 = 0 + Math.max(currentContentInsetStart, i8);
        iArr[c3] = Math.max(0, currentContentInsetStart - i8);
        int i9 = 0;
        if (a(this.e)) {
            a(this.e, i2, max2, i3, 0, this.p);
            i9 = this.e.getMeasuredWidth() + b(this.e);
            i6 = Math.max(i6, this.e.getMeasuredHeight() + c(this.e));
            i7 = af.a(i7, ViewCompat.getMeasuredState(this.e));
        }
        int currentContentInsetEnd = getCurrentContentInsetEnd();
        int max3 = max2 + Math.max(currentContentInsetEnd, i9);
        iArr[c2] = Math.max(0, currentContentInsetEnd - i9);
        if (a(this.f318b)) {
            max3 += a(this.f318b, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.f318b.getMeasuredHeight() + c(this.f318b));
            i7 = af.a(i7, ViewCompat.getMeasuredState(this.f318b));
        }
        if (a(this.i)) {
            max3 += a(this.i, i2, max3, i3, 0, iArr);
            i6 = Math.max(i6, this.i.getMeasuredHeight() + c(this.i));
            i7 = af.a(i7, ViewCompat.getMeasuredState(this.i));
        }
        int childCount = getChildCount();
        int i10 = 0;
        int i11 = i6;
        int i12 = i7;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (((LayoutParams) childAt.getLayoutParams()).f322b != 0) {
                i4 = i12;
                i5 = i11;
            } else if (!a(childAt)) {
                i4 = i12;
                i5 = i11;
            } else {
                max3 += a(childAt, i2, max3, i3, 0, iArr);
                int max4 = Math.max(i11, childAt.getMeasuredHeight() + c(childAt));
                i4 = af.a(i12, ViewCompat.getMeasuredState(childAt));
                i5 = max4;
            }
            i10++;
            i12 = i4;
            i11 = i5;
        }
        int i13 = 0;
        int i14 = 0;
        int i15 = this.s + this.t;
        int i16 = this.q + this.r;
        if (a(this.f)) {
            a(this.f, i2, max3 + i16, i3, i15, iArr);
            i13 = b(this.f) + this.f.getMeasuredWidth();
            i14 = this.f.getMeasuredHeight() + c(this.f);
            i12 = af.a(i12, ViewCompat.getMeasuredState(this.f));
        }
        if (a(this.g)) {
            i13 = Math.max(i13, a(this.g, i2, max3 + i16, i3, i15 + i14, iArr));
            i14 += this.g.getMeasuredHeight() + c(this.g);
            i12 = af.a(i12, ViewCompat.getMeasuredState(this.g));
        }
        int max5 = Math.max(i11, i14);
        int paddingLeft = i13 + max3 + getPaddingLeft() + getPaddingRight();
        int paddingTop = max5 + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = ViewCompat.resolveSizeAndState(Math.max(paddingLeft, getSuggestedMinimumWidth()), i2, -16777216 & i12);
        int resolveSizeAndState2 = ViewCompat.resolveSizeAndState(Math.max(paddingTop, getSuggestedMinimumHeight()), i3, i12 << 16);
        if (r()) {
            resolveSizeAndState2 = 0;
        }
        setMeasuredDimension(resolveSizeAndState, resolveSizeAndState2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        boolean z3;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        if (ViewCompat.getLayoutDirection(this) == 1) {
            z3 = true;
        } else {
            z3 = false;
        }
        int width = getWidth();
        int height = getHeight();
        int paddingLeft = getPaddingLeft();
        int paddingRight = getPaddingRight();
        int paddingTop = getPaddingTop();
        int paddingBottom = getPaddingBottom();
        int i19 = width - paddingRight;
        int[] iArr = this.G;
        iArr[1] = 0;
        iArr[0] = 0;
        int minimumHeight = ViewCompat.getMinimumHeight(this);
        int min = minimumHeight >= 0 ? Math.min(minimumHeight, i5 - i3) : 0;
        if (!a(this.h)) {
            i6 = i19;
            i7 = paddingLeft;
        } else if (z3) {
            i6 = b(this.h, i19, iArr, min);
            i7 = paddingLeft;
        } else {
            int i20 = i19;
            i7 = a(this.h, paddingLeft, iArr, min);
            i6 = i20;
        }
        if (a(this.f317a)) {
            if (z3) {
                i6 = b(this.f317a, i6, iArr, min);
            } else {
                i7 = a(this.f317a, i7, iArr, min);
            }
        }
        if (a(this.e)) {
            if (z3) {
                i7 = a(this.e, i7, iArr, min);
            } else {
                i6 = b(this.e, i6, iArr, min);
            }
        }
        int currentContentInsetLeft = getCurrentContentInsetLeft();
        int currentContentInsetRight = getCurrentContentInsetRight();
        iArr[0] = Math.max(0, currentContentInsetLeft - i7);
        iArr[1] = Math.max(0, currentContentInsetRight - ((width - paddingRight) - i6));
        int max = Math.max(i7, currentContentInsetLeft);
        int min2 = Math.min(i6, (width - paddingRight) - currentContentInsetRight);
        if (a(this.f318b)) {
            if (z3) {
                min2 = b(this.f318b, min2, iArr, min);
            } else {
                max = a(this.f318b, max, iArr, min);
            }
        }
        if (!a(this.i)) {
            i8 = min2;
            i9 = max;
        } else if (z3) {
            i8 = b(this.i, min2, iArr, min);
            i9 = max;
        } else {
            i8 = min2;
            i9 = a(this.i, max, iArr, min);
        }
        boolean a2 = a(this.f);
        boolean a3 = a(this.g);
        int i21 = 0;
        if (a2) {
            LayoutParams layoutParams = (LayoutParams) this.f.getLayoutParams();
            i21 = 0 + layoutParams.bottomMargin + layoutParams.topMargin + this.f.getMeasuredHeight();
        }
        if (a3) {
            LayoutParams layoutParams2 = (LayoutParams) this.g.getLayoutParams();
            i10 = layoutParams2.bottomMargin + layoutParams2.topMargin + this.g.getMeasuredHeight() + i21;
        } else {
            i10 = i21;
        }
        if (a2 || a3) {
            TextView textView = a2 ? this.f : this.g;
            TextView textView2 = a3 ? this.g : this.f;
            LayoutParams layoutParams3 = (LayoutParams) textView.getLayoutParams();
            LayoutParams layoutParams4 = (LayoutParams) textView2.getLayoutParams();
            boolean z4 = (a2 && this.f.getMeasuredWidth() > 0) || (a3 && this.g.getMeasuredWidth() > 0);
            switch (this.x & 112) {
                case 48:
                    i11 = layoutParams3.topMargin + getPaddingTop() + this.s;
                    break;
                case 80:
                    i11 = (((height - paddingBottom) - layoutParams4.bottomMargin) - this.t) - i10;
                    break;
                default:
                    int i22 = (((height - paddingTop) - paddingBottom) - i10) / 2;
                    if (i22 < layoutParams3.topMargin + this.s) {
                        i18 = layoutParams3.topMargin + this.s;
                    } else {
                        int i23 = (((height - paddingBottom) - i10) - i22) - paddingTop;
                        if (i23 < layoutParams3.bottomMargin + this.t) {
                            i18 = Math.max(0, i22 - ((layoutParams4.bottomMargin + this.t) - i23));
                        } else {
                            i18 = i22;
                        }
                    }
                    i11 = paddingTop + i18;
                    break;
            }
            if (z3) {
                int i24 = (z4 ? this.q : 0) - iArr[1];
                int max2 = i8 - Math.max(0, i24);
                iArr[1] = Math.max(0, -i24);
                if (a2) {
                    int measuredWidth = max2 - this.f.getMeasuredWidth();
                    int measuredHeight = this.f.getMeasuredHeight() + i11;
                    this.f.layout(measuredWidth, i11, max2, measuredHeight);
                    int i25 = measuredWidth - this.r;
                    i11 = measuredHeight + ((LayoutParams) this.f.getLayoutParams()).bottomMargin;
                    i15 = i25;
                } else {
                    i15 = max2;
                }
                if (a3) {
                    LayoutParams layoutParams5 = (LayoutParams) this.g.getLayoutParams();
                    int i26 = layoutParams5.topMargin + i11;
                    int measuredHeight2 = this.g.getMeasuredHeight() + i26;
                    this.g.layout(max2 - this.g.getMeasuredWidth(), i26, max2, measuredHeight2);
                    int i27 = layoutParams5.bottomMargin + measuredHeight2;
                    i16 = max2 - this.r;
                } else {
                    i16 = max2;
                }
                if (z4) {
                    i17 = Math.min(i15, i16);
                } else {
                    i17 = max2;
                }
                i8 = i17;
            } else {
                int i28 = (z4 ? this.q : 0) - iArr[0];
                i9 += Math.max(0, i28);
                iArr[0] = Math.max(0, -i28);
                if (a2) {
                    int measuredWidth2 = this.f.getMeasuredWidth() + i9;
                    int measuredHeight3 = this.f.getMeasuredHeight() + i11;
                    this.f.layout(i9, i11, measuredWidth2, measuredHeight3);
                    int i29 = ((LayoutParams) this.f.getLayoutParams()).bottomMargin + measuredHeight3;
                    i12 = measuredWidth2 + this.r;
                    i13 = i29;
                } else {
                    i12 = i9;
                    i13 = i11;
                }
                if (a3) {
                    LayoutParams layoutParams6 = (LayoutParams) this.g.getLayoutParams();
                    int i30 = i13 + layoutParams6.topMargin;
                    int measuredWidth3 = this.g.getMeasuredWidth() + i9;
                    int measuredHeight4 = this.g.getMeasuredHeight() + i30;
                    this.g.layout(i9, i30, measuredWidth3, measuredHeight4);
                    int i31 = layoutParams6.bottomMargin + measuredHeight4;
                    i14 = this.r + measuredWidth3;
                } else {
                    i14 = i9;
                }
                if (z4) {
                    i9 = Math.max(i12, i14);
                }
            }
        }
        a(this.E, 3);
        int size = this.E.size();
        int i32 = i9;
        for (int i33 = 0; i33 < size; i33++) {
            i32 = a(this.E.get(i33), i32, iArr, min);
        }
        a(this.E, 5);
        int size2 = this.E.size();
        for (int i34 = 0; i34 < size2; i34++) {
            i8 = b(this.E.get(i34), i8, iArr, min);
        }
        a(this.E, 1);
        int a4 = a(this.E, iArr);
        int i35 = ((((width - paddingLeft) - paddingRight) / 2) + paddingLeft) - (a4 / 2);
        int i36 = a4 + i35;
        if (i35 < i32) {
            i35 = i32;
        } else if (i36 > i8) {
            i35 -= i36 - i8;
        }
        int size3 = this.E.size();
        int i37 = i35;
        for (int i38 = 0; i38 < size3; i38++) {
            i37 = a(this.E.get(i38), i37, iArr, min);
        }
        this.E.clear();
    }

    private int a(List<View> list, int[] iArr) {
        int i2 = iArr[0];
        int i3 = iArr[1];
        int size = list.size();
        int i4 = 0;
        int i5 = 0;
        int i6 = i3;
        int i7 = i2;
        while (i4 < size) {
            View view = list.get(i4);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int i8 = layoutParams.leftMargin - i7;
            int i9 = layoutParams.rightMargin - i6;
            int max = Math.max(0, i8);
            int max2 = Math.max(0, i9);
            i7 = Math.max(0, -i8);
            i6 = Math.max(0, -i9);
            i4++;
            i5 += view.getMeasuredWidth() + max + max2;
        }
        return i5;
    }

    private int a(View view, int i2, int[] iArr, int i3) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i4 = layoutParams.leftMargin - iArr[0];
        int max = Math.max(0, i4) + i2;
        iArr[0] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max, a2, max + measuredWidth, view.getMeasuredHeight() + a2);
        return layoutParams.rightMargin + measuredWidth + max;
    }

    private int b(View view, int i2, int[] iArr, int i3) {
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int i4 = layoutParams.rightMargin - iArr[1];
        int max = i2 - Math.max(0, i4);
        iArr[1] = Math.max(0, -i4);
        int a2 = a(view, i3);
        int measuredWidth = view.getMeasuredWidth();
        view.layout(max - measuredWidth, a2, max, view.getMeasuredHeight() + a2);
        return max - (layoutParams.leftMargin + measuredWidth);
    }

    private int a(View view, int i2) {
        int max;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int measuredHeight = view.getMeasuredHeight();
        int i3 = i2 > 0 ? (measuredHeight - i2) / 2 : 0;
        switch (a(layoutParams.f35a)) {
            case 48:
                return getPaddingTop() - i3;
            case 80:
                return (((getHeight() - getPaddingBottom()) - measuredHeight) - layoutParams.bottomMargin) - i3;
            default:
                int paddingTop = getPaddingTop();
                int paddingBottom = getPaddingBottom();
                int height = getHeight();
                int i4 = (((height - paddingTop) - paddingBottom) - measuredHeight) / 2;
                if (i4 < layoutParams.topMargin) {
                    max = layoutParams.topMargin;
                } else {
                    int i5 = (((height - paddingBottom) - measuredHeight) - i4) - paddingTop;
                    max = i5 < layoutParams.bottomMargin ? Math.max(0, i4 - (layoutParams.bottomMargin - i5)) : i4;
                }
                return max + paddingTop;
        }
    }

    private int a(int i2) {
        int i3 = i2 & 112;
        switch (i3) {
            case 16:
            case 48:
            case 80:
                return i3;
            default:
                return this.x & 112;
        }
    }

    private void a(List<View> list, int i2) {
        boolean z2 = true;
        if (ViewCompat.getLayoutDirection(this) != 1) {
            z2 = false;
        }
        int childCount = getChildCount();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, ViewCompat.getLayoutDirection(this));
        list.clear();
        if (z2) {
            for (int i3 = childCount - 1; i3 >= 0; i3--) {
                View childAt = getChildAt(i3);
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.f322b == 0 && a(childAt) && b(layoutParams.f35a) == absoluteGravity) {
                    list.add(childAt);
                }
            }
            return;
        }
        for (int i4 = 0; i4 < childCount; i4++) {
            View childAt2 = getChildAt(i4);
            LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
            if (layoutParams2.f322b == 0 && a(childAt2) && b(layoutParams2.f35a) == absoluteGravity) {
                list.add(childAt2);
            }
        }
    }

    private int b(int i2) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int absoluteGravity = GravityCompat.getAbsoluteGravity(i2, layoutDirection) & 7;
        switch (absoluteGravity) {
            case 1:
            case 3:
            case 5:
                return absoluteGravity;
            case 2:
            case 4:
            default:
                return layoutDirection == 1 ? 5 : 3;
        }
    }

    private boolean a(View view) {
        return (view == null || view.getParent() != this || view.getVisibility() == 8) ? false : true;
    }

    private int b(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return MarginLayoutParamsCompat.getMarginEnd(marginLayoutParams) + MarginLayoutParamsCompat.getMarginStart(marginLayoutParams);
    }

    private int c(View view) {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        return marginLayoutParams.bottomMargin + marginLayoutParams.topMargin;
    }

    /* renamed from: a */
    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof LayoutParams) {
            return new LayoutParams((LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ActionBar.LayoutParams) {
            return new LayoutParams((ActionBar.LayoutParams) layoutParams);
        }
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new LayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public LayoutParams generateDefaultLayoutParams() {
        return new LayoutParams(-2, -2);
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return super.checkLayoutParams(layoutParams) && (layoutParams instanceof LayoutParams);
    }

    public n getWrapper() {
        if (this.I == null) {
            this.I = new ad(this, true);
        }
        return this.I;
    }

    /* access modifiers changed from: package-private */
    public void k() {
        for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = getChildAt(childCount);
            if (!(((LayoutParams) childAt.getLayoutParams()).f322b == 2 || childAt == this.e)) {
                removeViewAt(childCount);
                this.F.add(childAt);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        for (int size = this.F.size() - 1; size >= 0; size--) {
            addView(this.F.get(size));
        }
        this.F.clear();
    }

    private boolean d(View view) {
        return view.getParent() == this || this.F.contains(view);
    }

    public void setCollapsible(boolean z2) {
        this.N = z2;
        requestLayout();
    }

    public void a(l.a aVar, MenuBuilder.a aVar2) {
        this.L = aVar;
        this.M = aVar2;
        if (this.e != null) {
            this.e.a(aVar, aVar2);
        }
    }

    private void s() {
        if (this.u == null) {
            this.u = new v();
        }
    }

    public static class LayoutParams extends ActionBar.LayoutParams {

        /* renamed from: b  reason: collision with root package name */
        int f322b = 0;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.f35a = 8388627;
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ActionBar.LayoutParams) layoutParams);
            this.f322b = layoutParams.f322b;
        }

        public LayoutParams(ActionBar.LayoutParams layoutParams) {
            super(layoutParams);
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            a(marginLayoutParams);
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }

        /* access modifiers changed from: package-private */
        public void a(ViewGroup.MarginLayoutParams marginLayoutParams) {
            this.leftMargin = marginLayoutParams.leftMargin;
            this.topMargin = marginLayoutParams.topMargin;
            this.rightMargin = marginLayoutParams.rightMargin;
            this.bottomMargin = marginLayoutParams.bottomMargin;
        }
    }

    public static class SavedState extends AbsSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.newCreator(new ParcelableCompatCreatorCallbacks<SavedState>() {
            /* renamed from: a */
            public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new SavedState(parcel, classLoader);
            }

            /* renamed from: a */
            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        });

        /* renamed from: a  reason: collision with root package name */
        int f323a;

        /* renamed from: b  reason: collision with root package name */
        boolean f324b;

        public SavedState(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.f323a = parcel.readInt();
            this.f324b = parcel.readInt() != 0;
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f323a);
            parcel.writeInt(this.f324b ? 1 : 0);
        }
    }

    private class a implements l {

        /* renamed from: a  reason: collision with root package name */
        MenuBuilder f325a;

        /* renamed from: b  reason: collision with root package name */
        MenuItemImpl f326b;

        a() {
        }

        public void a(Context context, MenuBuilder menuBuilder) {
            if (!(this.f325a == null || this.f326b == null)) {
                this.f325a.d(this.f326b);
            }
            this.f325a = menuBuilder;
        }

        public void a(boolean z) {
            boolean z2 = false;
            if (this.f326b != null) {
                if (this.f325a != null) {
                    int size = this.f325a.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.f325a.getItem(i) == this.f326b) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    b(this.f325a, this.f326b);
                }
            }
        }

        public void a(l.a aVar) {
        }

        public boolean a(SubMenuBuilder subMenuBuilder) {
            return false;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean b() {
            return false;
        }

        public boolean a(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            Toolbar.this.i();
            if (Toolbar.this.f317a.getParent() != Toolbar.this) {
                Toolbar.this.addView(Toolbar.this.f317a);
            }
            Toolbar.this.f318b = menuItemImpl.getActionView();
            this.f326b = menuItemImpl;
            if (Toolbar.this.f318b.getParent() != Toolbar.this) {
                LayoutParams j = Toolbar.this.generateDefaultLayoutParams();
                j.f35a = 8388611 | (Toolbar.this.c & 112);
                j.f322b = 2;
                Toolbar.this.f318b.setLayoutParams(j);
                Toolbar.this.addView(Toolbar.this.f318b);
            }
            Toolbar.this.k();
            Toolbar.this.requestLayout();
            menuItemImpl.e(true);
            if (Toolbar.this.f318b instanceof c) {
                ((c) Toolbar.this.f318b).a();
            }
            return true;
        }

        public boolean b(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            if (Toolbar.this.f318b instanceof c) {
                ((c) Toolbar.this.f318b).b();
            }
            Toolbar.this.removeView(Toolbar.this.f318b);
            Toolbar.this.removeView(Toolbar.this.f317a);
            Toolbar.this.f318b = null;
            Toolbar.this.l();
            this.f326b = null;
            Toolbar.this.requestLayout();
            menuItemImpl.e(false);
            return true;
        }
    }
}
