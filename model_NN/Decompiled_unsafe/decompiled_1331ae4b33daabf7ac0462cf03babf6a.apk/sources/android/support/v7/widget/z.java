package android.support.v7.widget;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: TintContextWrapper */
public class z extends ContextWrapper {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f396a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static ArrayList<WeakReference<z>> f397b;
    private final Resources c;
    private final Resources.Theme d;

    public static Context a(Context context) {
        if (!b(context)) {
            return context;
        }
        synchronized (f396a) {
            if (f397b == null) {
                f397b = new ArrayList<>();
            } else {
                for (int size = f397b.size() - 1; size >= 0; size--) {
                    WeakReference weakReference = f397b.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        f397b.remove(size);
                    }
                }
                for (int size2 = f397b.size() - 1; size2 >= 0; size2--) {
                    WeakReference weakReference2 = f397b.get(size2);
                    z zVar = weakReference2 != null ? (z) weakReference2.get() : null;
                    if (zVar != null && zVar.getBaseContext() == context) {
                        return zVar;
                    }
                }
            }
            z zVar2 = new z(context);
            f397b.add(new WeakReference(zVar2));
            return zVar2;
        }
    }

    private static boolean b(Context context) {
        if ((context instanceof z) || (context.getResources() instanceof ab) || (context.getResources() instanceof ae)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || ae.a()) {
            return true;
        }
        return false;
    }

    private z(Context context) {
        super(context);
        if (ae.a()) {
            this.c = new ae(this, context.getResources());
            this.d = this.c.newTheme();
            this.d.setTo(context.getTheme());
            return;
        }
        this.c = new ab(this, context.getResources());
        this.d = null;
    }

    public Resources.Theme getTheme() {
        return this.d == null ? super.getTheme() : this.d;
    }

    public void setTheme(int i) {
        if (this.d == null) {
            super.setTheme(i);
        } else {
            this.d.applyStyle(i, true);
        }
    }

    public Resources getResources() {
        return this.c;
    }
}
