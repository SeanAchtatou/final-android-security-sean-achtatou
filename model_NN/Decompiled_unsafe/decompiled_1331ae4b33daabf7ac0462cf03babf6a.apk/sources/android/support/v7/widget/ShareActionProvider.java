package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.v4.view.ActionProvider;
import android.support.v7.a.a;
import android.support.v7.b.a.b;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ShareActionProvider extends ActionProvider {

    /* renamed from: a  reason: collision with root package name */
    final Context f308a;

    /* renamed from: b  reason: collision with root package name */
    String f309b;
    private int c;
    private final a d;

    public View onCreateActionView() {
        ActivityChooserView activityChooserView = new ActivityChooserView(this.f308a);
        if (!activityChooserView.isInEditMode()) {
            activityChooserView.setActivityChooserModel(d.a(this.f308a, this.f309b));
        }
        TypedValue typedValue = new TypedValue();
        this.f308a.getTheme().resolveAttribute(a.C0002a.actionModeShareDrawable, typedValue, true);
        activityChooserView.setExpandActivityOverflowButtonDrawable(b.b(this.f308a, typedValue.resourceId));
        activityChooserView.setProvider(this);
        activityChooserView.setDefaultActionButtonContentDescription(a.i.abc_shareactionprovider_share_with_application);
        activityChooserView.setExpandActivityOverflowButtonContentDescription(a.i.abc_shareactionprovider_share_with);
        return activityChooserView;
    }

    public boolean hasSubMenu() {
        return true;
    }

    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        d a2 = d.a(this.f308a, this.f309b);
        PackageManager packageManager = this.f308a.getPackageManager();
        int a3 = a2.a();
        int min = Math.min(a3, this.c);
        for (int i = 0; i < min; i++) {
            ResolveInfo a4 = a2.a(i);
            subMenu.add(0, i, i, a4.loadLabel(packageManager)).setIcon(a4.loadIcon(packageManager)).setOnMenuItemClickListener(this.d);
        }
        if (min < a3) {
            SubMenu addSubMenu = subMenu.addSubMenu(0, min, min, this.f308a.getString(a.i.abc_activity_chooser_view_see_all));
            for (int i2 = 0; i2 < a3; i2++) {
                ResolveInfo a5 = a2.a(i2);
                addSubMenu.add(0, i2, i2, a5.loadLabel(packageManager)).setIcon(a5.loadIcon(packageManager)).setOnMenuItemClickListener(this.d);
            }
        }
    }

    private class a implements MenuItem.OnMenuItemClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ ShareActionProvider f310a;

        public boolean onMenuItemClick(MenuItem menuItem) {
            Intent b2 = d.a(this.f310a.f308a, this.f310a.f309b).b(menuItem.getItemId());
            if (b2 == null) {
                return true;
            }
            String action = b2.getAction();
            if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                this.f310a.a(b2);
            }
            this.f310a.f308a.startActivity(b2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Intent intent) {
        if (Build.VERSION.SDK_INT >= 21) {
            intent.addFlags(134742016);
        } else {
            intent.addFlags(524288);
        }
    }
}
