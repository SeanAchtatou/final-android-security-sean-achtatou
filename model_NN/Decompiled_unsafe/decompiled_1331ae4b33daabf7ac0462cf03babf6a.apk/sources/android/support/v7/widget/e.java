package android.support.v7.widget;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v7.a.a;
import android.util.AttributeSet;
import android.view.View;

/* compiled from: AppCompatBackgroundHelper */
class e {

    /* renamed from: a  reason: collision with root package name */
    private final View f361a;

    /* renamed from: b  reason: collision with root package name */
    private final g f362b;
    private int c = -1;
    private a d;
    private a e;
    private a f;

    e(View view) {
        this.f361a = view;
        this.f362b = g.a();
    }

    /* access modifiers changed from: package-private */
    public void a(AttributeSet attributeSet, int i) {
        ac a2 = ac.a(this.f361a.getContext(), attributeSet, a.k.ViewBackgroundHelper, i, 0);
        try {
            if (a2.g(a.k.ViewBackgroundHelper_android_background)) {
                this.c = a2.g(a.k.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.f362b.b(this.f361a.getContext(), this.c);
                if (b2 != null) {
                    b(b2);
                }
            }
            if (a2.g(a.k.ViewBackgroundHelper_backgroundTint)) {
                ViewCompat.setBackgroundTintList(this.f361a, a2.e(a.k.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.g(a.k.ViewBackgroundHelper_backgroundTintMode)) {
                ViewCompat.setBackgroundTintMode(this.f361a, o.a(a2.a(a.k.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.a();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.c = i;
        b(this.f362b != null ? this.f362b.b(this.f361a.getContext(), i) : null);
        if (d()) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Drawable drawable) {
        this.c = -1;
        b((ColorStateList) null);
        if (d()) {
            c();
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ColorStateList colorStateList) {
        if (this.e == null) {
            this.e = new a();
        }
        this.e.f363a = colorStateList;
        this.e.f330b = null;
        this.e.e = true;
        if (d()) {
            c();
        }
    }

    private boolean d() {
        ColorStateList a2;
        if (this.e != null && this.e.e) {
            if (this.c >= 0 && (a2 = this.f362b.a(this.f361a.getContext(), this.c, this.e.f363a)) != null) {
                this.e.f330b = a2;
                return true;
            } else if (this.e.f330b != this.e.f363a) {
                this.e.f330b = this.e.f363a;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public ColorStateList a() {
        if (this.e != null) {
            return this.e.f330b;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(PorterDuff.Mode mode) {
        if (this.e == null) {
            this.e = new a();
        }
        this.e.c = mode;
        this.e.d = true;
        c();
    }

    /* access modifiers changed from: package-private */
    public PorterDuff.Mode b() {
        if (this.e != null) {
            return this.e.c;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        Drawable background = this.f361a.getBackground();
        if (background == null) {
            return;
        }
        if (e() && b(background)) {
            return;
        }
        if (this.e != null) {
            g.a(background, this.e, this.f361a.getDrawableState());
        } else if (this.d != null) {
            g.a(background, this.d, this.f361a.getDrawableState());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new a();
            }
            this.d.f330b = colorStateList;
            this.d.e = true;
        } else {
            this.d = null;
        }
        c();
    }

    private boolean e() {
        int i = Build.VERSION.SDK_INT;
        if (i < 21) {
            return false;
        }
        if (i == 21 || this.d != null) {
            return true;
        }
        return false;
    }

    private boolean b(Drawable drawable) {
        if (this.f == null) {
            this.f = new a();
        }
        a aVar = this.f;
        aVar.a();
        ColorStateList backgroundTintList = ViewCompat.getBackgroundTintList(this.f361a);
        if (backgroundTintList != null) {
            aVar.e = true;
            aVar.f330b = backgroundTintList;
        }
        PorterDuff.Mode backgroundTintMode = ViewCompat.getBackgroundTintMode(this.f361a);
        if (backgroundTintMode != null) {
            aVar.d = true;
            aVar.c = backgroundTintMode;
        }
        if (!aVar.e && !aVar.d) {
            return false;
        }
        g.a(drawable, aVar, this.f361a.getDrawableState());
        return true;
    }

    /* compiled from: AppCompatBackgroundHelper */
    private static class a extends aa {

        /* renamed from: a  reason: collision with root package name */
        public ColorStateList f363a;

        a() {
        }

        /* access modifiers changed from: package-private */
        public void a() {
            super.a();
            this.f363a = null;
        }
    }
}
