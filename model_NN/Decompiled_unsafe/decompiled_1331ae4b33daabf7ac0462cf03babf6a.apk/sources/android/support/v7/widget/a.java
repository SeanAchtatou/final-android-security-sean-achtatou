package android.support.v7.widget;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;

/* compiled from: ActionBarBackgroundDrawable */
class a extends Drawable {

    /* renamed from: a  reason: collision with root package name */
    final ActionBarContainer f329a;

    public a(ActionBarContainer actionBarContainer) {
        this.f329a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        if (!this.f329a.d) {
            if (this.f329a.f195a != null) {
                this.f329a.f195a.draw(canvas);
            }
            if (this.f329a.f196b != null && this.f329a.e) {
                this.f329a.f196b.draw(canvas);
            }
        } else if (this.f329a.c != null) {
            this.f329a.c.draw(canvas);
        }
    }

    public void setAlpha(int i) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }

    public int getOpacity() {
        return 0;
    }
}
