package android.support.v7.widget;

import android.graphics.Outline;

/* compiled from: ActionBarBackgroundDrawableV21 */
class b extends a {
    public b(ActionBarContainer actionBarContainer) {
        super(actionBarContainer);
    }

    public void getOutline(Outline outline) {
        if (this.f329a.d) {
            if (this.f329a.c != null) {
                this.f329a.c.getOutline(outline);
            }
        } else if (this.f329a.f195a != null) {
            this.f329a.f195a.getOutline(outline);
        }
    }
}
