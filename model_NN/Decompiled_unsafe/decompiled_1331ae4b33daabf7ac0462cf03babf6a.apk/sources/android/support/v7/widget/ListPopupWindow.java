package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.a.a;
import android.support.v7.view.menu.p;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import java.lang.reflect.Method;

public class ListPopupWindow implements p {

    /* renamed from: a  reason: collision with root package name */
    private static Method f265a;

    /* renamed from: b  reason: collision with root package name */
    private static Method f266b;
    private static Method h;
    private AdapterView.OnItemSelectedListener A;
    private final d B;
    private final c C;
    private final a D;
    private Runnable E;
    private final Rect F;
    private Rect G;
    private boolean H;
    p c;
    int d;
    final e e;
    final Handler f;
    PopupWindow g;
    private Context i;
    private ListAdapter j;
    private int k;
    private int l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private boolean s;
    private boolean t;
    private View u;
    private int v;
    private DataSetObserver w;
    private View x;
    private Drawable y;
    private AdapterView.OnItemClickListener z;

    static {
        Class<PopupWindow> cls = PopupWindow.class;
        try {
            f265a = cls.getDeclaredMethod("setClipToScreenEnabled", Boolean.TYPE);
        } catch (NoSuchMethodException e2) {
            Log.i("ListPopupWindow", "Could not find method setClipToScreenEnabled() on PopupWindow. Oh well.");
        }
        Class<PopupWindow> cls2 = PopupWindow.class;
        try {
            f266b = cls2.getDeclaredMethod("getMaxAvailableHeight", View.class, Integer.TYPE, Boolean.TYPE);
        } catch (NoSuchMethodException e3) {
            Log.i("ListPopupWindow", "Could not find method getMaxAvailableHeight(View, int, boolean) on PopupWindow. Oh well.");
        }
        try {
            h = PopupWindow.class.getDeclaredMethod("setEpicenterBounds", Rect.class);
        } catch (NoSuchMethodException e4) {
            Log.i("ListPopupWindow", "Could not find method setEpicenterBounds(Rect) on PopupWindow. Oh well.");
        }
    }

    public ListPopupWindow(Context context) {
        this(context, null, a.C0002a.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.C0002a.listPopupWindowStyle);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    public ListPopupWindow(Context context, AttributeSet attributeSet, int i2, int i3) {
        this.k = -2;
        this.l = -2;
        this.o = 1002;
        this.q = true;
        this.r = 0;
        this.s = false;
        this.t = false;
        this.d = Integer.MAX_VALUE;
        this.v = 0;
        this.e = new e();
        this.B = new d();
        this.C = new c();
        this.D = new a();
        this.F = new Rect();
        this.i = context;
        this.f = new Handler(context.getMainLooper());
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a.k.ListPopupWindow, i2, i3);
        this.m = obtainStyledAttributes.getDimensionPixelOffset(a.k.ListPopupWindow_android_dropDownHorizontalOffset, 0);
        this.n = obtainStyledAttributes.getDimensionPixelOffset(a.k.ListPopupWindow_android_dropDownVerticalOffset, 0);
        if (this.n != 0) {
            this.p = true;
        }
        obtainStyledAttributes.recycle();
        if (Build.VERSION.SDK_INT >= 11) {
            this.g = new AppCompatPopupWindow(context, attributeSet, i2, i3);
        } else {
            this.g = new AppCompatPopupWindow(context, attributeSet, i2);
        }
        this.g.setInputMethodMode(1);
    }

    public void a(ListAdapter listAdapter) {
        if (this.w == null) {
            this.w = new b();
        } else if (this.j != null) {
            this.j.unregisterDataSetObserver(this.w);
        }
        this.j = listAdapter;
        if (this.j != null) {
            listAdapter.registerDataSetObserver(this.w);
        }
        if (this.c != null) {
            this.c.setAdapter(this.j);
        }
    }

    public void a(int i2) {
        this.v = i2;
    }

    public void a(boolean z2) {
        this.H = z2;
        this.g.setFocusable(z2);
    }

    public boolean g() {
        return this.H;
    }

    public Drawable h() {
        return this.g.getBackground();
    }

    public void a(Drawable drawable) {
        this.g.setBackgroundDrawable(drawable);
    }

    public void b(int i2) {
        this.g.setAnimationStyle(i2);
    }

    public View i() {
        return this.x;
    }

    public void b(View view) {
        this.x = view;
    }

    public int j() {
        return this.m;
    }

    public void c(int i2) {
        this.m = i2;
    }

    public int k() {
        if (!this.p) {
            return 0;
        }
        return this.n;
    }

    public void d(int i2) {
        this.n = i2;
        this.p = true;
    }

    public void a(Rect rect) {
        this.G = rect;
    }

    public void e(int i2) {
        this.r = i2;
    }

    public int l() {
        return this.l;
    }

    public void f(int i2) {
        this.l = i2;
    }

    public void g(int i2) {
        Drawable background = this.g.getBackground();
        if (background != null) {
            background.getPadding(this.F);
            this.l = this.F.left + this.F.right + i2;
            return;
        }
        f(i2);
    }

    public void a(AdapterView.OnItemClickListener onItemClickListener) {
        this.z = onItemClickListener;
    }

    public void a() {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        boolean z2 = true;
        boolean z3 = false;
        int i7 = -1;
        int f2 = f();
        boolean n2 = n();
        PopupWindowCompat.setWindowLayoutType(this.g, this.o);
        if (this.g.isShowing()) {
            if (this.l == -1) {
                i3 = -1;
            } else if (this.l == -2) {
                i3 = i().getWidth();
            } else {
                i3 = this.l;
            }
            if (this.k == -1) {
                if (!n2) {
                    f2 = -1;
                }
                if (n2) {
                    PopupWindow popupWindow = this.g;
                    if (this.l == -1) {
                        i6 = -1;
                    } else {
                        i6 = 0;
                    }
                    popupWindow.setWidth(i6);
                    this.g.setHeight(0);
                    i4 = f2;
                } else {
                    PopupWindow popupWindow2 = this.g;
                    if (this.l == -1) {
                        i5 = -1;
                    } else {
                        i5 = 0;
                    }
                    popupWindow2.setWidth(i5);
                    this.g.setHeight(-1);
                    i4 = f2;
                }
            } else if (this.k == -2) {
                i4 = f2;
            } else {
                i4 = this.k;
            }
            PopupWindow popupWindow3 = this.g;
            if (!this.t && !this.s) {
                z3 = true;
            }
            popupWindow3.setOutsideTouchable(z3);
            PopupWindow popupWindow4 = this.g;
            View i8 = i();
            int i9 = this.m;
            int i10 = this.n;
            if (i3 < 0) {
                i3 = -1;
            }
            if (i4 >= 0) {
                i7 = i4;
            }
            popupWindow4.update(i8, i9, i10, i3, i7);
            return;
        }
        if (this.l == -1) {
            i2 = -1;
        } else if (this.l == -2) {
            i2 = i().getWidth();
        } else {
            i2 = this.l;
        }
        if (this.k == -1) {
            f2 = -1;
        } else if (this.k != -2) {
            f2 = this.k;
        }
        this.g.setWidth(i2);
        this.g.setHeight(f2);
        b(true);
        PopupWindow popupWindow5 = this.g;
        if (this.t || this.s) {
            z2 = false;
        }
        popupWindow5.setOutsideTouchable(z2);
        this.g.setTouchInterceptor(this.B);
        if (h != null) {
            try {
                h.invoke(this.g, this.G);
            } catch (Exception e2) {
                Log.e("ListPopupWindow", "Could not invoke setEpicenterBounds on PopupWindow", e2);
            }
        }
        PopupWindowCompat.showAsDropDown(this.g, i(), this.m, this.n, this.r);
        this.c.setSelection(-1);
        if (!this.H || this.c.isInTouchMode()) {
            m();
        }
        if (!this.H) {
            this.f.post(this.D);
        }
    }

    public void c() {
        this.g.dismiss();
        b();
        this.g.setContentView(null);
        this.c = null;
        this.f.removeCallbacks(this.e);
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.g.setOnDismissListener(onDismissListener);
    }

    private void b() {
        if (this.u != null) {
            ViewParent parent = this.u.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.u);
            }
        }
    }

    public void h(int i2) {
        this.g.setInputMethodMode(i2);
    }

    public void i(int i2) {
        p pVar = this.c;
        if (d() && pVar != null) {
            pVar.setListSelectionHidden(false);
            pVar.setSelection(i2);
            if (Build.VERSION.SDK_INT >= 11 && pVar.getChoiceMode() != 0) {
                pVar.setItemChecked(i2, true);
            }
        }
    }

    public void m() {
        p pVar = this.c;
        if (pVar != null) {
            pVar.setListSelectionHidden(true);
            pVar.requestLayout();
        }
    }

    public boolean d() {
        return this.g.isShowing();
    }

    public boolean n() {
        return this.g.getInputMethodMode() == 2;
    }

    public ListView e() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public p a(Context context, boolean z2) {
        return new p(context, z2);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: android.support.v7.widget.p} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v16, resolved type: android.support.v7.widget.p} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v12, resolved type: android.widget.LinearLayout} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v22, resolved type: android.support.v7.widget.p} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int f() {
        /*
            r10 = this;
            r9 = 1073741824(0x40000000, float:2.0)
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = -1
            r1 = 1
            r2 = 0
            android.support.v7.widget.p r0 = r10.c
            if (r0 != 0) goto L_0x010c
            android.content.Context r5 = r10.i
            android.support.v7.widget.ListPopupWindow$1 r0 = new android.support.v7.widget.ListPopupWindow$1
            r0.<init>()
            r10.E = r0
            boolean r0 = r10.H
            if (r0 != 0) goto L_0x00f8
            r0 = r1
        L_0x0019:
            android.support.v7.widget.p r0 = r10.a(r5, r0)
            r10.c = r0
            android.graphics.drawable.Drawable r0 = r10.y
            if (r0 == 0) goto L_0x002a
            android.support.v7.widget.p r0 = r10.c
            android.graphics.drawable.Drawable r6 = r10.y
            r0.setSelector(r6)
        L_0x002a:
            android.support.v7.widget.p r0 = r10.c
            android.widget.ListAdapter r6 = r10.j
            r0.setAdapter(r6)
            android.support.v7.widget.p r0 = r10.c
            android.widget.AdapterView$OnItemClickListener r6 = r10.z
            r0.setOnItemClickListener(r6)
            android.support.v7.widget.p r0 = r10.c
            r0.setFocusable(r1)
            android.support.v7.widget.p r0 = r10.c
            r0.setFocusableInTouchMode(r1)
            android.support.v7.widget.p r0 = r10.c
            android.support.v7.widget.ListPopupWindow$2 r6 = new android.support.v7.widget.ListPopupWindow$2
            r6.<init>()
            r0.setOnItemSelectedListener(r6)
            android.support.v7.widget.p r0 = r10.c
            android.support.v7.widget.ListPopupWindow$c r6 = r10.C
            r0.setOnScrollListener(r6)
            android.widget.AdapterView$OnItemSelectedListener r0 = r10.A
            if (r0 == 0) goto L_0x005e
            android.support.v7.widget.p r0 = r10.c
            android.widget.AdapterView$OnItemSelectedListener r6 = r10.A
            r0.setOnItemSelectedListener(r6)
        L_0x005e:
            android.support.v7.widget.p r0 = r10.c
            android.view.View r7 = r10.u
            if (r7 == 0) goto L_0x0196
            android.widget.LinearLayout r6 = new android.widget.LinearLayout
            r6.<init>(r5)
            r6.setOrientation(r1)
            android.widget.LinearLayout$LayoutParams r5 = new android.widget.LinearLayout$LayoutParams
            r8 = 1065353216(0x3f800000, float:1.0)
            r5.<init>(r3, r2, r8)
            int r8 = r10.v
            switch(r8) {
                case 0: goto L_0x0102;
                case 1: goto L_0x00fb;
                default: goto L_0x0078;
            }
        L_0x0078:
            java.lang.String r0 = "ListPopupWindow"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "Invalid hint position "
            java.lang.StringBuilder r5 = r5.append(r8)
            int r8 = r10.v
            java.lang.StringBuilder r5 = r5.append(r8)
            java.lang.String r5 = r5.toString()
            android.util.Log.e(r0, r5)
        L_0x0092:
            int r0 = r10.l
            if (r0 < 0) goto L_0x0109
            int r0 = r10.l
            r5 = r0
            r0 = r4
        L_0x009a:
            int r0 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r7.measure(r0, r2)
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r7.getMeasuredHeight()
            int r7 = r0.topMargin
            int r5 = r5 + r7
            int r0 = r0.bottomMargin
            int r0 = r0 + r5
            r5 = r6
        L_0x00b2:
            android.widget.PopupWindow r6 = r10.g
            r6.setContentView(r5)
            r6 = r0
        L_0x00b8:
            android.widget.PopupWindow r0 = r10.g
            android.graphics.drawable.Drawable r0 = r0.getBackground()
            if (r0 == 0) goto L_0x012a
            android.graphics.Rect r5 = r10.F
            r0.getPadding(r5)
            android.graphics.Rect r0 = r10.F
            int r0 = r0.top
            android.graphics.Rect r5 = r10.F
            int r5 = r5.bottom
            int r0 = r0 + r5
            boolean r5 = r10.p
            if (r5 != 0) goto L_0x0190
            android.graphics.Rect r5 = r10.F
            int r5 = r5.top
            int r5 = -r5
            r10.n = r5
            r7 = r0
        L_0x00da:
            android.widget.PopupWindow r0 = r10.g
            int r0 = r0.getInputMethodMode()
            r5 = 2
            if (r0 != r5) goto L_0x0131
        L_0x00e3:
            android.view.View r0 = r10.i()
            int r5 = r10.n
            int r5 = r10.a(r0, r5, r1)
            boolean r0 = r10.s
            if (r0 != 0) goto L_0x00f5
            int r0 = r10.k
            if (r0 != r3) goto L_0x0133
        L_0x00f5:
            int r0 = r5 + r7
        L_0x00f7:
            return r0
        L_0x00f8:
            r0 = r2
            goto L_0x0019
        L_0x00fb:
            r6.addView(r0, r5)
            r6.addView(r7)
            goto L_0x0092
        L_0x0102:
            r6.addView(r7)
            r6.addView(r0, r5)
            goto L_0x0092
        L_0x0109:
            r0 = r2
            r5 = r2
            goto L_0x009a
        L_0x010c:
            android.widget.PopupWindow r0 = r10.g
            android.view.View r0 = r0.getContentView()
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0
            android.view.View r5 = r10.u
            if (r5 == 0) goto L_0x0193
            android.view.ViewGroup$LayoutParams r0 = r5.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r5 = r5.getMeasuredHeight()
            int r6 = r0.topMargin
            int r5 = r5 + r6
            int r0 = r0.bottomMargin
            int r0 = r0 + r5
            r6 = r0
            goto L_0x00b8
        L_0x012a:
            android.graphics.Rect r0 = r10.F
            r0.setEmpty()
            r7 = r2
            goto L_0x00da
        L_0x0131:
            r1 = r2
            goto L_0x00e3
        L_0x0133:
            int r0 = r10.l
            switch(r0) {
                case -2: goto L_0x015a;
                case -1: goto L_0x0175;
                default: goto L_0x0138;
            }
        L_0x0138:
            int r0 = r10.l
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
        L_0x013e:
            android.support.v7.widget.p r0 = r10.c
            int r4 = r5 - r6
            r5 = r3
            int r0 = r0.a(r1, r2, r3, r4, r5)
            if (r0 <= 0) goto L_0x0158
            android.support.v7.widget.p r1 = r10.c
            int r1 = r1.getPaddingTop()
            android.support.v7.widget.p r2 = r10.c
            int r2 = r2.getPaddingBottom()
            int r1 = r1 + r2
            int r1 = r1 + r7
            int r6 = r6 + r1
        L_0x0158:
            int r0 = r0 + r6
            goto L_0x00f7
        L_0x015a:
            android.content.Context r0 = r10.i
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.F
            int r1 = r1.left
            android.graphics.Rect r8 = r10.F
            int r8 = r8.right
            int r1 = r1 + r8
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r4)
            goto L_0x013e
        L_0x0175:
            android.content.Context r0 = r10.i
            android.content.res.Resources r0 = r0.getResources()
            android.util.DisplayMetrics r0 = r0.getDisplayMetrics()
            int r0 = r0.widthPixels
            android.graphics.Rect r1 = r10.F
            int r1 = r1.left
            android.graphics.Rect r4 = r10.F
            int r4 = r4.right
            int r1 = r1 + r4
            int r0 = r0 - r1
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            goto L_0x013e
        L_0x0190:
            r7 = r0
            goto L_0x00da
        L_0x0193:
            r6 = r2
            goto L_0x00b8
        L_0x0196:
            r5 = r0
            r0 = r2
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ListPopupWindow.f():int");
    }

    private class b extends DataSetObserver {
        b() {
        }

        public void onChanged() {
            if (ListPopupWindow.this.d()) {
                ListPopupWindow.this.a();
            }
        }

        public void onInvalidated() {
            ListPopupWindow.this.c();
        }
    }

    private class a implements Runnable {
        a() {
        }

        public void run() {
            ListPopupWindow.this.m();
        }
    }

    private class e implements Runnable {
        e() {
        }

        public void run() {
            if (ListPopupWindow.this.c != null && ViewCompat.isAttachedToWindow(ListPopupWindow.this.c) && ListPopupWindow.this.c.getCount() > ListPopupWindow.this.c.getChildCount() && ListPopupWindow.this.c.getChildCount() <= ListPopupWindow.this.d) {
                ListPopupWindow.this.g.setInputMethodMode(2);
                ListPopupWindow.this.a();
            }
        }
    }

    private class d implements View.OnTouchListener {
        d() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            int x = (int) motionEvent.getX();
            int y = (int) motionEvent.getY();
            if (action == 0 && ListPopupWindow.this.g != null && ListPopupWindow.this.g.isShowing() && x >= 0 && x < ListPopupWindow.this.g.getWidth() && y >= 0 && y < ListPopupWindow.this.g.getHeight()) {
                ListPopupWindow.this.f.postDelayed(ListPopupWindow.this.e, 250);
                return false;
            } else if (action != 1) {
                return false;
            } else {
                ListPopupWindow.this.f.removeCallbacks(ListPopupWindow.this.e);
                return false;
            }
        }
    }

    private class c implements AbsListView.OnScrollListener {
        c() {
        }

        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (i == 1 && !ListPopupWindow.this.n() && ListPopupWindow.this.g.getContentView() != null) {
                ListPopupWindow.this.f.removeCallbacks(ListPopupWindow.this.e);
                ListPopupWindow.this.e.run();
            }
        }
    }

    private void b(boolean z2) {
        if (f265a != null) {
            try {
                f265a.invoke(this.g, Boolean.valueOf(z2));
            } catch (Exception e2) {
                Log.i("ListPopupWindow", "Could not call setClipToScreenEnabled() on PopupWindow. Oh well.");
            }
        }
    }

    private int a(View view, int i2, boolean z2) {
        if (f266b != null) {
            try {
                return ((Integer) f266b.invoke(this.g, view, Integer.valueOf(i2), Boolean.valueOf(z2))).intValue();
            } catch (Exception e2) {
                Log.i("ListPopupWindow", "Could not call getMaxAvailableHeightMethod(View, int, boolean) on PopupWindow. Using the public version.");
            }
        }
        return this.g.getMaxAvailableHeight(view, i2);
    }
}
