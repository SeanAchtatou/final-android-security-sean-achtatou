package android.support.v7.widget;

import android.os.Build;
import android.os.SystemClock;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.view.menu.p;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewParent;
import android.view.ViewTreeObserver;

/* compiled from: ForwardingListener */
public abstract class r implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final float f378a;

    /* renamed from: b  reason: collision with root package name */
    private final int f379b;
    final View c;
    private final int d;
    private Runnable e;
    private Runnable f;
    private boolean g;
    private int h;
    private final int[] i = new int[2];

    public abstract p a();

    public r(View view) {
        this.c = view;
        view.setLongClickable(true);
        if (Build.VERSION.SDK_INT >= 12) {
            a(view);
        } else {
            b(view);
        }
        this.f378a = (float) ViewConfiguration.get(view.getContext()).getScaledTouchSlop();
        this.f379b = ViewConfiguration.getTapTimeout();
        this.d = (this.f379b + ViewConfiguration.getLongPressTimeout()) / 2;
    }

    private void a(View view) {
        view.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                r.this.e();
            }
        });
    }

    private void b(View view) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            /* renamed from: a  reason: collision with root package name */
            boolean f381a = ViewCompat.isAttachedToWindow(r.this.c);

            public void onGlobalLayout() {
                boolean z = this.f381a;
                this.f381a = ViewCompat.isAttachedToWindow(r.this.c);
                if (z && !this.f381a) {
                    r.this.e();
                }
            }
        });
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        boolean z;
        boolean z2 = this.g;
        if (!z2) {
            boolean z3 = a(motionEvent) && b();
            if (z3) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                this.c.onTouchEvent(obtain);
                obtain.recycle();
            }
            z = z3;
        } else if (b(motionEvent) || !c()) {
            z = true;
        } else {
            z = false;
        }
        this.g = z;
        if (z || z2) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void e() {
        this.g = false;
        this.h = -1;
        if (this.e != null) {
            this.c.removeCallbacks(this.e);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        p a2 = a();
        if (a2 == null || a2.d()) {
            return true;
        }
        a2.a();
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean c() {
        p a2 = a();
        if (a2 == null || !a2.d()) {
            return true;
        }
        a2.c();
        return true;
    }

    private boolean a(MotionEvent motionEvent) {
        View view = this.c;
        if (!view.isEnabled()) {
            return false;
        }
        switch (MotionEventCompat.getActionMasked(motionEvent)) {
            case 0:
                this.h = motionEvent.getPointerId(0);
                if (this.e == null) {
                    this.e = new a();
                }
                view.postDelayed(this.e, (long) this.f379b);
                if (this.f == null) {
                    this.f = new b();
                }
                view.postDelayed(this.f, (long) this.d);
                return false;
            case 1:
            case 3:
                f();
                return false;
            case 2:
                int findPointerIndex = motionEvent.findPointerIndex(this.h);
                if (findPointerIndex < 0 || a(view, motionEvent.getX(findPointerIndex), motionEvent.getY(findPointerIndex), this.f378a)) {
                    return false;
                }
                f();
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return true;
            default:
                return false;
        }
    }

    private void f() {
        if (this.f != null) {
            this.c.removeCallbacks(this.f);
        }
        if (this.e != null) {
            this.c.removeCallbacks(this.e);
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        f();
        View view = this.c;
        if (view.isEnabled() && !view.isLongClickable() && b()) {
            view.getParent().requestDisallowInterceptTouchEvent(true);
            long uptimeMillis = SystemClock.uptimeMillis();
            MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
            view.onTouchEvent(obtain);
            obtain.recycle();
            this.g = true;
        }
    }

    private boolean b(MotionEvent motionEvent) {
        p pVar;
        boolean z;
        boolean z2;
        View view = this.c;
        p a2 = a();
        if (a2 == null || !a2.d() || (pVar = (p) a2.e()) == null || !pVar.isShown()) {
            return false;
        }
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        b(view, obtainNoHistory);
        a(pVar, obtainNoHistory);
        boolean a3 = pVar.a(obtainNoHistory, this.h);
        obtainNoHistory.recycle();
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent);
        if (actionMasked == 1 || actionMasked == 3) {
            z = false;
        } else {
            z = true;
        }
        if (!a3 || !z) {
            z2 = false;
        } else {
            z2 = true;
        }
        return z2;
    }

    private static boolean a(View view, float f2, float f3, float f4) {
        return f2 >= (-f4) && f3 >= (-f4) && f2 < ((float) (view.getRight() - view.getLeft())) + f4 && f3 < ((float) (view.getBottom() - view.getTop())) + f4;
    }

    private boolean a(View view, MotionEvent motionEvent) {
        int[] iArr = this.i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) (-iArr[0]), (float) (-iArr[1]));
        return true;
    }

    private boolean b(View view, MotionEvent motionEvent) {
        int[] iArr = this.i;
        view.getLocationOnScreen(iArr);
        motionEvent.offsetLocation((float) iArr[0], (float) iArr[1]);
        return true;
    }

    /* compiled from: ForwardingListener */
    private class a implements Runnable {
        a() {
        }

        public void run() {
            ViewParent parent = r.this.c.getParent();
            if (parent != null) {
                parent.requestDisallowInterceptTouchEvent(true);
            }
        }
    }

    /* compiled from: ForwardingListener */
    private class b implements Runnable {
        b() {
        }

        public void run() {
            r.this.d();
        }
    }
}
