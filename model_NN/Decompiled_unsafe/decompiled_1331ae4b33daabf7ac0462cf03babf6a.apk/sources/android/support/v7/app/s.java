package android.support.v7.app;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.content.PermissionChecker;
import android.util.Log;
import java.util.Calendar;

/* compiled from: TwilightManager */
class s {

    /* renamed from: a  reason: collision with root package name */
    private static s f104a;

    /* renamed from: b  reason: collision with root package name */
    private final Context f105b;
    private final LocationManager c;
    private final a d = new a();

    static s a(Context context) {
        if (f104a == null) {
            Context applicationContext = context.getApplicationContext();
            f104a = new s(applicationContext, (LocationManager) applicationContext.getSystemService("location"));
        }
        return f104a;
    }

    s(Context context, LocationManager locationManager) {
        this.f105b = context;
        this.c = locationManager;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        a aVar = this.d;
        if (c()) {
            return aVar.f106a;
        }
        Location b2 = b();
        if (b2 != null) {
            a(b2);
            return aVar.f106a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    private Location b() {
        Location location;
        Location location2 = null;
        if (PermissionChecker.checkSelfPermission(this.f105b, "android.permission.ACCESS_COARSE_LOCATION") == 0) {
            location = a("network");
        } else {
            location = null;
        }
        if (PermissionChecker.checkSelfPermission(this.f105b, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location2 = a("gps");
        }
        if (location2 == null || location == null) {
            if (location2 == null) {
                location2 = location;
            }
            return location2;
        } else if (location2.getTime() > location.getTime()) {
            return location2;
        } else {
            return location;
        }
    }

    private Location a(String str) {
        if (this.c != null) {
            try {
                if (this.c.isProviderEnabled(str)) {
                    return this.c.getLastKnownLocation(str);
                }
            } catch (Exception e) {
                Log.d("TwilightManager", "Failed to get last known location", e);
            }
        }
        return null;
    }

    private boolean c() {
        return this.d != null && this.d.f > System.currentTimeMillis();
    }

    private void a(Location location) {
        long j;
        long j2;
        a aVar = this.d;
        long currentTimeMillis = System.currentTimeMillis();
        r a2 = r.a();
        a2.a(currentTimeMillis - LogBuilder.MAX_INTERVAL, location.getLatitude(), location.getLongitude());
        long j3 = a2.f102a;
        a2.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.c == 1;
        long j4 = a2.f103b;
        long j5 = a2.f102a;
        a2.a(LogBuilder.MAX_INTERVAL + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j6 = a2.f103b;
        if (j4 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            if (currentTimeMillis > j5) {
                j2 = 0 + j6;
            } else if (currentTimeMillis > j4) {
                j2 = 0 + j5;
            } else {
                j2 = 0 + j4;
            }
            j = j2 + 60000;
        }
        aVar.f106a = z;
        aVar.f107b = j3;
        aVar.c = j4;
        aVar.d = j5;
        aVar.e = j6;
        aVar.f = j;
    }

    /* compiled from: TwilightManager */
    private static class a {

        /* renamed from: a  reason: collision with root package name */
        boolean f106a;

        /* renamed from: b  reason: collision with root package name */
        long f107b;
        long c;
        long d;
        long e;
        long f;

        a() {
        }
    }
}
