package android.support.v7.app;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.KeyEventCompat;
import android.support.v7.view.b;
import android.support.v7.widget.ae;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class AppCompatActivity extends FragmentActivity implements TaskStackBuilder.SupportParentable, c {

    /* renamed from: a  reason: collision with root package name */
    private d f38a;

    /* renamed from: b  reason: collision with root package name */
    private int f39b = 0;
    private boolean c;
    private Resources d;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        d d2 = d();
        d2.h();
        d2.a(bundle);
        if (d2.i() && this.f39b != 0) {
            if (Build.VERSION.SDK_INT >= 23) {
                onApplyThemeResource(getTheme(), this.f39b, false);
            } else {
                setTheme(this.f39b);
            }
        }
        super.onCreate(bundle);
    }

    public void setTheme(int i) {
        super.setTheme(i);
        this.f39b = i;
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        d().b(bundle);
    }

    public ActionBar a() {
        return d().a();
    }

    public MenuInflater getMenuInflater() {
        return d().b();
    }

    public void setContentView(int i) {
        d().b(i);
    }

    public void setContentView(View view) {
        d().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        d().a(view, layoutParams);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        d().b(view, layoutParams);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        d().a(configuration);
        if (this.d != null) {
            this.d.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
    }

    /* access modifiers changed from: protected */
    public void onPostResume() {
        super.onPostResume();
        d().e();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        d().c();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        d().d();
    }

    public View findViewById(int i) {
        return d().a(i);
    }

    public final boolean onMenuItemSelected(int i, MenuItem menuItem) {
        if (super.onMenuItemSelected(i, menuItem)) {
            return true;
        }
        ActionBar a2 = a();
        if (menuItem.getItemId() != 16908332 || a2 == null || (a2.a() & 4) == 0) {
            return false;
        }
        return b();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        d().g();
    }

    /* access modifiers changed from: protected */
    public void onTitleChanged(CharSequence charSequence, int i) {
        super.onTitleChanged(charSequence, i);
        d().a(charSequence);
    }

    public void supportInvalidateOptionsMenu() {
        d().f();
    }

    public void invalidateOptionsMenu() {
        d().f();
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
    }

    public b a(b.a aVar) {
        return null;
    }

    public void a(TaskStackBuilder taskStackBuilder) {
        taskStackBuilder.addParentStack(this);
    }

    public void b(TaskStackBuilder taskStackBuilder) {
    }

    public boolean b() {
        Intent supportParentActivityIntent = getSupportParentActivityIntent();
        if (supportParentActivityIntent == null) {
            return false;
        }
        if (a(supportParentActivityIntent)) {
            TaskStackBuilder create = TaskStackBuilder.create(this);
            a(create);
            b(create);
            create.startActivities();
            try {
                ActivityCompat.finishAffinity(this);
            } catch (IllegalStateException e) {
                finish();
            }
        } else {
            b(supportParentActivityIntent);
        }
        return true;
    }

    public Intent getSupportParentActivityIntent() {
        return NavUtils.getParentActivityIntent(this);
    }

    public boolean a(Intent intent) {
        return NavUtils.shouldUpRecreateTask(this, intent);
    }

    public void b(Intent intent) {
        NavUtils.navigateUpTo(this, intent);
    }

    public void onContentChanged() {
        c();
    }

    @Deprecated
    public void c() {
    }

    public boolean onMenuOpened(int i, Menu menu) {
        return super.onMenuOpened(i, menu);
    }

    public void onPanelClosed(int i, Menu menu) {
        super.onPanelClosed(i, menu);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        d().c(bundle);
    }

    public d d() {
        if (this.f38a == null) {
            this.f38a = d.a(this, this);
        }
        return this.f38a;
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (KeyEventCompat.isCtrlPressed(keyEvent) && keyEvent.getUnicodeChar(keyEvent.getMetaState() & -28673) == 60) {
            int action = keyEvent.getAction();
            if (action == 0) {
                ActionBar a2 = a();
                if (a2 != null && a2.b() && a2.g()) {
                    this.c = true;
                    return true;
                }
            } else if (action == 1 && this.c) {
                this.c = false;
                return true;
            }
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public Resources getResources() {
        if (this.d == null && ae.a()) {
            this.d = new ae(this, super.getResources());
        }
        return this.d == null ? super.getResources() : this.d;
    }
}
