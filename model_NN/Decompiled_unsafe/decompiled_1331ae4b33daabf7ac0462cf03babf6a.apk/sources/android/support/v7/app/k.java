package android.support.v7.app;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.a.a;
import android.support.v7.view.b;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;

/* compiled from: AppCompatDialog */
public class k extends Dialog implements c {

    /* renamed from: a  reason: collision with root package name */
    private d f92a;

    public k(Context context, int i) {
        super(context, a(context, i));
        a().a((Bundle) null);
        a().i();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        a().h();
        super.onCreate(bundle);
        a().a(bundle);
    }

    public void setContentView(int i) {
        a().b(i);
    }

    public void setContentView(View view) {
        a().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    public View findViewById(int i) {
        return a().a(i);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    public void setTitle(int i) {
        super.setTitle(i);
        a().a(getContext().getString(i));
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a().d();
    }

    public boolean a(int i) {
        return a().c(i);
    }

    public void invalidateOptionsMenu() {
        a().f();
    }

    public d a() {
        if (this.f92a == null) {
            this.f92a = d.a(this, this);
        }
        return this.f92a;
    }

    private static int a(Context context, int i) {
        if (i != 0) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a.C0002a.dialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void a(b bVar) {
    }

    public void b(b bVar) {
    }

    public b a(b.a aVar) {
        return null;
    }
}
