package android.support.v7.app;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.l;
import android.support.v7.widget.n;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import java.util.ArrayList;

/* compiled from: ToolbarActionBar */
class q extends ActionBar {

    /* renamed from: a  reason: collision with root package name */
    n f97a;

    /* renamed from: b  reason: collision with root package name */
    Window.Callback f98b;
    private boolean c;
    private boolean d;
    private ArrayList<ActionBar.a> e;
    private final Runnable f;

    public void a(boolean z) {
    }

    public void a(float f2) {
        ViewCompat.setElevation(this.f97a.a(), f2);
    }

    public Context c() {
        return this.f97a.b();
    }

    public void c(boolean z) {
    }

    public void d(boolean z) {
    }

    public void a(Configuration configuration) {
        super.a(configuration);
    }

    public void a(CharSequence charSequence) {
        this.f97a.a(charSequence);
    }

    public boolean g() {
        ViewGroup a2 = this.f97a.a();
        if (a2 == null || a2.hasFocus()) {
            return false;
        }
        a2.requestFocus();
        return true;
    }

    public int a() {
        return this.f97a.o();
    }

    public boolean b() {
        return this.f97a.q() == 0;
    }

    public boolean e() {
        this.f97a.a().removeCallbacks(this.f);
        ViewCompat.postOnAnimation(this.f97a.a(), this.f);
        return true;
    }

    public boolean f() {
        if (!this.f97a.c()) {
            return false;
        }
        this.f97a.d();
        return true;
    }

    public boolean a(int i, KeyEvent keyEvent) {
        boolean z;
        Menu i2 = i();
        if (i2 != null) {
            if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() != 1) {
                z = true;
            } else {
                z = false;
            }
            i2.setQwertyMode(z);
            i2.performShortcut(i, keyEvent, 0);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.f97a.a().removeCallbacks(this.f);
    }

    public void e(boolean z) {
        if (z != this.d) {
            this.d = z;
            int size = this.e.size();
            for (int i = 0; i < size; i++) {
                this.e.get(i).a(z);
            }
        }
    }

    private Menu i() {
        if (!this.c) {
            this.f97a.a(new a(), new b());
            this.c = true;
        }
        return this.f97a.r();
    }

    /* compiled from: ToolbarActionBar */
    private final class a implements l.a {

        /* renamed from: b  reason: collision with root package name */
        private boolean f100b;

        a() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            if (q.this.f98b == null) {
                return false;
            }
            q.this.f98b.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (!this.f100b) {
                this.f100b = true;
                q.this.f97a.n();
                if (q.this.f98b != null) {
                    q.this.f98b.onPanelClosed(108, menuBuilder);
                }
                this.f100b = false;
            }
        }
    }

    /* compiled from: ToolbarActionBar */
    private final class b implements MenuBuilder.a {
        b() {
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            if (q.this.f98b == null) {
                return;
            }
            if (q.this.f97a.i()) {
                q.this.f98b.onPanelClosed(108, menuBuilder);
            } else if (q.this.f98b.onPreparePanel(0, null, menuBuilder)) {
                q.this.f98b.onMenuOpened(108, menuBuilder);
            }
        }
    }
}
