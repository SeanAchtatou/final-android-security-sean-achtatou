package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.os.BuildCompat;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

/* compiled from: AppCompatDelegate */
public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    private static int f67a = -1;

    /* renamed from: b  reason: collision with root package name */
    private static boolean f68b = false;

    public abstract ActionBar a();

    public abstract View a(int i);

    public abstract void a(Configuration configuration);

    public abstract void a(Bundle bundle);

    public abstract void a(View view);

    public abstract void a(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void a(CharSequence charSequence);

    public abstract MenuInflater b();

    public abstract void b(int i);

    public abstract void b(Bundle bundle);

    public abstract void b(View view, ViewGroup.LayoutParams layoutParams);

    public abstract void c();

    public abstract void c(Bundle bundle);

    public abstract boolean c(int i);

    public abstract void d();

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public abstract void h();

    public abstract boolean i();

    public static d a(Activity activity, c cVar) {
        return a(activity, activity.getWindow(), cVar);
    }

    public static d a(Dialog dialog, c cVar) {
        return a(dialog.getContext(), dialog.getWindow(), cVar);
    }

    private static d a(Context context, Window window, c cVar) {
        int i = Build.VERSION.SDK_INT;
        if (BuildCompat.isAtLeastN()) {
            return new f(context, window, cVar);
        }
        if (i >= 23) {
            return new i(context, window, cVar);
        }
        if (i >= 14) {
            return new h(context, window, cVar);
        }
        if (i >= 11) {
            return new g(context, window, cVar);
        }
        return new j(context, window, cVar);
    }

    d() {
    }

    public static int j() {
        return f67a;
    }

    public static boolean k() {
        return f68b;
    }
}
