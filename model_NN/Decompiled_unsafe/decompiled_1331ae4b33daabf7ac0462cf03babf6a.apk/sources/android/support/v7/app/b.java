package android.support.v7.app;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.a.a;
import android.support.v7.app.a;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListAdapter;

/* compiled from: AlertDialog */
public class b extends k implements DialogInterface {

    /* renamed from: a  reason: collision with root package name */
    final a f64a = new a(getContext(), this, getWindow());

    protected b(Context context, int i) {
        super(context, a(context, i));
    }

    static int a(Context context, int i) {
        if (i >= 16777216) {
            return i;
        }
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(a.C0002a.alertDialogTheme, typedValue, true);
        return typedValue.resourceId;
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        this.f64a.a(charSequence);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f64a.a();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (this.f64a.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        if (this.f64a.b(i, keyEvent)) {
            return true;
        }
        return super.onKeyUp(i, keyEvent);
    }

    /* compiled from: AlertDialog */
    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private final a.C0003a f65a;

        /* renamed from: b  reason: collision with root package name */
        private final int f66b;

        public a(Context context) {
            this(context, b.a(context, 0));
        }

        public a(Context context, int i) {
            this.f65a = new a.C0003a(new ContextThemeWrapper(context, b.a(context, i)));
            this.f66b = i;
        }

        public Context a() {
            return this.f65a.f53a;
        }

        public a a(CharSequence charSequence) {
            this.f65a.f = charSequence;
            return this;
        }

        public a a(View view) {
            this.f65a.g = view;
            return this;
        }

        public a a(Drawable drawable) {
            this.f65a.d = drawable;
            return this;
        }

        public a a(DialogInterface.OnKeyListener onKeyListener) {
            this.f65a.r = onKeyListener;
            return this;
        }

        public a a(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
            this.f65a.t = listAdapter;
            this.f65a.u = onClickListener;
            return this;
        }

        public b b() {
            b bVar = new b(this.f65a.f53a, this.f66b);
            this.f65a.a(bVar.f64a);
            bVar.setCancelable(this.f65a.o);
            if (this.f65a.o) {
                bVar.setCanceledOnTouchOutside(true);
            }
            bVar.setOnCancelListener(this.f65a.p);
            bVar.setOnDismissListener(this.f65a.q);
            if (this.f65a.r != null) {
                bVar.setOnKeyListener(this.f65a.r);
            }
            return bVar;
        }
    }
}
