package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.ViewPropertyAnimatorUpdateListener;
import android.support.v7.a.a;
import android.support.v7.app.ActionBar;
import android.support.v7.view.b;
import android.support.v7.view.g;
import android.support.v7.view.h;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.widget.ActionBarContainer;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ActionBarOverlayLayout;
import android.support.v7.widget.ScrollingTabContainerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.n;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

/* compiled from: WindowDecorActionBar */
public class t extends ActionBar implements ActionBarOverlayLayout.a {
    static final /* synthetic */ boolean s = (!t.class.desiredAssertionStatus());
    private static final Interpolator t = new AccelerateInterpolator();
    private static final Interpolator u = new DecelerateInterpolator();
    private static final boolean v;
    private int A = -1;
    private boolean B;
    private boolean C;
    private ArrayList<ActionBar.a> D = new ArrayList<>();
    private boolean E;
    private int F = 0;
    private boolean G;
    private boolean H = true;
    private boolean I;

    /* renamed from: a  reason: collision with root package name */
    Context f108a;

    /* renamed from: b  reason: collision with root package name */
    ActionBarOverlayLayout f109b;
    ActionBarContainer c;
    n d;
    ActionBarContextView e;
    View f;
    ScrollingTabContainerView g;
    a h;
    b i;
    b.a j;
    boolean k = true;
    boolean l;
    boolean m;
    h n;
    boolean o;
    final ViewPropertyAnimatorListener p = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            if (t.this.k && t.this.f != null) {
                ViewCompat.setTranslationY(t.this.f, 0.0f);
                ViewCompat.setTranslationY(t.this.c, 0.0f);
            }
            t.this.c.setVisibility(8);
            t.this.c.setTransitioning(false);
            t.this.n = null;
            t.this.i();
            if (t.this.f109b != null) {
                ViewCompat.requestApplyInsets(t.this.f109b);
            }
        }
    };
    final ViewPropertyAnimatorListener q = new ViewPropertyAnimatorListenerAdapter() {
        public void onAnimationEnd(View view) {
            t.this.n = null;
            t.this.c.requestLayout();
        }
    };
    final ViewPropertyAnimatorUpdateListener r = new ViewPropertyAnimatorUpdateListener() {
        public void onAnimationUpdate(View view) {
            ((View) t.this.c.getParent()).invalidate();
        }
    };
    private Context w;
    private Activity x;
    private Dialog y;
    private ArrayList<Object> z = new ArrayList<>();

    static {
        boolean z2 = true;
        if (Build.VERSION.SDK_INT < 14) {
            z2 = false;
        }
        v = z2;
    }

    public t(Activity activity, boolean z2) {
        this.x = activity;
        View decorView = activity.getWindow().getDecorView();
        a(decorView);
        if (!z2) {
            this.f = decorView.findViewById(16908290);
        }
    }

    public t(Dialog dialog) {
        this.y = dialog;
        a(dialog.getWindow().getDecorView());
    }

    private void a(View view) {
        boolean z2;
        this.f109b = (ActionBarOverlayLayout) view.findViewById(a.f.decor_content_parent);
        if (this.f109b != null) {
            this.f109b.setActionBarVisibilityCallback(this);
        }
        this.d = b(view.findViewById(a.f.action_bar));
        this.e = (ActionBarContextView) view.findViewById(a.f.action_context_bar);
        this.c = (ActionBarContainer) view.findViewById(a.f.action_bar_container);
        if (this.d == null || this.e == null || this.c == null) {
            throw new IllegalStateException(getClass().getSimpleName() + " can only be used " + "with a compatible window decor layout");
        }
        this.f108a = this.d.b();
        boolean z3 = (this.d.o() & 4) != 0;
        if (z3) {
            this.B = true;
        }
        android.support.v7.view.a a2 = android.support.v7.view.a.a(this.f108a);
        if (a2.f() || z3) {
            z2 = true;
        } else {
            z2 = false;
        }
        a(z2);
        k(a2.d());
        TypedArray obtainStyledAttributes = this.f108a.obtainStyledAttributes(null, a.k.ActionBar, a.C0002a.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(a.k.ActionBar_hideOnContentScroll, false)) {
            b(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(a.k.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    private n b(View view) {
        if (view instanceof n) {
            return (n) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        throw new IllegalStateException(new StringBuilder().append("Can't make a decor toolbar out of ").append(view).toString() != null ? view.getClass().getSimpleName() : "null");
    }

    public void a(float f2) {
        ViewCompat.setElevation(this.c, f2);
    }

    public void a(Configuration configuration) {
        k(android.support.v7.view.a.a(this.f108a).d());
    }

    private void k(boolean z2) {
        boolean z3;
        boolean z4;
        boolean z5 = true;
        this.E = z2;
        if (!this.E) {
            this.d.a((ScrollingTabContainerView) null);
            this.c.setTabContainer(this.g);
        } else {
            this.c.setTabContainer(null);
            this.d.a(this.g);
        }
        if (j() == 2) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (this.g != null) {
            if (z3) {
                this.g.setVisibility(0);
                if (this.f109b != null) {
                    ViewCompat.requestApplyInsets(this.f109b);
                }
            } else {
                this.g.setVisibility(8);
            }
        }
        n nVar = this.d;
        if (this.E || !z3) {
            z4 = false;
        } else {
            z4 = true;
        }
        nVar.a(z4);
        ActionBarOverlayLayout actionBarOverlayLayout = this.f109b;
        if (this.E || !z3) {
            z5 = false;
        }
        actionBarOverlayLayout.setHasNonEmbeddedTabs(z5);
    }

    /* access modifiers changed from: package-private */
    public void i() {
        if (this.j != null) {
            this.j.a(this.i);
            this.i = null;
            this.j = null;
        }
    }

    public void a(int i2) {
        this.F = i2;
    }

    public void d(boolean z2) {
        this.I = z2;
        if (!z2 && this.n != null) {
            this.n.c();
        }
    }

    public void e(boolean z2) {
        if (z2 != this.C) {
            this.C = z2;
            int size = this.D.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.D.get(i2).a(z2);
            }
        }
    }

    public void f(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    public void a(boolean z2) {
        this.d.b(z2);
    }

    public void a(CharSequence charSequence) {
        this.d.a(charSequence);
    }

    public boolean g() {
        ViewGroup a2 = this.d.a();
        if (a2 == null || a2.hasFocus()) {
            return false;
        }
        a2.requestFocus();
        return true;
    }

    public void a(int i2, int i3) {
        int o2 = this.d.o();
        if ((i3 & 4) != 0) {
            this.B = true;
        }
        this.d.c((o2 & (i3 ^ -1)) | (i2 & i3));
    }

    public int j() {
        return this.d.p();
    }

    public int a() {
        return this.d.o();
    }

    public b a(b.a aVar) {
        if (this.h != null) {
            this.h.c();
        }
        this.f109b.setHideOnContentScrollEnabled(false);
        this.e.c();
        a aVar2 = new a(this.e.getContext(), aVar);
        if (!aVar2.e()) {
            return null;
        }
        this.h = aVar2;
        aVar2.d();
        this.e.a(aVar2);
        j(true);
        this.e.sendAccessibilityEvent(32);
        return aVar2;
    }

    public int k() {
        return this.c.getHeight();
    }

    public void g(boolean z2) {
        this.k = z2;
    }

    private void p() {
        if (!this.G) {
            this.G = true;
            if (this.f109b != null) {
                this.f109b.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    public void l() {
        if (this.m) {
            this.m = false;
            l(true);
        }
    }

    private void q() {
        if (this.G) {
            this.G = false;
            if (this.f109b != null) {
                this.f109b.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    public void m() {
        if (!this.m) {
            this.m = true;
            l(true);
        }
    }

    public void b(boolean z2) {
        if (!z2 || this.f109b.a()) {
            this.o = z2;
            this.f109b.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    public int d() {
        return this.f109b.getActionBarHideOffset();
    }

    static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        if (z2 || z3) {
            return false;
        }
        return true;
    }

    private void l(boolean z2) {
        if (a(this.l, this.m, this.G)) {
            if (!this.H) {
                this.H = true;
                h(z2);
            }
        } else if (this.H) {
            this.H = false;
            i(z2);
        }
    }

    public void h(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        this.c.setVisibility(0);
        if (this.F != 0 || !v || (!this.I && !z2)) {
            ViewCompat.setAlpha(this.c, 1.0f);
            ViewCompat.setTranslationY(this.c, 0.0f);
            if (this.k && this.f != null) {
                ViewCompat.setTranslationY(this.f, 0.0f);
            }
            this.q.onAnimationEnd(null);
        } else {
            ViewCompat.setTranslationY(this.c, 0.0f);
            float f2 = (float) (-this.c.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.c.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            ViewCompat.setTranslationY(this.c, f2);
            h hVar = new h();
            ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.c).translationY(0.0f);
            translationY.setUpdateListener(this.r);
            hVar.a(translationY);
            if (this.k && this.f != null) {
                ViewCompat.setTranslationY(this.f, f2);
                hVar.a(ViewCompat.animate(this.f).translationY(0.0f));
            }
            hVar.a(u);
            hVar.a(250);
            hVar.a(this.q);
            this.n = hVar;
            hVar.a();
        }
        if (this.f109b != null) {
            ViewCompat.requestApplyInsets(this.f109b);
        }
    }

    public void i(boolean z2) {
        if (this.n != null) {
            this.n.c();
        }
        if (this.F != 0 || !v || (!this.I && !z2)) {
            this.p.onAnimationEnd(null);
            return;
        }
        ViewCompat.setAlpha(this.c, 1.0f);
        this.c.setTransitioning(true);
        h hVar = new h();
        float f2 = (float) (-this.c.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.c.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ViewPropertyAnimatorCompat translationY = ViewCompat.animate(this.c).translationY(f2);
        translationY.setUpdateListener(this.r);
        hVar.a(translationY);
        if (this.k && this.f != null) {
            hVar.a(ViewCompat.animate(this.f).translationY(f2));
        }
        hVar.a(t);
        hVar.a(250);
        hVar.a(this.p);
        this.n = hVar;
        hVar.a();
    }

    public boolean b() {
        int k2 = k();
        return this.H && (k2 == 0 || d() < k2);
    }

    public void j(boolean z2) {
        ViewPropertyAnimatorCompat a2;
        ViewPropertyAnimatorCompat a3;
        if (z2) {
            p();
        } else {
            q();
        }
        if (r()) {
            if (z2) {
                a3 = this.d.a(4, 100);
                a2 = this.e.a(0, 200);
            } else {
                a2 = this.d.a(0, 200);
                a3 = this.e.a(8, 100);
            }
            h hVar = new h();
            hVar.a(a3, a2);
            hVar.a();
        } else if (z2) {
            this.d.d(4);
            this.e.setVisibility(0);
        } else {
            this.d.d(0);
            this.e.setVisibility(8);
        }
    }

    private boolean r() {
        return ViewCompat.isLaidOut(this.c);
    }

    public Context c() {
        if (this.w == null) {
            TypedValue typedValue = new TypedValue();
            this.f108a.getTheme().resolveAttribute(a.C0002a.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.w = new ContextThemeWrapper(this.f108a, i2);
            } else {
                this.w = this.f108a;
            }
        }
        return this.w;
    }

    public void n() {
        if (this.n != null) {
            this.n.c();
            this.n = null;
        }
    }

    public void o() {
    }

    public boolean f() {
        if (this.d == null || !this.d.c()) {
            return false;
        }
        this.d.d();
        return true;
    }

    /* compiled from: WindowDecorActionBar */
    public class a extends b implements MenuBuilder.a {

        /* renamed from: b  reason: collision with root package name */
        private final Context f114b;
        private final MenuBuilder c;
        private b.a d;
        private WeakReference<View> e;

        public a(Context context, b.a aVar) {
            this.f114b = context;
            this.d = aVar;
            this.c = new MenuBuilder(context).a(1);
            this.c.a(this);
        }

        public MenuInflater a() {
            return new g(this.f114b);
        }

        public Menu b() {
            return this.c;
        }

        public void c() {
            if (t.this.h == this) {
                if (!t.a(t.this.l, t.this.m, false)) {
                    t.this.i = this;
                    t.this.j = this.d;
                } else {
                    this.d.a(this);
                }
                this.d = null;
                t.this.j(false);
                t.this.e.b();
                t.this.d.a().sendAccessibilityEvent(32);
                t.this.f109b.setHideOnContentScrollEnabled(t.this.o);
                t.this.h = null;
            }
        }

        public void d() {
            if (t.this.h == this) {
                this.c.g();
                try {
                    this.d.b(this, this.c);
                } finally {
                    this.c.h();
                }
            }
        }

        public boolean e() {
            this.c.g();
            try {
                return this.d.a(this, this.c);
            } finally {
                this.c.h();
            }
        }

        public void a(View view) {
            t.this.e.setCustomView(view);
            this.e = new WeakReference<>(view);
        }

        public void a(CharSequence charSequence) {
            t.this.e.setSubtitle(charSequence);
        }

        public void b(CharSequence charSequence) {
            t.this.e.setTitle(charSequence);
        }

        public void a(int i) {
            b(t.this.f108a.getResources().getString(i));
        }

        public void b(int i) {
            a((CharSequence) t.this.f108a.getResources().getString(i));
        }

        public CharSequence f() {
            return t.this.e.getTitle();
        }

        public CharSequence g() {
            return t.this.e.getSubtitle();
        }

        public void a(boolean z) {
            super.a(z);
            t.this.e.setTitleOptional(z);
        }

        public boolean h() {
            return t.this.e.d();
        }

        public View i() {
            if (this.e != null) {
                return this.e.get();
            }
            return null;
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            if (this.d != null) {
                return this.d.a(this, menuItem);
            }
            return false;
        }

        public void a(MenuBuilder menuBuilder) {
            if (this.d != null) {
                d();
                t.this.e.a();
            }
        }
    }

    public void c(boolean z2) {
        if (!this.B) {
            f(z2);
        }
    }
}
