package android.support.v7.b.a;

import java.lang.reflect.Array;

/* compiled from: GrowingArrayUtils */
final class c {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f119a = (!c.class.desiredAssertionStatus());

    public static <T> T[] a(Object[] objArr, int i, Object obj) {
        Object[] objArr2;
        if (f119a || i <= objArr.length) {
            if (i + 1 > objArr.length) {
                objArr2 = (Object[]) Array.newInstance(objArr.getClass().getComponentType(), a(i));
                System.arraycopy(objArr, 0, objArr2, 0, i);
            } else {
                objArr2 = objArr;
            }
            objArr2[i] = obj;
            return objArr2;
        }
        throw new AssertionError();
    }

    public static int[] a(int[] iArr, int i, int i2) {
        if (f119a || i <= iArr.length) {
            if (i + 1 > iArr.length) {
                int[] iArr2 = new int[a(i)];
                System.arraycopy(iArr, 0, iArr2, 0, i);
                iArr = iArr2;
            }
            iArr[i] = i2;
            return iArr;
        }
        throw new AssertionError();
    }

    public static int a(int i) {
        if (i <= 4) {
            return 8;
        }
        return i * 2;
    }

    private c() {
    }
}
