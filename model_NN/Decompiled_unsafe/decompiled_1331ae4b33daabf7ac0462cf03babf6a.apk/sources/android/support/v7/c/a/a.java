package android.support.v7.c.a;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;

/* compiled from: DrawableWrapper */
public class a extends Drawable implements Drawable.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f120a;

    public a(Drawable drawable) {
        a(drawable);
    }

    public void draw(Canvas canvas) {
        this.f120a.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        this.f120a.setBounds(rect);
    }

    public void setChangingConfigurations(int i) {
        this.f120a.setChangingConfigurations(i);
    }

    public int getChangingConfigurations() {
        return this.f120a.getChangingConfigurations();
    }

    public void setDither(boolean z) {
        this.f120a.setDither(z);
    }

    public void setFilterBitmap(boolean z) {
        this.f120a.setFilterBitmap(z);
    }

    public void setAlpha(int i) {
        this.f120a.setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.f120a.setColorFilter(colorFilter);
    }

    public boolean isStateful() {
        return this.f120a.isStateful();
    }

    public boolean setState(int[] iArr) {
        return this.f120a.setState(iArr);
    }

    public int[] getState() {
        return this.f120a.getState();
    }

    public void jumpToCurrentState() {
        DrawableCompat.jumpToCurrentState(this.f120a);
    }

    public Drawable getCurrent() {
        return this.f120a.getCurrent();
    }

    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.f120a.setVisible(z, z2);
    }

    public int getOpacity() {
        return this.f120a.getOpacity();
    }

    public Region getTransparentRegion() {
        return this.f120a.getTransparentRegion();
    }

    public int getIntrinsicWidth() {
        return this.f120a.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        return this.f120a.getIntrinsicHeight();
    }

    public int getMinimumWidth() {
        return this.f120a.getMinimumWidth();
    }

    public int getMinimumHeight() {
        return this.f120a.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        return this.f120a.getPadding(rect);
    }

    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        return this.f120a.setLevel(i);
    }

    public void setAutoMirrored(boolean z) {
        DrawableCompat.setAutoMirrored(this.f120a, z);
    }

    public boolean isAutoMirrored() {
        return DrawableCompat.isAutoMirrored(this.f120a);
    }

    public void setTint(int i) {
        DrawableCompat.setTint(this.f120a, i);
    }

    public void setTintList(ColorStateList colorStateList) {
        DrawableCompat.setTintList(this.f120a, colorStateList);
    }

    public void setTintMode(PorterDuff.Mode mode) {
        DrawableCompat.setTintMode(this.f120a, mode);
    }

    public void setHotspot(float f, float f2) {
        DrawableCompat.setHotspot(this.f120a, f, f2);
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        DrawableCompat.setHotspotBounds(this.f120a, i, i2, i3, i4);
    }

    public Drawable a() {
        return this.f120a;
    }

    public void a(Drawable drawable) {
        if (this.f120a != null) {
            this.f120a.setCallback(null);
        }
        this.f120a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
