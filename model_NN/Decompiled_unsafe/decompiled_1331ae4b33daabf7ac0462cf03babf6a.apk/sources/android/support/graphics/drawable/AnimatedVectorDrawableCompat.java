package android.support.graphics.drawable;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.ArrayMap;
import android.util.AttributeSet;
import java.io.IOException;
import java.util.ArrayList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@TargetApi(21)
public class AnimatedVectorDrawableCompat extends d implements Animatable {

    /* renamed from: a  reason: collision with root package name */
    final Drawable.Callback f1a;
    private a c;
    private Context d;
    private ArgbEvaluator e;

    public /* bridge */ /* synthetic */ void clearColorFilter() {
        super.clearColorFilter();
    }

    public /* bridge */ /* synthetic */ ColorFilter getColorFilter() {
        return super.getColorFilter();
    }

    public /* bridge */ /* synthetic */ Drawable getCurrent() {
        return super.getCurrent();
    }

    public /* bridge */ /* synthetic */ int getMinimumHeight() {
        return super.getMinimumHeight();
    }

    public /* bridge */ /* synthetic */ int getMinimumWidth() {
        return super.getMinimumWidth();
    }

    public /* bridge */ /* synthetic */ boolean getPadding(Rect rect) {
        return super.getPadding(rect);
    }

    public /* bridge */ /* synthetic */ int[] getState() {
        return super.getState();
    }

    public /* bridge */ /* synthetic */ Region getTransparentRegion() {
        return super.getTransparentRegion();
    }

    public /* bridge */ /* synthetic */ void jumpToCurrentState() {
        super.jumpToCurrentState();
    }

    public /* bridge */ /* synthetic */ void setChangingConfigurations(int i) {
        super.setChangingConfigurations(i);
    }

    public /* bridge */ /* synthetic */ void setColorFilter(int i, PorterDuff.Mode mode) {
        super.setColorFilter(i, mode);
    }

    public /* bridge */ /* synthetic */ void setFilterBitmap(boolean z) {
        super.setFilterBitmap(z);
    }

    public /* bridge */ /* synthetic */ void setHotspot(float f, float f2) {
        super.setHotspot(f, f2);
    }

    public /* bridge */ /* synthetic */ void setHotspotBounds(int i, int i2, int i3, int i4) {
        super.setHotspotBounds(i, i2, i3, i4);
    }

    public /* bridge */ /* synthetic */ boolean setState(int[] iArr) {
        return super.setState(iArr);
    }

    AnimatedVectorDrawableCompat() {
        this(null, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context) {
        this(context, null, null);
    }

    private AnimatedVectorDrawableCompat(Context context, a aVar, Resources resources) {
        this.e = null;
        this.f1a = new Drawable.Callback() {
            public void invalidateDrawable(Drawable drawable) {
                AnimatedVectorDrawableCompat.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                AnimatedVectorDrawableCompat.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                AnimatedVectorDrawableCompat.this.unscheduleSelf(runnable);
            }
        };
        this.d = context;
        if (aVar != null) {
            this.c = aVar;
        } else {
            this.c = new a(context, aVar, this.f1a, resources);
        }
    }

    public Drawable mutate() {
        if (this.f22b != null) {
            this.f22b.mutate();
            return this;
        }
        throw new IllegalStateException("Mutate() is not supported for older platform");
    }

    public static AnimatedVectorDrawableCompat a(Context context, Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat(context);
        animatedVectorDrawableCompat.inflate(resources, xmlPullParser, attributeSet, theme);
        return animatedVectorDrawableCompat;
    }

    public Drawable.ConstantState getConstantState() {
        if (this.f22b != null) {
            return new b(this.f22b.getConstantState());
        }
        return null;
    }

    public int getChangingConfigurations() {
        if (this.f22b != null) {
            return this.f22b.getChangingConfigurations();
        }
        return super.getChangingConfigurations() | this.c.f3a;
    }

    public void draw(Canvas canvas) {
        if (this.f22b != null) {
            this.f22b.draw(canvas);
            return;
        }
        this.c.f4b.draw(canvas);
        if (a()) {
            invalidateSelf();
        }
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f22b != null) {
            this.f22b.setBounds(rect);
        } else {
            this.c.f4b.setBounds(rect);
        }
    }

    /* access modifiers changed from: protected */
    public boolean onStateChange(int[] iArr) {
        if (this.f22b != null) {
            return this.f22b.setState(iArr);
        }
        return this.c.f4b.setState(iArr);
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        if (this.f22b != null) {
            return this.f22b.setLevel(i);
        }
        return this.c.f4b.setLevel(i);
    }

    public int getAlpha() {
        if (this.f22b != null) {
            return DrawableCompat.getAlpha(this.f22b);
        }
        return this.c.f4b.getAlpha();
    }

    public void setAlpha(int i) {
        if (this.f22b != null) {
            this.f22b.setAlpha(i);
        } else {
            this.c.f4b.setAlpha(i);
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        if (this.f22b != null) {
            this.f22b.setColorFilter(colorFilter);
        } else {
            this.c.f4b.setColorFilter(colorFilter);
        }
    }

    public void setTint(int i) {
        if (this.f22b != null) {
            DrawableCompat.setTint(this.f22b, i);
        } else {
            this.c.f4b.setTint(i);
        }
    }

    public void setTintList(ColorStateList colorStateList) {
        if (this.f22b != null) {
            DrawableCompat.setTintList(this.f22b, colorStateList);
        } else {
            this.c.f4b.setTintList(colorStateList);
        }
    }

    public void setTintMode(PorterDuff.Mode mode) {
        if (this.f22b != null) {
            DrawableCompat.setTintMode(this.f22b, mode);
        } else {
            this.c.f4b.setTintMode(mode);
        }
    }

    public boolean setVisible(boolean z, boolean z2) {
        if (this.f22b != null) {
            return this.f22b.setVisible(z, z2);
        }
        this.c.f4b.setVisible(z, z2);
        return super.setVisible(z, z2);
    }

    public boolean isStateful() {
        if (this.f22b != null) {
            return this.f22b.isStateful();
        }
        return this.c.f4b.isStateful();
    }

    public int getOpacity() {
        if (this.f22b != null) {
            return this.f22b.getOpacity();
        }
        return this.c.f4b.getOpacity();
    }

    public int getIntrinsicWidth() {
        if (this.f22b != null) {
            return this.f22b.getIntrinsicWidth();
        }
        return this.c.f4b.getIntrinsicWidth();
    }

    public int getIntrinsicHeight() {
        if (this.f22b != null) {
            return this.f22b.getIntrinsicHeight();
        }
        return this.c.f4b.getIntrinsicHeight();
    }

    public boolean isAutoMirrored() {
        if (this.f22b != null) {
            return DrawableCompat.isAutoMirrored(this.f22b);
        }
        return this.c.f4b.isAutoMirrored();
    }

    public void setAutoMirrored(boolean z) {
        if (this.f22b != null) {
            this.f22b.setAutoMirrored(z);
        } else {
            this.c.f4b.setAutoMirrored(z);
        }
    }

    static TypedArray a(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws XmlPullParserException, IOException {
        if (this.f22b != null) {
            DrawableCompat.inflate(this.f22b, resources, xmlPullParser, attributeSet, theme);
            return;
        }
        int eventType = xmlPullParser.getEventType();
        int depth = xmlPullParser.getDepth() + 1;
        while (eventType != 1) {
            if (xmlPullParser.getDepth() >= depth || eventType != 3) {
                if (eventType == 2) {
                    String name = xmlPullParser.getName();
                    if ("animated-vector".equals(name)) {
                        TypedArray a2 = a(resources, theme, attributeSet, a.e);
                        int resourceId = a2.getResourceId(0, 0);
                        if (resourceId != 0) {
                            VectorDrawableCompat a3 = VectorDrawableCompat.a(resources, resourceId, theme);
                            a3.a(false);
                            a3.setCallback(this.f1a);
                            if (this.c.f4b != null) {
                                this.c.f4b.setCallback(null);
                            }
                            this.c.f4b = a3;
                        }
                        a2.recycle();
                    } else if ("target".equals(name)) {
                        TypedArray obtainAttributes = resources.obtainAttributes(attributeSet, a.f);
                        String string = obtainAttributes.getString(0);
                        int resourceId2 = obtainAttributes.getResourceId(1, 0);
                        if (resourceId2 != 0) {
                            if (this.d != null) {
                                a(string, AnimatorInflater.loadAnimator(this.d, resourceId2));
                            } else {
                                throw new IllegalStateException("Context can't be null when inflating animators");
                            }
                        }
                        obtainAttributes.recycle();
                    } else {
                        continue;
                    }
                }
                eventType = xmlPullParser.next();
            } else {
                return;
            }
        }
    }

    public void inflate(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet) throws XmlPullParserException, IOException {
        inflate(resources, xmlPullParser, attributeSet, null);
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f22b != null) {
            DrawableCompat.applyTheme(this.f22b, theme);
        }
    }

    public boolean canApplyTheme() {
        if (this.f22b != null) {
            return DrawableCompat.canApplyTheme(this.f22b);
        }
        return false;
    }

    private static class b extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        private final Drawable.ConstantState f5a;

        public b(Drawable.ConstantState constantState) {
            this.f5a = constantState;
        }

        public Drawable newDrawable() {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f22b = this.f5a.newDrawable();
            animatedVectorDrawableCompat.f22b.setCallback(animatedVectorDrawableCompat.f1a);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f22b = this.f5a.newDrawable(resources);
            animatedVectorDrawableCompat.f22b.setCallback(animatedVectorDrawableCompat.f1a);
            return animatedVectorDrawableCompat;
        }

        public Drawable newDrawable(Resources resources, Resources.Theme theme) {
            AnimatedVectorDrawableCompat animatedVectorDrawableCompat = new AnimatedVectorDrawableCompat();
            animatedVectorDrawableCompat.f22b = this.f5a.newDrawable(resources, theme);
            animatedVectorDrawableCompat.f22b.setCallback(animatedVectorDrawableCompat.f1a);
            return animatedVectorDrawableCompat;
        }

        public boolean canApplyTheme() {
            return this.f5a.canApplyTheme();
        }

        public int getChangingConfigurations() {
            return this.f5a.getChangingConfigurations();
        }
    }

    private static class a extends Drawable.ConstantState {

        /* renamed from: a  reason: collision with root package name */
        int f3a;

        /* renamed from: b  reason: collision with root package name */
        VectorDrawableCompat f4b;
        ArrayList<Animator> c;
        ArrayMap<Animator, String> d;

        public a(Context context, a aVar, Drawable.Callback callback, Resources resources) {
            if (aVar != null) {
                this.f3a = aVar.f3a;
                if (aVar.f4b != null) {
                    Drawable.ConstantState constantState = aVar.f4b.getConstantState();
                    if (resources != null) {
                        this.f4b = (VectorDrawableCompat) constantState.newDrawable(resources);
                    } else {
                        this.f4b = (VectorDrawableCompat) constantState.newDrawable();
                    }
                    this.f4b = (VectorDrawableCompat) this.f4b.mutate();
                    this.f4b.setCallback(callback);
                    this.f4b.setBounds(aVar.f4b.getBounds());
                    this.f4b.a(false);
                }
                if (aVar.c != null) {
                    int size = aVar.c.size();
                    this.c = new ArrayList<>(size);
                    this.d = new ArrayMap<>(size);
                    for (int i = 0; i < size; i++) {
                        Animator animator = aVar.c.get(i);
                        Animator clone = animator.clone();
                        String str = aVar.d.get(animator);
                        clone.setTarget(this.f4b.a(str));
                        this.c.add(clone);
                        this.d.put(clone, str);
                    }
                }
            }
        }

        public Drawable newDrawable() {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public Drawable newDrawable(Resources resources) {
            throw new IllegalStateException("No constant state support for SDK < 24.");
        }

        public int getChangingConfigurations() {
            return this.f3a;
        }
    }

    private void a(Animator animator) {
        ArrayList<Animator> childAnimations;
        if ((animator instanceof AnimatorSet) && (childAnimations = ((AnimatorSet) animator).getChildAnimations()) != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= childAnimations.size()) {
                    break;
                }
                a(childAnimations.get(i2));
                i = i2 + 1;
            }
        }
        if (animator instanceof ObjectAnimator) {
            ObjectAnimator objectAnimator = (ObjectAnimator) animator;
            String propertyName = objectAnimator.getPropertyName();
            if ("fillColor".equals(propertyName) || "strokeColor".equals(propertyName)) {
                if (this.e == null) {
                    this.e = new ArgbEvaluator();
                }
                objectAnimator.setEvaluator(this.e);
            }
        }
    }

    private void a(String str, Animator animator) {
        animator.setTarget(this.c.f4b.a(str));
        if (Build.VERSION.SDK_INT < 21) {
            a(animator);
        }
        if (this.c.c == null) {
            this.c.c = new ArrayList<>();
            this.c.d = new ArrayMap<>();
        }
        this.c.c.add(animator);
        this.c.d.put(animator, str);
    }

    public boolean isRunning() {
        if (this.f22b != null) {
            return ((AnimatedVectorDrawable) this.f22b).isRunning();
        }
        ArrayList<Animator> arrayList = this.c.c;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (arrayList.get(i).isRunning()) {
                return true;
            }
        }
        return false;
    }

    private boolean a() {
        ArrayList<Animator> arrayList = this.c.c;
        if (arrayList == null) {
            return false;
        }
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (arrayList.get(i).isRunning()) {
                return true;
            }
        }
        return false;
    }

    public void start() {
        if (this.f22b != null) {
            ((AnimatedVectorDrawable) this.f22b).start();
        } else if (!a()) {
            ArrayList<Animator> arrayList = this.c.c;
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                arrayList.get(i).start();
            }
            invalidateSelf();
        }
    }

    public void stop() {
        if (this.f22b != null) {
            ((AnimatedVectorDrawable) this.f22b).stop();
            return;
        }
        ArrayList<Animator> arrayList = this.c.c;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            arrayList.get(i).end();
        }
    }
}
