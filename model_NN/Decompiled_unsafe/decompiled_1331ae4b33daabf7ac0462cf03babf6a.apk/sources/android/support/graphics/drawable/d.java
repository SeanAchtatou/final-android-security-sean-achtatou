package android.support.graphics.drawable;

import android.annotation.TargetApi;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.graphics.drawable.TintAwareDrawable;
import android.util.AttributeSet;

@TargetApi(21)
/* compiled from: VectorDrawableCommon */
abstract class d extends Drawable implements TintAwareDrawable {

    /* renamed from: b  reason: collision with root package name */
    Drawable f22b;

    d() {
    }

    static TypedArray b(Resources resources, Resources.Theme theme, AttributeSet attributeSet, int[] iArr) {
        if (theme == null) {
            return resources.obtainAttributes(attributeSet, iArr);
        }
        return theme.obtainStyledAttributes(attributeSet, iArr, 0, 0);
    }

    public void setColorFilter(int i, PorterDuff.Mode mode) {
        if (this.f22b != null) {
            this.f22b.setColorFilter(i, mode);
        } else {
            super.setColorFilter(i, mode);
        }
    }

    public ColorFilter getColorFilter() {
        if (this.f22b != null) {
            return DrawableCompat.getColorFilter(this.f22b);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public boolean onLevelChange(int i) {
        if (this.f22b != null) {
            return this.f22b.setLevel(i);
        }
        return super.onLevelChange(i);
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        if (this.f22b != null) {
            this.f22b.setBounds(rect);
        } else {
            super.onBoundsChange(rect);
        }
    }

    public void setHotspot(float f, float f2) {
        if (this.f22b != null) {
            DrawableCompat.setHotspot(this.f22b, f, f2);
        }
    }

    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        if (this.f22b != null) {
            DrawableCompat.setHotspotBounds(this.f22b, i, i2, i3, i4);
        }
    }

    public void setFilterBitmap(boolean z) {
        if (this.f22b != null) {
            this.f22b.setFilterBitmap(z);
        }
    }

    public void jumpToCurrentState() {
        if (this.f22b != null) {
            DrawableCompat.jumpToCurrentState(this.f22b);
        }
    }

    public void applyTheme(Resources.Theme theme) {
        if (this.f22b != null) {
            DrawableCompat.applyTheme(this.f22b, theme);
        }
    }

    public void clearColorFilter() {
        if (this.f22b != null) {
            this.f22b.clearColorFilter();
        } else {
            super.clearColorFilter();
        }
    }

    public Drawable getCurrent() {
        if (this.f22b != null) {
            return this.f22b.getCurrent();
        }
        return super.getCurrent();
    }

    public int getMinimumWidth() {
        if (this.f22b != null) {
            return this.f22b.getMinimumWidth();
        }
        return super.getMinimumWidth();
    }

    public int getMinimumHeight() {
        if (this.f22b != null) {
            return this.f22b.getMinimumHeight();
        }
        return super.getMinimumHeight();
    }

    public boolean getPadding(Rect rect) {
        if (this.f22b != null) {
            return this.f22b.getPadding(rect);
        }
        return super.getPadding(rect);
    }

    public int[] getState() {
        if (this.f22b != null) {
            return this.f22b.getState();
        }
        return super.getState();
    }

    public Region getTransparentRegion() {
        if (this.f22b != null) {
            return this.f22b.getTransparentRegion();
        }
        return super.getTransparentRegion();
    }

    public void setChangingConfigurations(int i) {
        if (this.f22b != null) {
            this.f22b.setChangingConfigurations(i);
        } else {
            super.setChangingConfigurations(i);
        }
    }

    public boolean setState(int[] iArr) {
        if (this.f22b != null) {
            return this.f22b.setState(iArr);
        }
        return super.setState(iArr);
    }
}
