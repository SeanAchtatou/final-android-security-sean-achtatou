package com.baidu.BaiduMap.base;

public class YNeedPayCallbackInfo {
    protected OnNeedPayReceiver payReceiverListener;

    public interface OnNeedPayReceiver {
        void OnNeedPayReceiver(int i, String str);
    }

    public YNeedPayCallbackInfo(OnNeedPayReceiver _payReceiverListener) {
        this.payReceiverListener = _payReceiverListener;
    }

    public void postPayReceiver(int state, String price) {
        if (this.payReceiverListener != null) {
            this.payReceiverListener.OnNeedPayReceiver(state, price);
        }
    }

    public void setPayReceiverListener(OnNeedPayReceiver payReceiverListener2) {
        this.payReceiverListener = payReceiverListener2;
    }
}
