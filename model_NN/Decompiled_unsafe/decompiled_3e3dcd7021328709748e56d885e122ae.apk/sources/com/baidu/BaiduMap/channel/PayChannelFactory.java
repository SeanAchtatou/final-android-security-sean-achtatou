package com.baidu.BaiduMap.channel;

import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;

public class PayChannelFactory {
    public static final BasePayChannel getPayChannelByChannelId(int channelId) {
        Log.i(CrashHandler.TAG, "当前支付通道：" + channelId);
        BasePayChannel payChannel = new ChannelForDefault();
        Log.i(CrashHandler.TAG, "通道：default");
        return payChannel;
    }
}
