package com.baidu.BaiduMap.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

public class PayProgressDialog {
    static ProgressDialog progressDialog;

    public static void show(Context context) {
        if (progressDialog == null) {
            progressDialog = ProgressDialog.show(context, null, "请稍等...", true, false);
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    PayProgressDialog.progressDialog.dismiss();
                    PayProgressDialog.progressDialog = null;
                }
            }, 5000);
        }
    }
}
