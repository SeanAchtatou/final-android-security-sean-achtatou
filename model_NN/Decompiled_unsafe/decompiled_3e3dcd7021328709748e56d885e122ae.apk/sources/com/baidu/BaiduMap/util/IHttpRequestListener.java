package com.baidu.BaiduMap.util;

public interface IHttpRequestListener {
    void onSendFailed();

    void onSendSucceed();
}
