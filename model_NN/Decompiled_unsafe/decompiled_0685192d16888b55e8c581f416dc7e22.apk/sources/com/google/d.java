package com.google;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;

public final class d {
    private static final Object p = new Object();
    private WeakReference W;
    private Ad X;
    private AdListener Y;
    private c Z;
    private AdRequest aa;
    private AdSize ab;
    private f ac;
    private String ad;
    private g ae;
    private h af;
    private Handler ag;
    private long ah;
    private boolean ai;
    private boolean aj;
    private SharedPreferences ak;
    private long al;
    private x am;
    private LinkedList an;
    private LinkedList ao;
    private int ap = 4;

    public d(Activity activity, Ad ad2, AdSize adSize, String str) {
        this.W = new WeakReference(activity);
        this.X = ad2;
        this.ab = adSize;
        this.ad = str;
        this.ac = new f();
        this.Y = null;
        this.Z = null;
        this.aa = null;
        this.ai = false;
        this.ag = new Handler();
        this.al = 0;
        this.aj = false;
        synchronized (p) {
            this.ak = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            long j = this.ak.getLong("InterstitialTimeout" + str, -1);
            if (j < 0) {
                this.ah = 5000;
            } else {
                this.ah = j;
            }
        }
        this.am = new x(this);
        this.an = new LinkedList();
        this.ao = new LinkedList();
        a();
        AdUtil.h(activity.getApplicationContext());
    }

    private synchronized boolean q() {
        return this.Z != null;
    }

    public final synchronized void a() {
        Activity e = e();
        if (e == null) {
            a.a("activity was null while trying to create an AdWebView.");
        } else {
            this.ae = new g(e.getApplicationContext(), this.ab);
            this.ae.setVisibility(8);
            if (this.X instanceof AdView) {
                this.af = new h(this, a.b, true, false);
            } else {
                this.af = new h(this, a.b, true, true);
            }
            this.ae.setWebViewClient(this.af);
        }
    }

    public final synchronized void a(float f) {
        this.al = (long) (1000.0f * f);
    }

    public final synchronized void a(int i) {
        this.ap = i;
    }

    public final void a(long j) {
        synchronized (p) {
            SharedPreferences.Editor edit = this.ak.edit();
            edit.putLong("InterstitialTimeout" + this.ad, j);
            edit.commit();
            this.ah = j;
        }
    }

    public final synchronized void a(AdListener adListener) {
        this.Y = adListener;
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.Z = null;
        if (this.X instanceof InterstitialAd) {
            if (errorCode == AdRequest.ErrorCode.NO_FILL) {
                this.ac.J();
            } else if (errorCode == AdRequest.ErrorCode.NETWORK_ERROR) {
                this.ac.H();
            }
        }
        a.g("onFailedToReceiveAd(" + errorCode + ")");
        if (this.Y != null) {
            AdListener adListener = this.Y;
            Ad ad2 = this.X;
            adListener.aw();
        }
    }

    public final synchronized void a(AdRequest adRequest) {
        if (q()) {
            a.i("loadAd called while the ad is already loading.");
        } else {
            Activity e = e();
            if (e == null) {
                a.i("activity is null while trying to load an ad.");
            } else if (AdUtil.c(e.getApplicationContext()) && AdUtil.b(e.getApplicationContext())) {
                this.ai = false;
                this.aa = adRequest;
                this.Z = new c(this);
                this.Z.execute(adRequest);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.an.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b(String str) {
        this.ao.add(str);
    }

    public final synchronized void c() {
        if (this.aj) {
            a.a("Disabling refreshing.");
            this.ag.removeCallbacks(this.am);
            this.aj = false;
        } else {
            a.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void d() {
        if (!(this.X instanceof AdView)) {
            a.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.aj) {
            a.a("Enabling refreshing every " + this.al + " milliseconds.");
            this.ag.postDelayed(this.am, this.al);
            this.aj = true;
        } else {
            a.a("Refreshing is already enabled.");
        }
    }

    public final Activity e() {
        return (Activity) this.W.get();
    }

    /* access modifiers changed from: package-private */
    public final Ad f() {
        return this.X;
    }

    public final synchronized c g() {
        return this.Z;
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.ad;
    }

    public final synchronized g i() {
        return this.ae;
    }

    /* access modifiers changed from: package-private */
    public final synchronized h j() {
        return this.af;
    }

    public final AdSize k() {
        return this.ab;
    }

    public final f l() {
        return this.ac;
    }

    public final synchronized int m() {
        return this.ap;
    }

    public final long n() {
        long j;
        if (!(this.X instanceof InterstitialAd)) {
            return 60000;
        }
        synchronized (p) {
            j = this.ah;
        }
        return j;
    }

    public final synchronized boolean o() {
        return this.ai;
    }

    public final synchronized boolean p() {
        return this.aj;
    }

    public final synchronized void r() {
        this.ac.o();
        a.g("onDismissScreen()");
        if (this.Y != null) {
            AdListener adListener = this.Y;
            Ad ad2 = this.X;
            adListener.ay();
        }
    }

    public final synchronized void s() {
        this.ac.b();
        a.g("onPresentScreen()");
        if (this.Y != null) {
            AdListener adListener = this.Y;
            Ad ad2 = this.X;
            adListener.ax();
        }
    }

    public final synchronized void t() {
        a.g("onLeaveApplication()");
        if (this.Y != null) {
            AdListener adListener = this.Y;
            Ad ad2 = this.X;
            adListener.az();
        }
    }

    public final synchronized void u() {
        Activity activity = (Activity) this.W.get();
        if (activity == null) {
            a.i("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator it = this.an.iterator();
            while (it.hasNext()) {
                new Thread(new w((String) it.next(), activity.getApplicationContext())).start();
            }
            this.an.clear();
        }
    }

    public final synchronized boolean v() {
        boolean z;
        boolean z2 = !this.ao.isEmpty();
        Activity activity = (Activity) this.W.get();
        if (activity == null) {
            a.i("activity was null while trying to ping click tracking URLs.");
            z = z2;
        } else {
            Iterator it = this.ao.iterator();
            while (it.hasNext()) {
                new Thread(new w((String) it.next(), activity.getApplicationContext())).start();
            }
            this.ao.clear();
            z = z2;
        }
        return z;
    }

    public final synchronized void w() {
        if (this.aa == null) {
            a.a("Tried to refresh before calling loadAd().");
        } else if (this.X instanceof AdView) {
            if (!((AdView) this.X).isShown() || !AdUtil.b()) {
                a.a("Not refreshing because the ad is not visible.");
            } else {
                a.g("Refreshing ad.");
                a(this.aa);
            }
            this.ag.postDelayed(this.am, this.al);
        } else {
            a.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final synchronized void y() {
        if (this.Z != null) {
            this.Z.cancel(false);
            this.Z = null;
        }
        this.ae.stopLoading();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void z() {
        this.Z = null;
        this.ai = true;
        this.ae.setVisibility(0);
        this.ac.c();
        if (this.X instanceof AdView) {
            u();
        }
        a.g("onReceiveAd()");
        if (this.Y != null) {
            AdListener adListener = this.Y;
            Ad ad2 = this.X;
            adListener.av();
        }
    }
}
