package com.google;

import android.content.Context;
import android.os.AsyncTask;
import com.google.ads.AdRequest;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

public final class b extends AsyncTask {
    private c g;
    private d h;
    private Context i;

    b(c cVar, d dVar, Context context) {
        this.g = cVar;
        this.h = dVar;
        this.i = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public Void doInBackground(String... strArr) {
        HttpURLConnection httpURLConnection;
        int responseCode;
        String str = strArr[0];
        while (true) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
                AdUtil.a(httpURLConnection, this.i);
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    str = httpURLConnection.getHeaderField("Location");
                    if (str == null) {
                        a.g("Could not get redirect location from a " + responseCode + " redirect.");
                        this.g.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                        return null;
                    }
                    a(httpURLConnection);
                }
            } catch (MalformedURLException e) {
                a.a("Received malformed ad url from javascript.", e);
                this.g.a(AdRequest.ErrorCode.INTERNAL_ERROR);
                return null;
            } catch (IOException e2) {
                a.c("IOException connecting to ad url.", e2);
                this.g.a(AdRequest.ErrorCode.NETWORK_ERROR);
                return null;
            }
        }
        if (responseCode == 200) {
            a(httpURLConnection);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), ap.GAME_D_PRESSED);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append("\n");
            }
            String sb2 = sb.toString();
            a.a("Response content is: " + sb2);
            if (sb2 == null || sb2.trim().length() <= 0) {
                a.a("Response message is null or zero length: " + sb2);
                this.g.a(AdRequest.ErrorCode.NO_FILL);
                return null;
            }
            this.g.a(sb2, str);
            return null;
        } else if (responseCode == 400) {
            a.g("Bad request");
            this.g.a(AdRequest.ErrorCode.INVALID_REQUEST);
            return null;
        } else {
            a.g("Invalid response code: " + responseCode);
            this.g.a(AdRequest.ErrorCode.INTERNAL_ERROR);
            return null;
        }
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.h.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                this.h.a(Float.parseFloat(headerField2));
                if (!this.h.p()) {
                    this.h.d();
                }
            } catch (NumberFormatException e) {
                a.c("Could not get refresh value: " + headerField2, e);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.h.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e2) {
                a.c("Could not get timeout value: " + headerField3, e2);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 == null) {
            return;
        }
        if (headerField4.equals("portrait")) {
            this.h.a(1);
        } else if (headerField4.equals("landscape")) {
            this.h.a(0);
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.h.b(stringTokenizer.nextToken());
            }
        }
    }
}
