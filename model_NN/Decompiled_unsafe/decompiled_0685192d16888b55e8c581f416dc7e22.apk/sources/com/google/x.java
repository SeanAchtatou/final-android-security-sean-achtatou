package com.google;

import com.google.ads.util.a;
import java.lang.ref.WeakReference;

public final class x implements Runnable {
    private WeakReference aB;

    public x(d dVar) {
        this.aB = new WeakReference(dVar);
    }

    public final void run() {
        d dVar = (d) this.aB.get();
        if (dVar == null) {
            a.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            dVar.w();
        }
    }
}
