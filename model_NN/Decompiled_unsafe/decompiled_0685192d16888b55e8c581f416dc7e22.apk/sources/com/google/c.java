package com.google;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.InterstitialAd;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public final class c extends AsyncTask {
    String a = null;
    public String b = null;
    public AdRequest.ErrorCode c = null;
    private String j;
    private b k;
    private d l;
    private WebView m;
    private boolean n = false;
    private boolean o = false;

    final class a extends Exception {
        public a(String str) {
            super(str);
        }
    }

    final class b extends Exception {
        public b(String str) {
            super(str);
        }
    }

    public c(d dVar) {
        this.l = dVar;
        Activity e = dVar.e();
        if (e != null) {
            this.m = new WebView(e.getApplicationContext());
            this.m.getSettings().setJavaScriptEnabled(true);
            this.m.setWebViewClient(new h(dVar, a.a, false, false));
            AdUtil.a(this.m);
            this.m.setVisibility(8);
            this.m.setWillNotDraw(true);
            this.k = new b(this, dVar, e.getApplicationContext());
            return;
        }
        this.m = null;
        this.k = null;
        com.google.ads.util.a.i("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.ads.AdRequest.ErrorCode doInBackground(com.google.ads.AdRequest... r14) {
        /*
            r13 = this;
            r11 = 0
            r10 = 0
            monitor-enter(r13)
            android.webkit.WebView r0 = r13.m     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x000c
            com.google.b r0 = r13.k     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x0015
        L_0x000c:
            java.lang.String r0 = "adRequestWebView was null while trying to load an ad."
            com.google.ads.util.a.i(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
        L_0x0014:
            return r0
        L_0x0015:
            r0 = 0
            r0 = r14[r0]     // Catch:{ all -> 0x0029 }
            com.google.d r1 = r13.l     // Catch:{ all -> 0x0029 }
            android.app.Activity r1 = r1.e()     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x002c
            java.lang.String r0 = "activity was null while forming an ad request."
            com.google.ads.util.a.i(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0029:
            r0 = move-exception
            monitor-exit(r13)
            throw r0
        L_0x002c:
            java.lang.String r2 = r13.a(r0, r1)     // Catch:{ b -> 0x0054, a -> 0x005e }
            android.webkit.WebView r0 = r13.m     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            com.google.d r0 = r13.l     // Catch:{ all -> 0x0029 }
            long r6 = r0.n()     // Catch:{ all -> 0x0029 }
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            int r0 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r0 <= 0) goto L_0x004c
            r13.wait(r6)     // Catch:{ InterruptedException -> 0x0068 }
        L_0x004c:
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x0083
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0054:
            r0 = move-exception
            java.lang.String r1 = "Unable to connect to network."
            com.google.ads.util.a.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x005e:
            r0 = move-exception
            java.lang.String r1 = "Caught internal exception."
            com.google.ads.util.a.b(r1, r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0068:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the URL: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.i(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x0083:
            java.lang.String r0 = r13.b     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x00a8
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the URL."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.g(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00a8:
            r0 = 1
            java.lang.String[] r0 = new java.lang.String[r0]     // Catch:{ all -> 0x0029 }
            r1 = 0
            java.lang.String r2 = r13.b     // Catch:{ all -> 0x0029 }
            r0[r1] = r2     // Catch:{ all -> 0x0029 }
            r13.publishProgress(r0)     // Catch:{ all -> 0x0029 }
            long r0 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r0 = r0 - r8
            long r0 = r6 - r0
            int r2 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r2 <= 0) goto L_0x00c1
            r13.wait(r0)     // Catch:{ InterruptedException -> 0x00ca }
        L_0x00c1:
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            if (r0 == 0) goto L_0x00e6
            com.google.ads.AdRequest$ErrorCode r0 = r13.c     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00ca:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r1.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = "AdLoader InterruptedException while getting the HTML: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.i(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x00e6:
            java.lang.String r0 = r13.a     // Catch:{ all -> 0x0029 }
            if (r0 != 0) goto L_0x010b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while getting the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.g(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x010b:
            com.google.d r0 = r13.l     // Catch:{ all -> 0x0029 }
            com.google.g r0 = r0.i()     // Catch:{ all -> 0x0029 }
            com.google.d r1 = r13.l     // Catch:{ all -> 0x0029 }
            com.google.h r1 = r1.j()     // Catch:{ all -> 0x0029 }
            r1.a()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r13.j     // Catch:{ all -> 0x0029 }
            java.lang.String r2 = r13.a     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "text/html"
            java.lang.String r4 = "utf-8"
            r5 = 0
            r0.loadDataWithBaseURL(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0029 }
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0029 }
            long r1 = r1 - r8
            long r1 = r6 - r1
            int r3 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r3 <= 0) goto L_0x0134
            r13.wait(r1)     // Catch:{ InterruptedException -> 0x013c }
        L_0x0134:
            boolean r1 = r13.o     // Catch:{ all -> 0x0029 }
            if (r1 == 0) goto L_0x015b
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            r0 = r10
            goto L_0x0014
        L_0x013c:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r2.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r3 = "AdLoader InterruptedException while loading the HTML: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.i(r1)     // Catch:{ all -> 0x0029 }
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.INTERNAL_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        L_0x015b:
            r0.stopLoading()     // Catch:{ all -> 0x0029 }
            r0 = 1
            r13.n = r0     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0029 }
            r0.<init>()     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "AdLoader timed out after "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.StringBuilder r0 = r0.append(r6)     // Catch:{ all -> 0x0029 }
            java.lang.String r1 = "ms while loading the HTML."
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ all -> 0x0029 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0029 }
            com.google.ads.util.a.g(r0)     // Catch:{ all -> 0x0029 }
            com.google.ads.AdRequest$ErrorCode r0 = com.google.ads.AdRequest.ErrorCode.NETWORK_ERROR     // Catch:{ all -> 0x0029 }
            monitor-exit(r13)     // Catch:{ all -> 0x0029 }
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.c.doInBackground(com.google.ads.AdRequest[]):com.google.ads.AdRequest$ErrorCode");
    }

    private String a(AdRequest adRequest, Activity activity) {
        Context applicationContext = activity.getApplicationContext();
        Map requestMap = adRequest.getRequestMap(applicationContext);
        f l2 = this.l.l();
        long D = l2.D();
        if (D > 0) {
            requestMap.put("prl", Long.valueOf(D));
        }
        String C = l2.C();
        if (C != null) {
            requestMap.put("ppcl", C);
        }
        String B = l2.B();
        if (B != null) {
            requestMap.put("pcl", B);
        }
        long A = l2.A();
        if (A > 0) {
            requestMap.put("pcc", Long.valueOf(A));
        }
        requestMap.put("preqs", Long.valueOf(f.E()));
        String F = l2.F();
        if (F != null) {
            requestMap.put("pai", F);
        }
        if (l2.G()) {
            requestMap.put("aoi_timeout", "true");
        }
        if (l2.I()) {
            requestMap.put("aoi_nofill", "true");
        }
        String K = l2.K();
        if (K != null) {
            requestMap.put("pit", K);
        }
        l2.a();
        l2.d();
        if (this.l.f() instanceof InterstitialAd) {
            requestMap.put("format", "interstitial_mb");
        } else {
            AdSize k2 = this.l.k();
            String adSize = k2.toString();
            if (adSize != null) {
                requestMap.put("format", adSize);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(k2.getWidth()));
                hashMap.put("h", Integer.valueOf(k2.getHeight()));
                requestMap.put("ad_frame", hashMap);
            }
        }
        requestMap.put("slotname", this.l.h());
        requestMap.put("js", "afma-sdk-a-v4.1.0");
        try {
            int i = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            requestMap.put("msid", applicationContext.getPackageName());
            requestMap.put("app_name", i + ".android." + applicationContext.getPackageName());
            requestMap.put("isu", AdUtil.a(applicationContext));
            String d = AdUtil.d(applicationContext);
            if (d == null) {
                throw new b("NETWORK_ERROR");
            }
            requestMap.put("net", d);
            String e = AdUtil.e(applicationContext);
            if (!(e == null || e.length() == 0)) {
                requestMap.put("cap", e);
            }
            requestMap.put("u_audio", Integer.valueOf(AdUtil.f(applicationContext).ordinal()));
            requestMap.put("u_so", AdUtil.g(applicationContext));
            DisplayMetrics a2 = AdUtil.a(activity);
            requestMap.put("u_sd", Float.valueOf(a2.density));
            requestMap.put("u_h", Integer.valueOf((int) (((float) a2.heightPixels) / a2.density)));
            requestMap.put("u_w", Integer.valueOf((int) (((float) a2.widthPixels) / a2.density)));
            requestMap.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.a()) {
                requestMap.put("simulator", 1);
            }
            String str = "<html><head><script src=\"http://www.gstatic.com/afma/sdk-core-v40.js\"></script><script>AFMA_buildAdURL(" + AdUtil.a(requestMap) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.a.g("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e2) {
            throw new a("NameNotFound!");
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.o = true;
        notify();
    }

    public final synchronized void a(AdRequest.ErrorCode errorCode) {
        this.c = errorCode;
        notify();
    }

    public final synchronized void a(String str) {
        this.b = str;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.j = str2;
        this.a = str;
        notify();
    }

    /* access modifiers changed from: protected */
    public final void onCancelled() {
        com.google.ads.util.a.a("AdLoader cancelled.");
        this.m.stopLoading();
        this.m.destroy();
        this.k.cancel(false);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        AdRequest.ErrorCode errorCode = (AdRequest.ErrorCode) obj;
        synchronized (this) {
            if (errorCode == null) {
                this.l.z();
            } else {
                this.m.stopLoading();
                this.m.destroy();
                this.k.cancel(false);
                if (this.n) {
                    this.l.i().setVisibility(8);
                }
                this.l.a(errorCode);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onProgressUpdate(Object[] objArr) {
        this.k.execute(((String[]) objArr)[0]);
    }
}
