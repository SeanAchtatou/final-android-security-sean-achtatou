package com.google.ads;

import javax.microedition.media.Player;

public class AdSize {
    public static final AdSize BANNER = new AdSize(320, 50, "320x50_mb");
    public static final AdSize IAB_BANNER = new AdSize(468, 60, "468x60_as");
    public static final AdSize IAB_LEADERBOARD = new AdSize(728, 90, "728x90_as");
    public static final AdSize IAB_MRECT = new AdSize(Player.PREFETCHED, 250, "300x250_as");
    private int I;
    private String J;
    private int b;

    private AdSize(int i, int i2, String str) {
        this.I = i;
        this.b = i2;
        this.J = str;
    }

    public int getHeight() {
        return this.b;
    }

    public int getWidth() {
        return this.I;
    }

    public String toString() {
        return this.J;
    }
}
