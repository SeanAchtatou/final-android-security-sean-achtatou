package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class AdRequest {
    public static final String LOGTAG = "Ads";
    public static final String TEST_EMULATOR = AdUtil.a("emulator");
    public static final String VERSION = "4.1.0";
    private Gender A = null;
    private Set B = null;
    private Map C = null;
    private Location D = null;
    private boolean E = false;
    private Set F = null;
    private String b = null;
    private boolean f = false;

    public enum ErrorCode {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String a;

        private ErrorCode(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public enum Gender {
        MALE(AdActivity.TYPE_PARAM),
        FEMALE("f");
        
        private String a;

        private Gender(String str) {
            this.a = str;
        }

        public final String toString() {
            return this.a;
        }
    }

    public void addKeyword(String str) {
        if (this.B == null) {
            this.B = new HashSet();
        }
        this.B.add(str);
    }

    public Map getRequestMap(Context context) {
        HashMap hashMap = new HashMap();
        if (this.B != null) {
            hashMap.put("kw", this.B);
        }
        if (this.A != null) {
            hashMap.put("cust_gender", this.A.toString());
        }
        if (this.b != null) {
            hashMap.put("cust_age", this.b);
        }
        if (this.D != null) {
            hashMap.put("uule", AdUtil.a(this.D));
        }
        if (this.f) {
            hashMap.put("testing", 1);
        }
        if (isTestDevice(context)) {
            hashMap.put("adtest", "on");
        } else if (!this.E) {
            a.g("To get test ads on this device, call adRequest.addTestDevice(" + (AdUtil.a() ? "AdRequest.TEST_EMULATOR" : "\"" + AdUtil.a(context) + "\"") + ");");
            this.E = true;
        }
        if (this.C != null) {
            hashMap.put("extras", this.C);
        }
        return hashMap;
    }

    public boolean isTestDevice(Context context) {
        if (this.F != null) {
            String a = AdUtil.a(context);
            if (a == null) {
                return false;
            }
            if (this.F.contains(a)) {
                return true;
            }
        }
        return false;
    }

    public void setTesting(boolean z) {
        this.f = z;
    }
}
