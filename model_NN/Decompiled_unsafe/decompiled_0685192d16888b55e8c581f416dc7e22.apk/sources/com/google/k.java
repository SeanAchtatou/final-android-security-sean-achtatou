package com.google;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

public final class k implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        Uri parse;
        String host;
        String str;
        String str2 = (String) hashMap.get(AdActivity.URL_PARAM);
        if (str2 == null) {
            a.i("Could not get URL from click gmsg.");
            return;
        }
        dVar.l().a((String) hashMap.get("ai"));
        f l = dVar.l();
        if (!(l == null || (host = (parse = Uri.parse(str2)).getHost()) == null || !host.toLowerCase().endsWith(".admob.com"))) {
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str = split[2] + "/" + split[3];
                    l.b(str);
                }
            }
            str = null;
            l.b(str);
        }
        new Thread(new w(str2, webView.getContext().getApplicationContext())).start();
    }
}
