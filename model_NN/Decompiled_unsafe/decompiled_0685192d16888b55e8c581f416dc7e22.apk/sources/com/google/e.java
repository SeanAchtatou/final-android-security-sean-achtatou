package com.google;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

public final class e {
    private String a;
    private HashMap aq;

    public e(Bundle bundle) {
        this.a = bundle.getString("action");
        Serializable serializable = bundle.getSerializable("params");
        this.aq = serializable instanceof HashMap ? (HashMap) serializable : null;
    }

    public e(String str) {
        this.a = str;
    }

    public e(String str, HashMap hashMap) {
        this(str);
        this.aq = hashMap;
    }

    public final Bundle a() {
        Bundle bundle = new Bundle();
        bundle.putString("action", this.a);
        bundle.putSerializable("params", this.aq);
        return bundle;
    }

    public final String b() {
        return this.a;
    }

    public final HashMap c() {
        return this.aq;
    }
}
