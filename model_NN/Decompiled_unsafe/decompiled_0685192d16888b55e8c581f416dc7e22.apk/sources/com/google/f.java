package com.google;

import android.os.SystemClock;
import com.google.ads.util.a;
import java.util.LinkedList;

public final class f {
    private static long av = 0;
    public String a;
    private LinkedList ar = new LinkedList();
    private long as;
    private long at;
    private LinkedList au = new LinkedList();
    private String aw;
    private boolean n = false;
    private boolean o = false;

    f() {
        a();
    }

    static long E() {
        return av;
    }

    /* access modifiers changed from: package-private */
    public final long A() {
        if (this.ar.size() != this.au.size()) {
            return -1;
        }
        return (long) this.ar.size();
    }

    /* access modifiers changed from: package-private */
    public final String B() {
        if (this.ar.isEmpty() || this.ar.size() != this.au.size()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.ar.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.au.get(i2)).longValue() - ((Long) this.ar.get(i2)).longValue()));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final String C() {
        if (this.ar.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.ar.size()) {
                return sb.toString();
            }
            if (i2 != 0) {
                sb.append(",");
            }
            sb.append(Long.toString(((Long) this.ar.get(i2)).longValue() - this.as));
            i = i2 + 1;
        }
    }

    /* access modifiers changed from: package-private */
    public final long D() {
        return this.as - this.at;
    }

    /* access modifiers changed from: package-private */
    public final String F() {
        return this.aw;
    }

    /* access modifiers changed from: package-private */
    public final boolean G() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public final void H() {
        a.h("Interstitial network error.");
        this.n = true;
    }

    /* access modifiers changed from: package-private */
    public final boolean I() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public final void J() {
        a.h("Interstitial no fill.");
        this.o = true;
    }

    /* access modifiers changed from: package-private */
    public final String K() {
        return this.a;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.ar.clear();
        this.as = 0;
        this.at = 0;
        this.au.clear();
        this.aw = null;
        this.n = false;
        this.o = false;
    }

    public final void a(String str) {
        a.h("Prior ad identifier = " + str);
        this.aw = str;
    }

    /* access modifiers changed from: package-private */
    public final void b() {
        a.h("Ad clicked.");
        this.ar.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }

    public final void b(String str) {
        a.h("Prior impression ticket = " + str);
        this.a = str;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        a.h("Ad request loaded.");
        this.as = SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        a.h("Ad request started.");
        this.at = SystemClock.elapsedRealtime();
        av++;
    }

    public final void o() {
        a.h("Landing page dismissed.");
        this.au.add(Long.valueOf(SystemClock.elapsedRealtime()));
    }
}
