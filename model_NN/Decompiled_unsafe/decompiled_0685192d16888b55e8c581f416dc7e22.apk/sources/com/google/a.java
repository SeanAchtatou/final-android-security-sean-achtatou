package com.google;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public final class a {
    public static final Map a;
    public static final Map b;

    static {
        HashMap hashMap = new HashMap();
        hashMap.put("/invalidRequest", new o());
        hashMap.put("/loadAdURL", new p());
        a = Collections.unmodifiableMap(hashMap);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("/open", new r());
        hashMap2.put("/canOpenURLs", new j());
        hashMap2.put("/close", new l());
        hashMap2.put("/evalInOpener", new m());
        hashMap2.put("/log", new q());
        hashMap2.put("/click", new k());
        hashMap2.put("/httpTrack", new n());
        hashMap2.put("/reloadRequest", new s());
        hashMap2.put("/settings", new t());
        hashMap2.put("/touch", new u());
        hashMap2.put("/video", new v());
        b = Collections.unmodifiableMap(hashMap2);
    }

    private a() {
    }

    public static void a(WebView webView) {
        com.google.ads.util.a.h("Calling onshow.");
        a(webView, "onshow", "{'version': 'afma-sdk-a-v4.1.0'}");
    }

    public static void a(WebView webView, String str) {
        webView.loadUrl("javascript:" + str);
    }

    private static void a(WebView webView, String str, String str2) {
        if (str2 != null) {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "', " + str2 + ");");
        } else {
            a(webView, "AFMA_ReceiveMessage" + "('" + str + "');");
        }
    }

    public static void a(WebView webView, Map map) {
        a(webView, "openableURLs", new JSONObject(map).toString());
    }

    static void a(d dVar, Map map, Uri uri, WebView webView) {
        String str;
        HashMap b2 = AdUtil.b(uri);
        if (b2 == null) {
            com.google.ads.util.a.i("An error occurred while parsing the message parameters.");
            return;
        }
        if (d(uri)) {
            String host = uri.getHost();
            if (host == null) {
                com.google.ads.util.a.i("An error occurred while parsing the AMSG parameters.");
                str = null;
            } else if (host.equals("launch")) {
                b2.put("a", "intent");
                b2.put(AdActivity.URL_PARAM, b2.get("url"));
                b2.remove("url");
                str = "/open";
            } else if (host.equals("closecanvas")) {
                str = "/close";
            } else if (host.equals("log")) {
                str = "/log";
            } else {
                com.google.ads.util.a.i("An error occurred while parsing the AMSG: " + uri.toString());
                str = null;
            }
        } else if (c(uri)) {
            str = uri.getPath();
        } else {
            com.google.ads.util.a.i("Message was neither a GMSG nor an AMSG.");
            str = null;
        }
        if (str == null) {
            com.google.ads.util.a.i("An error occurred while parsing the message.");
            return;
        }
        i iVar = (i) map.get(str);
        if (iVar == null) {
            com.google.ads.util.a.i("No AdResponse found, <message: " + str + ">");
        } else {
            iVar.a(dVar, b2, webView);
        }
    }

    public static boolean a(Uri uri) {
        if (uri == null || !uri.isHierarchical()) {
            return false;
        }
        return c(uri) || d(uri);
    }

    public static void b(WebView webView) {
        com.google.ads.util.a.h("Calling onhide.");
        a(webView, "onhide", null);
    }

    private static boolean c(Uri uri) {
        String scheme = uri.getScheme();
        if (scheme == null || !scheme.equals("gmsg")) {
            return false;
        }
        String authority = uri.getAuthority();
        return authority != null && authority.equals("mobileads.google.com");
    }

    private static boolean d(Uri uri) {
        String scheme = uri.getScheme();
        return scheme != null && scheme.equals("admob");
    }
}
