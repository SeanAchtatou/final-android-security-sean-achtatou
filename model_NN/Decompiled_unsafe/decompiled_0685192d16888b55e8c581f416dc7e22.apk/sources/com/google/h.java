package com.google;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.AdActivity;
import com.google.ads.util.AdUtil;
import com.google.ads.util.a;
import java.util.HashMap;
import java.util.Map;

public final class h extends WebViewClient {
    private d K;
    private boolean az;
    private Map b;
    private boolean d;
    private boolean e = false;
    private boolean f = false;

    public h(d dVar, Map map, boolean z, boolean z2) {
        this.K = dVar;
        this.b = map;
        this.az = z;
        this.d = z2;
    }

    public final void a() {
        this.e = true;
    }

    public final void b() {
        this.f = true;
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.e) {
            c g = this.K.g();
            if (g != null) {
                g.a();
            } else {
                a.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.e = false;
        }
        if (this.f) {
            a.a(webView);
            this.f = false;
        }
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        if (a.a(parse)) {
            a.a(this.K, this.b, parse, webView);
            return true;
        } else if (this.d) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put(AdActivity.URL_PARAM, str);
            AdActivity.launchAdActivity(this.K, new e("intent", hashMap));
            return true;
        } else if (this.az) {
            HashMap b2 = AdUtil.b(parse);
            if (b2 == null) {
                a.i("An error occurred while parsing the url parameters.");
                return true;
            }
            this.K.l().a((String) b2.get("ai"));
            String str2 = (!this.K.v() || !AdUtil.a(parse)) ? "intent" : "webapp";
            HashMap hashMap2 = new HashMap();
            hashMap2.put(AdActivity.URL_PARAM, parse.toString());
            AdActivity.launchAdActivity(this.K, new e(str2, hashMap2));
            return true;
        } else {
            a.i("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }
}
