package com.google;

import android.webkit.WebView;
import com.google.ads.AdRequest;
import com.google.ads.util.a;
import java.util.HashMap;

public final class o implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        a.i("Invalid " + ((String) hashMap.get("type")) + " request error: " + ((String) hashMap.get("errors")));
        c g = dVar.g();
        if (g != null) {
            g.a(AdRequest.ErrorCode.INVALID_REQUEST);
        }
    }
}
