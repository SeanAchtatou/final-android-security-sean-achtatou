package com.google;

import android.webkit.WebView;
import com.google.ads.AdActivity;
import com.google.ads.util.a;
import java.util.HashMap;

public final class r implements i {
    public final void a(d dVar, HashMap hashMap, WebView webView) {
        String str = (String) hashMap.get("a");
        if (str == null) {
            a.a("Could not get the action parameter for open GMSG.");
        } else if (str.equals("webapp")) {
            AdActivity.launchAdActivity(dVar, new e("webapp", hashMap));
        } else {
            AdActivity.launchAdActivity(dVar, new e("intent", hashMap));
        }
    }
}
