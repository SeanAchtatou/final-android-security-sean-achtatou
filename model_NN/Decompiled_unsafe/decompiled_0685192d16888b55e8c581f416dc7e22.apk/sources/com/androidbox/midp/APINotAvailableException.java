package com.androidbox.midp;

public class APINotAvailableException extends Exception {
    private static final long serialVersionUID = 1;

    public String toString() {
        return "APIBridge is not available on this device. Try a standard midp api instead.";
    }
}
