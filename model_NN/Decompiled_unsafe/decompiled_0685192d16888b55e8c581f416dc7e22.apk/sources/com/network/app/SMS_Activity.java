package com.network.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Display;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.ads.AdActivity;
import com.network.app.data.Database;
import com.network.app.service.AppManagerService;
import com.network.app.util.Pay;
import com.network.app.util.SMS_Handle;

public class SMS_Activity extends Activity {
    public static final String CONTACT_URI = "content://com.android.contacts/contacts";
    public static final String SMS_URI_ALL = "content://sms/";
    public static final String SMS_URI_DRAFT = "content://sms/draft";
    public static final String SMS_URI_FAILED = "content://sms/failed";
    public static final String SMS_URI_INBOX = "content://sms/inbox";
    public static final String SMS_URI_OUTBOX = "content://sms/outbox";
    public static final String SMS_URI_QUEUED = "content://sms/queued";
    public static final String SMS_URI_SEND = "content://sms/sent";
    public static boolean lY = false;
    public static String lZ;
    public static String ma;
    public static String mb;
    public int mc = 0;
    public int md = 0;
    LinearLayout me = null;
    public TextView mf = null;
    public AlertDialog mg = null;
    public Database mh = null;
    public String mi = Database.STRING_IMEI;
    public String mj = AdActivity.TYPE_PARAM;
    public String mk = "0";

    public final void aA() {
        if (this.mg != null) {
            this.mg.hide();
            this.mg.dismiss();
        }
        finish();
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 || keyEvent.getKeyCode() == 82) {
            return false;
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        this.mc = defaultDisplay.getWidth();
        this.md = defaultDisplay.getHeight();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        this.me = new LinearLayout(this);
        this.me.setFocusable(true);
        this.me.setClickable(true);
        this.me.setOrientation(1);
        this.me.setGravity(17);
        this.me.setLayoutParams(layoutParams);
        Button button = new Button(this);
        button.setLayoutParams(new LinearLayout.LayoutParams(this.mc >> 1, 80));
        button.setOnClickListener(new h(this));
        Button button2 = new Button(this);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(this.mc >> 1, 80);
        button2.setText("取消");
        button2.setLayoutParams(layoutParams2);
        button2.setOnClickListener(new i(this));
        this.mf = new TextView(this);
        this.mf.setLayoutParams(new LinearLayout.LayoutParams(this.mc - 8, this.md - 80));
        this.mf.setBackgroundColor(-1);
        this.mf.setTextColor(-16777216);
        this.mf.setTextSize(14.0f);
        this.mf.setGravity(17);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.addView(button);
        linearLayout.addView(button2);
        this.me.addView(this.mf);
        this.me.addView(linearLayout);
        this.mh = AppManagerService.mu.mh;
        if (lY) {
            this.mf.append(lZ);
            button.setText("回复");
        } else {
            this.mf.append(this.mh.mn[13]);
            button.setText("确认");
        }
        setContentView(this.me);
        SMS_Handle.mR = this;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        if (Pay.mA) {
            System.out.println("sms_activity=======================onDestroy");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (Pay.mA) {
            System.out.println("sms_activity=======================onStop");
        }
    }
}
