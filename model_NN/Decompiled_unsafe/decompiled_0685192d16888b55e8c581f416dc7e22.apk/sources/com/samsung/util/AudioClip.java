package com.samsung.util;

public class AudioClip {
    public static final int TYPE_MIDI = 3;
    public static final int TYPE_MMF = 1;
    public static final int TYPE_MP3 = 2;

    private AudioClip() {
    }
}
