package com.nokia.mid.ui;

import android.util.Log;
import java.util.HashMap;

public class DirectUtils {
    private static final HashMap mU = new HashMap();

    public static DirectGraphics a(ak akVar) {
        if (mU.containsKey(akVar)) {
            return (DirectGraphics) mU.get(akVar);
        }
        m mVar = new m(akVar);
        mU.put(akVar, mVar);
        Log.d("DirectUtils", "Create a new Nokia DirectUtils.");
        return mVar;
    }
}
