package org.meteoroid.core;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

public class MeteoroidActivity extends Activity {
    public static final String LOG_TAG = "Meteoroid";
    public static final int MSG_ACTIVITY_CONF_CHANGE = 40965;
    public static final int MSG_ACTIVITY_PAUSE = 40960;
    public static final int MSG_ACTIVITY_RESTART = 40963;
    public static final int MSG_ACTIVITY_RESUME = 40961;
    public static final int MSG_ACTIVITY_START = 40962;
    public static final int MSG_ACTIVITY_STOP = 40964;
    private static boolean qY = true;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        bw.b(bw.a((int) cd.MSG_SYSTEM_ACTIVITY_RESULT, new int[]{i, i2}));
        super.onActivityResult(i, i2, intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        bw.b(bw.a((int) MSG_ACTIVITY_CONF_CHANGE, this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setBackgroundDrawable(null);
        getWindow().requestFeature(1);
        getWindow().setFlags(128, 128);
        getWindow().setFlags(ap.GAME_B_PRESSED, ap.GAME_B_PRESSED);
        bw.a((int) MSG_ACTIVITY_PAUSE, "MSG_ACTIVITY_PAUSE");
        bw.a((int) MSG_ACTIVITY_RESUME, "MSG_ACTIVITY_RESUME");
        bw.a((int) MSG_ACTIVITY_START, "MSG_ACTIVITY_START");
        bw.a((int) MSG_ACTIVITY_RESTART, "MSG_ACTIVITY_RESTART");
        bw.a((int) MSG_ACTIVITY_STOP, "MSG_ACTIVITY_STOP");
        bw.a((int) MSG_ACTIVITY_CONF_CHANGE, "MSG_ACTIVITY_CONF_CHANGE");
        cd.b(this);
        super.onCreate(bundle);
        bw.b(bw.a((int) cd.MSG_SYSTEM_INIT_COMPLETE, bundle));
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        cd.bD();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!cj.aV()) {
            cd.bE();
        }
        return true;
    }

    public void onLowMemory() {
        cd.bG();
        super.onLowMemory();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        by.a(menuItem);
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        bw.b(bw.a((int) MSG_ACTIVITY_PAUSE, this));
        cd.pause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        by.onPrepareOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        bw.b(bw.a((int) MSG_ACTIVITY_RESTART, this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        bw.b(bw.a((int) MSG_ACTIVITY_RESUME, this));
        if (qY) {
            qY = false;
        } else {
            cd.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        bw.b(bw.a((int) MSG_ACTIVITY_START, this));
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        bw.b(bw.a((int) MSG_ACTIVITY_STOP, this));
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        return bm.onTrackballEvent(motionEvent);
    }
}
