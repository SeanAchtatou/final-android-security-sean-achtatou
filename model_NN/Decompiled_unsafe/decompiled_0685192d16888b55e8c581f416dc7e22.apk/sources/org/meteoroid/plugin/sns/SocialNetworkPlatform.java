package org.meteoroid.plugin.sns;

import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class SocialNetworkPlatform implements bx, cp, AbstractPaymentManager.Payment {
    public static final int LAUNCH_PARAM_NONE = -1;
    public static final int MSG_SOCIAL_PLATFORM_LAUNCH = 1013249;
    public static final int MSG_SOCIAL_PLATFORM_UPDATE = 1013250;
}
