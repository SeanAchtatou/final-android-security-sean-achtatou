package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import com.androidbox.sgmj2net8.R;
import com.network.app.data.Database;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Pattern;
import javax.microedition.media.Player;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;

public class MIDPDevice extends ScreenWidget implements bn, bs, bx, co {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_POINTER_EVENT = 44033;
    public static final int MSG_MIDP_REPAINT_LATER = 44036;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    private static final Properties rG = new Properties();
    public static final String[] rq = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    private static final HashMap ry = new HashMap(rq.length);
    private int height = -1;
    private Bitmap rA;
    private Bitmap rB;
    private Canvas rC;
    private cx rD;
    private cx rE;
    private cx rF;
    private int rH;
    private int rI;
    private int rJ;
    private int rK;
    private int rL;
    private boolean rr;
    private boolean rs;
    private boolean rt;
    private boolean ru;
    private boolean rv;
    private boolean rw;
    private boolean rx;
    private Bitmap rz;
    private int width = -1;

    public static int P(int i) {
        if (ry.containsKey(rq[0]) && i == ((Integer) ry.get(rq[0])).intValue()) {
            return 1;
        }
        if (ry.containsKey(rq[1]) && i == ((Integer) ry.get(rq[1])).intValue()) {
            return 6;
        }
        if (ry.containsKey(rq[2]) && i == ((Integer) ry.get(rq[2])).intValue()) {
            return 2;
        }
        if (ry.containsKey(rq[3]) && i == ((Integer) ry.get(rq[3])).intValue()) {
            return 5;
        }
        if (ry.containsKey(rq[4]) && i == ((Integer) ry.get(rq[4])).intValue()) {
            return 8;
        }
        if (ry.containsKey(rq[5]) && i == ((Integer) ry.get(rq[5])).intValue()) {
            return 9;
        }
        if (ry.containsKey(rq[6]) && i == ((Integer) ry.get(rq[6])).intValue()) {
            return 10;
        }
        if (ry.containsKey(rq[7]) && i == ((Integer) ry.get(rq[7])).intValue()) {
            return 11;
        }
        if (ry.containsKey(rq[12]) && i == ((Integer) ry.get(rq[12])).intValue()) {
            return 35;
        }
        if (ry.containsKey(rq[11]) && i == ((Integer) ry.get(rq[11])).intValue()) {
            return 42;
        }
        if (ry.containsKey("NUM_0") && i == ((Integer) ry.get("NUM_0")).intValue()) {
            return 48;
        }
        if (ry.containsKey("NUM_1") && i == ((Integer) ry.get("NUM_1")).intValue()) {
            return 49;
        }
        if (ry.containsKey("NUM_2") && i == ((Integer) ry.get("NUM_2")).intValue()) {
            return 50;
        }
        if (ry.containsKey("NUM_3") && i == ((Integer) ry.get("NUM_3")).intValue()) {
            return 51;
        }
        if (ry.containsKey("NUM_4") && i == ((Integer) ry.get("NUM_4")).intValue()) {
            return 52;
        }
        if (ry.containsKey("NUM_5") && i == ((Integer) ry.get("NUM_5")).intValue()) {
            return 53;
        }
        if (ry.containsKey("NUM_6") && i == ((Integer) ry.get("NUM_6")).intValue()) {
            return 54;
        }
        if (ry.containsKey("NUM_7") && i == ((Integer) ry.get("NUM_7")).intValue()) {
            return 55;
        }
        if (!ry.containsKey("NUM_8") || i != ((Integer) ry.get("NUM_8")).intValue()) {
            return (!ry.containsKey("NUM_9") || i != ((Integer) ry.get("NUM_9")).intValue()) ? 0 : 57;
        }
        return 56;
    }

    public static int Q(int i) {
        if (i == 1) {
            return ((Integer) ry.get(rq[9])).intValue();
        }
        if (i == 2) {
            return ((Integer) ry.get(rq[10])).intValue();
        }
        return 0;
    }

    private final int R(int i) {
        if (i == Q(1)) {
            return 17;
        }
        if (i == Q(2)) {
            return 18;
        }
        switch (P(i)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case ab.KEY_POUND:
                return 11;
            case ab.KEY_STAR:
                return 10;
            case ab.KEY_NUM0:
                return 0;
            case ab.KEY_NUM1:
                return 1;
            case ab.KEY_NUM2:
                return 2;
            case ab.KEY_NUM3:
                return 3;
            case ab.KEY_NUM4:
                return 4;
            case ab.KEY_NUM5:
                return 5;
            case ab.KEY_NUM6:
                return 6;
            case ab.KEY_NUM7:
                return 7;
            case ab.KEY_NUM8:
                return 8;
            case ab.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private boolean S(int i) {
        return ((1 << R(i)) & this.rL) != 0;
    }

    private static cx a(Bitmap bitmap) {
        return new cx(new Canvas(bitmap));
    }

    public static p a(String str, int i, boolean z) {
        String trim = str.trim();
        if (trim.startsWith("http:")) {
            return new cy(trim, i, z);
        }
        if (trim.startsWith("sms:")) {
            return new da(trim);
        }
        if (trim.startsWith("socket:")) {
            return new de(trim, i);
        }
        if (trim.startsWith("file:")) {
            return new cu(trim);
        }
        throw new IOException("Unkown protocol:" + trim);
    }

    private void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
            }
            if (properties.containsKey("font.size.large")) {
                cw.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
            }
            if (properties.containsKey("font.size.medium")) {
                cw.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
            }
            if (properties.containsKey("font.size.small")) {
                cw.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
            }
            for (int i = 0; i < 10; i++) {
                if (properties.containsKey("key." + i)) {
                    ry.put("NUM_" + i, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i).trim())));
                }
            }
            for (int i2 = 0; i2 < rq.length; i2++) {
                if (properties.containsKey("key." + rq[i2])) {
                    ry.put(rq[i2], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + rq[i2]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e) {
            Log.w("MIDPDevice", "Device property exception. " + e);
        }
        this.rs = b("hasRepeatEvents", false);
        this.ru = b("hasPointerMotionEvents", true);
        this.rv = b("isDoubleBuffered", true);
        this.rr = b("isPositiveUpdate", false);
        this.rw = b("enableMultiTouch", false);
        this.rx = b("fixTouchPosition", false);
    }

    private boolean b(String str, boolean z) {
        String property = rG.getProperty(str);
        return property == null ? z : Boolean.parseBoolean(property);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    private final void p(int i, int i2) {
        int R = R(i2);
        if (R == -1) {
            return;
        }
        if (i == 0) {
            this.rL = (1 << R) | this.rL;
            return;
        }
        this.rL = (1 << R) ^ this.rL;
    }

    private boolean q(int i, int i2) {
        switch (i) {
            case 0:
                if (!S(i2)) {
                    ae.a((MIDlet) null).y(i2);
                } else if (this.rs) {
                    ae.a((MIDlet) null).B(i2);
                }
                p(i, i2);
                return true;
            case 1:
                if (S(i2)) {
                    ae.a((MIDlet) null).z(i2);
                }
                p(i, i2);
                return true;
            default:
                return false;
        }
    }

    private synchronized void unlock() {
        notify();
    }

    public final void a(int i, float f, float f2, int i2) {
        if (this.rw || i2 == 0) {
            bw.b(bw.a((int) MSG_MIDP_POINTER_EVENT, new int[]{i, (int) f, (int) f2}));
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        this.tr = attributeSet.getAttributeBooleanValue(str, "touchable", false);
        this.rt = this.tr;
        boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue(str, "filter", true);
        if (!(this.tp == 1.0f && this.tq == 1.0f)) {
            this.tn.setFilterBitmap(attributeBooleanValue);
        }
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.to = dl.M(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            Rect M = dl.M(attributeValue2);
            this.tm.left = M.left;
            this.tm.top = M.top;
            this.tm.right = M.right;
            this.tm.bottom = M.bottom;
        }
    }

    public final void a(cq cqVar) {
        if (this.rv) {
            this.rz = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.rA = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.rE = a(this.rz);
            this.rF = a(this.rA);
            this.rD = this.rE;
            this.rB = this.rA;
        } else {
            this.rz = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.rA = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
            this.rB = this.rA;
            this.rC = new Canvas(this.rB);
            this.rD = a(this.rz);
        }
        ci();
        if (!this.rr) {
            bi.br();
        }
    }

    public final boolean a(Message message) {
        boolean z;
        if (message.what == 47872) {
            HashMap hashMap = new HashMap();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(cd.getActivity().getAssets().open("META-INF/MANIFEST.MF"), Database.ENCODING));
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        break;
                    }
                    readLine.trim();
                    int indexOf = readLine.indexOf(58);
                    if (indexOf >= 0) {
                        String trim = readLine.substring(0, indexOf).trim();
                        String trim2 = readLine.substring(indexOf + 1).trim();
                        if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                            int indexOf2 = trim2.indexOf(44);
                            int lastIndexOf = trim2.lastIndexOf(44);
                            if (indexOf2 < 0 || lastIndexOf < 0) {
                                Log.w("MIDPDevice", "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                            } else {
                                String trim3 = trim2.substring(0, indexOf2).trim();
                                trim2.substring(indexOf2 + 1, lastIndexOf).trim();
                                hashMap.put(trim3, trim2.substring(lastIndexOf + 1).trim());
                            }
                        } else {
                            bc.f(trim, trim2);
                        }
                    }
                }
                if (hashMap.isEmpty()) {
                    Log.w("MIDPDevice", "No midlets found in MANIFEST.MF.");
                    cd.b(cd.getString(R.string.no_midlet_found), 1);
                } else if (hashMap.size() == 1) {
                    bc.x((String) hashMap.values().toArray()[0]);
                } else {
                    String[] strArr = new String[hashMap.keySet().size()];
                    hashMap.keySet().toArray(strArr);
                    AlertDialog.Builder builder = new AlertDialog.Builder(cd.getActivity());
                    builder.setItems(strArr, new cs(this, hashMap, strArr));
                    builder.setCancelable(false);
                    cd.getHandler().post(new ct(this, builder));
                }
            } catch (IOException e) {
                Log.w("MIDPDevice", "MANIFEST.MF may not exist or invalid.");
                cd.b(cd.getString(R.string.no_midlet_found), 1);
            }
            return true;
        }
        switch (message.what) {
            case MSG_MIDP_POINTER_EVENT /*44033*/:
                int[] iArr = (int[]) message.obj;
                switch (iArr[0]) {
                    case 0:
                        if (this.rt) {
                            ae.a((MIDlet) null).k(iArr[1], iArr[2]);
                            this.rH = iArr[1];
                            this.rI = iArr[2];
                        }
                        return true;
                    case 1:
                        if (this.rt) {
                            ae.a((MIDlet) null).l(iArr[1], iArr[2]);
                        }
                        return true;
                    case 2:
                        if (this.rt && this.ru) {
                            int i = iArr[1];
                            int i2 = iArr[2];
                            if (!this.rx) {
                                z = true;
                            } else {
                                if (this.rJ == 0) {
                                    this.rJ = getWidth() / 60;
                                }
                                if (this.rK == 0) {
                                    this.rK = getHeight() / 60;
                                }
                                z = Math.abs(i - this.rH) >= this.rJ || Math.abs(i2 - this.rI) >= this.rK;
                            }
                            if (z) {
                                ae.a((MIDlet) null).m(iArr[1], iArr[2]);
                            }
                        }
                        return true;
                    default:
                        return false;
                }
            case MSG_MIDP_COMMAND_EVENT /*44034*/:
                ae.a((MIDlet) null);
                ae.a((ac) message.obj);
                return true;
            case MSG_MIDP_DISPLAY_CALL_SERIALLY /*44035*/:
                if (message.obj != null) {
                    ((Runnable) message.obj).run();
                }
                return true;
            case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                int cb = ((VirtualKey) message.obj).cb();
                String ck = ((VirtualKey) message.obj).ck();
                return q(cb, ry.containsKey(ck) ? ((Integer) ry.get(ck)).intValue() : 65535);
            default:
                return false;
        }
    }

    public final boolean a(KeyEvent keyEvent) {
        int Q;
        int action = keyEvent.getAction();
        switch (keyEvent.getKeyCode()) {
            case 4:
            case 12:
                Q = ((Integer) ry.get("NUM_5")).intValue();
                break;
            case 7:
                Q = ((Integer) ry.get("NUM_0")).intValue();
                break;
            case 8:
                Q = ((Integer) ry.get("NUM_1")).intValue();
                break;
            case 9:
                Q = ((Integer) ry.get("NUM_2")).intValue();
                break;
            case 10:
                Q = ((Integer) ry.get("NUM_3")).intValue();
                break;
            case 11:
                Q = ((Integer) ry.get("NUM_4")).intValue();
                break;
            case 13:
                Q = ((Integer) ry.get("NUM_6")).intValue();
                break;
            case 14:
            case 99:
                Q = ((Integer) ry.get("NUM_7")).intValue();
                break;
            case Database.INT_FEECUE:
                Q = ((Integer) ry.get("NUM_8")).intValue();
                break;
            case 16:
            case Player.UNREALIZED:
                Q = ((Integer) ry.get("NUM_9")).intValue();
                break;
            case Database.INT_FEES:
            case 102:
                Q = ((Integer) ry.get(rq[11])).intValue();
                break;
            case 18:
            case 103:
                Q = ((Integer) ry.get(rq[12])).intValue();
                break;
            case Database.INT_MTWORD:
                Q = ((Integer) ry.get(rq[0])).intValue();
                break;
            case Database.INT_TIMEWAIT:
                Q = ((Integer) ry.get(rq[1])).intValue();
                break;
            case Database.INT_FAILWORD:
                Q = ((Integer) ry.get(rq[2])).intValue();
                break;
            case Database.INT_SUCWORD:
                Q = ((Integer) ry.get(rq[3])).intValue();
                break;
            case Database.INT_CONFIRMWORD:
                Q = ((Integer) ry.get(rq[4])).intValue();
                break;
            case 66:
                Q = ((Integer) ry.get(rq[9])).intValue();
                break;
            case 108:
                Q = Q(2);
                break;
            case 109:
                Q = Q(1);
                break;
            default:
                Q = 0;
                break;
        }
        q(action, Q);
        return false;
    }

    public final void bS() {
        if (!this.rv) {
            this.rC.drawBitmap(this.rz, 0.0f, 0.0f, (Paint) null);
        } else if (this.rB == this.rz) {
            this.rB = this.rA;
            this.rD = this.rE;
        } else {
            this.rB = this.rz;
            this.rD = this.rF;
        }
        if (this.rD != null) {
            this.rD.bd();
        }
        if (this.rr) {
            unlock();
        } else {
            bi.bs();
        }
    }

    public final Bitmap bT() {
        return this.rB;
    }

    public final ak bf() {
        return this.rD;
    }

    public final int getHeight() {
        return this.height == -1 ? cd.bI() : this.height;
    }

    public final int getWidth() {
        return this.width == -1 ? cd.bH() : this.width;
    }

    public final void onCreate() {
        bw.a((int) MSG_MIDP_POINTER_EVENT, "MSG_MIDP_POINTER_EVENT");
        bw.a((int) MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        bw.a((int) MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        bw.a((int) MSG_MIDP_REPAINT_LATER, "MSG_MIDP_REPAINT_LATER");
        bw.a(this);
        rG.clear();
        try {
            rG.load(cd.getActivity().getResources().openRawResource(dl.L("device")));
        } catch (IOException e) {
            Log.e("MIDPDevice", "device.properties not exist or valid." + e);
        }
        a(rG);
        bm.a((bn) this);
        bm.a((bs) this);
        System.gc();
    }

    public final void onDestroy() {
        if (this.rz != null) {
            this.rz.recycle();
        }
        this.rz = null;
        if (this.rA != null) {
            this.rA.recycle();
        }
        this.rA = null;
        if (this.rB != null) {
            this.rB.recycle();
        }
        this.rB = null;
        this.rD = null;
    }

    public final void onDraw(Canvas canvas) {
        if (this.rB != null && !this.rB.isRecycled()) {
            canvas.drawBitmap(this.rB, this.to, this.tm, this.tn);
        }
        if (this.rr) {
            lock();
        }
    }

    public final void setVisible(boolean z) {
    }
}
