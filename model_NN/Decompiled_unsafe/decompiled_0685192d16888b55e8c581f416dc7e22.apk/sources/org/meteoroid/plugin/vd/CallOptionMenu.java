package org.meteoroid.plugin.vd;

public class CallOptionMenu extends SimpleButton {
    private boolean qd;

    public final void onClick() {
        if (this.qd) {
            cd.getActivity().closeOptionsMenu();
        } else {
            cd.getActivity().openOptionsMenu();
        }
        this.qd = !this.qd;
    }
}
