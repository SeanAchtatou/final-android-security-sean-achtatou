package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public class Background implements cr {
    int color = -16777216;
    Rect pf;
    private Paint rZ;
    cq rp;
    private boolean sX = true;
    Bitmap tc;

    public void a(AttributeSet attributeSet, String str) {
        this.pf = dl.M(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.tc = dl.N(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public final void a(cq cqVar) {
        this.rp = cqVar;
        if (this.tc == null) {
            this.rZ = new Paint();
            this.rZ.setAntiAlias(true);
            this.rZ.setStyle(Paint.Style.FILL);
        }
    }

    public final boolean bR() {
        return true;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        return false;
    }

    public final boolean isTouchable() {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.sX) {
            if (this.tc == null) {
                this.rZ.setColor(this.color);
                canvas.drawRect(this.pf, this.rZ);
                return;
            }
            canvas.drawBitmap(this.tc, (Rect) null, this.pf, (Paint) null);
        }
    }

    public final void setVisible(boolean z) {
        this.sX = z;
    }
}
