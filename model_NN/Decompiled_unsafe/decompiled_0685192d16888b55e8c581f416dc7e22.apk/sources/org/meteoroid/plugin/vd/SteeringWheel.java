package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] ty = new VirtualKey[2];
    private int orientation = 0;
    private final Matrix pT = new Matrix();
    private float tA = 0.0f;
    public int tB = 135;
    public int tC = 5;
    private float tz = -1.0f;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.tB = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.tC = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.sT = dl.O(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(cq cqVar) {
        super.a(cqVar);
        ty[0] = new VirtualKey();
        ty[0].tE = "LEFT";
        ty[1] = new VirtualKey();
        ty[1].tE = "RIGHT";
        reset();
    }

    public final boolean h(int i, int i2, int i3, int i4) {
        VirtualKey virtualKey;
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.sY) || sqrt < ((double) this.sZ)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.tz == -1.0f) {
                    this.tz = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    float a = a((float) i2, (float) i3);
                    float f = this.tz;
                    float f2 = a - f;
                    if (this.orientation == 0) {
                        if (Math.abs(f2) > 180.0f) {
                            if (f2 > 0.0f) {
                                this.orientation = 1;
                            } else if (f2 < 0.0f) {
                                this.orientation = 2;
                            }
                        } else if (a > f) {
                            this.orientation = 2;
                        } else if (a < f) {
                            this.orientation = 1;
                        }
                    }
                    this.tA = (this.orientation != 1 || f2 <= 0.0f) ? (this.orientation != 2 || f2 >= 0.0f) ? f2 : 360.0f + f2 : f2 - 360.0f;
                    if (this.tA > ((float) this.tB)) {
                        this.tA = (float) this.tB;
                    } else if (this.tA < ((float) (this.tB * -1))) {
                        this.tA = (float) (this.tB * -1);
                    }
                    if (this.tA >= ((float) this.tC) && this.tA <= ((float) this.tB)) {
                        virtualKey = ty[0];
                    } else if (this.tA > ((float) (this.tC * -1)) || this.tA < ((float) (this.tB * -1))) {
                        if (this.tA < ((float) this.tC) && this.tA > ((float) (this.tC * -1))) {
                            this.orientation = 0;
                            break;
                        } else {
                            virtualKey = null;
                        }
                    } else {
                        virtualKey = ty[1];
                    }
                    if (this.ta != virtualKey) {
                        if (this.ta != null && this.ta.state == 0) {
                            this.ta.state = 1;
                            VirtualKey.b(this.ta);
                        }
                        this.ta = virtualKey;
                    }
                    if (this.ta != null) {
                        this.ta.state = 0;
                        VirtualKey.b(this.ta);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sX = true;
        }
        if (this.sX) {
            if (this.sU > 0 && this.state == 1) {
                this.sV++;
                this.pe.setAlpha(255 - ((this.sV * 255) / this.sU));
                if (this.sV >= this.sU) {
                    this.sV = 0;
                    this.sX = false;
                }
            }
            if (this.state == 1) {
                if (this.tA > 0.0f) {
                    this.tA -= 15.0f;
                    this.tA = this.tA < 0.0f ? 0.0f : this.tA;
                } else if (this.tA < 0.0f) {
                    this.tA += 15.0f;
                    this.tA = this.tA > 0.0f ? 0.0f : this.tA;
                }
            }
            if (this.sT != null) {
                canvas.save();
                if (this.tA != 0.0f) {
                    this.pT.setRotate(this.tA * -1.0f, (float) (this.sT[this.state].getWidth() >> 1), (float) (this.sT[this.state].getHeight() >> 1));
                }
                canvas.translate((float) (this.centerX - (this.sT[this.state].getWidth() >> 1)), (float) (this.centerY - (this.sT[this.state].getHeight() >> 1)));
                canvas.drawBitmap(this.sT[this.state], this.pT, null);
                canvas.restore();
                this.pT.reset();
            }
        }
    }

    public final void reset() {
        this.state = 1;
        this.tz = -1.0f;
        this.orientation = 0;
    }
}
