package org.meteoroid.plugin.vd;

import java.util.Iterator;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public final void ce() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) cn.rp;
        Iterator it = defaultVirtualDevice.cg().iterator();
        while (it.hasNext()) {
            cr crVar = (cr) it.next();
            if (crVar != defaultVirtualDevice.ch()) {
                crVar.setVisible(false);
            }
        }
    }

    public final void cf() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) cn.rp;
        Iterator it = defaultVirtualDevice.cg().iterator();
        while (it.hasNext()) {
            cr crVar = (cr) it.next();
            if (crVar != defaultVirtualDevice.ch()) {
                crVar.setVisible(true);
            }
        }
    }

    public final void setVisible(boolean z) {
    }
}
