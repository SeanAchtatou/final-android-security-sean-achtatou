package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public abstract class AbstractButton implements cr {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    int delay;
    int id = -1;
    Paint pe = new Paint();
    private cq rp;
    Rect sR;
    Rect sS;
    Bitmap[] sT;
    int sU;
    int sV;
    boolean sW = true;
    boolean sX = true;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.sT = dl.O(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.sR = dl.M(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.sS = dl.M(attributeValue2);
        }
        String attributeValue3 = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                this.sU = Integer.parseInt(split[0]);
            } else {
                this.sU = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
    }

    public void a(cq cqVar) {
        this.rp = cqVar;
        if (this.sR == null) {
            this.sR = this.sS;
        }
    }

    public final boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length - 1 && bitmapArr[this.state] != null;
    }

    public boolean bR() {
        return this.sT != null;
    }

    public final int cb() {
        return this.state;
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        return h(i, i2, i3, i4);
    }

    public abstract boolean h(int i, int i2, int i3, int i4);

    public final boolean isTouchable() {
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sX = true;
        }
        if (this.sX) {
            if (this.sU > 0 && this.state == 1) {
                this.sV++;
                this.pe.setAlpha(255 - ((this.sV * 255) / this.sU));
                if (this.sV >= this.sU) {
                    this.sV = 0;
                    this.sX = false;
                }
            }
            if (a(this.sT)) {
                canvas.drawBitmap(this.sT[this.state], (Rect) null, this.sS, (this.sU == -1 || this.state != 1) ? null : this.pe);
            }
        }
    }

    public void setVisible(boolean z) {
        this.sX = z;
    }
}
