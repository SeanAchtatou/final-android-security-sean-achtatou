package org.meteoroid.plugin.vd;

import android.util.Log;
import android.view.Display;
import java.util.LinkedHashSet;
import java.util.Properties;

public class DefaultVirtualDevice implements cq {
    public static final int WIDGET_TYPE_ARCADE_JOYSTICK = 11;
    public static final int WIDGET_TYPE_BACKGROUND = 0;
    public static final int WIDGET_TYPE_CALL_OPTIONMENU = 12;
    public static final int WIDGET_TYPE_CHECKIN = 13;
    public static final int WIDGET_TYPE_DEVICE_SCREEN = 2;
    public static final int WIDGET_TYPE_EXIT_BUTTON = 6;
    public static final int WIDGET_TYPE_HIDE_VD = 10;
    public static final int WIDGET_TYPE_INTELEGENCE_BG = 7;
    public static final int WIDGET_TYPE_JOYSTICK = 3;
    public static final int WIDGET_TYPE_MUTE_SWITCHER = 4;
    public static final int WIDGET_TYPE_SENSOR_SWITCHER = 5;
    public static final int WIDGET_TYPE_SNSBUTTON = 14;
    public static final int WIDGET_TYPE_STEERINGWHEEL = 9;
    public static final int WIDGET_TYPE_URL_BUTTON = 8;
    public static final int WIDGET_TYPE_VIRTUAL_BUTTON = 1;
    public static final String[] tf = {"Background", "VirtualKey", "ScreenWidget", "Joystick", "MuteSwitcher", "SensorSwitcher", "ExitButton", "IntellegenceBackground", "URLButton", "SteeringWheel", "HideVirtualDeviceSwitcher", "ArcadeJoyStick", "CallOptionMenu", "CheckinButton", "SNSButton"};
    private final LinkedHashSet td = new LinkedHashSet();
    private ScreenWidget te;
    private int tg;

    private void a(cr crVar) {
        crVar.a(this);
        this.td.add(crVar);
        if (crVar.bR()) {
            bi.a(crVar);
        }
        if (crVar.isTouchable()) {
            bm.a(crVar);
        }
    }

    public final LinkedHashSet cg() {
        return this.td;
    }

    public final ScreenWidget ch() {
        return this.te;
    }

    public final int getOrientation() {
        return this.tg;
    }

    public final void onCreate() {
        String str;
        bw.a((int) VirtualKey.MSG_VIRTUAL_KEY_EVENT, "MSG_VIRTUAL_BUTTON_EVENT");
        Display defaultDisplay = cd.getActivity().getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int min = Math.min(width, height);
        int max = Math.max(width, height);
        if (min == 480 && max == 854) {
            str = "fwvga";
        } else if (min == 480 && max == 960) {
            str = "uwvga";
        } else if (min == 480 && max == 800) {
            str = "wvga";
        } else if (min == 360 && max == 640) {
            str = "nhd";
        } else if (min == 320 && max == 480) {
            str = "hvga";
        } else if (min == 240 && max == 320) {
            str = "qvga";
        } else if (min == 240 && max == 400) {
            str = "wqvga";
        } else if (min == 480 && max == 640) {
            str = "vga";
        } else if (min == 600 && max == 800) {
            str = "svga";
        } else if (min == 600 && max == 1024) {
            str = "wsvga";
        } else if (min == 640 && max == 960) {
            str = "retina";
        } else if (min == 540 && max == 960) {
            str = "qhd";
        } else if (min == 800 && max == 1280) {
            str = "wxga";
        } else {
            Log.w(cq.LOG_TAG, "Unkown screen resolution:" + min + "x" + max);
            str = null;
        }
        Properties properties = new Properties();
        try {
            properties.load(cd.getActivity().getResources().openRawResource(dl.L("vd_" + str)));
            if (properties.containsKey("widget.num")) {
                int parseInt = Integer.parseInt(properties.getProperty("widget.num"));
                dj djVar = new dj(properties);
                for (int i = 1; i <= parseInt; i++) {
                    try {
                        if (properties.containsKey("widget." + i + ".type")) {
                            String property = properties.getProperty("widget." + i + ".type");
                            try {
                                int parseInt2 = Integer.parseInt(property);
                                if (parseInt2 < 0 || parseInt2 >= tf.length) {
                                    Log.w(cq.LOG_TAG, "Unknown widget type:" + parseInt2);
                                } else {
                                    property = tf[parseInt2];
                                    if (property.equals(tf[2])) {
                                        ScreenWidget screenWidget = (ScreenWidget) bg.qg;
                                        screenWidget.a(djVar, "widget." + i + ".");
                                        screenWidget.ci();
                                        if (this.te != null) {
                                            ScreenWidget screenWidget2 = this.te;
                                            this.td.remove(screenWidget2);
                                            bi.b(screenWidget2);
                                            bm.b(screenWidget2);
                                        }
                                        if (screenWidget != null) {
                                            a(screenWidget);
                                        }
                                        this.te = screenWidget;
                                    } else {
                                        cr crVar = (cr) Class.forName("org.meteoroid.plugin.vd." + property).newInstance();
                                        crVar.a(djVar, "widget." + i + ".");
                                        a(crVar);
                                    }
                                }
                            } catch (Exception e) {
                            }
                        }
                    } catch (Exception e2) {
                        Log.w(cq.LOG_TAG, "Init widget[" + i + "] error." + e2);
                    }
                }
            }
            if (properties.containsKey("widget.orientation")) {
                String property2 = properties.getProperty("widget.orientation");
                this.tg = 2;
                if (property2.equals("landscape")) {
                    this.tg = 0;
                } else if (property2.equals("portrait")) {
                    this.tg = 1;
                } else if (property2.equals("auto")) {
                    this.tg = 4;
                } else {
                    Log.w(cq.LOG_TAG, "Orientation not specificed. It will be decided by user. ");
                }
                cd.O(this.tg);
            }
            properties.clear();
            System.gc();
        } catch (Exception e3) {
            Log.e(cq.LOG_TAG, "Load virtualdevice properties error. Maybe the file is missing." + e3);
        }
    }

    public final void onDestroy() {
        this.td.clear();
    }
}
