package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;

public abstract class ScreenWidget implements cr {
    public final Rect tm = new Rect();
    public final Paint tn = new Paint();
    public Rect to;
    public float tp;
    public float tq;
    public boolean tr;

    public abstract void a(int i, float f, float f2, int i2);

    public final boolean bR() {
        return true;
    }

    public abstract Bitmap bT();

    public final void ci() {
        if (bT() != null) {
            this.tp = ((float) this.tm.width()) / ((float) bT().getWidth());
            this.tq = ((float) this.tm.height()) / ((float) bT().getHeight());
            if (this.tp == 1.0f && this.tq == 1.0f) {
                this.tn.setFilterBitmap(false);
            } else {
                this.tn.setFilterBitmap(true);
            }
        }
    }

    public final boolean g(int i, int i2, int i3, int i4) {
        if (!this.tm.contains(i2, i3) || !this.tr) {
            return false;
        }
        a(i, ((float) (i2 - this.tm.left)) / this.tp, ((float) (i3 - this.tm.top)) / this.tq, i4);
        return false;
    }

    public final boolean isTouchable() {
        return this.tr;
    }
}
