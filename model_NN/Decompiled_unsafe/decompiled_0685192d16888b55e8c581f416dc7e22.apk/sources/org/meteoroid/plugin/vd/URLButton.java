package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;

public final class URLButton extends SimpleButton {
    private int count;
    private int index;
    private int sJ;
    public String[] tD;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.tD = attributeSet.getAttributeValue(str, "value").split(",");
        this.sJ = attributeSet.getAttributeIntValue(str, "interval", 40);
        this.count = 0;
        this.index = 0;
    }

    public final void onClick() {
        if (this.tD != null && this.tD[this.index] != null) {
            cd.F(this.tD[this.index]);
        }
    }

    public final void onDraw(Canvas canvas) {
        if (this.tD != null) {
            this.count++;
            if (this.count >= this.sJ) {
                this.count = 0;
                this.index++;
                if (this.index >= this.tD.length) {
                    this.index = 0;
                }
            }
        }
        if (a(this.sT)) {
            canvas.drawBitmap(this.sT[this.index], (Rect) null, this.sS, (Paint) null);
        }
    }
}
