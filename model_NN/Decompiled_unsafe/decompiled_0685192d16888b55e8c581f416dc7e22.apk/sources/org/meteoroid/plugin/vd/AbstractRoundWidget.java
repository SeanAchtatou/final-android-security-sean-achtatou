package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class AbstractRoundWidget extends AbstractButton {
    int centerX;
    int centerY;
    int sY;
    int sZ;
    VirtualKey ta;

    public final float a(float f, float f2) {
        float f3 = f - ((float) this.centerX);
        float f4 = f2 - ((float) this.centerY);
        double degrees = Math.toDegrees(Math.atan((double) (f4 / f3)));
        return (float) ((f3 < 0.0f || f4 >= 0.0f) ? (f3 >= 0.0f || f4 > 0.0f) ? (f3 > 0.0f || f4 <= 0.0f) ? (f3 <= 0.0f || f4 < 0.0f) ? degrees : 360.0d - Math.abs(degrees) : Math.abs(degrees) + 180.0d : 180.0d - degrees : Math.abs(degrees));
    }

    public void a(AttributeSet attributeSet, String str) {
        this.centerX = attributeSet.getAttributeIntValue(str, "x", cd.bH() / 2);
        this.centerY = attributeSet.getAttributeIntValue(str, "y", cd.bI() / 2);
        this.sY = attributeSet.getAttributeIntValue(str, "max", 60);
        this.sZ = attributeSet.getAttributeIntValue(str, "min", 5);
        String attributeValue = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue != null) {
            String[] split = attributeValue.split(",");
            if (split.length > 0) {
                this.sU = Integer.parseInt(split[0]);
            } else {
                this.sU = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
    }

    public boolean h(int i, int i2, int i3, int i4) {
        return false;
    }

    public final void release() {
        this.id = -1;
        if (this.ta != null && this.ta.state == 0) {
            this.ta.state = 1;
            VirtualKey.b(this.ta);
        }
        reset();
    }

    public abstract void reset();
}
