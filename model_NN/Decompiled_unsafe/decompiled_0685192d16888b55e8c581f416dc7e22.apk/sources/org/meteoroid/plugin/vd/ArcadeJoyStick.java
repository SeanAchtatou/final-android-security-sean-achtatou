package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

public class ArcadeJoyStick extends AbstractRoundWidget {
    public static final int CENTER = 0;
    public static final int DOWN = 2;
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int UP = 1;
    private static final VirtualKey[] tb = new VirtualKey[5];
    private boolean sX = true;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.sT = dl.O(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public final void a(cq cqVar) {
        super.a(cqVar);
        tb[0] = new VirtualKey();
        tb[0].tE = "RIGHT";
        tb[1] = new VirtualKey();
        tb[1].tE = "UP";
        tb[2] = new VirtualKey();
        tb[2].tE = "LEFT";
        tb[3] = new VirtualKey();
        tb[3].tE = "DOWN";
        tb[4] = tb[0];
        this.state = 0;
    }

    public final boolean bR() {
        return this.sT != null;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x003b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean h(int r11, int r12, int r13, int r14) {
        /*
            r10 = this;
            r9 = 1124532224(0x43070000, float:135.0)
            r7 = 4611686018427387904(0x4000000000000000, double:2.0)
            r6 = 0
            r5 = 1110704128(0x42340000, float:45.0)
            r4 = 1
            int r0 = r10.centerX
            int r0 = r12 - r0
            double r0 = (double) r0
            double r0 = java.lang.Math.pow(r0, r7)
            int r2 = r10.centerY
            int r2 = r13 - r2
            double r2 = (double) r2
            double r2 = java.lang.Math.pow(r2, r7)
            double r0 = r0 + r2
            double r0 = java.lang.Math.sqrt(r0)
            java.lang.Thread.yield()
            int r2 = r10.sY
            double r2 = (double) r2
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x00aa
            int r2 = r10.sZ
            double r2 = (double) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x00aa
            switch(r11) {
                case 0: goto L_0x0035;
                case 1: goto L_0x00a2;
                case 2: goto L_0x0037;
                default: goto L_0x0033;
            }
        L_0x0033:
            r0 = r4
        L_0x0034:
            return r0
        L_0x0035:
            r10.id = r14
        L_0x0037:
            int r0 = r10.id
            if (r0 != r14) goto L_0x0033
            float r0 = (float) r12
            float r1 = (float) r13
            float r0 = r10.a(r0, r1)
            int r1 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r1 < 0) goto L_0x0078
            int r1 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            if (r1 >= 0) goto L_0x0078
            r1 = r4
        L_0x004a:
            r10.state = r1
            org.meteoroid.plugin.vd.VirtualKey[] r1 = org.meteoroid.plugin.vd.ArcadeJoyStick.tb
            float r0 = r0 + r5
            r2 = 1119092736(0x42b40000, float:90.0)
            float r0 = r0 / r2
            int r0 = (int) r0
            r0 = r1[r0]
            org.meteoroid.plugin.vd.VirtualKey r1 = r10.ta
            if (r1 == r0) goto L_0x006e
            org.meteoroid.plugin.vd.VirtualKey r1 = r10.ta
            if (r1 == 0) goto L_0x006c
            org.meteoroid.plugin.vd.VirtualKey r1 = r10.ta
            int r1 = r1.state
            if (r1 != 0) goto L_0x006c
            org.meteoroid.plugin.vd.VirtualKey r1 = r10.ta
            r1.state = r4
            org.meteoroid.plugin.vd.VirtualKey r1 = r10.ta
            org.meteoroid.plugin.vd.VirtualKey.b(r1)
        L_0x006c:
            r10.ta = r0
        L_0x006e:
            org.meteoroid.plugin.vd.VirtualKey r0 = r10.ta
            r0.state = r6
            org.meteoroid.plugin.vd.VirtualKey r0 = r10.ta
            org.meteoroid.plugin.vd.VirtualKey.b(r0)
            goto L_0x0033
        L_0x0078:
            int r1 = (r0 > r9 ? 1 : (r0 == r9 ? 0 : -1))
            if (r1 < 0) goto L_0x0084
            r1 = 1130430464(0x43610000, float:225.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x0084
            r1 = 3
            goto L_0x004a
        L_0x0084:
            r1 = 1130430464(0x43610000, float:225.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 < 0) goto L_0x0093
            r1 = 1134395392(0x439d8000, float:315.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x0093
            r1 = 2
            goto L_0x004a
        L_0x0093:
            r1 = 1134395392(0x439d8000, float:315.0)
            int r1 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x009e
            int r1 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
            if (r1 >= 0) goto L_0x00a0
        L_0x009e:
            r1 = 4
            goto L_0x004a
        L_0x00a0:
            r1 = r6
            goto L_0x004a
        L_0x00a2:
            int r0 = r10.id
            if (r0 != r14) goto L_0x0033
            r10.release()
            goto L_0x0033
        L_0x00aa:
            if (r11 != r4) goto L_0x00b3
            int r0 = r10.id
            if (r0 != r14) goto L_0x00b3
            r10.release()
        L_0x00b3:
            r0 = r6
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.vd.ArcadeJoyStick.h(int, int, int, int):boolean");
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sX = true;
        }
        if (this.sX) {
            if (this.sU > 0 && this.state == 1) {
                this.sV++;
                this.pe.setAlpha(255 - ((this.sV * 255) / this.sU));
                if (this.sV >= this.sU) {
                    this.sV = 0;
                    this.sX = false;
                }
            }
            if (a(this.sT)) {
                canvas.drawBitmap(this.sT[this.state], (float) (this.centerX - (this.sT[this.state].getWidth() / 2)), (float) (this.centerY - (this.sT[this.state].getHeight() / 2)), (Paint) null);
            }
        }
    }

    public final void reset() {
        this.state = 0;
    }

    public final void setVisible(boolean z) {
        this.sX = z;
    }
}
