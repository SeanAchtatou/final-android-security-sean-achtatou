package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;

public final class IntellegenceBackground extends Background {
    public static final int SCREEN_BOTTOM = 1;
    public static final int SCREEN_LEFT = 2;
    public static final int SCREEN_RIGHT = 3;
    public static final int SCREEN_TOP = 0;
    int mode;

    public final void a(AttributeSet attributeSet, String str) {
        this.pf = dl.M(attributeSet.getAttributeValue(str, "rect"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 0);
    }

    public final void onDraw(Canvas canvas) {
        Bitmap bT;
        int pixel;
        if (!(this.rp == null || (bT = ((DefaultVirtualDevice) this.rp).ch().bT()) == null)) {
            int width = bT.getWidth();
            int height = bT.getHeight();
            switch (this.mode) {
                case 0:
                    pixel = bT.getPixel(width >> 1, 0);
                    break;
                case 1:
                    pixel = bT.getPixel(width >> 1, height - 1);
                    break;
                case 2:
                    pixel = bT.getPixel(0, height >> 1);
                    break;
                case 3:
                    pixel = bT.getPixel(width - 1, height >> 1);
                    break;
                default:
                    pixel = -16777216;
                    break;
            }
            if (this.pf != null) {
                this.color = pixel;
            }
        }
        super.onDraw(canvas);
    }
}
