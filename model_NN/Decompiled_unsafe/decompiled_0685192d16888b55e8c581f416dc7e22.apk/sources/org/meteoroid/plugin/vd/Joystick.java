package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] tb = new VirtualKey[5];
    private static final VirtualKey[] th = new VirtualKey[9];
    private int mode;
    private boolean sX = true;
    private int ti;
    private int tj;
    Bitmap[] tk;
    Bitmap[] tl;

    public final void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.tk = dl.O(attributeSet.getAttributeValue(str, "stick"));
        this.tl = dl.O(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public final void a(cq cqVar) {
        super.a(cqVar);
        if (this.mode == 8) {
            th[0] = new VirtualKey();
            th[0].tE = "RIGHT";
            th[1] = new VirtualKey();
            th[1].tE = "NUM_3";
            th[2] = new VirtualKey();
            th[2].tE = "UP";
            th[3] = new VirtualKey();
            th[3].tE = "NUM_1";
            th[4] = new VirtualKey();
            th[4].tE = "LEFT";
            th[5] = new VirtualKey();
            th[5].tE = "NUM_7";
            th[6] = new VirtualKey();
            th[6].tE = "DOWN";
            th[7] = new VirtualKey();
            th[7].tE = "NUM_9";
            th[8] = th[0];
        } else if (this.mode == 10) {
            th[0] = new VirtualKey();
            th[0].tE = "NUM_6";
            th[1] = new VirtualKey();
            th[1].tE = "NUM_3";
            th[2] = new VirtualKey();
            th[2].tE = "NUM_2";
            th[3] = new VirtualKey();
            th[3].tE = "NUM_1";
            th[4] = new VirtualKey();
            th[4].tE = "NUM_4";
            th[5] = new VirtualKey();
            th[5].tE = "NUM_7";
            th[6] = new VirtualKey();
            th[6].tE = "NUM_8";
            th[7] = new VirtualKey();
            th[7].tE = "NUM_9";
            th[8] = th[0];
        } else if (this.mode == 6) {
            tb[0] = new VirtualKey();
            tb[0].tE = "NUM_6";
            tb[1] = new VirtualKey();
            tb[1].tE = "NUM_2";
            tb[2] = new VirtualKey();
            tb[2].tE = "NUM_4";
            tb[3] = new VirtualKey();
            tb[3].tE = "NUM_8";
            tb[4] = tb[0];
        } else {
            tb[0] = new VirtualKey();
            tb[0].tE = "RIGHT";
            tb[1] = new VirtualKey();
            tb[1].tE = "UP";
            tb[2] = new VirtualKey();
            tb[2].tE = "LEFT";
            tb[3] = new VirtualKey();
            tb[3].tE = "DOWN";
            tb[4] = tb[0];
        }
        reset();
    }

    public final boolean bR() {
        return (this.tl == null && this.tk == null) ? false : true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean h(int r9, int r10, int r11, int r12) {
        /*
            r8 = this;
            r4 = 4611686018427387904(0x4000000000000000, double:2.0)
            r7 = 1
            r6 = 0
            int r0 = r8.centerX
            int r0 = r10 - r0
            double r0 = (double) r0
            double r0 = java.lang.Math.pow(r0, r4)
            int r2 = r8.centerY
            int r2 = r11 - r2
            double r2 = (double) r2
            double r2 = java.lang.Math.pow(r2, r4)
            double r0 = r0 + r2
            double r0 = java.lang.Math.sqrt(r0)
            java.lang.Thread.yield()
            int r2 = r8.sY
            double r2 = (double) r2
            int r2 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0092
            int r2 = r8.sZ
            double r2 = (double) r2
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 < 0) goto L_0x0092
            switch(r9) {
                case 0: goto L_0x0031;
                case 1: goto L_0x008a;
                case 2: goto L_0x0033;
                default: goto L_0x002f;
            }
        L_0x002f:
            r0 = r7
        L_0x0030:
            return r0
        L_0x0031:
            r8.id = r12
        L_0x0033:
            int r0 = r8.id
            if (r0 != r12) goto L_0x002f
            r8.state = r6
            r8.ti = r10
            r8.tj = r11
            float r0 = (float) r10
            float r1 = (float) r11
            float r0 = r8.a(r0, r1)
            int r1 = r8.mode
            r2 = 8
            if (r1 != r2) goto L_0x007e
            org.meteoroid.plugin.vd.VirtualKey[] r1 = org.meteoroid.plugin.vd.Joystick.th
            double r2 = (double) r0
            r4 = 4627026404658118656(0x4036800000000000, double:22.5)
            double r2 = r2 + r4
            r4 = 4631530004285489152(0x4046800000000000, double:45.0)
            double r2 = r2 / r4
            int r0 = (int) r2
            r0 = r1[r0]
        L_0x005b:
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.ta
            if (r1 == r0) goto L_0x0074
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.ta
            if (r1 == 0) goto L_0x0072
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.ta
            int r1 = r1.state
            if (r1 != 0) goto L_0x0072
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.ta
            r1.state = r7
            org.meteoroid.plugin.vd.VirtualKey r1 = r8.ta
            org.meteoroid.plugin.vd.VirtualKey.b(r1)
        L_0x0072:
            r8.ta = r0
        L_0x0074:
            org.meteoroid.plugin.vd.VirtualKey r0 = r8.ta
            r0.state = r6
            org.meteoroid.plugin.vd.VirtualKey r0 = r8.ta
            org.meteoroid.plugin.vd.VirtualKey.b(r0)
            goto L_0x002f
        L_0x007e:
            org.meteoroid.plugin.vd.VirtualKey[] r1 = org.meteoroid.plugin.vd.Joystick.tb
            r2 = 1110704128(0x42340000, float:45.0)
            float r0 = r0 + r2
            r2 = 1119092736(0x42b40000, float:90.0)
            float r0 = r0 / r2
            int r0 = (int) r0
            r0 = r1[r0]
            goto L_0x005b
        L_0x008a:
            int r0 = r8.id
            if (r0 != r12) goto L_0x002f
            r8.release()
            goto L_0x002f
        L_0x0092:
            if (r9 != r7) goto L_0x009b
            int r0 = r8.id
            if (r0 != r12) goto L_0x009b
            r8.release()
        L_0x009b:
            r0 = r6
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.vd.Joystick.h(int, int, int, int):boolean");
    }

    public final void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.sX = true;
        }
        if (this.sX) {
            if (this.sU > 0 && this.state == 1) {
                this.sV++;
                this.pe.setAlpha(255 - ((this.sV * 255) / this.sU));
                if (this.sV >= this.sU) {
                    this.sV = 0;
                    this.sX = false;
                }
            }
            if (a(this.tl)) {
                canvas.drawBitmap(this.tl[this.state], (float) (this.centerX - (this.tl[this.state].getWidth() / 2)), (float) (this.centerY - (this.tl[this.state].getHeight() / 2)), (this.sU == -1 || this.state != 1) ? null : this.pe);
            }
            if (a(this.tk)) {
                canvas.drawBitmap(this.tk[this.state], (float) (this.ti - (this.tk[this.state].getWidth() / 2)), (float) (this.tj - (this.tk[this.state].getHeight() / 2)), (this.sU == -1 || this.state != 1) ? null : this.pe);
            }
        }
    }

    public final void reset() {
        this.ti = this.centerX;
        this.tj = this.centerY;
        this.state = 1;
    }

    public final void setVisible(boolean z) {
        this.sX = z;
    }
}
