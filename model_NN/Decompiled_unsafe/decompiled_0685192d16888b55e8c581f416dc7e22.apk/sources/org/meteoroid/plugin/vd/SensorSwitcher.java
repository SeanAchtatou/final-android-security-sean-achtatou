package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static final VirtualKey[] tb = new VirtualKey[4];
    private int tg;
    private int[] ts;
    private int tt = 2;
    private int tu = 2;
    private VirtualKey tv;
    private final int[] tw = new int[3];
    private int tx = 0;
    private int x;
    private int y;
    private int z;

    private synchronized void a(VirtualKey virtualKey) {
        if (virtualKey != this.tv) {
            cj();
        }
        if (virtualKey != null && virtualKey.state == 1) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.tv = virtualKey;
        }
    }

    private void cj() {
        if (this.tv != null && this.tv.state == 0) {
            this.tv.state = 1;
            VirtualKey.b(this.tv);
            this.tv = null;
        }
    }

    public final void a(AttributeSet attributeSet, String str) {
        this.sT = dl.O(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.sR = dl.M(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.sS = dl.M(attributeValue2);
        }
        this.sU = attributeSet.getAttributeIntValue(str, "fade", -1);
        String attributeValue3 = attributeSet.getAttributeValue(str, "value");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(",");
            if (split.length > 0) {
                try {
                    if (Integer.parseInt(split[0]) == 1) {
                        ce();
                    }
                } catch (NumberFormatException e) {
                    if (Boolean.parseBoolean(split[0])) {
                        ce();
                    }
                }
            }
            if (split.length >= 3) {
                this.tt = Integer.parseInt(split[1]);
                this.tu = Integer.parseInt(split[2]);
            }
            if (split.length >= 6) {
                this.ts = new int[3];
                this.ts[0] = Integer.parseInt(split[3]);
                this.ts[1] = Integer.parseInt(split[4]);
                this.ts[2] = Integer.parseInt(split[5]);
            }
        }
    }

    public final void a(cq cqVar) {
        super.a(cqVar);
        tb[0] = new VirtualKey();
        tb[0].tE = "UP";
        tb[0].state = 1;
        tb[1] = new VirtualKey();
        tb[1].tE = "DOWN";
        tb[1].state = 1;
        tb[2] = new VirtualKey();
        tb[2].tE = "LEFT";
        tb[2].state = 1;
        tb[3] = new VirtualKey();
        tb[3].tE = "RIGHT";
        tb[3].state = 1;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.tx = 0;
        this.tw[0] = 0;
        this.tw[1] = 0;
        this.tw[2] = 0;
        cj();
    }

    public final void ce() {
        bm.a((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch on.");
    }

    public final void cf() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.tx = 0;
        this.tw[0] = 0;
        this.tw[1] = 0;
        this.tw[2] = 0;
        cj();
        bm.b((SensorEventListener) this);
        Log.w("SensorSwitcher", "Switch off.");
    }

    public final void onAccuracyChanged(Sensor sensor, int i) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        this.tg = cn.rp.getOrientation();
        if (this.tg == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.tg == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w("SensorSwitcher", "deviceOrientation:" + this.tg);
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.tx <= 0) {
            if (this.ts != null) {
                this.tw[0] = this.ts[0];
                this.tw[1] = this.ts[1];
                this.tw[2] = this.ts[2];
            } else {
                this.tw[0] = this.x;
                this.tw[1] = this.y;
                this.tw[2] = this.z;
            }
            this.tx++;
            return;
        }
        int i = this.x - this.tw[0];
        int i2 = this.y - this.tw[1];
        int i3 = this.z - this.tw[2];
        if (Math.abs(i) < this.tt && Math.abs(i2) < this.tu && Math.abs(i3) < this.tu) {
            cj();
        } else if (Math.abs(i) >= this.tt) {
            if (i < 0) {
                a(tb[2]);
            } else {
                a(tb[3]);
            }
        } else if (Math.abs(i2) >= this.tu) {
            if (i2 > 0) {
                a(tb[1]);
            } else {
                a(tb[0]);
            }
            a(this.tv);
        } else if (Math.abs(i3) < this.tu) {
        } else {
            if (i3 > 0) {
                a(tb[0]);
            } else {
                a(tb[1]);
            }
        }
    }
}
