package org.meteoroid.plugin.vd;

public final class MuteSwitcher extends BooleanSwitcher {
    public final void ce() {
        bt.d(true);
    }

    public final void cf() {
        bt.d(false);
    }
}
