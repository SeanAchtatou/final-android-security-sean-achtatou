package org.meteoroid.plugin.vd;

import android.util.AttributeSet;

public abstract class BooleanSwitcher extends Switcher {
    public static final int OFF = 0;
    public static final int ON = 1;

    private void cc() {
        if (this.state == 1) {
            ce();
        } else {
            cf();
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.state = attributeSet.getAttributeBooleanValue(str, "value", false) ? 0 : 1;
        cc();
    }

    public final void cd() {
        this.state = this.state == 0 ? 1 : 0;
        cc();
    }

    public abstract void ce();

    public abstract void cf();
}
