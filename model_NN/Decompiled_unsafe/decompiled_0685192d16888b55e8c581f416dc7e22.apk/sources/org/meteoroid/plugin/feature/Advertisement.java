package org.meteoroid.plugin.feature;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import java.util.TimerTask;

public abstract class Advertisement extends TimerTask implements bq, bx, cp {
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    /* access modifiers changed from: package-private */
    public static RelativeLayout sK;
    private int count;
    private int duration = 20;
    private dk sH;
    String sI;
    int sJ = 60;
    private boolean sL = false;
    boolean sM = false;
    private int sN = 1000;
    boolean sO = true;
    private int sP;

    public void I(String str) {
        this.sH = new dk(str);
        String J = J("DURATION");
        if (J != null) {
            this.duration = Integer.parseInt(J);
        }
        String J2 = J("INTERVAL");
        if (J2 != null) {
            this.sJ = Integer.parseInt(J2);
        }
        String J3 = J("TEST");
        if (J3 != null) {
            this.sM = Boolean.parseBoolean(J3);
        }
        String J4 = J("ALIGN");
        if (J4 != null) {
            this.sI = J4;
        }
        String J5 = J("FAKECLICK");
        if (J5 != null) {
            this.sN = Integer.parseInt(J5);
            bm.a(this);
        }
        String J6 = J("PACKAGE");
        if (J6 == null || !cd.G(J6)) {
            bw.a(this);
        } else {
            Log.e(getClass().getSimpleName(), "The depended package [" + J6 + "] has already been installed. So disable the feature:" + getClass().getSimpleName());
        }
    }

    public final String J(String str) {
        return (String) this.sH.tG.get(str);
    }

    public final void a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.count >= this.sN && this.sO && bW() && this.sL) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            motionEvent.setLocation((float) (bY().getRight() - 20), (float) (bY().getTop() + 20));
            if (action == 0 && bY().dispatchTouchEvent(motionEvent)) {
                this.sP++;
            } else if (this.sP != 0 && action == 1 && bY().dispatchTouchEvent(motionEvent) && this.sP >= 2) {
                this.count = 0;
                this.sO = false;
                this.sP = 0;
            }
            motionEvent.setLocation(x, y);
        }
        if (action == 0) {
            this.count++;
        }
    }

    public final boolean a(Message message) {
        if (message.what != 23041) {
            return false;
        }
        cd.getHandler().post(new di(this));
        bw.b(this);
        return false;
    }

    public abstract boolean bW();

    public void bX() {
        if (bW() && !this.sL) {
            bY().setVisibility(0);
            this.sL = true;
        }
    }

    public abstract View bY();

    public final void bZ() {
        if (bW() && this.sL && this.duration != 0) {
            bY().setVisibility(8);
            this.sL = false;
        }
    }

    public void onDestroy() {
        bw.b(this);
    }

    public void run() {
        Handler handler = cd.getHandler();
        handler.post(new dg(this));
        if (this.duration != 0) {
            handler.postDelayed(new dh(this), (long) (this.duration * 1000));
        }
    }
}
