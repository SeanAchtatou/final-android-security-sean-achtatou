package org.meteoroid.plugin.feature;

import android.os.Message;
import android.os.SystemClock;
import com.network.app.util.Pay;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public class Mbounce implements bx, Runnable, AbstractPaymentManager.Payment {
    public final void I(String str) {
        Pay.j(cd.getActivity());
        bw.a(this);
    }

    public final boolean a(Message message) {
        if (message.what == 15391744) {
            synchronized (this) {
                Pay.aF();
                new Thread(this).start();
            }
            return true;
        }
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        return false;
    }

    public final void onDestroy() {
    }

    public void run() {
        while (Pay.mB == 0) {
            SystemClock.sleep(500);
        }
        if (Pay.mB == 2) {
            bw.b(bw.a((int) da.MSG_GCF_SMS_CONNECTION_SEND_FAIL, (Object) null));
            bw.b(bw.a((int) AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        } else if (Pay.mB == 1) {
            bw.b(bw.a((int) da.MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, (Object) null));
            bw.b(bw.a((int) AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
        }
    }
}
