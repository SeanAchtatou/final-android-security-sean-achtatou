package org.meteoroid.plugin.feature;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;

public class Admob extends Advertisement implements AdListener {
    private AdView sF;
    private AdRequest sG;

    public final void I(String str) {
        super.I(str);
        String J = J("ADMOB_PUBLISHER_ID");
        if (J == null) {
            Log.e(getClass().getSimpleName(), "Hey, the most important publish id is missed.");
        }
        AdSize adSize = AdSize.BANNER;
        String J2 = J("ADSIZE");
        if (J2 != null) {
            if (J2.equalsIgnoreCase("IAB")) {
                adSize = AdSize.IAB_BANNER;
            } else if (J2.equalsIgnoreCase("LEADERBOARD")) {
                adSize = AdSize.IAB_LEADERBOARD;
            } else if (J2.equalsIgnoreCase("MRECT")) {
                adSize = AdSize.IAB_MRECT;
            }
        }
        this.sF = new AdView(cd.getActivity(), adSize, J);
        this.sF.setAdListener(this);
        this.sF.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.sG = new AdRequest();
        String J3 = J("KEYWORDS");
        if (J3 != null) {
            String[] split = J3.split(";");
            for (String addKeyword : split) {
                this.sG.addKeyword(addKeyword);
            }
        }
        this.sG.setTesting(this.sM);
        this.sF.loadAd(this.sG);
        View bY = bY();
        String str2 = this.sI;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        if (Advertisement.sK == null) {
            RelativeLayout relativeLayout = new RelativeLayout(cd.getActivity());
            Advertisement.sK = relativeLayout;
            relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        }
        if (!(str2 == null || str2 == null)) {
            if (str2.equalsIgnoreCase("bottom")) {
                layoutParams.addRule(12);
            } else if (str2.equalsIgnoreCase("top")) {
                layoutParams.addRule(10);
            }
        }
        Advertisement.sK.addView(bY, layoutParams);
        cd.bJ().schedule(this, 0, (long) (this.sJ * 1000));
    }

    public final void av() {
        this.sO = true;
    }

    public final void aw() {
    }

    public final void ax() {
    }

    public final void ay() {
        if (!this.sF.isRefreshing()) {
            this.sF.loadAd(this.sG);
        }
        bw.b(bw.a((int) cd.MSG_SYSTEM_LOG_EVENT, new String[]{"OnClick", getClass().getSimpleName()}));
    }

    public final void az() {
    }

    public final boolean bW() {
        if (this.sF != null) {
            return this.sF.isReady();
        }
        return false;
    }

    public final void bX() {
        super.bX();
        if (!this.sF.isRefreshing()) {
            this.sF.loadAd(this.sG);
        }
    }

    public final View bY() {
        return this.sF;
    }

    public final void onDestroy() {
        this.sF.stopLoading();
        if (this.sF != null) {
            this.sF.setAdListener(null);
        }
        super.onDestroy();
    }
}
