package defpackage;

import android.util.Log;

/* renamed from: bg  reason: default package */
public final class bg {
    private static final String LOG_TAG = "DeviceManager";
    public static co qg;

    protected static co C(String str) {
        try {
            co coVar = (co) Class.forName(str).newInstance();
            qg = coVar;
            coVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create device. " + e);
            e.printStackTrace();
        }
        return qg;
    }
}
