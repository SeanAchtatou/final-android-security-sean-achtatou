package defpackage;

import android.util.Log;
import java.util.HashMap;

/* renamed from: dk  reason: default package */
public final class dk {
    public HashMap tG = new HashMap();

    public dk(String str) {
        if (str != null && str.length() > 0) {
            String[] split = str.split(",");
            for (int i = 0; i < split.length; i++) {
                int indexOf = split[i].indexOf(":");
                if (indexOf != -1) {
                    this.tG.put(split[i].substring(0, indexOf).trim(), split[i].substring(indexOf + 1));
                } else {
                    Log.w("Configuration", "Unkown args for configuration." + split[i]);
                }
            }
        }
    }
}
