package defpackage;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.util.Log;
import com.androidbox.sgmj2net8.R;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/* renamed from: dl  reason: default package */
public final class dl {
    public static final String LOG_TAG = "ResourceUtils";
    private static final HashMap tH = new HashMap();

    private static int K(String str) {
        try {
            return a(R.drawable.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get resource id:" + str);
        }
    }

    public static int L(String str) {
        try {
            return a(R.raw.class, str);
        } catch (Exception e) {
            throw new IOException("Fail to get raw id:" + str);
        }
    }

    public static Rect M(String str) {
        if (str == null) {
            Log.w(LOG_TAG, "Rect string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length != 4) {
            throw new IllegalArgumentException("Invalid rect string:" + str);
        }
        int parseInt = Integer.parseInt(split[0].trim());
        int parseInt2 = Integer.parseInt(split[1].trim());
        return new Rect(parseInt, parseInt2, Integer.parseInt(split[2].trim()) + parseInt, Integer.parseInt(split[3].trim()) + parseInt2);
    }

    public static Bitmap N(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        } else if (str.trim().length() == 0) {
            return null;
        } else {
            if (tH.containsKey(str)) {
                return (Bitmap) tH.get(str);
            }
            try {
                Bitmap decodeResource = BitmapFactory.decodeResource(cd.getActivity().getResources(), K(str.trim()));
                tH.put(str, decodeResource);
                return decodeResource;
            } catch (Exception e) {
                throw new IOException("Fail to load bitmap " + str + ":" + e.toString());
            }
        }
    }

    public static Bitmap[] O(String str) {
        if (str == null) {
            Log.e(LOG_TAG, "Bitmap string couldn't be null.");
            return null;
        }
        String[] split = str.split(",");
        if (split.length <= 0) {
            return null;
        }
        Bitmap[] bitmapArr = new Bitmap[split.length];
        for (int i = 0; i < split.length; i++) {
            bitmapArr[i] = N(split[i].trim());
        }
        return bitmapArr;
    }

    public static final String P(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? "" : str.substring(0, lastIndexOf + 1);
    }

    public static final String Q(String str) {
        int lastIndexOf = str.lastIndexOf(File.separator);
        return lastIndexOf == -1 ? str : str.substring(lastIndexOf + 1);
    }

    private static int a(Class cls, String str) {
        return cls.getField(str).getInt(null);
    }
}
