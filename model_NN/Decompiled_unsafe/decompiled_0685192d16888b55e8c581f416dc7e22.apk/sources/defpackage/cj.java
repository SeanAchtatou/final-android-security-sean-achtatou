package defpackage;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/* renamed from: cj  reason: default package */
public final class cj {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static cm rk;
    /* access modifiers changed from: private */
    public static FrameLayout rl;
    public static boolean rm = false;
    /* access modifiers changed from: private */
    public static final ViewGroup.LayoutParams rn = new ViewGroup.LayoutParams(-1, -1);

    public static void a(cm cmVar) {
        if (cmVar != null && cmVar != rk) {
            cd.getHandler().post(new cl(cmVar));
        }
    }

    public static boolean aV() {
        if (rk != null) {
            return rk.aV() || rm;
        }
        return false;
    }

    protected static void b(Activity activity) {
        bw.a((int) MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        FrameLayout frameLayout = new FrameLayout(activity);
        rl = frameLayout;
        frameLayout.setBackgroundColor(-16777216);
        rl.setId(ROOT_VIEW_ID);
        rl.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        rl.requestFocus();
        bw.a(new ck());
    }

    public static cm bM() {
        return rk;
    }

    public static FrameLayout bN() {
        return rl;
    }

    protected static void onDestroy() {
        rk = null;
    }
}
