package defpackage;

import com.nokia.mid.ui.DirectGraphics;
import com.nokia.mid.ui.DirectUtils;
import com.nokia.mid.ui.FullCanvas;
import game.GameMIDlet;
import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Random;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;

/* renamed from: o  reason: default package */
public final class o extends FullCanvas implements f, Runnable {
    final int KEY_DOWN_ARROW = -2;
    final int KEY_LEFT_ARROW = -3;
    final int KEY_RIGHT_ARROW = -4;
    final int KEY_SEND = -10;
    final int KEY_SOFTKEY1 = -6;
    final int KEY_SOFTKEY2 = -7;
    final int KEY_SOFTKEY3 = -5;
    final int KEY_UP_ARROW = -1;
    final byte aH = d.jm;
    public final byte bE = 2;
    public byte bL;
    final byte bP = 3;
    int ba;
    int bj;
    final byte cR = 7;
    final byte cl = d.jl;
    final byte dL = 1;
    public final byte dN = 0;
    int dc = 0;
    private int eF;
    int eH;
    private int eW = 16631808;
    int eZ = 0;
    int ek = 0;
    byte[] fE = {-2, 2, 0, 0};
    public byte fm;
    final byte fq = d.bc;
    public int gC = 0;
    public int gn;
    int hB = 0;
    int ij = 1;
    final byte ja = d.jr;
    final byte jf = 6;
    public final byte jk = 4;
    final byte jq = 9;
    final byte jr = d.jp;
    final byte js = 1;
    final byte jt = d.dL;
    public final byte jw = 5;
    int kI;
    int kJ = 16;
    private int kL = 4;
    final byte kM = 5;
    final byte kR = 2;
    int kS;
    private final int kW = 100;
    final byte kY = d.jt;
    int ke = 0;
    String nA = "载入中……";
    String[] nB = {"关  于", "退  出", "新的开始", "战  役", "死  斗", "高级进阶", "猛将榜", "声音设置", "帮  助"};
    public int[][] nC = {new int[]{10, 20, 50}, new int[]{1, 2, 5}, new int[]{100, Player.REALIZED, 500}};
    short[] nD = {65, 90, 115, 140, 165, 190, 215, 240, 265};
    short[] nE = {65, 90, 115, 140, 165};
    String[] nF = {"返回游戏", "高级进阶", "游戏设置", "游戏帮助", "返回菜单"};
    String[] nG = {"温酒斩华雄", "虎牢鏖战", "官渡之战", "千里走单骑", "华容道", "征讨樊城", "败走麦城", "雪耻彝陵", "终战许昌", "--------", "--------"};
    al[][] nH = null;
    al[] nI = new al[5];
    boolean[] nJ = null;
    al nK = null;
    final byte nL = 0;
    final byte nM = 2;
    final byte nN = 3;
    final byte nO = 4;
    final byte nP = 5;
    final byte nQ = 7;
    final byte nR = 8;
    final byte nS = 10;
    final byte nT = d.jl;
    final byte nU = d.jm;
    final byte nV = d.jn;
    final byte nW = d.jo;
    final byte nX = d.jq;
    final byte nY = d.bo;
    final byte nZ = d.dX;
    c nk;
    GameMIDlet nl;
    public int nm = 1;
    public int nn = 160;
    public int np;
    public int nq;
    public int nr;
    al ns = null;
    al nt = null;
    public final byte nu = 1;
    public final byte nv = 3;
    ak nw;
    DirectGraphics nx;
    aj ny;
    int[] nz = new int[2];
    public String[][] oA = {new String[]{"武圣印", "生命+200 重击+5%"}, new String[]{"玄武玉", "生命+1000"}, new String[]{"凝神宝珠 ", "重击+15%"}};
    int oB = 16;
    private String[] oC = {"  教学关卡", "  东汉末年，黄巾之乱既平。|然洛阳董卓欺天罔地，灭国弑君；秽乱宫禁，残害生灵；狼戾不仁，罪恶充积！|曹操向诸镇发矫诏，十七路诸侯联营二百余里齐聚虎牢关之下共诛董贼！", "  徐州一役，曹操大败刘备，致使刘关张三兄弟失散，刘备投奔袁绍，关羽暂降曹操。|刘备闻关羽身在曹处，遂柬绍攻曹，顺便探听关羽消息。|绍遣大将颜良作先锋，进攻白马。", "  关羽少事皇叔，誓同生死，下邳失守后，与曹操约三事而降。|今关羽探知刘备现在袁绍军中，回思昔日之盟，岂容违背？故不顾千难万险单骑离开曹营，寻刘备而去。", "  为了抵抗曹操大举南犯，孙权与刘备两家结盟，于赤壁大败曹操。|曹败军逃至华容道正遇关羽。", "  建安二十四年秋七月，文武百官拜贺刘备为汉中王，曹操大怒，遂结连东吴，欲取荆州，孔明即差使命就送官诰与云长，令先起兵取樊城，以救荆州.", "  关羽自擒于禁、斩庞德，威震天下，华夏皆惊，魏将无不闻之惊骇。|曹操几欲迁都，被司马懿谏止。|东吴久窥荆州，与曹联合共敌关羽，关羽势单力孤，被吕蒙所算。", "  麦城之败关羽被吕蒙奸计所算，险被毒害。|先帝震怒，不顾百官谏阻，起倾国之兵强攻东吴。|遂令关羽为先锋，直取彝陵，誓与之不共日月！", "  先帝自荡平东吴，江东六郡八十一州尽归为蜀，曹操势不足以敌。|先帝遂发兵讨魏，誓一统华夏！"};
    String[] oD = {"玩家可以按住5键进行攻击，根据动作次数按3键发动特殊攻击，比连续按5键方便很多。", "活用5+5+3挑起敌人后紧接普通攻击可以更轻松的打出高连击。 ", "主角的移动很重要，尽量避免被敌人包围。", "千万不要正面接近强力敌人，要从侧面接近，活用上下移动。", "升级时要优先考虑武器和生命的升级。", "有必杀时要及时按9键使用，受到敌人攻击时更要毫不犹豫使用。", "通全关后玩家就可以挑战第二关的吕布了，还可开启难度更高的猛将难度！", "有些关卡有隐藏的敌人，血量一般比较高。", "在场景边缘有坛子，其中有回复道具，要仔细的查找。", "不死命，多杀敌兵，少损血，200连击以上并全灭敌将才能获得最强的武神评价。", "一个区域中刷出敌兵的数量是固定的，如果把本区所有敌将击败则不会再出现敌兵。", "强力敌将有无敌护身技，能阻止主角连续攻击，需及时躲避，可多用5+3击晕的招式对付。", "玩家只要死亡一次无论表现再好也必然是最差的伍长评价，最好的评价是武神。", "击败终战许昌的最后君主曹操掉落春秋书的概率是最高的。", "吕布第三阶段的攻击力非常高，多用5+3击晕是对付他的最好办法。", "注意！！！获得最终武器后可以在准备界面任意选择武器！！！", "有些关卡过关后会出现隐藏的近路，如第二关会出现直接通往吕布的通路，前提是将吕布杀死。", "注意！！！所有关卡获得武神评价后会获得无限使用必杀的能力！！！"};
    public int[][][] oE = {new int[][]{new int[]{0, 0, 0, 0, 0, 0}, new int[]{10, 226, 186, 0, 252, 157}, new int[]{255, 138, 0, 255, 207, 0}, new int[]{141, 3, 162, 253, 3, 244}, new int[]{189, 9, 0, 255, 26, 0}}, new int[0][]};
    final byte oF = 0;
    final byte oG = 4;
    final byte oH = 6;
    final byte oI = 8;
    final byte oJ = 9;
    final byte oK = 10;
    final byte oL = d.jn;
    final byte oM = d.jo;
    int[] oN;
    int[] oO;
    int[] oP;
    int[][] oQ;
    int[] oR;
    al oS;
    long oT;
    int oU;
    private String[] oV;
    String[][] oW;
    int oX;
    int oY;
    String oZ;
    final byte oa = d.js;
    public byte[] ob = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, d.jl, d.jm, d.jn, d.jo, d.jp, d.jq, -17, d.bo, d.dL, d.dX, d.js};
    private short[] oc = {184, 277, 93, 53, 115, 66, 30, 37, 51, 53, 14, 4, 8, 91, 14, 48, 1, 5, 1, 30, 9, 52, 4};
    private short[][] od = {new short[]{0, 36, -1}, new short[]{2, 40, 1}};
    public boolean oe = false;
    n[] of = null;
    private int[][] og = {new int[]{16252015, 89, 34, 88, 1}, new int[]{16763392, 89, 35, 88, 2}, new int[]{12818947, 89, 37, 88, 2}};
    private int[][] oh = {new int[]{16187141, 74, 284, 139, 1}, new int[]{16428293, 74, 285, 139, 2}, new int[]{11820590, 74, 287, 139, 2}};
    private int[][] oi = {new int[]{12228501, 74, 284, 139, 1}, new int[]{8928831, 74, 285, 139, 2}, new int[]{6160384, 74, 287, 139, 2}};
    private int[][] oj = {new int[]{5694203, 74, 294, 119, 1}, new int[]{38655, 74, 295, 119, 2}, new int[]{213157, 74, 297, 119, 2}};
    private int[][] ok = {new int[]{5882675, 15, 303, 158, 1}, new int[]{4365090, 15, 304, 158, 2}, new int[]{1854219, 15, 306, 158, 2}};
    private int[][] ol = {new int[]{15803}, new int[]{15264}, new int[]{13916}, new int[]{8445}, new int[]{9966}, new int[]{6967}, new int[]{4244}, new int[]{342, 688, 639, 532, 480, 298, 229, 960, 1126, 899, 1793, 519, 581, 362, 168, DirectGraphics.TYPE_USHORT_565_RGB, 747, DirectGraphics.TYPE_USHORT_444_RGB, 860, 269, 235, 362, 239, 402, 795, 423, 942, 621, 1046, 2043, 515, 490, 572, 448, 413, 250, 227}, new int[]{345, 1162, 559, 706, 728, 1102, 553, 558, 487, 368, 304, ap.GAME_A_PRESSED, 450, 403, 646, 756, 320, 588, 318, 225, 197, 295, 209, 218, 600, 283, 269, 248, 231, 365, 385, 222, 507, 492, 1850, 1762, 376, 420, 393, 372, 329, 218, 261, 239, 375, 897, 448, 468, 309, 250, 227}, new int[]{1671, 2738, 1151, 1116, 1119, 1490, 770, 621, 788, 579, 250, 227, 313, 430, 335, 551, 535, 865, 926, 459, 498, 908, 318, 598, 438, 1637, 212, 238, 168, 536, 284, 508, 317, 425, 345, 1836, 420, 380, 373, 885, 872, 288, 892, 233, 316, 768, 319, 492, 328, 607, 318, 299, 372}, new int[]{6016}, new int[]{37016}, new int[]{1554, 1887, 2198, 2543, 2121, 2463, 1752, 1981, 2138}, new int[]{31157}, new int[]{8464}, new int[]{3206}, new int[]{963}, new int[]{1857, 689, 1175, 1918, 473}, new int[]{1861}, new int[]{1256}, new int[]{733, 733, 762, 330, 355, 350, 317, 323, 317}, new int[]{389, 2897, 1844, 2057, 457, 1157, 1420, 660, 757, 622, 556, 307, 267, 534, 310, 627, 501, 298, 329, 415, 1019, 956, 625, 410, 439, 376, 420, 372, 1805, 464, 338, 317, 419, 536, 284, 501, 318, 242, 212, 720, 162, 456, 598, 486, 460, 513, 891, 341, 299, 430, 250, 227, 551, 517}, new int[]{446, 439, 478, 489}};
    private byte[][] om = {new byte[]{1, 9}, new byte[]{10, d.jl, d.jm}, new byte[]{5, 6, 7}, new byte[]{d.jn}};
    public int[][][] on = {new int[][]{new int[]{-70, 0, 0}}, new int[][]{new int[]{38, 0, 0}, new int[]{-112, 107, 0}, new int[]{223, 107, 3}}, new int[][]{new int[]{151, 0, 0}, new int[]{187, 0, 3}, new int[]{-64, 107, 0}, new int[]{153, 107, 3}, new int[]{174, 214, 3}}, new int[][]{new int[]{151, 0, 0}, new int[]{187, 0, 3}, new int[]{44, 107, 0}, new int[]{80, 107, 3}, new int[]{71, 214, 3}}, new int[][]{new int[]{151, 0, 0}, new int[]{187, 0, 3}, new int[]{44, 107, 0}, new int[]{80, 107, 3}, new int[]{-63, 214, 0}, new int[]{-27, 214, 3}}, new int[][]{new int[]{38, 0, 0}, new int[]{44, 107, 0}, new int[]{80, 107, 3}, new int[]{-63, 214, 0}, new int[]{-27, 214, 3}}, new int[][]{new int[]{-70, 0, 0}, new int[]{-64, 107, 0}, new int[]{153, 107, 3}, new int[]{-63, 214, 0}, new int[]{-27, 214, 3}}, new int[][]{new int[]{-112, 107, 0}, new int[]{223, 107, 3}, new int[]{71, 214, 3}}, new int[][]{new int[]{174, 214, 3}}};
    public int[][][] oo = {new int[0][], new int[][]{new int[]{0, 0, 38, 107}}, new int[][]{new int[]{0, 0, 151, 107}}, new int[][]{new int[]{0, 0, 151, 107}, new int[]{0, 107, 44, 107}, new int[]{222, 107, 18, 107}, new int[]{213, 214, 27, 107}}, new int[][]{new int[]{0, 0, 151, 107}, new int[]{0, 107, 44, 107}, new int[]{222, 107, 18, 107}, new int[]{115, 214, 125, 107}}, new int[][]{new int[]{0, 0, 38, 107}, new int[]{0, 107, 44, 107}, new int[]{222, 107, 18, 107}, new int[]{115, 214, 125, 107}}, new int[][]{new int[]{115, 214, 125, 107}}, new int[][]{new int[]{213, 214, 27, 107}}, new int[0][]};
    private byte[][] op = {new byte[]{0, 1, 2, 3, 4, 4, 4}, new byte[]{4, 4, 4, 5, 6, 7, 8}};
    public byte[][] oq = {new byte[]{0, 0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 2, 2, 2, 2, 1, 1}, new byte[]{0, 0, 1, 1, 1, 2, 2, 1, 1, 0, 0, 0, 1, 1, 2, 2, 2, 1, 1, 1}};
    public int[] or = {0, 0, 0};
    private int[] os = {0, 0, 0};
    Random ot = new Random();
    public byte[][][] ou = {new byte[][]{new byte[]{3, 0, 36, 48, 0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}}, new byte[][]{new byte[]{3, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{1, 6, d.dL, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{2, 0, 36, 48, 0}}, new byte[][]{new byte[]{0}, new byte[]{2, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{3, 6, d.dL, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}};
    public byte[][][] ov = {new byte[][]{new byte[]{3, 0, 36, 48, 0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{1, 6, d.jr, d.jv, 1}, new byte[]{0}, new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{1, 6, d.jr, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{2, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{3, 0, 36, 48, 0}}, new byte[][]{new byte[]{0}, new byte[]{2, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{2, 3, d.dX, d.jw, 1}}, new byte[][]{new byte[]{0}, new byte[]{1, 0, 36, 48, 0}, new byte[]{0}, new byte[]{2, 0, 36, 48, 0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{1, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}}, new byte[][]{new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{0}, new byte[]{2, 3, d.dX, d.jw, 1}, new byte[]{0}, new byte[]{0}}};
    public String[] ow = {"生命+100", "生命+200", "生命+300", "生命+400|外功+20", "生命+500|外功+40|内功+10"};
    public String[] ox = {"伍长", "中郎将", "大将军", "武神", "武神", "武神"};
    public String[] oy = {" ", "|", "《", "》", "攻击符 ", "重击符 ", "春秋", "春秋", "攻击之书", "重击之书", "生命之书", "增加角色", "攻击力", "重击率", "生命力", "数量越多效果越好|", "等级越高效果越好|", "获得", "已有", "/", "册。", "已收集", "个", "顶级神器必备之物", "集满", "长坂救主", "赤壁之战", "全武将击破吧！", "已获得顶级武器！", "已达到最高级！", "经验值不足！", "游戏保存成功！", "开启战役", "未开启战役！", "未解锁模式！|选择开启全部关卡后即可解锁", "升级成功", "等级", "保留！", "已拥有更高等级物品。", "２４６８键控制移动！", "５键攻击！", "破敌成功！|从上方路口进入下一区域！", "按右软键后再进入升级界面！", "按５键把经验分配到属性上进行升级！", "怒气满！|按９键释放无双攻击！", "当前", "，", "：", "20", "死斗模式 成功开启！|主菜单中可以选择！", "以获得顶级武器！", "武器", "5+3可以击晕敌人！非常实用！", "下方绿条显示已获得的经验。", "该物品可以无限重复取得！", "攻击+5", "重击+1%", "全部收集可获终极武器", "收集全满！", "全武将击破！|即可获得终极武器！", "生命+100", "生命+200", "生命+300", "生命+400|攻击+20", "生命+500|攻击+40|重击+10%", "另外还可尝试5+5+3和5+5+5+3！均有不同效果！", "按1键可以查看本关地图！", "左右键移动地图！右软键关闭地图。", "攻击+", "重击+", "生命+", "顶级武器需集满春秋,攻击+2000|重击+10%|生命+1500", "顶级武器需集满春秋,攻击+1500|重击+20%|生命+700", "温酒斩华雄速杀达成| 杀气+60", "乌巢烧粮达成，袁绍士气下降,袁绍血量下降20%", "完成百人斩！|获得武圣印", "完成200连斩!|获得玄武玉", "完成所有战役！|获得凝神宝珠", "详细信息按左软键后进入高级进阶查看", "关羽通关后貂婵开启", "获得此武器后才可更换使用！", "装备成功！", "吕布开始愤怒了！！", "追击吕布开启！|困难模式开启！", "吕布发狂了！！", "获得20本，|获得最终武器，|开启武器选择界面", "更换成功！", "无限无双开启！|战役中无双槽永久满槽状态！", "原", "提升量", "现", ""};
    public byte[][] oz = {new byte[]{4, 1, d.jl, d.jm, 1, d.js, 47, -127, d.jt, 68, -125}, new byte[]{5, 1, d.jl, d.jn, 1, d.js, 47, -127, d.jt, 69, -125}, new byte[]{d.jr, 47, 2, 6, 3, 46, 1, 57, 1, d.bo, -127, d.dL, 48, d.dX}, new byte[]{d.jr, 47, 2, 7, 3, 46, 1, 57, 1, d.bo, -127, d.dL, 48, d.dX}, new byte[]{d.jr, d.bc}, new byte[]{d.ju, 6, 46, d.jv, d.jw}, new byte[]{d.ju, 7, 46, d.bI, d.jw}, new byte[]{d.jx}, new byte[]{10, d.jl, d.jo}, new byte[]{8, d.jl, d.jm}, new byte[]{9, d.jl, d.jn}, new byte[]{d.hD}, new byte[]{d.hL}, new byte[]{4, 1, d.jl, d.jm, 1, d.js, 47, -127, d.jt}, new byte[]{39}, new byte[]{40}, new byte[]{41}, new byte[]{42}, new byte[]{43}, new byte[]{44}, new byte[]{d.jz, -124}, new byte[]{49}, new byte[]{d.jy}, new byte[]{88, d.jo, -118, 1, 89, -117, 1, 90, d.jo, -119}, new byte[]{88, d.jm, -116, 1, 89, -115, 1, 90, d.jm, -114}, new byte[]{88, d.jn, -113, 1, 89, -112, 1, 90, d.jn, -111}, new byte[]{51, 36, -127, 1, 35}, new byte[]{d.jo, 36, -127, 1, d.hL}, new byte[]{d.jm, 36, -127, 1, d.hL}, new byte[]{d.jn, 36, -127, 1, d.hL}, new byte[]{50}, new byte[]{d.jr, 2, 8, 3, 1, -127, d.dL, 48, d.dX, 1, 68, -122}, new byte[]{d.jr, -127, 2, 8, 3, 1, 38}, new byte[]{d.jr, 2, 9, 3, 1, -127, d.dL, 48, d.dX, 1, 69, -121}, new byte[]{d.jr, 36, -127, 2, 9, 3, 1, 38}, new byte[]{d.jr, 2, 10, 3, 1, -127, d.dL, 48, d.dX, 1, 70, -120}, new byte[]{d.jr, 36, -127, 2, 10, 3, 1, 38}, new byte[]{52}, new byte[]{53}, new byte[]{d.jr, 4, 55, 1, 54}, new byte[]{d.jr, 5, 56, 1, 54}, new byte[]{58, -126, 46, 59}, new byte[]{ToneControl.C4}, new byte[]{61}, new byte[]{62}, new byte[]{63}, new byte[]{64}, new byte[]{65}, new byte[]{66}, new byte[]{67}, new byte[]{57, 1, d.bo, -127, d.dL, 48, d.dX}, new byte[]{57, 1, d.bo, -127, d.dL, 48, d.dX}, new byte[]{33}, new byte[]{34}, new byte[]{71}, new byte[]{72}, new byte[]{73}, new byte[]{74}, new byte[]{75}, new byte[]{76}, new byte[]{77, 1, -110}, new byte[]{81}, new byte[]{82}, new byte[]{83}, new byte[]{78}, new byte[]{79}, new byte[]{80}, new byte[]{84}, new byte[]{6, 85}, new byte[]{7, 85}, new byte[]{86}, new byte[]{87}, new byte[]{88}, new byte[]{89, 91}, new byte[]{90, 91}};
    boolean pa;
    int pb;
    public byte[] pc;
    String[] pd;

    public o(GameMIDlet gameMIDlet) {
        DirectGraphics directGraphics = this.nx;
        DirectGraphics directGraphics2 = this.nx;
        DirectGraphics directGraphics3 = this.nx;
        DirectGraphics directGraphics4 = this.nx;
        this.oN = new int[]{0, 8192, 16384, 24576};
        DirectGraphics directGraphics5 = this.nx;
        DirectGraphics directGraphics6 = this.nx;
        DirectGraphics directGraphics7 = this.nx;
        DirectGraphics directGraphics8 = this.nx;
        DirectGraphics directGraphics9 = this.nx;
        DirectGraphics directGraphics10 = this.nx;
        this.oO = new int[]{0, 2, 1, 3, 90, 180, 270, 2, 90, 182, 270};
        this.oP = new int[]{0, 1, 2, 3, 4, 5, 6, 7};
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        ak akVar9 = this.nw;
        ak akVar10 = this.nw;
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        ak akVar15 = this.nw;
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        ak akVar18 = this.nw;
        ak akVar19 = this.nw;
        ak akVar20 = this.nw;
        ak akVar21 = this.nw;
        ak akVar22 = this.nw;
        ak akVar23 = this.nw;
        ak akVar24 = this.nw;
        ak akVar25 = this.nw;
        ak akVar26 = this.nw;
        ak akVar27 = this.nw;
        ak akVar28 = this.nw;
        ak akVar29 = this.nw;
        ak akVar30 = this.nw;
        ak akVar31 = this.nw;
        ak akVar32 = this.nw;
        this.oQ = new int[][]{new int[]{20, 36, 24, 40, 20, 36, 24, 40}, new int[]{24, 40, 20, 36, 24, 40, 20, 36}};
        ak akVar33 = this.nw;
        ak akVar34 = this.nw;
        ak akVar35 = this.nw;
        ak akVar36 = this.nw;
        ak akVar37 = this.nw;
        ak akVar38 = this.nw;
        this.oR = new int[]{36, 33, 40};
        this.oS = null;
        this.oT = 0;
        this.kS = 8;
        this.oU = 0;
        this.oV = new String[]{"吾乃解良一武夫,|蒙吾主以手足相待,|安肯背义投敌国乎?|城若破,有死而已。|玉可碎而不可改其白,|竹可焚而不可毁其节,|身虽殒,|名可垂于竹帛", "滚滚长江东逝水|浪花淘尽英雄 |是非成败转头空 |青山依旧在几度夕阳红 |白发渔樵江楮上 |惯看秋月春风 |一壶浊酒喜相逢 |古今多少事都付笑谈中"};
        this.eF = 0;
        this.oW = new String[][]{new String[]{"长刀", "龙纹刀", "玄黄", "青龙偃月", "破天"}, new String[]{"花伞", "竹伞", "灵蛇", "赤月", "遮天"}};
        this.ba = 0;
        this.oX = 0;
        this.oY = 0;
        this.oZ = "右软键进入商店!";
        this.eH = 0;
        this.kI = 0;
        this.pa = false;
        this.pb = 0;
        this.fm = 0;
        this.bL = 0;
        this.pc = new byte[]{-29, -15, -5, 10, d.dX, d.jv, d.dX};
        this.bj = 0;
        this.pd = new String[]{"原地满血满必杀复活！并获得8000点经验值。信息费2元/条（共1条，合计2元），不含通信费", "8000点经验值。信息费2元/条（共1条，合计2元），不含通信费", "", "10本春秋书，集齐20本可获最强武器。信息费2元/条（共1条，合计2元），不含通信费", "", "开启武器升级上限到最高级。信息费2元/条（共1条，合计2元），不含通信费", "开启角色全部攻击升级上限。信息费2元/条（共1条，合计2元），不含通信费", "", "", "开启角色全部生命升级上限。信息费2元/条（共1条，合计2元），不含通信费", "开启角色全部重击升级上限。信息费2元/条（共1条，合计2元），不含通信费", ""};
        aN();
        this.nl = gameMIDlet;
        this.nk = new c(this, this.nl);
        this.gn = this.nk.aR;
        this.np = this.nk.aS;
        this.nq = this.nk.aT;
        this.nr = this.nk.aU;
        o();
        if (this.of == null) {
            this.dc = 25;
            this.ek = 55;
            this.of = new n[2];
            this.of[0] = new n();
            this.of[1] = new n();
            this.of[0].b(38, 285, this.dc, this.ek);
            this.of[1].b(DirectGraphics.ROTATE_180, 285, this.dc, this.ek);
        }
        a();
    }

    private int A(int i) {
        switch (i) {
            case FullCanvas.KEY_SOFTKEY2:
                this.nk.getClass();
                return 6;
            case FullCanvas.KEY_SOFTKEY1:
                this.nk.getClass();
                return 5;
            case FullCanvas.KEY_SOFTKEY3:
            case ab.KEY_NUM5:
                this.nk.getClass();
                return 4;
            case FullCanvas.KEY_RIGHT_ARROW:
                this.nk.getClass();
                return 3;
            case FullCanvas.KEY_LEFT_ARROW:
                this.nk.getClass();
                return 2;
            case -2:
            case ab.KEY_NUM8:
                this.nk.getClass();
                return 1;
            case -1:
                this.nk.getClass();
                return 0;
            case ab.KEY_POUND:
                this.nk.getClass();
                return 16;
            case ab.KEY_STAR:
                this.nk.getClass();
                return 15;
            case ab.KEY_NUM0:
                this.nk.getClass();
                return 14;
            case ab.KEY_NUM1:
                this.nk.getClass();
                return 10;
            case ab.KEY_NUM2:
                this.nk.getClass();
                int i2 = this.nk.ci;
                this.nk.getClass();
                if (i2 != 5) {
                    return 0;
                }
                this.nk.getClass();
                return 18;
            case ab.KEY_NUM3:
                this.nk.getClass();
                return 11;
            case ab.KEY_NUM4:
                this.nk.getClass();
                int i3 = this.nk.ci;
                this.nk.getClass();
                if (i3 != 5) {
                    return 2;
                }
                this.nk.getClass();
                return 17;
            case ab.KEY_NUM6:
                this.nk.getClass();
                int i4 = this.nk.ci;
                this.nk.getClass();
                if (i4 != 5) {
                    return 3;
                }
                this.nk.getClass();
                return 19;
            case ab.KEY_NUM7:
                this.nk.getClass();
                return 12;
            case ab.KEY_NUM9:
                this.nk.getClass();
                return 13;
            default:
                return -1;
        }
    }

    private void H() {
        int i = this.nk.jP % 10;
        int i2 = (this.nk.jP / 10) % 10;
        int i3 = this.nk.jP / 100;
        if (this.nk.jQ) {
            if (this.nk.jO) {
                if (this.eH < 40) {
                    this.eH += 20;
                    int i4 = this.eH + 148;
                    ak akVar = this.nw;
                    ak akVar2 = this.nw;
                    c(13, i + 48, i4, 57, 20);
                    if (this.nk.jP >= 10 || i3 != 0) {
                        int i5 = this.eH + 134;
                        ak akVar3 = this.nw;
                        ak akVar4 = this.nw;
                        c(13, i2 + 48, i5, 57, 20);
                    }
                    if (this.nk.jP >= 100) {
                        int i6 = this.eH + 120;
                        ak akVar5 = this.nw;
                        ak akVar6 = this.nw;
                        c(13, i3 + 48, i6, 57, 20);
                    }
                } else {
                    this.eH = 0;
                    this.kI = 0;
                    this.nk.jO = false;
                    this.pa = true;
                }
            }
            if (this.pa) {
                this.kI++;
                if (this.kI <= 2) {
                    int i7 = this.gn - 25;
                    ak akVar7 = this.nw;
                    ak akVar8 = this.nw;
                    c(13, 76, i7, 55, 20);
                    ak akVar9 = this.nw;
                    ak akVar10 = this.nw;
                    c(13, i + 38, 218, 57, 24);
                    if (this.nk.jP >= 10 || i3 != 0) {
                        ak akVar11 = this.nw;
                        ak akVar12 = this.nw;
                        c(13, i2 + 38, 198, 57, 24);
                    }
                    if (this.nk.jP >= 100) {
                        ak akVar13 = this.nw;
                        ak akVar14 = this.nw;
                        c(13, i3 + 38, 178, 57, 24);
                        return;
                    }
                    return;
                }
                int i8 = this.gn - 23;
                ak akVar15 = this.nw;
                ak akVar16 = this.nw;
                c(13, 76, i8, 55, 20);
                ak akVar17 = this.nw;
                ak akVar18 = this.nw;
                c(13, i + 48, 216, 57, 24);
                if (this.nk.jP >= 10 || i3 != 0) {
                    ak akVar19 = this.nw;
                    ak akVar20 = this.nw;
                    c(13, i2 + 48, 202, 57, 24);
                }
                if (this.nk.jP >= 100) {
                    ak akVar21 = this.nw;
                    ak akVar22 = this.nw;
                    c(13, i3 + 48, 188, 57, 24);
                }
            }
        }
    }

    private al a(String str, byte[] bArr) {
        byte[] o = o(str);
        for (int i = 0; i < o.length; i++) {
            bArr[i + 94] = o[i];
        }
        return al.a(bArr, 0, bArr.length);
    }

    private void a() {
        new Thread(this).start();
    }

    private void a(int i, int i2, int i3, String str) {
        this.nw.setColor(0);
        this.nw.e(20, 110, 199, 118);
        c(20, 110, 5, 0);
        a(this.nw, str, 29, 118, 185, this.eW);
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 19, 116, 235, 33);
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 31, 116, 231, 33);
    }

    private void a(int i, int i2, String str, int i3, int i4, int i5) {
        if (i4 == 0) {
            int i6 = 35 - i5;
            for (int i7 = 0; i7 < i3; i7++) {
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                c(13, 25, i + (i6 * i7), i2, 17);
            }
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 24, i - (i6 / 2), i2, 24);
            ak akVar5 = this.nw;
            ak akVar6 = this.nw;
            c(13, 24, ((i3 - 1) * i6) + (i6 / 2) + i, i2, 20, 2);
            ak akVar7 = this.nw;
            ak akVar8 = this.nw;
            this.nw.b(str, (((i3 - 1) * i6) / 2) + i, i2 + 4, 17);
        } else if (i4 == 1) {
            this.ba = (this.nz[this.eZ] / 3) % 2 == 0 ? 23 : 25;
            this.oX = (this.nz[this.eZ] / 3) % 2 == 0 ? 22 : 24;
            int i8 = 35 - i5;
            for (int i9 = 0; i9 < i3; i9++) {
                ak akVar9 = this.nw;
                ak akVar10 = this.nw;
                c(13, this.ba, i + (i8 * i9), i2, 17);
            }
            ak akVar11 = this.nw;
            ak akVar12 = this.nw;
            c(13, this.oX, i - (i8 / 2), i2, 24);
            ak akVar13 = this.nw;
            ak akVar14 = this.nw;
            c(13, this.oX, ((i3 - 1) * i8) + (i8 / 2) + i, i2, 20, 2);
            ak akVar15 = this.nw;
            ak akVar16 = this.nw;
            this.nw.b(str, (((i3 - 1) * i8) / 2) + i, i2 + 4, 17);
        } else if (i4 == 2) {
            this.ba = 23;
            this.oX = 22;
            int i10 = 35 - i5;
            for (int i11 = 0; i11 < i3; i11++) {
                ak akVar17 = this.nw;
                ak akVar18 = this.nw;
                c(13, this.ba, i + (i10 * i11), i2, 17);
            }
            ak akVar19 = this.nw;
            ak akVar20 = this.nw;
            c(13, this.oX, i - (i10 / 2), i2, 24);
            ak akVar21 = this.nw;
            ak akVar22 = this.nw;
            c(13, this.oX, ((i3 - 1) * i10) + (i10 / 2) + i, i2, 20, 2);
            ak akVar23 = this.nw;
            ak akVar24 = this.nw;
            this.nw.b(str, (((i3 - 1) * i10) / 2) + i, i2 + 4, 17);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v103, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v104, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v219, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v220, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v231, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v232, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r13, int r14, int[][] r15) {
        /*
            r12 = this;
            al r0 = r12.oS
            if (r0 == 0) goto L_0x0011
            ak r0 = r12.nw
            al r1 = r12.oS
            ak r2 = r12.nw
            ak r2 = r12.nw
            r2 = 20
            r0.a(r1, r13, r14, r2)
        L_0x0011:
            c r0 = r12.nk
            int r0 = r0.ci
            c r1 = r12.nk
            r1.getClass()
            r1 = 9
            if (r0 == r1) goto L_0x01d4
            byte[][][] r0 = r12.ou
            c r1 = r12.nk
            int r1 = r1.dO
            r0 = r0[r1]
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            r1 = 0
            byte r0 = r0[r1]
            if (r0 == 0) goto L_0x01d4
            int[] r0 = r12.or
            r1 = 0
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ou
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x01bc
            int[] r0 = r12.or
            r1 = 0
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x0059:
            int[] r0 = r12.or
            r1 = 1
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ou
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x01c4
            int[] r0 = r12.or
            r1 = 1
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x0081:
            int[] r0 = r12.or
            r1 = 2
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ou
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x01cc
            int[] r0 = r12.or
            r1 = 2
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x00a9:
            r0 = 0
            r10 = r0
        L_0x00ab:
            byte[][][] r0 = r12.ou
            c r1 = r12.nk
            int r1 = r1.dO
            r0 = r0[r1]
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            r1 = 0
            byte r0 = r0[r1]
            if (r10 >= r0) goto L_0x01d4
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 20
            r1 = r1[r2]
            byte[][] r2 = r12.oq
            byte[][][] r3 = r12.ou
            c r4 = r12.nk
            int r4 = r4.dO
            r3 = r3[r4]
            c r4 = r12.nk
            int r4 = r4.dY
            r3 = r3[r4]
            r4 = 4
            byte r3 = r3[r4]
            r2 = r2[r3]
            int[] r3 = r12.or
            r3 = r3[r10]
            byte r2 = r2[r3]
            byte[][][] r3 = r12.ou
            c r4 = r12.nk
            int r4 = r4.dO
            r3 = r3[r4]
            c r4 = r12.nk
            int r4 = r4.dY
            r3 = r3[r4]
            r4 = 1
            byte r3 = r3[r4]
            int r2 = r2 + r3
            r1 = r1[r2]
            r2 = 0
            r3 = 0
            al[][] r4 = r12.nH
            r5 = 20
            r4 = r4[r5]
            byte[][] r5 = r12.oq
            byte[][][] r6 = r12.ou
            c r7 = r12.nk
            int r7 = r7.dO
            r6 = r6[r7]
            c r7 = r12.nk
            int r7 = r7.dY
            r6 = r6[r7]
            r7 = 4
            byte r6 = r6[r7]
            r5 = r5[r6]
            int[] r6 = r12.or
            r6 = r6[r10]
            byte r5 = r5[r6]
            byte[][][] r6 = r12.ou
            c r7 = r12.nk
            int r7 = r7.dO
            r6 = r6[r7]
            c r7 = r12.nk
            int r7 = r7.dY
            r6 = r6[r7]
            r7 = 1
            byte r6 = r6[r7]
            int r5 = r5 + r6
            r4 = r4[r5]
            int r4 = r4.getWidth()
            al[][] r5 = r12.nH
            r6 = 20
            r5 = r5[r6]
            byte[][] r6 = r12.oq
            byte[][][] r7 = r12.ou
            c r8 = r12.nk
            int r8 = r8.dO
            r7 = r7[r8]
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r8 = 4
            byte r7 = r7[r8]
            r6 = r6[r7]
            int[] r7 = r12.or
            r7 = r7[r10]
            byte r6 = r6[r7]
            byte[][][] r7 = r12.ou
            c r8 = r12.nk
            int r8 = r8.dO
            r7 = r7[r8]
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r8 = 1
            byte r7 = r7[r8]
            int r6 = r6 + r7
            r5 = r5[r6]
            int r5 = r5.getHeight()
            r6 = 0
            c r7 = r12.nk
            short[][][] r7 = r7.km
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 1
            short r7 = r7[r8]
            byte[][][] r8 = r12.ou
            c r9 = r12.nk
            int r9 = r9.dO
            r8 = r8[r9]
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r9 = 2
            byte r8 = r8[r9]
            int r7 = r7 + r8
            int r7 = r7 + r13
            c r8 = r12.nk
            short[][][] r8 = r8.km
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r8 = r8[r10]
            r9 = 2
            short r8 = r8[r9]
            byte[][][] r9 = r12.ou
            c r11 = r12.nk
            int r11 = r11.dO
            r9 = r9[r11]
            c r11 = r12.nk
            int r11 = r11.dY
            r9 = r9[r11]
            r11 = 3
            byte r9 = r9[r11]
            int r8 = r8 + r9
            int r8 = r8 + r14
            ak r9 = r12.nw
            ak r9 = r12.nw
            r9 = 40
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            r10 = r0
            goto L_0x00ab
        L_0x01bc:
            int[] r0 = r12.or
            r1 = 0
            r2 = 0
            r0[r1] = r2
            goto L_0x0059
        L_0x01c4:
            int[] r0 = r12.or
            r1 = 1
            r2 = 0
            r0[r1] = r2
            goto L_0x0081
        L_0x01cc:
            int[] r0 = r12.or
            r1 = 2
            r2 = 0
            r0[r1] = r2
            goto L_0x00a9
        L_0x01d4:
            c r0 = r12.nk
            int r0 = r0.gz
            r1 = 4
            if (r0 <= r1) goto L_0x01f8
            ak r0 = r12.nw
            c r1 = r12.nk
            int r1 = r1.gz
            int r1 = r1 / 4
            int r1 = r1 % 2
            if (r1 != 0) goto L_0x02c7
            r1 = 16777215(0xffffff, float:2.3509886E-38)
        L_0x01ea:
            r0.setColor(r1)
            ak r0 = r12.nw
            r1 = 0
            r2 = 0
            int r3 = r12.gn
            int r4 = r12.np
            r0.e(r1, r2, r3, r4)
        L_0x01f8:
            c r0 = r12.nk
            boolean r0 = r0.iN
            r1 = 1
            if (r0 != r1) goto L_0x0210
            ak r0 = r12.nw
            r1 = 0
            r0.setColor(r1)
            ak r0 = r12.nw
            r1 = 0
            r2 = 0
            int r3 = r12.gn
            int r4 = r12.np
            r0.e(r1, r2, r3, r4)
        L_0x0210:
            if (r15 == 0) goto L_0x0378
            r0 = 0
            r7 = r0
        L_0x0214:
            int r0 = r15.length
            if (r7 >= r0) goto L_0x0378
            c r0 = r12.nk
            int r0 = r0.gz
            r1 = 4
            if (r0 <= r1) goto L_0x0225
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            if (r0 < 0) goto L_0x02c2
        L_0x0225:
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            if (r0 < 0) goto L_0x02cb
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            r1 = 12
            if (r0 > r1) goto L_0x02cb
            r0 = r15[r7]
            r1 = 0
            r1 = r0[r1]
            r0 = r15[r7]
            r2 = 7
            r2 = r0[r2]
            r0 = r15[r7]
            r3 = 4
            r0 = r0[r3]
            int r3 = r0 + r13
            r0 = r15[r7]
            r4 = 5
            r0 = r0[r4]
            int r4 = r0 + r14
            r0 = r15[r7]
            r5 = 1
            r5 = r0[r5]
            r0 = r15[r7]
            r6 = 2
            r6 = r0[r6]
            r0 = r12
            r0.b(r1, r2, r3, r4, r5, r6)
            r0 = r15[r7]
            r1 = 1
            r0 = r0[r1]
            r1 = 7
            if (r0 == r1) goto L_0x0273
            r0 = r15[r7]
            r1 = 1
            r0 = r0[r1]
            r1 = 6
            if (r0 == r1) goto L_0x0273
            r0 = r15[r7]
            r1 = 1
            r0 = r0[r1]
            r1 = 5
            if (r0 != r1) goto L_0x02c2
        L_0x0273:
            r0 = 31
            c r1 = r12.nk
            d r1 = r1.dp
            boolean r1 = r1.li
            if (r1 != 0) goto L_0x089f
            r0 = 30
            r5 = r0
        L_0x0280:
            r0 = 31
            if (r5 != r0) goto L_0x028c
            r0 = r15[r7]
            r1 = 2
            r0 = r0[r1]
            r1 = 6
            if (r0 < r1) goto L_0x0298
        L_0x028c:
            r0 = 30
            if (r5 != r0) goto L_0x02c2
            r0 = r15[r7]
            r1 = 2
            r0 = r0[r1]
            r1 = 4
            if (r0 >= r1) goto L_0x02c2
        L_0x0298:
            c r0 = r12.nk
            boolean r0 = r0.iN
            r1 = 1
            if (r0 == r1) goto L_0x02c2
            r0 = r15[r7]
            r1 = 0
            r1 = r0[r1]
            r0 = r15[r7]
            r2 = 7
            r2 = r0[r2]
            r0 = r15[r7]
            r3 = 9
            r0 = r0[r3]
            int r3 = r0 + r13
            r0 = r15[r7]
            r4 = 10
            r0 = r0[r4]
            int r4 = r0 + r14
            r0 = r15[r7]
            r6 = 2
            r6 = r0[r6]
            r0 = r12
            r0.b(r1, r2, r3, r4, r5, r6)
        L_0x02c2:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0214
        L_0x02c7:
            r1 = 16711680(0xff0000, float:2.3418052E-38)
            goto L_0x01ea
        L_0x02cb:
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            r1 = -10
            if (r0 == r1) goto L_0x02c2
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            r1 = -11
            if (r0 != r1) goto L_0x034a
            r0 = r15[r7]
            r1 = 2
            r0 = r0[r1]
            r1 = 4
            if (r0 != r1) goto L_0x0314
            r0 = r15[r7]
            r1 = 3
            r0 = r0[r1]
            r1 = 17
            if (r0 >= r1) goto L_0x0310
            r0 = 10
            r5 = r0
        L_0x02f1:
            r1 = 6
            r0 = r15[r7]
            r2 = 7
            r2 = r0[r2]
            r0 = r15[r7]
            r3 = 4
            r0 = r0[r3]
            int r3 = r0 + r13
            r0 = r15[r7]
            r4 = 6
            r0 = r0[r4]
            int r4 = r0 + r14
            r0 = r15[r7]
            r6 = 3
            r0 = r0[r6]
            int r6 = -r0
            r0 = r12
            r0.b(r1, r2, r3, r4, r5, r6)
            goto L_0x02c2
        L_0x0310:
            r0 = 11
            r5 = r0
            goto L_0x02f1
        L_0x0314:
            r0 = r15[r7]
            r1 = 2
            r1 = r0[r1]
            r0 = r15[r7]
            r2 = 3
            r2 = r0[r2]
            r0 = r15[r7]
            r3 = 4
            r0 = r0[r3]
            int r3 = r0 + r13
            r0 = r15[r7]
            r4 = 6
            r0 = r0[r4]
            int r4 = r0 + r14
            short[][] r0 = r12.od
            r5 = r15[r7]
            r6 = 7
            r5 = r5[r6]
            r0 = r0[r5]
            r5 = 1
            short r5 = r0[r5]
            short[][] r0 = r12.od
            r6 = r15[r7]
            r8 = 7
            r6 = r6[r8]
            r0 = r0[r6]
            r6 = 0
            short r6 = r0[r6]
            r0 = r12
            r0.c(r1, r2, r3, r4, r5, r6)
            goto L_0x02c2
        L_0x034a:
            r0 = r15[r7]
            r1 = 0
            r0 = r0[r1]
            r1 = -12
            if (r0 != r1) goto L_0x02c2
            r0 = r15[r7]
            r1 = 3
            r1 = r0[r1]
            r0 = r15[r7]
            r2 = 8
            r2 = r0[r2]
            r0 = r15[r7]
            r3 = 4
            r0 = r0[r3]
            int r3 = r0 + r13
            r0 = r15[r7]
            r4 = 5
            r0 = r0[r4]
            int r4 = r0 + r14
            ak r0 = r12.nw
            ak r0 = r12.nw
            r5 = 33
            r0 = r12
            r0.c(r1, r2, r3, r4, r5)
            goto L_0x02c2
        L_0x0378:
            c r0 = r12.nk
            boolean r0 = r0.iN
            r1 = 1
            if (r0 != r1) goto L_0x0380
        L_0x037f:
            return
        L_0x0380:
            c r0 = r12.nk
            int r0 = r0.ci
            c r1 = r12.nk
            r1.getClass()
            r1 = 9
            if (r0 == r1) goto L_0x0602
            r0 = 0
            r10 = r0
        L_0x038f:
            c r0 = r12.nk
            short[][][] r0 = r0.kj
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            int r0 = r0.length
            if (r10 >= r0) goto L_0x044c
            int[] r0 = r12.oO
            c r1 = r12.nk
            short[][][] r1 = r1.kj
            c r2 = r12.nk
            int r2 = r2.dY
            r1 = r1[r2]
            r1 = r1[r10]
            r2 = 4
            short r1 = r1[r2]
            r6 = r0[r1]
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            c r2 = r12.nk
            byte[] r2 = r2.eA
            c r3 = r12.nk
            int r3 = r3.dD
            byte r2 = r2[r3]
            int r2 = r2 + 7
            r1 = r1[r2]
            c r2 = r12.nk
            short[][][] r2 = r2.kj
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r2 = r2[r10]
            r3 = 3
            short r2 = r2[r3]
            r1 = r1[r2]
            r2 = 0
            r3 = 0
            al[][] r4 = r12.nH
            c r5 = r12.nk
            byte[] r5 = r5.eA
            c r7 = r12.nk
            int r7 = r7.dD
            byte r5 = r5[r7]
            int r5 = r5 + 7
            r4 = r4[r5]
            c r5 = r12.nk
            short[][][] r5 = r5.kj
            c r7 = r12.nk
            int r7 = r7.dY
            r5 = r5[r7]
            r5 = r5[r10]
            r7 = 3
            short r5 = r5[r7]
            r4 = r4[r5]
            int r4 = r4.getWidth()
            al[][] r5 = r12.nH
            c r7 = r12.nk
            byte[] r7 = r7.eA
            c r8 = r12.nk
            int r8 = r8.dD
            byte r7 = r7[r8]
            int r7 = r7 + 7
            r5 = r5[r7]
            c r7 = r12.nk
            short[][][] r7 = r7.kj
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 3
            short r7 = r7[r8]
            r5 = r5[r7]
            int r5 = r5.getHeight()
            c r7 = r12.nk
            short[][][] r7 = r7.kj
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 1
            short r7 = r7[r8]
            int r7 = r7 + r13
            c r8 = r12.nk
            short[][][] r8 = r8.kj
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r8 = r8[r10]
            r9 = 2
            short r8 = r8[r9]
            int r8 = r8 + r14
            ak r9 = r12.nw
            ak r9 = r12.nw
            r9 = 20
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            r10 = r0
            goto L_0x038f
        L_0x044c:
            byte[][][] r0 = r12.ov
            c r1 = r12.nk
            int r1 = r1.dO
            r0 = r0[r1]
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            r1 = 0
            byte r0 = r0[r1]
            if (r0 == 0) goto L_0x06c1
            int[] r0 = r12.os
            r1 = 0
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ov
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x05ea
            int[] r0 = r12.os
            r1 = 0
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x0487:
            int[] r0 = r12.os
            r1 = 1
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ov
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x05f2
            int[] r0 = r12.os
            r1 = 1
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x04af:
            int[] r0 = r12.os
            r1 = 2
            r0 = r0[r1]
            byte[][] r1 = r12.oq
            byte[][][] r2 = r12.ov
            c r3 = r12.nk
            int r3 = r3.dO
            r2 = r2[r3]
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r3 = 4
            byte r2 = r2[r3]
            r1 = r1[r2]
            int r1 = r1.length
            r2 = 1
            int r1 = r1 - r2
            if (r0 >= r1) goto L_0x05fa
            int[] r0 = r12.os
            r1 = 2
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
        L_0x04d7:
            r0 = 0
            r10 = r0
        L_0x04d9:
            byte[][][] r0 = r12.ov
            c r1 = r12.nk
            int r1 = r1.dO
            r0 = r0[r1]
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            r1 = 0
            byte r0 = r0[r1]
            if (r10 >= r0) goto L_0x06c1
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 20
            r1 = r1[r2]
            byte[][] r2 = r12.oq
            byte[][][] r3 = r12.ov
            c r4 = r12.nk
            int r4 = r4.dO
            r3 = r3[r4]
            c r4 = r12.nk
            int r4 = r4.dY
            r3 = r3[r4]
            r4 = 4
            byte r3 = r3[r4]
            r2 = r2[r3]
            int[] r3 = r12.os
            r3 = r3[r10]
            byte r2 = r2[r3]
            byte[][][] r3 = r12.ov
            c r4 = r12.nk
            int r4 = r4.dO
            r3 = r3[r4]
            c r4 = r12.nk
            int r4 = r4.dY
            r3 = r3[r4]
            r4 = 1
            byte r3 = r3[r4]
            int r2 = r2 + r3
            r1 = r1[r2]
            r2 = 0
            r3 = 0
            al[][] r4 = r12.nH
            r5 = 20
            r4 = r4[r5]
            byte[][] r5 = r12.oq
            byte[][][] r6 = r12.ov
            c r7 = r12.nk
            int r7 = r7.dO
            r6 = r6[r7]
            c r7 = r12.nk
            int r7 = r7.dY
            r6 = r6[r7]
            r7 = 4
            byte r6 = r6[r7]
            r5 = r5[r6]
            int[] r6 = r12.os
            r6 = r6[r10]
            byte r5 = r5[r6]
            byte[][][] r6 = r12.ov
            c r7 = r12.nk
            int r7 = r7.dO
            r6 = r6[r7]
            c r7 = r12.nk
            int r7 = r7.dY
            r6 = r6[r7]
            r7 = 1
            byte r6 = r6[r7]
            int r5 = r5 + r6
            r4 = r4[r5]
            int r4 = r4.getWidth()
            al[][] r5 = r12.nH
            r6 = 20
            r5 = r5[r6]
            byte[][] r6 = r12.oq
            byte[][][] r7 = r12.ov
            c r8 = r12.nk
            int r8 = r8.dO
            r7 = r7[r8]
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r8 = 4
            byte r7 = r7[r8]
            r6 = r6[r7]
            int[] r7 = r12.os
            r7 = r7[r10]
            byte r6 = r6[r7]
            byte[][][] r7 = r12.ov
            c r8 = r12.nk
            int r8 = r8.dO
            r7 = r7[r8]
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r8 = 1
            byte r7 = r7[r8]
            int r6 = r6 + r7
            r5 = r5[r6]
            int r5 = r5.getHeight()
            r6 = 0
            c r7 = r12.nk
            short[][][] r7 = r7.kl
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 1
            short r7 = r7[r8]
            byte[][][] r8 = r12.ov
            c r9 = r12.nk
            int r9 = r9.dO
            r8 = r8[r9]
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r9 = 2
            byte r8 = r8[r9]
            int r7 = r7 + r8
            int r7 = r7 + r13
            c r8 = r12.nk
            short[][][] r8 = r8.kl
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r8 = r8[r10]
            r9 = 2
            short r8 = r8[r9]
            byte[][][] r9 = r12.ov
            c r11 = r12.nk
            int r11 = r11.dO
            r9 = r9[r11]
            c r11 = r12.nk
            int r11 = r11.dY
            r9 = r9[r11]
            r11 = 3
            byte r9 = r9[r11]
            int r8 = r8 + r9
            int r8 = r8 + r14
            ak r9 = r12.nw
            ak r9 = r12.nw
            r9 = 40
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            r10 = r0
            goto L_0x04d9
        L_0x05ea:
            int[] r0 = r12.os
            r1 = 0
            r2 = 0
            r0[r1] = r2
            goto L_0x0487
        L_0x05f2:
            int[] r0 = r12.os
            r1 = 1
            r2 = 0
            r0[r1] = r2
            goto L_0x04af
        L_0x05fa:
            int[] r0 = r12.os
            r1 = 2
            r2 = 0
            r0[r1] = r2
            goto L_0x04d7
        L_0x0602:
            r0 = 0
            r10 = r0
        L_0x0604:
            c r0 = r12.nk
            short[][][] r0 = r0.kj
            c r1 = r12.nk
            int r1 = r1.dY
            r0 = r0[r1]
            int r0 = r0.length
            if (r10 >= r0) goto L_0x06c1
            int[] r0 = r12.oO
            c r1 = r12.nk
            short[][][] r1 = r1.kj
            c r2 = r12.nk
            int r2 = r2.dY
            r1 = r1[r2]
            r1 = r1[r10]
            r2 = 4
            short r1 = r1[r2]
            r6 = r0[r1]
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            c r2 = r12.nk
            byte[] r2 = r2.eB
            c r3 = r12.nk
            byte r3 = r3.gI
            byte r2 = r2[r3]
            int r2 = r2 + 7
            r1 = r1[r2]
            c r2 = r12.nk
            short[][][] r2 = r2.kj
            c r3 = r12.nk
            int r3 = r3.dY
            r2 = r2[r3]
            r2 = r2[r10]
            r3 = 3
            short r2 = r2[r3]
            r1 = r1[r2]
            r2 = 0
            r3 = 0
            al[][] r4 = r12.nH
            c r5 = r12.nk
            byte[] r5 = r5.eB
            c r7 = r12.nk
            byte r7 = r7.gI
            byte r5 = r5[r7]
            int r5 = r5 + 7
            r4 = r4[r5]
            c r5 = r12.nk
            short[][][] r5 = r5.kj
            c r7 = r12.nk
            int r7 = r7.dY
            r5 = r5[r7]
            r5 = r5[r10]
            r7 = 3
            short r5 = r5[r7]
            r4 = r4[r5]
            int r4 = r4.getWidth()
            al[][] r5 = r12.nH
            c r7 = r12.nk
            byte[] r7 = r7.eB
            c r8 = r12.nk
            byte r8 = r8.gI
            byte r7 = r7[r8]
            int r7 = r7 + 7
            r5 = r5[r7]
            c r7 = r12.nk
            short[][][] r7 = r7.kj
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 3
            short r7 = r7[r8]
            r5 = r5[r7]
            int r5 = r5.getHeight()
            c r7 = r12.nk
            short[][][] r7 = r7.kj
            c r8 = r12.nk
            int r8 = r8.dY
            r7 = r7[r8]
            r7 = r7[r10]
            r8 = 1
            short r7 = r7[r8]
            int r7 = r7 + r13
            c r8 = r12.nk
            short[][][] r8 = r8.kj
            c r9 = r12.nk
            int r9 = r9.dY
            r8 = r8[r9]
            r8 = r8[r10]
            r9 = 2
            short r8 = r8[r9]
            int r8 = r8 + r14
            ak r9 = r12.nw
            ak r9 = r12.nw
            r9 = 20
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9)
            int r0 = r10 + 1
            r10 = r0
            goto L_0x0604
        L_0x06c1:
            c r0 = r12.nk
            boolean r0 = r0.fC
            r1 = 1
            if (r0 != r1) goto L_0x072b
            c r0 = r12.nk
            int r0 = r0.fG
            if (r0 != 0) goto L_0x079e
            c r0 = r12.nk
            int r0 = r0.fF
            r1 = 6
            if (r0 >= r1) goto L_0x0790
            c r0 = r12.nk
            boolean r0 = r0.gS
            if (r0 != 0) goto L_0x0790
            c r0 = r12.nk
            int r1 = r0.fF
            int r1 = r1 + 1
            r0.fF = r1
        L_0x06e3:
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            c r2 = r12.nk
            byte[] r2 = r2.fE
            c r3 = r12.nk
            int r3 = r3.fD
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte[] r2 = r12.pc
            c r3 = r12.nk
            int r3 = r3.fF
            byte r2 = r2[r3]
            r3 = 10
            int r2 = r2 - r3
            r3 = 80
            ak r4 = r12.nw
            ak r4 = r12.nw
            r4 = 20
            r0.a(r1, r2, r3, r4)
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            r2 = 3
            r1 = r1[r2]
            byte[] r2 = r12.pc
            c r3 = r12.nk
            int r3 = r3.fF
            byte r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            ak r4 = r12.nw
            ak r4 = r12.nw
            r4 = 20
            r0.a(r1, r2, r3, r4)
        L_0x072b:
            c r0 = r12.nk
            int r0 = r0.dD
            r1 = 1
            if (r0 != r1) goto L_0x037f
            c r0 = r12.nk
            int r0 = r0.dY
            r1 = 3
            if (r0 != r1) goto L_0x037f
            c r0 = r12.nk
            d[] r0 = r0.dq
            if (r0 == 0) goto L_0x037f
            ak r0 = r12.nw
            r1 = 15529(0x3ca9, float:2.1761E-41)
            r0.setColor(r1)
            c r0 = r12.nk
            d[] r0 = r0.dq
            r1 = 0
            r0 = r0[r1]
            int r0 = r0.kF
            if (r0 <= 0) goto L_0x037f
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "吕布体力:"
            java.lang.StringBuffer r0 = r0.append(r1)
            c r1 = r12.nk
            d[] r1 = r1.dq
            r2 = 0
            r1 = r1[r2]
            int r1 = r1.kF
            int r1 = r1 * 100
            c r2 = r12.nk
            d[] r2 = r2.dq
            r3 = 0
            r2 = r2[r3]
            int r2 = r2.kG
            int r1 = r1 / r2
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = "%"
            java.lang.StringBuffer r0 = r0.append(r1)
            java.lang.String r1 = r0.toString()
            int r2 = r12.nq
            r3 = 50
            r4 = 0
            ak r0 = r12.nw
            ak r0 = r12.nw
            r5 = 33
            r0 = r12
            r0.a(r1, r2, r3, r4, r5)
            goto L_0x037f
        L_0x0790:
            c r0 = r12.nk
            int r0 = r0.fF
            r1 = 6
            if (r0 != r1) goto L_0x06e3
            c r0 = r12.nk
            r1 = 2
            r0.fG = r1
            goto L_0x06e3
        L_0x079e:
            c r0 = r12.nk
            int r0 = r0.fG
            r1 = 1
            if (r0 != r1) goto L_0x0834
            c r0 = r12.nk
            int r0 = r0.fF
            if (r0 <= 0) goto L_0x07fe
            c r0 = r12.nk
            boolean r0 = r0.gS
            if (r0 != 0) goto L_0x07fe
            c r0 = r12.nk
            int r1 = r0.fF
            r2 = 1
            int r1 = r1 - r2
            r0.fF = r1
        L_0x07b9:
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            c r2 = r12.nk
            byte[] r2 = r2.fE
            c r3 = r12.nk
            int r3 = r3.fD
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte[] r2 = r12.pc
            c r3 = r12.nk
            int r3 = r3.fF
            byte r2 = r2[r3]
            r3 = 10
            int r2 = r2 - r3
            r3 = 80
            ak r4 = r12.nw
            ak r4 = r12.nw
            r4 = 20
            r0.a(r1, r2, r3, r4)
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            r2 = 3
            r1 = r1[r2]
            byte[] r2 = r12.pc
            c r3 = r12.nk
            int r3 = r3.fF
            byte r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            r4 = 0
            r0.a(r1, r2, r3, r4)
            goto L_0x072b
        L_0x07fe:
            c r0 = r12.nk
            int r0 = r0.fF
            if (r0 != 0) goto L_0x07b9
            c r0 = r12.nk
            int r0 = r0.fD
            int r0 = r0 + 1
            c r1 = r12.nk
            byte[] r1 = r1.fE
            int r1 = r1.length
            if (r0 > r1) goto L_0x07b9
            c r0 = r12.nk
            byte[] r0 = r0.fE
            c r1 = r12.nk
            int r1 = r1.fD
            int r1 = r1 + 1
            byte r0 = r0[r1]
            r1 = 3
            if (r0 == r1) goto L_0x07b9
            c r0 = r12.nk
            boolean r0 = r0.gS
            if (r0 != 0) goto L_0x07b9
            c r0 = r12.nk
            int r1 = r0.fD
            int r1 = r1 + 1
            r0.fD = r1
            c r0 = r12.nk
            r1 = 0
            r0.fG = r1
            goto L_0x07b9
        L_0x0834:
            c r0 = r12.nk
            int r0 = r0.fG
            r1 = 2
            if (r0 != r1) goto L_0x072b
            c r0 = r12.nk
            boolean r0 = r0.gS
            if (r0 != 0) goto L_0x0849
            c r0 = r12.nk
            int r1 = r0.fH
            int r1 = r1 + 1
            r0.fH = r1
        L_0x0849:
            c r0 = r12.nk
            int r0 = r0.fH
            r1 = 20
            if (r0 < r1) goto L_0x085b
            c r0 = r12.nk
            r1 = 0
            r0.fH = r1
            c r0 = r12.nk
            r1 = 1
            r0.fG = r1
        L_0x085b:
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            c r2 = r12.nk
            byte[] r2 = r2.fE
            c r3 = r12.nk
            int r3 = r3.fD
            byte r2 = r2[r3]
            r1 = r1[r2]
            byte[] r2 = r12.pc
            r3 = 4
            byte r2 = r2[r3]
            r3 = 10
            int r2 = r2 - r3
            r3 = 80
            ak r4 = r12.nw
            ak r4 = r12.nw
            r4 = 20
            r0.a(r1, r2, r3, r4)
            ak r0 = r12.nw
            al[][] r1 = r12.nH
            r2 = 22
            r1 = r1[r2]
            r2 = 3
            r1 = r1[r2]
            byte[] r2 = r12.pc
            r3 = 4
            byte r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            ak r4 = r12.nw
            ak r4 = r12.nw
            r4 = 20
            r0.a(r1, r2, r3, r4)
            goto L_0x072b
        L_0x089f:
            r5 = r0
            goto L_0x0280
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.o.a(int, int, int[][]):void");
    }

    private void a(ak akVar, String str, int i, int i2, int i3, int i4) {
        int height = this.ny.getHeight();
        int i5 = i3 / 18;
        int i6 = (i3 % 18) / 2;
        akVar.setColor(i4);
        int i7 = 0;
        for (int i8 = 0; i8 < str.length(); i8++) {
            String substring = str.substring(i8, i8 + 1);
            if (substring.equals("|") || substring == null) {
                i7 = (i8 + i7) % i5 != 0 ? i7 + ((i5 - ((i8 + i7) % i5)) - 1) : i7 - 1;
            } else {
                akVar.b(substring, i + i6 + (((i7 + i8) % i5) * 18), ((height + 1) * ((i7 + i8) / i5)) + i2, 20);
            }
        }
    }

    private void a(String str, int i, int i2, int i3, int i4) {
        if (i3 == 0) {
            this.nw.e(0, 128, 0);
        } else if (i3 == 1) {
            this.nw.setColor(9850892);
        } else if (i3 == 2) {
            int[][] iArr = {new int[]{0, 128, 0}, new int[]{128, 0, 0}, new int[]{0, 0, 128}};
            this.nw.e(iArr[this.nz[this.eZ] % 2][0], iArr[this.nz[this.eZ] % 2][1], iArr[this.nz[this.eZ] % 2][2]);
        }
        this.nw.b(str, i + 1, i2, i4);
        this.nw.b(str, i - 1, i2, i4);
        this.nw.b(str, i, i2 + 1, i4);
        this.nw.b(str, i, i2 - 1, i4);
        if (i3 == 0) {
            this.nw.setColor(16777215);
        } else {
            this.nw.setColor(16770168);
        }
        this.nw.b(str, i, i2, i4);
    }

    private void a(String str, String str2) {
        if (str != "") {
            int i = this.np;
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 73, 0, i, 36);
        }
        if (str2 != "") {
            int i2 = this.gn;
            int i3 = this.np;
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 74, i2, i3, 40);
        }
    }

    private void a(int[][] iArr, int i, int i2, boolean z) {
        for (int i3 = 0; i3 < iArr.length; i3++) {
            if (z) {
                this.nw.setColor(16777215);
            } else {
                this.nw.setColor(iArr[i3][0]);
            }
            if (i2 == 0) {
                if (i > 0) {
                    this.nw.e(iArr[i3][1], iArr[i3][2], (iArr[i3][3] * i) / 100, iArr[i3][4]);
                }
            } else if (i2 == 1) {
                this.nw.e(iArr[i3][1], (iArr[i3][2] + iArr[i3][4]) - ((iArr[i3][4] * i) / 100), iArr[i3][3], (iArr[i3][4] * i) / 100);
            } else {
                this.nw.e(iArr[i3][1] + ((iArr[i3][3] * (100 - i)) / 100), iArr[i3][2], iArr[i3][3] - ((iArr[i3][3] * (100 - i)) / 100), iArr[i3][4]);
            }
        }
    }

    private byte[] a(int[] iArr, String str, int i) {
        int i2 = 0;
        for (int i3 : iArr) {
            i2 += i3;
        }
        byte[] bArr = new byte[i2];
        try {
            getClass();
            InputStream p = MIDPHelper.p(str);
            for (int i4 = 0; i4 < i2; i4++) {
                bArr[i4] = (byte) (p.read() - 7);
            }
            p.close();
            return bArr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static al[] a(byte[] bArr, int[] iArr) {
        int i = 0;
        al[] alVarArr = new al[iArr.length];
        int i2 = 0;
        while (i < iArr.length) {
            try {
                alVarArr[i] = al.a(bArr, i2, iArr[i]);
                i2 += iArr[i];
                i++;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return alVarArr;
    }

    private String[] a(String str, int i, int i2, int i3) {
        int i4 = i / 18;
        int i5 = i4 * 11;
        Vector vector = new Vector();
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = -1;
        int i10 = 0;
        for (int i11 = 0; i11 < str.length(); i11++) {
            String substring = str.substring(i11, i11 + 1);
            if (!substring.equals("$")) {
                if (substring.equals("|")) {
                    i6 += i4 - i8;
                } else {
                    i7++;
                    i8 = (i8 % i4) + 1;
                    if (i11 < str.length() - 1 || i7 + i6 >= i5) {
                        vector.addElement(str.substring(((i11 - i7) + 1) - i10, i11 + 1));
                        i8 = 0;
                        i9++;
                        i7 = 0;
                        i10 = 0;
                        i6 = 0;
                    }
                }
            }
            i10++;
            if (i11 < str.length() - 1) {
            }
            vector.addElement(str.substring(((i11 - i7) + 1) - i10, i11 + 1));
            i8 = 0;
            i9++;
            i7 = 0;
            i10 = 0;
            i6 = 0;
        }
        this.nk.ip = i9;
        String[] strArr = new String[(i9 + 1)];
        vector.copyInto(strArr);
        vector.removeAllElements();
        return strArr;
    }

    private void aL() {
        this.nw.setColor(0);
        this.nw.e(0, 0, this.gn, this.np);
    }

    private void aN() {
        this.nH = new al[23][];
        this.nJ = new boolean[23];
    }

    private void b(int i, int i2, int i3, int i4, int i5, int i6) {
        short s = i6 >= 0 ? e.ls[i][i5][i6][0] : -i6;
        int i7 = 0;
        while (true) {
            int i8 = i7;
            if (i8 < this.nk.fO[i][s].length) {
                short s2 = this.nk.fO[i][s][i8][0];
                c(this.nk.fN[e.lF[i]][s2][0], this.nk.fN[e.lF[i]][s2][5], i3 - (this.nk.fO[i][s][i8][2] * this.od[i2][2]), i4 + this.nk.fO[i][s][i8][3], this.oQ[i2][this.nk.fO[i][s][i8][1]], this.od[i2][0] ^ this.oP[this.nk.fO[i][s][i8][1]]);
                i7 = i8 + 1;
            } else {
                return;
            }
        }
    }

    private void c(int i, int i2) {
        if (this.nw != null) {
            this.nw.setColor(5455918);
            this.nw.e(0, 0, this.gn, this.np);
            if (this.nH[13] != null) {
                this.nw.setColor(8474403);
                int i3 = this.nq;
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                c(13, 59, i3, 0, 17);
                ak akVar3 = this.nw;
                ak akVar4 = this.nw;
                c(13, 59, 0, 0, 20);
                int i4 = this.gn;
                ak akVar5 = this.nw;
                ak akVar6 = this.nw;
                c(13, 59, i4, 0, 24);
                int i5 = this.nq;
                int i6 = this.np;
                ak akVar7 = this.nw;
                ak akVar8 = this.nw;
                c(13, 59, i5, i6, 33);
                int i7 = this.np;
                ak akVar9 = this.nw;
                ak akVar10 = this.nw;
                c(13, 59, 0, i7, 36);
                int i8 = this.gn;
                int i9 = this.np;
                ak akVar11 = this.nw;
                ak akVar12 = this.nw;
                c(13, 59, i8, i9, 40);
                ak akVar13 = this.nw;
                ak akVar14 = this.nw;
                c(13, 58, 0, 140, 20);
                int i10 = this.gn;
                ak akVar15 = this.nw;
                ak akVar16 = this.nw;
                c(13, 58, i10, 140, 24, 2);
                ak akVar17 = this.nw;
                ak akVar18 = this.nw;
                c(13, 58, 0, 60, 20);
                ak akVar19 = this.nw;
                ak akVar20 = this.nw;
                c(13, 58, 0, 0, 20);
                int i11 = this.np;
                ak akVar21 = this.nw;
                ak akVar22 = this.nw;
                c(13, 58, 0, i11, 36);
                int i12 = this.gn;
                ak akVar23 = this.nw;
                ak akVar24 = this.nw;
                c(13, 58, i12, 60, 24, 2);
                int i13 = this.gn;
                ak akVar25 = this.nw;
                ak akVar26 = this.nw;
                c(13, 58, i13, 0, 24, 2);
                int i14 = this.gn;
                int i15 = this.np;
                ak akVar27 = this.nw;
                ak akVar28 = this.nw;
                c(13, 58, i14, i15, 40, 2);
                int i16 = 0;
                while (true) {
                    int i17 = i16;
                    if (i17 >= 4) {
                        break;
                    }
                    ak akVar29 = this.nw;
                    ak akVar30 = this.nw;
                    c(13, 34, this.gn - 7, (i17 * 77) + 7, 24);
                    ak akVar31 = this.nw;
                    ak akVar32 = this.nw;
                    c(13, 34, ((this.gn - 7) - 57) + 1, (i17 * 77) + 7, 24);
                    ak akVar33 = this.nw;
                    ak akVar34 = this.nw;
                    c(13, 34, 7, (i17 * 77) + 7, 20, 2);
                    ak akVar35 = this.nw;
                    ak akVar36 = this.nw;
                    c(13, 34, 63, (i17 * 77) + 7, 20, 2);
                    i16 = i17 + 1;
                }
                ak akVar37 = this.nw;
                ak akVar38 = this.nw;
                c(13, 60, 0, 0, 20);
                int i18 = this.gn;
                ak akVar39 = this.nw;
                ak akVar40 = this.nw;
                c(13, 60, i18, 0, 24, 2);
                if (i < 5) {
                    ak akVar41 = this.nw;
                    ak akVar42 = this.nw;
                    c(13, 70, 5, 0, 20, 2);
                    int i19 = this.gn - 5;
                    ak akVar43 = this.nw;
                    ak akVar44 = this.nw;
                    c(13, 70, i19, 0, 24);
                    int i20 = this.np - 7;
                    ak akVar45 = this.nw;
                    ak akVar46 = this.nw;
                    c(13, 71, 5, i20, 36);
                    int i21 = this.gn - 5;
                    int i22 = this.np - 7;
                    ak akVar47 = this.nw;
                    ak akVar48 = this.nw;
                    c(13, 71, i21, i22, 40, 2);
                }
                if (i == 1) {
                    this.bL = (byte) (this.bL + 1);
                    if (this.bL >= 2) {
                        this.bL = 0;
                        if (this.fm < 2) {
                            this.fm = (byte) (this.fm + 1);
                        } else {
                            this.fm = 0;
                        }
                    }
                    int i23 = this.fm + 35;
                    int i24 = this.nq - 50;
                    ak akVar49 = this.nw;
                    ak akVar50 = this.nw;
                    c(13, i23, i24, i2, 40);
                    int i25 = this.fm + 35;
                    int i26 = this.nq + 50;
                    ak akVar51 = this.nw;
                    ak akVar52 = this.nw;
                    c(13, i25, i26, i2, 36, 2);
                }
            }
        }
    }

    private void c(int i, int i2, int i3) {
        this.nw.setColor(0);
        this.nw.e(i2 - 22, i3 - 32, 44, 32);
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 21, i2, i3 - 1, 33);
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, i, i2, i3 - 5, 33);
    }

    private void c(int i, int i2, int i3, int i4) {
        int i5 = i + 15 + (i3 * 34);
        int i6 = i2 + 9 + (i3 * 20);
        for (int i7 = 0; i7 < i3; i7++) {
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 61, (i7 * 34) + i + 15, i2, 20);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 61, (i7 * 34) + i + 15, i6 + 9, 36, 1);
            ak akVar5 = this.nw;
            ak akVar6 = this.nw;
            c(13, 62, i, i2 + 9 + (i7 * 20), 20);
            ak akVar7 = this.nw;
            ak akVar8 = this.nw;
            c(13, 62, (i5 + 15) - 4, (i7 * 20) + i2 + 9, 20, 2);
        }
        ak akVar9 = this.nw;
        ak akVar10 = this.nw;
        c(13, 63, i, i2, 20);
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        c(13, 63, i5, i2, 20, 2);
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        c(13, 63, i, i6, 20, 1);
        ak akVar15 = this.nw;
        ak akVar16 = this.nw;
        c(13, 63, i5 + 15, i6, 24, 3);
        if (i4 == 1) {
            ak akVar17 = this.nw;
            ak akVar18 = this.nw;
            c(13, 70, i, i2 + 6, 24);
            ak akVar19 = this.nw;
            ak akVar20 = this.nw;
            c(13, 70, i5 + 15, i2 + 6, 20, 2);
        }
    }

    private void c(int i, int i2, int i3, int i4, int i5) {
        int[] i6 = i(i, i2);
        this.nw.a(this.nH[i][0], i6[0], i6[1], i6[2], i6[3], 0, i3, i4, i5);
    }

    private void c(int i, int i2, int i3, int i4, int i5, int i6) {
        int[] i7 = i(i, i2);
        this.nw.a(this.nH[i][0], i7[0], i7[1], i7[2], i7[3], i6, i3, i4, i5);
    }

    private void d(int i, int i2, int i3, int i4, int i5, int i6) {
        if (i == 1) {
            this.nk.iw = false;
            if (this.ke < 140) {
                this.ke += 20;
            } else {
                this.ke = 140;
                this.nk.iv = true;
            }
        } else if (i == 2) {
            this.nk.iv = false;
            if (this.ke >= -41) {
                this.ke -= 20;
            } else {
                this.ke = -80;
                this.nk.iw = true;
            }
        }
        for (int i7 = 0; i7 < i6; i7++) {
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 >= 5) {
                    break;
                }
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                c(13, 29, (this.gn + (i9 * 37)) - this.ke, (i7 * 60) + i4, 24);
                i8 = i9 + 1;
            }
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 27, (this.gn - 37) - this.ke, (i7 * 60) + i4, 24);
        }
        if (i3 == 1) {
            int i10 = 0;
            while (true) {
                int i11 = i10;
                if (i11 < 5) {
                    int i12 = this.ke + (0 - (i11 * 37));
                    ak akVar5 = this.nw;
                    ak akVar6 = this.nw;
                    c(13, 29, i12, i5, 20);
                    i10 = i11 + 1;
                } else {
                    int i13 = this.ke + 37;
                    ak akVar7 = this.nw;
                    ak akVar8 = this.nw;
                    c(13, 28, i13, i5, 20);
                    return;
                }
            }
        }
    }

    private void e(int i, int i2, int i3, int i4, int i5, int i6) {
        this.nw.setColor(i5);
        if (i6 == 3) {
            this.nw.e(i, i2, 3, i4);
            this.nw.e(i, i2, i3, 3);
            this.nw.e(i, (i2 + i4) - 3, i3, 3);
            this.nw.e((i + i3) - 3, i2, 3, i4);
        } else if (i6 == 1) {
            this.nw.e(i + 1, i2 + 1, 1, i4 - 2);
            this.nw.e(i + 1, i2 + 1, i3 - 2, 1);
            this.nw.e(i + 1, (i2 + i4) - 2, i3 - 2, 1);
            this.nw.e((i + i3) - 2, i2 + 1, 1, i4 - 2);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v2, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: int[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v5, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v10, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v15, resolved type: java.lang.Object[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v20, resolved type: java.lang.Object[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int[] i(int r8, int r9) {
        /*
            r7 = this;
            r2 = 4
            r6 = 3
            r5 = 2
            r4 = 1
            r3 = 0
            r0 = 0
            r1 = 6
            if (r8 > r1) goto L_0x003b
            int[] r0 = new int[r2]
            c r1 = r7.nk
            short[][][] r1 = r1.fN
            r1 = r1[r8]
            r1 = r1[r9]
            short r1 = r1[r4]
            r0[r3] = r1
            c r1 = r7.nk
            short[][][] r1 = r1.fN
            r1 = r1[r8]
            r1 = r1[r9]
            short r1 = r1[r5]
            r0[r4] = r1
            c r1 = r7.nk
            short[][][] r1 = r1.fN
            r1 = r1[r8]
            r1 = r1[r9]
            short r1 = r1[r6]
            r0[r5] = r1
            c r1 = r7.nk
            short[][][] r1 = r1.fN
            r1 = r1[r8]
            r1 = r1[r9]
            short r1 = r1[r2]
            r0[r6] = r1
        L_0x003b:
            r1 = 10
            if (r8 < r1) goto L_0x006e
            switch(r8) {
                case 10: goto L_0x006f;
                case 11: goto L_0x007d;
                case 12: goto L_0x0071;
                case 13: goto L_0x0073;
                case 14: goto L_0x0075;
                case 15: goto L_0x0077;
                case 16: goto L_0x0079;
                case 17: goto L_0x0042;
                case 18: goto L_0x007b;
                case 19: goto L_0x007f;
                default: goto L_0x0042;
            }
        L_0x0042:
            r0 = r3
        L_0x0043:
            int[] r1 = new int[r2]
            short[][][] r2 = defpackage.e.lC
            r2 = r2[r0]
            r2 = r2[r9]
            short r2 = r2[r3]
            r1[r3] = r2
            short[][][] r2 = defpackage.e.lC
            r2 = r2[r0]
            r2 = r2[r9]
            short r2 = r2[r4]
            r1[r4] = r2
            short[][][] r2 = defpackage.e.lC
            r2 = r2[r0]
            r2 = r2[r9]
            short r2 = r2[r5]
            r1[r5] = r2
            short[][][] r2 = defpackage.e.lC
            r0 = r2[r0]
            r0 = r0[r9]
            short r0 = r0[r6]
            r1[r6] = r0
            r0 = r1
        L_0x006e:
            return r0
        L_0x006f:
            r0 = r3
            goto L_0x0043
        L_0x0071:
            r0 = r4
            goto L_0x0043
        L_0x0073:
            r0 = r5
            goto L_0x0043
        L_0x0075:
            r0 = r6
            goto L_0x0043
        L_0x0077:
            r0 = r2
            goto L_0x0043
        L_0x0079:
            r0 = 5
            goto L_0x0043
        L_0x007b:
            r0 = 6
            goto L_0x0043
        L_0x007d:
            r0 = 7
            goto L_0x0043
        L_0x007f:
            r0 = 8
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.o.i(int, int):int[]");
    }

    private void j(int i, int i2) {
        if (i2 < 0) {
            this.nw.setColor(0);
            this.nw.e(0, 0, this.gn, this.np);
            return;
        }
        byte b = this.op[i][i2];
        int length = this.on[b].length;
        for (int i3 = 0; i3 < length; i3++) {
            DirectGraphics directGraphics = this.nx;
            al alVar = this.nH[18][0];
            int i4 = this.on[b][i3][0];
            int i5 = this.on[b][i3][1];
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            directGraphics.a(alVar, i4, i5, 20, this.oN[this.on[b][i3][2]]);
        }
        this.nw.setColor(0);
        int length2 = this.oo[b].length;
        for (int i6 = 0; i6 < length2; i6++) {
            this.nw.e(this.oo[b][i6][0], this.oo[b][i6][1], this.oo[b][i6][2], this.oo[b][i6][3]);
        }
    }

    private void m(int i) {
        this.nw.setColor(16777215);
        switch (i) {
            case 0:
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                this.nw.b("下一页", 5, this.np - 5, 36);
                break;
            case 1:
                ak akVar3 = this.nw;
                ak akVar4 = this.nw;
                this.nw.b("确定", 5, this.np - 5, 36);
                break;
        }
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        this.nw.b("返回", this.gn - 5, this.np - 5, 40);
    }

    private void o() {
        for (int i = 0; i < 3; i++) {
            this.or[i] = Math.abs(this.ot.nextInt()) % 20;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            this.os[i2] = Math.abs(this.ot.nextInt()) % 20;
        }
    }

    private byte[] o(String str) {
        try {
            getClass();
            DataInputStream dataInputStream = new DataInputStream(MIDPHelper.p(new StringBuffer().append("/data/").append(str).toString()));
            byte[] bArr = new byte[4];
            for (int i = 0; i < 4; i++) {
                bArr[i] = dataInputStream.readByte();
            }
            byte[] bArr2 = new byte[(((bArr[3] & ToneControl.SILENCE) | ((((bArr[0] & ToneControl.SILENCE) << d.ju) | ((bArr[1] & ToneControl.SILENCE) << d.jq)) | ((bArr[2] & ToneControl.SILENCE) << 8))) - 4)];
            dataInputStream.read(bArr2);
            dataInputStream.close();
            return bArr2;
        } catch (Exception e) {
            System.out.println("getColor ERROR!");
            return null;
        }
    }

    public final void M() {
        try {
            if (this.ns == null) {
                this.ns = al.r("/exit.png");
            }
        } catch (Exception e) {
            System.out.println("there is a error when exit");
        }
        this.nw.setColor(0);
        this.nw.e(0, 0, this.gn, this.np);
        ak akVar = this.nw;
        al alVar = this.ns;
        int i = this.nq;
        int i2 = this.nr;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.a(alVar, i, i2, 3);
        this.nw.e(255, 255, 255);
        ak akVar4 = this.nw;
        ak akVar5 = this.nw;
        this.nw.b("更多精彩游戏", this.nq, this.nr - 20, 33);
        ak akVar6 = this.nw;
        int i3 = this.nq;
        int i4 = this.nr;
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        akVar6.b("尽在游戏频道", i3, i4, 33);
        ak akVar9 = this.nw;
        ak akVar10 = this.nw;
        this.nw.b("wap.xjoys.com", this.nq, this.nr + 20, 33);
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        this.nw.b("退出", this.gn - 5, this.np - 2, 40);
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        this.nw.b("确认", 5, this.np - 2, 36);
    }

    public final void P() {
        boolean z;
        if (this.nk.eZ > 0 && this.nw != null) {
            if (this.nk.gL) {
                ak akVar = this.nw;
                int length = this.op[1].length - this.nk.fa;
                if (length == this.op[1].length) {
                    z = true;
                } else {
                    j(1, length);
                    if (this.nk.fa >= 0) {
                        this.nk.fa--;
                    }
                    z = this.nk.fa < 0;
                }
                if (z) {
                    this.nk.gL = false;
                    this.nk.eZ = 0;
                    this.nk.fa = 0;
                    return;
                }
                return;
            }
            ak akVar2 = this.nw;
            boolean z2 = this.nk.gL;
            j(0, this.nk.fa);
            if (this.nk.fa < this.op[0].length) {
                this.nk.fa++;
            }
            if (this.nk.fa >= this.op[0].length) {
                this.nk.fa = this.op[1].length;
                this.nk.gL = true;
                this.nk.eZ = 2;
            }
        }
    }

    public final void R() {
        if (this.nw != null) {
            aL();
            c(0, 0);
            this.nw.setColor(this.eW);
            this.nw.f(0, 100, this.gn, this.nk.eW);
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            this.nw.b("恭喜通关", this.nq, this.nr - 40, 17);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            this.nw.b("敬请期待新品推出", this.nq, this.nr - 20, 17);
            ak akVar5 = this.nw;
            int i = this.nq;
            int i2 = this.nr;
            ak akVar6 = this.nw;
            ak akVar7 = this.nw;
            akVar5.b("华娱无线", i, i2, 17);
            ak akVar8 = this.nw;
            ak akVar9 = this.nw;
            this.nw.b("七曜工作组", this.nq, this.nr + 20, 17);
            ak akVar10 = this.nw;
            ak akVar11 = this.nw;
            this.nw.b("荣誉出品", this.nq, this.nr + 40, 17);
            this.nw.f(0, 0, this.gn, this.np);
        }
    }

    public final void Z() {
        c(0, 0);
        this.nw.e(255, 255, 255);
        ak akVar = this.nw;
        int i = this.nq;
        int i2 = this.nr;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.b("版本ID:0417080603", i, i2, 33);
        a("ok", "");
    }

    public final void a(int i, int i2) {
        if (i != -1 && i < this.oz.length) {
            String str = "";
            for (int i3 = 0; i3 < this.oz[i].length; i3++) {
                if (this.oz[i][i3] < 0) {
                    if (this.oz[i][i3] == -127) {
                        str = new StringBuffer().append(str).append(i2 % 1000).toString();
                    }
                    if (this.oz[i][i3] == -126) {
                        str = new StringBuffer().append(str).append(this.nG[i2]).toString();
                    }
                    if (this.oz[i][i3] == -125) {
                        str = new StringBuffer().append(str).append(i2 / 1000).toString();
                    }
                    if (this.oz[i][i3] == -124) {
                        str = new StringBuffer().append(str).append(" ").append(this.nG[i2]).toString();
                    }
                    if (this.oz[i][i3] == -123) {
                        str = new StringBuffer().append(str).append("").append(this.oy[this.nk.dp.kH + 6]).toString();
                    }
                    if (this.oz[i][i3] == -122) {
                        str = new StringBuffer().append(str).append("").append(this.nC[this.nk.fU][this.nk.fV]).toString();
                    }
                    if (this.oz[i][i3] == -121) {
                        str = new StringBuffer().append(str).append("").append(this.nC[this.nk.fU][this.nk.fW]).toString();
                    }
                    if (this.oz[i][i3] == -120) {
                        str = new StringBuffer().append(str).append("").append(this.nC[this.nk.fU][this.nk.fX]).toString();
                    }
                    if (this.oz[i][i3] == -119) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.kG).toString();
                    }
                    if (this.oz[i][i3] == -118) {
                        str = new StringBuffer().append(str).append("").append(this.nk.iA).toString();
                    }
                    if (this.oz[i][i3] == -117) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.kG - this.nk.iA).toString();
                    }
                    if (this.oz[i][i3] == -116) {
                        str = new StringBuffer().append(str).append("").append(this.nk.iB).toString();
                    }
                    if (this.oz[i][i3] == -115) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.hP - this.nk.iB).toString();
                    }
                    if (this.oz[i][i3] == -114) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.hP).toString();
                    }
                    if (this.oz[i][i3] == -111) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.ij).append("%").toString();
                    }
                    if (this.oz[i][i3] == -112) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dp.ij - this.nk.iC).append("%").toString();
                    }
                    if (this.oz[i][i3] == -113) {
                        str = new StringBuffer().append(str).append("").append(this.nk.iC).append("%").toString();
                    }
                    if (this.oz[i][i3] == -110) {
                        str = new StringBuffer().append(str).append("").append(this.nk.dC == 0 ? "貂蝉" : "关羽").append("传等待玩家体验").toString();
                    }
                } else {
                    str = new StringBuffer().append(str).append(this.oy[this.oz[i][i3]]).toString();
                }
            }
            a(20, 110, 5, str);
        }
    }

    public final void a(int[] iArr) {
        this.nz = iArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.a(int[][], int, int, boolean):void
     arg types: [int[][], int, int, int]
     candidates:
      o.a(int, int, int, java.lang.String):void
      o.a(java.lang.String, int, int, int):java.lang.String[]
      o.a(int[][], int, int, boolean):void */
    public final void a(int[][] iArr) {
        int i;
        short s;
        int i2;
        if (this.nw != null) {
            if (this.nk.dN != this.nk.dK || this.nk.eG <= 10) {
                if (this.nK == null) {
                    try {
                        this.nK = al.r("/data/up.png");
                    } catch (Exception e) {
                        System.out.println("up pic is err");
                    }
                }
                a(this.nk.eC, ((this.nk.gB <= 0 || this.nk.gB > 4) ? 0 : this.fE[this.nk.gB - 1]) + this.nk.eD + this.nk.gt, iArr);
                if (this.nk.dS) {
                    int i3 = ((this.nz[this.eZ] / 2) % 4) + 1;
                    short[][][] sArr = g.lK;
                    int i4 = this.nk.dO;
                    int i5 = this.nk.dY;
                    for (int i6 = 0; i6 < this.nk.hh[this.nk.dO][this.nk.dY].length; i6++) {
                        short s2 = this.nk.hh[this.nk.dO][this.nk.dY][i6][2];
                        short s3 = this.nk.hh[this.nk.dO][this.nk.dY][i6][0];
                        short s4 = this.nk.hh[this.nk.dO][this.nk.dY][i6][1];
                        ak akVar = this.nw;
                        ak akVar2 = this.nw;
                        ak akVar3 = this.nw;
                        ak akVar4 = this.nw;
                        ak akVar5 = this.nw;
                        ak akVar6 = this.nw;
                        ak akVar7 = this.nw;
                        ak akVar8 = this.nw;
                        int[][] iArr2 = {new int[]{20, 0, 20, 2}, new int[]{20, 0, 20, 0}, new int[]{20, 0, 20, 0}, new int[]{20, 0, 20, 1}};
                        int i7 = this.nk.eC + s3;
                        int i8 = this.nk.eD + s4;
                        if (s2 == 0 || s2 == 2) {
                            c(13, 86, iArr2[s2][1] + i7 + i3, iArr2[s2][2] + i8, iArr2[s2][0], iArr2[s2][3]);
                        } else {
                            c(13, 87, iArr2[s2][1] + i7, iArr2[s2][2] + i8 + i3, iArr2[s2][0], iArr2[s2][3]);
                        }
                    }
                }
                if (this.nk.hN) {
                    String[] strArr = {"命+20%", "命+40%", "命+60%", "怒+100%"};
                    int[] iArr3 = {(this.nk.dp.kG * 20) / 100, (this.nk.dp.kG * 40) / 100, (this.nk.dp.kG * 60) / 100, this.nk.gj};
                    int i9 = this.nk.hQ + this.nk.eC;
                    int i10 = (this.nk.hR + this.nk.eD) - (this.nk.hO * 2);
                    this.nw.e(0, 128, 0);
                    if (this.nk.hP == 3) {
                        this.nw.e(0, 0, 128);
                    }
                    ak akVar9 = this.nw;
                    ak akVar10 = this.nw;
                    this.nw.b(strArr[this.nk.hP], i9 - 1, i10, 33);
                    ak akVar11 = this.nw;
                    ak akVar12 = this.nw;
                    this.nw.b(strArr[this.nk.hP], i9, i10 - 1, 33);
                    ak akVar13 = this.nw;
                    ak akVar14 = this.nw;
                    this.nw.b(strArr[this.nk.hP], i9 + 1, i10, 33);
                    ak akVar15 = this.nw;
                    ak akVar16 = this.nw;
                    this.nw.b(strArr[this.nk.hP], i9, i10 + 1, 33);
                    this.nw.setColor(16777215);
                    ak akVar17 = this.nw;
                    String str = strArr[this.nk.hP];
                    ak akVar18 = this.nw;
                    ak akVar19 = this.nw;
                    akVar17.b(str, i9, i10, 33);
                }
                this.nw.setColor(0);
                if (this.nk.gB > 0 && this.nk.gB <= 9 && this.nk.dp.lf >= this.nk.dp.kB[this.nk.dp.kC].length - 2) {
                    this.nw.setColor(this.nk.gB % 2 == 0 ? 16711680 : 16777215);
                }
                this.nw.e(0, 0, this.gn, this.nk.gs);
                this.nw.e(0, this.np - this.nk.gt, this.gn, this.nk.gt);
                if (this.nk.gS) {
                    this.nw.setColor(0);
                    this.nk.gT++;
                    if (this.nk.gU == 0 && this.nk.gT / 3 == 2) {
                        this.nk.gU = 1;
                        this.nk.gT = 0;
                    }
                    if (this.nk.gV > g.lM[this.nk.gP].length - 1) {
                        d(2, 1, 1, 189, 81, 1);
                    }
                    if (this.nk.gU == 2) {
                        if (2 - (this.nk.gT / 3) == 0) {
                            this.nk.gP = this.nk.gQ;
                            this.nk.gS = false;
                            if (this.nk.gX == 1) {
                                this.nk.gX = 2;
                            }
                        }
                    } else if (this.nk.gU == 1) {
                        if (!(g.lL[this.nk.gP][0] == -2 && g.lL[this.nk.gP][1] == -2)) {
                            d(1, 1, 1, 189, 81, 1);
                        }
                        if (this.nk.iv) {
                            int i11 = this.nq;
                            int i12 = this.np - 10;
                            ak akVar20 = this.nw;
                            ak akVar21 = this.nw;
                            a("按0键继续！", i11, i12, 0, 33);
                            if (g.lL[this.nk.gP][0] != -2) {
                                this.nw.setColor(16777215);
                                int i13 = g.lL[this.nk.gP][0] == -1 ? this.nk.dp.kH : this.nk.dq[g.lL[this.nk.gP][0]].lj;
                                short s5 = g.lL[this.nk.gP][0] == -1 ? this.nk.dp.lg : this.nk.dq[g.lL[this.nk.gP][0]].lg;
                                if (g.lM[this.nk.gP][this.nk.gV][1] == 1) {
                                    i13 += g.lM[this.nk.gP][this.nk.gV][2];
                                    ak akVar22 = this.nw;
                                    ak akVar23 = this.nw;
                                    this.nw.b(this.nk.gN[g.lM[this.nk.gP][this.nk.gV][0]], this.gn - 2, 208, 24);
                                }
                                int i14 = i13;
                                int i15 = this.gn - 30;
                                ak akVar24 = this.nw;
                                ak akVar25 = this.nw;
                                c(13, 21, i15, 208, 33, 2);
                                int i16 = this.gn - 30;
                                ak akVar26 = this.nw;
                                ak akVar27 = this.nw;
                                c(13, i14, i16, 203, 33, 2);
                                String str2 = g.lW[s5];
                                int i17 = this.gn - 53;
                                ak akVar28 = this.nw;
                                ak akVar29 = this.nw;
                                a(str2, i17, 206, 1, 40);
                                this.nw.setColor(16777215);
                            }
                            if (g.lL[this.nk.gP][1] != -2) {
                                this.nw.setColor(16777215);
                                if (g.lL[this.nk.gP][1] == -4) {
                                    i = 9;
                                    s = 48;
                                } else {
                                    i = g.lL[this.nk.gP][1] == -1 ? this.nk.dp.kH : this.nk.dq[g.lL[this.nk.gP][1]].lj;
                                    if (i == 1) {
                                        i = 2;
                                    }
                                    s = g.lL[this.nk.gP][1] == -1 ? this.nk.dp.lg : this.nk.dq[g.lL[this.nk.gP][1]].lg;
                                }
                                if (g.lM[this.nk.gP][this.nk.gV][1] == -1) {
                                    i2 = i2 == 1 ? i2 + g.lM[this.nk.gP][this.nk.gV][2] + 1 : i2 + g.lM[this.nk.gP][this.nk.gV][2];
                                    ak akVar30 = this.nw;
                                    String str3 = this.nk.gN[g.lM[this.nk.gP][this.nk.gV][0]];
                                    ak akVar31 = this.nw;
                                    ak akVar32 = this.nw;
                                    akVar30.b(str3, 2, 100, 20);
                                }
                                int i18 = i2;
                                ak akVar33 = this.nw;
                                ak akVar34 = this.nw;
                                c(13, 21, 30, 100, 33);
                                ak akVar35 = this.nw;
                                ak akVar36 = this.nw;
                                c(13, i18, 30, 95, 33);
                                String str4 = g.lW[s];
                                ak akVar37 = this.nw;
                                ak akVar38 = this.nw;
                                a(str4, 53, 98, 1, 36);
                                this.nw.setColor(16777215);
                            }
                        }
                    }
                } else {
                    int i19 = this.nk.dp.kH == 0 ? 0 : 2;
                    ak akVar39 = this.nw;
                    ak akVar40 = this.nw;
                    c(13, 75, 2, 14, 20);
                    ak akVar41 = this.nw;
                    ak akVar42 = this.nw;
                    c(13, 75, 24, 14, 20, 2);
                    this.nw.setColor(16711680);
                    this.nw.b("斩", 8, 20, 0);
                    ak akVar43 = this.nw;
                    String stringBuffer = new StringBuffer().append("").append(this.nk.ep).toString();
                    ak akVar44 = this.nw;
                    ak akVar45 = this.nw;
                    akVar43.b(stringBuffer, 38, 20, 17);
                    this.nw.b("人", 50, 20, 0);
                    int i20 = this.nq;
                    int i21 = this.np;
                    ak akVar46 = this.nw;
                    ak akVar47 = this.nw;
                    c(13, 77, i20, i21, 33);
                    if (this.nk.ga[this.nk.dp.kH][21] == 1) {
                        if (this.nk.dn == 0) {
                            int i22 = this.np - 41;
                            ak akVar48 = this.nw;
                            ak akVar49 = this.nw;
                            c(13, 69, 14, i22, 36);
                        } else {
                            int i23 = this.np - 41;
                            ak akVar50 = this.nw;
                            ak akVar51 = this.nw;
                            c(13, 66, 14, i23, 36);
                        }
                    }
                    if (this.nk.ga[this.nk.dp.kH][22] == 1) {
                        if (this.nk.dn == 1) {
                            int i24 = this.np - 29;
                            ak akVar52 = this.nw;
                            ak akVar53 = this.nw;
                            c(13, 68, 14, i24, 36);
                        } else {
                            int i25 = this.np - 29;
                            ak akVar54 = this.nw;
                            ak akVar55 = this.nw;
                            c(13, 65, 14, i25, 36);
                        }
                    }
                    if (this.nk.ga[this.nk.dp.kH][23] == 1) {
                        if (this.nk.dn == 2) {
                            int i26 = this.np - 17;
                            ak akVar56 = this.nw;
                            ak akVar57 = this.nw;
                            c(13, 67, 14, i26, 36);
                        } else {
                            int i27 = this.np - 17;
                            ak akVar58 = this.nw;
                            ak akVar59 = this.nw;
                            c(13, 64, 14, i27, 36);
                        }
                    }
                    c cVar = this.nk;
                    int i28 = c.a(this.nk.dp.kC, this.nk.gu) ? i19 + 1 : i19;
                    int i29 = this.np - 24;
                    ak akVar60 = this.nw;
                    ak akVar61 = this.nw;
                    c(13, i28, 29, i29, 36);
                    int i30 = this.np;
                    ak akVar62 = this.nw;
                    ak akVar63 = this.nw;
                    c(13, 71, 0, i30, 36);
                    int i31 = this.gn;
                    int i32 = this.np;
                    ak akVar64 = this.nw;
                    ak akVar65 = this.nw;
                    c(13, 71, i31, i32, 40, 2);
                    a(this.oi, (this.nk.dp.z * 100) / this.nk.dp.kG, 0, false);
                    a(this.oh, (this.nk.dp.kF * 100) / this.nk.dp.kG, 0, this.nk.dp.kF < this.nk.dp.kG / 5 && this.nz[this.eZ] % 2 == 0);
                    a(this.oj, (this.nk.gi * 100) / this.nk.gj, 0, this.nk.gi == this.nk.gj && this.nz[this.eZ] % 2 == 1);
                    if (!this.nk.hW) {
                        int i33 = (this.nk.gp / 100) / 10;
                        int i34 = (this.nk.gp / 100) % 10;
                        a(this.ok, ((this.nk.gp % 100) * 100) / 100, 0, false);
                        this.nw.setColor(16777215);
                        if (i33 > 0) {
                            ak akVar66 = this.nw;
                            ak akVar67 = this.nw;
                            this.nw.b(new StringBuffer().append("").append(i33).toString(), 197, this.np - 20, 20);
                        }
                        if (i34 > 0 || i33 > 0) {
                            ak akVar68 = this.nw;
                            ak akVar69 = this.nw;
                            this.nw.b("X", 189, this.np - 20, 20);
                            ak akVar70 = this.nw;
                            ak akVar71 = this.nw;
                            this.nw.b(new StringBuffer().append("").append(i34).toString(), 215, this.np - 20, 24);
                        }
                    }
                    if (this.nk.fB != -1) {
                        int i35 = this.gn - 30;
                        ak akVar72 = this.nw;
                        ak akVar73 = this.nw;
                        c(13, 21, i35, 5, 17);
                        int i36 = this.nk.dq[this.nk.fB].lj;
                        int i37 = this.gn - 30;
                        ak akVar74 = this.nw;
                        ak akVar75 = this.nw;
                        c(13, i36, i37, 9, 17, 2);
                        int i38 = this.gn;
                        ak akVar76 = this.nw;
                        ak akVar77 = this.nw;
                        c(13, 71, i38, 55, 40, 2);
                        if (this.nk.dq[this.nk.fB].kF <= 0 && (this.nz[this.eZ] / 4) % 2 == 0) {
                            int i39 = this.gn - 30;
                            ak akVar78 = this.nw;
                            ak akVar79 = this.nw;
                            c(13, 15, i39, 9, 17);
                        }
                        ak akVar80 = this.nw;
                        ak akVar81 = this.nw;
                        c(13, 26, 81, 32, 20);
                        ak akVar82 = this.nw;
                        ak akVar83 = this.nw;
                        c(13, 26, 185, 32, 24, 2);
                        a(this.og, (this.nk.dq[this.nk.fB].kF * 100) / this.nk.dq[this.nk.fB].kG, 2, false);
                        this.nw.setColor(16777215);
                        a(135, 5, g.lW[this.nk.dq[this.nk.fB].lg], 2, 0, 5);
                        H();
                    }
                    if (!this.nk.y) {
                        a(20, 110, 5, "");
                        this.nw.setColor(this.eW);
                        ak akVar84 = this.nw;
                        int i40 = this.nq;
                        ak akVar85 = this.nw;
                        ak akVar86 = this.nw;
                        akVar84.b("是否开启教学?", i40, 140, 33);
                        a(this.nq - 40, (int) DirectGraphics.ROTATE_180, "是", 1, this.nk.aV == 0 ? 1 : 0, 0);
                        a(this.nq + 40, (int) DirectGraphics.ROTATE_180, "否", 1, this.nk.aV == 1 ? 1 : 0, 0);
                    }
                    if (this.nk.gG) {
                        if (this.nk.hs[5] < 0 && this.nk.hs[6] > 0 && this.nz[this.eZ] % 4 != 0) {
                            int i41 = this.nq;
                            int i42 = this.nr - 20;
                            ak akVar87 = this.nw;
                            ak akVar88 = this.nw;
                            a("按右软键进入人物升级界面!", i41, i42, 0, 33);
                        }
                        if (this.nk.hs[3] < 0 && this.nk.hs[8] > 0 && this.nz[this.eZ] % 4 != 0) {
                            this.nw.setColor(this.eW);
                            int i43 = this.nq;
                            int i44 = this.nr;
                            ak akVar89 = this.nw;
                            ak akVar90 = this.nw;
                            a("使用5+3击晕敌人！", i43, i44, 0, 33);
                            int i45 = this.nr + 20;
                            ak akVar91 = this.nw;
                            ak akVar92 = this.nw;
                            c(13, 19, 105, i45, 33);
                            int i46 = (this.nr - 4) + 20;
                            ak akVar93 = this.nw;
                            ak akVar94 = this.nw;
                            c(13, 31, 105, i46, 33);
                            int i47 = this.nr + 20;
                            ak akVar95 = this.nw;
                            ak akVar96 = this.nw;
                            a("+", 122, i47, 0, 33);
                            int i48 = this.nr + 20;
                            ak akVar97 = this.nw;
                            ak akVar98 = this.nw;
                            c(13, 19, 140, i48, 33);
                            int i49 = (this.nr - 4) + 20;
                            ak akVar99 = this.nw;
                            ak akVar100 = this.nw;
                            c(13, 30, 140, i49, 33);
                        }
                        if (this.nk.hs[0] < 0 && this.nk.hs[10] > 0 && this.nz[this.eZ] % 4 != 0) {
                            int i50 = this.nq;
                            int i51 = this.nr;
                            ak akVar101 = this.nw;
                            ak akVar102 = this.nw;
                            a("按1键可以查看本关地图", i50, i51, 0, 33);
                        }
                        if (this.nk.dY == 3 && this.nk.hs[10] < 0) {
                            int[] iArr4 = this.nz;
                            int i52 = this.eZ;
                        }
                        if (this.nk.hs[7] == -1 && this.nk.gi == this.nk.gj && this.nz[this.eZ] % 4 != 0 && this.nk.hX < 100) {
                            boolean z = this.nk.hY;
                        }
                    }
                    if ((this.nk.ib[34] == 1 || this.nk.ib[35] == 1 || this.nk.ib[36] == 1) && this.nz[this.eZ] % 4 != 0) {
                        if (this.nk.ib[(this.nk.dC * 15) + 10] == 0 && this.nk.ib[(this.nk.dC * 15) + 11] == 0) {
                            int i53 = this.nq;
                            ak akVar103 = this.nw;
                            ak akVar104 = this.nw;
                            a("请您现在升级武器，", i53, 60, 0, 33);
                            int i54 = this.nq;
                            ak akVar105 = this.nw;
                            ak akVar106 = this.nw;
                            a("按右软键进入升级界面。", i54, 80, 0, 33);
                        }
                        if (!(this.nk.ib[(this.nk.dC * 15) + 10] == 0 && this.nk.ib[(this.nk.dC * 15) + 11] == 0)) {
                            int i55 = this.nq;
                            ak akVar107 = this.nw;
                            ak akVar108 = this.nw;
                            a("请您现在升级属性，", i55, 60, 0, 33);
                            int i56 = this.nq;
                            ak akVar109 = this.nw;
                            ak akVar110 = this.nw;
                            a("按右软键进入升级界面。", i56, 80, 0, 33);
                        }
                    }
                    if (this.nk.iR && this.nz[this.eZ] % 4 != 0) {
                        int i57 = this.nq;
                        int i58 = this.np - 55;
                        ak akVar111 = this.nw;
                        ak akVar112 = this.nw;
                        a("9键发动必杀！！！", i57, i58, 0, 33);
                        int i59 = this.np - 29;
                        ak akVar113 = this.nw;
                        ak akVar114 = this.nw;
                        c(13, 19, 47, i59, 33);
                        int i60 = this.np - 32;
                        ak akVar115 = this.nw;
                        ak akVar116 = this.nw;
                        c(13, 33, 47, i60, 33);
                    }
                    if (this.nk.dD > 0 && this.nk.gi == this.nk.gj && this.nz[this.eZ] % 4 != 0) {
                        int i61 = this.np - 29;
                        ak akVar117 = this.nw;
                        ak akVar118 = this.nw;
                        c(13, 19, 47, i61, 33);
                        int i62 = this.np - 32;
                        ak akVar119 = this.nw;
                        ak akVar120 = this.nw;
                        c(13, 33, 47, i62, 33);
                    }
                    ak akVar121 = this.nw;
                    ak akVar122 = this.nw;
                    this.nw.a(this.nK, this.gn, this.np - 55, 40);
                    this.nw.f(0, 0, this.gn, this.np);
                }
            } else {
                aL();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void aM() {
        int i = this.nk.ci;
        this.nk.getClass();
        if (i != 0) {
            int i2 = this.nk.ci;
            this.nk.getClass();
            if (i2 != 1) {
                int i3 = this.nk.ci;
                this.nk.getClass();
                if (i3 != 7) {
                    int i4 = this.nk.ci;
                    this.nk.getClass();
                    if (i4 != 31) {
                        this.nk.hZ = this.nk.ci;
                        c cVar = this.nk;
                        this.nk.getClass();
                        cVar.ci = 7;
                        this.nk.ac();
                    }
                }
            }
        }
    }

    public final void ak() {
        this.oe = true;
    }

    public final void al() {
        if (this.nw != null) {
            if (this.nH[17] == null) {
                k(17);
            }
            if (this.nH[7] == null) {
                k(7);
            }
            aL();
        }
    }

    public final void am() {
        c(2, 0);
        this.nw.setColor(this.eW);
        byte[][] bArr = {new byte[]{0, 2, 4, 6, 8, 10, -1, -1}, new byte[]{-1, -1, 10, 8, 6, 4, 2, 0}};
        int i = this.nk.eQ;
        int i2 = this.nk.eR;
        int i3 = this.nk.eQ;
        int i4 = this.nk.eQ == -1 ? bArr[0][this.nk.eR] * this.nk.eQ * -1 : bArr[1][this.nk.eR] * this.nk.eQ;
        c(this.nk.dG == 0 ? 2 : 0, 60 - i4, 93 - i4);
        c(this.nk.dG == 0 ? 0 : 2, i4 + 50, i4 + 83);
        if ((this.nz[this.eZ] / 5) % 2 == 0) {
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 20, 84, 75, 36);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 20, 26, 75, 40, 2);
        }
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        c(13, 18, 60, 117, 40);
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        c(13, 18, 45, 117, 36, 2);
        this.nw.setColor(this.eW);
        ak akVar9 = this.nw;
        String str = this.nk.dG == 0 ? "关   羽" : "貂   蝉";
        ak akVar10 = this.nw;
        ak akVar11 = this.nw;
        akVar9.b(str, 52, 113, 33);
        ak akVar12 = this.nw;
        ak akVar13 = this.nw;
        c(13, 18, 157, 76, 40);
        ak akVar14 = this.nw;
        ak akVar15 = this.nw;
        c(13, 18, 157, 76, 36, 2);
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        ak akVar18 = this.nw;
        akVar16.b("死  斗", 157, 72, 33);
        a(this.nw, "源源不断的敌人！|考验你的生存能力！|获得称号会有属性奖励！", 26, 155, 140, this.eW);
        a("ok", "cancel");
    }

    public final void an() {
        this.nw.setColor(0);
        this.nw.e(0, 0, this.gn, this.np);
        this.nw.setColor(this.eW);
        ak akVar = this.nw;
        int i = this.nq;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.b("按5键返回", i, 90, 33);
    }

    public final void ao() {
        c(1, 110);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 3) {
                break;
            }
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 18, (this.gn >> 1) - 20, (i2 * 25) + 60, 17);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 18, (this.gn >> 1) + 20, (i2 * 25) + 60, 17, 2);
            i = i2 + 1;
        }
        this.ba = (this.nz[this.eZ] / 3) % 2 == 0 ? 18 : 17;
        int i3 = this.ba;
        int i4 = (this.gn >> 1) - 20;
        int i5 = (this.nk.cY * 25) + 60;
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        c(13, i3, i4, i5, 17);
        int i6 = this.ba;
        int i7 = (this.gn >> 1) + 20;
        int i8 = (this.nk.cY * 25) + 60;
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        c(13, i6, i7, i8, 17, 2);
        this.nw.setColor(16768515);
        ak akVar9 = this.nw;
        ak akVar10 = this.nw;
        this.nw.b("人物升级", this.gn >> 1, 63, 17);
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        this.nw.b("物品收集", this.gn >> 1, 88, 17);
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        this.nw.b("购买春秋", this.gn >> 1, 113, 17);
        ak akVar15 = this.nw;
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        akVar15.b("提示:", 20, 150, 20);
        a(this.nw, this.oD[this.nk.hJ], 20, 170, (int) Player.REALIZED, this.eW);
        a("ok", "cancel");
    }

    public final void ap() {
        if (this.nw != null) {
            short[][] sArr = {new short[]{3, -33, 25}, new short[]{2, -35, 70}, new short[]{0, 8, 60}, new short[]{4, 22, 110}, new short[]{8, 0, 110}};
            aL();
            this.nw.setColor(16711680);
            c(0, 0);
            for (int i = 0; i < sArr.length; i++) {
                short s = sArr[i][0];
                int i2 = sArr[i][1] + this.nq;
                short s2 = sArr[i][2];
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                c(10, s, i2, s2, 20);
            }
            this.nw.setColor(0);
            this.nw.e(36, 160, 166, 98);
            c(36, 160, 4, 1);
            this.nw.setColor(this.eW);
            this.nw.setColor(this.eW);
            ak akVar3 = this.nw;
            int i3 = this.nq;
            ak akVar4 = this.nw;
            ak akVar5 = this.nw;
            akVar3.b("请开启各种升级上", i3, 205, 33);
            ak akVar6 = this.nw;
            int i4 = this.nq;
            ak akVar7 = this.nw;
            ak akVar8 = this.nw;
            akVar6.b("限提升主角能力！", i4, 225, 33);
            a("", "cancel");
        }
    }

    public final void aq() {
        aL();
        if (this.nH[17][0] != null) {
            this.nH[17][0] = null;
            this.nH[17][1] = null;
            this.nH[17][2] = null;
            this.nH[17][3] = null;
            w();
        }
        this.nw.setColor(16711680);
        ak akVar = this.nw;
        int i = this.nq;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.b("是否开启声音?", i, 90, 33);
        ak akVar4 = this.nw;
        int i2 = this.np;
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        akVar4.b("是", 0, i2, 36);
        ak akVar7 = this.nw;
        int i3 = this.gn;
        int i4 = this.np;
        ak akVar8 = this.nw;
        ak akVar9 = this.nw;
        akVar7.b("否", i3, i4, 40);
    }

    public final void ar() {
        this.oS = null;
        this.oS = al.n(500, 260);
    }

    public final void as() {
        try {
            if (this.ns == null) {
                this.ns = al.r("/exit.png");
            }
        } catch (Exception e) {
            System.out.println("there is a error when exit");
        }
        this.nw.setColor(0);
        this.nw.e(0, 0, this.gn, this.np);
        ak akVar = this.nw;
        al alVar = this.ns;
        int i = this.nq;
        int i2 = this.nr;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.a(alVar, i, i2, 3);
        this.nw.setColor(16777215);
        ak akVar4 = this.nw;
        int i3 = this.nq;
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        akVar4.b("您确定要离开?", i3, 90, 33);
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        this.nw.b("否", this.gn - 5, this.np - 2, 40);
        ak akVar9 = this.nw;
        ak akVar10 = this.nw;
        this.nw.b("是", 5, this.np - 2, 36);
    }

    public final void at() {
        if (this.nw != null) {
            aL();
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(11, 0, 0, 0, 20);
            int[] iArr = {2, 2, 3, 3, 4, 4, 3, 3};
            if (this.nz[this.eZ] < 30) {
                this.nw.setColor(0);
                this.nw.e(0, 0 - (this.nz[this.eZ] * 5), this.gn, this.nr);
                this.nw.e(0, this.nr + (this.nz[this.eZ] * 5), this.gn, this.nr);
            } else if (this.nz[this.eZ] < 36) {
                int i = this.gn >> 1;
                int i2 = ((this.nz[this.eZ] - 30) * 45) + 0;
                ak akVar3 = this.nw;
                ak akVar4 = this.nw;
                c(11, 1, i, i2, 33);
            } else if (this.nz[this.eZ] < 37) {
                int i3 = this.gn >> 1;
                ak akVar5 = this.nw;
                ak akVar6 = this.nw;
                c(11, 1, i3, -4, 17);
            } else if (this.nz[this.eZ] < 38) {
                this.nw.setColor(16777215);
                this.nw.e(0, 291, this.gn, 8);
            } else if (this.nz[this.eZ] < 40) {
                this.nw.setColor(16777215);
                this.nw.e(0, 293, this.gn, 4);
            } else if (this.nz[this.eZ] < 45) {
                int i4 = (this.nq - 12) - ((this.nz[this.eZ] - 40) * 13);
                ak akVar7 = this.nw;
                ak akVar8 = this.nw;
                c(11, 2, i4, 280, 20);
                int i5 = this.nq + 12 + ((this.nz[this.eZ] - 40) * 13);
                ak akVar9 = this.nw;
                ak akVar10 = this.nw;
                c(11, 2, i5, 280, 24);
                this.nw.f((this.gn >> 1) - ((this.nz[this.eZ] - 40) * 13), 280, (this.nz[this.eZ] - 40) * 26, 50);
                int i6 = this.gn >> 1;
                ak akVar11 = this.nw;
                ak akVar12 = this.nw;
                c(11, 3, i6, 285, 17);
                this.nw.f(0, 0, this.gn, this.np);
            } else {
                if (this.of != null) {
                    for (int i7 = 0; i7 < this.of.length; i7++) {
                        if (this.of[i7] != null) {
                            this.of[i7].c(this.nw);
                        }
                    }
                }
                ak akVar13 = this.nw;
                ak akVar14 = this.nw;
                c(11, 2, 38, 280, 20);
                ak akVar15 = this.nw;
                ak akVar16 = this.nw;
                c(11, 2, DirectGraphics.ROTATE_180, 280, 20);
                int i8 = this.gn >> 1;
                ak akVar17 = this.nw;
                ak akVar18 = this.nw;
                c(11, 3, i8, 285, 17);
            }
            if (this.nz[this.eZ] >= 37) {
                int i9 = this.gn >> 1;
                ak akVar19 = this.nw;
                ak akVar20 = this.nw;
                c(11, 1, i9, 0, 17);
            }
        }
    }

    public final void au() {
        this.nw.setColor(0);
        this.nw.e(0, this.nr - 20, this.gn, 40);
        this.nw.setColor(this.eW);
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        this.nw.b(new StringBuffer().append("载入中").append(new String[]{".", "..", "..."}[(this.nz[this.eZ] / 3) % 3]).toString(), this.nq - 40, this.nr - 10, 20);
    }

    public final void b() {
        if (this.nw != null) {
            aL();
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(11, 0, 0, 0, 20);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 21, 0, 26, 20);
            int i = this.nk.dp.kH == 0 ? 0 : 2;
            ak akVar5 = this.nw;
            ak akVar6 = this.nw;
            c(13, i, 4, 30, 20);
            this.nw.setColor(16777215);
            this.nw.f(this.gn - this.nk.eV, 0, this.nk.eV, Player.PREFETCHED);
            ak akVar7 = this.nw;
            String str = this.oV[this.nk.dp.kH];
            int i2 = this.gn;
            int i3 = this.np;
            int a = this.ny.a(22269);
            if (a == 0) {
                a = 16;
            }
            int height = this.ny.getHeight();
            int i4 = i2 / a;
            int i5 = (i3 % height) / 2;
            akVar7.setColor(16777215);
            int i6 = 0;
            for (int i7 = 0; i7 < str.length(); i7++) {
                String substring = str.substring(i7, i7 + 1);
                if (substring.equals("|") || substring == null) {
                    i6 = (i7 + i6) % i4 != 0 ? i6 + ((i4 - ((i7 + i6) % i4)) - 1) : i6 - 1;
                } else {
                    int i8 = 210 - ((a + 5) * ((i6 + i7) / i4));
                    if (i8 >= 0 && i8 <= this.gn + 32) {
                        akVar7.b(substring, i8, i5 + 3 + (((i6 + i7) % i4) * height), 20);
                    }
                }
            }
            this.nw.f(0, 0, this.gn, this.np);
            this.nw.setColor(this.eW);
            ak akVar8 = this.nw;
            ak akVar9 = this.nw;
            this.nw.b("0键返回", this.nq, this.np - 5, 33);
        }
    }

    public final void b(int i, int i2) {
        int[] iArr = {35, 95, 155, 220};
        aL();
        d(i, 1, 0, 25, 0, 4);
        this.nw.setColor(this.eW);
        if (this.nk.iv) {
            if (this.nk.ga[this.nk.dp.kH][21] == 1) {
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                c(14, 10, 60, 33, 20);
                ak akVar3 = this.nw;
                String str = this.oA[0][0];
                ak akVar4 = this.nw;
                ak akVar5 = this.nw;
                akVar3.b(str, 110, 30, 20);
                ak akVar6 = this.nw;
                String str2 = this.oA[0][1];
                ak akVar7 = this.nw;
                ak akVar8 = this.nw;
                akVar6.b(str2, 95, 50, 20);
            } else {
                ak akVar9 = this.nw;
                ak akVar10 = this.nw;
                ak akVar11 = this.nw;
                akVar9.b("未获得！获得条件：", 60, 30, 20);
                ak akVar12 = this.nw;
                ak akVar13 = this.nw;
                ak akVar14 = this.nw;
                akVar12.b("关内，达成百人斩", 60, 50, 20);
            }
            if (this.nk.ga[this.nk.dp.kH][22] == 1) {
                ak akVar15 = this.nw;
                ak akVar16 = this.nw;
                c(14, 11, 60, 93, 20);
                ak akVar17 = this.nw;
                String str3 = this.oA[1][0];
                ak akVar18 = this.nw;
                ak akVar19 = this.nw;
                akVar17.b(str3, 110, 90, 20);
                ak akVar20 = this.nw;
                String str4 = this.oA[1][1];
                ak akVar21 = this.nw;
                ak akVar22 = this.nw;
                akVar20.b(str4, 110, 110, 20);
            } else {
                ak akVar23 = this.nw;
                ak akVar24 = this.nw;
                ak akVar25 = this.nw;
                akVar23.b("未获得！获得条件：", 60, 90, 20);
                ak akVar26 = this.nw;
                ak akVar27 = this.nw;
                ak akVar28 = this.nw;
                akVar26.b("最高连击数达到200", 60, 110, 20);
            }
            if (this.nk.ga[this.nk.dp.kH][23] == 1) {
                ak akVar29 = this.nw;
                ak akVar30 = this.nw;
                c(14, 12, 60, 153, 20);
                ak akVar31 = this.nw;
                String str5 = this.oA[2][0];
                ak akVar32 = this.nw;
                ak akVar33 = this.nw;
                akVar31.b(str5, 110, 150, 20);
                ak akVar34 = this.nw;
                String str6 = this.oA[2][1];
                ak akVar35 = this.nw;
                ak akVar36 = this.nw;
                akVar34.b(str6, 110, 170, 20);
            } else {
                ak akVar37 = this.nw;
                ak akVar38 = this.nw;
                ak akVar39 = this.nw;
                akVar37.b("未获得！获得条件：", 60, 150, 20);
                ak akVar40 = this.nw;
                ak akVar41 = this.nw;
                ak akVar42 = this.nw;
                akVar40.b("通全关", 60, 170, 20);
            }
            ak akVar43 = this.nw;
            ak akVar44 = this.nw;
            ak akVar45 = this.nw;
            akVar43.b("不装备", 90, 215, 20);
            int i3 = ((this.nz[this.eZ] / 3) % 2) + 20;
            int i4 = iArr[i2];
            ak akVar46 = this.nw;
            ak akVar47 = this.nw;
            c(13, 20, i3, i4, 3);
            a("ok", "cancel");
        }
    }

    public final void b(ak akVar) {
        this.nw = akVar;
        this.nx = DirectUtils.a(this.nw);
        this.nw.f(0, 0, this.gn, this.np);
        this.ny = aj.d(0, 0, 8);
        this.nw.a(this.ny);
        this.nk.d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.a(int[][], int, int, boolean):void
     arg types: [int[][], int, int, int]
     candidates:
      o.a(int, int, int, java.lang.String):void
      o.a(java.lang.String, int, int, int):java.lang.String[]
      o.a(int[][], int, int, boolean):void */
    public final void b(int[][] iArr) {
        if (this.nw != null) {
            a(this.nk.eC, ((this.nk.gB <= 0 || this.nk.gB > 4) ? 0 : this.fE[this.nk.gB - 1]) + this.nk.eD + this.nk.gt, iArr);
            this.nw.setColor(0);
            if (this.nk.gB > 0 && this.nk.gB <= 9 && this.nk.dp.lf >= this.nk.dp.kB[this.nk.dp.kC].length - 2) {
                this.nw.setColor(this.nk.gB % 2 == 0 ? 16711680 : 16777215);
            }
            this.nw.e(0, 0, this.gn, this.nk.gs);
            this.nw.e(0, this.np - this.nk.gt, this.gn, this.nk.gt);
            int i = this.nk.dp.kH == 0 ? 0 : 2;
            this.nw.setColor(16711680);
            this.nw.b("斩", 8, 20, 0);
            ak akVar = this.nw;
            String stringBuffer = new StringBuffer().append("").append(this.nk.ep).toString();
            ak akVar2 = this.nw;
            ak akVar3 = this.nw;
            akVar.b(stringBuffer, 48, 20, 17);
            this.nw.b("人", 60, 20, 0);
            int i2 = this.nq;
            int i3 = this.np;
            ak akVar4 = this.nw;
            ak akVar5 = this.nw;
            c(13, 77, i2, i3, 33);
            if (this.nk.ga[this.nk.dp.kH][21] == 1) {
                if (this.nk.dn == 0) {
                    int i4 = this.np - 41;
                    ak akVar6 = this.nw;
                    ak akVar7 = this.nw;
                    c(13, 69, 14, i4, 36);
                } else {
                    int i5 = this.np - 41;
                    ak akVar8 = this.nw;
                    ak akVar9 = this.nw;
                    c(13, 66, 14, i5, 36);
                }
            }
            if (this.nk.ga[this.nk.dp.kH][22] == 1) {
                if (this.nk.dn == 1) {
                    int i6 = this.np - 29;
                    ak akVar10 = this.nw;
                    ak akVar11 = this.nw;
                    c(13, 68, 14, i6, 36);
                } else {
                    int i7 = this.np - 29;
                    ak akVar12 = this.nw;
                    ak akVar13 = this.nw;
                    c(13, 65, 14, i7, 36);
                }
            }
            if (this.nk.ga[this.nk.dp.kH][23] == 1) {
                if (this.nk.dn == 2) {
                    int i8 = this.np - 17;
                    ak akVar14 = this.nw;
                    ak akVar15 = this.nw;
                    c(13, 67, 14, i8, 36);
                } else {
                    int i9 = this.np - 17;
                    ak akVar16 = this.nw;
                    ak akVar17 = this.nw;
                    c(13, 64, 14, i9, 36);
                }
            }
            c cVar = this.nk;
            int i10 = c.a(this.nk.dp.kC, this.nk.gu) ? i + 1 : i;
            int i11 = this.np - 24;
            ak akVar18 = this.nw;
            ak akVar19 = this.nw;
            c(13, i10, 29, i11, 36);
            int i12 = this.np;
            ak akVar20 = this.nw;
            ak akVar21 = this.nw;
            c(13, 71, 0, i12, 36);
            int i13 = this.gn;
            int i14 = this.np;
            ak akVar22 = this.nw;
            ak akVar23 = this.nw;
            c(13, 71, i13, i14, 40, 2);
            a(this.oi, (this.nk.dp.z * 100) / this.nk.dp.kG, 0, false);
            a(this.oh, (this.nk.dp.kF * 100) / this.nk.dp.kG, 0, this.nk.dp.kF < this.nk.dp.kG / 5 && this.nz[this.eZ] % 2 == 0);
            a(this.oj, (this.nk.gi * 100) / this.nk.gj, 0, this.nk.gi == this.nk.gj && this.nz[this.eZ] % 2 == 1);
            if (this.nk.fB != -1) {
                int i15 = this.gn - 30;
                ak akVar24 = this.nw;
                ak akVar25 = this.nw;
                c(13, 21, i15, 5, 17);
                int i16 = this.nk.dq[this.nk.fB].lj;
                int i17 = this.gn - 30;
                ak akVar26 = this.nw;
                ak akVar27 = this.nw;
                c(13, i16, i17, 9, 17, 2);
                int i18 = this.gn;
                ak akVar28 = this.nw;
                ak akVar29 = this.nw;
                c(13, 71, i18, 55, 40, 2);
                if (this.nk.dq[this.nk.fB].kF <= 0 && (this.nz[this.eZ] / 4) % 2 == 0) {
                    int i19 = this.gn - 30;
                    ak akVar30 = this.nw;
                    ak akVar31 = this.nw;
                    c(13, 15, i19, 9, 17);
                }
                ak akVar32 = this.nw;
                ak akVar33 = this.nw;
                c(13, 26, 81, 32, 20);
                ak akVar34 = this.nw;
                ak akVar35 = this.nw;
                c(13, 26, 185, 32, 24, 2);
                a(this.og, (this.nk.dq[this.nk.fB].kF * 100) / this.nk.dq[this.nk.fB].kG, 2, false);
                this.nw.setColor(16777215);
                a(140, 11, g.lW[this.nk.dq[this.nk.fB].lg], 2, 0, 9);
                H();
            }
            this.nw.f(0, 0, this.gn, this.np);
            H();
        }
    }

    public final void e(int i) {
        int i2 = this.gn - 10;
        int i3 = (this.gn - i2) >> 1;
        c(6, 0);
        this.nw.setColor(this.eW);
        int[] iArr = {0, 4, 2, 5, 8, 8, 8, 9, 6, 10};
        int[] iArr2 = {0, 5, 2, 3, 4, 0, 0, 12, 11, 13};
        switch (i) {
            case 0:
            case 1:
            case 10:
            default:
                return;
            case 2:
                if (this.nk.ib[(this.nk.dC * 15) + 7] == 0) {
                    String[] a = a("开启全部战役。信息费2元/条（共1条，合计2元），不含通信费!", i2, 18, 11);
                    ak akVar = this.nw;
                    String str = a[this.nk.iq];
                    int i4 = this.np;
                    a(akVar, str, i3, 5, i2, 16777215);
                    this.nk.ik = false;
                    this.nw.e(255, 0, 0);
                    if (this.nk.iq != this.nk.ip) {
                        m(0);
                        return;
                    } else {
                        m(1);
                        return;
                    }
                } else {
                    ak akVar2 = this.nw;
                    int i5 = this.nq;
                    int i6 = this.nr;
                    ak akVar3 = this.nw;
                    ak akVar4 = this.nw;
                    akVar2.b("关卡已开启", i5, i6, 33);
                    this.nk.ik = true;
                    a("", "cancel");
                    return;
                }
            case 3:
                ak akVar5 = this.nw;
                int i7 = this.gn - 40;
                int i8 = this.np;
                a(akVar5, "您是否购买当前战役?|信息费2元/条（共1条，合计2元），不含通信费!", 20, 20, i7, this.eW);
                this.nk.ik = false;
                this.nw.e(255, 0, 0);
                ak akVar6 = this.nw;
                ak akVar7 = this.nw;
                this.nw.b("确定", 5, this.np - 5, 36);
                ak akVar8 = this.nw;
                ak akVar9 = this.nw;
                this.nw.b("返回", this.gn - 5, this.np - 5, 40);
                return;
            case 4:
                if (this.nk.ib[(this.nk.dC * 15) + 1] == 0) {
                    String[] a2 = a(this.pd[5], i2, 18, 11);
                    ak akVar10 = this.nw;
                    String str2 = a2[this.nk.iq];
                    int i9 = this.np;
                    a(akVar10, str2, i3, 5, i2, 16777215);
                    this.nk.ik = false;
                    this.nw.e(255, 0, 0);
                    if (this.nk.iq != this.nk.ip) {
                        m(0);
                        return;
                    } else {
                        m(1);
                        return;
                    }
                } else {
                    ak akVar11 = this.nw;
                    int i10 = this.nq;
                    int i11 = this.nr;
                    ak akVar12 = this.nw;
                    ak akVar13 = this.nw;
                    akVar11.b("你已经开启武器升级上限", i10, i11, 33);
                    this.nk.ik = true;
                    a("", "cancel");
                    return;
                }
            case 5:
                String[] a3 = a(this.pd[1], i2, 18, 11);
                ak akVar14 = this.nw;
                String str3 = a3[this.nk.iq];
                int i12 = this.np;
                a(akVar14, str3, i3, 5, i2, 16777215);
                this.nk.ik = false;
                this.nw.setColor(16777215);
                if (this.nk.iq != this.nk.ip) {
                    m(0);
                    return;
                } else {
                    m(1);
                    return;
                }
            case 6:
                String[] a4 = a(this.pd[0], i2, 18, 11);
                ak akVar15 = this.nw;
                String str4 = a4[this.nk.iq];
                int i13 = this.np;
                a(akVar15, str4, i3, 5, i2, 16777215);
                this.nk.ik = false;
                this.nw.e(255, 0, 0);
                if (this.nk.iq != this.nk.ip) {
                    m(0);
                    return;
                } else {
                    m(1);
                    return;
                }
            case 7:
            case 8:
            case 9:
                if (this.nk.ib[(i - 7) + 4 + (this.nk.dC * 15)] <= 3) {
                    String[] a5 = a(this.pd[iArr[i]], i2, 18, 11);
                    ak akVar16 = this.nw;
                    String str5 = a5[this.nk.iq];
                    int i14 = this.np;
                    a(akVar16, str5, i3, 5, i2, 16777215);
                    this.nk.ik = false;
                    this.nw.e(255, 0, 0);
                    if (this.nk.iq != this.nk.ip) {
                        m(0);
                        return;
                    } else {
                        m(1);
                        return;
                    }
                } else {
                    ak akVar17 = this.nw;
                    int i15 = this.nq;
                    int i16 = this.nr;
                    ak akVar18 = this.nw;
                    ak akVar19 = this.nw;
                    akVar17.b("你已开启属性升级上限", i15, i16, 33);
                    this.nk.ik = true;
                    a("", "cancel");
                    return;
                }
            case 11:
                if (this.nk.ga[this.nk.dC][12] != 20) {
                    String[] a6 = a(new StringBuffer().append(this.pd[3]).append(new StringBuffer().append("您已经有").append(this.nk.ga[this.nk.dC][12]).append("本，还需").append(20 - this.nk.ga[this.nk.dC][12]).append("本。").toString()).toString(), i2, 18, 11);
                    ak akVar20 = this.nw;
                    String str6 = a6[this.nk.iq];
                    int i17 = this.np;
                    a(akVar20, str6, i3, 5, i2, 16777215);
                    this.nk.ik = false;
                    this.nw.setColor(16777215);
                    if (this.nk.iq != this.nk.ip) {
                        m(0);
                        return;
                    } else {
                        m(1);
                        return;
                    }
                } else {
                    for (int i18 = 0; i18 < 5; i18++) {
                        this.nk.ib[i18 + 67 + (this.nk.dC * 4)] = 1;
                    }
                    this.nk.ga[this.nk.dC][5] = 5;
                    this.nk.c(this.nk.ib);
                    this.nk.aj();
                    ak akVar21 = this.nw;
                    int i19 = this.nq;
                    int i20 = this.nr;
                    ak akVar22 = this.nw;
                    ak akVar23 = this.nw;
                    akVar21.b("你的书已经收集齐", i19, i20, 33);
                    this.nk.ik = true;
                    a("", "cancel");
                    return;
                }
            case 12:
                ak akVar24 = this.nw;
                int i21 = this.nq;
                int i22 = this.nr;
                ak akVar25 = this.nw;
                ak akVar26 = this.nw;
                akVar24.b("购买中", i21, i22, 33);
                return;
            case 13:
                ak akVar27 = this.nw;
                int i23 = this.nq;
                int i24 = this.nr;
                ak akVar28 = this.nw;
                ak akVar29 = this.nw;
                akVar27.b("购买成功", i23, i24, 33);
                a("ok", "");
                return;
            case 14:
                ak akVar30 = this.nw;
                int i25 = this.nq;
                int i26 = this.nr;
                ak akVar31 = this.nw;
                ak akVar32 = this.nw;
                akVar30.b("购买失败", i25, i26, 33);
                a("", "cancel");
                return;
        }
    }

    public final void e(int i, int i2) {
        c(3, 0);
        byte[][] bArr = {new byte[]{0}, new byte[]{d.dX, -20}, new byte[]{45, 0, -45}, new byte[]{38, d.jn, -13, -38}, new byte[]{50, d.jv, 0, -25, -50}};
        this.nw.setColor(this.eW);
        int i3 = this.nq;
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 18, i3, 45, 40);
        int i4 = this.nq;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 18, i4, 45, 36, 2);
        if (i == 0) {
            ak akVar5 = this.nw;
            String str = this.nG[this.nk.dO];
            int i5 = this.nq;
            ak akVar6 = this.nw;
            ak akVar7 = this.nw;
            akVar5.b(str, i5, 41, 33);
            ak akVar8 = this.nw;
            ak akVar9 = this.nw;
            c(13, 21, 45, 111, 33);
            int i6 = this.nk.dp.kH == 0 ? 0 : 2;
            ak akVar10 = this.nw;
            ak akVar11 = this.nw;
            c(13, i6, 45, 107, 33);
            ak akVar12 = this.nw;
            ak akVar13 = this.nw;
            ak akVar14 = this.nw;
            akVar12.b("获得杀气：", 86, 78, 36);
            ak akVar15 = this.nw;
            ak akVar16 = this.nw;
            ak akVar17 = this.nw;
            akVar15.b("武将击破：", 86, 105, 36);
            ak akVar18 = this.nw;
            ak akVar19 = this.nw;
            ak akVar20 = this.nw;
            akVar18.b("敌兵击破：", 86, 132, 36);
            ak akVar21 = this.nw;
            ak akVar22 = this.nw;
            ak akVar23 = this.nw;
            akVar21.b("伤血总量：", 86, 159, 36);
            ak akVar24 = this.nw;
            ak akVar25 = this.nw;
            ak akVar26 = this.nw;
            akVar24.b("最高连击：", 86, 186, 36);
            if (this.nz[this.eZ] > 20) {
                if (this.nk.ho[0] % 10 > this.nk.ho[4] % 10) {
                    int[] iArr = this.nk.ho;
                    iArr[4] = iArr[4] + 1;
                } else if ((this.nk.ho[0] / 10) % 10 > (this.nk.ho[4] / 10) % 10) {
                    int[] iArr2 = this.nk.ho;
                    iArr2[4] = iArr2[4] + 10;
                } else if ((this.nk.ho[0] / 100) % 10 > (this.nk.ho[4] / 100) % 10) {
                    int[] iArr3 = this.nk.ho;
                    iArr3[4] = iArr3[4] + 100;
                } else if ((this.nk.ho[0] / 1000) % 10 > (this.nk.ho[4] / 1000) % 10) {
                    int[] iArr4 = this.nk.ho;
                    iArr4[4] = iArr4[4] + 1000;
                } else if (this.nk.ho[1] > this.nk.ho[5] || this.nk.ho[2] > this.nk.ho[6] || this.nk.ho[3] > this.nk.ho[7]) {
                    if (this.nk.ho[1] > this.nk.ho[5]) {
                        int[] iArr5 = this.nk.ho;
                        iArr5[5] = iArr5[5] + 1;
                    }
                    if (this.nz[this.eZ] > 30 && this.nk.ho[2] > this.nk.ho[6]) {
                        int[] iArr6 = this.nk.ho;
                        iArr6[6] = iArr6[6] + 1;
                    }
                    if (this.nz[this.eZ] > 40 && this.nk.ho[3] > this.nk.ho[7]) {
                        int[] iArr7 = this.nk.ho;
                        iArr7[7] = iArr7[7] + 1;
                    }
                } else if (this.nk.ho[8] > 0) {
                    int[] iArr8 = this.nk.ho;
                    iArr8[8] = iArr8[8] - 1;
                    this.nw.setColor(16777215);
                    this.nw.e(0, 254, this.gn, 0);
                } else if (this.nk.ho[8] == 0) {
                    this.nk.ho[8] = this.nk.q(0);
                } else if (this.nk.ho[9] > 0) {
                    int[] iArr9 = this.nk.ho;
                    iArr9[9] = iArr9[9] - 1;
                    this.nw.setColor(16777215);
                    this.nw.e(0, 250, this.gn, 4);
                } else {
                    int i7 = this.nk.kh ? 0 : i2;
                    int i8 = 0;
                    while (true) {
                        int i9 = i8;
                        if (i9 >= this.om[i7].length) {
                            break;
                        }
                        if (this.nk.ib[this.nk.dD + 7] == 1 && this.nk.kw) {
                            byte b = this.om[3][0];
                            int i10 = bArr[this.om[3].length - 1][0] + this.nq;
                            ak akVar27 = this.nw;
                            ak akVar28 = this.nw;
                            c(10, b, i10, 250, 3);
                        } else if (!this.nk.kw) {
                            byte b2 = this.om[i7][i9];
                            int i11 = bArr[this.om[i7].length - 1][i9] + this.nq;
                            ak akVar29 = this.nw;
                            ak akVar30 = this.nw;
                            c(10, b2, i11, 250, 3);
                        }
                        i8 = i9 + 1;
                    }
                    String[] strArr = {"高", "中", "低", "微"};
                    if (this.nk.ho[7] <= 30) {
                        this.eF = 3;
                    } else if (this.nk.ho[7] > 30 && this.nk.ho[7] <= 60) {
                        this.eF = 2;
                    } else if (this.nk.ho[7] > 60 && this.nk.ho[7] <= 140) {
                        this.eF = 1;
                    } else if (this.nk.ho[7] > 140) {
                        this.eF = 0;
                    }
                    ak akVar31 = this.nw;
                    String stringBuffer = new StringBuffer().append(strArr[this.eF]).append("").toString();
                    ak akVar32 = this.nw;
                    ak akVar33 = this.nw;
                    akVar31.b(stringBuffer, 212, 159, 40);
                }
            }
            ak akVar34 = this.nw;
            String stringBuffer2 = new StringBuffer().append(this.nk.ho[4]).append("").toString();
            ak akVar35 = this.nw;
            ak akVar36 = this.nw;
            akVar34.b(stringBuffer2, 212, 78, 40);
            ak akVar37 = this.nw;
            String stringBuffer3 = new StringBuffer().append(this.nk.ho[5]).append("%").toString();
            ak akVar38 = this.nw;
            ak akVar39 = this.nw;
            akVar37.b(stringBuffer3, 212, 105, 40);
            ak akVar40 = this.nw;
            String stringBuffer4 = new StringBuffer().append(this.nk.ho[6]).append("%").toString();
            ak akVar41 = this.nw;
            ak akVar42 = this.nw;
            akVar40.b(stringBuffer4, 212, 132, 40);
            ak akVar43 = this.nw;
            String stringBuffer5 = new StringBuffer().append(this.nk.ho[10]).append("").toString();
            ak akVar44 = this.nw;
            ak akVar45 = this.nw;
            akVar43.b(stringBuffer5, 212, 186, 40);
            if (this.nk.fZ) {
                if (this.nm < 60) {
                    this.nm += 12;
                    this.nw.setColor(0);
                    this.nw.e(this.nq - this.nm, this.nr - this.nm, (this.nm * 2) + 0, (this.nm * 2) + 0);
                } else {
                    this.nm = 60;
                }
                if (this.nm == 60) {
                    this.gC++;
                    if (this.gC == 50) {
                        this.gC = 0;
                        this.nk.fZ = false;
                    }
                    this.nw.setColor(0);
                    this.nw.e((this.nq - this.nm) - 8, this.nr - this.nm, (this.nm * 2) + 8, (this.nm * 2) + 0);
                    int i12 = this.nq;
                    int i13 = this.nr - 30;
                    ak akVar46 = this.nw;
                    ak akVar47 = this.nw;
                    a("困难难度开启！", i12, i13, 2, 17);
                    int i14 = this.nq;
                    int i15 = this.nr - 10;
                    ak akVar48 = this.nw;
                    ak akVar49 = this.nw;
                    a("困难难度掉落物", i14, i15, 2, 17);
                    int i16 = this.nq;
                    int i17 = this.nr + 10;
                    ak akVar50 = this.nw;
                    ak akVar51 = this.nw;
                    a("品几率大幅提升！", i16, i17, 2, 17);
                }
            }
            if (!this.nk.fZ) {
                this.nw.setColor(16777215);
                ak akVar52 = this.nw;
                ak akVar53 = this.nw;
                this.nw.b("按5键继续", this.nq, this.np - 4, 33);
            }
        } else if (i == 1) {
            String[] strArr2 = {"六", "五", "四", "三", "二", "一"};
            ak akVar54 = this.nw;
            int i18 = this.nq;
            ak akVar55 = this.nw;
            ak akVar56 = this.nw;
            akVar54.b("死斗模式", i18, 43, 33);
            ak akVar57 = this.nw;
            ak akVar58 = this.nw;
            c(13, 21, 35, 111, 33);
            int i19 = this.nk.dp.kH == 0 ? 0 : 2;
            ak akVar59 = this.nw;
            ak akVar60 = this.nw;
            c(13, i19, 35, 107, 33);
            ak akVar61 = this.nw;
            ak akVar62 = this.nw;
            ak akVar63 = this.nw;
            akVar61.b("武将击破：", 76, 81, 36);
            ak akVar64 = this.nw;
            ak akVar65 = this.nw;
            ak akVar66 = this.nw;
            akVar64.b("敌人击破：", 76, 106, 36);
            ak akVar67 = this.nw;
            ak akVar68 = this.nw;
            ak akVar69 = this.nw;
            akVar67.b("获得分数：", 76, 131, 36);
            ak akVar70 = this.nw;
            ak akVar71 = this.nw;
            ak akVar72 = this.nw;
            akVar70.b("综合评价：", 26, 156, 36);
            ak akVar73 = this.nw;
            String stringBuffer6 = new StringBuffer().append(this.nk.ho[0]).append("").toString();
            ak akVar74 = this.nw;
            ak akVar75 = this.nw;
            akVar73.b(stringBuffer6, 212, 81, 40);
            ak akVar76 = this.nw;
            String stringBuffer7 = new StringBuffer().append(this.nk.ho[1]).append("").toString();
            ak akVar77 = this.nw;
            ak akVar78 = this.nw;
            akVar76.b(stringBuffer7, 212, 106, 40);
            ak akVar79 = this.nw;
            String stringBuffer8 = new StringBuffer().append(this.nk.ho[2]).append("").toString();
            ak akVar80 = this.nw;
            ak akVar81 = this.nw;
            akVar79.b(stringBuffer8, 212, 131, 40);
            if (this.nz[this.eZ] <= 20) {
                return;
            }
            if (this.nk.ho[3] > 0) {
                int[] iArr10 = this.nk.ho;
                iArr10[3] = iArr10[3] - 1;
                this.nw.setColor(16777215);
                this.nw.e(0, 163, this.gn, 20);
            } else if (this.nk.ho[3] == 0) {
                this.nk.ho[3] = -4;
            } else if (this.nk.ho[4] > 0) {
                int[] iArr11 = this.nk.ho;
                iArr11[4] = iArr11[4] - 1;
                this.nw.setColor(16777215);
                this.nw.e(0, 170, this.gn, 4);
                if (i2 <= 1) {
                    this.oU = 0;
                } else if (i2 > 1 && i2 <= 3) {
                    this.oU = 1;
                } else if (i2 > 3 && i2 <= 4) {
                    this.oU = 2;
                } else if (i2 >= 5) {
                    this.oU = 3;
                }
                for (int i20 = 0; i20 < this.om[this.oU].length; i20++) {
                }
            } else {
                int i21 = 0;
                while (true) {
                    int i22 = i21;
                    if (i22 < this.om[this.oU].length) {
                        byte b3 = this.om[this.oU][i22];
                        int i23 = (this.nq + bArr[this.om[this.oU].length][i22]) - 15;
                        ak akVar82 = this.nw;
                        ak akVar83 = this.nw;
                        c(10, b3, i23, 194, 3);
                        i21 = i22 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final void f(int i) {
        al a = a(new String[][]{new String[]{"", "0g.date", "1g.date", "2g.date", "3g.date", "4g.date"}, new String[]{"", "d1.date", "d2.date", "d3.date", "d4.date", "d5.date"}}[i][this.nk.ga[i][5]], a(this.ol[i], new StringBuffer().append("/data/").append(i).append(".png").toString(), 7));
        this.nH[i] = new al[1];
        this.nH[i][0] = a;
    }

    public final void f(int i, int i2) {
        String[] strArr = {"击败华雄", "击败董卓", "击败袁绍", "击退夏侯惇", "击退曹操", "击退曹仁", "撤出麦城", "击败孙权", "击败曹操", "击退曹仁"};
        String[] strArr2 = {"华雄", "董卓", "袁绍", "夏侯", "曹操", "曹仁", "吕蒙", "孙权", "曹操", "曹仁"};
        aL();
        int i3 = 0;
        while (true) {
            int i4 = i3;
            if (i4 >= 5) {
                break;
            }
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 29, i4 * 37, this.np - 30, 36);
            i3 = i4 + 1;
        }
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 27, 185, this.np - 30, 36, 2);
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        c(13, 70, 0, this.np - 40, 36, 2);
        int i5 = (i != 5 || this.nk.cA) ? i : 9;
        int i6 = 0;
        while (true) {
            int i7 = i6;
            if (i7 >= g.fP[i5].length) {
                break;
            }
            e((g.fP[i5][i7][0] + this.nk.eE) - 2, (g.fP[i5][i7][1] + 50) - 2, (g.fP[i5][i7][2] + 4) - 1, (g.fP[i5][i7][3] + 4) - 1, 5395018, 3);
            i6 = i7 + 1;
        }
        int i8 = 0;
        while (true) {
            int i9 = i8;
            if (i9 >= g.fP[i5].length) {
                break;
            }
            e((g.fP[i5][i9][0] + this.nk.eE) - 2, (g.fP[i5][i9][1] + 50) - 2, (g.fP[i5][i9][2] + 4) - 1, (g.fP[i5][i9][3] + 4) - 1, 14601934, 1);
            i8 = i9 + 1;
        }
        int i10 = g.fP[i5][i2][0] + this.nk.eE;
        int i11 = g.fP[i5][i2][1] + 50;
        short s = g.fP[i5][i2][2];
        short s2 = g.fP[i5][i2][3];
        e(i10 - 2, i11 - 2, (s + 4) - 1, (s2 + 4) - 1, 10240518, 3);
        e(i10 - 2, i11 - 2, (s + 4) - 1, (s2 + 4) - 1, 15772968, 1);
        for (int i12 = 0; i12 < g.lJ[i5].length; i12++) {
            int i13 = g.lJ[i5][i12][0] + this.nk.eE;
            short s3 = g.lJ[i5][i12][2];
            short s4 = g.lJ[i5][i12][3];
            this.nw.setColor(0);
            this.nw.e(i13 - 1, (g.lJ[i5][i12][1] + 50) - 1, s3 + 2, s4 + 2);
        }
        for (int i14 = 0; i14 < g.fP[i5].length; i14++) {
            int i15 = g.fP[i5][i14][0] + this.nk.eE;
            int i16 = g.fP[i5][i14][1] + 50;
            short s5 = g.fP[i5][i14][2];
            short s6 = g.fP[i5][i14][3];
            this.nw.setColor(1133658);
            if (this.nk.dT[i14]) {
                this.nw.setColor(0);
            }
            if (i2 == i14) {
                this.nw.setColor(2036060);
            }
            this.nw.e(i15 + 1, i16 + 1, s5 - 3, s6 - 3);
        }
        this.nw.setColor(14601934);
        this.nw.d(20, DirectGraphics.ROTATE_180, 10, 10);
        this.nw.d(20, Player.REALIZED, 10, 10);
        this.nw.setColor(1133658);
        this.nw.e(21, 201, 9, 9);
        this.nw.setColor(15772968);
        this.nw.d(20, 220, 10, 10);
        this.nw.setColor(2036060);
        this.nw.e(21, 221, 9, 9);
        this.nw.setColor(16777215);
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        ak akVar9 = this.nw;
        akVar7.b("未探索区域", 40, DirectGraphics.ROTATE_180, 20);
        ak akVar10 = this.nw;
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        akVar10.b("已探索区域", 40, Player.REALIZED, 20);
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        ak akVar15 = this.nw;
        akVar13.b("当前所在区域", 40, 221, 20);
        if ((this.nz[this.ij] / 2) % 5 == 0) {
            this.nw.setColor(this.eW);
        }
        for (int i17 = 0; i17 < g.fP[i5].length; i17++) {
            if (i17 == g.fP[i5].length - 1) {
                ak akVar16 = this.nw;
                ak akVar17 = this.nw;
                this.nw.b(strArr2[i5], g.fP[i5][i17][0] + 10 + this.nk.eE, ((g.fP[i5][i17][1] + (g.fP[i5][i17][3] >> 1)) + 50) - 5, 20);
            }
        }
        this.nw.setColor(this.eW);
        ak akVar18 = this.nw;
        ak akVar19 = this.nw;
        this.nw.b("本关任务：", 10, this.np - 50, 36);
        ak akVar20 = this.nw;
        ak akVar21 = this.nw;
        this.nw.b(strArr[i5], 100, this.np - 50, 36);
        a("", "cancel");
    }

    public final void g(int i, int i2) {
        DirectGraphics a = DirectUtils.a(this.oS.bf());
        int i3 = (i2 == 0 ? this.nk.eA[i] : this.nk.eB[i]) + 7;
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 < this.nk.kk[this.nk.dY].length) {
                int i6 = this.oN[this.nk.kk[this.nk.dY][i5][4]];
                al alVar = this.nH[i3][this.nk.kk[this.nk.dY][i5][3]];
                short s = this.nk.kk[this.nk.dY][i5][1];
                short s2 = this.nk.kk[this.nk.dY][i5][2];
                ak akVar = this.nw;
                ak akVar2 = this.nw;
                a.a(alVar, s, s2, 20, i6);
                i4 = i5 + 1;
            } else {
                return;
            }
        }
    }

    public final void h(int i, int i2) {
        String[] strArr = {"击败华雄", "击败董卓", "击败袁绍", "击退夏侯惇", "击退曹操", "击退曹仁", "从麦城成功撤退", "击败孙权", "击败曹操"};
        int[][] iArr = {new int[]{0, 1, 0}, new int[]{0, 0, 1}, new int[]{1, 0, 0}};
        c(0, 0);
        int i3 = this.nq - 20;
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 18, i3, 30, 17);
        int i4 = this.nq + 20;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 18, i4, 30, 17, 2);
        this.nw.setColor(this.eW);
        ak akVar5 = this.nw;
        int i5 = this.nq;
        ak akVar6 = this.nw;
        ak akVar7 = this.nw;
        akVar5.b("关卡介绍", i5, 34, 17);
        this.nw.f(0, 60, 240, 55);
        if (i2 == 0) {
            ak akVar8 = this.nw;
            String str = this.oC[0];
            int i6 = this.nq;
            ak akVar9 = this.nw;
            ak akVar10 = this.nw;
            akVar8.b(str, i6, 60, 17);
        } else {
            String str2 = this.oC[i2];
            a(this.nw, str2, 35, 140 - (this.nz[this.ij] % (str2.length() * 4)), 170, this.eW);
        }
        this.nw.f(0, 0, this.gn, this.np);
        int i7 = this.nq - 20;
        ak akVar11 = this.nw;
        ak akVar12 = this.nw;
        c(13, 18, i7, 115, 17);
        int i8 = this.nq + 20;
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        c(13, 18, i8, 115, 17, 2);
        ak akVar15 = this.nw;
        int i9 = this.nq;
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        akVar15.b("任务", i9, 119, 17);
        ak akVar18 = this.nw;
        String stringBuffer = new StringBuffer().append("").append(strArr[i2]).toString();
        int i10 = this.nq;
        ak akVar19 = this.nw;
        ak akVar20 = this.nw;
        akVar18.b(stringBuffer, i10, 149, 17);
        a(this.nq, 190, "武器", 1, iArr[i][0], 0);
        a(this.nq, 220, "饰品", 1, iArr[i][1], 0);
        a(this.nq, 250, "出战", 1, iArr[i][2], 0);
        a("ok", "cancel");
    }

    public final void j(int i) {
        int[][] iArr = {new int[]{80, 0, 0}, new int[]{DirectGraphics.ROTATE_180, 0, 100}, new int[]{Player.PREFETCHED, 1, Player.STARTED}, new int[]{600, 3, 700}, new int[]{2000, 10, 1500}, new int[]{80, 0, 0}, new int[]{Player.REALIZED, 1, 0}, new int[]{350, 3, 100}, new int[]{700, 6, Player.STARTED}, new int[]{1500, 20, 700}};
        c(0, 0);
        for (int i2 = 0; i2 < 4; i2++) {
            this.nw.setColor(this.eW);
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, 18, this.nq - 60, (i2 * 60) + 10, 17);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, 18, this.nq - 20, (i2 * 60) + 10, 17, 2);
            ak akVar5 = this.nw;
            ak akVar6 = this.nw;
            c(14, i2 + (this.nk.dp.kH * 5), this.nq - 70, (i2 * 60) + 36, 17);
            ak akVar7 = this.nw;
            ak akVar8 = this.nw;
            this.nw.b(new StringBuffer().append("攻击+").append(iArr[(this.nk.dp.kH * 5) + i2][0]).toString(), this.nq - 50, (i2 * 60) + 35, 20);
            ak akVar9 = this.nw;
            ak akVar10 = this.nw;
            this.nw.b(new StringBuffer().append("重击+").append(iArr[(this.nk.dp.kH * 5) + i2][1]).append("%").toString(), this.nq + 35, (i2 * 60) + 35, 20);
            ak akVar11 = this.nw;
            ak akVar12 = this.nw;
            this.nw.b(new StringBuffer().append("生命+").append(iArr[(this.nk.dp.kH * 5) + i2][2]).toString(), this.nq - 50, (i2 * 60) + 52, 20);
        }
        int i3 = this.nq - 60;
        ak akVar13 = this.nw;
        ak akVar14 = this.nw;
        c(13, 18, i3, 250, 17);
        int i4 = this.nq - 20;
        ak akVar15 = this.nw;
        ak akVar16 = this.nw;
        c(13, 18, i4, 250, 17, 2);
        int i5 = (this.nk.dp.kH * 5) + 4;
        int i6 = this.nq - 80;
        ak akVar17 = this.nw;
        ak akVar18 = this.nw;
        c(14, i5, i6, 276, 17);
        ak akVar19 = this.nw;
        ak akVar20 = this.nw;
        this.nw.b(new StringBuffer().append("攻击+").append(iArr[(this.nk.dp.kH * 5) + 4][0]).toString(), this.nq - 60, 275, 20);
        ak akVar21 = this.nw;
        ak akVar22 = this.nw;
        this.nw.b(new StringBuffer().append("重击+").append(iArr[(this.nk.dp.kH * 5) + 4][1]).append("%").toString(), this.nq + 25, 275, 20);
        ak akVar23 = this.nw;
        ak akVar24 = this.nw;
        this.nw.b(new StringBuffer().append("生命+").append(iArr[(this.nk.dp.kH * 5) + 4][2]).toString(), this.nq - 60, 292, 20);
        if ((this.nz[this.ij] / 2) % 3 == 0) {
            ak akVar25 = this.nw;
            ak akVar26 = this.nw;
            c(13, 17, this.nq - 60, (i * 60) + 10, 17);
            ak akVar27 = this.nw;
            ak akVar28 = this.nw;
            c(13, 17, this.nq - 20, (i * 60) + 10, 17, 2);
        }
        for (int i7 = 0; i7 < 5; i7++) {
            if (this.nk.ib[i7 + 67 + (this.nk.dC * 5)] == 0) {
                this.nw.setColor(10592416);
            } else {
                this.nw.setColor(this.eW);
            }
            ak akVar29 = this.nw;
            ak akVar30 = this.nw;
            this.nw.b(this.oW[this.nk.dp.kH][i7], this.nq - 40, (i7 * 60) + 14, 17);
        }
        a("ok", "cancel");
        if (this.hB <= 240) {
            this.hB += 10;
        } else {
            this.hB = 0;
        }
        this.nw.f(this.hB, 250, 20, 60);
        this.nw.setColor(16777215);
        ak akVar31 = this.nw;
        ak akVar32 = this.nw;
        this.nw.b(this.oW[this.nk.dp.kH][4], this.nq - 40, 254, 17);
        ak akVar33 = this.nw;
        ak akVar34 = this.nw;
        this.nw.b(new StringBuffer().append("攻击+").append(iArr[(this.nk.dp.kH * 5) + 4][0]).toString(), this.nq - 60, 275, 20);
        ak akVar35 = this.nw;
        ak akVar36 = this.nw;
        this.nw.b(new StringBuffer().append("重击+").append(iArr[(this.nk.dp.kH * 5) + 4][1]).append("%").toString(), this.nq + 25, 275, 20);
        ak akVar37 = this.nw;
        ak akVar38 = this.nw;
        this.nw.b(new StringBuffer().append("生命+").append(iArr[(this.nk.dp.kH * 5) + 4][2]).toString(), this.nq - 60, 292, 20);
        this.nw.f(0, 0, this.gn, this.np);
    }

    public final void k(int i) {
        if (!this.nJ[i]) {
            this.nH[i] = null;
            al[] a = a(a(this.ol[i], new StringBuffer().append("/data/").append(i).append(".png").toString(), 7), this.ol[i]);
            if (i == 17 || i == 7 || i == 8 || i == 9 || i == 21 || i == 20 || i == 18 || i == 12 || i == 16 || i == 22) {
                this.nH[i] = a;
            } else {
                this.nH[i] = new al[1];
                this.nH[i][0] = a[0];
            }
            this.nJ[i] = true;
        }
    }

    public final void o(int i) {
        c(2, 0);
        this.nw.setColor(this.eW);
        ak akVar = this.nw;
        int i2 = this.nq;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.b("清除存档开始新游戏？", i2, 90, 33);
        a(this.nq - 30, 173, "是", 1, i == 0 ? 1 : 0, 0);
        a(this.nq + 30, 173, "否", 1, i == 1 ? 1 : 0, 0);
        a("ok", "cancel");
    }

    public final void r(int i) {
        if (this.nw != null && this.nz[this.eZ] - 1 <= i) {
            this.nI = null;
            this.nt = null;
            aL();
            this.nw.setColor(16777215);
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            this.nw.b(new StringBuffer().append("数据载入中").append(((this.nz[this.eZ] - 1) * 100) / i).append("%").toString(), this.nq, this.np - 45, 33);
            this.nw.setColor(16718336);
            this.nw.e(this.nq - 30, this.np - 33, ((this.nz[this.eZ] - 1) * 60) / i, 4);
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            this.nw.a(this.nH[17][4], this.nq, this.np - 40, 17);
        }
    }

    public final void run() {
        while (!this.oe) {
            try {
                this.nn = 60;
                long currentTimeMillis = System.currentTimeMillis();
                if (this.nk.gz > 0) {
                    this.nn = 150;
                }
                this.nk.c();
                aS();
                aT();
                long currentTimeMillis2 = System.currentTimeMillis() - currentTimeMillis;
                if (currentTimeMillis2 < ((long) this.nn)) {
                    Thread.sleep(((long) this.nn) - currentTimeMillis2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        this.nk.ab();
        GameMIDlet gameMIDlet = this.nl;
        GameMIDlet.au();
    }

    public final void s(int i) {
        c(0, 0);
        this.nw.setColor(16768515);
        int i2 = this.nq - 20;
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 17, i2, 20, 17);
        int i3 = this.nq + 20;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 17, i3, 20, 17, 2);
        ak akVar5 = this.nw;
        int i4 = this.nq;
        ak akVar6 = this.nw;
        ak akVar7 = this.nw;
        akVar5.b("人物选择", i4, 22, 17);
        int i5 = this.nq - 40;
        ak akVar8 = this.nw;
        ak akVar9 = this.nw;
        c(13, 21, i5, 56, 17);
        int i6 = this.nq - 40;
        ak akVar10 = this.nw;
        ak akVar11 = this.nw;
        c(13, 0, i6, 60, 17);
        int i7 = this.nq + 40;
        ak akVar12 = this.nw;
        ak akVar13 = this.nw;
        c(13, 21, i7, 56, 17);
        int i8 = this.nq + 40;
        ak akVar14 = this.nw;
        ak akVar15 = this.nw;
        c(13, 2, i8, 60, 17);
        if (this.nk.iE == 0) {
            a(this.nq - 40, 100, "关羽", 1, this.nk.dC == 0 ? 1 : 0, 0);
            a(this.nq + 40, 100, "貂婵", 1, this.nk.dC, 0);
        } else if (this.nk.dC == 0) {
            a(this.nq - 40, 100, "关羽", 1, 2, 0);
            a(this.nq + 40, 100, "貂婵", 1, 0, 0);
        } else {
            a(this.nq - 40, 100, "关羽", 1, 0, 0);
            a(this.nq + 40, 100, "貂婵", 1, 2, 0);
        }
        int i9 = this.nq - 20;
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        c(13, 17, i9, 140, 17);
        int i10 = this.nq + 20;
        ak akVar18 = this.nw;
        ak akVar19 = this.nw;
        c(13, 17, i10, 140, 17, 2);
        this.nw.setColor(16768515);
        ak akVar20 = this.nw;
        int i11 = this.nq;
        ak akVar21 = this.nw;
        ak akVar22 = this.nw;
        akVar20.b("关卡选择", i11, 142, 17);
        ak akVar23 = this.nw;
        al alVar = this.nH[12][i];
        ak akVar24 = this.nw;
        ak akVar25 = this.nw;
        akVar23.a(alVar, 76, 175, 20);
        c(71, 170, 2, 1);
        int[][] iArr = {new int[]{40, Player.REALIZED, 110}, new int[]{40, Player.REALIZED, 280}};
        int i12 = (this.nz[this.eZ] / 3) % 2;
        if (this.nk.iE == 0) {
            int i13 = iArr[this.nk.iE][1] + i12;
            int i14 = iArr[this.nk.iE][2];
            ak akVar26 = this.nw;
            ak akVar27 = this.nw;
            c(13, 20, i13, i14, 3);
            int i15 = iArr[this.nk.iE][0] - i12;
            int i16 = iArr[this.nk.iE][2];
            ak akVar28 = this.nw;
            ak akVar29 = this.nw;
            c(13, 20, i15, i16, 3, 2);
        } else {
            if (i > 0) {
                int i17 = iArr[this.nk.iE][0] - i12;
                int i18 = iArr[this.nk.iE][2];
                ak akVar30 = this.nw;
                ak akVar31 = this.nw;
                c(13, 20, i17, i18, 3, 2);
            }
            if (i < 8) {
                int i19 = iArr[this.nk.iE][1] + i12;
                int i20 = iArr[this.nk.iE][2];
                ak akVar32 = this.nw;
                ak akVar33 = this.nw;
                c(13, 20, i19, i20, 3);
            }
        }
        this.nw.f(65, 250, 110, 22);
        if (i > this.nk.ga[this.nk.dC][11]) {
            this.nw.setColor(9671314);
        }
        if (this.nk.iF == 1) {
            if (this.oY <= 120) {
                this.oY += 35;
            } else {
                this.oY = 0;
                this.nk.iF = 0;
            }
        }
        if (this.nk.iF == 2) {
            if (this.oY >= -120) {
                this.oY -= 35;
            } else {
                this.oY = 0;
                this.nk.iF = 0;
            }
        }
        int i21 = 0;
        while (true) {
            int i22 = i21;
            if (i22 >= 3) {
                break;
            }
            a(this.oY + ((i22 * 120) - 35), 250, this.nG[i], 3, 0, 0);
            i21 = i22 + 1;
        }
        this.nw.f(0, 0, this.gn, this.np);
        this.nw.f(63, 280, 100, 10);
        int i23 = 0;
        while (true) {
            int i24 = i23;
            if (i24 >= 9) {
                break;
            }
            ak akVar34 = this.nw;
            ak akVar35 = this.nw;
            c(13, 89, (i24 * 10) + 74, 280, 20);
            i23 = i24 + 1;
        }
        int i25 = 0;
        while (true) {
            int i26 = i25;
            if (i26 > this.nk.ga[this.nk.dC][11]) {
                break;
            }
            ak akVar36 = this.nw;
            ak akVar37 = this.nw;
            c(13, 88, (i26 * 10) + 74, 280, 20);
            i25 = i26 + 1;
        }
        if (this.nk.iE == 1) {
            if (this.nk.iJ) {
                this.ba = (this.nz[this.eZ] / 3) % 2 == 0 ? 90 : 88;
            } else {
                this.ba = (this.nz[this.eZ] / 3) % 2 == 0 ? 90 : 89;
            }
            ak akVar38 = this.nw;
            ak akVar39 = this.nw;
            c(13, this.ba, (i * 10) + 74, 280, 20);
        } else {
            ak akVar40 = this.nw;
            ak akVar41 = this.nw;
            c(13, 90, (i * 10) + 74, 280, 20);
        }
        this.nw.f(0, 0, this.gn, this.np);
        if (this.nk.iG) {
            a(20, 110, 5, "");
            this.nw.setColor(this.eW);
            ak akVar42 = this.nw;
            int i27 = this.nq;
            ak akVar43 = this.nw;
            ak akVar44 = this.nw;
            akVar42.b("请选择关卡难度", i27, 140, 33);
            a(this.nq - 70, (int) DirectGraphics.ROTATE_180, "普通模式", 2, this.nk.iH == 0 ? 1 : 0, 0);
            a(this.nq + 35, (int) DirectGraphics.ROTATE_180, "猛将模式", 2, this.nk.iH == 1 ? 1 : 0, 0);
        }
        a("ok", "cancel");
    }

    public final void t(int i) {
        if (this.nw != null) {
            this.ns = null;
            if (i == 0) {
                c(1, 160);
            } else {
                c(0, 0);
            }
            switch (i) {
                case 0:
                    this.nw.f(0, 65, MIDIControl.CONTROL_CHANGE, 175);
                    if (this.nk.da == 1) {
                        this.nk.cZ -= 5;
                        for (int i2 = 0; i2 < this.nD.length; i2++) {
                            short[] sArr = this.nD;
                            sArr[i2] = (short) (sArr[i2] - 5);
                            if (this.nD[i2] <= 40) {
                                this.nD[i2] = 265;
                            }
                        }
                        if (this.nk.cZ <= -25) {
                            this.nk.cZ = 0;
                            this.nk.da = 0;
                        }
                    } else if (this.nk.da == 2) {
                        this.nk.cZ += 5;
                        for (int i3 = 0; i3 < this.nD.length; i3++) {
                            short[] sArr2 = this.nD;
                            sArr2[i3] = (short) (sArr2[i3] + 5);
                            if (this.nD[i3] >= 290) {
                                this.nD[i3] = 65;
                            }
                        }
                        if (this.nk.cZ >= 25) {
                            this.nk.cZ = 0;
                            this.nk.da = 0;
                        }
                    }
                    int i4 = 0;
                    while (true) {
                        int i5 = i4;
                        if (i5 < this.nB.length) {
                            int i6 = (this.gn >> 1) - 20;
                            short s = this.nD[i5];
                            ak akVar = this.nw;
                            ak akVar2 = this.nw;
                            c(13, 18, i6, s, 17);
                            int i7 = (this.gn >> 1) + 20;
                            short s2 = this.nD[i5];
                            ak akVar3 = this.nw;
                            ak akVar4 = this.nw;
                            c(13, 18, i7, s2, 17, 2);
                            i4 = i5 + 1;
                        } else {
                            int i8 = (this.gn >> 1) - 20;
                            int i9 = this.nk.cZ + 140;
                            ak akVar5 = this.nw;
                            ak akVar6 = this.nw;
                            c(13, 17, i8, i9, 17);
                            int i10 = (this.gn >> 1) + 20;
                            int i11 = this.nk.cZ + 140;
                            ak akVar7 = this.nw;
                            ak akVar8 = this.nw;
                            c(13, 17, i10, i11, 17, 2);
                            for (int i12 = 0; i12 < this.nB.length; i12++) {
                                this.nw.setColor(this.eW);
                                ak akVar9 = this.nw;
                                ak akVar10 = this.nw;
                                this.nw.b(this.nB[i12], this.gn >> 1, this.nD[i12] + 4, 17);
                            }
                            ak akVar11 = this.nw;
                            ak akVar12 = this.nw;
                            c(13, 72, 71, 136, 20);
                            ak akVar13 = this.nw;
                            ak akVar14 = this.nw;
                            c(13, 72, 170, 136, 24, 2);
                            ak akVar15 = this.nw;
                            ak akVar16 = this.nw;
                            c(13, 72, 71, 150, 20, 1);
                            ak akVar17 = this.nw;
                            ak akVar18 = this.nw;
                            c(13, 72, 170, 150, 24, 3);
                            if (this.hB <= 240) {
                                this.hB += 5;
                            } else {
                                this.hB = 0;
                            }
                            if (this.nD[5] > 64 && this.nD[5] < 220) {
                                this.nw.f(this.hB, this.nD[5] + 4, 10, 20);
                                this.nw.setColor(16777215);
                                ak akVar19 = this.nw;
                                ak akVar20 = this.nw;
                                this.nw.b(this.nB[5], this.gn >> 1, this.nD[5] + 4, 17);
                            }
                            this.nw.f(0, 0, this.gn, this.np);
                            a("ok", "cancel");
                            return;
                        }
                    }
                case 1:
                    this.nw.setColor(this.eW);
                    int i13 = this.nq;
                    ak akVar21 = this.nw;
                    ak akVar22 = this.nw;
                    c(13, 18, i13, 45, 40);
                    int i14 = this.nq;
                    ak akVar23 = this.nw;
                    ak akVar24 = this.nw;
                    c(13, 18, i14, 45, 36, 2);
                    ak akVar25 = this.nw;
                    int i15 = this.nq;
                    ak akVar26 = this.nw;
                    ak akVar27 = this.nw;
                    akVar25.b("设 置", i15, 41, 33);
                    ak akVar28 = this.nw;
                    ak akVar29 = this.nw;
                    c(13, 18, 70, 90, 40);
                    ak akVar30 = this.nw;
                    ak akVar31 = this.nw;
                    c(13, 18, 70, 90, 36, 2);
                    ak akVar32 = this.nw;
                    ak akVar33 = this.nw;
                    ak akVar34 = this.nw;
                    akVar32.b("声音", 70, 86, 33);
                    ak akVar35 = this.nw;
                    String str = new String[]{"关", "小", "大"}[this.nk.aN];
                    ak akVar36 = this.nw;
                    ak akVar37 = this.nw;
                    akVar35.b(str, 150, 86, 36);
                    this.nw.setColor(16777215);
                    ak akVar38 = this.nw;
                    int i16 = this.nq;
                    ak akVar39 = this.nw;
                    ak akVar40 = this.nw;
                    akVar38.b("左右方向键调节音量", i16, 155, 33);
                    a("", "cancel");
                    return;
                case 2:
                    byte[] bArr = {45, 44, 43, 41, 42, 41, 43, 44};
                    int i17 = (this.nz[this.eZ] / 3) % 2;
                    int i18 = this.nq + 55 + i17;
                    ak akVar41 = this.nw;
                    ak akVar42 = this.nw;
                    c(13, 20, i18, 35, 6);
                    int i19 = (this.nq - 55) - i17;
                    ak akVar43 = this.nw;
                    ak akVar44 = this.nw;
                    c(13, 20, i19, 35, 10, 2);
                    this.nw.setColor(this.eW);
                    int i20 = this.nq;
                    ak akVar45 = this.nw;
                    ak akVar46 = this.nw;
                    c(13, 18, i20, 45, 40);
                    int i21 = this.nq;
                    ak akVar47 = this.nw;
                    ak akVar48 = this.nw;
                    c(13, 18, i21, 45, 36, 2);
                    ak akVar49 = this.nw;
                    String str2 = g.lQ[this.nk.de][0];
                    int i22 = this.nq;
                    ak akVar50 = this.nw;
                    ak akVar51 = this.nw;
                    akVar49.b(str2, i22, 41, 33);
                    ak akVar52 = this.nw;
                    String str3 = g.lQ[this.nk.de][1];
                    int i23 = this.gn - 40;
                    int i24 = this.np;
                    a(akVar52, str3, 20, 50, i23, this.eW);
                    ak akVar53 = this.nw;
                    ak akVar54 = this.nw;
                    this.nw.b(new StringBuffer().append(this.nk.de + 1).append("/").append(this.nk.df + 1).toString(), this.nq, this.np - 5, 33);
                    a("", "cancel");
                    return;
                case 3:
                    String[] strArr = {"3", "3", "2", "2"};
                    if (!this.nk.di) {
                        int[][] iArr = {new int[]{1, 0, 0, 0}, new int[]{0, 1, 0, 0}, new int[]{0, 0, 1, 0}, new int[]{0, 0, 0, 1}};
                        this.nw.setColor(this.eW);
                        a(this.nq - 50, 50, g.lR[0][0], 4, iArr[this.nk.dg][0], 0);
                        a(this.nq - 50, 90, g.lR[1][0], 4, iArr[this.nk.dg][1], 0);
                        a(this.nq - 50, 130, g.lR[2][0], 4, iArr[this.nk.dg][2], 0);
                        a(this.nq - 50, 170, g.lR[3][0], 4, iArr[this.nk.dg][3], 0);
                        this.nw.setColor(16777215);
                        ak akVar55 = this.nw;
                        int i25 = this.nq;
                        ak akVar56 = this.nw;
                        ak akVar57 = this.nw;
                        akVar55.b("按5键察看", i25, 210, 17);
                    } else {
                        this.nw.setColor(this.eW);
                        int i26 = (this.nz[this.eZ] / 3) % 2;
                        int i27 = this.nq + 80 + i26;
                        ak akVar58 = this.nw;
                        ak akVar59 = this.nw;
                        c(13, 20, i27, 40, 6);
                        int i28 = (this.nq - 75) - i26;
                        ak akVar60 = this.nw;
                        ak akVar61 = this.nw;
                        c(13, 20, i28, 40, 10, 2);
                        if (this.nk.dg == 0) {
                            a(this.nq - 50, 30, g.lR[0][0], 4, 0, 0);
                            ak akVar62 = this.nw;
                            String str4 = g.lR[this.nk.dg][this.nk.dh];
                            int i29 = this.gn - 40;
                            int i30 = this.np;
                            a(akVar62, str4, 20, 60, i29, this.eW);
                        } else if (this.nk.dg == 1) {
                            a(this.nq - 50, 30, g.lR[1][0], 4, 0, 0);
                            ak akVar63 = this.nw;
                            String str5 = g.lR[this.nk.dg][this.nk.dh];
                            int i31 = this.gn - 40;
                            int i32 = this.np;
                            a(akVar63, str5, 20, 60, i31, this.eW);
                        } else if (this.nk.dg == 2) {
                            a(this.nq - 50, 30, g.lR[2][0], 4, 0, 0);
                            ak akVar64 = this.nw;
                            String str6 = g.lR[this.nk.dg][this.nk.dh];
                            int i33 = this.gn - 40;
                            int i34 = this.np;
                            a(akVar64, str6, 20, 60, i33, this.eW);
                        } else if (this.nk.dg == 3) {
                            a(this.nq - 50, 30, g.lR[3][0], 4, 0, 0);
                            ak akVar65 = this.nw;
                            String str7 = g.lR[this.nk.dg][this.nk.dh];
                            int i35 = this.gn - 40;
                            int i36 = this.np;
                            a(akVar65, str7, 20, 60, i35, this.eW);
                        }
                        ak akVar66 = this.nw;
                        ak akVar67 = this.nw;
                        this.nw.b(new StringBuffer().append(this.nk.dh).append("/").append(strArr[this.nk.dg]).toString(), this.nq, this.np - 5, 33);
                    }
                    a("", "cancel");
                    return;
                case 4:
                    String[] strArr2 = {"开发商：", "华娱无线", "客服电话：", "010-88901665", "客服邮箱：", "geekan@imy.cn"};
                    this.nw.setColor(this.eW);
                    for (int i37 = 0; i37 < strArr2.length; i37++) {
                        ak akVar68 = this.nw;
                        ak akVar69 = this.nw;
                        this.nw.b(strArr2[i37], 20, (i37 * 25) + 25, 20);
                    }
                    a("", "return");
                    return;
                case 5:
                    String[] strArr3 = {"伍长", "中郎将", "大将军", "武神"};
                    int[] iArr2 = {10, 4};
                    this.nw.setColor(this.eW);
                    int i38 = this.nq + 3;
                    ak akVar70 = this.nw;
                    ak akVar71 = this.nw;
                    c(13, 18, i38, 33, 40);
                    int i39 = this.nq + 3;
                    ak akVar72 = this.nw;
                    ak akVar73 = this.nw;
                    c(13, 18, i39, 33, 36, 2);
                    ak akVar74 = this.nw;
                    String str8 = this.nk.eT == 1 ? "连击排行" : "关卡评价";
                    ak akVar75 = this.nw;
                    ak akVar76 = this.nw;
                    akVar74.b(str8, this.nq + 3, 31, 33);
                    if (!this.nk.bt) {
                        int i40 = (this.nz[this.eZ] / 3) % 2;
                        int i41 = this.nq + 60 + i40;
                        ak akVar77 = this.nw;
                        ak akVar78 = this.nw;
                        c(13, 20, i41, 22, 6);
                        int i42 = (this.nq - 55) - i40;
                        ak akVar79 = this.nw;
                        ak akVar80 = this.nw;
                        c(13, 20, i42, 23, 10, 2);
                    }
                    for (int i43 = 1; i43 < iArr2[this.nk.eT]; i43++) {
                        ak akVar81 = this.nw;
                        ak akVar82 = this.nw;
                        this.nw.b(new StringBuffer().append("").append(i43).toString(), 40, (i43 * 20) + 43, 33);
                    }
                    if (this.nk.eT == 0) {
                        for (int i44 = 0; i44 < 9; i44++) {
                            ak akVar83 = this.nw;
                            ak akVar84 = this.nw;
                            this.nw.b(this.nG[i44], 60, (i44 * 20) + 60, 36);
                            ak akVar85 = this.nw;
                            ak akVar86 = this.nw;
                            this.nw.b(new StringBuffer().append("").append(strArr3[this.nk.fS[i44]]).toString(), 160, (i44 * 20) + 60, 36);
                        }
                        ak akVar87 = this.nw;
                        int i45 = this.nq;
                        ak akVar88 = this.nw;
                        ak akVar89 = this.nw;
                        akVar87.b("全关卡武神评价", i45, 260, 17);
                        ak akVar90 = this.nw;
                        int i46 = this.nq;
                        ak akVar91 = this.nw;
                        ak akVar92 = this.nw;
                        akVar90.b("将开启无限无双!", i46, 280, 17);
                        if (this.nk.bt) {
                            if ((this.nz[this.eZ] / 3) % 2 == 0) {
                                this.nw.setColor(16777215);
                            }
                            ak akVar93 = this.nw;
                            ak akVar94 = this.nw;
                            this.nw.b(this.nG[this.nk.dO], 60, (this.nk.dO * 20) + 60, 36);
                            ak akVar95 = this.nw;
                            ak akVar96 = this.nw;
                            this.nw.b(new StringBuffer().append("").append(strArr3[this.nk.fS[this.nk.dO]]).toString(), 160, (this.nk.dO * 20) + 60, 36);
                            if (this.nz[this.eZ] >= 20 && this.nz[this.eZ] <= 22) {
                                this.nw.setColor(16777215);
                                this.nw.e(0, (this.nk.dO * 20) + 40 + 8, this.gn, 4);
                            } else if (this.nz[this.eZ] == 23) {
                                this.nw.setColor(16777215);
                                this.nw.e(0, (this.nk.dO * 20) + 40 + 6, this.gn, 8);
                            }
                        }
                    }
                    if (this.nk.eT == 1) {
                        for (int i47 = 0; i47 < 3; i47++) {
                            ak akVar97 = this.nw;
                            ak akVar98 = this.nw;
                            this.nw.b(new StringBuffer().append("").append(this.nk.fS[i47 + 9]).toString(), 80, (i47 * 20) + 60, 36);
                            ak akVar99 = this.nw;
                            ak akVar100 = this.nw;
                            this.nw.b("连斩", 120, (i47 * 20) + 60, 36);
                        }
                    }
                    a("", "return");
                    return;
                default:
                    return;
            }
        }
    }

    public final void u(int i) {
        c(2, 0);
        int i2 = i - 11;
        this.nw.setColor(this.eW);
        int i3 = 0;
        while (i3 < this.nF.length) {
            int i4 = i3 != i2 ? 18 : 17;
            ak akVar = this.nw;
            ak akVar2 = this.nw;
            c(13, i4, this.nq, (i3 * 28) + 50, 24);
            int i5 = i3 != i2 ? 18 : 17;
            ak akVar3 = this.nw;
            ak akVar4 = this.nw;
            c(13, i5, this.nq, (i3 * 28) + 50, 20, 2);
            ak akVar5 = this.nw;
            ak akVar6 = this.nw;
            this.nw.b(this.nF[i3], this.nq, (i3 * 28) + 70, 65);
            i3++;
        }
        if (this.hB <= 240) {
            this.hB += 5;
        } else {
            this.hB = 0;
        }
        this.nw.f(this.hB, 85, 10, 20);
        this.nw.setColor(16777215);
        ak akVar7 = this.nw;
        ak akVar8 = this.nw;
        this.nw.b(this.nF[1], this.gn >> 1, 98, 65);
        this.nw.f(0, 0, this.gn, this.np);
        a("ok", "");
    }

    public final void v(int i) {
        int[] iArr = {10, 11, 12};
        int[][] iArr2 = {new int[]{1, 0, 0, 0}, new int[]{0, 1, 0, 0}, new int[]{0, 0, 1, 0}, new int[]{0, 0, 0, 1}};
        byte[] bArr = {45, 44, 43, 41, 42, 41, 43, 44};
        int[] iArr3 = {this.nk.ga[this.nk.dp.kH][2], this.nk.ga[this.nk.dp.kH][3], this.nk.ga[this.nk.dp.kH][4], this.nk.ga[this.nk.dp.kH][5], this.nk.ga[this.nk.dp.kH][17], this.nk.ga[this.nk.dp.kH][18], this.nk.ga[this.nk.dp.kH][19], this.nk.ga[this.nk.dp.kH][20], this.nk.ga[this.nk.dp.kH][1]};
        short[][] sArr = {new short[]{80, 0, 0, 180, 0, 100, 300, 1, 400, 600, 3, 700, 2000, 10, 1500}, new short[]{80, 0, 0, 200, 1, 0, 350, 3, 100, 700, 6, 400, 1500, 20, 700}};
        this.nw.setColor(this.eW);
        byte[][] bArr2 = {new byte[]{0, 1, 2, 3, 4, 5, -1, -1}, new byte[]{-1, -1, 5, 4, 3, 2, 1, 0}};
        int i2 = this.nk.eQ;
        int i3 = this.nk.eR;
        int i4 = this.nk.eQ;
        if (this.nk.eQ == -1) {
            int i5 = this.nk.eR;
            int i6 = this.nk.eQ;
        } else {
            int i7 = this.nk.eR;
            int i8 = this.nk.eQ;
        }
        c(0, 0);
        int i9 = (this.gn >> 1) - 20;
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 18, i9, 20, 17);
        int i10 = (this.gn >> 1) + 20;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 18, i10, 20, 17, 2);
        this.nw.setColor(16768515);
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        this.nw.b("人物升级", this.gn >> 1, 23, 17);
        ak akVar7 = this.nw;
        String stringBuffer = new StringBuffer().append("经验：").append(this.nk.hW ? "----" : new StringBuffer().append(iArr3[8]).append("").toString()).toString();
        ak akVar8 = this.nw;
        ak akVar9 = this.nw;
        akVar7.b(stringBuffer, 22, 65, 36);
        if ((i != 3 || (iArr3[3] <= 3 && this.nk.ga[this.nk.dp.kH][12] < 20)) && iArr3[i] <= 19) {
            ak akVar10 = this.nw;
            String stringBuffer2 = new StringBuffer().append("升级：").append(g.lP[i][iArr3[i]] - iArr3[i + 4]).toString();
            int i11 = this.nq;
            ak akVar11 = this.nw;
            ak akVar12 = this.nw;
            akVar10.b(stringBuffer2, i11, 65, 36);
        } else {
            ak akVar13 = this.nw;
            int i12 = this.nq;
            ak akVar14 = this.nw;
            ak akVar15 = this.nw;
            akVar13.b("升级：----", i12, 65, 36);
        }
        a(65, 90, "生  命", 2, iArr2[i][0], 4);
        a(145, 90, "攻  击", 2, iArr2[i][1], 4);
        a(65, 120, "重  击", 2, iArr2[i][2], 4);
        a(145, 120, "武  器", 2, iArr2[i][3], 4);
        this.nw.setColor(65280);
        this.nw.e(82, 71, (iArr3[i + 4] * 71) / g.lP[i][iArr3[i]], 5);
        ak akVar16 = this.nw;
        ak akVar17 = this.nw;
        c(13, 63, 168, 70, 24);
        ak akVar18 = this.nw;
        ak akVar19 = this.nw;
        c(13, 63, 72, 70, 20, 2);
        int i13 = 0;
        while (true) {
            int i14 = i13;
            if (i14 >= 2) {
                break;
            }
            ak akVar20 = this.nw;
            ak akVar21 = this.nw;
            c(13, 61, (i14 * 34) + 86, 70, 20, 1);
            ak akVar22 = this.nw;
            ak akVar23 = this.nw;
            c(13, 61, (i14 * 34) + 86, 77, 20);
            i13 = i14 + 1;
        }
        String[] strArr = {"一", "二", "三", "四", "顶"};
        this.nw.setColor(this.eW);
        ak akVar24 = this.nw;
        String str = this.oW[this.nk.dp.kH][iArr3[3] - 1];
        ak akVar25 = this.nw;
        ak akVar26 = this.nw;
        akVar24.b(str, 80, 192, 17);
        int i15 = (this.nk.dp.kH == 0 ? 0 : 5) + (iArr3[3] - 1);
        ak akVar27 = this.nw;
        ak akVar28 = this.nw;
        c(14, i15, 62, 156, 20);
        if (i == 0) {
            ak akVar29 = this.nw;
            String stringBuffer3 = new StringBuffer().append("生命：").append(this.nk.dp.kG).toString();
            ak akVar30 = this.nw;
            ak akVar31 = this.nw;
            akVar29.b(stringBuffer3, 120, 156, 20);
            ak akVar32 = this.nw;
            String stringBuffer4 = new StringBuffer().append("等级：").append(iArr3[0]).append("级").toString();
            ak akVar33 = this.nw;
            ak akVar34 = this.nw;
            akVar32.b(stringBuffer4, 120, 177, 20);
        } else if (i == 1) {
            ak akVar35 = this.nw;
            String stringBuffer5 = new StringBuffer().append("攻击：").append(this.nk.dp.hP).toString();
            ak akVar36 = this.nw;
            ak akVar37 = this.nw;
            akVar35.b(stringBuffer5, 120, 156, 20);
            ak akVar38 = this.nw;
            String stringBuffer6 = new StringBuffer().append("等级：").append(iArr3[1]).append("级").toString();
            ak akVar39 = this.nw;
            ak akVar40 = this.nw;
            akVar38.b(stringBuffer6, 120, 177, 20);
        } else if (i == 2) {
            ak akVar41 = this.nw;
            String stringBuffer7 = new StringBuffer().append("重击：").append(this.nk.dp.ij).append("%").toString();
            ak akVar42 = this.nw;
            ak akVar43 = this.nw;
            akVar41.b(stringBuffer7, 120, 156, 20);
            ak akVar44 = this.nw;
            String stringBuffer8 = new StringBuffer().append("等级：").append(iArr3[2]).append("级").toString();
            ak akVar45 = this.nw;
            ak akVar46 = this.nw;
            akVar44.b(stringBuffer8, 120, 177, 20);
        } else if (i == 3) {
            ak akVar47 = this.nw;
            String stringBuffer9 = new StringBuffer().append("攻击+").append((int) sArr[this.nk.dp.kH][(iArr3[3] * 3) - 3]).toString();
            ak akVar48 = this.nw;
            ak akVar49 = this.nw;
            akVar47.b(stringBuffer9, 120, 156, 20);
            ak akVar50 = this.nw;
            String stringBuffer10 = new StringBuffer().append("重击+").append((int) sArr[this.nk.dp.kH][(iArr3[3] * 3) - 2]).append("%").toString();
            ak akVar51 = this.nw;
            ak akVar52 = this.nw;
            akVar50.b(stringBuffer10, 120, 175, 20);
            ak akVar53 = this.nw;
            String stringBuffer11 = new StringBuffer().append("生命+").append((int) sArr[this.nk.dp.kH][(iArr3[3] * 3) - 1]).toString();
            ak akVar54 = this.nw;
            ak akVar55 = this.nw;
            akVar53.b(stringBuffer11, 120, 191, 20);
            ak akVar56 = this.nw;
            String stringBuffer12 = new StringBuffer().append(strArr[iArr3[3] - 1]).append("").toString();
            ak akVar57 = this.nw;
            ak akVar58 = this.nw;
            akVar56.b(stringBuffer12, 50, 156, 17);
            ak akVar59 = this.nw;
            ak akVar60 = this.nw;
            ak akVar61 = this.nw;
            akVar59.b("级", 50, 172, 17);
        }
        if (this.nk.dn != 3) {
            int i16 = iArr[this.nk.dn];
            ak akVar62 = this.nw;
            ak akVar63 = this.nw;
            c(14, i16, 63, 225, 20);
            ak akVar64 = this.nw;
            String str2 = this.oA[this.nk.dn][0];
            ak akVar65 = this.nw;
            ak akVar66 = this.nw;
            akVar64.b(str2, 100, 220, 20);
        } else {
            this.nw.setColor(this.eW);
            ak akVar67 = this.nw;
            ak akVar68 = this.nw;
            ak akVar69 = this.nw;
            akVar67.b("饰品未装备", 68, 225, 20);
        }
        if (!this.nk.hW) {
            this.nw.setColor(16777215);
            if ((this.nz[this.eZ] / 5) % 2 == 0) {
                ak akVar70 = this.nw;
                ak akVar71 = this.nw;
                this.nw.b("按5键升级相应选项", this.nq - 10, 260, 17);
            }
        }
        a("", "cancel");
    }

    public final void w() {
        Runtime.getRuntime().gc();
        Thread.yield();
    }

    public final void w(int i) {
        String[] strArr = {"攻击符", "重击符", "春秋", "攻击之书", "重击之书", "生命之书"};
        int[] iArr = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        int i2 = this.nk.dp.kH;
        strArr[2] = "春秋";
        c(0, 0);
        int i3 = (this.gn >> 1) - 20;
        ak akVar = this.nw;
        ak akVar2 = this.nw;
        c(13, 18, i3, 20, 17);
        int i4 = (this.gn >> 1) + 20;
        ak akVar3 = this.nw;
        ak akVar4 = this.nw;
        c(13, 18, i4, 20, 17, 2);
        this.nw.setColor(16768515);
        ak akVar5 = this.nw;
        ak akVar6 = this.nw;
        this.nw.b("物品收集", this.gn >> 1, 23, 17);
        int i5 = 0;
        while (true) {
            int i6 = i5;
            if (i6 >= strArr.length) {
                break;
            }
            a(70, (i6 * 30) + 60, strArr[i6], 2, i6 == i ? 1 : 0, 0);
            String valueOf = String.valueOf(this.nk.ga[this.nk.dp.kH][i6 < 2 ? i6 + 9 : i6 + 10]);
            if (i6 > 1) {
                valueOf = new StringBuffer().append(valueOf).append("/20").toString();
            }
            ak akVar7 = this.nw;
            ak akVar8 = this.nw;
            this.nw.b(valueOf, 150, (i6 * 30) + 60, 20);
            i5 = i6 + 1;
        }
        this.nw.setColor(16777215);
        if ((this.nz[this.eZ] / 5) % 2 == 0) {
            ak akVar9 = this.nw;
            int i7 = this.nq;
            ak akVar10 = this.nw;
            ak akVar11 = this.nw;
            akVar9.b("按5键查看详细信息", i7, 260, 17);
        }
        a("", "cancel");
    }

    public final void x(int i) {
        a(20, 110, 5, "");
        this.nw.setColor(this.eW);
        ak akVar = this.nw;
        int i2 = this.nq;
        ak akVar2 = this.nw;
        ak akVar3 = this.nw;
        akVar.b("是否追击吕布?", i2, 140, 33);
        a(this.nq - 40, (int) DirectGraphics.ROTATE_180, "否", 1, i == 0 ? 1 : 0, 0);
        a(this.nq + 40, (int) DirectGraphics.ROTATE_180, "是", 1, i == 1 ? 1 : 0, 0);
    }

    public final void y(int i) {
        this.nk.i(A(i));
    }

    public final void z(int i) {
        this.nk.j(A(i));
    }
}
