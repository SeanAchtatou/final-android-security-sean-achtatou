package defpackage;

import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;

/* renamed from: bp  reason: default package */
final class bp implements View.OnTouchListener {
    final /* synthetic */ bm qA;

    /* synthetic */ bp(bm bmVar) {
        this(bmVar, (byte) 0);
    }

    private bp(bm bmVar, byte b) {
        this.qA = bmVar;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!bm.qy.isEmpty()) {
            this.qA.qt.onTouchEvent(motionEvent);
        }
        if (!bm.qz.isEmpty()) {
            Iterator it = bm.qz.iterator();
            while (it.hasNext()) {
                ((bq) it.next()).a(motionEvent);
            }
        }
        int action = motionEvent.getAction();
        int i = action & 255;
        int i2 = action >> 8;
        if (i == 6) {
            return bm.g(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
        }
        if (i == 5) {
            return bm.g(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
        }
        int pointerCount = motionEvent.getPointerCount();
        boolean z = false;
        for (int i3 = 0; i3 < pointerCount; i3++) {
            if (bm.g(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                z = true;
            }
        }
        return z;
    }
}
