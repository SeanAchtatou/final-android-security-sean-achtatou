package defpackage;

/* renamed from: cl  reason: default package */
final class cl implements Runnable {
    final /* synthetic */ cm ro;

    cl(cm cmVar) {
        this.ro = cmVar;
    }

    public final void run() {
        if (cj.rl.getParent() == null) {
            cd.getActivity().setContentView(cj.rl);
        }
        if (cj.rk != null) {
            cj.rk.ba();
        }
        if (cj.rk == null || cj.rk.getView() != this.ro.getView()) {
            cj.rl.removeAllViews();
            cj.rl.addView(this.ro.getView(), cj.rn);
            cj.rl.invalidate();
        }
        cm unused = cj.rk = this.ro;
        cj.rk.aU();
        bw.b(bw.a((int) cj.MSG_VIEW_CHANGED, cj.rk.getView()));
    }
}
