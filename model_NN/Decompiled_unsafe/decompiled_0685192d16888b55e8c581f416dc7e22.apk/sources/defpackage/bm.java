package defpackage;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: bm  reason: default package */
public final class bm extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final bm qs = new bm();
    private static final LinkedHashSet qv = new LinkedHashSet();
    private static final LinkedHashSet qw = new LinkedHashSet();
    private static final LinkedHashSet qx = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static final LinkedHashSet qy = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static final LinkedHashSet qz = new LinkedHashSet();
    /* access modifiers changed from: private */
    public GestureDetector qt;
    private SensorManager qu;

    public static final void a(SensorEventListener sensorEventListener) {
        qs.qu.registerListener(sensorEventListener, qs.qu.getDefaultSensor(1), 3);
    }

    protected static void a(View view) {
        View.OnTouchListener onTouchListener;
        if (!cj.rm) {
            view.setOnKeyListener(qs);
        }
        if (cd.bK() >= 5) {
            bm bmVar = qs;
            bmVar.getClass();
            onTouchListener = new bp(bmVar);
        } else {
            onTouchListener = qs;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void a(bn bnVar) {
        qv.add(bnVar);
    }

    public static final void a(bo boVar) {
        qw.add(boVar);
    }

    public static final void a(bq bqVar) {
        qz.add(bqVar);
    }

    public static final void a(bs bsVar) {
        qx.add(bsVar);
    }

    protected static void b(Activity activity) {
        qs.qt = new GestureDetector(activity, qs);
        qs.qu = (SensorManager) activity.getSystemService("sensor");
    }

    public static final void b(SensorEventListener sensorEventListener) {
        qs.qu.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final void b(bo boVar) {
        qw.remove(boVar);
    }

    public static final boolean g(int i, int i2, int i3, int i4) {
        Iterator it = qw.iterator();
        while (it.hasNext()) {
            if (((bo) it.next()).g(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        qy.clear();
        qw.clear();
        qx.clear();
        qv.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (qx.isEmpty()) {
            return false;
        }
        Iterator it = qx.iterator();
        while (it.hasNext()) {
            it.next();
        }
        return false;
    }

    public final boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator it = qy.iterator();
        while (it.hasNext()) {
            if (((br) it.next()).bx()) {
                return true;
            }
        }
        return false;
    }

    public final boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (qv.isEmpty()) {
            return false;
        }
        Iterator it = qv.iterator();
        while (it.hasNext()) {
            ((bn) it.next()).a(keyEvent);
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!qy.isEmpty()) {
            this.qt.onTouchEvent(motionEvent);
        }
        if (!qz.isEmpty()) {
            Iterator it = qz.iterator();
            while (it.hasNext()) {
                ((bq) it.next()).a(motionEvent);
            }
        }
        return g(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
