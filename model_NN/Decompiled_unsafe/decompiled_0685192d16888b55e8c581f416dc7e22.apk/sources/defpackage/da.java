package defpackage;

import android.os.Message;
import java.io.InterruptedIOException;

/* renamed from: da  reason: default package */
public final class da implements ay, bx {
    public static final int MSG_GCF_SMS_CONNECTION_RECEIVE = 15391747;
    public static final int MSG_GCF_SMS_CONNECTION_SEND = 15391744;
    public static final int MSG_GCF_SMS_CONNECTION_SEND_COMPLETE = 15391745;
    public static final int MSG_GCF_SMS_CONNECTION_SEND_FAIL = 15391746;
    private static final int NOT_SENT = 0;
    private static final int SENT_FAIL = 2;
    private static final int SENT_OK = 1;
    private String ma;
    private int sr = 0;
    private az ss;
    private ax st;

    public da(String str) {
        bw.a((int) MSG_GCF_SMS_CONNECTION_SEND, "MSG_GCF_SMS_CONNECTION_SEND");
        bw.a((int) MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, "MSG_GCF_SMS_CONNECTION_SEND_COMPLETE");
        bw.a((int) MSG_GCF_SMS_CONNECTION_SEND_FAIL, "MSG_GCF_SMS_CONNECTION_SEND_FAIL");
        bw.a((int) MSG_GCF_SMS_CONNECTION_RECEIVE, "MSG_GCF_SMS_CONNECTION_RECEIVE");
        this.ma = str;
        bw.a(this);
    }

    public final void a(ax axVar) {
        bw.a(this);
        bw.b(bw.a((int) MSG_GCF_SMS_CONNECTION_SEND, axVar));
        synchronized (this) {
            try {
                this.sr = 0;
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        bw.b(this);
        if (this.sr != 1) {
            throw new InterruptedIOException("Fail to send.");
        }
        return;
    }

    public final boolean a(Message message) {
        if (message.what == 15391745) {
            this.sr = 1;
            synchronized (this) {
                notifyAll();
            }
            return true;
        } else if (message.what == 15391746) {
            this.sr = 2;
            synchronized (this) {
                notifyAll();
            }
            return true;
        } else if (message.what != 15391747) {
            return false;
        } else {
            this.st = (ax) message.obj;
            if (this.ss != null) {
                az azVar = this.ss;
            }
            synchronized (this) {
                notifyAll();
            }
            return true;
        }
    }

    public final void close() {
        bw.b(this);
    }

    public final ax v(String str) {
        ax ddVar;
        if (str.equals(ay.TEXT_MESSAGE)) {
            ddVar = new dc();
        } else if (str.equals(ay.BINARY_MESSAGE)) {
            ddVar = new db();
        } else if (str.equals(ay.MULTIPART_MESSAGE)) {
            ddVar = new dd();
        } else {
            throw new IllegalArgumentException(str);
        }
        if (this.ma != null) {
            ddVar.setAddress(this.ma);
        }
        return ddVar;
    }
}
