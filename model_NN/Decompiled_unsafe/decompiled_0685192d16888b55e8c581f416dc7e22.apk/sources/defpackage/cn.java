package defpackage;

import android.util.Log;

/* renamed from: cn  reason: default package */
public final class cn {
    public static final String LOG_TAG = "VDManager";
    public static cq rp;

    protected static cq H(String str) {
        try {
            cq cqVar = (cq) Class.forName(str).newInstance();
            rp = cqVar;
            cqVar.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        return rp;
    }
}
