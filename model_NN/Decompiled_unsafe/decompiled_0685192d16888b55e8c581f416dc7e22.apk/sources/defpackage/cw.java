package defpackage;

import android.graphics.Typeface;
import java.util.HashMap;

/* renamed from: cw  reason: default package */
public final class cw {
    public static int SIZE_LARGE = 16;
    public static int SIZE_MEDIUM = 14;
    public static int SIZE_SMALL = 12;
    private static final HashMap rX = new HashMap();

    public static int a(aj ajVar, char c) {
        cv b = b(ajVar);
        b.rW[0] = c;
        return (int) b.pe.measureText(b.rW, 0, 1);
    }

    public static cv b(aj ajVar) {
        cv cvVar = (cv) rX.get(ajVar);
        if (cvVar != null) {
            return cvVar;
        }
        Typeface typeface = Typeface.SANS_SERIF;
        if (ajVar.bc() == 0) {
            typeface = Typeface.SANS_SERIF;
        } else if (ajVar.bc() == 32) {
            typeface = Typeface.MONOSPACE;
        } else if (ajVar.bc() == 64) {
            typeface = Typeface.SANS_SERIF;
        }
        int i = (ajVar.getStyle() & 0) != 0 ? 0 : 0;
        if ((ajVar.getStyle() & 1) != 0) {
            i = 1;
        }
        if ((ajVar.getStyle() & 2) != 0) {
            i |= 2;
        }
        cv cvVar2 = new cv(Typeface.create(typeface, i), ajVar.getSize() == 8 ? SIZE_SMALL : ajVar.getSize() == 0 ? SIZE_MEDIUM : ajVar.getSize() == 16 ? SIZE_LARGE : 0, (ajVar.getStyle() & 4) != 0);
        rX.put(ajVar, cvVar2);
        return cvVar2;
    }

    public static int c(aj ajVar) {
        cv b = b(ajVar);
        return b.pe.getFontMetricsInt(b.rV);
    }
}
