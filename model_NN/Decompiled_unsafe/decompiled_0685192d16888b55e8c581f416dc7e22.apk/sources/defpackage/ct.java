package defpackage;

import android.app.AlertDialog;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ct  reason: default package */
public final class ct implements Runnable {
    final /* synthetic */ MIDPDevice rO;
    final /* synthetic */ AlertDialog.Builder rP;

    public ct(MIDPDevice mIDPDevice, AlertDialog.Builder builder) {
        this.rO = mIDPDevice;
        this.rP = builder;
    }

    public final void run() {
        this.rP.create().show();
    }
}
