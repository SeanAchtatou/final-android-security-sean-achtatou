package defpackage;

import android.app.AlertDialog;
import com.androidbox.sgmj2net8.R;

/* renamed from: ca  reason: default package */
final class ca implements cc {
    /* synthetic */ ca() {
        this((byte) 0);
    }

    private ca(byte b) {
    }

    public final String aW() {
        return cd.getString(R.string.about);
    }

    public final void aY() {
        AlertDialog.Builder builder = new AlertDialog.Builder(cd.getActivity());
        builder.setTitle((int) R.string.about);
        builder.setMessage((int) R.string.about_dialog);
        builder.setNegativeButton((int) R.string.close, new cb(this));
        builder.create().show();
    }

    public final int getId() {
        return by.OPTION_MENU_ITEM_ABOUT;
    }
}
