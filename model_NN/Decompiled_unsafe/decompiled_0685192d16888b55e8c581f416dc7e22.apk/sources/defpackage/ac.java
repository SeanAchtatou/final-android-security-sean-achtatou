package defpackage;

import android.view.View;
import android.widget.Button;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ac  reason: default package */
public final class ac implements View.OnClickListener, cc {
    public static final int BACK = 2;
    public static final int CANCEL = 3;
    public static final int EXIT = 7;
    public static final int HELP = 5;
    public static final int ITEM = 8;
    public static final int OK = 4;
    public static final int SCREEN = 1;
    public static final int STOP = 6;
    private String label;
    private int pp = 4;
    private Button pq = new Button(cd.getActivity());
    private int priority = 0;

    public ac(String str, int i, int i2) {
        this.label = str;
        this.pq.setText(str);
        this.pq.setOnClickListener(this);
    }

    public final String aW() {
        return this.label;
    }

    public final Button aX() {
        return this.pq;
    }

    public final void aY() {
        bw.b(bw.a((int) MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }

    public final int getId() {
        return this.priority;
    }

    public final void onClick(View view) {
        bw.b(bw.a((int) MIDPDevice.MSG_MIDP_COMMAND_EVENT, this));
    }
}
