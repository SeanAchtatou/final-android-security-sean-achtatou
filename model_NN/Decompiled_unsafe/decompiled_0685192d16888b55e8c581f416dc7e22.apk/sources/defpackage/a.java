package defpackage;

/* renamed from: a  reason: default package */
public final class a implements Runnable {
    private b aC = null;
    private String aD;
    private String aE;
    private String aF;
    private Thread aG = null;
    private String b;
    private boolean f = false;

    public a() {
        Class.forName("ax");
        this.aC = null;
        if (this.aG == null) {
            this.aG = new Thread(this);
            this.aG.start();
        }
    }

    public a(b bVar) {
        Class.forName("ax");
        this.aC = bVar;
        if (this.aG == null) {
            this.aG = new Thread(this);
            this.aG.start();
        }
    }

    public final synchronized void a(String str) {
        this.aD = str;
    }

    public final synchronized boolean a(String str, String str2, String str3) {
        boolean z;
        if (!this.f) {
            this.aE = str;
            this.aF = str2;
            this.f = true;
            this.b = null;
            notify();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b9 A[SYNTHETIC, Splitter:B:50:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c0 A[Catch:{ IOException -> 0x00cb, Exception -> 0x00ee, all -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00dc A[SYNTHETIC, Splitter:B:63:0x00dc] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x00e3 A[Catch:{ IOException -> 0x00cb, Exception -> 0x00ee, all -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x00df A[EDGE_INSN: B:83:0x00df->B:65:0x00df ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r6 = this;
            r4 = 3
        L_0x0001:
            monitor-enter(r6)
            boolean r0 = r6.f     // Catch:{ all -> 0x007c }
            if (r0 != 0) goto L_0x0009
            r6.wait()     // Catch:{ Exception -> 0x0102 }
        L_0x0009:
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            int r0 = r0.length()     // Catch:{ all -> 0x007c }
            if (r0 < r4) goto L_0x007f
            java.lang.String r1 = r6.aE     // Catch:{ all -> 0x007c }
            r2 = 0
            r3 = 3
            java.lang.String r1 = r1.substring(r2, r3)     // Catch:{ all -> 0x007c }
            java.lang.String r2 = "+86"
            if (r1 != r2) goto L_0x007f
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            r1 = 3
            java.lang.String r0 = r0.substring(r1)     // Catch:{ all -> 0x007c }
        L_0x0026:
            r1 = 0
            java.lang.String r2 = r6.b     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            if (r2 == 0) goto L_0x0098
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r2.<init>()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r3 = r6.aD     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r2 = ":"
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r2 = r6.b     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.StringBuffer r0 = r0.append(r2)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r2 = r0
        L_0x004b:
            p r0 = defpackage.r.q(r2)     // Catch:{ Exception -> 0x00ad }
            ay r0 = (defpackage.ay) r0     // Catch:{ Exception -> 0x00ad }
            java.lang.String r1 = "text"
            ax r1 = r0.v(r1)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            bb r1 = (defpackage.bb) r1     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.setAddress(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            java.lang.String r2 = r6.aF     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r1.w(r2)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            r0.a(r1)     // Catch:{ Exception -> 0x010a, all -> 0x0105 }
            if (r0 == 0) goto L_0x0069
            r0.close()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x0069:
            b r0 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            if (r0 == 0) goto L_0x0077
            b r0 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r1 = r6.aF     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r1 = r6.b     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r1 = 1
            r0.b(r1)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x0077:
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x007c }
        L_0x007a:
            monitor-exit(r6)     // Catch:{ all -> 0x007c }
            goto L_0x0001
        L_0x007c:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x007f:
            if (r0 <= 0) goto L_0x0095
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            r1 = 0
            r2 = 1
            java.lang.String r0 = r0.substring(r1, r2)     // Catch:{ all -> 0x007c }
            java.lang.String r1 = "+"
            if (r0 != r1) goto L_0x0095
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            r1 = 1
            java.lang.String r0 = r0.substring(r1)     // Catch:{ all -> 0x007c }
            goto L_0x0026
        L_0x0095:
            java.lang.String r0 = r6.aE     // Catch:{ all -> 0x007c }
            goto L_0x0026
        L_0x0098:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r2.<init>()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r3 = r6.aD     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.StringBuffer r2 = r2.append(r3)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.StringBuffer r0 = r2.append(r0)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r2 = r0
            goto L_0x004b
        L_0x00ad:
            r0 = move-exception
        L_0x00ae:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x00d9 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00d9 }
            r2.println(r0)     // Catch:{ all -> 0x00d9 }
            if (r1 == 0) goto L_0x00bc
            r1.close()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x00bc:
            b r0 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            if (r0 == 0) goto L_0x0077
            b r0 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r1 = r6.aF     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r1 = r6.b     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r1 = 0
            r0.b(r1)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            goto L_0x0077
        L_0x00cb:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ all -> 0x00fd }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00fd }
            r1.println(r0)     // Catch:{ all -> 0x00fd }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x007c }
            goto L_0x007a
        L_0x00d9:
            r0 = move-exception
        L_0x00da:
            if (r1 == 0) goto L_0x00df
            r1.close()     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x00df:
            b r1 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            if (r1 == 0) goto L_0x00ed
            b r1 = r6.aC     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r2 = r6.aF     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            java.lang.String r2 = r6.b     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
            r2 = 0
            r1.b(r2)     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x00ed:
            throw r0     // Catch:{ IOException -> 0x00cb, Exception -> 0x00ee }
        L_0x00ee:
            r0 = move-exception
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ all -> 0x00fd }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00fd }
            r1.println(r0)     // Catch:{ all -> 0x00fd }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x007c }
            goto L_0x007a
        L_0x00fd:
            r0 = move-exception
            r1 = 0
            r6.f = r1     // Catch:{ all -> 0x007c }
            throw r0     // Catch:{ all -> 0x007c }
        L_0x0102:
            r0 = move-exception
            goto L_0x0009
        L_0x0105:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00da
        L_0x010a:
            r1 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x00ae
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a.run():void");
    }
}
