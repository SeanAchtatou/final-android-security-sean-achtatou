package defpackage;

import android.util.Log;
import java.util.HashMap;

/* renamed from: bc  reason: default package */
public final class bc {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static be qe;
    private static final HashMap qf = new HashMap();

    protected static void bq() {
        bw.a((int) MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        bw.a((int) MSG_APPLICATION_START, "MSG_APPLICATION_START");
        bw.a((int) MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        bw.a((int) MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        bw.a((int) MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        bw.a(new bd());
    }

    public static void f(String str, String str2) {
        qf.put(str, str2);
    }

    protected static void onDestroy() {
        if (qe != null && qe.getState() != 3) {
            qe.onDestroy();
            bw.b(bw.a((int) MSG_APPLICATION_QUIT, qe));
        }
    }

    public static void pause() {
        if (qe == null) {
            return;
        }
        if (qe.getState() == 1) {
            qe.onPause();
            bw.b(bw.a((int) MSG_APPLICATION_PAUSE, qe));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + qe.getState());
    }

    public static void resume() {
        if (qe == null) {
            return;
        }
        if (qe.getState() == 2) {
            qe.onResume();
            bw.b(bw.a((int) MSG_APPLICATION_RESUME, qe));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + qe.getState());
    }

    public static void start() {
        if (qe == null) {
            return;
        }
        if (qe.getState() == 0) {
            qe.onStart();
            bw.b(bw.a((int) MSG_APPLICATION_START, qe));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + qe.getState());
    }

    public static void x(String str) {
        if (str != null) {
            try {
                qe = (be) Class.forName(str).newInstance();
                bw.b(bw.a((int) MSG_APPLICATION_LAUNCH, qe));
            } catch (Exception e) {
                Log.w(LOG_TAG, e.toString());
            }
            if (qe != null) {
                qe.bj();
            } else {
                Log.w(LOG_TAG, "The application failed to launch.");
            }
        } else {
            Log.w(LOG_TAG, "No available application could launch.");
        }
    }

    public static String y(String str) {
        return (String) qf.get(str);
    }
}
