package defpackage;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import java.io.IOException;

/* renamed from: al  reason: default package */
public final class al {
    private static final Matrix pT = new Matrix();
    public int height;
    private ak mV;
    public Bitmap pR;
    public boolean pS;
    public int width;

    private al(Bitmap bitmap) {
        this.pR = bitmap;
        if (bitmap != null) {
            this.width = bitmap.getWidth();
            this.height = bitmap.getHeight();
        }
    }

    public static al a(byte[] bArr, int i, int i2) {
        Bitmap c = bi.c(bArr, i, i2);
        if (c == null) {
            return null;
        }
        al alVar = new al(c);
        alVar.pS = false;
        return alVar;
    }

    public static al n(int i, int i2) {
        al alVar = new al(bi.a(500, 260, false, -16777216));
        alVar.pS = true;
        return alVar;
    }

    public static al r(String str) {
        Bitmap a = bi.a(cd.D(str));
        if (a == null) {
            throw new IOException();
        }
        al alVar = new al(a);
        alVar.pS = false;
        return alVar;
    }

    public final ak bf() {
        if (this.pS) {
            if (this.mV == null) {
                this.mV = new cx(this.pR);
            }
            this.mV.bd();
            return this.mV;
        }
        throw new IllegalStateException("Image is immutable");
    }

    public final int getHeight() {
        return this.height;
    }

    public final int getWidth() {
        return this.width;
    }

    public final boolean isMutable() {
        return this.pS;
    }
}
