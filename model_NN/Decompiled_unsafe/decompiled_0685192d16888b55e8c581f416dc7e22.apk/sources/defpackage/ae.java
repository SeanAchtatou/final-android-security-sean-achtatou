package defpackage;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ae  reason: default package */
public final class ae implements bx {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static ai pr = null;
    public static final ae ps = new ae();
    private static MIDlet pt;
    private Message pu = bw.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, bw.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean pv;
    private boolean pw = true;
    private boolean px;
    private MIDPDevice py;

    private ae() {
        bw.a(this);
    }

    public static ae a(MIDlet mIDlet) {
        if (mIDlet != null) {
            pt = mIDlet;
        }
        return ps;
    }

    public static void a(ac acVar) {
        if (pr != null) {
            ad adVar = pr.pG;
            ai aiVar = pr;
        }
    }

    public static void aT() {
        Thread.yield();
    }

    private static boolean aZ() {
        return pt.getState() == 1;
    }

    public final void B(int i) {
        if (!this.pv && pr != null && aZ()) {
            ai aiVar = pr;
        }
    }

    public final void a(ab abVar) {
        if (this.pw && aZ()) {
            this.pw = false;
            if (this.py == null) {
                this.py = (MIDPDevice) bg.qg;
            }
            try {
                abVar.b(this.py.bf());
            } catch (Exception e) {
                Log.w(LOG_TAG, "Exception in paint!" + e);
                e.printStackTrace();
            }
            if (aZ()) {
                if (this.py == null) {
                    this.py = (MIDPDevice) bg.qg;
                }
                this.py.bS();
            }
            this.pw = true;
        } else if (!this.px) {
            this.px = true;
            bw.N(MIDPDevice.MSG_MIDP_REPAINT_LATER);
        }
    }

    public final void a(ai aiVar) {
        long aQ;
        long aQ2;
        if (aiVar != pr && aiVar != null) {
            if (aiVar.aR() != 5) {
                if (pr != null && pr.pI) {
                    pr.pD = null;
                }
                pr = aiVar;
                aiVar.pD = this;
                this.pv = true;
                cj.a(pr);
                return;
            }
            z zVar = (z) aiVar;
            Activity activity = cd.getActivity();
            View view = zVar.getView();
            String str = zVar.pH;
            aa aO = zVar.aO();
            int aQ3 = zVar.aQ();
            AlertDialog create = new AlertDialog.Builder(activity).create();
            cd.getHandler().post(new af(this, str, create, view));
            if (aQ3 == -2) {
                return;
            }
            if (aO != aa.po && aO != aa.pl && aO != aa.pn) {
                Handler handler = cd.getHandler();
                ah ahVar = new ah(this, create);
                if (zVar.aQ() <= z.aP()) {
                    z.aP();
                    aQ2 = 2000;
                } else {
                    aQ2 = (long) zVar.aQ();
                }
                handler.postDelayed(ahVar, aQ2);
            } else if (zVar.aQ() != 0) {
                Handler handler2 = cd.getHandler();
                ag agVar = new ag(this, create);
                if (zVar.aQ() <= z.aP()) {
                    z.aP();
                    aQ = 2000;
                } else {
                    aQ = (long) zVar.aQ();
                }
                handler2.postDelayed(agVar, aQ);
            }
        }
    }

    public final boolean a(Message message) {
        if (message.what != 44036) {
            return false;
        }
        if (pr != null && pr.aR() == 0) {
            a((ab) pr);
        }
        this.px = false;
        return true;
    }

    public final synchronized void k(int i, int i2) {
        if (this.pv) {
            this.pv = false;
        }
        if (pr != null && aZ()) {
            try {
                ai aiVar = pr;
            } catch (Exception e) {
            }
        }
    }

    public final synchronized void l(int i, int i2) {
        if (!this.pv) {
            if (pr != null && aZ()) {
                try {
                    ai aiVar = pr;
                } catch (Exception e) {
                }
            }
        }
    }

    public final synchronized void m(int i, int i2) {
        if (!this.pv) {
            if (pr != null && aZ()) {
                try {
                    ai aiVar = pr;
                } catch (Exception e) {
                }
            }
        }
    }

    public final void y(int i) {
        if (this.pv) {
            this.pv = false;
        }
        if (pr != null && aZ()) {
            if (!pr.pF.isEmpty()) {
                co coVar = bg.qg;
                if (i == MIDPDevice.Q(1)) {
                    a((ac) pr.pF.get(0));
                } else if (pr.pF.size() > 1) {
                    co coVar2 = bg.qg;
                    if (i == MIDPDevice.Q(2)) {
                        a((ac) pr.pF.get(1));
                    }
                }
            }
            if (pr.aR() == 1) {
                ((ap) pr).o(0, i);
            }
            pr.y(i);
        }
    }

    public final void z(int i) {
        if (!this.pv && pr != null && aZ()) {
            if (pr.aR() == 1) {
                ((ap) pr).o(1, i);
            }
            pr.z(i);
        }
    }
}
