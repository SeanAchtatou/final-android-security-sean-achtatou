package defpackage;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.concurrent.ConcurrentLinkedQueue;

/* renamed from: bw  reason: default package */
public final class bw implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final ConcurrentLinkedQueue qS = new ConcurrentLinkedQueue();
    private static final LinkedHashSet qT = new LinkedHashSet();
    private static final bw qU = new bw();
    private static final HashMap qV = new HashMap();
    private static final HashSet qX = new HashSet();
    private boolean qW;

    public static void N(int i) {
        b(a(i, null, 0, 0));
    }

    public static Message a(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = 0;
        obtain.arg2 = i3;
        return obtain;
    }

    public static void a(int i, String str) {
        if (!qV.containsKey(Integer.valueOf(i))) {
            qV.put(Integer.valueOf(i), str);
        } else {
            Log.w(LOG_TAG, "The message id has already been registed by " + ((String) qV.get(Integer.valueOf(i))));
        }
    }

    public static void a(bx bxVar) {
        qT.add(bxVar);
    }

    public static void b(Message message) {
        if (!qS.contains(message)) {
            qS.add(message);
        }
    }

    public static void b(bx bxVar) {
        qX.add(bxVar);
    }

    protected static void bq() {
        new Thread(qU).start();
    }

    protected static void onDestroy() {
        qU.qW = true;
        qS.clear();
        qT.clear();
    }

    public final void run() {
        boolean z;
        while (!this.qW) {
            while (true) {
                Message message = (Message) qS.poll();
                if (message == null) {
                    break;
                }
                Iterator it = qT.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((bx) it.next()).a(message)) {
                            z = true;
                            Thread.yield();
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    Log.w(LOG_TAG, "There is a message[" + (qV.containsKey(Integer.valueOf(message.what)) ? (Serializable) qV.get(Integer.valueOf(message.what)) : Integer.valueOf(message.what)) + "] which no consumer could deal with it.");
                }
                if (message.arg2 != -13295) {
                    message.recycle();
                }
            }
            if (!qX.isEmpty()) {
                Iterator it2 = qX.iterator();
                while (it2.hasNext()) {
                    qT.remove((bx) it2.next());
                }
                qX.clear();
            }
            SystemClock.sleep(1);
        }
    }
}
