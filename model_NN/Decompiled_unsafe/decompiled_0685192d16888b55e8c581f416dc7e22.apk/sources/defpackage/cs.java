package defpackage;

import android.content.DialogInterface;
import java.util.HashMap;
import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: cs  reason: default package */
public final class cs implements DialogInterface.OnClickListener {
    final /* synthetic */ HashMap rM;
    final /* synthetic */ String[] rN;
    final /* synthetic */ MIDPDevice rO;

    public cs(MIDPDevice mIDPDevice, HashMap hashMap, String[] strArr) {
        this.rO = mIDPDevice;
        this.rM = hashMap;
        this.rN = strArr;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        bc.x((String) this.rM.get(this.rN[i]));
    }
}
