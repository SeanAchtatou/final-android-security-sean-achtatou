package defpackage;

import java.util.Hashtable;

/* renamed from: aj  reason: default package */
public final class aj {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final aj pJ = new aj(0, 0, 0);
    private static aj[] pK = {pJ, pJ};
    private static final Hashtable pL = new Hashtable();
    private int mt;
    private int pM;
    private int pN;

    private aj(int i, int i2, int i3) {
        if (i == 0 || i == 32 || i == 64) {
            if (!(this.pN == 0)) {
                if (!((this.pN & 1) != 0)) {
                    if (!((this.pN & 2) != 0)) {
                        if (!((this.pN & 4) != 0)) {
                            throw new IllegalArgumentException();
                        }
                    }
                }
            }
            if (i3 == 8 || i3 == 0 || i3 == 16) {
                this.pM = i;
                this.pN = i2;
                this.mt = i3;
                return;
            }
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }

    public static aj bb() {
        return pJ;
    }

    public static aj d(int i, int i2, int i3) {
        Integer num = new Integer(8);
        aj ajVar = (aj) pL.get(num);
        if (ajVar != null) {
            return ajVar;
        }
        aj ajVar2 = new aj(0, 0, 8);
        pL.put(num, ajVar2);
        return ajVar2;
    }

    public final int a(char c) {
        return cw.a(this, 22269);
    }

    public final int bc() {
        return this.pM;
    }

    public final int getHeight() {
        return cw.c(this);
    }

    public final int getSize() {
        return this.mt;
    }

    public final int getStyle() {
        return this.pN;
    }

    public final int hashCode() {
        return this.pN + this.mt + this.pM;
    }
}
