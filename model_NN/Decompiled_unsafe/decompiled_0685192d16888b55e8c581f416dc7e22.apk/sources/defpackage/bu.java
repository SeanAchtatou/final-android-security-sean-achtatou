package defpackage;

import android.os.Message;

/* renamed from: bu  reason: default package */
final class bu implements bx {
    bu() {
    }

    public final boolean a(Message message) {
        if (message.what == 47873) {
            for (bv bvVar : bt.qI.values()) {
                if (bvVar.qQ != null && bvVar.qQ.isPlaying() && bvVar.qN) {
                    bvVar.qQ.pause();
                }
            }
            int unused = bt.qL = bt.by();
            bt.K(bt.qF);
            return false;
        } else if (message.what != 47874) {
            return false;
        } else {
            bt.K(bt.qL);
            for (bv bvVar2 : bt.qI.values()) {
                if (bvVar2.qQ != null && bvVar2.qN && !bvVar2.qQ.isPlaying()) {
                    bvVar2.qQ.start();
                }
            }
            return false;
        }
    }
}
