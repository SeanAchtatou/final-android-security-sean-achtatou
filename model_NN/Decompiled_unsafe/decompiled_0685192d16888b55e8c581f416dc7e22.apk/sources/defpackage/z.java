package defpackage;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

/* renamed from: z  reason: default package */
public final class z extends am {
    public static final int FOREVER = -2;
    protected static Context mq = null;
    public static final ac pg = new ac("", 4, 0);
    private aa ph;
    private int pi;
    private LinearLayout pj;

    public static int aP() {
        return 2000;
    }

    public final aa aO() {
        return this.ph;
    }

    public final int aQ() {
        return this.pi;
    }

    public final int aR() {
        return 5;
    }

    public final View getView() {
        return this.pj;
    }
}
