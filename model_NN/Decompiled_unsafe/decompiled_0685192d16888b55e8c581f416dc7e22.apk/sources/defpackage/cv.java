package defpackage;

import android.graphics.Paint;
import android.graphics.Typeface;

/* renamed from: cv  reason: default package */
public final class cv {
    public final Paint pe = new Paint(1);
    public Paint.FontMetricsInt rV;
    final char[] rW = new char[1];

    public cv(Typeface typeface, int i, boolean z) {
        this.pe.setTypeface(typeface);
        this.pe.setTextSize((float) i);
        this.pe.setUnderlineText(z);
        this.rV = this.pe.getFontMetricsInt();
    }
}
