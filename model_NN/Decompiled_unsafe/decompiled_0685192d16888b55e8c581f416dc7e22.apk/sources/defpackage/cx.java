package defpackage;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Region;

/* renamed from: cx  reason: default package */
public final class cx extends ak {
    private static final DashPathEffect sb = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
    private Paint rY;
    private Paint rZ;
    public cv sa;
    public Canvas sc;
    private aj sd;
    private int se;
    private Matrix sf;
    private Bitmap sg;
    private int sh;
    private int si;
    private int sj;
    private int sk;

    private cx() {
        this.rY = new Paint();
        this.rZ = new Paint();
        this.se = 0;
        this.sf = new Matrix();
        this.sh = 0;
        this.si = 0;
        this.sj = 0;
        this.sk = 0;
        this.rY.setAntiAlias(true);
        this.rY.setStyle(Paint.Style.STROKE);
        this.rZ.setAntiAlias(true);
        this.rZ.setStyle(Paint.Style.FILL);
    }

    public cx(Bitmap bitmap) {
        this();
        this.sg = bitmap;
        a(new Canvas(bitmap));
    }

    public cx(Canvas canvas) {
        this();
        a(canvas);
    }

    private void a(Canvas canvas) {
        this.sc = canvas;
        a(aj.bb());
        translate(-this.pO, -this.pP);
        if (this.sg != null) {
            f(0, 0, this.sg.getWidth(), this.sg.getHeight());
        } else {
            f(0, 0, bg.qg.getWidth(), bg.qg.getHeight());
        }
    }

    public final void a(aj ajVar) {
        aj bb = ajVar == null ? aj.bb() : ajVar;
        this.sd = bb;
        this.sa = cw.b(bb);
    }

    public final void a(al alVar, int i, int i2, int i3) {
        if (alVar != null && this.sj > 0 && this.sk > 0) {
            int i4 = i3 == 0 ? 20 : i3;
            this.sc.drawBitmap(alVar.pR, (float) ((i4 & 8) != 0 ? i - alVar.width : (i4 & 1) != 0 ? i - (alVar.width / 2) : i), (float) ((i4 & 32) != 0 ? i2 - alVar.height : (i4 & 2) != 0 ? i2 - (alVar.height / 2) : i2), (Paint) null);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x014c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(defpackage.al r10, int r11, int r12, int r13, int r14, int r15, int r16, int r17, int r18) {
        /*
            r9 = this;
            if (r13 <= 0) goto L_0x0004
            if (r14 > 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            if (r10 != 0) goto L_0x000d
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            r0.<init>()
            throw r0
        L_0x000d:
            if (r11 < 0) goto L_0x0011
            if (r12 >= 0) goto L_0x0034
        L_0x0011:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Area out of Image: x"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r11)
            java.lang.String r2 = " y:"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r12)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0034:
            boolean r0 = r10.isMutable()
            if (r0 == 0) goto L_0x0048
            ak r0 = r10.bf()
            if (r0 != r9) goto L_0x0048
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Image is source and target"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            android.graphics.Matrix r0 = r9.sf
            r0.reset()
            switch(r15) {
                case 0: goto L_0x0058;
                case 1: goto L_0x00e3;
                case 2: goto L_0x00b8;
                case 3: goto L_0x0099;
                case 4: goto L_0x00fc;
                case 5: goto L_0x008a;
                case 6: goto L_0x00a9;
                case 7: goto L_0x00c9;
                default: goto L_0x0050;
            }
        L_0x0050:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Bad transform"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            int r0 = -r11
            int r1 = -r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
        L_0x005e:
            r4 = 0
            if (r18 != 0) goto L_0x017d
            r5 = 20
        L_0x0063:
            r6 = r5 & 64
            if (r6 == 0) goto L_0x0068
            r4 = 1
        L_0x0068:
            r6 = r5 & 16
            if (r6 == 0) goto L_0x0114
            r6 = r5 & 34
            if (r6 == 0) goto L_0x0178
        L_0x0070:
            r4 = 1
            r6 = r4
            r4 = r17
        L_0x0074:
            r7 = r5 & 4
            if (r7 == 0) goto L_0x0133
            r5 = r5 & 9
            if (r5 == 0) goto L_0x0174
        L_0x007c:
            r5 = 1
            r6 = r5
            r5 = r16
        L_0x0080:
            if (r6 == 0) goto L_0x014c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Bad Anchor"
            r0.<init>(r1)
            throw r0
        L_0x008a:
            android.graphics.Matrix r0 = r9.sf
            r1 = 1119092736(0x42b40000, float:90.0)
            r0.preRotate(r1)
            int r0 = r14 + r12
            int r1 = -r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x005e
        L_0x0099:
            android.graphics.Matrix r0 = r9.sf
            r1 = 1127481344(0x43340000, float:180.0)
            r0.preRotate(r1)
            int r0 = r13 + r11
            int r1 = r14 + r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x005e
        L_0x00a9:
            android.graphics.Matrix r0 = r9.sf
            r1 = 1132920832(0x43870000, float:270.0)
            r0.preRotate(r1)
            int r0 = -r12
            int r1 = r13 + r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x005e
        L_0x00b8:
            android.graphics.Matrix r0 = r9.sf
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            int r0 = r13 + r11
            int r1 = -r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x005e
        L_0x00c9:
            android.graphics.Matrix r0 = r9.sf
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.sf
            r1 = -1028390912(0xffffffffc2b40000, float:-90.0)
            r0.preRotate(r1)
            int r0 = r14 + r12
            int r1 = r13 + r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x005e
        L_0x00e3:
            android.graphics.Matrix r0 = r9.sf
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.sf
            r1 = -1020002304(0xffffffffc3340000, float:-180.0)
            r0.preRotate(r1)
            int r0 = -r11
            int r1 = r14 + r12
            r2 = r0
            r3 = r1
            r0 = r14
            r1 = r13
            goto L_0x005e
        L_0x00fc:
            android.graphics.Matrix r0 = r9.sf
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
            r2 = 1065353216(0x3f800000, float:1.0)
            r0.preScale(r1, r2)
            android.graphics.Matrix r0 = r9.sf
            r1 = -1014562816(0xffffffffc3870000, float:-270.0)
            r0.preRotate(r1)
            int r0 = -r12
            int r1 = -r11
            r2 = r0
            r3 = r1
            r0 = r13
            r1 = r14
            goto L_0x005e
        L_0x0114:
            r6 = r5 & 32
            if (r6 == 0) goto L_0x0123
            r6 = r5 & 2
            if (r6 != 0) goto L_0x0070
            int r6 = r17 - r0
            r8 = r6
            r6 = r4
            r4 = r8
            goto L_0x0074
        L_0x0123:
            r6 = r5 & 2
            if (r6 == 0) goto L_0x0070
            r6 = 1
            int r6 = r0 - r6
            int r6 = r6 >>> 1
            int r6 = r17 - r6
            r8 = r6
            r6 = r4
            r4 = r8
            goto L_0x0074
        L_0x0133:
            r7 = r5 & 8
            if (r7 == 0) goto L_0x013f
            r5 = r5 & 1
            if (r5 != 0) goto L_0x007c
            int r5 = r16 - r1
            goto L_0x0080
        L_0x013f:
            r5 = r5 & 1
            if (r5 == 0) goto L_0x007c
            r5 = 1
            int r5 = r1 - r5
            int r5 = r5 >>> 1
            int r5 = r16 - r5
            goto L_0x0080
        L_0x014c:
            android.graphics.Canvas r6 = r9.sc
            r6.save()
            android.graphics.Canvas r6 = r9.sc
            int r1 = r1 + r5
            int r0 = r0 + r4
            r6.clipRect(r5, r4, r1, r0)
            android.graphics.Canvas r0 = r9.sc
            int r1 = r5 + r2
            float r1 = (float) r1
            int r2 = r4 + r3
            float r2 = (float) r2
            r0.translate(r1, r2)
            android.graphics.Canvas r0 = r9.sc
            android.graphics.Bitmap r1 = r10.pR
            android.graphics.Matrix r2 = r9.sf
            r3 = 0
            r0.drawBitmap(r1, r2, r3)
            android.graphics.Canvas r0 = r9.sc
            r0.restore()
            goto L_0x0004
        L_0x0174:
            r5 = r16
            goto L_0x0080
        L_0x0178:
            r6 = r4
            r4 = r17
            goto L_0x0074
        L_0x017d:
            r5 = r18
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.cx.a(al, int, int, int, int, int, int, int, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void}
     arg types: [int[], int, int, int, int, int, int, int, android.graphics.Paint]
     candidates:
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, float, float, int, int, boolean, android.graphics.Paint):void}
      ClspMth{android.graphics.Canvas.drawBitmap(int[], int, int, int, int, int, int, boolean, android.graphics.Paint):void} */
    public final void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        if (this.sj > 0 && this.sk > 0 && i5 > 0 && i6 > 0) {
            if (iArr == null) {
                throw new NullPointerException();
            } else if (i5 > 0 && i6 > 0) {
                this.sc.drawBitmap(iArr, 0, i2 < i5 ? i5 : i2, i3, i4, i5, iArr.length - 0 < i5 * i6 ? (iArr.length - 0) / i5 : i6, true, this.rY);
            }
        }
    }

    public final void b(String str, int i, int i2, int i3) {
        if (this.sj > 0 && this.sk > 0) {
            int i4 = i3 == 0 ? 20 : i3;
            int i5 = (i4 & 16) != 0 ? i2 - this.sa.rV.top : (i4 & 32) != 0 ? i2 - this.sa.rV.bottom : (i4 & 2) != 0 ? (((this.sa.rV.descent - this.sa.rV.ascent) / 2) - this.sa.rV.descent) + i2 : i2;
            if ((i4 & 1) != 0) {
                this.sa.pe.setTextAlign(Paint.Align.CENTER);
            } else if ((i4 & 8) != 0) {
                this.sa.pe.setTextAlign(Paint.Align.RIGHT);
            } else if ((i4 & 4) != 0) {
                this.sa.pe.setTextAlign(Paint.Align.LEFT);
            }
            this.sa.pe.setColor(this.rY.getColor());
            this.sc.drawText(str, (float) i, (float) i5, this.sa.pe);
        }
    }

    public final void bd() {
        a(this.sc);
    }

    public final void d(int i, int i2, int i3, int i4) {
        if (this.sj > 0 && this.sk > 0) {
            this.sc.drawRect(20.0f, (float) i2, 30.0f, (float) (i2 + 10), this.rY);
        }
    }

    public final void e(int i, int i2, int i3, int i4) {
        if (this.sj > 0 && this.sk > 0 && i3 > 0 && i4 > 0) {
            this.sc.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.rZ);
        }
    }

    public final void f(int i, int i2, int i3, int i4) {
        this.sh = i;
        this.si = i2;
        this.sj = i3;
        this.sk = i4;
        if (i3 > 0 && i4 > 0) {
            this.sc.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
        }
    }

    public final void setColor(int i) {
        this.rY.setColor(-16777216 | i);
        this.rZ.setColor(-16777216 | i);
        super.setColor(i);
    }

    public final void translate(int i, int i2) {
        super.translate(i, i2);
        this.sc.translate((float) i, (float) i2);
    }
}
