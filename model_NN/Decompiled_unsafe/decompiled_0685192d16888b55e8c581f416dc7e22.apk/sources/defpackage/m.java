package defpackage;

import com.nokia.mid.ui.DirectGraphics;

/* renamed from: m  reason: default package */
public final class m implements DirectGraphics {
    private ak mV;
    private final int[] mW = new int[2];

    public m(ak akVar) {
        this.mV = akVar;
    }

    public final void a(al alVar, int i, int i2, int i3, int i4) {
        int i5 = 3;
        ak akVar = this.mV;
        int width = alVar.getWidth();
        int height = alVar.getHeight();
        switch (i4) {
            case DirectGraphics.ROTATE_90:
                i5 = 6;
                break;
            case DirectGraphics.ROTATE_180:
            case 24576:
                break;
            case DirectGraphics.ROTATE_270:
                i5 = 5;
                break;
            case DirectGraphics.FLIP_HORIZONTAL:
                i5 = 2;
                break;
            case 8282:
                i5 = 7;
                break;
            case 8372:
                i5 = 1;
                break;
            case 8462:
                i5 = 4;
                break;
            case DirectGraphics.FLIP_VERTICAL:
                i5 = 1;
                break;
            case 16474:
                i5 = 4;
                break;
            case 16564:
                i5 = 2;
                break;
            case 16654:
                i5 = 7;
                break;
            default:
                i5 = 0;
                break;
        }
        akVar.a(alVar, 0, 0, width, height, i5, i, i2, 20);
    }
}
