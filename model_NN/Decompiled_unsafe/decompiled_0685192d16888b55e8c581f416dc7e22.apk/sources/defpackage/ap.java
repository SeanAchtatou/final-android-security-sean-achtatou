package defpackage;

import org.meteoroid.plugin.device.MIDPDevice;

/* renamed from: ap  reason: default package */
public abstract class ap extends ab {
    public static final int DOWN_PRESSED = 64;
    public static final int FIRE_PRESSED = 256;
    public static final int GAME_A_PRESSED = 512;
    public static final int GAME_B_PRESSED = 1024;
    public static final int GAME_C_PRESSED = 2048;
    public static final int GAME_D_PRESSED = 4096;
    public static final int LEFT_PRESSED = 4;
    public static final int RIGHT_PRESSED = 32;
    public static final int UP_PRESSED = 2;
    private int pV;

    private static final boolean C(int i) {
        return i == 6 || i == 1 || i == 2 || i == 5 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12;
    }

    public final void o(int i, int i2) {
        co coVar = bg.qg;
        int P = MIDPDevice.P(i2);
        if (i == 0 && C(P)) {
            this.pV = (1 << P) | this.pV;
        } else if (i == 1 && C(P)) {
            this.pV = (1 << P) ^ this.pV;
        }
    }
}
