package defpackage;

import android.os.Environment;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/* renamed from: cu  reason: default package */
public final class cu implements y {
    File file;
    String rQ;
    DataInputStream rR;
    DataOutputStream rS;
    InputStream rT;
    OutputStream rU;

    public cu(String str) {
        this.rQ = str;
        String substring = str.substring("file://".length());
        if (Environment.getExternalStorageState().equals("mounted")) {
            this.file = new File(Environment.getExternalStorageDirectory() + substring);
            return;
        }
        throw new IOException("SD card not ready.");
    }

    public final void close() {
        if (this.rS != null) {
            this.rS.close();
            this.rS = null;
        }
        if (this.rR != null) {
            this.rR.close();
            this.rR = null;
        }
        if (this.rU != null) {
            this.rU.close();
            this.rU = null;
        }
        if (this.rT != null) {
            this.rT.close();
            this.rT = null;
        }
    }
}
