package defpackage;

import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: ar  reason: default package */
public final class ar {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    private int mt;
    private String name;
    private HashMap pZ = new HashMap();
    private int qa;
    private long qb;
    private HashSet qc = new HashSet();
    private boolean qd;
    private int version;

    private ar(String str) {
        this.name = str;
    }

    public static ar a(String str, boolean z) {
        ar arVar = new ar(str);
        if (bf.A(PREFIX + str)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(bf.B(PREFIX + str));
                arVar.a(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new as();
            }
        } else {
            try {
                arVar.bm();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new as();
            }
        }
        arVar.qd = true;
        return arVar;
    }

    private synchronized void a(DataInputStream dataInputStream) {
        this.version = dataInputStream.readInt();
        this.qb = dataInputStream.readLong();
        this.qa = dataInputStream.readInt();
        int readInt = dataInputStream.readInt();
        for (int i = 0; i < readInt; i++) {
            int readInt2 = dataInputStream.readInt();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.read(bArr);
            this.pZ.put(Integer.valueOf(readInt2), bArr);
        }
        bk();
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        this.version++;
        dataOutputStream.writeInt(this.version);
        dataOutputStream.writeLong(System.currentTimeMillis());
        dataOutputStream.writeInt(this.qa);
        dataOutputStream.writeInt(this.pZ.size());
        for (Map.Entry entry : this.pZ.entrySet()) {
            dataOutputStream.writeInt(((Integer) entry.getKey()).intValue());
            byte[] bArr = (byte[]) entry.getValue();
            dataOutputStream.writeInt(bArr.length);
            dataOutputStream.write(bArr);
        }
    }

    private void bk() {
        this.mt = 0;
        for (byte[] length : this.pZ.values()) {
            this.mt = length.length + this.mt;
        }
    }

    private synchronized void bl() {
        if (bf.A(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(bf.B(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                dataInputStream.close();
                if (readInt > this.version) {
                    a(new DataInputStream(bf.B(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    private synchronized void bm() {
        DataOutputStream dataOutputStream = new DataOutputStream(cd.getActivity().openFileOutput(bf.z(PREFIX + this.name) + ".dat", 1));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        bk();
    }

    private boolean isOpen() {
        if (this.qd) {
            return this.qd;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new av();
    }

    public final int F(int i) {
        isOpen();
        bl();
        if (this.pZ.containsKey(1)) {
            return ((byte[]) this.pZ.get(1)).length;
        }
        throw new aq();
    }

    public final byte[] G(int i) {
        isOpen();
        bl();
        if (this.pZ.containsKey(1)) {
            return (byte[]) this.pZ.get(1);
        }
        throw new aq();
    }

    public final void a(int i, byte[] bArr, int i2, int i3) {
        isOpen();
        if (this.pZ.containsKey(1)) {
            this.pZ.remove(1);
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, 0, bArr2, 0, i3);
            this.pZ.put(1, bArr2);
            try {
                bm();
            } catch (Exception e) {
            }
            Iterator it = this.qc.iterator();
            while (it.hasNext()) {
                it.next();
            }
            return;
        }
        throw new aq();
    }

    public final int b(byte[] bArr, int i, int i2) {
        isOpen();
        this.qa++;
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, 0, bArr2, 0, i2);
        }
        this.pZ.put(Integer.valueOf(this.qa), bArr2);
        try {
            bm();
            Iterator it = this.qc.iterator();
            while (it.hasNext()) {
                it.next();
                int i3 = this.qa;
            }
            return this.qa;
        } catch (Exception e) {
            throw new as();
        }
    }

    public final void bn() {
        isOpen();
        bl();
        this.qc.clear();
        this.qc = null;
        this.pZ.clear();
        this.qd = false;
        this.pZ = null;
        this.name = null;
        System.gc();
    }

    public final int bo() {
        isOpen();
        bl();
        return this.pZ.size();
    }

    public final int bp() {
        isOpen();
        bl();
        return this.qa + 1;
    }
}
