package defpackage;

import com.network.app.data.Database;
import game.GameMIDlet;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.Vector;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.midlet.MIDlet;

/* renamed from: c  reason: default package */
public final class c implements b {
    public static int gq = 9999;
    public static String kA;
    public static String ky = "";
    public static String kz;
    public final int I;
    public final int U;
    public final byte aB;
    public final byte aH;
    public f aI;
    public GameMIDlet aJ;
    public int[] aK;
    Player aL;
    public boolean aM;
    public int aN;
    VolumeControl aO;
    public boolean aP;
    public int aQ;
    public int aR;
    public int aS;
    public int aT;
    public int aU;
    public int aV;
    public int aW;
    public final int aX;
    public final int aY;
    public final int aZ;
    public final byte bA;
    public final byte bB;
    public final byte bC;
    public final byte bD;
    public final byte bE;
    public final byte bF;
    public final byte bG;
    public final byte bH;
    public final byte bI;
    public final byte bJ;
    public final byte bK;
    public final byte bL;
    public final byte bM;
    public final byte bN;
    public final byte bO;
    public final byte bP;
    public final byte bQ;
    public final byte bR;
    public final byte bS;
    public final byte bT;
    public final byte bU;
    public final byte bV;
    public final byte bW;
    public final byte bX;
    public final byte bY;
    public final byte bZ;
    public final int ba;
    public final int bb;
    final byte bc;
    public final int bd;
    public final int be;
    public final int bf;
    public final int bg;
    public final int bh;
    public final int bi;
    public final int bj;
    public final int bk;
    public final int bl;
    public final int bm;
    public final int bn;
    public final byte bo;
    public final int bp;
    public final int bq;
    public final int br;
    int bs;
    public boolean bt;
    int[] bu;
    boolean bv;
    public final byte bw;
    boolean bx;
    public byte by;
    public boolean bz;
    public boolean cA;
    public boolean cB;
    public int cC;
    public boolean cD;
    public final byte cE;
    public final byte cF;
    public final byte cG;
    public final byte cH;
    public final byte cI;
    int cJ;
    public final byte cK;
    public final byte cL;
    public final byte cM;
    public final byte cN;
    public final byte cO;
    public final byte cP;
    public final byte cQ;
    public final byte cR;
    public final byte cS;
    public final byte cT;
    public final byte cU;
    public final byte cV;
    public final byte cW;
    public final byte cX;
    public int cY;
    public int cZ;
    public final byte ca;
    public final byte cb;
    public final byte cc;
    public final byte cd;
    public final byte ce;
    public final byte cf;
    public final byte cg;
    public final byte ch;
    public int ci;
    final byte cj;
    final byte ck;
    final byte cl;
    final byte cm;
    final byte cn;
    final byte co;
    final byte cp;
    final byte cq;
    final byte cr;
    final byte cs;
    final byte ct;
    final byte cu;
    final byte cv;
    final byte cw;
    int cx;
    public boolean cy;
    public boolean cz;
    private boolean dA;
    public int dB;
    public int dC;
    public int dD;
    private int dE;
    public int dF;
    public int dG;
    private int[][] dH;
    public int[][] dI;
    public byte dJ;
    public byte dK;
    final byte dL;
    public byte dM;
    public byte dN;
    public int dO;
    public int dP;
    public int dQ;
    public boolean dR;
    public boolean dS;
    public boolean[] dT;
    public int dU;
    public int dV;
    public boolean dW;
    final byte dX;
    public int dY;
    public int dZ;
    public int da;
    int db;
    int dc;
    int dd;
    public int de;
    public int df;
    public int dg;
    public int dh;
    public boolean di;
    private int dj;
    public int dk;
    public int dl;
    public int dm;
    public int dn;

    /* renamed from: do  reason: not valid java name */
    public Random f0do;
    public d dp;
    public d[] dq;
    public int[][] dr;
    public boolean ds;
    public boolean dt;
    public final byte du;
    public final byte dv;
    public int[] dw;
    public int[] dx;
    private byte[][] dy;
    public short[][][] dz;
    public byte[] eA;
    public byte[] eB;
    public int eC;
    public int eD;
    public int eE;
    public int eF;
    public int eG;
    private int eH;
    private int eI;
    private int eJ;
    private int eK;
    private int eL;
    private int eM;
    int[] eN;
    public int eO;
    public int eP;
    public int eQ;
    public int eR;
    public int eS;
    public int eT;
    public int eU;
    public int eV;
    public int eW;
    public int eX;
    public int eY;
    public int eZ;
    private int ea;
    private int eb;
    private int ec;
    private int ed;
    private int ee;
    final byte ef;
    private int eg;
    private int eh;
    private int ei;
    private int ej;
    private int ek;
    public int el;
    public int em;
    public int en;
    public int eo;
    public int ep;
    private int eq;
    private int er;
    private int es;
    private int[] et;
    private InputStream eu;
    public byte[][][] ev;
    public byte[][][] ew;
    public byte[][] ex;
    public int ey;
    public int ez;
    public int fA;
    public int fB;
    public boolean fC;
    public int fD;
    public byte[] fE;
    public int fF;
    public int fG;
    public int fH;
    public int fI;
    public int fJ;
    public int fK;
    public int fL;
    public int fM;
    public short[][][] fN;
    public short[][][][] fO;
    public short[][][] fP;
    public short[][][] fQ;
    public int fR;
    public int[] fS;
    public int fT;
    public int fU;
    public int fV;
    public int fW;
    public int fX;
    public int[][] fY;
    public boolean fZ;
    public int fa;
    public int fb;
    final byte fc;
    final byte fd;
    final byte fe;
    final byte ff;
    final byte fg;
    final byte fh;
    final byte fi;
    final byte fj;
    final byte fk;
    final byte fl;
    final byte fm;
    final byte fn;
    final byte fo;
    final byte fp;
    final byte fq;
    final byte fr;
    final byte fs;
    final byte ft;
    final byte fu;
    final byte fv;
    final byte fw;
    private int[] fx;
    private int[] fy;
    public int fz;
    public int gA;
    public int gB;
    public int gC;
    public int gD;
    public int gE;
    public int gF;
    public boolean gG;
    public short[][][] gH;
    public byte gI;
    public int gJ;
    public int gK;
    public boolean gL;
    public byte[] gM;
    public String[] gN;
    public int gO;
    public int gP;
    public int gQ;
    public int gR;
    public boolean gS;
    public int gT;
    public int gU;
    public int gV;
    public int gW;
    public int gX;
    public boolean gY;
    public int gZ;
    public int[][] ga;
    public int gb;
    public int[][] gc;
    public int gd;
    public int ge;
    public byte[] gf;
    public int gg;
    public int gh;
    public int gi;
    public int gj;
    public int gk;
    public int gl;
    public int gm;
    public int gn;
    public int go;
    public int gp;
    public int[] gr;
    public int gs;
    public int gt;
    public int[] gu;
    public int gv;
    public int gw;
    public short[] gx;
    public short[] gy;
    public int gz;
    public int hA;
    public int hB;
    public int hC;
    private final byte hD;
    public boolean hE;
    public int hF;
    public int hG;
    public boolean hH;
    public boolean hI;
    public int hJ;
    public int hK;
    private final byte hL;
    public int hM;
    public boolean hN;
    public int hO;
    public int hP;
    public int hQ;
    public int hR;
    private int[] hS;
    private int[] hT;
    private String[] hU;
    private int[] hV;
    public boolean hW;
    public int hX;
    public boolean hY;
    public int hZ;
    public boolean ha;
    public boolean hb;
    public short[] hc;
    public short[] hd;
    public short[] he;
    public short[] hf;
    public short[] hg;
    public short[][][][] hh;
    public boolean hi;
    public int hj;
    public boolean hk;
    public byte[][] hl;
    public int hm;
    public int hn;
    public int[] ho;
    public int hp;
    public int hq;
    public int hr;
    public byte[] hs;
    public int ht;
    public int hu;
    public int hv;
    public boolean hw;
    public int hx;
    public int hy;
    public int hz;
    public int iA;
    public int iB;
    public int iC;
    int[] iD;
    public int iE;
    public int iF;
    public boolean iG;
    public int iH;
    public boolean iI;
    public boolean iJ;
    boolean[] iK;
    boolean iL;
    int[] iM;
    public boolean iN;
    public int[] iO;
    public int iP;
    public int iQ;
    public boolean iR;
    private final byte iS;
    private final byte iT;
    private final byte iU;
    private final byte iV;
    private final byte iW;
    private final byte iX;
    private final byte iY;
    private final byte iZ;
    public a ia;
    public byte[] ib;
    ar ic;
    public int id;
    public int ie;

    /* renamed from: if  reason: not valid java name */
    public int f1if;
    int ig;
    int ih;
    public int ii;
    public int ij;
    public boolean ik;
    public boolean il;
    final String[] im;
    private int in;
    String[] io;
    public int ip;
    public int iq;
    public byte[] ir;
    public String[] is;
    public String[] it;
    public int iu;
    public boolean iv;
    public boolean iw;
    public boolean ix;
    int iy;
    public int iz;
    public String j;
    private final byte jA;
    private final byte jB;
    private final byte jC;
    private final byte jD;
    private final byte jE;
    private final byte jF;
    private final byte jG;
    private final byte jH;
    public boolean jI;
    public boolean jJ;
    public boolean jK;
    boolean jL;
    boolean jM;
    byte jN;
    public boolean jO;
    public int jP;
    public boolean jQ;
    public int jR;
    public int[] jS;
    public int jT;
    public boolean jU;
    public boolean jV;
    public int[] jW;
    public int jX;
    public int[][][] jY;
    private int jZ;
    private int ka;
    private int kb;
    private int kc;
    private int kd;
    private int ke;
    private int kf;
    public int[][] kg;
    public boolean kh;
    public short[][] ki;
    public short[][][] kj;
    public short[][][] kk;
    public short[][][] kl;
    public short[][][] km;
    public byte[][] kn;
    public int[][] ko;
    public int[][] kp;
    byte[] kq;
    private int[][] kr;
    private ar ks;
    public int[][] kt;
    public int[] ku;
    public int[] kv;
    public boolean kw;
    public String kx;
    public int w;
    public int x;
    public boolean y;

    public c(f fVar, GameMIDlet gameMIDlet) {
        this.j = "";
        this.aK = new int[2];
        this.aM = true;
        this.aN = 2;
        this.aP = false;
        this.w = 0;
        this.y = false;
        this.aV = 0;
        this.aW = 0;
        this.aX = -1;
        this.aY = 0;
        this.aZ = 1;
        this.ba = 2;
        this.bb = 3;
        this.bd = 4;
        this.I = 5;
        this.be = 6;
        this.bf = 7;
        this.bg = 8;
        this.bh = 9;
        this.bi = 10;
        this.bj = 11;
        this.bk = 12;
        this.bl = 13;
        this.bm = 14;
        this.bn = 15;
        this.bp = 16;
        this.U = 17;
        this.bq = 18;
        this.br = 19;
        this.bs = 0;
        this.bt = false;
        this.bu = new int[17];
        this.bv = false;
        this.bx = true;
        this.by = 0;
        this.bz = false;
        this.bA = 0;
        this.bB = 1;
        this.bC = 2;
        this.bD = 3;
        this.bE = 4;
        this.bF = 5;
        this.bG = 6;
        this.bH = 7;
        this.bJ = 8;
        this.bK = 9;
        this.bL = 10;
        this.bM = d.jl;
        this.bN = d.jm;
        this.bO = d.jn;
        this.bP = d.jo;
        this.bQ = d.jp;
        this.bR = d.jq;
        this.bS = d.jr;
        this.bT = d.bo;
        this.bU = d.dL;
        this.bV = d.dX;
        this.aB = d.js;
        this.bW = d.jt;
        this.bX = d.bc;
        this.bY = d.ju;
        this.bZ = d.jv;
        this.ca = d.bI;
        this.aH = d.jw;
        this.cb = d.jx;
        this.cc = d.hL;
        this.cd = d.hD;
        this.ce = d.jy;
        this.cf = d.jz;
        this.cg = 33;
        this.ch = -1;
        this.ci = 0;
        this.cj = 0;
        this.ck = 1;
        this.cl = 2;
        this.cm = 3;
        this.cn = 4;
        this.co = 5;
        this.cp = 6;
        this.cq = 7;
        this.cr = 8;
        this.cs = 9;
        this.ct = 10;
        this.cu = d.jl;
        this.bc = d.jm;
        this.cv = d.jn;
        this.cw = d.jo;
        this.cy = false;
        this.cz = false;
        this.cA = false;
        this.cB = false;
        this.cC = 0;
        this.cD = false;
        this.cE = 0;
        this.cF = 1;
        this.bo = 2;
        this.cG = 3;
        this.cH = 4;
        this.cI = 5;
        this.cJ = 0;
        this.cK = 0;
        this.cL = 1;
        this.cM = 2;
        this.bw = 3;
        this.cN = 4;
        this.cO = 5;
        this.cP = 6;
        this.cQ = 7;
        this.cR = 8;
        this.cS = 9;
        this.cT = 10;
        this.cU = d.jl;
        this.cV = d.jm;
        this.cW = d.jn;
        this.cX = d.jo;
        this.bI = d.jp;
        this.cY = 0;
        this.cZ = 0;
        this.da = 0;
        this.db = 0;
        this.dc = 0;
        this.dd = 0;
        this.de = 0;
        this.df = 3;
        this.dg = 0;
        this.dh = 1;
        this.di = false;
        this.dj = 3;
        this.dl = 0;
        this.dm = 0;
        this.dn = 3;
        this.x = 0;
        this.f0do = new Random();
        this.ds = false;
        this.dt = false;
        this.du = 0;
        this.dv = 1;
        this.dw = new int[]{Player.REALIZED, 1000, 0, 0};
        this.dx = new int[]{5, 0, 15, 0};
        this.dy = new byte[][]{new byte[]{1, 1, 0, 2, 2, 2, 0, 0, 0, 0}, new byte[]{1, 1, 0, 2, 2, 5, 5, 2, 3, 0}, new byte[]{1, 2, 5, 2, 1, 2, 2, 1, 5, 2}, new byte[]{2, 5, 1, 2, 1, 2, 2, 2, 5, 1}, new byte[]{1, 5, 2, 2, 2, 5, 2, 1, 1, 2}, new byte[]{1, 5, 2, 2, 2, 5, 2, 1, 1, 2}, new byte[]{5, 1, 2, 2, 1, 2, 5, 2, 1, 2}, new byte[]{5, 1, 2, 2, 1, 2, 5, 2, 1, 2}, new byte[]{2, 5, 1, 2, 1, 2, 2, 2, 5, 1}, new byte[]{2, 1, 2, 5, 2, 1, 2, 2, 1, 5}, new byte[]{1, 5, 2, 5, 2, 5, 1, 5, 1, 2}, new byte[]{5, 2, 1, 5, 2, 5, 2, 1, 5, 2}, new byte[]{1, 1, 2, 5, 2, 2, 2, 1, 2, 5}};
        this.dz = new short[][][]{new short[][]{new short[0], new short[0]}, new short[][]{new short[0], new short[0]}, new short[][]{new short[]{53, 15, 25, 15}, new short[]{110, 12, 15, 25}}, new short[][]{new short[]{45, 15, 25, 20}, new short[]{70, 13, 25, 15}}, new short[][]{new short[]{40, 13, 25, 15}, new short[]{120, 10, 10, 25}}, new short[][]{new short[]{40, 15, 25, 15}, new short[]{120, 10, 10, 25}}, new short[][]{new short[]{50, 15, 25, 20}, new short[]{70, 13, 25, 15}}, new short[][]{new short[]{50, 15, 25, 20}, new short[]{70, 13, 25, 15}}, new short[][]{new short[]{45, 10, 20, 15}, new short[]{45, 10, 20, 15}}, new short[][]{new short[]{40, 12, 20, 20}, new short[]{10, 12, 20, 20}}, new short[][]{new short[]{80, 10, 15, 25}, new short[]{80, 10, 15, 25}}, new short[][]{new short[]{30, 10, 25, 15}, new short[]{30, 10, 25, 15}}, new short[][]{new short[]{40, 15, 25, 10}, new short[]{120, 10, 10, 25}}};
        this.dA = true;
        this.dB = -1;
        this.dC = 0;
        this.dD = 0;
        this.dE = 8;
        this.dF = 0;
        this.dG = 0;
        this.dH = new int[][]{new int[]{120, 130, 0}, new int[]{22, 132, 0}, new int[]{260, 119, 1}, new int[]{21, 101, 0}, new int[]{88, 156, 0}, new int[]{280, 112, 1}, new int[]{250, 95, 1}, new int[]{16, 67, 0}, new int[]{30, 120, 0}};
        this.dI = null;
        this.dJ = 0;
        this.dK = 1;
        this.dM = 2;
        this.dN = this.dJ;
        this.dO = 0;
        this.dP = -1;
        this.dQ = -1;
        this.dR = false;
        this.dS = false;
        this.dT = null;
        this.dU = -1;
        this.dV = -1;
        this.dW = false;
        this.dY = 0;
        this.dZ = 0;
        this.ea = 0;
        this.eb = 0;
        this.ec = 0;
        this.ed = 0;
        this.ee = 0;
        this.eg = 0;
        this.eh = 0;
        this.ei = 0;
        this.ej = 0;
        this.ek = 0;
        this.el = 0;
        this.em = 0;
        this.en = 0;
        this.eo = 0;
        this.ep = 0;
        this.eq = 0;
        this.er = 0;
        this.es = 3;
        this.et = new int[]{9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20, 19, 21, 23, 24};
        this.eu = null;
        this.ev = null;
        this.ew = null;
        this.ex = (byte[][]) Array.newInstance(Byte.TYPE, 2, 7);
        this.ey = 0;
        this.ez = 0;
        this.eA = new byte[]{0, 0, 1, 2, d.jo, 2, 0, 1, d.jo};
        this.eB = new byte[]{0, d.jo, 1, 2};
        this.eC = 0;
        this.eD = 0;
        this.eE = 0;
        this.eF = 0;
        this.eG = 0;
        this.eH = 18;
        this.eI = 0;
        this.eJ = 0;
        this.eK = 0;
        this.eL = 0;
        this.eM = 64;
        this.eN = new int[]{8, 6, 5};
        this.eO = 0;
        this.eP = 0;
        this.eQ = 0;
        this.eR = 0;
        this.eS = 7;
        this.eT = 0;
        this.eU = 198;
        this.eV = 0;
        this.eW = 0;
        this.eX = -1;
        this.eY = -1;
        this.eZ = 0;
        this.fa = 0;
        this.fb = -1;
        this.dL = 0;
        this.fc = 1;
        this.fd = 2;
        this.fe = 3;
        this.ff = 4;
        this.fg = 5;
        this.fh = 6;
        this.fi = 7;
        this.fj = 8;
        this.fk = 9;
        this.fl = 10;
        this.fm = d.jl;
        this.dX = d.jm;
        this.fn = d.jn;
        this.fo = d.jo;
        this.fp = d.jp;
        this.fq = d.jq;
        this.fr = d.jr;
        this.fs = d.bo;
        this.ft = d.dL;
        this.ef = d.dX;
        this.fu = d.js;
        this.fv = d.jt;
        this.fw = d.bc;
        this.fx = new int[]{11, 13, 18, 19, 16, 10, 14, 15, 22, 2, 3, 4, 5, 6, 20};
        this.fy = new int[]{7, 8, 9, 21};
        this.fz = 0;
        this.fA = 2;
        this.fB = -1;
        this.fC = false;
        this.fD = 0;
        this.fE = new byte[60];
        this.fF = 0;
        this.fG = 0;
        this.fH = 0;
        this.fI = 0;
        this.fJ = 99999;
        this.fK = 0;
        this.fL = 0;
        this.fM = 0;
        this.fN = new short[7][][];
        this.fO = new short[13][][][];
        this.fP = new short[13][][];
        this.fQ = new short[13][][];
        this.fR = 0;
        this.fS = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        this.fT = 0;
        this.fU = 0;
        this.fV = 0;
        this.fW = 0;
        this.fX = 0;
        this.fY = new int[][]{new int[]{0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 170, 190, 210, 230, 280, 280}};
        this.fZ = false;
        this.ga = new int[][]{new int[]{600, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, new int[]{500, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};
        this.gb = 1;
        this.gc = new int[][]{new int[]{600, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}, new int[]{500, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1}};
        this.gd = 1;
        this.ge = 0;
        this.gf = new byte[]{10, 10, 10, 4};
        this.gg = 0;
        this.gh = 0;
        this.gi = 0;
        this.gj = 1000;
        this.gk = 0;
        this.gl = 0;
        this.gm = 0;
        this.gn = 0;
        this.go = 0;
        this.gp = 0;
        this.gr = new int[]{55, 64};
        this.gs = 55;
        this.gt = 55;
        this.gu = new int[]{20, 21, 23, 24};
        this.gv = 0;
        this.gw = 0;
        this.gx = new short[]{14, 22, 31, 40};
        this.gy = new short[]{450, 1100, 2000, 3500, 5300, 7000};
        this.gz = -1;
        this.gA = 2;
        this.gB = -1;
        this.gC = 0;
        this.gD = 1;
        this.gE = 0;
        this.gF = 6;
        this.gG = false;
        this.gH = new short[][][]{new short[][]{new short[]{-25, 85, 1, 46}, new short[]{303, 85, 1, 46}}, new short[][]{new short[]{-25, 92, 1, 46}, new short[]{340, 92, 1, 46}}, new short[][]{new short[]{-7, 78, 1, 46}, new short[]{320, 78, 1, 46}}, new short[][]{new short[]{-7, 70, 1, 40}, new short[]{341, 70, 1, 40}}};
        this.gI = -1;
        this.gJ = 0;
        this.gK = 1;
        this.gL = true;
        this.gM = new byte[]{-1, -1, 0, 0, 0, 0, -1, -1, 1, 2, -1, -1, -1, -1, -1, 4, -1, 3, -1, -1, -1, -1, -1, -1, -1, -1, 0, -1, -1, -1, -1, -1, -1, -1};
        this.gO = -1;
        this.gP = -1;
        this.gQ = -1;
        this.gR = -1;
        this.gS = false;
        this.gT = -1;
        this.gU = -1;
        this.gV = -1;
        this.gW = 0;
        this.gX = 0;
        this.gY = false;
        this.gZ = 0;
        this.ha = true;
        this.hb = false;
        this.hc = new short[]{245, 229, 3, 224, 177, 264, 240, 3, 100, 30};
        this.hd = new short[]{260, 62, 1, 250, 1, 305, 52, 3, 70, 190};
        this.he = new short[]{14, 139, 0, -16, 51, 16, 169, 1, 339, 112};
        this.hf = new short[]{267, 216, 3, 253, 169, 284, 208, 5, 79, 40};
        this.hg = new short[]{354, 140, 2, 350, 71, 379, 139, 4, 0, 85};
        this.hh = new short[][][][]{new short[][][]{new short[][]{new short[]{350, 125, 2, 349, 86, 378, 136, 1, 0, 120}}, new short[][]{new short[]{15, 145, 0, 0, 86, 14, 179, 0, 350, 117}, new short[]{145, 60, 1, 131, 0, 165, 87, 2, 77, 230}}, new short[][]{new short[]{82, 236, 3, 68, 200, 101, 238, 1, 160, 30}, new short[]{410, 250, 3, 390, 201, 459, 233, 3, 60, 0}}, new short[0][]}, new short[][][]{new short[][]{new short[]{243, 55, 1, 227, 0, 262, 91, 1, 53, 270}, new short[]{243, 55, 1, 227, 0, 262, 91, 1, 53, 270}}, new short[][]{new short[]{52, 244, 3, 40, 170, 70, 237, 0, 265, 30}, new short[]{330, 244, 3, 312, 171, 346, 237, 2, 89, 20}}, new short[][]{new short[]{81, 50, 1, 61, 0, 96, 94, 1, 327, 216}, new short[]{104, 240, 3, 93, 180, 118, 236, 3, 365, 24}}, new short[][]{new short[]{356, 58, 1, 342, 0, 376, 80, 2, 100, 229}, new short[]{16, 160, 0, 0, 120, 42, 180, 4, 377, 134}}, new short[0][]}, new short[][][]{new short[][]{new short[]{65, 60, 1, 54, 0, 86, 68, 5, 235, 208}, new short[]{60, 210, 3, 51, 181, 84, 210, 1, 280, 30}}, new short[][]{new short[]{259, 64, 1, 241, 0, 289, 50, 0, 70, 230}, new short[]{21, 125, 0, 0, 68, 30, 125, 2, 358, 145}}, new short[][]{new short[]{326, 170, 2, 315, 110, 359, 175, 1, 0, 95}, new short[]{60, 60, 1, 51, 5, 79, 61, 6, 59, 237}, new short[]{30, 110, 0, 0, 72, 52, 104, 7, 361, 179}, new short[]{76, 231, 3, 59, 191, 92, 238, 3, 255, 30}}, new short[][]{new short[]{238, 55, 1, 225, 0, 257, 56, 2, 74, 239}, new short[]{27, 120, 0, 0, 80, 56, 125, 4, 300, 101}}, new short[][]{new short[]{266, 128, 2, 260, 90, 300, 125, 3, 0, 94}, new short[]{120, 68, 1, 115, 0, 140, 55, 7, 100, 240}}, new short[][]{new short[]{250, 212, 3, 239, 180, 272, 209, 0, 82, 50}, new short[]{21, 142, 0, 0, 110, 31, 142, 6, 360, 83}}, new short[][]{new short[]{316, 105, 2, 300, 67, 360, 99, 5, 0, 123}, new short[]{50, 225, 3, 44, 179, 75, 240, 2, 75, 30}}, new short[0][]}, new short[][][]{new short[][]{new short[]{289, 105, 2, 269, 51, 320, 125, 1, 0, 123}}, new short[][]{new short[]{17, 140, 0, 0, 85, 29, 140, 0, 313, 76}, new short[]{220, 199, 3, 210, 141, 255, 193, 2, 240, 20}, new short[]{220, 199, 3, 210, 141, 255, 193, 2, 240, 20}}, new short[][]{new short[]{218, 56, 1, 199, 0, 255, 51, 1, 225, 187}, new short[]{270, 145, 2, 260, 60, 297, 149, 3, 60, 97}}, new short[][]{new short[]{20, 115, 0, 0, 71, 41, 110, 2, 297, 122}, new short[]{250, 62, 1, 232, 0, 266, 49, 4, 208, 194}}, new short[][]{new short[]{195, 193, 3, 180, 149, 230, 192, 3, 265, 30}, new short[]{253, 109, 2, 248, 51, 277, 105, 5, 0, 181}}, new short[][]{new short[]{247, 246, 3, 232, 201, 265, 238, 7, 91, 20}, new short[]{18, 200, 0, 0, 155, 36, 197, 4, 278, 83}, new short[]{-30, 0, 0, 0, 71, 36, 110, 6, 276, 89}}, new short[][]{new short[]{258, 108, 2, 248, 51, 282, 119, 5, 0, 89}}, new short[0][]}, new short[][][]{new short[][]{new short[]{325, 150, 2, 311, 90, 361, 160, 1, 2, 101}}, new short[][]{new short[]{14, 110, 0, 0, 65, 30, 159, 0, 357, 128}, new short[]{217, 61, 1, 199, 0, 235, 40, 2, 66, 189}}, new short[][]{new short[]{60, 193, 3, 50, 158, 80, 189, 1, 235, 30}, new short[]{244, 198, 3, 229, 158, 279, 191, 3, 85, 30}}, new short[][]{new short[]{80, 50, 1, 61, 0, 96, 42, 2, 239, 189}, new short[]{220, 204, 3, 213, 160, 245, 193, 4, 76, 30}}, new short[][]{new short[]{50, 50, 1, 30, 0, 85, 61, 3, 229, 191}, new short[]{294, 125, 2, 277, 61, 321, 149, 5, 0, 125}}, new short[0][]}, new short[][][]{new short[][]{new short[]{14, 139, 0, -16, 51, 16, 169, 1, 339, 112}, new short[]{14, 139, 0, -16, 51, 16, 169, 1, 339, 112}, new short[]{14, 139, 0, -16, 51, 16, 169, 1, 339, 112}}, new short[][]{new short[]{9, 130, 0, 0, 51, 16, 139, 2, 362, 127}, new short[]{300, 130, 2, 330, 51, 346, 139, 0, 16, 127}}, new short[][]{new short[]{329, 143, 2, 343, 61, 372, 169, 1, 5, 108}}, new short[][]{new short[]{70, 198, 3, 31, 158, 110, 188, 0, 280, 30}, new short[]{249, 62, 1, 238, 4, 289, 51, 4, 58, 185}}, new short[][]{new short[]{60, 190, 3, 44, 159, 100, 189, 3, 275, 30}, new short[]{250, 200, 3, 220, 160, 289, 192, 8, 90, 30}}, new short[][]{new short[]{50, 60, 1, 31, 5, 95, 61, 0, 266, 220}, new short[]{240, 60, 1, 232, 0, 269, 51, 6, 60, 183}}, new short[][]{new short[]{52, 183, 3, 42, 132, 77, 189, 5, 265, 30}, new short[]{248, 54, 1, 235, 1, 289, 51, 8, 79, 215}, new short[]{-30, 0, 3, 240, 134, 277, 188, 7, 85, 23}}, new short[][]{new short[]{62, 61, 1, 53, 1, 87, 52, 6, 260, 181}}, new short[0][], new short[0][]}, new short[][][]{new short[][]{new short[]{20, 130, 0, 0, 75, 31, 130, 1, 299, 111}}, new short[][]{new short[]{283, 139, 2, 284, 71, 300, 149, 0, 5, 108}, new short[]{20, 137, 0, 0, 85, 31, 149, 2, 339, 141}}, new short[][]{new short[]{307, 169, 2, 299, 118, 338, 180, 1, 5, 107}, new short[]{170, 82, 1, 153, 1, 188, 82, 3, 158, 225}}, new short[0][]}, new short[][][]{new short[][]{new short[]{290, 142, 2, 278, 80, 320, 148, 1, 2, 108}}, new short[][]{new short[]{21, 135, 0, -16, 71, 16, 139, 0, 315, 118}, new short[]{341, 135, 2, 371, 71, 390, 139, 2, 8, 118}}, new short[][]{new short[]{19, 140, 0, 0, 85, 42, 150, 1, 363, 105}, new short[]{72, 217, 3, 41, 185, 110, 217, 4, 250, 30}, new short[]{289, 145, 2, 306, 51, 331, 189, 3, 4, 119}}, new short[][]{new short[]{20, 140, 0, -16, 71, 9, 189, 2, 294, 124}, new short[]{225, 223, 3, 210, 188, 245, 219, 6, 98, 30}}, new short[][]{new short[]{219, 55, 1, 202, 3, 259, 53, 2, 78, 230}, new short[]{263, 125, 2, 258, 85, 300, 145, 5, 9, 118}, new short[]{-30, 0, 3, 60, 158, 94, 190, 7, 210, 30}}, new short[][]{new short[]{19, 149, 0, 2, 85, 61, 155, 4, 297, 110}, new short[]{288, 147, 2, 276, 85, 320, 150, 6, 7, 101}}, new short[][]{new short[]{15, 115, 0, 2, 70, 44, 130, 5, 317, 118}, new short[]{65, 50, 1, 41, 1, 118, 51, 3, 228, 219}, new short[]{65, 200, 3, 55, 158, 118, 191, 8, 260, 30}}, new short[][]{new short[]{190, 55, 1, 181, 3, 215, 51, 4, 77, 190}}, new short[][]{new short[]{245, 65, 1, 229, 2, 265, 50, 6, 80, 187}, new short[]{308, 128, 2, 296, 70, 339, 145, 9, 6, 128}}, new short[0][]}, new short[][][]{new short[][]{new short[]{230, 72, 1, 218, 2, 265, 72, 1, 79, 179}, new short[]{301, 155, 2, 196, 95, 337, 165, 4, 8, 96}}, new short[][]{new short[]{77, 196, 3, 41, 156, 107, 188, 0, 255, 30}, new short[]{280, 134, 2, 288, 61, 313, 159, 2, 8, 99}}, new short[][]{new short[]{17, 132, 0, -16, 61, 9, 149, 1, 289, 106}, new short[]{307, 135, 2, 296, 90, 340, 126, 3, 8, 107}, new short[]{245, 190, 3, 210, 144, 299, 188, 5, 95, 30}}, new short[][]{new short[]{20, 130, 0, -16, 95, 45, 129, 2, 329, 108}, new short[]{250, 230, 3, 200, 175, 299, 217, 6, 90, 30}}, new short[][]{new short[]{18, 123, 0, 2, 70, 43, 140, 0, 332, 130}, new short[]{223, 199, 3, 190, 156, 259, 190, 7, 90, 30}}, new short[][]{new short[]{75, 57, 1, 66, 3, 105, 54, 2, 249, 170}, new short[]{235, 194, 3, 210, 148, 279, 189, 8, 85, 30}}, new short[][]{new short[]{65, 51, 1, 41, 2, 113, 55, 3, 250, 210}, new short[]{65, 198, 3, 41, 146, 110, 191, 8, 275, 30}, new short[]{268, 196, 3, 235, 146, 319, 191, 9, 99, 30}}, new short[][]{new short[]{60, 53, 1, 41, 1, 101, 53, 4, 219, 190}, new short[]{321, 129, 2, 295, 88, 340, 117, 8, 6, 107}}, new short[][]{new short[]{11, 135, 0, 0, 95, 43, 128, 7, 321, 100}, new short[]{68, 63, 1, 41, 2, 105, 53, 5, 236, 175}, new short[]{250, 57, 1, 231, 0, 299, 52, 6, 80, 180}}, new short[0][]}};
        this.hi = false;
        this.hj = 0;
        this.hk = false;
        this.hl = new byte[][]{new byte[]{d.ju, d.jp, d.jq, d.jp, d.jq, d.jr, d.dL, d.dL, d.bo}, new byte[]{d.ju, d.jp, d.jq, d.jq, d.jq, d.jq, d.jq, d.jq, d.jq, d.jq, d.jq, d.jq, d.bo}};
        this.hm = 0;
        this.hn = 0;
        this.ho = null;
        this.hp = -1;
        this.hq = 0;
        this.hr = 0;
        this.hs = null;
        this.ht = -1;
        this.hu = 0;
        this.hv = 0;
        this.hw = false;
        this.hx = 100;
        this.hy = 100;
        this.hz = 100;
        this.hA = 0;
        this.hB = 0;
        this.hC = 0;
        this.hE = false;
        this.hF = 0;
        this.hG = 0;
        this.hH = false;
        this.hI = false;
        this.hJ = 0;
        this.hK = 17;
        this.hM = 0;
        this.hN = false;
        this.hO = 0;
        this.hP = 0;
        this.hQ = 0;
        this.hR = 0;
        this.hS = new int[]{27610, 19630, 6290, 13574, 4760, 8726, 11376, 14266, 4058, 7620, 3364, 3316, 7594};
        this.hT = new int[]{118, 78, 734, 118, 390, 6, 14, 38, 246};
        this.hU = new String[]{"/data/0.data", "/data/1.data"};
        this.hV = new int[]{132184, 1742};
        this.hW = false;
        this.hX = -1;
        this.hY = false;
        this.hZ = -1;
        this.ib = null;
        this.ic = null;
        this.ie = 0;
        this.f1if = 5;
        this.ig = 4;
        this.ih = -1;
        this.ii = 0;
        this.ij = 0;
        this.ik = false;
        this.il = true;
        this.im = new String[]{"PAYID:", "CPID:", "GAMEID:", "CHANNELID:"};
        this.ir = new byte[]{4, 7, 8, 9, 5, d.jl, 2, 3, 6};
        this.is = new String[]{"YX,812,2,7f5f", "YX,812,3,ee1f", "YX,812,3,ee1f", "YX,812,3,ee1f", "YX,812,4,5d68", "YX,812,5,fe5a", "YX,812,1,c69b", "", "YX,812,4,5d68"};
        this.it = new String[]{"11", "21", "21", "21", "31", "41", "51", "51", "31"};
        this.iu = 0;
        this.iv = false;
        this.iw = false;
        this.ix = false;
        this.iy = 1;
        this.iz = 0;
        this.iD = new int[]{5, 6, 4};
        this.iE = 0;
        this.iF = 0;
        this.iG = false;
        this.iI = false;
        this.iJ = false;
        this.iK = new boolean[]{false, false, false, false, false};
        this.iL = false;
        this.iM = new int[]{0, 0, 0, 0, 0};
        this.iN = false;
        this.iO = new int[]{18, 10};
        this.iP = 0;
        this.iQ = 0;
        this.iR = false;
        this.iS = 0;
        this.hD = 1;
        this.iT = 2;
        this.iU = 3;
        this.iV = 4;
        this.iW = 5;
        this.iX = 6;
        this.iY = 7;
        this.iZ = 8;
        this.hL = 9;
        this.jA = 10;
        this.jB = d.jm;
        this.jC = d.jn;
        this.jD = d.jo;
        this.jE = d.jp;
        this.jF = d.jq;
        this.jG = d.jr;
        this.jH = d.bo;
        this.jI = false;
        this.jJ = false;
        this.jK = false;
        this.jL = false;
        this.jM = false;
        this.jN = 0;
        this.jO = false;
        this.jP = 0;
        this.jQ = false;
        this.id = 0;
        this.jR = 0;
        this.jS = new int[]{10, 25};
        this.jT = 0;
        this.jU = false;
        this.jV = false;
        this.jW = new int[]{30, 40};
        this.jX = 0;
        this.jY = new int[][][]{new int[][]{new int[]{0}, new int[]{0}, new int[]{140, 80, 258, 118, 140, 166, 258, 199}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{154, 58, 224, 98, 154, 140, 224, MIDIControl.CONTROL_CHANGE}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{145, 64, 221, 79, 145, 121, 221, 151}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}, new int[][]{new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}, new int[]{0}}};
        this.kg = null;
        this.kh = false;
        this.kn = null;
        this.ko = null;
        this.kp = null;
        this.kq = new byte[]{-1, 1};
        this.kr = null;
        this.kt = new int[][]{new int[]{80, 90, 99, 100}, new int[]{40, 60, 70, 100}, new int[]{100, 150, 199, 9999}, new int[]{30, 60, 140, 9999}};
        this.ku = new int[]{1, 2, 5, 10, 10};
        this.kv = new int[]{10, 5, 2, 1};
        this.kw = false;
        this.kx = "";
        this.ci = 0;
        byte[] af = af();
        e.ls = new short[13][][][];
        this.dp = new d();
        this.aJ = gameMIDlet;
        GameMIDlet gameMIDlet2 = this.aJ;
        this.j = GameMIDlet.nc;
        for (int i = 0; i < 13; i++) {
            b(i, af);
        }
        byte[] ae = ae();
        e.lC = new short[9][][];
        for (int i2 = 0; i2 < e.lC.length; i2++) {
            e.lC[i2] = a(i2, ae);
        }
        this.il = true;
        this.aI = fVar;
        Z();
        a();
        this.io = a(c("/data/order.bin"), this.im);
    }

    private static void L() {
        try {
            GameMIDlet gameMIDlet = GameMIDlet.nb;
            MIDlet.u("http://go.i139.cn/gcomm1/portal/spchannel.do?url=http://gamepie.i139.cn/wap/s.do?j=3channel");
        } catch (q e) {
        }
    }

    private void M() {
        if (this.aN <= 0 || this.aN >= 3) {
            this.aM = false;
            this.aN = 0;
        } else {
            this.aM = true;
        }
        if (!this.aM) {
            ab();
            return;
        }
        this.gO = this.gM[this.ci];
        m(this.gO);
        aa();
    }

    private void N() {
        d(10, 2);
        this.dd = 0;
        this.dc = 0;
    }

    private void O() {
        d(5, 0);
        this.cJ = 0;
        if (this.w == 0) {
            this.db = 0;
        } else {
            this.db = 1;
        }
    }

    private void P() {
        if (this.iN) {
            this.iP++;
            if (this.iP >= this.iO[this.dp.kH]) {
                if (this.dq != null) {
                    for (d dVar : this.dq) {
                        dVar.lp = false;
                    }
                }
                this.iP = 0;
                this.iN = false;
            }
        }
    }

    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v394, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v411, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v412, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v413, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void Q() {
        /*
            r9 = this;
            r4 = 5
            r8 = 2
            r3 = 3
            r7 = 1
            r6 = 0
            int r0 = r9.hp
            if (r0 < 0) goto L_0x0011
            r9.hi = r7
            int r0 = r9.hr
            int r0 = r0 + 1
            r9.hr = r0
        L_0x0011:
            boolean r0 = r9.hN
            if (r0 == 0) goto L_0x0023
            int r0 = r9.hO
            int r0 = r0 + 1
            r9.hO = r0
            int r0 = r9.hO
            r1 = 10
            if (r0 <= r1) goto L_0x0023
            r9.hN = r6
        L_0x0023:
            int[] r0 = r9.aK
            r1 = r0[r7]
            int r1 = r1 + 1
            r0[r7] = r1
            int[] r0 = r9.aK
            r0 = r0[r7]
            int r0 = r0 % 100
            if (r0 != 0) goto L_0x0035
            r9.hv = r6
        L_0x0035:
            boolean r0 = r9.hi
            if (r0 == 0) goto L_0x003a
        L_0x0039:
            return
        L_0x003a:
            int[] r0 = r9.aK
            r1 = r0[r6]
            int r1 = r1 + 1
            r0[r6] = r1
            int r0 = r9.ci
            switch(r0) {
                case 0: goto L_0x06f5;
                case 1: goto L_0x07a7;
                case 2: goto L_0x0047;
                case 3: goto L_0x0047;
                case 4: goto L_0x07cd;
                case 5: goto L_0x057d;
                case 6: goto L_0x0047;
                case 7: goto L_0x0047;
                case 8: goto L_0x00f2;
                case 9: goto L_0x0552;
                case 10: goto L_0x0047;
                case 11: goto L_0x0662;
                case 12: goto L_0x0690;
                case 13: goto L_0x0047;
                case 14: goto L_0x0047;
                case 15: goto L_0x0654;
                case 16: goto L_0x080e;
                case 17: goto L_0x0047;
                case 18: goto L_0x0047;
                case 19: goto L_0x0047;
                case 20: goto L_0x0047;
                case 21: goto L_0x0047;
                case 22: goto L_0x0047;
                case 23: goto L_0x0047;
                case 24: goto L_0x082d;
                case 25: goto L_0x0047;
                case 26: goto L_0x07ff;
                case 27: goto L_0x0047;
                case 28: goto L_0x00a1;
                case 29: goto L_0x00ad;
                case 30: goto L_0x00e5;
                default: goto L_0x0047;
            }
        L_0x0047:
            int r0 = r9.fb
            if (r0 >= r8) goto L_0x0075
            int r0 = r9.eZ
            if (r0 != r8) goto L_0x0075
            int r0 = r9.fb
            if (r0 != 0) goto L_0x005d
            int r0 = r9.eX
            int r1 = r9.ci
            if (r0 == r1) goto L_0x005d
            int r0 = r9.eX
            r9.ci = r0
        L_0x005d:
            int r0 = r9.fb
            if (r0 != r7) goto L_0x006b
            int r0 = r9.eY
            int r1 = r9.cJ
            if (r0 == r1) goto L_0x006b
            int r0 = r9.eY
            r9.cJ = r0
        L_0x006b:
            r9.eZ = r7
            int[] r0 = r9.aK
            r0[r6] = r6
            int[] r0 = r9.aK
            r0[r7] = r6
        L_0x0075:
            boolean r0 = r9.aM
            if (r0 == 0) goto L_0x0099
            byte[] r0 = r9.gM
            int r1 = r9.ci
            byte r0 = r0[r1]
            boolean r1 = r9.hE
            if (r1 == 0) goto L_0x008b
            int r1 = r9.ci
            r2 = 8
            if (r1 != r2) goto L_0x008b
            int r0 = r0 + 1
        L_0x008b:
            int r1 = r9.gO
            if (r0 == r1) goto L_0x0099
            r9.gO = r0
            int r0 = r9.gO
            r9.m(r0)
            r9.aa()
        L_0x0099:
            f r0 = r9.aI
            int[] r1 = r9.aK
            r0.a(r1)
            goto L_0x0039
        L_0x00a1:
            boolean r0 = r9.iw
            if (r0 != r7) goto L_0x0047
            r0 = 25
            r9.d(r0, r6)
            r9.iw = r6
            goto L_0x0047
        L_0x00ad:
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 10
            if (r0 != r1) goto L_0x00c2
            byte r0 = r9.by
            r1 = 9
            if (r0 != r1) goto L_0x00d7
            f r0 = r9.aI
            int r1 = r9.dG
            r0.f(r1)
        L_0x00c2:
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 20
            if (r0 < r1) goto L_0x0047
            int r0 = r9.cC
            if (r0 != 0) goto L_0x00df
            byte r0 = r9.by
            r9.d(r0, r6)
        L_0x00d3:
            r9.cC = r6
            goto L_0x0047
        L_0x00d7:
            f r0 = r9.aI
            int r1 = r9.dC
            r0.f(r1)
            goto L_0x00c2
        L_0x00df:
            byte r0 = r9.by
            r9.d(r0, r8)
            goto L_0x00d3
        L_0x00e5:
            boolean r0 = r9.aP
            if (r0 != r7) goto L_0x0047
            r0 = 25
            r9.d(r0, r6)
            r9.aP = r6
            goto L_0x0047
        L_0x00f2:
            int r0 = r9.dD
            if (r0 != 0) goto L_0x01a0
            int r0 = r9.gi
            int r1 = r9.gj
            if (r0 != r1) goto L_0x01a0
            r9.iR = r7
        L_0x00fe:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 500(0x1f4, float:7.0E-43)
            if (r0 < r1) goto L_0x011f
            byte[] r0 = r9.ib
            r1 = 34
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x011f
            byte[] r0 = r9.ib
            r1 = 34
            r0[r1] = r7
            byte[] r0 = r9.ib
            r9.c(r0)
        L_0x011f:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 < r1) goto L_0x0140
            byte[] r0 = r9.ib
            r1 = 35
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x0140
            byte[] r0 = r9.ib
            r1 = 35
            r0[r1] = r7
            byte[] r0 = r9.ib
            r9.c(r0)
        L_0x0140:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 3000(0xbb8, float:4.204E-42)
            if (r0 < r1) goto L_0x0161
            byte[] r0 = r9.ib
            r1 = 36
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x0161
            byte[] r0 = r9.ib
            r1 = 36
            r0[r1] = r7
            byte[] r0 = r9.ib
            r9.c(r0)
        L_0x0161:
            boolean r0 = r9.y
            if (r0 != 0) goto L_0x016b
            int[] r0 = r9.aK
            r0 = r0[r6]
            if (r0 >= r4) goto L_0x0039
        L_0x016b:
            int[] r0 = r9.fS
            r1 = 16
            r0 = r0[r1]
            if (r0 != r7) goto L_0x0177
            int r0 = r9.gj
            r9.gi = r0
        L_0x0177:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 21
            r0 = r0[r1]
            if (r0 != 0) goto L_0x01a4
            int r0 = r9.ep
            r1 = 100
            if (r0 <= r1) goto L_0x01a4
            r0 = 58
            r9.hp = r0
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 21
            r0[r1] = r7
            r9.aj()
            goto L_0x0039
        L_0x01a0:
            r9.iR = r6
            goto L_0x00fe
        L_0x01a4:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 22
            r0 = r0[r1]
            if (r0 != 0) goto L_0x01cb
            boolean r0 = r9.jV
            if (r0 != r7) goto L_0x01cb
            r0 = 59
            r9.hp = r0
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 22
            r0[r1] = r7
            r9.aj()
            r9.jV = r6
        L_0x01cb:
            int r0 = r9.jP
            int r1 = r9.iQ
            if (r0 <= r1) goto L_0x01d5
            int r0 = r9.jP
            r9.iQ = r0
        L_0x01d5:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 22
            r0 = r0[r1]
            if (r0 != 0) goto L_0x01eb
            int r0 = r9.jP
            r1 = 200(0xc8, float:2.8E-43)
            if (r0 < r1) goto L_0x01eb
            r9.jV = r7
        L_0x01eb:
            r9.P()
            boolean r0 = r9.gG
            if (r0 == 0) goto L_0x02a2
            int r0 = r9.dY
            if (r0 != 0) goto L_0x0207
            byte[] r0 = r9.hs
            byte r0 = r0[r6]
            if (r0 < 0) goto L_0x0207
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 10
            if (r0 <= r1) goto L_0x0207
            r9.k(r6)
        L_0x0207:
            int r0 = r9.dY
            if (r0 != r7) goto L_0x022d
            boolean r0 = r9.gS
            if (r0 != 0) goto L_0x022d
            byte[] r0 = r9.hs
            byte r0 = r0[r7]
            if (r0 < 0) goto L_0x0220
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 10
            if (r0 <= r1) goto L_0x0220
            r9.k(r7)
        L_0x0220:
            byte[] r0 = r9.hs
            byte r0 = r0[r8]
            if (r0 < 0) goto L_0x022d
            boolean r0 = r9.dS
            if (r0 == 0) goto L_0x022d
            r9.k(r8)
        L_0x022d:
            int r0 = r9.dY
            if (r0 != r8) goto L_0x0269
            byte[] r0 = r9.hs
            byte r0 = r0[r3]
            if (r0 < 0) goto L_0x0242
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 15
            if (r0 <= r1) goto L_0x0242
            r9.k(r3)
        L_0x0242:
            byte[] r0 = r9.hs
            byte r0 = r0[r3]
            if (r0 >= 0) goto L_0x0259
            byte[] r0 = r9.hs
            r1 = 4
            byte r0 = r0[r1]
            if (r0 < 0) goto L_0x0259
            boolean r0 = r9.ai()
            if (r0 == 0) goto L_0x0259
            r0 = 4
            r9.k(r0)
        L_0x0259:
            byte[] r0 = r9.hs
            r1 = 4
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x0269
            byte[] r0 = r9.hs
            byte r0 = r0[r4]
            if (r0 < 0) goto L_0x0269
            r9.k(r4)
        L_0x0269:
            byte[] r0 = r9.hs
            r1 = 7
            byte r0 = r0[r1]
            if (r0 < 0) goto L_0x027a
            int r0 = r9.gi
            int r1 = r9.gj
            if (r0 != r1) goto L_0x027a
            r0 = 7
            r9.k(r0)
        L_0x027a:
            r9.gG = r6
            r0 = r6
        L_0x027d:
            byte[] r1 = r9.hs
            int r1 = r1.length
            if (r0 >= r1) goto L_0x028b
            byte[] r1 = r9.hs
            byte r1 = r1[r0]
            r2 = -1
            if (r1 == r2) goto L_0x02f1
            r9.gG = r7
        L_0x028b:
            byte[] r0 = r9.hs
            r1 = 7
            byte r0 = r0[r1]
            if (r0 >= 0) goto L_0x02a2
            int r0 = r9.gi
            int r1 = r9.gj
            if (r0 != r1) goto L_0x02a2
            boolean r0 = r9.hY
            if (r0 != 0) goto L_0x02a2
            int r0 = r9.hX
            int r0 = r0 + 1
            r9.hX = r0
        L_0x02a2:
            r9.S()
            r9.T()
            r9.R()
            boolean r0 = r9.iN
            if (r0 != 0) goto L_0x02b2
            r9.ag()
        L_0x02b2:
            r9.V()
            r9.U()
            r9.X()
            r9.W()
            r9.Y()
            d r0 = r9.dp
            int r0 = r0.kH
            int[][] r1 = r9.ga
            r0 = r1[r0]
            int r1 = r9.gp
            r0[r7] = r1
            int r0 = r9.dO
            switch(r0) {
                case 0: goto L_0x02f4;
                case 1: goto L_0x0324;
                case 2: goto L_0x0375;
                case 3: goto L_0x03a1;
                case 4: goto L_0x03dc;
                case 5: goto L_0x045b;
                default: goto L_0x02d2;
            }
        L_0x02d2:
            int r0 = r9.hu
            if (r0 != r7) goto L_0x04a2
            boolean r0 = r9.hI
            if (r0 != 0) goto L_0x04a2
            int r0 = r9.gP
            if (r0 <= 0) goto L_0x04a2
            r9.gS = r7
            r9.gT = r6
            r9.gU = r6
            r9.gV = r6
            r9.hI = r7
        L_0x02e8:
            int r0 = r9.en
            int r1 = r9.eo
            int r0 = r0 + r1
            r9.ep = r0
            goto L_0x0047
        L_0x02f1:
            int r0 = r0 + 1
            goto L_0x027d
        L_0x02f4:
            boolean r0 = r9.hE
            if (r0 == 0) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cz
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.eH
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.hz
            int r1 = r1 - r8
            if (r0 != r1) goto L_0x02d2
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x02d2
            r0 = 56
            r9.hp = r0
            int r0 = r9.gp
            int r0 = r0 + 60
            r9.gp = r0
            r9.cz = r7
            goto L_0x02d2
        L_0x0324:
            int r0 = r9.dY
            if (r0 != r3) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.gY
            if (r0 != 0) goto L_0x035d
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 400(0x190, float:5.6E-43)
            if (r0 <= r1) goto L_0x035d
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            if (r0 >= 0) goto L_0x035d
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 24
            r0 = r0[r1]
            if (r0 == r7) goto L_0x0039
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 24
            r0[r1] = r7
            r9.aj()
        L_0x035d:
            boolean r0 = r9.cz
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            if (r0 >= 0) goto L_0x02d2
            boolean r0 = r9.gY
            if (r0 != 0) goto L_0x02d2
            r0 = 43
            r9.gP = r0
            r9.cz = r7
            goto L_0x02d2
        L_0x0375:
            int r0 = r9.dY
            r1 = 4
            if (r0 != r1) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cy
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.eH
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.hz
            int r1 = r1 - r8
            if (r0 != r1) goto L_0x02d2
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 300(0x12c, float:4.2E-43)
            if (r0 >= r1) goto L_0x02d2
            r0 = 57
            r9.hp = r0
            r9.cy = r7
            goto L_0x02d2
        L_0x03a1:
            int r0 = r9.dY
            r1 = 4
            if (r0 != r1) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cz
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.kG
            int r1 = r1 * 25
            int r1 = r1 / 100
            if (r0 >= r1) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.kG
            int r1 = r1 * 25
            int r1 = r1 / 100
            r0.kF = r1
            r9.hu = r7
            r0 = 44
            r9.gP = r0
            r9.gY = r7
            r9.cz = r7
            goto L_0x02d2
        L_0x03dc:
            int r0 = r9.dY
            if (r0 != 0) goto L_0x0406
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cz
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.kG
            int r1 = r1 * 50
            int r1 = r1 / 100
            if (r0 >= r1) goto L_0x02d2
            r9.hu = r7
            r0 = 49
            r9.gP = r0
            r9.gY = r7
            r9.cz = r7
            goto L_0x02d2
        L_0x0406:
            int r0 = r9.dY
            if (r0 != r8) goto L_0x0430
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cy
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.kG
            int r1 = r1 * 40
            int r1 = r1 / 100
            if (r0 >= r1) goto L_0x02d2
            r9.hu = r7
            r0 = 50
            r9.gP = r0
            r9.gY = r7
            r9.cy = r7
            goto L_0x02d2
        L_0x0430:
            int r0 = r9.dY
            r1 = 4
            if (r0 != r1) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cB
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            d[] r1 = r9.dq
            r1 = r1[r6]
            int r1 = r1.kG
            int r1 = r1 * 30
            int r1 = r1 / 100
            if (r0 >= r1) goto L_0x02d2
            r9.hu = r7
            r0 = 51
            r9.gP = r0
            r9.gY = r7
            r9.cB = r7
            goto L_0x02d2
        L_0x045b:
            int r0 = r9.dY
            if (r0 != r8) goto L_0x02d2
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x02d2
            boolean r0 = r9.cz
            if (r0 != 0) goto L_0x02d2
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            if (r0 >= 0) goto L_0x02d2
            r9.hu = r7
            r0 = 52
            r9.gP = r0
            r9.gY = r7
            r0 = r6
        L_0x0478:
            short[] r1 = r9.hd
            int r1 = r1.length
            if (r0 >= r1) goto L_0x049c
            short[][][][] r1 = r9.hh
            r1 = r1[r4]
            r1 = r1[r6]
            r1 = r1[r7]
            short[] r2 = r9.hd
            short r2 = r2[r0]
            r1[r0] = r2
            short[][][][] r1 = r9.hh
            r1 = r1[r4]
            r1 = r1[r6]
            r1 = r1[r8]
            short[] r2 = r9.hf
            short r2 = r2[r0]
            r1[r0] = r2
            int r0 = r0 + 1
            goto L_0x0478
        L_0x049c:
            r9.cA = r7
            r9.cz = r7
            goto L_0x02d2
        L_0x04a2:
            int r0 = r9.gP
            if (r0 < 0) goto L_0x02e8
            boolean r0 = r9.gS
            if (r0 != 0) goto L_0x02e8
            byte[][] r0 = defpackage.g.lL
            int r1 = r9.gP
            r0 = r0[r1]
            byte r0 = r0[r8]
            switch(r0) {
                case 0: goto L_0x04e5;
                case 1: goto L_0x0522;
                case 2: goto L_0x0533;
                default: goto L_0x04b5;
            }
        L_0x04b5:
            boolean r0 = r9.gS
            if (r0 == 0) goto L_0x02e8
            r9.gT = r6
            r9.gU = r6
            r9.gV = r6
            int r0 = r9.gR
            int r0 = r0 + 1
            byte[][][] r1 = defpackage.g.lN
            int r2 = r9.dO
            r1 = r1[r2]
            int r2 = r9.dY
            r1 = r1[r2]
            int r1 = r1.length
            int r1 = r1 - r7
            if (r0 >= r1) goto L_0x054d
            byte[][][] r0 = defpackage.g.lN
            int r1 = r9.dO
            r0 = r0[r1]
            int r1 = r9.dY
            r0 = r0[r1]
            int r1 = r9.gR
            int r1 = r1 + 1
            byte r0 = r0[r1]
            r9.gQ = r0
            goto L_0x02e8
        L_0x04e5:
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x04b5
            d[] r0 = r9.dq
            byte[][] r1 = defpackage.g.lL
            int r2 = r9.gP
            r1 = r1[r2]
            byte r1 = r1[r6]
            r0 = r0[r1]
            int r0 = r0.kF
            byte[][] r1 = defpackage.g.lL
            int r2 = r9.gP
            r1 = r1[r2]
            byte r1 = r1[r3]
            int r1 = r1 * 10
            if (r0 >= r1) goto L_0x04b5
            r9.gS = r7
            d[] r0 = r9.dq
            byte[][] r1 = defpackage.g.lL
            int r2 = r9.gP
            r1 = r1[r2]
            byte r1 = r1[r6]
            r0 = r0[r1]
            d[] r1 = r9.dq
            byte[][] r2 = defpackage.g.lL
            int r3 = r9.gP
            r2 = r2[r3]
            byte r2 = r2[r6]
            r1 = r1[r2]
            r1.z = r8
            r0.kF = r8
            goto L_0x04b5
        L_0x0522:
            int[] r0 = r9.aK
            r0 = r0[r6]
            byte[][] r1 = defpackage.g.lL
            int r2 = r9.gP
            r1 = r1[r2]
            byte r1 = r1[r3]
            if (r0 <= r1) goto L_0x04b5
            r9.gS = r7
            goto L_0x04b5
        L_0x0533:
            d[] r0 = r9.dq
            if (r0 == 0) goto L_0x04b5
            d[] r0 = r9.dq
            r0 = r0[r6]
            int r0 = r0.kF
            byte[][] r1 = defpackage.g.lL
            int r2 = r9.gP
            r1 = r1[r2]
            byte r1 = r1[r3]
            int r1 = r1 * 100
            if (r0 >= r1) goto L_0x04b5
            r9.gS = r7
            goto L_0x04b5
        L_0x054d:
            r0 = -1
            r9.gQ = r0
            goto L_0x02e8
        L_0x0552:
            r9.P()
            r9.S()
            r9.T()
            r9.R()
            boolean r0 = r9.iN
            if (r0 != 0) goto L_0x0565
            r9.ag()
        L_0x0565:
            r9.V()
            r9.U()
            r9.X()
            r9.W()
            r9.Y()
            int r0 = r9.en
            int r1 = r9.eo
            int r0 = r0 + r1
            r9.ep = r0
            goto L_0x0047
        L_0x057d:
            r9.bz = r6
            r0 = r6
        L_0x0580:
            if (r0 >= r4) goto L_0x0593
            boolean[] r1 = r9.iK
            boolean r1 = r1[r0]
            if (r1 != r7) goto L_0x0590
            int[] r1 = r9.iM
            r2 = r1[r0]
            int r2 = r2 + 1
            r1[r0] = r2
        L_0x0590:
            int r0 = r0 + 1
            goto L_0x0580
        L_0x0593:
            int r0 = r9.cJ
            if (r0 != 0) goto L_0x05a0
            int r0 = r9.eP
            if (r0 <= 0) goto L_0x064a
            int r0 = r9.eP
            int r0 = r0 - r7
            r9.eP = r0
        L_0x05a0:
            int r0 = r9.cJ
            if (r0 != r4) goto L_0x0047
            int r0 = r9.eR
            if (r0 <= 0) goto L_0x05cc
            int r0 = r9.eR
            int r0 = r0 - r7
            r9.eR = r0
            int r0 = r9.eR
            if (r0 != r4) goto L_0x05bd
            int r0 = r9.eQ
            r1 = -1
            if (r0 != r1) goto L_0x05bd
            int r0 = r9.gJ
            if (r0 != 0) goto L_0x064e
            r0 = r7
        L_0x05bb:
            r9.gJ = r0
        L_0x05bd:
            int r0 = r9.eR
            if (r0 != r7) goto L_0x05cc
            int r0 = r9.eQ
            if (r0 != r7) goto L_0x05cc
            int r0 = r9.gJ
            if (r0 != 0) goto L_0x0651
            r0 = r7
        L_0x05ca:
            r9.gJ = r0
        L_0x05cc:
            boolean r0 = r9.bt
            if (r0 != r7) goto L_0x0047
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 20
            if (r0 != r1) goto L_0x05eb
            int r0 = r9.gw
            int r1 = r9.dO
            if (r0 > r3) goto L_0x05e8
            int[] r2 = r9.fS
            r2 = r2[r1]
            if (r2 >= r0) goto L_0x05e8
            int[] r2 = r9.fS
            r2[r1] = r0
        L_0x05e8:
            r9.aj()
        L_0x05eb:
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 24
            if (r0 != r1) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 16
            r0 = r0[r1]
            if (r0 != 0) goto L_0x0047
            int[] r0 = r9.fS
            r0 = r0[r6]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r0 = r0[r7]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r0 = r0[r8]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r0 = r0[r3]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 4
            r0 = r0[r1]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r0 = r0[r4]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 6
            r0 = r0[r1]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 7
            r0 = r0[r1]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 8
            r0 = r0[r1]
            if (r0 != r3) goto L_0x0047
            int[] r0 = r9.fS
            r1 = 16
            r0 = r0[r1]
            if (r0 != 0) goto L_0x0047
            r0 = 71
            r9.hp = r0
            int[] r0 = r9.fS
            r1 = 16
            r0[r1] = r7
            goto L_0x0047
        L_0x064a:
            r9.eO = r6
            goto L_0x05a0
        L_0x064e:
            r0 = r6
            goto L_0x05bb
        L_0x0651:
            r0 = r6
            goto L_0x05ca
        L_0x0654:
            int r0 = r9.eV
            int r1 = r9.eU
            if (r0 >= r1) goto L_0x0047
            int r0 = r9.eV
            int r0 = r0 + 1
            r9.eV = r0
            goto L_0x0047
        L_0x0662:
            int r0 = r9.eR
            if (r0 <= 0) goto L_0x0047
            int r0 = r9.eR
            int r0 = r0 - r7
            r9.eR = r0
            int r0 = r9.eR
            if (r0 != r4) goto L_0x067b
            int r0 = r9.eQ
            r1 = -1
            if (r0 != r1) goto L_0x067b
            int r0 = r9.gC
            if (r0 != 0) goto L_0x068c
            r0 = r7
        L_0x0679:
            r9.gC = r0
        L_0x067b:
            int r0 = r9.eR
            if (r0 != r7) goto L_0x0047
            int r0 = r9.eQ
            if (r0 != r7) goto L_0x0047
            int r0 = r9.gC
            if (r0 != 0) goto L_0x068e
            r0 = r7
        L_0x0688:
            r9.gC = r0
            goto L_0x0047
        L_0x068c:
            r0 = r6
            goto L_0x0679
        L_0x068e:
            r0 = r6
            goto L_0x0688
        L_0x0690:
            boolean r0 = r9.gG
            if (r0 == 0) goto L_0x06a6
            byte[] r0 = r9.hs
            r1 = 6
            byte r0 = r0[r1]
            if (r0 < 0) goto L_0x06a6
            byte[] r0 = r9.hs
            byte r0 = r0[r4]
            r1 = -1
            if (r0 != r1) goto L_0x06a6
            r0 = 6
            r9.k(r0)
        L_0x06a6:
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            r1 = 20
            if (r0 != r1) goto L_0x06f3
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r3]
            r1 = 20
            if (r0 != r1) goto L_0x06f3
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 4
            r0 = r0[r1]
            r1 = 20
            if (r0 != r1) goto L_0x06f3
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 12
            r0 = r0[r1]
            r1 = 20
            if (r0 >= r1) goto L_0x06ee
            int[][] r0 = r9.ga
            d r1 = r9.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r4]
            r1 = 4
            if (r0 < r1) goto L_0x06f3
        L_0x06ee:
            r0 = r7
        L_0x06ef:
            r9.hW = r0
            goto L_0x0047
        L_0x06f3:
            r0 = r6
            goto L_0x06ef
        L_0x06f5:
            int[] r0 = r9.aK
            r0 = r0[r6]
            if (r0 <= r8) goto L_0x0047
            r0 = 19
            r9.ci = r0
            java.lang.String r0 = "save"
            r1 = 1
            ar r0 = defpackage.ar.a(r0, r1)     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            r9.ks = r0     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            ar r0 = r9.ks     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            int r0 = r0.bo()     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            if (r0 > 0) goto L_0x0721
            r0 = 1
            byte[] r0 = new byte[r0]     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            r1 = 0
            r2 = 0
            r0[r1] = r2     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            ar r1 = r9.ks     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            r2 = 0
            int r3 = r0.length     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            r1.b(r0, r2, r3)     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
            r9.aj()     // Catch:{ Exception -> 0x0757, all -> 0x0760 }
        L_0x0721:
            ar r0 = r9.ks     // Catch:{ Exception -> 0x084b }
            r0.bn()     // Catch:{ Exception -> 0x084b }
        L_0x0726:
            java.lang.String r0 = "save"
            r1 = 1
            ar r0 = defpackage.ar.a(r0, r1)     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r9.ks = r0     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            ar r0 = r9.ks     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r1 = 1
            r0.F(r1)     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            ar r0 = r9.ks     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r1 = 1
            byte[] r0 = r0.G(r1)     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            java.io.ByteArrayInputStream r1 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r2 = r6
        L_0x0747:
            int[] r3 = r9.fS     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r3 = r3.length     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            if (r2 >= r3) goto L_0x0767
            int[] r3 = r9.fS     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r4 = r0.readInt()     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r3[r2] = r4     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r2 = r2 + 1
            goto L_0x0747
        L_0x0757:
            r0 = move-exception
            ar r0 = r9.ks     // Catch:{ Exception -> 0x075e }
            r0.bn()     // Catch:{ Exception -> 0x075e }
            goto L_0x0726
        L_0x075e:
            r0 = move-exception
            goto L_0x0726
        L_0x0760:
            r0 = move-exception
            ar r1 = r9.ks     // Catch:{ Exception -> 0x0848 }
            r1.bn()     // Catch:{ Exception -> 0x0848 }
        L_0x0766:
            throw r0
        L_0x0767:
            r2 = r6
        L_0x0768:
            int[][] r3 = r9.ga     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r3 = r3.length     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            if (r2 >= r3) goto L_0x0785
            r3 = r6
        L_0x076e:
            int[][] r4 = r9.ga     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r4 = r4[r2]     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r4 = r4.length     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            if (r3 >= r4) goto L_0x0782
            int[][] r4 = r9.ga     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r4 = r4[r2]     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r5 = r0.readInt()     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r4[r3] = r5     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            int r3 = r3 + 1
            goto L_0x076e
        L_0x0782:
            int r2 = r2 + 1
            goto L_0x0768
        L_0x0785:
            r0.close()     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            r1.close()     // Catch:{ Exception -> 0x0797, all -> 0x07a0 }
            ar r0 = r9.ks     // Catch:{ Exception -> 0x0845 }
            r0.bn()     // Catch:{ Exception -> 0x0845 }
        L_0x0790:
            f r0 = r9.aI
            r0.w()
            goto L_0x0047
        L_0x0797:
            r0 = move-exception
            ar r0 = r9.ks     // Catch:{ Exception -> 0x079e }
            r0.bn()     // Catch:{ Exception -> 0x079e }
            goto L_0x0790
        L_0x079e:
            r0 = move-exception
            goto L_0x0790
        L_0x07a0:
            r0 = move-exception
            ar r1 = r9.ks     // Catch:{ Exception -> 0x0842 }
            r1.bn()     // Catch:{ Exception -> 0x0842 }
        L_0x07a6:
            throw r0
        L_0x07a7:
            int[] r0 = r9.aK
            r0 = r0[r6]
            int r0 = r0 - r7
            int[] r1 = r9.fx
            int r1 = r1.length
            if (r0 >= r1) goto L_0x07c1
            f r0 = r9.aI
            int[] r1 = r9.fx
            int[] r2 = r9.aK
            r2 = r2[r6]
            int r2 = r2 - r7
            r1 = r1[r2]
            r0.k(r1)
            goto L_0x0047
        L_0x07c1:
            f r0 = r9.aI
            r0.ar()
            r0 = 26
            r9.d(r0, r8)
            goto L_0x0047
        L_0x07cd:
            int r0 = r9.eR
            if (r0 <= 0) goto L_0x07fb
            int r0 = r9.eR
            int r0 = r0 - r7
            r9.eR = r0
            int r0 = r9.eR
            if (r0 != r4) goto L_0x07e6
            int r0 = r9.eQ
            r1 = -1
            if (r0 != r1) goto L_0x07e6
            int r0 = r9.dG
            if (r0 != 0) goto L_0x07f7
            r0 = r7
        L_0x07e4:
            r9.dG = r0
        L_0x07e6:
            int r0 = r9.eR
            if (r0 != r7) goto L_0x0047
            int r0 = r9.eQ
            if (r0 != r7) goto L_0x0047
            int r0 = r9.dG
            if (r0 != 0) goto L_0x07f9
            r0 = r7
        L_0x07f3:
            r9.dG = r0
            goto L_0x0047
        L_0x07f7:
            r0 = r6
            goto L_0x07e4
        L_0x07f9:
            r0 = r6
            goto L_0x07f3
        L_0x07fb:
            r9.eQ = r6
            goto L_0x0047
        L_0x07ff:
            int r0 = r9.gT
            int r0 = r0 + 1
            r9.gT = r0
            boolean r0 = r9.bz
            if (r0 == 0) goto L_0x0047
            r9.d(r4, r6)
            goto L_0x0047
        L_0x080e:
            int r0 = r9.eW
            r1 = 120(0x78, float:1.68E-43)
            if (r0 > r1) goto L_0x081a
            int r0 = r9.eW
            int r0 = r0 + 3
            r9.eW = r0
        L_0x081a:
            int[] r0 = r9.aK
            r0 = r0[r6]
            r1 = 140(0x8c, float:1.96E-43)
            if (r0 <= r1) goto L_0x0047
            r9.d(r4, r6)
            r9.cJ = r6
            r9.db = r6
            r9.eW = r6
            goto L_0x0047
        L_0x082d:
            byte[] r0 = r9.hs
            byte r0 = r0[r6]
            if (r0 >= 0) goto L_0x0047
            byte[] r0 = r9.hs
            r1 = 10
            byte r0 = r0[r1]
            if (r0 < 0) goto L_0x0047
            r0 = 10
            r9.k(r0)
            goto L_0x0047
        L_0x0842:
            r1 = move-exception
            goto L_0x07a6
        L_0x0845:
            r0 = move-exception
            goto L_0x0790
        L_0x0848:
            r1 = move-exception
            goto L_0x0766
        L_0x084b:
            r0 = move-exception
            goto L_0x0726
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.Q():void");
    }

    private void R() {
        this.dS = ai();
        if (this.gG) {
            if (this.dY == 2) {
                this.dS = this.dS && this.hs[8] < 0 && this.hs[6] < 0;
            } else if (this.dY == 0) {
                this.dS = this.dS && this.hs[10] < 0;
            }
        }
        if (this.ci != 8 || g.lK[this.dO][this.dY][3] < 210) {
            this.gs = this.gr[1];
            this.gt = this.gr[1];
        } else {
            this.gs = this.gr[0];
            this.gt = this.gr[0];
        }
        if (this.ci == 9) {
            this.gs = this.gr[1];
            this.gt = this.gr[1];
        }
        this.eC = (this.aR / 2) - this.dp.kD;
        this.eD = ((((this.aS - this.gs) - this.gt) / 2) - this.dp.eg) + 30;
        if (this.eC > (-this.eb)) {
            this.eC = -this.eb;
        }
        if (this.eD > (-this.ea)) {
            this.eD = -this.ea;
        }
        if (this.eC < this.aR - this.ec) {
            this.eC = this.aR - this.ec;
        }
        if (this.eD < ((this.aS - this.gs) - this.gt) - this.ed) {
            this.eD = ((this.aS - this.gs) - this.gt) - this.ed;
        }
        if (this.dp != null) {
            this.dp.dP = this.dp.kJ;
            if (this.dp.kF < this.dp.kG / 5) {
                this.gi += this.dp.kH == 0 ? 6 : 8;
                if (this.gi > this.gj) {
                    this.gi = this.gj;
                }
            }
            if (this.dp.z > this.dp.kF) {
                int i = (this.dp.z - this.dp.kF) / 10;
                if (i < 4) {
                    i = 4;
                }
                this.dp.z -= i;
            } else {
                this.dp.z = this.dp.kF;
            }
            this.dp.kO = this.gS;
            if (this.hk) {
                if (this.dp.kC == this.hl[this.dp.kH][0] && this.dp.lf == 0) {
                    this.iN = true;
                }
                if (this.hm >= this.hl[this.dp.kH].length) {
                    this.hk = false;
                    this.bs = -1;
                } else {
                    this.dp.dd = this.hl[this.dp.kH][this.hm];
                    if (this.dp.kC == this.hl[this.dp.kH][this.hm] && this.dp.lf == 1 && this.dp.le == 0) {
                        this.hm++;
                    }
                }
            }
            this.dp.e(this.dp.dd);
            if (this.gG && this.hs[3] < 0 && this.hs[8] > 8) {
                if ((this.dp.kC == 20 || this.dp.kC == 21 || this.dp.kC == 23) && !this.hk && this.dp.lf >= this.dp.kB[this.dp.kC].length - 1) {
                    this.hH = true;
                }
                if (this.hH) {
                    this.hF++;
                }
                if (this.hF > 10) {
                    k(8);
                    this.hH = false;
                    this.hF = 0;
                }
            }
            if (this.gG && this.hs[8] < 0 && this.hs[11] > 0) {
                k(11);
            }
            if (this.hb) {
                d dVar = this.dp;
                d dVar2 = this.dp;
                int i2 = this.dp.kG;
                dVar2.kF = i2;
                dVar.kQ = i2;
            }
            if ((this.dp.kC == 14 && this.dp.lf == this.dp.kB[this.dp.kC].length - 2 && this.dp.le == 0) || ((this.dp.kC == 18 && this.dp.lf == this.dp.kB[this.dp.kC].length - 2 && this.dp.le == 0) || (this.dp.kC == 23 && this.dp.lf == this.dp.kB[this.dp.kC].length - 2 && this.dp.le == 0))) {
                this.gB = 0;
            }
            if (!a(this.dp)) {
                this.dp.ak();
            }
            if (!this.iN) {
                a(this.dp, -1);
            }
            if (this.dq != null) {
                for (int i3 = 0; i3 < this.dq.length; i3++) {
                    this.dq[i3].kO = this.gS;
                    if (!this.dq[i3].kO && this.gY) {
                        this.dq[i3].ix = (this.dq[i3].kR / 3) % 2 == 0;
                        if (this.dq[i3].kR < 0) {
                            this.dq[i3].ix = false;
                            this.dq[i3].kF = -1;
                            this.dq[i3].eH = -1;
                        }
                    }
                    if (this.dW) {
                        this.dq[i3].kF = -1;
                    }
                    this.dq[i3].li = this.hk;
                    if (this.dq[i3].kC == 6 && this.dq[i3].kF <= 0 && this.dq[i3].kR < -10) {
                        this.dq[i3].kC = 0;
                        this.dq[i3].dd = 8;
                    }
                    this.dq[i3].e(this.dq[i3].dd);
                    if (!a(this.dq[i3])) {
                        this.dq[i3].ak();
                    }
                    if (!this.iN) {
                        a(this.dq[i3], i3);
                    }
                }
            }
        }
    }

    private void S() {
        if (this.dN != this.dK) {
            int i = this.bs;
            if (i != -1) {
                if (this.bu[i] < 0) {
                    this.bu[i] = 0;
                }
                if (this.bu[i] < 20) {
                    int[] iArr = this.bu;
                    iArr[i] = iArr[i] + 1;
                }
            }
            for (int i2 = 0; i2 < this.bu.length; i2++) {
                if (i2 != i) {
                    if (this.bu[i2] > 0) {
                        this.bu[i2] = 0;
                    }
                    if (this.bu[i2] > -20) {
                        int[] iArr2 = this.bu;
                        iArr2[i2] = iArr2[i2] - 1;
                    }
                }
            }
            int i3 = this.bs;
            if (this.dN != this.dK && this.dp.kF > 0 && !this.gS && (!this.hk || i3 == 2 || i3 == 3)) {
                switch (i3) {
                    case -1:
                        if (this.dp.kC != 9) {
                            if (this.dp.kC != 10) {
                                if (this.dp.kC != 11) {
                                    if (this.dp.kC != 12) {
                                        if (this.dp.kC != 13) {
                                            this.dp.dd = 0;
                                            break;
                                        } else {
                                            this.dp.dd = 29;
                                            break;
                                        }
                                    } else {
                                        this.dp.dd = 28;
                                        break;
                                    }
                                } else {
                                    this.dp.dd = 27;
                                    break;
                                }
                            } else {
                                this.dp.dd = 26;
                                break;
                            }
                        } else {
                            this.dp.dd = 25;
                            break;
                        }
                    case 0:
                        this.dp.dd = 2;
                        this.dp.kI = 1;
                        if (this.dp.kC == 1) {
                            this.dp.kC = 2;
                            break;
                        }
                        break;
                    case 1:
                        this.dp.dd = 2;
                        this.dp.kI = 0;
                        if (this.dp.kC == 1) {
                            this.dp.kC = 2;
                            break;
                        }
                        break;
                    case 2:
                        this.dp.dd = 1;
                        if ((!a(this.dp.kC, this.et) && this.dp.kC != 6) || this.hk) {
                            this.dp.kJ = 1;
                        }
                        if (this.dp.kC == 2) {
                            this.dp.kC = 1;
                            break;
                        }
                        break;
                    case 3:
                        this.dp.dd = 1;
                        if ((!a(this.dp.kC, this.et) && this.dp.kC != 6) || this.hk) {
                            this.dp.kJ = 0;
                        }
                        if (this.dp.kC == 2) {
                            this.dp.kC = 1;
                            break;
                        }
                        break;
                    case 4:
                        if (this.dp.kC != 9 && this.dp.kC != 25) {
                            if (this.dp.kC != 10 && this.dp.kC != 26) {
                                if (this.dp.kC != 11 && this.dp.kC != 27) {
                                    if (this.dp.kC != 12 && this.dp.kC != 28) {
                                        if (this.dp.kC != 13 && this.dp.kC != 29) {
                                            this.dp.dd = 9;
                                            break;
                                        } else {
                                            this.dp.dd = 14;
                                            break;
                                        }
                                    } else {
                                        this.dp.dd = 13;
                                        break;
                                    }
                                } else {
                                    this.dp.dd = 12;
                                    break;
                                }
                            } else {
                                this.dp.dd = 11;
                                break;
                            }
                        } else {
                            this.dp.dd = 10;
                            break;
                        }
                        break;
                    case 11:
                        if (this.dp.kC != 9 && this.dp.kC != 25) {
                            if (this.dp.kC != 10 && this.dp.kC != 26) {
                                if (this.dp.kC != 11 && this.dp.kC != 27) {
                                    this.dp.dd = 0;
                                    break;
                                } else {
                                    this.dp.dd = 23;
                                    break;
                                }
                            } else {
                                this.dp.dd = 21;
                                break;
                            }
                        } else {
                            this.dp.dd = 20;
                            break;
                        }
                        break;
                    case 13:
                        if (this.gi != this.gj) {
                            this.dp.dd = 0;
                            break;
                        } else {
                            this.gi = 0;
                            this.hk = true;
                            if (this.dq != null) {
                                for (d dVar : this.dq) {
                                    dVar.lp = true;
                                }
                            }
                            this.hm = 0;
                            this.dp.dd = this.hl[this.dp.kH][0];
                            this.hX = 0;
                            this.hY = true;
                            break;
                        }
                }
                if (this.ii >= 2) {
                    this.gi = this.gj;
                }
            }
            if (!this.dA) {
                this.dA = true;
            }
        }
    }

    private void T() {
        if (this.dp != null && this.dp.ix && !this.gS) {
            b(this.dp);
            if (this.dq != null) {
                for (int i = 0; i < this.dq.length; i++) {
                    b(this.dq[i]);
                    if (this.jY[this.dO][this.dY][0] > 0) {
                        if (this.dq[i].kD > this.jZ && this.dq[i].kD < this.jZ + 30 && this.dq[i].eg < this.kb) {
                            this.dq[i].kD = this.jZ;
                        }
                        if (this.dq[i].kD < this.ka && this.dq[i].kD > this.ka - 30 && this.dq[i].eg < this.kb) {
                            this.dq[i].kD = this.ka;
                        }
                        if (this.dq[i].kD > this.kc && this.dq[i].kD < this.kc + 30 && this.dq[i].eg > this.kd) {
                            this.dq[i].kD = this.kc;
                        }
                        if (this.dq[i].kD < this.ke && this.dq[i].kD > this.ke - 30 && this.dq[i].eg > this.kd) {
                            this.dq[i].kD = this.ke;
                        }
                        if (this.dq[i].eg < this.kb + 10 && this.dq[i].kD > this.jZ && this.dq[i].kD < this.ka) {
                            this.dq[i].eg = this.kb + 10;
                        }
                        if (this.dq[i].eg > this.kd - 10 && this.dq[i].kD > this.kc && this.dq[i].kD < this.ke) {
                            this.dq[i].eg = this.kd - 10;
                        }
                    }
                    if (this.dq[i].ix && this.dq[i].kC != 8 && this.dq[i].eH >= this.dq[i].hz && this.dq[i].jC != 8) {
                        int n = n(this.dq[i].eg - this.dp.eg);
                        int n2 = n(this.dq[i].kD - this.dp.kD);
                        if (this.dq[i].kC <= 2) {
                            if (this.dq[i].ee > this.dp.kD) {
                                this.dq[i].dP = 1;
                            } else if (this.dq[i].hy < this.dp.kD) {
                                this.dq[i].dP = 0;
                            }
                        }
                        if (this.hu <= 0) {
                            switch (this.dq[i].kH) {
                                case 2:
                                    if (this.dq[i].kF < (this.dq[i].kG * 30) / 100) {
                                        this.hu = 1;
                                        if (this.dO == 1) {
                                            this.gP = 45;
                                        } else {
                                            this.gP = 53;
                                        }
                                        this.dq[i].ld = false;
                                        this.dq[i].lc = true;
                                        d dVar = this.dq[i];
                                        d dVar2 = this.dq[i];
                                        int i2 = (this.dq[i].kG * 30) / 100;
                                        dVar2.z = i2;
                                        dVar.kF = i2;
                                        break;
                                    }
                                    break;
                                case 3:
                                    if (!this.jJ && this.aK[0] == 400) {
                                        this.hu = 1;
                                        this.gP = 42;
                                        this.jJ = true;
                                    }
                                    if (this.dq[i].kF < (this.dq[i].kG * 80) / 100) {
                                        this.dq[i].kQ *= 3;
                                        if (!this.jK) {
                                            this.hp = 62;
                                            this.jK = true;
                                        }
                                    }
                                    if (this.dq[i].kF < (this.dq[i].kG * 50) / 100) {
                                        this.dq[i].kQ *= 6;
                                        if (!this.jI) {
                                            this.hp = 67;
                                            this.jI = true;
                                            break;
                                        }
                                    }
                                    break;
                                case 4:
                                    if (this.dq[i].kF < (this.dq[i].kG * 30) / 100) {
                                        this.hu = 1;
                                        this.gP = 46;
                                        this.dq[i].ld = false;
                                        this.dq[i].lc = true;
                                        d dVar3 = this.dq[i];
                                        d dVar4 = this.dq[i];
                                        int i3 = (this.dq[i].kG * 30) / 100;
                                        dVar4.z = i3;
                                        dVar3.kF = i3;
                                        break;
                                    }
                                    break;
                                case 5:
                                    if (this.dq[i].kF < (this.dq[i].kG * 30) / 100) {
                                        this.hu = 1;
                                        if (this.dO == 2) {
                                            this.gP = 47;
                                        } else {
                                            this.gP = 48;
                                        }
                                        this.dq[i].ld = false;
                                        this.dq[i].lc = true;
                                        break;
                                    }
                                    break;
                            }
                        }
                        int i4 = this.hu;
                        int n3 = n(this.f0do.nextInt());
                        int i5 = (this.dO * 1) + 20;
                        if (this.gY && this.dq[i].jC != 8) {
                            this.dq[i].jC = 8;
                            this.dq[i].kR = d.dX;
                            this.dq[i].dd = 0;
                            this.dq[i].kJ = (this.dq[i].kJ + 1) % 2;
                            if (this.dq[i].kH > 9) {
                                if (this.dq[i].kF > 0) {
                                    this.en++;
                                }
                                int[] iArr = this.ga[this.dp.kH];
                                iArr[1] = iArr[1] + 5;
                            } else {
                                if (this.dq[i].kF > 0 && this.dO != 1) {
                                    this.eo++;
                                }
                                int[] iArr2 = this.ga[this.dp.kH];
                                iArr2[1] = iArr2[1] + 30;
                            }
                        }
                        if (this.dq[i].kR < 0) {
                            this.dq[i].jC = 0;
                        }
                        if (!(this.aK[0] >= 10 || this.dq[i].jC == 10 || this.dq[i].jC == 15 || this.dq[i].jC == 9 || this.dq[i].jC == 17 || this.dq[i].jC == 18)) {
                            this.dq[i].jC = 0;
                        }
                        if (!this.dq[i].lk) {
                            this.dq[i].jC = d.jo;
                        }
                        int i6 = n3 % 100;
                        switch (this.dq[i].jC) {
                            case 0:
                                this.dq[i].jC = this.dy[this.dq[i].kH][n(this.f0do.nextInt()) % this.dy[this.dq[i].kH].length];
                                this.dq[i].kR = d.dX;
                                if (this.dq[i].jC == 1) {
                                    this.dq[i].kR = (byte) this.dz[this.dq[i].kH][this.hu][2];
                                }
                                if (this.dq[i].jC == 2) {
                                    this.dq[i].kR = (byte) this.dz[this.dq[i].kH][this.hu][3];
                                    break;
                                } else {
                                    continue;
                                }
                            case 1:
                                this.dq[i].dd = 1;
                                if (this.dq[i].ee > this.dp.hy) {
                                    this.dq[i].kJ = 1;
                                } else if (this.dq[i].hy < this.dp.ee) {
                                    this.dq[i].kJ = 0;
                                }
                                a(i, n2, n, i6, i5);
                                if (this.dq[i].kR <= 0) {
                                    this.dq[i].jC = 0;
                                    break;
                                } else {
                                    continue;
                                }
                            case 2:
                                this.dq[i].dd = 2;
                                if (this.dq[i].jC == 2) {
                                    if (this.dq[i].eg - this.dp.eg > 6) {
                                        this.dq[i].kI = 1;
                                    } else if (this.dp.eg - this.dq[i].eg > 6) {
                                        this.dq[i].kI = 0;
                                    } else if (n(this.dq[i].eg - this.dp.eg) < 6) {
                                        this.dq[i].jC = 1;
                                    }
                                }
                                a(i, n2, n, i6, i5);
                                if (this.dq[i].kR <= 0) {
                                    this.dq[i].jC = 0;
                                    break;
                                } else {
                                    continue;
                                }
                            case 5:
                                this.dq[i].dd = 0;
                                a(i, n2, n, i6, i5);
                                if (this.dq[i].jC == 5 && this.dq[i].kR <= 0) {
                                    this.dq[i].jC = 0;
                                    break;
                                }
                            case 6:
                                switch (this.dq[i].kH) {
                                    case 2:
                                        this.dq[i].dd = 9;
                                        break;
                                    case 3:
                                        this.dq[i].dd = (i6 % 3) + 9;
                                        break;
                                    case 4:
                                        this.dq[i].dd = this.hu == 0 ? 9 : 11;
                                        break;
                                    case 5:
                                        this.dq[i].dd = 9;
                                        break;
                                    case 6:
                                    case 7:
                                        this.dq[i].dd = (i6 % 2) + 9;
                                        break;
                                    case 8:
                                    case 9:
                                        this.dq[i].dd = (i6 % 3) + 9;
                                        break;
                                    case 10:
                                    case 11:
                                        this.dq[i].dd = 9;
                                        break;
                                    case 12:
                                        this.dq[i].dd = 9;
                                        break;
                                }
                                if (this.dq[i].kR <= 0) {
                                    this.dq[i].jC = 5;
                                    this.dq[i].kR = 10;
                                    break;
                                } else {
                                    continue;
                                }
                            case 7:
                                switch (this.dq[i].kH) {
                                    case 2:
                                        this.dq[i].dd = 10;
                                        break;
                                    case 4:
                                        this.dq[i].dd = this.hu == 0 ? 10 : 12;
                                        break;
                                    case 5:
                                        this.dq[i].dd = this.hu == 0 ? 10 : 11;
                                        break;
                                    case 12:
                                        this.dq[i].dd = 10;
                                        break;
                                }
                                if (this.dq[i].kR <= 0) {
                                    this.dq[i].jC = 5;
                                    this.dq[i].kR = 10;
                                    break;
                                } else {
                                    continue;
                                }
                            case 8:
                                if (this.dq[i].kR < 0) {
                                    this.dq[i].ix = false;
                                    break;
                                } else {
                                    continue;
                                }
                            case 9:
                                this.dq[i].dd = 5;
                                this.dq[i].jC = 5;
                                if (this.hu > 0 && this.dq[i].kH < 7) {
                                    this.dq[i].jC = 7;
                                }
                                this.dq[i].kR = 10;
                                continue;
                            case 10:
                                byte[] bArr = {0, -1, -3, -5};
                                this.dq[i].dd = 6;
                                this.dq[i].kU = (this.dq[i].eg - this.dp.eg) / 5;
                                this.dq[i].kV = bArr[n(this.f0do.nextInt()) % bArr.length];
                                this.dq[i].jC = 0;
                                if (this.dq[i].kF <= 0) {
                                    this.dq[i].dd = 5;
                                    break;
                                } else {
                                    continue;
                                }
                            case 12:
                                if (this.dq[i].kR < 0) {
                                    this.dq[i].jC = 0;
                                    break;
                                } else {
                                    continue;
                                }
                            case 13:
                                if (this.dq[i].kH == 3) {
                                    this.dq[i].dd = 10;
                                }
                                this.dq[i].jC = 5;
                                continue;
                            case 14:
                                this.dq[i].dd = this.dq[i].ll;
                                this.dq[i].kJ = this.dq[i].lm;
                                this.dq[i].kI = this.dq[i].lm;
                                if (!a(this.eg, this.ee, this.eh, this.ei, this.dq[i].kD, this.dq[i].eg)) {
                                    if (this.dq[i].kR <= 0) {
                                        if (this.dq[i].kD <= this.eg || this.dq[i].kD >= this.eh) {
                                            this.dq[i].dd = 1;
                                            this.dq[i].kJ = this.dq[i].kD - this.dp.kD > 0 ? 1 : 0;
                                        } else if (this.dq[i].eg <= this.ee || this.dq[i].eg >= this.ei) {
                                            this.dq[i].dd = 2;
                                            this.dq[i].kI = this.dq[i].eg - this.dp.eg > 0 ? 1 : 0;
                                        }
                                        this.dq[i].kR = d.dX;
                                        break;
                                    } else {
                                        break;
                                    }
                                } else {
                                    this.dq[i].lk = true;
                                    this.dq[i].jC = 5;
                                    this.dq[i].kR = 5;
                                    continue;
                                }
                            case Database.INT_FEECUE:
                                this.dq[i].dd = 7;
                                this.dq[i].jC = 0;
                                if (this.dq[i].kF <= 0) {
                                    this.dq[i].dd = 5;
                                    break;
                                } else {
                                    continue;
                                }
                            case 16:
                                this.dq[i].dd = 11;
                                this.dq[i].jC = 5;
                                continue;
                            case Database.INT_FEES:
                                this.dq[i].dd = 32;
                                this.dq[i].jC = 0;
                                if (this.dq[i].kF <= 0) {
                                    this.dq[i].dd = 5;
                                    break;
                                } else {
                                    continue;
                                }
                            case 18:
                                this.dq[i].dd = 15;
                                this.dq[i].jC = 0;
                                continue;
                        }
                    }
                }
            }
        }
    }

    private void U() {
        if (this.gz <= 4) {
            if (!(this.dq == null || this.dp == null || this.dp.eH != this.dp.hz || this.dp.kC == 5 || this.dp.kC == 6)) {
                int i = -1;
                boolean z = false;
                for (int i2 = 0; i2 < this.dq.length; i2++) {
                    if (this.dq[i2].ix && this.dq[i2].kZ && this.dq[i2].jC != 8 && this.dq[i2].ee != this.dq[i2].hy) {
                        if (!a(this.dp, this.dq[i2]) || !a(this.dq[i2], this.dp) || this.dq[i2].kJ != (this.dp.kJ + 1) % 2 || this.hk) {
                            if (a(this.dp, this.dq[i2]) && a(this.dp.kC, this.et)) {
                                this.jO = true;
                                this.jQ = true;
                                this.jP++;
                                if (!this.hk) {
                                    this.gi += this.dp.kH == 0 ? 20 : 15;
                                    if (this.dp.kC <= 13) {
                                        this.jT = 0;
                                        if (!this.jL) {
                                            if (this.dq[i2].jC == 15) {
                                                this.dq[i2].jC = 10;
                                            } else {
                                                this.dq[i2].jC = 9;
                                            }
                                        } else if (this.jN <= this.jW[this.dp.kH]) {
                                            this.dq[i2].jC = d.jp;
                                            this.jM = true;
                                        } else {
                                            this.jN = 0;
                                            this.jM = false;
                                            this.jL = false;
                                        }
                                    } else if (this.dp.kC == 14) {
                                        this.jT = 0;
                                        this.dq[i2].jC = 10;
                                    } else if (this.dp.kC == 21) {
                                        this.jT = 0;
                                        if (!this.jM) {
                                            this.dq[i2].jC = d.jp;
                                            this.jL = true;
                                        } else {
                                            this.dq[i2].jC = 10;
                                        }
                                    } else if (this.dp.kC == 20) {
                                        this.dq[i2].jC = d.jr;
                                        this.jL = true;
                                        this.jM = true;
                                        this.jT = 1;
                                    } else if (this.dp.kC == 23) {
                                        this.jT = 0;
                                        this.dq[i2].jC = 10;
                                    }
                                } else if (this.dp.kC == 14 || this.dp.kC == 18) {
                                    this.dq[i2].jC = 10;
                                } else {
                                    this.dq[i2].jC = d.jp;
                                }
                                if (i == -1 || this.dq[i].kH > this.dq[i2].kH) {
                                    i = i2;
                                }
                                if ((!(this.dq[i2].kH == 4 || this.dq[i2].kH == 10 || this.dq[i2].kH == 11 || this.dq[i2].kH == 8 || this.dq[i2].kH == 9) || (this.dO == 3 && this.dY == 6 && this.dq[i2].kH == 9)) && n(this.f0do.nextInt()) % 100 < 10 && !this.jL) {
                                    this.dq[i2].jC = d.bo;
                                }
                                if (this.dq[i2].kH == 3 && this.dq[i2].kC == 11) {
                                    this.dq[i2].jC = d.jn;
                                    this.dq[i2].kC = 0;
                                    d dVar = this.dq[i2];
                                    this.dq[i2].le = 0;
                                    dVar.lf = 0;
                                }
                                if (this.ci == 8) {
                                    this.dq[i2].kF -= ((((this.dp.kQ * this.hx) / 100) * this.hy) / 100) + (this.ii * 500);
                                } else {
                                    this.dq[i2].kF -= this.dp.kQ;
                                }
                                if (this.dp.kJ == 0) {
                                    this.dq[i2].kJ = this.dq[i2].hy < this.dp.ee ? 0 : 1;
                                } else if (this.dp.kJ == 1) {
                                    this.dq[i2].kJ = this.dq[i2].ee < this.dp.hy ? 1 : 0;
                                }
                                this.dq[i2].kK = (this.dp.dP + 1) % 2;
                                z = true;
                            }
                            if (a(this.dq[i2], this.dp) && a(this.dq[i2].kC, this.et) && !this.hk) {
                                if (i == -1 || this.dq[i].kH > this.dq[i2].kH) {
                                    i = i2;
                                }
                                if (this.dq[i2].kH > 6) {
                                    d dVar2 = this.dp;
                                    int i3 = this.dq[i2].kC;
                                    d[] dVarArr = this.dq;
                                    dVar2.dd = i3 == 11 ? 6 : 5;
                                } else {
                                    this.dp.dd = this.dq[i2].fi == 2 ? 6 : 5;
                                }
                                int i4 = this.dq[i2].kC;
                                d[] dVarArr2 = this.dq;
                                if (i4 == 15) {
                                    this.dp.dd = 6;
                                }
                                this.hv++;
                                if (this.hv > 2) {
                                    this.hv = 0;
                                    this.dp.dd = 6;
                                }
                                this.dp.e(this.dp.dd);
                                this.dp.kK = (this.dq[i2].dP + 1) % 2;
                                if (this.ci == 8) {
                                    this.dp.kF -= (this.dq[i2].kQ * this.hz) / 100;
                                    this.jX += (this.dq[i2].kQ * this.hz) / 100;
                                } else {
                                    this.dp.kF -= this.dq[i2].kQ;
                                }
                                this.gi += this.dp.kH == 0 ? 100 : 130;
                                if (!a(this.dp.kC, this.et) && !this.hk) {
                                    this.bs = -1;
                                }
                            }
                        } else {
                            if (i == -1 || this.dq[i].kH > this.dq[i2].kH) {
                                i = i2;
                            }
                            this.dq[i2].jC = 9;
                            this.dq[i2].kF -= this.dp.kQ / 2;
                            this.dp.dd = 5;
                            this.dp.e(this.dp.dd);
                            this.dp.kF -= this.dq[i2].kQ / 3;
                            this.jX += this.dq[i2].kQ / 3;
                            z = true;
                        }
                    }
                }
                if (this.gi > this.gj) {
                    this.gi = this.gj;
                }
                if (z) {
                    if (this.gz == -1 && (this.dp.kC == 12 || this.dp.kC == 13)) {
                        this.gz = 0;
                        this.gA = 2;
                    }
                    if (z) {
                        this.fB = i;
                        if (this.jL) {
                            this.jN = (byte) (this.jN + 1);
                        }
                        this.jR = 0;
                        this.id = 0;
                    }
                }
            }
            if (this.dr != null) {
                for (int i5 = 0; i5 < this.dr.length; i5++) {
                    if (this.dr[i5][0] != 0) {
                        if (this.dq != null) {
                            for (int i6 = 0; i6 < this.dq.length; i6++) {
                                if (a(i5, this.dq[i6])) {
                                    this.dq[i6].jC = 10;
                                    if (this.dr[i5][0] == 8) {
                                        this.dq[i6].jC = 9;
                                    }
                                    this.dq[i6].kF -= this.dr[i5][11];
                                }
                            }
                        }
                        if (a(i5, this.dp) && !this.hk) {
                            this.dp.dd = this.dr[i5][0] == 8 ? 5 : 6;
                            if (this.dp.kC < 9) {
                                this.bs = -1;
                            }
                            this.dp.e(this.dp.dd);
                            this.dp.kF -= this.dr[i5][11];
                        }
                    }
                }
            }
            if (this.kr != null && this.dp.kF > 0) {
                for (int i7 = 0; i7 < this.kr.length; i7++) {
                    if (this.kr[i7][0] != 0) {
                        int i8 = this.kr[i7][2] - this.dp.eg;
                        int i9 = this.kr[i7][1] - this.kr[i7][3];
                        int i10 = this.kr[i7][1] + this.kr[i7][3];
                        int i11 = this.dp.ee;
                        int i12 = this.dp.hy;
                        int i13 = this.dp.es;
                        int i14 = this.dp.dV;
                        if (this.kr[i7][0] != 1 || i8 < -8 || i8 > 10 || i13 > i10 || i14 < i9 || i13 == i14) {
                            if (this.kr[i7][0] == 6 || this.kr[i7][0] == 9) {
                                int[] iArr = this.kr[i7];
                                iArr[6] = iArr[6] - 1;
                                if (this.kr[i7][6] < 0) {
                                    if (this.kr[i7][0] == 6) {
                                        this.kr[i7][0] = 9;
                                        this.kr[i7][6] = 2;
                                        b(this.kr[i7][5], this.kr[i7][1], this.kr[i7][2] - 3, -1, 0);
                                    } else {
                                        this.kr[i7][0] = 0;
                                    }
                                }
                            }
                            if (i11 <= i10 && i12 >= i9 && i8 >= -5 && i8 <= 8) {
                                switch (this.kr[i7][0]) {
                                    case 2:
                                        this.dp.kF += (this.dp.kG * 20) / 100;
                                        this.dp.kF = this.dp.kF > this.dp.kG ? this.dp.kG : this.dp.kF;
                                        this.kr[i7][0] = 0;
                                        this.hN = true;
                                        this.hO = 0;
                                        this.hP = 0;
                                        this.hQ = this.dp.kD;
                                        this.hR = this.dp.eg + 16;
                                        break;
                                    case 3:
                                        this.dp.kF += (this.dp.kG * 40) / 100;
                                        this.dp.kF = this.dp.kF > this.dp.kG ? this.dp.kG : this.dp.kF;
                                        this.kr[i7][0] = 0;
                                        this.hN = true;
                                        this.hO = 0;
                                        this.hP = 1;
                                        this.hQ = this.dp.kD;
                                        this.hR = this.dp.eg + 16;
                                        break;
                                    case 4:
                                        this.dp.kF += (this.dp.kG * 60) / 100;
                                        this.dp.kF = this.dp.kF > this.dp.kG ? this.dp.kG : this.dp.kF;
                                        this.kr[i7][0] = 0;
                                        this.hN = true;
                                        this.hO = 0;
                                        this.hP = 2;
                                        this.hQ = this.dp.kD;
                                        this.hR = this.dp.eg + 16;
                                        break;
                                    case 5:
                                        this.gi = this.gj;
                                        this.kr[i7][0] = 0;
                                        this.hN = true;
                                        this.hO = 0;
                                        this.hP = 3;
                                        this.hQ = this.dp.kD;
                                        this.hR = this.dp.eg + 16;
                                        break;
                                    case 10:
                                        int[] iArr2 = this.ga[this.dp.kH];
                                        iArr2[10] = iArr2[10] + 1;
                                        o(this.dp.kH);
                                        this.hp = 40;
                                        this.kr[i7][0] = 0;
                                        break;
                                    case 11:
                                        int[] iArr3 = this.ga[this.dp.kH];
                                        iArr3[9] = iArr3[9] + 1;
                                        o(this.dp.kH);
                                        this.hp = 39;
                                        this.kr[i7][0] = 0;
                                        break;
                                    case 12:
                                    case 13:
                                        int[] iArr4 = this.ga[this.dp.kH];
                                        iArr4[12] = iArr4[12] + 1;
                                        if (this.ga[this.dp.kH][12] > 20) {
                                            this.ga[this.dp.kH][12] = 20;
                                            this.cD = true;
                                        }
                                        if (this.ga[this.dp.kH][12] < 20) {
                                            this.hp = this.dp.kH + 2;
                                            o(this.dp.kH);
                                        }
                                        if (this.ga[this.dp.kH][12] == 20 && !this.cD) {
                                            this.ga[this.dp.kH][12] = 20;
                                            this.hp = this.dp.kH + 68;
                                            this.ga[this.dp.kH][5] = 5;
                                            o(this.dp.kH);
                                            aj();
                                        }
                                        this.hq = this.ga[this.dp.kH][12];
                                        this.kr[i7][0] = 0;
                                        break;
                                    case 14:
                                        if (this.ga[this.dp.kH][13] < 20) {
                                            int[] iArr5 = this.ga[this.dp.kH];
                                            iArr5[13] = iArr5[13] + 1;
                                            o(this.dp.kH);
                                            this.hq = this.ga[this.dp.kH][13];
                                            if (this.ga[this.dp.kH][13] <= 15) {
                                                this.fU = 0;
                                                this.fV = 0;
                                            } else if (this.ga[this.dp.kH][13] > 15 && this.ga[this.dp.kH][13] <= 19) {
                                                this.fU = 0;
                                                this.fV = 1;
                                            } else if (this.ga[this.dp.kH][13] == 20) {
                                                this.fU = 0;
                                                this.fV = 2;
                                            }
                                            this.hp = 31;
                                        }
                                        this.kr[i7][0] = 0;
                                        break;
                                    case Database.INT_FEECUE:
                                        if (this.ga[this.dp.kH][14] <= 20) {
                                            int[] iArr6 = this.ga[this.dp.kH];
                                            iArr6[14] = iArr6[14] + 1;
                                            o(this.dp.kH);
                                            this.hq = this.ga[this.dp.kH][14];
                                            if (this.ga[this.dp.kH][14] <= 15) {
                                                this.fU = 1;
                                                this.fW = 0;
                                            } else if (this.ga[this.dp.kH][14] > 15 && this.ga[this.dp.kH][13] <= 19) {
                                                this.fU = 1;
                                                this.fW = 1;
                                            } else if (this.ga[this.dp.kH][14] == 20) {
                                                this.fU = 1;
                                                this.fW = 2;
                                            }
                                            this.hp = 33;
                                        }
                                        this.kr[i7][0] = 0;
                                        break;
                                    case 16:
                                        if (this.ga[this.dp.kH][15] <= 20) {
                                            int[] iArr7 = this.ga[this.dp.kH];
                                            iArr7[15] = iArr7[15] + 1;
                                            o(this.dp.kH);
                                            this.hq = this.ga[this.dp.kH][15];
                                            if (this.ga[this.dp.kH][15] <= 15) {
                                                this.fU = 2;
                                                this.fX = 0;
                                            } else if (this.ga[this.dp.kH][15] > 15 && this.ga[this.dp.kH][15] <= 19) {
                                                this.fU = 2;
                                                this.fX = 1;
                                            } else if (this.ga[this.dp.kH][15] == 20) {
                                                this.fU = 2;
                                                this.fX = 2;
                                            }
                                            this.hp = 35;
                                        }
                                        this.kr[i7][0] = 0;
                                        break;
                                }
                            }
                        } else {
                            this.kr[i7][0] = 6;
                            this.kr[i7][6] = 2;
                        }
                    }
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x03ea A[LOOP:0: B:76:0x01d3->B:97:0x03ea, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x022f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void V() {
        /*
            r14 = this;
            r13 = 6
            r12 = 4
            r11 = -1
            r10 = 1
            r9 = 0
            boolean r0 = r14.dS
            if (r0 != 0) goto L_0x0041
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.eg
            if (r0 >= r1) goto L_0x0017
            d r0 = r14.dp
            int r1 = r14.eg
            r0.kD = r1
        L_0x0017:
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.eh
            if (r0 <= r1) goto L_0x0025
            d r0 = r14.dp
            int r1 = r14.eh
            r0.kD = r1
        L_0x0025:
            d r0 = r14.dp
            int r0 = r0.eg
            int r1 = r14.ee
            if (r0 >= r1) goto L_0x0033
            d r0 = r14.dp
            int r1 = r14.ee
            r0.eg = r1
        L_0x0033:
            d r0 = r14.dp
            int r0 = r0.eg
            int r1 = r14.ei
            if (r0 <= r1) goto L_0x0041
            d r0 = r14.dp
            int r1 = r14.ei
            r0.eg = r1
        L_0x0041:
            int r0 = r14.dO
            int r1 = r14.dY
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r2 = r2[r9]
            if (r2 <= 0) goto L_0x016b
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r2 = r2[r9]
            r14.jZ = r2
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r2 = r2[r10]
            r14.in = r2
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r3 = 2
            r2 = r2[r3]
            r14.ka = r2
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r3 = 3
            r2 = r2[r3]
            r14.kb = r2
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.jZ
            if (r2 <= r3) goto L_0x0099
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.jZ
            int r3 = r3 + 21
            if (r2 >= r3) goto L_0x0099
            d r2 = r14.dp
            int r2 = r2.eg
            int r3 = r14.kb
            if (r2 >= r3) goto L_0x0099
            d r2 = r14.dp
            int r3 = r14.jZ
            r2.kD = r3
        L_0x0099:
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.ka
            if (r2 >= r3) goto L_0x00ba
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.ka
            r4 = 21
            int r3 = r3 - r4
            if (r2 <= r3) goto L_0x00ba
            d r2 = r14.dp
            int r2 = r2.eg
            int r3 = r14.kb
            if (r2 >= r3) goto L_0x00ba
            d r2 = r14.dp
            int r3 = r14.ka
            r2.kD = r3
        L_0x00ba:
            d r2 = r14.dp
            int r2 = r2.eg
            int r3 = r14.kb
            int r3 = r3 + 10
            if (r2 >= r3) goto L_0x00dc
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.jZ
            if (r2 <= r3) goto L_0x00dc
            d r2 = r14.dp
            int r2 = r2.kD
            int r3 = r14.ka
            if (r2 >= r3) goto L_0x00dc
            d r2 = r14.dp
            int r3 = r14.kb
            int r3 = r3 + 10
            r2.eg = r3
        L_0x00dc:
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r2 = r2[r12]
            r14.kc = r2
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r3 = 5
            r2 = r2[r3]
            r14.kd = r2
            int[][][] r2 = r14.jY
            r2 = r2[r0]
            r2 = r2[r1]
            r2 = r2[r13]
            r14.ke = r2
            int[][][] r2 = r14.jY
            r0 = r2[r0]
            r0 = r0[r1]
            r1 = 7
            r0 = r0[r1]
            r14.kf = r0
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.kc
            if (r0 <= r1) goto L_0x0126
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.kc
            int r1 = r1 + 21
            if (r0 >= r1) goto L_0x0126
            d r0 = r14.dp
            int r0 = r0.eg
            int r1 = r14.kd
            if (r0 <= r1) goto L_0x0126
            d r0 = r14.dp
            int r1 = r14.kc
            r0.kD = r1
        L_0x0126:
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.ke
            if (r0 >= r1) goto L_0x0147
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.ke
            r2 = 21
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x0147
            d r0 = r14.dp
            int r0 = r0.eg
            int r1 = r14.kd
            if (r0 <= r1) goto L_0x0147
            d r0 = r14.dp
            int r1 = r14.ke
            r0.kD = r1
        L_0x0147:
            d r0 = r14.dp
            int r0 = r0.eg
            int r1 = r14.kd
            r2 = 10
            int r1 = r1 - r2
            if (r0 <= r1) goto L_0x016b
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.kc
            if (r0 <= r1) goto L_0x016b
            d r0 = r14.dp
            int r0 = r0.kD
            int r1 = r14.ke
            if (r0 >= r1) goto L_0x016b
            d r0 = r14.dp
            int r1 = r14.kd
            r2 = 10
            int r1 = r1 - r2
            r0.eg = r1
        L_0x016b:
            int r6 = r14.dO
            int r0 = r14.dY
            boolean r0 = r14.dS
            if (r0 == 0) goto L_0x0177
            boolean r0 = r14.hk
            if (r0 == 0) goto L_0x01c2
        L_0x0177:
            r0 = r11
        L_0x0178:
            r14.dP = r0
            int r0 = r14.dP
            if (r0 == r11) goto L_0x01c1
            int r0 = r14.dY
            int r1 = r14.dP
            if (r0 == r1) goto L_0x01c1
            byte r0 = r14.dN
            byte r1 = r14.dK
            if (r0 == r1) goto L_0x01c1
            r14.fB = r11
            int r0 = r14.dO
            int r1 = r14.dP
            short[][][] r2 = defpackage.g.lK
            r14.a(r0, r1, r9, r2)
            d r0 = r14.dp
            if (r0 == 0) goto L_0x019f
            r0.kC = r9
            r0.dd = r9
            r0.lf = r9
        L_0x019f:
            byte r0 = r14.dK
            r14.dN = r0
            int r0 = r14.eH
            r14.eG = r0
            r14.W()
            int r0 = r14.dO
            int r1 = r14.dP
            short[][][] r2 = defpackage.g.lK
            r14.a(r0, r1, r10, r2)
            int r0 = r14.dO
            int r1 = r14.dY
            r14.a(r0, r1)
            f r0 = r14.aI
            int r1 = r14.dO
            r0.g(r1, r9)
        L_0x01c1:
            return
        L_0x01c2:
            int r0 = r14.ci
            r1 = 9
            if (r0 == r1) goto L_0x03ef
            d r0 = r14.dp
            int r4 = r0.kD
            d r0 = r14.dp
            int r5 = r0.eg
            r14.dQ = r11
            r7 = r9
        L_0x01d3:
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            int r0 = r0.length
            if (r7 >= r0) goto L_0x03ef
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 3
            short r0 = r0[r1]
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            short r1 = r1[r12]
            short[][][][] r2 = r14.hh
            r2 = r2[r6]
            int r3 = r14.dY
            r2 = r2[r3]
            r2 = r2[r7]
            r3 = 5
            short r2 = r2[r3]
            short[][][][] r3 = r14.hh
            r3 = r3[r6]
            int r8 = r14.dY
            r3 = r3[r8]
            r3 = r3[r7]
            short r3 = r3[r13]
            boolean r0 = a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x022c
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 2
            short r0 = r0[r1]
            int r0 = r0 + 1
            r14.dQ = r0
            int r0 = r14.dQ
            switch(r0) {
                case 1: goto L_0x0244;
                case 2: goto L_0x02b3;
                case 3: goto L_0x0312;
                case 4: goto L_0x0385;
                default: goto L_0x022c;
            }
        L_0x022c:
            r0 = r9
        L_0x022d:
            if (r0 == 0) goto L_0x03ea
            int r0 = r14.dZ
            int r0 = r0 + 1
            r14.dZ = r0
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 7
            short r0 = r0[r1]
            goto L_0x0178
        L_0x0244:
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 3
            short r0 = r0[r1]
            int r0 = r0 + 10
            if (r4 > r0) goto L_0x022c
            int r0 = r14.dY
            r14.hA = r0
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 3
            short r0 = r0[r1]
            int r0 = r0 + 11
            r14.hB = r0
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r12]
            int r0 = r0 + 10
            r14.hC = r0
            int r0 = r14.eM
            int r1 = r14.eH
            int r0 = r0 / r1
            r14.eI = r0
            r14.eJ = r9
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 8
            short r1 = r1[r2]
            r0.kD = r1
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 9
            short r1 = r1[r2]
            r0.eg = r1
            d r0 = r14.dp
            int r1 = r0.kD
            int r2 = r14.eM
            int r1 = r1 - r2
            r0.kD = r1
            r0 = r10
            goto L_0x022d
        L_0x02b3:
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r12]
            int r0 = r0 + 10
            if (r5 > r0) goto L_0x022c
            int r0 = r14.dY
            r14.hA = r0
            r14.hB = r4
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r12]
            int r0 = r0 + 50
            r14.hC = r0
            r14.eI = r9
            int r0 = r14.eM
            int r1 = r14.eH
            int r0 = r0 / r1
            r14.eJ = r0
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 8
            short r1 = r1[r2]
            r0.kD = r1
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 9
            short r1 = r1[r2]
            r0.eg = r1
            d r0 = r14.dp
            int r1 = r0.eg
            int r2 = r14.eM
            int r1 = r1 - r2
            r0.eg = r1
            r0 = r10
            goto L_0x022d
        L_0x0312:
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 5
            short r0 = r0[r1]
            r1 = 10
            int r0 = r0 - r1
            if (r4 < r0) goto L_0x022c
            int r0 = r14.dY
            r14.hA = r0
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            r1 = 5
            short r0 = r0[r1]
            r1 = 11
            int r0 = r0 - r1
            r14.hB = r0
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r13]
            r1 = 10
            int r0 = r0 - r1
            r14.hC = r0
            int r0 = r14.eM
            int r0 = -r0
            int r1 = r14.eH
            int r0 = r0 / r1
            r14.eI = r0
            r14.eJ = r9
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 8
            short r1 = r1[r2]
            r0.kD = r1
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 9
            short r1 = r1[r2]
            r0.eg = r1
            d r0 = r14.dp
            int r1 = r0.kD
            int r2 = r14.eM
            int r1 = r1 + r2
            r0.kD = r1
            r0 = r10
            goto L_0x022d
        L_0x0385:
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r13]
            r1 = 10
            int r0 = r0 - r1
            if (r5 < r0) goto L_0x022c
            int r0 = r14.dY
            r14.hA = r0
            r14.hB = r4
            short[][][][] r0 = r14.hh
            r0 = r0[r6]
            int r1 = r14.dY
            r0 = r0[r1]
            r0 = r0[r7]
            short r0 = r0[r13]
            r1 = 11
            int r0 = r0 - r1
            r14.hC = r0
            r14.eI = r9
            int r0 = r14.eM
            int r0 = -r0
            int r1 = r14.eH
            int r0 = r0 / r1
            int r0 = r0 - r10
            r14.eJ = r0
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 8
            short r1 = r1[r2]
            int r1 = r1 + 50
            r0.kD = r1
            d r0 = r14.dp
            short[][][][] r1 = r14.hh
            r1 = r1[r6]
            int r2 = r14.dY
            r1 = r1[r2]
            r1 = r1[r7]
            r2 = 9
            short r1 = r1[r2]
            r0.eg = r1
            d r0 = r14.dp
            int r1 = r0.kD
            int r2 = r14.eM
            int r1 = r1 - r2
            r0.kD = r1
            r0 = r10
            goto L_0x022d
        L_0x03ea:
            int r0 = r7 + 1
            r7 = r0
            goto L_0x01d3
        L_0x03ef:
            r0 = r11
            goto L_0x0178
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.V():void");
    }

    private void W() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        this.kg = null;
        if (this.dq != null) {
            int i9 = 0;
            for (int i10 = 0; i10 < this.dq.length; i10++) {
                if (this.dq[i10].ix && this.dq[i10].kZ) {
                    i9++;
                }
            }
            i = i9;
        } else {
            i = 0;
        }
        if (this.dr != null) {
            int i11 = 0;
            for (int i12 = 0; i12 < this.dr.length; i12++) {
                if (this.dr[i12][0] > 0) {
                    i11++;
                }
            }
            i2 = i11;
        } else {
            i2 = 0;
        }
        if (this.kr != null) {
            int i13 = 0;
            for (int i14 = 0; i14 < this.kr.length; i14++) {
                if (this.kr[i14][0] != 0) {
                    i13++;
                }
            }
            i3 = i13;
        } else {
            i3 = 0;
        }
        this.kg = (int[][]) Array.newInstance(Integer.TYPE, i + 1 + 0 + i2 + i3, 5);
        int i15 = 0 + 1;
        a(0, this.dp.kH, this.dp.kC, this.dp.lf, this.dp.fi, this.dp.kD, this.dp.eg, this.dp.kE, this.dp.dP, -1, this.dp.gg, this.dp.ln, this.dp.eo);
        if (this.dN == this.dK) {
            this.kg[0][1] = 1;
            this.kg[0][2] = (this.eH - this.eG) % this.dp.kB[1].length;
            int[] iArr = this.kg[0];
            iArr[4] = iArr[4] + this.eK;
            int[] iArr2 = this.kg[0];
            iArr2[5] = iArr2[5] + this.eL;
        }
        if (i != 0) {
            int i16 = i15;
            int i17 = 0;
            while (i17 < this.dq.length) {
                if (!this.dq[i17].ix || !this.dq[i17].kZ) {
                    i8 = i16;
                } else {
                    a(i16, this.dq[i17].kH, this.dq[i17].kC, this.dq[i17].lf, this.dq[i17].fi, this.dq[i17].kD, this.dq[i17].eg, this.dq[i17].kE, this.dq[i17].dP, -1, this.dq[i17].gg, this.dq[i17].ln, this.dp.eo);
                    i8 = i16 + 1;
                }
                i17++;
                i16 = i8;
            }
            i4 = i16;
        } else {
            i4 = i15;
        }
        if (!(i2 == 0 || this.dr == null)) {
            int i18 = 0;
            while (true) {
                int i19 = i18;
                i7 = i5;
                if (i19 >= this.dr.length) {
                    break;
                }
                if (this.dr[i19][0] > 0) {
                    a(i7, -11, -1, this.dr[i19][1], this.dr[i19][2], this.dr[i19][3], this.dr[i19][4], this.dr[i19][5], this.dr[i19][8], this.dr[i19][2], 0, 0, 0);
                    i5 = i7 + 1;
                } else {
                    i5 = i7;
                }
                i18 = i19 + 1;
            }
            i5 = i7;
        }
        if (i3 != 0 && this.kr != null) {
            int i20 = 0;
            while (true) {
                int i21 = i20;
                int i22 = i5;
                if (i21 >= this.kr.length) {
                    break;
                }
                if (this.kr[i21][0] > 0) {
                    a(i22, -12, -1, -1, 13, this.kr[i21][1], this.kr[i21][2], 0, 0, new byte[]{78, 78, 81, 82, 83, 84, 79, 79, 85, 80, 85, 85, 85, 85, 85, 85, 85}[this.kr[i21][0]], 0, 0, 0);
                    i6 = i22 + 1;
                } else {
                    i6 = i22;
                }
                i20 = i21 + 1;
            }
        }
        for (int i23 = 0; i23 < this.kg.length - 1; i23++) {
            for (int i24 = i23; i24 < this.kg.length; i24++) {
                if (this.kg[i23][5] > this.kg[i24][5]) {
                    int[] iArr3 = this.kg[i23];
                    this.kg[i23] = this.kg[i24];
                    this.kg[i24] = iArr3;
                }
            }
        }
    }

    private void X() {
        if (this.dp.eH < this.dp.hz) {
            this.dp.dd = 8;
            this.dp.eH--;
            if (this.dp.eH == 6) {
                this.dB = this.ci;
                this.ci = 31;
                this.cx = 6;
                this.ik = false;
                this.kh = true;
                this.dp.eH--;
            }
            if (this.dp.eH <= 0) {
                aj();
                if (this.ci == 8) {
                    this.gv = 0;
                    this.kh = true;
                    d(17, 0);
                    this.fR = 0;
                }
                if (this.ci == 9) {
                    this.ga[this.dp.kH][1] = this.hM;
                    p(1);
                    this.gv = 1;
                    this.ho = new int[]{this.eo, this.en, this.dp.kS, 2, 2};
                    d(18, 0);
                }
            }
        }
        if (this.dq != null) {
            for (int i = 0; i < this.dq.length; i++) {
                byte[] bArr = {0, 0, 100, 100, 100, 100, 100, d.hD, d.hD, d.hD, 3, 3, 100};
                if (this.dq[i].eH < this.dq[i].hz && this.dq[i].kC != 6) {
                    this.dq[i].dd = 8;
                    this.dq[i].eH--;
                    if (this.dq[i].eH > 8) {
                        this.dq[i].ix = (this.dq[i].eH / 4) % 2 == 0;
                    } else if (this.dq[i].eH > 0) {
                        this.dq[i].ix = (this.dq[i].eH / 2) % 2 == 0;
                    } else {
                        this.dq[i].ix = false;
                    }
                    if (this.dq[i].eH == this.dq[i].hz - 2) {
                        if (this.hy == 100) {
                            int[] iArr = this.ga[this.dp.kH];
                            iArr[1] = iArr[1] + bArr[this.dq[i].kH];
                        } else {
                            int[] iArr2 = this.ga[this.dp.kH];
                            iArr2[1] = iArr2[1] + (bArr[this.dq[i].kH] * 3);
                        }
                    }
                    if (i == this.fB) {
                        this.fB = -1;
                    }
                }
                if (this.dq[i].eH == 0 && !this.dq[i].ix && this.ci == 8) {
                    if (this.hy == 100) {
                        this.gp += bArr[this.dq[i].kH];
                        this.hn = bArr[this.dq[i].kH] + this.hn;
                    } else {
                        this.gp += bArr[this.dq[i].kH] * 3;
                        this.hn = (bArr[this.dq[i].kH] * 3) + this.hn;
                    }
                    if (this.gp > gq) {
                        this.gp = gq;
                    }
                }
                if (this.dq[i].eH == 0 && !this.dq[i].ix) {
                    if (this.dq[i].kH != 10 && this.dq[i].kH != 11) {
                        this.eo++;
                        if (this.ci == 8) {
                            if (this.dq[i].lj == 7 || this.dq[i].lj == 11) {
                                this.fE[this.x] = 0;
                                this.x++;
                            } else if (this.dq[i].lj == 13 || this.dq[i].lj == 9 || this.dq[i].lj == 6 || this.dq[i].lj == 10) {
                                this.fE[this.x] = 1;
                                this.x++;
                            } else if (this.dq[i].lj == 4 || this.dq[i].lj == 14 || this.dq[i].lj == 5) {
                                this.fE[this.x] = 2;
                                this.x++;
                            }
                            this.fC = true;
                            if (this.dO < 7) {
                                if (this.dO == 3 && this.dY == 6) {
                                    a(i, 0, 0);
                                } else if (this.dO == 5 && this.dY == 7) {
                                    a(i, 1, 0);
                                } else if (this.dO == 1 && this.dY == 3) {
                                    b(i, 1, this.iH);
                                } else if (this.dq[i].lj == 7 || this.dq[i].lj == 9 || this.dq[i].lj == 13 || this.dq[i].lj == 10 || this.dq[i].lj == 11 || this.dq[i].lj == 4 || this.dq[i].lj == 14 || this.dq[i].lj == 5) {
                                    if (this.dY == e.lt[this.dO].length - 1 && this.dq[i] == this.dq[0]) {
                                        b(i, 1);
                                    } else {
                                        b(i, 0);
                                    }
                                }
                            } else if (this.dO == 7 && this.dY == 7) {
                                a(i, 2, this.iH);
                            } else if (this.dq[i].lj == 14) {
                                int i2 = this.iH;
                                int n = n(this.f0do.nextInt()) % 100;
                                if (i2 == 0) {
                                    if (this.ga[this.dp.kH][12] >= 20) {
                                        b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                    } else if (n <= 5) {
                                        b(this.dp.kH + 12, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                    } else {
                                        b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                    }
                                } else if (this.ga[this.dp.kH][12] >= 20) {
                                    b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                } else if (n <= 20) {
                                    b(this.dp.kH + 12, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                } else {
                                    b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                                }
                            } else if (this.dq[i].lj == 5) {
                                b(i, 0, this.iH);
                            } else if (this.dq[i].lj == 7 || this.dq[i].lj == 9 || this.dq[i].lj == 13 || this.dq[i].lj == 10 || this.dq[i].lj == 11) {
                                if (this.dY == e.lt[this.dO].length - 1 && this.dq[i] == this.dq[0]) {
                                    b(i, 1);
                                } else {
                                    b(i, 0);
                                }
                            }
                        }
                    } else if (this.dq[i].kH == 11) {
                        this.en++;
                        this.ek--;
                        if (this.ek >= 0 && g.lV[this.dO][this.dY].length != 0) {
                            byte b = g.lV[this.dO][this.dY][n(this.f0do.nextInt()) % g.lV[this.dO][this.dY].length];
                            this.dq[i].b(0, g.lT[b][0] + (n(this.f0do.nextInt()) % g.lT[b][2]), g.lT[b][1] + (n(this.f0do.nextInt()) % g.lT[b][3]));
                            if (g.lT[b][2] == 1) {
                                this.dq[i].ll = 1;
                                this.dq[i].lm = this.dq[i].kD - this.dp.kD > 0 ? (byte) 1 : 0;
                            }
                            if (g.lT[b][3] == 1) {
                                this.dq[i].ll = 2;
                                this.dq[i].lm = this.dq[i].eg - this.dp.eg > 0 ? (byte) 1 : 0;
                            }
                            this.dq[i].kR = ToneControl.C4;
                        }
                    } else if (this.dq[i].kH == 10) {
                        this.en++;
                        this.ej--;
                        if (this.ej > 0 && g.lV[this.dO][this.dY].length != 0) {
                            byte b2 = g.lV[this.dO][this.dY][n(this.f0do.nextInt()) % g.lV[this.dO][this.dY].length];
                            this.dq[i].b(0, g.lT[b2][0] + (n(this.f0do.nextInt()) % g.lT[b2][2]), g.lT[b2][1] + (n(this.f0do.nextInt()) % g.lT[b2][3]));
                            if (g.lT[b2][2] == 1) {
                                this.dq[i].ll = 1;
                                this.dq[i].lm = this.dq[i].kD - this.dp.kD > 0 ? (byte) 1 : 0;
                            }
                            if (g.lT[b2][3] == 1) {
                                this.dq[i].ll = 2;
                                this.dq[i].lm = this.dq[i].eg - this.dp.eg > 0 ? (byte) 1 : 0;
                            }
                            this.dq[i].kR = ToneControl.C4;
                        }
                    }
                }
                if (this.dq[i].eH == 0) {
                    this.dq[i].ix = false;
                    if (i == this.dV) {
                        this.dq[i].ix = true;
                    }
                }
            }
        }
        if (this.ci == 9 && this.dS) {
            ah();
        }
    }

    private void Y() {
        boolean z;
        if (!this.iN) {
            this.id++;
        }
        if (this.id >= 15) {
            this.id = 0;
            this.jP = 0;
            this.jQ = false;
        }
        if (this.dN == this.dK) {
            this.eK = this.eI * this.eG;
            this.eL = this.eJ * this.eG;
            this.eG--;
            if (this.eG < 0) {
                this.dN = this.dJ;
                this.aI.w();
                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                }
                this.aK[0] = 0;
            }
        }
        if (this.gz >= 0) {
            this.gz++;
            if (this.gz > this.gA) {
                this.gz = -1;
            }
        }
        if (this.jL) {
            if (this.jR >= this.jS[this.jT]) {
                this.jR = 0;
                this.jN = 0;
                this.jM = false;
                this.jL = false;
            } else if (!this.iN) {
                this.jR++;
            }
        }
        if (this.ci == 8 && this.dW && this.gz == -1 && !this.gS && this.gX != 2) {
            this.gS = true;
            this.gT = 0;
            this.gU = 0;
            this.gV = 0;
            this.gP = g.lN[this.dO][this.dY][g.lN[this.dO][this.dY].length - 1];
            this.gQ = -1;
            this.gX = 1;
            this.gY = true;
        }
        if (this.gX == 2 && !this.hk) {
            if (this.hj <= 100) {
                this.hj++;
            } else {
                p(0);
                int[] iArr = new int[11];
                iArr[0] = this.hn;
                iArr[1] = this.em == 0 ? 0 : (this.eo * 100) / this.em;
                iArr[2] = this.el == 0 ? 0 : (this.en * 100) / this.el;
                iArr[3] = (this.jX * 100) / this.dp.kG;
                iArr[4] = 0;
                iArr[5] = 0;
                iArr[6] = 0;
                iArr[7] = 0;
                iArr[8] = 2;
                iArr[9] = 2;
                iArr[10] = this.iQ;
                this.ho = iArr;
                aj();
                this.hj = 0;
                d(18, 0);
            }
            this.gv = 0;
        }
        if (this.gB >= 0) {
            this.gB++;
            if (this.gB > 100) {
                this.gB = -1;
            }
        }
        if (this.dO == 3 && this.dY == 7) {
            this.dV = 0;
            if (this.dq[this.dV].kF <= 0 && !this.dW && this.gX == 0) {
                for (d dVar : this.dq) {
                    dVar.kF = -1;
                }
                this.dT[this.dY] = false;
                this.dR = true;
                this.dW = true;
                this.gz = 0;
                this.gA = 30;
            }
        } else if (this.ci == 8 && this.dT[this.dY]) {
            int i = this.dO;
            int i2 = this.dY;
            if (this.dq != null) {
                short s = e.lt[i][i2][0];
                int i3 = i2 == this.dU ? 7 : 10;
                if (this.hw) {
                    i3 = 12;
                }
                int i4 = 0;
                while (true) {
                    if (i4 < s) {
                        if (this.dq[i4].kH < i3 && this.dq[i4].kF > 0) {
                            z = false;
                            break;
                        }
                        i4++;
                    } else {
                        z = true;
                        break;
                    }
                }
            } else {
                z = false;
            }
            if (z) {
                this.dT[this.dY] = false;
                this.dR = true;
                this.ek = 0;
                this.ej = 0;
                if (this.dU == this.dY) {
                    this.dW = true;
                    this.gz = 0;
                    this.gA = 30;
                }
            }
        }
    }

    private void Z() {
        this.aQ = 0;
        this.aR = 240;
        this.aS = 320;
        this.aT = this.aR >> 1;
        this.aU = this.aS >> 1;
    }

    private byte a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        byte b;
        if (this.dr == null) {
            this.dr = (int[][]) Array.newInstance(Integer.TYPE, 20, 14);
        }
        int i10 = 0;
        while (true) {
            if (i10 >= this.dr.length) {
                b = 0;
                break;
            } else if (this.dr[i10][0] == 0) {
                b = (byte) i10;
                break;
            } else {
                i10++;
            }
        }
        this.dr[b] = null;
        switch (i) {
            case 5:
                this.dr[b] = a(i, 2, 24, i2, i3, i4, i5, 0, i7, this.fN[2][24][3] / 2, this.fN[2][24][4] / 2, i9, 99, i8);
                break;
            case 6:
                this.dr[b] = a(i, 3, 34, i2, i3, i4, i5, 0, i7, this.fN[3][34][3] / 2, this.fN[3][34][4] / 2, i9, 99, i8);
                break;
            case 7:
                this.dr[b] = a(i, 2, 31, i2, i3, i4, i5, 0, i7, this.fN[2][31][3] / 4, this.fN[2][31][4] / 2, i9, 99, i8);
                break;
            case 8:
                this.dr[b] = a(i, 6, 13, i2, i3, i4, i5, 0, i7, this.fN[6][13][3] / 4, this.fN[6][13][4] / 2, i9, 99, i8);
                break;
            case 12:
                this.dr[b] = a(i, 2, 47, i2, i3, i4, i5, 0, i7, this.fN[2][47][3] / 2, this.fN[2][47][4] / 2, i9, 99, i8);
                break;
        }
        return b;
    }

    private void a() {
        try {
            this.ia = new a(this);
            this.ia.a("sms://");
            this.ib = new byte[]{0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0};
            this.ib = b(this.ib);
            if (this.ib[2] == 1) {
                this.ga[0][21] = 1;
                this.ga[0][22] = 1;
                this.ga[0][23] = 1;
            } else if (this.ib[17] == 1) {
                this.ga[1][21] = 1;
                this.ga[1][22] = 1;
                this.ga[1][23] = 1;
            }
            String e = e("/sms.txt");
            ky = e;
            int indexOf = e.indexOf("|");
            kz = ky.substring(indexOf + 1);
            kA = ky.substring(0, indexOf);
        } catch (ClassNotFoundException e2) {
        }
    }

    /* JADX WARN: Type inference failed for: r0v59, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r11, int r12) {
        /*
            r10 = this;
            r9 = 2
            r1 = 0
            r0 = -1
            r8 = 1
            r5 = 0
            r10.jR = r5
            r10.jN = r5
            r10.jM = r5
            r10.jL = r5
            r10.jP = r5
            r10.dY = r12
            r10.fB = r0
            r10.dq = r1
            r10.kr = r1
            r10.dr = r1
            r10.gP = r0
            r10.gQ = r0
            r10.gR = r0
            r10.gS = r5
            r10.gT = r0
            r10.gU = r0
            r10.gV = r0
            r10.gY = r5
            int[] r0 = r10.aK
            r0[r5] = r5
            r10.hu = r5
            r10.hI = r5
            boolean[] r0 = r10.dT
            boolean r0 = r0[r12]
            if (r0 != 0) goto L_0x0038
        L_0x0037:
            return
        L_0x0038:
            r10.dR = r5
            r10.dS = r5
            r10.hw = r8
            r10.hE = r5
            if (r11 != 0) goto L_0x0048
            if (r12 != 0) goto L_0x0048
            r10.dR = r8
            r10.dS = r8
        L_0x0048:
            if (r11 != 0) goto L_0x0050
            if (r12 != r8) goto L_0x0050
            boolean[] r0 = r10.dT
            r0[r5] = r5
        L_0x0050:
            byte[][][] r0 = defpackage.e.lu
            r0 = r0[r11]
            r0 = r0[r12]
            byte r0 = r0[r5]
            r10.ej = r0
            byte[][][] r0 = defpackage.e.lu
            r0 = r0[r11]
            r0 = r0[r12]
            byte r0 = r0[r8]
            r10.ek = r0
            short[][][] r0 = defpackage.e.lt
            r0 = r0[r11]
            r0 = r0[r12]
            short r0 = r0[r5]
            if (r0 <= 0) goto L_0x012f
            r10.dq = r1
            short[][][] r0 = defpackage.e.lt
            r0 = r0[r11]
            r0 = r0[r12]
            short r0 = r0[r5]
            d[] r0 = new defpackage.d[r0]
            r10.dq = r0
            r0 = r5
        L_0x007d:
            d[] r1 = r10.dq
            int r1 = r1.length
            if (r0 >= r1) goto L_0x012f
            d[] r1 = r10.dq
            d r2 = new d
            r2.<init>()
            r1[r0] = r2
            d[] r1 = r10.dq
            r1 = r1[r0]
            short[][][] r2 = defpackage.e.lt
            r2 = r2[r11]
            r2 = r2[r12]
            int r3 = r0 * 6
            int r3 = r3 + 3
            short r2 = r2[r3]
            short[][][] r3 = defpackage.e.lt
            r3 = r3[r11]
            r3 = r3[r12]
            int r4 = r0 * 6
            int r4 = r4 + 6
            short r3 = r3[r4]
            short[][][] r4 = defpackage.e.lt
            r4 = r4[r11]
            r4 = r4[r12]
            int r6 = r0 * 6
            int r6 = r6 + 1
            short r4 = r4[r6]
            short[][][] r6 = defpackage.e.lt
            r6 = r6[r11]
            r6 = r6[r12]
            int r7 = r0 * 6
            int r7 = r7 + 2
            short r6 = r6[r7]
            r1.a(r2, r3, r4, r6)
            d[] r1 = r10.dq
            r1 = r1[r0]
            short[][][] r2 = defpackage.e.lt
            r2 = r2[r11]
            r2 = r2[r12]
            int r3 = r0 * 6
            int r3 = r3 + 4
            short r2 = r2[r3]
            r1.kJ = r2
            d[] r1 = r10.dq
            r1 = r1[r0]
            short[][][] r2 = defpackage.e.lt
            r2 = r2[r11]
            r2 = r2[r12]
            int r3 = r0 * 6
            int r3 = r3 + 5
            short r2 = r2[r3]
            r1.lg = r2
            d[] r1 = r10.dq
            r1 = r1[r0]
            short[][][] r2 = defpackage.e.lt
            r2 = r2[r11]
            r2 = r2[r12]
            int r3 = r0 * 6
            int r3 = r3 + 6
            short r2 = r2[r3]
            r1.lh = r2
            d[] r1 = r10.dq
            r1 = r1[r0]
            int r1 = r1.kH
            r2 = 10
            if (r1 >= r2) goto L_0x0104
            r10.hw = r5
        L_0x0104:
            d[] r1 = r10.dq
            r1 = r1[r0]
            int r1 = r1.kH
            r2 = 7
            if (r1 >= r2) goto L_0x010f
            r10.hE = r8
        L_0x010f:
            d[] r1 = r10.dq
            r1 = r1[r0]
            boolean r1 = r1.lo
            if (r1 == 0) goto L_0x012b
            d[] r1 = r10.dq
            r1 = r1[r0]
            java.util.Random r2 = r10.f0do
            int r2 = r2.nextInt()
            int r2 = r2 % 20
            int r2 = n(r2)
            int r2 = r2 + 40
            r1.ih = r2
        L_0x012b:
            int r0 = r0 + 1
            goto L_0x007d
        L_0x012f:
            short[][][] r0 = defpackage.g.lU
            r0 = r0[r11]
            r0 = r0[r12]
            short r6 = r0[r5]
            r7 = r5
        L_0x0138:
            if (r7 >= r6) goto L_0x0172
            short[][][] r0 = defpackage.g.lU
            r0 = r0[r11]
            r0 = r0[r12]
            int r1 = r7 * 4
            int r1 = r1 + 3
            short r1 = r0[r1]
            short[][][] r0 = defpackage.g.lU
            r0 = r0[r11]
            r0 = r0[r12]
            int r2 = r7 * 4
            int r2 = r2 + 1
            short r2 = r0[r2]
            short[][][] r0 = defpackage.g.lU
            r0 = r0[r11]
            r0 = r0[r12]
            int r3 = r7 * 4
            int r3 = r3 + 2
            short r3 = r0[r3]
            short[][][] r0 = defpackage.g.lU
            r0 = r0[r11]
            r0 = r0[r12]
            int r4 = r7 * 4
            int r4 = r4 + 4
            short r4 = r0[r4]
            r0 = r10
            r0.b(r1, r2, r3, r4, r5)
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0138
        L_0x0172:
            byte[][][] r0 = defpackage.g.lN
            r0 = r0[r11]
            r0 = r0[r12]
            int r0 = r0.length
            if (r0 <= 0) goto L_0x0187
            byte[][][] r0 = defpackage.g.lN
            r0 = r0[r11]
            r0 = r0[r12]
            byte r0 = r0[r5]
            r10.gP = r0
            r10.gR = r5
        L_0x0187:
            boolean r0 = r10.cy
            if (r0 != r8) goto L_0x01a0
            if (r11 != r9) goto L_0x01a0
            r0 = 7
            if (r12 != r0) goto L_0x01a0
            d[] r0 = r10.dq
            r0 = r0[r5]
            d[] r1 = r10.dq
            r1 = r1[r5]
            int r1 = r1.kG
            int r1 = r1 * 80
            int r1 = r1 / 100
            r0.kF = r1
        L_0x01a0:
            if (r11 != r8) goto L_0x01cb
            int r0 = r10.dY
            if (r0 != 0) goto L_0x01cb
            int[][] r0 = r10.ga
            d r1 = r10.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 24
            r0 = r0[r1]
            if (r0 != r8) goto L_0x01cb
            r0 = r5
        L_0x01b5:
            short[] r1 = r10.hc
            int r1 = r1.length
            if (r0 >= r1) goto L_0x01cb
            short[][][][] r1 = r10.hh
            r1 = r1[r8]
            r1 = r1[r5]
            r1 = r1[r8]
            short[] r2 = r10.hc
            short r2 = r2[r0]
            r1[r0] = r2
            int r0 = r0 + 1
            goto L_0x01b5
        L_0x01cb:
            r0 = 3
            if (r11 != r0) goto L_0x0037
            int r0 = r10.dY
            if (r0 != r8) goto L_0x0037
            int[][] r0 = r10.ga
            d r1 = r10.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            r1 = 8
            if (r0 < r1) goto L_0x0037
            r0 = r5
        L_0x01e3:
            short[] r1 = r10.hc
            int r1 = r1.length
            if (r0 >= r1) goto L_0x0037
            short[][][][] r1 = r10.hh
            r2 = 3
            r1 = r1[r2]
            r1 = r1[r8]
            r1 = r1[r9]
            short[] r2 = r10.hg
            short r2 = r2[r0]
            r1[r0] = r2
            int r0 = r0 + 1
            goto L_0x01e3
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.a(int, int):void");
    }

    private void a(int i, int i2, int i3) {
        int i4 = i3 == 0 ? 1 : 2;
        short s = this.dq[i].lg;
        new int[1][0] = 0;
        if (s != 21) {
            int n = n(this.f0do.nextInt()) % 100;
            if (i2 == 0) {
                if (n <= 50) {
                    b(3, this.dq[i].kD, this.dq[i].eg, 0, 99);
                    return;
                }
                if (n % 3 == 0 && this.ga[this.dp.kH][13] < 20) {
                    b(14, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
                if (n % 3 == 1 && this.ga[this.dp.kH][14] < 20) {
                    b(15, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
                if (n % 3 == 2 && this.ga[this.dp.kH][15] < 20) {
                    b(16, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
            } else if (i2 == 1) {
                if (n <= 70) {
                    b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                } else {
                    b(10, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
            } else if (i2 != 2) {
            } else {
                if (n > (i4 * 50) - ((i4 * (this.ga[this.dp.kH][12] * 2)) + 9)) {
                    b(10, this.dq[i].kD, this.dq[i].eg, 0, 99);
                } else if (this.ga[this.dp.kH][12] < 20) {
                    b(this.dp.kH + 12, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
            }
        }
    }

    private void a(int i, int i2, int i3, int i4, int i5) {
        if (i2 >= this.dz[this.dq[i].kH][0][0] || i3 >= this.dz[this.dq[i].kH][0][1]) {
            if (i2 < this.dz[this.dq[i].kH][1][0] && i3 < this.dz[this.dq[i].kH][1][1] && i2 > this.dz[this.dq[i].kH][0][0]) {
                if (i4 > 13) {
                    this.dq[i].jC = 0;
                } else if (this.dq[i].kH == 2 || this.dq[i].kH == 4 || this.dq[i].kH == 5 || this.dq[i].kH == 12) {
                    this.dq[i].jC = 7;
                }
            }
        } else if (this.dq[i].kH < 10) {
            if (this.dq[i].kM < this.dq[i].bL) {
                d dVar = this.dq[i];
                dVar.kM = (byte) (dVar.kM + 1);
                return;
            }
            this.dq[i].jC = 6;
            this.dq[i].kM = 0;
        } else if (i5 <= i4 && this.dq[i].x < 0) {
            this.dq[i].jC = 6;
            this.dq[i].x = this.dq[i].ih;
        }
    }

    private void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13) {
        this.kg[i] = new int[]{i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13};
    }

    private void a(int i, int i2, boolean z, short[][][] sArr) {
        this.kp = null;
        this.dY = i2;
        this.eb = sArr[i][i2][0];
        this.ea = sArr[i][i2][1];
        this.ec = sArr[i][i2][2];
        this.ed = sArr[i][i2][3];
        this.eg = sArr[i][i2][5];
        this.ee = sArr[i][i2][6];
        this.eh = sArr[i][i2][7];
        this.ei = sArr[i][i2][8];
        int i3 = 0;
        for (int i4 = 0; i4 < this.ko.length; i4++) {
            if (!z && this.ko[i4][0] > 0) {
                i3++;
            } else if (this.ko[i4][3] + this.ko[i4][6] <= this.ea || this.ko[i4][3] > this.ed + 100 || this.ko[i4][2] + this.ko[i4][5] <= this.eb || this.ko[i4][2] > this.ec) {
                this.ko[i4][0] = -1;
            } else {
                if (this.ko[i4][3] >= this.ei) {
                    this.ko[i4][0] = 3;
                } else {
                    this.ko[i4][0] = 1;
                }
                i3++;
            }
        }
        if (i3 > 0 && i3 < this.ko.length) {
            this.kp = (int[][]) Array.newInstance(Integer.TYPE, i3, 7);
            int i5 = 0;
            for (int i6 = 0; i6 < this.ko.length; i6++) {
                if (this.ko[i6][0] > 0) {
                    this.kp[i5] = this.ko[i6];
                    i5++;
                }
            }
        }
    }

    private void a(d dVar, int i) {
        switch (dVar.kH) {
            case 0:
            case 1:
            case 7:
            case 8:
            case 9:
            case 11:
            default:
                return;
            case 2:
            case 3:
                if (dVar.kC == 10 && dVar.kB[dVar.kC][dVar.lf][0] == 27 && dVar.le == 0) {
                    dVar.kY = a(5, dVar.kD - (this.kq[dVar.dP] * 44), dVar.eg, (dVar.eg - 30) + 27, 10, 0, dVar.dP, i, dVar.hP);
                    return;
                }
                return;
            case 4:
                if ((dVar.kC == 10 || dVar.kC == 11 || dVar.kC == 12) && dVar.kB[dVar.kC][dVar.lf][0] == 37 && dVar.le == 1) {
                    dVar.kY = a(6, dVar.kD - (this.kq[dVar.dP] * d.hD), dVar.eg, (dVar.eg - 44) + 27, 10, 0, dVar.dP, i, dVar.hP);
                    return;
                }
                return;
            case 5:
                if (dVar.kC != 11 && dVar.kC != 10) {
                    return;
                }
                if (dVar.kC == 11) {
                    if (dVar.kB[dVar.kC][dVar.lf][0] == 35 && dVar.le == 0) {
                        dVar.kY = a(7, dVar.kD - (this.kq[dVar.dP] * d.hD), dVar.eg, (dVar.eg - 22) + 27, 10, 0, dVar.dP, i, dVar.hP);
                        return;
                    }
                    return;
                } else if (dVar.kC == 10 && dVar.kB[dVar.kC][dVar.lf][0] == 33 && dVar.le == 0) {
                    dVar.kY = a(7, dVar.kD - (this.kq[dVar.dP] * d.hD), dVar.eg, (dVar.eg - 22) + 27, 10, 0, dVar.dP, i, dVar.hP);
                    return;
                } else {
                    return;
                }
            case 6:
                if (dVar.kC == 9 && dVar.kB[dVar.kC][dVar.lf][0] == 10 && dVar.le == 0) {
                    dVar.kY = a(7, this.dp.kD, this.dp.eg + 1, this.dp.eg + 1, 10, 0, dVar.dP, i, this.dp.hP);
                    return;
                }
                return;
            case 10:
                if (dVar.kC == 9 && dVar.kB[dVar.kC][dVar.lf][0] == 27 && dVar.le == 0) {
                    dVar.kY = a(8, dVar.kD - (this.kq[dVar.dP] * d.dX), dVar.eg, dVar.eg - 32, 15, 0, dVar.dP, i, dVar.hP);
                    return;
                }
                return;
            case 12:
                if ((dVar.kC == 11 || dVar.kC == 10) && dVar.kB[dVar.kC][dVar.lf][0] == 33 && dVar.le == 1) {
                    dVar.kY = a(12, dVar.kD - (this.kq[dVar.dP] * d.hD), dVar.eg, dVar.eg - 22, 10, 0, dVar.dP, i, dVar.hP);
                    if (dVar.kH == 5) {
                        this.dq[i].kY = -1;
                        return;
                    }
                    return;
                }
                return;
        }
    }

    private static boolean a(int i, int i2, int i3, int i4, int i5, int i6) {
        return i5 >= i && i5 <= i3 && i6 >= i2 && i6 <= i4;
    }

    private boolean a(int i, d dVar) {
        if ((dVar.kH == 4 || dVar.kH == 5) && this.dr[i][0] == 6) {
            return false;
        }
        if ((dVar.kH == 2 || dVar.kH == 3) && this.dr[i][0] == 5) {
            return false;
        }
        return !a(dVar.kC, this.eN) && this.dr[i][9] != 0 && this.dr[i][10] != 0 && ((this.dr[i][8] == 0 && this.dr[i][3] + this.dr[i][9] >= dVar.ee && this.dr[i][3] - this.dr[i][9] <= dVar.hy) || (this.dr[i][8] == 1 && this.dr[i][3] - this.dr[i][9] <= dVar.hy && this.dr[i][3] + this.dr[i][9] >= dVar.ee)) && n(this.dr[i][4] - dVar.eg) <= this.dr[i][10] && this.dr[i][11] > 0;
    }

    public static boolean a(int i, int[] iArr) {
        if (iArr == null) {
            return false;
        }
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    private boolean a(d dVar) {
        if (dVar.kH > 1 && !dVar.lk) {
            return true;
        }
        boolean a = a(this.eg, this.ee, this.eh, this.ei, dVar.kD, dVar.eg);
        if (a) {
            return true;
        }
        if (dVar.kH > 1 || !this.dS) {
            return a;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.hh[this.dO][this.dY].length || a) {
                return a;
            }
            a = a(this.hh[this.dO][this.dY][i2][3], this.hh[this.dO][this.dY][i2][4], this.hh[this.dO][this.dY][i2][5], this.hh[this.dO][this.dY][i2][6], dVar.kD, dVar.eg);
            i = i2 + 1;
        }
    }

    private boolean a(d dVar, d dVar2) {
        int i = dVar.es;
        int i2 = dVar.dZ;
        int i3 = dVar.dV;
        int i4 = dVar.kT;
        return i < dVar2.hy && i3 > dVar2.ee && i2 < dVar2.eg && i4 > dVar2.eg - 1;
    }

    private static int[] a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, int i14) {
        return new int[]{i, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, 99, i14};
    }

    private static String[] a(String str, String[] strArr) {
        Vector vector = new Vector();
        int indexOf = str.indexOf("\n", 1);
        int i = 0;
        while (indexOf != -1) {
            vector.addElement(str.substring(i, indexOf - 1));
            int i2 = indexOf + 1;
            i = i2;
            indexOf = str.indexOf("\n", i2);
        }
        vector.addElement(str.substring(i, str.length()));
        String[] strArr2 = new String[vector.size()];
        vector.copyInto(strArr2);
        vector.removeAllElements();
        for (int i3 = 0; i3 < strArr2.length; i3++) {
            if (!strArr2[i3].equals(strArr[i3])) {
                strArr2[i3] = strArr2[i3].substring(strArr[i3].length());
            } else {
                strArr2[i3] = "";
            }
            System.out.println(new StringBuffer().append("R ").append(i3).append(" = ").append(strArr2[i3]).append("  R[i].length()==").append(strArr2[i3].length()).toString());
        }
        return strArr2;
    }

    private short[][] a(int i, byte[] bArr) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= this.hT.length) {
                i2 = 0;
                break;
            } else if (i == i3) {
                i2 = this.hT[i3];
                break;
            } else {
                i4 += this.hT[i3];
                i3++;
            }
        }
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i4, bArr2, 0, i2);
        try {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr2));
            dataInputStream.readInt();
            short[][] sArr = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 4);
            for (int i5 = 0; i5 < sArr.length; i5++) {
                for (int i6 = 0; i6 < sArr[i5].length; i6++) {
                    sArr[i5][i6] = dataInputStream.readShort();
                }
            }
            return sArr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void aa() {
        try {
            if (this.aM) {
                this.aL.D(-1);
                this.aL.start();
            }
        } catch (Exception e) {
        }
    }

    private void ad() {
        this.ko = null;
        this.ko = (int[][]) Array.newInstance(Integer.TYPE, Player.PREFETCHED, 7);
        for (int[] iArr : this.ko) {
            iArr[0] = -1;
        }
    }

    private byte[] ae() {
        byte[] bArr = new byte[this.hV[1]];
        try {
            getClass();
            this.eu = MIDPHelper.p(this.hU[1]);
            this.eu.read(bArr);
            this.eu.close();
            return bArr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] af() {
        byte[] bArr = new byte[this.hV[0]];
        try {
            getClass();
            this.eu = MIDPHelper.p(this.hU[0]);
            this.eu.read(bArr);
            this.eu.close();
            return bArr;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private void ag() {
        if (this.dr != null) {
            for (int i = 0; i < this.dr.length; i++) {
                if (this.dr[i][0] != 0) {
                    if (this.dr[i][3] < this.eb - 60 || this.dr[i][3] > this.ec + 60 || this.dr[i][12] < 0) {
                        this.dr[i][0] = 0;
                        int i2 = this.dr[i][13];
                        if (this.dq != null && i2 < this.dq.length && i2 >= 0 && this.dq[i2] != null) {
                            this.dq[i2].kY = -1;
                        }
                    } else {
                        this.dr[i][3] = this.dr[i][3] - (this.kq[this.dr[i][8]] * this.dr[i][6]);
                    }
                    int[] iArr = this.dr[i];
                    iArr[12] = iArr[12] - 1;
                }
            }
        }
    }

    private void ah() {
        if (this.fI >= this.fJ) {
            d(15, 0);
            return;
        }
        this.fI++;
        this.fK = 1;
        this.fL = ((this.fI + 1) / 2) + 1;
        this.fM = (this.fI / 2) + 7;
        this.dq = null;
        this.dq = new d[(this.fK + this.fL + this.fM)];
        this.dq[0] = new d();
        short[][] sArr = this.gH[this.gI];
        int n = n(this.f0do.nextInt()) % sArr.length;
        this.dq[0].a(new int[]{6, 7}[n % 2], -1, sArr[n][0] + (n(this.f0do.nextInt()) % sArr[n][2]), (n(this.f0do.nextInt()) % sArr[n][3]) + sArr[n][1]);
        d dVar = this.dq[0];
        d dVar2 = this.dq[0];
        int i = (this.fI * Player.PREFETCHED) + 700;
        dVar2.kG = i;
        dVar.kF = i;
        this.dq[0].hP = (this.fI * 25) + 100;
        this.dq[0].kS = 50;
        this.dq[0].lh = (short) ((this.fI + 5) % 5);
        this.dq[0].lg = 114;
        this.dq[0].ll = 1;
        this.dq[0].lm = this.dq[0].kD - this.dp.kD > 0 ? (byte) 1 : 0;
        this.dq[0].kR = d.hD;
        int i2 = 0 + 1;
        int i3 = 0;
        while (i3 < this.fM) {
            this.dq[i2] = new d();
            int n2 = n(this.f0do.nextInt()) % sArr.length;
            this.dq[i2].a(11, -1, sArr[n2][0] + (n(this.f0do.nextInt()) % sArr[n2][2]), (n(this.f0do.nextInt()) % sArr[n2][3]) + sArr[n2][1]);
            d dVar3 = this.dq[i2];
            d dVar4 = this.dq[i2];
            int i4 = (this.fI * 120) + Player.PREFETCHED;
            dVar4.kG = i4;
            dVar3.kF = i4;
            this.dq[i2].hP = (this.fI * 10) + 30;
            this.dq[i2].kS = 10;
            this.dq[i2].lg = 48;
            this.dq[i2].ll = 1;
            this.dq[i2].lm = this.dq[i2].kD - this.dp.kD > 0 ? (byte) 1 : 0;
            this.dq[i2].kR = d.hD;
            i3++;
            i2++;
        }
        int i5 = 0;
        while (i5 < this.fL) {
            this.dq[i2] = new d();
            int n3 = n(this.f0do.nextInt()) % sArr.length;
            this.dq[i2].a(10, -1, sArr[n3][0] + (n(this.f0do.nextInt()) % sArr[n3][2]), (n(this.f0do.nextInt()) % sArr[n3][3]) + sArr[n3][1]);
            d dVar5 = this.dq[i2];
            d dVar6 = this.dq[i2];
            int i6 = (this.fI * 100) + Player.REALIZED;
            dVar6.kG = i6;
            dVar5.kF = i6;
            this.dq[i2].hP = (this.fI * 7) + 10;
            this.dq[i2].kS = 10;
            this.dq[i2].lg = 47;
            this.dq[i2].ll = 1;
            this.dq[i2].lm = this.dq[i2].kD - this.dp.kD > 0 ? (byte) 1 : 0;
            this.dq[i2].kR = d.hD;
            i5++;
            i2++;
        }
    }

    private boolean ai() {
        if (this.dq == null) {
            return true;
        }
        for (int i = 0; i < this.dq.length; i++) {
            if (this.dq[i].eH > 0 && this.dq[i].ix) {
                return false;
            }
        }
        return true;
    }

    private int b(int i, int i2, int i3, int i4, int i5) {
        if (this.kr == null) {
            this.kr = (int[][]) Array.newInstance(Integer.TYPE, 20, 7);
        }
        int i6 = -1;
        int i7 = 0;
        while (true) {
            if (i7 >= this.kr.length) {
                break;
            } else if (this.kr[i7][0] == 0) {
                i6 = i7;
                break;
            } else {
                i7++;
            }
        }
        this.kr[i6] = null;
        this.kr[i6] = new int[]{i, i2, i3, 8, 8, i4, i5};
        return i6;
    }

    private void b(int i) {
        switch (this.cx) {
            case 0:
            case 1:
            case 10:
            case 12:
            default:
                return;
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 11:
                if (i != 5 || this.ik) {
                    if (i != 6) {
                        return;
                    }
                    if (this.iq != 0) {
                        this.iq--;
                        return;
                    } else if (this.cx == 6) {
                        this.ci = this.dB;
                        return;
                    } else if (this.cx == 2 || this.cx == 3) {
                        if (this.dB == 3) {
                            this.ci = this.dB;
                            return;
                        }
                        return;
                    } else if (this.cx == 11) {
                        if (this.dB == 12 || this.dB == 30) {
                            this.ci = this.dB;
                            return;
                        } else {
                            this.ci = 11;
                            return;
                        }
                    } else if (((this.cx >= 7 && this.cx <= 9) || this.cx == 5 || this.cx == 4) && this.dB == 12) {
                        this.ci = this.dB;
                        return;
                    } else {
                        return;
                    }
                } else if (this.iq != this.ip) {
                    this.iq++;
                    return;
                } else {
                    this.ik = true;
                    this.iq = 0;
                    for (int i2 = 0; i2 < 9; i2++) {
                        if (this.cx == this.ir[i2]) {
                            this.ia.a(kz, kA, null);
                        }
                    }
                    this.ih = this.cx;
                    this.cx = 12;
                    return;
                }
            case 13:
                if (i != 5) {
                    return;
                }
                if (this.ih == 6) {
                    this.ci = this.dB;
                    return;
                } else if (this.ih == 11) {
                    if (this.dB == 12 || this.dB == 30) {
                        this.ci = this.dB;
                        return;
                    } else {
                        this.ci = 11;
                        return;
                    }
                } else if (this.ih == 3 || this.ih == 2) {
                    this.ci = this.dB;
                    return;
                } else if ((this.ih == 5 || ((this.ih >= 7 && this.ih <= 9) || this.ih == 4)) && this.dB == 12) {
                    this.ci = this.dB;
                    return;
                } else {
                    return;
                }
            case 14:
                if (i == 6) {
                    this.cx = this.ih;
                    return;
                }
                return;
        }
    }

    private void b(int i, int i2) {
        boolean z;
        short s = this.dq[i].lg;
        int[] iArr = {0};
        if (s != 21) {
            int n = n(this.f0do.nextInt()) % 100;
            if (a(s, iArr)) {
                if (n < 75) {
                    b(11, this.dq[i].kD, this.dq[i].eg, 0, 99);
                } else {
                    b(10, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                if (i2 != 1) {
                    if (this.dp.kF < (this.dp.kG * 40) / 100 && n < 40) {
                        if (n < 10) {
                            b(3, this.dq[i].kD, this.dq[i].eg, 0, 99);
                            z = true;
                        } else {
                            b(2, this.dq[i].kD, this.dq[i].eg, 0, 99);
                            z = true;
                        }
                    }
                    if (z) {
                        return;
                    }
                }
                if (n % 3 == 0 && n > this.ga[this.dp.kH][13] * 5 && this.ga[this.dp.kH][13] < 20) {
                    b(14, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
                if (n % 3 == 1 && n > this.ga[this.dp.kH][14] * 5 && this.ga[this.dp.kH][14] < 20) {
                    b(15, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
                if (n % 3 == 2 && n > this.ga[this.dp.kH][15] * 5 && this.ga[this.dp.kH][15] < 20) {
                    b(16, this.dq[i].kD, this.dq[i].eg, 0, 99);
                }
            }
        }
    }

    private void b(int i, int i2, int i3) {
        int i4 = i3 == 0 ? 1 : 2;
        int n = n(this.f0do.nextInt()) % 100;
        if (n <= (i4 * 50) - ((i4 * (this.ga[this.dp.kH][12] * 2)) + 9)) {
            if (this.ga[this.dp.kH][12] < 20) {
                b(this.dp.kH + 12, this.dq[i].kD, this.dq[i].eg, 0, 99);
            }
        } else if (i2 == 0) {
            if (n % 3 == 0 && this.ga[this.dp.kH][13] < 20) {
                b(14, this.dq[i].kD, this.dq[i].eg, 0, 99);
            }
            if (n % 3 == 1 && this.ga[this.dp.kH][14] < 20) {
                b(15, this.dq[i].kD, this.dq[i].eg, 0, 99);
            }
            if (n % 3 == 2 && this.ga[this.dp.kH][15] < 20) {
                b(16, this.dq[i].kD, this.dq[i].eg, 0, 99);
            }
        } else {
            b(10, this.dq[i].kD, this.dq[i].eg, 0, 99);
        }
    }

    private void b(int i, byte[] bArr) {
        int i2;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            if (i3 >= this.hS.length) {
                i2 = 0;
                break;
            } else if (i == i3) {
                i2 = this.hS[i3];
                break;
            } else {
                i4 += this.hS[i3];
                i3++;
            }
        }
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i4, bArr2, 0, i2);
        byte b = e.lF[i];
        try {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr2));
            this.fN[b] = null;
            try {
                dataInputStream.readInt();
                this.fN[b] = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 6);
                for (int i5 = 0; i5 < this.fN[b].length; i5++) {
                    for (int i6 = 0; i6 < this.fN[b][i5].length; i6++) {
                        this.fN[b][i5][i6] = dataInputStream.readShort();
                    }
                }
                this.fP[i] = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 4);
                for (int i7 = 0; i7 < this.fP[i].length; i7++) {
                    for (int i8 = 0; i8 < this.fP[i][i7].length; i8++) {
                        this.fP[i][i7][i8] = dataInputStream.readShort();
                    }
                }
                this.fQ[i] = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 4);
                for (int i9 = 0; i9 < this.fQ[i].length; i9++) {
                    for (int i10 = 0; i10 < this.fQ[i][i9].length; i10++) {
                        this.fQ[i][i9][i10] = dataInputStream.readShort();
                    }
                }
                this.fO[i] = new short[(-dataInputStream.readShort())][][];
                for (int i11 = 0; i11 < this.fO[i].length; i11++) {
                    this.fO[i][i11] = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 4);
                    for (int i12 = 0; i12 < this.fO[i][i11].length; i12++) {
                        for (int i13 = 0; i13 < this.fO[i][i11][i12].length; i13++) {
                            this.fO[i][i11][i12][i13] = dataInputStream.readShort();
                        }
                    }
                }
                e.ls[i] = new short[(-dataInputStream.readShort())][][];
                for (int i14 = 0; i14 < e.ls[i].length; i14++) {
                    e.ls[i][i14] = (short[][]) Array.newInstance(Short.TYPE, -dataInputStream.readShort(), 2);
                    for (int i15 = 0; i15 < e.ls[i][i14].length; i15++) {
                        for (int i16 = 0; i16 < e.ls[i][i14][i15].length; i16++) {
                            e.ls[i][i14][i15][i16] = dataInputStream.readShort();
                        }
                    }
                }
            } catch (IOException e) {
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(d dVar) {
        if (dVar.ix) {
            if (dVar.lk) {
                if (dVar.kD < this.eg) {
                    dVar.kD = this.eg;
                }
                if (dVar.kD > this.eh) {
                    dVar.kD = this.eh;
                }
                if (dVar.eg < this.ee) {
                    dVar.eg = this.ee;
                }
                if (dVar.eg > this.ei) {
                    dVar.eg = this.ei;
                }
            }
            short s = dVar.kB[dVar.kC][dVar.lf][0];
            int i = (dVar.le != 0 || s >= this.fQ[dVar.kH].length - 1) ? s : s + 1;
            int i2 = dVar.dP;
            d dVar2 = this.dp;
            if (i2 == 0) {
                dVar.ee = dVar.kD + this.fQ[dVar.kH][s][0];
                dVar.hy = this.fQ[dVar.kH][s][2] + dVar.kD;
                dVar.es = dVar.kD + this.fP[dVar.kH][i][0];
                dVar.dV = dVar.kD + this.fP[dVar.kH][i][2];
            } else {
                int i3 = dVar.dP;
                d dVar3 = this.dp;
                if (i3 == 1) {
                    dVar.ee = dVar.kD - this.fQ[dVar.kH][s][2];
                    dVar.hy = dVar.kD - this.fQ[dVar.kH][s][0];
                    dVar.es = dVar.kD - this.fP[dVar.kH][i][2];
                    dVar.dV = dVar.kD - this.fP[dVar.kH][i][0];
                }
            }
            dVar.dZ = dVar.eg + this.fP[dVar.kH][i][1];
            dVar.kT = dVar.eg + this.fP[dVar.kH][i][3];
            if (dVar.kC < 9 || dVar.kC > 24) {
                dVar.kQ = 0;
            } else {
                dVar.kQ = (dVar.hP * e.lH[dVar.kH][dVar.kC - 9]) / 100;
                if (dVar.kH < 2) {
                    dVar.li = n(this.f0do.nextInt()) % 100 <= dVar.ij;
                    if (dVar.li) {
                        dVar.kQ += dVar.kQ / 2;
                    }
                }
            }
            dVar.kZ = dVar.kD + this.eC > -32 && dVar.kD + this.eC < this.aR + 32 && dVar.eg + this.eD > -32 && dVar.eg + this.eD < this.aS + 32;
        }
    }

    private byte[] b(byte[] bArr) {
        try {
            this.ic = ar.a("mj2znn", true);
            if (this.ic.bp() == 1) {
                this.ic.b(bArr, 0, bArr.length);
            }
            byte[] G = this.ic.G(1);
            this.ic.bn();
            return G;
        } catch (Exception e) {
            e.printStackTrace();
            return bArr;
        }
    }

    private String c(String str) {
        String str2;
        Exception e;
        String str3 = "";
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        byte[] bArr = new byte[10240];
        try {
            getClass();
            InputStream p = MIDPHelper.p(str);
            if (p != null) {
                while (true) {
                    int read = p.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    dataOutputStream.write(bArr, 0, read);
                }
                String str4 = new String(byteArrayOutputStream.toByteArray(), Database.ENCODING);
                try {
                    p.close();
                    str3 = str4;
                } catch (Exception e2) {
                    e = e2;
                    str2 = str4;
                    System.out.println(new StringBuffer().append("getTextByUTF Error:").append(e.toString()).toString());
                    return str2;
                }
            }
            try {
                dataOutputStream.close();
                byteArrayOutputStream.close();
                return str3;
            } catch (Exception e3) {
                Exception exc = e3;
                str2 = str3;
                e = exc;
            }
        } catch (Exception e4) {
            Exception exc2 = e4;
            str2 = str3;
            e = exc2;
            System.out.println(new StringBuffer().append("getTextByUTF Error:").append(e.toString()).toString());
            return str2;
        }
    }

    private void c(int i) {
        if (i == 0 || i == 1) {
            if (i == 0) {
                if (this.dl > 0) {
                    this.dl--;
                } else {
                    this.dl = 2;
                }
            } else if (i != 1) {
            } else {
                if (this.dl < 2) {
                    this.dl++;
                } else {
                    this.dl = 0;
                }
            }
        } else if (i == 5 || i == 4) {
            if (this.dl == 0) {
                this.dm = 1;
                d(28, 2);
            } else if (this.dl == 1) {
                o(this.dp.kH);
                if (this.dn == 0) {
                    if (this.ga[this.dp.kH][21] == 1) {
                        this.dn = 0;
                    } else {
                        this.dn = 3;
                    }
                } else if (this.dn == 1) {
                    if (this.ga[this.dp.kH][22] == 1) {
                        this.dn = 1;
                    } else {
                        this.dn = 3;
                    }
                } else if (this.dn == 2) {
                    if (this.ga[this.dp.kH][23] == 1) {
                        this.dn = 2;
                    } else {
                        this.dn = 3;
                    }
                } else if (this.dn == 3) {
                    this.dn = 3;
                }
                this.dm = 2;
                this.dl = 1;
                o(this.dC);
                this.jX = 0;
                d dVar = this.dp;
                d dVar2 = this.dp;
                d dVar3 = this.dp;
                int i2 = this.gh;
                dVar3.z = i2;
                dVar2.kF = i2;
                dVar.kG = i2;
                this.by = 8;
                d(29, 2);
            } else if (this.dl == 2) {
                d(30, 2);
            }
        } else if (i == 6) {
            d(3, 2);
        }
    }

    private void c(int i, int i2) {
        String[][] strArr = {new String[]{"2.data", "3.data", "4.data", "5.data", "6.data", "7.data", "8.data", "9.data", "10.data"}, new String[]{"11.data", "12.data", "13.data", "14.data"}};
        byte[] bArr = new byte[4];
        this.ki = null;
        this.kk = null;
        this.kj = null;
        try {
            getClass();
            this.eu = MIDPHelper.p(new StringBuffer().append("/data/").append(strArr[i][i2]).toString());
            this.eu.read(bArr);
            byte[] bArr2 = new byte[(new DataInputStream(new ByteArrayInputStream(bArr)).readInt() - 4)];
            this.eu.read(bArr2);
            this.eu.close();
            this.eu = null;
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr2));
            try {
                this.ki = (short[][]) Array.newInstance(Short.TYPE, dataInputStream.readShort(), 7);
                for (int i3 = 0; i3 < this.ki.length; i3++) {
                    for (int i4 = 0; i4 < this.ki[i3].length; i4++) {
                        this.ki[i3][i4] = dataInputStream.readShort();
                    }
                }
                this.kk = new short[dataInputStream.readShort()][][];
                for (int i5 = 0; i5 < this.kk.length; i5++) {
                    this.kk[i5] = (short[][]) Array.newInstance(Short.TYPE, dataInputStream.readShort(), 5);
                    for (int i6 = 0; i6 < this.kk[i5].length; i6++) {
                        for (int i7 = 0; i7 < 5; i7++) {
                            this.kk[i5][i6][i7] = dataInputStream.readShort();
                        }
                    }
                }
                this.kj = new short[dataInputStream.readShort()][][];
                for (int i8 = 0; i8 < this.kj.length; i8++) {
                    this.kj[i8] = (short[][]) Array.newInstance(Short.TYPE, dataInputStream.readShort(), 5);
                    for (int i9 = 0; i9 < this.kj[i8].length; i9++) {
                        for (int i10 = 0; i10 < 5; i10++) {
                            this.kj[i8][i9][i10] = dataInputStream.readShort();
                        }
                    }
                }
                this.km = new short[dataInputStream.readShort()][][];
                for (int i11 = 0; i11 < this.km.length; i11++) {
                    this.km[i11] = (short[][]) Array.newInstance(Short.TYPE, dataInputStream.readShort(), 5);
                    for (int i12 = 0; i12 < this.km[i11].length; i12++) {
                        for (int i13 = 0; i13 < 5; i13++) {
                            this.km[i11][i12][i13] = dataInputStream.readShort();
                        }
                    }
                }
                this.kl = new short[dataInputStream.readShort()][][];
                for (int i14 = 0; i14 < this.kl.length; i14++) {
                    this.kl[i14] = (short[][]) Array.newInstance(Short.TYPE, dataInputStream.readShort(), 5);
                    for (int i15 = 0; i15 < this.kl[i14].length; i15++) {
                        for (int i16 = 0; i16 < 5; i16++) {
                            this.kl[i14][i15][i16] = dataInputStream.readShort();
                        }
                    }
                }
            } catch (IOException e) {
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void d(int i) {
        if (i == 0) {
            if (this.dn <= 0) {
                this.dn = 3;
            } else {
                this.dn--;
            }
        } else if (i == 1) {
            if (this.dn >= 3) {
                this.dn = 0;
            } else {
                this.dn++;
            }
        } else if (i == 4 || i == 5) {
            if (this.dn == 0) {
                if (this.ga[this.dp.kH][21] == 1) {
                    this.dn = 0;
                    this.hp = 61;
                } else {
                    this.dn = 3;
                }
            } else if (this.dn == 1) {
                if (this.ga[this.dp.kH][22] == 1) {
                    this.dn = 1;
                    this.hp = 61;
                } else {
                    this.dn = 3;
                }
            } else if (this.dn == 2) {
                if (this.ga[this.dp.kH][23] == 1) {
                    this.dn = 2;
                    this.hp = 61;
                } else {
                    this.dn = 3;
                }
            } else if (this.dn == 3) {
                this.dn = 3;
            }
            this.dm = 2;
            o(this.dC);
            d dVar = this.dp;
            d dVar2 = this.dp;
            d dVar3 = this.dp;
            int i2 = this.gh;
            dVar3.z = i2;
            dVar2.kF = i2;
            dVar.kG = i2;
        } else if (i == 6) {
            this.dm = 2;
            this.dn = 3;
        }
    }

    private void d(int i, int i2) {
        if (this.eZ <= 0) {
            this.fb = i2;
            this.hp = -1;
            this.bs = -1;
            if (i2 == 0) {
                this.eX = i;
            } else if (i2 == 1) {
                this.eY = i;
            } else {
                if (i2 == 2) {
                    this.ci = i;
                } else if (i2 == 3) {
                    this.cJ = i;
                }
                this.aK[0] = 0;
                this.aK[1] = 0;
                return;
            }
            this.eZ = 3;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0056 A[LOOP:1: B:13:0x0053->B:15:0x0056, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a7 A[LOOP:2: B:28:0x00a1->B:30:0x00a7, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String[] d(java.lang.String r11) {
        /*
            r10 = this;
            r8 = 1
            r7 = 0
            java.lang.String r0 = ""
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            java.io.DataOutputStream r2 = new java.io.DataOutputStream
            r2.<init>(r1)
            r3 = 10240(0x2800, float:1.4349E-41)
            byte[] r3 = new byte[r3]
            r10.getClass()     // Catch:{ Exception -> 0x0026 }
            java.io.InputStream r4 = javax.microedition.enhance.MIDPHelper.p(r11)     // Catch:{ Exception -> 0x0026 }
            if (r4 == 0) goto L_0x007c
        L_0x001b:
            int r5 = r4.read(r3)     // Catch:{ Exception -> 0x0026 }
            if (r5 <= 0) goto L_0x006d
            r6 = 0
            r2.write(r3, r6, r5)     // Catch:{ Exception -> 0x0026 }
            goto L_0x001b
        L_0x0026:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x002a:
            java.io.PrintStream r2 = java.lang.System.out     // Catch:{ all -> 0x0088 }
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ all -> 0x0088 }
            r3.<init>()     // Catch:{ all -> 0x0088 }
            java.lang.String r4 = "getTextByUTF Error:"
            java.lang.StringBuffer r3 = r3.append(r4)     // Catch:{ all -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0088 }
            java.lang.StringBuffer r0 = r3.append(r0)     // Catch:{ all -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0088 }
            r2.println(r0)     // Catch:{ all -> 0x0088 }
            r0 = r1
        L_0x0047:
            java.util.Vector r1 = new java.util.Vector
            r1.<init>()
            java.lang.String r2 = "\n"
            int r2 = r0.indexOf(r2, r8)
            r3 = r7
        L_0x0053:
            r4 = -1
            if (r2 == r4) goto L_0x008a
            java.lang.String r3 = r0.substring(r3, r2)
            java.lang.String r3 = r3.trim()
            r1.addElement(r3)
            java.lang.String r3 = "\n"
            int r4 = r2 + 1
            int r3 = r0.indexOf(r3, r4)
            r9 = r3
            r3 = r2
            r2 = r9
            goto L_0x0053
        L_0x006d:
            byte[] r3 = r1.toByteArray()     // Catch:{ Exception -> 0x0026 }
            java.lang.String r5 = new java.lang.String     // Catch:{ Exception -> 0x0026 }
            java.lang.String r6 = "UTF-8"
            r5.<init>(r3, r6)     // Catch:{ Exception -> 0x0026 }
            r4.close()     // Catch:{ Exception -> 0x00b3 }
            r0 = r5
        L_0x007c:
            r2.close()     // Catch:{ Exception -> 0x0083 }
            r1.close()     // Catch:{ Exception -> 0x0083 }
            goto L_0x0047
        L_0x0083:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x002a
        L_0x0088:
            r0 = move-exception
            throw r0
        L_0x008a:
            int r2 = r0.length()
            int r2 = r2 - r8
            java.lang.String r0 = r0.substring(r3, r2)
            java.lang.String r0 = r0.trim()
            r1.addElement(r0)
            int r0 = r1.size()
            java.lang.String[] r0 = new java.lang.String[r0]
            r2 = r7
        L_0x00a1:
            int r3 = r1.size()
            if (r2 >= r3) goto L_0x00b2
            java.lang.Object r10 = r1.elementAt(r2)
            java.lang.String r10 = (java.lang.String) r10
            r0[r2] = r10
            int r2 = r2 + 1
            goto L_0x00a1
        L_0x00b2:
            return r0
        L_0x00b3:
            r0 = move-exception
            r1 = r5
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.d(java.lang.String):java.lang.String[]");
    }

    private String e(String str) {
        String str2;
        Exception e;
        getClass();
        InputStream p = MIDPHelper.p(str);
        if (p == null) {
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            try {
                int read = p.read();
                if (read == -1) {
                    String str3 = new String(byteArrayOutputStream.toByteArray(), Database.ENCODING);
                    try {
                        p.close();
                        byteArrayOutputStream.close();
                        return str3;
                    } catch (Exception e2) {
                        e = e2;
                        str2 = str3;
                        e.printStackTrace();
                        return str2;
                    }
                } else if (!(read == 13 || read == 10)) {
                    byteArrayOutputStream.write(read);
                }
            } catch (Exception e3) {
                Exception exc = e3;
                str2 = "";
                e = exc;
                e.printStackTrace();
                return str2;
            }
        }
    }

    private void e(int i) {
        if (i == 0) {
            if (this.aW > 0) {
                this.aW--;
            } else {
                this.aW = 4;
            }
        } else if (i == 1) {
            if (this.aW < 4) {
                this.aW++;
            } else {
                this.aW = 0;
            }
        } else if (i == 4 || i == 5) {
            if (this.aW == 4 && this.ib[(this.dC * 5) + 71] == 0) {
                this.dB = this.ci;
                this.ci = 31;
                this.cx = 11;
            } else if ((this.aW == 3 && this.ib[(this.dC * 5) + 70] == 0) || ((this.aW == 2 && this.ib[(this.dC * 5) + 69] == 0) || (this.aW == 1 && this.ib[(this.dC * 5) + 68] == 0))) {
                this.hp = 66;
            } else if ((this.aW == 4 && this.ib[(this.dC * 5) + 71] != 0) || ((this.aW == 3 && this.ib[(this.dC * 5) + 70] != 0) || ((this.aW == 2 && this.ib[(this.dC * 5) + 69] != 0) || ((this.aW == 1 && this.ib[(this.dC * 5) + 68] != 0) || (this.aW == 0 && this.ib[(this.dC * 5) + 67] != 0))))) {
                this.ga[this.dC][5] = this.aW + 1;
                this.hp = 70;
                this.aP = true;
            }
        } else if (i == 6) {
            this.aP = true;
        }
    }

    private void f(int i) {
        if (i == 12) {
            d(this.dB, 2);
        } else if (i == 0) {
            this.er--;
            if (this.er < 0) {
                this.er = this.es;
            }
        } else if (i == 1) {
            this.er++;
            if (this.er > this.es) {
                this.er = 0;
            }
        } else if (i == 5 || i == 4) {
            this.eq = this.er;
            this.gd = this.ga[this.dp.kH][this.eq + 2] + 1;
            if (this.gd <= this.gf[this.eq]) {
                if (this.eq == 0 || this.eq == 1) {
                    this.ge = (this.gd * 100) + Player.STARTED;
                } else if (this.eq == 2 || this.eq == 3) {
                    this.ge = (this.gd * Player.REALIZED) + Player.PREFETCHED;
                }
                d(22, 2);
                this.fR = 0;
            }
        } else if (i == 6) {
            d(8, 2);
        }
    }

    private void g(int i) {
        switch (i) {
            case 5:
                this.dB = this.ci;
                if (this.ci == 9) {
                    this.ga[this.dp.kH][1] = this.hM;
                }
                d(10, 2);
                this.db = 11;
                return;
            case 6:
                this.cY = 0;
                if (this.ci == 8) {
                    this.hJ++;
                    if (this.hJ > this.hK) {
                        this.hJ = 0;
                    }
                    if (this.ib[34] == 1) {
                        this.ib[34] = 2;
                        c(this.ib);
                    }
                    if (this.ib[35] == 1) {
                        this.ib[35] = 2;
                        c(this.ib);
                    }
                    if (this.ib[36] == 1) {
                        this.ib[36] = 2;
                        c(this.ib);
                    }
                    this.dB = 8;
                    d(11, 2);
                    this.gC = 0;
                    this.gE = 0;
                    ac();
                    return;
                }
                return;
            case 7:
            case 8:
            case 9:
            default:
                return;
            case 10:
                if (this.ci == 8) {
                    this.eE = (-g.fP[this.dO][this.dY][0]) + (this.aT - g.fP[this.dO][this.dY][2]);
                    this.dB = 8;
                    d(24, 2);
                    return;
                }
                return;
        }
    }

    private void h(int i) {
        if (i == 2 || i == 3) {
            this.fR = (this.fR + 1) % 2;
        }
        if (i == 4) {
            if (this.fR == 0) {
                this.gY = true;
            }
            this.hu = 0;
            d(8, 2);
        }
    }

    private void k(int i) {
        this.hp = this.hs[i];
        this.ht = i;
    }

    private void l(int i) {
        this.cA = false;
        this.iQ = 0;
        this.fC = false;
        for (int i2 = 0; i2 < this.fE.length; i2++) {
            this.fE[i2] = 3;
        }
        this.x = 0;
        this.fF = 0;
        this.fG = 0;
        this.fH = 0;
        this.fD = 0;
        this.jI = false;
        this.jJ = false;
        this.jK = false;
        this.kh = false;
        this.jV = false;
        this.dT = new boolean[g.lK[i].length];
        this.dU = g.lK[i].length - 1;
        this.dV = -1;
        this.dW = false;
        this.gi = 0;
        this.gX = 0;
        this.hk = false;
        this.gZ = 0;
        this.cy = false;
        this.cz = false;
        this.cB = false;
        for (int i3 = 0; i3 < this.he.length; i3++) {
            this.hh[5][0][1][i3] = this.he[i3];
            this.hh[5][0][2][i3] = this.he[i3];
        }
        this.hW = this.ga[this.dp.kH][2] == 20 && this.ga[this.dp.kH][3] == 20 && this.ga[this.dp.kH][4] == 20 && (this.ga[this.dp.kH][12] >= 20 || this.ga[this.dp.kH][5] >= 4);
        this.hs = new byte[]{d.jo, d.jp, d.jq, 37, 38, d.jr, d.bo, d.dL, 47, 48, 49, 64};
        for (int i4 = 0; i4 < this.dT.length; i4++) {
            this.dT[i4] = true;
        }
        this.dO = i;
        this.el = 0;
        this.em = 0;
        this.en = 0;
        this.eo = 0;
        this.ep = 0;
        this.hA = 0;
        this.hB = this.dH[this.dD][0];
        this.hC = this.dH[this.dD][1];
        for (int i5 = 0; i5 < e.lt[i].length; i5++) {
            for (int i6 = 0; i6 < e.lt[i][i5][0]; i6++) {
                if (e.lt[i][i5][(i6 * 6) + 3] <= 9 || e.lt[i][i5][(i6 * 6) + 3] == 12) {
                    this.em++;
                } else {
                    this.el++;
                }
                if (i5 == this.dU && e.lt[i][i5][(i6 * 6) + 3] < 9) {
                    this.dV = i6;
                }
            }
        }
        for (int i7 = 0; i7 < e.lu[i].length; i7++) {
            this.el += e.lu[i][i7][0] + e.lu[i][i7][1];
        }
        if (this.dD == 0 && this.dY == 0) {
            this.y = false;
            return;
        }
        this.y = true;
        this.gG = false;
    }

    private void m(int i) {
        if (i == -1) {
            ab();
        } else if (this.aM) {
            try {
                getClass();
                InputStream p = MIDPHelper.p(new StringBuffer().append("/data/").append(i).append(".mid").toString());
                if (this.aL != null) {
                    this.aL.close();
                    this.aL = null;
                    this.aI.w();
                }
                this.aL = Manager.a(p, "audio/midi");
                this.aL.bg();
                this.aO = (VolumeControl) this.aL.s("VolumeControl");
                this.aO.E(this.aN * 40);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static int n(int i) {
        return i < 0 ? i * -1 : i;
    }

    private void o(int i) {
        this.gh = this.ga[i][0] + (this.ga[i][2] * (i == 0 ? Player.STARTED : Player.PREFETCHED));
        this.gp = this.ga[i][1];
        this.gk = (this.ga[i][3] * 30) + (i == 0 ? 40 : 10) + (this.ga[i][9] * 5);
        this.gl = (this.ga[i][4] * 1) + (this.ga[i][10] * 1);
        this.gm = 0;
        this.gn = this.ga[i][6];
        this.go = g.lO[i][this.ga[i][5] - 1][0];
        this.gl += g.lO[i][this.ga[i][5] - 1][1];
        this.gh += g.lO[i][this.ga[i][5] - 1][2];
        this.go += this.fY[0][this.ga[i][13]];
        this.gl += this.ga[i][14];
        this.gh += this.fY[0][this.ga[i][15]] * 10;
        this.go += g.lS[this.ga[i][7]][0];
        this.gl += g.lS[this.ga[i][7]][1];
        this.gh += g.lS[this.ga[i][7]][2];
        this.gh += this.dw[this.dn];
        this.gl += this.dx[this.dn];
        this.dp.kG = this.gh;
        this.dp.hP = this.go + this.gk + this.gm;
        this.dp.ij = this.gl;
        this.hx = ((((this.ga[i][5] * 2) + this.ga[i][3]) - (this.dO * 2)) * 10) + 100;
        if (this.hy != 100) {
            this.hz = ((this.dO - this.ga[i][2]) * 10) + Player.PREFETCHED;
        } else if (this.ga[i][2] <= 9) {
            this.hz = ((this.dO - this.ga[i][2]) * 10) + 100;
        } else {
            this.hz = 100;
        }
    }

    private void p(int i) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        if (i == 0) {
            int i7 = 0;
            while (true) {
                if (i7 >= this.kt[0].length) {
                    i3 = 0;
                    break;
                } else if ((this.eo * 100) / this.em <= this.kt[0][i7]) {
                    i3 = this.ku[i7];
                    break;
                } else {
                    i7++;
                }
            }
            int i8 = 0;
            while (true) {
                if (i8 >= this.kt[1].length) {
                    i4 = 0;
                    break;
                } else if ((this.en * 100) / this.el <= this.kt[1][i8]) {
                    i4 = this.ku[i8];
                    break;
                } else {
                    i8++;
                }
            }
            int i9 = 0;
            while (true) {
                if (i9 >= this.kt[2].length) {
                    i5 = 0;
                    break;
                } else if (this.iQ <= this.kt[2][i9]) {
                    i5 = this.ku[i9];
                    break;
                } else {
                    i9++;
                }
            }
            int i10 = 0;
            while (true) {
                if (i10 >= this.kt[3].length) {
                    i6 = 0;
                    break;
                } else if ((this.jX * 100) / this.dp.kG <= this.kt[3][i10]) {
                    i6 = this.kv[i10];
                    break;
                } else {
                    i10++;
                }
            }
            i2 = i3 + i4 + i5 + i6;
            this.gw = this.gx.length - 1;
            int i11 = 0;
            while (true) {
                if (i11 > this.gx.length) {
                    break;
                } else if (i2 < this.gx[i11]) {
                    this.gw = i11;
                    break;
                } else {
                    i11++;
                }
            }
        } else {
            this.dp.kS = (this.eo * 50) + (this.en * 10);
            i2 = this.dp.kS;
            this.gw = this.gy.length - 1;
            int i12 = 0;
            while (true) {
                if (i12 >= this.gy.length - 1) {
                    break;
                } else if (i2 <= this.gy[i12]) {
                    this.gw = i12;
                    break;
                } else {
                    i12++;
                }
            }
        }
        this.fT = i2;
    }

    private void x() {
        this.dO = this.dD;
        this.dY = 0;
        this.dZ = 0;
        this.kw = false;
        this.dp = new d();
        this.dp.a(this.dC, -1, 50, 50);
        this.dp.lg = (byte) (this.dp.kH + 51);
        o(this.dC);
        d dVar = this.dp;
        d dVar2 = this.dp;
        d dVar3 = this.dp;
        int i = this.gh;
        dVar3.z = i;
        dVar2.kF = i;
        dVar.kG = i;
        if (this.ga[this.dp.kH][11] < this.dD) {
            this.hp = 52;
            return;
        }
        this.eP = 0;
        this.dO = this.dD;
        this.dY = 0;
        this.aI.k(this.eA[this.dD] + 7);
        c(0, this.dD);
        if (this.gN != null) {
            this.gN = null;
        }
        this.gN = d(new StringBuffer().append("/data/dh").append(this.dC).append(".txt").toString());
        ad();
        int i2 = this.dD;
        l(this.dD);
        a(this.dD, this.dY);
        a(this.dD, this.dY, true, g.lK);
        this.aI.g(this.dD, 0);
        this.dp.kD = this.dH[this.dD][0];
        this.dp.eg = this.dH[this.dD][1];
        this.dp.kJ = this.dH[this.dD][2];
        d dVar4 = this.dp;
        d dVar5 = this.dp;
        int i3 = this.dp.kG;
        dVar5.z = i3;
        dVar4.kF = i3;
        this.gi = 0;
        this.hn = 0;
        this.dl = 1;
        d(25, 2);
        this.dm = 1;
        this.dk = 0;
        this.eE = 16;
        this.eF = ((this.aS - 56) - (this.ez / 6)) / 2;
        if (this.ga[this.dC][25] == 0) {
            this.iH = 0;
            this.hy = 100;
        }
    }

    public final void ab() {
        if (this.aL != null) {
            this.aL.close();
            this.aL = null;
            this.gO = -1;
            this.aI.w();
        }
    }

    public final void ac() {
        try {
            if (this.aL != null && this.aL.getState() == 400) {
                this.aL.stop();
            }
        } catch (Exception e) {
        }
    }

    public final void aj() {
        try {
            this.ks = ar.a("save", true);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            for (int writeInt : this.fS) {
                dataOutputStream.writeInt(writeInt);
            }
            for (int i = 0; i < this.ga.length; i++) {
                for (int writeInt2 : this.ga[i]) {
                    dataOutputStream.writeInt(writeInt2);
                }
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            this.ks.a(1, byteArray, 0, byteArray.length);
            dataOutputStream.close();
            byteArrayOutputStream.close();
            try {
                this.ks.bn();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            try {
                this.ks.bn();
            } catch (Exception e3) {
            }
        } catch (Throwable th) {
            try {
                this.ks.bn();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public final void b(boolean z) {
        if (z) {
            if (this.ih == 3) {
                this.cx = 2;
            } else {
                this.cx = 13;
            }
            int i = this.ih;
            switch (i) {
                case 2:
                    this.ib[7] = 1;
                    this.ib[22] = 1;
                    for (int i2 = 0; i2 < 9; i2++) {
                        this.ib[i2 + 0 + 49] = 1;
                        this.ib[i2 + 9 + 49] = 1;
                    }
                    this.ib[0] = 1;
                    this.ib[15] = 1;
                    this.ib[8] = 0;
                    this.ib[23] = 0;
                    break;
                case 3:
                    this.ib[37] = 0;
                    this.ib[this.dD + 49] = 1;
                    this.ib[this.dD + 58] = 1;
                    break;
                case 4:
                    this.ib[39] = 0;
                    this.ib[1] = 1;
                    this.ib[16] = 1;
                    break;
                case 5:
                    this.ib[43] = 0;
                    int[] iArr = this.ga[0];
                    iArr[1] = iArr[1] + 8000;
                    int[] iArr2 = this.ga[1];
                    iArr2[1] = iArr2[1] + 8000;
                    this.gp += 8000;
                    break;
                case 6:
                    this.ib[44] = 0;
                    this.dp.kF = this.dp.kG;
                    this.dp.eH = this.dp.hz;
                    int[] iArr3 = this.ga[0];
                    iArr3[1] = iArr3[1] + 8000;
                    int[] iArr4 = this.ga[1];
                    iArr4[1] = iArr4[1] + 8000;
                    this.gp += 8000;
                    this.gi = 0;
                    this.hk = true;
                    if (this.dq != null) {
                        for (d dVar : this.dq) {
                            dVar.lp = true;
                        }
                    }
                    this.hm = 0;
                    this.dp.dd = this.hl[this.dp.kH][0];
                    this.hX = 0;
                    this.hY = true;
                    break;
                case 7:
                case 8:
                case 9:
                    if (i == 7) {
                        this.ib[40] = 0;
                    }
                    if (i == 8) {
                        this.ib[41] = 0;
                    }
                    if (i == 9) {
                        this.ib[42] = 0;
                    }
                    byte[] bArr = this.ib;
                    int i3 = (i - 7) + 4 + 0;
                    bArr[i3] = (byte) (bArr[i3] + d.bo);
                    byte[] bArr2 = this.ib;
                    int i4 = (i - 7) + 4 + 15;
                    bArr2[i4] = (byte) (bArr2[i4] + d.bo);
                    break;
                case 11:
                    this.ib[38] = 0;
                    if (this.ga[this.dC][12] >= 20) {
                        this.ga[0][12] = 20;
                        this.ga[1][12] = 20;
                        for (int i5 = 0; i5 < 5; i5++) {
                            this.ib[i5 + 67 + 0] = 1;
                            this.ib[i5 + 67 + 4] = 1;
                        }
                        this.ga[0][5] = 5;
                        this.ga[1][5] = 5;
                        break;
                    } else {
                        int[] iArr5 = this.ga[0];
                        iArr5[12] = iArr5[12] + 10;
                        int[] iArr6 = this.ga[1];
                        iArr6[12] = iArr6[12] + 10;
                        break;
                    }
            }
            c(this.ib);
            aj();
            return;
        }
        this.cx = 14;
    }

    /* JADX WARN: Type inference failed for: r11v0, types: [c] */
    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: ?
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final void c() {
        /*
            r11 = this;
            r10 = 3
            r9 = 5
            r5 = 2
            r8 = 1
            r7 = 0
            boolean r0 = r11.bv
            if (r0 == 0) goto L_0x0011
            boolean r0 = r11.bx
            if (r0 == 0) goto L_0x0011
            int r0 = r11.eZ
            if (r0 <= 0) goto L_0x0015
        L_0x0011:
            r11.Q()
            return
        L_0x0015:
            int r0 = r11.ci
            r1 = 24
            if (r0 == r1) goto L_0x002e
            int r0 = r11.ci
            r1 = 12
            if (r0 != r1) goto L_0x0043
            int r0 = r11.gC
            if (r0 != 0) goto L_0x0043
            int r0 = r11.bs
            r1 = 4
            if (r0 != r1) goto L_0x0043
            int r0 = r11.hp
            if (r0 >= 0) goto L_0x0043
        L_0x002e:
            r0 = r8
        L_0x002f:
            r11.bx = r0
            int r0 = r11.hp
            if (r0 < 0) goto L_0x0095
            boolean r0 = r11.bx
            if (r0 == 0) goto L_0x0045
            int r0 = r11.ci
            r1 = 24
            if (r0 == r1) goto L_0x0045
            r0 = -1
            r11.bs = r0
            goto L_0x0011
        L_0x0043:
            r0 = r7
            goto L_0x002f
        L_0x0045:
            int r0 = r11.bs
            r1 = 4
            if (r0 != r1) goto L_0x0011
            r0 = -1
            r11.bs = r0
            int r0 = r11.hr
            if (r0 < r9) goto L_0x0011
            int r0 = r11.ci
            r1 = 8
            if (r0 != r1) goto L_0x007c
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 != r9) goto L_0x007c
            int r0 = r11.hp
            d r1 = r11.dp
            int r1 = r1.kH
            int r1 = r1 + 68
            if (r0 != r1) goto L_0x007c
            boolean r0 = r11.cD
            if (r0 != 0) goto L_0x007c
            r0 = 29
            r11.d(r0, r5)
            r0 = 8
            r11.by = r0
            r11.cC = r8
        L_0x007c:
            r11.hr = r7
            r11.hi = r7
            r0 = -1
            r11.hp = r0
            boolean r0 = r11.gG
            if (r0 == 0) goto L_0x0011
            int r0 = r11.ht
            r1 = -1
            if (r0 == r1) goto L_0x0011
            byte[] r0 = r11.hs
            int r1 = r11.ht
            r2 = -1
            r0[r1] = r2
            goto L_0x0011
        L_0x0095:
            int r0 = r11.ci
            switch(r0) {
                case 0: goto L_0x0011;
                case 1: goto L_0x0011;
                case 2: goto L_0x009a;
                case 3: goto L_0x009c;
                case 4: goto L_0x0cf6;
                case 5: goto L_0x0363;
                case 6: goto L_0x009a;
                case 7: goto L_0x0f43;
                case 8: goto L_0x00b7;
                case 9: goto L_0x017e;
                case 10: goto L_0x06dc;
                case 11: goto L_0x0774;
                case 12: goto L_0x07d9;
                case 13: goto L_0x0ab9;
                case 14: goto L_0x0ef1;
                case 15: goto L_0x06c1;
                case 16: goto L_0x009a;
                case 17: goto L_0x06b3;
                case 18: goto L_0x0185;
                case 19: goto L_0x0de8;
                case 20: goto L_0x009a;
                case 21: goto L_0x0e79;
                case 22: goto L_0x0e09;
                case 23: goto L_0x0edb;
                case 24: goto L_0x0ef8;
                case 25: goto L_0x0f3c;
                case 26: goto L_0x035a;
                case 27: goto L_0x0f5f;
                case 28: goto L_0x0f66;
                case 29: goto L_0x0011;
                case 30: goto L_0x0f6d;
                case 31: goto L_0x0f74;
                case 32: goto L_0x0f7b;
                case 33: goto L_0x0f83;
                default: goto L_0x009a;
            }
        L_0x009a:
            goto L_0x0011
        L_0x009c:
            int r0 = r11.bs
            int r1 = r11.eP
            if (r1 > 0) goto L_0x0011
            int r1 = r11.eR
            if (r1 > 0) goto L_0x0011
            boolean r1 = r11.iG
            if (r1 != r8) goto L_0x0be1
            if (r0 == r5) goto L_0x00ae
            if (r0 != r10) goto L_0x0bc1
        L_0x00ae:
            int r0 = r11.iH
            if (r0 != r8) goto L_0x0bbe
            r0 = r7
        L_0x00b3:
            r11.iH = r0
            goto L_0x0011
        L_0x00b7:
            boolean r0 = r11.y
            if (r0 != 0) goto L_0x00f5
            int[] r0 = r11.aK
            r0 = r0[r7]
            if (r0 < r9) goto L_0x00f5
            int r0 = r11.bs
            if (r0 == r5) goto L_0x00c9
            int r0 = r11.bs
            if (r0 != r10) goto L_0x00d4
        L_0x00c9:
            int r0 = r11.aV
            if (r0 != 0) goto L_0x00d2
            r0 = r8
        L_0x00ce:
            r11.aV = r0
            goto L_0x0011
        L_0x00d2:
            r0 = r7
            goto L_0x00ce
        L_0x00d4:
            int r0 = r11.bs
            r1 = 4
            if (r0 == r1) goto L_0x00dd
            int r0 = r11.bs
            if (r0 != r9) goto L_0x0011
        L_0x00dd:
            int r0 = r11.aV
            if (r0 != 0) goto L_0x00eb
            r11.gG = r8
            r11.y = r8
            int[] r0 = r11.aK
            r0[r7] = r9
            goto L_0x0011
        L_0x00eb:
            r11.gG = r7
            r11.y = r8
            int[] r0 = r11.aK
            r0[r7] = r9
            goto L_0x0011
        L_0x00f5:
            boolean r0 = r11.gS
            if (r0 == 0) goto L_0x0177
            int r0 = r11.gU
            if (r0 != r8) goto L_0x0177
            int r0 = r11.bs
            r1 = 14
            if (r0 != r1) goto L_0x0011
            int r0 = r11.gV
            int r0 = r0 + 1
            r11.gV = r0
            int r0 = r11.gV
            short[][][] r1 = defpackage.g.lM
            int r2 = r11.gP
            r1 = r1[r2]
            int r1 = r1.length
            int r1 = r1 - r8
            if (r0 <= r1) goto L_0x0011
            r11.gU = r5
            r11.gT = r7
            int r0 = r11.dO
            if (r0 != r8) goto L_0x015e
            int r0 = r11.dY
            if (r0 != r10) goto L_0x015e
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            r1 = 8
            if (r0 != r1) goto L_0x016e
            int r0 = r11.gP
            r1 = 42
            if (r0 != r1) goto L_0x015e
            r0 = r7
        L_0x0138:
            d[] r1 = r11.dq
            int r1 = r1.length
            if (r0 >= r1) goto L_0x0159
            d[] r1 = r11.dq
            r1 = r1[r0]
            int r1 = r1.dd
            r2 = 8
            if (r1 != r2) goto L_0x0156
            d[] r1 = r11.dq
            r1 = r1[r0]
            int r1 = r1.kF
            if (r1 < 0) goto L_0x0156
            d[] r1 = r11.dq
            r1 = r1[r0]
            r2 = -1
            r1.kF = r2
        L_0x0156:
            int r0 = r0 + 1
            goto L_0x0138
        L_0x0159:
            r0 = 27
            r11.d(r0, r5)
        L_0x015e:
            int r0 = r11.hu
            if (r0 != r8) goto L_0x0011
            boolean r0 = r11.dW
            if (r0 != 0) goto L_0x0011
            int r0 = r11.dO
            if (r0 == 0) goto L_0x0011
            r11.gS = r7
            goto L_0x0011
        L_0x016e:
            int r0 = r11.gP
            r1 = 42
            if (r0 != r1) goto L_0x015e
            r11.gY = r8
            goto L_0x015e
        L_0x0177:
            int r0 = r11.bs
            r11.g(r0)
            goto L_0x0011
        L_0x017e:
            int r0 = r11.bs
            r11.g(r0)
            goto L_0x0011
        L_0x0185:
            int r0 = r11.bs
            r1 = 4
            if (r0 != r1) goto L_0x0304
            boolean r0 = r11.fZ
            if (r0 != 0) goto L_0x0304
            int r0 = r11.gv
            if (r0 != 0) goto L_0x032d
            int[] r0 = r11.ho
            r1 = 8
            r0 = r0[r1]
            if (r0 <= 0) goto L_0x01b7
            r0 = r7
        L_0x019b:
            r1 = 4
            if (r0 >= r1) goto L_0x01ab
            int[] r1 = r11.ho
            int r2 = r0 + 4
            int[] r3 = r11.ho
            r3 = r3[r0]
            r1[r2] = r3
            int r0 = r0 + 1
            goto L_0x019b
        L_0x01ab:
            int[] r0 = r11.ho
            r1 = 8
            int r2 = r11.q(r7)
            r0[r1] = r2
            goto L_0x0011
        L_0x01b7:
            int[] r0 = r11.ho
            r1 = 8
            r0 = r0[r1]
            r1 = -1
            if (r0 != r1) goto L_0x01dd
            int[] r0 = r11.ho
            r1 = 8
            r2 = -1
            int r2 = r11.q(r2)
            r0[r1] = r2
            r0 = 7
            r11.hp = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0[r9] = r9
            r11.aj()
            goto L_0x0011
        L_0x01dd:
            int[] r0 = r11.ho
            r1 = 8
            r0 = r0[r1]
            r1 = -2
            if (r0 != r1) goto L_0x0210
            int[] r0 = r11.ho
            r1 = 8
            r2 = -2
            int r2 = r11.q(r2)
            r0[r1] = r2
            r0 = 20
            r11.hp = r0
            int r0 = r11.dO
            int r0 = r0 + 1
            r11.hq = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 11
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
            r11.aj()
            goto L_0x0011
        L_0x0210:
            int[] r0 = r11.ho
            r1 = 8
            r0 = r0[r1]
            r1 = -3
            if (r0 != r1) goto L_0x0226
            int[] r0 = r11.ho
            r1 = 8
            r2 = -4
            r0[r1] = r2
            r0 = 22
            r11.hp = r0
            goto L_0x0011
        L_0x0226:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 23
            r0 = r0[r1]
            if (r0 != 0) goto L_0x024f
            int r0 = r11.dO
            r1 = 8
            if (r0 != r1) goto L_0x024f
            r0 = 60
            r11.hp = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 23
            r0[r1] = r8
            r11.aj()
            goto L_0x0011
        L_0x024f:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 25
            r0 = r0[r1]
            if (r0 != 0) goto L_0x0275
            int r0 = r11.dO
            r1 = 8
            if (r0 != r1) goto L_0x0275
            r0 = 63
            r11.hp = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 25
            r0[r1] = r8
            goto L_0x0011
        L_0x0275:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 26
            r0 = r0[r1]
            if (r0 != 0) goto L_0x029c
            int r0 = r11.dO
            r1 = 8
            if (r0 != r1) goto L_0x029c
            r11.fZ = r8
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 26
            r0[r1] = r8
            r11.aj()
            goto L_0x0011
        L_0x029c:
            int[] r0 = r11.fS
            int r1 = r11.dO
            r0 = r0[r1]
            int r1 = r11.gw
            if (r0 >= r1) goto L_0x02b3
            boolean r0 = r11.kh
            if (r0 != 0) goto L_0x02b3
            r11.eT = r7
            r11.d(r9, r7)
            r11.cJ = r9
            r11.bt = r8
        L_0x02b3:
            int r0 = r11.iQ
            r1 = r7
        L_0x02b6:
            if (r1 >= r10) goto L_0x02c3
            int[] r2 = r11.fS
            int r3 = r1 + 9
            r2 = r2[r3]
            if (r2 < r0) goto L_0x02c3
            int r1 = r1 + 1
            goto L_0x02b6
        L_0x02c3:
            if (r1 == r10) goto L_0x02df
            r2 = r5
        L_0x02c6:
            if (r2 <= r1) goto L_0x02d9
            int[] r3 = r11.fS
            int r4 = r2 + 9
            int[] r5 = r11.fS
            int r6 = r2 - r8
            int r6 = r6 + 9
            r5 = r5[r6]
            r3[r4] = r5
            int r2 = r2 + -1
            goto L_0x02c6
        L_0x02d9:
            int[] r2 = r11.fS
            int r1 = r1 + 9
            r2[r1] = r0
        L_0x02df:
            r11.aj()
            boolean r0 = r11.bt
            if (r0 != 0) goto L_0x0304
            int r0 = r11.dO
            r1 = 8
            if (r0 != r1) goto L_0x0312
            byte[] r0 = r11.ib
            r1 = 48
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x0308
            r0 = 15
            r11.d(r0, r7)
            byte[] r0 = r11.ib
            r1 = 48
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
        L_0x0304:
            r11.ii = r7
            goto L_0x0011
        L_0x0308:
            r11.d(r9, r7)
            r11.cJ = r7
            r11.db = r7
            r11.eW = r7
            goto L_0x0304
        L_0x0312:
            f r0 = r11.aI
            r1 = 12
            r0.k(r1)
            r11.d(r10, r7)
            int r0 = r11.dD
            int r0 = r0 + 1
            r11.dD = r0
            int r0 = r11.dD
            int r1 = r11.dE
            if (r0 <= r1) goto L_0x0304
            int r0 = r11.dE
            r11.dD = r0
            goto L_0x0304
        L_0x032d:
            int[] r0 = r11.ho
            r0 = r0[r10]
            if (r0 <= 0) goto L_0x033a
            int[] r0 = r11.ho
            r1 = -4
            r0[r10] = r1
            goto L_0x0011
        L_0x033a:
            int[] r0 = r11.ho
            r0 = r0[r10]
            r1 = -4
            if (r0 != r1) goto L_0x0353
            int r0 = r11.gw
            if (r0 <= 0) goto L_0x0353
            int[] r0 = r11.ho
            r1 = -5
            r0[r10] = r1
            int r0 = r11.gw
            int r0 = r0 + 42
            int r0 = r0 - r8
            r11.hp = r0
            goto L_0x0011
        L_0x0353:
            r11.O()
            r11.aj()
            goto L_0x0304
        L_0x035a:
            int r0 = r11.bs
            r1 = 4
            if (r0 != r1) goto L_0x0011
            r11.bz = r8
            goto L_0x0011
        L_0x0363:
            int r0 = r11.bs
            int r1 = r11.cJ
            switch(r1) {
                case 0: goto L_0x036c;
                case 1: goto L_0x0540;
                case 2: goto L_0x0579;
                case 3: goto L_0x05b6;
                case 4: goto L_0x063f;
                case 5: goto L_0x064d;
                default: goto L_0x036a;
            }
        L_0x036a:
            goto L_0x0011
        L_0x036c:
            int r1 = r11.da
            if (r1 != 0) goto L_0x0011
            if (r0 == r5) goto L_0x0374
            if (r0 != 0) goto L_0x0387
        L_0x0374:
            r11.da = r5
            int r0 = r11.db
            if (r0 != 0) goto L_0x0380
            r0 = 8
            r11.db = r0
            goto L_0x0011
        L_0x0380:
            int r0 = r11.db
            int r0 = r0 - r8
            r11.db = r0
            goto L_0x0011
        L_0x0387:
            if (r0 == r10) goto L_0x038b
            if (r0 != r8) goto L_0x039f
        L_0x038b:
            r11.da = r8
            int r0 = r11.db
            r1 = 8
            if (r0 != r1) goto L_0x0397
            r11.db = r7
            goto L_0x0011
        L_0x0397:
            int r0 = r11.db
            int r0 = r0 + 1
            r11.db = r0
            goto L_0x0011
        L_0x039f:
            r1 = 16
            if (r0 != r1) goto L_0x03a9
            boolean[] r0 = r11.iK
            r0[r7] = r8
            goto L_0x0011
        L_0x03a9:
            r1 = 12
            if (r0 != r1) goto L_0x03c9
            int[] r1 = r11.iM
            r1 = r1[r7]
            if (r1 < r5) goto L_0x03c9
            int[] r1 = r11.iM
            r1 = r1[r7]
            r2 = 30
            if (r1 > r2) goto L_0x03c9
            boolean[] r0 = r11.iK
            r0[r7] = r7
            int[] r0 = r11.iM
            r0[r7] = r7
            boolean[] r0 = r11.iK
            r0[r8] = r8
            goto L_0x0011
        L_0x03c9:
            r1 = 17
            if (r0 != r1) goto L_0x03e9
            int[] r1 = r11.iM
            r1 = r1[r8]
            if (r1 < r5) goto L_0x03e9
            int[] r1 = r11.iM
            r1 = r1[r8]
            r2 = 30
            if (r1 > r2) goto L_0x03e9
            boolean[] r0 = r11.iK
            r0[r8] = r7
            int[] r0 = r11.iM
            r0[r8] = r7
            boolean[] r0 = r11.iK
            r0[r5] = r8
            goto L_0x0011
        L_0x03e9:
            r1 = 13
            if (r0 != r1) goto L_0x0409
            int[] r1 = r11.iM
            r1 = r1[r5]
            if (r1 < r5) goto L_0x0409
            int[] r1 = r11.iM
            r1 = r1[r5]
            r2 = 30
            if (r1 > r2) goto L_0x0409
            boolean[] r0 = r11.iK
            r0[r5] = r7
            int[] r0 = r11.iM
            r0[r5] = r7
            boolean[] r0 = r11.iK
            r0[r10] = r8
            goto L_0x0011
        L_0x0409:
            r1 = 18
            if (r0 != r1) goto L_0x042a
            int[] r1 = r11.iM
            r1 = r1[r10]
            if (r1 < r5) goto L_0x042a
            int[] r1 = r11.iM
            r1 = r1[r10]
            r2 = 30
            if (r1 > r2) goto L_0x042a
            boolean[] r0 = r11.iK
            r0[r10] = r7
            int[] r0 = r11.iM
            r0[r10] = r7
            boolean[] r0 = r11.iK
            r1 = 4
            r0[r1] = r8
            goto L_0x0011
        L_0x042a:
            r1 = 19
            if (r0 != r1) goto L_0x0454
            int[] r1 = r11.iM
            r2 = 4
            r1 = r1[r2]
            if (r1 < r5) goto L_0x0454
            int[] r1 = r11.iM
            r2 = 4
            r1 = r1[r2]
            r2 = 30
            if (r1 > r2) goto L_0x0454
            r0 = r7
        L_0x043f:
            if (r0 >= r9) goto L_0x044c
            boolean[] r1 = r11.iK
            r1[r0] = r7
            int[] r1 = r11.iM
            r1[r0] = r7
            int r0 = r0 + 1
            goto L_0x043f
        L_0x044c:
            r11.iL = r8
            r0 = 32
            r11.ci = r0
            goto L_0x0011
        L_0x0454:
            r1 = 6
            if (r0 != r1) goto L_0x0473
            f r0 = r11.aI
            int[] r1 = r11.fx
            r1 = r1[r7]
            r0.k(r1)
            r0 = 26
            r11.d(r0, r7)
            r0 = r7
        L_0x0466:
            if (r0 >= r9) goto L_0x0011
            boolean[] r1 = r11.iK
            r1[r0] = r7
            int[] r1 = r11.iM
            r1[r0] = r7
            int r0 = r0 + 1
            goto L_0x0466
        L_0x0473:
            if (r0 == r9) goto L_0x0478
            r1 = 4
            if (r0 != r1) goto L_0x0011
        L_0x0478:
            r0 = r7
        L_0x0479:
            if (r0 >= r9) goto L_0x0486
            boolean[] r1 = r11.iK
            r1[r0] = r7
            int[] r1 = r11.iM
            r1[r0] = r7
            int r0 = r0 + 1
            goto L_0x0479
        L_0x0486:
            r11.eP = r7
            int r0 = r11.db
            if (r0 != 0) goto L_0x04a8
            r11.iE = r7
            r11.dC = r7
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            r11.dD = r0
            f r0 = r11.aI
            r1 = 12
            r0.k(r1)
            r11.d(r10, r5)
            goto L_0x0011
        L_0x04a8:
            int r0 = r11.db
            if (r0 != r8) goto L_0x04d4
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 15
            int r1 = r1 + 7
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x04cc
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 8
            int r1 = r1 + 8
            int r1 = r1 + 49
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x04cc
            r0 = 53
            r11.hp = r0
            goto L_0x0011
        L_0x04cc:
            r11.dG = r7
            r0 = 4
            r11.d(r0, r5)
            goto L_0x0011
        L_0x04d4:
            int r0 = r11.db
            r1 = 4
            if (r0 != r1) goto L_0x04e0
            r11.d(r8, r10)
            r11.dd = r7
            goto L_0x0011
        L_0x04e0:
            int r0 = r11.db
            if (r0 != r9) goto L_0x04ed
            r11.d(r5, r10)
            r11.dc = r7
            r11.de = r7
            goto L_0x0011
        L_0x04ed:
            int r0 = r11.db
            if (r0 != r5) goto L_0x04f8
            r11.d(r10, r10)
            r11.dc = r7
            goto L_0x0011
        L_0x04f8:
            int r0 = r11.db
            r1 = 6
            if (r0 != r1) goto L_0x0503
            r0 = 4
            r11.d(r0, r10)
            goto L_0x0011
        L_0x0503:
            int r0 = r11.db
            if (r0 != r10) goto L_0x050e
            r11.gJ = r7
            r11.d(r9, r10)
            goto L_0x0011
        L_0x050e:
            int r0 = r11.db
            r1 = 7
            if (r0 != r1) goto L_0x051a
            f r0 = r11.aI
            r0.ak()
            goto L_0x0011
        L_0x051a:
            int r0 = r11.db
            r1 = 8
            if (r0 != r1) goto L_0x0011
            r0 = 21
            r11.d(r0, r5)
            r11.fR = r7
            byte[] r0 = r11.ib
            r1 = 34
            r0[r1] = r7
            byte[] r0 = r11.ib
            r1 = 35
            r0[r1] = r7
            byte[] r0 = r11.ib
            r1 = 36
            r0[r1] = r7
            byte[] r0 = r11.ib
            r11.c(r0)
            goto L_0x0011
        L_0x0540:
            if (r0 != r10) goto L_0x0553
            int r0 = r11.aN
            int r0 = r0 + 1
            r11.aN = r0
            int r0 = r11.aN
            if (r0 <= r5) goto L_0x054e
            r11.aN = r7
        L_0x054e:
            r11.M()
            goto L_0x0011
        L_0x0553:
            if (r0 != r5) goto L_0x0565
            int r0 = r11.aN
            int r0 = r0 - r8
            r11.aN = r0
            int r0 = r11.aN
            if (r0 >= 0) goto L_0x0560
            r11.aN = r5
        L_0x0560:
            r11.M()
            goto L_0x0011
        L_0x0565:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            int r0 = r11.dd
            if (r0 != 0) goto L_0x0574
            r11.d(r7, r10)
            r0 = 4
            r11.db = r0
            goto L_0x0011
        L_0x0574:
            r11.N()
            goto L_0x0011
        L_0x0579:
            if (r0 != r5) goto L_0x058c
            int r0 = r11.de
            if (r0 > 0) goto L_0x0585
            int r0 = r11.df
            r11.de = r0
            goto L_0x0011
        L_0x0585:
            int r0 = r11.de
            int r0 = r0 - r8
            r11.de = r0
            goto L_0x0011
        L_0x058c:
            if (r0 != r10) goto L_0x05a0
            int r0 = r11.de
            int r1 = r11.df
            if (r0 < r1) goto L_0x0598
            r11.de = r7
            goto L_0x0011
        L_0x0598:
            int r0 = r11.de
            int r0 = r0 + 1
            r11.de = r0
            goto L_0x0011
        L_0x05a0:
            r1 = 6
            if (r0 == r1) goto L_0x05a6
            r1 = 7
            if (r0 != r1) goto L_0x0011
        L_0x05a6:
            int r0 = r11.dc
            if (r0 != 0) goto L_0x05b1
            r11.d(r7, r10)
            r11.db = r9
            goto L_0x0011
        L_0x05b1:
            r11.N()
            goto L_0x0011
        L_0x05b6:
            boolean r1 = r11.di
            if (r1 != 0) goto L_0x0600
            if (r0 != 0) goto L_0x05cd
            int r0 = r11.dg
            if (r0 > 0) goto L_0x05c6
            int r0 = r11.dj
            r11.dg = r0
            goto L_0x0011
        L_0x05c6:
            int r0 = r11.dg
            int r0 = r0 - r8
            r11.dg = r0
            goto L_0x0011
        L_0x05cd:
            if (r0 != r8) goto L_0x05e1
            int r0 = r11.dg
            int r1 = r11.dj
            if (r0 < r1) goto L_0x05d9
            r11.dg = r7
            goto L_0x0011
        L_0x05d9:
            int r0 = r11.dg
            int r0 = r0 + 1
            r11.dg = r0
            goto L_0x0011
        L_0x05e1:
            r1 = 4
            if (r0 == r1) goto L_0x05e6
            if (r0 != r9) goto L_0x05ea
        L_0x05e6:
            r11.di = r8
            goto L_0x0011
        L_0x05ea:
            r1 = 6
            if (r0 == r1) goto L_0x05f0
            r1 = 7
            if (r0 != r1) goto L_0x0011
        L_0x05f0:
            int r0 = r11.dc
            if (r0 != 0) goto L_0x05fb
            r11.d(r7, r10)
            r11.db = r5
            goto L_0x0011
        L_0x05fb:
            r11.N()
            goto L_0x0011
        L_0x0600:
            if (r0 != r5) goto L_0x0619
            int r0 = r11.dh
            if (r0 > r8) goto L_0x0612
            java.lang.String[][] r0 = defpackage.g.lR
            int r1 = r11.dg
            r0 = r0[r1]
            int r0 = r0.length
            int r0 = r0 - r8
            r11.dh = r0
            goto L_0x0011
        L_0x0612:
            int r0 = r11.dh
            int r0 = r0 - r8
            r11.dh = r0
            goto L_0x0011
        L_0x0619:
            if (r0 != r10) goto L_0x0633
            int r0 = r11.dh
            java.lang.String[][] r1 = defpackage.g.lR
            int r2 = r11.dg
            r1 = r1[r2]
            int r1 = r1.length
            int r1 = r1 - r8
            if (r0 < r1) goto L_0x062b
            r11.dh = r8
            goto L_0x0011
        L_0x062b:
            int r0 = r11.dh
            int r0 = r0 + 1
            r11.dh = r0
            goto L_0x0011
        L_0x0633:
            r1 = 6
            if (r0 == r1) goto L_0x0639
            r1 = 7
            if (r0 != r1) goto L_0x0011
        L_0x0639:
            r11.di = r7
            r11.dh = r8
            goto L_0x0011
        L_0x063f:
            r1 = 6
            if (r0 == r1) goto L_0x0645
            r1 = 7
            if (r0 != r1) goto L_0x0011
        L_0x0645:
            r11.d(r7, r10)
            r0 = 6
            r11.db = r0
            goto L_0x0011
        L_0x064d:
            boolean r1 = r11.bt
            if (r1 != 0) goto L_0x0678
            r1 = 6
            if (r0 == r1) goto L_0x0657
            r1 = 7
            if (r0 != r1) goto L_0x065e
        L_0x0657:
            r11.d(r7, r10)
            r11.db = r10
            goto L_0x0011
        L_0x065e:
            if (r0 != r5) goto L_0x066b
            int r0 = r11.eT
            if (r0 != 0) goto L_0x0669
            r0 = r8
        L_0x0665:
            r11.eT = r0
            goto L_0x0011
        L_0x0669:
            r0 = r7
            goto L_0x0665
        L_0x066b:
            if (r0 != r10) goto L_0x0011
            int r0 = r11.eT
            if (r0 != 0) goto L_0x0676
            r0 = r8
        L_0x0672:
            r11.eT = r0
            goto L_0x0011
        L_0x0676:
            r0 = r7
            goto L_0x0672
        L_0x0678:
            int[] r1 = r11.aK
            r1 = r1[r7]
            r2 = 25
            if (r1 < r2) goto L_0x0011
            r1 = 6
            if (r0 == r1) goto L_0x0689
            r1 = 7
            if (r0 == r1) goto L_0x0689
            r1 = 4
            if (r0 != r1) goto L_0x0011
        L_0x0689:
            int r0 = r11.dO
            r1 = 8
            if (r0 != r1) goto L_0x0698
            r0 = 15
            r11.d(r0, r7)
        L_0x0694:
            r11.bt = r7
            goto L_0x0011
        L_0x0698:
            f r0 = r11.aI
            r1 = 12
            r0.k(r1)
            r11.d(r10, r7)
            int r0 = r11.dD
            int r0 = r0 + 1
            r11.dD = r0
            int r0 = r11.dD
            int r1 = r11.dE
            if (r0 <= r1) goto L_0x0694
            int r0 = r11.dE
            r11.dD = r0
            goto L_0x0694
        L_0x06b3:
            int r0 = r11.bs
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.d(r9, r7)
            r11.cJ = r7
            r11.db = r7
            goto L_0x0011
        L_0x06c1:
            int r0 = r11.bs
            r1 = 14
            if (r0 != r1) goto L_0x0011
            int r0 = r11.eV
            int r1 = r11.eU
            if (r0 >= r1) goto L_0x06d3
            int r0 = r11.eU
            r11.eV = r0
            goto L_0x0011
        L_0x06d3:
            r0 = 16
            r11.d(r0, r7)
            r11.eV = r7
            goto L_0x0011
        L_0x06dc:
            int r0 = r11.bs
            int r1 = r11.db
            r2 = 11
            if (r1 == r2) goto L_0x0700
            int r1 = r11.db
            r2 = 13
            if (r1 == r2) goto L_0x0700
            int r1 = r11.db
            r2 = 14
            if (r1 == r2) goto L_0x0700
            int r1 = r11.db
            r2 = 15
            if (r1 == r2) goto L_0x0700
            int r1 = r11.db
            r2 = 12
            if (r1 == r2) goto L_0x0700
            r1 = 11
            r11.db = r1
        L_0x0700:
            switch(r0) {
                case 0: goto L_0x0705;
                case 1: goto L_0x0715;
                case 2: goto L_0x0705;
                case 3: goto L_0x0715;
                case 4: goto L_0x0726;
                case 5: goto L_0x0726;
                default: goto L_0x0703;
            }
        L_0x0703:
            goto L_0x0011
        L_0x0705:
            int r0 = r11.db
            r1 = 11
            if (r0 != r1) goto L_0x0711
            r0 = 15
        L_0x070d:
            r11.db = r0
            goto L_0x0011
        L_0x0711:
            int r0 = r11.db
            int r0 = r0 - r8
            goto L_0x070d
        L_0x0715:
            int r0 = r11.db
            r1 = 15
            if (r0 != r1) goto L_0x0721
            r0 = 11
        L_0x071d:
            r11.db = r0
            goto L_0x0011
        L_0x0721:
            int r0 = r11.db
            int r0 = r0 + 1
            goto L_0x071d
        L_0x0726:
            int r0 = r11.db
            r1 = 11
            if (r0 != r1) goto L_0x0733
            int r0 = r11.dB
            r11.d(r0, r5)
            goto L_0x0011
        L_0x0733:
            int r0 = r11.db
            r1 = 13
            if (r0 != r1) goto L_0x0742
            r11.d(r9, r5)
            r11.cJ = r8
            r11.dd = r8
            goto L_0x0011
        L_0x0742:
            int r0 = r11.db
            r1 = 14
            if (r0 != r1) goto L_0x0753
            r11.d(r9, r5)
            r11.cJ = r5
            r11.dc = r8
            r11.de = r7
            goto L_0x0011
        L_0x0753:
            int r0 = r11.db
            r1 = 12
            if (r0 != r1) goto L_0x0766
            r11.d(r9, r5)
            r11.cJ = r10
            r11.dg = r7
            r11.dh = r8
            r11.dc = r8
            goto L_0x0011
        L_0x0766:
            int r0 = r11.db
            r1 = 15
            if (r0 != r1) goto L_0x0011
            r11.aj()
            r11.O()
            goto L_0x0011
        L_0x0774:
            int r0 = r11.bs
            if (r0 == r5) goto L_0x077a
            if (r0 != 0) goto L_0x0785
        L_0x077a:
            int r1 = r11.cY
            int r1 = r1 - r8
            r11.cY = r1
            int r1 = r11.cY
            if (r1 >= 0) goto L_0x0785
            r11.cY = r5
        L_0x0785:
            if (r0 == r10) goto L_0x0789
            if (r0 != r8) goto L_0x0795
        L_0x0789:
            int r1 = r11.cY
            int r1 = r1 + 1
            r11.cY = r1
            int r1 = r11.cY
            if (r1 <= r5) goto L_0x0795
            r11.cY = r7
        L_0x0795:
            r1 = 4
            if (r0 == r1) goto L_0x079a
            if (r0 != r9) goto L_0x07cf
        L_0x079a:
            int r0 = r11.cY
            if (r0 != 0) goto L_0x07b1
            r0 = 12
            r11.d(r0, r5)
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            r11.gb = r0
            goto L_0x0011
        L_0x07b1:
            int r0 = r11.cY
            if (r0 != r8) goto L_0x07bc
            r0 = 13
            r11.d(r0, r5)
            goto L_0x0011
        L_0x07bc:
            int r0 = r11.cY
            if (r0 != r5) goto L_0x0011
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.d(r0, r5)
            r0 = 11
            r11.cx = r0
            goto L_0x0011
        L_0x07cf:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r0 = 8
            r11.d(r0, r5)
            goto L_0x0011
        L_0x07d9:
            int r0 = r11.bs
            switch(r0) {
                case 0: goto L_0x07e0;
                case 1: goto L_0x0a73;
                case 2: goto L_0x0a54;
                case 3: goto L_0x0a63;
                case 4: goto L_0x07ef;
                case 5: goto L_0x07ef;
                case 6: goto L_0x0a83;
                case 7: goto L_0x07de;
                case 8: goto L_0x07de;
                case 9: goto L_0x07de;
                case 10: goto L_0x07de;
                case 11: goto L_0x07de;
                case 12: goto L_0x0011;
                default: goto L_0x07de;
            }
        L_0x07de:
            goto L_0x0011
        L_0x07e0:
            int r0 = r11.gE
            if (r0 == 0) goto L_0x0011
            int r0 = r11.gE
            if (r0 == r8) goto L_0x0011
            int r0 = r11.gE
            int r0 = r0 - r5
            r11.gE = r0
            goto L_0x0011
        L_0x07ef:
            d r0 = r11.dp
            int r0 = r0.kG
            r11.iA = r0
            d r0 = r11.dp
            int r0 = r0.hP
            r11.iB = r0
            d r0 = r11.dp
            int r0 = r0.ij
            r11.iC = r0
            int r0 = r11.gE
            if (r0 != r10) goto L_0x088f
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 12
            r0 = r0[r1]
            r1 = 20
            if (r0 >= r1) goto L_0x0821
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 <= r10) goto L_0x088f
        L_0x0821:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            r1 = 4
            if (r0 > r1) goto L_0x083e
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = 12
            r0 = r0[r1]
            r1 = 20
            if (r0 < r1) goto L_0x0882
        L_0x083e:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0[r9] = r9
            r11.aj()
            byte[] r0 = r11.ib
            r1 = 47
            r0[r1] = r8
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 5
            int r1 = r1 + 71
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
            r0 = 7
            r11.hp = r0
        L_0x0863:
            int r0 = r11.hp
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 != r5) goto L_0x09fc
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 5
            int r1 = r1 + 68
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
            goto L_0x0011
        L_0x0882:
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.ci = r0
            r0 = 11
            r11.cx = r0
            goto L_0x0863
        L_0x088f:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 2
            r0 = r0[r1]
            short[][] r1 = defpackage.g.lP
            int r2 = r11.gE
            r1 = r1[r2]
            int r1 = r1.length
            int r1 = r1 - r8
            if (r0 < r1) goto L_0x08b8
            int r0 = r11.gE
            int r0 = r0 + 27
            r11.hp = r0
            short[][] r0 = defpackage.g.lP
            int r1 = r11.gE
            r0 = r0[r1]
            int r0 = r0.length
            int r0 = r0 - r8
            r11.hq = r0
            goto L_0x0863
        L_0x08b8:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            if (r0 <= 0) goto L_0x09e4
            int r0 = r11.gE
            if (r0 != r10) goto L_0x08ed
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 < r5) goto L_0x08ed
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 15
            int r1 = r1 + 1
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x08ed
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.ci = r0
            r0 = 4
            r11.cx = r0
            goto L_0x0011
        L_0x08ed:
            int r0 = r11.gE
            if (r0 == r10) goto L_0x091e
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 2
            r0 = r0[r1]
            byte[] r1 = r11.ib
            int r2 = r11.gE
            int r2 = r2 + 4
            int r3 = r11.dC
            int r3 = r3 * 15
            int r2 = r2 + r3
            byte r1 = r1[r2]
            if (r0 != r1) goto L_0x091e
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.ci = r0
            int r0 = r11.gE
            int r0 = r0 + 7
            r11.cx = r0
            goto L_0x0011
        L_0x091e:
            int r0 = r11.iy
            int r0 = r0 + 1
            r11.iy = r0
            int r0 = r11.iy
            r1 = 100
            if (r0 <= r1) goto L_0x092e
            r0 = 100
            r11.iy = r0
        L_0x092e:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            int r1 = r11.iy
            int r0 = r0 - r1
            if (r0 > 0) goto L_0x0943
            int r0 = r11.iy
            if (r0 == r8) goto L_0x0943
            r11.iy = r7
        L_0x0943:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 17
            r2 = r0[r1]
            int r3 = r11.iy
            int r2 = r2 + r3
            r0[r1] = r2
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = r0[r8]
            int r2 = r11.iy
            int r1 = r1 - r2
            r0[r8] = r1
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 17
            r0 = r0[r1]
            short[][] r1 = defpackage.g.lP
            int r2 = r11.gE
            r1 = r1[r2]
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            int r3 = r11.gE
            int r3 = r3 + 2
            r2 = r2[r3]
            short r1 = r1[r2]
            int r0 = r0 - r1
            if (r0 < 0) goto L_0x0863
            int[][] r1 = r11.ga
            d r2 = r11.dp
            int r2 = r2.kH
            r1 = r1[r2]
            r2 = r1[r8]
            int r0 = r0 + r2
            r1[r8] = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 17
            r0[r1] = r7
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 2
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
            int r0 = r11.gE
            int r0 = r0 + 23
            r11.hp = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.gE
            int r1 = r1 + 2
            r0 = r0[r1]
            r11.hq = r0
            d r0 = r11.dp
            int r0 = r0.kH
            r11.o(r0)
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            if (r0 > 0) goto L_0x0863
            goto L_0x0863
        L_0x09e4:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            if (r0 > 0) goto L_0x0863
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.ci = r0
            r11.cx = r9
            goto L_0x0863
        L_0x09fc:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 != r10) goto L_0x0a19
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 5
            int r1 = r1 + 69
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
            goto L_0x0011
        L_0x0a19:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            r1 = 4
            if (r0 != r1) goto L_0x0a37
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 5
            int r1 = r1 + 70
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
            goto L_0x0011
        L_0x0a37:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            if (r0 != r9) goto L_0x0011
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 5
            int r1 = r1 + 71
            r0[r1] = r8
            byte[] r0 = r11.ib
            r11.c(r0)
            goto L_0x0011
        L_0x0a54:
            int r0 = r11.gE
            if (r0 == 0) goto L_0x0011
            int r0 = r11.gE
            if (r0 == r5) goto L_0x0011
            int r0 = r11.gE
            int r0 = r0 - r8
            r11.gE = r0
            goto L_0x0011
        L_0x0a63:
            int r0 = r11.gE
            if (r0 == r8) goto L_0x0011
            int r0 = r11.gE
            if (r0 == r10) goto L_0x0011
            int r0 = r11.gE
            int r0 = r0 + 1
            r11.gE = r0
            goto L_0x0011
        L_0x0a73:
            int r0 = r11.gE
            if (r0 == r5) goto L_0x0011
            int r0 = r11.gE
            if (r0 == r10) goto L_0x0011
            int r0 = r11.gE
            int r0 = r0 + 2
            r11.gE = r0
            goto L_0x0011
        L_0x0a83:
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r9]
            int r1 = r11.gb
            if (r0 == r1) goto L_0x0aac
            d r0 = r11.dp
            int r0 = r0.kH
            r11.o(r0)
            r0 = 11
            r11.by = r0
            r0 = 29
            r11.d(r0, r5)
        L_0x0aa1:
            boolean r0 = r11.iI
            if (r0 == 0) goto L_0x0011
            r11.x()
            r11.iI = r7
            goto L_0x0011
        L_0x0aac:
            d r0 = r11.dp
            int r0 = r0.kH
            r11.o(r0)
            r0 = 11
            r11.d(r0, r5)
            goto L_0x0aa1
        L_0x0ab9:
            int r0 = r11.bs
            if (r0 != 0) goto L_0x0acd
            int r0 = r11.gE
            int r0 = r0 - r8
            r11.gE = r0
            int r0 = r11.gE
            if (r0 >= 0) goto L_0x0011
            int r0 = r11.gF
            int r0 = r0 - r8
            r11.gE = r0
            goto L_0x0011
        L_0x0acd:
            if (r0 != r8) goto L_0x0adf
            int r0 = r11.gE
            int r0 = r0 + 1
            r11.gE = r0
            int r0 = r11.gE
            int r1 = r11.gF
            if (r0 < r1) goto L_0x0011
            r11.gE = r7
            goto L_0x0011
        L_0x0adf:
            r1 = 4
            if (r0 == r1) goto L_0x0ae4
            if (r0 != r9) goto L_0x0bab
        L_0x0ae4:
            r0 = 6
            byte[] r0 = new byte[r0]
            r0[r7] = r7
            r0[r8] = r8
            d r1 = r11.dp
            int r1 = r1.kH
            int r1 = r1 + 50
            byte r1 = (byte) r1
            r0[r5] = r1
            r1 = 9
            r0[r10] = r1
            r1 = 4
            r2 = 10
            r0[r1] = r2
            r1 = 8
            r0[r9] = r1
            r1 = 6
            int[] r1 = new int[r1]
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            r3 = 9
            r2 = r2[r3]
            r1[r7] = r2
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            r3 = 10
            r2 = r2[r3]
            r1[r8] = r2
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            r3 = 12
            r2 = r2[r3]
            r1[r5] = r2
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            r3 = 13
            r2 = r2[r3]
            r1[r10] = r2
            r2 = 4
            int[][] r3 = r11.ga
            d r4 = r11.dp
            int r4 = r4.kH
            r3 = r3[r4]
            r4 = 14
            r3 = r3[r4]
            r1[r2] = r3
            int[][] r2 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r2 = r2[r3]
            r3 = 15
            r2 = r2[r3]
            r1[r9] = r2
            r2 = 6
            int[] r2 = new int[r2]
            r3 = r1[r7]
            int r3 = r3 * 5
            r2[r7] = r3
            r3 = r1[r8]
            r2[r8] = r3
            r2[r5] = r7
            r3 = r1[r10]
            int r3 = r3 * 6
            r2[r10] = r3
            r3 = 4
            r4 = 4
            r4 = r1[r4]
            r2[r3] = r4
            r3 = r1[r9]
            int r3 = r3 * 80
            r2[r9] = r3
            int r3 = r11.gE
            byte r0 = r0[r3]
            r11.hp = r0
            int r0 = r11.gE
            if (r0 != r5) goto L_0x0b9c
            r0 = r1[r5]
            r3 = 20
            if (r0 < r3) goto L_0x0b9c
            r0 = 7
            r11.hp = r0
            int[][] r0 = r11.ga
            d r3 = r11.dp
            int r3 = r3.kH
            r0 = r0[r3]
            r0 = r0[r9]
            if (r0 < r9) goto L_0x0b9c
            r0 = 7
            r11.hp = r0
        L_0x0b9c:
            int r0 = r11.gE
            r0 = r1[r0]
            int r1 = r11.gE
            r1 = r2[r1]
            int r1 = r1 * 1000
            int r0 = r0 + r1
            r11.hq = r0
            goto L_0x0011
        L_0x0bab:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            d r0 = r11.dp
            int r0 = r0.kH
            r11.o(r0)
            r0 = 11
            r11.d(r0, r5)
            r11.gE = r7
            goto L_0x0011
        L_0x0bbe:
            r0 = r8
            goto L_0x00b3
        L_0x0bc1:
            r1 = 4
            if (r0 == r1) goto L_0x0bc6
            if (r0 != r9) goto L_0x0bda
        L_0x0bc6:
            int r0 = r11.iH
            if (r0 != 0) goto L_0x0bd5
            r0 = 100
            r11.hy = r0
        L_0x0bce:
            r11.iG = r7
            r11.x()
            goto L_0x0011
        L_0x0bd5:
            r0 = 60
            r11.hy = r0
            goto L_0x0bce
        L_0x0bda:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.iG = r7
            goto L_0x0011
        L_0x0be1:
            if (r0 != 0) goto L_0x0bfe
            int r0 = r11.iE
            if (r0 != 0) goto L_0x0bfc
            r0 = r8
        L_0x0be8:
            r11.iE = r0
        L_0x0bea:
            int r0 = r11.dD
            int[][] r1 = r11.ga
            int r2 = r11.dC
            r1 = r1[r2]
            r2 = 11
            r1 = r1[r2]
            if (r0 > r1) goto L_0x0cf2
            r11.iJ = r8
            goto L_0x0011
        L_0x0bfc:
            r0 = r7
            goto L_0x0be8
        L_0x0bfe:
            if (r0 != r8) goto L_0x0c0a
            int r0 = r11.iE
            if (r0 != r8) goto L_0x0c08
            r0 = r7
        L_0x0c05:
            r11.iE = r0
            goto L_0x0bea
        L_0x0c08:
            r0 = r8
            goto L_0x0c05
        L_0x0c0a:
            if (r0 == r5) goto L_0x0c0e
            if (r0 != r10) goto L_0x0c5f
        L_0x0c0e:
            int r1 = r11.iE
            if (r1 != 0) goto L_0x0c1c
            int r0 = r11.dC
            if (r0 != r8) goto L_0x0c1a
            r0 = r7
        L_0x0c17:
            r11.dC = r0
            goto L_0x0bea
        L_0x0c1a:
            r0 = r8
            goto L_0x0c17
        L_0x0c1c:
            int r1 = r11.iE
            if (r1 != r8) goto L_0x0bea
            if (r0 != r5) goto L_0x0c3d
            int r0 = r11.iF
            if (r0 == r8) goto L_0x0011
            int r0 = r11.dD
            if (r0 <= 0) goto L_0x0c32
            int r0 = r11.dD
            int r0 = r0 - r8
            r11.dD = r0
            r11.iF = r8
            goto L_0x0bea
        L_0x0c32:
            int r0 = r11.dD
            if (r0 > 0) goto L_0x0bea
            r11.eP = r7
            r11.dD = r7
            r11.iF = r7
            goto L_0x0bea
        L_0x0c3d:
            if (r0 != r10) goto L_0x0bea
            int r0 = r11.iF
            if (r0 == r5) goto L_0x0011
            int r0 = r11.dD
            int r1 = r11.dE
            if (r0 >= r1) goto L_0x0c52
            int r0 = r11.dD
            int r0 = r0 + 1
            r11.dD = r0
            r11.iF = r5
            goto L_0x0bea
        L_0x0c52:
            int r0 = r11.dD
            int r1 = r11.dE
            if (r0 < r1) goto L_0x0bea
            int r0 = r11.dE
            r11.dD = r0
            r11.iF = r7
            goto L_0x0bea
        L_0x0c5f:
            r1 = 4
            if (r0 == r1) goto L_0x0c64
            if (r0 != r9) goto L_0x0ce6
        L_0x0c64:
            d r0 = r11.dp
            int r1 = r11.dC
            r0.kH = r1
            r11.w = r7
            int r0 = r11.iE
            if (r0 != 0) goto L_0x0c74
            r11.iE = r8
            goto L_0x0bea
        L_0x0c74:
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 25
            r0 = r0[r1]
            if (r0 == 0) goto L_0x0c84
            r11.iG = r8
            goto L_0x0bea
        L_0x0c84:
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            if (r0 < r5) goto L_0x0cbb
            byte[] r0 = r11.ib
            int r1 = r11.dC
            int r1 = r1 * 9
            int r2 = r11.dD
            int r1 = r1 + r2
            int r1 = r1 + 49
            byte r0 = r0[r1]
            if (r0 != 0) goto L_0x0cbb
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            int r1 = r11.dD
            if (r0 != r1) goto L_0x0cbb
            int r0 = r11.ci
            r11.dB = r0
            r0 = 31
            r11.ci = r0
            r11.cx = r5
            r11.iu = r7
            goto L_0x0bea
        L_0x0cbb:
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            if (r0 < r5) goto L_0x0ce1
            int[][] r0 = r11.ga
            int r1 = r11.dC
            r0 = r0[r1]
            r1 = 11
            r0 = r0[r1]
            int r1 = r11.dD
            if (r0 != r1) goto L_0x0ce1
            int r0 = r11.ci
            r11.dB = r0
            r0 = 12
            r11.ci = r0
            r11.iI = r8
            goto L_0x0bea
        L_0x0ce1:
            r11.x()
            goto L_0x0bea
        L_0x0ce6:
            r1 = 6
            if (r0 != r1) goto L_0x0bea
            r11.d(r9, r7)
            r11.cJ = r7
            r11.db = r7
            goto L_0x0bea
        L_0x0cf2:
            r11.iJ = r7
            goto L_0x0011
        L_0x0cf6:
            int r0 = r11.bs
            int r1 = r11.eR
            if (r1 > 0) goto L_0x0011
            if (r0 != r5) goto L_0x0d06
            int r0 = r11.eS
            r11.eR = r0
            r11.eQ = r8
            goto L_0x0011
        L_0x0d06:
            if (r0 != r10) goto L_0x0d11
            int r0 = r11.eS
            r11.eR = r0
            r0 = -1
            r11.eQ = r0
            goto L_0x0011
        L_0x0d11:
            r1 = 4
            if (r0 == r1) goto L_0x0d16
            if (r0 != r9) goto L_0x0ddc
        L_0x0d16:
            r11.w = r8
            r11.dO = r7
            r11.dY = r7
            r0 = 0
            r11.ev = r0
            r0 = 0
            r11.ew = r0
            r0 = 0
            r11.ko = r0
            r0 = 0
            r11.dq = r0
            r0 = 0
            r11.kr = r0
            r0 = 9
            r11.by = r0
            r0 = 29
            r11.d(r0, r5)
            r11.fI = r7
            r0 = 99999(0x1869f, float:1.40128E-40)
            r11.fJ = r0
            r11.gY = r7
            r11.gX = r7
            java.util.Random r0 = r11.f0do
            int r0 = r0.nextInt()
            int r0 = n(r0)
            int r0 = r0 % 4
            r1 = 0
            r11.dq = r1
            byte r1 = (byte) r0
            r11.gI = r1
            f r1 = r11.aI
            byte[] r2 = r11.eB
            byte r2 = r2[r0]
            int r2 = r2 + 7
            r1.k(r2)
            r11.c(r8, r0)
            r11.ad()
            short[][][] r1 = defpackage.g.lC
            r11.a(r0, r7, r8, r1)
            f r1 = r11.aI
            r1.g(r0, r8)
            r11.dF = r7
            r0 = 0
            r11.dp = r0
            d r0 = new d
            r0.<init>()
            r11.dp = r0
            d r0 = r11.dp
            int r1 = r11.dG
            r2 = -1
            short[][][] r3 = defpackage.g.lC
            r3 = r3[r7]
            r3 = r3[r7]
            r4 = 7
            short r3 = r3[r4]
            int r3 = r3 >> 1
            short[][][] r4 = defpackage.g.lC
            r4 = r4[r7]
            r4 = r4[r7]
            r5 = 8
            short r4 = r4[r5]
            int r4 = r4 >> 1
            r0.a(r1, r2, r3, r4)
            d r0 = r11.dp
            d r1 = r11.dp
            int r1 = r1.kH
            int r1 = r1 + 51
            byte r1 = (byte) r1
            r0.lg = r1
            r11.dO = r7
            r11.dY = r7
            r11.en = r7
            r11.eo = r7
            int r0 = r11.dG
            r11.o(r0)
            d r0 = r11.dp
            d r1 = r11.dp
            d r2 = r11.dp
            int r3 = r11.gh
            r2.z = r3
            r1.kF = r3
            r0.kG = r3
            r0 = -1
            r11.dV = r0
            r11.dW = r7
            r11.gi = r7
            r11.gX = r7
            r11.gZ = r7
            r0 = -1
            r11.fB = r0
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            r11.hM = r0
            r11.ah()
            goto L_0x0011
        L_0x0ddc:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.d(r9, r7)
            r11.cJ = r7
            r11.db = r8
            goto L_0x0011
        L_0x0de8:
            int r0 = r11.bs
            if (r0 != r9) goto L_0x0df8
            r11.ci = r8
            int[] r0 = r11.aK
            r0[r7] = r7
            r11.aM = r8
            r11.aN = r5
            goto L_0x0011
        L_0x0df8:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.ci = r8
            int[] r0 = r11.aK
            r0[r7] = r7
            r11.aN = r7
            r11.aM = r7
            r11.aN = r7
            goto L_0x0011
        L_0x0e09:
            int r0 = r11.bs
            if (r0 == r5) goto L_0x0e0f
            if (r0 != r10) goto L_0x0e19
        L_0x0e0f:
            int r0 = r11.fR
            int r0 = r0 + 1
            int r0 = r0 % 2
            r11.fR = r0
            goto L_0x0011
        L_0x0e19:
            r1 = 4
            if (r0 == r1) goto L_0x0e1e
            if (r0 != r9) goto L_0x0e6f
        L_0x0e1e:
            int r0 = r11.fR
            if (r0 != 0) goto L_0x0e68
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r0 = r0[r8]
            int r1 = r11.ge
            if (r0 < r1) goto L_0x0011
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            r1 = r0[r8]
            int r2 = r11.ge
            int r1 = r1 - r2
            r0[r8] = r1
            int[][] r0 = r11.ga
            d r1 = r11.dp
            int r1 = r1.kH
            r0 = r0[r1]
            int r1 = r11.eq
            int r1 = r1 + 2
            r2 = r0[r1]
            int r2 = r2 + 1
            r0[r1] = r2
            d r0 = r11.dp
            int r0 = r0.kH
            r11.o(r0)
            d r0 = r11.dp
            int r1 = r11.gh
            r0.kG = r1
            r0 = 14
            r11.d(r0, r5)
            r11.aj()
            goto L_0x0011
        L_0x0e68:
            r0 = 14
            r11.d(r0, r5)
            goto L_0x0011
        L_0x0e6f:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r0 = 14
            r11.d(r0, r5)
            goto L_0x0011
        L_0x0e79:
            int r0 = r11.bs
            if (r0 == r5) goto L_0x0e7f
            if (r0 != r10) goto L_0x0e89
        L_0x0e7f:
            int r0 = r11.fR
            int r0 = r0 + 1
            int r0 = r0 % 2
            r11.fR = r0
            goto L_0x0011
        L_0x0e89:
            r1 = 4
            if (r0 == r1) goto L_0x0e8e
            if (r0 != r9) goto L_0x0ed3
        L_0x0e8e:
            int r0 = r11.fR
            if (r0 != 0) goto L_0x0ece
            int[][] r0 = r11.gc
            r0 = r0[r7]
            int[][] r1 = r11.ga
            r1 = r1[r7]
            int[][] r2 = r11.gc
            r2 = r2[r7]
            int r2 = r2.length
            java.lang.System.arraycopy(r0, r7, r1, r7, r2)
            int[][] r0 = r11.gc
            r0 = r0[r8]
            int[][] r1 = r11.ga
            r1 = r1[r8]
            int[][] r2 = r11.gc
            r2 = r2[r8]
            int r2 = r2.length
            java.lang.System.arraycopy(r0, r7, r1, r7, r2)
            r11.aj()
            d r0 = new d
            r0.<init>()
            r11.dp = r0
            r11.dC = r7
            r11.dD = r7
            r11.da = r8
            f r0 = r11.aI
            r1 = 12
            r0.k(r1)
            r11.d(r10, r7)
            goto L_0x0011
        L_0x0ece:
            r11.d(r9, r5)
            goto L_0x0011
        L_0x0ed3:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.d(r9, r5)
            goto L_0x0011
        L_0x0edb:
            int r0 = r11.bs
            r1 = 4
            if (r0 == r1) goto L_0x0ee2
            if (r0 != r9) goto L_0x0ee9
        L_0x0ee2:
            r0 = 33
            r11.d(r0, r5)
            goto L_0x0011
        L_0x0ee9:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            r11.d(r9, r7)
            goto L_0x0011
        L_0x0ef1:
            int r0 = r11.bs
            r11.f(r0)
            goto L_0x0011
        L_0x0ef8:
            int r0 = r11.bs
            short[][] r1 = defpackage.g.lI
            int r2 = r11.dO
            r1 = r1[r2]
            short r1 = r1[r7]
            int r2 = r11.eE
            int r1 = r1 + r2
            short[][] r2 = defpackage.g.lI
            int r3 = r11.dO
            r2 = r2[r3]
            short r2 = r2[r8]
            int r3 = r11.eE
            int r2 = r2 + r3
            r3 = 10
            if (r0 == r3) goto L_0x0f1a
            r3 = 4
            if (r0 == r3) goto L_0x0f1a
            r3 = 6
            if (r0 != r3) goto L_0x0f21
        L_0x0f1a:
            r0 = 8
            r11.d(r0, r5)
            goto L_0x0011
        L_0x0f21:
            if (r0 != r10) goto L_0x0f2e
            int r0 = r11.aT
            if (r2 < r0) goto L_0x0011
            int r0 = r11.eE
            int r0 = r0 - r5
            r11.eE = r0
            goto L_0x0011
        L_0x0f2e:
            if (r0 != r5) goto L_0x0011
            int r0 = r11.aT
            if (r1 > r0) goto L_0x0011
            int r0 = r11.eE
            int r0 = r0 + 2
            r11.eE = r0
            goto L_0x0011
        L_0x0f3c:
            int r0 = r11.bs
            r11.c(r0)
            goto L_0x0011
        L_0x0f43:
            int r0 = r11.bs
            r1 = 4
            if (r0 == r1) goto L_0x0f4a
            if (r0 != r9) goto L_0x0011
        L_0x0f4a:
            r11.bx = r7
            int r0 = r11.hZ
            r11.d(r0, r5)
            int r0 = r11.gO
            r11.m(r0)
            int r0 = r11.aN
            if (r0 <= 0) goto L_0x0011
            r11.aa()
            goto L_0x0011
        L_0x0f5f:
            int r0 = r11.bs
            r11.h(r0)
            goto L_0x0011
        L_0x0f66:
            int r0 = r11.bs
            r11.d(r0)
            goto L_0x0011
        L_0x0f6d:
            int r0 = r11.bs
            r11.e(r0)
            goto L_0x0011
        L_0x0f74:
            int r0 = r11.bs
            r11.b(r0)
            goto L_0x0011
        L_0x0f7b:
            int r0 = r11.bs
            if (r0 != r9) goto L_0x0011
            r11.ci = r9
            goto L_0x0011
        L_0x0f83:
            int r0 = r11.bs
            if (r0 != r9) goto L_0x0f8f
            L()
            f r1 = r11.aI
            r1.ak()
        L_0x0f8f:
            r1 = 6
            if (r0 != r1) goto L_0x0011
            f r0 = r11.aI
            r0.ak()
            goto L_0x0011
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c.c():void");
    }

    public final void c(byte[] bArr) {
        try {
            this.ic = ar.a("mj2znn", true);
            this.ic.a(1, bArr, 0, bArr.length);
            this.ic.bn();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void d() {
        switch (this.ci) {
            case 0:
                this.aI.al();
                break;
            case 1:
                this.aI.r(this.fx.length);
                break;
            case 3:
                this.aI.s(this.dD);
                break;
            case 4:
                f fVar = this.aI;
                int i = this.dG;
                fVar.am();
                break;
            case 5:
                f fVar2 = this.aI;
                int i2 = this.cJ;
                int i3 = this.db;
                fVar2.t(i2);
                break;
            case 7:
                f fVar3 = this.aI;
                int i4 = this.dD;
                fVar3.an();
                break;
            case 8:
                this.aI.a(this.kg);
                break;
            case 9:
                this.aI.b(this.kg);
                break;
            case 10:
                this.aI.u(this.db);
                break;
            case 11:
                f fVar4 = this.aI;
                int i5 = this.gC;
                int i6 = this.gE;
                fVar4.ao();
                break;
            case 12:
                f fVar5 = this.aI;
                int i7 = this.gC;
                fVar5.v(this.gE);
                break;
            case 13:
                f fVar6 = this.aI;
                int i8 = this.gC;
                fVar6.w(this.gE);
                break;
            case 14:
                f fVar7 = this.aI;
                int i9 = this.er;
                break;
            case Database.INT_FEECUE:
                this.aI.b();
                break;
            case 16:
                this.aI.R();
                break;
            case Database.INT_FEES:
                f fVar8 = this.aI;
                int i10 = this.fR;
                fVar8.ap();
                break;
            case 18:
                this.aI.e(this.gv, this.gw);
                break;
            case Database.INT_MTWORD:
                f fVar9 = this.aI;
                int i11 = this.fR;
                fVar9.aq();
                break;
            case Database.INT_TIMEWAIT:
                f fVar10 = this.aI;
                int i12 = this.fR;
                break;
            case Database.INT_FAILWORD:
                this.aI.o(this.fR);
                break;
            case Database.INT_CONFIRMWORD:
                f fVar11 = this.aI;
                int i13 = this.fR;
                fVar11.as();
                break;
            case 24:
                f fVar12 = this.aI;
                int i14 = this.dO;
                int i15 = this.dY;
                int i16 = this.dk;
                fVar12.f(i14, i15);
                break;
            case 25:
                this.aI.h(this.dl, this.dO);
                break;
            case 26:
                this.aI.at();
                break;
            case 27:
                this.aI.x(this.fR);
                break;
            case 28:
                this.aI.b(this.dm, this.dn);
                break;
            case 29:
                if (this.aK[0] <= 20) {
                    this.aI.au();
                    break;
                }
                break;
            case 30:
                this.aI.j(this.aW);
                break;
            case 31:
                this.aI.e(this.cx);
                break;
            case 32:
                this.aI.Z();
                break;
            case 33:
                this.aI.M();
                break;
        }
        this.aI.P();
        this.aI.a(this.hp, this.hq);
    }

    public final void i(int i) {
        this.bs = i;
        this.bv = true;
        this.bx = true;
    }

    public final void j(int i) {
        this.bv = false;
        this.bx = false;
        if (this.bs == i) {
            this.bs = -1;
        }
    }

    public final int q(int i) {
        if (this.ga[this.dp.kH][11] != this.dO || this.dO == 8 || i <= -2) {
            return (this.ga[0][6] != 0 || i <= -3) ? -4 : -3;
        }
        return -2;
    }
}
