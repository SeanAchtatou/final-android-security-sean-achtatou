package defpackage;

import android.media.MediaPlayer;
import android.util.Log;
import java.util.HashSet;
import java.util.Iterator;
import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;

/* renamed from: cz  reason: default package */
public final class cz implements MediaPlayer.OnCompletionListener, Player {
    private static final String LOG_TAG = "MIDP player";
    private bv sm;
    private HashSet sn;
    private int so;
    private int sp;
    private df sq;
    private int state = 100;

    public cz(bv bvVar) {
        this.sm = bvVar;
        this.so = 1;
        bvVar.qQ.setOnCompletionListener(this);
        this.sn = new HashSet();
    }

    private final void a(String str, Object obj) {
        Iterator it = this.sn.iterator();
        while (it.hasNext()) {
            PlayerListener playerListener = (PlayerListener) it.next();
            if (playerListener != null) {
                playerListener.bh();
            }
        }
    }

    private void bV() {
        bg();
        if (this.state == 200) {
            try {
                this.sm.qQ.prepare();
                this.state = Player.PREFETCHED;
            } catch (Exception e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
    }

    public final void D(int i) {
        this.so = -1;
    }

    public final void bg() {
        try {
            if (this.state == 100) {
                this.sm.qQ.reset();
                this.sm.qQ.setDataSource(this.sm.qM);
                this.state = Player.REALIZED;
            }
        } catch (Exception e) {
            Log.w(LOG_TAG, e);
            throw new MediaException();
        }
    }

    public final void close() {
        try {
            stop();
        } catch (Exception e) {
            a(PlayerListener.ERROR, e.getMessage());
            Log.w(LOG_TAG, "Exception when stop before close.");
        }
        this.state = 0;
        a(PlayerListener.CLOSED, null);
        bv bvVar = this.sm;
        if (bvVar.name != null) {
            bt.qI.remove(bvVar.name);
        }
        if (bvVar.qQ != null) {
            bvVar.qQ.release();
        }
        bvVar.qQ = null;
    }

    public final int getState() {
        return this.state;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        if (mediaPlayer == this.sm.qQ) {
            this.sp++;
            if (this.sp < this.so) {
                try {
                    this.sm.qQ.reset();
                    this.sm.qQ.setDataSource(this.sm.qM);
                    this.sm.qQ.prepare();
                    this.sm.qQ.start();
                } catch (Exception e) {
                    a(PlayerListener.ERROR, e.getMessage());
                }
            } else if (this.sn.isEmpty()) {
                try {
                    stop();
                } catch (MediaException e2) {
                    a(PlayerListener.ERROR, e2.getMessage());
                }
            } else {
                a(PlayerListener.END_OF_MEDIA, null);
            }
        }
    }

    public final Control s(String str) {
        if (str.indexOf("VolumeControl") == -1) {
            return null;
        }
        if (this.sq == null) {
            this.sq = new df(this.sm);
        }
        return this.sq;
    }

    public final void start() {
        bg();
        bV();
        if (this.state == 300) {
            this.state = Player.STARTED;
            try {
                if (this.so == -1) {
                    this.sm.qQ.setLooping(true);
                } else {
                    this.sm.qQ.setLooping(false);
                }
                this.sm.qQ.start();
                this.sm.qN = true;
                a(PlayerListener.STARTED, null);
            } catch (Exception e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
    }

    public final void stop() {
        if (this.state == 400) {
            this.state = 100;
            try {
                this.sm.qQ.stop();
                this.sm.qN = false;
                this.sp = 1;
                a(PlayerListener.STOPPED, null);
            } catch (IllegalStateException e) {
                a(PlayerListener.ERROR, e.getMessage());
                throw new MediaException();
            }
        }
        bg();
        bV();
    }
}
