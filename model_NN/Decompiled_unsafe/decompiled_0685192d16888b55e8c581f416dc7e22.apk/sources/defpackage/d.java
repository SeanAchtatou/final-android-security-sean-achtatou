package defpackage;

/* renamed from: d  reason: default package */
public final class d {
    public static final byte aB = 10;
    public static final byte aH = 0;
    public static final byte bI = 26;
    public static final byte bc = 23;
    public static final byte bo = 18;
    public static final byte bw = 1;
    public static final byte dL = 19;
    public static final byte dX = 20;
    public static final byte ef = 1;
    public static final byte hD = 30;
    public static final byte hL = 29;
    public static final byte ja = 1;
    public static final byte jb = 0;
    public static final byte jc = 0;
    public static final byte jd = 2;
    public static final byte je = 3;
    public static final byte jf = 4;
    public static final byte jg = 5;
    public static final byte jh = 6;
    public static final byte ji = 7;
    public static final byte jj = 8;
    public static final byte jk = 9;
    public static final byte jl = 11;
    public static final byte jm = 12;
    public static final byte jn = 13;
    public static final byte jo = 14;
    public static final byte jp = 15;
    public static final byte jq = 16;
    public static final byte jr = 17;
    public static final byte js = 21;
    public static final byte jt = 22;
    public static final byte ju = 24;
    public static final byte jv = 25;
    public static final byte jw = 27;
    public static final byte jx = 28;
    public static final byte jy = 31;
    public static final byte jz = 32;
    private static byte[] kN = {10, 1, 1, 2, 2, 82, 90, 89, 85, 70, 70, 70, 70, 70, 70, 71, 71, 71, 71, 71, 71, 71, 71, 71, 71, dX, dX, dX, dX, dX, dX, dX, 80};
    public byte bL = hD;
    public int dP;
    public int dV = 0;
    public int dZ = 0;
    public int dd;
    public int eH = 16;
    public int ee;
    public int eg;
    public int eo = 0;
    public int es = 0;
    public byte fi = -1;
    public int gg;
    public int hP = 0;
    public int hy;
    public int hz;
    public int ih = 0;
    public int ij = 0;
    public boolean ix = true;
    public byte jC = 0;
    public short[][][] kB;
    public int kC;
    public int kD;
    public int kE;
    public int kF;
    public int kG;
    public int kH;
    public int kI;
    public int kJ;
    public int kK;
    public int kL = 0;
    public byte kM = 0;
    public boolean kO = false;
    public byte kP = -1;
    public int kQ = 0;
    public byte kR = 0;
    public int kS;
    public int kT = 0;
    public int kU = 0;
    public int kV = 0;
    public int kW = 0;
    public int kX = 0;
    public byte kY = -1;
    boolean kZ = false;
    public int la = 0;
    public int lb = 0;
    public boolean lc = false;
    public boolean ld = false;
    public int le = -1;
    public int lf = -1;
    public short lg = -1;
    public short lh = -1;
    public boolean li = false;
    public int lj = -1;
    public boolean lk = false;
    public byte ll = 0;
    public byte lm = 0;
    public int ln;
    public boolean lo = false;
    public boolean lp = false;
    private byte[] lq = {1, -1};
    public int x = -1;
    public int z;

    private void S() {
        byte b;
        d dVar;
        byte b2;
        int i;
        this.la = this.kD;
        this.lb = this.eg;
        switch (this.kC) {
            case 1:
                int i2 = this.kD;
                b = e.lr[this.kH][0];
                dVar = this;
                int i3 = i2;
                b2 = this.lq[this.kJ];
                i = i3;
                dVar.kD = (b2 * b) + i;
                break;
            case 2:
                this.eg += e.lr[this.kH][1] * this.lq[this.kI];
                break;
            case 3:
            case 4:
            case 7:
            default:
                int length = e.lB[this.kH][this.kC].length;
                int i4 = this.kD;
                byte b3 = this.lq[this.dP];
                if (this.eo >= length) {
                    b2 = 0;
                    dVar = this;
                    int i5 = i4;
                    b = b3;
                    i = i5;
                } else {
                    b2 = e.lB[this.kH][this.kC][this.eo];
                    dVar = this;
                    int i6 = i4;
                    b = b3;
                    i = i6;
                }
                dVar.kD = (b2 * b) + i;
                break;
            case 5:
            case 6:
                this.kD = ((this.eo >= e.lB[this.kH][this.kC].length ? 0 : e.lB[this.kH][this.kC][this.eo]) * this.lq[this.kK]) + this.kD;
                break;
            case 8:
                break;
        }
        if (this.kC == 6 && this.la != this.kD) {
            this.eg += this.kU;
            this.kD -= this.lq[this.dP] * this.kV;
        }
    }

    private void d(int i) {
        int i2 = (!this.kO || this.kC == 8 || i == 8) ? i : 0;
        this.kC = (byte) i2;
        this.lf = 0;
        this.le = this.kB[i2][0][1];
        this.eo = 0;
        this.kY = -1;
        this.gg = this.kD;
        this.ln = this.eg;
    }

    public final void a(int i, int i2, int i3, int i4) {
        this.kD = i3;
        this.eg = i4;
        this.kH = i;
        this.kB = e.ls[i];
        this.lh = (short) i2;
        this.fi = e.lF[i];
        this.lj = e.lG[i];
        b(0, i3, i4);
        this.lc = e.lz[i];
        this.ld = e.lA[i];
        this.kO = false;
        if (i > 9) {
            this.lo = true;
        }
    }

    public final void ak() {
        this.kD = this.la;
        this.eg = this.lb;
    }

    public final void b(int i, int i2, int i3) {
        this.lk = false;
        this.kC = 0;
        this.lf = 0;
        this.le = this.kB[this.kC][0][1];
        this.eo = 0;
        int i4 = (this.kH == 0 || this.kH == 1) ? 50 : 16;
        this.hz = i4;
        this.eH = i4;
        this.ix = true;
        if (this.lh != -1) {
            try {
                this.hP = e.lE[this.lh][0];
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
            try {
                this.kG = e.lE[this.lh][1] * 10;
            } catch (RuntimeException e2) {
                e2.printStackTrace();
            }
            int i5 = this.kG;
            this.z = i5;
            this.kF = i5;
            this.kD = i2;
            this.eg = i3;
            this.ll = 0;
            this.lm = 0;
        }
    }

    public final void e(int i) {
        int i2;
        if (this.kF > 0 || this.eH != this.hz) {
            i2 = i;
        } else {
            i2 = this.kC != 6 ? 6 : i;
            this.eH--;
        }
        if (i2 >= 10 && this.kB[i2].length == 0) {
            i2 = 9;
        }
        if (this.kH < 2 && ((i2 == 1 || i2 == 2) && (this.kC == 25 || this.kC == 26 || this.kC == 27 || this.kC == 28 || this.kC == 29 || this.kC == 0))) {
            d(i2);
        }
        if (!this.lp) {
            if (this.le <= 0) {
                this.lf++;
                if (this.lf >= this.kB[this.kC].length) {
                    d(this.dd);
                    S();
                    this.eo++;
                } else {
                    this.le = this.kB[this.kC][this.lf][1];
                }
            }
            this.le--;
            this.kR = (byte) (this.kR - 1);
            if (this.lo) {
                this.x--;
            }
            S();
            this.eo++;
        }
        if (i2 == 5 && i2 == this.kC) {
            d(i2);
        }
        if (kN[i2] > kN[this.kC]) {
            d(i2);
        }
        if (this.kC == 7 && i2 == 7 && this.kH > 1 && this.kF > 0) {
            d(i2);
        }
        if (this.kH == 2 && this.kC == 11 && this.dd == 11 && this.lf > 1) {
            d(i2);
        }
    }
}
