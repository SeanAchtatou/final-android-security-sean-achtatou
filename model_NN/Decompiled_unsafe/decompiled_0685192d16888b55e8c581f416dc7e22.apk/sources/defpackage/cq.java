package defpackage;

/* renamed from: cq  reason: default package */
public interface cq {
    public static final String LOG_TAG = "VirtualDevice";

    int getOrientation();

    void onCreate();

    void onDestroy();
}
