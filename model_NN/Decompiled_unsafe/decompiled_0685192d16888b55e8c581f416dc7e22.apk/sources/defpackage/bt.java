package defpackage;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.HashMap;

/* renamed from: bt  reason: default package */
public final class bt {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static final bt qB = new bt();
    private static int qC = -1;
    private static String qD;
    private static int qE = 15;
    /* access modifiers changed from: private */
    public static int qF;
    private static float qG;
    private static AudioManager qH;
    /* access modifiers changed from: private */
    public static final HashMap qI = new HashMap();
    private static int qJ;
    private static int qK = 0;
    /* access modifiers changed from: private */
    public static int qL = 0;

    public static void I(int i) {
        qC = i;
    }

    public static void J(int i) {
        if (qC == -1) {
            K(i);
        } else if (qC != -2) {
        } else {
            if (i == 0) {
                qJ = by();
                K(0);
            } else if (qJ != 0) {
                K(qJ);
                qJ = 0;
            }
        }
    }

    /* access modifiers changed from: private */
    public static void K(int i) {
        qH.setStreamVolume(3, (int) (((float) i) * qG), 16);
    }

    public static bv a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        int available = inputStream.available();
        String str3 = MEDIA_TEMP_FILE + (available <= 0 ? "" : Integer.valueOf(available));
        if (qI.containsKey(str3)) {
            bv bvVar = (bv) qI.get(str3);
            bvVar.qN = false;
            bvVar.qP = false;
            bvVar.qO = by();
            bvVar.qQ.reset();
            return bvVar;
        }
        bt btVar = qB;
        btVar.getClass();
        bv bvVar2 = new bv(btVar);
        String str4 = "";
        if (str2.indexOf("mid") != -1) {
            str4 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str4 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str4 = ".amr";
        }
        bvVar2.name = str3;
        bvVar2.type = str2;
        bvVar2.qM = str3 + str4;
        FileOutputStream openFileOutput = cd.getActivity().openFileOutput(bvVar2.qM, 1);
        byte[] bArr = new byte[ap.FIRE_PRESSED];
        while (true) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                bvVar2.qQ = new MediaPlayer();
                bvVar2.qM = qD + bvVar2.qM;
                qI.put(str3, bvVar2);
                return bvVar2;
            }
        }
    }

    protected static void b(Activity activity) {
        qD = activity.getFilesDir().getAbsolutePath() + File.separator;
        AudioManager audioManager = (AudioManager) activity.getSystemService("audio");
        qH = audioManager;
        int streamMaxVolume = audioManager.getStreamMaxVolume(3);
        qE = streamMaxVolume;
        qG = ((float) streamMaxVolume) / 100.0f;
        qF = by();
        bw.a(new bu());
    }

    public static int by() {
        return (int) (((float) qH.getStreamVolume(3)) / qG);
    }

    public static void d(boolean z) {
        if (z) {
            qK = by();
            J(0);
        } else if (qK != 0) {
            J(qK);
        }
    }

    protected static void onDestroy() {
        for (bv bvVar : qI.values()) {
            if (bvVar.qQ != null) {
                bvVar.qQ.release();
            }
            bvVar.qQ = null;
        }
        qI.clear();
        K(qF);
    }
}
