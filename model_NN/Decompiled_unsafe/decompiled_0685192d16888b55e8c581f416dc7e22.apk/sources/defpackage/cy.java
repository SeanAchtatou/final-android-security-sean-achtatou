package defpackage;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.microedition.media.PlayerListener;

/* renamed from: cy  reason: default package */
public final class cy implements t {
    private HttpURLConnection sl;
    private URL url;

    public cy(String str, int i, boolean z) {
        try {
            if (!bU()) {
                throw new IOException("No avaliable connection.");
            }
            this.url = new URL(str);
            this.sl = (HttpURLConnection) this.url.openConnection();
            this.sl.setDoInput(true);
            if (i != 1) {
                this.sl.setDoOutput(true);
            }
            if (z) {
                this.sl.setConnectTimeout(10000);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private static boolean bU() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager bF = cd.bF();
            return bF != null && (activeNetworkInfo = bF.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            Log.v(PlayerListener.ERROR, e.toString());
        }
    }

    public final void close() {
        this.sl.disconnect();
    }
}
