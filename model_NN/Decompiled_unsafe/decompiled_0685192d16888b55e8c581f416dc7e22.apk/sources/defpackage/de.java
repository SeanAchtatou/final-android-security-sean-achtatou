package defpackage;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.net.Socket;
import java.net.URI;
import javax.microedition.media.PlayerListener;

/* renamed from: de  reason: default package */
public final class de implements w {
    private int mode;
    private Socket sC;

    public de(String str, int i) {
        this.mode = i;
        try {
            if (!bU()) {
                throw new IOException("No avaliable connection.");
            }
            URI uri = new URI(str);
            this.sC = new Socket(uri.getHost(), uri.getPort());
        } catch (Exception e) {
            throw new IOException("URISyntaxException");
        }
    }

    private static boolean bU() {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager bF = cd.bF();
            return bF != null && (activeNetworkInfo = bF.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
        } catch (Exception e) {
            Log.v(PlayerListener.ERROR, e.toString());
        }
    }

    public final void close() {
        this.sC.close();
        this.sC = null;
        System.gc();
    }
}
