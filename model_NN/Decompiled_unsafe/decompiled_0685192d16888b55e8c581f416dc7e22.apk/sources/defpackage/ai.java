package defpackage;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: ai  reason: default package */
public abstract class ai implements cm {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    private int height = -1;
    protected ae pD = null;
    public boolean pE = false;
    ArrayList pF = new ArrayList();
    ad pG = null;
    String pH;
    boolean pI;
    private int width = -1;

    ai() {
    }

    /* access modifiers changed from: protected */
    public void aM() {
    }

    public abstract int aR();

    public void aU() {
        if (!this.pI) {
            Iterator it = this.pF.iterator();
            while (it.hasNext()) {
                by.a((ac) it.next());
            }
            this.pI = true;
        }
    }

    public boolean aV() {
        return false;
    }

    public void ba() {
        if (this.pI) {
            Iterator it = this.pF.iterator();
            while (it.hasNext()) {
                by.b((ac) it.next());
            }
            this.pI = false;
            try {
                aM();
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void y(int i) {
    }

    /* access modifiers changed from: protected */
    public void z(int i) {
    }
}
