package defpackage;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;

/* renamed from: bf  reason: default package */
public final class bf {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    public static boolean A(String str) {
        String z = z(str);
        String z2 = z(z);
        String[] fileList = cd.getActivity().fileList();
        ArrayList<String> arrayList = new ArrayList<>();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(z2 == null || fileList[i].indexOf(z2) == -1) || z2 == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                }
            }
        }
        for (String equals : arrayList) {
            if (equals.equals(z)) {
                return true;
            }
        }
        return false;
    }

    public static InputStream B(String str) {
        return cd.getActivity().openFileInput(z(str) + DATA_STORE_FILE);
    }

    public static String z(String str) {
        return str.replace(File.separator, "_");
    }
}
