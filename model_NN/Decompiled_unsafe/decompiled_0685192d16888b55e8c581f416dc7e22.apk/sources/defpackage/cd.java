package defpackage;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Process;
import android.util.Log;
import com.androidbox.sgmj2net8.R;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.Timer;

/* renamed from: cd  reason: default package */
public final class cd {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47888;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47887;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_EVENT = 47904;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    private static Handler handler;
    /* access modifiers changed from: private */
    public static Activity rc;
    private static int rd = 0;
    private static final String[] re = {".png", ".mp3", ".jpg", ".jpeg", ".mpeg", ".bmp"};
    public static boolean rf = false;
    /* access modifiers changed from: private */
    public static boolean rg = false;
    private static final Timer rh = new Timer();

    public static final InputStream D(String str) {
        String str2;
        if (str == null) {
            throw new IOException("Can't load resource noname.");
        }
        String str3 = str;
        while (str3.startsWith(File.separator)) {
            str3 = str3.substring(1);
        }
        String[] split = str3.split("\\.");
        String P = dl.P(split[0]);
        String Q = dl.Q(split[0]);
        if (split.length == 1) {
            str2 = "a_b";
        } else if (split.length == 2) {
            str2 = split[1];
        } else {
            String str4 = "";
            for (int i = 1; i < split.length; i++) {
                str4 = str4 + split[i];
            }
            str2 = str4;
        }
        if (Q.length() == 0 || Q.charAt(0) == '_') {
            Q = "b_a" + Q;
        }
        int i2 = 0;
        while (true) {
            if (i2 >= re.length) {
                break;
            } else if (str2.equalsIgnoreCase(re[i2])) {
                str2 = str2.toLowerCase();
                break;
            } else {
                i2++;
            }
        }
        String str5 = P + Q + "." + str2;
        try {
            return rc.getAssets().open(str5);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str5 + " is not exist.");
            if (!rf) {
                return null;
            }
            throw new IOException();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: bw.a(int, java.lang.Object):android.os.Message
     arg types: [?, java.lang.String]
     candidates:
      bw.a(int, java.lang.String):void
      bw.a(int, java.lang.Object):android.os.Message */
    public static boolean E(String str) {
        bw.a(new ch());
        bw.b(bw.a((int) MSG_SYSTEM_FUNCTION_REQUEST, (Object) str));
        return true;
    }

    public static boolean F(String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            rc.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
            return true;
        } else if (str.startsWith("tel:")) {
            rc.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    public static boolean G(String str) {
        try {
            return rc.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static void O(int i) {
        rc.setRequestedOrientation(i);
    }

    public static void b(Activity activity) {
        rc = activity;
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(dl.L("globe")));
            handler = new Handler();
            bG();
            if (properties.containsKey("ThrowIOExceptions")) {
                rf = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                cj.rm = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            bw.bq();
            bi.b(activity);
            if (properties.containsKey("fps")) {
                bi.H(1000 / Integer.parseInt(properties.getProperty("fps")));
            }
            bh.bq();
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        bh.g(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            bc.bq();
            bm.b(activity);
            bt.b(activity);
            if (properties.containsKey("VolumeMode")) {
                bt.I(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            cj.b(activity);
            by.bq();
            bg.C(properties.getProperty("device"));
            cn.H(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            bw.a((int) MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
            bw.a((int) MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
            bw.a((int) MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
            bw.a((int) MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
            bw.a((int) MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
            bw.a((int) MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
            bw.a((int) MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
            bw.a(new ce());
            bw.b(bw.a((int) MSG_SYSTEM_LOG_EVENT, new String[]{"Launch"}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static void b(String str, int i) {
        handler.post(new ci(str, 1));
    }

    protected static void bC() {
        rh.cancel();
        rh.purge();
        by.onDestroy();
        bc.onDestroy();
        bi.onDestroy();
        bm.onDestroy();
        bt.onDestroy();
        cj.onDestroy();
        if (bg.qg != null) {
            bg.qg.onDestroy();
        }
        if (cn.rp != null) {
            cn.rp.onDestroy();
        }
        bh.onDestroy();
        bw.onDestroy();
        rc.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static void bD() {
        bw.b(bw.a((int) MSG_SYSTEM_EXIT, (Object) null));
    }

    public static void bE() {
        if (!rg) {
            AlertDialog.Builder builder = new AlertDialog.Builder(rc);
            builder.setTitle((int) R.string.alert);
            builder.setMessage((int) R.string.exit_dialog);
            builder.setPositiveButton((int) R.string.yes, new cf());
            builder.setNegativeButton((int) R.string.no, new cg());
            builder.setCancelable(false);
            pause();
            builder.create().show();
            rg = true;
        }
    }

    public static ConnectivityManager bF() {
        return (ConnectivityManager) rc.getSystemService("connectivity");
    }

    public static void bG() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) rc.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
            }
        }
    }

    public static int bH() {
        return rc.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int bI() {
        return rc.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static Timer bJ() {
        return rh;
    }

    public static int bK() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static Activity getActivity() {
        return rc;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getString(int i) {
        return rc.getString(i);
    }

    public static void pause() {
        int i = rd + 1;
        rd = i;
        if (i == 1) {
            bw.b(bw.a((int) MSG_SYSTEM_ON_PAUSE, (Object) null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + rd);
        }
    }

    public static void resume() {
        int i = rd - 1;
        rd = i;
        if (i == 0) {
            bw.b(bw.a((int) MSG_SYSTEM_ON_RESUME, (Object) null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + rd);
        }
        if (rd <= 0) {
            rd = 0;
        }
    }
}
