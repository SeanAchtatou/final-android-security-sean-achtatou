package defpackage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import java.io.InputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;

/* renamed from: bi  reason: default package */
public final class bi implements SurfaceHolder.Callback {
    public static final String LOG_TAG = "GraphicsManager";
    public static SurfaceView qi;
    public static final bi qj = new bi();
    /* access modifiers changed from: private */
    public static boolean qk = false;
    private static int ql = 33;
    private static SurfaceHolder qm;
    private static final Rect qn = new Rect(-1, -1, -1, -1);
    private static final LinkedHashSet qp = new LinkedHashSet();
    /* access modifiers changed from: private */
    public static boolean qq;
    private static final Runnable qr = new bk();
    /* access modifiers changed from: private */
    public boolean qo;

    public static void H(int i) {
        ql = i;
    }

    public static Bitmap a(int i, int i2, boolean z, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.RGB_565);
        createBitmap.eraseColor(-16777216);
        return createBitmap;
    }

    public static Bitmap a(InputStream inputStream) {
        if (inputStream == null) {
            return null;
        }
        try {
            return BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            Log.w(LOG_TAG, "createBitmap with InputStream error." + e);
            return null;
        }
    }

    public static final void a(bl blVar) {
        qp.add(blVar);
    }

    protected static void b(Activity activity) {
        SurfaceView surfaceView = new SurfaceView(activity);
        qi = surfaceView;
        surfaceView.setId(268049792);
        qi.setFocusable(true);
        qi.setFocusableInTouchMode(true);
        qi.setLongClickable(true);
        qi.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        qm = qi.getHolder();
        bm.a(qi);
        qm.addCallback(qj);
        try {
            qm.setType(1);
        } catch (Exception e) {
            try {
                qm.setType(2);
            } catch (Exception e2) {
                qm.setType(0);
            }
        }
        bw.a(new bj());
        if (!qq) {
            qq = true;
            new Thread(qr).start();
        }
    }

    public static final void b(bl blVar) {
        qp.remove(blVar);
    }

    public static void br() {
        qk = false;
        qq = false;
    }

    public static final void bs() {
        if (qm == null) {
            qm = qi.getHolder();
        }
        synchronized (qm) {
            Canvas lockCanvas = qm.lockCanvas();
            if (lockCanvas != null) {
                lockCanvas.drawColor(-16777216);
                Iterator it = qp.iterator();
                while (it.hasNext()) {
                    ((bl) it.next()).onDraw(lockCanvas);
                }
                qm.unlockCanvasAndPost(lockCanvas);
            }
        }
    }

    public static Bitmap c(byte[] bArr, int i, int i2) {
        if (bArr == null) {
            return null;
        }
        if (i < 0 || i2 < 0 || bArr.length < 0) {
            throw new IllegalArgumentException();
        } else if (i + i2 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException();
        } else {
            try {
                return BitmapFactory.decodeByteArray(bArr, i, i2);
            } catch (Exception e) {
                Log.w(LOG_TAG, "createBitmap with imageData error." + e);
                return null;
            }
        }
    }

    protected static void onDestroy() {
        br();
        qj.qo = false;
        qm.removeCallback(qj);
        bm.b(qi);
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        qj.qo = true;
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        qj.qo = false;
    }
}
