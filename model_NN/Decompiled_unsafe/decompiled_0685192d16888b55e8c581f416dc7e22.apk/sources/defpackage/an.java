package defpackage;

import android.view.ViewGroup;
import java.util.Iterator;

/* renamed from: an  reason: default package */
final class an implements Runnable {
    final /* synthetic */ am pU;

    an(am amVar) {
        this.pU = amVar;
    }

    public final void run() {
        Iterator it = this.pU.pF.iterator();
        while (it.hasNext()) {
            ((ViewGroup) this.pU.getView()).addView(((ac) it.next()).aX());
        }
    }
}
