package defpackage;

import android.app.AlertDialog;
import android.view.View;

/* renamed from: af  reason: default package */
final class af implements Runnable {
    final /* synthetic */ AlertDialog pA;
    final /* synthetic */ View pB;
    final /* synthetic */ ae pC;
    final /* synthetic */ String pz;

    af(ae aeVar, String str, AlertDialog alertDialog, View view) {
        this.pC = aeVar;
        this.pz = str;
        this.pA = alertDialog;
        this.pB = view;
    }

    public final void run() {
        if (this.pz != null) {
            this.pA.setTitle(this.pz);
            this.pA.setView(this.pB);
            this.pA.show();
        }
    }
}
