package defpackage;

/* renamed from: ak  reason: default package */
public class ak {
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final int VCENTER = 2;
    private static final StringBuffer pQ = new StringBuffer();
    private int color;
    public int pO = 0;
    public int pP = 0;

    private static void be() {
        try {
            throw new RuntimeException("Must be implemented in DisplayGraphics");
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void a(aj ajVar) {
        be();
    }

    public void a(al alVar, int i, int i2, int i3) {
        be();
    }

    public void a(al alVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        be();
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
        be();
    }

    public void b(String str, int i, int i2, int i3) {
        be();
    }

    public void bd() {
    }

    public void d(int i, int i2, int i3, int i4) {
        be();
    }

    public final void e(int i, int i2, int i3) {
        setColor((i2 << 8) + i3 + (i << 16));
    }

    public void e(int i, int i2, int i3, int i4) {
        be();
    }

    public void f(int i, int i2, int i3, int i4) {
    }

    public void setColor(int i) {
        this.color = i;
    }

    public void translate(int i, int i2) {
        this.pO += i;
        this.pP += i2;
    }
}
