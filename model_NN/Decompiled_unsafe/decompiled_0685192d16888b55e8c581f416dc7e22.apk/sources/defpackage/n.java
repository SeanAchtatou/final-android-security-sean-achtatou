package defpackage;

import java.util.Random;

/* renamed from: n  reason: default package */
public final class n {
    static Random nf = new Random();
    int eF = 0;
    int eQ = 0;
    final int gC = 2;
    int gT = 0;
    int[] gu;
    int kW = 80;
    int lf = 0;
    int ne = 0;
    int[] ng;
    int[] nh;
    int[] ni;
    int nj = 0;

    private void a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = i2 - i;
        int i10 = 255 - i3;
        int i11 = i6 - i5;
        int i12 = i8 - 0;
        for (int i13 = 0; i13 < i9; i13++) {
            this.gu[i + i13] = -16777216 | ((((i13 * i10) / i9) + i3) << 16) | ((((i13 * i11) / i9) + i5) << 8) | (((i13 * i12) / i9) + 0);
        }
    }

    /* access modifiers changed from: package-private */
    public final void b(int i, int i2, int i3, int i4) {
        this.lf = i;
        this.ne = 285;
        this.eQ = i3;
        this.eF = i4;
        this.nj = this.eF * 2;
        this.gu = new int[this.nj];
        this.ng = new int[(this.eQ * (this.eF + 2))];
        this.nh = new int[(this.eQ * (this.eF + 2))];
        for (int i5 = 0; i5 < this.ng.length; i5++) {
            if (Math.abs((i5 % this.eQ) - (this.eQ / 2)) < (this.eQ / 2) - 3) {
                this.ng[i5] = Math.abs(nf.nextInt() % this.nj);
            } else if (Math.abs(nf.nextInt() % 6) < 5) {
                this.ng[i5] = 0;
            }
        }
        int i6 = this.nj >> 2;
        int i7 = this.nj >> 1;
        int i8 = this.nj;
        a(0, i6, 0, 255, 0, 0, 0, 0);
        a(i6, i7, 255, 255, 0, 255, 0, 0);
        a(i7, i8, 255, 255, 255, 255, 0, 255);
        this.ni = new int[(this.eQ * this.eF)];
    }

    /* access modifiers changed from: package-private */
    public final void c(ak akVar) {
        this.gT = Math.abs(nf.nextInt() % this.eF);
        int i = this.eQ * this.eF;
        int i2 = this.eQ * this.gT;
        for (int i3 = 0; i3 < this.eQ; i3++) {
            for (int i4 = 0; i4 < 2; i4++) {
                this.nh[(this.eQ * this.eF) + (this.eQ * i4) + i3] = this.ng[(this.eQ * i4) + i2 + i3];
            }
        }
        int i5 = this.eQ * 2;
        for (int i6 = 0; i6 < i; i6++) {
            int i7 = (((this.nh[i6 + i5] + this.nh[((i6 - 1) + i5) - this.eQ]) + this.nh[(i6 + i5) - this.eQ]) + this.nh[((i6 + 1) + i5) - this.eQ]) >> 2;
            this.nh[i6] = i7 > 0 ? i7 - 1 : 0;
            int[] iArr = this.ni;
            int i8 = this.gu[this.nh[i6]];
            iArr[i6] = i8;
            if ((16711680 & i8) < ((this.kW << 24) >>> 8)) {
                this.ni[i6] = 0;
            }
        }
        akVar.a(this.ni, 0, this.eQ, this.lf, this.ne - this.eF, this.eQ, this.eF, true);
    }
}
