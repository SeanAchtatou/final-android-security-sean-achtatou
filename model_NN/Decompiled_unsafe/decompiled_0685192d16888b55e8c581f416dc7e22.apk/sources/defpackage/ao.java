package defpackage;

import android.view.ViewGroup;
import java.util.Iterator;

/* renamed from: ao  reason: default package */
final class ao implements Runnable {
    final /* synthetic */ am pU;

    ao(am amVar) {
        this.pU = amVar;
    }

    public final void run() {
        Iterator it = this.pU.pF.iterator();
        while (it.hasNext()) {
            ((ViewGroup) this.pU.getView()).removeView(((ac) it.next()).aX());
        }
    }
}
