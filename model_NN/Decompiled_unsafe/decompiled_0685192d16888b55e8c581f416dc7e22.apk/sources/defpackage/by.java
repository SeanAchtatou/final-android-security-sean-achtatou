package defpackage;

import android.view.Menu;
import android.view.MenuItem;
import java.util.ArrayList;
import java.util.Iterator;

/* renamed from: by  reason: default package */
public final class by {
    public static final int OPTION_MENU_ITEM_ABOUT = 64160;
    public static final int OPTION_MENU_ITEM_EXIT = 64161;
    private static final ArrayList qZ = new ArrayList();
    private static int[] ra;

    public static void a(MenuItem menuItem) {
        ((cc) qZ.get(menuItem.getItemId())).aY();
    }

    public static void a(cc ccVar) {
        if (!qZ.contains(ccVar)) {
            qZ.add(0, ccVar);
        }
    }

    public static void b(cc ccVar) {
        qZ.remove(ccVar);
    }

    protected static void bq() {
        a(new bz());
        a(new ca());
    }

    protected static void onDestroy() {
        qZ.clear();
    }

    public static void onPrepareOptionsMenu(Menu menu) {
        boolean z;
        int i;
        menu.clear();
        Iterator it = qZ.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            cc ccVar = (cc) it.next();
            if (ra != null) {
                boolean z2 = false;
                for (int i4 : ra) {
                    if (i4 == ccVar.getId()) {
                        z2 = true;
                    }
                }
                z = z2;
            } else {
                z = false;
            }
            if (!z) {
                menu.add(131072, i3, i2, ccVar.aW());
                i = i2 + 1;
            } else {
                i = i2;
            }
            i3++;
            i2 = i;
        }
    }
}
