package defpackage;

import javax.microedition.media.control.VolumeControl;

/* renamed from: df  reason: default package */
public final class df implements VolumeControl {
    private bv sm;

    public df(bv bvVar) {
        this.sm = bvVar;
    }

    public final int E(int i) {
        int i2 = i < 0 ? 0 : i;
        if (i2 > 100) {
            i2 = 100;
        }
        this.sm.qO = i2;
        bt.J(i2);
        return i2;
    }
}
