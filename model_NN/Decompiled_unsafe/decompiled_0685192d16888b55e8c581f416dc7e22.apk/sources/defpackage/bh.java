package defpackage;

import android.util.Log;
import java.util.HashSet;
import java.util.Iterator;

/* renamed from: bh  reason: default package */
public final class bh {
    public static final String LOG_TAG = "FeatureManager";
    private static HashSet qh = new HashSet();

    protected static void bq() {
    }

    public static cp g(String str, String str2) {
        cp cpVar;
        Exception e;
        try {
            cp cpVar2 = (cp) Class.forName(str).newInstance();
            try {
                cpVar2.I(str2);
                qh.add(cpVar2);
                return cpVar2;
            } catch (Exception e2) {
                e = e2;
                cpVar = cpVar2;
                Log.w(LOG_TAG, e);
                return cpVar;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            cpVar = null;
            e = exc;
            Log.w(LOG_TAG, e);
            return cpVar;
        }
    }

    protected static void onDestroy() {
        if (!qh.isEmpty()) {
            Iterator it = qh.iterator();
            while (it.hasNext()) {
                ((cp) it.next()).onDestroy();
            }
        }
    }
}
