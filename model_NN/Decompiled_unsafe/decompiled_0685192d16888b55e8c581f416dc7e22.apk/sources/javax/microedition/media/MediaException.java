package javax.microedition.media;

import java.io.IOException;

public class MediaException extends IOException {
    private static final long serialVersionUID = 1;
}
