package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface MIDIControl extends Control {
    public static final int CONTROL_CHANGE = 176;
    public static final int NOTE_ON = 144;
}
