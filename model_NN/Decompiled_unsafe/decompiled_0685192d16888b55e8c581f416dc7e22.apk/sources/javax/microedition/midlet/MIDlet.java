package javax.microedition.midlet;

public abstract class MIDlet implements be {
    private int pY;

    public static void bi() {
        bw.b(bw.a((int) cd.MSG_SYSTEM_EXIT, (Object) null));
    }

    public static String t(String str) {
        return bc.y(str);
    }

    public static boolean u(String str) {
        return cd.E(str);
    }

    public final void bj() {
        this.pY = 0;
        bc.start();
    }

    public final int getState() {
        return this.pY;
    }

    public final void onDestroy() {
        this.pY = 3;
    }

    public final void onPause() {
        this.pY = 2;
    }

    public final void onResume() {
        this.pY = 1;
    }

    public final void onStart() {
        this.pY = 1;
    }
}
