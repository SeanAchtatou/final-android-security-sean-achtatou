package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.Phonemetadata;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class MetadataManager {
    private static final String ALTERNATE_FORMATS_FILE_PREFIX = "/com/google/i18n/phonenumbers/data/PhoneNumberAlternateFormatsProto";
    private static final Logger LOGGER = Logger.getLogger(MetadataManager.class.getName());
    private static final String SHORT_NUMBER_METADATA_FILE_PREFIX = "/com/google/i18n/phonenumbers/data/ShortNumberMetadataProto";
    private static final Map<Integer, Phonemetadata.PhoneMetadata> callingCodeToAlternateFormatsMap = Collections.synchronizedMap(new HashMap());
    private static final Set<Integer> countryCodeSet = AlternateFormatsCountryCodeSet.getCountryCodeSet();
    private static final Set<String> regionCodeSet = ShortNumbersRegionCodeSet.getRegionCodeSet();
    private static final Map<String, Phonemetadata.PhoneMetadata> regionCodeToShortNumberMetadataMap = Collections.synchronizedMap(new HashMap());

    private MetadataManager() {
    }

    private static void close(InputStream in) {
        if (in != null) {
            try {
                in.close();
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, e.toString());
            }
        }
    }

    private static void loadAlternateFormatsMetadataFromFile(int countryCallingCode) {
        String valueOf = String.valueOf(String.valueOf("/com/google/i18n/phonenumbers/data/PhoneNumberAlternateFormatsProto_"));
        ObjectInputStream in = null;
        try {
            ObjectInputStream in2 = new ObjectInputStream(PhoneNumberMatcher.class.getResourceAsStream(new StringBuilder(valueOf.length() + 11).append(valueOf).append(countryCallingCode).toString()));
            try {
                Phonemetadata.PhoneMetadataCollection alternateFormats = new Phonemetadata.PhoneMetadataCollection();
                alternateFormats.readExternal(in2);
                for (Phonemetadata.PhoneMetadata metadata : alternateFormats.getMetadataList()) {
                    callingCodeToAlternateFormatsMap.put(Integer.valueOf(metadata.getCountryCode()), metadata);
                }
                close(in2);
            } catch (IOException e) {
                e = e;
                in = in2;
                try {
                    LOGGER.log(Level.WARNING, e.toString());
                    close(in);
                } catch (Throwable th) {
                    th = th;
                    close(in);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                close(in);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            LOGGER.log(Level.WARNING, e.toString());
            close(in);
        }
    }

    static Phonemetadata.PhoneMetadata getAlternateFormatsForCountry(int countryCallingCode) {
        if (!countryCodeSet.contains(Integer.valueOf(countryCallingCode))) {
            return null;
        }
        synchronized (callingCodeToAlternateFormatsMap) {
            if (!callingCodeToAlternateFormatsMap.containsKey(Integer.valueOf(countryCallingCode))) {
                loadAlternateFormatsMetadataFromFile(countryCallingCode);
            }
        }
        return callingCodeToAlternateFormatsMap.get(Integer.valueOf(countryCallingCode));
    }

    private static void loadShortNumberMetadataFromFile(String regionCode) {
        String str;
        Class<PhoneNumberMatcher> cls = PhoneNumberMatcher.class;
        String valueOf = String.valueOf("/com/google/i18n/phonenumbers/data/ShortNumberMetadataProto_");
        String valueOf2 = String.valueOf(regionCode);
        if (valueOf2.length() != 0) {
            str = valueOf.concat(valueOf2);
        } else {
            str = new String(valueOf);
        }
        ObjectInputStream in = null;
        try {
            ObjectInputStream in2 = new ObjectInputStream(cls.getResourceAsStream(str));
            try {
                Phonemetadata.PhoneMetadataCollection shortNumberMetadata = new Phonemetadata.PhoneMetadataCollection();
                shortNumberMetadata.readExternal(in2);
                for (Phonemetadata.PhoneMetadata metadata : shortNumberMetadata.getMetadataList()) {
                    regionCodeToShortNumberMetadataMap.put(regionCode, metadata);
                }
                close(in2);
            } catch (IOException e) {
                e = e;
                in = in2;
                try {
                    LOGGER.log(Level.WARNING, e.toString());
                    close(in);
                } catch (Throwable th) {
                    th = th;
                    close(in);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                in = in2;
                close(in);
                throw th;
            }
        } catch (IOException e2) {
            e = e2;
            LOGGER.log(Level.WARNING, e.toString());
            close(in);
        }
    }

    static Set<String> getShortNumberMetadataSupportedRegions() {
        return regionCodeSet;
    }

    static Phonemetadata.PhoneMetadata getShortNumberMetadataForRegion(String regionCode) {
        if (!regionCodeSet.contains(regionCode)) {
            return null;
        }
        synchronized (regionCodeToShortNumberMetadataMap) {
            if (!regionCodeToShortNumberMetadataMap.containsKey(regionCode)) {
                loadShortNumberMetadataFromFile(regionCode);
            }
        }
        return regionCodeToShortNumberMetadataMap.get(regionCode);
    }
}
