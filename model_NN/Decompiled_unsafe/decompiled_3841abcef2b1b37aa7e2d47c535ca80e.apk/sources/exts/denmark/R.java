package exts.denmark;

public final class R {

    public static final class anim {
        public static final int cycle_7 = 2130968576;
        public static final int dialog_close = 2130968577;
        public static final int dialog_open = 2130968578;
        public static final int fade_in = 2130968579;
        public static final int fade_out = 2130968580;
        public static final int shake = 2130968581;
        public static final int slide_in_right = 2130968582;
    }

    public static final class attr {
    }

    public static final class color {
        public static final int black = 2131099648;
        public static final int blue_dark = 2131099651;
        public static final int blue_light = 2131099654;
        public static final int blue_semi_light = 2131099655;
        public static final int darkgrey = 2131099650;
        public static final int grey = 2131099649;
        public static final int grey_blue = 2131099656;
        public static final int grey_darker = 2131099652;
        public static final int white = 2131099653;
        public static final int white_smoke = 2131099657;
    }

    public static final class dimen {
        public static final int phonenumber_text_size = 2131165186;
        public static final int text_large = 2131165185;
        public static final int text_title = 2131165184;
    }

    public static final class drawable {
        public static final int android = 2130837504;
        public static final int button_primary_raised_background = 2130837505;
        public static final int button_primary_raised_background_click = 2130837506;
        public static final int button_primary_raised_background_disabled = 2130837507;
        public static final int button_secondary_raised_background = 2130837508;
        public static final int button_secondary_raised_background_click = 2130837509;
        public static final int db_logo = 2130837510;
        public static final int dialog_full_holo_light = 2130837511;
        public static final int ic_launcher = 2130837512;
        public static final int mms_menu_icon = 2130837513;
        public static final int selector_button_primary_raised = 2130837514;
        public static final int selector_button_secondary_raised = 2130837515;
    }

    public static final class id {
        public static final int android_logo = 2131361804;
        public static final int confirm_identity_text = 2131361797;
        public static final int content_view = 2131361792;
        public static final int cpr_number = 2131361798;
        public static final int credit_card_details = 2131361793;
        public static final int header = 2131361796;
        public static final int header_space = 2131361795;
        public static final int loading_spinner = 2131361794;
        public static final int password = 2131361801;
        public static final int phone_code_text = 2131361800;
        public static final int phone_edit_text = 2131361799;
        public static final int send = 2131361803;
        public static final int take_numid_picture = 2131361802;
        public static final int update_text = 2131361805;
    }

    public static final class layout {
        public static final int base_frame = 2130903040;
        public static final int mobilebank = 2130903041;
        public static final int update = 2130903042;
    }

    public static final class string {
        public static final int app_name = 2131230720;
        public static final int confirm_identity = 2131230724;
        public static final int db_logo_description = 2131230726;
        public static final int hint_cpr_number = 2131230727;
        public static final int hint_password = 2131230728;
        public static final int hint_phone_number = 2131230725;
        public static final int phone_country_code = 2131230723;
        public static final int phone_country_code_with_plus = 2131230722;
        public static final int retake_numid_picture = 2131230730;
        public static final int send = 2131230731;
        public static final int take_numid_picture = 2131230729;
        public static final int update = 2131230721;
    }

    public static final class style {
        public static final int AppBaseTheme = 2131296256;
        public static final int DialogAnimation = 2131296258;
        public static final int LightDialogTheme = 2131296257;
        public static final int TextInput = 2131296259;
        public static final int TextTitle = 2131296260;
    }

    public static final class xml {
        public static final int policies = 2131034112;
    }
}
