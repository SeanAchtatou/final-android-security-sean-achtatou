package com.soft.android.appinstaller;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

public final class GlobalConfig {
    private static GlobalConfig instance = null;
    private static final String tag = "GlobalConfig";
    Context con;
    private HashMap<String, String> config;
    private HashMap<String, String> globalVars;
    private boolean isInitialized = false;
    private Resources res;

    private GlobalConfig() {
        Log.v(tag, "Creating instance...");
        this.globalVars = new HashMap<>();
    }

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    private void parseConfigLine(String s) {
        int div = s.indexOf(32);
        if (div > 0) {
            this.config.put(s.substring(0, div), s.substring(div + 1));
        }
    }

    public void init(Context con2) {
        if (!this.isInitialized) {
            this.con = con2;
            this.isInitialized = true;
            this.config = new HashMap<>();
            this.res = con2.getResources();
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(this.res.openRawResource(R.raw.config), "UTF-8"));
                while (true) {
                    String line = br.readLine();
                    if (line != null) {
                        parseConfigLine(line);
                    } else {
                        return;
                    }
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public String getPrefsName() {
        return "prefs" + getValue("id");
    }

    public String getValue(String key) {
        String s;
        if (this.config == null || (s = this.config.get(key)) == null) {
            return "";
        }
        return s;
    }

    public String getValue(String key, String defaultValue) {
        String s;
        if (this.config == null || (s = this.config.get(key)) == null) {
            return defaultValue;
        }
        return s;
    }

    public boolean getValueNull(String key) {
        if (this.config == null || this.config.get(key) == null) {
            return true;
        }
        return false;
    }

    public void setRuntimeValue(String key, String value) {
        this.globalVars.put(key, value);
    }

    public String getRuntimeValue(String key) {
        return this.globalVars.get(key);
    }

    public boolean existsRuntimeValue(String key) {
        return this.globalVars.containsKey(key);
    }
}
