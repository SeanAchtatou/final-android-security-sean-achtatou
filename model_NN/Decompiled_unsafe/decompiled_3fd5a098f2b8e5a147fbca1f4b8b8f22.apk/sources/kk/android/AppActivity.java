package kk.android;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;

public class AppActivity extends Activity {
    private static Context dae6r;

    /* renamed from: dae6r  reason: collision with other field name */
    ProgressDialog f0dae6r;

    /* renamed from: dae6r  reason: collision with other field name */
    private Handler f1dae6r = new Ohlah(this);

    /* renamed from: dae6r  reason: collision with other field name */
    dae6r f2dae6r;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.main);
        dae6r = this;
        ((Button) findViewById(R.id.button1)).setOnClickListener(new hohP7(this));
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        switch (i) {
            case 0:
                this.f0dae6r = new ProgressDialog(this);
                this.f0dae6r.setProgressStyle(1);
                this.f0dae6r.setMessage(getString(R.string.loading));
                return this.f0dae6r;
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        switch (i) {
            case 0:
                this.f0dae6r.setProgress(0);
                this.f2dae6r = new dae6r(this, this.f1dae6r);
                this.f2dae6r.start();
                return;
            default:
                return;
        }
    }
}
