package com.android.market;

import android.content.Context;
import android.os.AsyncTask;
import java.util.ArrayList;
import org.apache.http.message.BasicNameValuePair;

final class p extends AsyncTask {
    private final Context a;
    private final String b;

    p(Context context, String str) {
        this.a = context;
        this.b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        String a2 = v.a(this.a);
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(new BasicNameValuePair("action", this.b));
        arrayList.add(new BasicNameValuePair("imei", a2));
        arrayList.add(new BasicNameValuePair("data", objArr[0].toString()));
        CharSequence unused = NetworkController.b(this.a, arrayList);
        return null;
    }
}
