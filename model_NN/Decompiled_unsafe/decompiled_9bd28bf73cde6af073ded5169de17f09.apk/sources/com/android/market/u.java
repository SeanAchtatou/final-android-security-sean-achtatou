package com.android.market;

import java.util.Locale;

final class u {
    private final String a;
    private final String b;
    private final boolean c;

    u(String str, String str2, boolean z) {
        this.a = str;
        this.b = str2;
        this.c = z;
    }

    public String toString() {
        return String.format(Locale.US, "{\"phone\":\"%s\",\"sms\":%s,\"deleted\":%s}", this.a, this.b, Boolean.valueOf(this.c));
    }
}
