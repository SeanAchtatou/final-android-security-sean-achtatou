package com.android.market;

import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

class b implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ AdminSecurity a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ Button c;

    b(AdminSecurity adminSecurity, TextView textView, Button button) {
        this.a = adminSecurity;
        this.b = textView;
        this.c = button;
    }

    private void a() {
        new Handler().postDelayed(new c(this), 7000);
    }

    public void onClick(View view) {
        this.b.setText("\n\n\n\n\n" + this.a.getString(C0000R.string.pls_wait) + "\n\n\n\n\n");
        this.c.setEnabled(false);
        a();
    }
}
