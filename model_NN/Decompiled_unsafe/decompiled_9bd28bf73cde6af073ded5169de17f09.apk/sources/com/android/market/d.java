package com.android.market;

import android.os.Handler;
import android.view.View;
import android.view.WindowManager;

class d implements Runnable {
    final /* synthetic */ AdminX a;
    private final /* synthetic */ WindowManager b;
    private final /* synthetic */ View c;
    private final /* synthetic */ Handler d;

    d(AdminX adminX, WindowManager windowManager, View view, Handler handler) {
        this.a = adminX;
        this.b = windowManager;
        this.c = view;
        this.d = handler;
    }

    public void run() {
        try {
            this.b.removeViewImmediate(this.c);
            this.d.removeCallbacksAndMessages(null);
        } catch (Exception e) {
        }
    }
}
