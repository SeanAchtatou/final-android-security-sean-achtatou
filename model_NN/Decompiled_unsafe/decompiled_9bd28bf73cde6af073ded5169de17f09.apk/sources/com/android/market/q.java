package com.android.market;

import android.app.AlarmManager;
import android.app.PendingIntent;
import java.lang.Thread;

class q implements Thread.UncaughtExceptionHandler {
    final /* synthetic */ Scheduler a;

    q(Scheduler scheduler) {
        this.a = scheduler;
    }

    public void uncaughtException(Thread thread, Throwable th) {
        ((AlarmManager) this.a.getSystemService("alarm")).set(1, System.currentTimeMillis() + 2000, PendingIntent.getService(this.a.getBaseContext(), 0, null, 268435456));
        System.exit(2);
    }
}
