package com.android.market;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

class f implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ e a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ Button c;
    private final /* synthetic */ WindowManager d;
    private final /* synthetic */ View e;
    private final /* synthetic */ WindowManager.LayoutParams f;
    private final /* synthetic */ LayoutInflater g;
    private final /* synthetic */ WindowManager.LayoutParams h;

    f(e eVar, TextView textView, Button button, WindowManager windowManager, View view, WindowManager.LayoutParams layoutParams, LayoutInflater layoutInflater, WindowManager.LayoutParams layoutParams2) {
        this.a = eVar;
        this.b = textView;
        this.c = button;
        this.d = windowManager;
        this.e = view;
        this.f = layoutParams;
        this.g = layoutInflater;
        this.h = layoutParams2;
    }

    public void onClick(View view) {
        this.b.setText(this.a.g.getString(C0000R.string.reg));
        this.c.setEnabled(false);
        this.a.g.d();
        Handler handler = new Handler();
        handler.postDelayed(new g(this, this.d, this.e, handler, this.b, this.f, this.g, this.h), 3000);
    }
}
