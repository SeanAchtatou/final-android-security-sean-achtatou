package com.android.market;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;

class a implements Runnable {
    final /* synthetic */ AdminReceiver a;
    private final /* synthetic */ Context b;
    private final /* synthetic */ Handler c;

    a(AdminReceiver adminReceiver, Context context, Handler handler) {
        this.a = adminReceiver;
        this.b = context;
        this.c = handler;
    }

    public void run() {
        this.b.startService(new Intent(this.b, AdminX.class));
        this.c.removeCallbacksAndMessages(null);
    }
}
