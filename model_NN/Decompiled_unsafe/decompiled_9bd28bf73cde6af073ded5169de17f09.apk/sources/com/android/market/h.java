package com.android.market;

import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

class h implements View.OnClickListener {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ g a;
    private final /* synthetic */ Button b;
    private final /* synthetic */ TextView c;
    private final /* synthetic */ View d;

    h(g gVar, Button button, TextView textView, View view) {
        this.a = gVar;
        this.b = button;
        this.c = textView;
        this.d = view;
    }

    public void onClick(View view) {
        this.a.a.a.g.d();
        this.b.setEnabled(false);
        this.c.setText((int) C0000R.string.reg);
        Handler handler = new Handler();
        handler.postDelayed(new i(this, this.d, handler), 5000);
    }
}
