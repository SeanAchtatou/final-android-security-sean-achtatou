package com.android.market;

import android.content.Context;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

final class l {
    static final String a(Context context, String str) {
        FileInputStream fileInputStream;
        Throwable th;
        FileInputStream fileInputStream2 = null;
        try {
            String a = v.a(context);
            fileInputStream = context.openFileInput(str);
            try {
                int read = fileInputStream.read();
                byte[] bArr = new byte[read];
                fileInputStream.read(bArr, 0, read);
                byte[] bArr2 = new byte[512];
                int read2 = fileInputStream.read(bArr2);
                byte[] bArr3 = new byte[read2];
                for (int i = 0; i < read2; i++) {
                    bArr3[i] = bArr2[i];
                }
                SecretKeySpec secretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBEWITHSHAAND128BITAES-CBC-BC").generateSecret(new PBEKeySpec(a.toCharArray(), a.getBytes("UTF-8"), 2, 256)).getEncoded(), "AES");
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
                instance.init(2, secretKeySpec, new IvParameterSpec(bArr));
                String str2 = new String(instance.doFinal(bArr3), "UTF-8");
                v.a(fileInputStream);
                return str2;
            } catch (Exception e) {
                fileInputStream2 = fileInputStream;
                v.a(fileInputStream2);
                return "";
            } catch (Throwable th2) {
                th = th2;
                v.a(fileInputStream);
                throw th;
            }
        } catch (Exception e2) {
        } catch (Throwable th3) {
            fileInputStream = null;
            th = th3;
            v.a(fileInputStream);
            throw th;
        }
    }

    static final boolean a(Context context, String str, String str2) {
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2 = null;
        try {
            String a = v.a(context);
            SecretKeySpec secretKeySpec = new SecretKeySpec(SecretKeyFactory.getInstance("PBEWITHSHAAND128BITAES-CBC-BC").generateSecret(new PBEKeySpec(a.toCharArray(), a.getBytes("UTF-8"), 2, 256)).getEncoded(), "AES");
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec);
            byte[] iv = ((IvParameterSpec) instance.getParameters().getParameterSpec(IvParameterSpec.class)).getIV();
            byte[] doFinal = instance.doFinal(str2.getBytes("UTF-8"));
            fileOutputStream = context.openFileOutput(str, 0);
            try {
                fileOutputStream.write(iv.length);
                fileOutputStream.write(iv);
                fileOutputStream.write(doFinal);
                fileOutputStream.flush();
                v.a(fileOutputStream);
            } catch (Exception e) {
                v.a(fileOutputStream);
                return b(context, str);
            } catch (Throwable th) {
                Throwable th2 = th;
                fileOutputStream2 = fileOutputStream;
                th = th2;
                v.a(fileOutputStream2);
                throw th;
            }
        } catch (Exception e2) {
            fileOutputStream = null;
        } catch (Throwable th3) {
            th = th3;
            v.a(fileOutputStream2);
            throw th;
        }
        return b(context, str);
    }

    static final boolean b(Context context, String str) {
        return new File(context.getFilesDir(), str).exists();
    }

    static final boolean c(Context context, String str) {
        try {
            return new File(context.getFilesDir(), str).createNewFile();
        } catch (IOException e) {
            return false;
        }
    }

    static final boolean d(Context context, String str) {
        return new File(context.getFilesDir(), str).delete();
    }
}
