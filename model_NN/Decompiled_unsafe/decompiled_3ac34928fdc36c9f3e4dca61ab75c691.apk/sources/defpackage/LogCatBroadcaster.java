package defpackage;

import android.content.Context;
import android.content.Intent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* renamed from: LogCatBroadcaster  reason: default package */
public class LogCatBroadcaster implements Runnable {
    private static boolean started = false;
    private Context context;

    private LogCatBroadcaster(Context context2) {
        this.context = context2;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void start(android.content.Context r4) {
        /*
            r0 = 1
            java.lang.Class<LogCatBroadcaster> r1 = defpackage.LogCatBroadcaster.class
            monitor-enter(r1)
            boolean r2 = defpackage.LogCatBroadcaster.started     // Catch:{ all -> 0x0038 }
            if (r2 == 0) goto L_0x000a
        L_0x0008:
            monitor-exit(r1)
            return
        L_0x000a:
            r2 = 1
            defpackage.LogCatBroadcaster.started = r2     // Catch:{  }
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{  }
            r3 = 16
            if (r2 < r3) goto L_0x0008
            android.content.pm.ApplicationInfo r2 = r4.getApplicationInfo()     // Catch:{  }
            int r2 = r2.flags     // Catch:{  }
            r2 = r2 & 2
            if (r2 == 0) goto L_0x003b
        L_0x001d:
            if (r0 == 0) goto L_0x0008
            android.content.pm.PackageManager r0 = r4.getPackageManager()     // Catch:{ NameNotFoundException -> 0x003d }
            java.lang.String r2 = "com.aide.ui"
            r3 = 128(0x80, float:1.794E-43)
            r0.getPackageInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x003d }
            java.lang.Thread r0 = new java.lang.Thread     // Catch:{  }
            LogCatBroadcaster r2 = new LogCatBroadcaster     // Catch:{  }
            r2.<init>(r4)     // Catch:{  }
            r0.<init>(r2)     // Catch:{  }
            r0.start()     // Catch:{  }
            goto L_0x0008
        L_0x0038:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x003b:
            r0 = 0
            goto L_0x001d
        L_0x003d:
            r0 = move-exception
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.LogCatBroadcaster.start(android.content.Context):void");
    }

    public void run() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("logcat -v threadtime").getInputStream()), 20);
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    Intent intent = new Intent();
                    intent.setPackage("com.aide.ui");
                    intent.setAction("com.aide.runtime.VIEW_LOGCAT_ENTRY");
                    intent.putExtra("lines", new String[]{readLine});
                    this.context.sendBroadcast(intent);
                } else {
                    return;
                }
            }
        } catch (IOException e) {
        }
    }
}
