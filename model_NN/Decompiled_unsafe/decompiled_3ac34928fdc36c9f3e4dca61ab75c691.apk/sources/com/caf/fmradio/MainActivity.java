package com.caf.fmradio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

public class MainActivity extends Activity {
    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    @Override
    public void onCreate(Bundle bundle) {
        LogCatBroadcaster.start(this);
        requestWindowFeature(1);
        getWindow().addFlags(67108864);
        getWindow().setFlags(1024, 1024);
        getWindow().setFlags(1024, 1024);
        getWindow().addFlags(4194304);
        getWindow().addFlags(524288);
        super.onCreate(bundle);
        try {
            startService(new Intent(this, Class.forName("com.caf.fmradio.ClockService")));
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }

    @Override
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 3 || i == 4 || i == 82) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }
}
