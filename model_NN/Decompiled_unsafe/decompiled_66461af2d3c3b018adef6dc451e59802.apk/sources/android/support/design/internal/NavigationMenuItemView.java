package android.support.design.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.support.design.R;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewStub;
import android.widget.CheckedTextView;
import android.widget.FrameLayout;

public class NavigationMenuItemView extends ForegroundLinearLayout implements MenuView.ItemView {
    private static final int[] CHECKED_STATE_SET = {16842912};
    private FrameLayout mActionArea;
    private final int mIconSize;
    private ColorStateList mIconTintList;
    private MenuItemImpl mItemData;
    private final CheckedTextView mTextView;

    public NavigationMenuItemView(Context context) {
        this(context, null);
    }

    public NavigationMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.support.design.internal.NavigationMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public NavigationMenuItemView(android.content.Context r9, android.util.AttributeSet r10, int r11) {
        /*
            r8 = this;
            r0 = r8
            r1 = r9
            r2 = r10
            r3 = r11
            r4 = r0
            r5 = r1
            r6 = r2
            r7 = r3
            r4.<init>(r5, r6, r7)
            r4 = r0
            r5 = 0
            r4.setOrientation(r5)
            r4 = r1
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            int r5 = android.support.design.R.layout.design_navigation_menu_item
            r6 = r0
            r7 = 1
            android.view.View r4 = r4.inflate(r5, r6, r7)
            r4 = r0
            r5 = r1
            android.content.res.Resources r5 = r5.getResources()
            int r6 = android.support.design.R.dimen.design_navigation_icon_size
            int r5 = r5.getDimensionPixelSize(r6)
            r4.mIconSize = r5
            r4 = r0
            r5 = r0
            int r6 = android.support.design.R.id.design_menu_item_text
            android.view.View r5 = r5.findViewById(r6)
            android.widget.CheckedTextView r5 = (android.widget.CheckedTextView) r5
            r4.mTextView = r5
            r4 = r0
            android.widget.CheckedTextView r4 = r4.mTextView
            r5 = 1
            r4.setDuplicateParentStateEnabled(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.internal.NavigationMenuItemView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void initialize(MenuItemImpl menuItemImpl, int i) {
        MenuItemImpl menuItemImpl2 = menuItemImpl;
        this.mItemData = menuItemImpl2;
        setVisibility(menuItemImpl2.isVisible() ? 0 : 8);
        if (getBackground() == null) {
            setBackgroundDrawable(createDefaultBackground());
        }
        setCheckable(menuItemImpl2.isCheckable());
        setChecked(menuItemImpl2.isChecked());
        setEnabled(menuItemImpl2.isEnabled());
        setTitle(menuItemImpl2.getTitle());
        setIcon(menuItemImpl2.getIcon());
        setActionView(menuItemImpl2.getActionView());
    }

    public void recycle() {
        if (this.mActionArea != null) {
            this.mActionArea.removeAllViews();
        }
        this.mTextView.setCompoundDrawables(null, null, null, null);
    }

    private void setActionView(View view) {
        View view2 = view;
        if (this.mActionArea == null) {
            this.mActionArea = (FrameLayout) ((ViewStub) findViewById(R.id.design_menu_item_action_area_stub)).inflate();
        }
        this.mActionArea.removeAllViews();
        if (view2 != null) {
            this.mActionArea.addView(view2);
        }
    }

    private StateListDrawable createDefaultBackground() {
        TypedValue typedValue;
        StateListDrawable stateListDrawable;
        Drawable drawable;
        Drawable drawable2;
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        if (!getContext().getTheme().resolveAttribute(R.attr.colorControlHighlight, typedValue2, true)) {
            return null;
        }
        new StateListDrawable();
        StateListDrawable stateListDrawable2 = stateListDrawable;
        new ColorDrawable(typedValue2.data);
        stateListDrawable2.addState(CHECKED_STATE_SET, drawable);
        new ColorDrawable(0);
        stateListDrawable2.addState(EMPTY_STATE_SET, drawable2);
        return stateListDrawable2;
    }

    public MenuItemImpl getItemData() {
        return this.mItemData;
    }

    public void setTitle(CharSequence charSequence) {
        this.mTextView.setText(charSequence);
    }

    public void setCheckable(boolean z) {
        refreshDrawableState();
    }

    public void setChecked(boolean z) {
        refreshDrawableState();
        this.mTextView.setChecked(z);
    }

    public void setShortcut(boolean z, char c) {
    }

    public void setIcon(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (drawable2 != null) {
            Drawable.ConstantState constantState = drawable2.getConstantState();
            drawable2 = DrawableCompat.wrap(constantState == null ? drawable2 : constantState.newDrawable()).mutate();
            drawable2.setBounds(0, 0, this.mIconSize, this.mIconSize);
            DrawableCompat.setTintList(drawable2, this.mIconTintList);
        }
        TextViewCompat.setCompoundDrawablesRelative(this.mTextView, drawable2, null, null, null);
    }

    public boolean prefersCondensedTitle() {
        return false;
    }

    public boolean showsIcon() {
        return true;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.mItemData != null && this.mItemData.isCheckable() && this.mItemData.isChecked()) {
            int[] mergeDrawableStates = mergeDrawableStates(onCreateDrawableState, CHECKED_STATE_SET);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: package-private */
    public void setIconTintList(ColorStateList colorStateList) {
        this.mIconTintList = colorStateList;
        if (this.mItemData != null) {
            setIcon(this.mItemData.getIcon());
        }
    }

    public void setTextAppearance(Context context, int i) {
        this.mTextView.setTextAppearance(context, i);
    }

    public void setTextColor(ColorStateList colorStateList) {
        this.mTextView.setTextColor(colorStateList);
    }
}
