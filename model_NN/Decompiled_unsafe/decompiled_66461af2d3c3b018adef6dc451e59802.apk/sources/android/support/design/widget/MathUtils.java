package android.support.design.widget;

class MathUtils {
    MathUtils() {
    }

    static int constrain(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        return i4 < i5 ? i5 : i4 > i6 ? i6 : i4;
    }

    static float constrain(float f, float f2, float f3) {
        float f4 = f;
        float f5 = f2;
        float f6 = f3;
        return f4 < f5 ? f5 : f4 > f6 ? f6 : f4;
    }
}
