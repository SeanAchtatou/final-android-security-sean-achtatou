package android.support.design.widget;

import android.util.StateSet;
import android.view.View;
import android.view.animation.Animation;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

final class StateListAnimator {
    private Animation.AnimationListener mAnimationListener;
    private Tuple mLastMatch = null;
    /* access modifiers changed from: private */
    public Animation mRunningAnimation = null;
    private final ArrayList<Tuple> mTuples;
    private WeakReference<View> mViewRef;

    StateListAnimator() {
        ArrayList<Tuple> arrayList;
        Animation.AnimationListener animationListener;
        new ArrayList<>();
        this.mTuples = arrayList;
        new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (StateListAnimator.this.mRunningAnimation == animation) {
                    Animation access$002 = StateListAnimator.access$002(StateListAnimator.this, null);
                }
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };
        this.mAnimationListener = animationListener;
    }

    static /* synthetic */ Animation access$002(StateListAnimator stateListAnimator, Animation animation) {
        Animation animation2 = animation;
        Animation animation3 = animation2;
        stateListAnimator.mRunningAnimation = animation3;
        return animation2;
    }

    public void addState(int[] iArr, Animation animation) {
        Object obj;
        Animation animation2 = animation;
        new Tuple(iArr, animation2);
        animation2.setAnimationListener(this.mAnimationListener);
        boolean add = this.mTuples.add(obj);
    }

    /* access modifiers changed from: package-private */
    public Animation getRunningAnimation() {
        return this.mRunningAnimation;
    }

    /* access modifiers changed from: package-private */
    public View getTarget() {
        return this.mViewRef == null ? null : this.mViewRef.get();
    }

    /* access modifiers changed from: package-private */
    public void setTarget(View view) {
        WeakReference<View> weakReference;
        View view2 = view;
        View target = getTarget();
        if (target != view2) {
            if (target != null) {
                clearTarget();
            }
            if (view2 != null) {
                new WeakReference<>(view2);
                this.mViewRef = weakReference;
            }
        }
    }

    private void clearTarget() {
        View target = getTarget();
        int size = this.mTuples.size();
        for (int i = 0; i < size; i++) {
            if (target.getAnimation() == this.mTuples.get(i).mAnimation) {
                target.clearAnimation();
            }
        }
        this.mViewRef = null;
        this.mLastMatch = null;
        this.mRunningAnimation = null;
    }

    /* access modifiers changed from: package-private */
    public void setState(int[] iArr) {
        int[] iArr2 = iArr;
        Tuple tuple = null;
        int size = this.mTuples.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            Tuple tuple2 = this.mTuples.get(i);
            if (StateSet.stateSetMatches(tuple2.mSpecs, iArr2)) {
                tuple = tuple2;
                break;
            }
            i++;
        }
        if (tuple != this.mLastMatch) {
            if (this.mLastMatch != null) {
                cancel();
            }
            this.mLastMatch = tuple;
            View view = this.mViewRef.get();
            if (tuple != null && view != null && view.getVisibility() == 0) {
                start(tuple);
            }
        }
    }

    private void start(Tuple tuple) {
        this.mRunningAnimation = tuple.mAnimation;
        View target = getTarget();
        if (target != null) {
            target.startAnimation(this.mRunningAnimation);
        }
    }

    private void cancel() {
        if (this.mRunningAnimation != null) {
            View target = getTarget();
            if (target != null && target.getAnimation() == this.mRunningAnimation) {
                target.clearAnimation();
            }
            this.mRunningAnimation = null;
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<Tuple> getTuples() {
        return this.mTuples;
    }

    public void jumpToCurrentState() {
        View target;
        if (this.mRunningAnimation != null && (target = getTarget()) != null && target.getAnimation() == this.mRunningAnimation) {
            target.clearAnimation();
        }
    }

    static class Tuple {
        final Animation mAnimation;
        final int[] mSpecs;

        private Tuple(int[] iArr, Animation animation) {
            this.mSpecs = iArr;
            this.mAnimation = animation;
        }

        /* access modifiers changed from: package-private */
        public int[] getSpecs() {
            return this.mSpecs;
        }

        /* access modifiers changed from: package-private */
        public Animation getAnimation() {
            return this.mAnimation;
        }
    }
}
