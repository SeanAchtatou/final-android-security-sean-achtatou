package android.support.design.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.NestedScrollingParent;
import android.support.v4.view.NestedScrollingParentHelper;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.WindowInsetsCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoordinatorLayout extends ViewGroup implements NestedScrollingParent {
    static final Class<?>[] CONSTRUCTOR_PARAMS;
    static final CoordinatorLayoutInsetsHelper INSETS_HELPER;
    static final String TAG = "CoordinatorLayout";
    static final Comparator<View> TOP_SORTED_CHILDREN_COMPARATOR;
    private static final int TYPE_ON_INTERCEPT = 0;
    private static final int TYPE_ON_TOUCH = 1;
    static final String WIDGET_PACKAGE_NAME;
    static final ThreadLocal<Map<String, Constructor<Behavior>>> sConstructors;
    private View mBehaviorTouchView;
    private final List<View> mDependencySortedChildren;
    private boolean mDrawStatusBarBackground;
    private boolean mIsAttachedToWindow;
    private int[] mKeylines;
    private WindowInsetsCompat mLastInsets;
    final Comparator<View> mLayoutDependencyComparator;
    private boolean mNeedsPreDrawListener;
    private View mNestedScrollingDirectChild;
    private final NestedScrollingParentHelper mNestedScrollingParentHelper;
    private View mNestedScrollingTarget;
    /* access modifiers changed from: private */
    public ViewGroup.OnHierarchyChangeListener mOnHierarchyChangeListener;
    private OnPreDrawListener mOnPreDrawListener;
    private Paint mScrimPaint;
    private Drawable mStatusBarBackground;
    private final List<View> mTempDependenciesList;
    private final int[] mTempIntPair;
    private final List<View> mTempList1;
    private final Rect mTempRect1;
    private final Rect mTempRect2;
    private final Rect mTempRect3;

    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefaultBehavior {
        Class<? extends Behavior> value();
    }

    static {
        ThreadLocal<Map<String, Constructor<Behavior>>> threadLocal;
        Comparator<View> comparator;
        CoordinatorLayoutInsetsHelper coordinatorLayoutInsetsHelper;
        Package packageR = CoordinatorLayout.class.getPackage();
        WIDGET_PACKAGE_NAME = packageR != null ? packageR.getName() : null;
        if (Build.VERSION.SDK_INT >= 21) {
            new ViewElevationComparator();
            TOP_SORTED_CHILDREN_COMPARATOR = comparator;
            new CoordinatorLayoutInsetsHelperLollipop();
            INSETS_HELPER = coordinatorLayoutInsetsHelper;
        } else {
            TOP_SORTED_CHILDREN_COMPARATOR = null;
            INSETS_HELPER = null;
        }
        Class<?>[] clsArr = new Class[2];
        clsArr[0] = Context.class;
        Class<?>[] clsArr2 = clsArr;
        clsArr2[1] = AttributeSet.class;
        CONSTRUCTOR_PARAMS = clsArr2;
        new ThreadLocal<>();
        sConstructors = threadLocal;
    }

    public CoordinatorLayout(Context context) {
        this(context, null);
    }

    public CoordinatorLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CoordinatorLayout(android.content.Context r18, android.util.AttributeSet r19, int r20) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            r3 = r20
            r10 = r0
            r11 = r1
            r12 = r2
            r13 = r3
            r10.<init>(r11, r12, r13)
            r10 = r0
            android.support.design.widget.CoordinatorLayout$1 r11 = new android.support.design.widget.CoordinatorLayout$1
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r12.<init>()
            r10.mLayoutDependencyComparator = r11
            r10 = r0
            java.util.ArrayList r11 = new java.util.ArrayList
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mDependencySortedChildren = r11
            r10 = r0
            java.util.ArrayList r11 = new java.util.ArrayList
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mTempList1 = r11
            r10 = r0
            java.util.ArrayList r11 = new java.util.ArrayList
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mTempDependenciesList = r11
            r10 = r0
            android.graphics.Rect r11 = new android.graphics.Rect
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mTempRect1 = r11
            r10 = r0
            android.graphics.Rect r11 = new android.graphics.Rect
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mTempRect2 = r11
            r10 = r0
            android.graphics.Rect r11 = new android.graphics.Rect
            r15 = r11
            r11 = r15
            r12 = r15
            r12.<init>()
            r10.mTempRect3 = r11
            r10 = r0
            r11 = 2
            int[] r11 = new int[r11]
            r10.mTempIntPair = r11
            r10 = r0
            android.support.v4.view.NestedScrollingParentHelper r11 = new android.support.v4.view.NestedScrollingParentHelper
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r12.<init>(r13)
            r10.mNestedScrollingParentHelper = r11
            r10 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r10)
            r10 = r1
            r11 = r2
            int[] r12 = android.support.design.R.styleable.CoordinatorLayout
            r13 = r3
            int r14 = android.support.design.R.style.Widget_Design_CoordinatorLayout
            android.content.res.TypedArray r10 = r10.obtainStyledAttributes(r11, r12, r13, r14)
            r4 = r10
            r10 = r4
            int r11 = android.support.design.R.styleable.CoordinatorLayout_keylines
            r12 = 0
            int r10 = r10.getResourceId(r11, r12)
            r5 = r10
            r10 = r5
            if (r10 == 0) goto L_0x00c5
            r10 = r1
            android.content.res.Resources r10 = r10.getResources()
            r6 = r10
            r10 = r0
            r11 = r6
            r12 = r5
            int[] r11 = r11.getIntArray(r12)
            r10.mKeylines = r11
            r10 = r6
            android.util.DisplayMetrics r10 = r10.getDisplayMetrics()
            float r10 = r10.density
            r7 = r10
            r10 = r0
            int[] r10 = r10.mKeylines
            int r10 = r10.length
            r8 = r10
            r10 = 0
            r9 = r10
        L_0x00a9:
            r10 = r9
            r11 = r8
            if (r10 >= r11) goto L_0x00c5
            r10 = r0
            int[] r10 = r10.mKeylines
            r11 = r9
            r15 = r10
            r16 = r11
            r10 = r15
            r11 = r16
            r12 = r15
            r13 = r16
            r12 = r12[r13]
            float r12 = (float) r12
            r13 = r7
            float r12 = r12 * r13
            int r12 = (int) r12
            r10[r11] = r12
            int r9 = r9 + 1
            goto L_0x00a9
        L_0x00c5:
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.CoordinatorLayout_statusBarBackground
            android.graphics.drawable.Drawable r11 = r11.getDrawable(r12)
            r10.mStatusBarBackground = r11
            r10 = r4
            r10.recycle()
            android.support.design.widget.CoordinatorLayoutInsetsHelper r10 = android.support.design.widget.CoordinatorLayout.INSETS_HELPER
            if (r10 == 0) goto L_0x00e6
            android.support.design.widget.CoordinatorLayoutInsetsHelper r10 = android.support.design.widget.CoordinatorLayout.INSETS_HELPER
            r11 = r0
            android.support.design.widget.CoordinatorLayout$ApplyInsetsListener r12 = new android.support.design.widget.CoordinatorLayout$ApplyInsetsListener
            r15 = r12
            r12 = r15
            r13 = r15
            r14 = r0
            r13.<init>()
            r10.setupForWindowInsets(r11, r12)
        L_0x00e6:
            r10 = r0
            android.support.design.widget.CoordinatorLayout$HierarchyChangeListener r11 = new android.support.design.widget.CoordinatorLayout$HierarchyChangeListener
            r15 = r11
            r11 = r15
            r12 = r15
            r13 = r0
            r12.<init>()
            super.setOnHierarchyChangeListener(r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setOnHierarchyChangeListener(ViewGroup.OnHierarchyChangeListener onHierarchyChangeListener) {
        this.mOnHierarchyChangeListener = onHierarchyChangeListener;
    }

    public void onAttachedToWindow() {
        OnPreDrawListener onPreDrawListener;
        super.onAttachedToWindow();
        resetTouchBehaviors();
        if (this.mNeedsPreDrawListener) {
            if (this.mOnPreDrawListener == null) {
                new OnPreDrawListener();
                this.mOnPreDrawListener = onPreDrawListener;
            }
            getViewTreeObserver().addOnPreDrawListener(this.mOnPreDrawListener);
        }
        if (this.mLastInsets == null && ViewCompat.getFitsSystemWindows(this)) {
            ViewCompat.requestApplyInsets(this);
        }
        this.mIsAttachedToWindow = true;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        resetTouchBehaviors();
        if (this.mNeedsPreDrawListener && this.mOnPreDrawListener != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.mOnPreDrawListener);
        }
        if (this.mNestedScrollingTarget != null) {
            onStopNestedScroll(this.mNestedScrollingTarget);
        }
        this.mIsAttachedToWindow = false;
    }

    public void setStatusBarBackground(Drawable drawable) {
        this.mStatusBarBackground = drawable;
        invalidate();
    }

    public Drawable getStatusBarBackground() {
        return this.mStatusBarBackground;
    }

    public void setStatusBarBackgroundResource(int i) {
        int i2 = i;
        setStatusBarBackground(i2 != 0 ? ContextCompat.getDrawable(getContext(), i2) : null);
    }

    public void setStatusBarBackgroundColor(int i) {
        Drawable drawable;
        new ColorDrawable(i);
        setStatusBarBackground(drawable);
    }

    /* access modifiers changed from: private */
    public void setWindowInsets(WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
        if (this.mLastInsets != windowInsetsCompat2) {
            this.mLastInsets = windowInsetsCompat2;
            this.mDrawStatusBarBackground = windowInsetsCompat2 != null && windowInsetsCompat2.getSystemWindowInsetTop() > 0;
            setWillNotDraw(!this.mDrawStatusBarBackground && getBackground() == null);
            dispatchChildApplyWindowInsets(windowInsetsCompat2);
            requestLayout();
        }
    }

    private void resetTouchBehaviors() {
        if (this.mBehaviorTouchView != null) {
            Behavior behavior = ((LayoutParams) this.mBehaviorTouchView.getLayoutParams()).getBehavior();
            if (behavior != null) {
                long uptimeMillis = SystemClock.uptimeMillis();
                MotionEvent obtain = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                boolean onTouchEvent = behavior.onTouchEvent(this, this.mBehaviorTouchView, obtain);
                obtain.recycle();
            }
            this.mBehaviorTouchView = null;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            ((LayoutParams) getChildAt(i).getLayoutParams()).resetTouchBehaviorTracking();
        }
    }

    private void getTopSortedChildren(List<View> list) {
        List<View> list2 = list;
        list2.clear();
        boolean isChildrenDrawingOrderEnabled = isChildrenDrawingOrderEnabled();
        int childCount = getChildCount();
        for (int i = childCount - 1; i >= 0; i--) {
            boolean add = list2.add(getChildAt(isChildrenDrawingOrderEnabled ? getChildDrawingOrder(childCount, i) : i));
        }
        if (TOP_SORTED_CHILDREN_COMPARATOR != null) {
            Collections.sort(list2, TOP_SORTED_CHILDREN_COMPARATOR);
        }
    }

    private boolean performIntercept(MotionEvent motionEvent, int i) {
        MotionEvent motionEvent2 = motionEvent;
        int i2 = i;
        boolean z = false;
        boolean z2 = false;
        MotionEvent motionEvent3 = null;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        List<View> list = this.mTempList1;
        getTopSortedChildren(list);
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            View view = list.get(i3);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            Behavior behavior = layoutParams.getBehavior();
            if ((!z && !z2) || actionMasked == 0) {
                if (!z && behavior != null) {
                    switch (i2) {
                        case 0:
                            z = behavior.onInterceptTouchEvent(this, view, motionEvent2);
                            break;
                        case 1:
                            z = behavior.onTouchEvent(this, view, motionEvent2);
                            break;
                    }
                    if (z) {
                        this.mBehaviorTouchView = view;
                    }
                }
                boolean didBlockInteraction = layoutParams.didBlockInteraction();
                boolean isBlockingInteractionBelow = layoutParams.isBlockingInteractionBelow(this, view);
                z2 = isBlockingInteractionBelow && !didBlockInteraction;
                if (isBlockingInteractionBelow && !z2) {
                    list.clear();
                    return z;
                }
            } else if (behavior != null) {
                if (motionEvent3 == null) {
                    long uptimeMillis = SystemClock.uptimeMillis();
                    motionEvent3 = MotionEvent.obtain(uptimeMillis, uptimeMillis, 3, 0.0f, 0.0f, 0);
                }
                switch (i2) {
                    case 0:
                        boolean onInterceptTouchEvent = behavior.onInterceptTouchEvent(this, view, motionEvent3);
                        continue;
                    case 1:
                        boolean onTouchEvent = behavior.onTouchEvent(this, view, motionEvent3);
                        continue;
                }
            }
        }
        list.clear();
        return z;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        if (actionMasked == 0) {
            resetTouchBehaviors();
        }
        boolean performIntercept = performIntercept(motionEvent2, 0);
        if (0 != 0) {
            MotionEvent motionEvent3 = null;
            motionEvent3.recycle();
        }
        if (actionMasked == 1 || actionMasked == 3) {
            resetTouchBehaviors();
        }
        return performIntercept;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0023, code lost:
        if (r16 != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r18) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r8 = 0
            r2 = r8
            r8 = 0
            r3 = r8
            r8 = 0
            r4 = r8
            r8 = r1
            int r8 = android.support.v4.view.MotionEventCompat.getActionMasked(r8)
            r5 = r8
            r8 = r0
            android.view.View r8 = r8.mBehaviorTouchView
            if (r8 != 0) goto L_0x0025
            r8 = r0
            r9 = r1
            r10 = 1
            boolean r8 = r8.performIntercept(r9, r10)
            r16 = r8
            r8 = r16
            r9 = r16
            r3 = r9
            if (r8 == 0) goto L_0x0043
        L_0x0025:
            r8 = r0
            android.view.View r8 = r8.mBehaviorTouchView
            android.view.ViewGroup$LayoutParams r8 = r8.getLayoutParams()
            android.support.design.widget.CoordinatorLayout$LayoutParams r8 = (android.support.design.widget.CoordinatorLayout.LayoutParams) r8
            r6 = r8
            r8 = r6
            android.support.design.widget.CoordinatorLayout$Behavior r8 = r8.getBehavior()
            r7 = r8
            r8 = r7
            if (r8 == 0) goto L_0x0043
            r8 = r7
            r9 = r0
            r10 = r0
            android.view.View r10 = r10.mBehaviorTouchView
            r11 = r1
            boolean r8 = r8.onTouchEvent(r9, r10, r11)
            r2 = r8
        L_0x0043:
            r8 = r0
            android.view.View r8 = r8.mBehaviorTouchView
            if (r8 != 0) goto L_0x006d
            r8 = r2
            r9 = r0
            r10 = r1
            boolean r9 = super.onTouchEvent(r10)
            r8 = r8 | r9
            r2 = r8
        L_0x0051:
            r8 = r2
            if (r8 != 0) goto L_0x0057
            r8 = r5
            if (r8 != 0) goto L_0x0057
        L_0x0057:
            r8 = r4
            if (r8 == 0) goto L_0x005e
            r8 = r4
            r8.recycle()
        L_0x005e:
            r8 = r5
            r9 = 1
            if (r8 == r9) goto L_0x0066
            r8 = r5
            r9 = 3
            if (r8 != r9) goto L_0x006a
        L_0x0066:
            r8 = r0
            r8.resetTouchBehaviors()
        L_0x006a:
            r8 = r2
            r0 = r8
            return r0
        L_0x006d:
            r8 = r3
            if (r8 == 0) goto L_0x0051
            r8 = r4
            if (r8 != 0) goto L_0x0083
            long r8 = android.os.SystemClock.uptimeMillis()
            r6 = r8
            r8 = r6
            r10 = r6
            r12 = 3
            r13 = 0
            r14 = 0
            r15 = 0
            android.view.MotionEvent r8 = android.view.MotionEvent.obtain(r8, r10, r12, r13, r14, r15)
            r4 = r8
        L_0x0083:
            r8 = r0
            r9 = r4
            boolean r8 = super.onTouchEvent(r9)
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void requestDisallowInterceptTouchEvent(boolean z) {
        boolean z2 = z;
        super.requestDisallowInterceptTouchEvent(z2);
        if (z2) {
            resetTouchBehaviors();
        }
    }

    private int getKeyline(int i) {
        StringBuilder sb;
        StringBuilder sb2;
        int i2 = i;
        if (this.mKeylines == null) {
            new StringBuilder();
            int e = Log.e(TAG, sb2.append("No keylines defined for ").append(this).append(" - attempted index lookup ").append(i2).toString());
            return 0;
        } else if (i2 >= 0 && i2 < this.mKeylines.length) {
            return this.mKeylines[i2];
        } else {
            new StringBuilder();
            int e2 = Log.e(TAG, sb.append("Keyline index ").append(i2).append(" out of range for ").append(this).toString());
            return 0;
        }
    }

    static Behavior parseBehavior(Context context, AttributeSet attributeSet, String str) {
        String str2;
        String str3;
        StringBuilder sb;
        Throwable th;
        StringBuilder sb2;
        Map map;
        StringBuilder sb3;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        String str4 = str;
        if (TextUtils.isEmpty(str4)) {
            return null;
        }
        if (str4.startsWith(".")) {
            new StringBuilder();
            str3 = sb3.append(context2.getPackageName()).append(str4).toString();
        } else if (str4.indexOf(46) >= 0) {
            str3 = str4;
        } else {
            if (!TextUtils.isEmpty(WIDGET_PACKAGE_NAME)) {
                new StringBuilder();
                str2 = sb.append(WIDGET_PACKAGE_NAME).append('.').append(str4).toString();
            } else {
                str2 = str4;
            }
            str3 = str2;
        }
        try {
            Map map2 = sConstructors.get();
            if (map2 == null) {
                new HashMap();
                map2 = map;
                sConstructors.set(map2);
            }
            Constructor<?> constructor = (Constructor) map2.get(str3);
            if (constructor == null) {
                constructor = Class.forName(str3, true, context2.getClassLoader()).getConstructor(CONSTRUCTOR_PARAMS);
                constructor.setAccessible(true);
                Object put = map2.put(str3, constructor);
            }
            Object[] objArr = new Object[2];
            objArr[0] = context2;
            Object[] objArr2 = objArr;
            objArr2[1] = attributeSet2;
            return (Behavior) constructor.newInstance(objArr2);
        } catch (Exception e) {
            Exception exc = e;
            Throwable th2 = th;
            new StringBuilder();
            new RuntimeException(sb2.append("Could not inflate Behavior subclass ").append(str3).toString(), exc);
            throw th2;
        }
    }

    /* access modifiers changed from: package-private */
    public LayoutParams getResolvedLayoutParams(View view) {
        StringBuilder sb;
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (!layoutParams.mBehaviorResolved) {
            DefaultBehavior defaultBehavior = null;
            for (Class<? super Object> cls = view2.getClass(); cls != null; cls = cls.getSuperclass()) {
                DefaultBehavior defaultBehavior2 = (DefaultBehavior) cls.getAnnotation(DefaultBehavior.class);
                defaultBehavior = defaultBehavior2;
                if (defaultBehavior2 != null) {
                    break;
                }
            }
            if (defaultBehavior != null) {
                try {
                    layoutParams.setBehavior((Behavior) defaultBehavior.value().newInstance());
                } catch (Exception e) {
                    new StringBuilder();
                    int e2 = Log.e(TAG, sb.append("Default behavior class ").append(defaultBehavior.value().getName()).append(" could not be instantiated. Did you forget a default constructor?").toString(), e);
                }
            }
            layoutParams.mBehaviorResolved = true;
        }
        return layoutParams;
    }

    private void prepareChildren() {
        this.mDependencySortedChildren.clear();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            View findAnchorView = getResolvedLayoutParams(childAt).findAnchorView(this, childAt);
            boolean add = this.mDependencySortedChildren.add(childAt);
        }
        selectionSort(this.mDependencySortedChildren, this.mLayoutDependencyComparator);
    }

    /* access modifiers changed from: package-private */
    public void getDescendantRect(View view, Rect rect) {
        ViewGroupUtils.getDescendantRect(this, view, rect);
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumWidth() {
        return Math.max(super.getSuggestedMinimumWidth(), getPaddingLeft() + getPaddingRight());
    }

    /* access modifiers changed from: protected */
    public int getSuggestedMinimumHeight() {
        return Math.max(super.getSuggestedMinimumHeight(), getPaddingTop() + getPaddingBottom());
    }

    public void onMeasureChild(View view, int i, int i2, int i3, int i4) {
        measureChildWithMargins(view, i, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        prepareChildren();
        ensurePreDrawListener();
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        boolean z = layoutDirection == 1;
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        int mode2 = View.MeasureSpec.getMode(i4);
        int size2 = View.MeasureSpec.getSize(i4);
        int i5 = paddingLeft + paddingRight;
        int i6 = paddingTop + paddingBottom;
        int suggestedMinimumWidth = getSuggestedMinimumWidth();
        int suggestedMinimumHeight = getSuggestedMinimumHeight();
        int i7 = 0;
        boolean z2 = this.mLastInsets != null && ViewCompat.getFitsSystemWindows(this);
        int size3 = this.mDependencySortedChildren.size();
        for (int i8 = 0; i8 < size3; i8++) {
            View view = this.mDependencySortedChildren.get(i8);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            int i9 = 0;
            if (layoutParams.keyline >= 0 && mode != 0) {
                int keyline = getKeyline(layoutParams.keyline);
                int absoluteGravity = GravityCompat.getAbsoluteGravity(resolveKeylineGravity(layoutParams.gravity), layoutDirection) & 7;
                if ((absoluteGravity == 3 && !z) || (absoluteGravity == 5 && z)) {
                    i9 = Math.max(0, (size - paddingRight) - keyline);
                } else if ((absoluteGravity == 5 && !z) || (absoluteGravity == 3 && z)) {
                    i9 = Math.max(0, keyline - paddingLeft);
                }
            }
            int i10 = i3;
            int i11 = i4;
            if (z2 && !ViewCompat.getFitsSystemWindows(view)) {
                int systemWindowInsetLeft = this.mLastInsets.getSystemWindowInsetLeft() + this.mLastInsets.getSystemWindowInsetRight();
                int systemWindowInsetTop = this.mLastInsets.getSystemWindowInsetTop() + this.mLastInsets.getSystemWindowInsetBottom();
                i10 = View.MeasureSpec.makeMeasureSpec(size - systemWindowInsetLeft, mode);
                i11 = View.MeasureSpec.makeMeasureSpec(size2 - systemWindowInsetTop, mode2);
            }
            Behavior behavior = layoutParams.getBehavior();
            if (behavior == null || !behavior.onMeasureChild(this, view, i10, i9, i11, 0)) {
                onMeasureChild(view, i10, i9, i11, 0);
            }
            suggestedMinimumWidth = Math.max(suggestedMinimumWidth, i5 + view.getMeasuredWidth() + layoutParams.leftMargin + layoutParams.rightMargin);
            suggestedMinimumHeight = Math.max(suggestedMinimumHeight, i6 + view.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin);
            i7 = ViewCompat.combineMeasuredStates(i7, ViewCompat.getMeasuredState(view));
        }
        setMeasuredDimension(ViewCompat.resolveSizeAndState(suggestedMinimumWidth, i3, i7 & ViewCompat.MEASURED_STATE_MASK), ViewCompat.resolveSizeAndState(suggestedMinimumHeight, i4, i7 << 16));
    }

    private void dispatchChildApplyWindowInsets(WindowInsetsCompat windowInsetsCompat) {
        WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
        if (!windowInsetsCompat2.isConsumed()) {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = getChildAt(i);
                if (ViewCompat.getFitsSystemWindows(childAt)) {
                    Behavior behavior = ((LayoutParams) childAt.getLayoutParams()).getBehavior();
                    if (behavior != null) {
                        windowInsetsCompat2 = behavior.onApplyWindowInsets(this, childAt, windowInsetsCompat2);
                        if (windowInsetsCompat2.isConsumed()) {
                            return;
                        }
                    }
                    windowInsetsCompat2 = ViewCompat.dispatchApplyWindowInsets(childAt, windowInsetsCompat2);
                    if (windowInsetsCompat2.isConsumed()) {
                        return;
                    }
                }
            }
        }
    }

    public void onLayoutChild(View view, int i) {
        Throwable th;
        View view2 = view;
        int i2 = i;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (layoutParams.checkAnchorChanged()) {
            Throwable th2 = th;
            new IllegalStateException("An anchor may not be changed after CoordinatorLayout measurement begins before layout is complete.");
            throw th2;
        } else if (layoutParams.mAnchorView != null) {
            layoutChildWithAnchor(view2, layoutParams.mAnchorView, i2);
        } else if (layoutParams.keyline >= 0) {
            layoutChildWithKeyline(view2, layoutParams.keyline, i2);
        } else {
            layoutChild(view2, i2);
        }
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int size = this.mDependencySortedChildren.size();
        for (int i5 = 0; i5 < size; i5++) {
            View view = this.mDependencySortedChildren.get(i5);
            Behavior behavior = ((LayoutParams) view.getLayoutParams()).getBehavior();
            if (behavior == null || !behavior.onLayoutChild(this, view, layoutDirection)) {
                onLayoutChild(view, layoutDirection);
            }
        }
    }

    public void onDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.onDraw(canvas2);
        if (this.mDrawStatusBarBackground && this.mStatusBarBackground != null) {
            int systemWindowInsetTop = this.mLastInsets != null ? this.mLastInsets.getSystemWindowInsetTop() : 0;
            if (systemWindowInsetTop > 0) {
                this.mStatusBarBackground.setBounds(0, 0, getWidth(), systemWindowInsetTop);
                this.mStatusBarBackground.draw(canvas2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void recordLastChildRect(View view, Rect rect) {
        ((LayoutParams) view.getLayoutParams()).setLastChildRect(rect);
    }

    /* access modifiers changed from: package-private */
    public void getLastChildRect(View view, Rect rect) {
        rect.set(((LayoutParams) view.getLayoutParams()).getLastChildRect());
    }

    /* access modifiers changed from: package-private */
    public void getChildRect(View view, boolean z, Rect rect) {
        View view2 = view;
        boolean z2 = z;
        Rect rect2 = rect;
        if (view2.isLayoutRequested() || view2.getVisibility() == 8) {
            rect2.set(0, 0, 0, 0);
        } else if (z2) {
            getDescendantRect(view2, rect2);
        } else {
            rect2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
        }
    }

    /* access modifiers changed from: package-private */
    public void getDesiredAnchoredChildRect(View view, int i, Rect rect, Rect rect2) {
        int width;
        int height;
        View view2 = view;
        int i2 = i;
        Rect rect3 = rect;
        Rect rect4 = rect2;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(resolveAnchoredChildGravity(layoutParams.gravity), i2);
        int absoluteGravity2 = GravityCompat.getAbsoluteGravity(resolveGravity(layoutParams.anchorGravity), i2);
        int i3 = absoluteGravity & 7;
        int i4 = absoluteGravity & 112;
        int i5 = absoluteGravity2 & 7;
        int i6 = absoluteGravity2 & 112;
        int measuredWidth = view2.getMeasuredWidth();
        int measuredHeight = view2.getMeasuredHeight();
        switch (i5) {
            case 1:
                width = rect3.left + (rect3.width() / 2);
                break;
            case 5:
                width = rect3.right;
                break;
            default:
                width = rect3.left;
                break;
        }
        switch (i6) {
            case 16:
                height = rect3.top + (rect3.height() / 2);
                break;
            case 80:
                height = rect3.bottom;
                break;
            default:
                height = rect3.top;
                break;
        }
        switch (i3) {
            case 1:
                width -= measuredWidth / 2;
                break;
            case 5:
                break;
            default:
                width -= measuredWidth;
                break;
        }
        switch (i4) {
            case 16:
                height -= measuredHeight / 2;
                break;
            case 80:
                break;
            default:
                height -= measuredHeight;
                break;
        }
        int width2 = getWidth();
        int height2 = getHeight();
        int max = Math.max(getPaddingLeft() + layoutParams.leftMargin, Math.min(width, ((width2 - getPaddingRight()) - measuredWidth) - layoutParams.rightMargin));
        int max2 = Math.max(getPaddingTop() + layoutParams.topMargin, Math.min(height, ((height2 - getPaddingBottom()) - measuredHeight) - layoutParams.bottomMargin));
        rect4.set(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    private void layoutChildWithAnchor(View view, View view2, int i) {
        View view3 = view;
        LayoutParams layoutParams = (LayoutParams) view3.getLayoutParams();
        Rect rect = this.mTempRect1;
        Rect rect2 = this.mTempRect2;
        getDescendantRect(view2, rect);
        getDesiredAnchoredChildRect(view3, i, rect, rect2);
        view3.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }

    private void layoutChildWithKeyline(View view, int i, int i2) {
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        int absoluteGravity = GravityCompat.getAbsoluteGravity(resolveKeylineGravity(layoutParams.gravity), i4);
        int i5 = absoluteGravity & 7;
        int i6 = absoluteGravity & 112;
        int width = getWidth();
        int height = getHeight();
        int measuredWidth = view2.getMeasuredWidth();
        int measuredHeight = view2.getMeasuredHeight();
        if (i4 == 1) {
            i3 = width - i3;
        }
        int keyline = getKeyline(i3) - measuredWidth;
        int i7 = 0;
        switch (i5) {
            case 1:
                keyline += measuredWidth / 2;
                break;
            case 5:
                keyline += measuredWidth;
                break;
        }
        switch (i6) {
            case 16:
                i7 = 0 + (measuredHeight / 2);
                break;
            case 80:
                i7 = 0 + measuredHeight;
                break;
        }
        int max = Math.max(getPaddingLeft() + layoutParams.leftMargin, Math.min(keyline, ((width - getPaddingRight()) - measuredWidth) - layoutParams.rightMargin));
        int max2 = Math.max(getPaddingTop() + layoutParams.topMargin, Math.min(i7, ((height - getPaddingBottom()) - measuredHeight) - layoutParams.bottomMargin));
        view2.layout(max, max2, max + measuredWidth, max2 + measuredHeight);
    }

    private void layoutChild(View view, int i) {
        View view2 = view;
        int i2 = i;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        Rect rect = this.mTempRect1;
        rect.set(getPaddingLeft() + layoutParams.leftMargin, getPaddingTop() + layoutParams.topMargin, (getWidth() - getPaddingRight()) - layoutParams.rightMargin, (getHeight() - getPaddingBottom()) - layoutParams.bottomMargin);
        if (this.mLastInsets != null && ViewCompat.getFitsSystemWindows(this) && !ViewCompat.getFitsSystemWindows(view2)) {
            rect.left += this.mLastInsets.getSystemWindowInsetLeft();
            rect.top += this.mLastInsets.getSystemWindowInsetTop();
            rect.right -= this.mLastInsets.getSystemWindowInsetRight();
            rect.bottom -= this.mLastInsets.getSystemWindowInsetBottom();
        }
        Rect rect2 = this.mTempRect2;
        GravityCompat.apply(resolveGravity(layoutParams.gravity), view2.getMeasuredWidth(), view2.getMeasuredHeight(), rect, rect2, i2);
        view2.layout(rect2.left, rect2.top, rect2.right, rect2.bottom);
    }

    private static int resolveGravity(int i) {
        int i2 = i;
        return i2 == 0 ? 8388659 : i2;
    }

    private static int resolveKeylineGravity(int i) {
        int i2 = i;
        return i2 == 0 ? 8388661 : i2;
    }

    private static int resolveAnchoredChildGravity(int i) {
        int i2 = i;
        return i2 == 0 ? 17 : i2;
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j) {
        Paint paint;
        Canvas canvas2 = canvas;
        View view2 = view;
        long j2 = j;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (layoutParams.mBehavior != null && layoutParams.mBehavior.getScrimOpacity(this, view2) > 0.0f) {
            if (this.mScrimPaint == null) {
                new Paint();
                this.mScrimPaint = paint;
            }
            this.mScrimPaint.setColor(layoutParams.mBehavior.getScrimColor(this, view2));
            canvas2.drawRect((float) getPaddingLeft(), (float) getPaddingTop(), (float) (getWidth() - getPaddingRight()), (float) (getHeight() - getPaddingBottom()), this.mScrimPaint);
        }
        return super.drawChild(canvas2, view2, j2);
    }

    /* access modifiers changed from: package-private */
    public void dispatchOnDependentViewChanged(boolean z) {
        boolean z2 = z;
        int layoutDirection = ViewCompat.getLayoutDirection(this);
        int size = this.mDependencySortedChildren.size();
        for (int i = 0; i < size; i++) {
            View view = this.mDependencySortedChildren.get(i);
            LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
            for (int i2 = 0; i2 < i; i2++) {
                if (layoutParams.mAnchorDirectChild == this.mDependencySortedChildren.get(i2)) {
                    offsetChildToAnchor(view, layoutDirection);
                }
            }
            Rect rect = this.mTempRect1;
            Rect rect2 = this.mTempRect2;
            getLastChildRect(view, rect);
            getChildRect(view, true, rect2);
            if (!rect.equals(rect2)) {
                recordLastChildRect(view, rect2);
                for (int i3 = i + 1; i3 < size; i3++) {
                    View view2 = this.mDependencySortedChildren.get(i3);
                    LayoutParams layoutParams2 = (LayoutParams) view2.getLayoutParams();
                    Behavior behavior = layoutParams2.getBehavior();
                    if (behavior != null && behavior.layoutDependsOn(this, view2, view)) {
                        if (z2 || !layoutParams2.getChangedAfterNestedScroll()) {
                            boolean onDependentViewChanged = behavior.onDependentViewChanged(this, view2, view);
                            if (z2) {
                                layoutParams2.setChangedAfterNestedScroll(onDependentViewChanged);
                            }
                        } else {
                            layoutParams2.resetChangedAfterNestedScroll();
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchDependentViewRemoved(View view) {
        LayoutParams layoutParams;
        Behavior behavior;
        View view2 = view;
        int size = this.mDependencySortedChildren.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            View view3 = this.mDependencySortedChildren.get(i);
            if (view3 == view2) {
                z = true;
            } else if (z && (behavior = (layoutParams = (LayoutParams) view3.getLayoutParams()).getBehavior()) != null && layoutParams.dependsOn(this, view3, view2)) {
                behavior.onDependentViewRemoved(this, view3, view2);
            }
        }
    }

    public void dispatchDependentViewsChanged(View view) {
        LayoutParams layoutParams;
        Behavior behavior;
        View view2 = view;
        int size = this.mDependencySortedChildren.size();
        boolean z = false;
        for (int i = 0; i < size; i++) {
            View view3 = this.mDependencySortedChildren.get(i);
            if (view3 == view2) {
                z = true;
            } else if (z && (behavior = (layoutParams = (LayoutParams) view3.getLayoutParams()).getBehavior()) != null && layoutParams.dependsOn(this, view3, view2)) {
                boolean onDependentViewChanged = behavior.onDependentViewChanged(this, view3, view2);
            }
        }
    }

    public List<View> getDependencies(View view) {
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        List<View> list = this.mTempDependenciesList;
        list.clear();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != view2 && layoutParams.dependsOn(this, view2, childAt)) {
                boolean add = list.add(childAt);
            }
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    public void ensurePreDrawListener() {
        boolean z = false;
        int childCount = getChildCount();
        int i = 0;
        while (true) {
            if (i >= childCount) {
                break;
            } else if (hasDependencies(getChildAt(i))) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z == this.mNeedsPreDrawListener) {
            return;
        }
        if (z) {
            addPreDrawListener();
        } else {
            removePreDrawListener();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasDependencies(View view) {
        View view2 = view;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (layoutParams.mAnchorView != null) {
            return true;
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt != view2 && layoutParams.dependsOn(this, view2, childAt)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void addPreDrawListener() {
        OnPreDrawListener onPreDrawListener;
        if (this.mIsAttachedToWindow) {
            if (this.mOnPreDrawListener == null) {
                new OnPreDrawListener();
                this.mOnPreDrawListener = onPreDrawListener;
            }
            getViewTreeObserver().addOnPreDrawListener(this.mOnPreDrawListener);
        }
        this.mNeedsPreDrawListener = true;
    }

    /* access modifiers changed from: package-private */
    public void removePreDrawListener() {
        if (this.mIsAttachedToWindow && this.mOnPreDrawListener != null) {
            getViewTreeObserver().removeOnPreDrawListener(this.mOnPreDrawListener);
        }
        this.mNeedsPreDrawListener = false;
    }

    /* access modifiers changed from: package-private */
    public void offsetChildToAnchor(View view, int i) {
        Behavior behavior;
        View view2 = view;
        int i2 = i;
        LayoutParams layoutParams = (LayoutParams) view2.getLayoutParams();
        if (layoutParams.mAnchorView != null) {
            Rect rect = this.mTempRect1;
            Rect rect2 = this.mTempRect2;
            Rect rect3 = this.mTempRect3;
            getDescendantRect(layoutParams.mAnchorView, rect);
            getChildRect(view2, false, rect2);
            getDesiredAnchoredChildRect(view2, i2, rect, rect3);
            int i3 = rect3.left - rect2.left;
            int i4 = rect3.top - rect2.top;
            if (i3 != 0) {
                view2.offsetLeftAndRight(i3);
            }
            if (i4 != 0) {
                view2.offsetTopAndBottom(i4);
            }
            if ((i3 != 0 || i4 != 0) && (behavior = layoutParams.getBehavior()) != null) {
                boolean onDependentViewChanged = behavior.onDependentViewChanged(this, view2, layoutParams.mAnchorView);
            }
        }
    }

    public boolean isPointInChildBounds(View view, int i, int i2) {
        Rect rect = this.mTempRect1;
        getDescendantRect(view, rect);
        return rect.contains(i, i2);
    }

    public boolean doViewsOverlap(View view, View view2) {
        View view3 = view;
        View view4 = view2;
        if (view3.getVisibility() != 0 || view4.getVisibility() != 0) {
            return false;
        }
        Rect rect = this.mTempRect1;
        getChildRect(view3, view3.getParent() != this, rect);
        Rect rect2 = this.mTempRect2;
        getChildRect(view4, view4.getParent() != this, rect2);
        return rect.left <= rect2.right && rect.top <= rect2.bottom && rect.right >= rect2.left && rect.bottom >= rect2.top;
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2;
        LayoutParams layoutParams3;
        LayoutParams layoutParams4;
        ViewGroup.LayoutParams layoutParams5 = layoutParams;
        if (layoutParams5 instanceof LayoutParams) {
            new LayoutParams((LayoutParams) layoutParams5);
            return layoutParams4;
        } else if (layoutParams5 instanceof ViewGroup.MarginLayoutParams) {
            new LayoutParams((ViewGroup.MarginLayoutParams) layoutParams5);
            return layoutParams3;
        } else {
            new LayoutParams(layoutParams5);
            return layoutParams2;
        }
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        new LayoutParams(-2, -2);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        return (layoutParams2 instanceof LayoutParams) && super.checkLayoutParams(layoutParams2);
    }

    public boolean onStartNestedScroll(View view, View view2, int i) {
        View view3 = view;
        View view4 = view2;
        int i2 = i;
        boolean z = false;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            Behavior behavior = layoutParams.getBehavior();
            if (behavior != null) {
                boolean onStartNestedScroll = behavior.onStartNestedScroll(this, childAt, view3, view4, i2);
                z |= onStartNestedScroll;
                layoutParams.acceptNestedScroll(onStartNestedScroll);
            } else {
                layoutParams.acceptNestedScroll(false);
            }
        }
        return z;
    }

    public void onNestedScrollAccepted(View view, View view2, int i) {
        Behavior behavior;
        View view3 = view;
        View view4 = view2;
        int i2 = i;
        this.mNestedScrollingParentHelper.onNestedScrollAccepted(view3, view4, i2);
        this.mNestedScrollingDirectChild = view3;
        this.mNestedScrollingTarget = view4;
        int childCount = getChildCount();
        for (int i3 = 0; i3 < childCount; i3++) {
            View childAt = getChildAt(i3);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted() && (behavior = layoutParams.getBehavior()) != null) {
                behavior.onNestedScrollAccepted(this, childAt, view3, view4, i2);
            }
        }
    }

    public void onStopNestedScroll(View view) {
        View view2 = view;
        this.mNestedScrollingParentHelper.onStopNestedScroll(view2);
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted()) {
                Behavior behavior = layoutParams.getBehavior();
                if (behavior != null) {
                    behavior.onStopNestedScroll(this, childAt, view2);
                }
                layoutParams.resetNestedScroll();
                layoutParams.resetChangedAfterNestedScroll();
            }
        }
        this.mNestedScrollingDirectChild = null;
        this.mNestedScrollingTarget = null;
    }

    public void onNestedScroll(View view, int i, int i2, int i3, int i4) {
        Behavior behavior;
        View view2 = view;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        int childCount = getChildCount();
        boolean z = false;
        for (int i9 = 0; i9 < childCount; i9++) {
            View childAt = getChildAt(i9);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted() && (behavior = layoutParams.getBehavior()) != null) {
                behavior.onNestedScroll(this, childAt, view2, i5, i6, i7, i8);
                z = true;
            }
        }
        if (z) {
            dispatchOnDependentViewChanged(true);
        }
    }

    public void onNestedPreScroll(View view, int i, int i2, int[] iArr) {
        Behavior behavior;
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        int[] iArr2 = iArr;
        int i5 = 0;
        int i6 = 0;
        boolean z = false;
        int childCount = getChildCount();
        for (int i7 = 0; i7 < childCount; i7++) {
            View childAt = getChildAt(i7);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted() && (behavior = layoutParams.getBehavior()) != null) {
                int[] iArr3 = this.mTempIntPair;
                this.mTempIntPair[1] = 0;
                iArr3[0] = 0;
                behavior.onNestedPreScroll(this, childAt, view2, i3, i4, this.mTempIntPair);
                i5 = i3 > 0 ? Math.max(i5, this.mTempIntPair[0]) : Math.min(i5, this.mTempIntPair[0]);
                i6 = i4 > 0 ? Math.max(i6, this.mTempIntPair[1]) : Math.min(i6, this.mTempIntPair[1]);
                z = true;
            }
        }
        iArr2[0] = i5;
        iArr2[1] = i6;
        if (z) {
            dispatchOnDependentViewChanged(true);
        }
    }

    public boolean onNestedFling(View view, float f, float f2, boolean z) {
        Behavior behavior;
        View view2 = view;
        float f3 = f;
        float f4 = f2;
        boolean z2 = z;
        boolean z3 = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted() && (behavior = layoutParams.getBehavior()) != null) {
                z3 |= behavior.onNestedFling(this, childAt, view2, f3, f4, z2);
            }
        }
        if (z3) {
            dispatchOnDependentViewChanged(true);
        }
        return z3;
    }

    public boolean onNestedPreFling(View view, float f, float f2) {
        Behavior behavior;
        View view2 = view;
        float f3 = f;
        float f4 = f2;
        boolean z = false;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
            if (layoutParams.isNestedScrollAccepted() && (behavior = layoutParams.getBehavior()) != null) {
                z |= behavior.onNestedPreFling(this, childAt, view2, f3, f4);
            }
        }
        return z;
    }

    public int getNestedScrollAxes() {
        return this.mNestedScrollingParentHelper.getNestedScrollAxes();
    }

    class OnPreDrawListener implements ViewTreeObserver.OnPreDrawListener {
        OnPreDrawListener() {
        }

        public boolean onPreDraw() {
            CoordinatorLayout.this.dispatchOnDependentViewChanged(false);
            return true;
        }
    }

    static class ViewElevationComparator implements Comparator<View> {
        ViewElevationComparator() {
        }

        public int compare(View view, View view2) {
            float z = ViewCompat.getZ(view);
            float z2 = ViewCompat.getZ(view2);
            if (z > z2) {
                return -1;
            }
            if (z < z2) {
                return 1;
            }
            return 0;
        }
    }

    public static abstract class Behavior<V extends View> {
        public Behavior() {
        }

        public Behavior(Context context, AttributeSet attributeSet) {
        }

        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, V v, MotionEvent motionEvent) {
            return false;
        }

        public final int getScrimColor(CoordinatorLayout coordinatorLayout, V v) {
            return ViewCompat.MEASURED_STATE_MASK;
        }

        public final float getScrimOpacity(CoordinatorLayout coordinatorLayout, V v) {
            return 0.0f;
        }

        public boolean blocksInteractionBelow(CoordinatorLayout coordinatorLayout, V v) {
            return getScrimOpacity(coordinatorLayout, v) > 0.0f;
        }

        public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, V v, View view) {
            return false;
        }

        public void onDependentViewRemoved(CoordinatorLayout coordinatorLayout, V v, View view) {
        }

        public boolean isDirty(CoordinatorLayout coordinatorLayout, V v) {
            return false;
        }

        public boolean onMeasureChild(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3, int i4) {
            return false;
        }

        public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, View view, int i) {
            return false;
        }

        public static void setTag(View view, Object obj) {
            ((LayoutParams) view.getLayoutParams()).mBehaviorTag = obj;
        }

        public static Object getTag(View view) {
            return ((LayoutParams) view.getLayoutParams()).mBehaviorTag;
        }

        public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, View view, View view2, View view3, int i) {
            return false;
        }

        public void onNestedScrollAccepted(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        }

        public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, View view, View view2) {
        }

        public void onNestedScroll(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int i3, int i4) {
        }

        public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, View view, View view2, int i, int i2, int[] iArr) {
        }

        public boolean onNestedFling(CoordinatorLayout coordinatorLayout, View view, View view2, float f, float f2, boolean z) {
            return false;
        }

        public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
            return false;
        }

        public WindowInsetsCompat onApplyWindowInsets(CoordinatorLayout coordinatorLayout, V v, WindowInsetsCompat windowInsetsCompat) {
            return windowInsetsCompat;
        }

        public void onRestoreInstanceState(CoordinatorLayout coordinatorLayout, View view, Parcelable parcelable) {
        }

        public Parcelable onSaveInstanceState(CoordinatorLayout coordinatorLayout, View view) {
            return View.BaseSavedState.EMPTY_STATE;
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int anchorGravity = 0;
        public int gravity = 0;
        public int keyline = -1;
        View mAnchorDirectChild;
        int mAnchorId = -1;
        View mAnchorView;
        Behavior mBehavior;
        boolean mBehaviorResolved = false;
        Object mBehaviorTag;
        private boolean mDidAcceptNestedScroll;
        private boolean mDidBlockInteraction;
        private boolean mDidChangeAfterNestedScroll;
        final Rect mLastChildRect;

        public LayoutParams(int i, int i2) {
            super(i, i2);
            Rect rect;
            new Rect();
            this.mLastChildRect = rect;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        LayoutParams(android.content.Context r11, android.util.AttributeSet r12) {
            /*
                r10 = this;
                r0 = r10
                r1 = r11
                r2 = r12
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r0
                r5 = 0
                r4.mBehaviorResolved = r5
                r4 = r0
                r5 = 0
                r4.gravity = r5
                r4 = r0
                r5 = 0
                r4.anchorGravity = r5
                r4 = r0
                r5 = -1
                r4.keyline = r5
                r4 = r0
                r5 = -1
                r4.mAnchorId = r5
                r4 = r0
                android.graphics.Rect r5 = new android.graphics.Rect
                r9 = r5
                r5 = r9
                r6 = r9
                r6.<init>()
                r4.mLastChildRect = r5
                r4 = r1
                r5 = r2
                int[] r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_android_layout_gravity
                r7 = 0
                int r5 = r5.getInteger(r6, r7)
                r4.gravity = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_layout_anchor
                r7 = -1
                int r5 = r5.getResourceId(r6, r7)
                r4.mAnchorId = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_layout_anchorGravity
                r7 = 0
                int r5 = r5.getInteger(r6, r7)
                r4.anchorGravity = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_layout_keyline
                r7 = -1
                int r5 = r5.getInteger(r6, r7)
                r4.keyline = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_layout_behavior
                boolean r5 = r5.hasValue(r6)
                r4.mBehaviorResolved = r5
                r4 = r0
                boolean r4 = r4.mBehaviorResolved
                if (r4 == 0) goto L_0x007c
                r4 = r0
                r5 = r1
                r6 = r2
                r7 = r3
                int r8 = android.support.design.R.styleable.CoordinatorLayout_LayoutParams_layout_behavior
                java.lang.String r7 = r7.getString(r8)
                android.support.design.widget.CoordinatorLayout$Behavior r5 = android.support.design.widget.CoordinatorLayout.parseBehavior(r5, r6, r7)
                r4.mBehavior = r5
            L_0x007c:
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((ViewGroup.MarginLayoutParams) layoutParams);
            Rect rect;
            new Rect();
            this.mLastChildRect = rect;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            Rect rect;
            new Rect();
            this.mLastChildRect = rect;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            Rect rect;
            new Rect();
            this.mLastChildRect = rect;
        }

        public int getAnchorId() {
            return this.mAnchorId;
        }

        public void setAnchorId(int i) {
            invalidateAnchor();
            this.mAnchorId = i;
        }

        public Behavior getBehavior() {
            return this.mBehavior;
        }

        public void setBehavior(Behavior behavior) {
            Behavior behavior2 = behavior;
            if (this.mBehavior != behavior2) {
                this.mBehavior = behavior2;
                this.mBehaviorTag = null;
                this.mBehaviorResolved = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void setLastChildRect(Rect rect) {
            this.mLastChildRect.set(rect);
        }

        /* access modifiers changed from: package-private */
        public Rect getLastChildRect() {
            return this.mLastChildRect;
        }

        /* access modifiers changed from: package-private */
        public boolean checkAnchorChanged() {
            return this.mAnchorView == null && this.mAnchorId != -1;
        }

        /* access modifiers changed from: package-private */
        public boolean didBlockInteraction() {
            if (this.mBehavior == null) {
                this.mDidBlockInteraction = false;
            }
            return this.mDidBlockInteraction;
        }

        /* access modifiers changed from: package-private */
        public boolean isBlockingInteractionBelow(CoordinatorLayout coordinatorLayout, View view) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            View view2 = view;
            if (this.mDidBlockInteraction) {
                return true;
            }
            boolean blocksInteractionBelow = this.mDidBlockInteraction | (this.mBehavior != null ? this.mBehavior.blocksInteractionBelow(coordinatorLayout2, view2) : false);
            this.mDidBlockInteraction = blocksInteractionBelow;
            return blocksInteractionBelow;
        }

        /* access modifiers changed from: package-private */
        public void resetTouchBehaviorTracking() {
            this.mDidBlockInteraction = false;
        }

        /* access modifiers changed from: package-private */
        public void resetNestedScroll() {
            this.mDidAcceptNestedScroll = false;
        }

        /* access modifiers changed from: package-private */
        public void acceptNestedScroll(boolean z) {
            this.mDidAcceptNestedScroll = z;
        }

        /* access modifiers changed from: package-private */
        public boolean isNestedScrollAccepted() {
            return this.mDidAcceptNestedScroll;
        }

        /* access modifiers changed from: package-private */
        public boolean getChangedAfterNestedScroll() {
            return this.mDidChangeAfterNestedScroll;
        }

        /* access modifiers changed from: package-private */
        public void setChangedAfterNestedScroll(boolean z) {
            this.mDidChangeAfterNestedScroll = z;
        }

        /* access modifiers changed from: package-private */
        public void resetChangedAfterNestedScroll() {
            this.mDidChangeAfterNestedScroll = false;
        }

        /* access modifiers changed from: package-private */
        public boolean dependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
            View view3 = view2;
            return view3 == this.mAnchorDirectChild || (this.mBehavior != null && this.mBehavior.layoutDependsOn(coordinatorLayout, view, view3));
        }

        /* access modifiers changed from: package-private */
        public void invalidateAnchor() {
            this.mAnchorDirectChild = null;
            this.mAnchorView = null;
        }

        /* access modifiers changed from: package-private */
        public View findAnchorView(CoordinatorLayout coordinatorLayout, View view) {
            CoordinatorLayout coordinatorLayout2 = coordinatorLayout;
            View view2 = view;
            if (this.mAnchorId == -1) {
                this.mAnchorDirectChild = null;
                this.mAnchorView = null;
                return null;
            }
            if (this.mAnchorView == null || !verifyAnchorView(view2, coordinatorLayout2)) {
                resolveAnchorView(view2, coordinatorLayout2);
            }
            return this.mAnchorView;
        }

        /* access modifiers changed from: package-private */
        public boolean isDirty(CoordinatorLayout coordinatorLayout, View view) {
            return this.mBehavior != null && this.mBehavior.isDirty(coordinatorLayout, view);
        }

        private void resolveAnchorView(View view, CoordinatorLayout coordinatorLayout) {
            Throwable th;
            StringBuilder sb;
            Throwable th2;
            View view2 = view;
            ViewParent viewParent = coordinatorLayout;
            this.mAnchorView = viewParent.findViewById(this.mAnchorId);
            if (this.mAnchorView != null) {
                View view3 = this.mAnchorView;
                ViewParent parent = this.mAnchorView.getParent();
                while (true) {
                    ViewParent viewParent2 = parent;
                    if (viewParent2 == viewParent || viewParent2 == null) {
                        this.mAnchorDirectChild = view3;
                    } else if (viewParent2 != view2) {
                        if (viewParent2 instanceof View) {
                            view3 = (View) viewParent2;
                        }
                        parent = viewParent2.getParent();
                    } else if (viewParent.isInEditMode()) {
                        this.mAnchorDirectChild = null;
                        this.mAnchorView = null;
                        return;
                    } else {
                        Throwable th3 = th2;
                        new IllegalStateException("Anchor must not be a descendant of the anchored view");
                        throw th3;
                    }
                }
                this.mAnchorDirectChild = view3;
            } else if (viewParent.isInEditMode()) {
                this.mAnchorDirectChild = null;
                this.mAnchorView = null;
            } else {
                Throwable th4 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("Could not find CoordinatorLayout descendant view with id ").append(viewParent.getResources().getResourceName(this.mAnchorId)).append(" to anchor view ").append(view2).toString());
                throw th4;
            }
        }

        private boolean verifyAnchorView(View view, CoordinatorLayout coordinatorLayout) {
            View view2 = view;
            ViewParent viewParent = coordinatorLayout;
            if (this.mAnchorView.getId() != this.mAnchorId) {
                return false;
            }
            View view3 = this.mAnchorView;
            ViewParent parent = this.mAnchorView.getParent();
            while (true) {
                ViewParent viewParent2 = parent;
                if (viewParent2 == viewParent) {
                    this.mAnchorDirectChild = view3;
                    return true;
                } else if (viewParent2 == null || viewParent2 == view2) {
                    this.mAnchorDirectChild = null;
                    this.mAnchorView = null;
                } else {
                    if (viewParent2 instanceof View) {
                        view3 = (View) viewParent2;
                    }
                    parent = viewParent2.getParent();
                }
            }
            this.mAnchorDirectChild = null;
            this.mAnchorView = null;
            return false;
        }
    }

    final class ApplyInsetsListener implements OnApplyWindowInsetsListener {
        ApplyInsetsListener() {
        }

        public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
            WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
            CoordinatorLayout.this.setWindowInsets(windowInsetsCompat2);
            return windowInsetsCompat2.consumeSystemWindowInsets();
        }
    }

    final class HierarchyChangeListener implements ViewGroup.OnHierarchyChangeListener {
        HierarchyChangeListener() {
        }

        public void onChildViewAdded(View view, View view2) {
            View view3 = view;
            View view4 = view2;
            if (CoordinatorLayout.this.mOnHierarchyChangeListener != null) {
                CoordinatorLayout.this.mOnHierarchyChangeListener.onChildViewAdded(view3, view4);
            }
        }

        public void onChildViewRemoved(View view, View view2) {
            View view3 = view;
            View view4 = view2;
            CoordinatorLayout.this.dispatchDependentViewRemoved(view4);
            if (CoordinatorLayout.this.mOnHierarchyChangeListener != null) {
                CoordinatorLayout.this.mOnHierarchyChangeListener.onChildViewRemoved(view3, view4);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        Parcelable parcelable2;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        SparseArray<Parcelable> sparseArray = savedState.behaviorStates;
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            int id = childAt.getId();
            Behavior behavior = getResolvedLayoutParams(childAt).getBehavior();
            if (!(id == -1 || behavior == null || (parcelable2 = sparseArray.get(id)) == null)) {
                behavior.onRestoreInstanceState(this, childAt, parcelable2);
            }
        }
    }

    /* JADX WARN: Type inference failed for: r13v1 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.os.Parcelable onSaveInstanceState() {
        /*
            r14 = this;
            r0 = r14
            android.support.design.widget.CoordinatorLayout$SavedState r10 = new android.support.design.widget.CoordinatorLayout$SavedState
            r13 = r10
            r10 = r13
            r11 = r13
            r12 = r0
            android.os.Parcelable r12 = super.onSaveInstanceState()
            r11.<init>(r12)
            r1 = r10
            android.util.SparseArray r10 = new android.util.SparseArray
            r13 = r10
            r10 = r13
            r11 = r13
            r11.<init>()
            r2 = r10
            r10 = 0
            r3 = r10
            r10 = r0
            int r10 = r10.getChildCount()
            r4 = r10
        L_0x0020:
            r10 = r3
            r11 = r4
            if (r10 >= r11) goto L_0x005a
            r10 = r0
            r11 = r3
            android.view.View r10 = r10.getChildAt(r11)
            r5 = r10
            r10 = r5
            int r10 = r10.getId()
            r6 = r10
            r10 = r5
            android.view.ViewGroup$LayoutParams r10 = r10.getLayoutParams()
            android.support.design.widget.CoordinatorLayout$LayoutParams r10 = (android.support.design.widget.CoordinatorLayout.LayoutParams) r10
            r7 = r10
            r10 = r7
            android.support.design.widget.CoordinatorLayout$Behavior r10 = r10.getBehavior()
            r8 = r10
            r10 = r6
            r11 = -1
            if (r10 == r11) goto L_0x0057
            r10 = r8
            if (r10 == 0) goto L_0x0057
            r10 = r8
            r11 = r0
            r12 = r5
            android.os.Parcelable r10 = r10.onSaveInstanceState(r11, r12)
            r9 = r10
            r10 = r9
            if (r10 == 0) goto L_0x0057
            r10 = r2
            r11 = r6
            r12 = r9
            r10.append(r11, r12)
        L_0x0057:
            int r3 = r3 + 1
            goto L_0x0020
        L_0x005a:
            r10 = r1
            r11 = r2
            r10.behaviorStates = r11
            r10 = r1
            r0 = r10
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.onSaveInstanceState():android.os.Parcelable");
    }

    protected static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR;
        SparseArray<Parcelable> behaviorStates;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public SavedState(android.os.Parcel r13, java.lang.ClassLoader r14) {
            /*
                r12 = this;
                r0 = r12
                r1 = r13
                r2 = r14
                r7 = r0
                r8 = r1
                r7.<init>(r8)
                r7 = r1
                int r7 = r7.readInt()
                r3 = r7
                r7 = r3
                int[] r7 = new int[r7]
                r4 = r7
                r7 = r1
                r8 = r4
                r7.readIntArray(r8)
                r7 = r1
                r8 = r2
                android.os.Parcelable[] r7 = r7.readParcelableArray(r8)
                r5 = r7
                r7 = r0
                android.util.SparseArray r8 = new android.util.SparseArray
                r11 = r8
                r8 = r11
                r9 = r11
                r10 = r3
                r9.<init>(r10)
                r7.behaviorStates = r8
                r7 = 0
                r6 = r7
            L_0x002c:
                r7 = r6
                r8 = r3
                if (r7 >= r8) goto L_0x0041
                r7 = r0
                android.util.SparseArray<android.os.Parcelable> r7 = r7.behaviorStates
                r8 = r4
                r9 = r6
                r8 = r8[r9]
                r9 = r5
                r10 = r6
                r9 = r9[r10]
                r7.append(r8, r9)
                int r6 = r6 + 1
                goto L_0x002c
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.CoordinatorLayout.SavedState.<init>(android.os.Parcel, java.lang.ClassLoader):void");
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            int i2 = i;
            super.writeToParcel(parcel2, i2);
            int size = this.behaviorStates != null ? this.behaviorStates.size() : 0;
            parcel2.writeInt(size);
            int[] iArr = new int[size];
            Parcelable[] parcelableArr = new Parcelable[size];
            for (int i3 = 0; i3 < size; i3++) {
                iArr[i3] = this.behaviorStates.keyAt(i3);
                parcelableArr[i3] = this.behaviorStates.valueAt(i3);
            }
            parcel2.writeIntArray(iArr);
            parcel2.writeParcelableArray(parcelableArr, i2);
        }

        static {
            Object obj;
            new ParcelableCompatCreatorCallbacks<SavedState>() {
                public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                    SavedState savedState;
                    new SavedState(parcel, classLoader);
                    return savedState;
                }

                public SavedState[] newArray(int i) {
                    return new SavedState[i];
                }
            };
            CREATOR = ParcelableCompat.newCreator(obj);
        }
    }

    private static void selectionSort(List<View> list, Comparator<View> comparator) {
        List<View> list2 = list;
        Comparator<View> comparator2 = comparator;
        if (list2 != null && list2.size() >= 2) {
            View[] viewArr = new View[list2.size()];
            Object[] array = list2.toArray(viewArr);
            int length = viewArr.length;
            for (int i = 0; i < length; i++) {
                int i2 = i;
                for (int i3 = i + 1; i3 < length; i3++) {
                    if (comparator2.compare(viewArr[i3], viewArr[i2]) < 0) {
                        i2 = i3;
                    }
                }
                if (i != i2) {
                    View view = viewArr[i2];
                    viewArr[i2] = viewArr[i];
                    viewArr[i] = view;
                }
            }
            list2.clear();
            for (int i4 = 0; i4 < length; i4++) {
                boolean add = list2.add(viewArr[i4]);
            }
        }
    }
}
