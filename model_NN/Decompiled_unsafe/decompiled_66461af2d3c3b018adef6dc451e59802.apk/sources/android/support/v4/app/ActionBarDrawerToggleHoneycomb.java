package android.support.v4.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.lang.reflect.Method;

class ActionBarDrawerToggleHoneycomb {
    private static final String TAG = "ActionBarDrawerToggleHoneycomb";
    private static final int[] THEME_ATTRS = {16843531};

    ActionBarDrawerToggleHoneycomb() {
    }

    public static Object setActionBarUpIndicator(Object obj, Activity activity, Drawable drawable, int i) {
        Object obj2;
        Object obj3 = obj;
        Activity activity2 = activity;
        Drawable drawable2 = drawable;
        int i2 = i;
        if (obj3 == null) {
            new SetIndicatorInfo(activity2);
            obj3 = obj2;
        }
        SetIndicatorInfo setIndicatorInfo = (SetIndicatorInfo) obj3;
        if (setIndicatorInfo.setHomeAsUpIndicator != null) {
            try {
                ActionBar actionBar = activity2.getActionBar();
                Object invoke = setIndicatorInfo.setHomeAsUpIndicator.invoke(actionBar, drawable2);
                Object invoke2 = setIndicatorInfo.setHomeActionContentDescription.invoke(actionBar, Integer.valueOf(i2));
            } catch (Exception e) {
                int w = Log.w(TAG, "Couldn't set home-as-up indicator via JB-MR2 API", e);
            }
        } else if (setIndicatorInfo.upIndicatorView != null) {
            setIndicatorInfo.upIndicatorView.setImageDrawable(drawable2);
        } else {
            int w2 = Log.w(TAG, "Couldn't set home-as-up indicator");
        }
        return obj3;
    }

    public static Object setActionBarDescription(Object obj, Activity activity, int i) {
        Object obj2;
        Object obj3 = obj;
        Activity activity2 = activity;
        int i2 = i;
        if (obj3 == null) {
            new SetIndicatorInfo(activity2);
            obj3 = obj2;
        }
        SetIndicatorInfo setIndicatorInfo = (SetIndicatorInfo) obj3;
        if (setIndicatorInfo.setHomeAsUpIndicator != null) {
            try {
                ActionBar actionBar = activity2.getActionBar();
                Object invoke = setIndicatorInfo.setHomeActionContentDescription.invoke(actionBar, Integer.valueOf(i2));
                if (Build.VERSION.SDK_INT <= 19) {
                    actionBar.setSubtitle(actionBar.getSubtitle());
                }
            } catch (Exception e) {
                int w = Log.w(TAG, "Couldn't set content description via JB-MR2 API", e);
            }
        }
        return obj3;
    }

    public static Drawable getThemeUpIndicator(Activity activity) {
        TypedArray obtainStyledAttributes = activity.obtainStyledAttributes(THEME_ATTRS);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        return drawable;
    }

    private static class SetIndicatorInfo {
        public Method setHomeActionContentDescription;
        public Method setHomeAsUpIndicator;
        public ImageView upIndicatorView;

        SetIndicatorInfo(Activity activity) {
            Activity activity2 = activity;
            try {
                this.setHomeAsUpIndicator = ActionBar.class.getDeclaredMethod("setHomeAsUpIndicator", Drawable.class);
                this.setHomeActionContentDescription = ActionBar.class.getDeclaredMethod("setHomeActionContentDescription", Integer.TYPE);
            } catch (NoSuchMethodException e) {
                View findViewById = activity2.findViewById(16908332);
                if (findViewById != null) {
                    ViewGroup viewGroup = (ViewGroup) findViewById.getParent();
                    if (viewGroup.getChildCount() == 2) {
                        View childAt = viewGroup.getChildAt(0);
                        View childAt2 = childAt.getId() == 16908332 ? viewGroup.getChildAt(1) : childAt;
                        if (childAt2 instanceof ImageView) {
                            this.upIndicatorView = (ImageView) childAt2;
                        }
                    }
                }
            }
        }
    }
}
