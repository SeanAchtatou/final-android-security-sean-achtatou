package android.support.v4.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.RemoteInput;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.NotificationCompatBase;
import android.support.v4.app.RemoteInputCompatBase;
import android.widget.RemoteViews;
import java.util.ArrayList;
import java.util.Iterator;

class NotificationCompatApi21 {
    public static final String CATEGORY_ALARM = "alarm";
    public static final String CATEGORY_CALL = "call";
    public static final String CATEGORY_EMAIL = "email";
    public static final String CATEGORY_ERROR = "err";
    public static final String CATEGORY_EVENT = "event";
    public static final String CATEGORY_MESSAGE = "msg";
    public static final String CATEGORY_PROGRESS = "progress";
    public static final String CATEGORY_PROMO = "promo";
    public static final String CATEGORY_RECOMMENDATION = "recommendation";
    public static final String CATEGORY_SERVICE = "service";
    public static final String CATEGORY_SOCIAL = "social";
    public static final String CATEGORY_STATUS = "status";
    public static final String CATEGORY_SYSTEM = "sys";
    public static final String CATEGORY_TRANSPORT = "transport";
    private static final String KEY_AUTHOR = "author";
    private static final String KEY_MESSAGES = "messages";
    private static final String KEY_ON_READ = "on_read";
    private static final String KEY_ON_REPLY = "on_reply";
    private static final String KEY_PARTICIPANTS = "participants";
    private static final String KEY_REMOTE_INPUT = "remote_input";
    private static final String KEY_TEXT = "text";
    private static final String KEY_TIMESTAMP = "timestamp";

    NotificationCompatApi21() {
    }

    public static class Builder implements NotificationBuilderWithBuilderAccessor, NotificationBuilderWithActions {
        private Notification.Builder b;

        public Builder(Context context, Notification notification, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, RemoteViews remoteViews, int i, PendingIntent pendingIntent, PendingIntent pendingIntent2, Bitmap bitmap, int i2, int i3, boolean z, boolean z2, boolean z3, int i4, CharSequence charSequence4, boolean z4, String str, ArrayList<String> arrayList, Bundle bundle, int i5, int i6, Notification notification2, String str2, boolean z5, String str3) {
            Notification.Builder builder;
            boolean z6;
            boolean z7;
            boolean z8;
            boolean z9;
            Notification notification3 = notification;
            CharSequence charSequence5 = charSequence;
            CharSequence charSequence6 = charSequence2;
            CharSequence charSequence7 = charSequence3;
            int i7 = i;
            PendingIntent pendingIntent3 = pendingIntent;
            PendingIntent pendingIntent4 = pendingIntent2;
            Bitmap bitmap2 = bitmap;
            int i8 = i2;
            int i9 = i3;
            boolean z10 = z;
            boolean z11 = z3;
            int i10 = i4;
            CharSequence charSequence8 = charSequence4;
            boolean z12 = z4;
            String str4 = str;
            ArrayList<String> arrayList2 = arrayList;
            Bundle bundle2 = bundle;
            int i11 = i5;
            int i12 = i6;
            Notification notification4 = notification2;
            String str5 = str2;
            boolean z13 = z5;
            String str6 = str3;
            new Notification.Builder(context);
            Notification.Builder lights = builder.setWhen(notification3.when).setShowWhen(z2).setSmallIcon(notification3.icon, notification3.iconLevel).setContent(notification3.contentView).setTicker(notification3.tickerText, remoteViews).setSound(notification3.sound, notification3.audioStreamType).setVibrate(notification3.vibrate).setLights(notification3.ledARGB, notification3.ledOnMS, notification3.ledOffMS);
            if ((notification3.flags & 2) != 0) {
                z6 = true;
            } else {
                z6 = false;
            }
            Notification.Builder ongoing = lights.setOngoing(z6);
            if ((notification3.flags & 8) != 0) {
                z7 = true;
            } else {
                z7 = false;
            }
            Notification.Builder onlyAlertOnce = ongoing.setOnlyAlertOnce(z7);
            if ((notification3.flags & 16) != 0) {
                z8 = true;
            } else {
                z8 = false;
            }
            Notification.Builder deleteIntent = onlyAlertOnce.setAutoCancel(z8).setDefaults(notification3.defaults).setContentTitle(charSequence5).setContentText(charSequence6).setSubText(charSequence8).setContentInfo(charSequence7).setContentIntent(pendingIntent3).setDeleteIntent(notification3.deleteIntent);
            PendingIntent pendingIntent5 = pendingIntent4;
            if ((notification3.flags & 128) != 0) {
                z9 = true;
            } else {
                z9 = false;
            }
            this.b = deleteIntent.setFullScreenIntent(pendingIntent5, z9).setLargeIcon(bitmap2).setNumber(i7).setUsesChronometer(z11).setPriority(i10).setProgress(i8, i9, z10).setLocalOnly(z12).setExtras(bundle2).setGroup(str5).setGroupSummary(z13).setSortKey(str6).setCategory(str4).setColor(i11).setVisibility(i12).setPublicVersion(notification4);
            Iterator<String> it = arrayList2.iterator();
            while (it.hasNext()) {
                Notification.Builder addPerson = this.b.addPerson(it.next());
            }
        }

        public void addAction(NotificationCompatBase.Action action) {
            NotificationCompatApi20.addAction(this.b, action);
        }

        public Notification.Builder getBuilder() {
            return this.b;
        }

        public Notification build() {
            return this.b.build();
        }
    }

    public static String getCategory(Notification notification) {
        return notification.category;
    }

    static Bundle getBundleForUnreadConversation(NotificationCompatBase.UnreadConversation unreadConversation) {
        Bundle bundle;
        Bundle bundle2;
        NotificationCompatBase.UnreadConversation unreadConversation2 = unreadConversation;
        if (unreadConversation2 == null) {
            return null;
        }
        new Bundle();
        Bundle bundle3 = bundle;
        String str = null;
        if (unreadConversation2.getParticipants() != null && unreadConversation2.getParticipants().length > 1) {
            str = unreadConversation2.getParticipants()[0];
        }
        Parcelable[] parcelableArr = new Parcelable[unreadConversation2.getMessages().length];
        for (int i = 0; i < parcelableArr.length; i++) {
            new Bundle();
            Bundle bundle4 = bundle2;
            bundle4.putString(KEY_TEXT, unreadConversation2.getMessages()[i]);
            bundle4.putString(KEY_AUTHOR, str);
            parcelableArr[i] = bundle4;
        }
        bundle3.putParcelableArray(KEY_MESSAGES, parcelableArr);
        RemoteInputCompatBase.RemoteInput remoteInput = unreadConversation2.getRemoteInput();
        if (remoteInput != null) {
            bundle3.putParcelable(KEY_REMOTE_INPUT, fromCompatRemoteInput(remoteInput));
        }
        bundle3.putParcelable(KEY_ON_REPLY, unreadConversation2.getReplyPendingIntent());
        bundle3.putParcelable(KEY_ON_READ, unreadConversation2.getReadPendingIntent());
        bundle3.putStringArray(KEY_PARTICIPANTS, unreadConversation2.getParticipants());
        bundle3.putLong(KEY_TIMESTAMP, unreadConversation2.getLatestTimestamp());
        return bundle3;
    }

    static NotificationCompatBase.UnreadConversation getUnreadConversationFromBundle(Bundle bundle, NotificationCompatBase.UnreadConversation.Factory factory, RemoteInputCompatBase.RemoteInput.Factory factory2) {
        Bundle bundle2 = bundle;
        NotificationCompatBase.UnreadConversation.Factory factory3 = factory;
        RemoteInputCompatBase.RemoteInput.Factory factory4 = factory2;
        if (bundle2 == null) {
            return null;
        }
        Parcelable[] parcelableArray = bundle2.getParcelableArray(KEY_MESSAGES);
        String[] strArr = null;
        if (parcelableArray != null) {
            String[] strArr2 = new String[parcelableArray.length];
            boolean z = true;
            int i = 0;
            while (true) {
                if (i >= strArr2.length) {
                    break;
                } else if (!(parcelableArray[i] instanceof Bundle)) {
                    z = false;
                    break;
                } else {
                    strArr2[i] = ((Bundle) parcelableArray[i]).getString(KEY_TEXT);
                    if (strArr2[i] == null) {
                        z = false;
                        break;
                    }
                    i++;
                }
            }
            if (!z) {
                return null;
            }
            strArr = strArr2;
        }
        PendingIntent pendingIntent = (PendingIntent) bundle2.getParcelable(KEY_ON_READ);
        PendingIntent pendingIntent2 = (PendingIntent) bundle2.getParcelable(KEY_ON_REPLY);
        RemoteInput remoteInput = (RemoteInput) bundle2.getParcelable(KEY_REMOTE_INPUT);
        String[] stringArray = bundle2.getStringArray(KEY_PARTICIPANTS);
        if (stringArray == null || stringArray.length != 1) {
            return null;
        }
        return factory3.build(strArr, remoteInput != null ? toCompatRemoteInput(remoteInput, factory4) : null, pendingIntent2, pendingIntent, stringArray, bundle2.getLong(KEY_TIMESTAMP));
    }

    private static RemoteInput fromCompatRemoteInput(RemoteInputCompatBase.RemoteInput remoteInput) {
        RemoteInput.Builder builder;
        RemoteInputCompatBase.RemoteInput remoteInput2 = remoteInput;
        new RemoteInput.Builder(remoteInput2.getResultKey());
        return builder.setLabel(remoteInput2.getLabel()).setChoices(remoteInput2.getChoices()).setAllowFreeFormInput(remoteInput2.getAllowFreeFormInput()).addExtras(remoteInput2.getExtras()).build();
    }

    private static RemoteInputCompatBase.RemoteInput toCompatRemoteInput(RemoteInput remoteInput, RemoteInputCompatBase.RemoteInput.Factory factory) {
        RemoteInput remoteInput2 = remoteInput;
        return factory.build(remoteInput2.getResultKey(), remoteInput2.getLabel(), remoteInput2.getChoices(), remoteInput2.getAllowFreeFormInput(), remoteInput2.getExtras());
    }
}
