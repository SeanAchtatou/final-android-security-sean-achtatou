package android.support.v4.content;

import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;

public class SharedPreferencesCompat {

    public static class EditorCompat {
        private static EditorCompat sInstance;
        private final Helper mHelper;

        private interface Helper {
            void apply(@NonNull SharedPreferences.Editor editor);
        }

        private static class EditorHelperBaseImpl implements Helper {
            private EditorHelperBaseImpl() {
            }

            public void apply(@NonNull SharedPreferences.Editor editor) {
                boolean commit = editor.commit();
            }
        }

        private static class EditorHelperApi9Impl implements Helper {
            private EditorHelperApi9Impl() {
            }

            public void apply(@NonNull SharedPreferences.Editor editor) {
                EditorCompatGingerbread.apply(editor);
            }
        }

        private EditorCompat() {
            Helper helper;
            Helper helper2;
            if (Build.VERSION.SDK_INT >= 9) {
                new EditorHelperApi9Impl();
                this.mHelper = helper2;
                return;
            }
            new EditorHelperBaseImpl();
            this.mHelper = helper;
        }

        public static EditorCompat getInstance() {
            EditorCompat editorCompat;
            if (sInstance == null) {
                new EditorCompat();
                sInstance = editorCompat;
            }
            return sInstance;
        }

        public void apply(@NonNull SharedPreferences.Editor editor) {
            this.mHelper.apply(editor);
        }
    }
}
