package android.support.v4.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class LocalBroadcastManager {
    private static final boolean DEBUG = false;
    static final int MSG_EXEC_PENDING_BROADCASTS = 1;
    private static final String TAG = "LocalBroadcastManager";
    private static LocalBroadcastManager mInstance;
    private static final Object mLock;
    private final HashMap<String, ArrayList<ReceiverRecord>> mActions;
    private final Context mAppContext;
    private final Handler mHandler;
    private final ArrayList<BroadcastRecord> mPendingBroadcasts;
    private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> mReceivers;

    private static class ReceiverRecord {
        boolean broadcasting;
        final IntentFilter filter;
        final BroadcastReceiver receiver;

        ReceiverRecord(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.filter = intentFilter;
            this.receiver = broadcastReceiver;
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder(128);
            StringBuilder sb2 = sb;
            StringBuilder append = sb2.append("Receiver{");
            StringBuilder append2 = sb2.append(this.receiver);
            StringBuilder append3 = sb2.append(" filter=");
            StringBuilder append4 = sb2.append(this.filter);
            StringBuilder append5 = sb2.append("}");
            return sb2.toString();
        }
    }

    private static class BroadcastRecord {
        final Intent intent;
        final ArrayList<ReceiverRecord> receivers;

        BroadcastRecord(Intent intent2, ArrayList<ReceiverRecord> arrayList) {
            this.intent = intent2;
            this.receivers = arrayList;
        }
    }

    static {
        Object obj;
        new Object();
        mLock = obj;
    }

    public static LocalBroadcastManager getInstance(Context context) {
        LocalBroadcastManager localBroadcastManager;
        Context context2 = context;
        Object obj = mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (mInstance == null) {
                    new LocalBroadcastManager(context2.getApplicationContext());
                    mInstance = localBroadcastManager;
                }
                LocalBroadcastManager localBroadcastManager2 = mInstance;
                return localBroadcastManager2;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    private LocalBroadcastManager(Context context) {
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap;
        HashMap<String, ArrayList<ReceiverRecord>> hashMap2;
        ArrayList<BroadcastRecord> arrayList;
        Handler handler;
        Context context2 = context;
        new HashMap<>();
        this.mReceivers = hashMap;
        new HashMap<>();
        this.mActions = hashMap2;
        new ArrayList<>();
        this.mPendingBroadcasts = arrayList;
        this.mAppContext = context2;
        new Handler(context2.getMainLooper()) {
            public void handleMessage(Message message) {
                Message message2 = message;
                switch (message2.what) {
                    case 1:
                        LocalBroadcastManager.this.executePendingBroadcasts();
                        return;
                    default:
                        super.handleMessage(message2);
                        return;
                }
            }
        };
        this.mHandler = handler;
    }

    /* JADX INFO: finally extract failed */
    public void registerReceiver(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        Object obj;
        ArrayList arrayList;
        ArrayList arrayList2;
        BroadcastReceiver broadcastReceiver2 = broadcastReceiver;
        IntentFilter intentFilter2 = intentFilter;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap = this.mReceivers;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap2 = hashMap;
        synchronized (hashMap) {
            try {
                new ReceiverRecord(intentFilter2, broadcastReceiver2);
                Object obj2 = obj;
                ArrayList arrayList3 = this.mReceivers.get(broadcastReceiver2);
                if (arrayList3 == null) {
                    new ArrayList(1);
                    arrayList3 = arrayList2;
                    ArrayList<IntentFilter> put = this.mReceivers.put(broadcastReceiver2, arrayList3);
                }
                boolean add = arrayList3.add(intentFilter2);
                for (int i = 0; i < intentFilter2.countActions(); i++) {
                    String action = intentFilter2.getAction(i);
                    ArrayList arrayList4 = this.mActions.get(action);
                    if (arrayList4 == null) {
                        new ArrayList(1);
                        arrayList4 = arrayList;
                        ArrayList<ReceiverRecord> put2 = this.mActions.put(action, arrayList4);
                    }
                    boolean add2 = arrayList4.add(obj2);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap3 = hashMap2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        BroadcastReceiver broadcastReceiver2 = broadcastReceiver;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap = this.mReceivers;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap2 = hashMap;
        synchronized (hashMap) {
            try {
                ArrayList remove = this.mReceivers.remove(broadcastReceiver2);
                if (remove == null) {
                    return;
                }
                for (int i = 0; i < remove.size(); i++) {
                    IntentFilter intentFilter = (IntentFilter) remove.get(i);
                    for (int i2 = 0; i2 < intentFilter.countActions(); i2++) {
                        String action = intentFilter.getAction(i2);
                        ArrayList arrayList = this.mActions.get(action);
                        if (arrayList != null) {
                            int i3 = 0;
                            while (i3 < arrayList.size()) {
                                if (((ReceiverRecord) arrayList.get(i3)).receiver == broadcastReceiver2) {
                                    Object remove2 = arrayList.remove(i3);
                                    i3--;
                                }
                                i3++;
                            }
                            if (arrayList.size() <= 0) {
                                ArrayList<ReceiverRecord> remove3 = this.mActions.remove(action);
                            }
                        }
                    }
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap3 = hashMap2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean sendBroadcast(Intent intent) {
        Object obj;
        String str;
        StringBuilder sb;
        ArrayList arrayList;
        StringBuilder sb2;
        StringBuilder sb3;
        StringBuilder sb4;
        StringBuilder sb5;
        Intent intent2 = intent;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap = this.mReceivers;
        HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap2 = hashMap;
        synchronized (hashMap) {
            try {
                String action = intent2.getAction();
                String resolveTypeIfNeeded = intent2.resolveTypeIfNeeded(this.mAppContext.getContentResolver());
                Uri data = intent2.getData();
                String scheme = intent2.getScheme();
                Set<String> categories = intent2.getCategories();
                boolean z = (intent2.getFlags() & 8) != 0;
                if (z) {
                    new StringBuilder();
                    int v = Log.v(TAG, sb5.append("Resolving type ").append(resolveTypeIfNeeded).append(" scheme ").append(scheme).append(" of intent ").append(intent2).toString());
                }
                ArrayList arrayList2 = this.mActions.get(intent2.getAction());
                if (arrayList2 != null) {
                    if (z) {
                        new StringBuilder();
                        int v2 = Log.v(TAG, sb4.append("Action list: ").append(arrayList2).toString());
                    }
                    ArrayList arrayList3 = null;
                    for (int i = 0; i < arrayList2.size(); i++) {
                        ReceiverRecord receiverRecord = (ReceiverRecord) arrayList2.get(i);
                        if (z) {
                            new StringBuilder();
                            int v3 = Log.v(TAG, sb3.append("Matching against filter ").append(receiverRecord.filter).toString());
                        }
                        if (!receiverRecord.broadcasting) {
                            int match = receiverRecord.filter.match(action, resolveTypeIfNeeded, scheme, data, categories, TAG);
                            if (match >= 0) {
                                if (z) {
                                    new StringBuilder();
                                    int v4 = Log.v(TAG, sb2.append("  Filter matched!  match=0x").append(Integer.toHexString(match)).toString());
                                }
                                if (arrayList3 == null) {
                                    new ArrayList();
                                    arrayList3 = arrayList;
                                }
                                boolean add = arrayList3.add(receiverRecord);
                                receiverRecord.broadcasting = true;
                            } else if (z) {
                                switch (match) {
                                    case -4:
                                        str = "category";
                                        break;
                                    case -3:
                                        str = "action";
                                        break;
                                    case -2:
                                        str = "data";
                                        break;
                                    case -1:
                                        str = "type";
                                        break;
                                    default:
                                        str = "unknown reason";
                                        break;
                                }
                                new StringBuilder();
                                int v5 = Log.v(TAG, sb.append("  Filter did not match: ").append(str).toString());
                            }
                        } else if (z) {
                            int v6 = Log.v(TAG, "  Filter's target already added");
                        }
                    }
                    if (arrayList3 != null) {
                        for (int i2 = 0; i2 < arrayList3.size(); i2++) {
                            ((ReceiverRecord) arrayList3.get(i2)).broadcasting = false;
                        }
                        new BroadcastRecord(intent2, arrayList3);
                        boolean add2 = this.mPendingBroadcasts.add(obj);
                        if (!this.mHandler.hasMessages(1)) {
                            boolean sendEmptyMessage = this.mHandler.sendEmptyMessage(1);
                        }
                        return true;
                    }
                }
                return false;
            } catch (Throwable th) {
                Throwable th2 = th;
                HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap3 = hashMap2;
                throw th2;
            }
        }
    }

    public void sendBroadcastSync(Intent intent) {
        if (sendBroadcast(intent)) {
            executePendingBroadcasts();
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void executePendingBroadcasts() {
        while (true) {
            HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap = this.mReceivers;
            HashMap<BroadcastReceiver, ArrayList<IntentFilter>> hashMap2 = hashMap;
            synchronized (hashMap) {
                try {
                    int size = this.mPendingBroadcasts.size();
                    if (size <= 0) {
                        return;
                    }
                    BroadcastRecord[] broadcastRecordArr = new BroadcastRecord[size];
                    Object[] array = this.mPendingBroadcasts.toArray(broadcastRecordArr);
                    this.mPendingBroadcasts.clear();
                    for (int i = 0; i < broadcastRecordArr.length; i++) {
                        BroadcastRecord broadcastRecord = broadcastRecordArr[i];
                        for (int i2 = 0; i2 < broadcastRecord.receivers.size(); i2++) {
                            broadcastRecord.receivers.get(i2).receiver.onReceive(this.mAppContext, broadcastRecord.intent);
                        }
                    }
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }
    }
}
