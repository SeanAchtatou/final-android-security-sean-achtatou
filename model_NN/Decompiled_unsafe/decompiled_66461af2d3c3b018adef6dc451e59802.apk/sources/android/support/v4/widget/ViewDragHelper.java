package android.support.v4.widget;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.VelocityTrackerCompat;
import android.support.v4.view.ViewCompat;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import java.util.Arrays;

public class ViewDragHelper {
    private static final int BASE_SETTLE_DURATION = 256;
    public static final int DIRECTION_ALL = 3;
    public static final int DIRECTION_HORIZONTAL = 1;
    public static final int DIRECTION_VERTICAL = 2;
    public static final int EDGE_ALL = 15;
    public static final int EDGE_BOTTOM = 8;
    public static final int EDGE_LEFT = 1;
    public static final int EDGE_RIGHT = 2;
    private static final int EDGE_SIZE = 20;
    public static final int EDGE_TOP = 4;
    public static final int INVALID_POINTER = -1;
    private static final int MAX_SETTLE_DURATION = 600;
    public static final int STATE_DRAGGING = 1;
    public static final int STATE_IDLE = 0;
    public static final int STATE_SETTLING = 2;
    private static final String TAG = "ViewDragHelper";
    private static final Interpolator sInterpolator;
    private int mActivePointerId = -1;
    private final Callback mCallback;
    private View mCapturedView;
    private int mDragState;
    private int[] mEdgeDragsInProgress;
    private int[] mEdgeDragsLocked;
    private int mEdgeSize;
    private int[] mInitialEdgesTouched;
    private float[] mInitialMotionX;
    private float[] mInitialMotionY;
    private float[] mLastMotionX;
    private float[] mLastMotionY;
    private float mMaxVelocity;
    private float mMinVelocity;
    private final ViewGroup mParentView;
    private int mPointersDown;
    private boolean mReleaseInProgress;
    private ScrollerCompat mScroller;
    private final Runnable mSetIdleRunnable;
    private int mTouchSlop;
    private int mTrackingEdges;
    private VelocityTracker mVelocityTracker;

    public static abstract class Callback {
        public abstract boolean tryCaptureView(View view, int i);

        public void onViewDragStateChanged(int i) {
        }

        public void onViewPositionChanged(View view, int i, int i2, int i3, int i4) {
        }

        public void onViewCaptured(View view, int i) {
        }

        public void onViewReleased(View view, float f, float f2) {
        }

        public void onEdgeTouched(int i, int i2) {
        }

        public boolean onEdgeLock(int i) {
            return false;
        }

        public void onEdgeDragStarted(int i, int i2) {
        }

        public int getOrderedChildIndex(int i) {
            return i;
        }

        public int getViewHorizontalDragRange(View view) {
            return 0;
        }

        public int getViewVerticalDragRange(View view) {
            return 0;
        }

        public int clampViewPositionHorizontal(View view, int i, int i2) {
            return 0;
        }

        public int clampViewPositionVertical(View view, int i, int i2) {
            return 0;
        }
    }

    static {
        Interpolator interpolator;
        new Interpolator() {
            public float getInterpolation(float f) {
                float f2 = f - 1.0f;
                return (f2 * f2 * f2 * f2 * f2) + 1.0f;
            }
        };
        sInterpolator = interpolator;
    }

    public static ViewDragHelper create(ViewGroup viewGroup, Callback callback) {
        ViewDragHelper viewDragHelper;
        ViewGroup viewGroup2 = viewGroup;
        new ViewDragHelper(viewGroup2.getContext(), viewGroup2, callback);
        return viewDragHelper;
    }

    public static ViewDragHelper create(ViewGroup viewGroup, float f, Callback callback) {
        ViewDragHelper create = create(viewGroup, callback);
        create.mTouchSlop = (int) (((float) create.mTouchSlop) * (1.0f / f));
        return create;
    }

    private ViewDragHelper(Context context, ViewGroup viewGroup, Callback callback) {
        Runnable runnable;
        Throwable th;
        Throwable th2;
        Context context2 = context;
        ViewGroup viewGroup2 = viewGroup;
        Callback callback2 = callback;
        new Runnable() {
            public void run() {
                ViewDragHelper.this.setDragState(0);
            }
        };
        this.mSetIdleRunnable = runnable;
        if (viewGroup2 == null) {
            Throwable th3 = th2;
            new IllegalArgumentException("Parent view may not be null");
            throw th3;
        } else if (callback2 == null) {
            Throwable th4 = th;
            new IllegalArgumentException("Callback may not be null");
            throw th4;
        } else {
            this.mParentView = viewGroup2;
            this.mCallback = callback2;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context2);
            this.mEdgeSize = (int) ((20.0f * context2.getResources().getDisplayMetrics().density) + 0.5f);
            this.mTouchSlop = viewConfiguration.getScaledTouchSlop();
            this.mMaxVelocity = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.mMinVelocity = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.mScroller = ScrollerCompat.create(context2, sInterpolator);
        }
    }

    public void setMinVelocity(float f) {
        this.mMinVelocity = f;
    }

    public float getMinVelocity() {
        return this.mMinVelocity;
    }

    public int getViewDragState() {
        return this.mDragState;
    }

    public void setEdgeTrackingEnabled(int i) {
        this.mTrackingEdges = i;
    }

    public int getEdgeSize() {
        return this.mEdgeSize;
    }

    public void captureChildView(View view, int i) {
        Throwable th;
        StringBuilder sb;
        View view2 = view;
        int i2 = i;
        if (view2.getParent() != this.mParentView) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (").append(this.mParentView).append(")").toString());
            throw th2;
        }
        this.mCapturedView = view2;
        this.mActivePointerId = i2;
        this.mCallback.onViewCaptured(view2, i2);
        setDragState(1);
    }

    public View getCapturedView() {
        return this.mCapturedView;
    }

    public int getActivePointerId() {
        return this.mActivePointerId;
    }

    public int getTouchSlop() {
        return this.mTouchSlop;
    }

    public void cancel() {
        this.mActivePointerId = -1;
        clearMotionHistory();
        if (this.mVelocityTracker != null) {
            this.mVelocityTracker.recycle();
            this.mVelocityTracker = null;
        }
    }

    public void abort() {
        cancel();
        if (this.mDragState == 2) {
            int currX = this.mScroller.getCurrX();
            int currY = this.mScroller.getCurrY();
            this.mScroller.abortAnimation();
            int currX2 = this.mScroller.getCurrX();
            int currY2 = this.mScroller.getCurrY();
            this.mCallback.onViewPositionChanged(this.mCapturedView, currX2, currY2, currX2 - currX, currY2 - currY);
        }
        setDragState(0);
    }

    public boolean smoothSlideViewTo(View view, int i, int i2) {
        this.mCapturedView = view;
        this.mActivePointerId = -1;
        boolean forceSettleCapturedViewAt = forceSettleCapturedViewAt(i, i2, 0, 0);
        if (!forceSettleCapturedViewAt && this.mDragState == 0 && this.mCapturedView != null) {
            this.mCapturedView = null;
        }
        return forceSettleCapturedViewAt;
    }

    public boolean settleCapturedViewAt(int i, int i2) {
        Throwable th;
        int i3 = i;
        int i4 = i2;
        if (this.mReleaseInProgress) {
            return forceSettleCapturedViewAt(i3, i4, (int) VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mActivePointerId), (int) VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mActivePointerId));
        }
        Throwable th2 = th;
        new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
        throw th2;
    }

    private boolean forceSettleCapturedViewAt(int i, int i2, int i3, int i4) {
        int i5 = i3;
        int i6 = i4;
        int left = this.mCapturedView.getLeft();
        int top = this.mCapturedView.getTop();
        int i7 = i - left;
        int i8 = i2 - top;
        if (i7 == 0 && i8 == 0) {
            this.mScroller.abortAnimation();
            setDragState(0);
            return false;
        }
        this.mScroller.startScroll(left, top, i7, i8, computeSettleDuration(this.mCapturedView, i7, i8, i5, i6));
        setDragState(2);
        return true;
    }

    private int computeSettleDuration(View view, int i, int i2, int i3, int i4) {
        View view2 = view;
        int i5 = i;
        int i6 = i2;
        int clampMag = clampMag(i3, (int) this.mMinVelocity, (int) this.mMaxVelocity);
        int clampMag2 = clampMag(i4, (int) this.mMinVelocity, (int) this.mMaxVelocity);
        int abs = Math.abs(i5);
        int abs2 = Math.abs(i6);
        int abs3 = Math.abs(clampMag);
        int abs4 = Math.abs(clampMag2);
        int i7 = abs3 + abs4;
        int i8 = abs + abs2;
        return (int) ((((float) computeAxisDuration(i5, clampMag, this.mCallback.getViewHorizontalDragRange(view2))) * (clampMag != 0 ? ((float) abs3) / ((float) i7) : ((float) abs) / ((float) i8))) + (((float) computeAxisDuration(i6, clampMag2, this.mCallback.getViewVerticalDragRange(view2))) * (clampMag2 != 0 ? ((float) abs4) / ((float) i7) : ((float) abs2) / ((float) i8))));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    private int computeAxisDuration(int i, int i2, int i3) {
        int abs;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (i4 == 0) {
            return 0;
        }
        int width = this.mParentView.getWidth();
        int i7 = width / 2;
        float distanceInfluenceForSnapDuration = ((float) i7) + (((float) i7) * distanceInfluenceForSnapDuration(Math.min(1.0f, ((float) Math.abs(i4)) / ((float) width))));
        int abs2 = Math.abs(i5);
        if (abs2 > 0) {
            abs = 4 * Math.round(1000.0f * Math.abs(distanceInfluenceForSnapDuration / ((float) abs2)));
        } else {
            abs = (int) (((((float) Math.abs(i4)) / ((float) i6)) + 1.0f) * 256.0f);
        }
        return Math.min(abs, (int) MAX_SETTLE_DURATION);
    }

    private int clampMag(int i, int i2, int i3) {
        int i4 = i;
        int i5 = i3;
        int abs = Math.abs(i4);
        if (abs < i2) {
            return 0;
        }
        if (abs <= i5) {
            return i4;
        }
        return i4 > 0 ? i5 : -i5;
    }

    private float clampMag(float f, float f2, float f3) {
        float f4 = f;
        float f5 = f3;
        float abs = Math.abs(f4);
        if (abs < f2) {
            return 0.0f;
        }
        if (abs <= f5) {
            return f4;
        }
        return f4 > 0.0f ? f5 : -f5;
    }

    private float distanceInfluenceForSnapDuration(float f) {
        return (float) Math.sin((double) ((float) (((double) (f - 0.5f)) * 0.4712389167638204d)));
    }

    public void flingCapturedView(int i, int i2, int i3, int i4) {
        Throwable th;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (!this.mReleaseInProgress) {
            Throwable th2 = th;
            new IllegalStateException("Cannot flingCapturedView outside of a call to Callback#onViewReleased");
            throw th2;
        }
        this.mScroller.fling(this.mCapturedView.getLeft(), this.mCapturedView.getTop(), (int) VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mActivePointerId), (int) VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mActivePointerId), i5, i7, i6, i8);
        setDragState(2);
    }

    public boolean continueSettling(boolean z) {
        boolean z2;
        boolean z3 = z;
        if (this.mDragState == 2) {
            boolean computeScrollOffset = this.mScroller.computeScrollOffset();
            int currX = this.mScroller.getCurrX();
            int currY = this.mScroller.getCurrY();
            int left = currX - this.mCapturedView.getLeft();
            int top = currY - this.mCapturedView.getTop();
            if (left != 0) {
                this.mCapturedView.offsetLeftAndRight(left);
            }
            if (top != 0) {
                this.mCapturedView.offsetTopAndBottom(top);
            }
            if (!(left == 0 && top == 0)) {
                this.mCallback.onViewPositionChanged(this.mCapturedView, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.mScroller.getFinalX() && currY == this.mScroller.getFinalY()) {
                this.mScroller.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z3) {
                    boolean post = this.mParentView.post(this.mSetIdleRunnable);
                } else {
                    setDragState(0);
                }
            }
        }
        if (this.mDragState == 2) {
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    private void dispatchViewReleased(float f, float f2) {
        this.mReleaseInProgress = true;
        this.mCallback.onViewReleased(this.mCapturedView, f, f2);
        this.mReleaseInProgress = false;
        if (this.mDragState == 1) {
            setDragState(0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    private void clearMotionHistory() {
        if (this.mInitialMotionX != null) {
            Arrays.fill(this.mInitialMotionX, 0.0f);
            Arrays.fill(this.mInitialMotionY, 0.0f);
            Arrays.fill(this.mLastMotionX, 0.0f);
            Arrays.fill(this.mLastMotionY, 0.0f);
            Arrays.fill(this.mInitialEdgesTouched, 0);
            Arrays.fill(this.mEdgeDragsInProgress, 0);
            Arrays.fill(this.mEdgeDragsLocked, 0);
            this.mPointersDown = 0;
        }
    }

    private void clearMotionHistory(int i) {
        int i2 = i;
        if (this.mInitialMotionX != null) {
            this.mInitialMotionX[i2] = 0.0f;
            this.mInitialMotionY[i2] = 0.0f;
            this.mLastMotionX[i2] = 0.0f;
            this.mLastMotionY[i2] = 0.0f;
            this.mInitialEdgesTouched[i2] = 0;
            this.mEdgeDragsInProgress[i2] = 0;
            this.mEdgeDragsLocked[i2] = 0;
            this.mPointersDown = this.mPointersDown & ((1 << i2) ^ -1);
        }
    }

    private void ensureMotionHistorySizeForId(int i) {
        int i2 = i;
        if (this.mInitialMotionX == null || this.mInitialMotionX.length <= i2) {
            float[] fArr = new float[(i2 + 1)];
            float[] fArr2 = new float[(i2 + 1)];
            float[] fArr3 = new float[(i2 + 1)];
            float[] fArr4 = new float[(i2 + 1)];
            int[] iArr = new int[(i2 + 1)];
            int[] iArr2 = new int[(i2 + 1)];
            int[] iArr3 = new int[(i2 + 1)];
            if (this.mInitialMotionX != null) {
                System.arraycopy(this.mInitialMotionX, 0, fArr, 0, this.mInitialMotionX.length);
                System.arraycopy(this.mInitialMotionY, 0, fArr2, 0, this.mInitialMotionY.length);
                System.arraycopy(this.mLastMotionX, 0, fArr3, 0, this.mLastMotionX.length);
                System.arraycopy(this.mLastMotionY, 0, fArr4, 0, this.mLastMotionY.length);
                System.arraycopy(this.mInitialEdgesTouched, 0, iArr, 0, this.mInitialEdgesTouched.length);
                System.arraycopy(this.mEdgeDragsInProgress, 0, iArr2, 0, this.mEdgeDragsInProgress.length);
                System.arraycopy(this.mEdgeDragsLocked, 0, iArr3, 0, this.mEdgeDragsLocked.length);
            }
            this.mInitialMotionX = fArr;
            this.mInitialMotionY = fArr2;
            this.mLastMotionX = fArr3;
            this.mLastMotionY = fArr4;
            this.mInitialEdgesTouched = iArr;
            this.mEdgeDragsInProgress = iArr2;
            this.mEdgeDragsLocked = iArr3;
        }
    }

    private void saveInitialMotion(float f, float f2, int i) {
        float f3 = f;
        float f4 = f2;
        int i2 = i;
        ensureMotionHistorySizeForId(i2);
        float f5 = f3;
        this.mLastMotionX[i2] = f5;
        this.mInitialMotionX[i2] = f5;
        float f6 = f4;
        this.mLastMotionY[i2] = f6;
        this.mInitialMotionY[i2] = f6;
        this.mInitialEdgesTouched[i2] = getEdgesTouched((int) f3, (int) f4);
        this.mPointersDown = this.mPointersDown | (1 << i2);
    }

    private void saveLastMotion(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int pointerCount = MotionEventCompat.getPointerCount(motionEvent2);
        for (int i = 0; i < pointerCount; i++) {
            int pointerId = MotionEventCompat.getPointerId(motionEvent2, i);
            float x = MotionEventCompat.getX(motionEvent2, i);
            float y = MotionEventCompat.getY(motionEvent2, i);
            this.mLastMotionX[pointerId] = x;
            this.mLastMotionY[pointerId] = y;
        }
    }

    public boolean isPointerDown(int i) {
        return (this.mPointersDown & (1 << i)) != 0;
    }

    /* access modifiers changed from: package-private */
    public void setDragState(int i) {
        int i2 = i;
        boolean removeCallbacks = this.mParentView.removeCallbacks(this.mSetIdleRunnable);
        if (this.mDragState != i2) {
            this.mDragState = i2;
            this.mCallback.onViewDragStateChanged(i2);
            if (this.mDragState == 0) {
                this.mCapturedView = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean tryCaptureViewForDrag(View view, int i) {
        View view2 = view;
        int i2 = i;
        if (view2 == this.mCapturedView && this.mActivePointerId == i2) {
            return true;
        }
        if (view2 == null || !this.mCallback.tryCaptureView(view2, i2)) {
            return false;
        }
        this.mActivePointerId = i2;
        captureChildView(view2, i2);
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean canScroll(View view, boolean z, int i, int i2, int i3, int i4) {
        View view2 = view;
        boolean z2 = z;
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            int scrollX = view2.getScrollX();
            int scrollY = view2.getScrollY();
            for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i7 + scrollX >= childAt.getLeft() && i7 + scrollX < childAt.getRight() && i8 + scrollY >= childAt.getTop() && i8 + scrollY < childAt.getBottom() && canScroll(childAt, true, i5, i6, (i7 + scrollX) - childAt.getLeft(), (i8 + scrollY) - childAt.getTop())) {
                    return true;
                }
            }
        }
        return z2 && (ViewCompat.canScrollHorizontally(view2, -i5) || ViewCompat.canScrollVertically(view2, -i6));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02cc, code lost:
        if (r17 != r15) goto L_0x02ed;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean shouldInterceptTouchEvent(android.view.MotionEvent r28) {
        /*
            r27 = this;
            r2 = r27
            r3 = r28
            r23 = r3
            int r23 = android.support.v4.view.MotionEventCompat.getActionMasked(r23)
            r4 = r23
            r23 = r3
            int r23 = android.support.v4.view.MotionEventCompat.getActionIndex(r23)
            r5 = r23
            r23 = r4
            if (r23 != 0) goto L_0x001d
            r23 = r2
            r23.cancel()
        L_0x001d:
            r23 = r2
            r0 = r23
            android.view.VelocityTracker r0 = r0.mVelocityTracker
            r23 = r0
            if (r23 != 0) goto L_0x0033
            r23 = r2
            android.view.VelocityTracker r24 = android.view.VelocityTracker.obtain()
            r0 = r24
            r1 = r23
            r1.mVelocityTracker = r0
        L_0x0033:
            r23 = r2
            r0 = r23
            android.view.VelocityTracker r0 = r0.mVelocityTracker
            r23 = r0
            r24 = r3
            r23.addMovement(r24)
            r23 = r4
            switch(r23) {
                case 0: goto L_0x005a;
                case 1: goto L_0x0331;
                case 2: goto L_0x01a2;
                case 3: goto L_0x0331;
                case 4: goto L_0x0045;
                case 5: goto L_0x00f6;
                case 6: goto L_0x031e;
                default: goto L_0x0045;
            }
        L_0x0045:
            r23 = r2
            r0 = r23
            int r0 = r0.mDragState
            r23 = r0
            r24 = 1
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0338
            r23 = 1
        L_0x0057:
            r2 = r23
            return r2
        L_0x005a:
            r23 = r3
            float r23 = r23.getX()
            r6 = r23
            r23 = r3
            float r23 = r23.getY()
            r7 = r23
            r23 = r3
            r24 = 0
            int r23 = android.support.v4.view.MotionEventCompat.getPointerId(r23, r24)
            r8 = r23
            r23 = r2
            r24 = r6
            r25 = r7
            r26 = r8
            r23.saveInitialMotion(r24, r25, r26)
            r23 = r2
            r24 = r6
            r0 = r24
            int r0 = (int) r0
            r24 = r0
            r25 = r7
            r0 = r25
            int r0 = (int) r0
            r25 = r0
            android.view.View r23 = r23.findTopChildUnder(r24, r25)
            r9 = r23
            r23 = r9
            r24 = r2
            r0 = r24
            android.view.View r0 = r0.mCapturedView
            r24 = r0
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x00bf
            r23 = r2
            r0 = r23
            int r0 = r0.mDragState
            r23 = r0
            r24 = 2
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x00bf
            r23 = r2
            r24 = r9
            r25 = r8
            boolean r23 = r23.tryCaptureViewForDrag(r24, r25)
        L_0x00bf:
            r23 = r2
            r0 = r23
            int[] r0 = r0.mInitialEdgesTouched
            r23 = r0
            r24 = r8
            r23 = r23[r24]
            r10 = r23
            r23 = r10
            r24 = r2
            r0 = r24
            int r0 = r0.mTrackingEdges
            r24 = r0
            r23 = r23 & r24
            if (r23 == 0) goto L_0x0045
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r10
            r25 = r2
            r0 = r25
            int r0 = r0.mTrackingEdges
            r25 = r0
            r24 = r24 & r25
            r25 = r8
            r23.onEdgeTouched(r24, r25)
            goto L_0x0045
        L_0x00f6:
            r23 = r3
            r24 = r5
            int r23 = android.support.v4.view.MotionEventCompat.getPointerId(r23, r24)
            r6 = r23
            r23 = r3
            r24 = r5
            float r23 = android.support.v4.view.MotionEventCompat.getX(r23, r24)
            r7 = r23
            r23 = r3
            r24 = r5
            float r23 = android.support.v4.view.MotionEventCompat.getY(r23, r24)
            r8 = r23
            r23 = r2
            r24 = r7
            r25 = r8
            r26 = r6
            r23.saveInitialMotion(r24, r25, r26)
            r23 = r2
            r0 = r23
            int r0 = r0.mDragState
            r23 = r0
            if (r23 != 0) goto L_0x0160
            r23 = r2
            r0 = r23
            int[] r0 = r0.mInitialEdgesTouched
            r23 = r0
            r24 = r6
            r23 = r23[r24]
            r9 = r23
            r23 = r9
            r24 = r2
            r0 = r24
            int r0 = r0.mTrackingEdges
            r24 = r0
            r23 = r23 & r24
            if (r23 == 0) goto L_0x015e
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r9
            r25 = r2
            r0 = r25
            int r0 = r0.mTrackingEdges
            r25 = r0
            r24 = r24 & r25
            r25 = r6
            r23.onEdgeTouched(r24, r25)
        L_0x015e:
            goto L_0x0045
        L_0x0160:
            r23 = r2
            r0 = r23
            int r0 = r0.mDragState
            r23 = r0
            r24 = 2
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0045
            r23 = r2
            r24 = r7
            r0 = r24
            int r0 = (int) r0
            r24 = r0
            r25 = r8
            r0 = r25
            int r0 = (int) r0
            r25 = r0
            android.view.View r23 = r23.findTopChildUnder(r24, r25)
            r9 = r23
            r23 = r9
            r24 = r2
            r0 = r24
            android.view.View r0 = r0.mCapturedView
            r24 = r0
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x01a0
            r23 = r2
            r24 = r9
            r25 = r6
            boolean r23 = r23.tryCaptureViewForDrag(r24, r25)
        L_0x01a0:
            goto L_0x0045
        L_0x01a2:
            r23 = r2
            r0 = r23
            float[] r0 = r0.mInitialMotionX
            r23 = r0
            if (r23 == 0) goto L_0x0045
            r23 = r2
            r0 = r23
            float[] r0 = r0.mInitialMotionY
            r23 = r0
            if (r23 != 0) goto L_0x01b8
            goto L_0x0045
        L_0x01b8:
            r23 = r3
            int r23 = android.support.v4.view.MotionEventCompat.getPointerCount(r23)
            r6 = r23
            r23 = 0
            r7 = r23
        L_0x01c4:
            r23 = r7
            r24 = r6
            r0 = r23
            r1 = r24
            if (r0 >= r1) goto L_0x02e0
            r23 = r3
            r24 = r7
            int r23 = android.support.v4.view.MotionEventCompat.getPointerId(r23, r24)
            r8 = r23
            r23 = r3
            r24 = r7
            float r23 = android.support.v4.view.MotionEventCompat.getX(r23, r24)
            r9 = r23
            r23 = r3
            r24 = r7
            float r23 = android.support.v4.view.MotionEventCompat.getY(r23, r24)
            r10 = r23
            r23 = r9
            r24 = r2
            r0 = r24
            float[] r0 = r0.mInitialMotionX
            r24 = r0
            r25 = r8
            r24 = r24[r25]
            float r23 = r23 - r24
            r11 = r23
            r23 = r10
            r24 = r2
            r0 = r24
            float[] r0 = r0.mInitialMotionY
            r24 = r0
            r25 = r8
            r24 = r24[r25]
            float r23 = r23 - r24
            r12 = r23
            r23 = r2
            r24 = r9
            r0 = r24
            int r0 = (int) r0
            r24 = r0
            r25 = r10
            r0 = r25
            int r0 = (int) r0
            r25 = r0
            android.view.View r23 = r23.findTopChildUnder(r24, r25)
            r13 = r23
            r23 = r13
            if (r23 == 0) goto L_0x02e9
            r23 = r2
            r24 = r13
            r25 = r11
            r26 = r12
            boolean r23 = r23.checkTouchSlop(r24, r25, r26)
            if (r23 == 0) goto L_0x02e9
            r23 = 1
        L_0x023a:
            r14 = r23
            r23 = r14
            if (r23 == 0) goto L_0x02ed
            r23 = r13
            int r23 = r23.getLeft()
            r15 = r23
            r23 = r15
            r24 = r11
            r0 = r24
            int r0 = (int) r0
            r24 = r0
            int r23 = r23 + r24
            r16 = r23
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r13
            r25 = r16
            r26 = r11
            r0 = r26
            int r0 = (int) r0
            r26 = r0
            int r23 = r23.clampViewPositionHorizontal(r24, r25, r26)
            r17 = r23
            r23 = r13
            int r23 = r23.getTop()
            r18 = r23
            r23 = r18
            r24 = r12
            r0 = r24
            int r0 = (int) r0
            r24 = r0
            int r23 = r23 + r24
            r19 = r23
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r13
            r25 = r19
            r26 = r12
            r0 = r26
            int r0 = (int) r0
            r26 = r0
            int r23 = r23.clampViewPositionVertical(r24, r25, r26)
            r20 = r23
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r13
            int r23 = r23.getViewHorizontalDragRange(r24)
            r21 = r23
            r23 = r2
            r0 = r23
            android.support.v4.widget.ViewDragHelper$Callback r0 = r0.mCallback
            r23 = r0
            r24 = r13
            int r23 = r23.getViewVerticalDragRange(r24)
            r22 = r23
            r23 = r21
            if (r23 == 0) goto L_0x02ce
            r23 = r21
            if (r23 <= 0) goto L_0x02ed
            r23 = r17
            r24 = r15
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x02ed
        L_0x02ce:
            r23 = r22
            if (r23 == 0) goto L_0x02e0
            r23 = r22
            if (r23 <= 0) goto L_0x02ed
            r23 = r20
            r24 = r18
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x02ed
        L_0x02e0:
            r23 = r2
            r24 = r3
            r23.saveLastMotion(r24)
            goto L_0x0045
        L_0x02e9:
            r23 = 0
            goto L_0x023a
        L_0x02ed:
            r23 = r2
            r24 = r11
            r25 = r12
            r26 = r8
            r23.reportNewEdgeDrags(r24, r25, r26)
            r23 = r2
            r0 = r23
            int r0 = r0.mDragState
            r23 = r0
            r24 = 1
            r0 = r23
            r1 = r24
            if (r0 != r1) goto L_0x0309
            goto L_0x02e0
        L_0x0309:
            r23 = r14
            if (r23 == 0) goto L_0x031a
            r23 = r2
            r24 = r13
            r25 = r8
            boolean r23 = r23.tryCaptureViewForDrag(r24, r25)
            if (r23 == 0) goto L_0x031a
            goto L_0x02e0
        L_0x031a:
            int r7 = r7 + 1
            goto L_0x01c4
        L_0x031e:
            r23 = r3
            r24 = r5
            int r23 = android.support.v4.view.MotionEventCompat.getPointerId(r23, r24)
            r6 = r23
            r23 = r2
            r24 = r6
            r23.clearMotionHistory(r24)
            goto L_0x0045
        L_0x0331:
            r23 = r2
            r23.cancel()
            goto L_0x0045
        L_0x0338:
            r23 = 0
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.ViewDragHelper.shouldInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    public void processTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int actionMasked = MotionEventCompat.getActionMasked(motionEvent2);
        int actionIndex = MotionEventCompat.getActionIndex(motionEvent2);
        if (actionMasked == 0) {
            cancel();
        }
        if (this.mVelocityTracker == null) {
            this.mVelocityTracker = VelocityTracker.obtain();
        }
        this.mVelocityTracker.addMovement(motionEvent2);
        switch (actionMasked) {
            case 0:
                float x = motionEvent2.getX();
                float y = motionEvent2.getY();
                int pointerId = MotionEventCompat.getPointerId(motionEvent2, 0);
                saveInitialMotion(x, y, pointerId);
                boolean tryCaptureViewForDrag = tryCaptureViewForDrag(findTopChildUnder((int) x, (int) y), pointerId);
                int i = this.mInitialEdgesTouched[pointerId];
                if ((i & this.mTrackingEdges) != 0) {
                    this.mCallback.onEdgeTouched(i & this.mTrackingEdges, pointerId);
                    return;
                }
                return;
            case 1:
                if (this.mDragState == 1) {
                    releaseViewForPointerUp();
                }
                cancel();
                return;
            case 2:
                if (this.mDragState == 1) {
                    int findPointerIndex = MotionEventCompat.findPointerIndex(motionEvent2, this.mActivePointerId);
                    float x2 = MotionEventCompat.getX(motionEvent2, findPointerIndex);
                    float y2 = MotionEventCompat.getY(motionEvent2, findPointerIndex);
                    int i2 = (int) (x2 - this.mLastMotionX[this.mActivePointerId]);
                    int i3 = (int) (y2 - this.mLastMotionY[this.mActivePointerId]);
                    dragTo(this.mCapturedView.getLeft() + i2, this.mCapturedView.getTop() + i3, i2, i3);
                    saveLastMotion(motionEvent2);
                    return;
                }
                int pointerCount = MotionEventCompat.getPointerCount(motionEvent2);
                int i4 = 0;
                while (i4 < pointerCount) {
                    int pointerId2 = MotionEventCompat.getPointerId(motionEvent2, i4);
                    float x3 = MotionEventCompat.getX(motionEvent2, i4);
                    float y3 = MotionEventCompat.getY(motionEvent2, i4);
                    float f = x3 - this.mInitialMotionX[pointerId2];
                    float f2 = y3 - this.mInitialMotionY[pointerId2];
                    reportNewEdgeDrags(f, f2, pointerId2);
                    if (this.mDragState != 1) {
                        View findTopChildUnder = findTopChildUnder((int) x3, (int) y3);
                        if (!checkTouchSlop(findTopChildUnder, f, f2) || !tryCaptureViewForDrag(findTopChildUnder, pointerId2)) {
                            i4++;
                        }
                    }
                    saveLastMotion(motionEvent2);
                    return;
                }
                saveLastMotion(motionEvent2);
                return;
            case 3:
                if (this.mDragState == 1) {
                    dispatchViewReleased(0.0f, 0.0f);
                }
                cancel();
                return;
            case 4:
            default:
                return;
            case 5:
                int pointerId3 = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                float x4 = MotionEventCompat.getX(motionEvent2, actionIndex);
                float y4 = MotionEventCompat.getY(motionEvent2, actionIndex);
                saveInitialMotion(x4, y4, pointerId3);
                if (this.mDragState == 0) {
                    boolean tryCaptureViewForDrag2 = tryCaptureViewForDrag(findTopChildUnder((int) x4, (int) y4), pointerId3);
                    int i5 = this.mInitialEdgesTouched[pointerId3];
                    if ((i5 & this.mTrackingEdges) != 0) {
                        this.mCallback.onEdgeTouched(i5 & this.mTrackingEdges, pointerId3);
                        return;
                    }
                    return;
                } else if (isCapturedViewUnder((int) x4, (int) y4)) {
                    boolean tryCaptureViewForDrag3 = tryCaptureViewForDrag(this.mCapturedView, pointerId3);
                    return;
                } else {
                    return;
                }
            case 6:
                int pointerId4 = MotionEventCompat.getPointerId(motionEvent2, actionIndex);
                if (this.mDragState == 1 && pointerId4 == this.mActivePointerId) {
                    int i6 = -1;
                    int pointerCount2 = MotionEventCompat.getPointerCount(motionEvent2);
                    int i7 = 0;
                    while (true) {
                        if (i7 < pointerCount2) {
                            int pointerId5 = MotionEventCompat.getPointerId(motionEvent2, i7);
                            if (pointerId5 != this.mActivePointerId) {
                                if (findTopChildUnder((int) MotionEventCompat.getX(motionEvent2, i7), (int) MotionEventCompat.getY(motionEvent2, i7)) == this.mCapturedView && tryCaptureViewForDrag(this.mCapturedView, pointerId5)) {
                                    i6 = this.mActivePointerId;
                                }
                            }
                            i7++;
                        }
                    }
                    if (i6 == -1) {
                        releaseViewForPointerUp();
                    }
                }
                clearMotionHistory(pointerId4);
                return;
        }
    }

    private void reportNewEdgeDrags(float f, float f2, int i) {
        float f3 = f;
        float f4 = f2;
        int i2 = i;
        int i3 = 0;
        if (checkNewEdgeDrag(f3, f4, i2, 1)) {
            i3 = 0 | 1;
        }
        if (checkNewEdgeDrag(f4, f3, i2, 4)) {
            i3 |= 4;
        }
        if (checkNewEdgeDrag(f3, f4, i2, 2)) {
            i3 |= 2;
        }
        if (checkNewEdgeDrag(f4, f3, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.mEdgeDragsInProgress;
            int i4 = i2;
            iArr[i4] = iArr[i4] | i3;
            this.mCallback.onEdgeDragStarted(i3, i2);
        }
    }

    private boolean checkNewEdgeDrag(float f, float f2, int i, int i2) {
        int i3 = i;
        int i4 = i2;
        float abs = Math.abs(f);
        float abs2 = Math.abs(f2);
        if ((this.mInitialEdgesTouched[i3] & i4) != i4 || (this.mTrackingEdges & i4) == 0 || (this.mEdgeDragsLocked[i3] & i4) == i4 || (this.mEdgeDragsInProgress[i3] & i4) == i4 || (abs <= ((float) this.mTouchSlop) && abs2 <= ((float) this.mTouchSlop))) {
            return false;
        }
        if (abs >= abs2 * 0.5f || !this.mCallback.onEdgeLock(i4)) {
            return (this.mEdgeDragsInProgress[i3] & i4) == 0 && abs > ((float) this.mTouchSlop);
        }
        int[] iArr = this.mEdgeDragsLocked;
        int i5 = i3;
        iArr[i5] = iArr[i5] | i4;
        return false;
    }

    private boolean checkTouchSlop(View view, float f, float f2) {
        View view2 = view;
        float f3 = f;
        float f4 = f2;
        if (view2 == null) {
            return false;
        }
        boolean z = this.mCallback.getViewHorizontalDragRange(view2) > 0;
        boolean z2 = this.mCallback.getViewVerticalDragRange(view2) > 0;
        if (z && z2) {
            return (f3 * f3) + (f4 * f4) > ((float) (this.mTouchSlop * this.mTouchSlop));
        } else if (z) {
            return Math.abs(f3) > ((float) this.mTouchSlop);
        } else if (!z2) {
            return false;
        } else {
            return Math.abs(f4) > ((float) this.mTouchSlop);
        }
    }

    public boolean checkTouchSlop(int i) {
        int i2 = i;
        int length = this.mInitialMotionX.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (checkTouchSlop(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkTouchSlop(int i, int i2) {
        boolean z;
        int i3 = i;
        int i4 = i2;
        if (!isPointerDown(i4)) {
            return false;
        }
        boolean z2 = (i3 & 1) == 1;
        boolean z3 = (i3 & 2) == 2;
        float f = this.mLastMotionX[i4] - this.mInitialMotionX[i4];
        float f2 = this.mLastMotionY[i4] - this.mInitialMotionY[i4];
        if (z2 && z3) {
            if ((f * f) + (f2 * f2) > ((float) (this.mTouchSlop * this.mTouchSlop))) {
                z = true;
            } else {
                z = false;
            }
            return z;
        } else if (z2) {
            return Math.abs(f) > ((float) this.mTouchSlop);
        } else if (!z3) {
            return false;
        } else {
            return Math.abs(f2) > ((float) this.mTouchSlop);
        }
    }

    public boolean isEdgeTouched(int i) {
        int i2 = i;
        int length = this.mInitialEdgesTouched.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (isEdgeTouched(i2, i3)) {
                return true;
            }
        }
        return false;
    }

    public boolean isEdgeTouched(int i, int i2) {
        int i3 = i2;
        return isPointerDown(i3) && (this.mInitialEdgesTouched[i3] & i) != 0;
    }

    private void releaseViewForPointerUp() {
        this.mVelocityTracker.computeCurrentVelocity(1000, this.mMaxVelocity);
        dispatchViewReleased(clampMag(VelocityTrackerCompat.getXVelocity(this.mVelocityTracker, this.mActivePointerId), this.mMinVelocity, this.mMaxVelocity), clampMag(VelocityTrackerCompat.getYVelocity(this.mVelocityTracker, this.mActivePointerId), this.mMinVelocity, this.mMaxVelocity));
    }

    private void dragTo(int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        int i9 = i5;
        int i10 = i6;
        int left = this.mCapturedView.getLeft();
        int top = this.mCapturedView.getTop();
        if (i7 != 0) {
            i9 = this.mCallback.clampViewPositionHorizontal(this.mCapturedView, i5, i7);
            this.mCapturedView.offsetLeftAndRight(i9 - left);
        }
        if (i8 != 0) {
            i10 = this.mCallback.clampViewPositionVertical(this.mCapturedView, i6, i8);
            this.mCapturedView.offsetTopAndBottom(i10 - top);
        }
        if (i7 != 0 || i8 != 0) {
            this.mCallback.onViewPositionChanged(this.mCapturedView, i9, i10, i9 - left, i10 - top);
        }
    }

    public boolean isCapturedViewUnder(int i, int i2) {
        return isViewUnder(this.mCapturedView, i, i2);
    }

    public boolean isViewUnder(View view, int i, int i2) {
        View view2 = view;
        int i3 = i;
        int i4 = i2;
        if (view2 == null) {
            return false;
        }
        return i3 >= view2.getLeft() && i3 < view2.getRight() && i4 >= view2.getTop() && i4 < view2.getBottom();
    }

    public View findTopChildUnder(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        for (int childCount = this.mParentView.getChildCount() - 1; childCount >= 0; childCount--) {
            View childAt = this.mParentView.getChildAt(this.mCallback.getOrderedChildIndex(childCount));
            if (i3 >= childAt.getLeft() && i3 < childAt.getRight() && i4 >= childAt.getTop() && i4 < childAt.getBottom()) {
                return childAt;
            }
        }
        return null;
    }

    private int getEdgesTouched(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int i5 = 0;
        if (i3 < this.mParentView.getLeft() + this.mEdgeSize) {
            i5 = 0 | 1;
        }
        if (i4 < this.mParentView.getTop() + this.mEdgeSize) {
            i5 |= 4;
        }
        if (i3 > this.mParentView.getRight() - this.mEdgeSize) {
            i5 |= 2;
        }
        if (i4 > this.mParentView.getBottom() - this.mEdgeSize) {
            i5 |= 8;
        }
        return i5;
    }
}
