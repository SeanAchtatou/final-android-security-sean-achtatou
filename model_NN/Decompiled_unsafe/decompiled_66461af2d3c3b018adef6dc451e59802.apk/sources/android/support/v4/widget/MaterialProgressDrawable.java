package android.support.v4.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;

class MaterialProgressDrawable extends Drawable implements Animatable {
    private static final int ANIMATION_DURATION = 1332;
    private static final int ARROW_HEIGHT = 5;
    private static final int ARROW_HEIGHT_LARGE = 6;
    private static final float ARROW_OFFSET_ANGLE = 5.0f;
    private static final int ARROW_WIDTH = 10;
    private static final int ARROW_WIDTH_LARGE = 12;
    private static final float CENTER_RADIUS = 8.75f;
    private static final float CENTER_RADIUS_LARGE = 12.5f;
    private static final int CIRCLE_DIAMETER = 40;
    private static final int CIRCLE_DIAMETER_LARGE = 56;
    private static final float COLOR_START_DELAY_OFFSET = 0.75f;
    static final int DEFAULT = 1;
    private static final float END_TRIM_START_DELAY_OFFSET = 0.5f;
    private static final float FULL_ROTATION = 1080.0f;
    static final int LARGE = 0;
    private static final Interpolator LINEAR_INTERPOLATOR;
    /* access modifiers changed from: private */
    public static final Interpolator MATERIAL_INTERPOLATOR;
    private static final float MAX_PROGRESS_ARC = 0.8f;
    private static final float NUM_POINTS = 5.0f;
    private static final float START_TRIM_DURATION_OFFSET = 0.5f;
    private static final float STROKE_WIDTH = 2.5f;
    private static final float STROKE_WIDTH_LARGE = 3.0f;
    private final int[] COLORS = {-16777216};
    private Animation mAnimation;
    private final ArrayList<Animation> mAnimators;
    private final Drawable.Callback mCallback;
    boolean mFinishing;
    private double mHeight;
    private View mParent;
    private Resources mResources;
    private final Ring mRing;
    private float mRotation;
    /* access modifiers changed from: private */
    public float mRotationCount;
    private double mWidth;

    @Retention(RetentionPolicy.CLASS)
    public @interface ProgressDrawableSize {
    }

    static /* synthetic */ float access$402(MaterialProgressDrawable materialProgressDrawable, float f) {
        float f2 = f;
        float f3 = f2;
        materialProgressDrawable.mRotationCount = f3;
        return f2;
    }

    static {
        Interpolator interpolator;
        Interpolator interpolator2;
        new LinearInterpolator();
        LINEAR_INTERPOLATOR = interpolator;
        new FastOutSlowInInterpolator();
        MATERIAL_INTERPOLATOR = interpolator2;
    }

    public MaterialProgressDrawable(Context context, View view) {
        ArrayList<Animation> arrayList;
        Drawable.Callback callback;
        Ring ring;
        new ArrayList<>();
        this.mAnimators = arrayList;
        new Drawable.Callback() {
            public void invalidateDrawable(Drawable drawable) {
                MaterialProgressDrawable.this.invalidateSelf();
            }

            public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
                MaterialProgressDrawable.this.scheduleSelf(runnable, j);
            }

            public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
                MaterialProgressDrawable.this.unscheduleSelf(runnable);
            }
        };
        this.mCallback = callback;
        this.mParent = view;
        this.mResources = context.getResources();
        new Ring(this.mCallback);
        this.mRing = ring;
        this.mRing.setColors(this.COLORS);
        updateSizes(1);
        setupAnimators();
    }

    private void setSizeParameters(double d, double d2, double d3, double d4, float f, float f2) {
        Ring ring = this.mRing;
        float f3 = this.mResources.getDisplayMetrics().density;
        this.mWidth = d * ((double) f3);
        this.mHeight = d2 * ((double) f3);
        ring.setStrokeWidth(((float) d4) * f3);
        ring.setCenterRadius(d3 * ((double) f3));
        ring.setColorIndex(0);
        ring.setArrowDimensions(f * f3, f2 * f3);
        ring.setInsets((int) this.mWidth, (int) this.mHeight);
    }

    public void updateSizes(@ProgressDrawableSize int i) {
        if (i == 0) {
            setSizeParameters(56.0d, 56.0d, 12.5d, 3.0d, 12.0f, 6.0f);
        } else {
            setSizeParameters(40.0d, 40.0d, 8.75d, 2.5d, 10.0f, 5.0f);
        }
    }

    public void showArrow(boolean z) {
        this.mRing.setShowArrow(z);
    }

    public void setArrowScale(float f) {
        this.mRing.setArrowScale(f);
    }

    public void setStartEndTrim(float f, float f2) {
        this.mRing.setStartTrim(f);
        this.mRing.setEndTrim(f2);
    }

    public void setProgressRotation(float f) {
        this.mRing.setRotation(f);
    }

    public void setBackgroundColor(int i) {
        this.mRing.setBackgroundColor(i);
    }

    public void setColorSchemeColors(int... iArr) {
        this.mRing.setColors(iArr);
        this.mRing.setColorIndex(0);
    }

    public int getIntrinsicHeight() {
        return (int) this.mHeight;
    }

    public int getIntrinsicWidth() {
        return (int) this.mWidth;
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        Rect bounds = getBounds();
        int save = canvas2.save();
        canvas2.rotate(this.mRotation, bounds.exactCenterX(), bounds.exactCenterY());
        this.mRing.draw(canvas2, bounds);
        canvas2.restoreToCount(save);
    }

    public void setAlpha(int i) {
        this.mRing.setAlpha(i);
    }

    public int getAlpha() {
        return this.mRing.getAlpha();
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.mRing.setColorFilter(colorFilter);
    }

    /* access modifiers changed from: package-private */
    public void setRotation(float f) {
        this.mRotation = f;
        invalidateSelf();
    }

    private float getRotation() {
        return this.mRotation;
    }

    public int getOpacity() {
        return -3;
    }

    public boolean isRunning() {
        ArrayList<Animation> arrayList = this.mAnimators;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            Animation animation = arrayList.get(i);
            if (animation.hasStarted() && !animation.hasEnded()) {
                return true;
            }
        }
        return false;
    }

    public void start() {
        this.mAnimation.reset();
        this.mRing.storeOriginals();
        if (this.mRing.getEndTrim() != this.mRing.getStartTrim()) {
            this.mFinishing = true;
            this.mAnimation.setDuration(666);
            this.mParent.startAnimation(this.mAnimation);
            return;
        }
        this.mRing.setColorIndex(0);
        this.mRing.resetOriginals();
        this.mAnimation.setDuration(1332);
        this.mParent.startAnimation(this.mAnimation);
    }

    public void stop() {
        this.mParent.clearAnimation();
        setRotation(0.0f);
        this.mRing.setShowArrow(false);
        this.mRing.setColorIndex(0);
        this.mRing.resetOriginals();
    }

    /* access modifiers changed from: private */
    public float getMinProgressArc(Ring ring) {
        Ring ring2 = ring;
        return (float) Math.toRadians(((double) ring2.getStrokeWidth()) / (6.283185307179586d * ring2.getCenterRadius()));
    }

    private int evaluateColorChange(float f, int i, int i2) {
        float f2 = f;
        int intValue = Integer.valueOf(i).intValue();
        int i3 = (intValue >> 24) & 255;
        int i4 = (intValue >> 16) & 255;
        int i5 = (intValue >> 8) & 255;
        int i6 = intValue & 255;
        int intValue2 = Integer.valueOf(i2).intValue();
        return ((i3 + ((int) (f2 * ((float) (((intValue2 >> 24) & 255) - i3))))) << 24) | ((i4 + ((int) (f2 * ((float) (((intValue2 >> 16) & 255) - i4))))) << 16) | ((i5 + ((int) (f2 * ((float) (((intValue2 >> 8) & 255) - i5))))) << 8) | (i6 + ((int) (f2 * ((float) ((intValue2 & 255) - i6)))));
    }

    /* access modifiers changed from: private */
    public void updateRingColor(float f, Ring ring) {
        float f2 = f;
        Ring ring2 = ring;
        if (f2 > COLOR_START_DELAY_OFFSET) {
            ring2.setColor(evaluateColorChange((f2 - COLOR_START_DELAY_OFFSET) / 0.25f, ring2.getStartingColor(), ring2.getNextColor()));
        }
    }

    /* access modifiers changed from: private */
    public void applyFinishTranslation(float f, Ring ring) {
        float f2 = f;
        Ring ring2 = ring;
        updateRingColor(f2, ring2);
        float floor = (float) (Math.floor((double) (ring2.getStartingRotation() / MAX_PROGRESS_ARC)) + 1.0d);
        ring2.setStartTrim(ring2.getStartingStartTrim() + (((ring2.getStartingEndTrim() - getMinProgressArc(ring2)) - ring2.getStartingStartTrim()) * f2));
        ring2.setEndTrim(ring2.getStartingEndTrim());
        ring2.setRotation(ring2.getStartingRotation() + ((floor - ring2.getStartingRotation()) * f2));
    }

    private void setupAnimators() {
        Animation animation;
        Animation.AnimationListener animationListener;
        Ring ring = this.mRing;
        final Ring ring2 = ring;
        new Animation() {
            public void applyTransformation(float f, Transformation transformation) {
                float f2 = f;
                if (MaterialProgressDrawable.this.mFinishing) {
                    MaterialProgressDrawable.this.applyFinishTranslation(f2, ring2);
                    return;
                }
                float access$100 = MaterialProgressDrawable.this.getMinProgressArc(ring2);
                float startingEndTrim = ring2.getStartingEndTrim();
                float startingStartTrim = ring2.getStartingStartTrim();
                float startingRotation = ring2.getStartingRotation();
                MaterialProgressDrawable.this.updateRingColor(f2, ring2);
                if (f2 <= 0.5f) {
                    ring2.setStartTrim(startingStartTrim + ((MaterialProgressDrawable.MAX_PROGRESS_ARC - access$100) * MaterialProgressDrawable.MATERIAL_INTERPOLATOR.getInterpolation(f2 / 0.5f)));
                }
                if (f2 > 0.5f) {
                    ring2.setEndTrim(startingEndTrim + ((MaterialProgressDrawable.MAX_PROGRESS_ARC - access$100) * MaterialProgressDrawable.MATERIAL_INTERPOLATOR.getInterpolation((f2 - 0.5f) / 0.5f)));
                }
                ring2.setRotation(startingRotation + (0.25f * f2));
                MaterialProgressDrawable.this.setRotation((216.0f * f2) + (MaterialProgressDrawable.FULL_ROTATION * (MaterialProgressDrawable.this.mRotationCount / 5.0f)));
            }
        };
        Animation animation2 = animation;
        animation2.setRepeatCount(-1);
        animation2.setRepeatMode(1);
        animation2.setInterpolator(LINEAR_INTERPOLATOR);
        final Ring ring3 = ring;
        new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                float access$402 = MaterialProgressDrawable.access$402(MaterialProgressDrawable.this, 0.0f);
            }

            public void onAnimationEnd(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
                Animation animation2 = animation;
                ring3.storeOriginals();
                ring3.goToNextColor();
                ring3.setStartTrim(ring3.getEndTrim());
                if (MaterialProgressDrawable.this.mFinishing) {
                    MaterialProgressDrawable.this.mFinishing = false;
                    animation2.setDuration(1332);
                    ring3.setShowArrow(false);
                    return;
                }
                float access$402 = MaterialProgressDrawable.access$402(MaterialProgressDrawable.this, (MaterialProgressDrawable.this.mRotationCount + 1.0f) % 5.0f);
            }
        };
        animation2.setAnimationListener(animationListener);
        this.mAnimation = animation2;
    }

    private static class Ring {
        private int mAlpha;
        private Path mArrow;
        private int mArrowHeight;
        private final Paint mArrowPaint;
        private float mArrowScale;
        private int mArrowWidth;
        private int mBackgroundColor;
        private final Drawable.Callback mCallback;
        private final Paint mCirclePaint;
        private int mColorIndex;
        private int[] mColors;
        private int mCurrentColor;
        private float mEndTrim = 0.0f;
        private final Paint mPaint;
        private double mRingCenterRadius;
        private float mRotation = 0.0f;
        private boolean mShowArrow;
        private float mStartTrim = 0.0f;
        private float mStartingEndTrim;
        private float mStartingRotation;
        private float mStartingStartTrim;
        private float mStrokeInset = MaterialProgressDrawable.STROKE_WIDTH;
        private float mStrokeWidth = 5.0f;
        private final RectF mTempBounds;

        public Ring(Drawable.Callback callback) {
            RectF rectF;
            Paint paint;
            Paint paint2;
            Paint paint3;
            new RectF();
            this.mTempBounds = rectF;
            new Paint();
            this.mPaint = paint;
            new Paint();
            this.mArrowPaint = paint2;
            new Paint(1);
            this.mCirclePaint = paint3;
            this.mCallback = callback;
            this.mPaint.setStrokeCap(Paint.Cap.SQUARE);
            this.mPaint.setAntiAlias(true);
            this.mPaint.setStyle(Paint.Style.STROKE);
            this.mArrowPaint.setStyle(Paint.Style.FILL);
            this.mArrowPaint.setAntiAlias(true);
        }

        public void setBackgroundColor(int i) {
            this.mBackgroundColor = i;
        }

        public void setArrowDimensions(float f, float f2) {
            this.mArrowWidth = (int) f;
            this.mArrowHeight = (int) f2;
        }

        public void draw(Canvas canvas, Rect rect) {
            Canvas canvas2 = canvas;
            Rect rect2 = rect;
            RectF rectF = this.mTempBounds;
            rectF.set(rect2);
            rectF.inset(this.mStrokeInset, this.mStrokeInset);
            float f = (this.mStartTrim + this.mRotation) * 360.0f;
            float f2 = ((this.mEndTrim + this.mRotation) * 360.0f) - f;
            this.mPaint.setColor(this.mCurrentColor);
            canvas2.drawArc(rectF, f, f2, false, this.mPaint);
            drawTriangle(canvas2, f, f2, rect2);
            if (this.mAlpha < 255) {
                this.mCirclePaint.setColor(this.mBackgroundColor);
                this.mCirclePaint.setAlpha(255 - this.mAlpha);
                canvas2.drawCircle(rect2.exactCenterX(), rect2.exactCenterY(), (float) (rect2.width() / 2), this.mCirclePaint);
            }
        }

        private void drawTriangle(Canvas canvas, float f, float f2, Rect rect) {
            Path path;
            Canvas canvas2 = canvas;
            float f3 = f;
            float f4 = f2;
            Rect rect2 = rect;
            if (this.mShowArrow) {
                if (this.mArrow == null) {
                    new Path();
                    this.mArrow = path;
                    this.mArrow.setFillType(Path.FillType.EVEN_ODD);
                } else {
                    this.mArrow.reset();
                }
                float f5 = ((float) (((int) this.mStrokeInset) / 2)) * this.mArrowScale;
                float cos = (float) ((this.mRingCenterRadius * Math.cos(0.0d)) + ((double) rect2.exactCenterX()));
                float sin = (float) ((this.mRingCenterRadius * Math.sin(0.0d)) + ((double) rect2.exactCenterY()));
                this.mArrow.moveTo(0.0f, 0.0f);
                this.mArrow.lineTo(((float) this.mArrowWidth) * this.mArrowScale, 0.0f);
                this.mArrow.lineTo((((float) this.mArrowWidth) * this.mArrowScale) / 2.0f, ((float) this.mArrowHeight) * this.mArrowScale);
                this.mArrow.offset(cos - f5, sin);
                this.mArrow.close();
                this.mArrowPaint.setColor(this.mCurrentColor);
                canvas2.rotate((f3 + f4) - 5.0f, rect2.exactCenterX(), rect2.exactCenterY());
                canvas2.drawPath(this.mArrow, this.mArrowPaint);
            }
        }

        public void setColors(@NonNull int[] iArr) {
            this.mColors = iArr;
            setColorIndex(0);
        }

        public void setColor(int i) {
            this.mCurrentColor = i;
        }

        public void setColorIndex(int i) {
            this.mColorIndex = i;
            this.mCurrentColor = this.mColors[this.mColorIndex];
        }

        public int getNextColor() {
            return this.mColors[getNextColorIndex()];
        }

        private int getNextColorIndex() {
            return (this.mColorIndex + 1) % this.mColors.length;
        }

        public void goToNextColor() {
            setColorIndex(getNextColorIndex());
        }

        public void setColorFilter(ColorFilter colorFilter) {
            ColorFilter colorFilter2 = this.mPaint.setColorFilter(colorFilter);
            invalidateSelf();
        }

        public void setAlpha(int i) {
            this.mAlpha = i;
        }

        public int getAlpha() {
            return this.mAlpha;
        }

        public void setStrokeWidth(float f) {
            float f2 = f;
            this.mStrokeWidth = f2;
            this.mPaint.setStrokeWidth(f2);
            invalidateSelf();
        }

        public float getStrokeWidth() {
            return this.mStrokeWidth;
        }

        public void setStartTrim(float f) {
            this.mStartTrim = f;
            invalidateSelf();
        }

        public float getStartTrim() {
            return this.mStartTrim;
        }

        public float getStartingStartTrim() {
            return this.mStartingStartTrim;
        }

        public float getStartingEndTrim() {
            return this.mStartingEndTrim;
        }

        public int getStartingColor() {
            return this.mColors[this.mColorIndex];
        }

        public void setEndTrim(float f) {
            this.mEndTrim = f;
            invalidateSelf();
        }

        public float getEndTrim() {
            return this.mEndTrim;
        }

        public void setRotation(float f) {
            this.mRotation = f;
            invalidateSelf();
        }

        public float getRotation() {
            return this.mRotation;
        }

        public void setInsets(int i, int i2) {
            float f;
            float min = (float) Math.min(i, i2);
            if (this.mRingCenterRadius <= 0.0d || min < 0.0f) {
                f = (float) Math.ceil((double) (this.mStrokeWidth / 2.0f));
            } else {
                f = (float) (((double) (min / 2.0f)) - this.mRingCenterRadius);
            }
            this.mStrokeInset = f;
        }

        public float getInsets() {
            return this.mStrokeInset;
        }

        public void setCenterRadius(double d) {
            this.mRingCenterRadius = d;
        }

        public double getCenterRadius() {
            return this.mRingCenterRadius;
        }

        public void setShowArrow(boolean z) {
            boolean z2 = z;
            if (this.mShowArrow != z2) {
                this.mShowArrow = z2;
                invalidateSelf();
            }
        }

        public void setArrowScale(float f) {
            float f2 = f;
            if (f2 != this.mArrowScale) {
                this.mArrowScale = f2;
                invalidateSelf();
            }
        }

        public float getStartingRotation() {
            return this.mStartingRotation;
        }

        public void storeOriginals() {
            this.mStartingStartTrim = this.mStartTrim;
            this.mStartingEndTrim = this.mEndTrim;
            this.mStartingRotation = this.mRotation;
        }

        public void resetOriginals() {
            this.mStartingStartTrim = 0.0f;
            this.mStartingEndTrim = 0.0f;
            this.mStartingRotation = 0.0f;
            setStartTrim(0.0f);
            setEndTrim(0.0f);
            setRotation(0.0f);
        }

        private void invalidateSelf() {
            this.mCallback.invalidateDrawable(null);
        }
    }
}
