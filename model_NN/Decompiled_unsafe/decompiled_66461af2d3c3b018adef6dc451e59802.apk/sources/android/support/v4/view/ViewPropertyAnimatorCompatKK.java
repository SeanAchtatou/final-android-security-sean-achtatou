package android.support.v4.view;

import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;

class ViewPropertyAnimatorCompatKK {
    ViewPropertyAnimatorCompatKK() {
    }

    public static void setUpdateListener(View view, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener;
        View view2 = view;
        ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener2 = viewPropertyAnimatorUpdateListener;
        ValueAnimator.AnimatorUpdateListener animatorUpdateListener2 = null;
        if (viewPropertyAnimatorUpdateListener2 != null) {
            final ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener3 = viewPropertyAnimatorUpdateListener2;
            final View view3 = view2;
            new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    viewPropertyAnimatorUpdateListener3.onAnimationUpdate(view3);
                }
            };
            animatorUpdateListener2 = animatorUpdateListener;
        }
        ViewPropertyAnimator updateListener = view2.animate().setUpdateListener(animatorUpdateListener2);
    }
}
