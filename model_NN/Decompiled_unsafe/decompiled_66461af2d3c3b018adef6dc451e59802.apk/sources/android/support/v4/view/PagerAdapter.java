package android.support.v4.view;

import android.database.DataSetObservable;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;

public abstract class PagerAdapter {
    public static final int POSITION_NONE = -2;
    public static final int POSITION_UNCHANGED = -1;
    private DataSetObservable mObservable;

    public abstract int getCount();

    public abstract boolean isViewFromObject(View view, Object obj);

    public PagerAdapter() {
        DataSetObservable dataSetObservable;
        new DataSetObservable();
        this.mObservable = dataSetObservable;
    }

    public void startUpdate(ViewGroup viewGroup) {
        startUpdate((View) viewGroup);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.instantiateItem(android.view.View, int):java.lang.Object
     arg types: [android.view.ViewGroup, int]
     candidates:
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.ViewGroup, int):java.lang.Object
      android.support.v4.view.PagerAdapter.instantiateItem(android.view.View, int):java.lang.Object */
    public Object instantiateItem(ViewGroup viewGroup, int i) {
        return instantiateItem((View) viewGroup, i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.destroyItem(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.destroyItem(android.view.View, int, java.lang.Object):void */
    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        destroyItem((View) viewGroup, i, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.View, int, java.lang.Object):void
     arg types: [android.view.ViewGroup, int, java.lang.Object]
     candidates:
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.ViewGroup, int, java.lang.Object):void
      android.support.v4.view.PagerAdapter.setPrimaryItem(android.view.View, int, java.lang.Object):void */
    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        setPrimaryItem((View) viewGroup, i, obj);
    }

    public void finishUpdate(ViewGroup viewGroup) {
        finishUpdate((View) viewGroup);
    }

    public void startUpdate(View view) {
    }

    public Object instantiateItem(View view, int i) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Required method instantiateItem was not overridden");
        throw th2;
    }

    public void destroyItem(View view, int i, Object obj) {
        Throwable th;
        Throwable th2 = th;
        new UnsupportedOperationException("Required method destroyItem was not overridden");
        throw th2;
    }

    public void setPrimaryItem(View view, int i, Object obj) {
    }

    public void finishUpdate(View view) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void restoreState(Parcelable parcelable, ClassLoader classLoader) {
    }

    public int getItemPosition(Object obj) {
        return -1;
    }

    public void notifyDataSetChanged() {
        this.mObservable.notifyChanged();
    }

    public void registerDataSetObserver(DataSetObserver dataSetObserver) {
        this.mObservable.registerObserver(dataSetObserver);
    }

    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
        this.mObservable.unregisterObserver(dataSetObserver);
    }

    public CharSequence getPageTitle(int i) {
        return null;
    }

    public float getPageWidth(int i) {
        return 1.0f;
    }
}
