package android.support.v4.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

class LayoutInflaterCompatBase {
    LayoutInflaterCompatBase() {
    }

    static class FactoryWrapper implements LayoutInflater.Factory {
        final LayoutInflaterFactory mDelegateFactory;

        FactoryWrapper(LayoutInflaterFactory layoutInflaterFactory) {
            this.mDelegateFactory = layoutInflaterFactory;
        }

        public View onCreateView(String str, Context context, AttributeSet attributeSet) {
            return this.mDelegateFactory.onCreateView(null, str, context, attributeSet);
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append(getClass().getName()).append("{").append(this.mDelegateFactory).append("}").toString();
        }
    }

    static void setFactory(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        LayoutInflater.Factory factory;
        LayoutInflater.Factory factory2;
        LayoutInflaterFactory layoutInflaterFactory2 = layoutInflaterFactory;
        LayoutInflater layoutInflater2 = layoutInflater;
        if (layoutInflaterFactory2 != null) {
            factory = factory2;
            new FactoryWrapper(layoutInflaterFactory2);
        } else {
            factory = null;
        }
        layoutInflater2.setFactory(factory);
    }
}
