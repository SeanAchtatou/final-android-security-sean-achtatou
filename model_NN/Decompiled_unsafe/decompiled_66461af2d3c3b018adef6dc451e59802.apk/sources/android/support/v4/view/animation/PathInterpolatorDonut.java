package android.support.v4.view.animation;

import android.graphics.Path;
import android.graphics.PathMeasure;
import android.view.animation.Interpolator;

class PathInterpolatorDonut implements Interpolator {
    private static final float PRECISION = 0.002f;
    private final float[] mX;
    private final float[] mY;

    public PathInterpolatorDonut(Path path) {
        PathMeasure pathMeasure;
        new PathMeasure(path, false);
        PathMeasure pathMeasure2 = pathMeasure;
        float length = pathMeasure2.getLength();
        int i = ((int) (length / PRECISION)) + 1;
        this.mX = new float[i];
        this.mY = new float[i];
        float[] fArr = new float[2];
        for (int i2 = 0; i2 < i; i2++) {
            boolean posTan = pathMeasure2.getPosTan((((float) i2) * length) / ((float) (i - 1)), fArr, null);
            this.mX[i2] = fArr[0];
            this.mY[i2] = fArr[1];
        }
    }

    public PathInterpolatorDonut(float f, float f2) {
        this(createQuad(f, f2));
    }

    public PathInterpolatorDonut(float f, float f2, float f3, float f4) {
        this(createCubic(f, f2, f3, f4));
    }

    public float getInterpolation(float f) {
        float f2 = f;
        if (f2 <= 0.0f) {
            return 0.0f;
        }
        if (f2 >= 1.0f) {
            return 1.0f;
        }
        int i = 0;
        int length = this.mX.length - 1;
        while (length - i > 1) {
            int i2 = (i + length) / 2;
            if (f2 < this.mX[i2]) {
                length = i2;
            } else {
                i = i2;
            }
        }
        float f3 = this.mX[length] - this.mX[i];
        if (f3 == 0.0f) {
            return this.mY[i];
        }
        float f4 = (f2 - this.mX[i]) / f3;
        float f5 = this.mY[i];
        return f5 + (f4 * (this.mY[length] - f5));
    }

    private static Path createQuad(float f, float f2) {
        Path path;
        new Path();
        Path path2 = path;
        path2.moveTo(0.0f, 0.0f);
        path2.quadTo(f, f2, 1.0f, 1.0f);
        return path2;
    }

    private static Path createCubic(float f, float f2, float f3, float f4) {
        Path path;
        new Path();
        Path path2 = path;
        path2.moveTo(0.0f, 0.0f);
        path2.cubicTo(f, f2, f3, f4, 1.0f, 1.0f);
        return path2;
    }
}
