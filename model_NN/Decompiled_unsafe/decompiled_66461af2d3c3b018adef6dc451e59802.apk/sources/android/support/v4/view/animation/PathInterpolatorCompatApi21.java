package android.support.v4.view.animation;

import android.graphics.Path;
import android.view.animation.Interpolator;
import android.view.animation.PathInterpolator;

class PathInterpolatorCompatApi21 {
    private PathInterpolatorCompatApi21() {
    }

    public static Interpolator create(Path path) {
        Interpolator interpolator;
        new PathInterpolator(path);
        return interpolator;
    }

    public static Interpolator create(float f, float f2) {
        Interpolator interpolator;
        new PathInterpolator(f, f2);
        return interpolator;
    }

    public static Interpolator create(float f, float f2, float f3, float f4) {
        Interpolator interpolator;
        new PathInterpolator(f, f2, f3, f4);
        return interpolator;
    }
}
