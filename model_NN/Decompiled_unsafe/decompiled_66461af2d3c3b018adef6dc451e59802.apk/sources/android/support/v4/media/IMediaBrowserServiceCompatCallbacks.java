package android.support.v4.media;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.support.v4.media.session.MediaSessionCompat;
import java.util.List;

public interface IMediaBrowserServiceCompatCallbacks extends IInterface {
    void onConnect(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException;

    void onConnectFailed() throws RemoteException;

    void onLoadChildren(String str, List list) throws RemoteException;

    public static abstract class Stub extends Binder implements IMediaBrowserServiceCompatCallbacks {
        private static final String DESCRIPTOR = "android.support.v4.media.IMediaBrowserServiceCompatCallbacks";
        static final int TRANSACTION_onConnect = 1;
        static final int TRANSACTION_onConnectFailed = 2;
        static final int TRANSACTION_onLoadChildren = 3;

        public Stub() {
            attachInterface(this, DESCRIPTOR);
        }

        public static IMediaBrowserServiceCompatCallbacks asInterface(IBinder iBinder) {
            IMediaBrowserServiceCompatCallbacks iMediaBrowserServiceCompatCallbacks;
            IBinder iBinder2 = iBinder;
            if (iBinder2 == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder2.queryLocalInterface(DESCRIPTOR);
            if (queryLocalInterface != null && (queryLocalInterface instanceof IMediaBrowserServiceCompatCallbacks)) {
                return (IMediaBrowserServiceCompatCallbacks) queryLocalInterface;
            }
            new Proxy(iBinder2);
            return iMediaBrowserServiceCompatCallbacks;
        }

        public IBinder asBinder() {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            MediaSessionCompat.Token token;
            Bundle bundle;
            int i3 = i;
            Parcel parcel3 = parcel;
            Parcel parcel4 = parcel2;
            int i4 = i2;
            switch (i3) {
                case 1:
                    parcel3.enforceInterface(DESCRIPTOR);
                    String readString = parcel3.readString();
                    if (0 != parcel3.readInt()) {
                        token = MediaSessionCompat.Token.CREATOR.createFromParcel(parcel3);
                    } else {
                        token = null;
                    }
                    if (0 != parcel3.readInt()) {
                        bundle = (Bundle) Bundle.CREATOR.createFromParcel(parcel3);
                    } else {
                        bundle = null;
                    }
                    onConnect(readString, token, bundle);
                    return true;
                case 2:
                    parcel3.enforceInterface(DESCRIPTOR);
                    onConnectFailed();
                    return true;
                case 3:
                    parcel3.enforceInterface(DESCRIPTOR);
                    onLoadChildren(parcel3.readString(), parcel3.readArrayList(getClass().getClassLoader()));
                    return true;
                case 1598968902:
                    parcel4.writeString(DESCRIPTOR);
                    return true;
                default:
                    return super.onTransact(i3, parcel3, parcel4, i4);
            }
        }

        private static class Proxy implements IMediaBrowserServiceCompatCallbacks {
            private IBinder mRemote;

            Proxy(IBinder iBinder) {
                this.mRemote = iBinder;
            }

            public IBinder asBinder() {
                return this.mRemote;
            }

            public String getInterfaceDescriptor() {
                return Stub.DESCRIPTOR;
            }

            public void onConnect(String str, MediaSessionCompat.Token token, Bundle bundle) throws RemoteException {
                String str2 = str;
                MediaSessionCompat.Token token2 = token;
                Bundle bundle2 = bundle;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str2);
                    if (token2 != null) {
                        obtain.writeInt(1);
                        token2.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (bundle2 != null) {
                        obtain.writeInt(1);
                        bundle2.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    boolean transact = this.mRemote.transact(1, obtain, null, 1);
                    obtain.recycle();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    obtain.recycle();
                    throw th2;
                }
            }

            public void onConnectFailed() throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    boolean transact = this.mRemote.transact(2, obtain, null, 1);
                    obtain.recycle();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    obtain.recycle();
                    throw th2;
                }
            }

            public void onLoadChildren(String str, List list) throws RemoteException {
                String str2 = str;
                List list2 = list;
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken(Stub.DESCRIPTOR);
                    obtain.writeString(str2);
                    obtain.writeList(list2);
                    boolean transact = this.mRemote.transact(3, obtain, null, 1);
                    obtain.recycle();
                } catch (Throwable th) {
                    Throwable th2 = th;
                    obtain.recycle();
                    throw th2;
                }
            }
        }
    }
}
