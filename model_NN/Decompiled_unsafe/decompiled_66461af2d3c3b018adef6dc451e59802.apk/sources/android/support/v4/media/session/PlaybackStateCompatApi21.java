package android.support.v4.media.session;

import android.media.session.PlaybackState;
import android.os.Bundle;
import java.util.Iterator;
import java.util.List;

class PlaybackStateCompatApi21 {
    PlaybackStateCompatApi21() {
    }

    public static int getState(Object obj) {
        return ((PlaybackState) obj).getState();
    }

    public static long getPosition(Object obj) {
        return ((PlaybackState) obj).getPosition();
    }

    public static long getBufferedPosition(Object obj) {
        return ((PlaybackState) obj).getBufferedPosition();
    }

    public static float getPlaybackSpeed(Object obj) {
        return ((PlaybackState) obj).getPlaybackSpeed();
    }

    public static long getActions(Object obj) {
        return ((PlaybackState) obj).getActions();
    }

    public static CharSequence getErrorMessage(Object obj) {
        return ((PlaybackState) obj).getErrorMessage();
    }

    public static long getLastPositionUpdateTime(Object obj) {
        return ((PlaybackState) obj).getLastPositionUpdateTime();
    }

    public static List<Object> getCustomActions(Object obj) {
        return ((PlaybackState) obj).getCustomActions();
    }

    public static long getActiveQueueItemId(Object obj) {
        return ((PlaybackState) obj).getActiveQueueItemId();
    }

    public static Object newInstance(int i, long j, long j2, float f, long j3, CharSequence charSequence, long j4, List<Object> list, long j5) {
        PlaybackState.Builder builder;
        long j6 = j5;
        new PlaybackState.Builder();
        PlaybackState.Builder builder2 = builder;
        PlaybackState.Builder state = builder2.setState(i, j, f, j4);
        PlaybackState.Builder bufferedPosition = builder2.setBufferedPosition(j2);
        PlaybackState.Builder actions = builder2.setActions(j3);
        PlaybackState.Builder errorMessage = builder2.setErrorMessage(charSequence);
        Iterator<Object> it = list.iterator();
        while (it.hasNext()) {
            PlaybackState.Builder addCustomAction = builder2.addCustomAction((PlaybackState.CustomAction) it.next());
        }
        PlaybackState.Builder activeQueueItemId = builder2.setActiveQueueItemId(j6);
        return builder2.build();
    }

    static final class CustomAction {
        CustomAction() {
        }

        public static String getAction(Object obj) {
            return ((PlaybackState.CustomAction) obj).getAction();
        }

        public static CharSequence getName(Object obj) {
            return ((PlaybackState.CustomAction) obj).getName();
        }

        public static int getIcon(Object obj) {
            return ((PlaybackState.CustomAction) obj).getIcon();
        }

        public static Bundle getExtras(Object obj) {
            return ((PlaybackState.CustomAction) obj).getExtras();
        }

        public static Object newInstance(String str, CharSequence charSequence, int i, Bundle bundle) {
            PlaybackState.CustomAction.Builder builder;
            new PlaybackState.CustomAction.Builder(str, charSequence, i);
            PlaybackState.CustomAction.Builder builder2 = builder;
            PlaybackState.CustomAction.Builder extras = builder2.setExtras(bundle);
            return builder2.build();
        }
    }
}
