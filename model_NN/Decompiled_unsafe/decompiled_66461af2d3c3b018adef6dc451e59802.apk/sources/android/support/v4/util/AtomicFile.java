package android.support.v4.util;

import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AtomicFile {
    private final File mBackupName;
    private final File mBaseName;

    public AtomicFile(File file) {
        File file2;
        StringBuilder sb;
        File file3 = file;
        this.mBaseName = file3;
        new StringBuilder();
        new File(sb.append(file3.getPath()).append(".bak").toString());
        this.mBackupName = file2;
    }

    public File getBaseFile() {
        return this.mBaseName;
    }

    public void delete() {
        boolean delete = this.mBaseName.delete();
        boolean delete2 = this.mBackupName.delete();
    }

    public FileOutputStream startWrite() throws IOException {
        Throwable th;
        StringBuilder sb;
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2;
        Throwable th2;
        StringBuilder sb2;
        FileOutputStream fileOutputStream3;
        StringBuilder sb3;
        if (this.mBaseName.exists()) {
            if (this.mBackupName.exists()) {
                boolean delete = this.mBaseName.delete();
            } else if (!this.mBaseName.renameTo(this.mBackupName)) {
                new StringBuilder();
                int w = Log.w("AtomicFile", sb3.append("Couldn't rename file ").append(this.mBaseName).append(" to backup file ").append(this.mBackupName).toString());
            }
        }
        try {
            FileOutputStream fileOutputStream4 = fileOutputStream3;
            new FileOutputStream(this.mBaseName);
            fileOutputStream2 = fileOutputStream4;
        } catch (FileNotFoundException e) {
            if (!this.mBaseName.getParentFile().mkdir()) {
                Throwable th3 = th2;
                new StringBuilder();
                new IOException(sb2.append("Couldn't create directory ").append(this.mBaseName).toString());
                throw th3;
            }
            try {
                FileOutputStream fileOutputStream5 = fileOutputStream;
                new FileOutputStream(this.mBaseName);
                fileOutputStream2 = fileOutputStream5;
            } catch (FileNotFoundException e2) {
                Throwable th4 = th;
                new StringBuilder();
                new IOException(sb.append("Couldn't create ").append(this.mBaseName).toString());
                throw th4;
            }
        }
        return fileOutputStream2;
    }

    public void finishWrite(FileOutputStream fileOutputStream) {
        FileOutputStream fileOutputStream2 = fileOutputStream;
        if (fileOutputStream2 != null) {
            boolean sync = sync(fileOutputStream2);
            try {
                fileOutputStream2.close();
                boolean delete = this.mBackupName.delete();
            } catch (IOException e) {
                int w = Log.w("AtomicFile", "finishWrite: Got exception:", e);
            }
        }
    }

    public void failWrite(FileOutputStream fileOutputStream) {
        FileOutputStream fileOutputStream2 = fileOutputStream;
        if (fileOutputStream2 != null) {
            boolean sync = sync(fileOutputStream2);
            try {
                fileOutputStream2.close();
                boolean delete = this.mBaseName.delete();
                boolean renameTo = this.mBackupName.renameTo(this.mBaseName);
            } catch (IOException e) {
                int w = Log.w("AtomicFile", "failWrite: Got exception:", e);
            }
        }
    }

    public FileInputStream openRead() throws FileNotFoundException {
        FileInputStream fileInputStream;
        if (this.mBackupName.exists()) {
            boolean delete = this.mBaseName.delete();
            boolean renameTo = this.mBackupName.renameTo(this.mBaseName);
        }
        new FileInputStream(this.mBaseName);
        return fileInputStream;
    }

    /* JADX INFO: finally extract failed */
    public byte[] readFully() throws IOException {
        FileInputStream openRead = openRead();
        int i = 0;
        try {
            byte[] bArr = new byte[openRead.available()];
            while (true) {
                int read = openRead.read(bArr, i, bArr.length - i);
                if (read <= 0) {
                    byte[] bArr2 = bArr;
                    openRead.close();
                    return bArr2;
                }
                i += read;
                int available = openRead.available();
                if (available > bArr.length - i) {
                    byte[] bArr3 = new byte[(i + available)];
                    System.arraycopy(bArr, 0, bArr3, 0, i);
                    bArr = bArr3;
                }
            }
        } catch (Throwable th) {
            Throwable th2 = th;
            openRead.close();
            throw th2;
        }
    }

    static boolean sync(FileOutputStream fileOutputStream) {
        FileOutputStream fileOutputStream2 = fileOutputStream;
        if (fileOutputStream2 != null) {
            try {
                fileOutputStream2.getFD().sync();
            } catch (IOException e) {
                return false;
            }
        }
        return true;
    }
}
