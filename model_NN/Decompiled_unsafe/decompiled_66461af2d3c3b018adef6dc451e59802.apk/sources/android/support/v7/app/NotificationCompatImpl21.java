package android.support.v7.app;

import android.app.Notification;
import android.media.session.MediaSession;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;

class NotificationCompatImpl21 {
    NotificationCompatImpl21() {
    }

    public static void addMediaStyle(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, int[] iArr, Object obj) {
        Notification.MediaStyle mediaStyle;
        int[] iArr2 = iArr;
        Object obj2 = obj;
        new Notification.MediaStyle(notificationBuilderWithBuilderAccessor.getBuilder());
        Notification.MediaStyle mediaStyle2 = mediaStyle;
        if (iArr2 != null) {
            Notification.MediaStyle showActionsInCompactView = mediaStyle2.setShowActionsInCompactView(iArr2);
        }
        if (obj2 != null) {
            Notification.MediaStyle mediaSession = mediaStyle2.setMediaSession((MediaSession.Token) obj2);
        }
    }
}
