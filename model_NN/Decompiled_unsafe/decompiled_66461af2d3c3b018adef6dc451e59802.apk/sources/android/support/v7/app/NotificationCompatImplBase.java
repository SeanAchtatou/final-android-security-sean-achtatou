package android.support.v7.app;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.SystemClock;
import android.support.v4.app.NotificationBuilderWithBuilderAccessor;
import android.support.v4.app.NotificationCompatBase;
import android.support.v7.appcompat.R;
import android.widget.RemoteViews;
import java.text.NumberFormat;
import java.util.List;

class NotificationCompatImplBase {
    static final int MAX_MEDIA_BUTTONS = 5;
    static final int MAX_MEDIA_BUTTONS_IN_COMPACT = 3;

    NotificationCompatImplBase() {
    }

    public static <T extends NotificationCompatBase.Action> void overrideContentView(NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent) {
        NotificationBuilderWithBuilderAccessor notificationBuilderWithBuilderAccessor2 = notificationBuilderWithBuilderAccessor;
        boolean z3 = z2;
        Notification.Builder content = notificationBuilderWithBuilderAccessor2.getBuilder().setContent(generateContentView(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, list, iArr, z3, pendingIntent));
        if (z3) {
            Notification.Builder ongoing = notificationBuilderWithBuilderAccessor2.getBuilder().setOngoing(true);
        }
    }

    private static <T extends NotificationCompatBase.Action> RemoteViews generateContentView(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, int[] iArr, boolean z2, PendingIntent pendingIntent) {
        int min;
        Throwable th;
        Context context2 = context;
        List<T> list2 = list;
        int[] iArr2 = iArr;
        boolean z3 = z2;
        PendingIntent pendingIntent2 = pendingIntent;
        RemoteViews applyStandardTemplate = applyStandardTemplate(context2, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, R.layout.notification_template_media, true);
        int size = list2.size();
        if (iArr2 == null) {
            min = 0;
        } else {
            min = Math.min(iArr2.length, 3);
        }
        int i2 = min;
        applyStandardTemplate.removeAllViews(R.id.media_actions);
        if (i2 > 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                if (i3 >= size) {
                    Throwable th2 = th;
                    Object[] objArr = new Object[2];
                    objArr[0] = Integer.valueOf(i3);
                    Object[] objArr2 = objArr;
                    objArr2[1] = Integer.valueOf(size - 1);
                    new IllegalArgumentException(String.format("setShowActionsInCompactView: action %d out of bounds (max %d)", objArr2));
                    throw th2;
                }
                applyStandardTemplate.addView(R.id.media_actions, generateMediaActionButton(context2, (NotificationCompatBase.Action) list2.get(iArr2[i3])));
            }
        }
        if (z3) {
            applyStandardTemplate.setViewVisibility(R.id.end_padder, 8);
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
            applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, pendingIntent2);
            applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", context2.getResources().getInteger(R.integer.cancel_button_image_alpha));
        } else {
            applyStandardTemplate.setViewVisibility(R.id.end_padder, 0);
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    public static <T extends NotificationCompatBase.Action> void overrideBigContentView(Notification notification, Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, boolean z2, PendingIntent pendingIntent) {
        Notification notification2 = notification;
        boolean z3 = z2;
        notification2.bigContentView = generateBigContentView(context, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, list, z3, pendingIntent);
        if (z3) {
            notification2.flags |= 2;
        }
    }

    private static <T extends NotificationCompatBase.Action> RemoteViews generateBigContentView(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, List<T> list, boolean z2, PendingIntent pendingIntent) {
        Context context2 = context;
        List<T> list2 = list;
        boolean z3 = z2;
        PendingIntent pendingIntent2 = pendingIntent;
        int min = Math.min(list2.size(), 5);
        RemoteViews applyStandardTemplate = applyStandardTemplate(context2, charSequence, charSequence2, charSequence3, i, bitmap, charSequence4, z, j, getBigLayoutResource(min), false);
        applyStandardTemplate.removeAllViews(R.id.media_actions);
        if (min > 0) {
            for (int i2 = 0; i2 < min; i2++) {
                applyStandardTemplate.addView(R.id.media_actions, generateMediaActionButton(context2, (NotificationCompatBase.Action) list2.get(i2)));
            }
        }
        if (z3) {
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 0);
            applyStandardTemplate.setInt(R.id.cancel_action, "setAlpha", context2.getResources().getInteger(R.integer.cancel_button_image_alpha));
            applyStandardTemplate.setOnClickPendingIntent(R.id.cancel_action, pendingIntent2);
        } else {
            applyStandardTemplate.setViewVisibility(R.id.cancel_action, 8);
        }
        return applyStandardTemplate;
    }

    private static RemoteViews generateMediaActionButton(Context context, NotificationCompatBase.Action action) {
        RemoteViews remoteViews;
        Context context2 = context;
        NotificationCompatBase.Action action2 = action;
        boolean z = action2.getActionIntent() == null;
        new RemoteViews(context2.getPackageName(), R.layout.notification_media_action);
        RemoteViews remoteViews2 = remoteViews;
        remoteViews2.setImageViewResource(R.id.action0, action2.getIcon());
        if (!z) {
            remoteViews2.setOnClickPendingIntent(R.id.action0, action2.getActionIntent());
        }
        if (Build.VERSION.SDK_INT >= 15) {
            remoteViews2.setContentDescription(R.id.action0, action2.getTitle());
        }
        return remoteViews2;
    }

    private static int getBigLayoutResource(int i) {
        if (i <= 3) {
            return R.layout.notification_template_big_media_narrow;
        }
        return R.layout.notification_template_big_media;
    }

    private static RemoteViews applyStandardTemplate(Context context, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, Bitmap bitmap, CharSequence charSequence4, boolean z, long j, int i2, boolean z2) {
        RemoteViews remoteViews;
        Context context2 = context;
        CharSequence charSequence5 = charSequence;
        CharSequence charSequence6 = charSequence2;
        CharSequence charSequence7 = charSequence3;
        int i3 = i;
        Bitmap bitmap2 = bitmap;
        CharSequence charSequence8 = charSequence4;
        boolean z3 = z;
        long j2 = j;
        boolean z4 = z2;
        new RemoteViews(context2.getPackageName(), i2);
        RemoteViews remoteViews2 = remoteViews;
        boolean z5 = false;
        boolean z6 = false;
        if (bitmap2 == null || Build.VERSION.SDK_INT < 16) {
            remoteViews2.setViewVisibility(R.id.icon, 8);
        } else {
            remoteViews2.setViewVisibility(R.id.icon, 0);
            remoteViews2.setImageViewBitmap(R.id.icon, bitmap2);
        }
        if (charSequence5 != null) {
            remoteViews2.setTextViewText(R.id.title, charSequence5);
        }
        if (charSequence6 != null) {
            remoteViews2.setTextViewText(R.id.text, charSequence6);
            z5 = true;
        }
        if (charSequence7 != null) {
            remoteViews2.setTextViewText(R.id.info, charSequence7);
            remoteViews2.setViewVisibility(R.id.info, 0);
            z5 = true;
        } else if (i3 > 0) {
            if (i3 > context2.getResources().getInteger(R.integer.status_bar_notification_info_maxnum)) {
                remoteViews2.setTextViewText(R.id.info, context2.getResources().getString(R.string.status_bar_notification_info_overflow));
            } else {
                remoteViews2.setTextViewText(R.id.info, NumberFormat.getIntegerInstance().format((long) i3));
            }
            remoteViews2.setViewVisibility(R.id.info, 0);
            z5 = true;
        } else {
            remoteViews2.setViewVisibility(R.id.info, 8);
        }
        if (charSequence8 != null && Build.VERSION.SDK_INT >= 16) {
            remoteViews2.setTextViewText(R.id.text, charSequence8);
            if (charSequence6 != null) {
                remoteViews2.setTextViewText(R.id.text2, charSequence6);
                remoteViews2.setViewVisibility(R.id.text2, 0);
                z6 = true;
            } else {
                remoteViews2.setViewVisibility(R.id.text2, 8);
            }
        }
        if (z6 && Build.VERSION.SDK_INT >= 16) {
            if (z4) {
                remoteViews2.setTextViewTextSize(R.id.text, 0, (float) context2.getResources().getDimensionPixelSize(R.dimen.notification_subtext_size));
            }
            remoteViews2.setViewPadding(R.id.line1, 0, 0, 0, 0);
        }
        if (j2 != 0) {
            if (z3) {
                remoteViews2.setViewVisibility(R.id.chronometer, 0);
                remoteViews2.setLong(R.id.chronometer, "setBase", j2 + (SystemClock.elapsedRealtime() - System.currentTimeMillis()));
                remoteViews2.setBoolean(R.id.chronometer, "setStarted", true);
            } else {
                remoteViews2.setViewVisibility(R.id.time, 0);
                remoteViews2.setLong(R.id.time, "setTime", j2);
            }
        }
        remoteViews2.setViewVisibility(R.id.line3, z5 ? 0 : 8);
        return remoteViews2;
    }
}
