package android.support.v7.view.menu;

import android.content.Context;
import android.os.Build;
import android.support.v4.internal.view.SupportMenu;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public final class MenuWrapperFactory {
    private MenuWrapperFactory() {
    }

    public static Menu wrapSupportMenu(Context context, SupportMenu supportMenu) {
        Throwable th;
        Menu menu;
        Context context2 = context;
        SupportMenu supportMenu2 = supportMenu;
        if (Build.VERSION.SDK_INT >= 14) {
            new MenuWrapperICS(context2, supportMenu2);
            return menu;
        }
        Throwable th2 = th;
        new UnsupportedOperationException();
        throw th2;
    }

    public static MenuItem wrapSupportMenuItem(Context context, SupportMenuItem supportMenuItem) {
        Throwable th;
        MenuItem menuItem;
        MenuItem menuItem2;
        Context context2 = context;
        SupportMenuItem supportMenuItem2 = supportMenuItem;
        if (Build.VERSION.SDK_INT >= 16) {
            new MenuItemWrapperJB(context2, supportMenuItem2);
            return menuItem2;
        } else if (Build.VERSION.SDK_INT >= 14) {
            new MenuItemWrapperICS(context2, supportMenuItem2);
            return menuItem;
        } else {
            Throwable th2 = th;
            new UnsupportedOperationException();
            throw th2;
        }
    }

    public static SubMenu wrapSupportSubMenu(Context context, SupportSubMenu supportSubMenu) {
        Throwable th;
        SubMenu subMenu;
        Context context2 = context;
        SupportSubMenu supportSubMenu2 = supportSubMenu;
        if (Build.VERSION.SDK_INT >= 14) {
            new SubMenuWrapperICS(context2, supportSubMenu2);
            return subMenu;
        }
        Throwable th2 = th;
        new UnsupportedOperationException();
        throw th2;
    }
}
