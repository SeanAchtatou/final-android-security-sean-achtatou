package android.support.v7.view.menu;

import android.content.DialogInterface;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.support.v7.appcompat.R;
import android.support.v7.view.menu.MenuPresenter;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

class MenuDialogHelper implements DialogInterface.OnKeyListener, DialogInterface.OnClickListener, DialogInterface.OnDismissListener, MenuPresenter.Callback {
    private AlertDialog mDialog;
    private MenuBuilder mMenu;
    ListMenuPresenter mPresenter;
    private MenuPresenter.Callback mPresenterCallback;

    public MenuDialogHelper(MenuBuilder menuBuilder) {
        this.mMenu = menuBuilder;
    }

    public void show(IBinder iBinder) {
        AlertDialog.Builder builder;
        ListMenuPresenter listMenuPresenter;
        IBinder iBinder2 = iBinder;
        MenuBuilder menuBuilder = this.mMenu;
        new AlertDialog.Builder(menuBuilder.getContext());
        AlertDialog.Builder builder2 = builder;
        new ListMenuPresenter(builder2.getContext(), R.layout.abc_list_menu_item_layout);
        this.mPresenter = listMenuPresenter;
        this.mPresenter.setCallback(this);
        this.mMenu.addMenuPresenter(this.mPresenter);
        AlertDialog.Builder adapter = builder2.setAdapter(this.mPresenter.getAdapter(), this);
        View headerView = menuBuilder.getHeaderView();
        if (headerView != null) {
            AlertDialog.Builder customTitle = builder2.setCustomTitle(headerView);
        } else {
            AlertDialog.Builder title = builder2.setIcon(menuBuilder.getHeaderIcon()).setTitle(menuBuilder.getHeaderTitle());
        }
        AlertDialog.Builder onKeyListener = builder2.setOnKeyListener(this);
        this.mDialog = builder2.create();
        this.mDialog.setOnDismissListener(this);
        WindowManager.LayoutParams attributes = this.mDialog.getWindow().getAttributes();
        attributes.type = 1003;
        if (iBinder2 != null) {
            attributes.token = iBinder2;
        }
        attributes.flags |= 131072;
        this.mDialog.show();
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        Window window;
        View decorView;
        KeyEvent.DispatcherState keyDispatcherState;
        View decorView2;
        KeyEvent.DispatcherState keyDispatcherState2;
        DialogInterface dialogInterface2 = dialogInterface;
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (i2 == 82 || i2 == 4) {
            if (keyEvent2.getAction() == 0 && keyEvent2.getRepeatCount() == 0) {
                Window window2 = this.mDialog.getWindow();
                if (!(window2 == null || (decorView2 = window2.getDecorView()) == null || (keyDispatcherState2 = decorView2.getKeyDispatcherState()) == null)) {
                    keyDispatcherState2.startTracking(keyEvent2, this);
                    return true;
                }
            } else if (keyEvent2.getAction() == 1 && !keyEvent2.isCanceled() && (window = this.mDialog.getWindow()) != null && (decorView = window.getDecorView()) != null && (keyDispatcherState = decorView.getKeyDispatcherState()) != null && keyDispatcherState.isTracking(keyEvent2)) {
                this.mMenu.close(true);
                dialogInterface2.dismiss();
                return true;
            }
        }
        return this.mMenu.performShortcut(i2, keyEvent2, 0);
    }

    public void setPresenterCallback(MenuPresenter.Callback callback) {
        this.mPresenterCallback = callback;
    }

    public void dismiss() {
        if (this.mDialog != null) {
            this.mDialog.dismiss();
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        this.mPresenter.onCloseMenu(this.mMenu, true);
    }

    public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        MenuBuilder menuBuilder2 = menuBuilder;
        boolean z2 = z;
        if (z2 || menuBuilder2 == this.mMenu) {
            dismiss();
        }
        if (this.mPresenterCallback != null) {
            this.mPresenterCallback.onCloseMenu(menuBuilder2, z2);
        }
    }

    public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
        MenuBuilder menuBuilder2 = menuBuilder;
        if (this.mPresenterCallback != null) {
            return this.mPresenterCallback.onOpenSubMenu(menuBuilder2);
        }
        return false;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        boolean performItemAction = this.mMenu.performItemAction((MenuItemImpl) this.mPresenter.getAdapter().getItem(i), 0);
    }
}
