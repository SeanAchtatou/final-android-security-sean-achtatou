package android.support.v7.widget;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LongSparseArray;
import android.support.v4.util.Pools;
import android.support.v7.widget.RecyclerView;

class ViewInfoStore {
    private static final boolean DEBUG = false;
    final ArrayMap<RecyclerView.ViewHolder, InfoRecord> mLayoutHolderMap;
    final LongSparseArray<RecyclerView.ViewHolder> mOldChangedHolders;

    interface ProcessCallback {
        void processAppeared(RecyclerView.ViewHolder viewHolder, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        void processDisappeared(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @Nullable RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        void processPersistent(RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, @NonNull RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2);

        void unused(RecyclerView.ViewHolder viewHolder);
    }

    ViewInfoStore() {
        ArrayMap<RecyclerView.ViewHolder, InfoRecord> arrayMap;
        LongSparseArray<RecyclerView.ViewHolder> longSparseArray;
        new ArrayMap<>();
        this.mLayoutHolderMap = arrayMap;
        new LongSparseArray<>();
        this.mOldChangedHolders = longSparseArray;
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.mLayoutHolderMap.clear();
        this.mOldChangedHolders.clear();
    }

    /* access modifiers changed from: package-private */
    public void addToPreLayout(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2 = itemHolderInfo;
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder2);
        if (infoRecord == null) {
            infoRecord = InfoRecord.obtain();
            InfoRecord put = this.mLayoutHolderMap.put(viewHolder2, infoRecord);
        }
        infoRecord.preInfo = itemHolderInfo2;
        infoRecord.flags |= 4;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public RecyclerView.ItemAnimator.ItemHolderInfo popFromPreLayout(RecyclerView.ViewHolder viewHolder) {
        int indexOfKey = this.mLayoutHolderMap.indexOfKey(viewHolder);
        if (indexOfKey < 0) {
            return null;
        }
        InfoRecord valueAt = this.mLayoutHolderMap.valueAt(indexOfKey);
        if (valueAt == null || (valueAt.flags & 4) == 0) {
            return null;
        }
        valueAt.flags &= -5;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo = valueAt.preInfo;
        if (valueAt.flags == 0) {
            InfoRecord removeAt = this.mLayoutHolderMap.removeAt(indexOfKey);
            InfoRecord.recycle(valueAt);
        }
        return itemHolderInfo;
    }

    /* access modifiers changed from: package-private */
    public void addToOldChangeHolders(long j, RecyclerView.ViewHolder viewHolder) {
        this.mOldChangedHolders.put(j, viewHolder);
    }

    /* access modifiers changed from: package-private */
    public void addToAppearedInPreLayoutHolders(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2 = itemHolderInfo;
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder2);
        if (infoRecord == null) {
            infoRecord = InfoRecord.obtain();
            InfoRecord put = this.mLayoutHolderMap.put(viewHolder2, infoRecord);
        }
        infoRecord.flags |= 2;
        infoRecord.preInfo = itemHolderInfo2;
    }

    /* access modifiers changed from: package-private */
    public boolean isInPreLayout(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder);
        return (infoRecord == null || (infoRecord.flags & 4) == 0) ? false : true;
    }

    /* access modifiers changed from: package-private */
    public RecyclerView.ViewHolder getFromOldChangeHolders(long j) {
        return this.mOldChangedHolders.get(j);
    }

    /* access modifiers changed from: package-private */
    public void addToPostLayout(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2 = itemHolderInfo;
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder2);
        if (infoRecord == null) {
            infoRecord = InfoRecord.obtain();
            InfoRecord put = this.mLayoutHolderMap.put(viewHolder2, infoRecord);
        }
        infoRecord.postInfo = itemHolderInfo2;
        infoRecord.flags |= 8;
    }

    /* access modifiers changed from: package-private */
    public void addToDisappearedInLayout(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder2);
        if (infoRecord == null) {
            infoRecord = InfoRecord.obtain();
            InfoRecord put = this.mLayoutHolderMap.put(viewHolder2, infoRecord);
        }
        infoRecord.flags |= 1;
    }

    /* access modifiers changed from: package-private */
    public void removeFromDisappearedInLayout(RecyclerView.ViewHolder viewHolder) {
        InfoRecord infoRecord = this.mLayoutHolderMap.get(viewHolder);
        if (infoRecord != null) {
            infoRecord.flags &= -2;
        }
    }

    /* access modifiers changed from: package-private */
    public void process(ProcessCallback processCallback) {
        ProcessCallback processCallback2 = processCallback;
        for (int size = this.mLayoutHolderMap.size() - 1; size >= 0; size--) {
            RecyclerView.ViewHolder keyAt = this.mLayoutHolderMap.keyAt(size);
            InfoRecord removeAt = this.mLayoutHolderMap.removeAt(size);
            if ((removeAt.flags & 3) == 3) {
                processCallback2.unused(keyAt);
            } else if ((removeAt.flags & 1) != 0) {
                processCallback2.processDisappeared(keyAt, removeAt.preInfo, removeAt.postInfo);
            } else if ((removeAt.flags & 14) == 14) {
                processCallback2.processAppeared(keyAt, removeAt.preInfo, removeAt.postInfo);
            } else if ((removeAt.flags & 12) == 12) {
                processCallback2.processPersistent(keyAt, removeAt.preInfo, removeAt.postInfo);
            } else if ((removeAt.flags & 4) != 0) {
                processCallback2.processDisappeared(keyAt, removeAt.preInfo, null);
            } else if ((removeAt.flags & 8) != 0) {
                processCallback2.processAppeared(keyAt, removeAt.preInfo, removeAt.postInfo);
            } else if ((removeAt.flags & 2) != 0) {
            }
            InfoRecord.recycle(removeAt);
        }
    }

    /* access modifiers changed from: package-private */
    public void removeViewHolder(RecyclerView.ViewHolder viewHolder) {
        RecyclerView.ViewHolder viewHolder2 = viewHolder;
        int size = this.mOldChangedHolders.size() - 1;
        while (true) {
            if (size < 0) {
                break;
            } else if (viewHolder2 == this.mOldChangedHolders.valueAt(size)) {
                this.mOldChangedHolders.removeAt(size);
                break;
            } else {
                size--;
            }
        }
        InfoRecord remove = this.mLayoutHolderMap.remove(viewHolder2);
        if (remove != null) {
            InfoRecord.recycle(remove);
        }
    }

    /* access modifiers changed from: package-private */
    public void onDetach() {
        InfoRecord.drainCache();
    }

    static class InfoRecord {
        static final int FLAG_APPEAR = 2;
        static final int FLAG_APPEAR_AND_DISAPPEAR = 3;
        static final int FLAG_APPEAR_PRE_AND_POST = 14;
        static final int FLAG_DISAPPEARED = 1;
        static final int FLAG_POST = 8;
        static final int FLAG_PRE = 4;
        static final int FLAG_PRE_AND_POST = 12;
        static Pools.Pool<InfoRecord> sPool;
        int flags;
        @Nullable
        RecyclerView.ItemAnimator.ItemHolderInfo postInfo;
        @Nullable
        RecyclerView.ItemAnimator.ItemHolderInfo preInfo;

        static {
            Pools.Pool<InfoRecord> pool;
            new Pools.SimplePool(20);
            sPool = pool;
        }

        private InfoRecord() {
        }

        static InfoRecord obtain() {
            InfoRecord infoRecord;
            InfoRecord infoRecord2;
            InfoRecord acquire = sPool.acquire();
            if (acquire == null) {
                infoRecord = infoRecord2;
                new InfoRecord();
            } else {
                infoRecord = acquire;
            }
            return infoRecord;
        }

        static void recycle(InfoRecord infoRecord) {
            InfoRecord infoRecord2 = infoRecord;
            infoRecord2.flags = 0;
            infoRecord2.preInfo = null;
            infoRecord2.postInfo = null;
            boolean release = sPool.release(infoRecord2);
        }

        static void drainCache() {
            do {
            } while (sPool.acquire() != null);
        }
    }
}
