package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.ColorUtils;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.util.LruCache;
import android.support.v7.appcompat.R;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.WeakHashMap;

public final class AppCompatDrawableManager {
    private static final int[] COLORFILTER_COLOR_BACKGROUND_MULTIPLY;
    private static final int[] COLORFILTER_COLOR_CONTROL_ACTIVATED;
    private static final int[] COLORFILTER_TINT_COLOR_CONTROL_NORMAL;
    private static final ColorFilterLruCache COLOR_FILTER_CACHE;
    private static final boolean DEBUG = false;
    private static final PorterDuff.Mode DEFAULT_MODE = PorterDuff.Mode.SRC_IN;
    private static AppCompatDrawableManager INSTANCE = null;
    private static final String TAG = "TintManager";
    private static final int[] TINT_CHECKABLE_BUTTON_LIST;
    private static final int[] TINT_COLOR_CONTROL_NORMAL;
    private static final int[] TINT_COLOR_CONTROL_STATE_LIST;
    private ArrayList<InflateDelegate> mDelegates;
    private WeakHashMap<Context, SparseArray<ColorStateList>> mTintLists;

    public interface InflateDelegate {
        @Nullable
        Drawable onInflateDrawable(@NonNull Context context, @DrawableRes int i);
    }

    static {
        ColorFilterLruCache colorFilterLruCache;
        new ColorFilterLruCache(6);
        COLOR_FILTER_CACHE = colorFilterLruCache;
        int[] iArr = new int[3];
        iArr[0] = R.drawable.abc_textfield_search_default_mtrl_alpha;
        int[] iArr2 = iArr;
        iArr2[1] = R.drawable.abc_textfield_default_mtrl_alpha;
        int[] iArr3 = iArr2;
        iArr3[2] = R.drawable.abc_ab_share_pack_mtrl_alpha;
        COLORFILTER_TINT_COLOR_CONTROL_NORMAL = iArr3;
        int[] iArr4 = new int[12];
        iArr4[0] = R.drawable.abc_ic_ab_back_mtrl_am_alpha;
        int[] iArr5 = iArr4;
        iArr5[1] = R.drawable.abc_ic_go_search_api_mtrl_alpha;
        int[] iArr6 = iArr5;
        iArr6[2] = R.drawable.abc_ic_search_api_mtrl_alpha;
        int[] iArr7 = iArr6;
        iArr7[3] = R.drawable.abc_ic_commit_search_api_mtrl_alpha;
        int[] iArr8 = iArr7;
        iArr8[4] = R.drawable.abc_ic_clear_mtrl_alpha;
        int[] iArr9 = iArr8;
        iArr9[5] = R.drawable.abc_ic_menu_share_mtrl_alpha;
        int[] iArr10 = iArr9;
        iArr10[6] = R.drawable.abc_ic_menu_copy_mtrl_am_alpha;
        int[] iArr11 = iArr10;
        iArr11[7] = R.drawable.abc_ic_menu_cut_mtrl_alpha;
        int[] iArr12 = iArr11;
        iArr12[8] = R.drawable.abc_ic_menu_selectall_mtrl_alpha;
        int[] iArr13 = iArr12;
        iArr13[9] = R.drawable.abc_ic_menu_paste_mtrl_am_alpha;
        int[] iArr14 = iArr13;
        iArr14[10] = R.drawable.abc_ic_menu_moreoverflow_mtrl_alpha;
        int[] iArr15 = iArr14;
        iArr15[11] = R.drawable.abc_ic_voice_search_api_mtrl_alpha;
        TINT_COLOR_CONTROL_NORMAL = iArr15;
        int[] iArr16 = new int[4];
        iArr16[0] = R.drawable.abc_textfield_activated_mtrl_alpha;
        int[] iArr17 = iArr16;
        iArr17[1] = R.drawable.abc_textfield_search_activated_mtrl_alpha;
        int[] iArr18 = iArr17;
        iArr18[2] = R.drawable.abc_cab_background_top_mtrl_alpha;
        int[] iArr19 = iArr18;
        iArr19[3] = R.drawable.abc_text_cursor_material;
        COLORFILTER_COLOR_CONTROL_ACTIVATED = iArr19;
        int[] iArr20 = new int[3];
        iArr20[0] = R.drawable.abc_popup_background_mtrl_mult;
        int[] iArr21 = iArr20;
        iArr21[1] = R.drawable.abc_cab_background_internal_bg;
        int[] iArr22 = iArr21;
        iArr22[2] = R.drawable.abc_menu_hardkey_panel_mtrl_mult;
        COLORFILTER_COLOR_BACKGROUND_MULTIPLY = iArr22;
        int[] iArr23 = new int[10];
        iArr23[0] = R.drawable.abc_edit_text_material;
        int[] iArr24 = iArr23;
        iArr24[1] = R.drawable.abc_tab_indicator_material;
        int[] iArr25 = iArr24;
        iArr25[2] = R.drawable.abc_textfield_search_material;
        int[] iArr26 = iArr25;
        iArr26[3] = R.drawable.abc_spinner_mtrl_am_alpha;
        int[] iArr27 = iArr26;
        iArr27[4] = R.drawable.abc_spinner_textfield_background_material;
        int[] iArr28 = iArr27;
        iArr28[5] = R.drawable.abc_ratingbar_full_material;
        int[] iArr29 = iArr28;
        iArr29[6] = R.drawable.abc_switch_track_mtrl_alpha;
        int[] iArr30 = iArr29;
        iArr30[7] = R.drawable.abc_switch_thumb_material;
        int[] iArr31 = iArr30;
        iArr31[8] = R.drawable.abc_btn_default_mtrl_shape;
        int[] iArr32 = iArr31;
        iArr32[9] = R.drawable.abc_btn_borderless_material;
        TINT_COLOR_CONTROL_STATE_LIST = iArr32;
        int[] iArr33 = new int[2];
        iArr33[0] = R.drawable.abc_btn_check_material;
        int[] iArr34 = iArr33;
        iArr34[1] = R.drawable.abc_btn_radio_material;
        TINT_CHECKABLE_BUTTON_LIST = iArr34;
    }

    public static AppCompatDrawableManager get() {
        AppCompatDrawableManager appCompatDrawableManager;
        if (INSTANCE == null) {
            new AppCompatDrawableManager();
            INSTANCE = appCompatDrawableManager;
        }
        return INSTANCE;
    }

    public Drawable getDrawable(@NonNull Context context, @DrawableRes int i) {
        return getDrawable(context, i, false);
    }

    public Drawable getDrawable(@NonNull Context context, @DrawableRes int i, boolean z) {
        Drawable drawable;
        Context context2 = context;
        int i2 = i;
        boolean z2 = z;
        if (this.mDelegates != null) {
            int size = this.mDelegates.size();
            for (int i3 = 0; i3 < size; i3++) {
                Drawable onInflateDrawable = this.mDelegates.get(i3).onInflateDrawable(context2, i2);
                if (onInflateDrawable != null) {
                    return onInflateDrawable;
                }
            }
        }
        Drawable drawable2 = ContextCompat.getDrawable(context2, i2);
        if (drawable2 != null) {
            if (Build.VERSION.SDK_INT >= 8) {
                drawable2 = drawable2.mutate();
            }
            ColorStateList tintList = getTintList(context2, i2);
            if (tintList != null) {
                drawable2 = DrawableCompat.wrap(drawable2);
                DrawableCompat.setTintList(drawable2, tintList);
                PorterDuff.Mode tintMode = getTintMode(i2);
                if (tintMode != null) {
                    DrawableCompat.setTintMode(drawable2, tintMode);
                }
            } else if (i2 == R.drawable.abc_cab_background_top_material) {
                Drawable drawable3 = drawable;
                Drawable[] drawableArr = new Drawable[2];
                drawableArr[0] = getDrawable(context2, R.drawable.abc_cab_background_internal_bg);
                Drawable[] drawableArr2 = drawableArr;
                drawableArr2[1] = getDrawable(context2, R.drawable.abc_cab_background_top_mtrl_alpha);
                new LayerDrawable(drawableArr2);
                return drawable3;
            } else if (i2 == R.drawable.abc_seekbar_track_material) {
                LayerDrawable layerDrawable = (LayerDrawable) drawable2;
                setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908288), ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal), DEFAULT_MODE);
                setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908303), ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal), DEFAULT_MODE);
                setPorterDuffColorFilter(layerDrawable.findDrawableByLayerId(16908301), ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated), DEFAULT_MODE);
            } else if (!tintDrawableUsingColorFilter(context2, i2, drawable2) && z2) {
                drawable2 = null;
            }
        }
        return drawable2;
    }

    public final boolean tintDrawableUsingColorFilter(@NonNull Context context, @DrawableRes int i, @NonNull Drawable drawable) {
        Context context2 = context;
        int i2 = i;
        Drawable drawable2 = drawable;
        PorterDuff.Mode mode = DEFAULT_MODE;
        boolean z = false;
        int i3 = 0;
        int i4 = -1;
        if (arrayContains(COLORFILTER_TINT_COLOR_CONTROL_NORMAL, i2)) {
            i3 = R.attr.colorControlNormal;
            z = true;
        } else if (arrayContains(COLORFILTER_COLOR_CONTROL_ACTIVATED, i2)) {
            i3 = R.attr.colorControlActivated;
            z = true;
        } else if (arrayContains(COLORFILTER_COLOR_BACKGROUND_MULTIPLY, i2)) {
            i3 = 16842801;
            z = true;
            mode = PorterDuff.Mode.MULTIPLY;
        } else if (i2 == R.drawable.abc_list_divider_mtrl_alpha) {
            i3 = 16842800;
            z = true;
            i4 = Math.round(40.8f);
        }
        if (!z) {
            return false;
        }
        drawable2.setColorFilter(getPorterDuffColorFilter(ThemeUtils.getThemeAttrColor(context2, i3), mode));
        if (i4 != -1) {
            drawable2.setAlpha(i4);
        }
        return true;
    }

    public void addDelegate(@NonNull InflateDelegate inflateDelegate) {
        ArrayList<InflateDelegate> arrayList;
        InflateDelegate inflateDelegate2 = inflateDelegate;
        if (this.mDelegates == null) {
            new ArrayList<>();
            this.mDelegates = arrayList;
        }
        if (!this.mDelegates.contains(inflateDelegate2)) {
            boolean add = this.mDelegates.add(inflateDelegate2);
        }
    }

    public void removeDelegate(@NonNull InflateDelegate inflateDelegate) {
        InflateDelegate inflateDelegate2 = inflateDelegate;
        if (this.mDelegates != null) {
            boolean remove = this.mDelegates.remove(inflateDelegate2);
        }
    }

    private static boolean arrayContains(int[] iArr, int i) {
        int i2 = i;
        int[] iArr2 = iArr;
        int length = iArr2.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (iArr2[i3] == i2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public final PorterDuff.Mode getTintMode(int i) {
        PorterDuff.Mode mode = null;
        if (i == R.drawable.abc_switch_thumb_material) {
            mode = PorterDuff.Mode.MULTIPLY;
        }
        return mode;
    }

    public final ColorStateList getTintList(@NonNull Context context, @DrawableRes int i) {
        Context context2 = context;
        int i2 = i;
        ColorStateList tintListFromCache = getTintListFromCache(context2, i2);
        if (tintListFromCache == null) {
            if (i2 == R.drawable.abc_edit_text_material) {
                tintListFromCache = createEditTextColorStateList(context2);
            } else if (i2 == R.drawable.abc_switch_track_mtrl_alpha) {
                tintListFromCache = createSwitchTrackColorStateList(context2);
            } else if (i2 == R.drawable.abc_switch_thumb_material) {
                tintListFromCache = createSwitchThumbColorStateList(context2);
            } else if (i2 == R.drawable.abc_btn_default_mtrl_shape || i2 == R.drawable.abc_btn_borderless_material) {
                tintListFromCache = createDefaultButtonColorStateList(context2);
            } else if (i2 == R.drawable.abc_btn_colored_material) {
                tintListFromCache = createColoredButtonColorStateList(context2);
            } else if (i2 == R.drawable.abc_spinner_mtrl_am_alpha || i2 == R.drawable.abc_spinner_textfield_background_material) {
                tintListFromCache = createSpinnerColorStateList(context2);
            } else if (arrayContains(TINT_COLOR_CONTROL_NORMAL, i2)) {
                tintListFromCache = ThemeUtils.getThemeAttrColorStateList(context2, R.attr.colorControlNormal);
            } else if (arrayContains(TINT_COLOR_CONTROL_STATE_LIST, i2)) {
                tintListFromCache = createDefaultColorStateList(context2);
            } else if (arrayContains(TINT_CHECKABLE_BUTTON_LIST, i2)) {
                tintListFromCache = createCheckableButtonColorStateList(context2);
            } else if (i2 == R.drawable.abc_seekbar_thumb_material) {
                tintListFromCache = createSeekbarThumbColorStateList(context2);
            }
            if (tintListFromCache != null) {
                addTintListToCache(context2, i2, tintListFromCache);
            }
        }
        return tintListFromCache;
    }

    private ColorStateList getTintListFromCache(@NonNull Context context, @DrawableRes int i) {
        Context context2 = context;
        int i2 = i;
        if (this.mTintLists == null) {
            return null;
        }
        SparseArray sparseArray = this.mTintLists.get(context2);
        return sparseArray != null ? (ColorStateList) sparseArray.get(i2) : null;
    }

    private void addTintListToCache(@NonNull Context context, @DrawableRes int i, @NonNull ColorStateList colorStateList) {
        SparseArray sparseArray;
        WeakHashMap<Context, SparseArray<ColorStateList>> weakHashMap;
        Context context2 = context;
        int i2 = i;
        ColorStateList colorStateList2 = colorStateList;
        if (this.mTintLists == null) {
            new WeakHashMap<>();
            this.mTintLists = weakHashMap;
        }
        SparseArray sparseArray2 = this.mTintLists.get(context2);
        if (sparseArray2 == null) {
            new SparseArray();
            sparseArray2 = sparseArray;
            SparseArray<ColorStateList> put = this.mTintLists.put(context2, sparseArray2);
        }
        sparseArray2.append(i2, colorStateList2);
    }

    private ColorStateList createDefaultColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int themeAttrColor = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal);
        int themeAttrColor2 = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
        int[][] iArr = new int[7][];
        int[] iArr2 = new int[7];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorControlNormal);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.FOCUSED_STATE_SET;
        iArr2[i] = themeAttrColor2;
        int i2 = i + 1;
        iArr[i2] = ThemeUtils.ACTIVATED_STATE_SET;
        iArr2[i2] = themeAttrColor2;
        int i3 = i2 + 1;
        iArr[i3] = ThemeUtils.PRESSED_STATE_SET;
        iArr2[i3] = themeAttrColor2;
        int i4 = i3 + 1;
        iArr[i4] = ThemeUtils.CHECKED_STATE_SET;
        iArr2[i4] = themeAttrColor2;
        int i5 = i4 + 1;
        iArr[i5] = ThemeUtils.SELECTED_STATE_SET;
        iArr2[i5] = themeAttrColor2;
        int i6 = i5 + 1;
        iArr[i6] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i6] = themeAttrColor;
        int i7 = i6 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createCheckableButtonColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorControlNormal);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.CHECKED_STATE_SET;
        iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
        int i2 = i + 1;
        iArr[i2] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i2] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal);
        int i3 = i2 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createSwitchTrackColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getThemeAttrColor(context2, 16842800, 0.1f);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.CHECKED_STATE_SET;
        iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated, 0.3f);
        int i2 = i + 1;
        iArr[i2] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i2] = ThemeUtils.getThemeAttrColor(context2, 16842800, 0.3f);
        int i3 = i2 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createSwitchThumbColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        ColorStateList themeAttrColorStateList = ThemeUtils.getThemeAttrColorStateList(context2, R.attr.colorSwitchThumbNormal);
        if (themeAttrColorStateList == null || !themeAttrColorStateList.isStateful()) {
            iArr[0] = ThemeUtils.DISABLED_STATE_SET;
            iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorSwitchThumbNormal);
            int i = 0 + 1;
            iArr[i] = ThemeUtils.CHECKED_STATE_SET;
            iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
            int i2 = i + 1;
            iArr[i2] = ThemeUtils.EMPTY_STATE_SET;
            iArr2[i2] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorSwitchThumbNormal);
            int i3 = i2 + 1;
        } else {
            iArr[0] = ThemeUtils.DISABLED_STATE_SET;
            iArr2[0] = themeAttrColorStateList.getColorForState(iArr[0], 0);
            int i4 = 0 + 1;
            iArr[i4] = ThemeUtils.CHECKED_STATE_SET;
            iArr2[i4] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
            int i5 = i4 + 1;
            iArr[i5] = ThemeUtils.EMPTY_STATE_SET;
            iArr2[i5] = themeAttrColorStateList.getDefaultColor();
            int i6 = i5 + 1;
        }
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createEditTextColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorControlNormal);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.NOT_PRESSED_OR_FOCUSED_STATE_SET;
        iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal);
        int i2 = i + 1;
        iArr[i2] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i2] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
        int i3 = i2 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createDefaultButtonColorStateList(Context context) {
        return createButtonColorStateList(context, R.attr.colorButtonNormal);
    }

    private ColorStateList createColoredButtonColorStateList(Context context) {
        return createButtonColorStateList(context, R.attr.colorAccent);
    }

    private ColorStateList createButtonColorStateList(Context context, int i) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[4][];
        int[] iArr2 = new int[4];
        int themeAttrColor = ThemeUtils.getThemeAttrColor(context2, i);
        int themeAttrColor2 = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlHighlight);
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorButtonNormal);
        int i2 = 0 + 1;
        iArr[i2] = ThemeUtils.PRESSED_STATE_SET;
        iArr2[i2] = ColorUtils.compositeColors(themeAttrColor2, themeAttrColor);
        int i3 = i2 + 1;
        iArr[i3] = ThemeUtils.FOCUSED_STATE_SET;
        iArr2[i3] = ColorUtils.compositeColors(themeAttrColor2, themeAttrColor);
        int i4 = i3 + 1;
        iArr[i4] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i4] = themeAttrColor;
        int i5 = i4 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createSpinnerColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[3][];
        int[] iArr2 = new int[3];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorControlNormal);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.NOT_PRESSED_OR_FOCUSED_STATE_SET;
        iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlNormal);
        int i2 = i + 1;
        iArr[i2] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i2] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
        int i3 = i2 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private ColorStateList createSeekbarThumbColorStateList(Context context) {
        ColorStateList colorStateList;
        Context context2 = context;
        int[][] iArr = new int[2][];
        int[] iArr2 = new int[2];
        iArr[0] = ThemeUtils.DISABLED_STATE_SET;
        iArr2[0] = ThemeUtils.getDisabledThemeAttrColor(context2, R.attr.colorControlActivated);
        int i = 0 + 1;
        iArr[i] = ThemeUtils.EMPTY_STATE_SET;
        iArr2[i] = ThemeUtils.getThemeAttrColor(context2, R.attr.colorControlActivated);
        int i2 = i + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private static class ColorFilterLruCache extends LruCache<Integer, PorterDuffColorFilter> {
        public ColorFilterLruCache(int i) {
            super(i);
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter get(int i, PorterDuff.Mode mode) {
            return (PorterDuffColorFilter) get(Integer.valueOf(generateCacheKey(i, mode)));
        }

        /* access modifiers changed from: package-private */
        public PorterDuffColorFilter put(int i, PorterDuff.Mode mode, PorterDuffColorFilter porterDuffColorFilter) {
            return (PorterDuffColorFilter) put(Integer.valueOf(generateCacheKey(i, mode)), porterDuffColorFilter);
        }

        private static int generateCacheKey(int i, PorterDuff.Mode mode) {
            return (31 * ((31 * 1) + i)) + mode.hashCode();
        }
    }

    public static void tintDrawable(Drawable drawable, TintInfo tintInfo, int[] iArr) {
        Drawable drawable2 = drawable;
        TintInfo tintInfo2 = tintInfo;
        int[] iArr2 = iArr;
        if (!shouldMutateBackground(drawable2) || drawable2.mutate() == drawable2) {
            if (tintInfo2.mHasTintList || tintInfo2.mHasTintMode) {
                drawable2.setColorFilter(createTintFilter(tintInfo2.mHasTintList ? tintInfo2.mTintList : null, tintInfo2.mHasTintMode ? tintInfo2.mTintMode : DEFAULT_MODE, iArr2));
            } else {
                drawable2.clearColorFilter();
            }
            if (Build.VERSION.SDK_INT <= 10) {
                drawable2.invalidateSelf();
                return;
            }
            return;
        }
        int d = Log.d(TAG, "Mutated drawable is not the same instance as the input.");
    }

    private static boolean shouldMutateBackground(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (Build.VERSION.SDK_INT >= 16) {
            return true;
        }
        if (drawable2 instanceof LayerDrawable) {
            return Build.VERSION.SDK_INT >= 16;
        } else if (drawable2 instanceof InsetDrawable) {
            return Build.VERSION.SDK_INT >= 14;
        } else {
            if (drawable2 instanceof DrawableContainer) {
                Drawable.ConstantState constantState = drawable2.getConstantState();
                if (constantState instanceof DrawableContainer.DrawableContainerState) {
                    Drawable[] children = ((DrawableContainer.DrawableContainerState) constantState).getChildren();
                    int length = children.length;
                    for (int i = 0; i < length; i++) {
                        if (!shouldMutateBackground(children[i])) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }

    private static PorterDuffColorFilter createTintFilter(ColorStateList colorStateList, PorterDuff.Mode mode, int[] iArr) {
        ColorStateList colorStateList2 = colorStateList;
        PorterDuff.Mode mode2 = mode;
        int[] iArr2 = iArr;
        if (colorStateList2 == null || mode2 == null) {
            return null;
        }
        return getPorterDuffColorFilter(colorStateList2.getColorForState(iArr2, 0), mode2);
    }

    private static PorterDuffColorFilter getPorterDuffColorFilter(int i, PorterDuff.Mode mode) {
        PorterDuffColorFilter porterDuffColorFilter;
        int i2 = i;
        PorterDuff.Mode mode2 = mode;
        PorterDuffColorFilter porterDuffColorFilter2 = COLOR_FILTER_CACHE.get(i2, mode2);
        if (porterDuffColorFilter2 == null) {
            new PorterDuffColorFilter(i2, mode2);
            porterDuffColorFilter2 = porterDuffColorFilter;
            PorterDuffColorFilter put = COLOR_FILTER_CACHE.put(i2, mode2, porterDuffColorFilter2);
        }
        return porterDuffColorFilter2;
    }

    private static void setPorterDuffColorFilter(Drawable drawable, int i, PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        drawable.setColorFilter(getPorterDuffColorFilter(i, mode2 == null ? DEFAULT_MODE : mode2));
    }
}
