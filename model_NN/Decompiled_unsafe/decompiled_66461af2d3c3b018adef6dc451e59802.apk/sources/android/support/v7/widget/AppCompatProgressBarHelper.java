package android.support.v7.widget;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Shader;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.support.v4.graphics.drawable.DrawableWrapper;
import android.util.AttributeSet;
import android.widget.ProgressBar;

class AppCompatProgressBarHelper {
    private static final int[] TINT_ATTRS = {16843067, 16843068};
    private Bitmap mSampleTile;
    final TintManager mTintManager;
    private final ProgressBar mView;

    AppCompatProgressBarHelper(ProgressBar progressBar, TintManager tintManager) {
        this.mView = progressBar;
        this.mTintManager = tintManager;
    }

    /* access modifiers changed from: package-private */
    public void loadFromAttributes(AttributeSet attributeSet, int i) {
        TintTypedArray obtainStyledAttributes = TintTypedArray.obtainStyledAttributes(this.mView.getContext(), attributeSet, TINT_ATTRS, i, 0);
        Drawable drawableIfKnown = obtainStyledAttributes.getDrawableIfKnown(0);
        if (drawableIfKnown != null) {
            this.mView.setIndeterminateDrawable(tileifyIndeterminate(drawableIfKnown));
        }
        Drawable drawableIfKnown2 = obtainStyledAttributes.getDrawableIfKnown(1);
        if (drawableIfKnown2 != null) {
            this.mView.setProgressDrawable(tileify(drawableIfKnown2, false));
        }
        obtainStyledAttributes.recycle();
    }

    private Drawable tileify(Drawable drawable, boolean z) {
        ShapeDrawable shapeDrawable;
        Shader shader;
        ShapeDrawable shapeDrawable2;
        ShapeDrawable shapeDrawable3;
        LayerDrawable layerDrawable;
        Drawable drawable2 = drawable;
        boolean z2 = z;
        if (drawable2 instanceof DrawableWrapper) {
            Drawable wrappedDrawable = ((DrawableWrapper) drawable2).getWrappedDrawable();
            if (wrappedDrawable != null) {
                ((DrawableWrapper) drawable2).setWrappedDrawable(tileify(wrappedDrawable, z2));
            }
        } else if (drawable2 instanceof LayerDrawable) {
            LayerDrawable layerDrawable2 = (LayerDrawable) drawable2;
            int numberOfLayers = layerDrawable2.getNumberOfLayers();
            Drawable[] drawableArr = new Drawable[numberOfLayers];
            for (int i = 0; i < numberOfLayers; i++) {
                int id = layerDrawable2.getId(i);
                drawableArr[i] = tileify(layerDrawable2.getDrawable(i), id == 16908301 || id == 16908303);
            }
            new LayerDrawable(drawableArr);
            LayerDrawable layerDrawable3 = layerDrawable;
            for (int i2 = 0; i2 < numberOfLayers; i2++) {
                layerDrawable3.setId(i2, layerDrawable2.getId(i2));
            }
            return layerDrawable3;
        } else if (drawable2 instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable2).getBitmap();
            if (this.mSampleTile == null) {
                this.mSampleTile = bitmap;
            }
            new ShapeDrawable(getDrawableShape());
            ShapeDrawable shapeDrawable4 = shapeDrawable;
            new BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.CLAMP);
            Shader shader2 = shapeDrawable4.getPaint().setShader(shader);
            if (z2) {
                shapeDrawable2 = shapeDrawable3;
                new ClipDrawable(shapeDrawable4, 3, 1);
            } else {
                shapeDrawable2 = shapeDrawable4;
            }
            return shapeDrawable2;
        }
        return drawable2;
    }

    private Drawable tileifyIndeterminate(Drawable drawable) {
        AnimationDrawable animationDrawable;
        AnimationDrawable animationDrawable2 = drawable;
        if (animationDrawable2 instanceof AnimationDrawable) {
            AnimationDrawable animationDrawable3 = (AnimationDrawable) animationDrawable2;
            int numberOfFrames = animationDrawable3.getNumberOfFrames();
            new AnimationDrawable();
            AnimationDrawable animationDrawable4 = animationDrawable;
            animationDrawable4.setOneShot(animationDrawable3.isOneShot());
            for (int i = 0; i < numberOfFrames; i++) {
                Drawable tileify = tileify(animationDrawable3.getFrame(i), true);
                boolean level = tileify.setLevel(10000);
                animationDrawable4.addFrame(tileify, animationDrawable3.getDuration(i));
            }
            boolean level2 = animationDrawable4.setLevel(10000);
            animationDrawable2 = animationDrawable4;
        }
        return animationDrawable2;
    }

    private Shape getDrawableShape() {
        Shape shape;
        new RoundRectShape(new float[]{5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f}, null, null);
        return shape;
    }

    /* access modifiers changed from: package-private */
    public Bitmap getSampleTime() {
        return this.mSampleTile;
    }
}
