package android.support.v7.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.InputDeviceCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat extends ViewGroup {
    public static final int HORIZONTAL = 0;
    private static final int INDEX_BOTTOM = 2;
    private static final int INDEX_CENTER_VERTICAL = 0;
    private static final int INDEX_FILL = 3;
    private static final int INDEX_TOP = 1;
    public static final int SHOW_DIVIDER_BEGINNING = 1;
    public static final int SHOW_DIVIDER_END = 4;
    public static final int SHOW_DIVIDER_MIDDLE = 2;
    public static final int SHOW_DIVIDER_NONE = 0;
    public static final int VERTICAL = 1;
    private static final int VERTICAL_GRAVITY_COUNT = 4;
    private boolean mBaselineAligned;
    private int mBaselineAlignedChildIndex;
    private int mBaselineChildTop;
    private Drawable mDivider;
    private int mDividerHeight;
    private int mDividerPadding;
    private int mDividerWidth;
    private int mGravity;
    private int[] mMaxAscent;
    private int[] mMaxDescent;
    private int mOrientation;
    private int mShowDividers;
    private int mTotalLength;
    private boolean mUseLargestChild;
    private float mWeightSum;

    @Retention(RetentionPolicy.SOURCE)
    public @interface DividerMode {
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface OrientationMode {
    }

    public LinearLayoutCompat(Context context) {
        this(context, null);
    }

    public LinearLayoutCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public LinearLayoutCompat(android.content.Context r13, android.util.AttributeSet r14, int r15) {
        /*
            r12 = this;
            r0 = r12
            r1 = r13
            r2 = r14
            r3 = r15
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r3
            r7.<init>(r8, r9, r10)
            r7 = r0
            r8 = 1
            r7.mBaselineAligned = r8
            r7 = r0
            r8 = -1
            r7.mBaselineAlignedChildIndex = r8
            r7 = r0
            r8 = 0
            r7.mBaselineChildTop = r8
            r7 = r0
            r8 = 8388659(0x800033, float:1.1755015E-38)
            r7.mGravity = r8
            r7 = r1
            r8 = r2
            int[] r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat
            r10 = r3
            r11 = 0
            android.support.v7.widget.TintTypedArray r7 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r7, r8, r9, r10, r11)
            r4 = r7
            r7 = r4
            int r8 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_android_orientation
            r9 = -1
            int r7 = r7.getInt(r8, r9)
            r5 = r7
            r7 = r5
            if (r7 < 0) goto L_0x0039
            r7 = r0
            r8 = r5
            r7.setOrientation(r8)
        L_0x0039:
            r7 = r4
            int r8 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_android_gravity
            r9 = -1
            int r7 = r7.getInt(r8, r9)
            r5 = r7
            r7 = r5
            if (r7 < 0) goto L_0x004a
            r7 = r0
            r8 = r5
            r7.setGravity(r8)
        L_0x004a:
            r7 = r4
            int r8 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_android_baselineAligned
            r9 = 1
            boolean r7 = r7.getBoolean(r8, r9)
            r6 = r7
            r7 = r6
            if (r7 != 0) goto L_0x005b
            r7 = r0
            r8 = r6
            r7.setBaselineAligned(r8)
        L_0x005b:
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_android_weightSum
            r10 = -1082130432(0xffffffffbf800000, float:-1.0)
            float r8 = r8.getFloat(r9, r10)
            r7.mWeightSum = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex
            r10 = -1
            int r8 = r8.getInt(r9, r10)
            r7.mBaselineAlignedChildIndex = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_measureWithLargestChild
            r10 = 0
            boolean r8 = r8.getBoolean(r9, r10)
            r7.mUseLargestChild = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_divider
            android.graphics.drawable.Drawable r8 = r8.getDrawable(r9)
            r7.setDividerDrawable(r8)
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_showDividers
            r10 = 0
            int r8 = r8.getInt(r9, r10)
            r7.mShowDividers = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_dividerPadding
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mDividerPadding = r8
            r7 = r4
            r7.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.LinearLayoutCompat.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setShowDividers(int i) {
        int i2 = i;
        if (i2 != this.mShowDividers) {
            requestLayout();
        }
        this.mShowDividers = i2;
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public int getShowDividers() {
        return this.mShowDividers;
    }

    public Drawable getDividerDrawable() {
        return this.mDivider;
    }

    public void setDividerDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (drawable2 != this.mDivider) {
            this.mDivider = drawable2;
            if (drawable2 != null) {
                this.mDividerWidth = drawable2.getIntrinsicWidth();
                this.mDividerHeight = drawable2.getIntrinsicHeight();
            } else {
                this.mDividerWidth = 0;
                this.mDividerHeight = 0;
            }
            setWillNotDraw(drawable2 == null);
            requestLayout();
        }
    }

    public void setDividerPadding(int i) {
        this.mDividerPadding = i;
    }

    public int getDividerPadding() {
        return this.mDividerPadding;
    }

    public int getDividerWidth() {
        return this.mDividerWidth;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Canvas canvas2 = canvas;
        if (this.mDivider != null) {
            if (this.mOrientation == 1) {
                drawDividersVertical(canvas2);
            } else {
                drawDividersHorizontal(canvas2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void drawDividersVertical(Canvas canvas) {
        int bottom;
        Canvas canvas2 = canvas;
        int virtualChildCount = getVirtualChildCount();
        for (int i = 0; i < virtualChildCount; i++) {
            View virtualChildAt = getVirtualChildAt(i);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i))) {
                drawHorizontalDivider(canvas2, (virtualChildAt.getTop() - ((LayoutParams) virtualChildAt.getLayoutParams()).topMargin) - this.mDividerHeight);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 == null) {
                bottom = (getHeight() - getPaddingBottom()) - this.mDividerHeight;
            } else {
                bottom = virtualChildAt2.getBottom() + ((LayoutParams) virtualChildAt2.getLayoutParams()).bottomMargin;
            }
            drawHorizontalDivider(canvas2, bottom);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawDividersHorizontal(Canvas canvas) {
        int right;
        int left;
        Canvas canvas2 = canvas;
        int virtualChildCount = getVirtualChildCount();
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        for (int i = 0; i < virtualChildCount; i++) {
            View virtualChildAt = getVirtualChildAt(i);
            if (!(virtualChildAt == null || virtualChildAt.getVisibility() == 8 || !hasDividerBeforeChildAt(i))) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (isLayoutRtl) {
                    left = virtualChildAt.getRight() + layoutParams.rightMargin;
                } else {
                    left = (virtualChildAt.getLeft() - layoutParams.leftMargin) - this.mDividerWidth;
                }
                drawVerticalDivider(canvas2, left);
            }
        }
        if (hasDividerBeforeChildAt(virtualChildCount)) {
            View virtualChildAt2 = getVirtualChildAt(virtualChildCount - 1);
            if (virtualChildAt2 != null) {
                LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                if (isLayoutRtl) {
                    right = (virtualChildAt2.getLeft() - layoutParams2.leftMargin) - this.mDividerWidth;
                } else {
                    right = virtualChildAt2.getRight() + layoutParams2.rightMargin;
                }
            } else if (isLayoutRtl) {
                right = getPaddingLeft();
            } else {
                right = (getWidth() - getPaddingRight()) - this.mDividerWidth;
            }
            drawVerticalDivider(canvas2, right);
        }
    }

    /* access modifiers changed from: package-private */
    public void drawHorizontalDivider(Canvas canvas, int i) {
        int i2 = i;
        this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, i2, (getWidth() - getPaddingRight()) - this.mDividerPadding, i2 + this.mDividerHeight);
        this.mDivider.draw(canvas);
    }

    /* access modifiers changed from: package-private */
    public void drawVerticalDivider(Canvas canvas, int i) {
        int i2 = i;
        this.mDivider.setBounds(i2, getPaddingTop() + this.mDividerPadding, i2 + this.mDividerWidth, (getHeight() - getPaddingBottom()) - this.mDividerPadding);
        this.mDivider.draw(canvas);
    }

    public boolean isBaselineAligned() {
        return this.mBaselineAligned;
    }

    public void setBaselineAligned(boolean z) {
        this.mBaselineAligned = z;
    }

    public boolean isMeasureWithLargestChildEnabled() {
        return this.mUseLargestChild;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.mUseLargestChild = z;
    }

    public int getBaseline() {
        int i;
        Throwable th;
        Throwable th2;
        if (this.mBaselineAlignedChildIndex < 0) {
            return super.getBaseline();
        }
        if (getChildCount() <= this.mBaselineAlignedChildIndex) {
            Throwable th3 = th2;
            new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
            throw th3;
        }
        View childAt = getChildAt(this.mBaselineAlignedChildIndex);
        int baseline = childAt.getBaseline();
        if (baseline != -1) {
            int i2 = this.mBaselineChildTop;
            if (this.mOrientation == 1 && (i = this.mGravity & 112) != 48) {
                switch (i) {
                    case 16:
                        i2 += ((((getBottom() - getTop()) - getPaddingTop()) - getPaddingBottom()) - this.mTotalLength) / 2;
                        break;
                    case 80:
                        i2 = ((getBottom() - getTop()) - getPaddingBottom()) - this.mTotalLength;
                        break;
                }
            }
            return i2 + ((LayoutParams) childAt.getLayoutParams()).topMargin + baseline;
        } else if (this.mBaselineAlignedChildIndex == 0) {
            return -1;
        } else {
            Throwable th4 = th;
            new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
            throw th4;
        }
    }

    public int getBaselineAlignedChildIndex() {
        return this.mBaselineAlignedChildIndex;
    }

    public void setBaselineAlignedChildIndex(int i) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        if (i2 < 0 || i2 >= getChildCount()) {
            Throwable th2 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("base aligned child index out of range (0, ").append(getChildCount()).append(")").toString());
            throw th2;
        }
        this.mBaselineAlignedChildIndex = i2;
    }

    /* access modifiers changed from: package-private */
    public View getVirtualChildAt(int i) {
        return getChildAt(i);
    }

    /* access modifiers changed from: package-private */
    public int getVirtualChildCount() {
        return getChildCount();
    }

    public float getWeightSum() {
        return this.mWeightSum;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void setWeightSum(float f) {
        this.mWeightSum = Math.max(0.0f, f);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        if (this.mOrientation == 1) {
            measureVertical(i3, i4);
        } else {
            measureHorizontal(i3, i4);
        }
    }

    /* access modifiers changed from: protected */
    public boolean hasDividerBeforeChildAt(int i) {
        boolean z;
        int i2 = i;
        if (i2 == 0) {
            if ((this.mShowDividers & 1) != 0) {
                z = true;
            } else {
                z = false;
            }
            return z;
        } else if (i2 == getChildCount()) {
            return (this.mShowDividers & 4) != 0;
        } else if ((this.mShowDividers & 2) == 0) {
            return false;
        } else {
            boolean z2 = false;
            int i3 = i2 - 1;
            while (true) {
                if (i3 < 0) {
                    break;
                } else if (getChildAt(i3).getVisibility() != 8) {
                    z2 = true;
                    break;
                } else {
                    i3--;
                }
            }
            return z2;
        }
    }

    /* access modifiers changed from: package-private */
    public void measureVertical(int i, int i2) {
        Throwable th;
        int i3 = i;
        int i4 = i2;
        this.mTotalLength = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        boolean z = true;
        float f = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = View.MeasureSpec.getMode(i3);
        int mode2 = View.MeasureSpec.getMode(i4);
        boolean z2 = false;
        boolean z3 = false;
        int i9 = this.mBaselineAlignedChildIndex;
        boolean z4 = this.mUseLargestChild;
        int i10 = Integer.MIN_VALUE;
        int i11 = 0;
        while (i11 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i11);
            if (virtualChildAt == null) {
                this.mTotalLength = this.mTotalLength + measureNullChild(i11);
            } else if (virtualChildAt.getVisibility() == 8) {
                i11 += getChildrenSkipCount(virtualChildAt, i11);
            } else {
                if (hasDividerBeforeChildAt(i11)) {
                    this.mTotalLength = this.mTotalLength + this.mDividerHeight;
                }
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                f += layoutParams.weight;
                if (mode2 == 1073741824 && layoutParams.height == 0 && layoutParams.weight > 0.0f) {
                    int i12 = this.mTotalLength;
                    this.mTotalLength = Math.max(i12, i12 + layoutParams.topMargin + layoutParams.bottomMargin);
                    z3 = true;
                } else {
                    int i13 = Integer.MIN_VALUE;
                    if (layoutParams.height == 0 && layoutParams.weight > 0.0f) {
                        i13 = 0;
                        layoutParams.height = -2;
                    }
                    measureChildBeforeLayout(virtualChildAt, i11, i3, 0, i4, f == 0.0f ? this.mTotalLength : 0);
                    if (i13 != Integer.MIN_VALUE) {
                        layoutParams.height = i13;
                    }
                    int measuredHeight = virtualChildAt.getMeasuredHeight();
                    int i14 = this.mTotalLength;
                    this.mTotalLength = Math.max(i14, i14 + measuredHeight + layoutParams.topMargin + layoutParams.bottomMargin + getNextLocationOffset(virtualChildAt));
                    if (z4) {
                        i10 = Math.max(measuredHeight, i10);
                    }
                }
                if (i9 >= 0 && i9 == i11 + 1) {
                    this.mBaselineChildTop = this.mTotalLength;
                }
                if (i11 >= i9 || layoutParams.weight <= 0.0f) {
                    boolean z5 = false;
                    if (mode != 1073741824 && layoutParams.width == -1) {
                        z2 = true;
                        z5 = true;
                    }
                    int i15 = layoutParams.leftMargin + layoutParams.rightMargin;
                    int measuredWidth = virtualChildAt.getMeasuredWidth() + i15;
                    i5 = Math.max(i5, measuredWidth);
                    i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(virtualChildAt));
                    z = z && layoutParams.width == -1;
                    if (layoutParams.weight > 0.0f) {
                        i8 = Math.max(i8, z5 ? i15 : measuredWidth);
                    } else {
                        i7 = Math.max(i7, z5 ? i15 : measuredWidth);
                    }
                    i11 += getChildrenSkipCount(virtualChildAt, i11);
                } else {
                    Throwable th2 = th;
                    new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
                    throw th2;
                }
            }
            i11++;
        }
        if (this.mTotalLength > 0 && hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength = this.mTotalLength + this.mDividerHeight;
        }
        if (z4 && (mode2 == Integer.MIN_VALUE || mode2 == 0)) {
            this.mTotalLength = 0;
            int i16 = 0;
            while (i16 < virtualChildCount) {
                View virtualChildAt2 = getVirtualChildAt(i16);
                if (virtualChildAt2 == null) {
                    this.mTotalLength = this.mTotalLength + measureNullChild(i16);
                } else if (virtualChildAt2.getVisibility() == 8) {
                    i16 += getChildrenSkipCount(virtualChildAt2, i16);
                } else {
                    LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                    int i17 = this.mTotalLength;
                    this.mTotalLength = Math.max(i17, i17 + i10 + layoutParams2.topMargin + layoutParams2.bottomMargin + getNextLocationOffset(virtualChildAt2));
                }
                i16++;
            }
        }
        this.mTotalLength = this.mTotalLength + getPaddingTop() + getPaddingBottom();
        int resolveSizeAndState = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), i4, 0);
        int i18 = (resolveSizeAndState & ViewCompat.MEASURED_SIZE_MASK) - this.mTotalLength;
        if (z3 || (i18 != 0 && f > 0.0f)) {
            float f2 = this.mWeightSum > 0.0f ? this.mWeightSum : f;
            this.mTotalLength = 0;
            for (int i19 = 0; i19 < virtualChildCount; i19++) {
                View virtualChildAt3 = getVirtualChildAt(i19);
                if (virtualChildAt3.getVisibility() != 8) {
                    LayoutParams layoutParams3 = (LayoutParams) virtualChildAt3.getLayoutParams();
                    float f3 = layoutParams3.weight;
                    if (f3 > 0.0f) {
                        int i20 = (int) ((f3 * ((float) i18)) / f2);
                        f2 -= f3;
                        i18 -= i20;
                        int childMeasureSpec = getChildMeasureSpec(i3, getPaddingLeft() + getPaddingRight() + layoutParams3.leftMargin + layoutParams3.rightMargin, layoutParams3.width);
                        if (layoutParams3.height == 0 && mode2 == 1073741824) {
                            virtualChildAt3.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(i20 > 0 ? i20 : 0, 1073741824));
                        } else {
                            int measuredHeight2 = virtualChildAt3.getMeasuredHeight() + i20;
                            if (measuredHeight2 < 0) {
                                measuredHeight2 = 0;
                            }
                            virtualChildAt3.measure(childMeasureSpec, View.MeasureSpec.makeMeasureSpec(measuredHeight2, 1073741824));
                        }
                        i6 = ViewUtils.combineMeasuredStates(i6, ViewCompat.getMeasuredState(virtualChildAt3) & InputDeviceCompat.SOURCE_ANY);
                    }
                    int i21 = layoutParams3.leftMargin + layoutParams3.rightMargin;
                    int measuredWidth2 = virtualChildAt3.getMeasuredWidth() + i21;
                    i5 = Math.max(i5, measuredWidth2);
                    i7 = Math.max(i7, mode != 1073741824 && layoutParams3.width == -1 ? i21 : measuredWidth2);
                    z = z && layoutParams3.width == -1;
                    int i22 = this.mTotalLength;
                    this.mTotalLength = Math.max(i22, i22 + virtualChildAt3.getMeasuredHeight() + layoutParams3.topMargin + layoutParams3.bottomMargin + getNextLocationOffset(virtualChildAt3));
                }
            }
            this.mTotalLength = this.mTotalLength + getPaddingTop() + getPaddingBottom();
        } else {
            i7 = Math.max(i7, i8);
            if (z4 && mode2 != 1073741824) {
                for (int i23 = 0; i23 < virtualChildCount; i23++) {
                    View virtualChildAt4 = getVirtualChildAt(i23);
                    if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                        virtualChildAt4.measure(View.MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i10, 1073741824));
                    }
                }
            }
        }
        if (!z && mode != 1073741824) {
            i5 = i7;
        }
        setMeasuredDimension(ViewCompat.resolveSizeAndState(Math.max(i5 + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth()), i3, i6), resolveSizeAndState);
        if (z2) {
            forceUniformWidth(virtualChildCount, i4);
        }
    }

    private void forceUniformWidth(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
        for (int i5 = 0; i5 < i3; i5++) {
            View virtualChildAt = getVirtualChildAt(i5);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.width == -1) {
                    int i6 = layoutParams.height;
                    layoutParams.height = virtualChildAt.getMeasuredHeight();
                    measureChildWithMargins(virtualChildAt, makeMeasureSpec, 0, i4, 0);
                    layoutParams.height = i6;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void measureHorizontal(int i, int i2) {
        int baseline;
        int i3;
        int baseline2;
        int i4;
        int i5 = i;
        int i6 = i2;
        this.mTotalLength = 0;
        int i7 = 0;
        int i8 = 0;
        int i9 = 0;
        int i10 = 0;
        boolean z = true;
        float f = 0.0f;
        int virtualChildCount = getVirtualChildCount();
        int mode = View.MeasureSpec.getMode(i5);
        int mode2 = View.MeasureSpec.getMode(i6);
        boolean z2 = false;
        boolean z3 = false;
        if (this.mMaxAscent == null || this.mMaxDescent == null) {
            this.mMaxAscent = new int[4];
            this.mMaxDescent = new int[4];
        }
        int[] iArr = this.mMaxAscent;
        int[] iArr2 = this.mMaxDescent;
        iArr[3] = -1;
        iArr[2] = -1;
        iArr[1] = -1;
        iArr[0] = -1;
        iArr2[3] = -1;
        iArr2[2] = -1;
        iArr2[1] = -1;
        iArr2[0] = -1;
        boolean z4 = this.mBaselineAligned;
        boolean z5 = this.mUseLargestChild;
        boolean z6 = mode == 1073741824;
        int i11 = Integer.MIN_VALUE;
        int i12 = 0;
        while (i12 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i12);
            if (virtualChildAt == null) {
                this.mTotalLength = this.mTotalLength + measureNullChild(i12);
            } else if (virtualChildAt.getVisibility() == 8) {
                i12 += getChildrenSkipCount(virtualChildAt, i12);
            } else {
                if (hasDividerBeforeChildAt(i12)) {
                    this.mTotalLength = this.mTotalLength + this.mDividerWidth;
                }
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                f += layoutParams.weight;
                if (mode == 1073741824 && layoutParams.width == 0 && layoutParams.weight > 0.0f) {
                    if (z6) {
                        this.mTotalLength = this.mTotalLength + layoutParams.leftMargin + layoutParams.rightMargin;
                    } else {
                        int i13 = this.mTotalLength;
                        this.mTotalLength = Math.max(i13, i13 + layoutParams.leftMargin + layoutParams.rightMargin);
                    }
                    if (z4) {
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                        virtualChildAt.measure(makeMeasureSpec, makeMeasureSpec);
                    } else {
                        z3 = true;
                    }
                } else {
                    int i14 = Integer.MIN_VALUE;
                    if (layoutParams.width == 0 && layoutParams.weight > 0.0f) {
                        i14 = 0;
                        layoutParams.width = -2;
                    }
                    measureChildBeforeLayout(virtualChildAt, i12, i5, f == 0.0f ? this.mTotalLength : 0, i6, 0);
                    if (i14 != Integer.MIN_VALUE) {
                        layoutParams.width = i14;
                    }
                    int measuredWidth = virtualChildAt.getMeasuredWidth();
                    if (z6) {
                        this.mTotalLength = this.mTotalLength + measuredWidth + layoutParams.leftMargin + layoutParams.rightMargin + getNextLocationOffset(virtualChildAt);
                    } else {
                        int i15 = this.mTotalLength;
                        this.mTotalLength = Math.max(i15, i15 + measuredWidth + layoutParams.leftMargin + layoutParams.rightMargin + getNextLocationOffset(virtualChildAt));
                    }
                    if (z5) {
                        i11 = Math.max(measuredWidth, i11);
                    }
                }
                boolean z7 = false;
                if (mode2 != 1073741824 && layoutParams.height == -1) {
                    z2 = true;
                    z7 = true;
                }
                int i16 = layoutParams.topMargin + layoutParams.bottomMargin;
                int measuredHeight = virtualChildAt.getMeasuredHeight() + i16;
                i8 = ViewUtils.combineMeasuredStates(i8, ViewCompat.getMeasuredState(virtualChildAt));
                if (z4 && (baseline2 = virtualChildAt.getBaseline()) != -1) {
                    if (layoutParams.gravity < 0) {
                        i4 = this.mGravity;
                    } else {
                        i4 = layoutParams.gravity;
                    }
                    int i17 = (((i4 & 112) >> 4) & -2) >> 1;
                    iArr[i17] = Math.max(iArr[i17], baseline2);
                    iArr2[i17] = Math.max(iArr2[i17], measuredHeight - baseline2);
                }
                i7 = Math.max(i7, measuredHeight);
                z = z && layoutParams.height == -1;
                if (layoutParams.weight > 0.0f) {
                    i10 = Math.max(i10, z7 ? i16 : measuredHeight);
                } else {
                    i9 = Math.max(i9, z7 ? i16 : measuredHeight);
                }
                i12 += getChildrenSkipCount(virtualChildAt, i12);
            }
            i12++;
        }
        if (this.mTotalLength > 0 && hasDividerBeforeChildAt(virtualChildCount)) {
            this.mTotalLength = this.mTotalLength + this.mDividerWidth;
        }
        if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
            i7 = Math.max(i7, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
        }
        if (z5 && (mode == Integer.MIN_VALUE || mode == 0)) {
            this.mTotalLength = 0;
            int i18 = 0;
            while (i18 < virtualChildCount) {
                View virtualChildAt2 = getVirtualChildAt(i18);
                if (virtualChildAt2 == null) {
                    this.mTotalLength = this.mTotalLength + measureNullChild(i18);
                } else if (virtualChildAt2.getVisibility() == 8) {
                    i18 += getChildrenSkipCount(virtualChildAt2, i18);
                } else {
                    LayoutParams layoutParams2 = (LayoutParams) virtualChildAt2.getLayoutParams();
                    if (z6) {
                        this.mTotalLength = this.mTotalLength + i11 + layoutParams2.leftMargin + layoutParams2.rightMargin + getNextLocationOffset(virtualChildAt2);
                    } else {
                        int i19 = this.mTotalLength;
                        this.mTotalLength = Math.max(i19, i19 + i11 + layoutParams2.leftMargin + layoutParams2.rightMargin + getNextLocationOffset(virtualChildAt2));
                    }
                }
                i18++;
            }
        }
        this.mTotalLength = this.mTotalLength + getPaddingLeft() + getPaddingRight();
        int resolveSizeAndState = ViewCompat.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), i5, 0);
        int i20 = (resolveSizeAndState & ViewCompat.MEASURED_SIZE_MASK) - this.mTotalLength;
        if (z3 || (i20 != 0 && f > 0.0f)) {
            float f2 = this.mWeightSum > 0.0f ? this.mWeightSum : f;
            iArr[3] = -1;
            iArr[2] = -1;
            iArr[1] = -1;
            iArr[0] = -1;
            iArr2[3] = -1;
            iArr2[2] = -1;
            iArr2[1] = -1;
            iArr2[0] = -1;
            i7 = -1;
            this.mTotalLength = 0;
            for (int i21 = 0; i21 < virtualChildCount; i21++) {
                View virtualChildAt3 = getVirtualChildAt(i21);
                if (!(virtualChildAt3 == null || virtualChildAt3.getVisibility() == 8)) {
                    LayoutParams layoutParams3 = (LayoutParams) virtualChildAt3.getLayoutParams();
                    float f3 = layoutParams3.weight;
                    if (f3 > 0.0f) {
                        int i22 = (int) ((f3 * ((float) i20)) / f2);
                        f2 -= f3;
                        i20 -= i22;
                        int childMeasureSpec = getChildMeasureSpec(i6, getPaddingTop() + getPaddingBottom() + layoutParams3.topMargin + layoutParams3.bottomMargin, layoutParams3.height);
                        if (layoutParams3.width == 0 && mode == 1073741824) {
                            virtualChildAt3.measure(View.MeasureSpec.makeMeasureSpec(i22 > 0 ? i22 : 0, 1073741824), childMeasureSpec);
                        } else {
                            int measuredWidth2 = virtualChildAt3.getMeasuredWidth() + i22;
                            if (measuredWidth2 < 0) {
                                measuredWidth2 = 0;
                            }
                            virtualChildAt3.measure(View.MeasureSpec.makeMeasureSpec(measuredWidth2, 1073741824), childMeasureSpec);
                        }
                        i8 = ViewUtils.combineMeasuredStates(i8, ViewCompat.getMeasuredState(virtualChildAt3) & ViewCompat.MEASURED_STATE_MASK);
                    }
                    if (z6) {
                        this.mTotalLength = this.mTotalLength + virtualChildAt3.getMeasuredWidth() + layoutParams3.leftMargin + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt3);
                    } else {
                        int i23 = this.mTotalLength;
                        this.mTotalLength = Math.max(i23, i23 + virtualChildAt3.getMeasuredWidth() + layoutParams3.leftMargin + layoutParams3.rightMargin + getNextLocationOffset(virtualChildAt3));
                    }
                    boolean z8 = mode2 != 1073741824 && layoutParams3.height == -1;
                    int i24 = layoutParams3.topMargin + layoutParams3.bottomMargin;
                    int measuredHeight2 = virtualChildAt3.getMeasuredHeight() + i24;
                    i7 = Math.max(i7, measuredHeight2);
                    i9 = Math.max(i9, z8 ? i24 : measuredHeight2);
                    z = z && layoutParams3.height == -1;
                    if (z4 && (baseline = virtualChildAt3.getBaseline()) != -1) {
                        if (layoutParams3.gravity < 0) {
                            i3 = this.mGravity;
                        } else {
                            i3 = layoutParams3.gravity;
                        }
                        int i25 = (((i3 & 112) >> 4) & -2) >> 1;
                        iArr[i25] = Math.max(iArr[i25], baseline);
                        iArr2[i25] = Math.max(iArr2[i25], measuredHeight2 - baseline);
                    }
                }
            }
            this.mTotalLength = this.mTotalLength + getPaddingLeft() + getPaddingRight();
            if (!(iArr[1] == -1 && iArr[0] == -1 && iArr[2] == -1 && iArr[3] == -1)) {
                i7 = Math.max(i7, Math.max(iArr[3], Math.max(iArr[0], Math.max(iArr[1], iArr[2]))) + Math.max(iArr2[3], Math.max(iArr2[0], Math.max(iArr2[1], iArr2[2]))));
            }
        } else {
            i9 = Math.max(i9, i10);
            if (z5 && mode != 1073741824) {
                for (int i26 = 0; i26 < virtualChildCount; i26++) {
                    View virtualChildAt4 = getVirtualChildAt(i26);
                    if (!(virtualChildAt4 == null || virtualChildAt4.getVisibility() == 8 || ((LayoutParams) virtualChildAt4.getLayoutParams()).weight <= 0.0f)) {
                        virtualChildAt4.measure(View.MeasureSpec.makeMeasureSpec(i11, 1073741824), View.MeasureSpec.makeMeasureSpec(virtualChildAt4.getMeasuredHeight(), 1073741824));
                    }
                }
            }
        }
        if (!z && mode2 != 1073741824) {
            i7 = i9;
        }
        setMeasuredDimension(resolveSizeAndState | (i8 & ViewCompat.MEASURED_STATE_MASK), ViewCompat.resolveSizeAndState(Math.max(i7 + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight()), i6, i8 << 16));
        if (z2) {
            forceUniformHeight(virtualChildCount, i5);
        }
    }

    private void forceUniformHeight(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
        for (int i5 = 0; i5 < i3; i5++) {
            View virtualChildAt = getVirtualChildAt(i5);
            if (virtualChildAt.getVisibility() != 8) {
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (layoutParams.height == -1) {
                    int i6 = layoutParams.width;
                    layoutParams.width = virtualChildAt.getMeasuredWidth();
                    measureChildWithMargins(virtualChildAt, i4, 0, makeMeasureSpec, 0);
                    layoutParams.width = i6;
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public int getChildrenSkipCount(View view, int i) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int measureNullChild(int i) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void measureChildBeforeLayout(View view, int i, int i2, int i3, int i4, int i5) {
        measureChildWithMargins(view, i2, i3, i4, i5);
    }

    /* access modifiers changed from: package-private */
    public int getLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getNextLocationOffset(View view) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        int i7 = i3;
        int i8 = i4;
        if (this.mOrientation == 1) {
            layoutVertical(i5, i6, i7, i8);
        } else {
            layoutHorizontal(i5, i6, i7, i8);
        }
    }

    /* access modifiers changed from: package-private */
    public void layoutVertical(int i, int i2, int i3, int i4) {
        int paddingTop;
        int i5;
        int i6 = i2;
        int i7 = i4;
        int paddingLeft = getPaddingLeft();
        int i8 = i3 - i;
        int paddingRight = i8 - getPaddingRight();
        int paddingRight2 = (i8 - paddingLeft) - getPaddingRight();
        int virtualChildCount = getVirtualChildCount();
        int i9 = this.mGravity & 112;
        int i10 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        switch (i9) {
            case 16:
                paddingTop = getPaddingTop() + (((i7 - i6) - this.mTotalLength) / 2);
                break;
            case 80:
                paddingTop = ((getPaddingTop() + i7) - i6) - this.mTotalLength;
                break;
            default:
                paddingTop = getPaddingTop();
                break;
        }
        int i11 = 0;
        while (i11 < virtualChildCount) {
            View virtualChildAt = getVirtualChildAt(i11);
            if (virtualChildAt == null) {
                paddingTop += measureNullChild(i11);
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                int i12 = layoutParams.gravity;
                if (i12 < 0) {
                    i12 = i10;
                }
                switch (GravityCompat.getAbsoluteGravity(i12, ViewCompat.getLayoutDirection(this)) & 7) {
                    case 1:
                        i5 = ((paddingLeft + ((paddingRight2 - measuredWidth) / 2)) + layoutParams.leftMargin) - layoutParams.rightMargin;
                        break;
                    case 5:
                        i5 = (paddingRight - measuredWidth) - layoutParams.rightMargin;
                        break;
                    default:
                        i5 = paddingLeft + layoutParams.leftMargin;
                        break;
                }
                if (hasDividerBeforeChildAt(i11)) {
                    paddingTop += this.mDividerHeight;
                }
                int i13 = paddingTop + layoutParams.topMargin;
                setChildFrame(virtualChildAt, i5, i13 + getLocationOffset(virtualChildAt), measuredWidth, measuredHeight);
                paddingTop = i13 + measuredHeight + layoutParams.bottomMargin + getNextLocationOffset(virtualChildAt);
                i11 += getChildrenSkipCount(virtualChildAt, i11);
            }
            i11++;
        }
    }

    /* access modifiers changed from: package-private */
    public void layoutHorizontal(int i, int i2, int i3, int i4) {
        int paddingLeft;
        int i5;
        int i6 = i;
        int i7 = i3;
        boolean isLayoutRtl = ViewUtils.isLayoutRtl(this);
        int paddingTop = getPaddingTop();
        int i8 = i4 - i2;
        int paddingBottom = i8 - getPaddingBottom();
        int paddingBottom2 = (i8 - paddingTop) - getPaddingBottom();
        int virtualChildCount = getVirtualChildCount();
        int i9 = this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        int i10 = this.mGravity & 112;
        boolean z = this.mBaselineAligned;
        int[] iArr = this.mMaxAscent;
        int[] iArr2 = this.mMaxDescent;
        switch (GravityCompat.getAbsoluteGravity(i9, ViewCompat.getLayoutDirection(this))) {
            case 1:
                paddingLeft = getPaddingLeft() + (((i7 - i6) - this.mTotalLength) / 2);
                break;
            case 5:
                paddingLeft = ((getPaddingLeft() + i7) - i6) - this.mTotalLength;
                break;
            default:
                paddingLeft = getPaddingLeft();
                break;
        }
        int i11 = 0;
        int i12 = 1;
        if (isLayoutRtl) {
            i11 = virtualChildCount - 1;
            i12 = -1;
        }
        int i13 = 0;
        while (i13 < virtualChildCount) {
            int i14 = i11 + (i12 * i13);
            View virtualChildAt = getVirtualChildAt(i14);
            if (virtualChildAt == null) {
                paddingLeft += measureNullChild(i14);
            } else if (virtualChildAt.getVisibility() != 8) {
                int measuredWidth = virtualChildAt.getMeasuredWidth();
                int measuredHeight = virtualChildAt.getMeasuredHeight();
                int i15 = -1;
                LayoutParams layoutParams = (LayoutParams) virtualChildAt.getLayoutParams();
                if (z && layoutParams.height != -1) {
                    i15 = virtualChildAt.getBaseline();
                }
                int i16 = layoutParams.gravity;
                if (i16 < 0) {
                    i16 = i10;
                }
                switch (i16 & 112) {
                    case 16:
                        i5 = ((paddingTop + ((paddingBottom2 - measuredHeight) / 2)) + layoutParams.topMargin) - layoutParams.bottomMargin;
                        break;
                    case 48:
                        i5 = paddingTop + layoutParams.topMargin;
                        if (i15 != -1) {
                            i5 += iArr[1] - i15;
                            break;
                        }
                        break;
                    case 80:
                        i5 = (paddingBottom - measuredHeight) - layoutParams.bottomMargin;
                        if (i15 != -1) {
                            i5 -= iArr2[2] - (virtualChildAt.getMeasuredHeight() - i15);
                            break;
                        }
                        break;
                    default:
                        i5 = paddingTop;
                        break;
                }
                if (hasDividerBeforeChildAt(i14)) {
                    paddingLeft += this.mDividerWidth;
                }
                int i17 = paddingLeft + layoutParams.leftMargin;
                setChildFrame(virtualChildAt, i17 + getLocationOffset(virtualChildAt), i5, measuredWidth, measuredHeight);
                paddingLeft = i17 + measuredWidth + layoutParams.rightMargin + getNextLocationOffset(virtualChildAt);
                i13 += getChildrenSkipCount(virtualChildAt, i14);
            }
            i13++;
        }
    }

    private void setChildFrame(View view, int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i2;
        view.layout(i5, i6, i5 + i3, i6 + i4);
    }

    public void setOrientation(int i) {
        int i2 = i;
        if (this.mOrientation != i2) {
            this.mOrientation = i2;
            requestLayout();
        }
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    public void setGravity(int i) {
        int i2 = i;
        if (this.mGravity != i2) {
            if ((i2 & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK) == 0) {
                i2 |= 8388611;
            }
            if ((i2 & 112) == 0) {
                i2 |= 48;
            }
            this.mGravity = i2;
            requestLayout();
        }
    }

    public void setHorizontalGravity(int i) {
        int i2 = i & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK;
        if ((this.mGravity & GravityCompat.RELATIVE_HORIZONTAL_GRAVITY_MASK) != i2) {
            this.mGravity = (this.mGravity & -8388616) | i2;
            requestLayout();
        }
    }

    public void setVerticalGravity(int i) {
        int i2 = i & 112;
        if ((this.mGravity & 112) != i2) {
            this.mGravity = (this.mGravity & -113) | i2;
            requestLayout();
        }
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        LayoutParams layoutParams;
        new LayoutParams(getContext(), attributeSet);
        return layoutParams;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams;
        LayoutParams layoutParams2;
        if (this.mOrientation == 0) {
            new LayoutParams(-2, -2);
            return layoutParams2;
        } else if (this.mOrientation != 1) {
            return null;
        } else {
            new LayoutParams(-1, -2);
            return layoutParams;
        }
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        LayoutParams layoutParams2;
        new LayoutParams(layoutParams);
        return layoutParams2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams instanceof LayoutParams;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityEvent(accessibilityEvent2);
            accessibilityEvent2.setClassName(LinearLayoutCompat.class.getName());
        }
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeInfo;
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo2);
            accessibilityNodeInfo2.setClassName(LinearLayoutCompat.class.getName());
        }
    }

    public static class LayoutParams extends ViewGroup.MarginLayoutParams {
        public int gravity;
        public float weight;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.content.Context r9, android.util.AttributeSet r10) {
            /*
                r8 = this;
                r0 = r8
                r1 = r9
                r2 = r10
                r4 = r0
                r5 = r1
                r6 = r2
                r4.<init>(r5, r6)
                r4 = r0
                r5 = -1
                r4.gravity = r5
                r4 = r1
                r5 = r2
                int[] r6 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_Layout
                android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r6)
                r3 = r4
                r4 = r0
                r5 = r3
                int r6 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_Layout_android_layout_weight
                r7 = 0
                float r5 = r5.getFloat(r6, r7)
                r4.weight = r5
                r4 = r0
                r5 = r3
                int r6 = android.support.v7.appcompat.R.styleable.LinearLayoutCompat_Layout_android_layout_gravity
                r7 = -1
                int r5 = r5.getInt(r6, r7)
                r4.gravity = r5
                r4 = r3
                r4.recycle()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.LinearLayoutCompat.LayoutParams.<init>(android.content.Context, android.util.AttributeSet):void");
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.gravity = -1;
            this.weight = 0.0f;
        }

        public LayoutParams(int i, int i2, float f) {
            super(i, i2);
            this.gravity = -1;
            this.weight = f;
        }

        public LayoutParams(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
            this.gravity = -1;
        }

        public LayoutParams(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
            this.gravity = -1;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public LayoutParams(android.support.v7.widget.LinearLayoutCompat.LayoutParams r5) {
            /*
                r4 = this;
                r0 = r4
                r1 = r5
                r2 = r0
                r3 = r1
                r2.<init>(r3)
                r2 = r0
                r3 = -1
                r2.gravity = r3
                r2 = r0
                r3 = r1
                float r3 = r3.weight
                r2.weight = r3
                r2 = r0
                r3 = r1
                int r3 = r3.gravity
                r2.gravity = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.LinearLayoutCompat.LayoutParams.<init>(android.support.v7.widget.LinearLayoutCompat$LayoutParams):void");
        }
    }
}
