package android.support.v7.widget;

import android.support.v4.util.Pools;
import android.support.v7.widget.OpReorderer;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class AdapterHelper implements OpReorderer.Callback {
    private static final boolean DEBUG = false;
    static final int POSITION_TYPE_INVISIBLE = 0;
    static final int POSITION_TYPE_NEW_OR_LAID_OUT = 1;
    private static final String TAG = "AHT";
    final Callback mCallback;
    final boolean mDisableRecycler;
    private int mExistingUpdateTypes;
    Runnable mOnItemProcessedCallback;
    final OpReorderer mOpReorderer;
    final ArrayList<UpdateOp> mPendingUpdates;
    final ArrayList<UpdateOp> mPostponedList;
    private Pools.Pool<UpdateOp> mUpdateOpPool;

    interface Callback {
        RecyclerView.ViewHolder findViewHolder(int i);

        void markViewHoldersUpdated(int i, int i2, Object obj);

        void offsetPositionsForAdd(int i, int i2);

        void offsetPositionsForMove(int i, int i2);

        void offsetPositionsForRemovingInvisible(int i, int i2);

        void offsetPositionsForRemovingLaidOutOrNewView(int i, int i2);

        void onDispatchFirstPass(UpdateOp updateOp);

        void onDispatchSecondPass(UpdateOp updateOp);
    }

    AdapterHelper(Callback callback) {
        this(callback, false);
    }

    AdapterHelper(Callback callback, boolean z) {
        Pools.Pool<UpdateOp> pool;
        ArrayList<UpdateOp> arrayList;
        ArrayList<UpdateOp> arrayList2;
        OpReorderer opReorderer;
        new Pools.SimplePool(30);
        this.mUpdateOpPool = pool;
        new ArrayList<>();
        this.mPendingUpdates = arrayList;
        new ArrayList<>();
        this.mPostponedList = arrayList2;
        this.mExistingUpdateTypes = 0;
        this.mCallback = callback;
        this.mDisableRecycler = z;
        new OpReorderer(this);
        this.mOpReorderer = opReorderer;
    }

    /* access modifiers changed from: package-private */
    public AdapterHelper addUpdateOp(UpdateOp... updateOpArr) {
        boolean addAll = Collections.addAll(this.mPendingUpdates, updateOpArr);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        recycleUpdateOpsAndClearList(this.mPendingUpdates);
        recycleUpdateOpsAndClearList(this.mPostponedList);
        this.mExistingUpdateTypes = 0;
    }

    /* access modifiers changed from: package-private */
    public void preProcess() {
        this.mOpReorderer.reorderOps(this.mPendingUpdates);
        int size = this.mPendingUpdates.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.mPendingUpdates.get(i);
            switch (updateOp.cmd) {
                case 1:
                    applyAdd(updateOp);
                    break;
                case 2:
                    applyRemove(updateOp);
                    break;
                case 4:
                    applyUpdate(updateOp);
                    break;
                case 8:
                    applyMove(updateOp);
                    break;
            }
            if (this.mOnItemProcessedCallback != null) {
                this.mOnItemProcessedCallback.run();
            }
        }
        this.mPendingUpdates.clear();
    }

    /* access modifiers changed from: package-private */
    public void consumePostponedUpdates() {
        int size = this.mPostponedList.size();
        for (int i = 0; i < size; i++) {
            this.mCallback.onDispatchSecondPass(this.mPostponedList.get(i));
        }
        recycleUpdateOpsAndClearList(this.mPostponedList);
        this.mExistingUpdateTypes = 0;
    }

    private void applyMove(UpdateOp updateOp) {
        postponeAndUpdateViewHolders(updateOp);
    }

    private void applyRemove(UpdateOp updateOp) {
        UpdateOp updateOp2 = updateOp;
        int i = updateOp2.positionStart;
        int i2 = 0;
        int i3 = updateOp2.positionStart + updateOp2.itemCount;
        boolean z = true;
        int i4 = updateOp2.positionStart;
        while (i4 < i3) {
            boolean z2 = false;
            if (this.mCallback.findViewHolder(i4) != null || canFindInPreLayout(i4)) {
                if (!z) {
                    dispatchAndUpdateViewHolders(obtainUpdateOp(2, i, i2, null));
                    z2 = true;
                }
                z = true;
            } else {
                if (z) {
                    postponeAndUpdateViewHolders(obtainUpdateOp(2, i, i2, null));
                    z2 = true;
                }
                z = false;
            }
            if (z2) {
                i4 -= i2;
                i3 -= i2;
                i2 = 1;
            } else {
                i2++;
            }
            i4++;
        }
        if (i2 != updateOp2.itemCount) {
            recycleUpdateOp(updateOp2);
            updateOp2 = obtainUpdateOp(2, i, i2, null);
        }
        if (!z) {
            dispatchAndUpdateViewHolders(updateOp2);
        } else {
            postponeAndUpdateViewHolders(updateOp2);
        }
    }

    private void applyUpdate(UpdateOp updateOp) {
        boolean z;
        UpdateOp updateOp2 = updateOp;
        int i = updateOp2.positionStart;
        int i2 = 0;
        int i3 = updateOp2.positionStart + updateOp2.itemCount;
        boolean z2 = true;
        for (int i4 = updateOp2.positionStart; i4 < i3; i4++) {
            if (this.mCallback.findViewHolder(i4) != null || canFindInPreLayout(i4)) {
                if (!z2) {
                    dispatchAndUpdateViewHolders(obtainUpdateOp(4, i, i2, updateOp2.payload));
                    i2 = 0;
                    i = i4;
                }
                z = true;
            } else {
                if (z2) {
                    postponeAndUpdateViewHolders(obtainUpdateOp(4, i, i2, updateOp2.payload));
                    i2 = 0;
                    i = i4;
                }
                z = false;
            }
            z2 = z;
            i2++;
        }
        if (i2 != updateOp2.itemCount) {
            recycleUpdateOp(updateOp2);
            updateOp2 = obtainUpdateOp(4, i, i2, updateOp2.payload);
        }
        if (!z2) {
            dispatchAndUpdateViewHolders(updateOp2);
        } else {
            postponeAndUpdateViewHolders(updateOp2);
        }
    }

    private void dispatchAndUpdateViewHolders(UpdateOp updateOp) {
        Throwable th;
        int i;
        Throwable th2;
        StringBuilder sb;
        UpdateOp updateOp2 = updateOp;
        if (updateOp2.cmd == 1 || updateOp2.cmd == 8) {
            Throwable th3 = th;
            new IllegalArgumentException("should not dispatch add or move for pre layout");
            throw th3;
        }
        int updatePositionWithPostponed = updatePositionWithPostponed(updateOp2.positionStart, updateOp2.cmd);
        int i2 = 1;
        int i3 = updateOp2.positionStart;
        switch (updateOp2.cmd) {
            case 2:
                i = 0;
                break;
            case 3:
            default:
                Throwable th4 = th2;
                new StringBuilder();
                new IllegalArgumentException(sb.append("op should be remove or update.").append(updateOp2).toString());
                throw th4;
            case 4:
                i = 1;
                break;
        }
        for (int i4 = 1; i4 < updateOp2.itemCount; i4++) {
            int updatePositionWithPostponed2 = updatePositionWithPostponed(updateOp2.positionStart + (i * i4), updateOp2.cmd);
            boolean z = false;
            switch (updateOp2.cmd) {
                case 2:
                    z = updatePositionWithPostponed2 == updatePositionWithPostponed;
                    break;
                case 4:
                    z = updatePositionWithPostponed2 == updatePositionWithPostponed + 1;
                    break;
            }
            if (z) {
                i2++;
            } else {
                UpdateOp obtainUpdateOp = obtainUpdateOp(updateOp2.cmd, updatePositionWithPostponed, i2, updateOp2.payload);
                dispatchFirstPassAndUpdateViewHolders(obtainUpdateOp, i3);
                recycleUpdateOp(obtainUpdateOp);
                if (updateOp2.cmd == 4) {
                    i3 += i2;
                }
                updatePositionWithPostponed = updatePositionWithPostponed2;
                i2 = 1;
            }
        }
        Object obj = updateOp2.payload;
        recycleUpdateOp(updateOp2);
        if (i2 > 0) {
            UpdateOp obtainUpdateOp2 = obtainUpdateOp(updateOp2.cmd, updatePositionWithPostponed, i2, obj);
            dispatchFirstPassAndUpdateViewHolders(obtainUpdateOp2, i3);
            recycleUpdateOp(obtainUpdateOp2);
        }
    }

    /* access modifiers changed from: package-private */
    public void dispatchFirstPassAndUpdateViewHolders(UpdateOp updateOp, int i) {
        Throwable th;
        UpdateOp updateOp2 = updateOp;
        int i2 = i;
        this.mCallback.onDispatchFirstPass(updateOp2);
        switch (updateOp2.cmd) {
            case 2:
                this.mCallback.offsetPositionsForRemovingInvisible(i2, updateOp2.itemCount);
                return;
            case 3:
            default:
                Throwable th2 = th;
                new IllegalArgumentException("only remove and update ops can be dispatched in first pass");
                throw th2;
            case 4:
                this.mCallback.markViewHoldersUpdated(i2, updateOp2.itemCount, updateOp2.payload);
                return;
        }
    }

    private int updatePositionWithPostponed(int i, int i2) {
        int i3;
        int i4;
        int i5 = i;
        int i6 = i2;
        for (int size = this.mPostponedList.size() - 1; size >= 0; size--) {
            UpdateOp updateOp = this.mPostponedList.get(size);
            if (updateOp.cmd == 8) {
                if (updateOp.positionStart < updateOp.itemCount) {
                    i3 = updateOp.positionStart;
                    i4 = updateOp.itemCount;
                } else {
                    i3 = updateOp.itemCount;
                    i4 = updateOp.positionStart;
                }
                if (i5 < i3 || i5 > i4) {
                    if (i5 < updateOp.positionStart) {
                        if (i6 == 1) {
                            updateOp.positionStart++;
                            updateOp.itemCount++;
                        } else if (i6 == 2) {
                            UpdateOp updateOp2 = updateOp;
                            updateOp2.positionStart--;
                            UpdateOp updateOp3 = updateOp;
                            updateOp3.itemCount--;
                        }
                    }
                } else if (i3 == updateOp.positionStart) {
                    if (i6 == 1) {
                        updateOp.itemCount++;
                    } else if (i6 == 2) {
                        UpdateOp updateOp4 = updateOp;
                        updateOp4.itemCount--;
                    }
                    i5++;
                } else {
                    if (i6 == 1) {
                        updateOp.positionStart++;
                    } else if (i6 == 2) {
                        UpdateOp updateOp5 = updateOp;
                        updateOp5.positionStart--;
                    }
                    i5--;
                }
            } else if (updateOp.positionStart <= i5) {
                if (updateOp.cmd == 1) {
                    i5 -= updateOp.itemCount;
                } else if (updateOp.cmd == 2) {
                    i5 += updateOp.itemCount;
                }
            } else if (i6 == 1) {
                updateOp.positionStart++;
            } else if (i6 == 2) {
                UpdateOp updateOp6 = updateOp;
                updateOp6.positionStart--;
            }
        }
        for (int size2 = this.mPostponedList.size() - 1; size2 >= 0; size2--) {
            UpdateOp updateOp7 = this.mPostponedList.get(size2);
            if (updateOp7.cmd == 8) {
                if (updateOp7.itemCount == updateOp7.positionStart || updateOp7.itemCount < 0) {
                    UpdateOp remove = this.mPostponedList.remove(size2);
                    recycleUpdateOp(updateOp7);
                }
            } else if (updateOp7.itemCount <= 0) {
                UpdateOp remove2 = this.mPostponedList.remove(size2);
                recycleUpdateOp(updateOp7);
            }
        }
        return i5;
    }

    private boolean canFindInPreLayout(int i) {
        int i2 = i;
        int size = this.mPostponedList.size();
        for (int i3 = 0; i3 < size; i3++) {
            UpdateOp updateOp = this.mPostponedList.get(i3);
            if (updateOp.cmd == 8) {
                if (findPositionOffset(updateOp.itemCount, i3 + 1) == i2) {
                    return true;
                }
            } else if (updateOp.cmd == 1) {
                int i4 = updateOp.positionStart + updateOp.itemCount;
                for (int i5 = updateOp.positionStart; i5 < i4; i5++) {
                    if (findPositionOffset(i5, i3 + 1) == i2) {
                        return true;
                    }
                }
                continue;
            } else {
                continue;
            }
        }
        return false;
    }

    private void applyAdd(UpdateOp updateOp) {
        postponeAndUpdateViewHolders(updateOp);
    }

    private void postponeAndUpdateViewHolders(UpdateOp updateOp) {
        Throwable th;
        StringBuilder sb;
        UpdateOp updateOp2 = updateOp;
        boolean add = this.mPostponedList.add(updateOp2);
        switch (updateOp2.cmd) {
            case 1:
                this.mCallback.offsetPositionsForAdd(updateOp2.positionStart, updateOp2.itemCount);
                return;
            case 2:
                this.mCallback.offsetPositionsForRemovingLaidOutOrNewView(updateOp2.positionStart, updateOp2.itemCount);
                return;
            case 3:
            case 5:
            case 6:
            case 7:
            default:
                Throwable th2 = th;
                new StringBuilder();
                new IllegalArgumentException(sb.append("Unknown update op type for ").append(updateOp2).toString());
                throw th2;
            case 4:
                this.mCallback.markViewHoldersUpdated(updateOp2.positionStart, updateOp2.itemCount, updateOp2.payload);
                return;
            case 8:
                this.mCallback.offsetPositionsForMove(updateOp2.positionStart, updateOp2.itemCount);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean hasPendingUpdates() {
        return this.mPendingUpdates.size() > 0;
    }

    /* access modifiers changed from: package-private */
    public boolean hasAnyUpdateTypes(int i) {
        return (this.mExistingUpdateTypes & i) != 0;
    }

    /* access modifiers changed from: package-private */
    public int findPositionOffset(int i) {
        return findPositionOffset(i, 0);
    }

    /* access modifiers changed from: package-private */
    public int findPositionOffset(int i, int i2) {
        int i3 = i;
        int size = this.mPostponedList.size();
        for (int i4 = i2; i4 < size; i4++) {
            UpdateOp updateOp = this.mPostponedList.get(i4);
            if (updateOp.cmd == 8) {
                if (updateOp.positionStart == i3) {
                    i3 = updateOp.itemCount;
                } else {
                    if (updateOp.positionStart < i3) {
                        i3--;
                    }
                    if (updateOp.itemCount <= i3) {
                        i3++;
                    }
                }
            } else if (updateOp.positionStart > i3) {
                continue;
            } else if (updateOp.cmd == 2) {
                if (i3 < updateOp.positionStart + updateOp.itemCount) {
                    return -1;
                }
                i3 -= updateOp.itemCount;
            } else if (updateOp.cmd == 1) {
                i3 += updateOp.itemCount;
            }
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public boolean onItemRangeChanged(int i, int i2, Object obj) {
        boolean add = this.mPendingUpdates.add(obtainUpdateOp(4, i, i2, obj));
        this.mExistingUpdateTypes = this.mExistingUpdateTypes | 4;
        return this.mPendingUpdates.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public boolean onItemRangeInserted(int i, int i2) {
        boolean add = this.mPendingUpdates.add(obtainUpdateOp(1, i, i2, null));
        this.mExistingUpdateTypes = this.mExistingUpdateTypes | 1;
        return this.mPendingUpdates.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public boolean onItemRangeRemoved(int i, int i2) {
        boolean add = this.mPendingUpdates.add(obtainUpdateOp(2, i, i2, null));
        this.mExistingUpdateTypes = this.mExistingUpdateTypes | 2;
        return this.mPendingUpdates.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public boolean onItemRangeMoved(int i, int i2, int i3) {
        Throwable th;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (i4 == i5) {
            return false;
        }
        if (i6 != 1) {
            Throwable th2 = th;
            new IllegalArgumentException("Moving more than 1 item is not supported yet");
            throw th2;
        }
        boolean add = this.mPendingUpdates.add(obtainUpdateOp(8, i4, i5, null));
        this.mExistingUpdateTypes = this.mExistingUpdateTypes | 8;
        return this.mPendingUpdates.size() == 1;
    }

    /* access modifiers changed from: package-private */
    public void consumeUpdatesInOnePass() {
        consumePostponedUpdates();
        int size = this.mPendingUpdates.size();
        for (int i = 0; i < size; i++) {
            UpdateOp updateOp = this.mPendingUpdates.get(i);
            switch (updateOp.cmd) {
                case 1:
                    this.mCallback.onDispatchSecondPass(updateOp);
                    this.mCallback.offsetPositionsForAdd(updateOp.positionStart, updateOp.itemCount);
                    break;
                case 2:
                    this.mCallback.onDispatchSecondPass(updateOp);
                    this.mCallback.offsetPositionsForRemovingInvisible(updateOp.positionStart, updateOp.itemCount);
                    break;
                case 4:
                    this.mCallback.onDispatchSecondPass(updateOp);
                    this.mCallback.markViewHoldersUpdated(updateOp.positionStart, updateOp.itemCount, updateOp.payload);
                    break;
                case 8:
                    this.mCallback.onDispatchSecondPass(updateOp);
                    this.mCallback.offsetPositionsForMove(updateOp.positionStart, updateOp.itemCount);
                    break;
            }
            if (this.mOnItemProcessedCallback != null) {
                this.mOnItemProcessedCallback.run();
            }
        }
        recycleUpdateOpsAndClearList(this.mPendingUpdates);
        this.mExistingUpdateTypes = 0;
    }

    public int applyPendingUpdatesToPosition(int i) {
        int i2 = i;
        int size = this.mPendingUpdates.size();
        for (int i3 = 0; i3 < size; i3++) {
            UpdateOp updateOp = this.mPendingUpdates.get(i3);
            switch (updateOp.cmd) {
                case 1:
                    if (updateOp.positionStart > i2) {
                        break;
                    } else {
                        i2 += updateOp.itemCount;
                        break;
                    }
                case 2:
                    if (updateOp.positionStart <= i2) {
                        if (updateOp.positionStart + updateOp.itemCount <= i2) {
                            i2 -= updateOp.itemCount;
                            break;
                        } else {
                            return -1;
                        }
                    } else {
                        continue;
                    }
                case 8:
                    if (updateOp.positionStart != i2) {
                        if (updateOp.positionStart < i2) {
                            i2--;
                        }
                        if (updateOp.itemCount > i2) {
                            break;
                        } else {
                            i2++;
                            break;
                        }
                    } else {
                        i2 = updateOp.itemCount;
                        break;
                    }
            }
        }
        return i2;
    }

    static class UpdateOp {
        static final int ADD = 1;
        static final int MOVE = 8;
        static final int POOL_SIZE = 30;
        static final int REMOVE = 2;
        static final int UPDATE = 4;
        int cmd;
        int itemCount;
        Object payload;
        int positionStart;

        UpdateOp(int i, int i2, int i3, Object obj) {
            this.cmd = i;
            this.positionStart = i2;
            this.itemCount = i3;
            this.payload = obj;
        }

        /* access modifiers changed from: package-private */
        public String cmdToString() {
            switch (this.cmd) {
                case 1:
                    return "add";
                case 2:
                    return "rm";
                case 3:
                case 5:
                case 6:
                case 7:
                default:
                    return "??";
                case 4:
                    return "up";
                case 8:
                    return "mv";
            }
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append(Integer.toHexString(System.identityHashCode(this))).append("[").append(cmdToString()).append(",s:").append(this.positionStart).append("c:").append(this.itemCount).append(",p:").append(this.payload).append("]").toString();
        }

        /* JADX WARN: Type inference failed for: r6v0, types: [java.lang.Object] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r6) {
            /*
                r5 = this;
                r0 = r5
                r1 = r6
                r3 = r0
                r4 = r1
                if (r3 != r4) goto L_0x0009
                r3 = 1
                r0 = r3
            L_0x0008:
                return r0
            L_0x0009:
                r3 = r1
                if (r3 == 0) goto L_0x0018
                r3 = r0
                java.lang.Class r3 = r3.getClass()
                r4 = r1
                java.lang.Class r4 = r4.getClass()
                if (r3 == r4) goto L_0x001b
            L_0x0018:
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x001b:
                r3 = r1
                android.support.v7.widget.AdapterHelper$UpdateOp r3 = (android.support.v7.widget.AdapterHelper.UpdateOp) r3
                r2 = r3
                r3 = r0
                int r3 = r3.cmd
                r4 = r2
                int r4 = r4.cmd
                if (r3 == r4) goto L_0x002a
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x002a:
                r3 = r0
                int r3 = r3.cmd
                r4 = 8
                if (r3 != r4) goto L_0x0052
                r3 = r0
                int r3 = r3.itemCount
                r4 = r0
                int r4 = r4.positionStart
                int r3 = r3 - r4
                int r3 = java.lang.Math.abs(r3)
                r4 = 1
                if (r3 != r4) goto L_0x0052
                r3 = r0
                int r3 = r3.itemCount
                r4 = r2
                int r4 = r4.positionStart
                if (r3 != r4) goto L_0x0052
                r3 = r0
                int r3 = r3.positionStart
                r4 = r2
                int r4 = r4.itemCount
                if (r3 != r4) goto L_0x0052
                r3 = 1
                r0 = r3
                goto L_0x0008
            L_0x0052:
                r3 = r0
                int r3 = r3.itemCount
                r4 = r2
                int r4 = r4.itemCount
                if (r3 == r4) goto L_0x005d
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x005d:
                r3 = r0
                int r3 = r3.positionStart
                r4 = r2
                int r4 = r4.positionStart
                if (r3 == r4) goto L_0x0068
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x0068:
                r3 = r0
                java.lang.Object r3 = r3.payload
                if (r3 == 0) goto L_0x007c
                r3 = r0
                java.lang.Object r3 = r3.payload
                r4 = r2
                java.lang.Object r4 = r4.payload
                boolean r3 = r3.equals(r4)
                if (r3 != 0) goto L_0x0084
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x007c:
                r3 = r2
                java.lang.Object r3 = r3.payload
                if (r3 == 0) goto L_0x0084
                r3 = 0
                r0 = r3
                goto L_0x0008
            L_0x0084:
                r3 = 1
                r0 = r3
                goto L_0x0008
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AdapterHelper.UpdateOp.equals(java.lang.Object):boolean");
        }

        public int hashCode() {
            return (31 * ((31 * this.cmd) + this.positionStart)) + this.itemCount;
        }
    }

    public UpdateOp obtainUpdateOp(int i, int i2, int i3, Object obj) {
        UpdateOp updateOp;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        Object obj2 = obj;
        UpdateOp acquire = this.mUpdateOpPool.acquire();
        if (acquire == null) {
            new UpdateOp(i4, i5, i6, obj2);
            acquire = updateOp;
        } else {
            acquire.cmd = i4;
            acquire.positionStart = i5;
            acquire.itemCount = i6;
            acquire.payload = obj2;
        }
        return acquire;
    }

    public void recycleUpdateOp(UpdateOp updateOp) {
        UpdateOp updateOp2 = updateOp;
        if (!this.mDisableRecycler) {
            updateOp2.payload = null;
            boolean release = this.mUpdateOpPool.release(updateOp2);
        }
    }

    /* access modifiers changed from: package-private */
    public void recycleUpdateOpsAndClearList(List<UpdateOp> list) {
        List<UpdateOp> list2 = list;
        int size = list2.size();
        for (int i = 0; i < size; i++) {
            recycleUpdateOp(list2.get(i));
        }
        list2.clear();
    }
}
