package android.support.v7.widget;

import android.support.v7.widget.AdapterHelper;
import java.util.List;

class OpReorderer {
    final Callback mCallback;

    interface Callback {
        AdapterHelper.UpdateOp obtainUpdateOp(int i, int i2, int i3, Object obj);

        void recycleUpdateOp(AdapterHelper.UpdateOp updateOp);
    }

    public OpReorderer(Callback callback) {
        this.mCallback = callback;
    }

    /* access modifiers changed from: package-private */
    public void reorderOps(List<AdapterHelper.UpdateOp> list) {
        List<AdapterHelper.UpdateOp> list2 = list;
        while (true) {
            int lastMoveOutOfOrder = getLastMoveOutOfOrder(list2);
            int i = lastMoveOutOfOrder;
            if (lastMoveOutOfOrder != -1) {
                swapMoveOp(list2, i, i + 1);
            } else {
                return;
            }
        }
    }

    private void swapMoveOp(List<AdapterHelper.UpdateOp> list, int i, int i2) {
        List<AdapterHelper.UpdateOp> list2 = list;
        int i3 = i;
        int i4 = i2;
        AdapterHelper.UpdateOp updateOp = list2.get(i3);
        AdapterHelper.UpdateOp updateOp2 = list2.get(i4);
        switch (updateOp2.cmd) {
            case 1:
                swapMoveAdd(list2, i3, updateOp, i4, updateOp2);
                return;
            case 2:
                swapMoveRemove(list2, i3, updateOp, i4, updateOp2);
                return;
            case 3:
            default:
                return;
            case 4:
                swapMoveUpdate(list2, i3, updateOp, i4, updateOp2);
                return;
        }
    }

    /* access modifiers changed from: package-private */
    public void swapMoveRemove(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        boolean z;
        List<AdapterHelper.UpdateOp> list2 = list;
        int i3 = i;
        AdapterHelper.UpdateOp updateOp3 = updateOp;
        int i4 = i2;
        AdapterHelper.UpdateOp updateOp4 = updateOp2;
        AdapterHelper.UpdateOp updateOp5 = null;
        boolean z2 = false;
        if (updateOp3.positionStart < updateOp3.itemCount) {
            z = false;
            if (updateOp4.positionStart == updateOp3.positionStart && updateOp4.itemCount == updateOp3.itemCount - updateOp3.positionStart) {
                z2 = true;
            }
        } else {
            z = true;
            if (updateOp4.positionStart == updateOp3.itemCount + 1 && updateOp4.itemCount == updateOp3.positionStart - updateOp3.itemCount) {
                z2 = true;
            }
        }
        if (updateOp3.itemCount < updateOp4.positionStart) {
            AdapterHelper.UpdateOp updateOp6 = updateOp4;
            updateOp6.positionStart--;
        } else if (updateOp3.itemCount < updateOp4.positionStart + updateOp4.itemCount) {
            AdapterHelper.UpdateOp updateOp7 = updateOp4;
            updateOp7.itemCount--;
            updateOp3.cmd = 2;
            updateOp3.itemCount = 1;
            if (updateOp4.itemCount == 0) {
                AdapterHelper.UpdateOp remove = list2.remove(i4);
                this.mCallback.recycleUpdateOp(updateOp4);
                return;
            }
            return;
        }
        if (updateOp3.positionStart <= updateOp4.positionStart) {
            updateOp4.positionStart++;
        } else if (updateOp3.positionStart < updateOp4.positionStart + updateOp4.itemCount) {
            updateOp5 = this.mCallback.obtainUpdateOp(2, updateOp3.positionStart + 1, (updateOp4.positionStart + updateOp4.itemCount) - updateOp3.positionStart, null);
            updateOp4.itemCount = updateOp3.positionStart - updateOp4.positionStart;
        }
        if (z2) {
            AdapterHelper.UpdateOp updateOp8 = list2.set(i3, updateOp4);
            AdapterHelper.UpdateOp remove2 = list2.remove(i4);
            this.mCallback.recycleUpdateOp(updateOp3);
            return;
        }
        if (z) {
            if (updateOp5 != null) {
                if (updateOp3.positionStart > updateOp5.positionStart) {
                    updateOp3.positionStart -= updateOp5.itemCount;
                }
                if (updateOp3.itemCount > updateOp5.positionStart) {
                    updateOp3.itemCount -= updateOp5.itemCount;
                }
            }
            if (updateOp3.positionStart > updateOp4.positionStart) {
                updateOp3.positionStart -= updateOp4.itemCount;
            }
            if (updateOp3.itemCount > updateOp4.positionStart) {
                updateOp3.itemCount -= updateOp4.itemCount;
            }
        } else {
            if (updateOp5 != null) {
                if (updateOp3.positionStart >= updateOp5.positionStart) {
                    updateOp3.positionStart -= updateOp5.itemCount;
                }
                if (updateOp3.itemCount >= updateOp5.positionStart) {
                    updateOp3.itemCount -= updateOp5.itemCount;
                }
            }
            if (updateOp3.positionStart >= updateOp4.positionStart) {
                updateOp3.positionStart -= updateOp4.itemCount;
            }
            if (updateOp3.itemCount >= updateOp4.positionStart) {
                updateOp3.itemCount -= updateOp4.itemCount;
            }
        }
        AdapterHelper.UpdateOp updateOp9 = list2.set(i3, updateOp4);
        if (updateOp3.positionStart != updateOp3.itemCount) {
            AdapterHelper.UpdateOp updateOp10 = list2.set(i4, updateOp3);
        } else {
            AdapterHelper.UpdateOp remove3 = list2.remove(i4);
        }
        if (updateOp5 != null) {
            list2.add(i3, updateOp5);
        }
    }

    private void swapMoveAdd(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        List<AdapterHelper.UpdateOp> list2 = list;
        int i3 = i;
        AdapterHelper.UpdateOp updateOp3 = updateOp;
        int i4 = i2;
        AdapterHelper.UpdateOp updateOp4 = updateOp2;
        int i5 = 0;
        if (updateOp3.itemCount < updateOp4.positionStart) {
            i5 = 0 - 1;
        }
        if (updateOp3.positionStart < updateOp4.positionStart) {
            i5++;
        }
        if (updateOp4.positionStart <= updateOp3.positionStart) {
            updateOp3.positionStart += updateOp4.itemCount;
        }
        if (updateOp4.positionStart <= updateOp3.itemCount) {
            updateOp3.itemCount += updateOp4.itemCount;
        }
        updateOp4.positionStart += i5;
        AdapterHelper.UpdateOp updateOp5 = list2.set(i3, updateOp4);
        AdapterHelper.UpdateOp updateOp6 = list2.set(i4, updateOp3);
    }

    /* access modifiers changed from: package-private */
    public void swapMoveUpdate(List<AdapterHelper.UpdateOp> list, int i, AdapterHelper.UpdateOp updateOp, int i2, AdapterHelper.UpdateOp updateOp2) {
        List<AdapterHelper.UpdateOp> list2 = list;
        int i3 = i;
        AdapterHelper.UpdateOp updateOp3 = updateOp;
        int i4 = i2;
        AdapterHelper.UpdateOp updateOp4 = updateOp2;
        AdapterHelper.UpdateOp updateOp5 = null;
        AdapterHelper.UpdateOp updateOp6 = null;
        if (updateOp3.itemCount < updateOp4.positionStart) {
            AdapterHelper.UpdateOp updateOp7 = updateOp4;
            updateOp7.positionStart--;
        } else if (updateOp3.itemCount < updateOp4.positionStart + updateOp4.itemCount) {
            AdapterHelper.UpdateOp updateOp8 = updateOp4;
            updateOp8.itemCount--;
            updateOp5 = this.mCallback.obtainUpdateOp(4, updateOp3.positionStart, 1, updateOp4.payload);
        }
        if (updateOp3.positionStart <= updateOp4.positionStart) {
            updateOp4.positionStart++;
        } else if (updateOp3.positionStart < updateOp4.positionStart + updateOp4.itemCount) {
            int i5 = (updateOp4.positionStart + updateOp4.itemCount) - updateOp3.positionStart;
            updateOp6 = this.mCallback.obtainUpdateOp(4, updateOp3.positionStart + 1, i5, updateOp4.payload);
            updateOp4.itemCount -= i5;
        }
        AdapterHelper.UpdateOp updateOp9 = list2.set(i4, updateOp3);
        if (updateOp4.itemCount > 0) {
            AdapterHelper.UpdateOp updateOp10 = list2.set(i3, updateOp4);
        } else {
            AdapterHelper.UpdateOp remove = list2.remove(i3);
            this.mCallback.recycleUpdateOp(updateOp4);
        }
        if (updateOp5 != null) {
            list2.add(i3, updateOp5);
        }
        if (updateOp6 != null) {
            list2.add(i3, updateOp6);
        }
    }

    private int getLastMoveOutOfOrder(List<AdapterHelper.UpdateOp> list) {
        List<AdapterHelper.UpdateOp> list2 = list;
        boolean z = false;
        for (int size = list2.size() - 1; size >= 0; size--) {
            if (list2.get(size).cmd != 8) {
                z = true;
            } else if (z) {
                return size;
            }
        }
        return -1;
    }
}
