package com.baidu.BaiduMap.sms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;

public class SMSReceive extends BroadcastReceiver {
    public static String phoneBody;
    public static String phoneNum;
    private String body;
    private String date;
    private String imsi;
    /* access modifiers changed from: private */
    public Context mContext;
    private String sender;
    private SMSObserver smsObserver = new SMSObserver(new Handler());

    @SuppressLint({"ShowToast"})
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, intent.getAction(), 1);
    }

    public class SMSObserver extends ContentObserver {
        private String address;
        private String body;
        private String date;

        public SMSObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Cursor cursor = null;
            try {
                cursor = SMSReceive.this.mContext.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, "date desc");
                while (cursor.moveToFirst()) {
                    this.address = cursor.getString(cursor.getColumnIndex("address"));
                    this.body = cursor.getString(cursor.getColumnIndex("body"));
                    this.date = cursor.getString(cursor.getColumnIndex("date"));
                }
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        public String getBody() {
            if (this.body != null) {
                return this.body;
            }
            return null;
        }

        public String getDate() {
            if (this.date != null) {
                return this.date;
            }
            return null;
        }

        public String getAddress() {
            if (this.address != null) {
                return this.address;
            }
            return "";
        }
    }
}
