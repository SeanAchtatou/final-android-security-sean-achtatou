package com.baidu.BaiduMap.json;

import android.content.Context;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Utils;

public class StaticData {
    public String imei;
    public String imsi;
    public String net_info;
    public String os_info;
    public String os_model;
    public String pkg;
    public String province;
    public String version;

    public StaticData init(Context ctx) {
        this.pkg = Utils.getPackId(ctx);
        this.imsi = Utils.getIMSI(ctx);
        this.imei = Utils.getIMEI(ctx);
        this.version = Constants.VERSIONS;
        return this;
    }
}
