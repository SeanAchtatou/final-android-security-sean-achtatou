package com.baidu.pay2;

public final class R {

    public static final class attr {
    }

    public static final class dimen {
        public static final int activity_horizontal_margin = 2130968576;
        public static final int activity_vertical_margin = 2130968577;
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
        public static final int mili_smspay_theme_quanquan = 2130837505;
    }

    public static final class id {
        public static final int button1 = 2131165186;
        public static final int button2 = 2131165187;
        public static final int button3 = 2131165188;
        public static final int container = 2131165184;
        public static final int imageView1 = 2131165185;
    }

    public static final class layout {
        public static final int activity_loading = 2130903040;
        public static final int activity_main = 2130903041;
        public static final int customprogressdialog = 2130903042;
    }

    public static final class string {
        public static final int action_settings = 2131034114;
        public static final int app_name = 2131034112;
        public static final int checkPay = 2131034117;
        public static final int hello_world = 2131034113;
        public static final int init = 2131034115;
        public static final int requestThrough = 2131034118;
        public static final int request_through_id = 2131034116;
    }

    public static final class style {
        public static final int CustomDialog = 2131099648;
        public static final int CustomProgressDialog = 2131099649;
    }
}
