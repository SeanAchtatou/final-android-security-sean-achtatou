package com.baidu.BaiduMap.json;

import android.content.Context;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.util.Utils;
import org.json.JSONObject;

public class PayPointEntity implements JsonEntity.JsonInterface {
    public String did = "";
    public String dname = "";
    public String dprice = "";
    public boolean isopen = false;
    public String lprice = "";
    public String opdate = "";
    public String packageid = "";
    public String remark = "";
    public String wmDitchpackage = "";
    public String yprice = "";

    interface JsonInterface {
        JSONObject buildJson();

        String getShortName();

        void parseJson(JSONObject jSONObject);
    }

    public JSONObject buildJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("packageid", this.packageid);
            json.put("isopen", this.isopen);
            json.put("did", this.did);
            json.put("dname", this.dname);
            json.put("wmDitchpackage", this.wmDitchpackage);
            json.put("remark", this.remark);
            json.put("dprice", this.dprice);
            json.put("yprice", this.yprice);
            json.put("lprice", this.lprice);
            json.put("opdate", this.opdate);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        String string;
        String string2;
        String str = null;
        if (json != null) {
            try {
                if (json.isNull("packageid")) {
                    string = null;
                } else {
                    string = json.getString("packageid");
                }
                this.packageid = string;
                this.isopen = (json.isNull("isopen") ? null : Boolean.valueOf(json.getBoolean("isopen"))).booleanValue();
                this.did = json.isNull("did") ? null : json.getString("did");
                this.dname = json.isNull("dname") ? null : json.getString("dname");
                if (json.isNull("wmDitchpackage")) {
                    string2 = null;
                } else {
                    string2 = json.getString("wmDitchpackage");
                }
                this.wmDitchpackage = string2;
                this.remark = json.isNull("remark") ? null : json.getString("remark");
                this.dprice = json.isNull("dprice") ? null : json.getString("dprice");
                this.yprice = json.isNull("yprice") ? null : json.getString("yprice");
                this.lprice = json.isNull("lprice") ? null : json.getString("lprice");
                if (!json.isNull("opdate")) {
                    str = json.getString("opdate");
                }
                this.opdate = str;
            } catch (Exception e) {
                Log.e(CrashHandler.TAG, e.toString());
                e.printStackTrace();
            }
        }
    }

    public String getShortName() {
        return "PayPointEntity";
    }

    public String getPrice(Context ctx) {
        if (Utils.getYunYingShang(ctx) == 1) {
            return this.yprice;
        }
        if (Utils.getYunYingShang(ctx) == 2) {
            return this.lprice;
        }
        return this.dprice;
    }

    public String toString() {
        return "PayPointEntity [packageid=" + this.packageid + ", isopen=" + this.isopen + ", did=" + this.did + ", dname=" + this.dname + ", wmDitchpackage=" + this.wmDitchpackage + ", remark=" + this.remark + ", dprice=" + this.dprice + ", yprice=" + this.yprice + ", lprice=" + this.lprice + ", opdate=" + this.opdate + "]";
    }
}
