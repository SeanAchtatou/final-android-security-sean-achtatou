package com.baidu.BaiduMap.util;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class OfflineSmsTool {
    static final /* synthetic */ boolean $assertionsDisabled = (!OfflineSmsTool.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final String baseAscII = " !\"#$%'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
    public static final BigInteger baseNum = BigInteger.valueOf((long) baseAscII.length());

    public static String sig(String data, String apSecert) throws NoSuchAlgorithmException, IOException {
        if (!$assertionsDisabled && data == null) {
            throw new AssertionError("签名数据不能为空");
        } else if ($assertionsDisabled || apSecert != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            stream.write(apSecert.getBytes());
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(data.getBytes());
            stream.write(md5.digest());
            md5.update(stream.toByteArray());
            byte[] md = md5.digest();
            byte[] ret = new byte[5];
            for (int i = 0; i < md.length; i++) {
                int i2 = (i % 4) + 1;
                ret[i2] = (byte) (ret[i2] ^ md[i]);
            }
            return toBase94(new BigInteger(ret));
        } else {
            throw new AssertionError("apSecert不能为空");
        }
    }

    public static String toBase94(long value) {
        return toBase94(BigInteger.valueOf(value));
    }

    public static String toBase94(BigInteger value) {
        if ($assertionsDisabled || value.compareTo(BigInteger.ZERO) >= 0) {
            StringBuilder result = new StringBuilder();
            while (value.compareTo(BigInteger.ZERO) > 0) {
                BigInteger[] bis = value.divideAndRemainder(baseNum);
                result.append(baseAscII.charAt(bis[1].intValue()));
                value = bis[0];
            }
            return result.reverse().toString();
        }
        throw new AssertionError("toBase94 值必须大于等于0");
    }

    public static BigInteger fromBase94(String value) {
        if ($assertionsDisabled || value != null) {
            BigInteger result = BigInteger.ZERO;
            char[] charArray = value.toCharArray();
            int length = charArray.length;
            int i = 0;
            while (i < length) {
                int i2 = baseAscII.indexOf(charArray[i]);
                if ($assertionsDisabled || i2 >= 0) {
                    result = result.multiply(baseNum).add(BigInteger.valueOf((long) i2));
                    i++;
                } else {
                    throw new AssertionError("不是一个有效的Base94字符串:" + value);
                }
            }
            return result;
        }
        throw new AssertionError("不是一个有效的Base94字符串: null");
    }

    public static String getCurTime() {
        return toBase94(System.currentTimeMillis() / 60000);
    }

    public static Long getOrderNumber(Context context) {
        String imei = Utils.getIMEI(context);
        try {
            return Long.valueOf(Long.parseLong(String.valueOf(imei.substring(imei.length() - 4, imei.length())) + System.currentTimeMillis()));
        } catch (Exception e) {
            return Long.valueOf(System.currentTimeMillis());
        }
    }

    public static String getContent(Context mContext, String customized_price, Long orderNumber, Long THChannelID, String ThroughId, String MD5, String did) {
        Long imsi;
        int price;
        try {
            imsi = Long.valueOf(Long.parseLong(Utils.getIMSI(mContext)));
        } catch (Exception e) {
            imsi = 0L;
        }
        try {
            price = Integer.parseInt(customized_price);
        } catch (Exception e2) {
            price = 0;
        }
        StringBuilder content = new StringBuilder();
        content.append(toBase94(1)).append("&").append(getCurTime()).append("&").append(toBase94((long) price)).append("&").append(toBase94(orderNumber.longValue())).append("&").append(toBase94(THChannelID.longValue())).append("&").append(toBase94(1)).append("&").append(toBase94(imsi.longValue())).append("&").append("ll" + ThroughId + "x" + Utils.getPackId(mContext));
        try {
            content.append("&").append(sig(content.toString(), MD5));
        } catch (Exception e3) {
        }
        return content.toString();
    }
}
