package a.a.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

final class a implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ b f0a;
    private /* synthetic */ c b;

    a(c cVar, b bVar) {
        this.b = cVar;
        this.f0a = bVar;
    }

    public final Object invoke(Object obj, Method method, Object[] objArr) {
        c.f1a.b(new StringBuilder().insert(0, "invoke: ").append(method.getName()).append(" listener: ").append(this.f0a).toString());
        if ("onZoom".equals(method.getName())) {
            this.f0a.a(((Boolean) objArr[0]).booleanValue());
            return null;
        } else if ("onVisibilityChanged".equals(method.getName())) {
            ((Boolean) objArr[0]).booleanValue();
            return null;
        } else {
            c.f1a.b(new StringBuilder().insert(0, "unhandled listener method: ").append(method).toString());
            return null;
        }
    }
}
