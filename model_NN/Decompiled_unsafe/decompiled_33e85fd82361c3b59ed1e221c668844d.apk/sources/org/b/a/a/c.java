package org.b.a.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private int f319a;
    private float[] b = new float[20];
    private float[] c = new float[20];
    private float[] d = new float[20];
    private int[] e = new int[20];
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private float k;
    private float l;
    private float m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    /* access modifiers changed from: private */
    public long t;

    static /* synthetic */ void a(c cVar, int i2, float[] fArr, float[] fArr2, float[] fArr3, int[] iArr, int i3, boolean z, long j2) {
        boolean z2;
        c cVar2;
        c cVar3;
        int i4 = 0;
        cVar.t = j2;
        cVar.s = i3;
        cVar.f319a = i2;
        int i5 = 0;
        while (i4 < i2) {
            cVar.b[i5] = fArr[i5];
            cVar.c[i5] = fArr2[i5];
            cVar.d[i5] = fArr3[i5];
            cVar.e[i5] = iArr[i5];
            i4 = i5 + 1;
            i5 = i4;
        }
        cVar.n = z;
        if (i2 >= 2) {
            z2 = true;
            cVar2 = cVar;
        } else {
            z2 = false;
            cVar2 = cVar;
        }
        cVar2.o = z2;
        if (cVar.o) {
            cVar.f = (fArr[0] + fArr[1]) * 0.5f;
            cVar.g = (fArr2[0] + fArr2[1]) * 0.5f;
            cVar.h = (fArr3[0] + fArr3[1]) * 0.5f;
            cVar.i = Math.abs(fArr[1] - fArr[0]);
            cVar.j = Math.abs(fArr2[1] - fArr2[0]);
            cVar3 = cVar;
        } else {
            cVar.f = fArr[0];
            cVar.g = fArr2[0];
            cVar.h = fArr3[0];
            cVar.j = 0.0f;
            cVar.i = 0.0f;
            cVar3 = cVar;
        }
        cVar3.r = false;
        cVar.q = false;
        cVar.p = false;
    }

    public final boolean a() {
        return this.o;
    }

    public final float b() {
        if (this.o) {
            return this.i;
        }
        return 0.0f;
    }

    public final float c() {
        if (this.o) {
            return this.j;
        }
        return 0.0f;
    }

    public final float d() {
        c cVar;
        c cVar2;
        c cVar3;
        float f2;
        float f3 = 0.0f;
        if (!this.q) {
            if (!this.o) {
                this.k = 0.0f;
            } else {
                if (!this.p) {
                    if (this.o) {
                        f2 = (this.i * this.i) + (this.j * this.j);
                        cVar3 = this;
                    } else {
                        cVar3 = this;
                        f2 = 0.0f;
                    }
                    cVar3.l = f2;
                    this.p = true;
                }
                float f4 = this.l;
                if (f4 == 0.0f) {
                    cVar = this;
                } else {
                    int i2 = (int) (f4 * 256.0f);
                    int i3 = 15;
                    int i4 = 32768;
                    int i5 = 0;
                    while (true) {
                        int i6 = i5;
                        int i7 = ((i5 << 1) + i4) << i3;
                        if (i2 >= i7) {
                            i2 -= i7;
                            i5 = i6 + i4;
                        } else {
                            i5 = i6;
                        }
                        i4 >>= 1;
                        if (i4 <= 0) {
                            break;
                        }
                        i3--;
                    }
                    f3 = ((float) i5) / 16.0f;
                    cVar = this;
                }
                cVar.k = f3;
                if (this.k < this.i) {
                    this.k = this.i;
                }
                if (this.k < this.j) {
                    this.k = this.j;
                    cVar2 = this;
                    cVar2.q = true;
                }
            }
            cVar2 = this;
            cVar2.q = true;
        }
        return this.k;
    }

    public final float e() {
        c cVar;
        if (!this.r) {
            if (!this.o) {
                this.m = 0.0f;
                cVar = this;
            } else {
                this.m = (float) Math.atan2((double) (this.c[1] - this.c[0]), (double) (this.b[1] - this.b[0]));
                cVar = this;
            }
            cVar.r = true;
        }
        return this.m;
    }

    public final float f() {
        return this.f;
    }

    public final float g() {
        return this.g;
    }

    public final boolean h() {
        return this.n;
    }

    public final long i() {
        return this.t;
    }
}
