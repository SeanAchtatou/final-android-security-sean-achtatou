package org.osmdroid.c;

import org.osmdroid.b.f;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private f f360a;
    private int b;

    public b(f fVar, int i) {
        this.f360a = fVar;
        this.b = i;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "ZoomEvent [source=").append(this.f360a).append(", zoomLevel=").append(this.b).append("]").toString();
    }
}
