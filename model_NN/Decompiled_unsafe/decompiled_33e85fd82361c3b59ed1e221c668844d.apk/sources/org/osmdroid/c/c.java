package org.osmdroid.c;

import org.osmdroid.b.f;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private f f361a;
    private int b;
    private int c;

    public c(f fVar, int i, int i2) {
        this.f361a = fVar;
        this.b = i;
        this.c = i2;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "ScrollEvent [source=").append(this.f361a).append(", x=").append(this.b).append(", y=").append(this.c).append("]").toString();
    }
}
