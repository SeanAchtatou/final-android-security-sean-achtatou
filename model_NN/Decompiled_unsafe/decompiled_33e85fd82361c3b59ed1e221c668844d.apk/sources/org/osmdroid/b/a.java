package org.osmdroid.b;

import android.graphics.Point;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f344a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final BoundingBoxE6 f;
    private final int g;
    private final int h;
    private final int i;
    private final Point j;
    private final Point k;
    private /* synthetic */ f l;

    /* synthetic */ a(f fVar) {
        this(fVar, (byte) 0);
    }

    private /* synthetic */ a(f fVar, byte b2) {
        this.l = fVar;
        this.f344a = this.l.getWidth() / 2;
        this.b = this.l.getHeight() / 2;
        this.c = this.l.j() / 2;
        this.d = -this.c;
        this.e = -this.c;
        this.g = fVar.c;
        this.h = fVar.d;
        this.i = f.a(this.h);
        int i2 = this.h;
        int i3 = this.g;
        int a2 = f.a(i2);
        int i4 = 1 << (i3 - 1);
        this.j = new Point((this.l.getScrollX() >> a2) + i4, (this.l.getScrollY() >> a2) + i4);
        this.k = f.a(fVar, this.j, this.h);
        this.f = fVar.d();
    }

    public final float a(float f2) {
        return (f2 / 4.0075016E7f) * ((float) this.l.j());
    }

    public final int a() {
        return this.h;
    }

    public final Point a(int i2, int i3, Point point) {
        Point point2;
        if (point != null) {
            point2 = point;
        } else {
            point = new Point();
            point2 = point;
        }
        point.set(i2 - this.f344a, i3 - this.b);
        point2.offset(this.l.getScrollX(), this.l.getScrollY());
        return point2;
    }

    public final Point a(GeoPoint geoPoint, Point point) {
        GeoPoint geoPoint2;
        if (point != null) {
            geoPoint2 = geoPoint;
        } else {
            point = new Point();
            geoPoint2 = geoPoint;
        }
        Point a2 = org.osmdroid.b.b.a.a(geoPoint2.a(), geoPoint.b(), this.l.k());
        point.set(a2.x, a2.y);
        point.offset(this.d, this.e);
        return point;
    }

    public final GeoPoint a(float f2, float f3) {
        return this.f.a(f2 / ((float) this.l.getWidth()), f3 / ((float) this.l.getHeight()));
    }

    public final GeoPoint a(int i2, int i3) {
        return a((float) i2, (float) i3);
    }

    public final int b() {
        return this.i;
    }

    public final Point b(GeoPoint geoPoint, Point point) {
        return a(geoPoint, point);
    }

    public final int c() {
        return this.g;
    }
}
