package org.osmdroid.a;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import org.osmdroid.a.b.v;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Queue f341a = new LinkedList();
    private final f b;
    private final g c;
    private v d;

    public d(f fVar, v[] vVarArr, g gVar) {
        Collections.addAll(this.f341a, vVarArr);
        this.b = fVar;
        this.c = gVar;
    }

    public final f a() {
        return this.b;
    }

    public final g b() {
        return this.c;
    }

    public final v c() {
        this.d = (v) this.f341a.poll();
        return this.d;
    }
}
