package org.osmdroid.a.a;

import org.osmdroid.a.f;

final class a extends d implements g {
    private Integer f = 1;

    a(String str, org.osmdroid.a aVar, int i, int i2, String str2, String... strArr) {
        super(str, aVar, 0, i, i2, str2, strArr);
    }

    public final String a() {
        return (this.f == null || this.f.intValue() <= 1) ? this.c : new StringBuilder().insert(0, this.c).append(this.f).toString();
    }

    public final String a(f fVar) {
        String a2 = org.osmdroid.a.c.a.a();
        String b = org.osmdroid.a.c.a.b();
        return String.format(b(), a2, this.f, Integer.valueOf(f()), Integer.valueOf(fVar.a()), Integer.valueOf(fVar.b()), Integer.valueOf(fVar.c()), this.d, b);
    }

    public final void a(String str) {
        this.f = Integer.getInteger(str);
    }
}
