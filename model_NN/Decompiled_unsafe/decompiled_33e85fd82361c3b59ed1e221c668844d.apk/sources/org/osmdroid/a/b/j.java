package org.osmdroid.a.b;

final class j extends i {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ q f331a;

    /* synthetic */ j(q qVar) {
        this(qVar, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private /* synthetic */ j(q qVar, byte b) {
        super(qVar);
        this.f331a = qVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.graphics.drawable.Drawable a(org.osmdroid.a.d r7) {
        /*
            r6 = this;
            r0 = 0
            org.osmdroid.a.b.q r1 = r6.f331a
            org.osmdroid.a.a.f r1 = r1.c
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            org.osmdroid.a.f r1 = r7.a()
            org.osmdroid.a.b.q r2 = r6.f331a
            boolean r2 = r2.a()
            if (r2 == 0) goto L_0x0007
            org.osmdroid.a.b.q r2 = r6.f331a     // Catch:{ Throwable -> 0x0030, all -> 0x0041 }
            java.io.InputStream r2 = r2.a(r1)     // Catch:{ Throwable -> 0x0030, all -> 0x0041 }
            if (r2 == 0) goto L_0x002a
            org.osmdroid.a.b.q r1 = r6.f331a     // Catch:{ Throwable -> 0x0052, all -> 0x004c }
            org.osmdroid.a.a.f r1 = r1.c     // Catch:{ Throwable -> 0x0052, all -> 0x004c }
            android.graphics.drawable.Drawable r0 = r1.a(r2)     // Catch:{ Throwable -> 0x0052, all -> 0x004c }
            if (r2 == 0) goto L_0x0007
            org.osmdroid.a.c.c.a(r2)
            goto L_0x0007
        L_0x002a:
            if (r2 == 0) goto L_0x0007
            org.osmdroid.a.c.c.a(r2)
            goto L_0x0007
        L_0x0030:
            r1 = move-exception
            r2 = r0
        L_0x0032:
            org.a.c r3 = org.osmdroid.a.b.q.e     // Catch:{ all -> 0x004f }
            java.lang.String r4 = "Error loading tile"
            r3.c(r4, r1)     // Catch:{ all -> 0x004f }
            if (r2 == 0) goto L_0x0007
            org.osmdroid.a.c.c.a(r2)
            goto L_0x0007
        L_0x0041:
            r1 = move-exception
            r2 = r0
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x0046:
            if (r2 == 0) goto L_0x004b
            org.osmdroid.a.c.c.a(r1)
        L_0x004b:
            throw r0
        L_0x004c:
            r0 = move-exception
            r1 = r2
            goto L_0x0046
        L_0x004f:
            r0 = move-exception
            r1 = r2
            goto L_0x0046
        L_0x0052:
            r1 = move-exception
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.j.a(org.osmdroid.a.d):android.graphics.drawable.Drawable");
    }
}
