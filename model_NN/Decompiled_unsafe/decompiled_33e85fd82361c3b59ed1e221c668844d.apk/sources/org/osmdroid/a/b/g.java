package org.osmdroid.a.b;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.a.a;
import org.a.c;
import org.osmdroid.a.a.f;

public class g implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final c f329a = a.a(g.class);
    private final ZipFile b;

    private /* synthetic */ g(ZipFile zipFile) {
        this.b = zipFile;
    }

    public static g a(File file) {
        return new g(new ZipFile(file));
    }

    public final InputStream a(f fVar, org.osmdroid.a.f fVar2) {
        try {
            ZipEntry entry = this.b.getEntry(fVar.b(fVar2));
            if (entry != null) {
                return this.b.getInputStream(entry);
            }
        } catch (IOException e) {
            f329a.b(new StringBuilder().insert(0, "Error getting zip stream: ").append(fVar2).toString(), e);
        }
        return null;
    }

    public String toString() {
        return new StringBuilder().insert(0, "ZipFileArchive [mZipFile=").append(this.b.getName()).append("]").toString();
    }
}
