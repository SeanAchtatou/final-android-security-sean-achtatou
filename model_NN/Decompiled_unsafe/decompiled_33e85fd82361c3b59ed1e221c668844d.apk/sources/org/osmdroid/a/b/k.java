package org.osmdroid.a.b;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.a.c;
import org.osmdroid.a.d.a;

public class k implements c, a {
    private static final c c = org.a.a.a(k.class);
    /* access modifiers changed from: private */
    public static long d;

    public k() {
        b bVar = new b(this);
        bVar.setPriority(1);
        bVar.start();
    }

    private static /* synthetic */ boolean a(File file) {
        if (file.mkdirs()) {
            return true;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        return file.exists();
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b() {
        int i = 0;
        synchronized (b) {
            if (d > 524288000) {
                c.b(new StringBuilder().insert(0, "Trimming tile cache from ").append(d).append(" to ").append(524288000L).toString());
                File[] fileArr = (File[]) c(b).toArray(new File[0]);
                Arrays.sort(fileArr, new a(this));
                int length = fileArr.length;
                int i2 = 0;
                while (i2 < length) {
                    File file = fileArr[i];
                    if (d <= 524288000) {
                        break;
                    }
                    long length2 = file.length();
                    if (file.delete()) {
                        d -= length2;
                    }
                    i++;
                    i2 = i;
                }
                c.b("Finished trimming tile cache");
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            int length = listFiles.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                File file2 = listFiles[i2];
                if (file2.isFile()) {
                    d += file2.length();
                }
                if (file2.isDirectory()) {
                    b(file2);
                }
                i = i2 + 1;
                i2 = i;
            }
        }
    }

    private /* synthetic */ List c(File file) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            int length = listFiles.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                File file2 = listFiles[i2];
                if (file2.isFile()) {
                    arrayList.add(file2);
                }
                if (file2.isDirectory()) {
                    arrayList.addAll(c(file2));
                }
                i = i2 + 1;
                i2 = i;
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(org.osmdroid.a.a.f r7, org.osmdroid.a.f r8, java.io.InputStream r9) {
        /*
            r6 = this;
            r2 = 0
            r0 = 0
            java.io.File r3 = new java.io.File
            java.io.File r1 = org.osmdroid.a.b.k.b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r7.b(r8)
            java.lang.StringBuilder r4 = r4.insert(r0, r5)
            java.lang.String r5 = ".tile"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r1, r4)
            java.io.File r1 = r3.getParentFile()
            boolean r4 = r1.exists()
            if (r4 != 0) goto L_0x0031
            boolean r1 = a(r1)
            if (r1 != 0) goto L_0x0031
        L_0x0030:
            return r0
        L_0x0031:
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0059, all -> 0x0061 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0059, all -> 0x0061 }
            java.lang.String r3 = r3.getPath()     // Catch:{ IOException -> 0x0059, all -> 0x0061 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0059, all -> 0x0061 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r4, r3)     // Catch:{ IOException -> 0x0059, all -> 0x0061 }
            long r2 = org.osmdroid.a.c.c.a(r9, r1)     // Catch:{ IOException -> 0x006c, all -> 0x0069 }
            long r4 = org.osmdroid.a.b.k.d     // Catch:{ IOException -> 0x006c, all -> 0x0069 }
            long r2 = r2 + r4
            org.osmdroid.a.b.k.d = r2     // Catch:{ IOException -> 0x006c, all -> 0x0069 }
            r4 = 629145600(0x25800000, double:3.10839227E-315)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0054
            r6.b()     // Catch:{ IOException -> 0x006c, all -> 0x0069 }
        L_0x0054:
            org.osmdroid.a.c.c.a(r1)
            r0 = 1
            goto L_0x0030
        L_0x0059:
            r1 = move-exception
            r1 = r2
        L_0x005b:
            if (r1 == 0) goto L_0x0030
            org.osmdroid.a.c.c.a(r2)
            goto L_0x0030
        L_0x0061:
            r0 = move-exception
            r1 = r2
        L_0x0063:
            if (r1 == 0) goto L_0x0068
            org.osmdroid.a.c.c.a(r2)
        L_0x0068:
            throw r0
        L_0x0069:
            r0 = move-exception
            r2 = r1
            goto L_0x0063
        L_0x006c:
            r2 = move-exception
            r2 = r1
            goto L_0x005b
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.a.b.k.a(org.osmdroid.a.a.f, org.osmdroid.a.f, java.io.InputStream):boolean");
    }
}
