package org.osmdroid.a;

import android.graphics.drawable.Drawable;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.osmdroid.a.d.a;

public final class h implements a {
    private b c;
    private final ReadWriteLock d;

    private /* synthetic */ h() {
        this.d = new ReentrantReadWriteLock();
        this.c = new b();
    }

    public h(byte b) {
        this();
    }

    public final Drawable a(f fVar) {
        this.d.readLock().lock();
        Drawable drawable = (Drawable) this.c.get(fVar);
        this.d.readLock().unlock();
        return drawable;
    }

    public final void a(int i) {
        this.d.readLock().lock();
        this.c.a(i);
        this.d.readLock().unlock();
    }

    public final void a(f fVar, Drawable drawable) {
        if (drawable != null) {
            this.d.writeLock().lock();
            this.c.put(fVar, drawable);
            this.d.writeLock().unlock();
        }
    }

    public final boolean b(f fVar) {
        this.d.readLock().lock();
        boolean containsKey = this.c.containsKey(fVar);
        this.d.readLock().unlock();
        return containsKey;
    }
}
