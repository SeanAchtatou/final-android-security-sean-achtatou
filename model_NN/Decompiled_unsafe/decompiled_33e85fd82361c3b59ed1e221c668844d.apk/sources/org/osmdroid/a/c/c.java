package org.osmdroid.a.c;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.a.a;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final org.a.c f339a = a.a(c.class);

    private /* synthetic */ c() {
    }

    public static long a(InputStream inputStream, OutputStream outputStream) {
        long j = 0;
        byte[] bArr = new byte[8192];
        while (true) {
            long j2 = j;
            int read = inputStream.read(bArr);
            if (read == -1) {
                return j2;
            }
            outputStream.write(bArr, 0, read);
            j = ((long) read) + j2;
        }
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                f339a.a("IO", "Could not close stream", e);
            }
        }
    }
}
