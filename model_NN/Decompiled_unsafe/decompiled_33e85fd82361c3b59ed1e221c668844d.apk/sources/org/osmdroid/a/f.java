package org.osmdroid.a;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final int f343a;
    private final int b;
    private final int c;

    public f(int i, int i2, int i3) {
        this.c = i;
        this.f343a = i2;
        this.b = i3;
    }

    public final int a() {
        return this.c;
    }

    public final int b() {
        return this.f343a;
    }

    public final int c() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        return this.c == fVar.c && this.f343a == fVar.f343a && this.b == fVar.b;
    }

    public final int hashCode() {
        return (this.c + 37) * 17 * (this.f343a + 37) * (this.b + 37);
    }

    public final String toString() {
        return new StringBuilder().insert(0, "/").append(this.c).append("/").append(this.f343a).append("/").append(this.b).toString();
    }
}
