package org.a.b;

import android.util.Log;
import org.a.a.a;
import org.a.a.h;

public final class c extends a {
    c(String str) {
        this.f315a = str;
    }

    public final void a(String str) {
        Log.d(this.f315a, str);
    }

    public final void a(String str, Object obj, Object obj2) {
        Log.e(this.f315a, h.a(str, new Object[]{obj, obj2}).a());
    }

    public final void a(String str, Throwable th) {
        Log.i(this.f315a, str, th);
    }

    public final void b(String str) {
        Log.i(this.f315a, str);
    }

    public final void b(String str, Throwable th) {
        Log.w(this.f315a, str, th);
    }

    public final void c(String str) {
        Log.w(this.f315a, str);
    }

    public final void c(String str, Throwable th) {
        Log.e(this.f315a, str, th);
    }

    public final void d(String str) {
        Log.e(this.f315a, str);
    }
}
