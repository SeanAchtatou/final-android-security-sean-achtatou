package org.a.b;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.a.b;

public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Map f316a = new HashMap();

    /* access modifiers changed from: private */
    /* renamed from: b */
    public /* synthetic */ c a(String str) {
        a aVar;
        String str2;
        String str3;
        StringTokenizer stringTokenizer;
        if (str == null || str.length() <= 23) {
            aVar = this;
            str2 = str;
        } else {
            StringTokenizer stringTokenizer2 = new StringTokenizer(str, ".");
            if (stringTokenizer2.hasMoreTokens()) {
                StringBuilder sb = new StringBuilder();
                do {
                    String nextToken = stringTokenizer2.nextToken();
                    if (nextToken.length() == 1) {
                        sb.append(nextToken);
                        sb.append('.');
                        stringTokenizer = stringTokenizer2;
                    } else if (stringTokenizer2.hasMoreTokens()) {
                        sb.append(nextToken.charAt(0));
                        sb.append("*.");
                        stringTokenizer = stringTokenizer2;
                    } else {
                        sb.append(nextToken);
                        stringTokenizer = stringTokenizer2;
                    }
                } while (stringTokenizer.hasMoreTokens());
                str3 = sb.toString();
                str2 = str3;
            } else {
                str3 = str;
                str2 = str;
            }
            if (str3.length() > 23) {
                str2 = new StringBuilder().insert(0, str2.substring(0, 22)).append('*').toString();
            }
            aVar = this;
        }
        synchronized (aVar) {
            c cVar = (c) this.f316a.get(str2);
            if (cVar == null) {
                if (!str2.equals(str)) {
                    Log.i(a.class.getSimpleName(), new StringBuilder().insert(0, "Logger name '").append(str).append("' exceeds maximum length of ").append(23).append(" characters, using '").append(str2).append("' instead.").toString());
                }
                cVar = new c(str2);
                this.f316a.put(str2, cVar);
            }
            return cVar;
        }
    }
}
