package org.c.a.a.a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class a {
    private static /* synthetic */ MessageDigest a(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] a(byte[] bArr) {
        return a("SHA-256").digest(bArr);
    }
}
