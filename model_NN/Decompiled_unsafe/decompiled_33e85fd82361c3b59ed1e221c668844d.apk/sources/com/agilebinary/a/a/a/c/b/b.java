package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.f;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.g;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.net.ConnectException;
import java.net.Socket;
import org.apache.commons.logging.Log;

public final class b implements m {

    /* renamed from: a  reason: collision with root package name */
    private final Log f43a = a.a(getClass());
    private h b;

    public b(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.b = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private static /* synthetic */ void a(Socket socket, e eVar) {
        boolean z = true;
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        socket.setTcpNoDelay(eVar.a("http.tcp.nodelay", true));
        socket.setSoTimeout(c.a(eVar));
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        int a2 = eVar.a("http.socket.linger", -1);
        if (a2 >= 0) {
            if (a2 <= 0) {
                z = false;
            }
            socket.setSoLinger(z, a2);
        }
    }

    public final g a() {
        return new h();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      com.agilebinary.a.a.a.h.b.i.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, com.agilebinary.a.a.a.e.e):java.net.Socket
      com.agilebinary.a.a.a.h.b.f.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public final void a(g gVar, com.agilebinary.a.a.a.b bVar, e eVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!gVar.l()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            com.agilebinary.a.a.a.h.b.g a2 = this.b.a(bVar.c());
            if (!(a2.b() instanceof f)) {
                throw new IllegalArgumentException(new StringBuilder().insert(0, "Target scheme (").append(a2.c()).append(") must have layered socket factory.").toString());
            }
            f fVar = (f) a2.b();
            try {
                Socket a3 = fVar.a(gVar.i_(), bVar.a(), bVar.b(), true);
                a(a3, eVar);
                gVar.a(a3, bVar, fVar.a(a3), eVar);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.a.h.f(bVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c5  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00ea A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.g r14, com.agilebinary.a.a.a.b r15, java.net.InetAddress r16, com.agilebinary.a.a.a.e.e r17) {
        /*
            r13 = this;
            if (r14 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Connection may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000a:
            if (r15 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Target host may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0014:
            if (r17 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Parameters may not be null"
            r1.<init>(r2)
            throw r1
        L_0x001e:
            boolean r1 = r14.l()
            if (r1 == 0) goto L_0x002c
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "Connection must not be open"
            r1.<init>(r2)
            throw r1
        L_0x002c:
            com.agilebinary.a.a.a.h.b.h r1 = r13.b
            java.lang.String r2 = r15.c()
            com.agilebinary.a.a.a.h.b.g r1 = r1.a(r2)
            com.agilebinary.a.a.a.h.b.i r2 = r1.b()
            java.lang.String r3 = r15.a()
            java.net.InetAddress[] r6 = java.net.InetAddress.getAllByName(r3)
            int r3 = r15.b()
            int r7 = r1.a(r3)
            r1 = 0
            r4 = r1
        L_0x004c:
            int r3 = r6.length
            if (r1 >= r3) goto L_0x00ab
            r8 = r6[r4]
            int r1 = r6.length
            int r1 = r1 + -1
            if (r4 != r1) goto L_0x00ac
            r1 = 1
            r3 = r1
            r1 = r2
        L_0x0059:
            java.net.Socket r5 = r1.f_()
            r14.a(r5, r15)
            java.net.InetSocketAddress r9 = new java.net.InetSocketAddress
            r9.<init>(r8, r7)
            r1 = 0
            if (r16 == 0) goto L_0x0070
            java.net.InetSocketAddress r1 = new java.net.InetSocketAddress
            r8 = 0
            r0 = r16
            r1.<init>(r0, r8)
        L_0x0070:
            org.apache.commons.logging.Log r8 = r13.f43a
            boolean r8 = r8.isDebugEnabled()
            if (r8 == 0) goto L_0x0091
            org.apache.commons.logging.Log r8 = r13.f43a
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r11 = 0
            java.lang.String r12 = "Connecting to "
            java.lang.StringBuilder r10 = r10.insert(r11, r12)
            java.lang.StringBuilder r10 = r10.append(r9)
            java.lang.String r10 = r10.toString()
            r8.debug(r10)
        L_0x0091:
            r0 = r17
            java.net.Socket r1 = r2.a(r5, r9, r1, r0)     // Catch:{ ConnectException -> 0x00b0, h -> 0x00b9 }
            if (r5 == r1) goto L_0x00ef
            r14.a(r1, r15)     // Catch:{ ConnectException -> 0x00b0, h -> 0x00b9 }
            r5 = r1
        L_0x009d:
            r0 = r17
            a(r1, r0)     // Catch:{ ConnectException -> 0x00b0, h -> 0x00b9 }
            boolean r1 = r2.a(r5)     // Catch:{ ConnectException -> 0x00b0, h -> 0x00b9 }
            r0 = r17
            r14.a(r1, r0)     // Catch:{ ConnectException -> 0x00b0, h -> 0x00b9 }
        L_0x00ab:
            return
        L_0x00ac:
            r1 = 0
            r3 = r1
            r1 = r2
            goto L_0x0059
        L_0x00b0:
            r1 = move-exception
            if (r3 == 0) goto L_0x00bd
            com.agilebinary.a.a.a.h.f r2 = new com.agilebinary.a.a.a.h.f
            r2.<init>(r15, r1)
            throw r2
        L_0x00b9:
            r1 = move-exception
            if (r3 == 0) goto L_0x00bd
            throw r1
        L_0x00bd:
            org.apache.commons.logging.Log r1 = r13.f43a
            boolean r1 = r1.isDebugEnabled()
            if (r1 == 0) goto L_0x00ea
            org.apache.commons.logging.Log r1 = r13.f43a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r5 = 0
            java.lang.String r8 = "Connect to "
            java.lang.StringBuilder r3 = r3.insert(r5, r8)
            java.lang.StringBuilder r3 = r3.append(r9)
            java.lang.String r5 = " timed out. "
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r5 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r3 = r3.append(r5)
            java.lang.String r3 = r3.toString()
            r1.debug(r3)
        L_0x00ea:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x004c
        L_0x00ef:
            r1 = r5
            goto L_0x009d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.b.a(com.agilebinary.a.a.a.h.g, com.agilebinary.a.a.a.b, java.net.InetAddress, com.agilebinary.a.a.a.e.e):void");
    }
}
