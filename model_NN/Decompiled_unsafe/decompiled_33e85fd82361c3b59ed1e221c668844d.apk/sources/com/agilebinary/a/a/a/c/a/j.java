package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.b;
import com.agilebinary.a.a.a.k.e;

public final class j extends x {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f28a;

    public j(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Array of date patterns may not be null");
        }
        this.f28a = strArr;
    }

    public final void a(b bVar, String str) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new e("Missing value for expires attribute");
        } else {
            try {
                bVar.a(l.a(str, this.f28a));
            } catch (k e) {
                throw new e(new StringBuilder().insert(0, "Unable to parse expires attribute: ").append(str).toString());
            }
        }
    }
}
