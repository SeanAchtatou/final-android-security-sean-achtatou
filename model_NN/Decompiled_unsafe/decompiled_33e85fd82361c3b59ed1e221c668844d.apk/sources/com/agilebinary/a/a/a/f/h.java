package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f;

public final class h implements ab {
    public final void a(f fVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!fVar.a("User-Agent")) {
            e g = fVar.g();
            if (g == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            String str = (String) g.a("http.useragent");
            if (str != null) {
                fVar.a("User-Agent", str);
            }
        }
    }
}
