package com.agilebinary.a.a.a.h.e;

import java.util.regex.Pattern;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f112a = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
    private static final Pattern b = Pattern.compile("^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$");
    private static final Pattern c = Pattern.compile("^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$");

    private /* synthetic */ a() {
    }

    public static boolean a(String str) {
        return f112a.matcher(str).matches();
    }

    public static boolean b(String str) {
        return b.matcher(str).matches() || c.matcher(str).matches();
    }
}
