package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.b.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.t;
import java.util.ArrayList;
import java.util.List;

public final class w extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static String[] f33a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private final String[] c;

    public w() {
        this(null);
    }

    public w(String[] strArr) {
        w wVar;
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
            wVar = this;
        } else {
            this.c = b;
            wVar = this;
        }
        wVar.a("path", new m());
        a("domain", new r());
        a("max-age", new ac());
        a("secure", new h());
        a("comment", new o());
        a("expires", new j(this.c));
    }

    public final int a() {
        return 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List a(com.agilebinary.a.a.a.t r7, com.agilebinary.a.a.a.k.f r8) {
        /*
            r6 = this;
            r2 = 1
            r5 = -1
            r3 = 0
            if (r7 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Header may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000d:
            if (r8 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Cookie origin may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0017:
            java.lang.String r0 = r7.a()
            java.lang.String r1 = r7.b()
            java.lang.String r4 = "Set-Cookie"
            boolean r0 = r0.equalsIgnoreCase(r4)
            if (r0 != 0) goto L_0x004a
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unrecognized cookie header '"
            java.lang.StringBuilder r1 = r1.insert(r3, r2)
            java.lang.String r2 = r7.toString()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "'"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x004a:
            java.util.Locale r0 = java.util.Locale.ENGLISH
            java.lang.String r0 = r1.toLowerCase(r0)
            java.lang.String r4 = "expires="
            int r0 = r0.indexOf(r4)
            if (r0 == r5) goto L_0x00a0
            java.lang.String r4 = "expires="
            int r4 = r4.length()
            int r4 = r4 + r0
            r0 = 59
            int r0 = r1.indexOf(r0, r4)
            if (r0 != r5) goto L_0x006b
            int r0 = r1.length()
        L_0x006b:
            java.lang.String r0 = r1.substring(r4, r0)     // Catch:{ k -> 0x009f }
            java.lang.String[] r1 = r6.c     // Catch:{ k -> 0x009f }
            com.agilebinary.a.a.a.c.a.l.a(r0, r1)     // Catch:{ k -> 0x009f }
            r0 = r2
        L_0x0075:
            if (r0 == 0) goto L_0x00c6
            boolean r0 = r7 instanceof com.agilebinary.a.a.a.r
            if (r0 == 0) goto L_0x00a2
            r0 = r7
            com.agilebinary.a.a.a.r r0 = (com.agilebinary.a.a.a.r) r0
            com.agilebinary.a.a.a.i.c r1 = r0.e()
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            com.agilebinary.a.a.a.r r7 = (com.agilebinary.a.a.a.r) r7
            int r4 = r7.d()
            int r5 = r1.c()
            r0.<init>(r4, r5)
        L_0x0091:
            com.agilebinary.a.a.a.m[] r2 = new com.agilebinary.a.a.a.m[r2]
            com.agilebinary.a.a.a.m r0 = com.agilebinary.a.a.a.c.a.aa.a(r1, r0)
            r2[r3] = r0
            r0 = r2
        L_0x009a:
            java.util.List r0 = r6.a(r0, r8)
            return r0
        L_0x009f:
            r0 = move-exception
        L_0x00a0:
            r0 = r3
            goto L_0x0075
        L_0x00a2:
            java.lang.String r0 = r7.b()
            if (r0 != 0) goto L_0x00b0
            com.agilebinary.a.a.a.k.e r0 = new com.agilebinary.a.a.a.k.e
            java.lang.String r1 = "Header value is null"
            r0.<init>(r1)
            throw r0
        L_0x00b0:
            com.agilebinary.a.a.a.i.c r1 = new com.agilebinary.a.a.a.i.c
            int r4 = r0.length()
            r1.<init>(r4)
            r1.a(r0)
            com.agilebinary.a.a.a.b.i r0 = new com.agilebinary.a.a.a.b.i
            int r4 = r1.c()
            r0.<init>(r3, r4)
            goto L_0x0091
        L_0x00c6:
            com.agilebinary.a.a.a.m[] r0 = r7.c()
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.a.w.a(com.agilebinary.a.a.a.t, com.agilebinary.a.a.a.k.f):java.util.List");
    }

    public final List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            c cVar = new c(list.size() * 20);
            int i = 0;
            cVar.a("Cookie");
            cVar.a(": ");
            while (true) {
                int i2 = i;
                if (i < list.size()) {
                    com.agilebinary.a.a.a.k.c cVar2 = (com.agilebinary.a.a.a.k.c) list.get(i2);
                    if (i2 > 0) {
                        cVar.a("; ");
                    }
                    cVar.a(cVar2.a());
                    cVar.a("=");
                    String b2 = cVar2.b();
                    if (b2 != null) {
                        cVar.a(b2);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new e(cVar));
                    return arrayList;
                }
            }
        }
    }

    public final t b() {
        return null;
    }

    public final String toString() {
        return "compatibility";
    }
}
