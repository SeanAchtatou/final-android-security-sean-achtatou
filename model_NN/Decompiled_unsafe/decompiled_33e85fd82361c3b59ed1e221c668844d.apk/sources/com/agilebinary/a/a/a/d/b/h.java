package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.a.t;
import java.util.Collection;
import java.util.Iterator;

public final class h implements ab {
    public final void a(f fVar, k kVar) {
        Collection collection;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!fVar.a().a().equalsIgnoreCase("CONNECT") && (collection = (Collection) fVar.g().a("http.default-headers")) != null) {
            Iterator it = collection.iterator();
            while (it.hasNext()) {
                fVar.a((t) it.next());
            }
        }
    }
}
