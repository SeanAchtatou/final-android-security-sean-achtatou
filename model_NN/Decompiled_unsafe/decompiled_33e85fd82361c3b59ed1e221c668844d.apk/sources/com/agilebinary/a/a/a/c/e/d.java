package com.agilebinary.a.a.a.c.e;

import com.agilebinary.a.a.a.g.a;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.t;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;

public final class d implements a {
    public final long a(x xVar) {
        long j;
        long j2;
        if (xVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        boolean c = xVar.g().c("http.protocol.strict-transfer-encoding");
        t c2 = xVar.c("Transfer-Encoding");
        t c3 = xVar.c("Content-Length");
        if (c2 != null) {
            try {
                m[] c4 = c2.c();
                if (c) {
                    int i = 0;
                    int i2 = 0;
                    while (i < c4.length) {
                        String a2 = c4[i2].a();
                        if (a2 == null || a2.length() <= 0 || a2.equalsIgnoreCase("chunked") || a2.equalsIgnoreCase("identity")) {
                            i = i2 + 1;
                            i2 = i;
                        } else {
                            throw new l(new StringBuilder().insert(0, "Unsupported transfer encoding: ").append(a2).toString());
                        }
                    }
                }
                int length = c4.length;
                if ("identity".equalsIgnoreCase(c2.b())) {
                    j = -1;
                } else if (length > 0 && "chunked".equalsIgnoreCase(c4[length - 1].a())) {
                    return -2;
                } else {
                    if (!c) {
                        return -1;
                    }
                    throw new l("Chunk-encoding must be the last one applied");
                }
            } catch (u e) {
                throw new l(new StringBuilder().insert(0, "Invalid Transfer-Encoding header value: ").append(c2).toString(), e);
            }
        } else if (c3 == null) {
            return -1;
        } else {
            t[] b = xVar.b("Content-Length");
            if (!c || b.length <= 1) {
                int length2 = b.length - 1;
                int i3 = length2;
                while (true) {
                    if (length2 < 0) {
                        j = -1;
                        j2 = -1;
                        break;
                    }
                    t tVar = b[i3];
                    try {
                        j = Long.parseLong(tVar.b());
                        j2 = j;
                        break;
                    } catch (NumberFormatException e2) {
                        if (c) {
                            throw new l(new StringBuilder().insert(0, "Invalid content length: ").append(tVar.b()).toString());
                        }
                        length2 = i3 - 1;
                        i3 = length2;
                    }
                }
                if (j2 < 0) {
                    return -1;
                }
            } else {
                throw new l("Multiple content length headers");
            }
        }
        return j;
    }
}
