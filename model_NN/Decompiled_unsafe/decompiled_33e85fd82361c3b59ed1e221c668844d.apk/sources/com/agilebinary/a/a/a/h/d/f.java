package com.agilebinary.a.a.a.h.d;

import com.agilebinary.a.a.a.e.c;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.h.b.b;
import com.agilebinary.a.a.a.h.b.j;
import com.agilebinary.a.a.a.h.h;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public final class f implements com.agilebinary.a.a.a.h.b.f, j {

    /* renamed from: a  reason: collision with root package name */
    private static b f111a = new e();
    private static b b = new c();
    private static b c = new a();
    private static final f d = new f();
    private final SSLSocketFactory e = HttpsURLConnection.getDefaultSSLSocketFactory();
    private final b f = null;
    private volatile b g = null;

    private /* synthetic */ f() {
    }

    public static f b() {
        return d;
    }

    public final Socket a() {
        return new Socket();
    }

    public final Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, e eVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            if (i2 < 0) {
                i2 = 0;
            }
            inetSocketAddress = new InetSocketAddress(inetAddress, i2);
        }
        return a(socket, new InetSocketAddress(this.f != null ? this.f.a() : InetAddress.getByName(str), i), inetSocketAddress, eVar);
    }

    public final Socket a(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.e.createSocket(socket, str, i, z);
        if (this.g != null) {
            this.g.a(str, sSLSocket);
        }
        return sSLSocket;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    public final Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, e eVar) {
        Socket socket2;
        InetSocketAddress inetSocketAddress3;
        SSLSocket sSLSocket;
        f fVar;
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            if (socket != null) {
                inetSocketAddress3 = inetSocketAddress2;
                socket2 = socket;
            } else {
                socket2 = new Socket();
                inetSocketAddress3 = inetSocketAddress2;
            }
            if (inetSocketAddress3 != null) {
                socket2.setReuseAddress(c.b(eVar));
                socket2.bind(inetSocketAddress2);
            }
            int c2 = c.c(eVar);
            int a2 = c.a(eVar);
            try {
                socket2.connect(inetSocketAddress, c2);
                socket2.setSoTimeout(a2);
                if (socket2 instanceof SSLSocket) {
                    sSLSocket = (SSLSocket) socket2;
                    fVar = this;
                } else {
                    sSLSocket = (SSLSocket) this.e.createSocket(socket2, inetSocketAddress.getHostName(), inetSocketAddress.getPort(), true);
                    fVar = this;
                }
                if (fVar.g != null) {
                    try {
                        this.g.a(inetSocketAddress.getHostName(), sSLSocket);
                    } catch (IOException e2) {
                        try {
                            sSLSocket.close();
                        } catch (Exception e3) {
                        }
                        throw e2;
                    }
                }
                return sSLSocket;
            } catch (SocketTimeoutException e4) {
                throw new h(new StringBuilder().insert(0, "Connect to ").append(inetSocketAddress.getHostName()).append("/").append(inetSocketAddress.getAddress()).append(" timed out").toString());
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed");
        }
    }

    public final Socket a_(Socket socket, String str, int i, boolean z) {
        return a(socket, str, i, z);
    }

    public final Socket f_() {
        return new Socket();
    }
}
