package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.e;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.w;
import java.util.NoSuchElementException;

public final class d implements e {

    /* renamed from: a  reason: collision with root package name */
    private w f9a;
    private String b;
    private String c;
    private int d;

    public d(w wVar) {
        if (wVar == null) {
            throw new IllegalArgumentException("Header iterator must not be null.");
        }
        this.f9a = wVar;
        this.d = a(-1);
    }

    private /* synthetic */ int a(int i) {
        int i2 = 0;
        if (i < 0) {
            if (!this.f9a.hasNext()) {
                return -1;
            }
            this.b = this.f9a.a().b();
        } else if (i < 0) {
            throw new IllegalArgumentException(new StringBuilder().insert(0, "Search position must not be negative: ").append(i).toString());
        } else {
            int length = this.b.length();
            boolean z = false;
            boolean z2 = false;
            int i3 = i;
            while (!z && i3 < length) {
                char charAt = this.b.charAt(i3);
                if (a(charAt)) {
                    z = true;
                    z2 = true;
                } else if (b(charAt)) {
                    i3++;
                    z = z2;
                } else if (c(charAt)) {
                    throw new u(new StringBuilder().insert(0, "Tokens without separator (pos ").append(i3).append("): ").append(this.b).toString());
                } else {
                    throw new u(new StringBuilder().insert(0, "Invalid character after token (pos ").append(i3).append("): ").append(this.b).toString());
                }
            }
            i2 = i3;
        }
        int b2 = b(i2);
        if (b2 < 0) {
            this.c = null;
            return -1;
        }
        int c2 = c(b2);
        this.c = this.b.substring(b2, c2);
        return c2;
    }

    private static /* synthetic */ boolean a(char c2) {
        return c2 == ',';
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a0 A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ int b(int r6) {
        /*
            r5 = this;
            r1 = 0
            if (r6 >= 0) goto L_0x001c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Search position must not be negative: "
            java.lang.StringBuilder r1 = r2.insert(r1, r3)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x001c:
            r0 = r1
        L_0x001d:
            r3 = r0
            r2 = r0
            r0 = r6
        L_0x0020:
            if (r3 != 0) goto L_0x009d
            java.lang.String r3 = r5.b
            if (r3 == 0) goto L_0x009d
            java.lang.String r3 = r5.b
            int r3 = r3.length()
            r6 = r0
            r0 = r2
        L_0x002e:
            if (r2 != 0) goto L_0x007c
            if (r6 >= r3) goto L_0x007c
            java.lang.String r2 = r5.b
            char r2 = r2.charAt(r6)
            boolean r4 = a(r2)
            if (r4 != 0) goto L_0x0044
            boolean r2 = b(r2)
            if (r2 == 0) goto L_0x0048
        L_0x0044:
            int r6 = r6 + 1
            r2 = r0
            goto L_0x002e
        L_0x0048:
            java.lang.String r0 = r5.b
            char r0 = r0.charAt(r6)
            boolean r0 = c(r0)
            if (r0 == 0) goto L_0x0057
            r0 = 1
            r2 = r0
            goto L_0x002e
        L_0x0057:
            com.agilebinary.a.a.a.u r0 = new com.agilebinary.a.a.a.u
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid character before token (pos "
            java.lang.StringBuilder r1 = r2.insert(r1, r3)
            java.lang.StringBuilder r1 = r1.append(r6)
            java.lang.String r2 = "): "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r5.b
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x007c:
            if (r0 != 0) goto L_0x001d
            com.agilebinary.a.a.a.w r2 = r5.f9a
            boolean r2 = r2.hasNext()
            if (r2 == 0) goto L_0x0096
            com.agilebinary.a.a.a.w r2 = r5.f9a
            com.agilebinary.a.a.a.t r2 = r2.a()
            java.lang.String r2 = r2.b()
            r5.b = r2
            r3 = r0
            r2 = r0
            r0 = r1
            goto L_0x0020
        L_0x0096:
            r2 = 0
            r5.b = r2
            r3 = r0
            r2 = r0
            r0 = r6
            goto L_0x0020
        L_0x009d:
            if (r2 == 0) goto L_0x00a0
        L_0x009f:
            return r0
        L_0x00a0:
            r0 = -1
            goto L_0x009f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.b.d.b(int):int");
    }

    private static /* synthetic */ boolean b(char c2) {
        return c2 == 9 || Character.isSpaceChar(c2);
    }

    private /* synthetic */ int c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException(new StringBuilder().insert(0, "Token start position must not be negative: ").append(i).toString());
        }
        int length = this.b.length();
        int i2 = i + 1;
        int i3 = i2;
        while (i2 < length && c(this.b.charAt(i3))) {
            i2 = i3 + 1;
            i3 = i2;
        }
        return i3;
    }

    private static /* synthetic */ boolean c(char c2) {
        if (Character.isLetterOrDigit(c2)) {
            return true;
        }
        if (Character.isISOControl(c2)) {
            return false;
        }
        return !(" ,;=()<>@:\\\"/[]?{}\t".indexOf(c2) >= 0);
    }

    public final String a() {
        if (this.c == null) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        String str = this.c;
        this.d = a(this.d);
        return str;
    }

    public final boolean hasNext() {
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Removing tokens is not supported.");
    }
}
