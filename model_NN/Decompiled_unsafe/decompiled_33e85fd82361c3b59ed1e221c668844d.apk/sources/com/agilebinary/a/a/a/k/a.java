package com.agilebinary.a.a.a.k;

import java.io.Serializable;
import java.util.Comparator;

public final class a implements Serializable, Comparator {
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* bridge */ /* synthetic */ int compare(java.lang.Object r6, java.lang.Object r7) {
        /*
            r5 = this;
            r2 = 46
            r4 = 0
            r3 = -1
            com.agilebinary.a.a.a.k.c r6 = (com.agilebinary.a.a.a.k.c) r6
            com.agilebinary.a.a.a.k.c r7 = (com.agilebinary.a.a.a.k.c) r7
            java.lang.String r0 = r6.a()
            java.lang.String r1 = r7.a()
            int r0 = r0.compareTo(r1)
            if (r0 != 0) goto L_0x002c
            java.lang.String r0 = r6.c()
            if (r0 != 0) goto L_0x0043
            java.lang.String r0 = ""
        L_0x001e:
            r1 = r0
            r0 = r7
        L_0x0020:
            java.lang.String r0 = r0.c()
            if (r0 != 0) goto L_0x005f
            java.lang.String r0 = ""
        L_0x0028:
            int r0 = r1.compareToIgnoreCase(r0)
        L_0x002c:
            if (r0 != 0) goto L_0x0042
            java.lang.String r0 = r6.d()
            if (r0 != 0) goto L_0x0036
            java.lang.String r0 = "/"
        L_0x0036:
            java.lang.String r1 = r7.d()
            if (r1 != 0) goto L_0x003e
            java.lang.String r1 = "/"
        L_0x003e:
            int r0 = r0.compareTo(r1)
        L_0x0042:
            return r0
        L_0x0043:
            int r1 = r0.indexOf(r2)
            if (r1 != r3) goto L_0x001e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.StringBuilder r0 = r1.insert(r4, r0)
            java.lang.String r1 = ".local"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = r0
            r0 = r7
            goto L_0x0020
        L_0x005f:
            int r2 = r0.indexOf(r2)
            if (r2 != r3) goto L_0x0028
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuilder r0 = r2.insert(r4, r0)
            java.lang.String r2 = ".local"
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.k.a.compare(java.lang.Object, java.lang.Object):int");
    }
}
