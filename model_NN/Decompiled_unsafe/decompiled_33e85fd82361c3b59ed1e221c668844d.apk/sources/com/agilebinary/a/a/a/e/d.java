package com.agilebinary.a.a.a.e;

public abstract class d implements e {
    protected d() {
    }

    public final int a(String str, int i) {
        Object a2 = a(str);
        return a2 == null ? i : ((Integer) a2).intValue();
    }

    public final boolean a(String str, boolean z) {
        Object a2 = a(str);
        return a2 == null ? z : ((Boolean) a2).booleanValue();
    }

    public final e b(String str) {
        a(str, Boolean.TRUE);
        return this;
    }

    public final e b(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.d.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.d.a(java.lang.String, boolean):boolean */
    public final boolean c(String str) {
        return a(str, false);
    }

    public final boolean d(String str) {
        return !a(str, false);
    }
}
