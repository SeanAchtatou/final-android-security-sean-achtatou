package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.q;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final Log f45a = a.a(getClass());
    private final Map b = new HashMap();

    public final void a() {
        this.b.clear();
    }

    public final void a(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (this.f45a.isDebugEnabled()) {
            this.f45a.debug(new StringBuilder().insert(0, "Checking for connections, idle timeout: ").append(currentTimeMillis).toString());
        }
        Iterator it = this.b.entrySet().iterator();
        while (true) {
            while (true) {
                if (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    q qVar = (q) entry.getKey();
                    long a2 = ((o) entry.getValue()).f53a;
                    if (a2 <= currentTimeMillis) {
                        if (this.f45a.isDebugEnabled()) {
                            this.f45a.debug(new StringBuilder().insert(0, "Closing idle connection, connection time: ").append(a2).toString());
                        }
                        try {
                            qVar.k();
                        } catch (IOException e) {
                            this.f45a.debug("I/O error closing connection", e);
                        }
                    }
                } else {
                    return;
                }
            }
        }
    }
}
