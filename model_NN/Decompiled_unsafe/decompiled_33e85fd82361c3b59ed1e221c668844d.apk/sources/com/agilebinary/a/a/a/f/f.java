package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.ad;
import com.agilebinary.a.a.a.j;

public final class f implements c {

    /* renamed from: a  reason: collision with root package name */
    private final ab[] f91a;
    private final ad[] b;

    public f(ab[] abVarArr, ad[] adVarArr) {
        int i = 0;
        if (abVarArr != null) {
            int length = abVarArr.length;
            this.f91a = new ab[length];
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                this.f91a[i3] = abVarArr[i3];
                i2 = i3 + 1;
                i3 = i2;
            }
        } else {
            this.f91a = new ab[0];
        }
        if (adVarArr != null) {
            int length2 = adVarArr.length;
            this.b = new ad[length2];
            int i4 = 0;
            while (i < length2) {
                this.b[i4] = adVarArr[i4];
                i = i4 + 1;
                i4 = i;
            }
            return;
        }
        this.b = new ad[0];
    }

    public final void a(com.agilebinary.a.a.a.f fVar, k kVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i < this.f91a.length) {
                this.f91a[i2].a(fVar, kVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final void a(j jVar, k kVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i < this.b.length) {
                this.b[i2].a(jVar, kVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }
}
