package com.agilebinary.a.a.a.c.a;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f29a = {"EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private static final Date b;
    private static TimeZone c = TimeZone.getTimeZone("GMT");

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(c);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        b = instance.getTime();
    }

    private /* synthetic */ l() {
    }

    public static Date a(String str, String[] strArr) {
        return b(str, strArr);
    }

    private static /* synthetic */ Date b(String str, String[] strArr) {
        if (str == null) {
            throw new IllegalArgumentException("dateValue is null");
        }
        if (strArr == null) {
            strArr = f29a;
        }
        Date date = b;
        if (str.length() > 1 && str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1);
        }
        int length = strArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            SimpleDateFormat a2 = ad.a(strArr[i2]);
            a2.set2DigitYearStart(date);
            try {
                return a2.parse(str);
            } catch (ParseException e) {
                i = i2 + 1;
                i2 = i;
            }
        }
        throw new k(new StringBuilder().insert(0, "Unable to parse the date ").append(str).toString());
    }
}
