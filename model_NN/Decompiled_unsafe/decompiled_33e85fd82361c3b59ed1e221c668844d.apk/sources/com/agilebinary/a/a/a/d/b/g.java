package com.agilebinary.a.a.a.d.b;

import com.agilebinary.a.a.a.a.c;
import com.agilebinary.a.a.a.a.e;
import com.agilebinary.a.a.a.a.h;
import com.agilebinary.a.a.a.ab;
import com.agilebinary.a.a.a.b;
import com.agilebinary.a.a.a.c.d.j;
import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.f.k;
import com.agilebinary.a.a.b.a.a;
import org.apache.commons.logging.Log;

public final class g implements ab {

    /* renamed from: a  reason: collision with root package name */
    private final Log f89a = a.a(getClass());

    private /* synthetic */ void a(b bVar, h hVar, e eVar, com.agilebinary.a.a.a.d.e eVar2) {
        String b = hVar.b();
        if (this.f89a.isDebugEnabled()) {
            this.f89a.debug(new StringBuilder().insert(0, "Re-using cached '").append(b).append("' auth scheme for ").append(bVar).toString());
        }
        com.agilebinary.a.a.a.a.g a2 = eVar2.a(new c(bVar.a(), bVar.b(), b));
        if (a2 != null) {
            eVar.a(hVar);
            eVar.a(a2);
            return;
        }
        this.f89a.debug("No credentials for preemptive authentication");
    }

    public final void a(f fVar, k kVar) {
        h a2;
        h a3;
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            com.agilebinary.a.a.a.d.g gVar = (com.agilebinary.a.a.a.d.g) kVar.a("http.auth.auth-cache");
            if (gVar == null) {
                kVar.a("http.auth.auth-cache", new j());
                return;
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a("http.auth.credentials-provider");
            b bVar = (b) kVar.a("http.target_host");
            if (!(bVar == null || (a3 = gVar.a(bVar)) == null)) {
                a(bVar, a3, (e) kVar.a("http.auth.target-scope"), eVar);
            }
            b bVar2 = (b) kVar.a("http.proxy_host");
            if (bVar2 != null && (a2 = gVar.a(bVar2)) != null) {
                a(bVar2, a2, (e) kVar.a("http.auth.proxy-scope"), eVar);
            }
        }
    }
}
