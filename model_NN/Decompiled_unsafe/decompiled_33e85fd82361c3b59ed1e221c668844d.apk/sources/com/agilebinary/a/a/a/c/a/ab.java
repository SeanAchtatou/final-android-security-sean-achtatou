package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.k.c;
import com.agilebinary.a.a.a.k.f;
import com.agilebinary.a.a.a.k.i;
import java.util.Locale;
import java.util.StringTokenizer;

public final class ab extends r {
    public final void a(c cVar, f fVar) {
        super.a(cVar, fVar);
        String a2 = fVar.a();
        String c = cVar.c();
        if (a2.contains(".")) {
            int countTokens = new StringTokenizer(c, ".").countTokens();
            String upperCase = c.toUpperCase(Locale.ENGLISH);
            if (upperCase.endsWith(".COM") || upperCase.endsWith(".EDU") || upperCase.endsWith(".NET") || upperCase.endsWith(".GOV") || upperCase.endsWith(".MIL") || upperCase.endsWith(".ORG") || upperCase.endsWith(".INT")) {
                if (countTokens < 2) {
                    throw new i(new StringBuilder().insert(0, "Domain attribute \"").append(c).append("\" violates the Netscape cookie specification for ").append("special domains").toString());
                }
            } else if (countTokens < 3) {
                throw new i(new StringBuilder().insert(0, "Domain attribute \"").append(c).append("\" violates the Netscape cookie specification").toString());
            }
        }
    }

    public final boolean b(c cVar, f fVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = fVar.a();
            String c = cVar.c();
            if (c == null) {
                return false;
            }
            return a2.endsWith(c);
        }
    }
}
