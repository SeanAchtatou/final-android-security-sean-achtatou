package com.agilebinary.a.a.a.f;

import com.agilebinary.a.a.a.f;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.z;
import java.io.IOException;

public final class b {
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00cf A[Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.f r7, com.agilebinary.a.a.a.z r8, com.agilebinary.a.a.a.f.k r9) {
        /*
            r3 = 1
            r4 = 0
            r2 = 0
            if (r7 != 0) goto L_0x000d
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)
            throw r1
        L_0x000d:
            if (r8 != 0) goto L_0x0017
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "Client connection may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0017:
            if (r9 != 0) goto L_0x0021
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)
            throw r1
        L_0x0021:
            if (r7 != 0) goto L_0x0030
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r2 = "HTTP request may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            throw r1     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x002b:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x0030:
            if (r8 != 0) goto L_0x003f
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r2 = "HTTP connection may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            throw r1     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x003a:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x003f:
            if (r9 != 0) goto L_0x004e
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r2 = "HTTP context may not be null"
            r1.<init>(r2)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            throw r1     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x0049:
            r1 = move-exception
            a(r8)
            throw r1
        L_0x004e:
            java.lang.String r1 = "http.request_sent"
            java.lang.String r5 = "http.connection"
            r9.a(r5, r8)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.Boolean r5 = java.lang.Boolean.FALSE     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r9.a(r1, r5)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r8.a(r7)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            boolean r1 = r7 instanceof com.agilebinary.a.a.a.p     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r1 == 0) goto L_0x00eb
            com.agilebinary.a.a.a.d r1 = r7.a()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            com.agilebinary.a.a.a.a r5 = r1.b()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r1 = r0
            boolean r1 = r1.g_()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r1 == 0) goto L_0x00e9
            com.agilebinary.a.a.a.aa r1 = com.agilebinary.a.a.a.aa.c     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            boolean r1 = r5.a(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r1 != 0) goto L_0x00e9
            r8.c()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            com.agilebinary.a.a.a.e.e r1 = r7.g()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r5 = "http.protocol.wait-for-continue"
            r6 = 2000(0x7d0, float:2.803E-42)
            int r1 = r1.a(r5, r6)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            boolean r1 = r8.a(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r1 == 0) goto L_0x00e9
            com.agilebinary.a.a.a.j r1 = r8.d()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            boolean r5 = a(r7, r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r5 == 0) goto L_0x009d
            r8.a(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x009d:
            com.agilebinary.a.a.a.h r5 = r1.a()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            int r5 = r5.b()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r6 = 200(0xc8, float:2.8E-43)
            if (r5 >= r6) goto L_0x00cb
            r4 = 100
            if (r5 == r4) goto L_0x00ee
            java.net.ProtocolException r2 = new java.net.ProtocolException     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r3.<init>()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r4 = 0
            java.lang.String r5 = "Unexpected response: "
            java.lang.StringBuilder r3 = r3.insert(r4, r5)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            com.agilebinary.a.a.a.h r1 = r1.a()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            throw r2     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x00cb:
            r2 = r1
            r1 = r4
        L_0x00cd:
            if (r1 == 0) goto L_0x00d6
            r0 = r7
            com.agilebinary.a.a.a.p r0 = (com.agilebinary.a.a.a.p) r0     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r1 = r0
            r8.a(r1)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x00d6:
            r1 = r2
            r2 = r8
        L_0x00d8:
            r2.c()     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            java.lang.String r2 = "http.request_sent"
            java.lang.Boolean r3 = java.lang.Boolean.TRUE     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            r9.a(r2, r3)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
            if (r1 != 0) goto L_0x00e8
            com.agilebinary.a.a.a.j r1 = b(r7, r8, r9)     // Catch:{ IOException -> 0x002b, k -> 0x003a, RuntimeException -> 0x0049 }
        L_0x00e8:
            return r1
        L_0x00e9:
            r1 = r3
            goto L_0x00cd
        L_0x00eb:
            r1 = r2
            r2 = r8
            goto L_0x00d8
        L_0x00ee:
            r1 = r3
            goto L_0x00cd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.f.b.a(com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.z, com.agilebinary.a.a.a.f.k):com.agilebinary.a.a.a.j");
    }

    public static void a(f fVar, c cVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            kVar.a("http.request", fVar);
            cVar.a(fVar, kVar);
        }
    }

    public static void a(j jVar, c cVar, k kVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            kVar.a("http.response", jVar);
            cVar.a(jVar, kVar);
        }
    }

    private static final /* synthetic */ void a(z zVar) {
        try {
            zVar.k();
        } catch (IOException e) {
        }
    }

    private static /* synthetic */ boolean a(f fVar, j jVar) {
        int b;
        return ("HEAD".equalsIgnoreCase(fVar.a().a()) || (b = jVar.a().b()) < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    private static /* synthetic */ j b(f fVar, z zVar, k kVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (zVar == null) {
            throw new IllegalArgumentException("HTTP connection may not be null");
        } else if (kVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            j jVar = null;
            int i = 0;
            j jVar2 = null;
            while (true) {
                if (jVar != null && i >= 200) {
                    return jVar2;
                }
                jVar = zVar.d();
                if (a(fVar, jVar)) {
                    zVar.a(jVar);
                }
                i = jVar.a().b();
                jVar2 = jVar;
            }
        }
    }
}
