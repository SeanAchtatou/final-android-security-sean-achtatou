package com.agilebinary.a.a.a.e;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class a extends d implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f90a = new HashMap();

    public e a(String str, Object obj) {
        this.f90a.put(str, obj);
        return this;
    }

    public Object a(String str) {
        return this.f90a.get(str);
    }

    public Object clone() {
        a aVar = (a) super.clone();
        Iterator it = this.f90a.entrySet().iterator();
        while (true) {
            while (true) {
                if (!it.hasNext()) {
                    return aVar;
                }
                Map.Entry entry = (Map.Entry) it.next();
                if (entry.getKey() instanceof String) {
                    aVar.a((String) entry.getKey(), entry.getValue());
                }
            }
        }
    }
}
