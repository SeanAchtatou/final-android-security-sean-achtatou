package com.agilebinary.a.a.a.c;

import com.agilebinary.a.a.a.ae;
import com.agilebinary.a.a.a.c.c.c;
import com.agilebinary.a.a.a.c.c.h;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.j.a;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class f extends a implements ae {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f83a;
    private volatile Socket b = null;

    /* access modifiers changed from: protected */
    public a a(Socket socket, int i, e eVar) {
        return new c(socket, i, eVar);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!this.f83a) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Socket socket, e eVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = socket;
            if (eVar == null) {
                throw new IllegalArgumentException("HTTP parameters may not be null");
            }
            int a2 = eVar.a("http.socket.buffer-size", -1);
            a(a(socket, a2, eVar), b(socket, a2, eVar), eVar);
            this.f83a = true;
        }
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.a.j.e b(Socket socket, int i, e eVar) {
        return new h(socket, i, eVar);
    }

    public final void b(int i) {
        a();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (this.f83a) {
            throw new IllegalStateException("Connection is already open");
        }
    }

    /* access modifiers changed from: protected */
    public Socket i_() {
        return this.b;
    }

    public void k() {
        Socket socket;
        if (this.f83a) {
            this.f83a = false;
            Socket socket2 = this.b;
            try {
                b();
                try {
                    socket2.shutdownOutput();
                    socket = socket2;
                } catch (IOException e) {
                    socket = socket2;
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket2.close();
            }
        }
    }

    public final boolean l() {
        return this.f83a;
    }

    public void m() {
        this.f83a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    public final InetAddress p() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    public final int q() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }
}
