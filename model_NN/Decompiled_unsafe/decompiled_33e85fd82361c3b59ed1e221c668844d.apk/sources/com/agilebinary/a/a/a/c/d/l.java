package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a;
import com.agilebinary.a.a.a.b.q;
import com.agilebinary.a.a.a.d;
import com.agilebinary.a.a.a.d.c.b;
import com.agilebinary.a.a.a.f;
import java.net.URI;
import java.net.URISyntaxException;

public class l extends com.agilebinary.a.a.a.b.l implements b {
    private final f c;
    private URI d;
    private String e;
    private a f;
    private int g;

    public l(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        this.c = fVar;
        a(fVar.g());
        a(fVar.e());
        if (fVar instanceof b) {
            this.d = ((b) fVar).e_();
            this.e = ((b) fVar).b();
            this.f = null;
        } else {
            d a2 = fVar.a();
            try {
                this.d = new URI(a2.c());
                this.e = a2.a();
                this.f = fVar.c();
            } catch (URISyntaxException e2) {
                throw new com.agilebinary.a.a.a.l(new StringBuilder().insert(0, "Invalid request URI: ").append(a2.c()).toString(), e2);
            }
        }
        this.g = 0;
    }

    public final d a() {
        String str = this.e;
        a c2 = c();
        String str2 = null;
        if (this.d != null) {
            str2 = this.d.toASCIIString();
        }
        if (str2 == null || str2.length() == 0) {
            str2 = "/";
        }
        return new q(str, str2, c2);
    }

    public final void a(URI uri) {
        this.d = uri;
    }

    public final String b() {
        return this.e;
    }

    public final a c() {
        if (this.f == null) {
            this.f = com.agilebinary.a.a.a.e.b.b(g());
        }
        return this.f;
    }

    public final void d() {
        throw new UnsupportedOperationException();
    }

    public final URI e_() {
        return this.d;
    }

    public boolean i() {
        return true;
    }

    public final void j() {
        this.f15a.a();
        a(this.c.e());
    }

    public final f k() {
        return this.c;
    }

    public final int l() {
        return this.g;
    }

    public final void m() {
        this.g++;
    }
}
