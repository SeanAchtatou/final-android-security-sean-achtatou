package com.agilebinary.a.a.a.k;

import com.agilebinary.a.a.a.e.e;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f118a = new ConcurrentHashMap();

    public final j a(String str, e eVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        g gVar = (g) this.f118a.get(str.toLowerCase(Locale.ENGLISH));
        if (gVar != null) {
            return gVar.a(eVar);
        }
        throw new IllegalStateException(new StringBuilder().insert(0, "Unsupported cookie spec: ").append(str).toString());
    }

    public final void a(String str, g gVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (gVar == null) {
            throw new IllegalArgumentException("Cookie spec factory may not be null");
        } else {
            this.f118a.put(str.toLowerCase(Locale.ENGLISH), gVar);
        }
    }
}
