package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.d.d;
import com.agilebinary.a.a.a.k.a;
import com.agilebinary.a.a.a.k.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

public final class o implements d, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final TreeSet f74a = new TreeSet(new a());

    public final List a() {
        ArrayList arrayList;
        synchronized (this) {
            arrayList = new ArrayList(this.f74a);
        }
        return arrayList;
    }

    public final void a(c cVar) {
        synchronized (this) {
            if (cVar != null) {
                this.f74a.remove(cVar);
                if (!cVar.b(new Date())) {
                    this.f74a.add(cVar);
                }
            }
        }
    }

    public final String toString() {
        String treeSet;
        synchronized (this) {
            treeSet = this.f74a.toString();
        }
        return treeSet;
    }
}
