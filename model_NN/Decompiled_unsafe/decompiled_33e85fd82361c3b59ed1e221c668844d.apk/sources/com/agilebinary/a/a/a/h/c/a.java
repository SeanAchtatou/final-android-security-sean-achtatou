package com.agilebinary.a.a.a.h.c;

public final class a implements d {
    private static /* synthetic */ int b(b bVar, b bVar2) {
        int c;
        int c2;
        if (bVar2.c() <= 1 || !bVar.a().equals(bVar2.a()) || (c = bVar.c()) < (c2 = bVar2.c())) {
            return -1;
        }
        int i = 0;
        int i2 = 0;
        while (i < c2 - 1) {
            if (!bVar.a(i2).equals(bVar2.a(i2))) {
                return -1;
            }
            i = i2 + 1;
            i2 = i;
        }
        if (c > c2) {
            return 4;
        }
        if (bVar2.d() && !bVar.d()) {
            return -1;
        }
        if (bVar2.e() && !bVar.e()) {
            return -1;
        }
        if (bVar.d() && !bVar2.d()) {
            return 3;
        }
        if (!bVar.e() || bVar2.e()) {
            return bVar.f() == bVar2.f() ? 0 : -1;
        }
        return 5;
    }

    public final int a(b bVar, b bVar2) {
        if (bVar == null) {
            throw new IllegalArgumentException("Planned route may not be null.");
        } else if (bVar2 == null || bVar2.c() <= 0) {
            return bVar.c() > 1 ? 2 : 1;
        } else {
            if (bVar.c() > 1) {
                return b(bVar, bVar2);
            }
            if (bVar2.c() > 1 || !bVar.a().equals(bVar2.a()) || bVar.f() != bVar2.f()) {
                return -1;
            }
            return (bVar.b() == null || bVar.b().equals(bVar2.b())) ? 0 : -1;
        }
    }
}
