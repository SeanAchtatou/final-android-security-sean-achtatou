package com.agilebinary.a.a.a.c.a;

import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.k.g;
import com.agilebinary.a.a.a.k.j;
import java.util.Collection;

public final class v implements g {
    public final j a(e eVar) {
        if (eVar == null) {
            return new w();
        }
        Collection collection = (Collection) eVar.a("http.protocol.cookie-datepatterns");
        return new w(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null);
    }
}
