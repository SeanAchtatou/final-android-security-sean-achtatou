package com.agilebinary.a.a.a.c.b;

import com.agilebinary.a.a.a.h.b;
import com.agilebinary.a.a.a.h.b.h;
import com.agilebinary.a.a.a.h.c.c;
import com.agilebinary.a.a.a.h.c.f;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.m;
import com.agilebinary.a.a.b.a.a;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public final class i implements k {

    /* renamed from: a  reason: collision with root package name */
    protected final m f48a;
    private final Log b = a.a(getClass());
    private h c;
    private boolean d;
    private f e;
    private c f;
    private long g;
    private long h;
    private volatile boolean i;

    public i(h hVar) {
        if (hVar == null) {
            throw new IllegalArgumentException("Scheme registry must not be null.");
        }
        this.c = hVar;
        this.f48a = new b(hVar);
        this.e = new f(this);
        this.f = null;
        this.g = -1;
        this.d = false;
        this.i = false;
    }

    private /* synthetic */ void b() {
        if (this.i) {
            throw new IllegalStateException("Manager is shut down.");
        }
    }

    private /* synthetic */ void c() {
        synchronized (this) {
            if (System.currentTimeMillis() >= this.h) {
                a(0, TimeUnit.MILLISECONDS);
            }
        }
    }

    private /* synthetic */ void d() {
        synchronized (this) {
            this.i = true;
            if (this.f != null) {
                this.f.j();
            }
            try {
                if (this.e != null) {
                    this.e.d();
                }
                this.e = null;
            } catch (IOException e2) {
                this.b.debug("Problem while shutting down manager.", e2);
                this.e = null;
            } catch (Throwable th) {
                this.e = null;
                throw th;
            }
        }
        return;
    }

    public final com.agilebinary.a.a.a.h.a a(c cVar) {
        boolean z;
        c cVar2;
        boolean z2 = true;
        boolean z3 = false;
        synchronized (this) {
            if (cVar == null) {
                throw new IllegalArgumentException("Route may not be null.");
            }
            b();
            if (this.b.isDebugEnabled()) {
                this.b.debug(new StringBuilder().insert(0, "Get connection for route ").append(cVar).toString());
            }
            if (this.f != null) {
                throw new IllegalStateException("Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
            }
            c();
            if (this.e.f35a.l()) {
                f fVar = this.e.c;
                boolean z4 = fVar == null || !fVar.h().equals(cVar);
                z = false;
                z3 = z4;
            } else {
                z = true;
            }
            if (z3) {
                try {
                    this.e.d();
                } catch (IOException e2) {
                    this.b.debug("Problem shutting down connection.", e2);
                }
            } else {
                z2 = z;
            }
            if (z2) {
                this.e = new f(this);
            }
            this.f = new c(this, this.e, cVar);
            cVar2 = this.f;
        }
        return cVar2;
    }

    public final h a() {
        return this.c;
    }

    public final b a(c cVar, Object obj) {
        return new d(this, cVar, obj);
    }

    public final void a(long j, TimeUnit timeUnit) {
        synchronized (this) {
            b();
            if (timeUnit == null) {
                throw new IllegalArgumentException("Time unit must not be null.");
            } else if (this.f == null && this.e.f35a.l()) {
                if (this.g <= System.currentTimeMillis() - timeUnit.toMillis(j)) {
                    try {
                        this.e.c();
                    } catch (IOException e2) {
                        this.b.debug("Problem closing idle connection.", e2);
                    }
                }
            }
        }
        return;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:35:0x0070=Splitter:B:35:0x0070, B:45:0x00a2=Splitter:B:45:0x00a2} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.a.a.a.h.a r7, long r8, java.util.concurrent.TimeUnit r10) {
        /*
            r6 = this;
            r4 = 0
            monitor-enter(r6)
            r6.b()     // Catch:{ all -> 0x0012 }
            boolean r0 = r7 instanceof com.agilebinary.a.a.a.c.b.c     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x0012:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        L_0x0015:
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ all -> 0x0012 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x0036
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0012 }
            r1.<init>()     // Catch:{ all -> 0x0012 }
            r2 = 0
            java.lang.String r3 = "Releasing connection "
            java.lang.StringBuilder r1 = r1.insert(r2, r3)     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0012 }
            r0.debug(r1)     // Catch:{ all -> 0x0012 }
        L_0x0036:
            com.agilebinary.a.a.a.c.b.c r7 = (com.agilebinary.a.a.a.c.b.c) r7     // Catch:{ all -> 0x0012 }
            com.agilebinary.a.a.a.c.b.a r0 = r7.f52a     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x003e
        L_0x003c:
            monitor-exit(r6)
            return
        L_0x003e:
            com.agilebinary.a.a.a.h.k r0 = r7.h()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x004e
            if (r0 == r6) goto L_0x004e
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x004e:
            boolean r0 = r7.l()     // Catch:{ IOException -> 0x0092 }
            if (r0 == 0) goto L_0x0070
            boolean r0 = r6.d     // Catch:{ IOException -> 0x0092 }
            if (r0 != 0) goto L_0x005e
            boolean r0 = r7.r()     // Catch:{ IOException -> 0x0092 }
            if (r0 != 0) goto L_0x0070
        L_0x005e:
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ IOException -> 0x0092 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ IOException -> 0x0092 }
            if (r0 == 0) goto L_0x006d
            org.apache.commons.logging.Log r0 = r6.b     // Catch:{ IOException -> 0x0092 }
            java.lang.String r1 = "Released connection open but not reusable."
            r0.debug(r1)     // Catch:{ IOException -> 0x0092 }
        L_0x006d:
            r7.m()     // Catch:{ IOException -> 0x0092 }
        L_0x0070:
            r7.j()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r6.g = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x008a
            long r0 = r10.toMillis(r8)     // Catch:{ all -> 0x0012 }
            long r2 = r6.g     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r6.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003c
        L_0x008a:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003c
        L_0x0092:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r6.b     // Catch:{ all -> 0x00c5 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00c5 }
            if (r1 == 0) goto L_0x00a2
            org.apache.commons.logging.Log r1 = r6.b     // Catch:{ all -> 0x00c5 }
            java.lang.String r2 = "Exception shutting down released connection."
            r1.debug(r2, r0)     // Catch:{ all -> 0x00c5 }
        L_0x00a2:
            r7.j()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r6.f = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r6.g = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00bc
            long r0 = r10.toMillis(r8)     // Catch:{ all -> 0x0012 }
            long r2 = r6.g     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r6.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003c
        L_0x00bc:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003c
        L_0x00c5:
            r0 = move-exception
            r7.j()     // Catch:{ all -> 0x0012 }
            r1 = 0
            r6.f = r1     // Catch:{ all -> 0x0012 }
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r6.g = r2     // Catch:{ all -> 0x0012 }
            int r1 = (r8 > r4 ? 1 : (r8 == r4 ? 0 : -1))
            if (r1 <= 0) goto L_0x00e0
            long r2 = r10.toMillis(r8)     // Catch:{ all -> 0x0012 }
            long r4 = r6.g     // Catch:{ all -> 0x0012 }
            long r2 = r2 + r4
            r6.h = r2     // Catch:{ all -> 0x0012 }
        L_0x00df:
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x00e0:
            r2 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r6.h = r2     // Catch:{ all -> 0x0012 }
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.b.i.a(com.agilebinary.a.a.a.h.a, long, java.util.concurrent.TimeUnit):void");
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            d();
        } finally {
            super.finalize();
        }
    }
}
