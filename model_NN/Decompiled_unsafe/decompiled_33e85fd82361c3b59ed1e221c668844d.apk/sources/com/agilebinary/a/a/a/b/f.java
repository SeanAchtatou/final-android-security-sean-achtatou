package com.agilebinary.a.a.a.b;

import com.agilebinary.a.a.a.f.d;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.m;
import com.agilebinary.a.a.a.o;
import java.util.ArrayList;

public final class f implements j {

    /* renamed from: a  reason: collision with root package name */
    public static final f f11a = new f();
    private static final char[] b = {';', ','};

    public static o a(c cVar, i iVar, char[] cArr) {
        int i;
        boolean z;
        boolean z2;
        String str;
        boolean z3;
        int i2;
        int i3;
        boolean z4;
        int i4;
        boolean z5;
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int b3 = iVar.b();
            int a2 = iVar.a();
            int i5 = b2;
            while (true) {
                if (b2 < a2) {
                    char a3 = cVar.a(i5);
                    if (a3 == '=') {
                        break;
                    } else if (a(a3, cArr)) {
                        i = i5;
                        z = true;
                        break;
                    } else {
                        b2 = i5 + 1;
                        i5 = b2;
                    }
                } else {
                    break;
                }
            }
            i = i5;
            z = false;
            if (i == a2) {
                String b4 = cVar.b(b3, a2);
                z2 = true;
                str = b4;
                z = true;
            } else {
                String b5 = cVar.b(b3, i5);
                i5++;
                z2 = z;
                str = b5;
            }
            if (z) {
                iVar.a(i5);
                return a(str, (String) null);
            }
            int i6 = i5;
            boolean z6 = false;
            boolean z7 = false;
            int i7 = i5;
            while (true) {
                if (i6 >= a2) {
                    i3 = i5;
                    i2 = i5;
                    z3 = z2;
                    break;
                }
                char a4 = cVar.a(i7);
                if (a4 == '\"' && !z7) {
                    if (z6) {
                        z6 = false;
                        z4 = false;
                        if (z6 && !z7 && a(a4, cArr)) {
                            i3 = i5;
                            i2 = i5;
                            z3 = true;
                            break;
                        }
                        if (!z7 || !z4 || a4 != '\\') {
                            i4 = i7;
                            z5 = false;
                        } else {
                            i4 = i7;
                            z5 = true;
                        }
                        int i8 = i4 + 1;
                        z7 = z5;
                        i7 = i8;
                        int i9 = i8;
                        z6 = z4;
                        i6 = i9;
                    } else {
                        z6 = true;
                    }
                }
                z4 = z6;
                if (z6) {
                }
                if (!z7) {
                }
                i4 = i7;
                z5 = false;
                int i82 = i4 + 1;
                z7 = z5;
                i7 = i82;
                int i92 = i82;
                z6 = z4;
                i6 = i92;
            }
            while (i3 < i7 && d.a(cVar.a(i2))) {
                i3 = i2 + 1;
                i2 = i3;
            }
            int i10 = i7;
            int i11 = i7;
            while (i10 > i2 && d.a(cVar.a(i11 - 1))) {
                i11--;
                i10 = i11;
            }
            if (i11 - i2 >= 2 && cVar.a(i2) == '\"' && cVar.a(i11 - 1) == '\"') {
                i2++;
                i11--;
            }
            String a5 = cVar.a(i2, i11);
            if (z3) {
                i7++;
            }
            iVar.a(i7);
            return a(str, a5);
        }
    }

    private static /* synthetic */ o a(String str, String str2) {
        return new o(str, str2);
    }

    private static /* synthetic */ boolean a(char c, char[] cArr) {
        if (cArr == null) {
            return false;
        }
        int i = 0;
        int i2 = 0;
        while (i < cArr.length) {
            if (c == cArr[i2]) {
                return true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return false;
    }

    public static final m[] a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Value to parse may not be null");
        }
        f fVar = f11a;
        c cVar = new c(str.length());
        cVar.a(str);
        return fVar.a(cVar, new i(0, str.length()));
    }

    private /* synthetic */ o[] c(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = iVar.b();
            int a2 = iVar.a();
            int i = b2;
            while (b2 < a2 && d.a(cVar.a(i))) {
                b2 = i + 1;
                i = b2;
            }
            iVar.a(i);
            if (iVar.c()) {
                return new o[0];
            }
            ArrayList arrayList = new ArrayList();
            while (!iVar.c()) {
                arrayList.add(a(cVar, iVar, b));
                if (cVar.a(iVar.b() - 1) == ',') {
                    break;
                }
            }
            return (o[]) arrayList.toArray(new o[arrayList.size()]);
        }
    }

    public final m[] a(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            ArrayList arrayList = new ArrayList();
            while (true) {
                while (true) {
                    if (iVar.c()) {
                        return (m[]) arrayList.toArray(new m[arrayList.size()]);
                    }
                    m b2 = b(cVar, iVar);
                    if (b2.a().length() != 0 || b2.b() != null) {
                        arrayList.add(b2);
                    }
                }
            }
        }
    }

    public final m b(c cVar, i iVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            o d = a(cVar, iVar, b);
            o[] oVarArr = null;
            if (!iVar.c() && cVar.a(iVar.b() - 1) != ',') {
                oVarArr = c(cVar, iVar);
            }
            return new a(d.a(), d.b(), oVarArr);
        }
    }
}
