package com.agilebinary.a.a.a.c.c;

import com.agilebinary.a.a.a.b.s;
import com.agilebinary.a.a.a.b.t;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.i.c;
import com.agilebinary.a.a.a.j.a;
import com.agilebinary.a.a.a.j.d;
import com.agilebinary.a.a.a.l;
import com.agilebinary.a.a.a.u;
import com.agilebinary.a.a.a.x;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class f implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final t f61a;
    private final a b;
    private final int c;
    private final int d;
    private final List e;
    private int f;
    private x g;

    public f(a aVar, e eVar) {
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = aVar;
            this.c = eVar.a("http.connection.max-header-count", -1);
            this.d = eVar.a("http.connection.max-line-length", -1);
            this.f61a = s.f21a;
            this.e = new ArrayList();
            this.f = 0;
        }
    }

    public static com.agilebinary.a.a.a.t[] a(a aVar, int i, int i2, t tVar, List list) {
        c cVar;
        a aVar2;
        int i3 = 0;
        if (aVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Line parser may not be null");
        } else if (list == null) {
            throw new IllegalArgumentException("Header line list may not be null");
        } else {
            c cVar2 = null;
            c cVar3 = null;
            while (true) {
                if (cVar3 == null) {
                    cVar = new c(64);
                    aVar2 = aVar;
                } else {
                    cVar3.a();
                    cVar = cVar3;
                    aVar2 = aVar;
                }
                if (aVar2.a(cVar) == -1 || cVar.c() <= 0) {
                    com.agilebinary.a.a.a.t[] tVarArr = new com.agilebinary.a.a.a.t[list.size()];
                } else {
                    if ((cVar.a(0) == ' ' || cVar.a(0) == 9) && cVar2 != null) {
                        int i4 = 0;
                        int i5 = 0;
                        while (i4 < cVar.c() && ((r0 = cVar.a(i5)) == ' ' || r0 == 9)) {
                            i4 = i5 + 1;
                            i5 = i4;
                        }
                        if (i2 <= 0 || ((cVar2.c() + 1) + cVar.c()) - i5 <= i2) {
                            cVar2.a(' ');
                            cVar2.a(cVar, i5, cVar.c() - i5);
                            cVar3 = cVar;
                            cVar = cVar2;
                        } else {
                            throw new IOException("Maximum line length limit exceeded");
                        }
                    } else {
                        list.add(cVar);
                        cVar2 = cVar;
                        cVar3 = null;
                    }
                    if (i <= 0) {
                        cVar2 = cVar;
                    } else if (list.size() >= i) {
                        throw new IOException("Maximum header count exceeded");
                    }
                }
            }
            com.agilebinary.a.a.a.t[] tVarArr2 = new com.agilebinary.a.a.a.t[list.size()];
            while (true) {
                int i6 = i3;
                if (i3 >= list.size()) {
                    return tVarArr2;
                }
                try {
                    tVarArr2[i6] = tVar.a((c) list.get(i6));
                    i3 = i6 + 1;
                } catch (u e2) {
                    throw new l(e2.getMessage());
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final x a() {
        switch (this.f) {
            case 0:
                try {
                    this.g = a(this.b);
                    this.f = 1;
                    break;
                } catch (u e2) {
                    throw new l(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException("Inconsistent parser state");
        }
        this.g.a(a(this.b, this.c, this.d, this.f61a, this.e));
        x xVar = this.g;
        this.g = null;
        this.e.clear();
        this.f = 0;
        return xVar;
    }

    /* access modifiers changed from: protected */
    public abstract x a(a aVar);
}
