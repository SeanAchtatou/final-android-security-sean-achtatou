package com.agilebinary.a.a.a.a;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private h f5a;
    private c b;
    private g c;

    public final void a() {
        this.f5a = null;
        this.b = null;
        this.c = null;
    }

    public final void a(c cVar) {
        this.b = cVar;
    }

    public final void a(g gVar) {
        this.c = gVar;
    }

    public final void a(h hVar) {
        if (hVar == null) {
            a();
        } else {
            this.f5a = hVar;
        }
    }

    public final boolean b() {
        return this.f5a != null;
    }

    public final h c() {
        return this.f5a;
    }

    public final g d() {
        return this.c;
    }

    public final c e() {
        return this.b;
    }

    public final String toString() {
        String str;
        StringBuilder sb;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("auth scope [");
        sb2.append(this.b);
        sb2.append("]; credentials set [");
        if (this.c != null) {
            str = "true";
            sb = sb2;
        } else {
            str = "false";
            sb = sb2;
        }
        sb.append(str);
        sb2.append("]");
        return sb2.toString();
    }
}
