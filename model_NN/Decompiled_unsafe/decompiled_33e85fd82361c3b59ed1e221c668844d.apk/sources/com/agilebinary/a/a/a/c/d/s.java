package com.agilebinary.a.a.a.c.d;

import com.agilebinary.a.a.a.a.g;
import com.agilebinary.a.a.a.d.a;
import com.agilebinary.a.a.a.d.f;
import com.agilebinary.a.a.a.d.l;
import com.agilebinary.a.a.a.e.e;
import com.agilebinary.a.a.a.f.b;
import com.agilebinary.a.a.a.f.c;
import com.agilebinary.a.a.a.h.c.h;
import com.agilebinary.a.a.a.h.k;
import com.agilebinary.a.a.a.h.n;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.p;
import com.agilebinary.a.a.a.t;
import java.io.IOException;
import java.net.URI;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public final class s implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Log f78a;
    private k b;
    private h c;
    private com.agilebinary.a.a.a.s d;
    private n e;
    private b f;
    private c g;
    private com.agilebinary.a.a.a.d.k h;
    private com.agilebinary.a.a.a.d.h i = null;
    private com.agilebinary.a.a.a.d.c j;
    private com.agilebinary.a.a.a.d.b k;
    private com.agilebinary.a.a.a.d.b l;
    private f m;
    private e n;
    private com.agilebinary.a.a.a.h.a o;
    private com.agilebinary.a.a.a.a.e p;
    private com.agilebinary.a.a.a.a.e q;
    private int r;
    private int s;
    private int t;
    private com.agilebinary.a.a.a.b u;

    public s(Log log, b bVar, k kVar, com.agilebinary.a.a.a.s sVar, n nVar, h hVar, c cVar, com.agilebinary.a.a.a.d.k kVar2, com.agilebinary.a.a.a.d.c cVar2, com.agilebinary.a.a.a.d.b bVar2, com.agilebinary.a.a.a.d.b bVar3, f fVar, e eVar) {
        if (log == null) {
            throw new IllegalArgumentException("Log may not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Request executor may not be null.");
        } else if (kVar == null) {
            throw new IllegalArgumentException("Client connection manager may not be null.");
        } else if (sVar == null) {
            throw new IllegalArgumentException("Connection reuse strategy may not be null.");
        } else if (nVar == null) {
            throw new IllegalArgumentException("Connection keep alive strategy may not be null.");
        } else if (hVar == null) {
            throw new IllegalArgumentException("Route planner may not be null.");
        } else if (cVar == null) {
            throw new IllegalArgumentException("HTTP protocol processor may not be null.");
        } else if (kVar2 == null) {
            throw new IllegalArgumentException("HTTP request retry handler may not be null.");
        } else if (cVar2 == null) {
            throw new IllegalArgumentException("Redirect strategy may not be null.");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Target authentication handler may not be null.");
        } else if (bVar3 == null) {
            throw new IllegalArgumentException("Proxy authentication handler may not be null.");
        } else if (fVar == null) {
            throw new IllegalArgumentException("User token handler may not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.f78a = log;
            this.f = bVar;
            this.b = kVar;
            this.d = sVar;
            this.e = nVar;
            this.c = hVar;
            this.g = cVar;
            this.h = kVar2;
            this.j = cVar2;
            this.k = bVar2;
            this.l = bVar3;
            this.m = fVar;
            this.n = eVar;
            this.o = null;
            this.r = 0;
            this.s = 0;
            this.t = this.n.a("http.protocol.max-redirects", 100);
            this.p = new com.agilebinary.a.a.a.a.e();
            this.q = new com.agilebinary.a.a.a.a.e();
        }
    }

    private static /* synthetic */ l a(com.agilebinary.a.a.a.f fVar) {
        return fVar instanceof p ? new k((p) fVar) : new l(fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    private /* synthetic */ m a(m mVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        l a2 = mVar.a();
        e g2 = a2.g();
        if (g2 == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else if (!g2.a("http.protocol.handle-redirects", true) || !this.j.a(a2, jVar)) {
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(g2)) {
                if (this.k.a(jVar)) {
                    com.agilebinary.a.a.a.b bVar = (com.agilebinary.a.a.a.b) kVar.a("http.target_host");
                    com.agilebinary.a.a.a.b a3 = bVar == null ? b2.a() : bVar;
                    this.f78a.debug("Target requested authentication");
                    try {
                        a(this.k.b(jVar), this.p, this.k, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(new StringBuilder().insert(0, "Authentication error: ").append(e2.getMessage()).toString());
                            return null;
                        }
                    }
                    a(this.p, a3, eVar);
                    if (this.p.d() == null) {
                        return null;
                    }
                    return mVar;
                }
                this.p.a((com.agilebinary.a.a.a.a.c) null);
                if (this.l.a(jVar)) {
                    com.agilebinary.a.a.a.b g3 = b2.g();
                    this.f78a.debug("Proxy requested authentication");
                    try {
                        a(this.l.b(jVar), this.q, this.l, jVar, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e3) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(new StringBuilder().insert(0, "Authentication error: ").append(e3.getMessage()).toString());
                            return null;
                        }
                    }
                    a(this.q, g3, eVar);
                    if (this.q.d() == null) {
                        return null;
                    }
                    return mVar;
                }
                this.q.a((com.agilebinary.a.a.a.a.c) null);
            }
            return null;
        } else if (this.s >= this.t) {
            throw new l(new StringBuilder().insert(0, "Maximum redirects (").append(this.t).append(") exceeded").toString());
        } else {
            this.s++;
            this.u = null;
            com.agilebinary.a.a.a.d.c.b a4 = this.j.a(a2, jVar, kVar);
            a4.a(a2.k().e());
            URI e_ = a4.e_();
            if (e_.getHost() == null) {
                throw new com.agilebinary.a.a.a.l(new StringBuilder().insert(0, "Redirect URI does not specify a valid host name: ").append(e_).toString());
            }
            com.agilebinary.a.a.a.b bVar2 = new com.agilebinary.a.a.a.b(e_.getHost(), e_.getPort(), e_.getScheme());
            this.p.a((com.agilebinary.a.a.a.a.c) null);
            this.q.a((com.agilebinary.a.a.a.a.c) null);
            if (!b2.a().equals(bVar2)) {
                this.p.a();
                com.agilebinary.a.a.a.a.h c2 = this.q.c();
                if (c2 != null && c2.d()) {
                    this.q.a();
                }
            }
            l a5 = a(a4);
            a5.a(g2);
            com.agilebinary.a.a.a.h.c.c a6 = a(bVar2, a5);
            m mVar2 = new m(a5, a6);
            if (!this.f78a.isDebugEnabled()) {
                return mVar2;
            }
            this.f78a.debug(new StringBuilder().insert(0, "Redirecting to '").append(e_).append("' via ").append(a6).toString());
            return mVar2;
        }
    }

    private /* synthetic */ com.agilebinary.a.a.a.h.c.c a(com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.f fVar) {
        com.agilebinary.a.a.a.b bVar2 = bVar == null ? (com.agilebinary.a.a.a.b) fVar.g().a("http.default-host") : bVar;
        if (bVar2 != null) {
            return this.c.a(bVar2, fVar);
        }
        throw new IllegalStateException("Target host must not be null, or set in parameters.");
    }

    private /* synthetic */ void a() {
        try {
            this.o.d_();
        } catch (IOException e2) {
            this.f78a.debug("IOException releasing connection", e2);
        }
        this.o = null;
    }

    private /* synthetic */ void a(com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.b bVar, com.agilebinary.a.a.a.d.e eVar2) {
        g gVar;
        com.agilebinary.a.a.a.a.e eVar3;
        if (eVar.b()) {
            String a2 = bVar.a();
            int b2 = bVar.b();
            if (b2 < 0) {
                b2 = this.b.a().a(bVar).a();
            }
            com.agilebinary.a.a.a.a.h c2 = eVar.c();
            com.agilebinary.a.a.a.a.c cVar = new com.agilebinary.a.a.a.a.c(a2, b2, c2.c(), c2.b());
            if (this.f78a.isDebugEnabled()) {
                this.f78a.debug(new StringBuilder().insert(0, "Authentication scope: ").append(cVar).toString());
            }
            g d2 = eVar.d();
            if (d2 == null) {
                d2 = eVar2.a(cVar);
                if (this.f78a.isDebugEnabled()) {
                    if (d2 != null) {
                        this.f78a.debug("Found credentials");
                    } else {
                        this.f78a.debug("Credentials not found");
                        gVar = d2;
                        eVar3 = eVar;
                        eVar3.a(cVar);
                        eVar.a(gVar);
                    }
                }
            } else if (c2.e()) {
                this.f78a.debug("Authentication failed");
                gVar = null;
                eVar3 = eVar;
                eVar3.a(cVar);
                eVar.a(gVar);
            }
            gVar = d2;
            eVar3 = eVar;
            eVar3.a(cVar);
            eVar.a(gVar);
        }
    }

    private /* synthetic */ void a(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        s sVar;
        int a2;
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        boolean z = true;
        int i2 = 0;
        boolean z2 = true;
        while (z) {
            int i3 = i2 + 1;
            try {
                if (!this.o.l()) {
                    this.o.a(b2, kVar, this.n);
                } else {
                    this.o.b(com.agilebinary.a.a.a.e.c.a(this.n));
                }
                com.agilebinary.a.a.a.h.c.a aVar = new com.agilebinary.a.a.a.h.c.a();
                do {
                    com.agilebinary.a.a.a.h.c.c c_ = this.o.c_();
                    a2 = aVar.a(b2, c_);
                    switch (a2) {
                        case -1:
                            throw new IllegalStateException(new StringBuilder().insert(0, "Unable to establish route.\nplanned = ").append(b2).append("\ncurrent = ").append(c_).toString());
                        case 0:
                            break;
                        case 1:
                        case 2:
                            this.o.a(b2, kVar, this.n);
                            continue;
                        case 3:
                            a(b2, kVar);
                            this.f78a.debug("Tunnel to target created.");
                            this.o.a(this.n);
                            continue;
                        case 4:
                            throw new UnsupportedOperationException("Proxy chains are not supported.");
                        case 5:
                            this.o.a(kVar, this.n);
                            continue;
                        default:
                            throw new IllegalStateException(new StringBuilder().insert(0, "Unknown step indicator ").append(a2).append(" from RouteDirector.").toString());
                    }
                } while (a2 > 0);
                z = false;
                i2 = i3;
                z2 = false;
            } catch (IOException e2) {
                IOException iOException = e2;
                try {
                    this.o.k();
                    sVar = this;
                } catch (IOException e3) {
                    sVar = this;
                }
                if (sVar.h.a(iOException, i3, kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info(new StringBuilder().insert(0, "I/O exception (").append(iOException.getClass().getName()).append(") caught when connecting to the target host: ").append(iOException.getMessage()).toString());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(iOException.getMessage(), iOException);
                    }
                    this.f78a.info("Retrying connect");
                    z = z2;
                    i2 = i3;
                } else {
                    throw iOException;
                }
            }
        }
    }

    private /* synthetic */ void a(Map map, com.agilebinary.a.a.a.a.e eVar, com.agilebinary.a.a.a.d.b bVar, j jVar, com.agilebinary.a.a.a.f.k kVar) {
        com.agilebinary.a.a.a.a.h c2 = eVar.c();
        if (c2 == null) {
            c2 = bVar.a(map, jVar, kVar);
            eVar.a(c2);
        }
        String b2 = c2.b();
        if (((t) map.get(b2.toLowerCase(Locale.ENGLISH))) == null) {
            throw new com.agilebinary.a.a.a.a.b(new StringBuilder().insert(0, b2).append(" authorization challenge expected, but not found").toString());
        }
        this.f78a.debug("Authorization challenge processed");
    }

    private /* synthetic */ boolean a(com.agilebinary.a.a.a.h.c.c cVar, com.agilebinary.a.a.a.f.k kVar) {
        j jVar;
        com.agilebinary.a.a.a.b g2 = cVar.g();
        com.agilebinary.a.a.a.b a2 = cVar.a();
        boolean z = false;
        j jVar2 = null;
        while (true) {
            if (z) {
                jVar = jVar2;
                break;
            }
            if (!this.o.l()) {
                this.o.a(cVar, kVar, this.n);
            }
            com.agilebinary.a.a.a.b a3 = cVar.a();
            String a4 = a3.a();
            int b2 = a3.b();
            if (b2 < 0) {
                b2 = this.b.a().a(a3.c()).a();
            }
            StringBuilder sb = new StringBuilder(a4.length() + 6);
            sb.append(a4);
            sb.append(':');
            sb.append(Integer.toString(b2));
            com.agilebinary.a.a.a.b.n nVar = new com.agilebinary.a.a.a.b.n("CONNECT", sb.toString(), com.agilebinary.a.a.a.e.b.b(this.n));
            nVar.a(this.n);
            kVar.a("http.target_host", a2);
            kVar.a("http.proxy_host", g2);
            kVar.a("http.connection", this.o);
            kVar.a("http.auth.target-scope", this.p);
            kVar.a("http.auth.proxy-scope", this.q);
            kVar.a("http.request", nVar);
            b.a(nVar, this.g, kVar);
            jVar2 = b.a(nVar, this.o, kVar);
            jVar2.a(this.n);
            b.a(jVar2, this.g, kVar);
            if (jVar2.a().b() < 200) {
                throw new com.agilebinary.a.a.a.k(new StringBuilder().insert(0, "Unexpected response to CONNECT request: ").append(jVar2.a()).toString());
            }
            com.agilebinary.a.a.a.d.e eVar = (com.agilebinary.a.a.a.d.e) kVar.a("http.auth.credentials-provider");
            if (eVar != null && com.agilebinary.a.a.a.d.d.a.a(this.n)) {
                if (this.l.a(jVar2)) {
                    this.f78a.debug("Proxy requested authentication");
                    try {
                        a(this.l.b(jVar2), this.q, this.l, jVar2, kVar);
                    } catch (com.agilebinary.a.a.a.a.b e2) {
                        if (this.f78a.isWarnEnabled()) {
                            this.f78a.warn(new StringBuilder().insert(0, "Authentication error: ").append(e2.getMessage()).toString());
                            jVar = jVar2;
                            break;
                        }
                    }
                    a(this.q, g2, eVar);
                    if (this.q.d() == null) {
                        z = true;
                    } else if (this.d.a(jVar2, kVar)) {
                        this.f78a.debug("Connection kept alive");
                        com.agilebinary.a.a.a.i.b.a(jVar2.b());
                        z = false;
                    } else {
                        this.o.k();
                        z = false;
                    }
                } else {
                    this.q.a((com.agilebinary.a.a.a.a.c) null);
                }
            }
            z = true;
        }
        if (jVar2.a().b() > 299) {
            com.agilebinary.a.a.a.c b3 = jVar.b();
            if (b3 != null) {
                jVar.a(new com.agilebinary.a.a.a.g.e(b3));
            }
            this.o.k();
            throw new g(new StringBuilder().insert(0, "CONNECT refused by proxy: ").append(jVar.a()).toString(), jVar);
        }
        this.o.g();
        return false;
    }

    private /* synthetic */ j b(m mVar, com.agilebinary.a.a.a.f.k kVar) {
        s sVar;
        IOException iOException = null;
        l a2 = mVar.a();
        com.agilebinary.a.a.a.h.c.c b2 = mVar.b();
        boolean z = true;
        boolean z2 = true;
        j jVar = null;
        while (z) {
            this.r++;
            a2.m();
            if (!a2.i()) {
                this.f78a.debug("Cannot retry non-repeatable request");
                if (iOException != null) {
                    throw new com.agilebinary.a.a.a.d.j("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", iOException);
                }
                throw new com.agilebinary.a.a.a.d.j("Cannot retry request with a non-repeatable request entity.");
            }
            try {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug(new StringBuilder().insert(0, "Attempt ").append(this.r).append(" to execute request").toString());
                }
                z2 = false;
                jVar = b.a(a2, this.o, kVar);
                z = false;
            } catch (IOException e2) {
                iOException = e2;
                this.f78a.debug("Closing the connection.");
                try {
                    this.o.k();
                    sVar = this;
                } catch (IOException e3) {
                    sVar = this;
                }
                if (sVar.h.a(iOException, a2.l(), kVar)) {
                    if (this.f78a.isInfoEnabled()) {
                        this.f78a.info(new StringBuilder().insert(0, "I/O exception (").append(iOException.getClass().getName()).append(") caught when processing request: ").append(iOException.getMessage()).toString());
                    }
                    if (this.f78a.isDebugEnabled()) {
                        this.f78a.debug(iOException.getMessage(), iOException);
                    }
                    this.f78a.info("Retrying request");
                    if (!b2.d()) {
                        this.f78a.debug("Reopening the direct connection.");
                        this.o.a(b2, kVar, this.n);
                        z = z2;
                    } else {
                        this.f78a.debug("Proxied connection. Need to start over.");
                        z = false;
                        z2 = false;
                    }
                } else {
                    throw iOException;
                }
            }
        }
        return jVar;
    }

    private /* synthetic */ void b() {
        com.agilebinary.a.a.a.h.a aVar = this.o;
        if (aVar != null) {
            this.o = null;
            try {
                aVar.b();
            } catch (IOException e2) {
                if (this.f78a.isDebugEnabled()) {
                    this.f78a.debug(e2.getMessage(), e2);
                }
            }
            try {
                aVar.d_();
            } catch (IOException e3) {
                this.f78a.debug("Error releasing connection", e3);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.a.e.e.a(java.lang.String, int):int
      com.agilebinary.a.a.a.e.e.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.a.e.e
      com.agilebinary.a.a.a.e.e.a(java.lang.String, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x02d1 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0116 A[Catch:{ URISyntaxException -> 0x025b, InterruptedException -> 0x008e, n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0160 A[Catch:{ URISyntaxException -> 0x025b, InterruptedException -> 0x008e, n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.a.a.a.j a(com.agilebinary.a.a.a.b r18, com.agilebinary.a.a.a.f r19, com.agilebinary.a.a.a.f.k r20) {
        /*
            r17 = this;
            com.agilebinary.a.a.a.c.d.l r3 = a(r19)
            r0 = r17
            com.agilebinary.a.a.a.e.e r2 = r0.n
            r3.a(r2)
            r0 = r17
            r1 = r18
            com.agilebinary.a.a.a.h.c.c r5 = r0.a(r1, r3)
            com.agilebinary.a.a.a.e.e r2 = r19.g()
            java.lang.String r4 = "http.virtual-host"
            java.lang.Object r2 = r2.a(r4)
            com.agilebinary.a.a.a.b r2 = (com.agilebinary.a.a.a.b) r2
            r0 = r17
            r0.u = r2
            com.agilebinary.a.a.a.c.d.m r4 = new com.agilebinary.a.a.a.c.d.m
            r4.<init>(r3, r5)
            r0 = r17
            com.agilebinary.a.a.a.e.e r2 = r0.n
            int r2 = com.agilebinary.a.a.a.e.c.c(r2)
            long r8 = (long) r2
            r3 = 0
            r5 = 0
            r2 = 0
        L_0x0034:
            r6 = r3
            r16 = r2
            r2 = r5
            r5 = r4
            r4 = r3
            r3 = r16
        L_0x003c:
            if (r6 != 0) goto L_0x022a
            com.agilebinary.a.a.a.c.d.l r6 = r5.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.h.c.c r7 = r5.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r2 = "http.user-token"
            r0 = r20
            java.lang.Object r10 = r0.a(r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r2 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 != 0) goto L_0x00d3
            r0 = r17
            com.agilebinary.a.a.a.h.k r2 = r0.b     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.h.b r11 = r2.a(r7, r10)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r19
            boolean r2 = r0 instanceof com.agilebinary.a.a.a.d.c.g     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x006a
            r0 = r19
            com.agilebinary.a.a.a.d.c.g r0 = (com.agilebinary.a.a.a.d.c.g) r0     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r2 = r0
            r2.a(r11)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x006a:
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException -> 0x008e }
            com.agilebinary.a.a.a.h.a r2 = r11.a(r8, r2)     // Catch:{ InterruptedException -> 0x008e }
            r0 = r17
            r0.o = r2     // Catch:{ InterruptedException -> 0x008e }
            r0 = r17
            com.agilebinary.a.a.a.e.e r2 = r0.n     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 != 0) goto L_0x009d
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r3 = "HTTP parameters may not be null"
            r2.<init>(r3)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            throw r2     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x0082:
            r2 = move-exception
            java.io.InterruptedIOException r3 = new java.io.InterruptedIOException
            java.lang.String r4 = "Connection has been shut down"
            r3.<init>(r4)
            r3.initCause(r2)
            throw r3
        L_0x008e:
            r2 = move-exception
            java.io.InterruptedIOException r3 = new java.io.InterruptedIOException     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.<init>()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.initCause(r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            throw r3     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x0098:
            r2 = move-exception
            r17.b()
            throw r2
        L_0x009d:
            java.lang.String r11 = "http.connection.stalecheck"
            r12 = 1
            boolean r2 = r2.a(r11, r12)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x00d3
            r0 = r17
            com.agilebinary.a.a.a.h.a r2 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r2 = r2.l()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x00d3
            r0 = r17
            org.apache.commons.logging.Log r2 = r0.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r11 = "Stale connection check"
            r2.debug(r11)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r2 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r2 = r2.e()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x00d3
            r0 = r17
            org.apache.commons.logging.Log r2 = r0.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r11 = "Stale connection detected"
            r2.debug(r11)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r2 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r2.k()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x00d3:
            r0 = r19
            boolean r2 = r0 instanceof com.agilebinary.a.a.a.d.c.g     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x00e5
            r0 = r19
            com.agilebinary.a.a.a.d.c.g r0 = (com.agilebinary.a.a.a.d.c.g) r0     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r2 = r0
            r0 = r17
            com.agilebinary.a.a.a.h.a r11 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r2.a(r11)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x00e5:
            r0 = r17
            r1 = r20
            r0.a(r5, r1)     // Catch:{ g -> 0x0210 }
            r6.j()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.net.URI r2 = r6.e_()     // Catch:{ URISyntaxException -> 0x025b }
            com.agilebinary.a.a.a.b r11 = r7.g()     // Catch:{ URISyntaxException -> 0x025b }
            if (r11 == 0) goto L_0x0249
            boolean r11 = r7.d()     // Catch:{ URISyntaxException -> 0x025b }
            if (r11 != 0) goto L_0x0249
            boolean r11 = r2.isAbsolute()     // Catch:{ URISyntaxException -> 0x025b }
            if (r11 != 0) goto L_0x0110
            com.agilebinary.a.a.a.b r11 = r7.a()     // Catch:{ URISyntaxException -> 0x025b }
            java.net.URI r2 = com.agilebinary.a.a.a.d.a.b.a(r2, r11)     // Catch:{ URISyntaxException -> 0x025b }
            r6.a(r2)     // Catch:{ URISyntaxException -> 0x025b }
        L_0x0110:
            r2 = r17
        L_0x0112:
            com.agilebinary.a.a.a.b r2 = r2.u     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 != 0) goto L_0x011a
            com.agilebinary.a.a.a.b r2 = r7.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x011a:
            com.agilebinary.a.a.a.b r7 = r7.g()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r11 = "http.auth.proxy-scope"
            java.lang.String r12 = "http.auth.target-scope"
            java.lang.String r13 = "http.connection"
            java.lang.String r14 = "http.proxy_host"
            java.lang.String r15 = "http.target_host"
            r0 = r20
            r0.a(r15, r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            r0.a(r14, r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r2 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            r0.a(r13, r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.a.e r2 = r0.p     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            r0.a(r12, r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.a.e r2 = r0.q     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            r0.a(r11, r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.f.c r2 = r0.g     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            com.agilebinary.a.a.a.f.b.a(r6, r2, r0)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            r1 = r20
            com.agilebinary.a.a.a.j r6 = r0.b(r5, r1)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r6 == 0) goto L_0x02d1
            r0 = r17
            com.agilebinary.a.a.a.e.e r2 = r0.n     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r6.a(r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.f.c r2 = r0.g     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            com.agilebinary.a.a.a.f.b.a(r6, r2, r0)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.s r2 = r0.d     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            boolean r2 = r2.a(r6, r0)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r2 == 0) goto L_0x01d2
            r0 = r17
            com.agilebinary.a.a.a.h.n r3 = r0.e     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            long r12 = r3.a(r6)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            org.apache.commons.logging.Log r3 = r0.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r3 = r3.isDebugEnabled()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r3 == 0) goto L_0x01c9
            r14 = 0
            int r3 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r3 < 0) goto L_0x0283
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.<init>()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r3 = r3.append(r12)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r7 = " "
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r3 = r3.toString()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r7 = r3
            r3 = r17
        L_0x01b0:
            org.apache.commons.logging.Log r3 = r3.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r11.<init>()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r14 = 0
            java.lang.String r15 = "Connection can be kept alive for "
            java.lang.StringBuilder r11 = r11.insert(r14, r15)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r7 = r11.append(r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r7 = r7.toString()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.debug(r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x01c9:
            r0 = r17
            com.agilebinary.a.a.a.h.a r3 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.a(r12, r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x01d2:
            r0 = r17
            r1 = r20
            com.agilebinary.a.a.a.c.d.m r7 = r0.a(r5, r6, r1)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r7 != 0) goto L_0x028a
            r3 = 1
            r4 = r5
            r5 = r6
        L_0x01df:
            r0 = r17
            com.agilebinary.a.a.a.h.a r7 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r7 == 0) goto L_0x0034
            if (r10 != 0) goto L_0x02ce
            r0 = r17
            com.agilebinary.a.a.a.d.f r5 = r0.m     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r20
            java.lang.Object r5 = r5.a(r0)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r7 = "http.user-token"
            r0 = r20
            r0.a(r7, r5)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r5 == 0) goto L_0x02ce
            r0 = r17
            com.agilebinary.a.a.a.h.a r7 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r7.a(r5)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r5 = r4
            r4 = r3
            r16 = r2
            r2 = r6
            r6 = r3
            r3 = r16
            goto L_0x003c
        L_0x020b:
            r2 = move-exception
            r17.b()
            throw r2
        L_0x0210:
            r2 = move-exception
            r0 = r17
            org.apache.commons.logging.Log r4 = r0.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r4 = r4.isDebugEnabled()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r4 == 0) goto L_0x0226
            r0 = r17
            org.apache.commons.logging.Log r4 = r0.f78a     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r5 = r2.getMessage()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r4.debug(r5)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x0226:
            com.agilebinary.a.a.a.j r2 = r2.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x022a:
            if (r2 == 0) goto L_0x023c
            com.agilebinary.a.a.a.c r4 = r2.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r4 == 0) goto L_0x023c
            com.agilebinary.a.a.a.c r4 = r2.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r4 = r4.g()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r4 != 0) goto L_0x02ba
        L_0x023c:
            if (r3 == 0) goto L_0x0245
            r0 = r17
            com.agilebinary.a.a.a.h.a r3 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.g()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x0245:
            r17.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x0248:
            return r2
        L_0x0249:
            boolean r11 = r2.isAbsolute()     // Catch:{ URISyntaxException -> 0x025b }
            if (r11 == 0) goto L_0x0110
            r11 = 0
            java.net.URI r2 = com.agilebinary.a.a.a.d.a.b.a(r2, r11)     // Catch:{ URISyntaxException -> 0x025b }
            r6.a(r2)     // Catch:{ URISyntaxException -> 0x025b }
            r2 = r17
            goto L_0x0112
        L_0x025b:
            r2 = move-exception
            com.agilebinary.a.a.a.l r3 = new com.agilebinary.a.a.a.l     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r4.<init>()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r5 = 0
            java.lang.String r7 = "Invalid URI: "
            java.lang.StringBuilder r4 = r4.insert(r5, r7)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.d r5 = r6.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r5 = r5.c()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            java.lang.String r4 = r4.toString()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.<init>(r4, r2)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            throw r3     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
        L_0x027e:
            r2 = move-exception
            r17.b()
            throw r2
        L_0x0283:
            java.lang.String r3 = "ever"
            r7 = r3
            r3 = r17
            goto L_0x01b0
        L_0x028a:
            if (r2 == 0) goto L_0x02b1
            com.agilebinary.a.a.a.c r3 = r6.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.i.b.a(r3)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r3 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.g()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3 = r7
        L_0x029b:
            com.agilebinary.a.a.a.h.c.c r3 = r3.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.h.c.c r5 = r5.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            boolean r3 = r3.equals(r5)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            if (r3 != 0) goto L_0x02cc
            r17.a()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3 = r4
        L_0x02ad:
            r5 = r6
            r4 = r7
            goto L_0x01df
        L_0x02b1:
            r0 = r17
            com.agilebinary.a.a.a.h.a r3 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3.k()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r3 = r7
            goto L_0x029b
        L_0x02ba:
            com.agilebinary.a.a.a.h.j r4 = new com.agilebinary.a.a.a.h.j     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            com.agilebinary.a.a.a.c r5 = r2.b()     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r0 = r17
            com.agilebinary.a.a.a.h.a r6 = r0.o     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r4.<init>(r5, r6, r3)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            r2.a(r4)     // Catch:{ n -> 0x0082, k -> 0x0098, IOException -> 0x020b, RuntimeException -> 0x027e }
            goto L_0x0248
        L_0x02cc:
            r3 = r4
            goto L_0x02ad
        L_0x02ce:
            r5 = r6
            goto L_0x0034
        L_0x02d1:
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            goto L_0x0034
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.a.c.d.s.a(com.agilebinary.a.a.a.b, com.agilebinary.a.a.a.f, com.agilebinary.a.a.a.f.k):com.agilebinary.a.a.a.j");
    }
}
