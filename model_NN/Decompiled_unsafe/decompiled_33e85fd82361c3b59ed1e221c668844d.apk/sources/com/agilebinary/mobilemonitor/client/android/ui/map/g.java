package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import java.util.List;
import org.osmdroid.b;
import org.osmdroid.b.a.a;
import org.osmdroid.b.a.c;
import org.osmdroid.b.a.j;

public final class g extends j {
    private int c;
    private boolean d;
    private final Point e;

    private /* synthetic */ g(Context context, List list, a aVar, b bVar) {
        super(context, list, aVar, bVar);
        this.e = new Point();
        this.c = Integer.MIN_VALUE;
    }

    public g(Context context, List list, a aVar, b bVar, byte b) {
        this(context, list, aVar, bVar);
    }

    public final void a(int i) {
        this.c = i;
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
        if (this.c != Integer.MIN_VALUE) {
            c cVar = (c) this.b.get(this.c);
            Drawable a2 = cVar.a(4);
            Point b = cVar.b(4);
            int intrinsicWidth = a2.getIntrinsicWidth();
            int intrinsicHeight = a2.getIntrinsicHeight();
            int i = this.e.x - b.x;
            int i2 = this.e.y - b.y;
            a2.setBounds(i, i2, intrinsicWidth + i, intrinsicHeight + i2);
            a2.draw(canvas);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas, int i, Point point) {
        if (this.c == Integer.MIN_VALUE || i != this.c) {
            super.a(canvas, i, point);
        } else {
            this.e.set(point.x, point.y);
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i, c cVar) {
        if (this.d) {
            this.c = i;
        }
        return this.f352a.a(cVar);
    }

    public final void c() {
        this.d = false;
    }
}
