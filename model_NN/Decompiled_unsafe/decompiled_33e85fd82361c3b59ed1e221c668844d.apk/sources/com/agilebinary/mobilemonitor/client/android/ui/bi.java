package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.biige.client.android.R;

public final class bi extends k {
    private TextView c;
    private TextView d;
    private ImageView e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.bi, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public bi(EventListActivity_base eventListActivity_base) {
        super(eventListActivity_base);
        ((LayoutInflater) eventListActivity_base.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_mms, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.d = (TextView) findViewById(R.id.eventlist_rowview_mms_text);
        this.c = (TextView) findViewById(R.id.eventlist_rowview_mms_dir);
        this.e = (ImageView) findViewById(R.id.eventlist_rowview_mms_thumb);
    }

    public final void a(Cursor cursor) {
        int i = 0;
        super.a(cursor);
        this.d.setText(cursor.getString(cursor.getColumnIndex("text")));
        this.c.setText(cursor.getString(cursor.getColumnIndex("dir")));
        byte[] blob = cursor.getBlob(cursor.getColumnIndex("thumbnail"));
        if (blob != null) {
            try {
                this.e.setImageBitmap(BitmapFactory.decodeByteArray(blob, 0, blob.length));
            } catch (OutOfMemoryError e2) {
                e2.printStackTrace();
            }
        }
        ImageView imageView = this.e;
        if (blob == null) {
            i = 8;
        }
        imageView.setVisibility(i);
    }
}
