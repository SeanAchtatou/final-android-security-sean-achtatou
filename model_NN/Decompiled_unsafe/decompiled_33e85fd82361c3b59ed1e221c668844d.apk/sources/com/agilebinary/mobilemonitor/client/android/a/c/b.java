package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.j;

public final class b extends k implements j {
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0091, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00bb, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00bc, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00c1, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00c2, code lost:
        r0 = null;
        r2 = r1;
        r1 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0091 A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:19:0x0088] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bb A[ExcHandler: all (th java.lang.Throwable), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.s a(com.agilebinary.mobilemonitor.client.android.d r10, com.agilebinary.mobilemonitor.client.android.h r11, com.agilebinary.mobilemonitor.client.android.a.f r12, com.agilebinary.mobilemonitor.client.android.a.o r13, com.agilebinary.mobilemonitor.client.android.a.g r14) {
        /*
            r9 = this;
            long r0 = r11.b     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            long r2 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            long r0 = r0 - r2
            r2 = 86400000(0x5265c00, double:4.2687272E-316)
            long r0 = r0 / r2
            double r0 = (double) r0     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            double r0 = java.lang.Math.floor(r0)     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            int r0 = (int) r0     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            int r2 = r0 * -1
            r0 = 2
            if (r2 > r0) goto L_0x00b8
            if (r2 < 0) goto L_0x00b8
            javax.xml.parsers.SAXParserFactory r0 = javax.xml.parsers.SAXParserFactory.newInstance()     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            javax.xml.parsers.SAXParser r0 = r0.newSAXParser()     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            org.xml.sax.XMLReader r3 = r0.getXMLReader()     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            com.agilebinary.mobilemonitor.client.android.a.c.e r0 = new com.agilebinary.mobilemonitor.client.android.a.c.e     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            byte r1 = r11.f187a     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            r0.<init>(r9, r12, r13, r1)     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            r3.setContentHandler(r0)     // Catch:{ Exception -> 0x00be, all -> 0x00bb }
            byte r0 = r11.f187a     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            r1 = -1
            long r4 = com.agilebinary.mobilemonitor.client.android.a.c.b.b     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            r6 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r4 = r4 - r6
            r12.a(r0, r1, r4)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            com.agilebinary.mobilemonitor.client.android.a.l r0 = com.agilebinary.mobilemonitor.client.android.a.l.a()     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            r9.c = r0     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            r0.<init>()     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            r1 = 0
            java.lang.String r4 = a()     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.StringBuilder r0 = r0.insert(r1, r4)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            byte r1 = r11.f187a     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            switch(r1) {
                case 1: goto L_0x00cc;
                case 2: goto L_0x009d;
                case 3: goto L_0x00c6;
                case 4: goto L_0x00c9;
                case 5: goto L_0x0051;
                case 6: goto L_0x00a0;
                case 7: goto L_0x0051;
                case 8: goto L_0x00cf;
                case 9: goto L_0x00d2;
                default: goto L_0x0051;
            }
        L_0x0051:
            java.lang.String r1 = ""
        L_0x0053:
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.String r1 = "_"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.StringBuilder r0 = r0.append(r2)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.String r1 = ".xml"
            java.lang.StringBuilder r0 = r0.append(r1)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            com.agilebinary.mobilemonitor.client.android.a.l r1 = r9.c     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            com.agilebinary.mobilemonitor.client.android.a.s r0 = r1.a(r0, r14)     // Catch:{ Exception -> 0x00c1, all -> 0x00bb }
            boolean r1 = r0.a()     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            if (r1 != 0) goto L_0x00a3
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            int r2 = r0.b()     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            throw r1     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
        L_0x0084:
            r1 = move-exception
            r2 = r1
            r1 = r0
        L_0x0087:
            r3 = 1
            r12.a(r3)     // Catch:{ Exception -> 0x0091, all -> 0x00d8 }
            com.agilebinary.mobilemonitor.client.android.a.q r1 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ Exception -> 0x0091 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0091 }
            throw r1     // Catch:{ Exception -> 0x0091 }
        L_0x0091:
            r1 = move-exception
        L_0x0092:
            com.agilebinary.mobilemonitor.client.android.a.q r2 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x0098 }
            r2.<init>(r1)     // Catch:{ all -> 0x0098 }
            throw r2     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r1 = move-exception
        L_0x0099:
            a(r0)
            throw r1
        L_0x009d:
            java.lang.String r1 = "call"
            goto L_0x0053
        L_0x00a0:
            java.lang.String r1 = "web"
            goto L_0x0053
        L_0x00a3:
            org.xml.sax.InputSource r1 = new org.xml.sax.InputSource     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            java.io.InputStream r2 = r0.d()     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            r3.parse(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            r1 = 0
            r12.a(r1)     // Catch:{ Exception -> 0x0084, all -> 0x00d6 }
            r1 = r0
        L_0x00b4:
            a(r0)
            return r1
        L_0x00b8:
            r0 = 0
            r1 = r0
            goto L_0x00b4
        L_0x00bb:
            r1 = move-exception
            r0 = 0
            goto L_0x0099
        L_0x00be:
            r1 = move-exception
            r0 = 0
            goto L_0x0092
        L_0x00c1:
            r1 = move-exception
            r0 = 0
            r2 = r1
            r1 = r0
            goto L_0x0087
        L_0x00c6:
            java.lang.String r1 = "sms"
            goto L_0x0053
        L_0x00c9:
            java.lang.String r1 = "mms"
            goto L_0x0053
        L_0x00cc:
            java.lang.String r1 = "location"
            goto L_0x0053
        L_0x00cf:
            java.lang.String r1 = "sys"
            goto L_0x0053
        L_0x00d2:
            java.lang.String r1 = "app"
            goto L_0x0053
        L_0x00d6:
            r1 = move-exception
            goto L_0x0099
        L_0x00d8:
            r0 = move-exception
            r8 = r0
            r0 = r1
            r1 = r8
            goto L_0x0099
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.c.b.a(com.agilebinary.mobilemonitor.client.android.d, com.agilebinary.mobilemonitor.client.android.h, com.agilebinary.mobilemonitor.client.android.a.f, com.agilebinary.mobilemonitor.client.android.a.o, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.s");
    }
}
