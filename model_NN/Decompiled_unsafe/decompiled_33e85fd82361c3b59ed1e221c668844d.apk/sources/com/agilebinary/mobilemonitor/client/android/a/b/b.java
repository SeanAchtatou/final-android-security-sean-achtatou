package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.d;

public final class b extends g implements a {
    private static final String b = com.agilebinary.mobilemonitor.client.android.c.b.a();

    public final long a(d dVar, byte b2, g gVar) {
        s sVar = null;
        new StringBuilder().insert(0, "").append((int) b2).toString();
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceEventRetrieveMaxId?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Type=%4$s").toString(), "1", dVar.b(), dVar.c(), Byte.valueOf(b2));
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, gVar);
            try {
                if (sVar.a()) {
                    long parseLong = Long.parseLong(b(sVar).trim());
                    a(sVar);
                    new StringBuilder().insert(0, "").append(parseLong).toString();
                    return parseLong;
                }
                throw new q(sVar.b());
            } catch (Exception e) {
                e = e;
                try {
                    throw new q(e);
                } catch (Throwable th) {
                    th = th;
                    a(sVar);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                a(sVar);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
        } catch (Throwable th3) {
            th = th3;
            a(sVar);
            throw th;
        }
    }
}
