package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.InputStream;

public final class c extends FilterInputStream {
    public c(InputStream inputStream) {
        super(inputStream);
    }

    public final void a(byte[] bArr) {
        int i = 0;
        do {
            int read = this.in.read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                return;
            }
        } while (i != bArr.length);
    }

    public final boolean a() {
        int read = this.in.read();
        if (read >= 0) {
            return read != 0;
        }
        throw new EOFException();
    }

    public final byte b() {
        int read = this.in.read();
        if (read >= 0) {
            return (byte) read;
        }
        throw new EOFException();
    }

    public final int c() {
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        int read4 = inputStream.read();
        if ((read | read2 | read3 | read4) >= 0) {
            return (read4 << 0) + (read << 24) + (read2 << 16) + (read3 << 8);
        }
        throw new EOFException();
    }

    public final long d() {
        return (((long) c()) << 32) + (((long) c()) & 4294967295L);
    }

    public final double e() {
        int i = 0;
        char[] cArr = new char[this.in.read()];
        int length = cArr.length / 2;
        int length2 = cArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i < (length2 & 1) + length) {
            int read = this.in.read();
            cArr[i3] = (char) ((read >> 4) + 45);
            if (cArr[i3] == '/') {
                cArr[i3] = 'E';
            }
            int i4 = i3 + 1;
            if (i4 < cArr.length) {
                cArr[i4] = (char) ((read & 15) + 45);
                if (cArr[i4] == '/') {
                    cArr[i4] = 'E';
                }
                i4++;
            }
            i2++;
            i3 = i4;
            i = i2;
        }
        try {
            return Double.valueOf(new String(cArr)).doubleValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public final String[] f() {
        String[] strArr = new String[c()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= strArr.length) {
                return strArr;
            }
            strArr[i2] = g();
            i = i2 + 1;
        }
    }

    public final String g() {
        if (b() == 1) {
            return null;
        }
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        if ((read | read2) < 0) {
            throw new EOFException();
        }
        int i = (read2 << 0) + (read << 8);
        char[] cArr = new char[i];
        byte[] bArr = new byte[i];
        read(bArr, 0, i);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i2 < i) {
            byte b = bArr[i3] & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    i2 = i3 + 1;
                    cArr[i4] = (char) b;
                    i4++;
                    i3 = i2;
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    throw new IllegalArgumentException();
                case 12:
                case 13:
                    i2 = i3 + 2;
                    if (i2 <= i) {
                        byte b2 = bArr[i2 - 1];
                        if ((b2 & 192) == 128) {
                            cArr[i4] = (char) ((b2 & 63) | ((b & 31) << 6));
                            i4++;
                            i3 = i2;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                case 14:
                    i2 = i3 + 3;
                    if (i2 <= i) {
                        byte b3 = bArr[i2 - 2];
                        byte b4 = bArr[i2 - 1];
                        if ((b3 & 192) == 128 && (b4 & 192) == 128) {
                            cArr[i4] = (char) (((b3 & 63) << 6) | ((b & 15) << 12) | ((b4 & 63) << 0));
                            i4++;
                            i3 = i2;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
            }
        }
        return new String(cArr, 0, i4);
    }

    public final int read(byte[] bArr, int i, int i2) {
        return this.in.read(bArr, i, i2);
    }
}
