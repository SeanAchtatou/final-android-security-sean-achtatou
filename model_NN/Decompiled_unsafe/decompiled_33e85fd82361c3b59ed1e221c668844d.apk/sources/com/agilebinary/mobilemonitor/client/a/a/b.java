package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public final class b extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    private Inflater f124a;
    private byte[] b;
    private int c;
    private boolean d;
    private boolean e;
    private int f = 0;
    private int g;
    private int h;

    public b(InputStream inputStream, Inflater inflater, byte[] bArr, int i) {
        super(inputStream);
        if (inputStream == null || inflater == null) {
            throw new NullPointerException();
        }
        this.g = i;
        this.f124a = inflater;
        this.b = bArr;
    }

    public final int a() {
        int i = this.g - this.h;
        if (i > 0) {
            System.out.println(new StringBuilder().insert(0, "################ AAAARRRRGGGGHHHHH, ").append(i).append(" bytes not read!!!").toString());
            int i2 = i;
            int i3 = i;
            while (i2 > 0) {
                i2 = i3 - this.in.read(this.b, 0, Math.min(i3, this.b.length));
                i3 = i2;
            }
        }
        return i;
    }

    public final int available() {
        if (!this.d) {
            return this.e ? 0 : 1;
        }
        throw new IOException();
    }

    public final void close() {
        if (!this.d) {
            this.f124a.end();
            this.d = true;
            this.e = true;
            super.close();
        }
    }

    public final void mark(int i) {
    }

    public final boolean markSupported() {
        return false;
    }

    public final int read() {
        byte[] bArr = new byte[1];
        if (read(bArr, 0, 1) == -1) {
            return -1;
        }
        return bArr[0] & 255;
    }

    public final int read(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException();
        } else if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            if (this.f124a.finished()) {
                this.e = true;
                return -1;
            } else if (i > bArr.length || i2 < 0 || i < 0 || bArr.length - i < i2) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                do {
                    if (this.f124a.needsInput()) {
                        if (this.d) {
                            throw new IOException();
                        }
                        this.c = this.in.read(this.b, 0, Math.min(this.b.length, this.g - this.h));
                        if (this.c > 0) {
                            this.h += this.c;
                            this.f124a.setInput(this.b, 0, this.c);
                        }
                    }
                    try {
                        int inflate = this.f124a.inflate(bArr, i, i2);
                        if (inflate > 0) {
                            return inflate;
                        }
                        if (this.f124a.finished()) {
                            this.e = true;
                            return -1;
                        } else if (this.f124a.needsDictionary()) {
                            return -1;
                        }
                    } catch (DataFormatException e2) {
                        if (this.c == -1) {
                            throw new EOFException();
                        }
                        throw ((IOException) new IOException().initCause(e2));
                    }
                } while (this.c != -1);
                throw new EOFException();
            }
        }
    }

    public final void reset() {
        throw new IOException();
    }

    public final long skip(long j) {
        long j2;
        int i;
        b bVar;
        long j3 = 0;
        if (j >= 0) {
            while (true) {
                j2 = j3;
                if (j3 >= j) {
                    break;
                }
                byte[] bArr = this.b;
                long j4 = j - j2;
                if (j4 > ((long) this.b.length)) {
                    i = this.b.length;
                    bVar = this;
                } else {
                    i = (int) j4;
                    bVar = this;
                }
                int read = bVar.read(bArr, 0, i);
                if (read == -1) {
                    this.e = true;
                    break;
                }
                j3 = ((long) read) + j2;
            }
            return j2;
        }
        throw new IllegalArgumentException();
    }
}
