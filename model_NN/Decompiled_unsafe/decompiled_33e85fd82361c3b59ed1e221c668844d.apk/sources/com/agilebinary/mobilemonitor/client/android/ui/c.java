package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.AsyncTask;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.android.ui.map.e;
import com.agilebinary.mobilemonitor.client.android.ui.map.f;
import com.agilebinary.mobilemonitor.client.android.ui.map.i;
import com.agilebinary.mobilemonitor.client.android.ui.map.m;
import com.biige.client.android.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class c extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ MapActivity_GOOGLE f265a;

    c(MapActivity_GOOGLE mapActivity_GOOGLE) {
        this.f265a = mapActivity_GOOGLE;
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* JADX WARN: Type inference failed for: r3v5, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
        this.f265a.b.setSatellite(this.f265a.i.a("MAP_SATELLITE_VIEW__BOOL", false));
        this.f265a.b.setClickable(true);
        List unused = this.f265a.h = this.f265a.w.a(((List[]) objArr)[0]);
        if (!isCancelled()) {
            Iterator it = this.f265a.h.iterator();
            loop0:
            while (true) {
                while (true) {
                    if (!it.hasNext()) {
                        break loop0;
                    }
                    s sVar = (s) it.next();
                    if (((d) sVar).i() == null) {
                        new StringBuilder().insert(0, "aaargh, mappable point is null ").append(sVar.u()).append("  / ").append(sVar.b(this.f265a)).append(" / ").append(sVar.c(this.f265a)).toString();
                        if (sVar instanceof h) {
                            new StringBuilder().insert(0, "CID =").append(((h) sVar).b()).toString();
                        }
                        it.remove();
                    }
                }
            }
        }
        c unused2 = this.f265a.u = null;
        return null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE, android.app.Activity] */
    /* JADX WARN: Type inference failed for: r2v7, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* JADX WARN: Type inference failed for: r2v8, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* JADX WARN: Type inference failed for: r4v2, types: [android.content.Context, com.agilebinary.mobilemonitor.client.android.ui.MapActivity_GOOGLE] */
    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
        if (!isCancelled()) {
            i unused = this.f265a.d = new i(this.f265a);
            this.f265a.b.getOverlays().add(this.f265a.d);
            this.f265a.d.a(((float) (this.f265a.getResources().getDisplayMetrics().widthPixels / 2)) - (this.f265a.getResources().getDisplayMetrics().xdpi / 2.0f));
            this.f265a.d.a(this.f265a.i.a("MAP_LAYER_SCALEBAR__BOOL", true));
            f unused2 = this.f265a.e = new f(this.f265a, this.f265a.b);
            this.f265a.b.getOverlays().add(this.f265a.e);
            this.f265a.e.a(this.f265a.h);
            this.f265a.e.a(false);
            m unused3 = this.f265a.f = new m(this.f265a);
            this.f265a.b.getOverlays().add(this.f265a.f);
            this.f265a.f.a(this.f265a.h);
            this.f265a.f.a(this.f265a.i.a("MAP_LAYER_ACCURACY__BOOL", true));
            ArrayList arrayList = new ArrayList(this.f265a.h.size());
            Iterator it = this.f265a.h.iterator();
            while (it.hasNext()) {
                arrayList.add(new ag((s) it.next(), this.f265a));
            }
            e unused4 = this.f265a.g = new e(this.f265a.getResources().getDrawable(R.drawable.mapmarker_green), arrayList, this.f265a);
            this.f265a.b.getOverlays().add(this.f265a.g);
            this.f265a.b.setBuiltInZoomControls(true);
            MapActivity_GOOGLE.j(this.f265a);
            this.f265a.e.a(this.f265a.i.a("MAP_LAYER_DIRECTIONS__BOOL", true));
            if (this.f265a.h.size() > 0) {
                this.f265a.a((s) this.f265a.h.get(0));
            }
        }
    }
}
