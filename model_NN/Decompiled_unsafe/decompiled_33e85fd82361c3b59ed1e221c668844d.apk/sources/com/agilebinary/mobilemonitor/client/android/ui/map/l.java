package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.n;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.biige.client.android.R;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.osmdroid.b;
import org.osmdroid.b.a;
import org.osmdroid.b.a.e;
import org.osmdroid.b.f;
import org.osmdroid.c;

public final class l extends e {

    /* renamed from: a  reason: collision with root package name */
    private Paint f287a;
    private Paint b;
    private Paint c;
    private Paint d;
    private List e;
    private Set f;
    private int g;

    public l(Context context) {
        this(context, new c(context));
    }

    private /* synthetic */ l(Context context, b bVar) {
        super(bVar);
        this.g = -1;
        this.f287a = new Paint();
        this.f287a.setColor(context.getResources().getColor(R.color.accuraccy_coords_fill));
        this.f287a.setStyle(Paint.Style.FILL);
        this.f287a.setAntiAlias(true);
        this.b = new Paint();
        this.b.setColor(context.getResources().getColor(R.color.accuraccy_coords_border));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth(1.0f);
        this.b.setAntiAlias(true);
        this.c = new Paint();
        this.c.setColor(context.getResources().getColor(R.color.accuraccy_cell_fill));
        this.c.setStyle(Paint.Style.FILL);
        this.c.setAntiAlias(true);
        this.d = new Paint();
        this.d.setColor(context.getResources().getColor(R.color.accuraccy_cell_border));
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(1.0f);
        this.d.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public final void a(Canvas canvas) {
    }

    public final void a(Canvas canvas, f fVar) {
        if (this.e != null && this.e.size() != 0) {
            boolean z = false;
            a e2 = fVar.e();
            if (this.g != e2.c()) {
                z = true;
                this.g = e2.c();
            }
            if (z) {
                this.f = new HashSet(this.e.size());
                Point point = new Point();
                Iterator it = this.e.iterator();
                while (it.hasNext()) {
                    d dVar = (d) ((s) it.next());
                    e2.a(dVar.h(), point);
                    this.f.add(new h(this, point.x, point.y, e2.a((float) dVar.g()), dVar instanceof n ? this.c : this.f287a, dVar instanceof n ? this.d : this.b));
                }
            }
            Iterator it2 = this.f.iterator();
            while (it2.hasNext()) {
                h hVar = (h) it2.next();
                canvas.drawCircle((float) hVar.f283a, (float) hVar.b, hVar.c, hVar.d);
                canvas.drawCircle((float) hVar.f283a, (float) hVar.b, hVar.c, hVar.e);
            }
        }
    }

    public final void a(List list) {
        this.e = list;
    }
}
