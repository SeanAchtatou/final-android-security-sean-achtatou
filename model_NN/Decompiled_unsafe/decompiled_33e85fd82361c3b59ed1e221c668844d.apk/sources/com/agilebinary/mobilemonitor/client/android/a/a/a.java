package com.agilebinary.mobilemonitor.client.android.a.a;

import java.io.Serializable;

public final class a extends c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f143a;
    private int b;
    private b c;

    public final String a() {
        return new StringBuilder().insert(0, "{\"create_date\":\"").append(this.f143a).append("\", \"geocoder_source_id\":\"").append(this.b).append("\", \"").append("mobile_country_code").append("\":\"").append(super.c()).append("\", \"").append("mobile_network_code").append("\":\"").append(super.d()).append("\", \"").append("cell_id").append("\":\"").append(super.e()).append("\", \"").append("location_area_code").append("\":\"").append(super.f()).append("\", \"").append("latitude").append("\":\"").append(this.c.f144a).append("\", \"").append("longitude").append("\":\"").append(this.c.b).append("\", \"").append("accuracy").append("\":\"").append(this.c.c).append("\", \"").append("country").append("\":\"").append(this.c.d).append("\", \"").append("country_code").append("\":\"").append(this.c.e).append("\", \"").append("region").append("\":\"").append(this.c.f).append("\", \"").append("city").append("\":\"").append(this.c.g).append("\", \"").append("street").append("\":\"").append(this.c.h).append("\"}").toString();
    }

    public final void a(int i) {
        this.b = i;
    }

    public final void a(long j) {
        this.f143a = j;
    }

    public final void a(b bVar) {
        this.c = bVar;
    }

    public final b b() {
        return this.c;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "GeocodeGsmCellLocation: ").append(a()).toString();
    }
}
