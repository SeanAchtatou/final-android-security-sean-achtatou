package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import com.agilebinary.mobilemonitor.client.android.i;
import com.biige.client.android.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventListActivity_LOC extends EventListActivity_base {
    protected AlertDialog h;
    protected int i = 2;
    private Button k;
    private Button l;
    private i m;
    private Set n = new HashSet();

    static /* synthetic */ void a(EventListActivity_LOC eventListActivity_LOC) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(eventListActivity_LOC.n);
        Collections.sort(arrayList, new ae(eventListActivity_LOC));
        eventListActivity_LOC.a(arrayList);
    }

    private /* synthetic */ void a(List list) {
        Intent intent = new Intent(this, MapActivity_GOOGLE.class);
        intent.putExtra("EXTRA_EVENT_IDS", (Serializable) list);
        startActivity(intent);
    }

    public final void a(long j, boolean z) {
        EventListActivity_LOC eventListActivity_LOC;
        if (z) {
            this.n.add(Long.valueOf(j));
            eventListActivity_LOC = this;
        } else {
            this.n.remove(Long.valueOf(j));
            eventListActivity_LOC = this;
        }
        eventListActivity_LOC.k.setEnabled(this.n.size() > 0);
    }

    /* access modifiers changed from: protected */
    public final void a(ax axVar) {
        boolean z = true;
        super.a(axVar);
        this.l.setEnabled(axVar != ax.PROGRESS);
        Button button = this.k;
        if (axVar == ax.PROGRESS) {
            z = false;
        }
        button.setEnabled(z);
    }

    public final void a(boolean z) {
        super.a(z);
        if (this.m != null) {
            this.m.c();
        }
    }

    /* access modifiers changed from: protected */
    public final ba a_() {
        return new ad(this);
    }

    public final void b(long j) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j));
        a(arrayList);
    }

    /* access modifiers changed from: protected */
    public final void b_() {
        super.b_();
        l();
    }

    public final boolean c(long j) {
        return this.n.contains(Long.valueOf(j));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void l() {
        /*
            r12 = this;
            android.database.Cursor r0 = r12.f
            java.lang.String r1 = "lat"
            int r6 = r0.getColumnIndex(r1)
            java.util.Set r0 = r12.n
            r0.clear()
            android.database.Cursor r0 = r12.f
            boolean r0 = r0.moveToFirst()
            if (r0 == 0) goto L_0x0020
            int r0 = r12.i
            switch(r0) {
                case 0: goto L_0x002f;
                case 1: goto L_0x0036;
                case 2: goto L_0x0058;
                case 3: goto L_0x0118;
                default: goto L_0x001a;
            }
        L_0x001a:
            r0 = r12
        L_0x001b:
            com.agilebinary.mobilemonitor.client.android.ui.a r0 = r0.c
            r0.notifyDataSetInvalidated()
        L_0x0020:
            android.widget.Button r0 = r12.k
            java.util.Set r1 = r12.n
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x0152
            r1 = 1
        L_0x002b:
            r0.setEnabled(r1)
            return
        L_0x002f:
            java.util.Set r0 = r12.n
            r0.clear()
            r0 = r12
            goto L_0x001b
        L_0x0036:
            android.database.Cursor r0 = r12.f
            boolean r0 = r0.isNull(r6)
            if (r0 != 0) goto L_0x004e
            java.util.Set r0 = r12.n
            android.database.Cursor r1 = r12.f
            r2 = 0
            long r2 = r1.getLong(r2)
            java.lang.Long r1 = java.lang.Long.valueOf(r2)
            r0.add(r1)
        L_0x004e:
            android.database.Cursor r0 = r12.f
            boolean r0 = r0.moveToNext()
            if (r0 != 0) goto L_0x0036
            r0 = r12
            goto L_0x001b
        L_0x0058:
            r1 = 0
            r2 = 0
            r0 = 0
        L_0x005b:
            android.database.Cursor r3 = r12.f
            r4 = 0
            long r8 = r3.getLong(r4)
            android.database.Cursor r3 = r12.f
            boolean r3 = r3.isNull(r6)
            if (r3 != 0) goto L_0x00d8
            if (r0 == 0) goto L_0x0155
            long r4 = r0.longValue()
            long r4 = r4 - r8
            long r4 = java.lang.Math.abs(r4)
            r10 = 5000(0x1388, double:2.4703E-320)
            int r0 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r0 <= 0) goto L_0x0155
            if (r2 == 0) goto L_0x0082
            java.util.Set r0 = r12.n
            r0.add(r2)
        L_0x0082:
            r2 = 0
            r1 = 0
            r0 = r12
        L_0x0085:
            android.database.Cursor r3 = r0.f
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            java.lang.String r0 = "contenttype"
            int r0 = r3.getColumnIndex(r0)
            int r0 = r3.getInt(r0)
            r7 = 5
            if (r0 == r7) goto L_0x009c
            r7 = 6
            if (r0 == r7) goto L_0x009c
            r7 = 4
            if (r0 != r7) goto L_0x0102
        L_0x009c:
            java.lang.String r0 = "powersave"
            int r0 = r3.getColumnIndex(r0)
            int r0 = r3.getInt(r0)
            r4 = 1
            if (r0 != r4) goto L_0x00ea
            r4 = 4611686018427387904(0x4000000000000000, double:2.0)
        L_0x00ab:
            r0 = r3
        L_0x00ac:
            java.lang.String r7 = "accuracy"
            int r3 = r3.getColumnIndex(r7)
            double r10 = r0.getDouble(r3)
            java.lang.Double r0 = java.lang.Double.valueOf(r10)
            if (r0 != 0) goto L_0x0111
            r4 = 9218868437227405311(0x7fefffffffffffff, double:1.7976931348623157E308)
            r0 = r2
        L_0x00c2:
            if (r0 == 0) goto L_0x00cc
            double r10 = r1.doubleValue()
            int r0 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d4
        L_0x00cc:
            java.lang.Double r1 = java.lang.Double.valueOf(r4)
            java.lang.Long r2 = java.lang.Long.valueOf(r8)
        L_0x00d4:
            java.lang.Long r0 = java.lang.Long.valueOf(r8)
        L_0x00d8:
            android.database.Cursor r3 = r12.f
            boolean r3 = r3.moveToNext()
            if (r3 != 0) goto L_0x005b
            if (r2 == 0) goto L_0x001a
            java.util.Set r0 = r12.n
            r0.add(r2)
            r0 = r12
            goto L_0x001b
        L_0x00ea:
            java.lang.String r0 = "valloc"
            int r0 = r3.getColumnIndex(r0)
            int r0 = r3.getInt(r0)
            r4 = 1
            if (r0 != r4) goto L_0x00fb
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r0 = r3
            goto L_0x00ac
        L_0x00fb:
            r4 = 4726483295817170944(0x4197d783fc000000, double:9.9999999E7)
            r0 = r3
            goto L_0x00ac
        L_0x0102:
            r7 = 7
            if (r0 == r7) goto L_0x010d
            r7 = 8
            if (r0 == r7) goto L_0x010d
            r7 = 12
            if (r0 != r7) goto L_0x00ab
        L_0x010d:
            r4 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            r0 = r3
            goto L_0x00ac
        L_0x0111:
            double r10 = r0.doubleValue()
            double r4 = r4 * r10
            r0 = r2
            goto L_0x00c2
        L_0x0118:
            android.database.Cursor r0 = r12.f
            java.lang.String r1 = "contenttype"
            int r0 = r0.getColumnIndex(r1)
        L_0x0120:
            android.database.Cursor r1 = r12.f
            boolean r1 = r1.isNull(r6)
            if (r1 != 0) goto L_0x0147
            android.database.Cursor r1 = r12.f
            int r1 = r1.getInt(r0)
            r2 = 5
            if (r1 == r2) goto L_0x0137
            r2 = 6
            if (r1 == r2) goto L_0x0137
            r2 = 4
            if (r1 != r2) goto L_0x0147
        L_0x0137:
            java.util.Set r1 = r12.n
            android.database.Cursor r2 = r12.f
            r3 = 0
            long r2 = r2.getLong(r3)
            java.lang.Long r2 = java.lang.Long.valueOf(r2)
            r1.add(r2)
        L_0x0147:
            android.database.Cursor r1 = r12.f
            boolean r1 = r1.moveToNext()
            if (r1 != 0) goto L_0x0120
            r0 = r12
            goto L_0x001b
        L_0x0152:
            r1 = 0
            goto L_0x002b
        L_0x0155:
            r0 = r12
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.EventListActivity_LOC.l():void");
    }

    public final boolean m() {
        return this.f201a.isEnabled();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.i = bundle.getInt("EXTRA_FILTER_ID");
        }
        this.b.addView(getLayoutInflater().inflate((int) R.layout.eventlist_footer_loc, (ViewGroup) null), 1);
        this.k = (Button) findViewById(R.id.eventlist_loc_showmap);
        this.k.setOnClickListener(new af(this));
        this.l = (Button) findViewById(R.id.eventlist_loc_filter);
        this.l.setOnClickListener(new ab(this));
        String string = getString(R.string.label_map_filter_clear);
        String string2 = getString(R.string.label_map_filter_all);
        String string3 = getString(R.string.label_map_filter_smart);
        String string4 = getString(R.string.label_map_filter_gpsnet);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.label_map_filter_prompt);
        CharSequence[] charSequenceArr = {string, string2, string3, string4};
        builder.setSingleChoiceItems(charSequenceArr, this.i, new ac(this));
        this.h = builder.create();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j) {
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("EXTRA_FILTER_ID", this.i);
    }
}
