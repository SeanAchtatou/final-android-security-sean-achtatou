package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.biige.client.android.R;

public final class at extends k {
    private TextView c;
    private TextView d;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.at, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public at(EventListActivity_base eventListActivity_base) {
        super(eventListActivity_base);
        ((LayoutInflater) eventListActivity_base.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_web, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public final void a() {
        super.a();
        this.c = (TextView) findViewById(R.id.eventlist_rowview_web_line1);
        this.d = (TextView) findViewById(R.id.eventlist_rowview_web_line2);
    }

    public final void a(Cursor cursor) {
        super.a(cursor);
        this.c.setText(cursor.getString(cursor.getColumnIndex("line1")));
        this.d.setText(cursor.getString(cursor.getColumnIndex("line2")));
    }
}
