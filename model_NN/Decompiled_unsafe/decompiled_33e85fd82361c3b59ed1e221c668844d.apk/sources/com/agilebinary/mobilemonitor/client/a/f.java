package com.agilebinary.mobilemonitor.client.a;

import com.agilebinary.mobilemonitor.client.android.c.b;
import java.util.zip.Inflater;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private static String f140a = b.a();
    private static byte[] b = new byte[8192];
    private static byte[] c = new byte[2048];
    private static Inflater d = new Inflater(false);

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0017, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0018, code lost:
        r3 = com.agilebinary.mobilemonitor.client.a.f.class;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001b, code lost:
        throw r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.agilebinary.mobilemonitor.client.a.b.o a(java.lang.String r15, long r16, long r18, boolean r20, boolean r21, int r22, java.io.InputStream r23, com.agilebinary.mobilemonitor.client.a.e r24, com.agilebinary.mobilemonitor.client.a.b r25, android.content.Context r26, java.io.ByteArrayOutputStream r27) {
        /*
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r2 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-enter(r2)
            int r2 = r23.read()     // Catch:{ all -> 0x0015 }
            int r3 = r23.read()     // Catch:{ all -> 0x0015 }
            r4 = r2 | r3
            if (r4 >= 0) goto L_0x001c
            java.io.EOFException r2 = new java.io.EOFException     // Catch:{ all -> 0x0015 }
            r2.<init>()     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x0015:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r2 = move-exception
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r3 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-exit(r3)
            throw r2
        L_0x001c:
            int r2 = r2 << 8
            int r3 = r3 << 0
            int r2 = r2 + r3
            short r2 = (short) r2
            if (r2 == 0) goto L_0x003e
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r4.<init>()     // Catch:{ all -> 0x0015 }
            r5 = 0
            java.lang.String r6 = "unknown encoding version: "
            java.lang.StringBuilder r4 = r4.insert(r5, r6)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ all -> 0x0015 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0015 }
            r3.<init>(r2)     // Catch:{ all -> 0x0015 }
            throw r3     // Catch:{ all -> 0x0015 }
        L_0x003e:
            int r2 = r23.read()     // Catch:{ all -> 0x0015 }
            if (r2 >= 0) goto L_0x004a
            java.io.EOFException r2 = new java.io.EOFException     // Catch:{ all -> 0x0015 }
            r2.<init>()     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x004a:
            byte r3 = (byte) r2     // Catch:{ all -> 0x0015 }
            if (r20 == 0) goto L_0x0055
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.String r3 = "PENDING: encryption not yet implemented"
            r2.<init>(r3)     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x0055:
            r13 = 0
            if (r21 == 0) goto L_0x0094
            java.util.zip.Inflater r2 = com.agilebinary.mobilemonitor.client.a.f.d     // Catch:{ all -> 0x0015 }
            r2.reset()     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.b r13 = new com.agilebinary.mobilemonitor.client.a.a.b     // Catch:{ all -> 0x0015 }
            java.util.zip.Inflater r2 = com.agilebinary.mobilemonitor.client.a.f.d     // Catch:{ all -> 0x0015 }
            byte[] r4 = com.agilebinary.mobilemonitor.client.a.f.c     // Catch:{ all -> 0x0015 }
            int r5 = r22 + -3
            r0 = r23
            r13.<init>(r0, r2, r4, r5)     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.c r8 = new com.agilebinary.mobilemonitor.client.a.a.c     // Catch:{ all -> 0x0015 }
            com.agilebinary.mobilemonitor.client.a.a.a r2 = new com.agilebinary.mobilemonitor.client.a.a.a     // Catch:{ all -> 0x0015 }
            byte[] r4 = com.agilebinary.mobilemonitor.client.a.f.b     // Catch:{ all -> 0x0015 }
            r2.<init>(r13, r4)     // Catch:{ all -> 0x0015 }
            r8.<init>(r2)     // Catch:{ all -> 0x0015 }
            r2 = r3
        L_0x0077:
            switch(r2) {
                case 1: goto L_0x00ca;
                case 2: goto L_0x00d9;
                case 3: goto L_0x00e8;
                case 4: goto L_0x00fd;
                case 5: goto L_0x010c;
                case 6: goto L_0x015a;
                case 7: goto L_0x011b;
                case 8: goto L_0x012a;
                case 9: goto L_0x014a;
                case 10: goto L_0x016a;
                case 11: goto L_0x009d;
                case 12: goto L_0x013a;
                case 13: goto L_0x017a;
                case 14: goto L_0x018a;
                default: goto L_0x007a;
            }     // Catch:{ all -> 0x0015 }
        L_0x007a:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r4.<init>()     // Catch:{ all -> 0x0015 }
            r5 = 0
            java.lang.String r6 = "unknown content type: "
            java.lang.StringBuilder r4 = r4.insert(r5, r6)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ all -> 0x0015 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0015 }
            r2.<init>(r3)     // Catch:{ all -> 0x0015 }
            throw r2     // Catch:{ all -> 0x0015 }
        L_0x0094:
            com.agilebinary.mobilemonitor.client.a.a.c r8 = new com.agilebinary.mobilemonitor.client.a.a.c     // Catch:{ all -> 0x0015 }
            r0 = r23
            r8.<init>(r0)     // Catch:{ all -> 0x0015 }
            r2 = r3
            goto L_0x0077
        L_0x009d:
            com.agilebinary.mobilemonitor.client.a.b.c r2 = new com.agilebinary.mobilemonitor.client.a.b.c     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
        L_0x00ab:
            if (r2 == 0) goto L_0x00c6
            int r2 = r13.a()     // Catch:{ all -> 0x0015 }
            if (r2 <= 0) goto L_0x00c6
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0015 }
            r2.<init>()     // Catch:{ all -> 0x0015 }
            r4 = 0
            java.lang.String r5 = "not all bytes read from compressed stream whhen deserializing "
            java.lang.StringBuilder r2 = r2.insert(r4, r5)     // Catch:{ all -> 0x0015 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0015 }
            r2.toString()     // Catch:{ all -> 0x0015 }
        L_0x00c6:
            java.lang.Class<com.agilebinary.mobilemonitor.client.a.f> r2 = com.agilebinary.mobilemonitor.client.a.f.class
            monitor-exit(r2)
            return r3
        L_0x00ca:
            com.agilebinary.mobilemonitor.client.a.b.u r2 = new com.agilebinary.mobilemonitor.client.a.b.u     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x00d9:
            com.agilebinary.mobilemonitor.client.a.b.j r2 = new com.agilebinary.mobilemonitor.client.a.b.j     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x00e8:
            com.agilebinary.mobilemonitor.client.a.b.f r2 = new com.agilebinary.mobilemonitor.client.a.b.f     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r10 = r25
            r11 = r26
            r12 = r27
            r2.<init>(r3, r4, r6, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x00fd:
            com.agilebinary.mobilemonitor.client.a.b.k r2 = new com.agilebinary.mobilemonitor.client.a.b.k     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x010c:
            com.agilebinary.mobilemonitor.client.a.b.b r2 = new com.agilebinary.mobilemonitor.client.a.b.b     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x011b:
            com.agilebinary.mobilemonitor.client.a.b.h r2 = new com.agilebinary.mobilemonitor.client.a.b.h     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x012a:
            com.agilebinary.mobilemonitor.client.a.b.r r2 = new com.agilebinary.mobilemonitor.client.a.b.r     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x013a:
            com.agilebinary.mobilemonitor.client.a.b.l r2 = new com.agilebinary.mobilemonitor.client.a.b.l     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x014a:
            com.agilebinary.mobilemonitor.client.a.b.p r2 = new com.agilebinary.mobilemonitor.client.a.b.p     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x015a:
            com.agilebinary.mobilemonitor.client.a.b.m r2 = new com.agilebinary.mobilemonitor.client.a.b.m     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x016a:
            com.agilebinary.mobilemonitor.client.a.b.d r2 = new com.agilebinary.mobilemonitor.client.a.b.d     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x017a:
            com.agilebinary.mobilemonitor.client.a.b.e r2 = new com.agilebinary.mobilemonitor.client.a.b.e     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        L_0x018a:
            com.agilebinary.mobilemonitor.client.a.b.g r2 = new com.agilebinary.mobilemonitor.client.a.b.g     // Catch:{ all -> 0x0015 }
            r3 = r15
            r4 = r16
            r6 = r18
            r9 = r24
            r2.<init>(r3, r4, r6, r8, r9)     // Catch:{ all -> 0x0015 }
            r3 = r2
            r2 = r13
            goto L_0x00ab
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.a.f.a(java.lang.String, long, long, boolean, boolean, int, java.io.InputStream, com.agilebinary.mobilemonitor.client.a.e, com.agilebinary.mobilemonitor.client.a.b, android.content.Context, java.io.ByteArrayOutputStream):com.agilebinary.mobilemonitor.client.a.b.o");
    }
}
