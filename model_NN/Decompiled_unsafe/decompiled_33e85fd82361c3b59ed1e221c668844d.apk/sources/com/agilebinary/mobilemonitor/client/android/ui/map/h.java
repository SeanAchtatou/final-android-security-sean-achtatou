package com.agilebinary.mobilemonitor.client.android.ui.map;

import android.graphics.Paint;

final class h {

    /* renamed from: a  reason: collision with root package name */
    int f283a;
    int b;
    float c;
    Paint d;
    Paint e;
    private /* synthetic */ l f;

    public h(l lVar, int i, int i2, float f2, Paint paint, Paint paint2) {
        this.f = lVar;
        this.f283a = i;
        this.b = i2;
        this.c = f2;
        this.d = paint;
        this.e = paint2;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        h hVar = (h) obj;
        if (!this.f.equals(hVar.f)) {
            return false;
        }
        if (this.d == null) {
            if (hVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(hVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (hVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(hVar.e)) {
            return false;
        }
        if (Float.floatToIntBits(this.c) != Float.floatToIntBits(hVar.c)) {
            return false;
        }
        if (this.f283a != hVar.f283a) {
            return false;
        }
        return this.b == hVar.b;
    }

    public final int hashCode() {
        int hashCode;
        h hVar;
        int i;
        int i2 = 0;
        int hashCode2 = this.f.hashCode();
        if (this.d == null) {
            hVar = this;
            hashCode = 0;
        } else {
            hashCode = this.d.hashCode();
            hVar = this;
        }
        if (hVar.e == null) {
            i = hashCode2;
        } else {
            i2 = this.e.hashCode();
            i = hashCode2;
        }
        return ((((((((((i + 31) * 31) + hashCode) * 31) + i2) * 31) + Float.floatToIntBits(this.c)) * 31) + this.f283a) * 31) + this.b;
    }
}
