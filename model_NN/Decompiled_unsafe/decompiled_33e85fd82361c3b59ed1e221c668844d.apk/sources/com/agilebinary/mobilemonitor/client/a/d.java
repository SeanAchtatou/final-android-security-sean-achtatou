package com.agilebinary.mobilemonitor.client.a;

import android.content.Context;
import org.osmdroid.util.GeoPoint;

public interface d {
    CharSequence a(Context context);

    double g();

    GeoPoint h();

    com.google.android.maps.GeoPoint i();

    boolean j();
}
