package com.agilebinary.mobilemonitor.client.android.c;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.b.i;
import com.biige.client.android.R;
import java.text.NumberFormat;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static NumberFormat f179a = NumberFormat.getIntegerInstance();

    private static /* synthetic */ double a(double d, String str) {
        return "mph".equals(str) ? (d * 3.6d) / 1.609d : d * 3.6d;
    }

    public static CharSequence a(Context context, i iVar) {
        String str;
        i iVar2;
        if (iVar.n()) {
            String string = context.getString(R.string.speed_unit);
            try {
                str = context.getResources().getString(com.a.a.a.a.class.getField(new StringBuilder().insert(0, "speed_unit_display_").append(string).toString()).getInt(null));
                iVar2 = iVar;
            } catch (Exception e) {
                e.printStackTrace();
                str = "";
                iVar2 = iVar;
            }
            Double valueOf = iVar2.q() ? Double.valueOf(a(iVar.o(), string)) : Double.valueOf(0.0d);
            String c = c.a().c(iVar.p());
            if (iVar.q()) {
                return context.getString(R.string.msg_event_speed, f179a.format(valueOf.doubleValue()), str, c);
            }
        }
        return context.getString(R.string.label_event_not_available);
    }

    public static CharSequence a(Context context, Double d) {
        if (d == null) {
            return context.getString(R.string.label_event_not_available);
        }
        String string = context.getString(R.string.speed_unit);
        String str = "";
        try {
            str = context.getResources().getString(com.a.a.a.a.class.getField(new StringBuilder().insert(0, "speed_unit_display_").append(string).toString()).getInt(null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context.getString(R.string.msg_event_speed_short, f179a.format(Double.valueOf(a(d.doubleValue(), string)).doubleValue()), str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x0426  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(android.content.Context r9, java.lang.String r10) {
        /*
            r8 = 1
            r4 = 0
            r2 = 0
            java.lang.String r0 = "\\|"
            java.lang.String[] r3 = r10.split(r0)
            int r0 = r3.length
            if (r0 <= 0) goto L_0x042a
            r0 = 0
            r0 = r3[r0]     // Catch:{ Exception -> 0x007b }
            java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r0)     // Catch:{ Exception -> 0x007b }
            r1 = r0
        L_0x0014:
            if (r0 == 0) goto L_0x042a
            java.lang.String r0 = r1.getDisplayName()
            r1 = r0
        L_0x001b:
            if (r1 != 0) goto L_0x001f
            r0 = r3[r4]
        L_0x001f:
            int r1 = r3.length
            if (r1 <= r8) goto L_0x0426
            r5 = r3[r8]
            int r1 = java.lang.Integer.parseInt(r5)     // Catch:{ NumberFormatException -> 0x0082 }
            r3 = r1
        L_0x0029:
            if (r1 != 0) goto L_0x0086
            java.lang.String r3 = r5.toUpperCase()
        L_0x002f:
            java.lang.Class<com.a.a.a.a> r1 = com.a.a.a.a.class
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x041f }
            r5.<init>()     // Catch:{ Exception -> 0x041f }
            r6 = 0
            java.lang.String r7 = "CC_"
            java.lang.StringBuilder r5 = r5.insert(r6, r7)     // Catch:{ Exception -> 0x041f }
            java.lang.StringBuilder r5 = r5.append(r3)     // Catch:{ Exception -> 0x041f }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x041f }
            java.lang.reflect.Field r1 = r1.getField(r5)     // Catch:{ Exception -> 0x041f }
            r5 = 0
            int r1 = r1.getInt(r5)     // Catch:{ Exception -> 0x041f }
            android.content.res.Resources r5 = r9.getResources()     // Catch:{ Exception -> 0x041f }
            java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x041f }
            r2 = r1
        L_0x0057:
            if (r2 != 0) goto L_0x0068
            android.content.res.Resources r1 = r9.getResources()
            r2 = 2131099830(0x7f0600b6, float:1.7812024E38)
            java.lang.Object[] r5 = new java.lang.Object[r8]
            r5[r4] = r3
            java.lang.String r1 = r1.getString(r2, r5)
        L_0x0068:
            android.content.res.Resources r2 = r9.getResources()
            r3 = 2131099831(0x7f0600b7, float:1.7812026E38)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r5[r4] = r1
            r5[r8] = r0
            java.lang.String r0 = r2.getString(r3, r5)
            return r0
        L_0x007b:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            r1 = r2
            goto L_0x0014
        L_0x0082:
            r1 = move-exception
            r1 = r4
            r3 = r4
            goto L_0x0029
        L_0x0086:
            switch(r3) {
                case 202: goto L_0x01b7;
                case 203: goto L_0x0089;
                case 204: goto L_0x02bf;
                case 205: goto L_0x0089;
                case 206: goto L_0x00cb;
                case 207: goto L_0x0089;
                case 208: goto L_0x0193;
                case 209: goto L_0x0089;
                case 210: goto L_0x0089;
                case 211: goto L_0x0089;
                case 212: goto L_0x0297;
                case 213: goto L_0x0097;
                case 214: goto L_0x036f;
                case 215: goto L_0x0089;
                case 216: goto L_0x01e7;
                case 217: goto L_0x0089;
                case 218: goto L_0x00e3;
                case 219: goto L_0x013f;
                case 220: goto L_0x034b;
                case 221: goto L_0x0089;
                case 222: goto L_0x020b;
                case 223: goto L_0x0089;
                case 224: goto L_0x0089;
                case 225: goto L_0x0403;
                case 226: goto L_0x031b;
                case 227: goto L_0x0089;
                case 228: goto L_0x0387;
                case 229: goto L_0x0089;
                case 230: goto L_0x014b;
                case 231: goto L_0x035b;
                case 232: goto L_0x00af;
                case 233: goto L_0x0089;
                case 234: goto L_0x03d3;
                case 235: goto L_0x03cf;
                case 236: goto L_0x0089;
                case 237: goto L_0x0089;
                case 238: goto L_0x0153;
                case 239: goto L_0x0089;
                case 240: goto L_0x0383;
                case 241: goto L_0x0089;
                case 242: goto L_0x02df;
                case 243: goto L_0x0089;
                case 244: goto L_0x018f;
                case 245: goto L_0x0089;
                case 246: goto L_0x0257;
                case 247: goto L_0x023f;
                case 248: goto L_0x017b;
                case 249: goto L_0x0089;
                case 250: goto L_0x031f;
                case 251: goto L_0x0089;
                case 252: goto L_0x0089;
                case 253: goto L_0x0089;
                case 254: goto L_0x0089;
                case 255: goto L_0x03bf;
                case 256: goto L_0x0089;
                case 257: goto L_0x00c7;
                case 258: goto L_0x0089;
                case 259: goto L_0x0293;
                case 260: goto L_0x0307;
                case 261: goto L_0x0089;
                case 262: goto L_0x01ab;
                case 263: goto L_0x0089;
                case 264: goto L_0x0089;
                case 265: goto L_0x0089;
                case 266: goto L_0x01b3;
                case 267: goto L_0x0089;
                case 268: goto L_0x030b;
                case 269: goto L_0x0089;
                case 270: goto L_0x025b;
                case 271: goto L_0x0089;
                case 272: goto L_0x0203;
                case 273: goto L_0x0089;
                case 274: goto L_0x01eb;
                case 275: goto L_0x0089;
                case 276: goto L_0x008e;
                case 277: goto L_0x0089;
                case 278: goto L_0x027b;
                case 279: goto L_0x0089;
                case 280: goto L_0x0147;
                case 281: goto L_0x0089;
                case 282: goto L_0x01a7;
                case 283: goto L_0x00a6;
                case 284: goto L_0x00f7;
                case 285: goto L_0x0089;
                case 286: goto L_0x03af;
                case 287: goto L_0x0089;
                case 288: goto L_0x0187;
                case 289: goto L_0x0089;
                case 290: goto L_0x01bb;
                case 291: goto L_0x0089;
                case 292: goto L_0x033b;
                case 293: goto L_0x035f;
                case 294: goto L_0x0263;
                case 295: goto L_0x0253;
                case 296: goto L_0x0089;
                case 297: goto L_0x029f;
                case 298: goto L_0x0089;
                case 299: goto L_0x0089;
                case 300: goto L_0x0089;
                case 301: goto L_0x0089;
                case 302: goto L_0x010b;
                case 303: goto L_0x0089;
                case 304: goto L_0x0089;
                case 305: goto L_0x0089;
                case 306: goto L_0x0089;
                case 307: goto L_0x0089;
                case 308: goto L_0x032f;
                case 309: goto L_0x0089;
                case 310: goto L_0x03d7;
                case 311: goto L_0x03db;
                case 312: goto L_0x03df;
                case 313: goto L_0x03e3;
                case 314: goto L_0x03e7;
                case 315: goto L_0x03eb;
                case 316: goto L_0x03ef;
                case 317: goto L_0x0089;
                case 318: goto L_0x0089;
                case 319: goto L_0x0089;
                case 320: goto L_0x0089;
                case 321: goto L_0x0089;
                case 322: goto L_0x0089;
                case 323: goto L_0x0089;
                case 324: goto L_0x0089;
                case 325: goto L_0x0089;
                case 326: goto L_0x0089;
                case 327: goto L_0x0089;
                case 328: goto L_0x0089;
                case 329: goto L_0x0089;
                case 330: goto L_0x030f;
                case 331: goto L_0x0089;
                case 332: goto L_0x03f3;
                case 333: goto L_0x0089;
                case 334: goto L_0x028b;
                case 335: goto L_0x0089;
                case 336: goto L_0x0089;
                case 337: goto L_0x0089;
                case 338: goto L_0x020f;
                case 339: goto L_0x0089;
                case 340: goto L_0x01c3;
                case 341: goto L_0x0089;
                case 342: goto L_0x00c3;
                case 343: goto L_0x0089;
                case 344: goto L_0x00a0;
                case 345: goto L_0x0089;
                case 346: goto L_0x0113;
                case 347: goto L_0x0089;
                case 348: goto L_0x00ef;
                case 349: goto L_0x0089;
                case 350: goto L_0x00d7;
                case 351: goto L_0x0089;
                case 352: goto L_0x01bf;
                case 353: goto L_0x0089;
                case 354: goto L_0x02a3;
                case 355: goto L_0x0089;
                case 356: goto L_0x0327;
                case 357: goto L_0x0089;
                case 358: goto L_0x032b;
                case 359: goto L_0x0089;
                case 360: goto L_0x0333;
                case 361: goto L_0x0089;
                case 362: goto L_0x02c3;
                case 363: goto L_0x00a9;
                case 364: goto L_0x00b7;
                case 365: goto L_0x009d;
                case 366: goto L_0x015b;
                case 367: goto L_0x0089;
                case 368: goto L_0x0143;
                case 369: goto L_0x0089;
                case 370: goto L_0x015f;
                case 371: goto L_0x0089;
                case 372: goto L_0x01db;
                case 373: goto L_0x0089;
                case 374: goto L_0x03a7;
                case 375: goto L_0x0089;
                case 376: goto L_0x03b7;
                case 377: goto L_0x0089;
                case 378: goto L_0x0089;
                case 379: goto L_0x0089;
                case 380: goto L_0x0089;
                case 381: goto L_0x0089;
                case 382: goto L_0x0089;
                case 383: goto L_0x0089;
                case 384: goto L_0x0089;
                case 385: goto L_0x0089;
                case 386: goto L_0x0089;
                case 387: goto L_0x0089;
                case 388: goto L_0x0089;
                case 389: goto L_0x0089;
                case 390: goto L_0x0089;
                case 391: goto L_0x0089;
                case 392: goto L_0x0089;
                case 393: goto L_0x0089;
                case 394: goto L_0x0089;
                case 395: goto L_0x0089;
                case 396: goto L_0x0089;
                case 397: goto L_0x0089;
                case 398: goto L_0x0089;
                case 399: goto L_0x0089;
                case 400: goto L_0x00b3;
                case 401: goto L_0x021f;
                case 402: goto L_0x00db;
                case 403: goto L_0x0089;
                case 404: goto L_0x01ef;
                case 405: goto L_0x01f3;
                case 406: goto L_0x0089;
                case 407: goto L_0x0089;
                case 408: goto L_0x0089;
                case 409: goto L_0x0089;
                case 410: goto L_0x02e7;
                case 411: goto L_0x0089;
                case 412: goto L_0x008b;
                case 413: goto L_0x0373;
                case 414: goto L_0x02af;
                case 415: goto L_0x0243;
                case 416: goto L_0x021b;
                case 417: goto L_0x038b;
                case 418: goto L_0x01ff;
                case 419: goto L_0x0233;
                case 420: goto L_0x0343;
                case 421: goto L_0x0413;
                case 422: goto L_0x02e3;
                case 423: goto L_0x02ef;
                case 424: goto L_0x03c3;
                case 425: goto L_0x0207;
                case 426: goto L_0x00bb;
                case 427: goto L_0x0313;
                case 428: goto L_0x029b;
                case 429: goto L_0x02bb;
                case 430: goto L_0x03c7;
                case 431: goto L_0x03cb;
                case 432: goto L_0x01fb;
                case 433: goto L_0x0089;
                case 434: goto L_0x03fb;
                case 435: goto L_0x0089;
                case 436: goto L_0x0393;
                case 437: goto L_0x0237;
                case 438: goto L_0x03b3;
                case 439: goto L_0x0089;
                case 440: goto L_0x0217;
                case 441: goto L_0x0213;
                case 442: goto L_0x0089;
                case 443: goto L_0x0089;
                case 444: goto L_0x0089;
                case 445: goto L_0x0089;
                case 446: goto L_0x0089;
                case 447: goto L_0x0089;
                case 448: goto L_0x0089;
                case 449: goto L_0x0089;
                case 450: goto L_0x022f;
                case 451: goto L_0x0089;
                case 452: goto L_0x040b;
                case 453: goto L_0x0089;
                case 454: goto L_0x01e3;
                case 455: goto L_0x025f;
                case 456: goto L_0x0103;
                case 457: goto L_0x023b;
                case 458: goto L_0x0089;
                case 459: goto L_0x0089;
                case 460: goto L_0x0123;
                case 461: goto L_0x0089;
                case 462: goto L_0x0089;
                case 463: goto L_0x0089;
                case 464: goto L_0x0089;
                case 465: goto L_0x0089;
                case 466: goto L_0x038f;
                case 467: goto L_0x022b;
                case 468: goto L_0x0089;
                case 469: goto L_0x0089;
                case 470: goto L_0x00bf;
                case 471: goto L_0x0089;
                case 472: goto L_0x0273;
                case 473: goto L_0x0089;
                case 474: goto L_0x0089;
                case 475: goto L_0x0089;
                case 476: goto L_0x0089;
                case 477: goto L_0x0089;
                case 478: goto L_0x0089;
                case 479: goto L_0x0089;
                case 480: goto L_0x0089;
                case 481: goto L_0x0089;
                case 482: goto L_0x0089;
                case 483: goto L_0x0089;
                case 484: goto L_0x0089;
                case 485: goto L_0x0089;
                case 486: goto L_0x0089;
                case 487: goto L_0x0089;
                case 488: goto L_0x0089;
                case 489: goto L_0x0089;
                case 490: goto L_0x0089;
                case 491: goto L_0x0089;
                case 492: goto L_0x0089;
                case 493: goto L_0x0089;
                case 494: goto L_0x0089;
                case 495: goto L_0x0089;
                case 496: goto L_0x0089;
                case 497: goto L_0x0089;
                case 498: goto L_0x0089;
                case 499: goto L_0x0089;
                case 500: goto L_0x0089;
                case 501: goto L_0x0089;
                case 502: goto L_0x026f;
                case 503: goto L_0x0089;
                case 504: goto L_0x0089;
                case 505: goto L_0x00ac;
                case 506: goto L_0x0089;
                case 507: goto L_0x0089;
                case 508: goto L_0x0089;
                case 509: goto L_0x0089;
                case 510: goto L_0x01f7;
                case 511: goto L_0x0089;
                case 512: goto L_0x0089;
                case 513: goto L_0x0089;
                case 514: goto L_0x0163;
                case 515: goto L_0x0303;
                case 516: goto L_0x0089;
                case 517: goto L_0x0089;
                case 518: goto L_0x0089;
                case 519: goto L_0x0089;
                case 520: goto L_0x039b;
                case 521: goto L_0x0089;
                case 522: goto L_0x0089;
                case 523: goto L_0x0089;
                case 524: goto L_0x0089;
                case 525: goto L_0x0357;
                case 526: goto L_0x0089;
                case 527: goto L_0x0089;
                case 528: goto L_0x00f3;
                case 529: goto L_0x0089;
                case 530: goto L_0x02cb;
                case 531: goto L_0x0089;
                case 532: goto L_0x0089;
                case 533: goto L_0x0089;
                case 534: goto L_0x02db;
                case 535: goto L_0x01c7;
                case 536: goto L_0x02b7;
                case 537: goto L_0x02f7;
                case 538: goto L_0x0089;
                case 539: goto L_0x03a3;
                case 540: goto L_0x0363;
                case 541: goto L_0x03ff;
                case 542: goto L_0x018b;
                case 543: goto L_0x040f;
                case 544: goto L_0x0094;
                case 545: goto L_0x0227;
                case 546: goto L_0x02c7;
                case 547: goto L_0x019b;
                case 548: goto L_0x0133;
                case 549: goto L_0x0337;
                case 550: goto L_0x028f;
                case 551: goto L_0x027f;
                case 552: goto L_0x02eb;
                case 553: goto L_0x0089;
                case 554: goto L_0x0089;
                case 555: goto L_0x0089;
                case 556: goto L_0x0089;
                case 557: goto L_0x0089;
                case 558: goto L_0x0089;
                case 559: goto L_0x0089;
                case 560: goto L_0x0089;
                case 561: goto L_0x0089;
                case 562: goto L_0x0089;
                case 563: goto L_0x0089;
                case 564: goto L_0x0089;
                case 565: goto L_0x0089;
                case 566: goto L_0x0089;
                case 567: goto L_0x0089;
                case 568: goto L_0x0089;
                case 569: goto L_0x0089;
                case 570: goto L_0x0089;
                case 571: goto L_0x0089;
                case 572: goto L_0x0089;
                case 573: goto L_0x0089;
                case 574: goto L_0x0089;
                case 575: goto L_0x0089;
                case 576: goto L_0x0089;
                case 577: goto L_0x0089;
                case 578: goto L_0x0089;
                case 579: goto L_0x0089;
                case 580: goto L_0x0089;
                case 581: goto L_0x0089;
                case 582: goto L_0x0089;
                case 583: goto L_0x0089;
                case 584: goto L_0x0089;
                case 585: goto L_0x0089;
                case 586: goto L_0x0089;
                case 587: goto L_0x0089;
                case 588: goto L_0x0089;
                case 589: goto L_0x0089;
                case 590: goto L_0x0089;
                case 591: goto L_0x0089;
                case 592: goto L_0x0089;
                case 593: goto L_0x0089;
                case 594: goto L_0x0089;
                case 595: goto L_0x0089;
                case 596: goto L_0x0089;
                case 597: goto L_0x0089;
                case 598: goto L_0x0089;
                case 599: goto L_0x0089;
                case 600: goto L_0x0089;
                case 601: goto L_0x0089;
                case 602: goto L_0x016b;
                case 603: goto L_0x0091;
                case 604: goto L_0x02a7;
                case 605: goto L_0x03ab;
                case 606: goto L_0x024f;
                case 607: goto L_0x01a3;
                case 608: goto L_0x0347;
                case 609: goto L_0x0283;
                case 610: goto L_0x0277;
                case 611: goto L_0x01cf;
                case 612: goto L_0x013b;
                case 613: goto L_0x00fb;
                case 614: goto L_0x02d3;
                case 615: goto L_0x039f;
                case 616: goto L_0x00d3;
                case 617: goto L_0x0287;
                case 618: goto L_0x024b;
                case 619: goto L_0x0353;
                case 620: goto L_0x01af;
                case 621: goto L_0x02d7;
                case 622: goto L_0x011b;
                case 623: goto L_0x0117;
                case 624: goto L_0x0107;
                case 625: goto L_0x010f;
                case 626: goto L_0x033f;
                case 627: goto L_0x0173;
                case 628: goto L_0x019f;
                case 629: goto L_0x012f;
                case 630: goto L_0x014f;
                case 631: goto L_0x009a;
                case 632: goto L_0x01d3;
                case 633: goto L_0x034f;
                case 634: goto L_0x0377;
                case 635: goto L_0x0323;
                case 636: goto L_0x017f;
                case 637: goto L_0x0367;
                case 638: goto L_0x0157;
                case 639: goto L_0x0223;
                case 640: goto L_0x0397;
                case 641: goto L_0x03bb;
                case 642: goto L_0x00ff;
                case 643: goto L_0x02ab;
                case 644: goto L_0x0089;
                case 645: goto L_0x0417;
                case 646: goto L_0x0267;
                case 647: goto L_0x0317;
                case 648: goto L_0x041b;
                case 649: goto L_0x02b3;
                case 650: goto L_0x026b;
                case 651: goto L_0x0247;
                case 652: goto L_0x00e7;
                case 653: goto L_0x037f;
                case 654: goto L_0x012b;
                case 655: goto L_0x036b;
                case 656: goto L_0x0089;
                case 657: goto L_0x0177;
                case 658: goto L_0x0089;
                case 659: goto L_0x0089;
                case 660: goto L_0x0089;
                case 661: goto L_0x0089;
                case 662: goto L_0x0089;
                case 663: goto L_0x0089;
                case 664: goto L_0x0089;
                case 665: goto L_0x0089;
                case 666: goto L_0x0089;
                case 667: goto L_0x0089;
                case 668: goto L_0x0089;
                case 669: goto L_0x0089;
                case 670: goto L_0x0089;
                case 671: goto L_0x0089;
                case 672: goto L_0x0089;
                case 673: goto L_0x0089;
                case 674: goto L_0x0089;
                case 675: goto L_0x0089;
                case 676: goto L_0x0089;
                case 677: goto L_0x0089;
                case 678: goto L_0x0089;
                case 679: goto L_0x0089;
                case 680: goto L_0x0089;
                case 681: goto L_0x0089;
                case 682: goto L_0x0089;
                case 683: goto L_0x0089;
                case 684: goto L_0x0089;
                case 685: goto L_0x0089;
                case 686: goto L_0x0089;
                case 687: goto L_0x0089;
                case 688: goto L_0x0089;
                case 689: goto L_0x0089;
                case 690: goto L_0x0089;
                case 691: goto L_0x0089;
                case 692: goto L_0x0089;
                case 693: goto L_0x0089;
                case 694: goto L_0x0089;
                case 695: goto L_0x0089;
                case 696: goto L_0x0089;
                case 697: goto L_0x0089;
                case 698: goto L_0x0089;
                case 699: goto L_0x0089;
                case 700: goto L_0x0089;
                case 701: goto L_0x0089;
                case 702: goto L_0x00cf;
                case 703: goto L_0x0089;
                case 704: goto L_0x01cb;
                case 705: goto L_0x0089;
                case 706: goto L_0x016f;
                case 707: goto L_0x0089;
                case 708: goto L_0x01df;
                case 709: goto L_0x0089;
                case 710: goto L_0x02cf;
                case 711: goto L_0x0089;
                case 712: goto L_0x0137;
                case 713: goto L_0x0089;
                case 714: goto L_0x02f3;
                case 715: goto L_0x0089;
                case 716: goto L_0x02ff;
                case 717: goto L_0x0089;
                case 718: goto L_0x0089;
                case 719: goto L_0x0089;
                case 720: goto L_0x0089;
                case 721: goto L_0x0089;
                case 722: goto L_0x00a3;
                case 723: goto L_0x0089;
                case 724: goto L_0x00eb;
                case 725: goto L_0x0089;
                case 726: goto L_0x0089;
                case 727: goto L_0x0089;
                case 728: goto L_0x0089;
                case 729: goto L_0x0089;
                case 730: goto L_0x011f;
                case 731: goto L_0x0089;
                case 732: goto L_0x0127;
                case 733: goto L_0x0089;
                case 734: goto L_0x0407;
                case 735: goto L_0x0089;
                case 736: goto L_0x00df;
                case 737: goto L_0x0089;
                case 738: goto L_0x01d7;
                case 739: goto L_0x0089;
                case 740: goto L_0x0167;
                case 741: goto L_0x0089;
                case 742: goto L_0x0197;
                case 743: goto L_0x0089;
                case 744: goto L_0x02fb;
                case 745: goto L_0x0089;
                case 746: goto L_0x037b;
                case 747: goto L_0x0089;
                case 748: goto L_0x03f7;
                case 749: goto L_0x0089;
                case 750: goto L_0x0183;
                default: goto L_0x0089;
            }
        L_0x0089:
            r3 = r2
            goto L_0x002f
        L_0x008b:
            java.lang.String r3 = "AF"
            goto L_0x002f
        L_0x008e:
            java.lang.String r3 = "AL"
            goto L_0x002f
        L_0x0091:
            java.lang.String r3 = "DZ"
            goto L_0x002f
        L_0x0094:
            java.lang.String r3 = "AS"
            goto L_0x002f
        L_0x0097:
            java.lang.String r3 = "AD"
            goto L_0x002f
        L_0x009a:
            java.lang.String r3 = "AO"
            goto L_0x002f
        L_0x009d:
            java.lang.String r3 = "AI"
            goto L_0x002f
        L_0x00a0:
            java.lang.String r3 = "AG"
            goto L_0x002f
        L_0x00a3:
            java.lang.String r3 = "AR"
            goto L_0x002f
        L_0x00a6:
            java.lang.String r3 = "AM"
            goto L_0x002f
        L_0x00a9:
            java.lang.String r3 = "AW"
            goto L_0x002f
        L_0x00ac:
            java.lang.String r3 = "AU"
            goto L_0x002f
        L_0x00af:
            java.lang.String r3 = "AT"
            goto L_0x002f
        L_0x00b3:
            java.lang.String r3 = "AZ"
            goto L_0x002f
        L_0x00b7:
            java.lang.String r3 = "BS"
            goto L_0x002f
        L_0x00bb:
            java.lang.String r3 = "BH"
            goto L_0x002f
        L_0x00bf:
            java.lang.String r3 = "BD"
            goto L_0x002f
        L_0x00c3:
            java.lang.String r3 = "BB"
            goto L_0x002f
        L_0x00c7:
            java.lang.String r3 = "BY"
            goto L_0x002f
        L_0x00cb:
            java.lang.String r3 = "BE"
            goto L_0x002f
        L_0x00cf:
            java.lang.String r3 = "BZ"
            goto L_0x002f
        L_0x00d3:
            java.lang.String r3 = "BJ"
            goto L_0x002f
        L_0x00d7:
            java.lang.String r3 = "BM"
            goto L_0x002f
        L_0x00db:
            java.lang.String r3 = "BT"
            goto L_0x002f
        L_0x00df:
            java.lang.String r3 = "BO"
            goto L_0x002f
        L_0x00e3:
            java.lang.String r3 = "BA"
            goto L_0x002f
        L_0x00e7:
            java.lang.String r3 = "BW"
            goto L_0x002f
        L_0x00eb:
            java.lang.String r3 = "BR"
            goto L_0x002f
        L_0x00ef:
            java.lang.String r3 = "VG"
            goto L_0x002f
        L_0x00f3:
            java.lang.String r3 = "BN"
            goto L_0x002f
        L_0x00f7:
            java.lang.String r3 = "BG"
            goto L_0x002f
        L_0x00fb:
            java.lang.String r3 = "BF"
            goto L_0x002f
        L_0x00ff:
            java.lang.String r3 = "BI"
            goto L_0x002f
        L_0x0103:
            java.lang.String r3 = "KH"
            goto L_0x002f
        L_0x0107:
            java.lang.String r3 = "CM"
            goto L_0x002f
        L_0x010b:
            java.lang.String r3 = "CA"
            goto L_0x002f
        L_0x010f:
            java.lang.String r3 = "CV"
            goto L_0x002f
        L_0x0113:
            java.lang.String r3 = "KY"
            goto L_0x002f
        L_0x0117:
            java.lang.String r3 = "CF"
            goto L_0x002f
        L_0x011b:
            java.lang.String r3 = "TD"
            goto L_0x002f
        L_0x011f:
            java.lang.String r3 = "CL"
            goto L_0x002f
        L_0x0123:
            java.lang.String r3 = "CN"
            goto L_0x002f
        L_0x0127:
            java.lang.String r3 = "CO"
            goto L_0x002f
        L_0x012b:
            java.lang.String r3 = "KM"
            goto L_0x002f
        L_0x012f:
            java.lang.String r3 = "CG"
            goto L_0x002f
        L_0x0133:
            java.lang.String r3 = "CK"
            goto L_0x002f
        L_0x0137:
            java.lang.String r3 = "CR"
            goto L_0x002f
        L_0x013b:
            java.lang.String r3 = "CI"
            goto L_0x002f
        L_0x013f:
            java.lang.String r3 = "HR"
            goto L_0x002f
        L_0x0143:
            java.lang.String r3 = "CU"
            goto L_0x002f
        L_0x0147:
            java.lang.String r3 = "CY"
            goto L_0x002f
        L_0x014b:
            java.lang.String r3 = "CZ"
            goto L_0x002f
        L_0x014f:
            java.lang.String r3 = "CD"
            goto L_0x002f
        L_0x0153:
            java.lang.String r3 = "DK"
            goto L_0x002f
        L_0x0157:
            java.lang.String r3 = "DJ"
            goto L_0x002f
        L_0x015b:
            java.lang.String r3 = "DM"
            goto L_0x002f
        L_0x015f:
            java.lang.String r3 = "DO"
            goto L_0x002f
        L_0x0163:
            java.lang.String r3 = "TL"
            goto L_0x002f
        L_0x0167:
            java.lang.String r3 = "EC"
            goto L_0x002f
        L_0x016b:
            java.lang.String r3 = "EG"
            goto L_0x002f
        L_0x016f:
            java.lang.String r3 = "SV"
            goto L_0x002f
        L_0x0173:
            java.lang.String r3 = "GQ"
            goto L_0x002f
        L_0x0177:
            java.lang.String r3 = "ER"
            goto L_0x002f
        L_0x017b:
            java.lang.String r3 = "EE"
            goto L_0x002f
        L_0x017f:
            java.lang.String r3 = "ET"
            goto L_0x002f
        L_0x0183:
            java.lang.String r3 = "FK"
            goto L_0x002f
        L_0x0187:
            java.lang.String r3 = "FO"
            goto L_0x002f
        L_0x018b:
            java.lang.String r3 = "FJ"
            goto L_0x002f
        L_0x018f:
            java.lang.String r3 = "FI"
            goto L_0x002f
        L_0x0193:
            java.lang.String r3 = "FR"
            goto L_0x002f
        L_0x0197:
            java.lang.String r3 = "GF"
            goto L_0x002f
        L_0x019b:
            java.lang.String r3 = "PF"
            goto L_0x002f
        L_0x019f:
            java.lang.String r3 = "GA"
            goto L_0x002f
        L_0x01a3:
            java.lang.String r3 = "GM"
            goto L_0x002f
        L_0x01a7:
            java.lang.String r3 = "GE"
            goto L_0x002f
        L_0x01ab:
            java.lang.String r3 = "DE"
            goto L_0x002f
        L_0x01af:
            java.lang.String r3 = "GH"
            goto L_0x002f
        L_0x01b3:
            java.lang.String r3 = "GI"
            goto L_0x002f
        L_0x01b7:
            java.lang.String r3 = "GR"
            goto L_0x002f
        L_0x01bb:
            java.lang.String r3 = "GL"
            goto L_0x002f
        L_0x01bf:
            java.lang.String r3 = "GD"
            goto L_0x002f
        L_0x01c3:
            java.lang.String r3 = "GP"
            goto L_0x002f
        L_0x01c7:
            java.lang.String r3 = "GU"
            goto L_0x002f
        L_0x01cb:
            java.lang.String r3 = "GT"
            goto L_0x002f
        L_0x01cf:
            java.lang.String r3 = "GN"
            goto L_0x002f
        L_0x01d3:
            java.lang.String r3 = "GW"
            goto L_0x002f
        L_0x01d7:
            java.lang.String r3 = "GY"
            goto L_0x002f
        L_0x01db:
            java.lang.String r3 = "HT"
            goto L_0x002f
        L_0x01df:
            java.lang.String r3 = "HN"
            goto L_0x002f
        L_0x01e3:
            java.lang.String r3 = "HK"
            goto L_0x002f
        L_0x01e7:
            java.lang.String r3 = "HU"
            goto L_0x002f
        L_0x01eb:
            java.lang.String r3 = "IS"
            goto L_0x002f
        L_0x01ef:
            java.lang.String r3 = "IN"
            goto L_0x002f
        L_0x01f3:
            java.lang.String r3 = "IN"
            goto L_0x002f
        L_0x01f7:
            java.lang.String r3 = "ID"
            goto L_0x002f
        L_0x01fb:
            java.lang.String r3 = "IR"
            goto L_0x002f
        L_0x01ff:
            java.lang.String r3 = "IQ"
            goto L_0x002f
        L_0x0203:
            java.lang.String r3 = "IE"
            goto L_0x002f
        L_0x0207:
            java.lang.String r3 = "IL"
            goto L_0x002f
        L_0x020b:
            java.lang.String r3 = "IT"
            goto L_0x002f
        L_0x020f:
            java.lang.String r3 = "JM"
            goto L_0x002f
        L_0x0213:
            java.lang.String r3 = "JP"
            goto L_0x002f
        L_0x0217:
            java.lang.String r3 = "JP"
            goto L_0x002f
        L_0x021b:
            java.lang.String r3 = "JO"
            goto L_0x002f
        L_0x021f:
            java.lang.String r3 = "KZ"
            goto L_0x002f
        L_0x0223:
            java.lang.String r3 = "KE"
            goto L_0x002f
        L_0x0227:
            java.lang.String r3 = "KI"
            goto L_0x002f
        L_0x022b:
            java.lang.String r3 = "KP"
            goto L_0x002f
        L_0x022f:
            java.lang.String r3 = "KR"
            goto L_0x002f
        L_0x0233:
            java.lang.String r3 = "KW"
            goto L_0x002f
        L_0x0237:
            java.lang.String r3 = "KG"
            goto L_0x002f
        L_0x023b:
            java.lang.String r3 = "LA"
            goto L_0x002f
        L_0x023f:
            java.lang.String r3 = "LV"
            goto L_0x002f
        L_0x0243:
            java.lang.String r3 = "LB"
            goto L_0x002f
        L_0x0247:
            java.lang.String r3 = "LS"
            goto L_0x002f
        L_0x024b:
            java.lang.String r3 = "LR"
            goto L_0x002f
        L_0x024f:
            java.lang.String r3 = "LY"
            goto L_0x002f
        L_0x0253:
            java.lang.String r3 = "LI"
            goto L_0x002f
        L_0x0257:
            java.lang.String r3 = "LT"
            goto L_0x002f
        L_0x025b:
            java.lang.String r3 = "LU"
            goto L_0x002f
        L_0x025f:
            java.lang.String r3 = "MO"
            goto L_0x002f
        L_0x0263:
            java.lang.String r3 = "MK"
            goto L_0x002f
        L_0x0267:
            java.lang.String r3 = "MG"
            goto L_0x002f
        L_0x026b:
            java.lang.String r3 = "MW"
            goto L_0x002f
        L_0x026f:
            java.lang.String r3 = "MY"
            goto L_0x002f
        L_0x0273:
            java.lang.String r3 = "MV"
            goto L_0x002f
        L_0x0277:
            java.lang.String r3 = "ML"
            goto L_0x002f
        L_0x027b:
            java.lang.String r3 = "MT"
            goto L_0x002f
        L_0x027f:
            java.lang.String r3 = "MH"
            goto L_0x002f
        L_0x0283:
            java.lang.String r3 = "MR"
            goto L_0x002f
        L_0x0287:
            java.lang.String r3 = "MU"
            goto L_0x002f
        L_0x028b:
            java.lang.String r3 = "MX"
            goto L_0x002f
        L_0x028f:
            java.lang.String r3 = "FM"
            goto L_0x002f
        L_0x0293:
            java.lang.String r3 = "MD"
            goto L_0x002f
        L_0x0297:
            java.lang.String r3 = "MC"
            goto L_0x002f
        L_0x029b:
            java.lang.String r3 = "MN"
            goto L_0x002f
        L_0x029f:
            java.lang.String r3 = "ME"
            goto L_0x002f
        L_0x02a3:
            java.lang.String r3 = "MS"
            goto L_0x002f
        L_0x02a7:
            java.lang.String r3 = "MA"
            goto L_0x002f
        L_0x02ab:
            java.lang.String r3 = "MZ"
            goto L_0x002f
        L_0x02af:
            java.lang.String r3 = "MM"
            goto L_0x002f
        L_0x02b3:
            java.lang.String r3 = "NA"
            goto L_0x002f
        L_0x02b7:
            java.lang.String r3 = "NR"
            goto L_0x002f
        L_0x02bb:
            java.lang.String r3 = "NP"
            goto L_0x002f
        L_0x02bf:
            java.lang.String r3 = "NL"
            goto L_0x002f
        L_0x02c3:
            java.lang.String r3 = "AN"
            goto L_0x002f
        L_0x02c7:
            java.lang.String r3 = "NC"
            goto L_0x002f
        L_0x02cb:
            java.lang.String r3 = "NZ"
            goto L_0x002f
        L_0x02cf:
            java.lang.String r3 = "NI"
            goto L_0x002f
        L_0x02d3:
            java.lang.String r3 = "NE"
            goto L_0x002f
        L_0x02d7:
            java.lang.String r3 = "NG"
            goto L_0x002f
        L_0x02db:
            java.lang.String r3 = "MP"
            goto L_0x002f
        L_0x02df:
            java.lang.String r3 = "NO"
            goto L_0x002f
        L_0x02e3:
            java.lang.String r3 = "OM"
            goto L_0x002f
        L_0x02e7:
            java.lang.String r3 = "PK"
            goto L_0x002f
        L_0x02eb:
            java.lang.String r3 = "PW"
            goto L_0x002f
        L_0x02ef:
            java.lang.String r3 = "PS"
            goto L_0x002f
        L_0x02f3:
            java.lang.String r3 = "PA"
            goto L_0x002f
        L_0x02f7:
            java.lang.String r3 = "PG"
            goto L_0x002f
        L_0x02fb:
            java.lang.String r3 = "PY"
            goto L_0x002f
        L_0x02ff:
            java.lang.String r3 = "PE"
            goto L_0x002f
        L_0x0303:
            java.lang.String r3 = "PH"
            goto L_0x002f
        L_0x0307:
            java.lang.String r3 = "PL"
            goto L_0x002f
        L_0x030b:
            java.lang.String r3 = "PT"
            goto L_0x002f
        L_0x030f:
            java.lang.String r3 = "PR"
            goto L_0x002f
        L_0x0313:
            java.lang.String r3 = "QA"
            goto L_0x002f
        L_0x0317:
            java.lang.String r3 = "RE"
            goto L_0x002f
        L_0x031b:
            java.lang.String r3 = "RO"
            goto L_0x002f
        L_0x031f:
            java.lang.String r3 = "RU"
            goto L_0x002f
        L_0x0323:
            java.lang.String r3 = "RW"
            goto L_0x002f
        L_0x0327:
            java.lang.String r3 = "KN"
            goto L_0x002f
        L_0x032b:
            java.lang.String r3 = "LC"
            goto L_0x002f
        L_0x032f:
            java.lang.String r3 = "PM"
            goto L_0x002f
        L_0x0333:
            java.lang.String r3 = "VC"
            goto L_0x002f
        L_0x0337:
            java.lang.String r3 = "WS"
            goto L_0x002f
        L_0x033b:
            java.lang.String r3 = "SM"
            goto L_0x002f
        L_0x033f:
            java.lang.String r3 = "ST"
            goto L_0x002f
        L_0x0343:
            java.lang.String r3 = "SA"
            goto L_0x002f
        L_0x0347:
            java.lang.String r3 = "SN"
            goto L_0x002f
        L_0x034b:
            java.lang.String r3 = "RS"
            goto L_0x002f
        L_0x034f:
            java.lang.String r3 = "SC"
            goto L_0x002f
        L_0x0353:
            java.lang.String r3 = "SL"
            goto L_0x002f
        L_0x0357:
            java.lang.String r3 = "SG"
            goto L_0x002f
        L_0x035b:
            java.lang.String r3 = "SK"
            goto L_0x002f
        L_0x035f:
            java.lang.String r3 = "SI"
            goto L_0x002f
        L_0x0363:
            java.lang.String r3 = "SB"
            goto L_0x002f
        L_0x0367:
            java.lang.String r3 = "SO"
            goto L_0x002f
        L_0x036b:
            java.lang.String r3 = "ZA"
            goto L_0x002f
        L_0x036f:
            java.lang.String r3 = "ES"
            goto L_0x002f
        L_0x0373:
            java.lang.String r3 = "LK"
            goto L_0x002f
        L_0x0377:
            java.lang.String r3 = "SD"
            goto L_0x002f
        L_0x037b:
            java.lang.String r3 = "SR"
            goto L_0x002f
        L_0x037f:
            java.lang.String r3 = "SZ"
            goto L_0x002f
        L_0x0383:
            java.lang.String r3 = "SE"
            goto L_0x002f
        L_0x0387:
            java.lang.String r3 = "CH"
            goto L_0x002f
        L_0x038b:
            java.lang.String r3 = "SY"
            goto L_0x002f
        L_0x038f:
            java.lang.String r3 = "TW"
            goto L_0x002f
        L_0x0393:
            java.lang.String r3 = "TJ"
            goto L_0x002f
        L_0x0397:
            java.lang.String r3 = "TZ"
            goto L_0x002f
        L_0x039b:
            java.lang.String r3 = "TH"
            goto L_0x002f
        L_0x039f:
            java.lang.String r3 = "TG"
            goto L_0x002f
        L_0x03a3:
            java.lang.String r3 = "TO"
            goto L_0x002f
        L_0x03a7:
            java.lang.String r3 = "TT"
            goto L_0x002f
        L_0x03ab:
            java.lang.String r3 = "TN"
            goto L_0x002f
        L_0x03af:
            java.lang.String r3 = "TR"
            goto L_0x002f
        L_0x03b3:
            java.lang.String r3 = "TM"
            goto L_0x002f
        L_0x03b7:
            java.lang.String r3 = "TC"
            goto L_0x002f
        L_0x03bb:
            java.lang.String r3 = "UG"
            goto L_0x002f
        L_0x03bf:
            java.lang.String r3 = "UA"
            goto L_0x002f
        L_0x03c3:
            java.lang.String r3 = "AE"
            goto L_0x002f
        L_0x03c7:
            java.lang.String r3 = "AE"
            goto L_0x002f
        L_0x03cb:
            java.lang.String r3 = "AE"
            goto L_0x002f
        L_0x03cf:
            java.lang.String r3 = "GB"
            goto L_0x002f
        L_0x03d3:
            java.lang.String r3 = "GB"
            goto L_0x002f
        L_0x03d7:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03db:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03df:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03e3:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03e7:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03eb:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03ef:
            java.lang.String r3 = "US"
            goto L_0x002f
        L_0x03f3:
            java.lang.String r3 = "VI"
            goto L_0x002f
        L_0x03f7:
            java.lang.String r3 = "UY"
            goto L_0x002f
        L_0x03fb:
            java.lang.String r3 = "UZ"
            goto L_0x002f
        L_0x03ff:
            java.lang.String r3 = "VU"
            goto L_0x002f
        L_0x0403:
            java.lang.String r3 = "VA"
            goto L_0x002f
        L_0x0407:
            java.lang.String r3 = "VE"
            goto L_0x002f
        L_0x040b:
            java.lang.String r3 = "VN"
            goto L_0x002f
        L_0x040f:
            java.lang.String r3 = "WF"
            goto L_0x002f
        L_0x0413:
            java.lang.String r3 = "YE"
            goto L_0x002f
        L_0x0417:
            java.lang.String r3 = "ZM"
            goto L_0x002f
        L_0x041b:
            java.lang.String r3 = "ZW"
            goto L_0x002f
        L_0x041f:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r2
            goto L_0x0057
        L_0x0426:
            r1 = r2
            r3 = r2
            goto L_0x0057
        L_0x042a:
            r0 = r2
            r1 = r2
            goto L_0x001b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.c.a.a(android.content.Context, java.lang.String):java.lang.CharSequence");
    }

    public static String a(Context context, String[] strArr, String[] strArr2) {
        if (strArr.length <= 0) {
            return context.getString(R.string.label_event_mms_address_not_provided);
        }
        StringBuffer stringBuffer = new StringBuffer();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i >= strArr2.length) {
                return stringBuffer.toString();
            }
            if (i2 > 0) {
                stringBuffer.append(", ");
            }
            stringBuffer.append(b(strArr[i2], strArr2[i2]));
            i = i2 + 1;
        }
    }

    public static String a(String str, String str2) {
        return (str2 == null || str2.trim().length() == 0) ? str : new StringBuilder().insert(0, str2).append(" <").append(str).append(">").toString();
    }

    public static boolean a(i iVar) {
        return iVar.n() && iVar.q() && iVar.o() > 1.7d;
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static String b(String str, String str2) {
        return a(str2) ? str : str2;
    }
}
