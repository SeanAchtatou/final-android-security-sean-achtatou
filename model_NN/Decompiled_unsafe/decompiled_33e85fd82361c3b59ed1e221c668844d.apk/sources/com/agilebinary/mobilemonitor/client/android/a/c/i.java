package com.agilebinary.mobilemonitor.client.android.a.c;

import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.p;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.d;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public abstract class i extends k implements p {
    public abstract InputStream a(g gVar);

    public final List a(d dVar, List list, g gVar) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            ArrayList arrayList = new ArrayList();
            xMLReader.setContentHandler(new j(this, arrayList));
            xMLReader.parse(new InputSource(a(gVar)));
            return arrayList;
        } catch (Exception e) {
            throw new q(e);
        }
    }
}
