package com.agilebinary.mobilemonitor.client.android.ui;

import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;
import com.biige.client.android.R;

public class EventDetailsActivity_WEB extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f199a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;
    private TextView i;
    private TableRow k;
    private TableRow l;
    private TableRow m;
    private TableRow n;
    private TableRow o;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_web, viewGroup, true);
        this.f199a = (TextView) findViewById(R.id.eventdetails_web_kind);
        this.b = (TextView) findViewById(R.id.eventdetails_web_time);
        this.c = (TextView) findViewById(R.id.eventdetails_web_keywords);
        this.d = (TextView) findViewById(R.id.eventdetails_web_domain);
        this.e = (TextView) findViewById(R.id.eventdetails_web_title);
        this.f = (TextView) findViewById(R.id.eventdetails_web_numvisits);
        this.h = (TextView) findViewById(R.id.eventdetails_web_bookmarked);
        this.i = (TextView) findViewById(R.id.eventdetails_web_speed);
        this.k = (TableRow) findViewById(R.id.eventdetails_web_row_keywords);
        this.l = (TableRow) findViewById(R.id.eventdetails_web_row_address);
        this.m = (TableRow) findViewById(R.id.eventdetails_web_row_title);
        this.n = (TableRow) findViewById(R.id.eventdetails_web_row_numvisits);
        this.o = (TableRow) findViewById(R.id.eventdetails_web_row_bookmarked);
        this.d.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.agilebinary.mobilemonitor.client.a.b.o r7) {
        /*
            r6 = this;
            r2 = 8
            r3 = 0
            super.a(r7)
            boolean r0 = r7 instanceof com.agilebinary.mobilemonitor.client.a.b.e
            if (r0 == 0) goto L_0x006b
            android.widget.TextView r0 = r6.f199a
            r1 = 2131099871(0x7f0600df, float:1.7812107E38)
            r0.setText(r1)
            android.widget.TableRow r0 = r6.k
            r0.setVisibility(r3)
            android.widget.TableRow r0 = r6.l
            r0.setVisibility(r2)
            android.widget.TableRow r0 = r6.m
            r0.setVisibility(r2)
            android.widget.TableRow r0 = r6.n
            r0.setVisibility(r2)
            android.widget.TableRow r0 = r6.o
            r0.setVisibility(r2)
            r0 = r7
            com.agilebinary.mobilemonitor.client.a.b.e r0 = (com.agilebinary.mobilemonitor.client.a.b.e) r0
            android.widget.TextView r1 = r6.c
            java.lang.String r2 = r0.b()
            r1.setText(r2)
            android.widget.TextView r1 = r6.b
            com.agilebinary.mobilemonitor.client.android.c.c r2 = com.agilebinary.mobilemonitor.client.android.c.c.a()
            long r4 = r0.a()
            java.lang.String r0 = r2.c(r4)
            r1.setText(r0)
        L_0x0048:
            r0 = r7
        L_0x0049:
            com.agilebinary.mobilemonitor.client.a.b.i r0 = (com.agilebinary.mobilemonitor.client.a.b.i) r0
            android.widget.TextView r1 = r6.i
            java.lang.CharSequence r2 = com.agilebinary.mobilemonitor.client.android.c.a.a(r6, r0)
            r1.setText(r2)
            boolean r0 = com.agilebinary.mobilemonitor.client.android.c.a.a(r0)
            if (r0 == 0) goto L_0x0107
            android.widget.TextView r0 = r6.i
            android.content.res.Resources r1 = r6.getResources()
            r2 = 2131034124(0x7f05000c, float:1.7678757E38)
            int r1 = r1.getColor(r2)
            r0.setTextColor(r1)
        L_0x006a:
            return
        L_0x006b:
            boolean r0 = r7 instanceof com.agilebinary.mobilemonitor.client.a.b.g
            if (r0 == 0) goto L_0x0048
            android.widget.TextView r0 = r6.f199a
            r1 = 2131099872(0x7f0600e0, float:1.781211E38)
            r0.setText(r1)
            android.widget.TableRow r0 = r6.k
            r0.setVisibility(r2)
            android.widget.TableRow r0 = r6.l
            r0.setVisibility(r3)
            android.widget.TableRow r0 = r6.m
            r0.setVisibility(r3)
            android.widget.TableRow r0 = r6.n
            r0.setVisibility(r3)
            android.widget.TableRow r0 = r6.o
            r0.setVisibility(r3)
            r0 = r7
            com.agilebinary.mobilemonitor.client.a.b.g r0 = (com.agilebinary.mobilemonitor.client.a.b.g) r0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "<a href=\""
            java.lang.StringBuilder r1 = r1.insert(r3, r2)
            java.lang.String r2 = r0.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "\" >"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = r0.b()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = "</a>"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            android.text.Spanned r1 = android.text.Html.fromHtml(r1)
            android.widget.TextView r2 = r6.d
            r2.setText(r1)
            android.widget.TextView r1 = r6.e
            java.lang.String r2 = r0.c()
            r1.setText(r2)
            android.widget.TextView r1 = r6.f
            int r2 = r0.e()
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.setText(r2)
            android.widget.TextView r1 = r6.h
            boolean r2 = r0.d()
            if (r2 == 0) goto L_0x0103
            r2 = 2131099758(0x7f06006e, float:1.7811878E38)
        L_0x00e8:
            java.lang.String r2 = r6.getString(r2)
            r1.setText(r2)
            android.widget.TextView r1 = r6.b
            com.agilebinary.mobilemonitor.client.android.c.c r2 = com.agilebinary.mobilemonitor.client.android.c.c.a()
            long r4 = r0.a()
            java.lang.String r0 = r2.c(r4)
            r1.setText(r0)
            r0 = r7
            goto L_0x0049
        L_0x0103:
            r2 = 2131099759(0x7f06006f, float:1.781188E38)
            goto L_0x00e8
        L_0x0107:
            android.widget.TextView r0 = r6.i
            android.content.res.Resources r1 = r6.getResources()
            r2 = 2131034118(0x7f050006, float:1.7678745E38)
            int r1 = r1.getColor(r2)
            r0.setTextColor(r1)
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.EventDetailsActivity_WEB.a(com.agilebinary.mobilemonitor.client.a.b.o):void");
    }
}
