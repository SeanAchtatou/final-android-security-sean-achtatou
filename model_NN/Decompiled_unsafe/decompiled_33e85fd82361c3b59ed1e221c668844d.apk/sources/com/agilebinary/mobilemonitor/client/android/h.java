package com.agilebinary.mobilemonitor.client.android;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    public byte f187a;
    public long b;
    public long c;
    private boolean d;

    public h(byte b2, long j, long j2) {
        this.f187a = b2;
        this.b = j;
        this.c = j2;
        this.d = j2 == 0;
    }

    public final String toString() {
        return new StringBuilder().insert(0, "EventContentRequest [eventType=").append((int) this.f187a).append(", timeFrom=").append(this.b).append(", timeTo=").append(this.c).append(", onlyNewEvents=").append(this.d).append("]").toString();
    }
}
