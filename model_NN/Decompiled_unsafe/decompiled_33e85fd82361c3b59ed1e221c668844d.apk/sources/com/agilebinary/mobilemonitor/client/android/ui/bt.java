package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bt implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f258a;
    private /* synthetic */ MapActivity_GOOGLE b;

    bt(MapActivity_GOOGLE mapActivity_GOOGLE, Dialog dialog) {
        this.b = mapActivity_GOOGLE;
        this.f258a = dialog;
    }

    public final void onClick(View view) {
        boolean z;
        bt btVar;
        if (!this.b.i.a("MAP_SATELLITE_VIEW__BOOL", false)) {
            z = true;
            btVar = this;
        } else {
            z = false;
            btVar = this;
        }
        btVar.b.b.setSatellite(z);
        this.b.b.invalidate();
        this.b.i.b("MAP_SATELLITE_VIEW__BOOL", z);
        this.f258a.dismiss();
    }
}
