package com.agilebinary.mobilemonitor.client.android.a;

import com.agilebinary.a.a.a.c;
import com.agilebinary.a.a.a.j;
import com.agilebinary.a.a.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

public final class s implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f163a = b.a();
    private j b;
    private c c;
    private int d;
    private boolean e;
    private InputStream f;
    private int g;

    public s(j jVar) {
        s sVar;
        this.b = jVar;
        this.d = jVar.a().b();
        this.c = jVar.b();
        try {
            if (this.c.g()) {
                this.f = this.c.f();
            }
            sVar = this;
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
            sVar = this;
        } catch (IOException e3) {
            e3.printStackTrace();
            sVar = this;
        }
        String a2 = sVar.a("SI-S");
        this.g = -2;
        if (a2 != null) {
            try {
                this.g = Integer.parseInt(a2);
            } catch (NumberFormatException e4) {
                new StringBuilder().insert(0, "failed to parse header-value of ").append(a2).toString();
            }
        }
    }

    private /* synthetic */ String a(String str) {
        t[] b2 = this.b.b(str);
        if (b2.length == 0) {
            return null;
        }
        return b2[0].b();
    }

    public static boolean a(int i) {
        return i == 8 || i == 9 || i == 5 || i == 7 || i == 11;
    }

    public static boolean b(int i) {
        return i == -3;
    }

    public static boolean c(int i) {
        return i == 304;
    }

    public final boolean a() {
        if (this.e) {
            return false;
        }
        int i = this.g;
        if (this.d == 200) {
            return i == 0 || i == -2 || a("SI-S-T") != null;
        }
        return false;
    }

    public final int b() {
        return this.g;
    }

    public final boolean c() {
        return a(this.g);
    }

    public final InputStream d() {
        return this.f;
    }

    public final void e() {
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public final boolean f() {
        return b(this.g);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0070 A[Catch:{ all -> 0x0076 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x007b A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String g() {
        /*
            r7 = this;
            r1 = 0
            r6 = 0
            java.io.InputStream r0 = r7.f
            if (r0 != 0) goto L_0x0008
            r0 = r1
        L_0x0007:
            return r0
        L_0x0008:
            com.agilebinary.a.a.a.c r0 = r7.c
            long r2 = r0.c()
            r4 = 2147483647(0x7fffffff, double:1.060997895E-314)
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x001d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP entity too large to be buffered in memory"
            r0.<init>(r1)
            throw r0
        L_0x001d:
            com.agilebinary.a.a.a.c r0 = r7.c
            long r2 = r0.c()
            int r0 = (int) r2
            if (r0 >= 0) goto L_0x0028
            r0 = 4096(0x1000, float:5.74E-42)
        L_0x0028:
            com.agilebinary.a.a.a.c r2 = r7.c
            if (r2 != 0) goto L_0x0034
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP entity may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0034:
            com.agilebinary.a.a.a.t r3 = r2.d()
            if (r3 == 0) goto L_0x0083
            com.agilebinary.a.a.a.t r2 = r2.d()
            com.agilebinary.a.a.a.m[] r2 = r2.c()
            int r3 = r2.length
            if (r3 <= 0) goto L_0x0083
            r2 = r2[r6]
            java.lang.String r3 = "charset"
            com.agilebinary.a.a.a.o r2 = r2.a(r3)
            if (r2 == 0) goto L_0x0083
            java.lang.String r1 = r2.b()
            r2 = r1
        L_0x0054:
            if (r2 != 0) goto L_0x0058
            java.lang.String r1 = "ISO-8859-1"
        L_0x0058:
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            java.io.InputStream r3 = r7.f
            r2.<init>(r3, r1)
            com.agilebinary.a.a.a.i.c r1 = new com.agilebinary.a.a.a.i.c
            r1.<init>(r0)
            r0 = 1024(0x400, float:1.435E-42)
            char[] r3 = new char[r0]     // Catch:{ all -> 0x0076 }
            r0 = r2
        L_0x0069:
            int r0 = r0.read(r3)     // Catch:{ all -> 0x0076 }
            r4 = -1
            if (r0 == r4) goto L_0x007b
            r4 = 0
            r1.a(r3, r4, r0)     // Catch:{ all -> 0x0076 }
            r0 = r2
            goto L_0x0069
        L_0x0076:
            r0 = move-exception
            r2.close()
            throw r0
        L_0x007b:
            r2.close()
            java.lang.String r0 = r1.toString()
            goto L_0x0007
        L_0x0083:
            r2 = r1
            goto L_0x0054
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.s.g():java.lang.String");
    }
}
