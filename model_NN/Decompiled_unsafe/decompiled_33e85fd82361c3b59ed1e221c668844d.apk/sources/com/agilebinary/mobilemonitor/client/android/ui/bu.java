package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class bu implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private /* synthetic */ Dialog f259a;
    private /* synthetic */ MapActivity_GOOGLE b;

    bu(MapActivity_GOOGLE mapActivity_GOOGLE, Dialog dialog) {
        this.b = mapActivity_GOOGLE;
        this.f259a = dialog;
    }

    public final void onClick(View view) {
        this.b.f.a(!this.b.f.a());
        this.b.b.invalidate();
        this.b.i.b("MAP_LAYER_ACCURACY__BOOL", this.b.e.a());
        this.f259a.dismiss();
    }
}
