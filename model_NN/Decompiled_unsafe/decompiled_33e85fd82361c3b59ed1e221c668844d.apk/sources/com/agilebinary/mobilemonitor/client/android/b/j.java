package com.agilebinary.mobilemonitor.client.android.b;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.a.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a.f;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.i;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class j implements f {

    /* renamed from: a  reason: collision with root package name */
    public static final byte[] f174a = {1, 2, 3, 4, 6, 8, 9};
    private static String b = b.a();
    private Context c;
    private SQLiteDatabase d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private byte n = -1;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private List q;
    private i r;
    private Calendar s;
    private ByteArrayOutputStream t = new ByteArrayOutputStream();
    private SQLiteStatement u;
    private SQLiteStatement v;
    private ByteArrayOutputStream w;

    public j(Context context, i iVar) {
        this.c = context;
        this.r = iVar;
        this.d = new e(this, this.c, "events").getWritableDatabase();
        this.e = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s,%11$s,%12$s) values (?,?,?,?,?,?,?,?,?,?,?) ", "location", f.b, f.c, f.d, "line1", "line2", "accuracy", "lat", "lon", "contenttype", "powersave", "valloc"));
        this.f = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "call", c.b, c.c, c.d, "line1", "line2"));
        this.g = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "sms", a.b, a.c, a.d, "dir", "text"));
        this.h = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", "mms", d.b, d.c, d.d, "dir", "text", "thumbnail"));
        this.i = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "web", k.b, k.c, k.d, "line1", "line2"));
        this.j = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s) values (?,?,?,?) ", "syst_m", l.b, l.c, l.d, "line1"));
        this.k = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", i.f173a, i.b, i.c, i.d, i.f, i.g));
        this.l = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "curday", "eventtype", "day"));
        this.p = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "oldestevent", "eventtype", "oldest"));
        this.u = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "lastseen", "eventtype", "lastid"));
        this.m = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "curday", "eventtype", "day"));
        this.o = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "oldestevent", "eventtype", "oldest"));
        this.v = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "lastseen", "eventtype", "lastid"));
    }

    private static /* synthetic */ int a(Calendar calendar) {
        return (calendar.get(1) * 1000) + calendar.get(6);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x005b A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011f A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01b3 A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01cb A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:6:0x0038 A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }, FALL_THROUGH] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01f1 A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0221 A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x022f A[Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(byte r11, com.agilebinary.mobilemonitor.client.a.b.o r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            switch(r11) {
                case 1: goto L_0x003f;
                case 2: goto L_0x0043;
                case 3: goto L_0x0047;
                case 4: goto L_0x004b;
                case 5: goto L_0x0004;
                case 6: goto L_0x004f;
                case 7: goto L_0x0004;
                case 8: goto L_0x0053;
                case 9: goto L_0x0057;
                default: goto L_0x0004;
            }
        L_0x0004:
            r3 = 0
            r2 = r10
        L_0x0006:
            java.io.ByteArrayOutputStream r2 = r2.t     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2.reset()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.io.ByteArrayOutputStream r4 = r10.t     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2.writeObject(r12)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2.flush()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 2
            r4 = 1
            r3.clearBindings()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r6 = r12.u()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindLong(r4, r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r4 = r12.t()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 3
            java.io.ByteArrayOutputStream r4 = r10.t     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            byte[] r4 = r4.toByteArray()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindBlob(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            switch(r11) {
                case 1: goto L_0x005b;
                case 2: goto L_0x011f;
                case 3: goto L_0x01b3;
                case 4: goto L_0x01cb;
                case 5: goto L_0x0038;
                case 6: goto L_0x01f1;
                case 7: goto L_0x0038;
                case 8: goto L_0x0221;
                case 9: goto L_0x022f;
                default: goto L_0x0038;
            }     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
        L_0x0038:
            r2 = r3
        L_0x0039:
            r3 = r2
        L_0x003a:
            r3.executeInsert()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
        L_0x003d:
            monitor-exit(r10)
            return
        L_0x003f:
            android.database.sqlite.SQLiteStatement r3 = r10.e     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x0043:
            android.database.sqlite.SQLiteStatement r3 = r10.f     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x0047:
            android.database.sqlite.SQLiteStatement r3 = r10.g     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x004b:
            android.database.sqlite.SQLiteStatement r3 = r10.h     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x004f:
            android.database.sqlite.SQLiteStatement r3 = r10.i     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x0053:
            android.database.sqlite.SQLiteStatement r3 = r10.j     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x0057:
            android.database.sqlite.SQLiteStatement r3 = r10.k     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r10
            goto L_0x0006
        L_0x005b:
            r0 = r12
            com.agilebinary.mobilemonitor.client.a.b.s r0 = (com.agilebinary.mobilemonitor.client.a.b.s) r0     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r0
            r4 = 5
            r5 = 4
            android.content.Context r6 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r6 = r2.b(r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r5, r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r5 = r2.c(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.Double r2 = r2.f()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 != 0) goto L_0x00c8
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r2 = r3
        L_0x007c:
            r6 = 6
            r2.bindDouble(r6, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 9
            byte r4 = r12.k()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r4 = (long) r4     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindLong(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.a     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x00ce
            com.agilebinary.mobilemonitor.client.a.b.a r12 = (com.agilebinary.mobilemonitor.client.a.b.a) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 8
            r4 = 7
            double r6 = r12.a()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindDouble(r4, r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            double r4 = r12.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindDouble(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            boolean r2 = r12.e()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x0246
            r4 = 1
            r2 = r3
        L_0x00aa:
            r6 = 10
            r2.bindLong(r6, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            boolean r2 = r12.d()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x024b
            r4 = 1
            r2 = r3
        L_0x00b8:
            r6 = 11
            r2.bindLong(r6, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x00bf:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x00c5 }
            goto L_0x003d
        L_0x00c5:
            r2 = move-exception
            monitor-exit(r10)
            throw r2
        L_0x00c8:
            double r4 = r2.doubleValue()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r3
            goto L_0x007c
        L_0x00ce:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.n     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x010a
            com.agilebinary.mobilemonitor.client.a.b.n r12 = (com.agilebinary.mobilemonitor.client.a.b.n) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            com.agilebinary.mobilemonitor.client.android.a.a.b r2 = r12.l()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x00ff
            r4 = 8
            r5 = 7
            double r6 = r2.a()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindDouble(r5, r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            double r6 = r2.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindDouble(r4, r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r3
        L_0x00ec:
            r4 = 10
            r2.bindNull(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 11
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x00f8:
            r2 = move-exception
            com.agilebinary.mobilemonitor.client.android.b.h r3 = new com.agilebinary.mobilemonitor.client.android.b.h     // Catch:{ all -> 0x00c5 }
            r3.<init>(r2)     // Catch:{ all -> 0x00c5 }
            throw r3     // Catch:{ all -> 0x00c5 }
        L_0x00ff:
            r2 = 8
            r4 = 7
            r3.bindNull(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r3
            goto L_0x00ec
        L_0x010a:
            r2 = 11
            r4 = 10
            r5 = 8
            r6 = 7
            r3.bindNull(r6)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindNull(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindNull(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x011f:
            com.agilebinary.mobilemonitor.client.a.b.u r12 = (com.agilebinary.mobilemonitor.client.a.b.u) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 4
            java.lang.String r4 = r12.e()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r5 = r12.f()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.a.b(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r6 = r12.c()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r8 = r12.b()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            long r6 = r6 - r8
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 / r8
            int r2 = (int) r6     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            byte r4 = r12.d()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            switch(r4) {
                case -1: goto L_0x01a1;
                case 0: goto L_0x0147;
                case 1: goto L_0x015e;
                case 2: goto L_0x0170;
                case 3: goto L_0x018f;
                default: goto L_0x0147;
            }
        L_0x0147:
            java.lang.String r4 = ""
            java.lang.String r2 = ""
            r5 = r4
            r4 = r2
            r2 = r3
        L_0x014e:
            r6 = 5
            r7 = 1
            java.lang.Object[] r7 = new java.lang.Object[r7]     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r8 = 0
            r7[r8] = r4     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = java.lang.String.format(r5, r7)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2.bindString(r6, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x015e:
            r2 = 2131099859(0x7f0600d3, float:1.7812083E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r4 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r5 = r4
            r4 = r2
            r2 = r3
            goto L_0x014e
        L_0x0170:
            r4 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 <= 0) goto L_0x0184
            r2 = 2131099860(0x7f0600d4, float:1.7812085E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r5 = r4
            r4 = r2
            r2 = r3
            goto L_0x014e
        L_0x0184:
            r2 = 2131099863(0x7f0600d7, float:1.7812091E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r5 = r4
            r4 = r2
            r2 = r3
            goto L_0x014e
        L_0x018f:
            r2 = 2131099862(0x7f0600d6, float:1.781209E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r4 = 2131099864(0x7f0600d8, float:1.7812093E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r5 = r4
            r4 = r2
            r2 = r3
            goto L_0x014e
        L_0x01a1:
            r2 = 2131099861(0x7f0600d5, float:1.7812087E38)
            java.lang.String r2 = r5.getString(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r4 = 2131099865(0x7f0600d9, float:1.7812095E38)
            java.lang.String r4 = r5.getString(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r5 = r4
            r4 = r2
            r2 = r3
            goto L_0x014e
        L_0x01b3:
            android.database.sqlite.SQLiteStatement r3 = r10.g     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            com.agilebinary.mobilemonitor.client.a.b.j r12 = (com.agilebinary.mobilemonitor.client.a.b.j) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 5
            r4 = 4
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r5 = r12.a(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = r12.d()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x01cb:
            com.agilebinary.mobilemonitor.client.a.b.f r12 = (com.agilebinary.mobilemonitor.client.a.b.f) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 5
            r4 = 4
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r5 = r12.a(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = r12.l()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            byte[] r2 = r12.m()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 != 0) goto L_0x01eb
            r2 = 6
            r3.bindNull(r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x01eb:
            r4 = 6
            r3.bindBlob(r4, r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x01f1:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.e     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x0209
            r4 = 4
            r0 = r12
            com.agilebinary.mobilemonitor.client.a.b.e r0 = (com.agilebinary.mobilemonitor.client.a.b.e) r0     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r0
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r2 = r2.a(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r2)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 5
            java.lang.String r4 = ""
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
        L_0x0209:
            boolean r2 = r12 instanceof com.agilebinary.mobilemonitor.client.a.b.g     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            if (r2 == 0) goto L_0x0038
            com.agilebinary.mobilemonitor.client.a.b.g r12 = (com.agilebinary.mobilemonitor.client.a.b.g) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 5
            r4 = 4
            java.lang.String r5 = r12.f()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = r12.g()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x0221:
            r2 = 4
            com.agilebinary.mobilemonitor.client.a.b.d r12 = (com.agilebinary.mobilemonitor.client.a.b.d) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            android.content.Context r4 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = r12.a(r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            goto L_0x003a
        L_0x022f:
            com.agilebinary.mobilemonitor.client.a.b.c r12 = (com.agilebinary.mobilemonitor.client.a.b.c) r12     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = 5
            r4 = 4
            android.content.Context r5 = r10.c     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r5 = r12.a(r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r4, r5)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            java.lang.String r4 = r12.c()     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r3.bindString(r2, r4)     // Catch:{ Exception -> 0x00bf, OutOfMemoryError -> 0x00f8 }
            r2 = r3
            goto L_0x0039
        L_0x0246:
            r4 = 0
            r2 = r3
            goto L_0x00aa
        L_0x024b:
            r4 = 0
            r2 = r3
            goto L_0x00b8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.j.a(byte, com.agilebinary.mobilemonitor.client.a.b.o):void");
    }

    public static void e() {
        SQLiteDatabase.releaseMemory();
    }

    private static /* synthetic */ String h(byte b2) {
        switch (b2) {
            case 1:
                return "location";
            case 2:
                return "call";
            case 3:
                return "sms";
            case 4:
                return "mms";
            case 5:
            case 7:
            default:
                return null;
            case 6:
                return "web";
            case 8:
                return "syst_m";
            case 9:
                return i.f173a;
        }
    }

    private /* synthetic */ void h(byte b2, long j2) {
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            new StringBuilder().insert(0, " ts=").append(j2).toString();
            this.p.bindLong(1, (long) b2);
            this.p.bindLong(2, j2);
            this.p.executeInsert();
        }
    }

    public final long a(byte b2) {
        long j2;
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            this.o.bindLong(1, (long) b2);
            j2 = 0;
            try {
                j2 = this.o.simpleQueryForLong();
            } catch (SQLiteDoneException e2) {
            }
            new StringBuilder().insert(0, " =").append(j2).toString();
        }
        return j2;
    }

    public final long a(byte b2, long j2) {
        new StringBuilder().insert(0, "").append((int) b2).toString();
        new StringBuilder().insert(0, " startOfDay=").append(j2).toString();
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", h(b2), b.b));
        long simpleQueryForLong = compileStatement.simpleQueryForLong();
        compileStatement.close();
        new StringBuilder().insert(0, " =").append(simpleQueryForLong).toString();
        return simpleQueryForLong == 0 ? j2 : simpleQueryForLong;
    }

    public final List a(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((s) g((byte) 1, ((Long) it.next()).longValue()));
        }
        return arrayList;
    }

    public final void a() {
        this.d.close();
    }

    public final void a(byte b2, int i2, long j2) {
        synchronized (this) {
            new StringBuilder().insert(0, " / ").append((int) b2).toString();
            new StringBuilder().insert(0, " / ").append(i2).toString();
            new StringBuilder().insert(0, " / ").append(new Date(j2)).toString();
            if (this.n != -1) {
                throw new IllegalStateException();
            }
            this.n = b2;
            if (b2 == 1) {
                this.q = new ArrayList();
                this.r.a();
            }
            h(b2, j2);
            this.w = new ByteArrayOutputStream(32000);
        }
    }

    public final void a(String str, byte b2, long j2, long j3, boolean z, boolean z2, int i2, InputStream inputStream) {
        synchronized (this) {
            new StringBuilder().insert(0, " type=").append((int) b2).toString();
            new StringBuilder().insert(0, " id=").append(j2).toString();
            if (this.n != -1) {
                try {
                    o a2 = com.agilebinary.mobilemonitor.client.a.f.a(str, j2, j3, z, z2, i2, inputStream, MyApplication.g(), MyApplication.f141a, this.c, this.w);
                    if (a2 != null) {
                        if (this.n != 1) {
                            a(b2, a2);
                        } else if (!(a2 instanceof c) || !((c) a2).a()) {
                            a(b2, a2);
                        } else if (this.r.a((c) a2)) {
                            a(b2, a2);
                        } else {
                            this.q.add((c) a2);
                        }
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                } catch (OutOfMemoryError e3) {
                    throw new h(e3);
                }
            }
        }
        return;
    }

    public final void a(boolean z) {
        synchronized (this) {
            new StringBuilder().insert(0, "").append(z).toString();
            this.w = null;
            if (this.n != -1) {
                if (!z) {
                    if (this.n == 1) {
                        this.r.a(this.q);
                        Iterator it = this.q.iterator();
                        while (it.hasNext()) {
                            try {
                                a(this.n, (s) ((c) it.next()));
                            } catch (h e2) {
                                e2.printStackTrace();
                            }
                        }
                        this.q.clear();
                    }
                }
                this.n = -1;
                this.q = null;
                this.r.b();
            }
        }
    }

    public final boolean a(byte b2, Calendar calendar) {
        boolean z = true;
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            new StringBuilder().insert(0, " c=").append(calendar).toString();
            this.m.bindLong(1, (long) b2);
            try {
                if (this.m.simpleQueryForLong() != ((long) a(calendar))) {
                    z = false;
                }
            } catch (SQLiteDoneException e2) {
                z = false;
            }
            new StringBuilder().insert(0, "=").append(z).toString();
        }
        return z;
    }

    public final long b(byte b2) {
        long j2;
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            this.v.bindLong(1, (long) b2);
            j2 = -1;
            try {
                j2 = this.v.simpleQueryForLong();
            } catch (SQLiteDoneException e2) {
            }
            new StringBuilder().insert(0, " =").append(j2).toString();
        }
        return j2;
    }

    public final void b() {
        this.d.execSQL(String.format("DELETE FROM %1$s", "location"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "call"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "sms"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "mms"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "web"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "syst_m"));
        this.d.execSQL(String.format("DELETE FROM %1$s", i.f173a));
        this.d.execSQL(String.format("DELETE FROM %1$s", "lastseen"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "curday"));
        this.d.execSQL(String.format("DELETE FROM %1$s", "oldestevent"));
    }

    public final void b(byte b2, long j2) {
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            new StringBuilder().insert(0, " ts=").append(j2).toString();
            if (j2 > b(b2)) {
                new StringBuilder().insert(0, "updateLastssenEventTs = ").append(j2).toString();
                this.u.bindLong(1, (long) b2);
                this.u.bindLong(2, j2);
                this.u.executeInsert();
            }
        }
    }

    public final void b(byte b2, Calendar calendar) {
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            new StringBuilder().insert(0, " c=").append(calendar).toString();
            this.s = calendar;
            int a2 = a(calendar);
            this.l.bindLong(1, (long) b2);
            this.l.bindLong(2, (long) a2);
            this.l.executeInsert();
        }
    }

    public final void c() {
        this.r.c();
    }

    public final void c(byte b2) {
        this.d.execSQL(String.format("DELETE FROM %1$s", h(b2)));
    }

    public final boolean c(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s<? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    public final o d(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s where %2$s<? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final Calendar d() {
        new StringBuilder().insert(0, "").append(this.s).toString();
        return this.s;
    }

    public final void d(byte b2) {
        synchronized (this) {
            new StringBuilder().insert(0, "").append((int) b2).toString();
            b(b2, a(b2, 0));
        }
    }

    public final Cursor e(byte b2) {
        Cursor query;
        String[] strArr = null;
        synchronized (this) {
            SQLiteDatabase sQLiteDatabase = this.d;
            String h2 = h(b2);
            switch (b2) {
                case 1:
                    strArr = f.f171a;
                    break;
                case 2:
                    strArr = c.f168a;
                    break;
                case 3:
                    strArr = a.f166a;
                    break;
                case 4:
                    strArr = d.f169a;
                    break;
                case 6:
                    strArr = k.f175a;
                    break;
                case 8:
                    strArr = l.f176a;
                    break;
                case 9:
                    strArr = i.h;
                    break;
            }
            query = sQLiteDatabase.query(h2, strArr, null, null, null, null, new StringBuilder().insert(0, b.b).append(" DESC").toString());
        }
        return query;
    }

    public final boolean e(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s>? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return false;
        }
    }

    public final o f(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s  ", h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o f(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s where %2$s>? ", h(b2), b.b));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o g(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", h(b2), b.b));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return g(b2, simpleQueryForLong);
        } catch (Exception e2) {
            a.e(e2);
            compileStatement.close();
            return null;
        }
    }

    public final o g(byte b2, long j2) {
        Cursor cursor;
        Cursor query = this.d.query(h(b2), b.e, new StringBuilder().insert(0, b.b).append("=?").toString(), new String[]{String.valueOf(j2)}, null, null, null);
        if (!query.moveToFirst()) {
            query.close();
            return null;
        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            o oVar = (o) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            query.close();
            return oVar;
        } catch (StreamCorruptedException e2) {
            a.e(e2);
            cursor = query;
            cursor.close();
            return null;
        } catch (OptionalDataException e3) {
            a.e(e3);
            cursor = query;
            cursor.close();
            return null;
        } catch (IOException e4) {
            a.e(e4);
            cursor = query;
            cursor.close();
            return null;
        } catch (ClassNotFoundException e5) {
            a.e(e5);
            cursor = query;
            cursor.close();
            return null;
        } catch (OutOfMemoryError e6) {
            a.e(e6);
            cursor = query;
            cursor.close();
            return null;
        }
    }
}
