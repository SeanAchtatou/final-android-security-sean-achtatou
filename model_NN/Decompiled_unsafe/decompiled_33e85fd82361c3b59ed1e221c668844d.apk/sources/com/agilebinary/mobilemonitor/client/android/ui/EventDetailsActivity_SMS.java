package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.j;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.biige.client.android.R;

public class EventDetailsActivity_SMS extends EventDetailsActivity_base {

    /* renamed from: a  reason: collision with root package name */
    private TextView f197a;
    private TextView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private TextView f;
    private TextView h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public final void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_sms, viewGroup, true);
        this.f197a = (TextView) findViewById(R.id.eventdetails_sms_direction);
        this.b = (TextView) findViewById(R.id.eventdetails_sms_time_received);
        this.c = (TextView) findViewById(R.id.eventdetails_sms_time_sent);
        this.h = (TextView) findViewById(R.id.eventdetails_sms_fromto);
        this.d = (TextView) findViewById(R.id.eventdetails_sms_remoteparty);
        this.e = (TextView) findViewById(R.id.eventdetails_sms_speed);
        this.f = (TextView) findViewById(R.id.eventdetails_sms_message);
    }

    /* access modifiers changed from: protected */
    public final void a(o oVar) {
        super.a(oVar);
        j jVar = (j) oVar;
        String a2 = a.a(jVar.b(), jVar.c());
        this.f197a.setText(getResources().getString(jVar.x() == 1 ? R.string.label_event_sms_incoming : R.string.label_event_sms_outgoing));
        this.b.setText(c.a().c(jVar.w()));
        this.c.setText(c.a().c(jVar.v()));
        this.h.setText(getResources().getString(jVar.x() == 1 ? R.string.label_event_from : R.string.label_event_to));
        this.d.setText(a2);
        this.e.setText(a.a(this, jVar));
        this.f.setText(jVar.a());
        if (jVar.x() != 2 || !a.a(jVar)) {
            this.e.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.e.setTextColor(getResources().getColor(R.color.warning));
        }
    }
}
