package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import com.agilebinary.mobilemonitor.client.a.b.a;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.a.d;
import com.biige.client.android.R;
import org.osmdroid.b.a.b;
import org.osmdroid.b.a.c;

public final class aw extends c {
    s c;

    public aw(s sVar, Context context) {
        super("foo", "bar", ((d) sVar).h(), (byte) 0);
        this.c = sVar;
        if (!(sVar instanceof a)) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_blue));
        } else if (((a) sVar).d()) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_green));
        } else {
            a(context.getResources().getDrawable(R.drawable.mapmarker_red));
        }
        a(b.BOTTOM_CENTER);
    }

    public final Drawable a(int i) {
        int[] iArr;
        aw awVar;
        if (this.b == null) {
            return null;
        }
        if (i == 4) {
            iArr = new int[]{16842908};
            awVar = this;
        } else {
            iArr = new int[0];
            awVar = this;
        }
        awVar.b.setState(iArr);
        return this.b;
    }

    public final Point b(int i) {
        return i == 0 ? new Point(10, 35) : new Point(14, 48);
    }
}
