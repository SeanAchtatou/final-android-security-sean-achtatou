package com.sssp;

import java.security.MessageDigest;

public class MD5Util {
    public static final String getMD5String(String str) {
        String str2;
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        try {
            byte[] bytes = str.getBytes();
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bytes);
            byte[] digest = instance.digest();
            int length = digest.length;
            char[] cArr2 = new char[(length * 2)];
            int i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                byte b = digest[i2];
                int i3 = i;
                int i4 = i + 1;
                cArr2[i3] = cArr[(b >>> 4) & 15];
                int i5 = i4;
                i = i4 + 1;
                cArr2[i5] = cArr[b & 15];
            }
            new String(cArr2);
            return str2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
