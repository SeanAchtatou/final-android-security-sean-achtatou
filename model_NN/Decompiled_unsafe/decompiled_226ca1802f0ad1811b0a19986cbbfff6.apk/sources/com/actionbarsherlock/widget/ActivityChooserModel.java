package com.actionbarsherlock.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.xmlpull.v1.XmlSerializer;

class ActivityChooserModel extends DataSetObservable {
    private static final String ATTRIBUTE_ACTIVITY = "activity";
    private static final String ATTRIBUTE_TIME = "time";
    private static final String ATTRIBUTE_WEIGHT = "weight";
    private static final boolean DEBUG = false;
    private static final int DEFAULT_ACTIVITY_INFLATION = 5;
    private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0f;
    public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
    public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
    private static final String HISTORY_FILE_EXTENSION = ".xml";
    private static final int INVALID_INDEX = -1;
    /* access modifiers changed from: private */
    public static final String LOG_TAG = ActivityChooserModel.class.getSimpleName();
    private static final Executor SERIAL_EXECUTOR = Executors.newSingleThreadExecutor();
    private static final String TAG_HISTORICAL_RECORD = "historical-record";
    private static final String TAG_HISTORICAL_RECORDS = "historical-records";
    private static final Map<String, ActivityChooserModel> sDataModelRegistry = new HashMap();
    private static final Object sRegistryLock = new Object();
    private final List<ActivityResolveInfo> mActivites = new ArrayList();
    private OnChooseActivityListener mActivityChoserModelPolicy;
    private ActivitySorter mActivitySorter = new DefaultSorter();
    private boolean mCanReadHistoricalData = true;
    /* access modifiers changed from: private */
    public final Context mContext;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public final List<HistoricalRecord> mHistoricalRecords = new ArrayList();
    /* access modifiers changed from: private */
    public boolean mHistoricalRecordsChanged = true;
    /* access modifiers changed from: private */
    public final String mHistoryFileName;
    private int mHistoryMaxSize = 50;
    /* access modifiers changed from: private */
    public final Object mInstanceLock = new Object();
    private Intent mIntent;
    private boolean mReadShareHistoryCalled = false;

    public interface ActivityChooserModelClient {
        void setActivityChooserModel(ActivityChooserModel activityChooserModel);
    }

    public interface ActivitySorter {
        void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2);
    }

    public interface OnChooseActivityListener {
        boolean onChooseActivity(ActivityChooserModel activityChooserModel, Intent intent);
    }

    public static ActivityChooserModel get(Context context, String str) {
        ActivityChooserModel activityChooserModel;
        synchronized (sRegistryLock) {
            activityChooserModel = sDataModelRegistry.get(str);
            if (activityChooserModel == null) {
                activityChooserModel = new ActivityChooserModel(context, str);
                sDataModelRegistry.put(str, activityChooserModel);
            }
            activityChooserModel.readHistoricalData();
        }
        return activityChooserModel;
    }

    private ActivityChooserModel(Context context, String str) {
        this.mContext = context.getApplicationContext();
        if (TextUtils.isEmpty(str) || str.endsWith(HISTORY_FILE_EXTENSION)) {
            this.mHistoryFileName = str;
        } else {
            this.mHistoryFileName = str + HISTORY_FILE_EXTENSION;
        }
    }

    public void setIntent(Intent intent) {
        synchronized (this.mInstanceLock) {
            if (this.mIntent != intent) {
                this.mIntent = intent;
                loadActivitiesLocked();
            }
        }
    }

    public Intent getIntent() {
        Intent intent;
        synchronized (this.mInstanceLock) {
            intent = this.mIntent;
        }
        return intent;
    }

    public int getActivityCount() {
        int size;
        synchronized (this.mInstanceLock) {
            size = this.mActivites.size();
        }
        return size;
    }

    public ResolveInfo getActivity(int i) {
        ResolveInfo resolveInfo;
        synchronized (this.mInstanceLock) {
            resolveInfo = this.mActivites.get(i).resolveInfo;
        }
        return resolveInfo;
    }

    public int getActivityIndex(ResolveInfo resolveInfo) {
        List<ActivityResolveInfo> list = this.mActivites;
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (list.get(i).resolveInfo == resolveInfo) {
                return i;
            }
        }
        return -1;
    }

    public Intent chooseActivity(int i) {
        ActivityResolveInfo activityResolveInfo = this.mActivites.get(i);
        ComponentName componentName = new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name);
        Intent intent = new Intent(this.mIntent);
        intent.setComponent(componentName);
        if (this.mActivityChoserModelPolicy != null) {
            if (this.mActivityChoserModelPolicy.onChooseActivity(this, new Intent(intent))) {
                return null;
            }
        }
        addHisoricalRecord(new HistoricalRecord(componentName, System.currentTimeMillis(), (float) DEFAULT_HISTORICAL_RECORD_WEIGHT));
        return intent;
    }

    public void setOnChooseActivityListener(OnChooseActivityListener onChooseActivityListener) {
        this.mActivityChoserModelPolicy = onChooseActivityListener;
    }

    public ResolveInfo getDefaultActivity() {
        synchronized (this.mInstanceLock) {
            if (this.mActivites.isEmpty()) {
                return null;
            }
            ResolveInfo resolveInfo = this.mActivites.get(0).resolveInfo;
            return resolveInfo;
        }
    }

    public void setDefaultActivity(int i) {
        float f;
        ActivityResolveInfo activityResolveInfo = this.mActivites.get(i);
        ActivityResolveInfo activityResolveInfo2 = this.mActivites.get(0);
        if (activityResolveInfo2 != null) {
            f = (activityResolveInfo2.weight - activityResolveInfo.weight) + 5.0f;
        } else {
            f = DEFAULT_HISTORICAL_RECORD_WEIGHT;
        }
        addHisoricalRecord(new HistoricalRecord(new ComponentName(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo.resolveInfo.activityInfo.name), System.currentTimeMillis(), f));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void readHistoricalData() {
        /*
            r4 = this;
            java.lang.Object r1 = r4.mInstanceLock
            monitor-enter(r1)
            boolean r0 = r4.mCanReadHistoricalData     // Catch:{ all -> 0x0028 }
            if (r0 == 0) goto L_0x000b
            boolean r0 = r4.mHistoricalRecordsChanged     // Catch:{ all -> 0x0028 }
            if (r0 != 0) goto L_0x000d
        L_0x000b:
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
        L_0x000c:
            return
        L_0x000d:
            r0 = 0
            r4.mCanReadHistoricalData = r0     // Catch:{ all -> 0x0028 }
            r0 = 1
            r4.mReadShareHistoryCalled = r0     // Catch:{ all -> 0x0028 }
            java.lang.String r0 = r4.mHistoryFileName     // Catch:{ all -> 0x0028 }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x0028 }
            if (r0 != 0) goto L_0x0026
            java.util.concurrent.Executor r0 = com.actionbarsherlock.widget.ActivityChooserModel.SERIAL_EXECUTOR     // Catch:{ all -> 0x0028 }
            com.actionbarsherlock.widget.ActivityChooserModel$HistoryLoader r2 = new com.actionbarsherlock.widget.ActivityChooserModel$HistoryLoader     // Catch:{ all -> 0x0028 }
            r3 = 0
            r2.<init>()     // Catch:{ all -> 0x0028 }
            r0.execute(r2)     // Catch:{ all -> 0x0028 }
        L_0x0026:
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            goto L_0x000c
        L_0x0028:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0028 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.widget.ActivityChooserModel.readHistoricalData():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void persistHistoricalData() {
        /*
            r4 = this;
            java.lang.Object r1 = r4.mInstanceLock
            monitor-enter(r1)
            boolean r0 = r4.mReadShareHistoryCalled     // Catch:{ all -> 0x000f }
            if (r0 != 0) goto L_0x0012
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x000f }
            java.lang.String r2 = "No preceding call to #readHistoricalData"
            r0.<init>(r2)     // Catch:{ all -> 0x000f }
            throw r0     // Catch:{ all -> 0x000f }
        L_0x000f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x000f }
            throw r0
        L_0x0012:
            boolean r0 = r4.mHistoricalRecordsChanged     // Catch:{ all -> 0x000f }
            if (r0 != 0) goto L_0x0018
            monitor-exit(r1)     // Catch:{ all -> 0x000f }
        L_0x0017:
            return
        L_0x0018:
            r0 = 0
            r4.mHistoricalRecordsChanged = r0     // Catch:{ all -> 0x000f }
            r0 = 1
            r4.mCanReadHistoricalData = r0     // Catch:{ all -> 0x000f }
            java.lang.String r0 = r4.mHistoryFileName     // Catch:{ all -> 0x000f }
            boolean r0 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x000f }
            if (r0 != 0) goto L_0x0031
            java.util.concurrent.Executor r0 = com.actionbarsherlock.widget.ActivityChooserModel.SERIAL_EXECUTOR     // Catch:{ all -> 0x000f }
            com.actionbarsherlock.widget.ActivityChooserModel$HistoryPersister r2 = new com.actionbarsherlock.widget.ActivityChooserModel$HistoryPersister     // Catch:{ all -> 0x000f }
            r3 = 0
            r2.<init>()     // Catch:{ all -> 0x000f }
            r0.execute(r2)     // Catch:{ all -> 0x000f }
        L_0x0031:
            monitor-exit(r1)     // Catch:{ all -> 0x000f }
            goto L_0x0017
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.widget.ActivityChooserModel.persistHistoricalData():void");
    }

    public void setActivitySorter(ActivitySorter activitySorter) {
        synchronized (this.mInstanceLock) {
            if (this.mActivitySorter != activitySorter) {
                this.mActivitySorter = activitySorter;
                sortActivities();
            }
        }
    }

    /* access modifiers changed from: private */
    public void sortActivities() {
        synchronized (this.mInstanceLock) {
            if (this.mActivitySorter != null && !this.mActivites.isEmpty()) {
                this.mActivitySorter.sort(this.mIntent, this.mActivites, Collections.unmodifiableList(this.mHistoricalRecords));
                notifyChanged();
            }
        }
    }

    public void setHistoryMaxSize(int i) {
        synchronized (this.mInstanceLock) {
            if (this.mHistoryMaxSize != i) {
                this.mHistoryMaxSize = i;
                pruneExcessiveHistoricalRecordsLocked();
                sortActivities();
            }
        }
    }

    public int getHistoryMaxSize() {
        int i;
        synchronized (this.mInstanceLock) {
            i = this.mHistoryMaxSize;
        }
        return i;
    }

    public int getHistorySize() {
        int size;
        synchronized (this.mInstanceLock) {
            size = this.mHistoricalRecords.size();
        }
        return size;
    }

    private boolean addHisoricalRecord(HistoricalRecord historicalRecord) {
        boolean add;
        synchronized (this.mInstanceLock) {
            add = this.mHistoricalRecords.add(historicalRecord);
            if (add) {
                this.mHistoricalRecordsChanged = true;
                pruneExcessiveHistoricalRecordsLocked();
                persistHistoricalData();
                sortActivities();
            }
        }
        return add;
    }

    /* access modifiers changed from: private */
    public void pruneExcessiveHistoricalRecordsLocked() {
        List<HistoricalRecord> list = this.mHistoricalRecords;
        int size = list.size() - this.mHistoryMaxSize;
        if (size > 0) {
            this.mHistoricalRecordsChanged = true;
            for (int i = 0; i < size; i++) {
                HistoricalRecord remove = list.remove(0);
            }
        }
    }

    private void loadActivitiesLocked() {
        this.mActivites.clear();
        if (this.mIntent != null) {
            List<ResolveInfo> queryIntentActivities = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0);
            int size = queryIntentActivities.size();
            for (int i = 0; i < size; i++) {
                this.mActivites.add(new ActivityResolveInfo(queryIntentActivities.get(i)));
            }
            sortActivities();
            return;
        }
        notifyChanged();
    }

    public final class HistoricalRecord {
        public final ComponentName activity;
        public final long time;
        public final float weight;

        public HistoricalRecord(String str, long j, float f) {
            this(ComponentName.unflattenFromString(str), j, f);
        }

        public HistoricalRecord(ComponentName componentName, long j, float f) {
            this.activity = componentName;
            this.time = j;
            this.weight = f;
        }

        public int hashCode() {
            return (((((this.activity == null ? 0 : this.activity.hashCode()) + 31) * 31) + ((int) (this.time ^ (this.time >>> 32)))) * 31) + Float.floatToIntBits(this.weight);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            HistoricalRecord historicalRecord = (HistoricalRecord) obj;
            if (this.activity == null) {
                if (historicalRecord.activity != null) {
                    return false;
                }
            } else if (!this.activity.equals(historicalRecord.activity)) {
                return false;
            }
            if (this.time != historicalRecord.time) {
                return false;
            }
            if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(historicalRecord.weight)) {
                return false;
            }
            return true;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("; activity:").append(this.activity);
            sb.append("; time:").append(this.time);
            sb.append("; weight:").append(new BigDecimal((double) this.weight));
            sb.append("]");
            return sb.toString();
        }
    }

    public final class ActivityResolveInfo implements Comparable<ActivityResolveInfo> {
        public final ResolveInfo resolveInfo;
        public float weight;

        public ActivityResolveInfo(ResolveInfo resolveInfo2) {
            this.resolveInfo = resolveInfo2;
        }

        public int hashCode() {
            return Float.floatToIntBits(this.weight) + 31;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (Float.floatToIntBits(this.weight) != Float.floatToIntBits(((ActivityResolveInfo) obj).weight)) {
                return false;
            }
            return true;
        }

        public int compareTo(ActivityResolveInfo activityResolveInfo) {
            return Float.floatToIntBits(activityResolveInfo.weight) - Float.floatToIntBits(this.weight);
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            sb.append("resolveInfo:").append(this.resolveInfo.toString());
            sb.append("; weight:").append(new BigDecimal((double) this.weight));
            sb.append("]");
            return sb.toString();
        }
    }

    final class DefaultSorter implements ActivitySorter {
        private static final float WEIGHT_DECAY_COEFFICIENT = 0.95f;
        private final Map<String, ActivityResolveInfo> mPackageNameToActivityMap;

        private DefaultSorter() {
            this.mPackageNameToActivityMap = new HashMap();
        }

        public void sort(Intent intent, List<ActivityResolveInfo> list, List<HistoricalRecord> list2) {
            float f;
            Map<String, ActivityResolveInfo> map = this.mPackageNameToActivityMap;
            map.clear();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                ActivityResolveInfo activityResolveInfo = list.get(i);
                activityResolveInfo.weight = 0.0f;
                map.put(activityResolveInfo.resolveInfo.activityInfo.packageName, activityResolveInfo);
            }
            float f2 = ActivityChooserModel.DEFAULT_HISTORICAL_RECORD_WEIGHT;
            int size2 = list2.size() - 1;
            while (size2 >= 0) {
                HistoricalRecord historicalRecord = list2.get(size2);
                ActivityResolveInfo activityResolveInfo2 = map.get(historicalRecord.activity.getPackageName());
                if (activityResolveInfo2 != null) {
                    activityResolveInfo2.weight = (historicalRecord.weight * f2) + activityResolveInfo2.weight;
                    f = WEIGHT_DECAY_COEFFICIENT * f2;
                } else {
                    f = f2;
                }
                size2--;
                f2 = f;
            }
            Collections.sort(list);
        }
    }

    final class HistoryLoader implements Runnable {
        private HistoryLoader() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:23:0x006a, code lost:
            r3 = com.actionbarsherlock.widget.ActivityChooserModel.access$500(r9.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0070, code lost:
            monitor-enter(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
            r4 = new java.util.LinkedHashSet(r0);
            r5 = com.actionbarsherlock.widget.ActivityChooserModel.access$600(r9.this$0);
            r1 = r5.size() - 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0083, code lost:
            if (r1 < 0) goto L_0x0103;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x0085, code lost:
            r4.add((com.actionbarsherlock.widget.ActivityChooserModel.HistoricalRecord) r5.get(r1));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x008e, code lost:
            r1 = r1 - 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x010b, code lost:
            if (r5.size() != r4.size()) goto L_0x0118;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x010d, code lost:
            monitor-exit(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x010e, code lost:
            if (r2 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
            r5.clear();
            r5.addAll(r4);
            com.actionbarsherlock.widget.ActivityChooserModel.access$702(r9.this$0, true);
            com.actionbarsherlock.widget.ActivityChooserModel.access$1000(r9.this$0).post(new com.actionbarsherlock.widget.ActivityChooserModel.HistoryLoader.AnonymousClass1(r9));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x0132, code lost:
            monitor-exit(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x0133, code lost:
            if (r2 == null) goto L_?;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:?, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:?, code lost:
            return;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r9 = this;
                r8 = 1
                com.actionbarsherlock.widget.ActivityChooserModel r0 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ FileNotFoundException -> 0x0145 }
                android.content.Context r0 = r0.mContext     // Catch:{ FileNotFoundException -> 0x0145 }
                com.actionbarsherlock.widget.ActivityChooserModel r1 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ FileNotFoundException -> 0x0145 }
                java.lang.String r1 = r1.mHistoryFileName     // Catch:{ FileNotFoundException -> 0x0145 }
                java.io.FileInputStream r2 = r0.openFileInput(r1)     // Catch:{ FileNotFoundException -> 0x0145 }
                org.xmlpull.v1.XmlPullParser r1 = android.util.Xml.newPullParser()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r0 = 0
                r1.setInput(r2, r0)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r0 = 0
            L_0x001a:
                if (r0 == r8) goto L_0x0024
                r3 = 2
                if (r0 == r3) goto L_0x0024
                int r0 = r1.next()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                goto L_0x001a
            L_0x0024:
                java.lang.String r0 = "historical-records"
                java.lang.String r3 = r1.getName()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                boolean r0 = r0.equals(r3)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                if (r0 != 0) goto L_0x005f
                org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                java.lang.String r1 = "Share records file does not start with historical-records tag."
                r0.<init>(r1)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                throw r0     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
            L_0x0038:
                r0 = move-exception
                java.lang.String r1 = com.actionbarsherlock.widget.ActivityChooserModel.LOG_TAG     // Catch:{ all -> 0x00fc }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fc }
                r3.<init>()     // Catch:{ all -> 0x00fc }
                java.lang.String r4 = "Error reading historical recrod file: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00fc }
                com.actionbarsherlock.widget.ActivityChooserModel r4 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ all -> 0x00fc }
                java.lang.String r4 = r4.mHistoryFileName     // Catch:{ all -> 0x00fc }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00fc }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00fc }
                android.util.Log.e(r1, r3, r0)     // Catch:{ all -> 0x00fc }
                if (r2 == 0) goto L_0x005e
                r2.close()     // Catch:{ IOException -> 0x0140 }
            L_0x005e:
                return
            L_0x005f:
                java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r0.<init>()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
            L_0x0064:
                int r3 = r1.next()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                if (r3 != r8) goto L_0x0092
                com.actionbarsherlock.widget.ActivityChooserModel r1 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                java.lang.Object r3 = r1.mInstanceLock     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                monitor-enter(r3)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                java.util.LinkedHashSet r4 = new java.util.LinkedHashSet     // Catch:{ all -> 0x013d }
                r4.<init>(r0)     // Catch:{ all -> 0x013d }
                com.actionbarsherlock.widget.ActivityChooserModel r0 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ all -> 0x013d }
                java.util.List r5 = r0.mHistoricalRecords     // Catch:{ all -> 0x013d }
                int r0 = r5.size()     // Catch:{ all -> 0x013d }
                int r0 = r0 + -1
                r1 = r0
            L_0x0083:
                if (r1 < 0) goto L_0x0103
                java.lang.Object r0 = r5.get(r1)     // Catch:{ all -> 0x013d }
                com.actionbarsherlock.widget.ActivityChooserModel$HistoricalRecord r0 = (com.actionbarsherlock.widget.ActivityChooserModel.HistoricalRecord) r0     // Catch:{ all -> 0x013d }
                r4.add(r0)     // Catch:{ all -> 0x013d }
                int r0 = r1 + -1
                r1 = r0
                goto L_0x0083
            L_0x0092:
                r4 = 3
                if (r3 == r4) goto L_0x0064
                r4 = 4
                if (r3 == r4) goto L_0x0064
                java.lang.String r3 = r1.getName()     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                java.lang.String r4 = "historical-record"
                boolean r3 = r4.equals(r3)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                if (r3 != 0) goto L_0x00d5
                org.xmlpull.v1.XmlPullParserException r0 = new org.xmlpull.v1.XmlPullParserException     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                java.lang.String r1 = "Share records file not well-formed."
                r0.<init>(r1)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                throw r0     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
            L_0x00ac:
                r0 = move-exception
                java.lang.String r1 = com.actionbarsherlock.widget.ActivityChooserModel.LOG_TAG     // Catch:{ all -> 0x00fc }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00fc }
                r3.<init>()     // Catch:{ all -> 0x00fc }
                java.lang.String r4 = "Error reading historical recrod file: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00fc }
                com.actionbarsherlock.widget.ActivityChooserModel r4 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ all -> 0x00fc }
                java.lang.String r4 = r4.mHistoryFileName     // Catch:{ all -> 0x00fc }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00fc }
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00fc }
                android.util.Log.e(r1, r3, r0)     // Catch:{ all -> 0x00fc }
                if (r2 == 0) goto L_0x005e
                r2.close()     // Catch:{ IOException -> 0x00d3 }
                goto L_0x005e
            L_0x00d3:
                r0 = move-exception
                goto L_0x005e
            L_0x00d5:
                r3 = 0
                java.lang.String r4 = "activity"
                java.lang.String r3 = r1.getAttributeValue(r3, r4)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r4 = 0
                java.lang.String r5 = "time"
                java.lang.String r4 = r1.getAttributeValue(r4, r5)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                long r4 = java.lang.Long.parseLong(r4)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r6 = 0
                java.lang.String r7 = "weight"
                java.lang.String r6 = r1.getAttributeValue(r6, r7)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                float r6 = java.lang.Float.parseFloat(r6)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                com.actionbarsherlock.widget.ActivityChooserModel$HistoricalRecord r7 = new com.actionbarsherlock.widget.ActivityChooserModel$HistoricalRecord     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r7.<init>(r3, r4, r6)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                r0.add(r7)     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
                goto L_0x0064
            L_0x00fc:
                r0 = move-exception
                if (r2 == 0) goto L_0x0102
                r2.close()     // Catch:{ IOException -> 0x0143 }
            L_0x0102:
                throw r0
            L_0x0103:
                int r0 = r5.size()     // Catch:{ all -> 0x013d }
                int r1 = r4.size()     // Catch:{ all -> 0x013d }
                if (r0 != r1) goto L_0x0118
                monitor-exit(r3)     // Catch:{ all -> 0x013d }
                if (r2 == 0) goto L_0x005e
                r2.close()     // Catch:{ IOException -> 0x0115 }
                goto L_0x005e
            L_0x0115:
                r0 = move-exception
                goto L_0x005e
            L_0x0118:
                r5.clear()     // Catch:{ all -> 0x013d }
                r5.addAll(r4)     // Catch:{ all -> 0x013d }
                com.actionbarsherlock.widget.ActivityChooserModel r0 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ all -> 0x013d }
                r1 = 1
                boolean unused = r0.mHistoricalRecordsChanged = r1     // Catch:{ all -> 0x013d }
                com.actionbarsherlock.widget.ActivityChooserModel r0 = com.actionbarsherlock.widget.ActivityChooserModel.this     // Catch:{ all -> 0x013d }
                android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x013d }
                com.actionbarsherlock.widget.ActivityChooserModel$HistoryLoader$1 r1 = new com.actionbarsherlock.widget.ActivityChooserModel$HistoryLoader$1     // Catch:{ all -> 0x013d }
                r1.<init>()     // Catch:{ all -> 0x013d }
                r0.post(r1)     // Catch:{ all -> 0x013d }
                monitor-exit(r3)     // Catch:{ all -> 0x013d }
                if (r2 == 0) goto L_0x005e
                r2.close()     // Catch:{ IOException -> 0x013a }
                goto L_0x005e
            L_0x013a:
                r0 = move-exception
                goto L_0x005e
            L_0x013d:
                r0 = move-exception
                monitor-exit(r3)     // Catch:{ all -> 0x013d }
                throw r0     // Catch:{ XmlPullParserException -> 0x0038, IOException -> 0x00ac }
            L_0x0140:
                r0 = move-exception
                goto L_0x005e
            L_0x0143:
                r1 = move-exception
                goto L_0x0102
            L_0x0145:
                r0 = move-exception
                goto L_0x005e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.widget.ActivityChooserModel.HistoryLoader.run():void");
        }
    }

    final class HistoryPersister implements Runnable {
        private HistoryPersister() {
        }

        public void run() {
            ArrayList arrayList;
            synchronized (ActivityChooserModel.this.mInstanceLock) {
                arrayList = new ArrayList(ActivityChooserModel.this.mHistoricalRecords);
            }
            try {
                FileOutputStream openFileOutput = ActivityChooserModel.this.mContext.openFileOutput(ActivityChooserModel.this.mHistoryFileName, 0);
                XmlSerializer newSerializer = Xml.newSerializer();
                try {
                    newSerializer.setOutput(openFileOutput, null);
                    newSerializer.startDocument("UTF-8", true);
                    newSerializer.startTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORDS);
                    int size = arrayList.size();
                    for (int i = 0; i < size; i++) {
                        HistoricalRecord historicalRecord = (HistoricalRecord) arrayList.remove(0);
                        newSerializer.startTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORD);
                        newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_ACTIVITY, historicalRecord.activity.flattenToString());
                        newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_TIME, String.valueOf(historicalRecord.time));
                        newSerializer.attribute(null, ActivityChooserModel.ATTRIBUTE_WEIGHT, String.valueOf(historicalRecord.weight));
                        newSerializer.endTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORD);
                    }
                    newSerializer.endTag(null, ActivityChooserModel.TAG_HISTORICAL_RECORDS);
                    newSerializer.endDocument();
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e) {
                        }
                    }
                } catch (IllegalArgumentException e2) {
                    Log.e(ActivityChooserModel.LOG_TAG, "Error writing historical recrod file: " + ActivityChooserModel.this.mHistoryFileName, e2);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e3) {
                        }
                    }
                } catch (IllegalStateException e4) {
                    Log.e(ActivityChooserModel.LOG_TAG, "Error writing historical recrod file: " + ActivityChooserModel.this.mHistoryFileName, e4);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e5) {
                        }
                    }
                } catch (IOException e6) {
                    Log.e(ActivityChooserModel.LOG_TAG, "Error writing historical recrod file: " + ActivityChooserModel.this.mHistoryFileName, e6);
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e7) {
                        }
                    }
                } catch (Throwable th) {
                    if (openFileOutput != null) {
                        try {
                            openFileOutput.close();
                        } catch (IOException e8) {
                        }
                    }
                    throw th;
                }
            } catch (FileNotFoundException e9) {
                Log.e(ActivityChooserModel.LOG_TAG, "Error writing historical recrod file: " + ActivityChooserModel.this.mHistoryFileName, e9);
            }
        }
    }
}
