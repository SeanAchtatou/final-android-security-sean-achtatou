package com.actionbarsherlock.internal.app;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.SpinnerAdapter;
import com.actionbarsherlock.app.ActionBar;
import java.util.HashSet;
import java.util.Set;

public class ActionBarWrapper extends ActionBar implements ActionBar.OnMenuVisibilityListener, ActionBar.OnNavigationListener {
    private final android.app.ActionBar mActionBar;
    /* access modifiers changed from: private */
    public final Activity mActivity;
    /* access modifiers changed from: private */
    public FragmentTransaction mFragmentTransaction;
    private Set<ActionBar.OnMenuVisibilityListener> mMenuVisibilityListeners = new HashSet(1);
    private ActionBar.OnNavigationListener mNavigationListener;

    public ActionBarWrapper(Activity activity) {
        boolean z = true;
        this.mActivity = activity;
        this.mActionBar = activity.getActionBar();
        if (this.mActionBar != null) {
            this.mActionBar.addOnMenuVisibilityListener(this);
            this.mActionBar.setHomeButtonEnabled((this.mActionBar.getDisplayOptions() & 4) == 0 ? false : z);
        }
    }

    public void setHomeButtonEnabled(boolean z) {
        this.mActionBar.setHomeButtonEnabled(z);
    }

    public Context getThemedContext() {
        return this.mActionBar.getThemedContext();
    }

    public void setCustomView(View view) {
        this.mActionBar.setCustomView(view);
    }

    public void setCustomView(View view, ActionBar.LayoutParams layoutParams) {
        ActionBar.LayoutParams layoutParams2 = new ActionBar.LayoutParams(layoutParams);
        layoutParams2.gravity = layoutParams.gravity;
        layoutParams2.bottomMargin = layoutParams.bottomMargin;
        layoutParams2.topMargin = layoutParams.topMargin;
        layoutParams2.leftMargin = layoutParams.leftMargin;
        layoutParams2.rightMargin = layoutParams.rightMargin;
        this.mActionBar.setCustomView(view, layoutParams2);
    }

    public void setCustomView(int i) {
        this.mActionBar.setCustomView(i);
    }

    public void setIcon(int i) {
        this.mActionBar.setIcon(i);
    }

    public void setIcon(Drawable drawable) {
        this.mActionBar.setIcon(drawable);
    }

    public void setLogo(int i) {
        this.mActionBar.setLogo(i);
    }

    public void setLogo(Drawable drawable) {
        this.mActionBar.setLogo(drawable);
    }

    public void setListNavigationCallbacks(SpinnerAdapter spinnerAdapter, ActionBar.OnNavigationListener onNavigationListener) {
        this.mNavigationListener = onNavigationListener;
        android.app.ActionBar actionBar = this.mActionBar;
        if (onNavigationListener == null) {
            this = null;
        }
        actionBar.setListNavigationCallbacks(spinnerAdapter, this);
    }

    public boolean onNavigationItemSelected(int i, long j) {
        return this.mNavigationListener.onNavigationItemSelected(i, j);
    }

    public void setSelectedNavigationItem(int i) {
        this.mActionBar.setSelectedNavigationItem(i);
    }

    public int getSelectedNavigationIndex() {
        return this.mActionBar.getSelectedNavigationIndex();
    }

    public int getNavigationItemCount() {
        return this.mActionBar.getNavigationItemCount();
    }

    public void setTitle(CharSequence charSequence) {
        this.mActionBar.setTitle(charSequence);
    }

    public void setTitle(int i) {
        this.mActionBar.setTitle(i);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.mActionBar.setSubtitle(charSequence);
    }

    public void setSubtitle(int i) {
        this.mActionBar.setSubtitle(i);
    }

    public void setDisplayOptions(int i) {
        this.mActionBar.setDisplayOptions(i);
        this.mActionBar.setHomeButtonEnabled((i & 4) != 0);
    }

    public void setDisplayOptions(int i, int i2) {
        this.mActionBar.setDisplayOptions(i, i2);
        if ((i2 & 4) != 0) {
            this.mActionBar.setHomeButtonEnabled((i & 4) != 0);
        }
    }

    public void setDisplayUseLogoEnabled(boolean z) {
        this.mActionBar.setDisplayUseLogoEnabled(z);
    }

    public void setDisplayShowHomeEnabled(boolean z) {
        this.mActionBar.setDisplayShowHomeEnabled(z);
    }

    public void setDisplayHomeAsUpEnabled(boolean z) {
        this.mActionBar.setDisplayHomeAsUpEnabled(z);
    }

    public void setDisplayShowTitleEnabled(boolean z) {
        this.mActionBar.setDisplayShowTitleEnabled(z);
    }

    public void setDisplayShowCustomEnabled(boolean z) {
        this.mActionBar.setDisplayShowCustomEnabled(z);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        this.mActionBar.setBackgroundDrawable(drawable);
    }

    public void setStackedBackgroundDrawable(Drawable drawable) {
        this.mActionBar.setStackedBackgroundDrawable(drawable);
    }

    public void setSplitBackgroundDrawable(Drawable drawable) {
        this.mActionBar.setSplitBackgroundDrawable(drawable);
    }

    public View getCustomView() {
        return this.mActionBar.getCustomView();
    }

    public CharSequence getTitle() {
        return this.mActionBar.getTitle();
    }

    public CharSequence getSubtitle() {
        return this.mActionBar.getSubtitle();
    }

    public int getNavigationMode() {
        return this.mActionBar.getNavigationMode();
    }

    public void setNavigationMode(int i) {
        this.mActionBar.setNavigationMode(i);
    }

    public int getDisplayOptions() {
        return this.mActionBar.getDisplayOptions();
    }

    public class TabWrapper extends ActionBar.Tab implements ActionBar.TabListener {
        private ActionBar.TabListener mListener;
        final ActionBar.Tab mNativeTab;
        private Object mTag;

        public TabWrapper(ActionBar.Tab tab) {
            this.mNativeTab = tab;
            this.mNativeTab.setTag(this);
        }

        public int getPosition() {
            return this.mNativeTab.getPosition();
        }

        public Drawable getIcon() {
            return this.mNativeTab.getIcon();
        }

        public CharSequence getText() {
            return this.mNativeTab.getText();
        }

        public ActionBar.Tab setIcon(Drawable drawable) {
            this.mNativeTab.setIcon(drawable);
            return this;
        }

        public ActionBar.Tab setIcon(int i) {
            this.mNativeTab.setIcon(i);
            return this;
        }

        public ActionBar.Tab setText(CharSequence charSequence) {
            this.mNativeTab.setText(charSequence);
            return this;
        }

        public ActionBar.Tab setText(int i) {
            this.mNativeTab.setText(i);
            return this;
        }

        public ActionBar.Tab setCustomView(View view) {
            this.mNativeTab.setCustomView(view);
            return this;
        }

        public ActionBar.Tab setCustomView(int i) {
            this.mNativeTab.setCustomView(i);
            return this;
        }

        public View getCustomView() {
            return this.mNativeTab.getCustomView();
        }

        public ActionBar.Tab setTag(Object obj) {
            this.mTag = obj;
            return this;
        }

        public Object getTag() {
            return this.mTag;
        }

        public ActionBar.Tab setTabListener(ActionBar.TabListener tabListener) {
            this.mNativeTab.setTabListener(tabListener != null ? this : null);
            this.mListener = tabListener;
            return this;
        }

        public void select() {
            this.mNativeTab.select();
        }

        public ActionBar.Tab setContentDescription(int i) {
            this.mNativeTab.setContentDescription(i);
            return this;
        }

        public ActionBar.Tab setContentDescription(CharSequence charSequence) {
            this.mNativeTab.setContentDescription(charSequence);
            return this;
        }

        public CharSequence getContentDescription() {
            return this.mNativeTab.getContentDescription();
        }

        public void onTabReselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
            if (this.mListener != null) {
                FragmentTransaction fragmentTransaction2 = null;
                if (ActionBarWrapper.this.mActivity instanceof FragmentActivity) {
                    fragmentTransaction2 = ((FragmentActivity) ActionBarWrapper.this.mActivity).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
                }
                this.mListener.onTabReselected(this, fragmentTransaction2);
                if (fragmentTransaction2 != null && !fragmentTransaction2.isEmpty()) {
                    fragmentTransaction2.commit();
                }
            }
        }

        public void onTabSelected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
            if (this.mListener != null) {
                if (ActionBarWrapper.this.mFragmentTransaction == null && (ActionBarWrapper.this.mActivity instanceof FragmentActivity)) {
                    FragmentTransaction unused = ActionBarWrapper.this.mFragmentTransaction = ((FragmentActivity) ActionBarWrapper.this.mActivity).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
                }
                this.mListener.onTabSelected(this, ActionBarWrapper.this.mFragmentTransaction);
                if (ActionBarWrapper.this.mFragmentTransaction != null) {
                    if (!ActionBarWrapper.this.mFragmentTransaction.isEmpty()) {
                        ActionBarWrapper.this.mFragmentTransaction.commit();
                    }
                    FragmentTransaction unused2 = ActionBarWrapper.this.mFragmentTransaction = null;
                }
            }
        }

        public void onTabUnselected(ActionBar.Tab tab, android.app.FragmentTransaction fragmentTransaction) {
            if (this.mListener != null) {
                FragmentTransaction fragmentTransaction2 = null;
                if (ActionBarWrapper.this.mActivity instanceof FragmentActivity) {
                    fragmentTransaction2 = ((FragmentActivity) ActionBarWrapper.this.mActivity).getSupportFragmentManager().beginTransaction().disallowAddToBackStack();
                    FragmentTransaction unused = ActionBarWrapper.this.mFragmentTransaction = fragmentTransaction2;
                }
                this.mListener.onTabUnselected(this, fragmentTransaction2);
            }
        }
    }

    public ActionBar.Tab newTab() {
        return new TabWrapper(this.mActionBar.newTab());
    }

    public void addTab(ActionBar.Tab tab) {
        this.mActionBar.addTab(((TabWrapper) tab).mNativeTab);
    }

    public void addTab(ActionBar.Tab tab, boolean z) {
        this.mActionBar.addTab(((TabWrapper) tab).mNativeTab, z);
    }

    public void addTab(ActionBar.Tab tab, int i) {
        this.mActionBar.addTab(((TabWrapper) tab).mNativeTab, i);
    }

    public void addTab(ActionBar.Tab tab, int i, boolean z) {
        this.mActionBar.addTab(((TabWrapper) tab).mNativeTab, i, z);
    }

    public void removeTab(ActionBar.Tab tab) {
        this.mActionBar.removeTab(((TabWrapper) tab).mNativeTab);
    }

    public void removeTabAt(int i) {
        this.mActionBar.removeTabAt(i);
    }

    public void removeAllTabs() {
        this.mActionBar.removeAllTabs();
    }

    public void selectTab(ActionBar.Tab tab) {
        this.mActionBar.selectTab(((TabWrapper) tab).mNativeTab);
    }

    public ActionBar.Tab getSelectedTab() {
        ActionBar.Tab selectedTab = this.mActionBar.getSelectedTab();
        if (selectedTab != null) {
            return (ActionBar.Tab) selectedTab.getTag();
        }
        return null;
    }

    public ActionBar.Tab getTabAt(int i) {
        ActionBar.Tab tabAt = this.mActionBar.getTabAt(i);
        if (tabAt != null) {
            return (ActionBar.Tab) tabAt.getTag();
        }
        return null;
    }

    public int getTabCount() {
        return this.mActionBar.getTabCount();
    }

    public int getHeight() {
        return this.mActionBar.getHeight();
    }

    public void show() {
        this.mActionBar.show();
    }

    public void hide() {
        this.mActionBar.hide();
    }

    public boolean isShowing() {
        return this.mActionBar.isShowing();
    }

    public void addOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        this.mMenuVisibilityListeners.add(onMenuVisibilityListener);
    }

    public void removeOnMenuVisibilityListener(ActionBar.OnMenuVisibilityListener onMenuVisibilityListener) {
        this.mMenuVisibilityListeners.remove(onMenuVisibilityListener);
    }

    public void onMenuVisibilityChanged(boolean z) {
        for (ActionBar.OnMenuVisibilityListener onMenuVisibilityChanged : this.mMenuVisibilityListeners) {
            onMenuVisibilityChanged.onMenuVisibilityChanged(z);
        }
    }
}
