package com.actionbarsherlock.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

public class TabsLinearLayout extends IcsLinearLayout {
    private static final int LinearLayout_measureWithLargestChild = 0;
    private static final int[] R_styleable_LinearLayout = {16843476};
    private boolean mUseLargestChild;

    public TabsLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R_styleable_LinearLayout);
        this.mUseLargestChild = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
    }

    public boolean isMeasureWithLargestChildEnabled() {
        return this.mUseLargestChild;
    }

    public void setMeasureWithLargestChildEnabled(boolean z) {
        this.mUseLargestChild = z;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (getChildCount() > 2) {
            int mode = View.MeasureSpec.getMode(i);
            if (this.mUseLargestChild && mode == 0 && getOrientation() == 0) {
                useLargestChildHorizontal();
            }
        }
    }

    private void useLargestChildHorizontal() {
        int i;
        int measuredWidth;
        int childCount = getChildCount();
        int i2 = 0;
        int i3 = 0;
        while (i2 < childCount) {
            i2++;
            i3 = Math.max(getChildAt(i2).getMeasuredWidth(), i3);
        }
        int i4 = 0;
        int i5 = 0;
        while (i4 < childCount) {
            View childAt = getChildAt(i4);
            if (childAt == null) {
                i = i5;
            } else if (childAt.getVisibility() == 8) {
                i = i5;
            } else {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) childAt.getLayoutParams();
                if (layoutParams.weight > 0.0f) {
                    childAt.measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), View.MeasureSpec.makeMeasureSpec(childAt.getMeasuredHeight(), 1073741824));
                    measuredWidth = i5 + i3;
                } else {
                    measuredWidth = i5 + childAt.getMeasuredWidth();
                }
                i = layoutParams.rightMargin + layoutParams.leftMargin + measuredWidth;
            }
            i4++;
            i5 = i;
        }
        setMeasuredDimension(getPaddingLeft() + getPaddingRight() + i5, getMeasuredHeight());
    }
}
