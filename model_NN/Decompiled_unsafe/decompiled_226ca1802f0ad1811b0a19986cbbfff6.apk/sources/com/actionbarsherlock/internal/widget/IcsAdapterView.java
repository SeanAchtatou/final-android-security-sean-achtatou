package com.actionbarsherlock.internal.widget;

import android.content.Context;
import android.database.DataSetObserver;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Adapter;
import android.widget.AdapterView;

public abstract class IcsAdapterView<T extends Adapter> extends ViewGroup {
    public static final int INVALID_POSITION = -1;
    public static final long INVALID_ROW_ID = Long.MIN_VALUE;
    public static final int ITEM_VIEW_TYPE_HEADER_OR_FOOTER = -2;
    public static final int ITEM_VIEW_TYPE_IGNORE = -1;
    static final int SYNC_FIRST_POSITION = 1;
    static final int SYNC_MAX_DURATION_MILLIS = 100;
    static final int SYNC_SELECTED_POSITION = 0;
    boolean mBlockLayoutRequests = false;
    boolean mDataChanged;
    private boolean mDesiredFocusableInTouchModeState;
    private boolean mDesiredFocusableState;
    private View mEmptyView;
    @ViewDebug.ExportedProperty(category = "scrolling")
    int mFirstPosition = 0;
    boolean mInLayout = false;
    @ViewDebug.ExportedProperty(category = "list")
    int mItemCount;
    private int mLayoutHeight;
    boolean mNeedSync = false;
    @ViewDebug.ExportedProperty(category = "list")
    int mNextSelectedPosition = -1;
    long mNextSelectedRowId = Long.MIN_VALUE;
    int mOldItemCount;
    int mOldSelectedPosition = -1;
    long mOldSelectedRowId = Long.MIN_VALUE;
    AdapterView.OnItemClickListener mOnItemClickListener;
    OnItemLongClickListener mOnItemLongClickListener;
    OnItemSelectedListener mOnItemSelectedListener;
    @ViewDebug.ExportedProperty(category = "list")
    int mSelectedPosition = -1;
    long mSelectedRowId = Long.MIN_VALUE;
    private IcsAdapterView<T>.SelectionNotifier mSelectionNotifier;
    int mSpecificTop;
    long mSyncHeight;
    int mSyncMode;
    int mSyncPosition;
    long mSyncRowId = Long.MIN_VALUE;

    public interface OnItemLongClickListener {
        boolean onItemLongClick(IcsAdapterView<?> icsAdapterView, View view, int i, long j);
    }

    public interface OnItemSelectedListener {
        void onItemSelected(IcsAdapterView<?> icsAdapterView, View view, int i, long j);

        void onNothingSelected(IcsAdapterView<?> icsAdapterView);
    }

    public abstract T getAdapter();

    public abstract View getSelectedView();

    public abstract void setAdapter(Adapter adapter);

    public abstract void setSelection(int i);

    public IcsAdapterView(Context context) {
        super(context);
    }

    public IcsAdapterView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public IcsAdapterView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    public final AdapterView.OnItemClickListener getOnItemClickListener() {
        return this.mOnItemClickListener;
    }

    public boolean performItemClick(View view, int i, long j) {
        if (this.mOnItemClickListener == null) {
            return false;
        }
        playSoundEffect(0);
        if (view != null) {
            view.sendAccessibilityEvent(1);
        }
        this.mOnItemClickListener.onItemClick(null, view, i, j);
        return true;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener onItemLongClickListener) {
        if (!isLongClickable()) {
            setLongClickable(true);
        }
        this.mOnItemLongClickListener = onItemLongClickListener;
    }

    public final OnItemLongClickListener getOnItemLongClickListener() {
        return this.mOnItemLongClickListener;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener) {
        this.mOnItemSelectedListener = onItemSelectedListener;
    }

    public final OnItemSelectedListener getOnItemSelectedListener() {
        return this.mOnItemSelectedListener;
    }

    public class AdapterContextMenuInfo implements ContextMenu.ContextMenuInfo {
        public long id;
        public int position;
        public View targetView;

        public AdapterContextMenuInfo(View view, int i, long j) {
            this.targetView = view;
            this.position = i;
            this.id = j;
        }
    }

    public void addView(View view) {
        throw new UnsupportedOperationException("addView(View) is not supported in AdapterView");
    }

    public void addView(View view, int i) {
        throw new UnsupportedOperationException("addView(View, int) is not supported in AdapterView");
    }

    public void addView(View view, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, LayoutParams) is not supported in AdapterView");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        throw new UnsupportedOperationException("addView(View, int, LayoutParams) is not supported in AdapterView");
    }

    public void removeView(View view) {
        throw new UnsupportedOperationException("removeView(View) is not supported in AdapterView");
    }

    public void removeViewAt(int i) {
        throw new UnsupportedOperationException("removeViewAt(int) is not supported in AdapterView");
    }

    public void removeAllViews() {
        throw new UnsupportedOperationException("removeAllViews() is not supported in AdapterView");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        this.mLayoutHeight = getHeight();
    }

    @ViewDebug.CapturedViewProperty
    public int getSelectedItemPosition() {
        return this.mNextSelectedPosition;
    }

    @ViewDebug.CapturedViewProperty
    public long getSelectedItemId() {
        return this.mNextSelectedRowId;
    }

    public Object getSelectedItem() {
        Adapter adapter = getAdapter();
        int selectedItemPosition = getSelectedItemPosition();
        if (adapter == null || adapter.getCount() <= 0 || selectedItemPosition < 0) {
            return null;
        }
        return adapter.getItem(selectedItemPosition);
    }

    @ViewDebug.CapturedViewProperty
    public int getCount() {
        return this.mItemCount;
    }

    public int getPositionForView(View view) {
        while (true) {
            try {
                View view2 = (View) view.getParent();
                if (view2.equals(this)) {
                    break;
                }
                view = view2;
            } catch (ClassCastException e) {
                return -1;
            }
        }
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (getChildAt(i).equals(view)) {
                return i + this.mFirstPosition;
            }
        }
        return -1;
    }

    public int getFirstVisiblePosition() {
        return this.mFirstPosition;
    }

    public int getLastVisiblePosition() {
        return (this.mFirstPosition + getChildCount()) - 1;
    }

    public void setEmptyView(View view) {
        this.mEmptyView = view;
        Adapter adapter = getAdapter();
        updateEmptyStatus(adapter == null || adapter.isEmpty());
    }

    public View getEmptyView() {
        return this.mEmptyView;
    }

    /* access modifiers changed from: package-private */
    public boolean isInFilterMode() {
        return false;
    }

    public void setFocusable(boolean z) {
        boolean z2 = true;
        Adapter adapter = getAdapter();
        boolean z3 = adapter == null || adapter.getCount() == 0;
        this.mDesiredFocusableState = z;
        if (!z) {
            this.mDesiredFocusableInTouchModeState = false;
        }
        if (!z || (z3 && !isInFilterMode())) {
            z2 = false;
        }
        super.setFocusable(z2);
    }

    public void setFocusableInTouchMode(boolean z) {
        boolean z2 = true;
        Adapter adapter = getAdapter();
        boolean z3 = adapter == null || adapter.getCount() == 0;
        this.mDesiredFocusableInTouchModeState = z;
        if (z) {
            this.mDesiredFocusableState = true;
        }
        if (!z || (z3 && !isInFilterMode())) {
            z2 = false;
        }
        super.setFocusableInTouchMode(z2);
    }

    /* access modifiers changed from: package-private */
    public void checkFocus() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        Adapter adapter = getAdapter();
        if (!(adapter == null || adapter.getCount() == 0) || isInFilterMode()) {
            z = true;
        } else {
            z = false;
        }
        if (!z || !this.mDesiredFocusableInTouchModeState) {
            z2 = false;
        } else {
            z2 = true;
        }
        super.setFocusableInTouchMode(z2);
        if (!z || !this.mDesiredFocusableState) {
            z3 = false;
        } else {
            z3 = true;
        }
        super.setFocusable(z3);
        if (this.mEmptyView != null) {
            if (adapter == null || adapter.isEmpty()) {
                z4 = true;
            }
            updateEmptyStatus(z4);
        }
    }

    private void updateEmptyStatus(boolean z) {
        if (isInFilterMode()) {
            z = false;
        }
        if (z) {
            if (this.mEmptyView != null) {
                this.mEmptyView.setVisibility(0);
                setVisibility(8);
            } else {
                setVisibility(0);
            }
            if (this.mDataChanged) {
                onLayout(false, getLeft(), getTop(), getRight(), getBottom());
                return;
            }
            return;
        }
        if (this.mEmptyView != null) {
            this.mEmptyView.setVisibility(8);
        }
        setVisibility(0);
    }

    public Object getItemAtPosition(int i) {
        Adapter adapter = getAdapter();
        if (adapter == null || i < 0) {
            return null;
        }
        return adapter.getItem(i);
    }

    public long getItemIdAtPosition(int i) {
        Adapter adapter = getAdapter();
        if (adapter == null || i < 0) {
            return Long.MIN_VALUE;
        }
        return adapter.getItemId(i);
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new RuntimeException("Don't call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead");
    }

    /* access modifiers changed from: protected */
    public void dispatchSaveInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchFreezeSelfOnly(sparseArray);
    }

    /* access modifiers changed from: protected */
    public void dispatchRestoreInstanceState(SparseArray<Parcelable> sparseArray) {
        dispatchThawSelfOnly(sparseArray);
    }

    class AdapterDataSetObserver extends DataSetObserver {
        private Parcelable mInstanceState = null;

        AdapterDataSetObserver() {
        }

        public void onChanged() {
            IcsAdapterView.this.mDataChanged = true;
            IcsAdapterView.this.mOldItemCount = IcsAdapterView.this.mItemCount;
            IcsAdapterView.this.mItemCount = IcsAdapterView.this.getAdapter().getCount();
            if (!IcsAdapterView.this.getAdapter().hasStableIds() || this.mInstanceState == null || IcsAdapterView.this.mOldItemCount != 0 || IcsAdapterView.this.mItemCount <= 0) {
                IcsAdapterView.this.rememberSyncState();
            } else {
                IcsAdapterView.this.onRestoreInstanceState(this.mInstanceState);
                this.mInstanceState = null;
            }
            IcsAdapterView.this.checkFocus();
            IcsAdapterView.this.requestLayout();
        }

        public void onInvalidated() {
            IcsAdapterView.this.mDataChanged = true;
            if (IcsAdapterView.this.getAdapter().hasStableIds()) {
                this.mInstanceState = IcsAdapterView.this.onSaveInstanceState();
            }
            IcsAdapterView.this.mOldItemCount = IcsAdapterView.this.mItemCount;
            IcsAdapterView.this.mItemCount = 0;
            IcsAdapterView.this.mSelectedPosition = -1;
            IcsAdapterView.this.mSelectedRowId = Long.MIN_VALUE;
            IcsAdapterView.this.mNextSelectedPosition = -1;
            IcsAdapterView.this.mNextSelectedRowId = Long.MIN_VALUE;
            IcsAdapterView.this.mNeedSync = false;
            IcsAdapterView.this.checkFocus();
            IcsAdapterView.this.requestLayout();
        }

        public void clearSavedState() {
            this.mInstanceState = null;
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.mSelectionNotifier);
    }

    class SelectionNotifier implements Runnable {
        private SelectionNotifier() {
        }

        public void run() {
            if (!IcsAdapterView.this.mDataChanged) {
                IcsAdapterView.this.fireOnSelected();
            } else if (IcsAdapterView.this.getAdapter() != null) {
                IcsAdapterView.this.post(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void selectionChanged() {
        if (this.mOnItemSelectedListener != null) {
            if (this.mInLayout || this.mBlockLayoutRequests) {
                if (this.mSelectionNotifier == null) {
                    this.mSelectionNotifier = new SelectionNotifier();
                }
                post(this.mSelectionNotifier);
            } else {
                fireOnSelected();
            }
        }
        if (this.mSelectedPosition != -1 && isShown() && !isInTouchMode()) {
            sendAccessibilityEvent(4);
        }
    }

    /* access modifiers changed from: private */
    public void fireOnSelected() {
        if (this.mOnItemSelectedListener != null) {
            int selectedItemPosition = getSelectedItemPosition();
            if (selectedItemPosition >= 0) {
                this.mOnItemSelectedListener.onItemSelected(this, getSelectedView(), selectedItemPosition, getAdapter().getItemId(selectedItemPosition));
                return;
            }
            this.mOnItemSelectedListener.onNothingSelected(this);
        }
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        View selectedView = getSelectedView();
        if (selectedView == null || selectedView.getVisibility() != 0 || !selectedView.dispatchPopulateAccessibilityEvent(accessibilityEvent)) {
            return false;
        }
        return true;
    }

    public boolean onRequestSendAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        if (!super.onRequestSendAccessibilityEvent(view, accessibilityEvent)) {
            return false;
        }
        AccessibilityEvent obtain = AccessibilityEvent.obtain();
        onInitializeAccessibilityEvent(obtain);
        view.dispatchPopulateAccessibilityEvent(obtain);
        accessibilityEvent.appendRecord(obtain);
        return true;
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setScrollable(isScrollableForAccessibility());
        View selectedView = getSelectedView();
        if (selectedView != null) {
            accessibilityNodeInfo.setEnabled(selectedView.isEnabled());
        }
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setScrollable(isScrollableForAccessibility());
        View selectedView = getSelectedView();
        if (selectedView != null) {
            accessibilityEvent.setEnabled(selectedView.isEnabled());
        }
        accessibilityEvent.setCurrentItemIndex(getSelectedItemPosition());
        accessibilityEvent.setFromIndex(getFirstVisiblePosition());
        accessibilityEvent.setToIndex(getLastVisiblePosition());
        accessibilityEvent.setItemCount(getCount());
    }

    private boolean isScrollableForAccessibility() {
        int count;
        Adapter adapter = getAdapter();
        if (adapter == null || (count = adapter.getCount()) <= 0) {
            return false;
        }
        if (getFirstVisiblePosition() > 0 || getLastVisiblePosition() < count - 1) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean canAnimate() {
        return super.canAnimate() && this.mItemCount > 0;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleDataChanged() {
        /*
            r8 = this;
            r6 = -9223372036854775808
            r5 = -1
            r2 = 1
            r1 = 0
            int r4 = r8.mItemCount
            if (r4 <= 0) goto L_0x0055
            boolean r0 = r8.mNeedSync
            if (r0 == 0) goto L_0x0053
            r8.mNeedSync = r1
            int r0 = r8.findSyncPosition()
            if (r0 < 0) goto L_0x0053
            int r3 = r8.lookForSelectablePosition(r0, r2)
            if (r3 != r0) goto L_0x0053
            r8.setNextSelectedPositionInt(r0)
            r3 = r2
        L_0x001f:
            if (r3 != 0) goto L_0x004f
            int r0 = r8.getSelectedItemPosition()
            if (r0 < r4) goto L_0x0029
            int r0 = r4 + -1
        L_0x0029:
            if (r0 >= 0) goto L_0x002c
            r0 = r1
        L_0x002c:
            int r4 = r8.lookForSelectablePosition(r0, r2)
            if (r4 >= 0) goto L_0x0051
            int r0 = r8.lookForSelectablePosition(r0, r1)
        L_0x0036:
            if (r0 < 0) goto L_0x004f
            r8.setNextSelectedPositionInt(r0)
            r8.checkSelectionChanged()
            r0 = r2
        L_0x003f:
            if (r0 != 0) goto L_0x004e
            r8.mSelectedPosition = r5
            r8.mSelectedRowId = r6
            r8.mNextSelectedPosition = r5
            r8.mNextSelectedRowId = r6
            r8.mNeedSync = r1
            r8.checkSelectionChanged()
        L_0x004e:
            return
        L_0x004f:
            r0 = r3
            goto L_0x003f
        L_0x0051:
            r0 = r4
            goto L_0x0036
        L_0x0053:
            r3 = r1
            goto L_0x001f
        L_0x0055:
            r0 = r1
            goto L_0x003f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.internal.widget.IcsAdapterView.handleDataChanged():void");
    }

    /* access modifiers changed from: package-private */
    public void checkSelectionChanged() {
        if (this.mSelectedPosition != this.mOldSelectedPosition || this.mSelectedRowId != this.mOldSelectedRowId) {
            selectionChanged();
            this.mOldSelectedPosition = this.mSelectedPosition;
            this.mOldSelectedRowId = this.mSelectedRowId;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: CFG modification limit reached, blocks count: 140 */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005c, code lost:
        if (r8 != false) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
        if (r0 != false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0060, code lost:
        if (r7 != false) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0062, code lost:
        r0 = r4 - 1;
        r4 = r0;
        r5 = r0;
        r0 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int findSyncPosition() {
        /*
            r15 = this;
            r2 = 1
            r6 = -1
            r1 = 0
            int r9 = r15.mItemCount
            if (r9 != 0) goto L_0x0009
            r5 = r6
        L_0x0008:
            return r5
        L_0x0009:
            long r10 = r15.mSyncRowId
            int r0 = r15.mSyncPosition
            r3 = -9223372036854775808
            int r3 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r3 != 0) goto L_0x0015
            r5 = r6
            goto L_0x0008
        L_0x0015:
            int r0 = java.lang.Math.max(r1, r0)
            int r3 = r9 + -1
            int r0 = java.lang.Math.min(r3, r0)
            long r3 = android.os.SystemClock.uptimeMillis()
            r7 = 100
            long r12 = r3 + r7
            android.widget.Adapter r14 = r15.getAdapter()
            if (r14 != 0) goto L_0x0068
            r5 = r6
            goto L_0x0008
        L_0x002f:
            if (r7 != 0) goto L_0x0035
            if (r0 == 0) goto L_0x005c
            if (r8 != 0) goto L_0x005c
        L_0x0035:
            int r0 = r3 + 1
            r3 = r0
            r5 = r0
            r0 = r1
        L_0x003a:
            long r7 = android.os.SystemClock.uptimeMillis()
            int r7 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r7 > 0) goto L_0x0056
            long r7 = r14.getItemId(r5)
            int r7 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r7 == 0) goto L_0x0008
            int r7 = r9 + -1
            if (r3 != r7) goto L_0x0058
            r8 = r2
        L_0x004f:
            if (r4 != 0) goto L_0x005a
            r7 = r2
        L_0x0052:
            if (r8 == 0) goto L_0x002f
            if (r7 == 0) goto L_0x002f
        L_0x0056:
            r5 = r6
            goto L_0x0008
        L_0x0058:
            r8 = r1
            goto L_0x004f
        L_0x005a:
            r7 = r1
            goto L_0x0052
        L_0x005c:
            if (r8 != 0) goto L_0x0062
            if (r0 != 0) goto L_0x003a
            if (r7 != 0) goto L_0x003a
        L_0x0062:
            int r0 = r4 + -1
            r4 = r0
            r5 = r0
            r0 = r2
            goto L_0x003a
        L_0x0068:
            r3 = r0
            r4 = r0
            r5 = r0
            r0 = r1
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.internal.widget.IcsAdapterView.findSyncPosition():int");
    }

    /* access modifiers changed from: package-private */
    public int lookForSelectablePosition(int i, boolean z) {
        return i;
    }

    /* access modifiers changed from: package-private */
    public void setSelectedPositionInt(int i) {
        this.mSelectedPosition = i;
        this.mSelectedRowId = getItemIdAtPosition(i);
    }

    /* access modifiers changed from: package-private */
    public void setNextSelectedPositionInt(int i) {
        this.mNextSelectedPosition = i;
        this.mNextSelectedRowId = getItemIdAtPosition(i);
        if (this.mNeedSync && this.mSyncMode == 0 && i >= 0) {
            this.mSyncPosition = i;
            this.mSyncRowId = this.mNextSelectedRowId;
        }
    }

    /* access modifiers changed from: package-private */
    public void rememberSyncState() {
        if (getChildCount() > 0) {
            this.mNeedSync = true;
            this.mSyncHeight = (long) this.mLayoutHeight;
            if (this.mSelectedPosition >= 0) {
                View childAt = getChildAt(this.mSelectedPosition - this.mFirstPosition);
                this.mSyncRowId = this.mNextSelectedRowId;
                this.mSyncPosition = this.mNextSelectedPosition;
                if (childAt != null) {
                    this.mSpecificTop = childAt.getTop();
                }
                this.mSyncMode = 0;
                return;
            }
            View childAt2 = getChildAt(0);
            Adapter adapter = getAdapter();
            if (this.mFirstPosition < 0 || this.mFirstPosition >= adapter.getCount()) {
                this.mSyncRowId = -1;
            } else {
                this.mSyncRowId = adapter.getItemId(this.mFirstPosition);
            }
            this.mSyncPosition = this.mFirstPosition;
            if (childAt2 != null) {
                this.mSpecificTop = childAt2.getTop();
            }
            this.mSyncMode = 1;
        }
    }
}
