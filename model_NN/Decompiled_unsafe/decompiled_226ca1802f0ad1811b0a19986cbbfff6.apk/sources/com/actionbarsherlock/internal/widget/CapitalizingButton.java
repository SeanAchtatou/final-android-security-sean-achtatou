package com.actionbarsherlock.internal.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;
import java.util.Locale;

public class CapitalizingButton extends Button {
    private static final boolean IS_GINGERBREAD;
    private static final int[] R_styleable_Button = {16842804};
    private static final int R_styleable_Button_textAppearance = 0;
    private static final int[] R_styleable_TextAppearance = {16843660};
    private static final int R_styleable_TextAppearance_textAllCaps = 0;
    private static final boolean SANS_ICE_CREAM = (Build.VERSION.SDK_INT < 14);
    private boolean mAllCaps;

    static {
        boolean z;
        if (Build.VERSION.SDK_INT >= 9) {
            z = true;
        } else {
            z = false;
        }
        IS_GINGERBREAD = z;
    }

    public CapitalizingButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes;
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, R_styleable_Button);
        int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
        obtainStyledAttributes2.recycle();
        if (resourceId != -1 && (obtainStyledAttributes = context.obtainStyledAttributes(resourceId, R_styleable_TextAppearance)) != null) {
            this.mAllCaps = obtainStyledAttributes.getBoolean(0, true);
            obtainStyledAttributes.recycle();
        }
    }

    public void setTextCompat(CharSequence charSequence) {
        if (!SANS_ICE_CREAM || !this.mAllCaps || charSequence == null) {
            setText(charSequence);
        } else if (IS_GINGERBREAD) {
            try {
                setText(charSequence.toString().toUpperCase(Locale.ROOT));
            } catch (NoSuchFieldError e) {
                setText(charSequence.toString().toUpperCase());
            }
        } else {
            setText(charSequence.toString().toUpperCase());
        }
    }
}
