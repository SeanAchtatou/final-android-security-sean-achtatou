package com.actionbarsherlock.internal.widget;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.actionbarsherlock.R;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.internal.ResourcesCompat;
import com.actionbarsherlock.internal.view.menu.ActionMenuItem;
import com.actionbarsherlock.internal.view.menu.ActionMenuPresenter;
import com.actionbarsherlock.internal.view.menu.ActionMenuView;
import com.actionbarsherlock.internal.view.menu.MenuBuilder;
import com.actionbarsherlock.internal.view.menu.MenuItemImpl;
import com.actionbarsherlock.internal.view.menu.MenuPresenter;
import com.actionbarsherlock.internal.view.menu.MenuView;
import com.actionbarsherlock.internal.view.menu.SubMenuBuilder;
import com.actionbarsherlock.internal.widget.IcsAdapterView;
import com.actionbarsherlock.view.CollapsibleActionView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.actionbarsherlock.widget.ActivityChooserView;

public class ActionBarView extends AbsActionBarView {
    private static final int DEFAULT_CUSTOM_GRAVITY = 19;
    public static final int DISPLAY_DEFAULT = 0;
    private static final int DISPLAY_RELAYOUT_MASK = 31;
    private static final String TAG = "ActionBarView";
    /* access modifiers changed from: private */
    public ActionBar.OnNavigationListener mCallback;
    private ActionBarContextView mContextView;
    /* access modifiers changed from: private */
    public View mCustomNavView;
    /* access modifiers changed from: private */
    public int mDisplayOptions = -1;
    View mExpandedActionView;
    private final View.OnClickListener mExpandedActionViewUpListener = new View.OnClickListener() {
        public void onClick(View view) {
            MenuItemImpl menuItemImpl = ActionBarView.this.mExpandedMenuPresenter.mCurrentExpandedItem;
            if (menuItemImpl != null) {
                menuItemImpl.collapseActionView();
            }
        }
    };
    /* access modifiers changed from: private */
    public HomeView mExpandedHomeLayout;
    /* access modifiers changed from: private */
    public ExpandedActionViewMenuPresenter mExpandedMenuPresenter;
    /* access modifiers changed from: private */
    public HomeView mHomeLayout;
    /* access modifiers changed from: private */
    public Drawable mIcon;
    private boolean mIncludeTabs;
    private int mIndeterminateProgressStyle;
    private IcsProgressBar mIndeterminateProgressView;
    private boolean mIsCollapsable;
    private boolean mIsCollapsed;
    private int mItemPadding;
    private IcsLinearLayout mListNavLayout;
    private Drawable mLogo;
    /* access modifiers changed from: private */
    public ActionMenuItem mLogoNavItem;
    private final IcsAdapterView.OnItemSelectedListener mNavItemSelectedListener = new IcsAdapterView.OnItemSelectedListener() {
        public void onItemSelected(IcsAdapterView icsAdapterView, View view, int i, long j) {
            if (ActionBarView.this.mCallback != null) {
                ActionBarView.this.mCallback.onNavigationItemSelected(i, j);
            }
        }

        public void onNothingSelected(IcsAdapterView icsAdapterView) {
        }
    };
    /* access modifiers changed from: private */
    public int mNavigationMode;
    private MenuBuilder mOptionsMenu;
    private int mProgressBarPadding;
    private int mProgressStyle;
    private IcsProgressBar mProgressView;
    /* access modifiers changed from: private */
    public IcsSpinner mSpinner;
    private SpinnerAdapter mSpinnerAdapter;
    private CharSequence mSubtitle;
    private int mSubtitleStyleRes;
    private TextView mSubtitleView;
    /* access modifiers changed from: private */
    public ScrollingTabContainerView mTabScrollView;
    private CharSequence mTitle;
    /* access modifiers changed from: private */
    public LinearLayout mTitleLayout;
    private int mTitleStyleRes;
    private View mTitleUpView;
    private TextView mTitleView;
    private final View.OnClickListener mUpClickListener = new View.OnClickListener() {
        public void onClick(View view) {
            ActionBarView.this.mWindowCallback.onMenuItemSelected(0, ActionBarView.this.mLogoNavItem);
        }
    };
    private boolean mUserTitle;
    Window.Callback mWindowCallback;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.actionbarsherlock.internal.widget.ActionBarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ActionBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int loadLogoFromManifest;
        setBackgroundResource(0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.SherlockActionBar, R.attr.actionBarStyle, 0);
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        PackageManager packageManager = context.getPackageManager();
        this.mNavigationMode = obtainStyledAttributes.getInt(6, 0);
        this.mTitle = obtainStyledAttributes.getText(8);
        this.mSubtitle = obtainStyledAttributes.getText(9);
        this.mLogo = obtainStyledAttributes.getDrawable(11);
        if (this.mLogo == null) {
            if (Build.VERSION.SDK_INT >= 11) {
                if (context instanceof Activity) {
                    try {
                        this.mLogo = packageManager.getActivityLogo(((Activity) context).getComponentName());
                    } catch (PackageManager.NameNotFoundException e) {
                        Log.e(TAG, "Activity component name not found!", e);
                    }
                }
                if (this.mLogo == null) {
                    this.mLogo = applicationInfo.loadLogo(packageManager);
                }
            } else if ((context instanceof Activity) && (loadLogoFromManifest = ResourcesCompat.loadLogoFromManifest((Activity) context)) != 0) {
                this.mLogo = context.getResources().getDrawable(loadLogoFromManifest);
            }
        }
        this.mIcon = obtainStyledAttributes.getDrawable(10);
        if (this.mIcon == null) {
            if (context instanceof Activity) {
                try {
                    this.mIcon = packageManager.getActivityIcon(((Activity) context).getComponentName());
                } catch (PackageManager.NameNotFoundException e2) {
                    Log.e(TAG, "Activity component name not found!", e2);
                }
            }
            if (this.mIcon == null) {
                this.mIcon = applicationInfo.loadIcon(packageManager);
            }
        }
        LayoutInflater from = LayoutInflater.from(context);
        int resourceId = obtainStyledAttributes.getResourceId(14, R.layout.abs__action_bar_home);
        this.mHomeLayout = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.mExpandedHomeLayout = (HomeView) from.inflate(resourceId, (ViewGroup) this, false);
        this.mExpandedHomeLayout.setUp(true);
        this.mExpandedHomeLayout.setOnClickListener(this.mExpandedActionViewUpListener);
        this.mExpandedHomeLayout.setContentDescription(getResources().getText(R.string.abs__action_bar_up_description));
        this.mTitleStyleRes = obtainStyledAttributes.getResourceId(5, 0);
        this.mSubtitleStyleRes = obtainStyledAttributes.getResourceId(4, 0);
        this.mProgressStyle = obtainStyledAttributes.getResourceId(15, 0);
        this.mIndeterminateProgressStyle = obtainStyledAttributes.getResourceId(16, 0);
        this.mProgressBarPadding = obtainStyledAttributes.getDimensionPixelOffset(17, 0);
        this.mItemPadding = obtainStyledAttributes.getDimensionPixelOffset(18, 0);
        setDisplayOptions(obtainStyledAttributes.getInt(7, 0));
        int resourceId2 = obtainStyledAttributes.getResourceId(13, 0);
        if (resourceId2 != 0) {
            this.mCustomNavView = from.inflate(resourceId2, (ViewGroup) this, false);
            this.mNavigationMode = 0;
            setDisplayOptions(this.mDisplayOptions | 16);
        }
        this.mContentHeight = obtainStyledAttributes.getLayoutDimension(3, 0);
        obtainStyledAttributes.recycle();
        this.mLogoNavItem = new ActionMenuItem(context, 0, 16908332, 0, 0, this.mTitle);
        this.mHomeLayout.setOnClickListener(this.mUpClickListener);
        this.mHomeLayout.setClickable(true);
        this.mHomeLayout.setFocusable(true);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.mTitleView = null;
        this.mSubtitleView = null;
        this.mTitleUpView = null;
        if (this.mTitleLayout != null && this.mTitleLayout.getParent() == this) {
            removeView(this.mTitleLayout);
        }
        this.mTitleLayout = null;
        if ((this.mDisplayOptions & 8) != 0) {
            initTitle();
        }
        if (this.mTabScrollView != null && this.mIncludeTabs) {
            ViewGroup.LayoutParams layoutParams = this.mTabScrollView.getLayoutParams();
            if (layoutParams != null) {
                layoutParams.width = -2;
                layoutParams.height = -1;
            }
            this.mTabScrollView.setAllowCollapse(true);
        }
    }

    public void setWindowCallback(Window.Callback callback) {
        this.mWindowCallback = callback;
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.mActionMenuPresenter != null) {
            this.mActionMenuPresenter.hideOverflowMenu();
            this.mActionMenuPresenter.hideSubMenus();
        }
    }

    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public void initProgress() {
        this.mProgressView = new IcsProgressBar(this.mContext, null, 0, this.mProgressStyle);
        this.mProgressView.setId(R.id.abs__progress_horizontal);
        this.mProgressView.setMax(10000);
        addView(this.mProgressView);
    }

    public void initIndeterminateProgress() {
        this.mIndeterminateProgressView = new IcsProgressBar(this.mContext, null, 0, this.mIndeterminateProgressStyle);
        this.mIndeterminateProgressView.setId(R.id.abs__progress_circular);
        addView(this.mIndeterminateProgressView);
    }

    public void setSplitActionBar(boolean z) {
        if (this.mSplitActionBar != z) {
            if (this.mMenuView != null) {
                ViewGroup viewGroup = (ViewGroup) this.mMenuView.getParent();
                if (viewGroup != null) {
                    viewGroup.removeView(this.mMenuView);
                }
                if (!z) {
                    addView(this.mMenuView);
                } else if (this.mSplitView != null) {
                    this.mSplitView.addView(this.mMenuView);
                }
            }
            if (this.mSplitView != null) {
                this.mSplitView.setVisibility(z ? 0 : 8);
            }
            super.setSplitActionBar(z);
        }
    }

    public boolean isSplitActionBar() {
        return this.mSplitActionBar;
    }

    public boolean hasEmbeddedTabs() {
        return this.mIncludeTabs;
    }

    public void setEmbeddedTabView(ScrollingTabContainerView scrollingTabContainerView) {
        if (this.mTabScrollView != null) {
            removeView(this.mTabScrollView);
        }
        this.mTabScrollView = scrollingTabContainerView;
        this.mIncludeTabs = scrollingTabContainerView != null;
        if (this.mIncludeTabs && this.mNavigationMode == 2) {
            addView(this.mTabScrollView);
            ViewGroup.LayoutParams layoutParams = this.mTabScrollView.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -1;
            scrollingTabContainerView.setAllowCollapse(true);
        }
    }

    public void setCallback(ActionBar.OnNavigationListener onNavigationListener) {
        this.mCallback = onNavigationListener;
    }

    public void setMenu(Menu menu, MenuPresenter.Callback callback) {
        ActionMenuView actionMenuView;
        ViewGroup viewGroup;
        if (menu != this.mOptionsMenu) {
            if (this.mOptionsMenu != null) {
                this.mOptionsMenu.removeMenuPresenter(this.mActionMenuPresenter);
                this.mOptionsMenu.removeMenuPresenter(this.mExpandedMenuPresenter);
            }
            MenuBuilder menuBuilder = (MenuBuilder) menu;
            this.mOptionsMenu = menuBuilder;
            if (!(this.mMenuView == null || (viewGroup = (ViewGroup) this.mMenuView.getParent()) == null)) {
                viewGroup.removeView(this.mMenuView);
            }
            if (this.mActionMenuPresenter == null) {
                this.mActionMenuPresenter = new ActionMenuPresenter(this.mContext);
                this.mActionMenuPresenter.setCallback(callback);
                this.mActionMenuPresenter.setId(R.id.abs__action_menu_presenter);
                this.mExpandedMenuPresenter = new ExpandedActionViewMenuPresenter();
            }
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -1);
            if (!this.mSplitActionBar) {
                this.mActionMenuPresenter.setExpandedActionViewsExclusive(ResourcesCompat.getResources_getBoolean(getContext(), R.bool.abs__action_bar_expanded_action_views_exclusive));
                configPresenters(menuBuilder);
                actionMenuView = (ActionMenuView) this.mActionMenuPresenter.getMenuView(this);
                ViewGroup viewGroup2 = (ViewGroup) actionMenuView.getParent();
                if (!(viewGroup2 == null || viewGroup2 == this)) {
                    viewGroup2.removeView(actionMenuView);
                }
                addView(actionMenuView, layoutParams);
            } else {
                this.mActionMenuPresenter.setExpandedActionViewsExclusive(false);
                this.mActionMenuPresenter.setWidthLimit(getContext().getResources().getDisplayMetrics().widthPixels, true);
                this.mActionMenuPresenter.setItemLimit(ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED);
                layoutParams.width = -1;
                configPresenters(menuBuilder);
                actionMenuView = (ActionMenuView) this.mActionMenuPresenter.getMenuView(this);
                if (this.mSplitView != null) {
                    ViewGroup viewGroup3 = (ViewGroup) actionMenuView.getParent();
                    if (!(viewGroup3 == null || viewGroup3 == this.mSplitView)) {
                        viewGroup3.removeView(actionMenuView);
                    }
                    actionMenuView.setVisibility(getAnimatedVisibility());
                    this.mSplitView.addView(actionMenuView, layoutParams);
                } else {
                    actionMenuView.setLayoutParams(layoutParams);
                }
            }
            this.mMenuView = actionMenuView;
        }
    }

    private void configPresenters(MenuBuilder menuBuilder) {
        if (menuBuilder != null) {
            menuBuilder.addMenuPresenter(this.mActionMenuPresenter);
            menuBuilder.addMenuPresenter(this.mExpandedMenuPresenter);
            return;
        }
        this.mActionMenuPresenter.initForMenu(this.mContext, null);
        this.mExpandedMenuPresenter.initForMenu(this.mContext, null);
        this.mActionMenuPresenter.updateMenuView(true);
        this.mExpandedMenuPresenter.updateMenuView(true);
    }

    public boolean hasExpandedActionView() {
        return (this.mExpandedMenuPresenter == null || this.mExpandedMenuPresenter.mCurrentExpandedItem == null) ? false : true;
    }

    public void collapseActionView() {
        MenuItemImpl menuItemImpl = this.mExpandedMenuPresenter == null ? null : this.mExpandedMenuPresenter.mCurrentExpandedItem;
        if (menuItemImpl != null) {
            menuItemImpl.collapseActionView();
        }
    }

    public void setCustomNavigationView(View view) {
        boolean z = (this.mDisplayOptions & 16) != 0;
        if (this.mCustomNavView != null && z) {
            removeView(this.mCustomNavView);
        }
        this.mCustomNavView = view;
        if (this.mCustomNavView != null && z) {
            addView(this.mCustomNavView);
        }
    }

    public CharSequence getTitle() {
        return this.mTitle;
    }

    public void setTitle(CharSequence charSequence) {
        this.mUserTitle = true;
        setTitleImpl(charSequence);
    }

    public void setWindowTitle(CharSequence charSequence) {
        if (!this.mUserTitle) {
            setTitleImpl(charSequence);
        }
    }

    private void setTitleImpl(CharSequence charSequence) {
        int i = 0;
        this.mTitle = charSequence;
        if (this.mTitleView != null) {
            this.mTitleView.setText(charSequence);
            boolean z = this.mExpandedActionView == null && (this.mDisplayOptions & 8) != 0 && (!TextUtils.isEmpty(this.mTitle) || !TextUtils.isEmpty(this.mSubtitle));
            LinearLayout linearLayout = this.mTitleLayout;
            if (!z) {
                i = 8;
            }
            linearLayout.setVisibility(i);
        }
        if (this.mLogoNavItem != null) {
            this.mLogoNavItem.setTitle(charSequence);
        }
    }

    public CharSequence getSubtitle() {
        return this.mSubtitle;
    }

    public void setSubtitle(CharSequence charSequence) {
        boolean z;
        int i = 0;
        this.mSubtitle = charSequence;
        if (this.mSubtitleView != null) {
            this.mSubtitleView.setText(charSequence);
            this.mSubtitleView.setVisibility(charSequence != null ? 0 : 8);
            if (this.mExpandedActionView != null || (this.mDisplayOptions & 8) == 0 || (TextUtils.isEmpty(this.mTitle) && TextUtils.isEmpty(this.mSubtitle))) {
                z = false;
            } else {
                z = true;
            }
            LinearLayout linearLayout = this.mTitleLayout;
            if (!z) {
                i = 8;
            }
            linearLayout.setVisibility(i);
        }
    }

    public void setHomeButtonEnabled(boolean z) {
        this.mHomeLayout.setEnabled(z);
        this.mHomeLayout.setFocusable(z);
        if (!z) {
            this.mHomeLayout.setContentDescription(null);
        } else if ((this.mDisplayOptions & 4) != 0) {
            this.mHomeLayout.setContentDescription(this.mContext.getResources().getText(R.string.abs__action_bar_up_description));
        } else {
            this.mHomeLayout.setContentDescription(this.mContext.getResources().getText(R.string.abs__action_bar_home_description));
        }
    }

    public void setDisplayOptions(int i) {
        boolean z;
        int i2;
        boolean z2;
        boolean z3;
        int i3 = 8;
        int i4 = -1;
        boolean z4 = true;
        if (this.mDisplayOptions != -1) {
            i4 = this.mDisplayOptions ^ i;
        }
        this.mDisplayOptions = i;
        if ((i4 & 31) != 0) {
            if ((i & 2) != 0) {
                z = true;
            } else {
                z = false;
            }
            if (!z || this.mExpandedActionView != null) {
                i2 = 8;
            } else {
                i2 = 0;
            }
            this.mHomeLayout.setVisibility(i2);
            if ((i4 & 4) != 0) {
                if ((i & 4) != 0) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                this.mHomeLayout.setUp(z3);
                if (z3) {
                    setHomeButtonEnabled(true);
                }
            }
            if ((i4 & 1) != 0) {
                this.mHomeLayout.setIcon(this.mLogo != null && (i & 1) != 0 ? this.mLogo : this.mIcon);
            }
            if ((i4 & 8) != 0) {
                if ((i & 8) != 0) {
                    initTitle();
                } else {
                    removeView(this.mTitleLayout);
                }
            }
            if (!(this.mTitleLayout == null || (i4 & 6) == 0)) {
                if ((this.mDisplayOptions & 4) != 0) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                View view = this.mTitleUpView;
                if (!z) {
                    i3 = z2 ? 0 : 4;
                }
                view.setVisibility(i3);
                LinearLayout linearLayout = this.mTitleLayout;
                if (z || !z2) {
                    z4 = false;
                }
                linearLayout.setEnabled(z4);
            }
            if (!((i4 & 16) == 0 || this.mCustomNavView == null)) {
                if ((i & 16) != 0) {
                    addView(this.mCustomNavView);
                } else {
                    removeView(this.mCustomNavView);
                }
            }
            requestLayout();
        } else {
            invalidate();
        }
        if (!this.mHomeLayout.isEnabled()) {
            this.mHomeLayout.setContentDescription(null);
        } else if ((i & 4) != 0) {
            this.mHomeLayout.setContentDescription(this.mContext.getResources().getText(R.string.abs__action_bar_up_description));
        } else {
            this.mHomeLayout.setContentDescription(this.mContext.getResources().getText(R.string.abs__action_bar_home_description));
        }
    }

    public void setIcon(Drawable drawable) {
        this.mIcon = drawable;
        if (drawable == null) {
            return;
        }
        if ((this.mDisplayOptions & 1) == 0 || this.mLogo == null) {
            this.mHomeLayout.setIcon(drawable);
        }
    }

    public void setIcon(int i) {
        setIcon(this.mContext.getResources().getDrawable(i));
    }

    public void setLogo(Drawable drawable) {
        this.mLogo = drawable;
        if (drawable != null && (this.mDisplayOptions & 1) != 0) {
            this.mHomeLayout.setIcon(drawable);
        }
    }

    public void setLogo(int i) {
        setLogo(this.mContext.getResources().getDrawable(i));
    }

    public void setNavigationMode(int i) {
        int i2 = this.mNavigationMode;
        if (i != i2) {
            switch (i2) {
                case 1:
                    if (this.mListNavLayout != null) {
                        removeView(this.mListNavLayout);
                        break;
                    }
                    break;
                case 2:
                    if (this.mTabScrollView != null && this.mIncludeTabs) {
                        removeView(this.mTabScrollView);
                        break;
                    }
            }
            switch (i) {
                case 1:
                    if (this.mSpinner == null) {
                        this.mSpinner = new IcsSpinner(this.mContext, null, R.attr.actionDropDownStyle);
                        this.mListNavLayout = (IcsLinearLayout) LayoutInflater.from(this.mContext).inflate(R.layout.abs__action_bar_tab_bar_view, (ViewGroup) null);
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -1);
                        layoutParams.gravity = 17;
                        this.mListNavLayout.addView(this.mSpinner, layoutParams);
                    }
                    if (this.mSpinner.getAdapter() != this.mSpinnerAdapter) {
                        this.mSpinner.setAdapter(this.mSpinnerAdapter);
                    }
                    this.mSpinner.setOnItemSelectedListener(this.mNavItemSelectedListener);
                    addView(this.mListNavLayout);
                    break;
                case 2:
                    if (this.mTabScrollView != null && this.mIncludeTabs) {
                        addView(this.mTabScrollView);
                        break;
                    }
            }
            this.mNavigationMode = i;
            requestLayout();
        }
    }

    public void setDropdownAdapter(SpinnerAdapter spinnerAdapter) {
        this.mSpinnerAdapter = spinnerAdapter;
        if (this.mSpinner != null) {
            this.mSpinner.setAdapter(spinnerAdapter);
        }
    }

    public SpinnerAdapter getDropdownAdapter() {
        return this.mSpinnerAdapter;
    }

    public void setDropdownSelectedPosition(int i) {
        this.mSpinner.setSelection(i);
    }

    public int getDropdownSelectedPosition() {
        return this.mSpinner.getSelectedItemPosition();
    }

    public View getCustomNavigationView() {
        return this.mCustomNavView;
    }

    public int getNavigationMode() {
        return this.mNavigationMode;
    }

    public int getDisplayOptions() {
        return this.mDisplayOptions;
    }

    /* access modifiers changed from: protected */
    public ViewGroup.LayoutParams generateDefaultLayoutParams() {
        return new ActionBar.LayoutParams(19);
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        ViewParent parent;
        super.onFinishInflate();
        addView(this.mHomeLayout);
        if (this.mCustomNavView != null && (this.mDisplayOptions & 16) != 0 && (parent = this.mCustomNavView.getParent()) != this) {
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.mCustomNavView);
            }
            addView(this.mCustomNavView);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, com.actionbarsherlock.internal.widget.ActionBarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    public void initTitle() {
        boolean z;
        boolean z2 = true;
        if (this.mTitleLayout == null) {
            this.mTitleLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.abs__action_bar_title_item, (ViewGroup) this, false);
            this.mTitleView = (TextView) this.mTitleLayout.findViewById(R.id.abs__action_bar_title);
            this.mSubtitleView = (TextView) this.mTitleLayout.findViewById(R.id.abs__action_bar_subtitle);
            this.mTitleUpView = this.mTitleLayout.findViewById(R.id.abs__up);
            this.mTitleLayout.setOnClickListener(this.mUpClickListener);
            if (this.mTitleStyleRes != 0) {
                this.mTitleView.setTextAppearance(this.mContext, this.mTitleStyleRes);
            }
            if (this.mTitle != null) {
                this.mTitleView.setText(this.mTitle);
            }
            if (this.mSubtitleStyleRes != 0) {
                this.mSubtitleView.setTextAppearance(this.mContext, this.mSubtitleStyleRes);
            }
            if (this.mSubtitle != null) {
                this.mSubtitleView.setText(this.mSubtitle);
                this.mSubtitleView.setVisibility(0);
            }
            boolean z3 = (this.mDisplayOptions & 4) != 0;
            if ((this.mDisplayOptions & 2) != 0) {
                z = true;
            } else {
                z = false;
            }
            this.mTitleUpView.setVisibility(!z ? z3 ? 0 : 4 : 8);
            LinearLayout linearLayout = this.mTitleLayout;
            if (!z3 || z) {
                z2 = false;
            }
            linearLayout.setEnabled(z2);
        }
        addView(this.mTitleLayout);
        if (this.mExpandedActionView != null || (TextUtils.isEmpty(this.mTitle) && TextUtils.isEmpty(this.mSubtitle))) {
            this.mTitleLayout.setVisibility(8);
        }
    }

    public void setContextView(ActionBarContextView actionBarContextView) {
        this.mContextView = actionBarContextView;
    }

    public void setCollapsable(boolean z) {
        this.mIsCollapsable = z;
    }

    public boolean isCollapsed() {
        return this.mIsCollapsed;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x035e  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x039c  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x019d  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01a4  */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x025a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onMeasure(int r20, int r21) {
        /*
            r19 = this;
            int r13 = r19.getChildCount()
            r0 = r19
            boolean r1 = r0.mIsCollapsable
            if (r1 == 0) goto L_0x0045
            r2 = 0
            r1 = 0
            r18 = r1
            r1 = r2
            r2 = r18
        L_0x0011:
            if (r2 >= r13) goto L_0x0036
            r0 = r19
            android.view.View r3 = r0.getChildAt(r2)
            int r4 = r3.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x0033
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r4 = r0.mMenuView
            if (r3 != r4) goto L_0x0031
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r3 = r0.mMenuView
            int r3 = r3.getChildCount()
            if (r3 == 0) goto L_0x0033
        L_0x0031:
            int r1 = r1 + 1
        L_0x0033:
            int r2 = r2 + 1
            goto L_0x0011
        L_0x0036:
            if (r1 != 0) goto L_0x0045
            r1 = 0
            r2 = 0
            r0 = r19
            r0.setMeasuredDimension(r1, r2)
            r1 = 1
            r0 = r19
            r0.mIsCollapsed = r1
        L_0x0044:
            return
        L_0x0045:
            r1 = 0
            r0 = r19
            r0.mIsCollapsed = r1
            int r1 = android.view.View.MeasureSpec.getMode(r20)
            r2 = 1073741824(0x40000000, float:2.0)
            if (r1 == r2) goto L_0x0079
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r19.getClass()
            java.lang.String r3 = r3.getSimpleName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " can only be used "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "with android:layout_width=\"match_parent\" (or fill_parent)"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x0079:
            int r1 = android.view.View.MeasureSpec.getMode(r21)
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 == r2) goto L_0x00a8
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r19.getClass()
            java.lang.String r3 = r3.getSimpleName()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " can only be used "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "with android:layout_height=\"wrap_content\""
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2)
            throw r1
        L_0x00a8:
            int r14 = android.view.View.MeasureSpec.getSize(r20)
            r0 = r19
            int r1 = r0.mContentHeight
            if (r1 <= 0) goto L_0x0271
            r0 = r19
            int r1 = r0.mContentHeight
            r3 = r1
        L_0x00b7:
            int r1 = r19.getPaddingTop()
            int r2 = r19.getPaddingBottom()
            int r15 = r1 + r2
            int r1 = r19.getPaddingLeft()
            int r2 = r19.getPaddingRight()
            int r10 = r3 - r15
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r6 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r4)
            int r1 = r14 - r1
            int r5 = r1 - r2
            int r4 = r5 / 2
            r0 = r19
            android.view.View r1 = r0.mExpandedActionView
            if (r1 == 0) goto L_0x0278
            r0 = r19
            com.actionbarsherlock.internal.widget.ActionBarView$HomeView r1 = r0.mExpandedHomeLayout
        L_0x00e1:
            int r2 = r1.getVisibility()
            r7 = 8
            if (r2 == r7) goto L_0x03ab
            android.view.ViewGroup$LayoutParams r2 = r1.getLayoutParams()
            int r7 = r2.width
            if (r7 >= 0) goto L_0x027e
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r2)
        L_0x00f7:
            r7 = 1073741824(0x40000000, float:2.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r7)
            r1.measure(r2, r7)
            int r2 = r1.getMeasuredWidth()
            int r1 = r1.getLeftOffset()
            int r1 = r1 + r2
            r2 = 0
            int r5 = r5 - r1
            int r2 = java.lang.Math.max(r2, r5)
            r5 = 0
            int r1 = r2 - r1
            int r1 = java.lang.Math.max(r5, r1)
        L_0x0116:
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r5 = r0.mMenuView
            if (r5 == 0) goto L_0x0141
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r5 = r0.mMenuView
            android.view.ViewParent r5 = r5.getParent()
            r0 = r19
            if (r5 != r0) goto L_0x0141
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r5 = r0.mMenuView
            r7 = 0
            r0 = r19
            int r2 = r0.measureChildView(r5, r2, r6, r7)
            r5 = 0
            r0 = r19
            com.actionbarsherlock.internal.view.menu.ActionMenuView r7 = r0.mMenuView
            int r7 = r7.getMeasuredWidth()
            int r4 = r4 - r7
            int r4 = java.lang.Math.max(r5, r4)
        L_0x0141:
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r5 = r0.mIndeterminateProgressView
            if (r5 == 0) goto L_0x016c
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r5 = r0.mIndeterminateProgressView
            int r5 = r5.getVisibility()
            r7 = 8
            if (r5 == r7) goto L_0x016c
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r5 = r0.mIndeterminateProgressView
            r7 = 0
            r0 = r19
            int r2 = r0.measureChildView(r5, r2, r6, r7)
            r5 = 0
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r6 = r0.mIndeterminateProgressView
            int r6 = r6.getMeasuredWidth()
            int r4 = r4 - r6
            int r4 = java.lang.Math.max(r5, r4)
        L_0x016c:
            r0 = r19
            android.widget.LinearLayout r5 = r0.mTitleLayout
            if (r5 == 0) goto L_0x0288
            r0 = r19
            android.widget.LinearLayout r5 = r0.mTitleLayout
            int r5 = r5.getVisibility()
            r6 = 8
            if (r5 == r6) goto L_0x0288
            r0 = r19
            int r5 = r0.mDisplayOptions
            r5 = r5 & 8
            if (r5 == 0) goto L_0x0288
            r5 = 1
        L_0x0187:
            r0 = r19
            android.view.View r6 = r0.mExpandedActionView
            if (r6 != 0) goto L_0x0194
            r0 = r19
            int r6 = r0.mNavigationMode
            switch(r6) {
                case 1: goto L_0x028b;
                case 2: goto L_0x02d5;
                default: goto L_0x0194;
            }
        L_0x0194:
            r6 = r1
            r7 = r2
        L_0x0196:
            r1 = 0
            r0 = r19
            android.view.View r2 = r0.mExpandedActionView
            if (r2 == 0) goto L_0x031f
            r0 = r19
            android.view.View r1 = r0.mExpandedActionView
            r12 = r1
        L_0x01a2:
            if (r12 == 0) goto L_0x022a
            android.view.ViewGroup$LayoutParams r1 = r12.getLayoutParams()
            r0 = r19
            android.view.ViewGroup$LayoutParams r2 = r0.generateLayoutParams(r1)
            boolean r1 = r2 instanceof com.actionbarsherlock.app.ActionBar.LayoutParams
            if (r1 == 0) goto L_0x0334
            r1 = r2
            com.actionbarsherlock.app.ActionBar$LayoutParams r1 = (com.actionbarsherlock.app.ActionBar.LayoutParams) r1
            r11 = r1
        L_0x01b6:
            r8 = 0
            r1 = 0
            if (r11 == 0) goto L_0x01c4
            int r1 = r11.leftMargin
            int r8 = r11.rightMargin
            int r8 = r8 + r1
            int r1 = r11.topMargin
            int r9 = r11.bottomMargin
            int r1 = r1 + r9
        L_0x01c4:
            r0 = r19
            int r9 = r0.mContentHeight
            if (r9 > 0) goto L_0x0338
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
        L_0x01cc:
            r16 = 0
            int r0 = r2.height
            r17 = r0
            if (r17 < 0) goto L_0x01de
            int r0 = r2.height
            r17 = r0
            r0 = r17
            int r10 = java.lang.Math.min(r0, r10)
        L_0x01de:
            int r1 = r10 - r1
            r0 = r16
            int r16 = java.lang.Math.max(r0, r1)
            int r1 = r2.width
            r10 = -2
            if (r1 == r10) goto L_0x0348
            r1 = 1073741824(0x40000000, float:2.0)
        L_0x01ed:
            r17 = 0
            int r10 = r2.width
            if (r10 < 0) goto L_0x034c
            int r10 = r2.width
            int r10 = java.lang.Math.min(r10, r7)
        L_0x01f9:
            int r10 = r10 - r8
            r0 = r17
            int r10 = java.lang.Math.max(r0, r10)
            if (r11 == 0) goto L_0x034f
            int r11 = r11.gravity
        L_0x0204:
            r11 = r11 & 7
            r17 = 1
            r0 = r17
            if (r11 != r0) goto L_0x03a5
            int r2 = r2.width
            r11 = -1
            if (r2 != r11) goto L_0x03a5
            int r2 = java.lang.Math.min(r6, r4)
            int r2 = r2 * 2
        L_0x0217:
            int r1 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r1)
            r0 = r16
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r0, r9)
            r12.measure(r1, r2)
            int r1 = r12.getMeasuredWidth()
            int r1 = r1 + r8
            int r7 = r7 - r1
        L_0x022a:
            r0 = r19
            android.view.View r1 = r0.mExpandedActionView
            if (r1 != 0) goto L_0x0254
            if (r5 == 0) goto L_0x0254
            r0 = r19
            android.widget.LinearLayout r1 = r0.mTitleLayout
            r0 = r19
            int r2 = r0.mContentHeight
            r4 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r4)
            r4 = 0
            r0 = r19
            r0.measureChildView(r1, r7, r2, r4)
            r1 = 0
            r0 = r19
            android.widget.LinearLayout r2 = r0.mTitleLayout
            int r2 = r2.getMeasuredWidth()
            int r2 = r6 - r2
            java.lang.Math.max(r1, r2)
        L_0x0254:
            r0 = r19
            int r1 = r0.mContentHeight
            if (r1 > 0) goto L_0x039c
            r2 = 0
            r1 = 0
            r3 = r1
        L_0x025d:
            if (r3 >= r13) goto L_0x0353
            r0 = r19
            android.view.View r1 = r0.getChildAt(r3)
            int r1 = r1.getMeasuredHeight()
            int r1 = r1 + r15
            if (r1 <= r2) goto L_0x03a2
        L_0x026c:
            int r2 = r3 + 1
            r3 = r2
            r2 = r1
            goto L_0x025d
        L_0x0271:
            int r1 = android.view.View.MeasureSpec.getSize(r21)
            r3 = r1
            goto L_0x00b7
        L_0x0278:
            r0 = r19
            com.actionbarsherlock.internal.widget.ActionBarView$HomeView r1 = r0.mHomeLayout
            goto L_0x00e1
        L_0x027e:
            int r2 = r2.width
            r7 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            goto L_0x00f7
        L_0x0288:
            r5 = 0
            goto L_0x0187
        L_0x028b:
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsLinearLayout r6 = r0.mListNavLayout
            if (r6 == 0) goto L_0x0194
            if (r5 == 0) goto L_0x02d0
            r0 = r19
            int r6 = r0.mItemPadding
            int r6 = r6 * 2
        L_0x0299:
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsLinearLayout r6 = r0.mListNavLayout
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            r6.measure(r7, r8)
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsLinearLayout r6 = r0.mListNavLayout
            int r6 = r6.getMeasuredWidth()
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r6 = r1
            r7 = r2
            goto L_0x0196
        L_0x02d0:
            r0 = r19
            int r6 = r0.mItemPadding
            goto L_0x0299
        L_0x02d5:
            r0 = r19
            com.actionbarsherlock.internal.widget.ScrollingTabContainerView r6 = r0.mTabScrollView
            if (r6 == 0) goto L_0x0194
            if (r5 == 0) goto L_0x031a
            r0 = r19
            int r6 = r0.mItemPadding
            int r6 = r6 * 2
        L_0x02e3:
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r0 = r19
            com.actionbarsherlock.internal.widget.ScrollingTabContainerView r6 = r0.mTabScrollView
            r7 = -2147483648(0xffffffff80000000, float:-0.0)
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r7)
            r8 = 1073741824(0x40000000, float:2.0)
            int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r10, r8)
            r6.measure(r7, r8)
            r0 = r19
            com.actionbarsherlock.internal.widget.ScrollingTabContainerView r6 = r0.mTabScrollView
            int r6 = r6.getMeasuredWidth()
            r7 = 0
            int r2 = r2 - r6
            int r2 = java.lang.Math.max(r7, r2)
            r7 = 0
            int r1 = r1 - r6
            int r1 = java.lang.Math.max(r7, r1)
            r6 = r1
            r7 = r2
            goto L_0x0196
        L_0x031a:
            r0 = r19
            int r6 = r0.mItemPadding
            goto L_0x02e3
        L_0x031f:
            r0 = r19
            int r2 = r0.mDisplayOptions
            r2 = r2 & 16
            if (r2 == 0) goto L_0x03a8
            r0 = r19
            android.view.View r2 = r0.mCustomNavView
            if (r2 == 0) goto L_0x03a8
            r0 = r19
            android.view.View r1 = r0.mCustomNavView
            r12 = r1
            goto L_0x01a2
        L_0x0334:
            r1 = 0
            r11 = r1
            goto L_0x01b6
        L_0x0338:
            int r9 = r2.height
            r16 = -2
            r0 = r16
            if (r9 == r0) goto L_0x0344
            r9 = 1073741824(0x40000000, float:2.0)
            goto L_0x01cc
        L_0x0344:
            r9 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x01cc
        L_0x0348:
            r1 = -2147483648(0xffffffff80000000, float:-0.0)
            goto L_0x01ed
        L_0x034c:
            r10 = r7
            goto L_0x01f9
        L_0x034f:
            r11 = 19
            goto L_0x0204
        L_0x0353:
            r0 = r19
            r0.setMeasuredDimension(r14, r2)
        L_0x0358:
            r0 = r19
            com.actionbarsherlock.internal.widget.ActionBarContextView r1 = r0.mContextView
            if (r1 == 0) goto L_0x0369
            r0 = r19
            com.actionbarsherlock.internal.widget.ActionBarContextView r1 = r0.mContextView
            int r2 = r19.getMeasuredHeight()
            r1.setContentHeight(r2)
        L_0x0369:
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r1 = r0.mProgressView
            if (r1 == 0) goto L_0x0044
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r1 = r0.mProgressView
            int r1 = r1.getVisibility()
            r2 = 8
            if (r1 == r2) goto L_0x0044
            r0 = r19
            com.actionbarsherlock.internal.widget.IcsProgressBar r1 = r0.mProgressView
            r0 = r19
            int r2 = r0.mProgressBarPadding
            int r2 = r2 * 2
            int r2 = r14 - r2
            r3 = 1073741824(0x40000000, float:2.0)
            int r2 = android.view.View.MeasureSpec.makeMeasureSpec(r2, r3)
            int r3 = r19.getMeasuredHeight()
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            int r3 = android.view.View.MeasureSpec.makeMeasureSpec(r3, r4)
            r1.measure(r2, r3)
            goto L_0x0044
        L_0x039c:
            r0 = r19
            r0.setMeasuredDimension(r14, r3)
            goto L_0x0358
        L_0x03a2:
            r1 = r2
            goto L_0x026c
        L_0x03a5:
            r2 = r10
            goto L_0x0217
        L_0x03a8:
            r12 = r1
            goto L_0x01a2
        L_0x03ab:
            r1 = r4
            r2 = r5
            goto L_0x0116
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.internal.widget.ActionBarView.onMeasure(int, int):void");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:100:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0155  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onLayout(boolean r11, int r12, int r13, int r14, int r15) {
        /*
            r10 = this;
            int r1 = r10.getPaddingLeft()
            int r2 = r10.getPaddingTop()
            int r0 = r15 - r13
            int r3 = r10.getPaddingTop()
            int r0 = r0 - r3
            int r3 = r10.getPaddingBottom()
            int r3 = r0 - r3
            if (r3 > 0) goto L_0x0018
        L_0x0017:
            return
        L_0x0018:
            android.view.View r0 = r10.mExpandedActionView
            if (r0 == 0) goto L_0x011f
            com.actionbarsherlock.internal.widget.ActionBarView$HomeView r0 = r10.mExpandedHomeLayout
        L_0x001e:
            int r4 = r0.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x01d4
            int r4 = r0.getLeftOffset()
            int r5 = r1 + r4
            int r0 = r10.positionChild(r0, r5, r2, r3)
            int r0 = r0 + r4
            int r0 = r0 + r1
        L_0x0032:
            android.view.View r1 = r10.mExpandedActionView
            if (r1 != 0) goto L_0x0059
            android.widget.LinearLayout r1 = r10.mTitleLayout
            if (r1 == 0) goto L_0x0123
            android.widget.LinearLayout r1 = r10.mTitleLayout
            int r1 = r1.getVisibility()
            r4 = 8
            if (r1 == r4) goto L_0x0123
            int r1 = r10.mDisplayOptions
            r1 = r1 & 8
            if (r1 == 0) goto L_0x0123
            r1 = 1
        L_0x004b:
            if (r1 == 0) goto L_0x0054
            android.widget.LinearLayout r4 = r10.mTitleLayout
            int r4 = r10.positionChild(r4, r0, r2, r3)
            int r0 = r0 + r4
        L_0x0054:
            int r4 = r10.mNavigationMode
            switch(r4) {
                case 0: goto L_0x0126;
                case 1: goto L_0x0129;
                case 2: goto L_0x013f;
                default: goto L_0x0059;
            }
        L_0x0059:
            r1 = r0
        L_0x005a:
            int r0 = r14 - r12
            int r4 = r10.getPaddingRight()
            int r0 = r0 - r4
            com.actionbarsherlock.internal.view.menu.ActionMenuView r4 = r10.mMenuView
            if (r4 == 0) goto L_0x0079
            com.actionbarsherlock.internal.view.menu.ActionMenuView r4 = r10.mMenuView
            android.view.ViewParent r4 = r4.getParent()
            if (r4 != r10) goto L_0x0079
            com.actionbarsherlock.internal.view.menu.ActionMenuView r4 = r10.mMenuView
            r10.positionChildInverse(r4, r0, r2, r3)
            com.actionbarsherlock.internal.view.menu.ActionMenuView r4 = r10.mMenuView
            int r4 = r4.getMeasuredWidth()
            int r0 = r0 - r4
        L_0x0079:
            com.actionbarsherlock.internal.widget.IcsProgressBar r4 = r10.mIndeterminateProgressView
            if (r4 == 0) goto L_0x01d1
            com.actionbarsherlock.internal.widget.IcsProgressBar r4 = r10.mIndeterminateProgressView
            int r4 = r4.getVisibility()
            r5 = 8
            if (r4 == r5) goto L_0x01d1
            com.actionbarsherlock.internal.widget.IcsProgressBar r4 = r10.mIndeterminateProgressView
            r10.positionChildInverse(r4, r0, r2, r3)
            com.actionbarsherlock.internal.widget.IcsProgressBar r2 = r10.mIndeterminateProgressView
            int r2 = r2.getMeasuredWidth()
            int r0 = r0 - r2
            r2 = r0
        L_0x0094:
            r0 = 0
            android.view.View r3 = r10.mExpandedActionView
            if (r3 == 0) goto L_0x0155
            android.view.View r0 = r10.mExpandedActionView
            r7 = r0
        L_0x009c:
            if (r7 == 0) goto L_0x00fb
            android.view.ViewGroup$LayoutParams r0 = r7.getLayoutParams()
            boolean r3 = r0 instanceof com.actionbarsherlock.app.ActionBar.LayoutParams
            if (r3 == 0) goto L_0x0164
            com.actionbarsherlock.app.ActionBar$LayoutParams r0 = (com.actionbarsherlock.app.ActionBar.LayoutParams) r0
            r5 = r0
        L_0x00a9:
            if (r5 == 0) goto L_0x0168
            int r0 = r5.gravity
        L_0x00ad:
            int r8 = r7.getMeasuredWidth()
            r4 = 0
            r3 = 0
            if (r5 == 0) goto L_0x01c8
            int r3 = r5.leftMargin
            int r4 = r1 + r3
            int r1 = r5.rightMargin
            int r3 = r2 - r1
            int r2 = r5.topMargin
            int r1 = r5.bottomMargin
            r5 = r2
            r6 = r3
            r3 = r4
            r4 = r1
        L_0x00c5:
            r1 = r0 & 7
            r2 = 1
            if (r1 != r2) goto L_0x0172
            int r2 = r10.getRight()
            int r9 = r10.getLeft()
            int r2 = r2 - r9
            int r2 = r2 - r8
            int r2 = r2 / 2
            if (r2 >= r3) goto L_0x016c
            r1 = 3
        L_0x00d9:
            r2 = r1
        L_0x00da:
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0179;
                case 2: goto L_0x00de;
                case 3: goto L_0x0188;
                case 4: goto L_0x00de;
                case 5: goto L_0x018b;
                default: goto L_0x00de;
            }
        L_0x00de:
            r2 = r1
        L_0x00df:
            r1 = r0 & 112(0x70, float:1.57E-43)
            r6 = -1
            if (r0 != r6) goto L_0x00e7
            r0 = 16
            r1 = r0
        L_0x00e7:
            r0 = 0
            switch(r1) {
                case 16: goto L_0x0190;
                case 48: goto L_0x01ad;
                case 80: goto L_0x01b4;
                default: goto L_0x00eb;
            }
        L_0x00eb:
            int r1 = r7.getMeasuredWidth()
            int r4 = r2 + r1
            int r5 = r7.getMeasuredHeight()
            int r5 = r5 + r0
            r7.layout(r2, r0, r4, r5)
            int r0 = r3 + r1
        L_0x00fb:
            com.actionbarsherlock.internal.widget.IcsProgressBar r0 = r10.mProgressView
            if (r0 == 0) goto L_0x0017
            com.actionbarsherlock.internal.widget.IcsProgressBar r0 = r10.mProgressView
            r0.bringToFront()
            com.actionbarsherlock.internal.widget.IcsProgressBar r0 = r10.mProgressView
            int r0 = r0.getMeasuredHeight()
            int r0 = r0 / 2
            com.actionbarsherlock.internal.widget.IcsProgressBar r1 = r10.mProgressView
            int r2 = r10.mProgressBarPadding
            int r3 = -r0
            int r4 = r10.mProgressBarPadding
            com.actionbarsherlock.internal.widget.IcsProgressBar r5 = r10.mProgressView
            int r5 = r5.getMeasuredWidth()
            int r4 = r4 + r5
            r1.layout(r2, r3, r4, r0)
            goto L_0x0017
        L_0x011f:
            com.actionbarsherlock.internal.widget.ActionBarView$HomeView r0 = r10.mHomeLayout
            goto L_0x001e
        L_0x0123:
            r1 = 0
            goto L_0x004b
        L_0x0126:
            r1 = r0
            goto L_0x005a
        L_0x0129:
            com.actionbarsherlock.internal.widget.IcsLinearLayout r4 = r10.mListNavLayout
            if (r4 == 0) goto L_0x0059
            if (r1 == 0) goto L_0x0132
            int r1 = r10.mItemPadding
            int r0 = r0 + r1
        L_0x0132:
            com.actionbarsherlock.internal.widget.IcsLinearLayout r1 = r10.mListNavLayout
            int r1 = r10.positionChild(r1, r0, r2, r3)
            int r4 = r10.mItemPadding
            int r1 = r1 + r4
            int r0 = r0 + r1
            r1 = r0
            goto L_0x005a
        L_0x013f:
            com.actionbarsherlock.internal.widget.ScrollingTabContainerView r4 = r10.mTabScrollView
            if (r4 == 0) goto L_0x0059
            if (r1 == 0) goto L_0x0148
            int r1 = r10.mItemPadding
            int r0 = r0 + r1
        L_0x0148:
            com.actionbarsherlock.internal.widget.ScrollingTabContainerView r1 = r10.mTabScrollView
            int r1 = r10.positionChild(r1, r0, r2, r3)
            int r4 = r10.mItemPadding
            int r1 = r1 + r4
            int r0 = r0 + r1
            r1 = r0
            goto L_0x005a
        L_0x0155:
            int r3 = r10.mDisplayOptions
            r3 = r3 & 16
            if (r3 == 0) goto L_0x01ce
            android.view.View r3 = r10.mCustomNavView
            if (r3 == 0) goto L_0x01ce
            android.view.View r0 = r10.mCustomNavView
            r7 = r0
            goto L_0x009c
        L_0x0164:
            r0 = 0
            r5 = r0
            goto L_0x00a9
        L_0x0168:
            r0 = 19
            goto L_0x00ad
        L_0x016c:
            int r2 = r2 + r8
            if (r2 <= r6) goto L_0x00d9
            r1 = 5
            goto L_0x00d9
        L_0x0172:
            r2 = -1
            if (r0 != r2) goto L_0x01c5
            r1 = 3
            r2 = r1
            goto L_0x00da
        L_0x0179:
            int r1 = r10.getRight()
            int r2 = r10.getLeft()
            int r1 = r1 - r2
            int r1 = r1 - r8
            int r1 = r1 / 2
            r2 = r1
            goto L_0x00df
        L_0x0188:
            r2 = r3
            goto L_0x00df
        L_0x018b:
            int r1 = r6 - r8
            r2 = r1
            goto L_0x00df
        L_0x0190:
            int r0 = r10.getPaddingTop()
            int r1 = r10.getBottom()
            int r4 = r10.getTop()
            int r1 = r1 - r4
            int r4 = r10.getPaddingBottom()
            int r1 = r1 - r4
            int r0 = r1 - r0
            int r1 = r7.getMeasuredHeight()
            int r0 = r0 - r1
            int r0 = r0 / 2
            goto L_0x00eb
        L_0x01ad:
            int r0 = r10.getPaddingTop()
            int r0 = r0 + r5
            goto L_0x00eb
        L_0x01b4:
            int r0 = r10.getHeight()
            int r1 = r10.getPaddingBottom()
            int r0 = r0 - r1
            int r1 = r7.getMeasuredHeight()
            int r0 = r0 - r1
            int r0 = r0 - r4
            goto L_0x00eb
        L_0x01c5:
            r2 = r1
            goto L_0x00da
        L_0x01c8:
            r5 = r4
            r6 = r2
            r4 = r3
            r3 = r1
            goto L_0x00c5
        L_0x01ce:
            r7 = r0
            goto L_0x009c
        L_0x01d1:
            r2 = r0
            goto L_0x0094
        L_0x01d4:
            r0 = r1
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.internal.widget.ActionBarView.onLayout(boolean, int, int, int, int):void");
    }

    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new ActionBar.LayoutParams(getContext(), attributeSet);
    }

    public ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams == null) {
            return generateDefaultLayoutParams();
        }
        return layoutParams;
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        if (!(this.mExpandedMenuPresenter == null || this.mExpandedMenuPresenter.mCurrentExpandedItem == null)) {
            savedState.expandedMenuItemId = this.mExpandedMenuPresenter.mCurrentExpandedItem.getItemId();
        }
        savedState.isOverflowOpen = isOverflowMenuShowing();
        return savedState;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        MenuItem findItem;
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        if (!(savedState.expandedMenuItemId == 0 || this.mExpandedMenuPresenter == null || this.mOptionsMenu == null || (findItem = this.mOptionsMenu.findItem(savedState.expandedMenuItemId)) == null)) {
            findItem.expandActionView();
        }
        if (savedState.isOverflowOpen) {
            postShowOverflowMenu();
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        int expandedMenuItemId;
        boolean isOverflowOpen;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.expandedMenuItemId = parcel.readInt();
            this.isOverflowOpen = parcel.readInt() != 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.expandedMenuItemId);
            parcel.writeInt(this.isOverflowOpen ? 1 : 0);
        }
    }

    public class HomeView extends FrameLayout {
        private ImageView mIconView;
        private View mUpView;
        private int mUpWidth;

        public HomeView(Context context) {
            this(context, null);
        }

        public HomeView(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public void setUp(boolean z) {
            this.mUpView.setVisibility(z ? 0 : 8);
        }

        public void setIcon(Drawable drawable) {
            this.mIconView.setImageDrawable(drawable);
        }

        public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            onPopulateAccessibilityEvent(accessibilityEvent);
            return true;
        }

        public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            if (Build.VERSION.SDK_INT >= 14) {
                super.onPopulateAccessibilityEvent(accessibilityEvent);
            }
            CharSequence contentDescription = getContentDescription();
            if (!TextUtils.isEmpty(contentDescription)) {
                accessibilityEvent.getText().add(contentDescription);
            }
        }

        public boolean dispatchHoverEvent(MotionEvent motionEvent) {
            return onHoverEvent(motionEvent);
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            this.mUpView = findViewById(R.id.abs__up);
            this.mIconView = (ImageView) findViewById(R.id.abs__home);
        }

        public int getLeftOffset() {
            if (this.mUpView.getVisibility() == 8) {
                return this.mUpWidth;
            }
            return 0;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            measureChildWithMargins(this.mUpView, i, 0, i2, 0);
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mUpView.getLayoutParams();
            this.mUpWidth = layoutParams.leftMargin + this.mUpView.getMeasuredWidth() + layoutParams.rightMargin;
            int i3 = this.mUpView.getVisibility() == 8 ? 0 : this.mUpWidth;
            int measuredHeight = layoutParams.topMargin + this.mUpView.getMeasuredHeight() + layoutParams.bottomMargin;
            measureChildWithMargins(this.mIconView, i, i3, i2, 0);
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.mIconView.getLayoutParams();
            int measuredWidth = i3 + layoutParams2.leftMargin + this.mIconView.getMeasuredWidth() + layoutParams2.rightMargin;
            int max = Math.max(measuredHeight, layoutParams2.bottomMargin + layoutParams2.topMargin + this.mIconView.getMeasuredHeight());
            int mode = View.MeasureSpec.getMode(i);
            int mode2 = View.MeasureSpec.getMode(i2);
            int size = View.MeasureSpec.getSize(i);
            int size2 = View.MeasureSpec.getSize(i2);
            switch (mode) {
                case Integer.MIN_VALUE:
                    size = Math.min(measuredWidth, size);
                    break;
                case 1073741824:
                    break;
                default:
                    size = measuredWidth;
                    break;
            }
            switch (mode2) {
                case Integer.MIN_VALUE:
                    size2 = Math.min(max, size2);
                    break;
                case 1073741824:
                    break;
                default:
                    size2 = max;
                    break;
            }
            setMeasuredDimension(size, size2);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            int i5 = 0;
            int i6 = (i4 - i2) / 2;
            if (this.mUpView.getVisibility() != 8) {
                FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.mUpView.getLayoutParams();
                int measuredHeight = this.mUpView.getMeasuredHeight();
                int measuredWidth = this.mUpView.getMeasuredWidth();
                int i7 = i6 - (measuredHeight / 2);
                this.mUpView.layout(0, i7, measuredWidth, measuredHeight + i7);
                int i8 = layoutParams.rightMargin + layoutParams.leftMargin + measuredWidth;
                i += i8;
                i5 = i8;
            }
            FrameLayout.LayoutParams layoutParams2 = (FrameLayout.LayoutParams) this.mIconView.getLayoutParams();
            int measuredHeight2 = this.mIconView.getMeasuredHeight();
            int measuredWidth2 = this.mIconView.getMeasuredWidth();
            int max = i5 + Math.max(layoutParams2.leftMargin, ((i3 - i) / 2) - (measuredWidth2 / 2));
            int max2 = Math.max(layoutParams2.topMargin, i6 - (measuredHeight2 / 2));
            this.mIconView.layout(max, max2, measuredWidth2 + max, measuredHeight2 + max2);
        }
    }

    class ExpandedActionViewMenuPresenter implements MenuPresenter {
        MenuItemImpl mCurrentExpandedItem;
        MenuBuilder mMenu;

        private ExpandedActionViewMenuPresenter() {
        }

        public void initForMenu(Context context, MenuBuilder menuBuilder) {
            if (!(this.mMenu == null || this.mCurrentExpandedItem == null)) {
                this.mMenu.collapseItemActionView(this.mCurrentExpandedItem);
            }
            this.mMenu = menuBuilder;
        }

        public MenuView getMenuView(ViewGroup viewGroup) {
            return null;
        }

        public void updateMenuView(boolean z) {
            boolean z2 = false;
            if (this.mCurrentExpandedItem != null) {
                if (this.mMenu != null) {
                    int size = this.mMenu.size();
                    int i = 0;
                    while (true) {
                        if (i >= size) {
                            break;
                        } else if (this.mMenu.getItem(i) == this.mCurrentExpandedItem) {
                            z2 = true;
                            break;
                        } else {
                            i++;
                        }
                    }
                }
                if (!z2) {
                    collapseItemActionView(this.mMenu, this.mCurrentExpandedItem);
                }
            }
        }

        public void setCallback(MenuPresenter.Callback callback) {
        }

        public boolean onSubMenuSelected(SubMenuBuilder subMenuBuilder) {
            return false;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
        }

        public boolean flagActionItems() {
            return false;
        }

        public boolean expandItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            ActionBarView.this.mExpandedActionView = menuItemImpl.getActionView();
            ActionBarView.this.mExpandedHomeLayout.setIcon(ActionBarView.this.mIcon.getConstantState().newDrawable());
            this.mCurrentExpandedItem = menuItemImpl;
            if (ActionBarView.this.mExpandedActionView.getParent() != ActionBarView.this) {
                ActionBarView.this.addView(ActionBarView.this.mExpandedActionView);
            }
            if (ActionBarView.this.mExpandedHomeLayout.getParent() != ActionBarView.this) {
                ActionBarView.this.addView(ActionBarView.this.mExpandedHomeLayout);
            }
            ActionBarView.this.mHomeLayout.setVisibility(8);
            if (ActionBarView.this.mTitleLayout != null) {
                ActionBarView.this.mTitleLayout.setVisibility(8);
            }
            if (ActionBarView.this.mTabScrollView != null) {
                ActionBarView.this.mTabScrollView.setVisibility(8);
            }
            if (ActionBarView.this.mSpinner != null) {
                ActionBarView.this.mSpinner.setVisibility(8);
            }
            if (ActionBarView.this.mCustomNavView != null) {
                ActionBarView.this.mCustomNavView.setVisibility(8);
            }
            ActionBarView.this.requestLayout();
            menuItemImpl.setActionViewExpanded(true);
            if (ActionBarView.this.mExpandedActionView instanceof CollapsibleActionView) {
                ((CollapsibleActionView) ActionBarView.this.mExpandedActionView).onActionViewExpanded();
            }
            return true;
        }

        public boolean collapseItemActionView(MenuBuilder menuBuilder, MenuItemImpl menuItemImpl) {
            if (ActionBarView.this.mExpandedActionView instanceof CollapsibleActionView) {
                ((CollapsibleActionView) ActionBarView.this.mExpandedActionView).onActionViewCollapsed();
            }
            ActionBarView.this.removeView(ActionBarView.this.mExpandedActionView);
            ActionBarView.this.removeView(ActionBarView.this.mExpandedHomeLayout);
            ActionBarView.this.mExpandedActionView = null;
            if ((ActionBarView.this.mDisplayOptions & 2) != 0) {
                ActionBarView.this.mHomeLayout.setVisibility(0);
            }
            if ((ActionBarView.this.mDisplayOptions & 8) != 0) {
                if (ActionBarView.this.mTitleLayout == null) {
                    ActionBarView.this.initTitle();
                } else {
                    ActionBarView.this.mTitleLayout.setVisibility(0);
                }
            }
            if (ActionBarView.this.mTabScrollView != null && ActionBarView.this.mNavigationMode == 2) {
                ActionBarView.this.mTabScrollView.setVisibility(0);
            }
            if (ActionBarView.this.mSpinner != null && ActionBarView.this.mNavigationMode == 1) {
                ActionBarView.this.mSpinner.setVisibility(0);
            }
            if (!(ActionBarView.this.mCustomNavView == null || (ActionBarView.this.mDisplayOptions & 16) == 0)) {
                ActionBarView.this.mCustomNavView.setVisibility(0);
            }
            ActionBarView.this.mExpandedHomeLayout.setIcon(null);
            this.mCurrentExpandedItem = null;
            ActionBarView.this.requestLayout();
            menuItemImpl.setActionViewExpanded(false);
            return true;
        }

        public int getId() {
            return 0;
        }

        public Parcelable onSaveInstanceState() {
            return null;
        }

        public void onRestoreInstanceState(Parcelable parcelable) {
        }
    }
}
