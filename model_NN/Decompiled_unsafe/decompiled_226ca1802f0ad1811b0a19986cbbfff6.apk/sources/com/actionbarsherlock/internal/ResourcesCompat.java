package com.actionbarsherlock.internal;

import android.app.Activity;
import android.content.Context;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.util.DisplayMetrics;
import com.actionbarsherlock.R;

public final class ResourcesCompat {
    private static final String TAG = "ResourcesCompat";

    private ResourcesCompat() {
    }

    public static boolean getResources_getBoolean(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 14) {
            return context.getResources().getBoolean(i);
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float f = ((float) displayMetrics.widthPixels) / displayMetrics.density;
        float f2 = ((float) displayMetrics.heightPixels) / displayMetrics.density;
        if (f < f2) {
            f2 = f;
        }
        if (i == R.bool.abs__action_bar_embed_tabs) {
            if (f >= 480.0f) {
                return true;
            }
            return false;
        } else if (i == R.bool.abs__split_action_bar_is_narrow) {
            if (f >= 480.0f) {
                return false;
            }
            return true;
        } else if (i == R.bool.abs__action_bar_expanded_action_views_exclusive) {
            if (f2 >= 600.0f) {
                return false;
            }
            return true;
        } else if (i != R.bool.abs__config_allowActionMenuItemTextWithIcon) {
            throw new IllegalArgumentException("Unknown boolean resource ID " + i);
        } else if (f >= 480.0f) {
            return true;
        } else {
            return false;
        }
    }

    public static int getResources_getInteger(Context context, int i) {
        if (Build.VERSION.SDK_INT >= 13) {
            return context.getResources().getInteger(i);
        }
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float f = ((float) displayMetrics.widthPixels) / displayMetrics.density;
        if (i != R.integer.abs__max_action_buttons) {
            throw new IllegalArgumentException("Unknown integer resource ID " + i);
        } else if (f >= 600.0f) {
            return 5;
        } else {
            if (f >= 500.0f) {
                return 4;
            }
            if (f >= 360.0f) {
                return 3;
            }
            return 2;
        }
    }

    public static int loadLogoFromManifest(Activity activity) {
        Exception e;
        int i;
        int i2;
        try {
            String name = activity.getClass().getName();
            String str = activity.getApplicationInfo().packageName;
            XmlResourceParser openXmlResourceParser = activity.createPackageContext(str, 0).getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i = 0;
            while (eventType != 1) {
                if (eventType == 2) {
                    try {
                        String name2 = openXmlResourceParser.getName();
                        if (!"application".equals(name2)) {
                            if ("activity".equals(name2)) {
                                boolean z = false;
                                String str2 = null;
                                Integer num = null;
                                for (int attributeCount = openXmlResourceParser.getAttributeCount() - 1; attributeCount >= 0; attributeCount--) {
                                    String attributeName = openXmlResourceParser.getAttributeName(attributeCount);
                                    if ("logo".equals(attributeName)) {
                                        num = Integer.valueOf(openXmlResourceParser.getAttributeResourceValue(attributeCount, 0));
                                    } else if ("name".equals(attributeName)) {
                                        str2 = ActionBarSherlockCompat.cleanActivityName(str, openXmlResourceParser.getAttributeValue(attributeCount));
                                        if (!name.equals(str2)) {
                                            break;
                                        }
                                        z = true;
                                    }
                                    if (!(num == null || str2 == null)) {
                                        i = num.intValue();
                                    }
                                }
                                if (z) {
                                    break;
                                }
                            }
                        } else {
                            int attributeCount2 = openXmlResourceParser.getAttributeCount() - 1;
                            while (true) {
                                if (attributeCount2 < 0) {
                                    break;
                                } else if ("logo".equals(openXmlResourceParser.getAttributeName(attributeCount2))) {
                                    i = openXmlResourceParser.getAttributeResourceValue(attributeCount2, 0);
                                    break;
                                } else {
                                    attributeCount2--;
                                }
                            }
                            i2 = i;
                            i = i2;
                            eventType = openXmlResourceParser.nextToken();
                        }
                    } catch (Exception e2) {
                        e = e2;
                        e.printStackTrace();
                        return i;
                    }
                }
                i2 = i;
                try {
                    i = i2;
                    eventType = openXmlResourceParser.nextToken();
                } catch (Exception e3) {
                    Exception exc = e3;
                    i = i2;
                    e = exc;
                    e.printStackTrace();
                    return i;
                }
            }
        } catch (Exception e4) {
            e = e4;
            i = 0;
            e.printStackTrace();
            return i;
        }
        return i;
    }
}
