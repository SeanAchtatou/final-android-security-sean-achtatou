package com.actionbarsherlock.internal.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.LinearLayout;
import com.actionbarsherlock.internal.view.menu.MenuBuilder;
import com.actionbarsherlock.internal.widget.IcsLinearLayout;

public class ActionMenuView extends IcsLinearLayout implements MenuBuilder.ItemInvoker, MenuView {
    static final int GENERATED_ITEM_PADDING = 4;
    private static final boolean IS_FROYO = (Build.VERSION.SDK_INT >= 8);
    static final int MIN_CELL_SIZE = 56;
    private boolean mFirst;
    private boolean mFormatItems;
    private int mFormatItemsWidth;
    private int mGeneratedItemPadding;
    private MenuBuilder mMenu;
    private int mMinCellSize;
    private ActionMenuPresenter mPresenter;
    private boolean mReserveOverflow;

    public interface ActionMenuChildView {
        boolean needsDividerAfter();

        boolean needsDividerBefore();
    }

    public ActionMenuView(Context context) {
        this(context, null);
    }

    public ActionMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.mFirst = true;
        setBaselineAligned(false);
        float f = context.getResources().getDisplayMetrics().density;
        this.mMinCellSize = (int) (56.0f * f);
        this.mGeneratedItemPadding = (int) (f * 4.0f);
    }

    public void setPresenter(ActionMenuPresenter actionMenuPresenter) {
        this.mPresenter = actionMenuPresenter;
    }

    public boolean isExpandedFormat() {
        return this.mFormatItems;
    }

    public void onConfigurationChanged(Configuration configuration) {
        if (IS_FROYO) {
            super.onConfigurationChanged(configuration);
        }
        this.mPresenter.updateMenuView(false);
        if (this.mPresenter != null && this.mPresenter.isOverflowMenuShowing()) {
            this.mPresenter.hideOverflowMenu();
            this.mPresenter.showOverflowMenu();
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (IS_FROYO || !this.mFirst) {
            super.onDraw(canvas);
            return;
        }
        this.mFirst = false;
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        boolean z = this.mFormatItems;
        this.mFormatItems = View.MeasureSpec.getMode(i) == 1073741824;
        if (z != this.mFormatItems) {
            this.mFormatItemsWidth = 0;
        }
        int mode = View.MeasureSpec.getMode(i);
        if (!(!this.mFormatItems || this.mMenu == null || mode == this.mFormatItemsWidth)) {
            this.mFormatItemsWidth = mode;
            this.mMenu.onItemsChanged(true);
        }
        if (this.mFormatItems) {
            onMeasureExactFormat(i, i2);
        } else {
            super.onMeasure(i, i2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:108:0x0268  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x01d0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void onMeasureExactFormat(int r34, int r35) {
        /*
            r33 = this;
            int r22 = android.view.View.MeasureSpec.getMode(r35)
            int r5 = android.view.View.MeasureSpec.getSize(r34)
            int r18 = android.view.View.MeasureSpec.getSize(r35)
            int r6 = r33.getPaddingLeft()
            int r7 = r33.getPaddingRight()
            int r6 = r6 + r7
            int r7 = r33.getPaddingTop()
            int r8 = r33.getPaddingBottom()
            int r23 = r7 + r8
            int r24 = r5 - r6
            r0 = r33
            int r5 = r0.mMinCellSize
            int r8 = r24 / r5
            r0 = r33
            int r5 = r0.mMinCellSize
            int r5 = r24 % r5
            if (r8 != 0) goto L_0x0038
            r5 = 0
            r0 = r33
            r1 = r24
            r0.setMeasuredDimension(r1, r5)
        L_0x0037:
            return
        L_0x0038:
            r0 = r33
            int r6 = r0.mMinCellSize
            int r5 = r5 / r8
            int r25 = r6 + r5
            r15 = 0
            r14 = 0
            r9 = 0
            r6 = 0
            r10 = 0
            r11 = 0
            int r26 = r33.getChildCount()
            r5 = 0
            r16 = r5
        L_0x004d:
            r0 = r16
            r1 = r26
            if (r0 >= r1) goto L_0x00f5
            r0 = r33
            r1 = r16
            android.view.View r7 = r0.getChildAt(r1)
            int r5 = r7.getVisibility()
            r13 = 8
            if (r5 != r13) goto L_0x0072
            r7 = r6
            r5 = r11
            r11 = r15
            r12 = r8
            r8 = r14
        L_0x0068:
            int r13 = r16 + 1
            r16 = r13
            r14 = r8
            r15 = r11
            r8 = r12
            r11 = r5
            r6 = r7
            goto L_0x004d
        L_0x0072:
            boolean r0 = r7 instanceof com.actionbarsherlock.internal.view.menu.ActionMenuItemView
            r17 = r0
            int r13 = r6 + 1
            if (r17 == 0) goto L_0x008e
            r0 = r33
            int r5 = r0.mGeneratedItemPadding
            r6 = 0
            r0 = r33
            int r0 = r0.mGeneratedItemPadding
            r19 = r0
            r20 = 0
            r0 = r19
            r1 = r20
            r7.setPadding(r5, r6, r0, r1)
        L_0x008e:
            android.view.ViewGroup$LayoutParams r5 = r7.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            r6 = 0
            r5.expanded = r6
            r6 = 0
            r5.extraPixels = r6
            r6 = 0
            r5.cellsUsed = r6
            r6 = 0
            r5.expandable = r6
            r6 = 0
            r5.leftMargin = r6
            r6 = 0
            r5.rightMargin = r6
            if (r17 == 0) goto L_0x00f1
            r6 = r7
            com.actionbarsherlock.internal.view.menu.ActionMenuItemView r6 = (com.actionbarsherlock.internal.view.menu.ActionMenuItemView) r6
            boolean r6 = r6.hasText()
            if (r6 == 0) goto L_0x00f1
            r6 = 1
        L_0x00b2:
            r5.preventEdgeOffset = r6
            boolean r6 = r5.isOverflowButton
            if (r6 == 0) goto L_0x00f3
            r6 = 1
        L_0x00b9:
            r0 = r25
            r1 = r35
            r2 = r23
            int r17 = measureChildForCells(r7, r0, r6, r1, r2)
            r0 = r17
            int r14 = java.lang.Math.max(r14, r0)
            boolean r6 = r5.expandable
            if (r6 == 0) goto L_0x0317
            int r6 = r9 + 1
        L_0x00cf:
            boolean r5 = r5.isOverflowButton
            if (r5 == 0) goto L_0x0314
            r5 = 1
        L_0x00d4:
            int r10 = r8 - r17
            int r7 = r7.getMeasuredHeight()
            int r9 = java.lang.Math.max(r15, r7)
            r7 = 1
            r0 = r17
            if (r0 != r7) goto L_0x0308
            r7 = 1
            int r7 = r7 << r16
            long r7 = (long) r7
            long r7 = r7 | r11
            r11 = r9
            r12 = r10
            r9 = r6
            r10 = r5
            r5 = r7
            r8 = r14
            r7 = r13
            goto L_0x0068
        L_0x00f1:
            r6 = 0
            goto L_0x00b2
        L_0x00f3:
            r6 = r8
            goto L_0x00b9
        L_0x00f5:
            if (r10 == 0) goto L_0x0132
            r5 = 2
            if (r6 != r5) goto L_0x0132
            r5 = 1
            r7 = r5
        L_0x00fc:
            r16 = 0
            r19 = r11
            r17 = r8
        L_0x0102:
            if (r9 <= 0) goto L_0x0304
            if (r17 <= 0) goto L_0x0304
            r13 = 2147483647(0x7fffffff, float:NaN)
            r11 = 0
            r8 = 0
            r5 = 0
            r21 = r5
        L_0x010f:
            r0 = r21
            r1 = r26
            if (r0 >= r1) goto L_0x0155
            r0 = r33
            r1 = r21
            android.view.View r5 = r0.getChildAt(r1)
            android.view.ViewGroup$LayoutParams r5 = r5.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            boolean r0 = r5.expandable
            r27 = r0
            if (r27 != 0) goto L_0x0135
            r5 = r8
            r8 = r13
        L_0x012b:
            int r13 = r21 + 1
            r21 = r13
            r13 = r8
            r8 = r5
            goto L_0x010f
        L_0x0132:
            r5 = 0
            r7 = r5
            goto L_0x00fc
        L_0x0135:
            int r0 = r5.cellsUsed
            r27 = r0
            r0 = r27
            if (r0 >= r13) goto L_0x0145
            int r8 = r5.cellsUsed
            r5 = 1
            int r5 = r5 << r21
            long r11 = (long) r5
            r5 = 1
            goto L_0x012b
        L_0x0145:
            int r5 = r5.cellsUsed
            if (r5 != r13) goto L_0x0300
            r5 = 1
            int r5 = r5 << r21
            long r0 = (long) r5
            r27 = r0
            long r11 = r11 | r27
            int r5 = r8 + 1
            r8 = r13
            goto L_0x012b
        L_0x0155:
            long r19 = r19 | r11
            r0 = r17
            if (r8 <= r0) goto L_0x01e0
            r11 = r19
        L_0x015d:
            if (r10 != 0) goto L_0x0265
            r5 = 1
            if (r6 != r5) goto L_0x0265
            r5 = 1
        L_0x0163:
            if (r17 <= 0) goto L_0x02b1
            r7 = 0
            int r7 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r7 == 0) goto L_0x02b1
            int r6 = r6 + -1
            r0 = r17
            if (r0 < r6) goto L_0x0176
            if (r5 != 0) goto L_0x0176
            r6 = 1
            if (r14 <= r6) goto L_0x02b1
        L_0x0176:
            int r6 = java.lang.Long.bitCount(r11)
            float r6 = (float) r6
            if (r5 != 0) goto L_0x02fa
            r7 = 1
            long r7 = r7 & r11
            r9 = 0
            int r5 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r5 == 0) goto L_0x019a
            r5 = 0
            r0 = r33
            android.view.View r5 = r0.getChildAt(r5)
            android.view.ViewGroup$LayoutParams r5 = r5.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            boolean r5 = r5.preventEdgeOffset
            if (r5 != 0) goto L_0x019a
            r5 = 1056964608(0x3f000000, float:0.5)
            float r6 = r6 - r5
        L_0x019a:
            r5 = 1
            int r7 = r26 + -1
            int r5 = r5 << r7
            long r7 = (long) r5
            long r7 = r7 & r11
            r9 = 0
            int r5 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r5 == 0) goto L_0x02fa
            int r5 = r26 + -1
            r0 = r33
            android.view.View r5 = r0.getChildAt(r5)
            android.view.ViewGroup$LayoutParams r5 = r5.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            boolean r5 = r5.preventEdgeOffset
            if (r5 != 0) goto L_0x02fa
            r5 = 1056964608(0x3f000000, float:0.5)
            float r5 = r6 - r5
        L_0x01bc:
            r6 = 0
            int r6 = (r5 > r6 ? 1 : (r5 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x0268
            int r6 = r17 * r25
            float r6 = (float) r6
            float r5 = r6 / r5
            int r5 = (int) r5
            r6 = r5
        L_0x01c8:
            r5 = 0
            r8 = r5
            r7 = r16
        L_0x01cc:
            r0 = r26
            if (r8 >= r0) goto L_0x02b3
            r5 = 1
            int r5 = r5 << r8
            long r9 = (long) r5
            long r9 = r9 & r11
            r13 = 0
            int r5 = (r9 > r13 ? 1 : (r9 == r13 ? 0 : -1))
            if (r5 != 0) goto L_0x026c
            r5 = r7
        L_0x01db:
            int r7 = r8 + 1
            r8 = r7
            r7 = r5
            goto L_0x01cc
        L_0x01e0:
            int r21 = r13 + 1
            r5 = 0
            r13 = r5
            r8 = r17
            r16 = r19
        L_0x01e8:
            r0 = r26
            if (r13 >= r0) goto L_0x025c
            r0 = r33
            android.view.View r19 = r0.getChildAt(r13)
            android.view.ViewGroup$LayoutParams r5 = r19.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            r20 = 1
            int r20 = r20 << r13
            r0 = r20
            long r0 = (long) r0
            r27 = r0
            long r27 = r27 & r11
            r29 = 0
            int r20 = (r27 > r29 ? 1 : (r27 == r29 ? 0 : -1))
            if (r20 != 0) goto L_0x021c
            int r5 = r5.cellsUsed
            r0 = r21
            if (r5 != r0) goto L_0x02fd
            r5 = 1
            int r5 = r5 << r13
            long r0 = (long) r5
            r19 = r0
            long r16 = r16 | r19
            r5 = r8
        L_0x0217:
            int r8 = r13 + 1
            r13 = r8
            r8 = r5
            goto L_0x01e8
        L_0x021c:
            if (r7 == 0) goto L_0x0249
            boolean r0 = r5.preventEdgeOffset
            r20 = r0
            if (r20 == 0) goto L_0x0249
            r20 = 1
            r0 = r20
            if (r8 != r0) goto L_0x0249
            r0 = r33
            int r0 = r0.mGeneratedItemPadding
            r20 = r0
            int r20 = r20 + r25
            r27 = 0
            r0 = r33
            int r0 = r0.mGeneratedItemPadding
            r28 = r0
            r29 = 0
            r0 = r19
            r1 = r20
            r2 = r27
            r3 = r28
            r4 = r29
            r0.setPadding(r1, r2, r3, r4)
        L_0x0249:
            int r0 = r5.cellsUsed
            r19 = r0
            int r19 = r19 + 1
            r0 = r19
            r5.cellsUsed = r0
            r19 = 1
            r0 = r19
            r5.expanded = r0
            int r5 = r8 + -1
            goto L_0x0217
        L_0x025c:
            r5 = 1
            r19 = r16
            r16 = r5
            r17 = r8
            goto L_0x0102
        L_0x0265:
            r5 = 0
            goto L_0x0163
        L_0x0268:
            r5 = 0
            r6 = r5
            goto L_0x01c8
        L_0x026c:
            r0 = r33
            android.view.View r9 = r0.getChildAt(r8)
            android.view.ViewGroup$LayoutParams r5 = r9.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            boolean r9 = r9 instanceof com.actionbarsherlock.internal.view.menu.ActionMenuItemView
            if (r9 == 0) goto L_0x028f
            r5.extraPixels = r6
            r7 = 1
            r5.expanded = r7
            if (r8 != 0) goto L_0x028c
            boolean r7 = r5.preventEdgeOffset
            if (r7 != 0) goto L_0x028c
            int r7 = -r6
            int r7 = r7 / 2
            r5.leftMargin = r7
        L_0x028c:
            r5 = 1
            goto L_0x01db
        L_0x028f:
            boolean r9 = r5.isOverflowButton
            if (r9 == 0) goto L_0x02a0
            r5.extraPixels = r6
            r7 = 1
            r5.expanded = r7
            int r7 = -r6
            int r7 = r7 / 2
            r5.rightMargin = r7
            r5 = 1
            goto L_0x01db
        L_0x02a0:
            if (r8 == 0) goto L_0x02a6
            int r9 = r6 / 2
            r5.leftMargin = r9
        L_0x02a6:
            int r9 = r26 + -1
            if (r8 == r9) goto L_0x02ae
            int r9 = r6 / 2
            r5.rightMargin = r9
        L_0x02ae:
            r5 = r7
            goto L_0x01db
        L_0x02b1:
            r7 = r16
        L_0x02b3:
            if (r7 == 0) goto L_0x02e8
            int r5 = r18 - r23
            r0 = r22
            int r7 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r0)
            r5 = 0
            r6 = r5
        L_0x02bf:
            r0 = r26
            if (r6 >= r0) goto L_0x02e8
            r0 = r33
            android.view.View r8 = r0.getChildAt(r6)
            android.view.ViewGroup$LayoutParams r5 = r8.getLayoutParams()
            com.actionbarsherlock.internal.view.menu.ActionMenuView$LayoutParams r5 = (com.actionbarsherlock.internal.view.menu.ActionMenuView.LayoutParams) r5
            boolean r9 = r5.expanded
            if (r9 != 0) goto L_0x02d7
        L_0x02d3:
            int r5 = r6 + 1
            r6 = r5
            goto L_0x02bf
        L_0x02d7:
            int r9 = r5.cellsUsed
            int r9 = r9 * r25
            int r5 = r5.extraPixels
            int r5 = r5 + r9
            r9 = 1073741824(0x40000000, float:2.0)
            int r5 = android.view.View.MeasureSpec.makeMeasureSpec(r5, r9)
            r8.measure(r5, r7)
            goto L_0x02d3
        L_0x02e8:
            r5 = 1073741824(0x40000000, float:2.0)
            r0 = r22
            if (r0 == r5) goto L_0x02f7
        L_0x02ee:
            r0 = r33
            r1 = r24
            r0.setMeasuredDimension(r1, r15)
            goto L_0x0037
        L_0x02f7:
            r15 = r18
            goto L_0x02ee
        L_0x02fa:
            r5 = r6
            goto L_0x01bc
        L_0x02fd:
            r5 = r8
            goto L_0x0217
        L_0x0300:
            r5 = r8
            r8 = r13
            goto L_0x012b
        L_0x0304:
            r11 = r19
            goto L_0x015d
        L_0x0308:
            r7 = r13
            r8 = r14
            r31 = r11
            r11 = r9
            r12 = r10
            r10 = r5
            r9 = r6
            r5 = r31
            goto L_0x0068
        L_0x0314:
            r5 = r10
            goto L_0x00d4
        L_0x0317:
            r6 = r9
            goto L_0x00cf
        */
        throw new UnsupportedOperationException("Method not decompiled: com.actionbarsherlock.internal.view.menu.ActionMenuView.onMeasureExactFormat(int, int):void");
    }

    static int measureChildForCells(View view, int i, int i2, int i3, int i4) {
        int i5;
        boolean z = false;
        LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(i3) - i4, View.MeasureSpec.getMode(i3));
        if (i2 > 0) {
            view.measure(View.MeasureSpec.makeMeasureSpec(i * i2, Integer.MIN_VALUE), makeMeasureSpec);
            int measuredWidth = view.getMeasuredWidth();
            int i6 = measuredWidth / i;
            i5 = measuredWidth % i != 0 ? i6 + 1 : i6;
        } else {
            i5 = 0;
        }
        ActionMenuItemView actionMenuItemView = view instanceof ActionMenuItemView ? (ActionMenuItemView) view : null;
        if (!layoutParams.isOverflowButton && actionMenuItemView != null && actionMenuItemView.hasText()) {
            z = true;
        }
        layoutParams.expandable = z;
        layoutParams.cellsUsed = i5;
        view.measure(View.MeasureSpec.makeMeasureSpec(i5 * i, 1073741824), makeMeasureSpec);
        return i5;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        boolean z2;
        if (!this.mFormatItems) {
            super.onLayout(z, i, i2, i3, i4);
            return;
        }
        int childCount = getChildCount();
        int i8 = (i2 + i4) / 2;
        int i9 = 0;
        int paddingRight = ((i3 - i) - getPaddingRight()) - getPaddingLeft();
        boolean z3 = false;
        int i10 = 0;
        while (i10 < childCount) {
            View childAt = getChildAt(i10);
            if (childAt.getVisibility() == 8) {
                z2 = z3;
                i7 = paddingRight;
                i6 = i9;
            } else {
                LayoutParams layoutParams = (LayoutParams) childAt.getLayoutParams();
                if (layoutParams.isOverflowButton) {
                    int measuredWidth = childAt.getMeasuredWidth();
                    if (hasDividerBeforeChildAt(i10)) {
                        measuredWidth += 0;
                    }
                    int measuredHeight = childAt.getMeasuredHeight();
                    int width = (getWidth() - getPaddingRight()) - layoutParams.rightMargin;
                    int i11 = i8 - (measuredHeight / 2);
                    childAt.layout(width - measuredWidth, i11, width, measuredHeight + i11);
                    i7 = paddingRight - measuredWidth;
                    z2 = true;
                    i6 = i9;
                } else {
                    int measuredWidth2 = paddingRight - (layoutParams.rightMargin + (childAt.getMeasuredWidth() + layoutParams.leftMargin));
                    i6 = i9 + 1;
                    boolean z4 = z3;
                    i7 = measuredWidth2;
                    z2 = z4;
                }
            }
            i10++;
            i9 = i6;
            paddingRight = i7;
            z3 = z2;
        }
        if (childCount != 1 || z3) {
            int i12 = i9 - (z3 ? 0 : 1);
            int max = Math.max(0, i12 > 0 ? paddingRight / i12 : 0);
            int paddingLeft = getPaddingLeft();
            int i13 = 0;
            while (i13 < childCount) {
                View childAt2 = getChildAt(i13);
                LayoutParams layoutParams2 = (LayoutParams) childAt2.getLayoutParams();
                if (childAt2.getVisibility() == 8) {
                    i5 = paddingLeft;
                } else if (layoutParams2.isOverflowButton) {
                    i5 = paddingLeft;
                } else {
                    int i14 = paddingLeft + layoutParams2.leftMargin;
                    int measuredWidth3 = childAt2.getMeasuredWidth();
                    int measuredHeight2 = childAt2.getMeasuredHeight();
                    int i15 = i8 - (measuredHeight2 / 2);
                    childAt2.layout(i14, i15, i14 + measuredWidth3, measuredHeight2 + i15);
                    i5 = layoutParams2.rightMargin + measuredWidth3 + max + i14;
                }
                i13++;
                paddingLeft = i5;
            }
            return;
        }
        View childAt3 = getChildAt(0);
        int measuredWidth4 = childAt3.getMeasuredWidth();
        int measuredHeight3 = childAt3.getMeasuredHeight();
        int i16 = ((i3 - i) / 2) - (measuredWidth4 / 2);
        int i17 = i8 - (measuredHeight3 / 2);
        childAt3.layout(i16, i17, measuredWidth4 + i16, measuredHeight3 + i17);
    }

    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.mPresenter.dismissPopupMenus();
    }

    public boolean isOverflowReserved() {
        return this.mReserveOverflow;
    }

    public void setOverflowReserved(boolean z) {
        this.mReserveOverflow = z;
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateDefaultLayoutParams() {
        LayoutParams layoutParams = new LayoutParams(-2, -2);
        layoutParams.gravity = 16;
        return layoutParams;
    }

    public LayoutParams generateLayoutParams(AttributeSet attributeSet) {
        return new LayoutParams(getContext(), attributeSet);
    }

    /* access modifiers changed from: protected */
    public LayoutParams generateLayoutParams(ViewGroup.LayoutParams layoutParams) {
        if (!(layoutParams instanceof LayoutParams)) {
            return generateDefaultLayoutParams();
        }
        LayoutParams layoutParams2 = new LayoutParams((LayoutParams) layoutParams);
        if (layoutParams2.gravity > 0) {
            return layoutParams2;
        }
        layoutParams2.gravity = 16;
        return layoutParams2;
    }

    /* access modifiers changed from: protected */
    public boolean checkLayoutParams(ViewGroup.LayoutParams layoutParams) {
        return layoutParams != null && (layoutParams instanceof LayoutParams);
    }

    public LayoutParams generateOverflowButtonLayoutParams() {
        LayoutParams generateDefaultLayoutParams = generateDefaultLayoutParams();
        generateDefaultLayoutParams.isOverflowButton = true;
        return generateDefaultLayoutParams;
    }

    public boolean invokeItem(MenuItemImpl menuItemImpl) {
        return this.mMenu.performItemAction(menuItemImpl, 0);
    }

    public int getWindowAnimations() {
        return 0;
    }

    public void initialize(MenuBuilder menuBuilder) {
        this.mMenu = menuBuilder;
    }

    /* access modifiers changed from: protected */
    public boolean hasDividerBeforeChildAt(int i) {
        boolean z = false;
        if (i == 0) {
            return false;
        }
        View childAt = getChildAt(i - 1);
        View childAt2 = getChildAt(i);
        if (i < getChildCount() && (childAt instanceof ActionMenuChildView)) {
            z = false | ((ActionMenuChildView) childAt).needsDividerAfter();
        }
        return (i <= 0 || !(childAt2 instanceof ActionMenuChildView)) ? z : ((ActionMenuChildView) childAt2).needsDividerBefore() | z;
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        return false;
    }

    public class LayoutParams extends LinearLayout.LayoutParams {
        public int cellsUsed;
        public boolean expandable;
        public boolean expanded;
        public int extraPixels;
        public boolean isOverflowButton;
        public boolean preventEdgeOffset;

        public LayoutParams(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public LayoutParams(LayoutParams layoutParams) {
            super((LinearLayout.LayoutParams) layoutParams);
            this.isOverflowButton = layoutParams.isOverflowButton;
        }

        public LayoutParams(int i, int i2) {
            super(i, i2);
            this.isOverflowButton = false;
        }

        public LayoutParams(int i, int i2, boolean z) {
            super(i, i2);
            this.isOverflowButton = z;
        }
    }
}
