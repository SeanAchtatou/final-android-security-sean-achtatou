package com.actionbarsherlock.internal.view.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.view.KeyEvent;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.SubMenu;
import java.util.ArrayList;
import java.util.List;

public class ActionMenu implements Menu {
    private Context mContext;
    private boolean mIsQwerty;
    private ArrayList<ActionMenuItem> mItems = new ArrayList<>();

    public ActionMenu(Context context) {
        this.mContext = context;
    }

    public Context getContext() {
        return this.mContext;
    }

    public MenuItem add(CharSequence charSequence) {
        return add(0, 0, 0, charSequence);
    }

    public MenuItem add(int i) {
        return add(0, 0, 0, i);
    }

    public MenuItem add(int i, int i2, int i3, int i4) {
        return add(i, i2, i3, this.mContext.getResources().getString(i4));
    }

    public MenuItem add(int i, int i2, int i3, CharSequence charSequence) {
        ActionMenuItem actionMenuItem = new ActionMenuItem(getContext(), i, i2, 0, i3, charSequence);
        this.mItems.add(i3, actionMenuItem);
        return actionMenuItem;
    }

    public int addIntentOptions(int i, int i2, int i3, ComponentName componentName, Intent[] intentArr, Intent intent, int i4, MenuItem[] menuItemArr) {
        Intent intent2;
        PackageManager packageManager = this.mContext.getPackageManager();
        List<ResolveInfo> queryIntentActivityOptions = packageManager.queryIntentActivityOptions(componentName, intentArr, intent, 0);
        int size = queryIntentActivityOptions != null ? queryIntentActivityOptions.size() : 0;
        if ((i4 & 1) == 0) {
            removeGroup(i);
        }
        for (int i5 = 0; i5 < size; i5++) {
            ResolveInfo resolveInfo = queryIntentActivityOptions.get(i5);
            if (resolveInfo.specificIndex < 0) {
                intent2 = intent;
            } else {
                intent2 = intentArr[resolveInfo.specificIndex];
            }
            Intent intent3 = new Intent(intent2);
            intent3.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName, resolveInfo.activityInfo.name));
            MenuItem intent4 = add(i, i2, i3, resolveInfo.loadLabel(packageManager)).setIcon(resolveInfo.loadIcon(packageManager)).setIntent(intent3);
            if (menuItemArr != null && resolveInfo.specificIndex >= 0) {
                menuItemArr[resolveInfo.specificIndex] = intent4;
            }
        }
        return size;
    }

    public SubMenu addSubMenu(CharSequence charSequence) {
        return null;
    }

    public SubMenu addSubMenu(int i) {
        return null;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, CharSequence charSequence) {
        return null;
    }

    public SubMenu addSubMenu(int i, int i2, int i3, int i4) {
        return null;
    }

    public void clear() {
        this.mItems.clear();
    }

    public void close() {
    }

    private int findItemIndex(int i) {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            if (arrayList.get(i2).getItemId() == i) {
                return i2;
            }
        }
        return -1;
    }

    public MenuItem findItem(int i) {
        return this.mItems.get(findItemIndex(i));
    }

    public MenuItem getItem(int i) {
        return this.mItems.get(i);
    }

    public boolean hasVisibleItems() {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i = 0; i < size; i++) {
            if (arrayList.get(i).isVisible()) {
                return true;
            }
        }
        return false;
    }

    private ActionMenuItem findItemWithShortcut(int i, KeyEvent keyEvent) {
        boolean z = this.mIsQwerty;
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ActionMenuItem actionMenuItem = arrayList.get(i2);
            if (i == (z ? actionMenuItem.getAlphabeticShortcut() : actionMenuItem.getNumericShortcut())) {
                return actionMenuItem;
            }
        }
        return null;
    }

    public boolean isShortcutKey(int i, KeyEvent keyEvent) {
        return findItemWithShortcut(i, keyEvent) != null;
    }

    public boolean performIdentifierAction(int i, int i2) {
        int findItemIndex = findItemIndex(i);
        if (findItemIndex < 0) {
            return false;
        }
        return this.mItems.get(findItemIndex).invoke();
    }

    public boolean performShortcut(int i, KeyEvent keyEvent, int i2) {
        ActionMenuItem findItemWithShortcut = findItemWithShortcut(i, keyEvent);
        if (findItemWithShortcut == null) {
            return false;
        }
        return findItemWithShortcut.invoke();
    }

    public void removeGroup(int i) {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            if (arrayList.get(i2).getGroupId() == i) {
                arrayList.remove(i2);
                size--;
            } else {
                i2++;
            }
        }
    }

    public void removeItem(int i) {
        this.mItems.remove(findItemIndex(i));
    }

    public void setGroupCheckable(int i, boolean z, boolean z2) {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ActionMenuItem actionMenuItem = arrayList.get(i2);
            if (actionMenuItem.getGroupId() == i) {
                actionMenuItem.setCheckable(z);
                actionMenuItem.setExclusiveCheckable(z2);
            }
        }
    }

    public void setGroupEnabled(int i, boolean z) {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ActionMenuItem actionMenuItem = arrayList.get(i2);
            if (actionMenuItem.getGroupId() == i) {
                actionMenuItem.setEnabled(z);
            }
        }
    }

    public void setGroupVisible(int i, boolean z) {
        ArrayList<ActionMenuItem> arrayList = this.mItems;
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            ActionMenuItem actionMenuItem = arrayList.get(i2);
            if (actionMenuItem.getGroupId() == i) {
                actionMenuItem.setVisible(z);
            }
        }
    }

    public void setQwertyMode(boolean z) {
        this.mIsQwerty = z;
    }

    public int size() {
        return this.mItems.size();
    }
}
