package com.a.a.a;

import android.view.View;
import android.widget.ListAdapter;
import com.a.a.b.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* compiled from: MergeAdapter */
class f {

    /* renamed from: a  reason: collision with root package name */
    protected ArrayList<e> f205a;

    /* renamed from: b  reason: collision with root package name */
    protected ArrayList<ListAdapter> f206b;

    private f() {
        this.f205a = new ArrayList<>();
        this.f206b = null;
    }

    /* access modifiers changed from: package-private */
    public void a(ListAdapter listAdapter) {
        this.f205a.add(new e(listAdapter, true));
    }

    /* access modifiers changed from: package-private */
    public void a(View view, boolean z) {
        Iterator<e> it = this.f205a.iterator();
        while (it.hasNext()) {
            e next = it.next();
            if ((next.f203a instanceof a) && ((a) next.f203a).a(view)) {
                next.f204b = z;
                this.f206b = null;
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<e> a() {
        return this.f205a;
    }

    /* access modifiers changed from: package-private */
    public List<ListAdapter> b() {
        if (this.f206b == null) {
            this.f206b = new ArrayList<>();
            Iterator<e> it = this.f205a.iterator();
            while (it.hasNext()) {
                e next = it.next();
                if (next.f204b) {
                    this.f206b.add(next.f203a);
                }
            }
        }
        return this.f206b;
    }
}
