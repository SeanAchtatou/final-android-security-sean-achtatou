package com.a.a.a;

import android.database.DataSetObserver;

/* compiled from: MergeAdapter */
class c extends DataSetObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f202a;

    private c(a aVar) {
        this.f202a = aVar;
    }

    public void onChanged() {
        this.f202a.notifyDataSetChanged();
    }

    public void onInvalidated() {
        this.f202a.notifyDataSetInvalidated();
    }
}
