package com.a.a.a;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.SectionIndexer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: MergeAdapter */
public class a extends BaseAdapter implements SectionIndexer {

    /* renamed from: a  reason: collision with root package name */
    protected f f201a = new f();

    public void a(ListAdapter listAdapter) {
        this.f201a.a(listAdapter);
        listAdapter.registerDataSetObserver(new c(this));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.a.a.a.a.a(android.view.View, boolean):void
     arg types: [android.view.View, int]
     candidates:
      com.a.a.a.a.a(java.util.List<android.view.View>, boolean):void
      com.a.a.a.a.a(android.view.View, boolean):void */
    public void a(View view) {
        a(view, false);
    }

    public void a(View view, boolean z) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(view);
        a(arrayList, z);
    }

    public void a(List<View> list, boolean z) {
        if (z) {
            a(new d(list));
        } else {
            a(new com.a.a.b.a(list));
        }
    }

    public Object getItem(int i) {
        for (ListAdapter next : a()) {
            int count = next.getCount();
            if (i < count) {
                return next.getItem(i);
            }
            i -= count;
        }
        return null;
    }

    public int getCount() {
        int i = 0;
        Iterator<ListAdapter> it = a().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = it.next().getCount() + i2;
        }
    }

    public int getViewTypeCount() {
        int i = 0;
        Iterator<e> it = this.f201a.a().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return Math.max(i2, 1);
            }
            i = it.next().f203a.getViewTypeCount() + i2;
        }
    }

    public int getItemViewType(int i) {
        int i2 = 0;
        Iterator<e> it = this.f201a.a().iterator();
        while (true) {
            int i3 = i2;
            if (!it.hasNext()) {
                return -1;
            }
            e next = it.next();
            if (next.f204b) {
                int count = next.f203a.getCount();
                if (i < count) {
                    return next.f203a.getItemViewType(i) + i3;
                }
                i -= count;
            }
            i2 = next.f203a.getViewTypeCount() + i3;
        }
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int i) {
        for (ListAdapter next : a()) {
            int count = next.getCount();
            if (i < count) {
                return next.isEnabled(i);
            }
            i -= count;
        }
        return false;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        for (ListAdapter next : a()) {
            int count = next.getCount();
            if (i < count) {
                return next.getView(i, view, viewGroup);
            }
            i -= count;
        }
        return null;
    }

    public long getItemId(int i) {
        for (ListAdapter next : a()) {
            int count = next.getCount();
            if (i < count) {
                return next.getItemId(i);
            }
            i -= count;
        }
        return -1;
    }

    public int getPositionForSection(int i) {
        int i2;
        int i3 = 0;
        for (ListAdapter next : a()) {
            if (next instanceof SectionIndexer) {
                Object[] sections = ((SectionIndexer) next).getSections();
                if (sections != null) {
                    i2 = sections.length;
                } else {
                    i2 = 0;
                }
                if (i < i2) {
                    return i3 + ((SectionIndexer) next).getPositionForSection(i);
                }
                if (sections != null) {
                    i -= i2;
                }
            }
            i3 = next.getCount() + i3;
        }
        return 0;
    }

    public int getSectionForPosition(int i) {
        Object[] sections;
        int i2 = 0;
        for (ListAdapter next : a()) {
            int count = next.getCount();
            if (i >= count) {
                if ((next instanceof SectionIndexer) && (sections = ((SectionIndexer) next).getSections()) != null) {
                    i2 += sections.length;
                }
                i -= count;
                i2 = i2;
            } else if (next instanceof SectionIndexer) {
                return i2 + ((SectionIndexer) next).getSectionForPosition(i);
            } else {
                return 0;
            }
        }
        return 0;
    }

    public Object[] getSections() {
        Object[] sections;
        ArrayList arrayList = new ArrayList();
        for (ListAdapter next : a()) {
            if ((next instanceof SectionIndexer) && (sections = ((SectionIndexer) next).getSections()) != null) {
                Collections.addAll(arrayList, sections);
            }
        }
        if (arrayList.size() == 0) {
            return new String[0];
        }
        return arrayList.toArray(new Object[0]);
    }

    public void b(View view, boolean z) {
        this.f201a.a(view, z);
        notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public List<ListAdapter> a() {
        return this.f201a.b();
    }
}
