package com.a.a.b;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.List;

/* compiled from: SackOfViewsAdapter */
public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List<View> f207a = null;

    public a(List<View> list) {
        this.f207a = list;
    }

    public Object getItem(int i) {
        return this.f207a.get(i);
    }

    public int getCount() {
        return this.f207a.size();
    }

    public int getViewTypeCount() {
        return getCount();
    }

    public int getItemViewType(int i) {
        return i;
    }

    public boolean areAllItemsEnabled() {
        return false;
    }

    public boolean isEnabled(int i) {
        return false;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = this.f207a.get(i);
        if (view2 != null) {
            return view2;
        }
        View a2 = a(i, viewGroup);
        this.f207a.set(i, a2);
        return a2;
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public boolean a(View view) {
        return this.f207a.contains(view);
    }

    /* access modifiers changed from: protected */
    public View a(int i, ViewGroup viewGroup) {
        throw new RuntimeException("You must override newView()!");
    }
}
