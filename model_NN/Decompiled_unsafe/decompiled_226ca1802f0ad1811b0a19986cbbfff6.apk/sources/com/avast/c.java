package com.avast;

import com.actionbarsherlock.view.Menu;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: Pipe */
public class c {
    public static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[Menu.CATEGORY_CONTAINER];
        while (true) {
            int read = inputStream.read(bArr);
            if (read >= 0) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }
}
