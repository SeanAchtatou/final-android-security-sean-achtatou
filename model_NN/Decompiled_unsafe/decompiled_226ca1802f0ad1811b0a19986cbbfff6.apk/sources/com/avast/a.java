package com.avast;

import java.io.InputStream;
import java.nio.ByteBuffer;

/* compiled from: ByteBufferBackedInputStream */
public class a extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final ByteBuffer f209a;

    public a(ByteBuffer byteBuffer) {
        this.f209a = byteBuffer;
    }

    public synchronized int read() {
        byte b2;
        if (!this.f209a.hasRemaining()) {
            b2 = -1;
        } else {
            b2 = this.f209a.get() & 255;
        }
        return b2;
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        int min;
        int remaining = this.f209a.remaining();
        if (remaining == 0) {
            min = -1;
        } else {
            min = Math.min(i2, remaining);
            this.f209a.get(bArr, i, min);
        }
        return min;
    }
}
