package com.avast;

import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ByteBufferBackedOutputStream */
public class b extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    protected final int f1266a;

    /* renamed from: b  reason: collision with root package name */
    protected ByteBuffer f1267b;

    /* renamed from: c  reason: collision with root package name */
    protected final List<ByteBuffer> f1268c = new LinkedList();
    protected int d = 0;

    public b(int i) {
        this.f1266a = i;
        this.f1267b = ByteBuffer.allocate(Math.max(1024, i));
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f1267b.flip();
        this.f1268c.add(this.f1267b);
        this.f1267b = ByteBuffer.allocate(Math.max(1024, this.f1266a));
    }

    public synchronized void write(int i) {
        if (this.f1267b.remaining() == 0) {
            a();
        }
        this.f1267b.put((byte) i);
    }

    public synchronized void write(byte[] bArr, int i, int i2) {
        while (i2 > 0) {
            if (this.f1267b.remaining() == 0) {
                a();
            }
            int min = Math.min(this.f1267b.remaining(), i2);
            this.f1267b.put(bArr, i, min);
            i += min;
            i2 -= min;
            this.d = min + this.d;
        }
    }

    public ByteBuffer b() {
        this.f1267b.flip();
        if (this.f1268c.isEmpty()) {
            return this.f1267b;
        }
        ByteBuffer allocate = ByteBuffer.allocate(this.d);
        this.f1268c.add(this.f1267b);
        for (ByteBuffer put : this.f1268c) {
            allocate.put(put);
        }
        allocate.flip();
        return allocate;
    }
}
