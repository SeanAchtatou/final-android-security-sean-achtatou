package com.avast.d;

import com.avast.a.a;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;
import org.apache.http.entity.AbstractHttpEntity;

/* compiled from: StreamBackClient */
class e extends AbstractHttpEntity {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ byte[] f1557a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ InputStream f1558b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ d f1559c;

    e(d dVar, byte[] bArr, InputStream inputStream) {
        this.f1559c = dVar;
        this.f1557a = bArr;
        this.f1558b = inputStream;
    }

    public boolean isRepeatable() {
        return false;
    }

    public long getContentLength() {
        return -1;
    }

    public boolean isStreaming() {
        return false;
    }

    public InputStream getContent() {
        throw new UnsupportedOperationException();
    }

    public void writeTo(OutputStream outputStream) {
        int i = 0;
        try {
            OutputStream a2 = a.a(outputStream, this.f1557a);
            byte[] bArr = new byte[this.f1559c.e.h];
            while (i >= 0) {
                i = this.f1558b.read(bArr);
                this.f1559c.b("StreamBackClient: read " + i + " bytes from input stream in AbstractEntity");
                if (i > 0) {
                    a2.write(bArr, 0, i);
                    this.f1559c.b("StreamBackClient: written " + i + " bytes to output stream in AbstractEntity");
                }
            }
            a2.flush();
            a2.close();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            throw new IOException(e.getMessage());
        } catch (NoSuchPaddingException e2) {
            e2.printStackTrace();
            throw new IOException(e2.getMessage());
        } catch (NoSuchAlgorithmException e3) {
            e3.printStackTrace();
            throw new IOException(e3.getMessage());
        } catch (InvalidAlgorithmParameterException e4) {
            e4.printStackTrace();
            throw new IOException(e4.getMessage());
        }
    }
}
