package com.avast.d;

import com.google.protobuf.ByteString;

/* compiled from: SbKey */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private final ByteString f1551a;

    /* renamed from: b  reason: collision with root package name */
    private final ByteString f1552b;

    /* renamed from: c  reason: collision with root package name */
    private final long f1553c;

    public c(ByteString byteString, ByteString byteString2, long j) {
        this.f1551a = byteString;
        this.f1552b = byteString2;
        this.f1553c = j;
    }

    public ByteString a() {
        return this.f1551a;
    }

    public ByteString b() {
        return this.f1552b;
    }

    public long c() {
        return this.f1553c;
    }

    public boolean d() {
        return this.f1553c < System.currentTimeMillis();
    }
}
