package com.avast.d.a;

import com.avast.d.b.s;
import com.avast.d.b.y;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* compiled from: BufferDataProvider */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f1503a;

    public a(byte[] bArr) {
        this.f1503a = bArr;
    }

    public InputStream a(Long l, Long l2, y yVar) {
        if (l == null) {
            l = 0L;
        }
        if (l2 == null || l.longValue() + l2.longValue() > ((long) this.f1503a.length)) {
            l2 = Long.valueOf(((long) this.f1503a.length) - l.longValue());
        }
        return new com.avast.a(ByteBuffer.wrap(this.f1503a, l.intValue(), l2.intValue()));
    }

    public void a(s sVar) {
    }
}
