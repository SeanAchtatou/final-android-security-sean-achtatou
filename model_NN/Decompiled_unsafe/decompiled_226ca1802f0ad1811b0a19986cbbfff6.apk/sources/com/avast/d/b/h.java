package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class h extends GeneratedMessageLite implements j {

    /* renamed from: a  reason: collision with root package name */
    private static final h f1513a = new h(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1514b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public n f1515c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public ByteString e;
    private byte f;
    private int g;

    private h(i iVar) {
        super(iVar);
        this.f = -1;
        this.g = -1;
    }

    private h(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static h a() {
        return f1513a;
    }

    public boolean b() {
        return (this.f1514b & 1) == 1;
    }

    public n c() {
        return this.f1515c;
    }

    public boolean d() {
        return (this.f1514b & 2) == 2;
    }

    public c e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1514b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    private void k() {
        this.f1515c = n.a();
        this.d = c.a();
        this.e = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1514b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1515c);
        }
        if ((this.f1514b & 2) == 2) {
            codedOutputStream.writeMessage(2, this.d);
        }
        if ((this.f1514b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1514b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1515c);
            }
            if ((this.f1514b & 2) == 2) {
                i += CodedOutputStream.computeMessageSize(2, this.d);
            }
            if ((this.f1514b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, this.e);
            }
            this.g = i;
        }
        return i;
    }

    public static i h() {
        return i.k();
    }

    /* renamed from: i */
    public i newBuilderForType() {
        return h();
    }

    public static i a(h hVar) {
        return h().mergeFrom(hVar);
    }

    /* renamed from: j */
    public i toBuilder() {
        return a(this);
    }

    static {
        f1513a.k();
    }
}
