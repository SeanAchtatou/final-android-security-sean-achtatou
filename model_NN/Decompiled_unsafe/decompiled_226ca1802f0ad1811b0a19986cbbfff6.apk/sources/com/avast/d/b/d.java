package com.avast.d.b;

import com.google.protobuf.Internal;

/* compiled from: StreamBack */
public enum d implements Internal.EnumLite {
    CHROME(0, 0),
    FIREFOX(1, 1),
    IE(2, 2),
    OPERA(3, 3),
    SAFAR(4, 4),
    PRODUCTS(5, 5),
    VIDEO(6, 6);
    
    private static Internal.EnumLiteMap<d> h = new e();
    private final int i;

    public final int getNumber() {
        return this.i;
    }

    public static d a(int i2) {
        switch (i2) {
            case 0:
                return CHROME;
            case 1:
                return FIREFOX;
            case 2:
                return IE;
            case 3:
                return OPERA;
            case 4:
                return SAFAR;
            case 5:
                return PRODUCTS;
            case 6:
                return VIDEO;
            default:
                return null;
        }
    }

    private d(int i2, int i3) {
        this.i = i3;
    }
}
