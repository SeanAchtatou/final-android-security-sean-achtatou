package com.avast.d.b;

import com.google.protobuf.Internal;

/* compiled from: StreamBack */
public enum s implements Internal.EnumLite {
    REMOVE(0, 0),
    LATER(1, 1),
    SEND(2, 2);
    
    private static Internal.EnumLiteMap<s> d = new t();
    private final int e;

    public final int getNumber() {
        return this.e;
    }

    public static s a(int i) {
        switch (i) {
            case 0:
                return REMOVE;
            case 1:
                return LATER;
            case 2:
                return SEND;
            default:
                return null;
        }
    }

    private s(int i, int i2) {
        this.e = i2;
    }
}
