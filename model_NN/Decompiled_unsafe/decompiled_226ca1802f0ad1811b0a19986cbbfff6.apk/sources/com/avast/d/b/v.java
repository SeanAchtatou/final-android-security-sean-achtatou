package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class v extends GeneratedMessageLite implements x {

    /* renamed from: a  reason: collision with root package name */
    private static final v f1539a = new v(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1540b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public n f1541c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public long g;
    /* access modifiers changed from: private */
    public ByteString h;
    private byte i;
    private int j;

    private v(w wVar) {
        super(wVar);
        this.i = -1;
        this.j = -1;
    }

    private v(boolean z) {
        this.i = -1;
        this.j = -1;
    }

    public static v a() {
        return f1539a;
    }

    public boolean b() {
        return (this.f1540b & 1) == 1;
    }

    public n c() {
        return this.f1541c;
    }

    public boolean d() {
        return (this.f1540b & 2) == 2;
    }

    public c e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1540b & 4) == 4;
    }

    public int g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1540b & 8) == 8;
    }

    public int i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1540b & 16) == 16;
    }

    public long k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1540b & 32) == 32;
    }

    public ByteString m() {
        return this.h;
    }

    private void q() {
        this.f1541c = n.a();
        this.d = c.a();
        this.e = 0;
        this.f = 0;
        this.g = 0;
        this.h = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.i;
        if (b2 == -1) {
            this.i = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1540b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1541c);
        }
        if ((this.f1540b & 2) == 2) {
            codedOutputStream.writeMessage(2, this.d);
        }
        if ((this.f1540b & 4) == 4) {
            codedOutputStream.writeInt32(4, this.e);
        }
        if ((this.f1540b & 8) == 8) {
            codedOutputStream.writeInt32(5, this.f);
        }
        if ((this.f1540b & 16) == 16) {
            codedOutputStream.writeInt64(6, this.g);
        }
        if ((this.f1540b & 32) == 32) {
            codedOutputStream.writeBytes(7, this.h);
        }
    }

    public int getSerializedSize() {
        int i2 = this.j;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1540b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeMessageSize(1, this.f1541c);
            }
            if ((this.f1540b & 2) == 2) {
                i2 += CodedOutputStream.computeMessageSize(2, this.d);
            }
            if ((this.f1540b & 4) == 4) {
                i2 += CodedOutputStream.computeInt32Size(4, this.e);
            }
            if ((this.f1540b & 8) == 8) {
                i2 += CodedOutputStream.computeInt32Size(5, this.f);
            }
            if ((this.f1540b & 16) == 16) {
                i2 += CodedOutputStream.computeInt64Size(6, this.g);
            }
            if ((this.f1540b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(7, this.h);
            }
            this.j = i2;
        }
        return i2;
    }

    public static w n() {
        return w.k();
    }

    /* renamed from: o */
    public w newBuilderForType() {
        return n();
    }

    public static w a(v vVar) {
        return n().mergeFrom(vVar);
    }

    /* renamed from: p */
    public w toBuilder() {
        return a(this);
    }

    static {
        f1539a.q();
    }
}
