package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class y extends GeneratedMessageLite implements aa {

    /* renamed from: a  reason: collision with root package name */
    private static final y f1545a = new y(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1546b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public n f1547c;
    /* access modifiers changed from: private */
    public q d;
    /* access modifiers changed from: private */
    public ByteString e;
    private byte f;
    private int g;

    private y(z zVar) {
        super(zVar);
        this.f = -1;
        this.g = -1;
    }

    private y(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static y a() {
        return f1545a;
    }

    public boolean b() {
        return (this.f1546b & 1) == 1;
    }

    public n c() {
        return this.f1547c;
    }

    public boolean d() {
        return (this.f1546b & 2) == 2;
    }

    public q e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1546b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    private void k() {
        this.f1547c = n.a();
        this.d = q.a();
        this.e = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1546b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1547c);
        }
        if ((this.f1546b & 2) == 2) {
            codedOutputStream.writeMessage(2, this.d);
        }
        if ((this.f1546b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f1546b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1547c);
            }
            if ((this.f1546b & 2) == 2) {
                i += CodedOutputStream.computeMessageSize(2, this.d);
            }
            if ((this.f1546b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, this.e);
            }
            this.g = i;
        }
        return i;
    }

    public static y a(ByteString byteString) {
        return ((z) h().mergeFrom(byteString)).l();
    }

    public static z h() {
        return z.k();
    }

    /* renamed from: i */
    public z newBuilderForType() {
        return h();
    }

    public static z a(y yVar) {
        return h().mergeFrom(yVar);
    }

    /* renamed from: j */
    public z toBuilder() {
        return a(this);
    }

    static {
        f1545a.k();
    }
}
