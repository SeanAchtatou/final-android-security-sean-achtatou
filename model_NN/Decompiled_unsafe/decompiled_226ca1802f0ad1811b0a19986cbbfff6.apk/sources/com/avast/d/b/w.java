package com.avast.d.b;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class w extends GeneratedMessageLite.Builder<v, w> implements x {

    /* renamed from: a  reason: collision with root package name */
    private int f1542a;

    /* renamed from: b  reason: collision with root package name */
    private n f1543b = n.a();

    /* renamed from: c  reason: collision with root package name */
    private c f1544c = c.a();
    private int d;
    private int e;
    private long f;
    private ByteString g = ByteString.EMPTY;

    private w() {
        j();
    }

    private void j() {
    }

    /* access modifiers changed from: private */
    public static w k() {
        return new w();
    }

    /* renamed from: a */
    public w clone() {
        return k().mergeFrom(d());
    }

    /* renamed from: b */
    public v getDefaultInstanceForType() {
        return v.a();
    }

    /* renamed from: c */
    public v build() {
        v d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public v d() {
        int i = 1;
        v vVar = new v(this);
        int i2 = this.f1542a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        n unused = vVar.f1541c = this.f1543b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        c unused2 = vVar.d = this.f1544c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        int unused3 = vVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        int unused4 = vVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        long unused5 = vVar.g = this.f;
        if ((i2 & 32) == 32) {
            i |= 32;
        }
        ByteString unused6 = vVar.h = this.g;
        int unused7 = vVar.f1540b = i;
        return vVar;
    }

    /* renamed from: a */
    public w mergeFrom(v vVar) {
        if (vVar != v.a()) {
            if (vVar.b()) {
                b(vVar.c());
            }
            if (vVar.d()) {
                b(vVar.e());
            }
            if (vVar.f()) {
                a(vVar.g());
            }
            if (vVar.h()) {
                b(vVar.i());
            }
            if (vVar.j()) {
                a(vVar.k());
            }
            if (vVar.l()) {
                a(vVar.m());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public w mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    o d2 = n.d();
                    if (e()) {
                        d2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case 18:
                    f v = c.v();
                    if (g()) {
                        v.mergeFrom(h());
                    }
                    codedInputStream.readMessage(v, extensionRegistryLite);
                    a(v.d());
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1542a |= 4;
                    this.d = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1542a |= 8;
                    this.e = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1542a |= 16;
                    this.f = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f1542a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1542a & 1) == 1;
    }

    public n f() {
        return this.f1543b;
    }

    public w a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException();
        }
        this.f1543b = nVar;
        this.f1542a |= 1;
        return this;
    }

    public w a(o oVar) {
        this.f1543b = oVar.build();
        this.f1542a |= 1;
        return this;
    }

    public w b(n nVar) {
        if ((this.f1542a & 1) != 1 || this.f1543b == n.a()) {
            this.f1543b = nVar;
        } else {
            this.f1543b = n.a(this.f1543b).mergeFrom(nVar).d();
        }
        this.f1542a |= 1;
        return this;
    }

    public boolean g() {
        return (this.f1542a & 2) == 2;
    }

    public c h() {
        return this.f1544c;
    }

    public w a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.f1544c = cVar;
        this.f1542a |= 2;
        return this;
    }

    public w b(c cVar) {
        if ((this.f1542a & 2) != 2 || this.f1544c == c.a()) {
            this.f1544c = cVar;
        } else {
            this.f1544c = c.a(this.f1544c).mergeFrom(cVar).d();
        }
        this.f1542a |= 2;
        return this;
    }

    public w a(int i) {
        this.f1542a |= 4;
        this.d = i;
        return this;
    }

    public w b(int i) {
        this.f1542a |= 8;
        this.e = i;
        return this;
    }

    public w a(long j) {
        this.f1542a |= 16;
        this.f = j;
        return this;
    }

    public w a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1542a |= 32;
        this.g = byteString;
        return this;
    }
}
