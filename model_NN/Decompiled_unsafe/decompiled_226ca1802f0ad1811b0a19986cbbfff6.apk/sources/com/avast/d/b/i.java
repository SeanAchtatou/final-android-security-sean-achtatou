package com.avast.d.b;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class i extends GeneratedMessageLite.Builder<h, i> implements j {

    /* renamed from: a  reason: collision with root package name */
    private int f1516a;

    /* renamed from: b  reason: collision with root package name */
    private n f1517b = n.a();

    /* renamed from: c  reason: collision with root package name */
    private c f1518c = c.a();
    private ByteString d = ByteString.EMPTY;

    private i() {
        j();
    }

    private void j() {
    }

    /* access modifiers changed from: private */
    public static i k() {
        return new i();
    }

    /* renamed from: a */
    public i clone() {
        return k().mergeFrom(d());
    }

    /* renamed from: b */
    public h getDefaultInstanceForType() {
        return h.a();
    }

    /* renamed from: c */
    public h build() {
        h d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public h d() {
        int i = 1;
        h hVar = new h(this);
        int i2 = this.f1516a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        n unused = hVar.f1515c = this.f1517b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        c unused2 = hVar.d = this.f1518c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = hVar.e = this.d;
        int unused4 = hVar.f1514b = i;
        return hVar;
    }

    /* renamed from: a */
    public i mergeFrom(h hVar) {
        if (hVar != h.a()) {
            if (hVar.b()) {
                b(hVar.c());
            }
            if (hVar.d()) {
                b(hVar.e());
            }
            if (hVar.f()) {
                a(hVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public i mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    o d2 = n.d();
                    if (e()) {
                        d2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case 18:
                    f v = c.v();
                    if (g()) {
                        v.mergeFrom(h());
                    }
                    codedInputStream.readMessage(v, extensionRegistryLite);
                    a(v.d());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1516a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1516a & 1) == 1;
    }

    public n f() {
        return this.f1517b;
    }

    public i a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException();
        }
        this.f1517b = nVar;
        this.f1516a |= 1;
        return this;
    }

    public i a(o oVar) {
        this.f1517b = oVar.build();
        this.f1516a |= 1;
        return this;
    }

    public i b(n nVar) {
        if ((this.f1516a & 1) != 1 || this.f1517b == n.a()) {
            this.f1517b = nVar;
        } else {
            this.f1517b = n.a(this.f1517b).mergeFrom(nVar).d();
        }
        this.f1516a |= 1;
        return this;
    }

    public boolean g() {
        return (this.f1516a & 2) == 2;
    }

    public c h() {
        return this.f1518c;
    }

    public i a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.f1518c = cVar;
        this.f1516a |= 2;
        return this;
    }

    public i b(c cVar) {
        if ((this.f1516a & 2) != 2 || this.f1518c == c.a()) {
            this.f1518c = cVar;
        } else {
            this.f1518c = c.a(this.f1518c).mergeFrom(cVar).d();
        }
        this.f1516a |= 2;
        return this;
    }

    public i a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1516a |= 4;
        this.d = byteString;
        return this;
    }
}
