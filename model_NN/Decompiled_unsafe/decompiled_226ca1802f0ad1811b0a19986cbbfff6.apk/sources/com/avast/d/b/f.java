package com.avast.d.b;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class f extends GeneratedMessageLite.Builder<c, f> implements g {

    /* renamed from: a  reason: collision with root package name */
    private int f1510a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1511b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1512c = ByteString.EMPTY;
    private ByteString d = ByteString.EMPTY;
    private ByteString e = ByteString.EMPTY;
    private d f = d.CHROME;
    private int g;
    private ByteString h = ByteString.EMPTY;
    private ByteString i = ByteString.EMPTY;
    private ByteString j = ByteString.EMPTY;
    private ByteString k = ByteString.EMPTY;

    private f() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static f g() {
        return new f();
    }

    /* renamed from: a */
    public f clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public c d() {
        int i2 = 1;
        c cVar = new c(this);
        int i3 = this.f1510a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        ByteString unused = cVar.f1506c = this.f1511b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        ByteString unused2 = cVar.d = this.f1512c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        ByteString unused3 = cVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        ByteString unused4 = cVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        d unused5 = cVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        int unused6 = cVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        ByteString unused7 = cVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        ByteString unused8 = cVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 256;
        }
        ByteString unused9 = cVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 512;
        }
        ByteString unused10 = cVar.l = this.k;
        int unused11 = cVar.f1505b = i2;
        return cVar;
    }

    /* renamed from: a */
    public f mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                b(cVar.e());
            }
            if (cVar.f()) {
                c(cVar.g());
            }
            if (cVar.h()) {
                d(cVar.i());
            }
            if (cVar.j()) {
                a(cVar.k());
            }
            if (cVar.l()) {
                a(cVar.m());
            }
            if (cVar.n()) {
                e(cVar.o());
            }
            if (cVar.p()) {
                f(cVar.q());
            }
            if (cVar.r()) {
                g(cVar.s());
            }
            if (cVar.t()) {
                h(cVar.u());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public f mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1510a |= 1;
                    this.f1511b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f1510a |= 2;
                    this.f1512c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1510a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f1510a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    d a2 = d.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1510a |= 16;
                        this.f = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f1510a |= 32;
                    this.g = codedInputStream.readSInt32();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f1510a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f1510a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f1510a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 82:
                    this.f1510a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public f a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 1;
        this.f1511b = byteString;
        return this;
    }

    public f b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 2;
        this.f1512c = byteString;
        return this;
    }

    public f c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 4;
        this.d = byteString;
        return this;
    }

    public f d(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 8;
        this.e = byteString;
        return this;
    }

    public f a(d dVar) {
        if (dVar == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 16;
        this.f = dVar;
        return this;
    }

    public f a(int i2) {
        this.f1510a |= 32;
        this.g = i2;
        return this;
    }

    public f e(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 64;
        this.h = byteString;
        return this;
    }

    public f f(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = byteString;
        return this;
    }

    public f g(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 256;
        this.j = byteString;
        return this;
    }

    public f h(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1510a |= 512;
        this.k = byteString;
        return this;
    }
}
