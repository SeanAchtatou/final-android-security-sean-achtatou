package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class q extends GeneratedMessageLite implements u {

    /* renamed from: a  reason: collision with root package name */
    private static final q f1530a = new q(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1531b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public s f1532c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public long f;
    /* access modifiers changed from: private */
    public long g;
    private byte h;
    private int i;

    private q(r rVar) {
        super(rVar);
        this.h = -1;
        this.i = -1;
    }

    private q(boolean z) {
        this.h = -1;
        this.i = -1;
    }

    public static q a() {
        return f1530a;
    }

    public boolean b() {
        return (this.f1531b & 1) == 1;
    }

    public s c() {
        return this.f1532c;
    }

    public boolean d() {
        return (this.f1531b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1531b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1531b & 8) == 8;
    }

    public long i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1531b & 16) == 16;
    }

    public long k() {
        return this.g;
    }

    private void o() {
        this.f1532c = s.REMOVE;
        this.d = ByteString.EMPTY;
        this.e = ByteString.EMPTY;
        this.f = 0;
        this.g = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.h;
        if (b2 == -1) {
            this.h = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1531b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f1532c.getNumber());
        }
        if ((this.f1531b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1531b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f1531b & 8) == 8) {
            codedOutputStream.writeInt64(4, this.f);
        }
        if ((this.f1531b & 16) == 16) {
            codedOutputStream.writeInt64(5, this.g);
        }
    }

    public int getSerializedSize() {
        int i2 = this.i;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1531b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeEnumSize(1, this.f1532c.getNumber());
            }
            if ((this.f1531b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1531b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f1531b & 8) == 8) {
                i2 += CodedOutputStream.computeInt64Size(4, this.f);
            }
            if ((this.f1531b & 16) == 16) {
                i2 += CodedOutputStream.computeInt64Size(5, this.g);
            }
            this.i = i2;
        }
        return i2;
    }

    public static r l() {
        return r.g();
    }

    /* renamed from: m */
    public r newBuilderForType() {
        return l();
    }

    public static r a(q qVar) {
        return l().mergeFrom(qVar);
    }

    /* renamed from: n */
    public r toBuilder() {
        return a(this);
    }

    static {
        f1530a.o();
    }
}
