package com.avast.d.b;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class r extends GeneratedMessageLite.Builder<q, r> implements u {

    /* renamed from: a  reason: collision with root package name */
    private int f1533a;

    /* renamed from: b  reason: collision with root package name */
    private s f1534b = s.REMOVE;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1535c = ByteString.EMPTY;
    private ByteString d = ByteString.EMPTY;
    private long e;
    private long f;

    private r() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static r g() {
        return new r();
    }

    /* renamed from: a */
    public r clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public q getDefaultInstanceForType() {
        return q.a();
    }

    /* renamed from: c */
    public q build() {
        q d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public q d() {
        int i = 1;
        q qVar = new q(this);
        int i2 = this.f1533a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        s unused = qVar.f1532c = this.f1534b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = qVar.d = this.f1535c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = qVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        long unused4 = qVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        long unused5 = qVar.g = this.f;
        int unused6 = qVar.f1531b = i;
        return qVar;
    }

    /* renamed from: a */
    public r mergeFrom(q qVar) {
        if (qVar != q.a()) {
            if (qVar.b()) {
                a(qVar.c());
            }
            if (qVar.d()) {
                a(qVar.e());
            }
            if (qVar.f()) {
                b(qVar.g());
            }
            if (qVar.h()) {
                a(qVar.i());
            }
            if (qVar.j()) {
                b(qVar.k());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public r mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    s a2 = s.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f1533a |= 1;
                        this.f1534b = a2;
                        break;
                    }
                case 18:
                    this.f1533a |= 2;
                    this.f1535c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1533a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1533a |= 8;
                    this.e = codedInputStream.readInt64();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f1533a |= 16;
                    this.f = codedInputStream.readInt64();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public r a(s sVar) {
        if (sVar == null) {
            throw new NullPointerException();
        }
        this.f1533a |= 1;
        this.f1534b = sVar;
        return this;
    }

    public r a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1533a |= 2;
        this.f1535c = byteString;
        return this;
    }

    public r b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1533a |= 4;
        this.d = byteString;
        return this;
    }

    public r a(long j) {
        this.f1533a |= 8;
        this.e = j;
        return this;
    }

    public r b(long j) {
        this.f1533a |= 16;
        this.f = j;
        return this;
    }
}
