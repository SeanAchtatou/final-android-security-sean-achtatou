package com.avast.d.b;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class l extends GeneratedMessageLite.Builder<k, l> implements m {

    /* renamed from: a  reason: collision with root package name */
    private int f1522a;

    /* renamed from: b  reason: collision with root package name */
    private n f1523b = n.a();

    /* renamed from: c  reason: collision with root package name */
    private ByteString f1524c = ByteString.EMPTY;
    private ByteString d = ByteString.EMPTY;
    private long e;

    private l() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static l i() {
        return new l();
    }

    /* renamed from: a */
    public l clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public k getDefaultInstanceForType() {
        return k.a();
    }

    /* renamed from: c */
    public k build() {
        k d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public k j() {
        k d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public k d() {
        int i = 1;
        k kVar = new k(this);
        int i2 = this.f1522a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        n unused = kVar.f1521c = this.f1523b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = kVar.d = this.f1524c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = kVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        long unused4 = kVar.f = this.e;
        int unused5 = kVar.f1520b = i;
        return kVar;
    }

    /* renamed from: a */
    public l mergeFrom(k kVar) {
        if (kVar != k.a()) {
            if (kVar.b()) {
                b(kVar.c());
            }
            if (kVar.d()) {
                a(kVar.e());
            }
            if (kVar.f()) {
                b(kVar.g());
            }
            if (kVar.h()) {
                a(kVar.i());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public l mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    o d2 = n.d();
                    if (e()) {
                        d2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case 18:
                    this.f1522a |= 2;
                    this.f1524c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1522a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f1522a |= 8;
                    this.e = codedInputStream.readInt64();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1522a & 1) == 1;
    }

    public n f() {
        return this.f1523b;
    }

    public l a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException();
        }
        this.f1523b = nVar;
        this.f1522a |= 1;
        return this;
    }

    public l b(n nVar) {
        if ((this.f1522a & 1) != 1 || this.f1523b == n.a()) {
            this.f1523b = nVar;
        } else {
            this.f1523b = n.a(this.f1523b).mergeFrom(nVar).d();
        }
        this.f1522a |= 1;
        return this;
    }

    public l a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1522a |= 2;
        this.f1524c = byteString;
        return this;
    }

    public l b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1522a |= 4;
        this.d = byteString;
        return this;
    }

    public l a(long j) {
        this.f1522a |= 8;
        this.e = j;
        return this;
    }
}
