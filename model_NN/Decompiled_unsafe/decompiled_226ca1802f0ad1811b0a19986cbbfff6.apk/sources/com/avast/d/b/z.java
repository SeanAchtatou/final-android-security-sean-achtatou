package com.avast.d.b;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class z extends GeneratedMessageLite.Builder<y, z> implements aa {

    /* renamed from: a  reason: collision with root package name */
    private int f1548a;

    /* renamed from: b  reason: collision with root package name */
    private n f1549b = n.a();

    /* renamed from: c  reason: collision with root package name */
    private q f1550c = q.a();
    private ByteString d = ByteString.EMPTY;

    private z() {
        j();
    }

    private void j() {
    }

    /* access modifiers changed from: private */
    public static z k() {
        return new z();
    }

    /* renamed from: a */
    public z clone() {
        return k().mergeFrom(d());
    }

    /* renamed from: b */
    public y getDefaultInstanceForType() {
        return y.a();
    }

    /* renamed from: c */
    public y build() {
        y d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public y l() {
        y d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public y d() {
        int i = 1;
        y yVar = new y(this);
        int i2 = this.f1548a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        n unused = yVar.f1547c = this.f1549b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        q unused2 = yVar.d = this.f1550c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        ByteString unused3 = yVar.e = this.d;
        int unused4 = yVar.f1546b = i;
        return yVar;
    }

    /* renamed from: a */
    public z mergeFrom(y yVar) {
        if (yVar != y.a()) {
            if (yVar.b()) {
                b(yVar.c());
            }
            if (yVar.d()) {
                b(yVar.e());
            }
            if (yVar.f()) {
                a(yVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public z mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    o d2 = n.d();
                    if (e()) {
                        d2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(d2, extensionRegistryLite);
                    a(d2.d());
                    break;
                case 18:
                    r l = q.l();
                    if (g()) {
                        l.mergeFrom(h());
                    }
                    codedInputStream.readMessage(l, extensionRegistryLite);
                    a(l.d());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f1548a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public boolean e() {
        return (this.f1548a & 1) == 1;
    }

    public n f() {
        return this.f1549b;
    }

    public z a(n nVar) {
        if (nVar == null) {
            throw new NullPointerException();
        }
        this.f1549b = nVar;
        this.f1548a |= 1;
        return this;
    }

    public z b(n nVar) {
        if ((this.f1548a & 1) != 1 || this.f1549b == n.a()) {
            this.f1549b = nVar;
        } else {
            this.f1549b = n.a(this.f1549b).mergeFrom(nVar).d();
        }
        this.f1548a |= 1;
        return this;
    }

    public boolean g() {
        return (this.f1548a & 2) == 2;
    }

    public q h() {
        return this.f1550c;
    }

    public z a(q qVar) {
        if (qVar == null) {
            throw new NullPointerException();
        }
        this.f1550c = qVar;
        this.f1548a |= 2;
        return this;
    }

    public z b(q qVar) {
        if ((this.f1548a & 2) != 2 || this.f1550c == q.a()) {
            this.f1550c = qVar;
        } else {
            this.f1550c = q.a(this.f1550c).mergeFrom(qVar).d();
        }
        this.f1548a |= 2;
        return this;
    }

    public z a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1548a |= 4;
        this.d = byteString;
        return this;
    }
}
