package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class n extends GeneratedMessageLite implements p {

    /* renamed from: a  reason: collision with root package name */
    private static final n f1525a = new n(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1526b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1527c;
    private byte d;
    private int e;

    private n(o oVar) {
        super(oVar);
        this.d = -1;
        this.e = -1;
    }

    private n(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static n a() {
        return f1525a;
    }

    public boolean b() {
        return (this.f1526b & 1) == 1;
    }

    public ByteString c() {
        return this.f1527c;
    }

    private void g() {
        this.f1527c = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1526b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1527c);
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f1526b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f1527c);
            }
            this.e = i;
        }
        return i;
    }

    public static o d() {
        return o.g();
    }

    /* renamed from: e */
    public o newBuilderForType() {
        return d();
    }

    public static o a(n nVar) {
        return d().mergeFrom(nVar);
    }

    /* renamed from: f */
    public o toBuilder() {
        return a(this);
    }

    static {
        f1525a.g();
    }
}
