package com.avast.d.b;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class c extends GeneratedMessageLite implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final c f1504a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1505b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f1506c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public ByteString f;
    /* access modifiers changed from: private */
    public d g;
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public ByteString i;
    /* access modifiers changed from: private */
    public ByteString j;
    /* access modifiers changed from: private */
    public ByteString k;
    /* access modifiers changed from: private */
    public ByteString l;
    private byte m;
    private int n;

    private c(f fVar) {
        super(fVar);
        this.m = -1;
        this.n = -1;
    }

    private c(boolean z) {
        this.m = -1;
        this.n = -1;
    }

    public static c a() {
        return f1504a;
    }

    public boolean b() {
        return (this.f1505b & 1) == 1;
    }

    public ByteString c() {
        return this.f1506c;
    }

    public boolean d() {
        return (this.f1505b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1505b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1505b & 8) == 8;
    }

    public ByteString i() {
        return this.f;
    }

    public boolean j() {
        return (this.f1505b & 16) == 16;
    }

    public d k() {
        return this.g;
    }

    public boolean l() {
        return (this.f1505b & 32) == 32;
    }

    public int m() {
        return this.h;
    }

    public boolean n() {
        return (this.f1505b & 64) == 64;
    }

    public ByteString o() {
        return this.i;
    }

    public boolean p() {
        return (this.f1505b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public ByteString q() {
        return this.j;
    }

    public boolean r() {
        return (this.f1505b & 256) == 256;
    }

    public ByteString s() {
        return this.k;
    }

    public boolean t() {
        return (this.f1505b & 512) == 512;
    }

    public ByteString u() {
        return this.l;
    }

    private void y() {
        this.f1506c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
        this.e = ByteString.EMPTY;
        this.f = ByteString.EMPTY;
        this.g = d.CHROME;
        this.h = 0;
        this.i = ByteString.EMPTY;
        this.j = ByteString.EMPTY;
        this.k = ByteString.EMPTY;
        this.l = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.m;
        if (b2 == -1) {
            this.m = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1505b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f1506c);
        }
        if ((this.f1505b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1505b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f1505b & 8) == 8) {
            codedOutputStream.writeBytes(4, this.f);
        }
        if ((this.f1505b & 16) == 16) {
            codedOutputStream.writeEnum(5, this.g.getNumber());
        }
        if ((this.f1505b & 32) == 32) {
            codedOutputStream.writeSInt32(6, this.h);
        }
        if ((this.f1505b & 64) == 64) {
            codedOutputStream.writeBytes(7, this.i);
        }
        if ((this.f1505b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, this.j);
        }
        if ((this.f1505b & 256) == 256) {
            codedOutputStream.writeBytes(9, this.k);
        }
        if ((this.f1505b & 512) == 512) {
            codedOutputStream.writeBytes(10, this.l);
        }
    }

    public int getSerializedSize() {
        int i2 = this.n;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f1505b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f1506c);
            }
            if ((this.f1505b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1505b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f1505b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, this.f);
            }
            if ((this.f1505b & 16) == 16) {
                i2 += CodedOutputStream.computeEnumSize(5, this.g.getNumber());
            }
            if ((this.f1505b & 32) == 32) {
                i2 += CodedOutputStream.computeSInt32Size(6, this.h);
            }
            if ((this.f1505b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, this.i);
            }
            if ((this.f1505b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, this.j);
            }
            if ((this.f1505b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(9, this.k);
            }
            if ((this.f1505b & 512) == 512) {
                i2 += CodedOutputStream.computeBytesSize(10, this.l);
            }
            this.n = i2;
        }
        return i2;
    }

    public static f v() {
        return f.g();
    }

    /* renamed from: w */
    public f newBuilderForType() {
        return v();
    }

    public static f a(c cVar) {
        return v().mergeFrom(cVar);
    }

    /* renamed from: x */
    public f toBuilder() {
        return a(this);
    }

    static {
        f1504a.y();
    }
}
