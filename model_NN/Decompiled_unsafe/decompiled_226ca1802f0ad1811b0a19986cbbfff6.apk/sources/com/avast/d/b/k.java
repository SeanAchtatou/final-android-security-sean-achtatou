package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.io.InputStream;

/* compiled from: StreamBack */
public final class k extends GeneratedMessageLite implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final k f1519a = new k(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1520b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public n f1521c;
    /* access modifiers changed from: private */
    public ByteString d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public long f;
    private byte g;
    private int h;

    private k(l lVar) {
        super(lVar);
        this.g = -1;
        this.h = -1;
    }

    private k(boolean z) {
        this.g = -1;
        this.h = -1;
    }

    public static k a() {
        return f1519a;
    }

    public boolean b() {
        return (this.f1520b & 1) == 1;
    }

    public n c() {
        return this.f1521c;
    }

    public boolean d() {
        return (this.f1520b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    public boolean f() {
        return (this.f1520b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f1520b & 8) == 8;
    }

    public long i() {
        return this.f;
    }

    private void m() {
        this.f1521c = n.a();
        this.d = ByteString.EMPTY;
        this.e = ByteString.EMPTY;
        this.f = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.g;
        if (b2 == -1) {
            this.g = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f1520b & 1) == 1) {
            codedOutputStream.writeMessage(1, this.f1521c);
        }
        if ((this.f1520b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
        if ((this.f1520b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f1520b & 8) == 8) {
            codedOutputStream.writeInt64(4, this.f);
        }
    }

    public int getSerializedSize() {
        int i = this.h;
        if (i == -1) {
            i = 0;
            if ((this.f1520b & 1) == 1) {
                i = 0 + CodedOutputStream.computeMessageSize(1, this.f1521c);
            }
            if ((this.f1520b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            if ((this.f1520b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f1520b & 8) == 8) {
                i += CodedOutputStream.computeInt64Size(4, this.f);
            }
            this.h = i;
        }
        return i;
    }

    public static k a(InputStream inputStream) {
        return ((l) j().mergeFrom(inputStream)).j();
    }

    public static l j() {
        return l.i();
    }

    /* renamed from: k */
    public l newBuilderForType() {
        return j();
    }

    public static l a(k kVar) {
        return j().mergeFrom(kVar);
    }

    /* renamed from: l */
    public l toBuilder() {
        return a(this);
    }

    static {
        f1519a.m();
    }
}
