package com.avast.d.b;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: StreamBack */
public final class o extends GeneratedMessageLite.Builder<n, o> implements p {

    /* renamed from: a  reason: collision with root package name */
    private int f1528a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f1529b = ByteString.EMPTY;

    private o() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static o g() {
        return new o();
    }

    /* renamed from: a */
    public o clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public n getDefaultInstanceForType() {
        return n.a();
    }

    /* renamed from: c */
    public n build() {
        n d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public n d() {
        int i = 1;
        n nVar = new n(this);
        if ((this.f1528a & 1) != 1) {
            i = 0;
        }
        ByteString unused = nVar.f1527c = this.f1529b;
        int unused2 = nVar.f1526b = i;
        return nVar;
    }

    /* renamed from: a */
    public o mergeFrom(n nVar) {
        if (nVar != n.a() && nVar.b()) {
            a(nVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public o mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f1528a |= 1;
                    this.f1529b = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public o a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f1528a |= 1;
        this.f1529b = byteString;
        return this;
    }
}
