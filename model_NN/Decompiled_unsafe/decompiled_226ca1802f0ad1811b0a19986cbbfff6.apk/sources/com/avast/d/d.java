package com.avast.d;

import com.avast.a.a;
import com.avast.c;
import com.avast.d.a.b;
import com.avast.d.b.h;
import com.avast.d.b.i;
import com.avast.d.b.k;
import com.avast.d.b.n;
import com.avast.d.b.q;
import com.avast.d.b.s;
import com.avast.d.b.v;
import com.avast.d.b.w;
import com.avast.d.b.y;
import com.google.protobuf.ByteString;
import com.google.protobuf.MessageLite;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.net.ssl.HttpsURLConnection;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

/* compiled from: StreamBackClient */
public class d {

    /* renamed from: c  reason: collision with root package name */
    protected static final char[] f1554c = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a  reason: collision with root package name */
    protected final AtomicLong f1555a = new AtomicLong(new Random().nextLong());

    /* renamed from: b  reason: collision with root package name */
    protected final AtomicLong f1556b = new AtomicLong(System.currentTimeMillis());
    private b d;
    /* access modifiers changed from: private */
    public a e;

    public d a(a aVar, b bVar) {
        this.e = aVar;
        this.d = bVar;
        return this;
    }

    public y a(MessageLite messageLite, int i, int i2, b bVar) {
        c a2 = this.d.a();
        if ((a2 == null || a2.d()) && (a2 = a(a2)) != null) {
            this.d.a(a2);
        }
        if (a2 == null) {
            throw new NullPointerException("Loading key failed!");
        }
        y a3 = a(i, i2, a2, messageLite);
        if (bVar != null && a3.d() && a3.e().b() && s.SEND.equals(a3.e().c())) {
            a(a3, a3.e(), bVar, a2);
        }
        if (bVar != null && a3.d() && a3.e().b()) {
            bVar.a(a3.e().c());
        }
        return a3;
    }

    private void a(y yVar, q qVar, b bVar, c cVar) {
        Long l = null;
        if (!qVar.d()) {
            throw new IllegalArgumentException("We don't have ticket for sending data!");
        }
        ByteString e2 = qVar.e();
        Long valueOf = qVar.h() ? Long.valueOf(qVar.i()) : null;
        if (qVar.j()) {
            l = Long.valueOf(qVar.k());
        }
        String str = this.e.e;
        if (qVar.f()) {
            str = "http://" + (qVar.g().size() == 4 ? Inet4Address.getByAddress(qVar.g().toByteArray()) : Inet6Address.getByAddress(qVar.g().toByteArray())).getHostAddress();
            a("sending data to explicitly defined host: " + str);
        } else {
            a("sending data to default host: " + str);
        }
        long incrementAndGet = this.f1556b.incrementAndGet();
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        try {
            HttpPost httpPost = new HttpPost(str + "/" + "V1" + "/" + "PD" + "/" + a(cVar.a().toByteArray()) + "/" + incrementAndGet + "/" + a(e2.toByteArray()));
            e eVar = new e(this, a.a(cVar.b().toByteArray(), incrementAndGet), bVar.a(valueOf, l, yVar));
            eVar.setContentType("binary/octet-stream");
            eVar.setChunked(true);
            httpPost.setEntity(eVar);
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            if (execute.getStatusLine().getStatusCode() != 200) {
                throw new IOException("Invalid response code: " + execute.getStatusLine().getStatusCode());
            }
            HttpEntity entity = execute.getEntity();
            if (entity != null) {
                entity.consumeContent();
            }
        } finally {
            defaultHttpClient.getConnectionManager().shutdown();
        }
    }

    private String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b2 = bArr[i] & 255;
            cArr[i * 2] = f1554c[b2 >>> 4];
            cArr[(i * 2) + 1] = f1554c[b2 & 15];
        }
        return new String(cArr);
    }

    private y a(int i, int i2, c cVar, MessageLite messageLite) {
        long incrementAndGet = this.f1556b.incrementAndGet();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.e.e + "/" + "V1" + "/" + "MD" + "/" + a(cVar.a().toByteArray()) + "/" + incrementAndGet).openConnection();
        try {
            httpURLConnection.setConnectTimeout(this.e.g);
            httpURLConnection.setReadTimeout(this.e.f);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
            byte[] byteArray = messageLite == null ? new byte[0] : messageLite.toByteArray();
            byte[] a2 = a.a(cVar.b().toByteArray(), incrementAndGet);
            w a3 = v.n().a(this.e.i).a(n.d().a(c())).a(i2).b(i).a(System.currentTimeMillis()).a(ByteString.copyFrom(byteArray));
            if (a()) {
                a("Sending metadata " + a(a3.f().c().toByteArray()));
            }
            OutputStream a4 = a.a(bufferedOutputStream, a2);
            a3.build().writeTo(a4);
            a4.close();
            if (httpURLConnection.getResponseCode() != 200) {
                throw new IOException("Invalid response code: " + httpURLConnection.getResponseCode());
            }
            com.avast.b bVar = new com.avast.b(1024);
            OutputStream b2 = a.b(bVar, a2);
            c.a(httpURLConnection.getInputStream(), b2);
            b2.close();
            ByteString copyFrom = ByteString.copyFrom(bVar.b());
            if (b()) {
                b("plain response: " + a(copyFrom.toByteArray()));
            }
            y a5 = y.a(copyFrom);
            if (a()) {
                a("Retrieve response " + a(a5.c().c().toByteArray()) + ", " + ((!a5.d() || !a5.e().b()) ? "NO RESOLUTION" : a5.e().c()));
            }
            return a5;
        } finally {
            httpURLConnection.disconnect();
        }
    }

    /* access modifiers changed from: protected */
    public c a(c cVar) {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.e.d + "/" + "V1" + "/" + "REG").openConnection();
        try {
            httpURLConnection.setConnectTimeout(this.e.g);
            httpURLConnection.setReadTimeout(this.e.f);
            if (httpURLConnection instanceof HttpsURLConnection) {
                if (this.e.f1501b != null) {
                    ((HttpsURLConnection) httpURLConnection).setSSLSocketFactory(this.e.f1501b.getSocketFactory());
                }
                if (this.e.f1502c != null) {
                    ((HttpsURLConnection) httpURLConnection).setHostnameVerifier(this.e.f1502c);
                }
            } else if (!this.e.f1500a) {
                throw new IllegalArgumentException("Try for unsecured connection while registration.");
            }
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod("POST");
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(httpURLConnection.getOutputStream());
            i a2 = h.h().a(this.e.i).a(n.d().a(c()));
            if (cVar != null) {
                a2.a(cVar.a());
            }
            if (a()) {
                a("Sending register message " + a(a2.f().c().toByteArray()));
            }
            a2.build().writeTo(bufferedOutputStream);
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            if (httpURLConnection.getResponseCode() != 200) {
                throw new IOException("Invalid response code: " + httpURLConnection.getResponseCode());
            }
            k a3 = k.a(new BufferedInputStream(httpURLConnection.getInputStream()));
            if (a()) {
                a("Retrieve registration response " + a(a3.c().c().toByteArray()) + ", key id: " + a(a3.e().toByteArray()) + ", key ttl: " + (a3.i() - System.currentTimeMillis()));
            }
            return new c(a3.e(), a3.g(), a3.i());
        } finally {
            httpURLConnection.disconnect();
        }
    }

    private ByteString c() {
        long incrementAndGet = this.f1555a.incrementAndGet();
        ByteBuffer allocate = ByteBuffer.allocate(8);
        allocate.putLong(incrementAndGet);
        allocate.flip();
        return ByteString.copyFrom(allocate);
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
    }

    /* access modifiers changed from: protected */
    public boolean b() {
        return false;
    }
}
