package com.avast.android.antitheft_setup;

import a.a.a.a.a.e;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import com.avast.android.antitheft_setup.a.a;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.ae;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.s;
import com.avast.android.generic.util.t;
import java.io.File;

public class Application extends com.avast.android.generic.Application {

    /* renamed from: c  reason: collision with root package name */
    private ab f227c;

    public void onCreate() {
        boolean z;
        boolean z2 = false;
        super.onCreate();
        g();
        a.a((Context) this);
        try {
            z = new File(Environment.getExternalStorageDirectory(), "avast-debug").exists();
        } catch (Exception e) {
            z = false;
        }
        if (s.a(this) || z) {
            z2 = true;
        }
        t.a(z2);
        t.a("AvastBackup");
        a.a.a.a.a.a.a().b(new e(getApplicationContext()).c()).a("http://ta-e.avast.com/eh.srv").a(getApplicationContext());
        if (Build.VERSION.SDK_INT >= 9) {
        }
    }

    public synchronized Object getSystemService(String str) {
        Object obj;
        if (ab.class.toString().equals(str) || ad.class.toString().equals(str) || ae.class.toString().equals(str)) {
            if (this.f227c == null) {
                synchronized (ab.class) {
                    if (this.f227c == null) {
                        this.f227c = new a(getApplicationContext());
                    }
                }
            }
            obj = this.f227c;
        } else if (d.class.toString().equals(str) || a.class.toString().equals(str)) {
            obj = a.a((Context) this);
        } else {
            obj = super.getSystemService(str);
        }
        return obj;
    }
}
