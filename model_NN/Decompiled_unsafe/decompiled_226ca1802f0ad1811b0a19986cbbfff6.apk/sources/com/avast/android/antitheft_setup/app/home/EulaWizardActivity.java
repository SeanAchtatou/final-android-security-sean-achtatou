package com.avast.android.antitheft_setup.app.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class EulaWizardActivity extends com.avast.android.generic.app.wizard.EulaWizardActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        return new EulaFragment();
    }
}
