package com.avast.android.antitheft_setup.a;

import android.content.Context;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.j;

/* compiled from: Tracker */
public class a extends d {

    /* renamed from: a  reason: collision with root package name */
    private static a f228a;

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f228a == null) {
                f228a = new a((Application) context.getApplicationContext());
            }
            aVar = f228a;
        }
        return aVar;
    }

    private a(Application application) {
        super(application, j.AAT_SETUP, application.a());
    }
}
