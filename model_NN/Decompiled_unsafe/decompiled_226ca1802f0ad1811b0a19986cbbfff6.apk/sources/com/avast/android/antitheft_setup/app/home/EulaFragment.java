package com.avast.android.antitheft_setup.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.avast.android.antitheft_setup_components.app.home.RootWizardActivity;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.h;

public class EulaFragment extends com.avast.android.generic.app.wizard.EulaFragment {

    /* renamed from: a  reason: collision with root package name */
    private d f229a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f229a = d.b(getActivity());
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return super.onCreateView(layoutInflater, viewGroup, bundle);
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void a_() {
        if (isAdded()) {
            this.f229a.a(h.ADVANCED_INSTALL);
            a("ms-atSetup", "install mode", "advanced", 0);
            Application.b(false);
            getActivity().startActivity(new Intent(getActivity(), RootWizardActivity.class));
            getActivity().finish();
        }
    }

    /* access modifiers changed from: protected */
    public void b_() {
    }
}
