package com.avast.android.antitheft_setup_components.app.home;

import android.view.View;

/* compiled from: RootMethodFragment */
class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f258a;

    aa(RootMethodFragment rootMethodFragment) {
        this.f258a = rootMethodFragment;
    }

    public void onClick(View view) {
        if (this.f258a.d.c()) {
            this.f258a.a("ms-atSetup", "root method", "update-zip", 0);
        } else if (this.f258a.f252c.c()) {
            this.f258a.a("ms-atSetup", "root method", "direct write", 0);
        } else {
            this.f258a.a("ms-atSetup", "root method, error", "invalid checkbox state", 0);
        }
        this.f258a.c();
    }
}
