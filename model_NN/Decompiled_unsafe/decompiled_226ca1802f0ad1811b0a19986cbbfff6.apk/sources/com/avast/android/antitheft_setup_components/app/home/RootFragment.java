package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avast.android.antitheft_setup_components.d;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.e.c;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ga.TrackedFragment;

public class RootFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private Button f247a;

    /* renamed from: b  reason: collision with root package name */
    private Button f248b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public CheckBoxRow f249c;

    public int a() {
        return g.l_root;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_root, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f247a = (Button) inflate.findViewById(d.f316c);
        this.f248b = (Button) inflate.findViewById(d.f315b);
        Application.a(false);
        this.f249c = (CheckBoxRow) inflate.findViewById(d.cb_root);
        this.f249c.b(getString(g.l_root_option));
        this.f249c.c(getString(g.l_root_option_desc));
        this.f249c.a(false);
        this.f249c.a((c) null);
        this.f249c.a(new p(this));
        this.f247a.setOnClickListener(new q(this));
        this.f248b.setOnClickListener(new r(this));
        return inflate;
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i != 100) {
            a.a(this);
        }
    }

    public String b() {
        return "/ms/antiTheftSetup/root";
    }
}
