package com.avast.android.antitheft_setup_components.app.home;

import android.content.DialogInterface;

/* compiled from: RootMethodFragment */
class u implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f300a;

    u(RootMethodFragment rootMethodFragment) {
        this.f300a = rootMethodFragment;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f300a.a("ms-atSetup", "root method, update-zip, zip backup failed", "continue", 0);
        this.f300a.c();
    }
}
