package com.avast.android.antitheft_setup_components.app.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class RootWizardActivity extends BaseSetupActivity {

    /* renamed from: b  reason: collision with root package name */
    private RootFragment f254b;

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f254b = new RootFragment();
        return this.f254b;
    }
}
