package com.avast.android.antitheft_setup_components.app.home;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import com.avast.android.antitheft_setup_components.b;
import com.avast.android.antitheft_setup_components.c;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.a;

/* compiled from: DownloadFragment */
class i implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f284a;

    i(f fVar) {
        this.f284a = fVar;
    }

    public void run() {
        if (this.f284a.f277a.isAdded()) {
            if (!this.f284a.f278b) {
                this.f284a.f277a.a("ms-atSetup", "choose name", "continue", 0);
                this.f284a.f277a.d.setText("");
                this.f284a.f277a.e.setText(g.l_download_succeeded);
                this.f284a.f277a.e.setTextColor(this.f284a.f277a.getResources().getColor(b.f306a));
                if (Application.c()) {
                    this.f284a.f277a.f237a.setText(g.l_continue);
                    this.f284a.f277a.startActivity(new Intent(this.f284a.f277a.getActivity(), RootMethodWizardActivity.class));
                    a.a(this.f284a.f277a);
                    return;
                }
                this.f284a.f277a.f237a.setText(g.l_install);
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setDataAndType(Uri.parse("file://" + this.f284a.f277a.getActivity().getFilesDir() + "/" + "AvastAntiTheftInstaller.temp.apk"), "application/vnd.android.package-archive");
                intent.setComponent(new ComponentName("com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity"));
                this.f284a.f277a.startActivity(intent);
                this.f284a.f277a.d();
                return;
            }
            this.f284a.f277a.d.setText("");
            this.f284a.f277a.e.setText(this.f284a.f279c);
            this.f284a.f277a.e.setTextColor(this.f284a.f277a.getResources().getColor(b.f307b));
            this.f284a.f277a.f237a.setText(g.Q);
            this.f284a.f277a.f237a.setEnabled(true);
            this.f284a.f277a.f237a.setBackgroundResource(c.f314c);
            this.f284a.f277a.f238b.setEnabled(true);
            this.f284a.f277a.f237a.setOnClickListener(new j(this));
        }
    }
}
