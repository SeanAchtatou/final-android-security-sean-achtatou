package com.avast.android.antitheft_setup_components.app.home;

import android.content.Context;
import android.support.v4.app.Fragment;
import com.avast.android.antitheft_setup_components.app.home.b.b;
import com.avast.android.antitheft_setup_components.app.home.b.c;
import com.avast.android.generic.ui.d.a;
import com.avast.android.generic.ui.d.d;
import java.util.List;

/* compiled from: SetupCheckProblemChecker */
public class aj extends a {
    public aj(Context context, Fragment fragment) {
        super(context, fragment);
    }

    public void a(Context context, List<d> list, boolean z) {
        if (com.avast.android.antitheft_setup_components.app.home.b.a.a(context)) {
            list.add(new com.avast.android.antitheft_setup_components.app.home.b.a(context));
        }
        if (c.a(context)) {
            list.add(new c());
        }
        if (b.a(context)) {
            list.add(new b());
        }
    }
}
