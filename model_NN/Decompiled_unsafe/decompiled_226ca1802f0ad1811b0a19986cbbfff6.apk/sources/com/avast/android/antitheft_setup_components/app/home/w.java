package com.avast.android.antitheft_setup_components.app.home;

import android.content.DialogInterface;

/* compiled from: RootMethodFragment */
class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f302a;

    w(RootMethodFragment rootMethodFragment) {
        this.f302a = rootMethodFragment;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f302a.a("ms-atSetup", "root method, update-zip, direct write failed", "continue", 0);
        this.f302a.c();
    }
}
