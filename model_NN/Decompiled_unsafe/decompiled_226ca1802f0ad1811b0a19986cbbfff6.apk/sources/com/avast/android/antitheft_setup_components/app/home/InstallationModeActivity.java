package com.avast.android.antitheft_setup_components.app.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.actionbarsherlock.app.ActionBar;
import com.avast.android.generic.Application;
import com.avast.android.generic.ui.s;

public class InstallationModeActivity extends BaseSetupActivity implements s {

    /* renamed from: b  reason: collision with root package name */
    private InstallationModeFragment f243b;

    public static void call(Context context, l lVar) {
        if (context.getPackageName().equals("com.avast.android.antitheft_setup")) {
            Application.g();
        }
        Intent intent = new Intent(context, InstallationModeActivity.class);
        intent.putExtra("InstallationModeActivity.INSTALLATION_SOURCE", lVar.a());
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f243b = new InstallationModeFragment();
        return this.f243b;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        finish();
    }
}
