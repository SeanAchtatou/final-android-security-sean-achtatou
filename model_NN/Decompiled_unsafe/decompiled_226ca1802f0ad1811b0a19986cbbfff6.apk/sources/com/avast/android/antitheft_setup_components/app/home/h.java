package com.avast.android.antitheft_setup_components.app.home;

/* compiled from: DownloadFragment */
class h implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f281a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ int f282b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ f f283c;

    h(f fVar, int i, int i2) {
        this.f283c = fVar;
        this.f281a = i;
        this.f282b = i2;
    }

    public void run() {
        if (this.f283c.f277a.isAdded()) {
            int round = Math.round((((float) this.f281a) * 100.0f) / ((float) this.f282b));
            this.f283c.f277a.d.setText(round + "%");
            this.f283c.f277a.f239c.setProgress(round);
        }
    }
}
