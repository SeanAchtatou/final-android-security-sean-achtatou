package com.avast.android.antitheft_setup_components.app.home.b;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.a;
import com.avast.android.generic.ui.d.d;
import com.avast.android.generic.util.w;

/* compiled from: NoInternetAvailableProblem */
public class b extends d {
    public b() {
        super(g.M, g.l_error_in_internet_connectivity);
    }

    public boolean a(Context context, Fragment fragment) {
        try {
            fragment.startActivityForResult(new Intent("android.settings.WIRELESS_SETTINGS"), 100);
        } catch (Exception e) {
            try {
                fragment.startActivityForResult(new Intent("android.settings.WIFI_SETTINGS"), 100);
            } catch (Exception e2) {
                try {
                    fragment.startActivityForResult(new Intent("android.settings.SETTINGS"), 100);
                } catch (Exception e3) {
                    a.a(context, context.getString(g.B, w.a(context, e3)));
                }
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        return !com.avast.android.antitheft_setup_components.a.a.a(context);
    }

    public String a() {
        return "No internet available";
    }
}
