package com.avast.android.antitheft_setup_components.app.home;

import com.avast.android.generic.Application;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.ui.widget.c;

/* compiled from: RootMethodFragment */
class s implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f298a;

    s(RootMethodFragment rootMethodFragment) {
        this.f298a = rootMethodFragment;
    }

    public void a(CheckBoxRow checkBoxRow, boolean z) {
        if (z) {
            Application.c(true);
            this.f298a.d.a(false);
            return;
        }
        Application.c(false);
        this.f298a.d.a(true);
    }
}
