package com.avast.android.antitheft_setup_components.app.home;

import android.content.DialogInterface;

/* compiled from: RootMethodFragment */
class v implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f301a;

    v(RootMethodFragment rootMethodFragment) {
        this.f301a = rootMethodFragment;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f301a.a("ms-atSetup", "root method, update-zip, zip backup failed", "cancel", 0);
    }
}
