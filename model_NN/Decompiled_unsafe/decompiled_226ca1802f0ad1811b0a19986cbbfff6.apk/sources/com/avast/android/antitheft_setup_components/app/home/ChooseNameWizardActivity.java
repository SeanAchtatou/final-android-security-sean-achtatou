package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class ChooseNameWizardActivity extends BaseSetupActivity {

    /* renamed from: b  reason: collision with root package name */
    private ChooseNameFragment f236b;

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f236b = new ChooseNameFragment();
        return this.f236b;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        finish();
    }
}
