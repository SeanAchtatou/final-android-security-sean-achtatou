package com.avast.android.antitheft_setup_components.app.home;

/* compiled from: InstallationModeActivity */
public enum l {
    DASHBOARD(1),
    NOTIFICATION(2);
    

    /* renamed from: c  reason: collision with root package name */
    private int f289c;

    private l(int i) {
        this.f289c = i;
    }

    public int a() {
        return this.f289c;
    }

    public static l a(int i) {
        if (i == NOTIFICATION.a()) {
            return NOTIFICATION;
        }
        if (i == DASHBOARD.a()) {
            return DASHBOARD;
        }
        return null;
    }
}
