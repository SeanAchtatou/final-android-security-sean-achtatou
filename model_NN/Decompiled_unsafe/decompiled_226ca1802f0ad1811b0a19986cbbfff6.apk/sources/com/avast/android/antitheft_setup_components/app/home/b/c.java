package com.avast.android.antitheft_setup_components.app.home.b;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.a;
import com.avast.android.generic.ui.d.d;
import com.avast.android.generic.util.w;

/* compiled from: NonMarketSettingDisabledProblem */
public class c extends d {
    public c() {
        super(g.l_settings_problem, g.l_enable_non_market);
        a(com.avast.android.antitheft_setup_components.c.help_unknown_sources);
        a("http://www.avast.com/FAQ/AVKB66#idt_165");
    }

    public boolean a(Context context, Fragment fragment) {
        try {
            if (Build.VERSION.SDK_INT < 14) {
                fragment.startActivityForResult(new Intent("android.settings.APPLICATION_SETTINGS"), 100);
                return false;
            }
            throw new Exception();
        } catch (Exception e) {
            try {
                fragment.startActivityForResult(new Intent("android.settings.SECURITY_SETTINGS"), 100);
            } catch (Exception e2) {
                try {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.setAction("android.intent.action.MAIN");
                    fragment.startActivityForResult(intent, 100);
                } catch (Exception e3) {
                    a.a(context, context.getString(g.B, w.a(context, e3)));
                }
            }
        }
    }

    public static boolean a(Context context) {
        try {
            if (Settings.System.getInt(context.getContentResolver(), "install_non_market_apps") == 0) {
                return true;
            }
            return false;
        } catch (Settings.SettingNotFoundException e) {
            return false;
        }
    }

    public String a() {
        return "Non-market disabled";
    }
}
