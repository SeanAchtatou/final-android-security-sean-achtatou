package com.avast.android.antitheft_setup_components.app.home;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class DownloadWizardActivity extends BaseSetupActivity {

    /* renamed from: b  reason: collision with root package name */
    private DownloadFragment f240b;

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f240b = new DownloadFragment();
        return this.f240b;
    }
}
