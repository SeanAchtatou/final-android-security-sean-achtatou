package com.avast.android.antitheft_setup_components.app.home;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avast.android.antitheft_setup_components.d;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.a;
import com.avast.android.generic.e.c;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.ag;
import com.avast.android.generic.util.al;
import com.avast.android.generic.util.ga.TrackedFragment;
import com.avast.android.generic.util.u;
import java.io.File;

public class RootMethodFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private Button f250a;

    /* renamed from: b  reason: collision with root package name */
    private Button f251b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public CheckBoxRow f252c;
    /* access modifiers changed from: private */
    public CheckBoxRow d;
    /* access modifiers changed from: private */
    public CheckBoxRow e;
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public boolean g = false;

    public int a() {
        return g.l_root_method;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_root_method, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f250a = (Button) inflate.findViewById(d.b_back);
        this.f251b = (Button) inflate.findViewById(d.f316c);
        this.f252c = (CheckBoxRow) inflate.findViewById(d.cb_root_directWrite);
        this.f252c.b(getString(g.l_root_directWrite));
        this.f252c.c(getString(g.l_root_directWrite_desc));
        this.f252c.a((c) null);
        this.f252c.a(true);
        this.d = (CheckBoxRow) inflate.findViewById(d.cb_root_updateZip);
        this.d.b(getString(g.l_root_updateZip));
        this.d.c(getString(g.l_root_updateZip_desc));
        this.d.a((c) null);
        this.d.a(false);
        this.e = (CheckBoxRow) inflate.findViewById(d.cb_root_cyanogenMod);
        this.e.b(getString(g.l_root_cyanogenMod));
        this.e.c(getString(g.l_root_cyanogenMod_desc));
        this.e.a((c) null);
        this.f252c.a(true);
        this.d.a(false);
        this.e.a(false);
        this.e.setEnabled(ag.a(getActivity()));
        Application.c(true);
        this.f252c.a(new s(this));
        this.d.a(new y(this));
        this.f250a.setOnClickListener(new z(this));
        this.f251b.setOnClickListener(new aa(this));
        return inflate;
    }

    /* access modifiers changed from: private */
    public void c() {
        if (!Application.e()) {
            e();
        } else {
            d();
        }
    }

    private void d() {
        if (isAdded()) {
            new Thread(new ab(this, getActivity(), ProgressDialog.show(getActivity(), "", getString(g.l_installing), true))).start();
        }
    }

    private void e() {
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            a.a(activity, getString(g.l_select_update_zip_format), getString(g.l_edify), getString(g.l_amend), new ad(this, activity), new af(this));
        }
    }

    /* access modifiers changed from: private */
    public void a(al alVar, u uVar) {
        if (isAdded()) {
            u uVar2 = uVar;
            new Thread(new ag(this, uVar2, getActivity(), alVar, ProgressDialog.show(getActivity(), "", getString(g.l_installing), true))).start();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            if (aa.g(activity)) {
                a.a(activity, getString(g.l_rommanager_found), getString(g.am), getString(g.N), new ai(this, activity), new t(this));
                return;
            }
            a("ms-atSetup", "root method, update-zip, success", "finished", 0);
            g();
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        if (isAdded()) {
            String string = getString(g.l_updatezip_failed, getString(g.l_unknown_reason));
            if (str != null && !str.equals("")) {
                string = getString(g.l_updatezip_failed, str);
            }
            a.a(getActivity(), string, getString(g.am), getString(g.N), new u(this), new v(this));
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        if (isAdded()) {
            String string = getString(g.l_direct_failed, getString(g.l_unknown_reason));
            if (str != null && !str.equals("")) {
                string = getString(g.l_direct_failed, str);
            }
            a.a(getActivity(), string, getString(g.am), getString(g.N), new w(this), new x(this));
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (isAdded() && getActivity() != null) {
            File file = new File(getActivity().getFilesDir() + "/" + "AvastAntiTheftInstaller.temp.apk");
            if (file != null && file.exists()) {
                file.delete();
            }
            ((RootMethodWizardActivity) getActivity()).d();
            com.avast.android.generic.util.a.a(this);
            startActivity(new Intent(getActivity(), InstallationFinishedWizardActivity.class));
        }
    }

    public String b() {
        return "/ms/antiTheftSetup/rootMethod";
    }
}
