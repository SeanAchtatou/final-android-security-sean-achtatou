package com.avast.android.antitheft_setup_components.app.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avast.android.generic.ui.s;

public class SetupCheckWizardActivity extends BaseSetupActivity implements s {

    /* renamed from: b  reason: collision with root package name */
    private SetupCheckFragment f255b;

    public static void call(Context context) {
        if (new aj(context, null).a(context)) {
            context.startActivity(new Intent(context, SetupCheckWizardActivity.class));
        } else {
            SetupCheckFragment.a(context);
        }
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        g().a();
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f255b = new SetupCheckFragment();
        return this.f255b;
    }
}
