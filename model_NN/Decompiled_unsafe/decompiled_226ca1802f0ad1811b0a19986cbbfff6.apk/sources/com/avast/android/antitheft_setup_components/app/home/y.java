package com.avast.android.antitheft_setup_components.app.home;

import com.avast.android.generic.Application;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.ui.widget.c;

/* compiled from: RootMethodFragment */
class y implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f304a;

    y(RootMethodFragment rootMethodFragment) {
        this.f304a = rootMethodFragment;
    }

    public void a(CheckBoxRow checkBoxRow, boolean z) {
        if (z) {
            Application.c(false);
            this.f304a.f252c.a(false);
            return;
        }
        Application.c(true);
        this.f304a.f252c.a(true);
    }
}
