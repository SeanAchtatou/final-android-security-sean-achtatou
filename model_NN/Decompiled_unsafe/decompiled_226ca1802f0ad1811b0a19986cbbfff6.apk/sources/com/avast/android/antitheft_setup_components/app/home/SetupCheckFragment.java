package com.avast.android.antitheft_setup_components.app.home;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.ui.CheckerFragment;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ak;

public class SetupCheckFragment extends CheckerFragment {
    public void a() {
        a("root method, update-zip, success", "finished", 0);
        if (Application.h()) {
            ComponentName componentName = new ComponentName(getActivity().getPackageName(), getActivity().getPackageName() + ".app.home.EulaWizardActivity");
            Intent intent = new Intent();
            intent.setComponent(componentName);
            startActivityForResult(intent, 2);
            return;
        }
        if (Application.d()) {
            getActivity().startActivity(new Intent(getActivity(), DownloadWizardActivity.class));
        } else {
            getActivity().startActivity(new Intent(getActivity(), RootWizardActivity.class));
        }
        a.a(this);
    }

    public static void a(Context context) {
        if (Application.h()) {
            ComponentName componentName = new ComponentName(context.getPackageName(), context.getPackageName() + ".app.home.EulaWizardActivity");
            Intent intent = new Intent();
            intent.setComponent(componentName);
            context.startActivity(intent);
        } else if (Application.d()) {
            context.startActivity(new Intent(context, DownloadWizardActivity.class));
        } else {
            context.startActivity(new Intent(context, RootWizardActivity.class));
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (!ak.b(getActivity())) {
            b(view).setVisibility(8);
        }
    }

    public com.avast.android.generic.ui.d.a b() {
        return new aj(getActivity(), this);
    }

    public int c() {
        if (!Application.h()) {
            return g.l_back;
        }
        return g.g;
    }

    public int d() {
        return e();
    }

    public int e() {
        return g.l_welcome;
    }

    public int f() {
        return g.l_setup_check_description_list;
    }

    public final String g() {
        return "antiTheftSetup/problems";
    }

    public String h() {
        return "ms-atSetup";
    }
}
