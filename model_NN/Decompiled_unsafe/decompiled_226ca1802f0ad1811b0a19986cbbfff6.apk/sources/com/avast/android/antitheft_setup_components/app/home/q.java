package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.view.View;

/* compiled from: RootFragment */
class q implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootFragment f296a;

    q(RootFragment rootFragment) {
        this.f296a = rootFragment;
    }

    public void onClick(View view) {
        if (this.f296a.f249c.c()) {
            this.f296a.a("ms-atSetup", "root mode", "rooted", 0);
        } else {
            this.f296a.a("ms-atSetup", "root mode", "non-rooted", 0);
        }
        this.f296a.startActivity(new Intent(this.f296a.getActivity(), ChooseNameWizardActivity.class));
    }
}
