package com.avast.android.antitheft_setup_components;

/* compiled from: R */
public final class g {

    /* renamed from: A */
    public static final int l_enable_non_market = 2131624177;
    public static final int B = 2131624178;

    /* renamed from: C */
    public static final int l_error_in_download = 2131624183;

    /* renamed from: D */
    public static final int l_error_in_internet_connectivity = 2131624184;

    /* renamed from: E */
    public static final int l_finished = 2131624238;

    /* renamed from: F */
    public static final int l_http_dns_error = 2131624261;

    /* renamed from: G */
    public static final int l_http_status_error = 2131624262;

    /* renamed from: H */
    public static final int l_imei_of_phone_not_found = 2131624267;

    /* renamed from: I */
    public static final int l_install = 2131624268;

    /* renamed from: J */
    public static final int l_installation_already_present = 2131624270;

    /* renamed from: K */
    public static final int l_installation_problem = 2131624271;

    /* renamed from: L */
    public static final int l_installing = 2131624273;
    public static final int M = 2131624276;
    public static final int N = 2131624301;
    public static final int O = 2131624320;

    /* renamed from: P */
    public static final int l_package_name_too_short = 2131624323;
    public static final int Q = 2131624345;
    public static final int R = 2131624346;

    /* renamed from: S */
    public static final int l_rommanager_found = 2131624349;

    /* renamed from: T */
    public static final int l_root = 2131624350;

    /* renamed from: U */
    public static final int l_root_cyanogenMod = 2131624351;

    /* renamed from: V */
    public static final int l_root_cyanogenMod_desc = 2131624352;

    /* renamed from: W */
    public static final int l_root_directWrite = 2131624355;

    /* renamed from: X */
    public static final int l_root_directWrite_desc = 2131624356;

    /* renamed from: Y */
    public static final int l_root_method = 2131624357;

    /* renamed from: Z */
    public static final int l_root_option = 2131624359;

    /* renamed from: a */
    public static final int default_app_name = 2131624040;

    /* renamed from: aa */
    public static final int l_root_option_desc = 2131624360;

    /* renamed from: ab */
    public static final int l_root_updateZip = 2131624361;

    /* renamed from: ac */
    public static final int l_root_updateZip_desc = 2131624362;

    /* renamed from: ad */
    public static final int l_select_update_zip_format = 2131624366;

    /* renamed from: ae */
    public static final int l_select_update_zip_format_edify = 2131624367;

    /* renamed from: af */
    public static final int l_settings_problem = 2131624368;

    /* renamed from: ag */
    public static final int l_setup_check_description_list = 2131624371;

    /* renamed from: ah */
    public static final int l_unknown_reason = 2131624454;

    /* renamed from: ai */
    public static final int l_updatezip_failed = 2131624456;

    /* renamed from: aj */
    public static final int l_updatezip_succeeded = 2131624457;

    /* renamed from: ak */
    public static final int l_welcome = 2131624461;

    /* renamed from: al */
    public static final int l_wrong_package_name = 2131624463;
    public static final int am = 2131624464;

    /* renamed from: an */
    public static final int msg_not_enough_free_space_for_copy = 2131624529;

    /* renamed from: b */
    public static final int l_advanced_mode = 2131624055;

    /* renamed from: c */
    public static final int l_advanced_mode_description = 2131624056;

    /* renamed from: d */
    public static final int l_amend = 2131624057;

    /* renamed from: e */
    public static final int l_appname = 2131624061;

    /* renamed from: f */
    public static final int l_back = 2131624086;
    public static final int g = 2131624087;

    /* renamed from: h */
    public static final int l_choose_appname = 2131624099;

    /* renamed from: i */
    public static final int l_connecting = 2131624139;

    /* renamed from: j */
    public static final int l_connection_error = 2131624140;

    /* renamed from: k */
    public static final int l_continue = 2131624145;

    /* renamed from: l */
    public static final int l_direct_failed = 2131624153;

    /* renamed from: m */
    public static final int l_direct_succeeded = 2131624154;

    /* renamed from: n */
    public static final int l_download_in_progress = 2131624157;

    /* renamed from: o */
    public static final int l_download_succeeded = 2131624158;

    /* renamed from: p */
    public static final int l_downloading = 2131624159;

    /* renamed from: q */
    public static final int l_easy_mode = 2131624161;

    /* renamed from: r */
    public static final int l_easy_mode_description = 2131624162;

    /* renamed from: s */
    public static final int l_easy_mode_description_play = 2131624163;

    /* renamed from: t */
    public static final int l_easy_mode_play = 2131624164;

    /* renamed from: u */
    public static final int l_edify = 2131624165;

    /* renamed from: v */
    public static final int l_edify_jb = 2131624166;

    /* renamed from: w */
    public static final int l_edify_new = 2131624167;

    /* renamed from: x */
    public static final int l_edify_old = 2131624168;

    /* renamed from: y */
    public static final int l_edify_tell_me_more = 2131624169;

    /* renamed from: z */
    public static final int l_edify_tell_me_more_desc = 2131624170;
}
