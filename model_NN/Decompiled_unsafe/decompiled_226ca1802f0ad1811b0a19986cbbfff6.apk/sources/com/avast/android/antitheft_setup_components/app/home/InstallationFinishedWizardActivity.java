package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class InstallationFinishedWizardActivity extends BaseSetupActivity {

    /* renamed from: b  reason: collision with root package name */
    private InstallationFinishedFragment f242b;

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        this.f242b = new InstallationFinishedFragment();
        return this.f242b;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        finish();
    }
}
