package com.avast.android.antitheft_setup_components.app.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import com.avast.android.antitheft_setup_components.b.a;
import com.avast.android.antitheft_setup_components.f;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.util.al;
import com.avast.android.generic.util.u;
import com.avast.android.generic.util.w;

/* compiled from: RootMethodFragment */
class ag implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ u f267a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f268b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ al f269c;
    final /* synthetic */ ProgressDialog d;
    final /* synthetic */ RootMethodFragment e;

    ag(RootMethodFragment rootMethodFragment, u uVar, Context context, al alVar, ProgressDialog progressDialog) {
        this.e = rootMethodFragment;
        this.f267a = uVar;
        this.f268b = context;
        this.f269c = alVar;
        this.d = progressDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, boolean):boolean
     arg types: [com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, int]
     candidates:
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, java.lang.String):java.lang.String
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.generic.util.al, com.avast.android.generic.util.u):void
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, boolean):boolean */
    public void run() {
        if (this.e.isAdded()) {
            boolean unused = this.e.g = false;
            String unused2 = this.e.f = "";
            try {
                com.avast.android.generic.util.ag.a(this.f268b, "AvastAntiTheftInstaller.temp.apk", Build.VERSION.SDK_INT < 19 ? "/system/app/com.avast.android.antitheft.apk" : "/system/priv-app/com.avast.android.antitheft.apk", true, f.signpb, f.signpr, false, false, a.a(this.f267a), this.f267a, this.f269c, this.e.e.c());
                boolean unused3 = this.e.g = true;
                this.e.a("ms-atSetup", "root method, update-zip", "success", 0);
            } catch (com.avast.android.generic.util.a.a e2) {
                com.avast.android.generic.util.a.a aVar = e2;
                if (this.e.isAdded()) {
                    this.e.a("ms-atSetup", "root method, update-zip, error", "not enough space", 0);
                    String unused4 = this.e.f = this.e.getString(g.msg_not_enough_free_space_for_copy, aVar.a(this.f268b), aVar.b(this.f268b));
                    boolean unused5 = this.e.g = false;
                } else {
                    return;
                }
            } catch (Exception e3) {
                Exception exc = e3;
                this.e.a("ms-atSetup", "root method, update-zip, error", exc.getMessage(), 0);
                exc.printStackTrace();
                String unused6 = this.e.f = w.a(this.e, exc);
                boolean unused7 = this.e.g = false;
            }
            com.avast.android.generic.util.a.a(this.e, new ah(this));
        }
    }
}
