package com.avast.android.antitheft_setup_components.app.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.avast.android.generic.ui.BaseSinglePaneActivity;
import com.avast.android.generic.util.ae;

public abstract class BaseSetupActivity extends BaseSinglePaneActivity {

    /* renamed from: a  reason: collision with root package name */
    public static final IntentFilter f230a = h();

    /* renamed from: b  reason: collision with root package name */
    private BaseActivityReceiver f231b = new BaseActivityReceiver();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b();
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setHomeButtonEnabled(false);
            supportActionBar.setDisplayUseLogoEnabled(false);
            supportActionBar.setDisplayShowTitleEnabled(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c_();
        super.onDestroy();
    }

    private static IntentFilter h() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.avast.android.antitheft_setup.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION");
        return intentFilter;
    }

    /* access modifiers changed from: protected */
    public void b() {
        registerReceiver(this.f231b, f230a);
    }

    /* access modifiers changed from: protected */
    public void c_() {
        try {
            unregisterReceiver(this.f231b);
        } catch (Exception e) {
        }
    }

    public class BaseActivityReceiver extends BroadcastReceiver {
        public BaseActivityReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("com.avast.android.antitheft_setup.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION")) {
                BaseSetupActivity.this.finish();
            }
        }
    }

    public void d() {
        Intent intent = new Intent("com.avast.android.antitheft_setup.FINISH_ALL_ACTIVITIES_ACTIVITY_ACTION");
        ae.a(intent);
        sendBroadcast(intent);
    }
}
