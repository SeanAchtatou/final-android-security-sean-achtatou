package com.avast.android.antitheft_setup_components.app.home;

import android.content.DialogInterface;
import com.avast.android.generic.util.al;
import com.avast.android.generic.util.u;

/* compiled from: RootMethodFragment */
class af implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f266a;

    af(RootMethodFragment rootMethodFragment) {
        this.f266a = rootMethodFragment;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f266a.a("ms-atSetup", "root method, update-zip", "amend", 0);
        this.f266a.a(al.AMEND, u.OLD);
    }
}
