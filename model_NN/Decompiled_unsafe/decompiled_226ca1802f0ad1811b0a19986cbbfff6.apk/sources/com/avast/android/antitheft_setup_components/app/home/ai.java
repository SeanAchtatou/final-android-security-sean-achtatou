package com.avast.android.antitheft_setup_components.app.home;

import android.content.Context;
import android.content.DialogInterface;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.aa;

/* compiled from: RootMethodFragment */
class ai implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f271a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f272b;

    ai(RootMethodFragment rootMethodFragment, Context context) {
        this.f272b = rootMethodFragment;
        this.f271a = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f272b.a("ms-atSetup", "root method, update-zip, success", "ROM manager", 0);
        aa.h(this.f271a);
        ((RootMethodWizardActivity) this.f271a).d();
        a.a(this.f272b);
    }
}
