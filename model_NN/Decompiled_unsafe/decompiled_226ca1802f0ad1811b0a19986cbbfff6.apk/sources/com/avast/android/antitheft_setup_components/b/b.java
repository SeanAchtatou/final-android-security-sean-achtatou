package com.avast.android.antitheft_setup_components.b;

import android.content.Context;
import android.content.DialogInterface;
import com.avast.android.generic.util.u;

/* compiled from: EdifyBinaryHelper */
final class b implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ e f308a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f309b;

    b(e eVar, Context context) {
        this.f308a = eVar;
        this.f309b = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == 0) {
            this.f308a.a(u.JB);
        } else if (i == 1) {
            this.f308a.a(u.ICS);
        } else if (i == 2) {
            this.f308a.a(u.OLD);
        } else {
            a.c(this.f309b, this.f308a);
        }
    }
}
