package com.avast.android.antitheft_setup_components.b;

import android.content.Context;
import com.avast.android.antitheft_setup_components.f;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.util.u;

/* compiled from: EdifyBinaryHelper */
public class a {
    public static int a(u uVar) {
        if (uVar == null) {
            return f.update_binary_jb;
        }
        switch (d.f312a[uVar.ordinal()]) {
            case 1:
                return f.update_binary_ics;
            case 2:
                return f.update_binary_jb;
            case 3:
                return f.update_binary;
            default:
                return f.update_binary_jb;
        }
    }

    public static void a(Context context, e eVar) {
        com.avast.android.generic.a.a(context, context.getString(g.l_select_update_zip_format_edify), new String[]{context.getString(g.l_edify_jb), context.getString(g.l_edify_new), context.getString(g.l_edify_old), context.getString(g.l_edify_tell_me_more)}, new b(eVar, context));
    }

    /* access modifiers changed from: private */
    public static void c(Context context, e eVar) {
        com.avast.android.generic.a.a(context, context.getString(g.l_edify_tell_me_more_desc), new c(context, eVar));
    }
}
