package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.avast.android.antitheft_setup_components.app.home.a.a;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.ui.widget.NextRow;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.ga.TrackedFragment;
import java.util.List;

public class InstallationModeFragment extends TrackedFragment {
    private static final Uri d = Uri.parse("market://details?id=com.avast.android.at_play&referrer=utm_source%3DAMS%26utm_medium%3Ddash%26utm_content%3Danti-theft%26utm_campaign%3DAT");
    private static final Uri e = Uri.parse("market://details?id=com.avast.android.at_play&referrer=utm_source%3DAMS%26utm_medium%3Dnotification%26utm_content%3Danti-theft%26utm_campaign%3DAT");

    /* renamed from: a  reason: collision with root package name */
    private NextRow f244a;

    /* renamed from: b  reason: collision with root package name */
    private NextRow f245b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public d f246c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f246c = d.b(getActivity());
    }

    public int a() {
        return g.l_root;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_installationmode, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f245b = (NextRow) inflate.findViewById(com.avast.android.antitheft_setup_components.d.nr_advanced);
        this.f245b.b(getString(g.l_advanced_mode));
        this.f245b.c(getString(g.l_advanced_mode_description));
        this.f245b.setOnClickListener(new m(this));
        c(inflate);
        return inflate;
    }

    public String b() {
        return "/ms/antiTheftSetup/installationMode";
    }

    private void c(View view) {
        String a2 = a.a();
        this.f244a = (NextRow) view.findViewById(com.avast.android.antitheft_setup_components.d.nr_easy);
        Intent a3 = a(c() == l.NOTIFICATION ? e : d);
        if (a(a3)) {
            this.f244a.b(getString(g.l_easy_mode_play));
            this.f244a.a(Html.fromHtml(getString(g.l_easy_mode_description_play, "avast! Anti-Theft")));
            this.f244a.setOnClickListener(new n(this, a3));
            return;
        }
        this.f244a.b(getString(g.l_easy_mode));
        this.f244a.a(Html.fromHtml(getString(g.l_easy_mode_description, a2)));
        this.f244a.setOnClickListener(new o(this, a2));
    }

    private l c() {
        l a2 = l.a(getArguments().getInt("InstallationModeActivity.INSTALLATION_SOURCE"));
        if (a2 == null) {
            return l.DASHBOARD;
        }
        return a2;
    }

    private Intent a(Uri uri) {
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        intent.addFlags(524288);
        return intent;
    }

    private boolean a(Intent intent) {
        if (!isAdded()) {
            return false;
        }
        Log.d("breadcrumbs", "Checking intent availability for uri: " + intent.getDataString());
        List<ResolveInfo> queryIntentActivities = getActivity().getPackageManager().queryIntentActivities(intent, 0);
        if (queryIntentActivities == null || queryIntentActivities.size() <= 0) {
            return false;
        }
        return true;
    }

    public void onResume() {
        if (isAdded() && aa.b(getActivity()) != null) {
            i();
        }
        super.onResume();
    }
}
