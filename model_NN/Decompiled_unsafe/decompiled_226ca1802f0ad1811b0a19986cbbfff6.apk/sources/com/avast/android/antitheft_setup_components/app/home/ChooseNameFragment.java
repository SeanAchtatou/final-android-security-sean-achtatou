package com.avast.android.antitheft_setup_components.app.home;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avast.android.antitheft_setup_components.d;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.e.c;
import com.avast.android.generic.ui.widget.EditTextRow;
import com.avast.android.generic.util.ga.TrackedFragment;
import java.util.regex.Pattern;

public class ChooseNameFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private Button f233a;

    /* renamed from: b  reason: collision with root package name */
    private Button f234b;

    /* renamed from: c  reason: collision with root package name */
    private Button f235c;
    /* access modifiers changed from: private */
    public EditTextRow d;
    /* access modifiers changed from: private */
    public Pattern e = Pattern.compile("[[a-z][A-Z][0-9][\\!\\-][ ]]*");
    /* access modifiers changed from: private */
    public Activity f;

    public int a() {
        return g.l_appname;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_choosename, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f233a = (Button) inflate.findViewById(d.f316c);
        this.f234b = (Button) inflate.findViewById(d.f315b);
        this.f235c = (Button) inflate.findViewById(d.b_generate_name);
        this.d = (EditTextRow) inflate.findViewById(d.r_choose_appname);
        this.d.b(getString(g.l_choose_appname));
        this.d.a((c) null);
        this.d.a(getString(g.default_app_name));
        this.f = getActivity();
        this.f235c.setOnClickListener(new a(this));
        this.f233a.setOnClickListener(new b(this));
        this.f234b.setOnClickListener(new c(this));
        return inflate;
    }

    public String b() {
        return "/ms/antiTheftSetup/chooseName";
    }
}
