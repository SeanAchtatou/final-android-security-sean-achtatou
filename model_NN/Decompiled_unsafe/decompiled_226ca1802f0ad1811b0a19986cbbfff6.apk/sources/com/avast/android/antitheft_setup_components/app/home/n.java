package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.view.View;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.h;

/* compiled from: InstallationModeFragment */
class n implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Intent f291a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ InstallationModeFragment f292b;

    n(InstallationModeFragment installationModeFragment, Intent intent) {
        this.f292b = installationModeFragment;
        this.f291a = intent;
    }

    public void onClick(View view) {
        this.f292b.f246c.a(h.EASY_INSTALL);
        this.f292b.a("ms-atSetup", "install mode", "easyPlay", 0);
        this.f292b.getActivity().startActivity(this.f291a);
        a.a(this.f292b);
    }
}
