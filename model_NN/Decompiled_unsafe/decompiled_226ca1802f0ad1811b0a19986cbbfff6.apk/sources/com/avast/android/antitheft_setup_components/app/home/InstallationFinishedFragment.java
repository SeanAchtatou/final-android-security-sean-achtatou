package com.avast.android.antitheft_setup_components.app.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.avast.android.antitheft_setup_components.d;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ga.TrackedFragment;

public class InstallationFinishedFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private Button f241a;

    public int a() {
        return g.l_finished;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_installationfinished, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f241a = (Button) inflate.findViewById(d.f316c);
        TextView textView = (TextView) inflate.findViewById(d.l_installationfinished);
        if (!Application.c()) {
            a.a(this);
        } else if (Application.e()) {
            textView.setText(getString(g.l_direct_succeeded));
            this.f241a.setText(getString(g.O));
        } else {
            textView.setText(getString(g.l_updatezip_succeeded));
            this.f241a.setText(getString(g.O));
        }
        this.f241a.setOnClickListener(new k(this));
        return inflate;
    }

    /* access modifiers changed from: private */
    public void c() {
        ((InstallationFinishedWizardActivity) getActivity()).d();
        a.a(this);
    }

    public String b() {
        return "/ms/antiTheftSetup/finished";
    }
}
