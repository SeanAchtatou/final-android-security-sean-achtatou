package com.avast.android.antitheft_setup_components.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.internet.b.c;
import com.avast.android.generic.util.af;
import com.avast.android.generic.util.v;
import com.avast.android.generic.util.z;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;

/* compiled from: SetupHttp */
public class a {
    public static HttpURLConnection a(Context context, String str) {
        HttpURLConnection a2 = b.a(new URL(str));
        try {
            a2.setAllowUserInteraction(false);
            a2.setInstanceFollowRedirects(true);
            a2.setRequestMethod("GET");
            a2.setConnectTimeout(50000);
            a2.setReadTimeout(150000);
            a2.connect();
            int responseCode = a2.getResponseCode();
            if (responseCode == 200) {
                return a2;
            }
            String responseMessage = a2.getResponseMessage();
            if (responseMessage == null) {
                throw new c(responseCode);
            }
            throw new com.avast.android.generic.internet.b.b(responseMessage);
        } catch (Exception e) {
            throw new com.avast.android.generic.internet.b.b(e.getMessage());
        } catch (Exception e2) {
            af.a(a2);
            throw e2;
        }
    }

    private static String a(Context context, String str, String str2) {
        int e = com.avast.android.generic.g.b.a.e(context);
        String str3 = (str2 + "/") + str + "?" + "os=android&f=1&avast=1&product=ta&mode=mobile";
        if (e > 0) {
            str3 = str3 + "&cc=" + String.valueOf(e);
        }
        String str4 = (str3 + "&l=" + Locale.getDefault().getLanguage()) + "&i=" + com.avast.android.generic.g.b.a.i(context);
        String a2 = com.avast.android.generic.g.b.a.a(context);
        if (a2 != null && !a2.equals("")) {
            try {
                str4 = str4 + "&ok=" + v.a(a2, true, true);
            } catch (Exception e2) {
                z.a("AvastAntiTheft", "Can not encode operator key", e2);
            }
        }
        return str4 + "&ab=" + String.valueOf(Build.VERSION.SDK_INT);
    }

    private static String b(Context context, String str) {
        return a(context, str, "https://ta-cdn.avast.com");
    }

    private static String a(String str, String str2, String str3) {
        return str + "&" + str2 + "=" + str3;
    }

    public static String a(Context context, String str, boolean z) {
        return a(a(b(context, "provide.php"), "t", v.a(str, true, true)), "btn", z ? "1" : "0");
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }
}
