package com.avast.android.antitheft_setup_components.app.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import com.avast.android.antitheft_setup_components.f;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.util.a.a;
import com.avast.android.generic.util.ag;
import com.avast.android.generic.util.w;
import java.io.File;

/* compiled from: RootMethodFragment */
class ab implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f259a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ProgressDialog f260b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f261c;

    ab(RootMethodFragment rootMethodFragment, Context context, ProgressDialog progressDialog) {
        this.f261c = rootMethodFragment;
        this.f259a = context;
        this.f260b = progressDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, boolean):boolean
     arg types: [com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, int]
     candidates:
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, java.lang.String):java.lang.String
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.generic.util.al, com.avast.android.generic.util.u):void
      com.avast.android.antitheft_setup_components.app.home.RootMethodFragment.a(com.avast.android.antitheft_setup_components.app.home.RootMethodFragment, boolean):boolean */
    public void run() {
        if (this.f261c.isAdded()) {
            boolean unused = this.f261c.g = false;
            String unused2 = this.f261c.f = "";
            try {
                ag.a(this.f259a, "AvastAntiTheftInstaller.temp.apk", Build.VERSION.SDK_INT < 19 ? "/system/app/com.avast.android.antitheft.apk" : "/system/priv-app/com.avast.android.antitheft.apk", false, f.signpb, f.signpr, false, false, 0, null, null, this.f261c.e.c());
                if (new File(Build.VERSION.SDK_INT < 19 ? "/system/app/com.avast.android.antitheft.apk" : "/system/priv-app/com.avast.android.antitheft.apk").exists()) {
                    boolean unused3 = this.f261c.g = true;
                }
            } catch (a e) {
                a aVar = e;
                if (this.f261c.isAdded()) {
                    this.f261c.a("ms-atSetup", "root method, direct write, error", "not enough space", 0);
                    String unused4 = this.f261c.f = this.f261c.getString(g.msg_not_enough_free_space_for_copy, aVar.a(this.f259a), aVar.b(this.f259a));
                    boolean unused5 = this.f261c.g = false;
                } else {
                    return;
                }
            } catch (Exception e2) {
                Exception exc = e2;
                this.f261c.a("ms-atSetup", "root method, direct write, error", exc.getMessage(), 0);
                exc.printStackTrace();
                String unused6 = this.f261c.f = w.a(this.f261c, exc);
                boolean unused7 = this.f261c.g = false;
            }
            com.avast.android.generic.util.a.a(this.f261c, new ac(this));
        }
    }
}
