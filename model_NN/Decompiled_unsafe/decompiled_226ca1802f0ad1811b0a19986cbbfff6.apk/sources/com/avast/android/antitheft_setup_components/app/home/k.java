package com.avast.android.antitheft_setup_components.app.home;

import android.view.View;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.a;

/* compiled from: InstallationFinishedFragment */
class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallationFinishedFragment f286a;

    k(InstallationFinishedFragment installationFinishedFragment) {
        this.f286a = installationFinishedFragment;
    }

    public void onClick(View view) {
        if (Application.c()) {
            this.f286a.c();
            return;
        }
        ((InstallationFinishedWizardActivity) this.f286a.getActivity()).d();
        a.a(this.f286a);
    }
}
