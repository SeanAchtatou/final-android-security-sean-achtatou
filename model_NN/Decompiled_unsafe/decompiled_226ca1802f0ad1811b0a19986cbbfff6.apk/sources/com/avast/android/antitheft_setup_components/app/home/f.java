package com.avast.android.antitheft_setup_components.app.home;

import android.annotation.SuppressLint;
import android.content.Context;
import com.avast.android.antitheft_setup_components.c;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.internet.b.a;
import com.avast.android.generic.internet.b.b;
import com.avast.android.generic.util.af;
import com.avast.android.generic.util.am;
import com.avast.android.generic.util.w;
import com.avast.android.generic.util.z;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

/* compiled from: DownloadFragment */
class f extends am<Void, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DownloadFragment f277a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f278b = false;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public String f279c = "";

    f(DownloadFragment downloadFragment) {
        this.f277a = downloadFragment;
    }

    public void a() {
        if (this.f277a.isAdded()) {
            this.f277a.d.setText("0%");
            this.f277a.e.setText(this.f277a.getString(g.l_connecting));
            this.f277a.e.setTextColor(-1);
            this.f277a.f239c.setProgress(0);
            this.f277a.f237a.setBackgroundResource(c.f313b);
            this.f277a.f237a.setEnabled(false);
            this.f277a.f238b.setEnabled(false);
            this.f278b = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.antitheft_setup_components.a.a.a(android.content.Context, java.lang.String, boolean):java.lang.String
     arg types: [android.support.v4.app.FragmentActivity, java.lang.String, int]
     candidates:
      com.avast.android.antitheft_setup_components.a.a.a(android.content.Context, java.lang.String, java.lang.String):java.lang.String
      com.avast.android.antitheft_setup_components.a.a.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
      com.avast.android.antitheft_setup_components.a.a.a(android.content.Context, java.lang.String, boolean):java.lang.String */
    @SuppressLint({"WorldReadableFiles"})
    public Void a(Void... voidArr) {
        FileOutputStream fileOutputStream;
        InputStream inputStream;
        HttpURLConnection httpURLConnection;
        a aVar;
        FileOutputStream fileOutputStream2;
        InputStream inputStream2;
        HttpURLConnection httpURLConnection2;
        Exception exc;
        com.avast.android.generic.internet.b.c cVar;
        BufferedReader bufferedReader;
        b bVar;
        int i;
        if (!this.f277a.isAdded()) {
            return null;
        }
        try {
            Thread.sleep(1000);
            if (!this.f277a.isAdded() || this.f277a.getActivity() == null) {
                return null;
            }
            String i2 = com.avast.android.generic.g.b.a.i(this.f277a.getActivity());
            if (i2 == null || i2.equals("")) {
                this.f277a.a("ms-atSetup", "download, error", "IMEI not found", 0);
                throw new Exception(this.f277a.getString(g.l_imei_of_phone_not_found));
            }
            BufferedReader bufferedReader2 = null;
            try {
                httpURLConnection = com.avast.android.antitheft_setup_components.a.a.a(this.f277a.getActivity(), com.avast.android.antitheft_setup_components.a.a.a((Context) this.f277a.getActivity(), Application.f(), false));
                try {
                    inputStream = httpURLConnection.getInputStream();
                    try {
                        if (httpURLConnection.getContentType().equals("application/vnd.android.package-archive")) {
                            com.avast.android.generic.util.a.a(this.f277a, new g(this));
                            if (httpURLConnection.getContentLength() > 0) {
                                i = httpURLConnection.getContentLength();
                            } else {
                                i = 2048000;
                            }
                            fileOutputStream = this.f277a.getActivity().openFileOutput("AvastAntiTheftInstaller.temp.apk", 1);
                            try {
                                byte[] bArr = new byte[1024];
                                int i3 = 0;
                                int i4 = 0;
                                while (true) {
                                    int read = inputStream.read(bArr);
                                    if (read <= 0) {
                                        break;
                                    }
                                    fileOutputStream.write(bArr, 0, read);
                                    i3 += read;
                                    i4++;
                                    if (i4 > 5) {
                                        i4 = 0;
                                        com.avast.android.generic.util.a.a(this.f277a, new h(this, i3, i));
                                    }
                                }
                                fileOutputStream.flush();
                                this.f277a.a("ms-atSetup", "download", "success", 0);
                                af.a(null);
                                af.a(inputStream);
                                af.a(fileOutputStream);
                                af.a(httpURLConnection);
                                return null;
                            } catch (a e) {
                                aVar = e;
                                try {
                                    this.f277a.a("ms-atSetup", "download, error", aVar.getMessage(), 0);
                                    throw new Exception(this.f277a.getString(g.l_http_dns_error));
                                } catch (Throwable th) {
                                    th = th;
                                    httpURLConnection2 = httpURLConnection;
                                    inputStream2 = inputStream;
                                    fileOutputStream2 = fileOutputStream;
                                    af.a(bufferedReader2);
                                    af.a(inputStream2);
                                    af.a(fileOutputStream2);
                                    af.a(httpURLConnection2);
                                    throw th;
                                }
                            } catch (b e2) {
                                httpURLConnection2 = httpURLConnection;
                                inputStream2 = inputStream;
                                fileOutputStream2 = fileOutputStream;
                                bufferedReader = null;
                                bVar = e2;
                                this.f277a.a("ms-atSetup", "download, error", bVar.getMessage(), 0);
                                throw new Exception(this.f277a.getString(g.l_connection_error, w.a(this.f277a, bVar)));
                            } catch (com.avast.android.generic.internet.b.c e3) {
                                cVar = e3;
                                this.f277a.a("ms-atSetup", "download, error", cVar.getMessage(), 0);
                                throw new Exception(this.f277a.getString(g.l_http_status_error, w.a(this.f277a, cVar)));
                            } catch (Exception e4) {
                                exc = e4;
                                this.f277a.a("ms-atSetup", "download, error", exc.getMessage(), 0);
                                z.a("AvastGeneric", "Exception in internet connectivity", exc);
                                throw new Exception(this.f277a.getString(g.l_error_in_download));
                            }
                        } else {
                            BufferedReader bufferedReader3 = new BufferedReader(new InputStreamReader(inputStream));
                            try {
                                throw new Exception(this.f277a.getString(g.l_connection_error, bufferedReader3.readLine()));
                            } catch (a e5) {
                                aVar = e5;
                                bufferedReader2 = bufferedReader3;
                                fileOutputStream = null;
                                this.f277a.a("ms-atSetup", "download, error", aVar.getMessage(), 0);
                                throw new Exception(this.f277a.getString(g.l_http_dns_error));
                            } catch (b e6) {
                                bVar = e6;
                                bufferedReader = bufferedReader3;
                                httpURLConnection2 = httpURLConnection;
                                inputStream2 = inputStream;
                                fileOutputStream2 = null;
                                this.f277a.a("ms-atSetup", "download, error", bVar.getMessage(), 0);
                                throw new Exception(this.f277a.getString(g.l_connection_error, w.a(this.f277a, bVar)));
                            } catch (com.avast.android.generic.internet.b.c e7) {
                                cVar = e7;
                                this.f277a.a("ms-atSetup", "download, error", cVar.getMessage(), 0);
                                throw new Exception(this.f277a.getString(g.l_http_status_error, w.a(this.f277a, cVar)));
                            } catch (Exception e8) {
                                exc = e8;
                                this.f277a.a("ms-atSetup", "download, error", exc.getMessage(), 0);
                                z.a("AvastGeneric", "Exception in internet connectivity", exc);
                                throw new Exception(this.f277a.getString(g.l_error_in_download));
                            } catch (Throwable th2) {
                                th = th2;
                                bufferedReader2 = bufferedReader3;
                                httpURLConnection2 = httpURLConnection;
                                inputStream2 = inputStream;
                                fileOutputStream2 = null;
                                af.a(bufferedReader2);
                                af.a(inputStream2);
                                af.a(fileOutputStream2);
                                af.a(httpURLConnection2);
                                throw th;
                            }
                        }
                    } catch (a e9) {
                        aVar = e9;
                        fileOutputStream = null;
                        this.f277a.a("ms-atSetup", "download, error", aVar.getMessage(), 0);
                        throw new Exception(this.f277a.getString(g.l_http_dns_error));
                    } catch (b e10) {
                        bufferedReader = null;
                        httpURLConnection2 = httpURLConnection;
                        inputStream2 = inputStream;
                        bVar = e10;
                        fileOutputStream2 = null;
                        this.f277a.a("ms-atSetup", "download, error", bVar.getMessage(), 0);
                        throw new Exception(this.f277a.getString(g.l_connection_error, w.a(this.f277a, bVar)));
                    } catch (com.avast.android.generic.internet.b.c e11) {
                        cVar = e11;
                        this.f277a.a("ms-atSetup", "download, error", cVar.getMessage(), 0);
                        throw new Exception(this.f277a.getString(g.l_http_status_error, w.a(this.f277a, cVar)));
                    } catch (Exception e12) {
                        exc = e12;
                        this.f277a.a("ms-atSetup", "download, error", exc.getMessage(), 0);
                        z.a("AvastGeneric", "Exception in internet connectivity", exc);
                        throw new Exception(this.f277a.getString(g.l_error_in_download));
                    } catch (Throwable th3) {
                        th = th3;
                        httpURLConnection2 = httpURLConnection;
                        inputStream2 = inputStream;
                        fileOutputStream2 = null;
                        af.a(bufferedReader2);
                        af.a(inputStream2);
                        af.a(fileOutputStream2);
                        af.a(httpURLConnection2);
                        throw th;
                    }
                } catch (a e13) {
                    aVar = e13;
                    fileOutputStream = null;
                    inputStream = null;
                } catch (b e14) {
                    bufferedReader = null;
                    fileOutputStream2 = null;
                    httpURLConnection2 = httpURLConnection;
                    inputStream2 = null;
                    bVar = e14;
                    this.f277a.a("ms-atSetup", "download, error", bVar.getMessage(), 0);
                    throw new Exception(this.f277a.getString(g.l_connection_error, w.a(this.f277a, bVar)));
                } catch (com.avast.android.generic.internet.b.c e15) {
                    cVar = e15;
                    this.f277a.a("ms-atSetup", "download, error", cVar.getMessage(), 0);
                    throw new Exception(this.f277a.getString(g.l_http_status_error, w.a(this.f277a, cVar)));
                } catch (Exception e16) {
                    exc = e16;
                    this.f277a.a("ms-atSetup", "download, error", exc.getMessage(), 0);
                    z.a("AvastGeneric", "Exception in internet connectivity", exc);
                    throw new Exception(this.f277a.getString(g.l_error_in_download));
                } catch (Throwable th4) {
                    th = th4;
                    fileOutputStream2 = null;
                    httpURLConnection2 = httpURLConnection;
                    inputStream2 = null;
                    af.a(bufferedReader2);
                    af.a(inputStream2);
                    af.a(fileOutputStream2);
                    af.a(httpURLConnection2);
                    throw th;
                }
            } catch (a e17) {
                fileOutputStream = null;
                inputStream = null;
                httpURLConnection = null;
                aVar = e17;
            } catch (b e18) {
                bufferedReader = null;
                fileOutputStream2 = null;
                inputStream2 = null;
                httpURLConnection2 = null;
                bVar = e18;
                this.f277a.a("ms-atSetup", "download, error", bVar.getMessage(), 0);
                throw new Exception(this.f277a.getString(g.l_connection_error, w.a(this.f277a, bVar)));
            } catch (com.avast.android.generic.internet.b.c e19) {
                cVar = e19;
                this.f277a.a("ms-atSetup", "download, error", cVar.getMessage(), 0);
                throw new Exception(this.f277a.getString(g.l_http_status_error, w.a(this.f277a, cVar)));
            } catch (Exception e20) {
                exc = e20;
                this.f277a.a("ms-atSetup", "download, error", exc.getMessage(), 0);
                z.a("AvastGeneric", "Exception in internet connectivity", exc);
                throw new Exception(this.f277a.getString(g.l_error_in_download));
            } catch (Throwable th5) {
                th = th5;
                bufferedReader2 = bufferedReader;
                af.a(bufferedReader2);
                af.a(inputStream2);
                af.a(fileOutputStream2);
                af.a(httpURLConnection2);
                throw th;
            }
        } catch (Exception e21) {
            Exception exc2 = e21;
            this.f277a.a("ms-atSetup", "download, error", exc2.getMessage(), 0);
            this.f278b = true;
            this.f279c = exc2.getMessage();
        }
    }

    public void a(Void voidR) {
        if (this.f277a.isAdded()) {
            com.avast.android.generic.util.a.a(this.f277a, new i(this));
        }
    }
}
