package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.view.View;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.Application;
import com.avast.android.generic.a;

/* compiled from: ChooseNameFragment */
class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChooseNameFragment f273a;

    b(ChooseNameFragment chooseNameFragment) {
        this.f273a = chooseNameFragment;
    }

    public void onClick(View view) {
        this.f273a.a("ms-atSetup", "choose name", "continue", 0);
        String trim = this.f273a.d.d().toString().trim();
        if (trim.length() <= 4) {
            a.a(this.f273a.f, this.f273a.getString(g.l_package_name_too_short));
        } else if (this.f273a.e.matcher(trim).matches()) {
            Application.a(this.f273a.d.d().toString());
            this.f273a.startActivity(new Intent(this.f273a.getActivity(), DownloadWizardActivity.class));
        } else {
            a.a(this.f273a.f, this.f273a.getString(g.l_wrong_package_name));
        }
    }
}
