package com.avast.android.antitheft_setup_components.app.home.b;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.support.v4.app.Fragment;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.ui.d.d;

/* compiled from: AntiTheftAlreadyExistingProblem */
public class a extends d {
    public a(Context context) {
        super(g.l_installation_problem, g.l_installation_already_present);
        b(context.getString(g.R));
    }

    public boolean a(Context context, Fragment fragment) {
        return false;
    }

    public static boolean a(Context context) {
        for (ApplicationInfo next : context.getPackageManager().getInstalledApplications(0)) {
            if (next.packageName != null) {
                if (next.packageName.equals("com.avast.android.antitheft")) {
                    return true;
                }
                if (next.packageName.equals("com.avast.android.at_play")) {
                    return true;
                }
            }
        }
        return false;
    }

    public String a() {
        return "Anti Theft already existing";
    }
}
