package com.avast.android.antitheft_setup_components.app.home;

import android.content.Context;
import android.content.DialogInterface;
import com.avast.android.antitheft_setup_components.b.a;

/* compiled from: RootMethodFragment */
class ad implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f263a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ RootMethodFragment f264b;

    ad(RootMethodFragment rootMethodFragment, Context context) {
        this.f264b = rootMethodFragment;
        this.f263a = context;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f264b.a("ms-atSetup", "root method, update-zip", "edify", 0);
        a.a(this.f263a, new ae(this));
    }
}
