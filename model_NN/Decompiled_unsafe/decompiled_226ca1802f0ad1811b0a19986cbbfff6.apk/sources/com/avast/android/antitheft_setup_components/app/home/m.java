package com.avast.android.antitheft_setup_components.app.home;

import android.view.View;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.h;

/* compiled from: InstallationModeFragment */
class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ InstallationModeFragment f290a;

    m(InstallationModeFragment installationModeFragment) {
        this.f290a = installationModeFragment;
    }

    public void onClick(View view) {
        this.f290a.f246c.a(h.ADVANCED_INSTALL);
        this.f290a.a("ms-atSetup", "install mode", "advanced", 0);
        Application.b(false);
        a.a(this.f290a);
        SetupCheckWizardActivity.call(this.f290a.getActivity());
    }
}
