package com.avast.android.antitheft_setup_components.app.home;

import android.view.View;
import com.avast.android.generic.Application;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.h;

/* compiled from: InstallationModeFragment */
class o implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f293a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ InstallationModeFragment f294b;

    o(InstallationModeFragment installationModeFragment, String str) {
        this.f294b = installationModeFragment;
        this.f293a = str;
    }

    public void onClick(View view) {
        this.f294b.f246c.a(h.EASY_INSTALL);
        this.f294b.a("ms-atSetup", "install mode", "easyOld", 0);
        Application.b(true);
        Application.a(false);
        Application.a(this.f293a);
        a.a(this.f294b);
        SetupCheckWizardActivity.call(this.f294b.getActivity());
    }
}
