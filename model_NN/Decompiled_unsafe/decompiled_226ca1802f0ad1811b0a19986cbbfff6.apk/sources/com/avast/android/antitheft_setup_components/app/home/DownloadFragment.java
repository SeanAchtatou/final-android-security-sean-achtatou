package com.avast.android.antitheft_setup_components.app.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.avast.android.antitheft_setup_components.d;
import com.avast.android.antitheft_setup_components.e;
import com.avast.android.antitheft_setup_components.g;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.ga.TrackedFragment;

public class DownloadFragment extends TrackedFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Button f237a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Button f238b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ProgressBar f239c;
    /* access modifiers changed from: private */
    public TextView d;
    /* access modifiers changed from: private */
    public TextView e;

    public int a() {
        return g.l_downloading;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(e.fragment_download, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f237a = (Button) inflate.findViewById(d.f316c);
        this.f238b = (Button) inflate.findViewById(d.f315b);
        this.f239c = (ProgressBar) inflate.findViewById(d.progress);
        this.f239c.setProgress(0);
        this.d = (TextView) inflate.findViewById(d.l);
        this.e = (TextView) inflate.findViewById(d.o);
        this.f237a.setOnClickListener(new d(this));
        this.f238b.setOnClickListener(new e(this));
        c();
        return inflate;
    }

    /* access modifiers changed from: private */
    public void c() {
        new f(this).c(new Void[0]);
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 101) {
            d();
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        ((BaseSetupActivity) getActivity()).d();
        a.a(this);
    }

    public String b() {
        return "/ms/antiTheftSetup/download";
    }
}
