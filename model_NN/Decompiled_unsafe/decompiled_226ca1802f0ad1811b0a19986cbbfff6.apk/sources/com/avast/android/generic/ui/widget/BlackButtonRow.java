package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Button;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class BlackButtonRow extends Row {

    /* renamed from: a  reason: collision with root package name */
    protected Button f1074a;

    /* renamed from: b  reason: collision with root package name */
    private String f1075b;

    public BlackButtonRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row));
    }

    public BlackButtonRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowStyle, y.Row));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        this.f1075b = typedArray.getString(3);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_black_button, this);
        this.f1074a = (Button) findViewById(r.button);
        this.f1074a.setId(-1);
    }

    public void b() {
        this.f1074a.setText(e().b(this.g, this.f1075b));
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(z);
        super.setClickable(z);
        this.f1074a.setEnabled(z);
    }
}
