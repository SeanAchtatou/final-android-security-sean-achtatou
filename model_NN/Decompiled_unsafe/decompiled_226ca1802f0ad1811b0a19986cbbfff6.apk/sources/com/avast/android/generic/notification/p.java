package com.avast.android.generic.notification;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: AvastPendingIntent */
final class p implements Parcelable.Creator {
    p() {
    }

    /* renamed from: a */
    public AvastPendingIntent createFromParcel(Parcel parcel) {
        return new AvastPendingIntent(parcel, null);
    }

    /* renamed from: a */
    public AvastPendingIntent[] newArray(int i) {
        return new AvastPendingIntent[i];
    }
}
