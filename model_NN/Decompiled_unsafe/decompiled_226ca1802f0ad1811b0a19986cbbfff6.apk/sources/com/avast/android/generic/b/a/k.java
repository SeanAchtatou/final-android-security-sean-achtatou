package com.avast.android.generic.b.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: BadNewsProto */
public final class k extends GeneratedMessageLite implements m {

    /* renamed from: a  reason: collision with root package name */
    private static final k f613a = new k(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f614b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f615c;
    /* access modifiers changed from: private */
    public ByteString d;
    private byte e;
    private int f;

    private k(l lVar) {
        super(lVar);
        this.e = -1;
        this.f = -1;
    }

    private k(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static k a() {
        return f613a;
    }

    public boolean b() {
        return (this.f614b & 1) == 1;
    }

    public ByteString c() {
        return this.f615c;
    }

    public boolean d() {
        return (this.f614b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    private void i() {
        this.f615c = ByteString.EMPTY;
        this.d = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f614b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f615c);
        }
        if ((this.f614b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f614b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, this.f615c);
            }
            if ((this.f614b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            this.f = i;
        }
        return i;
    }

    public static l f() {
        return l.g();
    }

    /* renamed from: g */
    public l newBuilderForType() {
        return f();
    }

    public static l a(k kVar) {
        return f().mergeFrom(kVar);
    }

    /* renamed from: h */
    public l toBuilder() {
        return a(this);
    }

    static {
        f613a.i();
    }
}
