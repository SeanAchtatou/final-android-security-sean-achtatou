package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class EditTextRow extends Row {

    /* renamed from: a  reason: collision with root package name */
    private EditText f1088a;

    public EditTextRow(Context context) {
        super(context);
    }

    public EditTextRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row));
    }

    public EditTextRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowStyle, y.Row));
    }

    private void a(Context context, TypedArray typedArray) {
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_edit_text, this);
        this.f1088a = (EditText) findViewById(r.edit);
        this.f1088a.setId(-1);
        this.f1088a.setOnFocusChangeListener(new g(this));
    }

    public boolean c() {
        if (e() != null) {
            return e().a(this.g, this.f1088a.getText().toString());
        }
        return false;
    }

    public void b() {
        if (e() != null) {
            this.f1088a.setText(e().a(this.g));
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(z);
        super.setClickable(z);
        this.f1088a.setEnabled(z);
        this.f1088a.setFocusable(z);
        this.f1088a.setClickable(z);
        this.f1088a.setFocusableInTouchMode(z);
    }

    public CharSequence d() {
        return this.f1088a.getText().toString();
    }

    public void a(String str) {
        this.f1088a.setText(str);
    }

    public void a(int i) {
        this.f1088a.setSelection(i);
    }
}
