package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum i {
    PURCHASE_CANCELLED("purchase_cancelled"),
    PURCHASE_FAILED("purchase_failed"),
    USER_CANCELLED("user_cancelled");
    
    String d;

    private i(String str) {
        this.d = str;
    }

    public String a() {
        return this.d;
    }
}
