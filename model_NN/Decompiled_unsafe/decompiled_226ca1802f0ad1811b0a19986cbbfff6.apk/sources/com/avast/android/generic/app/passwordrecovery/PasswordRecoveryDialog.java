package com.avast.android.generic.app.passwordrecovery;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.r;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public class PasswordRecoveryDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private boolean f473a = false;

    /* renamed from: b  reason: collision with root package name */
    private ProgressDialog f474b;

    /* renamed from: c  reason: collision with root package name */
    private BroadcastReceiver f475c;
    /* access modifiers changed from: private */
    public r d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.f473a = bundle.getBoolean("progress_showing", false);
        }
        this.f475c = new c(this);
        this.d = r.a(getActivity());
        this.d.a(this.f475c, new IntentFilter("com.avast.android.generic.app.passwordrecovery.ACTION_NEW_STATE"));
    }

    @TargetApi(8)
    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
        builder.setTitle(getString(x.l_password_recovery_dialog_title));
        builder.setMessage(x.msg_password_recovery_confirmation);
        builder.setPositiveButton(x.da, new d(this));
        builder.setNegativeButton(x.F, new e(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new f(this, create));
        return create;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.f473a) {
            a(getActivity());
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            this.d.a(this.f475c);
        } catch (Exception e) {
        }
        a();
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        a();
        RecoveryResultDialog recoveryResultDialog = new RecoveryResultDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("result", z);
        recoveryResultDialog.setArguments(bundle);
        recoveryResultDialog.show(getFragmentManager(), "recovery_result_dialog");
        dismiss();
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        a();
        if (isAdded()) {
            this.f474b = new ProgressDialog(context);
            this.f474b.setMessage(getString(x.l_password_recovery_sending));
            this.f474b.setCancelable(false);
            this.f474b.show();
        }
        this.f473a = true;
    }

    private void a() {
        if (this.f474b != null && isAdded()) {
            this.f474b.hide();
        }
        this.f473a = false;
        this.f474b = null;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("progress_showing", this.f473a);
    }
}
