package com.avast.android.generic.licensing;

import java.util.concurrent.Semaphore;

/* compiled from: Locker */
public class b implements l {

    /* renamed from: a  reason: collision with root package name */
    private Semaphore f848a;

    /* renamed from: b  reason: collision with root package name */
    private m f849b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f850c;
    private String d;

    public b(Semaphore semaphore) {
        this.f848a = semaphore;
    }

    public void a(m mVar, String str) {
        if (mVar == m.UNKNOWN || mVar == m.NONE || mVar == m.NOT_AVAILABLE || mVar == m.VALID) {
            this.f848a.release();
        }
        this.f849b = mVar;
    }

    public m a() {
        return this.f849b;
    }

    public void a(long j) {
    }

    public void a(boolean z) {
        this.f850c = z;
    }

    public void a(String str) {
        this.d = str;
    }
}
