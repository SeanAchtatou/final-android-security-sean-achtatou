package com.avast.android.generic.internet;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.avast.android.generic.ab;
import com.avast.android.generic.c.a;
import com.avast.android.generic.c.d;
import com.avast.android.generic.c.g;
import com.avast.android.generic.c.k;
import com.avast.android.generic.c.o;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.z;
import com.avast.b.a.a.h;
import java.util.Collections;
import java.util.LinkedList;

public class HttpSender {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public AvastService f741a;

    /* renamed from: b  reason: collision with root package name */
    private ab f742b;

    /* renamed from: c  reason: collision with root package name */
    private ab f743c;
    private boolean d;
    /* access modifiers changed from: private */
    public Thread e;
    /* access modifiers changed from: private */
    public boolean f;
    private b g;
    /* access modifiers changed from: private */
    public Object h;
    private Object i;
    private HttpStatusChangedBroadcastReceiver j;
    /* access modifiers changed from: private */
    public ConnectivityManager k;
    /* access modifiers changed from: private */
    public LinkedList<h> l;

    public class HttpStatusChangedBroadcastReceiver extends BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ HttpSender f744a;

        public void onReceive(Context context, Intent intent) {
            new Thread(new i(this, intent), "httpSenderOnlineChecker").start();
        }
    }

    public void a() {
        z.a("AvastComms", this.f741a, "HTTP sender went online");
        NetworkInfo activeNetworkInfo = this.k.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            b();
            return;
        }
        this.d = true;
        f();
    }

    public void b() {
        z.a("AvastComms", this.f741a, "HTTP sender went offline");
        NetworkInfo activeNetworkInfo = this.k.getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            this.d = false;
            d();
            return;
        }
        a();
    }

    public synchronized boolean c() {
        boolean z;
        z.a("AvastComms", this.f741a, "Checking HTTP sender sending state...");
        synchronized (this.l) {
            z.a("AvastComms", this.f741a, "Checked HTTP sender sending state (queue size is " + this.l.size() + ")");
            z = this.l.size() > 0;
        }
        return z;
    }

    public void a(e eVar, a aVar, boolean z) {
        boolean z2;
        boolean z3 = false;
        if (eVar != null && eVar.c() != null) {
            h hVar = new h(this);
            if (eVar.c() != null) {
                hVar.f821a = eVar.c();
            } else {
                hVar.f821a = null;
            }
            if (aVar != null) {
                hVar.i = aVar.l();
            }
            hVar.d = aVar;
            hVar.e = z;
            hVar.j = eVar.d();
            hVar.f822b = eVar.b();
            if (aVar == null || aVar.d() == null) {
                hVar.f823c = null;
            } else {
                hVar.f823c = new g(aVar.d());
            }
            if (!(aVar == null || hVar.f822b == null)) {
                aVar.a(true);
            }
            hVar.f = eVar.a();
            if (aVar == null || aVar.e() == null) {
                z2 = false;
            } else {
                z2 = true;
            }
            if (aVar != null && aVar.d().b() && TextUtils.isEmpty(aVar.e())) {
                z2 = false;
            }
            if (!AvastService.b((Context) this.f741a)) {
                z.a("AvastComms", this.f741a, "SMS permission is not available");
                z2 = false;
            }
            if (!z2) {
                z.a("AvastComms", this.f741a, "HTTP sender: SMS is not available for command reply");
            }
            if (!this.d) {
                NetworkInfo activeNetworkInfo = this.k.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    z3 = true;
                } else if (hVar.d == null || hVar.d.b() || !z2 || !hVar.d.d().o()) {
                    z.a("AvastComms", this.f741a, "HTTP sender has to queue non SMS enabled descriptor " + hVar.a());
                } else {
                    c(hVar);
                    return;
                }
            }
            a(hVar);
            if (z3) {
                a();
            } else if (!this.d) {
                d();
            } else {
                f();
            }
        }
    }

    private void a(h hVar) {
        synchronized (this.l) {
            z.a("AvastComms", this.f741a, "HTTP sender is queueing HTTP traffic...");
            this.l.add(hVar);
            Collections.sort(this.l);
            z.a("AvastComms", this.f741a, "HTTP sender queued HTTP traffic (length " + this.l.size() + ")");
        }
    }

    private void f() {
        boolean z = false;
        synchronized (this.l) {
            if (this.l.size() > 0 && this.d) {
                z = true;
            }
        }
        if (z) {
            synchronized (this.h) {
                if (this.e != null) {
                    if (this.e.isAlive()) {
                        this.e.interrupt();
                        return;
                    }
                    this.e = null;
                }
                z.a("AvastComms", this.f741a, "Starting HTTP sender thread...");
                this.f = false;
                this.e = new Thread(new f(this), "httpSender");
                this.e.start();
                z.a("AvastComms", this.f741a, "Started HTTP sender thread");
            }
        }
    }

    public void d() {
        int size;
        synchronized (this.l) {
            size = this.l.size();
        }
        if (size == 0 || !this.d) {
            synchronized (this.h) {
                if (this.e != null) {
                    z.a("AvastComms", this.f741a, "Stopping HTTP sender thread...");
                    this.f = true;
                    try {
                        if (this.e.isAlive()) {
                            this.e.interrupt();
                            this.e.join();
                        }
                    } catch (Exception e2) {
                    }
                    this.f = false;
                    this.e = null;
                    z.a("AvastComms", this.f741a, "Stopped HTTP sender thread");
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002c, code lost:
        if (r9.f823c == null) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r9.f823c.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x018d, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x018e, code lost:
        com.avast.android.generic.util.z.a("AvastComms", r14.f741a, "HTTP sender callback error", r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001f, code lost:
        if (r9.h > new java.util.Date().getTime()) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        com.avast.android.generic.util.z.a("AvastComms", r14.f741a, "HTTP sender thread begins sending");
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r14 = this;
            java.util.LinkedList<com.avast.android.generic.internet.h> r2 = r14.l
            monitor-enter(r2)
            java.util.LinkedList<com.avast.android.generic.internet.h> r1 = r14.l     // Catch:{ all -> 0x018a }
            java.lang.Object r1 = r1.peek()     // Catch:{ all -> 0x018a }
            r0 = r1
            com.avast.android.generic.internet.h r0 = (com.avast.android.generic.internet.h) r0     // Catch:{ all -> 0x018a }
            r9 = r0
            if (r9 != 0) goto L_0x0011
            monitor-exit(r2)     // Catch:{ all -> 0x018a }
        L_0x0010:
            return
        L_0x0011:
            monitor-exit(r2)     // Catch:{ all -> 0x018a }
            java.util.Date r1 = new java.util.Date
            r1.<init>()
            long r1 = r1.getTime()
            long r3 = r9.h
            int r1 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r1 > 0) goto L_0x0010
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.service.AvastService r2 = r14.f741a
            java.lang.String r3 = "HTTP sender thread begins sending"
            com.avast.android.generic.util.z.a(r1, r2, r3)
            com.avast.android.generic.internet.d r1 = r9.f823c
            if (r1 == 0) goto L_0x0033
            com.avast.android.generic.internet.d r1 = r9.f823c     // Catch:{ Exception -> 0x018d }
            r1.a()     // Catch:{ Exception -> 0x018d }
        L_0x0033:
            java.lang.String r1 = "com.avast.android.backup"
            com.avast.android.generic.service.AvastService r2 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            boolean r1 = r1.equals(r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r2.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            if (r1 == 0) goto L_0x0199
            java.lang.String r1 = "https://ff-backup.avast.com"
        L_0x0048:
            java.lang.StringBuilder r1 = r2.append(r1)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = "/"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r12 = r1.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r1 = r14.f742b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            boolean r13 = r1.t()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r7 = 0
            com.avast.b.a.a.ak r1 = r9.f821a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            if (r1 == 0) goto L_0x019d
            com.avast.b.a.a.ak r1 = r9.f821a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.b.a.a.ak r5 = r1.clone()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
        L_0x0068:
            if (r13 == 0) goto L_0x01a0
            java.lang.String r1 = "AvastGenericSync"
            java.lang.String r2 = "Getting sync data for internet communications"
            com.avast.android.generic.util.z.a(r1, r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r1 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.f()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r1 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.e()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b r1 = r14.g     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r2 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r3 = r14.f742b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r4 = r14.f743c     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            int r6 = r9.i     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r1 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b.a(r1, r5, r7)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r1 = "AvastGenericSync"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r2.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = "Got sync data for internet communications (took "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            long r3 = r3 - r7
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = "ms)"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.util.z.a(r1, r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r10 = r7
        L_0x00b5:
            int r1 = r9.g     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            int r1 = 3 - r1
            if (r1 <= 0) goto L_0x01bc
            r7 = 1
        L_0x00bc:
            com.avast.android.generic.internet.k r1 = r9.f822b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            if (r1 != 0) goto L_0x01bf
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r1 = r1.append(r12)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = "rest/upload?v=1"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r4.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = "HTTP sender internet text send to "
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = ": "
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = r9.a()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = ""
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.util.z.a(r2, r3, r4)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b r2 = r14.g     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r3 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.b.a.a.h r1 = r2.a(r3, r1, r5, r7)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            boolean r1 = r14.a(r1)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
        L_0x010d:
            if (r13 == 0) goto L_0x0148
            if (r1 != 0) goto L_0x0148
            java.lang.String r1 = "AvastGenericSync"
            java.lang.String r2 = "Committing sync"
            com.avast.android.generic.util.z.a(r1, r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b.a()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r1 = r14.f742b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.e(r10)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r1 = r14.f743c     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.e(r10)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r1 = "AvastGenericSync"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r2.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = "Committed sync (took "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            long r3 = r3 - r10
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = "ms)"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.util.z.a(r1, r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
        L_0x0148:
            com.avast.android.generic.internet.d r1 = r9.f823c     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            if (r1 == 0) goto L_0x0151
            com.avast.android.generic.internet.d r1 = r9.f823c     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.b()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
        L_0x0151:
            java.util.LinkedList<com.avast.android.generic.internet.h> r2 = r14.l     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            monitor-enter(r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.util.LinkedList<com.avast.android.generic.internet.h> r1 = r14.l     // Catch:{ all -> 0x0210 }
            r1.remove(r9)     // Catch:{ all -> 0x0210 }
            monitor-exit(r2)     // Catch:{ all -> 0x0210 }
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.service.AvastService r2 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = "HTTP sender send succeeded"
            com.avast.android.generic.util.z.a(r1, r2, r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            boolean r1 = r14.c()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            if (r1 != 0) goto L_0x0010
            com.avast.android.generic.service.AvastService r1 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.b()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            goto L_0x0010
        L_0x0170:
            r1 = move-exception
            java.lang.String r2 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r14.f741a
            java.lang.String r4 = "HTTP sender IO Exception"
            com.avast.android.generic.util.z.a(r2, r3, r4, r1)
            r14.b(r9)
            boolean r1 = r14.c()
            if (r1 != 0) goto L_0x0010
            com.avast.android.generic.service.AvastService r1 = r14.f741a
            r1.b()
            goto L_0x0010
        L_0x018a:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x018a }
            throw r1
        L_0x018d:
            r1 = move-exception
            java.lang.String r2 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r14.f741a
            java.lang.String r4 = "HTTP sender callback error"
            com.avast.android.generic.util.z.a(r2, r3, r4, r1)
            goto L_0x0033
        L_0x0199:
            java.lang.String r1 = "https://ff-at.avast.com"
            goto L_0x0048
        L_0x019d:
            r5 = 0
            goto L_0x0068
        L_0x01a0:
            java.lang.String r1 = "AvastGenericSync"
            java.lang.String r2 = "Internet NOT enabled, getting basic data structure"
            com.avast.android.generic.util.z.a(r1, r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b r1 = r14.g     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r2 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r3 = r14.f742b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.ab r4 = r14.f743c     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            int r6 = r9.i     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r1 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b.a(r1, r5, r7)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r10 = r7
            goto L_0x00b5
        L_0x01bc:
            r7 = 0
            goto L_0x00bc
        L_0x01bf:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r1.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r1 = r1.append(r12)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r2 = "rest/mime?v=1"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r4 = r1.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.service.AvastService r2 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            r3.<init>()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = "HTTP sender internet file send to "
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = ": "
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = r9.a()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r6 = " bytes"
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.util.z.a(r1, r2, r3)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.b r2 = r14.g     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.service.AvastService r3 = r14.f741a     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.android.generic.internet.k r6 = r9.f822b     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            com.avast.b.a.a.h r1 = r2.a(r3, r4, r5, r6, r7)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            boolean r1 = r14.a(r1)     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
            goto L_0x010d
        L_0x0210:
            r1 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0210 }
            throw r1     // Catch:{ IOException -> 0x0170, Exception -> 0x0213 }
        L_0x0213:
            r1 = move-exception
            java.lang.String r2 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r14.f741a
            java.lang.String r4 = "HTTP sender general exception"
            com.avast.android.generic.util.z.a(r2, r3, r4, r1)
            r14.b(r9)
            boolean r1 = r14.c()
            if (r1 != 0) goto L_0x0010
            com.avast.android.generic.service.AvastService r1 = r14.f741a
            r1.b()
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.HttpSender.g():void");
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(com.avast.android.generic.internet.h r9) {
        /*
            r8 = this;
            r1 = 1
            r0 = 0
            java.util.LinkedList<com.avast.android.generic.internet.h> r2 = r8.l
            monitor-enter(r2)
            java.util.LinkedList<com.avast.android.generic.internet.h> r3 = r8.l     // Catch:{ all -> 0x0083 }
            r3.remove(r9)     // Catch:{ all -> 0x0083 }
            int r3 = r9.g     // Catch:{ all -> 0x0083 }
            int r3 = r3 + 1
            r9.g = r3     // Catch:{ all -> 0x0083 }
            int r3 = r9.g     // Catch:{ all -> 0x0083 }
            r4 = 3
            if (r3 > r4) goto L_0x005a
            int r1 = r9.g     // Catch:{ all -> 0x0083 }
            int r3 = r9.g     // Catch:{ all -> 0x0083 }
            int r1 = r1 * r3
            int r1 = r1 * 2000
            long r3 = (long) r1     // Catch:{ all -> 0x0083 }
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.service.AvastService r5 = r8.f741a     // Catch:{ all -> 0x0083 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0083 }
            r6.<init>()     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = "HTTP sender is requeueing reliable HTTP descriptor (retry count "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0083 }
            int r7 = r9.g     // Catch:{ all -> 0x0083 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = ", timeout "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0083 }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ all -> 0x0083 }
            java.lang.String r7 = "ms)..."
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0083 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0083 }
            com.avast.android.generic.util.z.a(r1, r5, r6)     // Catch:{ all -> 0x0083 }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x0083 }
            r1.<init>()     // Catch:{ all -> 0x0083 }
            long r5 = r1.getTime()     // Catch:{ all -> 0x0083 }
            long r3 = r3 + r5
            r9.h = r3     // Catch:{ all -> 0x0083 }
            r8.a(r9)     // Catch:{ all -> 0x0083 }
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
        L_0x0059:
            return r0
        L_0x005a:
            com.avast.android.generic.service.AvastService r0 = r8.f741a     // Catch:{ all -> 0x0083 }
            boolean r0 = com.avast.android.generic.service.AvastService.b(r0)     // Catch:{ all -> 0x0083 }
            if (r0 != 0) goto L_0x0086
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r8.f741a     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = "HTTP sender is not able to forward reliable HTTP descriptor because SMS permission is missing"
            com.avast.android.generic.util.z.a(r0, r3, r4)     // Catch:{ all -> 0x0083 }
            com.avast.android.generic.internet.d r0 = r9.f823c     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x0075
            com.avast.android.generic.internet.d r0 = r9.f823c     // Catch:{ Exception -> 0x0078 }
            r3 = 0
            r0.a(r3)     // Catch:{ Exception -> 0x0078 }
        L_0x0075:
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            r0 = r1
            goto L_0x0059
        L_0x0078:
            r0 = move-exception
            java.lang.String r3 = "AvastComms"
            com.avast.android.generic.service.AvastService r4 = r8.f741a     // Catch:{ all -> 0x0083 }
            java.lang.String r5 = "HTTP sender error callback error"
            com.avast.android.generic.util.z.a(r3, r4, r5, r0)     // Catch:{ all -> 0x0083 }
            goto L_0x0075
        L_0x0083:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            throw r0
        L_0x0086:
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.service.AvastService r3 = r8.f741a     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = "HTTP sender is SMSing reliable HTTP descriptor because of too much failures"
            com.avast.android.generic.util.z.a(r0, r3, r4)     // Catch:{ all -> 0x0083 }
            r0 = 0
            r9.g = r0     // Catch:{ all -> 0x0083 }
            r8.c(r9)     // Catch:{ all -> 0x0083 }
            monitor-exit(r2)     // Catch:{ all -> 0x0083 }
            r0 = r1
            goto L_0x0059
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.HttpSender.b(com.avast.android.generic.internet.h):boolean");
    }

    /* access modifiers changed from: private */
    public void c(h hVar) {
        a aVar = hVar.d;
        if (aVar == null || aVar.b() || !hVar.d.d().o()) {
            z.a("AvastComms", this.f741a, "HTTP sender, SMSing is disabled");
            if (hVar.f823c != null) {
                try {
                    hVar.f823c.a(null);
                } catch (Exception e2) {
                    z.a("AvastComms", this.f741a, "HTTP sender error callback error", e2);
                }
            }
        } else if (aVar.e() != null) {
            if (!aVar.d().b() || !TextUtils.isEmpty(aVar.e())) {
                aVar.b(true);
                try {
                    if (aVar.f()) {
                        if (hVar.e) {
                            aVar.i();
                        } else {
                            aVar.g();
                        }
                        z.a("AvastComms", this.f741a, "HTTP sender finished SMS forwarding");
                    }
                    aVar.h();
                    z.a("AvastComms", this.f741a, "HTTP sender finished SMS forwarding");
                } catch (Exception e3) {
                    z.a("AvastComms", this.f741a, "HTTP sender error in handler handling", e3);
                }
            }
        }
    }

    public void e() {
        z.a("AvastComms", this.f741a, "Destroying HTTP sender...");
        synchronized (this.i) {
            if (this.j != null) {
                try {
                    this.f741a.unregisterReceiver(this.j);
                } catch (Exception e2) {
                }
            }
        }
        new Thread(new g(this), "httpSenderDestroyer").start();
        z.a("AvastComms", this.f741a, "Destroyed HTTP sender");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void */
    public boolean a(h hVar) {
        boolean z;
        if (hVar == null) {
            return false;
        }
        if (hVar.F() && !this.f742b.q().equals(hVar.G())) {
            this.f742b.i(hVar.G());
            this.f742b.b();
        }
        if (hVar.z() && !hVar.A()) {
            this.f741a.g();
        }
        this.f741a.a(hVar);
        if (!this.f742b.b("settingsLogicChange2573", false)) {
            this.f742b.a("settingsLogicChange2573", true);
            this.f742b.b();
        }
        if (!this.f742b.b("settingsLogicChange2582", false)) {
            this.f742b.a("settingsLogicChange2582", true);
            this.f742b.b();
        }
        if (!this.f742b.b("settingsLogicChange3891", false)) {
            this.f742b.a("settingsLogicChange3891", true);
            this.f742b.b();
        }
        if (!this.f742b.b("settingsLogicChange5026", false)) {
            this.f742b.a("settingsLogicChange5026", true);
            this.f742b.b();
        }
        if (hVar.t()) {
            z = hVar.u();
        } else {
            z = false;
        }
        if (hVar.e() <= 0) {
            return z;
        }
        for (String trim : hVar.d()) {
            String trim2 = trim.trim();
            if (!trim2.equals("")) {
                z.b(this.f741a, "INTERNET", "HTTP component received internet command " + trim2);
                k a2 = d.a(this.f741a, trim2);
                if (a2.f639a != o.TOOL || !this.f741a.getPackageName().equals(a2.f641c)) {
                    Intent intent = new Intent();
                    intent.setAction("com.avast.android.generic.action.ACTION_C2DM_MESSAGE");
                    intent.putExtra("message", trim2);
                    intent.putExtra("uid", a2.f640b);
                    d.a(this.f741a, intent, a2, null, a2.f640b, trim2, false);
                } else {
                    z.a(this.f741a, "HTTP component dispatches " + trim2 + " to own process");
                    Bundle bundle = new Bundle();
                    bundle.putString("message", trim2);
                    bundle.putString("uid", a2.f640b);
                    this.f741a.a("com.avast.android.generic.action.ACTION_C2DM_MESSAGE", bundle);
                }
                z.a(this.f741a, "HTTP component handled internet command " + trim2);
            }
        }
        return z;
    }
}
