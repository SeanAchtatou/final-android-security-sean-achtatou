package com.avast.android.generic.licensing;

import android.content.BroadcastReceiver;
import android.content.Context;
import com.avast.android.generic.Application;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;

public class PremiumHelper {

    public abstract class OnPremiumStateChangeListener extends BroadcastReceiver {
    }

    public static boolean a(Context context) {
        if (!Application.f317a) {
            return true;
        }
        return ((ab) aa.a(context, ab.class)).N();
    }

    public static long b(Context context) {
        if (!a(context)) {
            return -2;
        }
        return ((ab) aa.a(context, ab.class)).O().longValue();
    }

    public static boolean c(Context context) {
        if (!a(context)) {
            return false;
        }
        return ((ab) aa.a(context, ab.class)).P();
    }

    public static boolean d(Context context) {
        if (!a(context)) {
            return false;
        }
        String Q = ((ab) aa.a(context, ab.class)).Q();
        return Q != null && Q.contains("trial");
    }

    public static String e(Context context) {
        if (!Application.f317a) {
            return "";
        }
        return ((ab) aa.a(context, ab.class)).Q();
    }
}
