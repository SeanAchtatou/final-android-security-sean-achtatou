package com.avast.android.generic.licensing;

/* compiled from: LicensingBroadcastReceiver */
/* synthetic */ class a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f843a = new int[i.values().length];

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ int[] f844b = new int[k.values().length];

    static {
        try {
            f844b[k.INIT_STARTED.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f844b[k.INIT_FINISHED.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f844b[k.FAILED.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f844b[k.VALID.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f844b[k.NO_RUN.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f843a[i.GV_TOO_MANY_ACCOUNTS.ordinal()] = 1;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f843a[i.GV_TOO_MANY_GUIDS.ordinal()] = 2;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f843a[i.GV_NON_UNIQUE_GUID.ordinal()] = 3;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f843a[i.GV_VOUCHER_CODE_ALREADY_CONSUMED.ordinal()] = 4;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f843a[i.GV_VOUCHER_CODE_LOCKED.ordinal()] = 5;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f843a[i.GV_VOUCHER_CODE_UNKNOWN.ordinal()] = 6;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f843a[i.GV_VOUCHER_CODE_WRONG_LICENSE.ordinal()] = 7;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f843a[i.NO_GOOGLE_ACCOUNT.ordinal()] = 8;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f843a[i.GV_LICENSE_ALREADY_EXPIRED.ordinal()] = 9;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f843a[i.GV_LICENSE_INVALID.ordinal()] = 10;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f843a[i.NO_CONNECTION.ordinal()] = 11;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f843a[i.BILLING_NOT_AVAILABLE.ordinal()] = 12;
        } catch (NoSuchFieldError e17) {
        }
        try {
            f843a[i.CANNOT_RESTORE.ordinal()] = 13;
        } catch (NoSuchFieldError e18) {
        }
        try {
            f843a[i.COMMIT_COMMUNICATION_PROBLEM.ordinal()] = 14;
        } catch (NoSuchFieldError e19) {
        }
        try {
            f843a[i.RESTORE_TOO_OFTEN.ordinal()] = 15;
        } catch (NoSuchFieldError e20) {
        }
        try {
            f843a[i.GOOGLE_PLAY_ERROR.ordinal()] = 16;
        } catch (NoSuchFieldError e21) {
        }
        try {
            f843a[i.GOOGLE_PLAY_ERROR_INTENT.ordinal()] = 17;
        } catch (NoSuchFieldError e22) {
        }
        try {
            f843a[i.CAN_NOT_VALIDATE_GOOGLE_ACCOUNTS_GENERIC.ordinal()] = 18;
        } catch (NoSuchFieldError e23) {
        }
        try {
            f843a[i.GOOGLE_ACCOUNT_VALIDATION_ERROR_RETRY.ordinal()] = 19;
        } catch (NoSuchFieldError e24) {
        }
        try {
            f843a[i.GOOGLE_ACCOUNT_NOT_RECOVERABLE.ordinal()] = 20;
        } catch (NoSuchFieldError e25) {
        }
        try {
            f843a[i.INVALID.ordinal()] = 21;
        } catch (NoSuchFieldError e26) {
        }
        try {
            f843a[i.GENERIC.ordinal()] = 22;
        } catch (NoSuchFieldError e27) {
        }
    }
}
