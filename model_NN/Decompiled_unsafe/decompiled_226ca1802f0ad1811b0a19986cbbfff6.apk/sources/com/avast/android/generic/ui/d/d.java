package com.avast.android.generic.ui.d;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.x;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ProblemCheckerRowResource */
public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    private int f1037a;

    /* renamed from: b  reason: collision with root package name */
    private f f1038b;

    /* renamed from: c  reason: collision with root package name */
    private int f1039c;
    private String d;
    private int e = 0;
    private String f;

    public abstract String a();

    public abstract boolean a(Context context, Fragment fragment);

    public d(int i, int i2) {
        this.f1037a = i;
        this.f1039c = i2;
    }

    public void a(String str) {
        this.d = str;
    }

    public String b(Context context) {
        if (this.f1038b != null) {
            return this.f1038b.a(context);
        }
        return context.getString(this.f1037a);
    }

    public Spanned c(Context context) {
        if (this.f1038b != null) {
            return this.f1038b.b(context);
        }
        return Html.fromHtml(context.getString(this.f1039c));
    }

    public boolean b() {
        return false;
    }

    public boolean c() {
        return false;
    }

    public List<String> d(Context context) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(f(context));
        if (this.e > 0) {
            linkedList.add(context.getString(x.l_display_help_image));
        }
        if (this.d != null) {
            linkedList.add(context.getString(x.l_faq_entry));
        }
        return linkedList;
    }

    public boolean a(Context context, Fragment fragment, int i) {
        if (i == 0) {
            return a(context, fragment);
        }
        if (this.e > 0) {
            if (i == 1) {
                e(context);
            } else if (this.d != null && i == 2) {
                context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.d)));
            }
        } else if (this.d != null && i == 1) {
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.d)));
        }
        return false;
    }

    public void b(Context context, Fragment fragment) {
    }

    public void a(int i) {
        this.e = i;
    }

    /* access modifiers changed from: protected */
    public void e(Context context) {
        if (this.e > 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            View inflate = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(t.image_popup_window, (ViewGroup) null);
            ((ImageView) inflate.findViewById(r.fullimage)).setImageResource(this.e);
            builder.setView(inflate);
            builder.setPositiveButton(context.getString(x.bR), new e(this));
            builder.create();
            builder.show();
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.f = str;
    }

    /* access modifiers changed from: protected */
    public String f(Context context) {
        return this.f != null ? this.f : context.getString(x.l_fix_item);
    }
}
