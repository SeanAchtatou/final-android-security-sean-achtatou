package com.avast.android.generic.h;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: ShepherdResponseProto */
public final class u extends GeneratedMessageLite implements w {

    /* renamed from: a  reason: collision with root package name */
    private static final u f724a = new u(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f725b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f726c;
    /* access modifiers changed from: private */
    public ByteString d;
    private byte e;
    private int f;

    private u(v vVar) {
        super(vVar);
        this.e = -1;
        this.f = -1;
    }

    private u(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static u a() {
        return f724a;
    }

    public boolean b() {
        return (this.f725b & 1) == 1;
    }

    public int c() {
        return this.f726c;
    }

    public boolean d() {
        return (this.f725b & 2) == 2;
    }

    public ByteString e() {
        return this.d;
    }

    private void i() {
        this.f726c = 0;
        this.d = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 != -1) {
            if (b2 == 1) {
                return true;
            }
            return false;
        } else if (!b()) {
            this.e = 0;
            return false;
        } else {
            this.e = 1;
            return true;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f725b & 1) == 1) {
            codedOutputStream.writeInt32(1, this.f726c);
        }
        if ((this.f725b & 2) == 2) {
            codedOutputStream.writeBytes(2, this.d);
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f725b & 1) == 1) {
                i = 0 + CodedOutputStream.computeInt32Size(1, this.f726c);
            }
            if ((this.f725b & 2) == 2) {
                i += CodedOutputStream.computeBytesSize(2, this.d);
            }
            this.f = i;
        }
        return i;
    }

    public static u a(ByteString byteString) {
        return ((v) f().mergeFrom(byteString)).i();
    }

    public static v f() {
        return v.h();
    }

    /* renamed from: g */
    public v newBuilderForType() {
        return f();
    }

    public static v a(u uVar) {
        return f().mergeFrom(uVar);
    }

    /* renamed from: h */
    public v toBuilder() {
        return a(this);
    }

    static {
        f724a.i();
    }
}
