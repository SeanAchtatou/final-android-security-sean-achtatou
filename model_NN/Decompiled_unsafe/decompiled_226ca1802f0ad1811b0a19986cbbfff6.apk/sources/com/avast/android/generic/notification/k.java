package com.avast.android.generic.notification;

import android.os.AsyncTask;
import com.avast.android.generic.e;

/* compiled from: AvastNotificationManager */
class k extends AsyncTask<Void, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ long f939a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f940b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ h f941c;

    k(h hVar, long j, String str) {
        this.f941c = hVar;
        this.f939a = j;
        this.f940b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        this.f941c.f933a.getContentResolver().delete(e.a(this.f941c.f935c, this.f939a, this.f940b), null, null);
        return null;
    }
}
