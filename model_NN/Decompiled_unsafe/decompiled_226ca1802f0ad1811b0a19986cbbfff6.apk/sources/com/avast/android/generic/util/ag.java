package com.avast.android.generic.util;

import android.content.Context;
import android.os.Environment;
import android.os.StatFs;
import com.avast.android.generic.util.a.a;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

/* compiled from: SuUtil */
public class ag {

    /* renamed from: a  reason: collision with root package name */
    private static final String f1176a = (Environment.getExternalStorageDirectory() + "/UPDATE.zip");

    /* JADX WARNING: Removed duplicated region for block: B:107:0x01ee A[SYNTHETIC, Splitter:B:107:0x01ee] */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x026d A[SYNTHETIC, Splitter:B:143:0x026d] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r20, java.lang.String r21, java.lang.String r22, boolean r23, int r24, int r25, boolean r26, boolean r27, int r28, com.avast.android.generic.util.u r29, com.avast.android.generic.util.al r30, boolean r31) {
        /*
            r9 = 0
            r8 = 0
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 19
            if (r3 >= r4) goto L_0x0051
            r3 = 1
            r4 = r3
        L_0x000f:
            java.lang.String r6 = "/system"
            if (r4 == 0) goto L_0x0054
            java.lang.String r3 = "/system/app/com.avast.android.antitheft.apk"
        L_0x0015:
            r0 = r20
            r1 = r21
            a(r0, r1, r6, r3)     // Catch:{ Exception -> 0x0044 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x0044 }
            if (r4 == 0) goto L_0x0057
            java.lang.String r3 = "/system/app/com.avast.android.antitheft.odex"
        L_0x0022:
            r6.<init>(r3)     // Catch:{ Exception -> 0x0044 }
            boolean r14 = r6.exists()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = "mount"
            com.avast.android.generic.util.o.a(r3, r5)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = "\n"
            java.lang.String[] r12 = r3.split(r5)     // Catch:{ Exception -> 0x0044 }
            int r3 = r12.length     // Catch:{ Exception -> 0x0044 }
            r5 = 1
            if (r3 >= r5) goto L_0x005a
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "Could not get mount data"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0044 }
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x0044:
            r3 = move-exception
            r4 = r9
        L_0x0046:
            if (r4 == 0) goto L_0x004b
            r4.close()
        L_0x004b:
            if (r8 == 0) goto L_0x0050
            r8.close()
        L_0x0050:
            throw r3
        L_0x0051:
            r3 = 0
            r4 = r3
            goto L_0x000f
        L_0x0054:
            java.lang.String r3 = "/system/priv-app/com.avast.android.antitheft.apk"
            goto L_0x0015
        L_0x0057:
            java.lang.String r3 = "/system/priv-app/com.avast.android.antitheft.odex"
            goto L_0x0022
        L_0x005a:
            r10 = 0
            r7 = 0
            r6 = 0
            r5 = 0
            r3 = 0
            r19 = r3
            r3 = r5
            r5 = r6
            r6 = r7
            r7 = r10
            r10 = r19
        L_0x0067:
            int r11 = r12.length     // Catch:{ Exception -> 0x0044 }
            if (r10 >= r11) goto L_0x00b6
            r11 = r12[r10]     // Catch:{ Exception -> 0x0044 }
            java.lang.String r13 = " "
            java.lang.String[] r13 = r11.split(r13)     // Catch:{ Exception -> 0x0044 }
            int r11 = r13.length     // Catch:{ Exception -> 0x0044 }
            if (r11 <= 0) goto L_0x00b3
            r11 = 0
        L_0x0076:
            int r15 = r13.length     // Catch:{ Exception -> 0x0044 }
            if (r11 >= r15) goto L_0x00b3
            r15 = r13[r11]     // Catch:{ Exception -> 0x0044 }
            java.lang.String r16 = "/system"
            boolean r15 = r15.equals(r16)     // Catch:{ Exception -> 0x0044 }
            if (r15 == 0) goto L_0x0097
            if (r11 != 0) goto L_0x008d
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "Invalid mount data format"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0044 }
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x008d:
            int r6 = r11 + -1
            r7 = r13[r6]     // Catch:{ Exception -> 0x0044 }
            r6 = 0
            r6 = r13[r6]     // Catch:{ Exception -> 0x0044 }
        L_0x0094:
            int r11 = r11 + 1
            goto L_0x0076
        L_0x0097:
            r15 = r13[r11]     // Catch:{ Exception -> 0x0044 }
            java.lang.String r16 = "/data/local/mnt/system_ro"
            boolean r15 = r15.equals(r16)     // Catch:{ Exception -> 0x0044 }
            if (r15 == 0) goto L_0x0094
            if (r11 != 0) goto L_0x00ab
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "Invalid mount data format"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0044 }
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x00ab:
            int r3 = r11 + -1
            r5 = r13[r3]     // Catch:{ Exception -> 0x0044 }
            r3 = 0
            r3 = r13[r3]     // Catch:{ Exception -> 0x0044 }
            goto L_0x0094
        L_0x00b3:
            int r10 = r10 + 1
            goto L_0x0067
        L_0x00b6:
            java.lang.String r10 = "AvastGeneric"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r11.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r12 = "MTD Block 1: "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r11 = r11.append(r7)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r12 = ", MTD Block 2: "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r11 = r11.append(r6)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r12 = ", MTD Block 3: "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r11 = r11.append(r5)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r12 = ", MTD Block 4: "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r11 = r11.append(r3)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x0044 }
            r0 = r20
            com.avast.android.generic.util.z.a(r10, r0, r11)     // Catch:{ Exception -> 0x0044 }
            if (r7 == 0) goto L_0x0110
            java.lang.String r10 = ""
            boolean r10 = r7.equals(r10)     // Catch:{ Exception -> 0x0044 }
            if (r10 != 0) goto L_0x0110
            java.lang.String r10 = "/"
            boolean r10 = r7.startsWith(r10)     // Catch:{ Exception -> 0x0044 }
            if (r10 != 0) goto L_0x0726
            java.lang.String r10 = "emmc@"
            boolean r10 = r7.startsWith(r10)     // Catch:{ Exception -> 0x0044 }
            if (r10 != 0) goto L_0x0726
            java.lang.String r10 = "ubi"
            boolean r10 = r7.startsWith(r10)     // Catch:{ Exception -> 0x0044 }
            if (r10 != 0) goto L_0x0726
        L_0x0110:
            if (r6 == 0) goto L_0x0132
            java.lang.String r7 = ""
            boolean r7 = r6.equals(r7)     // Catch:{ Exception -> 0x0044 }
            if (r7 != 0) goto L_0x0132
            java.lang.String r7 = "/"
            boolean r7 = r6.startsWith(r7)     // Catch:{ Exception -> 0x0044 }
            if (r7 != 0) goto L_0x0723
            java.lang.String r7 = "emmc@"
            boolean r7 = r6.startsWith(r7)     // Catch:{ Exception -> 0x0044 }
            if (r7 != 0) goto L_0x0723
            java.lang.String r7 = "ubi"
            boolean r7 = r6.startsWith(r7)     // Catch:{ Exception -> 0x0044 }
            if (r7 != 0) goto L_0x0723
        L_0x0132:
            if (r5 == 0) goto L_0x0154
            java.lang.String r6 = ""
            boolean r6 = r5.equals(r6)     // Catch:{ Exception -> 0x0044 }
            if (r6 != 0) goto L_0x0154
            java.lang.String r6 = "/"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0044 }
            if (r6 != 0) goto L_0x0720
            java.lang.String r6 = "emmc@"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0044 }
            if (r6 != 0) goto L_0x0720
            java.lang.String r6 = "ubi"
            boolean r6 = r5.startsWith(r6)     // Catch:{ Exception -> 0x0044 }
            if (r6 != 0) goto L_0x0720
        L_0x0154:
            if (r3 == 0) goto L_0x0176
            java.lang.String r5 = ""
            boolean r5 = r3.equals(r5)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x0176
            java.lang.String r5 = "/"
            boolean r5 = r3.startsWith(r5)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x017e
            java.lang.String r5 = "emmc@"
            boolean r5 = r3.startsWith(r5)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x017e
            java.lang.String r5 = "ubi"
            boolean r5 = r3.startsWith(r5)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x017e
        L_0x0176:
            java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0044 }
            java.lang.String r4 = "Invalid MTD block"
            r3.<init>(r4)     // Catch:{ Exception -> 0x0044 }
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x017e:
            r13 = r3
        L_0x017f:
            java.lang.String r3 = "AvastGeneric"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r5.<init>()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r6 = "MTD block: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r5 = r5.append(r13)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0044 }
            r0 = r20
            com.avast.android.generic.util.z.a(r3, r0, r5)     // Catch:{ Exception -> 0x0044 }
            r11 = 0
            r10 = 0
            r3 = 0
            if (r31 == 0) goto L_0x071b
            java.lang.String r5 = "/system"
            r0 = r22
            boolean r5 = r0.startsWith(r5)     // Catch:{ Exception -> 0x0044 }
            if (r5 == 0) goto L_0x071b
            java.io.File r12 = new java.io.File     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = "/system/addon.d/50-cm.sh"
            r12.<init>(r3)     // Catch:{ Exception -> 0x0044 }
            boolean r7 = r12.exists()     // Catch:{ Exception -> 0x0044 }
            if (r4 == 0) goto L_0x01f2
            java.lang.String r3 = "app/com.avast.android.antitheft.apk"
        L_0x01b7:
            java.lang.String r15 = "etc/com.avast.android.antitheft.backup.enc"
            java.io.File r16 = new java.io.File     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = "/system/etc/custom_backup_list.txt"
            r0 = r16
            r0.<init>(r5)     // Catch:{ Exception -> 0x0044 }
            java.util.LinkedList r17 = new java.util.LinkedList     // Catch:{ Exception -> 0x0044 }
            r17.<init>()     // Catch:{ Exception -> 0x0044 }
            boolean r5 = r16.exists()     // Catch:{ Exception -> 0x0044 }
            if (r5 == 0) goto L_0x01f8
            r6 = 0
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0702 }
            java.io.FileReader r18 = new java.io.FileReader     // Catch:{ Exception -> 0x0702 }
            r0 = r18
            r1 = r16
            r0.<init>(r1)     // Catch:{ Exception -> 0x0702 }
            r0 = r18
            r5.<init>(r0)     // Catch:{ Exception -> 0x0702 }
        L_0x01de:
            java.lang.String r6 = r5.readLine()     // Catch:{ Exception -> 0x01ea }
            if (r6 == 0) goto L_0x01f5
            r0 = r17
            r0.add(r6)     // Catch:{ Exception -> 0x01ea }
            goto L_0x01de
        L_0x01ea:
            r3 = move-exception
            r4 = r5
        L_0x01ec:
            if (r4 == 0) goto L_0x01f1
            r4.close()     // Catch:{ Exception -> 0x0044 }
        L_0x01f1:
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x01f2:
            java.lang.String r3 = "priv-app/com.avast.android.antitheft.apk"
            goto L_0x01b7
        L_0x01f5:
            r5.close()     // Catch:{ Exception -> 0x01ea }
        L_0x01f8:
            r0 = r17
            boolean r5 = r0.contains(r3)     // Catch:{ Exception -> 0x0044 }
            if (r5 == 0) goto L_0x0208
            r0 = r17
            boolean r5 = r0.contains(r15)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x0245
        L_0x0208:
            r0 = r17
            boolean r5 = r0.contains(r3)     // Catch:{ Exception -> 0x0044 }
            if (r5 != 0) goto L_0x0215
            r0 = r17
            r0.add(r3)     // Catch:{ Exception -> 0x0044 }
        L_0x0215:
            r0 = r17
            boolean r3 = r0.contains(r15)     // Catch:{ Exception -> 0x0044 }
            if (r3 != 0) goto L_0x0222
            r0 = r17
            r0.add(r15)     // Catch:{ Exception -> 0x0044 }
        L_0x0222:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r5.<init>()     // Catch:{ Exception -> 0x0044 }
            java.util.Iterator r6 = r17.iterator()     // Catch:{ Exception -> 0x0044 }
        L_0x022b:
            boolean r3 = r6.hasNext()     // Catch:{ Exception -> 0x0044 }
            if (r3 == 0) goto L_0x0241
            java.lang.Object r3 = r6.next()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r11 = "\n"
            r3.append(r11)     // Catch:{ Exception -> 0x0044 }
            goto L_0x022b
        L_0x0241:
            java.lang.String r11 = r5.toString()     // Catch:{ Exception -> 0x0044 }
        L_0x0245:
            if (r7 == 0) goto L_0x0716
            r6 = 0
            java.util.LinkedList r10 = new java.util.LinkedList     // Catch:{ Exception -> 0x0044 }
            r10.<init>()     // Catch:{ Exception -> 0x0044 }
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x06fe }
            java.io.FileReader r3 = new java.io.FileReader     // Catch:{ Exception -> 0x06fe }
            r3.<init>(r12)     // Catch:{ Exception -> 0x06fe }
            r5.<init>(r3)     // Catch:{ Exception -> 0x06fe }
        L_0x0257:
            java.lang.String r3 = r5.readLine()     // Catch:{ Exception -> 0x0269 }
            if (r3 == 0) goto L_0x0281
            java.lang.String r6 = "etc/hosts"
            boolean r6 = r3.startsWith(r6)     // Catch:{ Exception -> 0x0269 }
            if (r6 != 0) goto L_0x0271
            r10.add(r3)     // Catch:{ Exception -> 0x0269 }
            goto L_0x0257
        L_0x0269:
            r3 = move-exception
            r4 = r5
        L_0x026b:
            if (r4 == 0) goto L_0x0270
            r4.close()     // Catch:{ Exception -> 0x0044 }
        L_0x0270:
            throw r3     // Catch:{ Exception -> 0x0044 }
        L_0x0271:
            if (r4 == 0) goto L_0x027e
            java.lang.String r3 = "app/com.avast.android.antitheft.apk"
        L_0x0275:
            r10.add(r3)     // Catch:{ Exception -> 0x0269 }
            java.lang.String r3 = "etc/com.avast.android.antitheft.backup.enc"
            r10.add(r3)     // Catch:{ Exception -> 0x0269 }
            goto L_0x0257
        L_0x027e:
            java.lang.String r3 = "priv-app/com.avast.android.antitheft.apk"
            goto L_0x0275
        L_0x0281:
            r5.close()     // Catch:{ Exception -> 0x0269 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0044 }
            r5.<init>()     // Catch:{ Exception -> 0x0044 }
            java.util.Iterator r6 = r10.iterator()     // Catch:{ Exception -> 0x0044 }
        L_0x028d:
            boolean r3 = r6.hasNext()     // Catch:{ Exception -> 0x0044 }
            if (r3 == 0) goto L_0x02a3
            java.lang.Object r3 = r6.next()     // Catch:{ Exception -> 0x0044 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0044 }
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x0044 }
            java.lang.String r10 = "\n"
            r3.append(r10)     // Catch:{ Exception -> 0x0044 }
            goto L_0x028d
        L_0x02a3:
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x0044 }
            r10 = r7
            r12 = r11
            r11 = r3
        L_0x02aa:
            if (r23 == 0) goto L_0x04f1
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0044 }
            java.lang.String r5 = com.avast.android.generic.util.ag.f1176a     // Catch:{ Exception -> 0x0044 }
            r3.<init>(r5)     // Catch:{ Exception -> 0x0044 }
            r3.createNewFile()     // Catch:{ Exception -> 0x0044 }
            java.util.zip.ZipOutputStream r6 = new java.util.zip.ZipOutputStream     // Catch:{ Exception -> 0x0044 }
            java.io.BufferedOutputStream r5 = new java.io.BufferedOutputStream     // Catch:{ Exception -> 0x0044 }
            java.io.FileOutputStream r7 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0044 }
            r7.<init>(r3)     // Catch:{ Exception -> 0x0044 }
            r5.<init>(r7)     // Catch:{ Exception -> 0x0044 }
            r6.<init>(r5)     // Catch:{ Exception -> 0x0044 }
            if (r30 != 0) goto L_0x02cf
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x06d7 }
            r5 = 8
            if (r3 >= r5) goto L_0x03d9
            com.avast.android.generic.util.al r30 = com.avast.android.generic.util.al.AMEND     // Catch:{ Exception -> 0x06d7 }
        L_0x02cf:
            if (r31 == 0) goto L_0x0320
            if (r12 == 0) goto L_0x02f7
            java.util.zip.ZipEntry r3 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r5 = "system/etc/custom_backup_list.txt"
            r3.<init>(r5)     // Catch:{ Exception -> 0x06d7 }
            r6.putNextEntry(r3)     // Catch:{ Exception -> 0x06d7 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06d7 }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "UTF-8"
            byte[] r7 = r12.getBytes(r7)     // Catch:{ Exception -> 0x06d7 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x06d7 }
            r5.<init>(r3)     // Catch:{ Exception -> 0x06d7 }
            a(r5, r6)     // Catch:{ Exception -> 0x06db }
            r5.close()     // Catch:{ Exception -> 0x06db }
            r8 = 0
            r6.closeEntry()     // Catch:{ Exception -> 0x06d7 }
        L_0x02f7:
            r5 = r8
            if (r10 == 0) goto L_0x0713
            if (r11 == 0) goto L_0x0713
            java.util.zip.ZipEntry r3 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06e0 }
            java.lang.String r7 = "system/addon.d/99-cm.sh"
            r3.<init>(r7)     // Catch:{ Exception -> 0x06e0 }
            r6.putNextEntry(r3)     // Catch:{ Exception -> 0x06e0 }
            java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06e0 }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x06e0 }
            java.lang.String r7 = "UTF-8"
            byte[] r7 = r11.getBytes(r7)     // Catch:{ Exception -> 0x06e0 }
            r3.<init>(r7)     // Catch:{ Exception -> 0x06e0 }
            r8.<init>(r3)     // Catch:{ Exception -> 0x06e0 }
            a(r8, r6)     // Catch:{ Exception -> 0x06d7 }
            r8.close()     // Catch:{ Exception -> 0x06d7 }
            r8 = 0
            r6.closeEntry()     // Catch:{ Exception -> 0x06d7 }
        L_0x0320:
            com.avast.android.generic.util.al r3 = com.avast.android.generic.util.al.AMEND     // Catch:{ Exception -> 0x06d7 }
            r0 = r30
            if (r0 != r3) goto L_0x03e9
            if (r4 == 0) goto L_0x03dd
            java.lang.String r3 = "delete SYSTEM:app/com.avast.android.antitheft.odex\n\r"
        L_0x032a:
            java.util.zip.ZipEntry r5 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "META-INF/com/google/android/update-script"
            r5.<init>(r7)     // Catch:{ Exception -> 0x06d7 }
            r6.putNextEntry(r5)     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r5.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "show_progress 0.1 0\n\r"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            if (r14 == 0) goto L_0x03e1
        L_0x0341:
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r5 = "copy_dir PACKAGE:system SYSTEM:\n\r"
            java.lang.StringBuilder r5 = r3.append(r5)     // Catch:{ Exception -> 0x06d7 }
            if (r4 == 0) goto L_0x03e5
            java.lang.String r3 = "set_perm_recursive 0 0 0755 0644 SYSTEM:app\n\r"
        L_0x034f:
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
            if (r31 == 0) goto L_0x0385
            if (r12 == 0) goto L_0x036e
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "set_perm 0 0 0755 0644 SYSTEM:etc/custom_backup_list.txt\n\r"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
        L_0x036e:
            if (r10 == 0) goto L_0x0385
            if (r11 == 0) goto L_0x0385
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "set_perm 0 0 0755 0644 SYSTEM:addon.d/99-cm.sh\n\r"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
        L_0x0385:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "show_progress 0.1 10\n\r"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
            r4 = r8
        L_0x0399:
            byte[] r3 = r3.getBytes()     // Catch:{ Exception -> 0x06ea }
            r6.write(r3)     // Catch:{ Exception -> 0x06ea }
            r6.closeEntry()     // Catch:{ Exception -> 0x06ea }
            java.util.zip.ZipEntry r3 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06ea }
            r5 = 1
            r0 = r22
            java.lang.String r5 = r0.substring(r5)     // Catch:{ Exception -> 0x06ea }
            r3.<init>(r5)     // Catch:{ Exception -> 0x06ea }
            r6.putNextEntry(r3)     // Catch:{ Exception -> 0x06ea }
            java.io.BufferedInputStream r8 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06ea }
            java.io.FileInputStream r3 = r20.openFileInput(r21)     // Catch:{ Exception -> 0x06ea }
            r8.<init>(r3)     // Catch:{ Exception -> 0x06ea }
            a(r8, r6)     // Catch:{ Exception -> 0x06d7 }
            r8.close()     // Catch:{ Exception -> 0x06d7 }
            r8 = 0
            r6.closeEntry()     // Catch:{ Exception -> 0x06d7 }
            r6.flush()     // Catch:{ Exception -> 0x06d7 }
            r6.close()     // Catch:{ Exception -> 0x06d7 }
            r4 = 0
            java.lang.String r3 = com.avast.android.generic.util.ag.f1176a     // Catch:{ Exception -> 0x06ef }
            r5 = 0
            r0 = r20
            r1 = r24
            r2 = r25
            com.avast.android.generic.d.d.a(r0, r1, r2, r3, r5)     // Catch:{ Exception -> 0x06ef }
        L_0x03d8:
            return
        L_0x03d9:
            com.avast.android.generic.util.al r30 = com.avast.android.generic.util.al.EDIFY     // Catch:{ Exception -> 0x06d7 }
            goto L_0x02cf
        L_0x03dd:
            java.lang.String r3 = "delete SYSTEM:priv-app/com.avast.android.antitheft.odex\n\r"
            goto L_0x032a
        L_0x03e1:
            java.lang.String r3 = ""
            goto L_0x0341
        L_0x03e5:
            java.lang.String r3 = "set_perm_recursive 0 0 0755 0644 SYSTEM:priv-app\n\r"
            goto L_0x034f
        L_0x03e9:
            if (r4 == 0) goto L_0x04e3
            java.lang.String r3 = "delete(\"/system/app/com.avast.android.antitheft.odex\");\n"
        L_0x03ed:
            java.util.zip.ZipEntry r5 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "META-INF/com/google/android/update-binary"
            r5.<init>(r7)     // Catch:{ Exception -> 0x06d7 }
            r6.putNextEntry(r5)     // Catch:{ Exception -> 0x06d7 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06d7 }
            android.content.res.Resources r7 = r20.getResources()     // Catch:{ Exception -> 0x06d7 }
            r0 = r28
            java.io.InputStream r7 = r7.openRawResource(r0)     // Catch:{ Exception -> 0x06d7 }
            r5.<init>(r7)     // Catch:{ Exception -> 0x06d7 }
            a(r5, r6)     // Catch:{ Exception -> 0x06e5 }
            r5.close()     // Catch:{ Exception -> 0x06e5 }
            r8 = 0
            r6.closeEntry()     // Catch:{ Exception -> 0x06d7 }
            java.util.zip.ZipEntry r5 = new java.util.zip.ZipEntry     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "META-INF/com/google/android/updater-script"
            r5.<init>(r7)     // Catch:{ Exception -> 0x06d7 }
            r6.putNextEntry(r5)     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r5.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "ui_print(\"Avast! Anti-Theft Edify Update Script\");\nshow_progress(1.000000, 0);\nui_print(\"Performing Update...\");\nui_print(\"Mounting mount points...\");\nifelse(is_mounted(\"/system\"), (ui_print(\"System partition is already mounted\");), (ifelse(run_program(\"/sbin/mount\", \""
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r5 = r5.append(r13)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "\", \"/system\") == \"0\", (ui_print(\"Mounted via sbin/mount\");), ("
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "ifelse(run_program(\"/sbin/busybox\", \"mount\", \""
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r5 = r5.append(r13)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "\", \"/system\") == \"0\", (ui_print(\"Mounted via sbin/busybox\");), ("
            java.lang.StringBuilder r7 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            com.avast.android.generic.util.u r5 = com.avast.android.generic.util.u.ICS     // Catch:{ Exception -> 0x06d7 }
            r0 = r29
            if (r0 == r5) goto L_0x044b
            com.avast.android.generic.util.u r5 = com.avast.android.generic.util.u.JB     // Catch:{ Exception -> 0x06d7 }
            r0 = r29
            if (r0 != r5) goto L_0x04e7
        L_0x044b:
            java.lang.String r5 = "ifelse(mount(\"yaffs2\", \"MTD\", \"system\", \"/system\") == \"system\", (ui_print(\"Mounted via mount command\");), ("
        L_0x044d:
            java.lang.StringBuilder r5 = r7.append(r5)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "ui_print(\"Can not mount "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r5 = r5.append(r13)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = " to /system, aborting process\"); "
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "abort();"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "))"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = ";))"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = ";))"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = ";));\n"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r7 = "set_progress(0.500000);\n"
            java.lang.StringBuilder r5 = r5.append(r7)     // Catch:{ Exception -> 0x06d7 }
            if (r14 == 0) goto L_0x04eb
        L_0x0487:
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r5 = "package_extract_dir(\"system\", \"/system\");\n"
            java.lang.StringBuilder r5 = r3.append(r5)     // Catch:{ Exception -> 0x06d7 }
            if (r4 == 0) goto L_0x04ee
            java.lang.String r3 = "set_perm_recursive(0, 0, 0755, 0644, \"/system/app\");\n"
        L_0x0495:
            java.lang.StringBuilder r3 = r5.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
            if (r31 == 0) goto L_0x04cd
            if (r31 == 0) goto L_0x04cd
            if (r12 == 0) goto L_0x04b6
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "set_perm(0, 0, 0755, 0644, \"/system/etc/custom_backup_list.txt\");\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
        L_0x04b6:
            if (r10 == 0) goto L_0x04cd
            if (r11 == 0) goto L_0x04cd
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "set_perm(0, 0, 0755, 0644, \"/system/addon.d/99-cm.sh\");\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
        L_0x04cd:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06d7 }
            r4.<init>()     // Catch:{ Exception -> 0x06d7 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r4 = "ui_print(\"Unmounting mount points...\");\nrun_program(\"/sbin/umount\", \"/system\");\nrun_program(\"/sbin/busybox\", \"umount\", \"/system\");\nunmount(\"/system\");\nui_print(\"Update Complete.\");\nset_progress(1.000000);\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x06d7 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x06d7 }
            r4 = r8
            goto L_0x0399
        L_0x04e3:
            java.lang.String r3 = "delete(\"/system/priv-app/com.avast.android.antitheft.odex\");\n"
            goto L_0x03ed
        L_0x04e7:
            java.lang.String r5 = "ifelse(mount(\"MTD\", \"system\", \"/system\") == \"system\", (ui_print(\"Mounted via mount command\");), ("
            goto L_0x044d
        L_0x04eb:
            java.lang.String r3 = ""
            goto L_0x0487
        L_0x04ee:
            java.lang.String r3 = "set_perm_recursive(0, 0, 0755, 0644, \"/system/priv-app\");\n"
            goto L_0x0495
        L_0x04f1:
            r5 = 0
            r3 = 0
            if (r31 == 0) goto L_0x070e
            if (r12 == 0) goto L_0x070a
            java.io.File r5 = r20.getCacheDir()     // Catch:{ Exception -> 0x06f2 }
            java.lang.String r6 = "bfp"
            java.lang.String r7 = "bfp"
            java.io.File r7 = java.io.File.createTempFile(r6, r7, r5)     // Catch:{ Exception -> 0x06f2 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06f2 }
            java.io.ByteArrayInputStream r6 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x06f2 }
            java.lang.String r15 = "UTF-8"
            byte[] r12 = r12.getBytes(r15)     // Catch:{ Exception -> 0x06f2 }
            r6.<init>(r12)     // Catch:{ Exception -> 0x06f2 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x06f2 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x06f5 }
            r6.<init>(r7)     // Catch:{ Exception -> 0x06f5 }
            a(r5, r6)     // Catch:{ Exception -> 0x06f5 }
            r5.close()     // Catch:{ Exception -> 0x06f5 }
            r6 = 0
        L_0x051f:
            if (r10 == 0) goto L_0x0706
            if (r11 == 0) goto L_0x0706
            java.io.File r3 = r20.getCacheDir()     // Catch:{ Exception -> 0x06f8 }
            java.lang.String r5 = "cfp"
            java.lang.String r8 = "cfp"
            java.io.File r3 = java.io.File.createTempFile(r5, r8, r3)     // Catch:{ Exception -> 0x06f8 }
            java.io.BufferedInputStream r5 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x06f8 }
            java.io.ByteArrayInputStream r8 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x06f8 }
            java.lang.String r10 = "UTF-8"
            byte[] r10 = r11.getBytes(r10)     // Catch:{ Exception -> 0x06f8 }
            r8.<init>(r10)     // Catch:{ Exception -> 0x06f8 }
            r5.<init>(r8)     // Catch:{ Exception -> 0x06f8 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x06fb }
            r6.<init>(r3)     // Catch:{ Exception -> 0x06fb }
            a(r5, r6)     // Catch:{ Exception -> 0x06fb }
            r5.close()     // Catch:{ Exception -> 0x06fb }
            r6 = 0
            r5 = r6
            r6 = r3
        L_0x054d:
            java.io.File r3 = r20.getFilesDir()     // Catch:{ Exception -> 0x0698 }
            java.lang.String r8 = r3.getAbsolutePath()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r3.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.String r10 = "mount -o rw,remount "
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r10 = " /system\n"
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            if (r31 == 0) goto L_0x05dc
            if (r7 == 0) goto L_0x05a6
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r10.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r10.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r10 = "cat "
            java.lang.StringBuilder r3 = r3.append(r10)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r7 = r7.getAbsolutePath()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r7 = " > /system/etc/custom_backup_list.txt\n"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r7.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r7 = "chmod 644 /system/etc/custom_backup_list.txt\n"
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
        L_0x05a6:
            if (r6 == 0) goto L_0x05dc
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r7.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r7 = "cat "
            java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r6 = r6.getAbsolutePath()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r6 = " > /system/addon.d/99-cm.sh\n"
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r6.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r6 = "chmod 644 /system/addon.d/99-cm.sh\n"
            java.lang.StringBuilder r3 = r3.append(r6)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
        L_0x05dc:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r6.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r6 = r6.append(r3)     // Catch:{ Exception -> 0x0698 }
            if (r14 == 0) goto L_0x06ad
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r3.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.String r7 = "rm "
            java.lang.StringBuilder r7 = r3.append(r7)     // Catch:{ Exception -> 0x0698 }
            if (r4 == 0) goto L_0x06a9
            java.lang.String r3 = "/system/app/com.avast.android.antitheft.odex"
        L_0x05f6:
            java.lang.StringBuilder r3 = r7.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
        L_0x0604:
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r4.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "cat "
            java.lang.StringBuilder r4 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            if (r26 == 0) goto L_0x06b1
        L_0x061d:
            r0 = r21
            java.lang.StringBuilder r3 = r4.append(r0)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = " > "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            r0 = r22
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r4.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "chmod 644 "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            r0 = r22
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r4.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "mount -o ro,remount "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r3.append(r13)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = " /system"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            if (r27 == 0) goto L_0x068a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r4.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r3 = r4.append(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r4 = "\nam start -a android.activity.MAIN -n com.avast.android.antitheft/com.avast.android.antitheft.app.AlwaysAvailableStartupActivity --ez updated true"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0698 }
        L_0x068a:
            com.avast.android.generic.util.o.a(r3)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = "AvastGeneric"
            java.lang.String r4 = "Copy succeeded"
            r0 = r20
            com.avast.android.generic.util.z.a(r3, r0, r4)     // Catch:{ Exception -> 0x0698 }
            goto L_0x03d8
        L_0x0698:
            r3 = move-exception
            r4 = r5
        L_0x069a:
            java.lang.String r5 = "AvastGeneric"
            java.lang.String r6 = "SU mode error"
            r0 = r20
            com.avast.android.generic.util.z.a(r5, r0, r6, r3)     // Catch:{ Exception -> 0x06a4 }
            throw r3     // Catch:{ Exception -> 0x06a4 }
        L_0x06a4:
            r3 = move-exception
            r8 = r4
            r4 = r9
            goto L_0x0046
        L_0x06a9:
            java.lang.String r3 = "/system/priv-app/com.avast.android.antitheft.odex"
            goto L_0x05f6
        L_0x06ad:
            java.lang.String r3 = ""
            goto L_0x0604
        L_0x06b1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0698 }
            r3.<init>()     // Catch:{ Exception -> 0x0698 }
            java.lang.StringBuilder r6 = r3.append(r8)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r3 = "/"
            boolean r3 = r8.endsWith(r3)     // Catch:{ Exception -> 0x0698 }
            if (r3 == 0) goto L_0x06d4
            java.lang.String r3 = ""
        L_0x06c4:
            java.lang.StringBuilder r3 = r6.append(r3)     // Catch:{ Exception -> 0x0698 }
            r0 = r21
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x0698 }
            java.lang.String r21 = r3.toString()     // Catch:{ Exception -> 0x0698 }
            goto L_0x061d
        L_0x06d4:
            java.lang.String r3 = "/"
            goto L_0x06c4
        L_0x06d7:
            r3 = move-exception
            r4 = r6
            goto L_0x0046
        L_0x06db:
            r3 = move-exception
            r8 = r5
            r4 = r6
            goto L_0x0046
        L_0x06e0:
            r3 = move-exception
            r8 = r5
            r4 = r6
            goto L_0x0046
        L_0x06e5:
            r3 = move-exception
            r8 = r5
            r4 = r6
            goto L_0x0046
        L_0x06ea:
            r3 = move-exception
            r8 = r4
            r4 = r6
            goto L_0x0046
        L_0x06ef:
            r3 = move-exception
            goto L_0x0046
        L_0x06f2:
            r3 = move-exception
            r4 = r8
            goto L_0x069a
        L_0x06f5:
            r3 = move-exception
            r4 = r5
            goto L_0x069a
        L_0x06f8:
            r3 = move-exception
            r4 = r6
            goto L_0x069a
        L_0x06fb:
            r3 = move-exception
            r4 = r5
            goto L_0x069a
        L_0x06fe:
            r3 = move-exception
            r4 = r6
            goto L_0x026b
        L_0x0702:
            r3 = move-exception
            r4 = r6
            goto L_0x01ec
        L_0x0706:
            r5 = r6
            r6 = r3
            goto L_0x054d
        L_0x070a:
            r7 = r5
            r6 = r8
            goto L_0x051f
        L_0x070e:
            r6 = r3
            r7 = r5
            r5 = r8
            goto L_0x054d
        L_0x0713:
            r8 = r5
            goto L_0x0320
        L_0x0716:
            r12 = r11
            r11 = r10
            r10 = r7
            goto L_0x02aa
        L_0x071b:
            r12 = r11
            r11 = r10
            r10 = r3
            goto L_0x02aa
        L_0x0720:
            r13 = r5
            goto L_0x017f
        L_0x0723:
            r13 = r6
            goto L_0x017f
        L_0x0726:
            r13 = r7
            goto L_0x017f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.util.ag.a(android.content.Context, java.lang.String, java.lang.String, boolean, int, int, boolean, boolean, int, com.avast.android.generic.util.u, com.avast.android.generic.util.al, boolean):void");
    }

    private static void a(Context context, String str, String str2, String str3) {
        try {
            StatFs statFs = new StatFs(str2);
            long availableBlocks = ((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize());
            if (availableBlocks >= 0) {
                File file = new File(context.getFilesDir().getAbsolutePath() + "/" + str);
                if (!file.exists() || !file.canRead()) {
                    throw new Exception("File does not exist or cannot be read");
                }
                File file2 = new File(str3);
                if (file2.exists()) {
                    availableBlocks += file2.length();
                }
                long length = file.length() + 102400;
                if (availableBlocks < length) {
                    throw new a(availableBlocks, length);
                }
            }
        } catch (Exception e) {
            z.a("AvastGeneric", "Can not query free space", e);
        }
    }

    public static void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[2048];
        while (true) {
            int read = inputStream.read(bArr, 0, 2048);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0034 A[SYNTHETIC, Splitter:B:14:0x0034] */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[Catch:{ Exception -> 0x0031 }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r5) {
        /*
            r0 = 0
            java.io.File r3 = new java.io.File
            java.lang.String r1 = "/system/build.prop"
            r3.<init>(r1)
            boolean r1 = r3.exists()
            if (r1 == 0) goto L_0x002c
            r2 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x003a }
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Exception -> 0x003a }
            r4.<init>(r3)     // Catch:{ Exception -> 0x003a }
            r1.<init>(r4)     // Catch:{ Exception -> 0x003a }
        L_0x0019:
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0031 }
            if (r2 == 0) goto L_0x002d
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ Exception -> 0x0031 }
            java.lang.String r3 = "ro.modversion"
            boolean r2 = r2.startsWith(r3)     // Catch:{ Exception -> 0x0031 }
            if (r2 == 0) goto L_0x0019
            r0 = 1
        L_0x002c:
            return r0
        L_0x002d:
            r1.close()     // Catch:{ Exception -> 0x0031 }
            goto L_0x002c
        L_0x0031:
            r2 = move-exception
        L_0x0032:
            if (r1 == 0) goto L_0x002c
            r1.close()     // Catch:{ Exception -> 0x0038 }
            goto L_0x002c
        L_0x0038:
            r1 = move-exception
            goto L_0x002c
        L_0x003a:
            r1 = move-exception
            r1 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.util.ag.a(android.content.Context):boolean");
    }
}
