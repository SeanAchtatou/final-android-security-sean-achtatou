package com.avast.android.generic.ui.rtl;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.avast.android.generic.ui.rtl.a.c;
import com.avast.android.generic.util.ak;

public class TextViewLtrToRtlConverter extends a {
    public TextViewLtrToRtlConverter(String str) {
        super(str);
    }

    public View ltrToRtlView(View view) {
        if (view instanceof TextView) {
            return ltrToRtlIfTextView((TextView) view);
        }
        throw new IllegalArgumentException("The view must be an instance of TextView and it's " + view.getClass() + ".");
    }

    private View ltrToRtlIfTextView(TextView textView) {
        if (ak.a()) {
            return textView;
        }
        ViewGroup viewGroup = (ViewGroup) textView.getParent();
        if (viewGroup != null) {
            int childCount = viewGroup.getChildCount();
            int i = 0;
            while (i < childCount) {
                if (viewGroup.getChildAt(i) == textView) {
                    Class<?> cls = textView.getClass();
                    if (cls.equals(Button.class)) {
                        viewGroup.removeView(textView);
                        b bVar = new b(textView, this.mCurrentLang);
                        viewGroup.addView(bVar, i);
                        return bVar;
                    } else if (cls.equals(TextView.class)) {
                        viewGroup.removeView(textView);
                        e eVar = new e(textView, this.mCurrentLang);
                        viewGroup.addView(eVar, i);
                        return eVar;
                    } else {
                        ltrToRtlTextViewSubclass(textView);
                        return textView;
                    }
                } else {
                    i++;
                }
            }
            return textView;
        }
        Class<?> cls2 = textView.getClass();
        if (cls2.equals(Button.class)) {
            return new b(textView, this.mCurrentLang);
        }
        if (cls2.equals(TextView.class)) {
            return new e(textView, this.mCurrentLang);
        }
        ltrToRtlTextViewSubclass(textView);
        return textView;
    }

    private void ltrToRtlTextViewSubclass(TextView textView) {
        int i;
        if (this.mCurrentLang != null && this.mCurrentLang.equals(a.LANG_ARABIC)) {
            textView.setText(c.c(textView.getText().toString()));
        }
        int gravity = textView.getGravity();
        if ((gravity & 3) == 3) {
            i = (gravity ^ 3) | 5;
        } else {
            i = (gravity & 5) == 5 ? (gravity ^ 5) | 3 : gravity;
        }
        if (i != gravity) {
            textView.setGravity(i);
        }
    }
}
