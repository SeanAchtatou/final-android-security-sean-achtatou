package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import java.util.regex.Pattern;

public class PasswordTextView extends EditText {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Pattern f1095a = Pattern.compile("\\**[^\\*]+\\**");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Pattern f1096b = Pattern.compile(".*\\*+.*");

    /* renamed from: c  reason: collision with root package name */
    private o f1097c;
    /* access modifiers changed from: private */
    public p d;
    /* access modifiers changed from: private */
    public n e;
    private l f;

    public PasswordTextView(Context context) {
        super(context);
        d();
    }

    public PasswordTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d();
    }

    public PasswordTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        d();
    }

    private void d() {
        setTypeface(Typeface.MONOSPACE);
        setInputType(3);
        addTextChangedListener(this.f1097c);
    }

    public void setFilters(InputFilter[] inputFilterArr) {
        InputFilter[] inputFilterArr2;
        if (inputFilterArr == null) {
            throw new IllegalArgumentException();
        }
        if (this.e == null) {
            this.e = new n(this);
        }
        if (this.d == null) {
            this.d = new p();
        }
        if (this.f1097c == null) {
            this.f1097c = new o(this);
        }
        if (this.f == null) {
            this.f = l.DIGITS;
        }
        if (inputFilterArr.length == 0) {
            inputFilterArr2 = new InputFilter[]{new m(this, this.f), this.d};
        } else {
            inputFilterArr2 = new InputFilter[(inputFilterArr.length + 2)];
            System.arraycopy(inputFilterArr, 0, inputFilterArr2, 0, inputFilterArr.length);
            inputFilterArr2[inputFilterArr.length] = new m(this, this.f);
            inputFilterArr2[inputFilterArr.length + 1] = this.d;
        }
        super.setFilters(inputFilterArr2);
    }

    public String a() {
        return this.d.f1155a.toString();
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.d.f1155a.append(savedState.f1098a);
        char[] cArr = new char[savedState.f1098a.length()];
        for (int i = 0; i < cArr.length; i++) {
            cArr[i] = '*';
        }
        setText(String.valueOf(cArr));
        setSelection(savedState.f1099b);
    }

    public Parcelable onSaveInstanceState() {
        String sb = this.d.f1155a.toString();
        int selectionStart = getSelectionStart();
        setText("");
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1098a = sb;
        savedState.f1099b = selectionStart;
        return savedState;
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new q();

        /* renamed from: a  reason: collision with root package name */
        String f1098a;

        /* renamed from: b  reason: collision with root package name */
        int f1099b;

        public SavedState(Parcel parcel) {
            super(parcel);
            this.f1098a = parcel.readString();
            this.f1099b = parcel.readInt();
        }

        public SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeString(this.f1098a);
            parcel.writeInt(this.f1099b);
        }
    }
}
