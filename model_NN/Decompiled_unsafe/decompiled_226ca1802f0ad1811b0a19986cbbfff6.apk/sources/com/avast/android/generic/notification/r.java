package com.avast.android.generic.notification;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import com.avast.android.generic.aa;
import com.avast.android.generic.b;

/* compiled from: NotificationsCursorLoader */
public class r extends b<Cursor> {
    private h f;
    private s g;

    public r(Context context, s sVar) {
        super(context);
        if (sVar == null) {
            throw new IllegalArgumentException("Type can't be null.");
        }
        this.f = (h) aa.a(context, h.class);
        this.g = sVar;
    }

    /* renamed from: f */
    public Cursor d() {
        if (this.g.equals(s.ONGOING)) {
            return w();
        }
        if (this.g.equals(s.TEMPORARY)) {
            return x();
        }
        return y();
    }

    private Cursor w() {
        MatrixCursor y = y();
        for (a next : this.f.d()) {
            if ((next.f & 2) > 0) {
                Object[] objArr = new Object[23];
                objArr[0] = Integer.valueOf(y.getCount() * -1);
                objArr[1] = Long.valueOf(next.f920a);
                objArr[2] = next.f921b;
                objArr[3] = next.l;
                objArr[4] = next.k;
                objArr[5] = Integer.valueOf(next.f);
                objArr[6] = Integer.valueOf(next.e);
                objArr[7] = 1;
                objArr[8] = next.g != null ? next.g.f917a : null;
                objArr[9] = next.g != null ? next.g.f919c : null;
                objArr[10] = next.g != null ? next.g.f918b != null ? next.g.f918b.toString() : null : null;
                objArr[11] = next.g != null ? next.g.b() : null;
                objArr[12] = next.g != null ? Integer.valueOf(next.g.d) : null;
                objArr[13] = next.g != null ? next.g.f.name() : null;
                objArr[14] = next.h != null ? next.h.f917a : null;
                objArr[15] = next.h != null ? next.h.f919c : null;
                objArr[16] = next.h != null ? next.h.f918b != null ? next.g.f918b.toString() : null : null;
                objArr[17] = next.h != null ? next.h.b() : null;
                objArr[18] = next.h != null ? Integer.valueOf(next.h.d) : null;
                objArr[19] = next.h != null ? next.h.f.name() : null;
                objArr[20] = Integer.valueOf(next.i);
                objArr[21] = Float.valueOf((((float) next.d) / ((float) next.f922c)) * 100.0f);
                objArr[22] = Long.valueOf(next.j);
                y.addRow(objArr);
            }
        }
        return y;
    }

    private Cursor x() {
        int size = this.f.d().size();
        MatrixCursor y = y();
        for (a next : this.f.e()) {
            Object[] objArr = new Object[23];
            objArr[0] = Integer.valueOf((y.getCount() * -1) - size);
            objArr[1] = Long.valueOf(next.f920a);
            objArr[2] = next.f921b;
            objArr[3] = next.l;
            objArr[4] = next.k;
            objArr[5] = Integer.valueOf(next.f);
            objArr[6] = Integer.valueOf(next.e);
            objArr[7] = 0;
            objArr[8] = next.g != null ? next.g.f917a : null;
            objArr[9] = next.g != null ? next.g.f919c : null;
            objArr[10] = next.g != null ? next.g.f918b != null ? next.g.f918b.toString() : null : null;
            objArr[11] = next.g != null ? next.g.b() : null;
            objArr[12] = next.g != null ? Integer.valueOf(next.g.d) : null;
            objArr[13] = next.g != null ? next.g.f.name() : null;
            objArr[14] = next.h != null ? next.h.f917a : null;
            objArr[15] = next.h != null ? next.h.f919c : null;
            objArr[16] = next.h != null ? next.h.f918b != null ? next.g.f918b.toString() : null : null;
            objArr[17] = next.h != null ? next.h.b() : null;
            objArr[18] = next.h != null ? Integer.valueOf(next.h.d) : null;
            objArr[19] = next.h != null ? next.h.f.name() : null;
            objArr[20] = Integer.valueOf(next.i);
            objArr[21] = Float.valueOf((((float) next.d) / ((float) next.f922c)) * 100.0f);
            objArr[22] = Long.valueOf(next.j);
            y.addRow(objArr);
        }
        return y;
    }

    private MatrixCursor y() {
        return new MatrixCursor(new String[]{"_id", "notification_id", "notification_tag", "contentText", "contentTitle", "flags", "number", "ongoing", "pendingIntentAction", "pendingIntentClass", "pendingIntentData", "pendingIntentExtras", "pendingIntentFlags", "pendingIntentType", "deleteIntentAction", "deleteIntentClass", "deleteIntentData", "deleteIntentExtras", "deleteIntentFlags", "deleteIntentType", "priority", "percentage", "timestamp"});
    }
}
