package com.avast.android.generic.internet;

import android.os.Bundle;
import com.avast.b.a.a.ak;

/* compiled from: HttpInfo */
public class e {

    /* renamed from: a  reason: collision with root package name */
    private ak f816a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f817b;

    /* renamed from: c  reason: collision with root package name */
    private k f818c;
    private Bundle d;

    public e(ak akVar, boolean z) {
        this(akVar, z, null);
    }

    public e(ak akVar, boolean z, k kVar) {
        a(akVar);
        a(z);
        a(kVar);
    }

    public void a(boolean z) {
        this.f817b = z;
    }

    public boolean a() {
        return this.f817b;
    }

    public void a(k kVar) {
        this.f818c = kVar;
    }

    public k b() {
        return this.f818c;
    }

    public void a(ak akVar) {
        this.f816a = akVar;
    }

    public ak c() {
        return this.f816a;
    }

    public Bundle d() {
        return this.d;
    }
}
