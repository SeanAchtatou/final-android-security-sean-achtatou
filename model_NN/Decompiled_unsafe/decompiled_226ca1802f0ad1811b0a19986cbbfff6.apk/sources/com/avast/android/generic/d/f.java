package com.avast.android.generic.d;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.cert.CRL;
import java.security.cert.Certificate;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.crypto.Cipher;
import org.bouncycastle.asn1.ASN1Encodable;
import org.bouncycastle.asn1.ASN1EncodableVector;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERInteger;
import org.bouncycastle.asn1.DERObject;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.DEROutputStream;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.DERTaggedObject;
import org.bouncycastle.asn1.pkcs.IssuerAndSerialNumber;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.DigestInfo;
import org.bouncycastle.asn1.x509.X509Name;

/* compiled from: PKCS7SignedData */
public class f implements e, PKCSObjectIdentifiers {

    /* renamed from: a  reason: collision with root package name */
    private int f657a;

    /* renamed from: b  reason: collision with root package name */
    private int f658b;

    /* renamed from: c  reason: collision with root package name */
    private Set f659c;
    private Collection d;
    private Collection e;
    private X509Certificate f;
    private byte[] g;
    private String h;
    private String i;
    private Signature j;
    private transient PrivateKey k;
    private final String l;
    private final String m;
    private final String n;
    private final String o;
    private final String p;
    private final String q;
    private final String r;

    public f(PrivateKey privateKey, Certificate[] certificateArr, String str) {
        this(privateKey, certificateArr, str, "BC");
    }

    public f(PrivateKey privateKey, Certificate[] certificateArr, String str, String str2) {
        this(privateKey, certificateArr, null, str, str2);
    }

    public f(PrivateKey privateKey, Certificate[] certificateArr, CRL[] crlArr, String str, String str2) {
        this.l = "1.2.840.113549.1.7.1";
        this.m = "1.2.840.113549.1.7.2";
        this.n = "1.2.840.113549.2.5";
        this.o = "1.2.840.113549.2.2";
        this.p = "1.3.14.3.2.26";
        this.q = "1.2.840.113549.1.1.1";
        this.r = "1.2.840.10040.4.1";
        this.k = privateKey;
        if (str.equals("MD5")) {
            this.h = "1.2.840.113549.2.5";
        } else if (str.equals("MD2")) {
            this.h = "1.2.840.113549.2.2";
        } else if (str.equals("SHA")) {
            this.h = "1.3.14.3.2.26";
        } else if (str.equals("SHA1")) {
            this.h = "1.3.14.3.2.26";
        } else {
            throw new NoSuchAlgorithmException("Unknown Hash Algorithm " + str);
        }
        this.f658b = 1;
        this.f657a = 1;
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.f659c = new HashSet();
        this.f659c.add(this.h);
        this.f = (X509Certificate) certificateArr[0];
        for (Certificate add : certificateArr) {
            this.d.add(add);
        }
        if (crlArr != null) {
            for (CRL add2 : crlArr) {
                this.e.add(add2);
            }
        }
        this.i = privateKey.getAlgorithm();
        if (this.i.equals("RSA")) {
            this.i = "1.2.840.113549.1.1.1";
        } else if (this.i.equals("DSA")) {
            this.i = "1.2.840.10040.4.1";
        } else {
            throw new NoSuchAlgorithmException("Unknown Key Algorithm " + this.i);
        }
        this.j = Signature.getInstance(a(), str2);
        this.j.initSign(privateKey);
    }

    public String a() {
        String str = this.h;
        String str2 = this.i;
        if (this.h.equals("1.2.840.113549.2.5")) {
            str = "MD5";
        } else if (this.h.equals("1.2.840.113549.2.2")) {
            str = "MD2";
        } else if (this.h.equals("1.3.14.3.2.26")) {
            str = "SHA1";
        }
        if (this.i.equals("1.2.840.113549.1.1.1")) {
            str2 = "RSA";
        } else if (this.i.equals("1.2.840.10040.4.1")) {
            str2 = "DSA";
        }
        return str + "with" + str2;
    }

    private DERObject a(byte[] bArr) {
        try {
            ASN1Sequence readObject = new ASN1InputStream(new ByteArrayInputStream(bArr)).readObject();
            return readObject.getObjectAt(readObject.getObjectAt(0) instanceof DERTaggedObject ? 3 : 2);
        } catch (IOException e2) {
            throw new Error("IOException reading from ByteArray: " + e2);
        }
    }

    public byte[] a(String str) {
        Class cls;
        Field field;
        try {
            try {
                field = Class.forName("org.bouncycastle.asn1.DERNull").getField("THE_ONE");
            } catch (NoSuchFieldException e2) {
                field = cls.getField("INSTANCE");
            }
            ASN1Encodable aSN1Encodable = (ASN1Encodable) field.get(null);
            DigestInfo digestInfo = new DigestInfo(new AlgorithmIdentifier(new DERObjectIdentifier("1.3.14.3.2.26"), aSN1Encodable), MessageDigest.getInstance("SHA-1").digest(str.getBytes("UTF-8")));
            Cipher instance = Cipher.getInstance("RSA/NONE/PKCS1Padding");
            instance.init(1, this.k);
            this.g = instance.doFinal(digestInfo.getEncoded("DER"));
            ASN1EncodableVector aSN1EncodableVector = new ASN1EncodableVector();
            for (String dERObjectIdentifier : this.f659c) {
                aSN1EncodableVector.add(new AlgorithmIdentifier(new DERObjectIdentifier(dERObjectIdentifier), aSN1Encodable));
            }
            DERSet dERSet = new DERSet(aSN1EncodableVector);
            DERSequence dERSequence = new DERSequence(new DERObjectIdentifier("1.2.840.113549.1.7.1"));
            ASN1EncodableVector aSN1EncodableVector2 = new ASN1EncodableVector();
            for (X509Certificate encoded : this.d) {
                aSN1EncodableVector2.add(new ASN1InputStream(new ByteArrayInputStream(encoded.getEncoded())).readObject());
            }
            DERSet dERSet2 = new DERSet(aSN1EncodableVector2);
            ASN1EncodableVector aSN1EncodableVector3 = new ASN1EncodableVector();
            aSN1EncodableVector3.add(new DERInteger(this.f658b));
            aSN1EncodableVector3.add(new IssuerAndSerialNumber(new X509Name(a(this.f.getTBSCertificate())), new DERInteger(this.f.getSerialNumber())));
            aSN1EncodableVector3.add(new AlgorithmIdentifier(new DERObjectIdentifier(this.h), aSN1Encodable));
            aSN1EncodableVector3.add(new AlgorithmIdentifier(new DERObjectIdentifier(this.i), aSN1Encodable));
            aSN1EncodableVector3.add(new DEROctetString(this.g));
            ASN1EncodableVector aSN1EncodableVector4 = new ASN1EncodableVector();
            aSN1EncodableVector4.add(new DERInteger(this.f657a));
            aSN1EncodableVector4.add(dERSet);
            aSN1EncodableVector4.add(dERSequence);
            aSN1EncodableVector4.add(new DERTaggedObject(false, 0, dERSet2));
            if (this.e.size() > 0) {
                ASN1EncodableVector aSN1EncodableVector5 = new ASN1EncodableVector();
                for (X509CRL encoded2 : this.e) {
                    aSN1EncodableVector5.add(new ASN1InputStream(new ByteArrayInputStream(encoded2.getEncoded())).readObject());
                }
                aSN1EncodableVector4.add(new DERTaggedObject(false, 1, new DERSet(aSN1EncodableVector5)));
            }
            aSN1EncodableVector4.add(new DERSet(new DERSequence(aSN1EncodableVector3)));
            ASN1EncodableVector aSN1EncodableVector6 = new ASN1EncodableVector();
            aSN1EncodableVector6.add(new DERObjectIdentifier("1.2.840.113549.1.7.2"));
            aSN1EncodableVector6.add(new DERTaggedObject(0, new DERSequence(aSN1EncodableVector4)));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DEROutputStream dEROutputStream = new DEROutputStream(byteArrayOutputStream);
            dEROutputStream.writeObject(new DERSequence(aSN1EncodableVector6));
            dEROutputStream.close();
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e3) {
            throw new RuntimeException(e3.toString());
        }
    }
}
