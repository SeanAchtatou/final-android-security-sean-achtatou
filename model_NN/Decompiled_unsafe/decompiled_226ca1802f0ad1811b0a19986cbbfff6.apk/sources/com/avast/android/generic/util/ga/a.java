package com.avast.android.generic.util.ga;

import android.app.Activity;
import android.content.Context;
import com.avast.android.generic.util.t;
import com.google.analytics.tracking.android.EasyTracker;

/* compiled from: EasyTrackerAdapter */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f1230a;

    /* renamed from: b  reason: collision with root package name */
    private c f1231b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f1232c = true;

    public static c a() {
        a b2 = b();
        if (b2.f1231b != null) {
            return b2.f1231b;
        }
        throw new IllegalStateException("You must call EasyTrackerAdapter.getInstance().setContext(context) or activityStart(Activity) before calling getTracker()");
    }

    public static synchronized a b() {
        a aVar;
        synchronized (a.class) {
            if (f1230a == null) {
                f1230a = new a();
            }
            aVar = f1230a;
        }
        return aVar;
    }

    private a() {
    }

    public void a(Activity activity) {
        a((Context) activity);
        if (this.f1232c) {
            EasyTracker.getInstance().activityStart(activity);
        }
    }

    public void b(Activity activity) {
        if (this.f1232c) {
            EasyTracker.getInstance().activityStop(activity);
        }
    }

    public void a(Context context) {
        EasyTracker.getInstance().setContext(context);
        if (this.f1231b == null) {
            this.f1231b = new c(this, EasyTracker.getTracker());
        }
    }

    public void a(boolean z) {
        t.c("GA tracking enabled: " + z);
        this.f1232c = z;
    }
}
