package com.avast.android.generic.app.account;

import android.content.Context;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.f;
import com.avast.android.generic.util.g;

/* compiled from: ConnectAccountHelper */
public abstract class r {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f449a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public v f450b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public z f451c;

    public abstract void a();

    public abstract void a(z zVar, String str, String str2, String str3, String str4, String str5, String str6, String str7);

    public abstract void a(String str);

    public r(Context context) {
        this.f449a = context;
    }

    public void a(bg bgVar, String str) {
        this.f451c = z.FACEBOOK;
        new s(this, str, bgVar).a(10000);
    }

    public void a(String str, String str2, String str3) {
        this.f451c = z.EMAIL;
        new t(this, str, str2, str3).a(10000);
    }

    public void b() {
    }

    /* access modifiers changed from: protected */
    public void a(z zVar, String str) {
        f fVar;
        switch (u.f458a[zVar.ordinal()]) {
            case 1:
                fVar = f.FACEBOOK;
                break;
            case 2:
                fVar = f.GPLUS;
                break;
            default:
                fVar = f.EMAIL;
                break;
        }
        d.b(this.f449a).a(g.ACCOUNT, fVar, str);
    }
}
