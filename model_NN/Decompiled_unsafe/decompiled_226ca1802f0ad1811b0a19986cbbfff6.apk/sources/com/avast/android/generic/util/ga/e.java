package com.avast.android.generic.util.ga;

import android.content.Context;
import android.text.TextUtils;

/* compiled from: TrackedObject */
public abstract class e {

    /* renamed from: a  reason: collision with root package name */
    private Context f1238a;

    public abstract String e_();

    private static String b(Context context) {
        String str;
        if (context == null) {
            return "unknownContext-advAnalytics";
        }
        String packageName = context.getPackageName();
        if (TextUtils.isEmpty(packageName)) {
            return "unknownContext-advAnalytics";
        }
        if (packageName.equals("com.avast.android.antitheft") || packageName.equals("com.avast.android.at_play")) {
            str = "antiTheft-";
        } else if (packageName.equals("com.avast.android.backup")) {
            str = "backup-";
        } else {
            str = "ms-";
        }
        return str + "advAnalytics";
    }

    public void a(String str) {
        try {
            b(str);
        } catch (Exception e) {
        }
    }

    private void b(String str) {
        try {
            String b2 = b(this.f1238a);
            String e_ = e_();
            String simpleName = getClass().getSimpleName();
            if (!TextUtils.isEmpty(e_)) {
                simpleName = simpleName + "-" + e_;
            }
            if (str != null) {
                r().a(b2, simpleName, "error-" + str, 1L);
            } else {
                r().a(b2, simpleName, "error-unknown", 1L);
            }
        } catch (Exception e) {
        }
    }

    public void q() {
        try {
            b();
        } catch (Exception e) {
        }
    }

    private void b() {
        try {
            String b2 = b(this.f1238a);
            String e_ = e_();
            String simpleName = getClass().getSimpleName();
            if (!TextUtils.isEmpty(e_)) {
                simpleName = simpleName + "-" + e_;
            }
            a.a().a(b2, simpleName, "success", 1L);
        } catch (Exception e) {
        }
    }

    public c r() {
        return a.a();
    }

    public void a(Context context) {
        this.f1238a = context;
    }
}
