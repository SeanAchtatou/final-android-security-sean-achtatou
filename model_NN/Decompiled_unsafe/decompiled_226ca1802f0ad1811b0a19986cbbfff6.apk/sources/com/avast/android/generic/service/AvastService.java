package com.avast.android.generic.service;

import android.app.Service;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import com.avast.android.generic.c.a;
import com.avast.android.generic.c.l;
import com.avast.android.generic.g.a.b;
import com.avast.android.generic.internet.HttpSender;
import com.avast.android.generic.internet.e;
import com.avast.android.generic.j.d;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.bc;
import com.avast.android.generic.util.z;
import com.avast.b.a.a.h;
import java.util.List;

public abstract class AvastService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private Thread f952a = null;

    /* renamed from: b  reason: collision with root package name */
    private String f953b = null;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public b f954c = null;
    /* access modifiers changed from: private */
    public HttpSender d = null;
    /* access modifiers changed from: private */
    public d e = null;
    /* access modifiers changed from: private */
    public l f = null;
    private final Handler g = new Handler();
    private boolean h = false;
    /* access modifiers changed from: private */
    public int i = 0;
    /* access modifiers changed from: private */
    public Object j = new Object();
    private boolean k = false;

    public abstract void a(h hVar);

    /* access modifiers changed from: protected */
    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract void c();

    public abstract int d();

    public abstract void e();

    public abstract void f();

    public abstract void g();

    public void b() {
        a(new a(this));
    }

    /* access modifiers changed from: private */
    public void h() {
        long currentTimeMillis = System.currentTimeMillis();
        z.a("AvastGenericSc", "Destroy syncing...");
        synchronized (this.j) {
            this.k = true;
            stopSelf();
            z.a("AvastGenericSc", "Sync for destroy took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            try {
                c();
            } catch (Exception e2) {
            }
            try {
                if (this.f != null) {
                    this.f.b();
                    this.f = null;
                }
            } catch (Exception e3) {
            }
            try {
                if (this.f954c != null) {
                    this.f954c.e();
                    this.f954c = null;
                }
            } catch (Exception e4) {
            }
            try {
                if (this.d != null) {
                    this.d.e();
                    this.d = null;
                }
            } catch (Exception e5) {
            }
            try {
                if (this.e != null) {
                    this.e.b();
                    this.e = null;
                }
            } catch (Exception e6) {
            }
            try {
                aw.a();
            } catch (Exception e7) {
            }
            z.a("AvastGenericSc", "Service destroy took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        }
    }

    public static boolean a(Context context) {
        return context.checkPermission("android.permission.WRITE_SECURE_SETTINGS", Process.myPid(), Process.myUid()) == 0;
    }

    public static boolean b(Context context) {
        return context.checkPermission("android.permission.SEND_SMS", Process.myPid(), Process.myUid()) == 0;
    }

    public final void a(Runnable runnable) {
        if (Thread.currentThread() != this.f952a) {
            this.g.post(runnable);
        } else {
            runnable.run();
        }
    }

    public void a(String str, Bundle bundle) {
        if (this.e != null) {
            this.e.a(str, bundle);
        }
    }

    public void a(e eVar, a aVar, boolean z) {
        if (this.d != null) {
            this.d.a(eVar, aVar, z);
        }
    }

    public void a(bc bcVar, com.avast.android.generic.g.a.a aVar, boolean z) {
        if (this.f954c != null) {
            this.f954c.a(bcVar, aVar, z);
        } else if (aVar != null) {
            try {
                aVar.a(1);
            } catch (Exception e2) {
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.a.b.a(com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean, short):void
     arg types: [com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean, int]
     candidates:
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e, int, boolean):void
      com.avast.android.generic.g.a.b.a(java.lang.String, java.util.List<com.avast.android.generic.util.bc>, com.avast.android.generic.g.a.a, boolean):void
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean, short):void */
    public void b(bc bcVar, com.avast.android.generic.g.a.a aVar, boolean z) {
        if (this.f954c != null) {
            this.f954c.a(bcVar, aVar, z, (short) 15875);
        } else if (aVar != null) {
            try {
                aVar.a(1);
            } catch (Exception e2) {
            }
        }
    }

    public void a(String str, List<bc> list, com.avast.android.generic.g.a.a aVar, boolean z) {
        if (this.f954c != null) {
            this.f954c.a(str, list, aVar, z);
        } else if (aVar != null) {
            try {
                aVar.a(1);
            } catch (Exception e2) {
            }
        }
    }

    public void b(String str, List<bc> list, com.avast.android.generic.g.a.a aVar, boolean z) {
        if (this.f954c != null) {
            this.f954c.a(str, list, aVar, z, 15875);
        } else if (aVar != null) {
            try {
                aVar.a(1);
            } catch (Exception e2) {
            }
        }
    }
}
