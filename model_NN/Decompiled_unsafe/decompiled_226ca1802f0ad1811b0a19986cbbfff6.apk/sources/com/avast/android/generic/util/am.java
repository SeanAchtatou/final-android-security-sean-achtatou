package com.avast.android.generic.util;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* compiled from: UserTask */
public abstract class am<Params, Progress, Result> {

    /* renamed from: a  reason: collision with root package name */
    private static final BlockingQueue<Runnable> f1181a = new LinkedBlockingQueue(15);

    /* renamed from: b  reason: collision with root package name */
    private static final ThreadFactory f1182b = new an();

    /* renamed from: c  reason: collision with root package name */
    private static final ThreadPoolExecutor f1183c = new ThreadPoolExecutor(5, 15, 10, TimeUnit.SECONDS, f1181a, f1182b);
    /* access modifiers changed from: private */
    public static final ar d = new ar(null);
    private final au<Params, Result> e = new ao(this);
    private final FutureTask<Result> f = new ap(this, this.e);
    private volatile as g = as.PENDING;

    public abstract Result a(Object... objArr);

    public void a() {
    }

    public void a(Object obj) {
    }

    public void b(Progress... progressArr) {
    }

    public void b() {
    }

    public final am<Params, Progress, Result> c(Params... paramsArr) {
        if (this.g != as.PENDING) {
            switch (aq.f1187a[this.g.ordinal()]) {
                case 1:
                    throw new IllegalStateException("Cannot execute task: the task is already running.");
                case 2:
                    throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.g = as.RUNNING;
        a();
        this.e.f1193b = paramsArr;
        f1183c.execute(this.f);
        return this;
    }

    /* access modifiers changed from: private */
    public void b(Result result) {
        a(result);
        this.g = as.FINISHED;
    }
}
