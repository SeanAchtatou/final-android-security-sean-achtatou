package com.avast.android.generic.h;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: CommunityIqProto */
public final class c extends GeneratedMessageLite implements k {

    /* renamed from: a  reason: collision with root package name */
    private static final c f700a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f701b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f702c;
    /* access modifiers changed from: private */
    public e d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public ByteString f;
    /* access modifiers changed from: private */
    public ByteString g;
    /* access modifiers changed from: private */
    public ByteString h;
    /* access modifiers changed from: private */
    public ByteString i;
    /* access modifiers changed from: private */
    public ByteString j;
    /* access modifiers changed from: private */
    public ByteString k;
    /* access modifiers changed from: private */
    public g l;
    /* access modifiers changed from: private */
    public i m;
    /* access modifiers changed from: private */
    public ByteString n;
    /* access modifiers changed from: private */
    public ByteString o;
    /* access modifiers changed from: private */
    public ByteString p;
    /* access modifiers changed from: private */
    public ByteString q;
    /* access modifiers changed from: private */
    public long r;
    /* access modifiers changed from: private */
    public ByteString s;
    /* access modifiers changed from: private */
    public long t;
    /* access modifiers changed from: private */
    public ByteString u;
    /* access modifiers changed from: private */
    public ByteString v;
    private byte w;
    private int x;

    private c(d dVar) {
        super(dVar);
        this.w = -1;
        this.x = -1;
    }

    private c(boolean z) {
        this.w = -1;
        this.x = -1;
    }

    public static c a() {
        return f700a;
    }

    public boolean b() {
        return (this.f701b & 1) == 1;
    }

    public ByteString c() {
        return this.f702c;
    }

    public boolean d() {
        return (this.f701b & 2) == 2;
    }

    public e e() {
        return this.d;
    }

    public boolean f() {
        return (this.f701b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f701b & 8) == 8;
    }

    public ByteString i() {
        return this.f;
    }

    public boolean j() {
        return (this.f701b & 16) == 16;
    }

    public ByteString k() {
        return this.g;
    }

    public boolean l() {
        return (this.f701b & 32) == 32;
    }

    public ByteString m() {
        return this.h;
    }

    public boolean n() {
        return (this.f701b & 64) == 64;
    }

    public ByteString o() {
        return this.i;
    }

    public boolean p() {
        return (this.f701b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public ByteString q() {
        return this.j;
    }

    public boolean r() {
        return (this.f701b & 256) == 256;
    }

    public ByteString s() {
        return this.k;
    }

    public boolean t() {
        return (this.f701b & 512) == 512;
    }

    public g u() {
        return this.l;
    }

    public boolean v() {
        return (this.f701b & 1024) == 1024;
    }

    public i w() {
        return this.m;
    }

    public boolean x() {
        return (this.f701b & 2048) == 2048;
    }

    public ByteString y() {
        return this.n;
    }

    public boolean z() {
        return (this.f701b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public ByteString A() {
        return this.o;
    }

    public boolean B() {
        return (this.f701b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public ByteString C() {
        return this.p;
    }

    public boolean D() {
        return (this.f701b & 16384) == 16384;
    }

    public ByteString E() {
        return this.q;
    }

    public boolean F() {
        return (this.f701b & 32768) == 32768;
    }

    public long G() {
        return this.r;
    }

    public boolean H() {
        return (this.f701b & Menu.CATEGORY_CONTAINER) == 65536;
    }

    public ByteString I() {
        return this.s;
    }

    public boolean J() {
        return (this.f701b & Menu.CATEGORY_SYSTEM) == 131072;
    }

    public long K() {
        return this.t;
    }

    public boolean L() {
        return (this.f701b & Menu.CATEGORY_ALTERNATIVE) == 262144;
    }

    public ByteString M() {
        return this.u;
    }

    public boolean N() {
        return (this.f701b & 524288) == 524288;
    }

    public ByteString O() {
        return this.v;
    }

    private void S() {
        this.f702c = ByteString.EMPTY;
        this.d = e.MOBILE_SECURITY_INSTALL;
        this.e = ByteString.EMPTY;
        this.f = ByteString.EMPTY;
        this.g = ByteString.EMPTY;
        this.h = ByteString.EMPTY;
        this.i = ByteString.EMPTY;
        this.j = ByteString.EMPTY;
        this.k = ByteString.EMPTY;
        this.l = g.UPDATE_CHECK_RESULT_UPDATE_AVAILABLE;
        this.m = i.UPDATE_RESULT_UP_TO_DATE;
        this.n = ByteString.EMPTY;
        this.o = ByteString.EMPTY;
        this.p = ByteString.EMPTY;
        this.q = ByteString.EMPTY;
        this.r = 0;
        this.s = ByteString.EMPTY;
        this.t = 0;
        this.u = ByteString.EMPTY;
        this.v = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.w;
        if (b2 == -1) {
            this.w = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f701b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f702c);
        }
        if ((this.f701b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
        if ((this.f701b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        if ((this.f701b & 8) == 8) {
            codedOutputStream.writeBytes(4, this.f);
        }
        if ((this.f701b & 16) == 16) {
            codedOutputStream.writeBytes(5, this.g);
        }
        if ((this.f701b & 32) == 32) {
            codedOutputStream.writeBytes(6, this.h);
        }
        if ((this.f701b & 64) == 64) {
            codedOutputStream.writeBytes(7, this.i);
        }
        if ((this.f701b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, this.j);
        }
        if ((this.f701b & 256) == 256) {
            codedOutputStream.writeBytes(9, this.k);
        }
        if ((this.f701b & 512) == 512) {
            codedOutputStream.writeEnum(10, this.l.getNumber());
        }
        if ((this.f701b & 1024) == 1024) {
            codedOutputStream.writeEnum(11, this.m.getNumber());
        }
        if ((this.f701b & 2048) == 2048) {
            codedOutputStream.writeBytes(12, this.n);
        }
        if ((this.f701b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBytes(13, this.o);
        }
        if ((this.f701b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBytes(14, this.p);
        }
        if ((this.f701b & 16384) == 16384) {
            codedOutputStream.writeBytes(15, this.q);
        }
        if ((this.f701b & 32768) == 32768) {
            codedOutputStream.writeInt64(16, this.r);
        }
        if ((this.f701b & Menu.CATEGORY_CONTAINER) == 65536) {
            codedOutputStream.writeBytes(17, this.s);
        }
        if ((this.f701b & Menu.CATEGORY_SYSTEM) == 131072) {
            codedOutputStream.writeInt64(18, this.t);
        }
        if ((this.f701b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            codedOutputStream.writeBytes(19, this.u);
        }
        if ((this.f701b & 524288) == 524288) {
            codedOutputStream.writeBytes(20, this.v);
        }
    }

    public int getSerializedSize() {
        int i2 = this.x;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f701b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f702c);
            }
            if ((this.f701b & 2) == 2) {
                i2 += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            if ((this.f701b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, this.e);
            }
            if ((this.f701b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, this.f);
            }
            if ((this.f701b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, this.g);
            }
            if ((this.f701b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(6, this.h);
            }
            if ((this.f701b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, this.i);
            }
            if ((this.f701b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, this.j);
            }
            if ((this.f701b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(9, this.k);
            }
            if ((this.f701b & 512) == 512) {
                i2 += CodedOutputStream.computeEnumSize(10, this.l.getNumber());
            }
            if ((this.f701b & 1024) == 1024) {
                i2 += CodedOutputStream.computeEnumSize(11, this.m.getNumber());
            }
            if ((this.f701b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBytesSize(12, this.n);
            }
            if ((this.f701b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBytesSize(13, this.o);
            }
            if ((this.f701b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBytesSize(14, this.p);
            }
            if ((this.f701b & 16384) == 16384) {
                i2 += CodedOutputStream.computeBytesSize(15, this.q);
            }
            if ((this.f701b & 32768) == 32768) {
                i2 += CodedOutputStream.computeInt64Size(16, this.r);
            }
            if ((this.f701b & Menu.CATEGORY_CONTAINER) == 65536) {
                i2 += CodedOutputStream.computeBytesSize(17, this.s);
            }
            if ((this.f701b & Menu.CATEGORY_SYSTEM) == 131072) {
                i2 += CodedOutputStream.computeInt64Size(18, this.t);
            }
            if ((this.f701b & Menu.CATEGORY_ALTERNATIVE) == 262144) {
                i2 += CodedOutputStream.computeBytesSize(19, this.u);
            }
            if ((this.f701b & 524288) == 524288) {
                i2 += CodedOutputStream.computeBytesSize(20, this.v);
            }
            this.x = i2;
        }
        return i2;
    }

    public static d P() {
        return d.g();
    }

    /* renamed from: Q */
    public d newBuilderForType() {
        return P();
    }

    public static d a(c cVar) {
        return P().mergeFrom(cVar);
    }

    /* renamed from: R */
    public d toBuilder() {
        return a(this);
    }

    static {
        f700a.S();
    }
}
