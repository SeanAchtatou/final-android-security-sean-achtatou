package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class SwitchRow extends Row {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1122a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public CompoundButton f1123b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1124c;
    private int m;
    /* access modifiers changed from: private */
    public ad n;
    /* access modifiers changed from: private */
    public boolean o = false;
    /* access modifiers changed from: private */
    public boolean p = false;

    public SwitchRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row));
    }

    public SwitchRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowStyle, y.Row));
        b(context, context.obtainStyledAttributes(attributeSet, z.g, n.rowStyle, y.Row));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        this.f1124c = typedArray.getBoolean(3, false);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void b(Context context, TypedArray typedArray) {
        this.m = typedArray.getResourceId(3, 0);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        View inflate = inflate(getContext(), t.row_switch, this);
        this.f1122a = (ImageView) inflate.findViewById(r.row_icon);
        if (this.m != 0) {
            this.f1122a.setVisibility(0);
            this.f1122a.setImageResource(this.m);
        }
        this.f1123b = (CompoundButton) inflate.findViewById(r.row_switch);
        this.f1123b.setId(-1);
        this.f1123b.setChecked(this.f1124c);
        post(new ab(this));
        this.f1123b.setOnCheckedChangeListener(new ac(this));
    }

    public void b() {
        if (e() != null) {
            this.o = true;
            try {
                this.f1123b.setChecked(e().b(this.g, this.f1124c));
            } finally {
                this.o = false;
            }
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(z);
        super.setClickable(z);
        this.f1123b.setEnabled(z);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1125a = this.f1123b.isChecked();
        savedState.f1126b = isEnabled();
        return savedState;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.o = true;
        try {
            this.f1123b.setChecked(savedState.f1125a);
            this.o = false;
            setEnabled(savedState.f1126b);
        } catch (Throwable th) {
            this.o = false;
            throw th;
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new ae();

        /* renamed from: a  reason: collision with root package name */
        boolean f1125a;

        /* renamed from: b  reason: collision with root package name */
        boolean f1126b;

        /* synthetic */ SavedState(Parcel parcel, ab abVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            Bundle readBundle = parcel.readBundle();
            this.f1125a = readBundle.getBoolean("checked");
            this.f1126b = readBundle.getBoolean("enabled");
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            Bundle bundle = new Bundle();
            bundle.putBoolean("checked", this.f1125a);
            bundle.putBoolean("enabled", this.f1126b);
            parcel.writeBundle(bundle);
        }
    }
}
