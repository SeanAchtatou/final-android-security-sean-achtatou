package com.avast.android.generic.b.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: BadNewsProto */
public final class l extends GeneratedMessageLite.Builder<k, l> implements m {

    /* renamed from: a  reason: collision with root package name */
    private int f616a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f617b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f618c = ByteString.EMPTY;

    private l() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static l g() {
        return new l();
    }

    /* renamed from: a */
    public l clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public k getDefaultInstanceForType() {
        return k.a();
    }

    /* renamed from: c */
    public k build() {
        k d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public k d() {
        int i = 1;
        k kVar = new k(this);
        int i2 = this.f616a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = kVar.f615c = this.f617b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        ByteString unused2 = kVar.d = this.f618c;
        int unused3 = kVar.f614b = i;
        return kVar;
    }

    /* renamed from: a */
    public l mergeFrom(k kVar) {
        if (kVar != k.a()) {
            if (kVar.b()) {
                a(kVar.c());
            }
            if (kVar.d()) {
                b(kVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public l mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f616a |= 1;
                    this.f617b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f616a |= 2;
                    this.f618c = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public l a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f616a |= 1;
        this.f617b = byteString;
        return this;
    }

    public l b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f616a |= 2;
        this.f618c = byteString;
        return this;
    }
}
