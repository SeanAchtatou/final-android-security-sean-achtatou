package com.avast.android.generic.f;

import android.os.Build;

/* compiled from: V23AndHigherSerials */
public class a {
    static {
        if (Build.VERSION.SDK_INT < 9) {
            throw new RuntimeException("Newer SDK");
        }
    }

    private a() {
    }

    public static a a() {
        return new a();
    }

    public String b() {
        String str = Build.SERIAL;
        if (str == null || !str.equals("unknown")) {
            return str;
        }
        return null;
    }
}
