package com.avast.android.generic.util;

import java.io.Closeable;
import java.net.HttpURLConnection;

/* compiled from: StreamUtil */
public class af {
    public static void a(Closeable... closeableArr) {
        if (closeableArr != null) {
            for (Closeable closeable : closeableArr) {
                if (closeable != null) {
                    try {
                        closeable.close();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    public static void a(HttpURLConnection... httpURLConnectionArr) {
        if (httpURLConnectionArr != null) {
            for (HttpURLConnection httpURLConnection : httpURLConnectionArr) {
                if (httpURLConnection != null) {
                    try {
                        httpURLConnection.disconnect();
                    } catch (Exception e) {
                    }
                }
            }
        }
    }
}
