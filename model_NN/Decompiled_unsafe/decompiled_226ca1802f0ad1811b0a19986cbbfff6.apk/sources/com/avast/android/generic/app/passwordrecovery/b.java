package com.avast.android.generic.app.passwordrecovery;

import android.util.SparseArray;
import java.util.EnumSet;
import java.util.Iterator;

/* compiled from: PasswordRecovery */
public enum b {
    UNINITIATED(0),
    SMS_QUERIED(1),
    INITIATED_AND_VALID(2),
    SMS_SENDING_FAILED(3),
    EXPIRED(4),
    INVALID(5),
    INIT_ERROR(6);
    
    private static final SparseArray<b> h = new SparseArray<>();
    private final int i;

    static {
        Iterator it = EnumSet.allOf(b.class).iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            h.put(bVar.a(), bVar);
        }
    }

    private b(int i2) {
        this.i = i2;
    }

    public final int a() {
        return this.i;
    }

    public static b a(int i2) {
        return h.get(i2);
    }
}
