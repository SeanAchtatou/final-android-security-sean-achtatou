package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import com.avast.android.generic.d;
import com.avast.android.generic.e.c;
import com.avast.android.generic.n;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;
import java.util.Calendar;

public class WeekDaysRow extends Row {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ToggleButton[] f1129a = new ToggleButton[7];

    /* renamed from: b  reason: collision with root package name */
    private int f1130b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public int f1131c;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public aj n;
    private CompoundButton.OnCheckedChangeListener o = new ai(this);

    public WeekDaysRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context.obtainStyledAttributes(attributeSet, z.h, i, y.Row_WeekDays));
    }

    public WeekDaysRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowWeekDaysStyle);
        a(context.obtainStyledAttributes(attributeSet, z.h, n.rowWeekDaysStyle, y.Row_WeekDays));
    }

    private void a(TypedArray typedArray) {
        this.f1130b = typedArray.getInteger(3, 0);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        String b2;
        inflate(getContext(), t.row_week_days, this);
        int firstDayOfWeek = Calendar.getInstance().getFirstDayOfWeek();
        int i = getResources().getConfiguration().orientation;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < 7) {
                int i4 = ((i3 + 7) + (firstDayOfWeek - 2)) % 7;
                ToggleButton toggleButton = (ToggleButton) findViewWithTag("" + i3);
                toggleButton.setOnCheckedChangeListener(this.o);
                if (i == 1) {
                    b2 = d.a(getContext(), i4);
                } else {
                    b2 = d.b(getContext(), i4);
                }
                toggleButton.setText(b2);
                toggleButton.setTextOn(b2);
                toggleButton.setTextOff(b2);
                this.f1129a[i3] = toggleButton;
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    public void b() {
        this.m = true;
        try {
            c e = e();
            if (e != null) {
                a(e.b(this.g, this.f1130b));
            }
        } finally {
            this.m = false;
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(false);
        super.setClickable(false);
        for (int i = 0; i < this.f1129a.length; i++) {
            this.f1129a[i].setEnabled(z);
            this.f1129a[i].setFocusable(z);
            this.f1129a[i].setClickable(z);
        }
    }

    private void a(int i) {
        int b2 = d.b(i);
        int i2 = 1;
        for (int i3 = 0; i3 < 7; i3++) {
            if ((b2 & i2) > 0) {
                this.f1129a[i3].setChecked(true);
            } else {
                this.f1129a[i3].setChecked(false);
            }
            i2 *= 2;
        }
        this.f1131c = i;
    }
}
