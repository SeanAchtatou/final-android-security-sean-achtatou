package com.avast.android.generic.ui.rtl;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import com.avast.android.generic.util.t;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/* compiled from: LtrToRtlConverter */
public abstract class a {
    public static final String LANG_ARABIC = "ar";
    private static Map<Class<? extends View>, Class<? extends a>> sConverterClassesMap = new HashMap();
    private static Map<String, Map<Class<? extends View>, a>> sLangMap = new HashMap(1);
    protected String mCurrentLang;

    public abstract View ltrToRtlView(View view);

    static {
        sConverterClassesMap.put(LinearLayout.class, LinearLayoutLtrToRtlConverter.class);
        sConverterClassesMap.put(RelativeLayout.class, RelativeLayoutLtrToRtlConverter.class);
        sConverterClassesMap.put(TextView.class, TextViewLtrToRtlConverter.class);
        sConverterClassesMap.put(TableLayout.class, TableLayoutLtrToRtlConverter.class);
    }

    public static a getConverter(View view, String str) {
        HashMap hashMap;
        InstantiationException e;
        a aVar;
        NoSuchMethodException e2;
        InvocationTargetException e3;
        IllegalAccessException e4;
        if (!sLangMap.containsKey(str)) {
            HashMap hashMap2 = new HashMap(2);
            sLangMap.put(str, hashMap2);
            hashMap = hashMap2;
        } else {
            hashMap = sLangMap.get(str);
        }
        Class<? super Object> cls = view.getClass();
        while (true) {
            if (cls == null || cls == View.class) {
                break;
            } else if (!sConverterClassesMap.containsKey(cls)) {
                cls = cls.getSuperclass();
            } else if (hashMap.containsKey(cls)) {
                return (a) hashMap.get(cls);
            }
        }
        if (!sConverterClassesMap.containsKey(cls)) {
            return null;
        }
        try {
            aVar = (a) sConverterClassesMap.get(cls).getConstructor(String.class).newInstance(str);
            try {
                hashMap.put(cls, aVar);
                return aVar;
            } catch (InstantiationException e5) {
                e = e5;
            } catch (IllegalAccessException e6) {
                e4 = e6;
                t.b("Can't get LTR to RTL converter constructor for class " + cls, e4);
                return aVar;
            } catch (InvocationTargetException e7) {
                e3 = e7;
                t.b("Can't invoke LTR to RTL converter constructor for class " + cls, e3);
                return aVar;
            } catch (NoSuchMethodException e8) {
                e2 = e8;
                t.b("Can't find LTR to RTL converter constructor for class " + cls, e2);
                return aVar;
            }
        } catch (InstantiationException e9) {
            e = e9;
            aVar = null;
            t.b("Can't instantiate LTR to RTL converter for class " + cls, e);
            return aVar;
        } catch (IllegalAccessException e10) {
            e4 = e10;
            aVar = null;
            t.b("Can't get LTR to RTL converter constructor for class " + cls, e4);
            return aVar;
        } catch (InvocationTargetException e11) {
            e3 = e11;
            aVar = null;
            t.b("Can't invoke LTR to RTL converter constructor for class " + cls, e3);
            return aVar;
        } catch (NoSuchMethodException e12) {
            e2 = e12;
            aVar = null;
            t.b("Can't find LTR to RTL converter constructor for class " + cls, e2);
            return aVar;
        }
    }

    protected a(String str) {
        this.mCurrentLang = str;
    }
}
