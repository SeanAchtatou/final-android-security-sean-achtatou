package com.avast.android.generic.app.settings;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: SetRecoveryNumberDialog */
class h implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ boolean f507a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SetRecoveryNumberDialog f508b;

    h(SetRecoveryNumberDialog setRecoveryNumberDialog, boolean z) {
        this.f508b = setRecoveryNumberDialog;
        this.f507a = z;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (!this.f507a) {
            i++;
        }
        this.f508b.a(i);
    }
}
