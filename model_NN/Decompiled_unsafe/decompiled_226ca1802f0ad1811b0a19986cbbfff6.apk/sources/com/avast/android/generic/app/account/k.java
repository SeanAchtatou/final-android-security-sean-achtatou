package com.avast.android.generic.app.account;

import android.content.Context;

/* compiled from: ConnectAccountFragment */
class k extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f438a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f439b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    k(ConnectAccountFragment connectAccountFragment, Context context, Context context2) {
        super(context);
        this.f439b = connectAccountFragment;
        this.f438a = context2;
    }

    public void a(String str) {
        if (this.f439b.isAdded()) {
            this.f439b.w.post(new l(this, str));
            this.f439b.e();
            this.f439b.v.setEnabled(true);
        }
        r unused = this.f439b.f360c = (r) null;
    }

    public void a() {
        if (this.f439b.isAdded()) {
            boolean unused = this.f439b.p = true;
            this.f439b.e();
            this.f439b.v.setEnabled(true);
            this.f439b.g();
            this.f439b.q();
        }
        r unused2 = this.f439b.f360c = (r) null;
    }

    public void a(z zVar, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        a(zVar, str7);
        this.f439b.a(this.f438a, str, str2, str3, str4, str5, str6);
        r unused = this.f439b.f360c = (r) null;
    }
}
