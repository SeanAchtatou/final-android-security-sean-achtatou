package com.avast.android.generic.app.subscription;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.actionbarsherlock.app.ActionBar;
import com.avast.android.generic.q;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.BaseSinglePaneActivity;
import com.avast.android.generic.x;

public class WelcomePremiumActivity extends BaseSinglePaneActivity {
    public static void a(Context context) {
        if (context instanceof BaseActivity) {
            ((BaseActivity) context).a(WelcomePremiumActivity.class);
        } else {
            context.startActivity(new Intent(context, WelcomePremiumActivity.class));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setBackgroundDrawable(getResources().getDrawable(q.xml_bg_actionbar_premium_wtb));
            supportActionBar.setDisplayOptions(14);
            supportActionBar.setHomeButtonEnabled(true);
            supportActionBar.setIcon(getResources().getDrawable(q.ic_app));
            supportActionBar.setTitle(x.l_subscription_welcome_title);
        }
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        return new WelcomePremiumFragment();
    }
}
