package com.avast.android.generic.app.account;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public class AccountDisconnectDialog extends DialogFragment {
    public static AccountDisconnectDialog a(FragmentManager fragmentManager) {
        AccountDisconnectDialog accountDisconnectDialog = new AccountDisconnectDialog();
        accountDisconnectDialog.show(fragmentManager, "dialog");
        return accountDisconnectDialog;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Dialog onCreateDialog(Bundle bundle) {
        Context c2 = ak.c(getActivity());
        View inflate = LayoutInflater.from(c2).inflate(t.dialog_avast_account_disconnect, (ViewGroup) null, false);
        TextView textView = (TextView) inflate.findViewById(r.l_disconnect_warning);
        if (aa.d(getActivity())) {
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        return new AlertDialog.Builder(c2).setTitle(getString(x.l_avast_account_disconnect_dialog_title)).setView(inflate).setPositiveButton(x.da, new b(this)).setNegativeButton(x.bB, new a(this)).setInverseBackgroundForced(true).create();
    }

    /* access modifiers changed from: private */
    public void a() {
        android.support.v4.content.r.a(getActivity()).a(new Intent("com.avast.android.generic.app.account.ACTION_DISCONNECT_CONFIRMED"));
    }
}
