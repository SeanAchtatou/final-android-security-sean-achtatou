package com.avast.android.generic.util.ga;

import a.a.a.a.a.a;
import android.os.Bundle;
import android.support.v4.app.FragmentBreadCrumbs;
import android.text.TextUtils;
import android.view.View;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListFragment;
import com.avast.android.generic.r;
import com.avast.android.generic.util.ak;

public abstract class TrackedListFragment extends SherlockListFragment {
    public abstract String k();

    public int d() {
        return 0;
    }

    public FragmentBreadCrumbs a(View view) {
        return (FragmentBreadCrumbs) view.findViewWithTag("breadcrumbs");
    }

    public View b(View view) {
        return view.findViewById(r.titlebar);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        c(view);
        a();
    }

    private void c(View view) {
        FragmentBreadCrumbs a2;
        if (!ak.b(getActivity()) && (a2 = a(view)) != null) {
            a2.setActivity(getActivity());
            if (d() > 0) {
                a2.setLastTitle(getString(d()), "");
            }
            if (getArguments() != null) {
                int i = getArguments().getInt("titleResourceId", 0);
                if (i > 0) {
                    a2.setLastTitle(getString(i), "");
                    return;
                }
                String string = getArguments().getString("title");
                if (!TextUtils.isEmpty(string)) {
                    a2.setLastTitle(string, "");
                }
            }
        }
    }

    private void a() {
        ActionBar supportActionBar = getSherlockActivity().getSupportActionBar();
        if (supportActionBar != null && (supportActionBar.getDisplayOptions() & 8) != 0) {
            if (d() > 0) {
                supportActionBar.setTitle(d());
            }
            if (getArguments() != null) {
                int i = getArguments().getInt("titleResourceId", 0);
                if (i > 0) {
                    supportActionBar.setTitle(i);
                    return;
                }
                String string = getArguments().getString("title");
                if (!TextUtils.isEmpty(string)) {
                    supportActionBar.setTitle(string);
                }
            }
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        setListAdapter(null);
    }

    public void onResume() {
        super.onResume();
        c a2 = a.a();
        String k = k();
        if (k != null) {
            a2.a(k);
        }
    }

    public void a(String str, String str2, String str3, long j) {
        try {
            a.a().a(str, str2, str3, Long.valueOf(j));
        } catch (Exception e) {
            a.a().a("GA: trackEvent", e);
        }
    }
}
