package com.avast.android.generic.g.b;

import android.content.Context;
import android.telephony.CellLocation;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;

/* compiled from: StateProvider */
public final class b {

    /* renamed from: a  reason: collision with root package name */
    private int f694a = -1;

    /* renamed from: b  reason: collision with root package name */
    private int f695b = -1;

    /* renamed from: c  reason: collision with root package name */
    private int f696c = -1;
    private int d = -1;
    private int e = -1;
    private int f = 1;
    private String g;
    private String h;

    public b(Context context, TelephonyManager telephonyManager, CellLocation cellLocation, int i, ServiceState serviceState) {
        if (cellLocation instanceof GsmCellLocation) {
            GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
            a(gsmCellLocation.getCid());
            b(gsmCellLocation.getLac());
        } else if (cellLocation instanceof CdmaCellLocation) {
            CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
            a(cdmaCellLocation.getBaseStationId());
            b(cdmaCellLocation.getNetworkId());
        }
        c(i);
        if (serviceState != null) {
            a(serviceState.getOperatorAlphaLong());
            f(serviceState.getState());
            a(context, serviceState.getOperatorNumeric());
        }
        b(telephonyManager.getSubscriberId());
    }

    private void a(Context context, String str) {
        if (str != null) {
            try {
                d(Integer.parseInt(str.substring(0, 3)));
                e(Integer.parseInt(str.substring(3)));
            } catch (IndexOutOfBoundsException | NumberFormatException e2) {
            }
        }
    }

    public void a(int i) {
        this.f694a = i;
    }

    public void b(int i) {
        this.f695b = i;
    }

    public void c(int i) {
        this.f696c = i;
    }

    public void d(int i) {
        this.d = i;
    }

    public void e(int i) {
        this.e = i;
    }

    public void f(int i) {
        this.f = i;
    }

    public int a() {
        return this.f;
    }

    public void a(String str) {
        this.g = str;
    }

    public void b(String str) {
        this.h = str;
    }
}
