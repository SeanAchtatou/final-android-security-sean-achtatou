package com.avast.android.generic.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: PasswordDialog */
class x implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f1160a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ PasswordDialog f1161b;

    x(PasswordDialog passwordDialog, AlertDialog alertDialog) {
        this.f1161b = passwordDialog;
        this.f1160a = alertDialog;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f1160a.getButton(-1).setOnClickListener(new y(this));
    }
}
