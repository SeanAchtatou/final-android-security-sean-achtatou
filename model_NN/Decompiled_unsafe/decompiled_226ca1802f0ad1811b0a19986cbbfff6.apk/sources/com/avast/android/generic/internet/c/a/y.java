package com.avast.android.generic.internet.c.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class y extends GeneratedMessageLite implements ac {

    /* renamed from: a  reason: collision with root package name */
    private static final y f795a = new y(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f796b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f797c;
    /* access modifiers changed from: private */
    public aa d;
    /* access modifiers changed from: private */
    public Object e;
    private byte f;
    private int g;

    private y(z zVar) {
        super(zVar);
        this.f = -1;
        this.g = -1;
    }

    private y(boolean z) {
        this.f = -1;
        this.g = -1;
    }

    public static y a() {
        return f795a;
    }

    public boolean b() {
        return (this.f796b & 1) == 1;
    }

    public String c() {
        Object obj = this.f797c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f797c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString k() {
        Object obj = this.f797c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f797c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f796b & 2) == 2;
    }

    public aa e() {
        return this.d;
    }

    public boolean f() {
        return (this.f796b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString l() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    private void m() {
        this.f797c = "";
        this.d = aa.GENERIC;
        this.e = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.f;
        if (b2 == -1) {
            this.f = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f796b & 1) == 1) {
            codedOutputStream.writeBytes(1, k());
        }
        if ((this.f796b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
        if ((this.f796b & 4) == 4) {
            codedOutputStream.writeBytes(3, l());
        }
    }

    public int getSerializedSize() {
        int i = this.g;
        if (i == -1) {
            i = 0;
            if ((this.f796b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, k());
            }
            if ((this.f796b & 2) == 2) {
                i += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            if ((this.f796b & 4) == 4) {
                i += CodedOutputStream.computeBytesSize(3, l());
            }
            this.g = i;
        }
        return i;
    }

    public static z h() {
        return z.g();
    }

    /* renamed from: i */
    public z newBuilderForType() {
        return h();
    }

    public static z a(y yVar) {
        return h().mergeFrom(yVar);
    }

    /* renamed from: j */
    public z toBuilder() {
        return a(this);
    }

    static {
        f795a.m();
    }
}
