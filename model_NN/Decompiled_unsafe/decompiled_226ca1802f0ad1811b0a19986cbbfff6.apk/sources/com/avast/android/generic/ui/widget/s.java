package com.avast.android.generic.ui.widget;

import android.os.Parcel;
import android.os.Parcelable;
import com.avast.android.generic.ui.widget.RowImage;

/* compiled from: RowImage */
final class s implements Parcelable.Creator<RowImage.SavedState> {
    s() {
    }

    /* renamed from: a */
    public RowImage.SavedState createFromParcel(Parcel parcel) {
        return new RowImage.SavedState(parcel);
    }

    /* renamed from: a */
    public RowImage.SavedState[] newArray(int i) {
        return new RowImage.SavedState[i];
    }
}
