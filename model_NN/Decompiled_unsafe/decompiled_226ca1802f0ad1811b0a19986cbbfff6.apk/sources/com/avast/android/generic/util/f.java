package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum f {
    FACEBOOK("facebook"),
    EMAIL("email"),
    GPLUS("gplus");
    
    String d;

    private f(String str) {
        this.d = str;
    }

    public String a() {
        return this.d;
    }
}
