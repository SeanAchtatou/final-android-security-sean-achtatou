package com.avast.android.generic.e;

import android.content.Context;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;

/* compiled from: PreferenceDAO */
public class b implements c {

    /* renamed from: a  reason: collision with root package name */
    private ab f666a = null;

    /* renamed from: b  reason: collision with root package name */
    private Context f667b = null;

    public b(Context context) {
        this.f667b = context;
        this.f666a = (ab) aa.a(context, ad.class);
    }

    public boolean a(String str, boolean z) {
        this.f666a.a();
        this.f666a.a(str, z);
        return this.f666a.b();
    }

    public boolean b(String str, boolean z) {
        return this.f666a.b(str, z);
    }

    public boolean a(String str, int i) {
        this.f666a.a();
        this.f666a.a(str, i);
        return this.f666a.b();
    }

    public int b(String str, int i) {
        return this.f666a.b(str, i);
    }

    public boolean a(String str, String str2) {
        this.f666a.a();
        this.f666a.a(str, str2);
        return this.f666a.b();
    }

    public String a(String str) {
        return b(str, (String) null);
    }

    public String b(String str, String str2) {
        return this.f666a.b(str, str2);
    }
}
