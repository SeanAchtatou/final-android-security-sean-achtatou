package com.avast.android.generic.internet.c;

import android.a.a.a;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.d.b;
import com.avast.android.generic.internet.c.a.d;
import com.avast.android.generic.internet.c.a.f;
import com.avast.android.generic.internet.c.a.g;
import com.avast.android.generic.internet.c.a.t;
import com.avast.android.generic.internet.c.a.u;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.av;
import com.avast.android.generic.util.l;
import com.avast.android.generic.util.s;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.client.CookieStore;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

@TargetApi(8)
/* compiled from: AvastAccountConnector */
public class c {
    private String A;
    private String B;

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f801a = {-69, -44, 61, -101, -93, 79, -116, 29, -104, -56, 85, 64, 98, -100, -4, -5};

    /* renamed from: b  reason: collision with root package name */
    private final String f802b = "https://pair.ff.avast.com";

    /* renamed from: c  reason: collision with root package name */
    private final String f803c = "/F/";
    private final String d = "/unpair";
    private final String e = "http://ai.ff.avast.com";
    private final String f = "/F/";
    private final byte[] g = {0, 4};
    private final byte[] h = {0, -1};
    private final int i = 1;
    private final int j = 2;
    private final int k = 1;
    private final int l = 2;
    private final int m = 3;
    private final int n = 4;
    private final int o = 5;
    private final int p = 6;
    private a q;
    private HttpContext r;
    private av s;
    private String t;
    private CookieStore u;
    private HttpParams v;
    private String w;
    private ab x;
    private boolean y;
    private String z;

    public c(Context context, Bundle bundle) {
        a(context);
        if (this.q == null) {
            throw new InstantiationException("Secure SSL client couldn't be created");
        }
        this.u = new BasicCookieStore();
        this.r = new BasicHttpContext();
        this.r.setAttribute("http.cookie-store", this.u);
        this.v = this.q.getParams();
        this.v.setBooleanParameter("http.protocol.handle-redirects", true);
        this.x = (ab) aa.a(context, ab.class);
        this.w = this.x.q();
        if (s.a(context)) {
            this.w = "00000000-0000-0000-0000-000000000000";
        }
        this.y = ak.a(context);
        this.s = av.a(context);
        if (this.s == null) {
            throw new InstantiationException("Can not read comm framework version");
        }
        this.t = com.avast.android.generic.g.b.a.i(context);
        if (TextUtils.isEmpty(this.t)) {
            throw new InstantiationException("Can not read device ID");
        }
        a(bundle);
    }

    private void a(Bundle bundle) {
        this.z = "https://pair.ff.avast.com/F/";
        this.A = "https://pair.ff.avast.com/unpair";
        this.B = "http://ai.ff.avast.com/F/";
        if (bundle != null) {
            if (bundle.containsKey("my_avast_pairing_server_address")) {
                this.z = bundle.getString("my_avast_pairing_server_address");
            }
            if (bundle.containsKey("my_avast_unpairing_server_address")) {
                this.A = bundle.getString("my_avast_unpairing_server_address");
            }
            if (bundle.containsKey("my_avast_status_server_address")) {
                this.B = bundle.getString("my_avast_status_server_address");
            }
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c0 A[Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00eb A[SYNTHETIC, Splitter:B:43:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00f5 A[SYNTHETIC, Splitter:B:50:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0106 A[SYNTHETIC, Splitter:B:57:0x0106] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0113 A[SYNTHETIC, Splitter:B:64:0x0113] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x011b A[Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0123 A[Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:47:0x00f2=Splitter:B:47:0x00f2, B:54:0x00fc=Splitter:B:54:0x00fc, B:61:0x010d=Splitter:B:61:0x010d} */
    public com.avast.android.generic.internet.c.f a(com.avast.android.generic.internet.c.e r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, boolean r14, boolean r15, boolean r16, java.lang.String r17, java.lang.String r18, long r19) {
        /*
            r9 = this;
            r1 = 0
            byte[] r0 = r9.g     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r2 = r9.w     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r0 = r9.a(r0, r2)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            org.apache.http.client.methods.HttpPost r2 = new org.apache.http.client.methods.HttpPost     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r3.<init>()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r4 = r9.z     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r0 = r0.toString()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r2.<init>(r0)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r0 = "AvastAccountConnector"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r3.<init>()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r4 = "Connecting device with GUID: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r4 = r9.w     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r3 = r3.toString()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            com.avast.android.generic.util.t.b(r0, r3)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r0 = "AvastAccountConnector"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r3.<init>()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r4 = "Sending request to: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.net.URI r4 = r2.getURI()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r4 = r4.toString()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            java.lang.String r3 = r3.toString()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            com.avast.android.generic.util.t.b(r0, r3)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            com.avast.android.generic.internet.c.a.g r0 = r9.b(r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            com.avast.android.generic.internet.c.a.f r0 = r0.build()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            byte[] r0 = r0.toByteArray()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            org.apache.http.entity.ByteArrayEntity r3 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r3.<init>(r0)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r2.setEntity(r3)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            android.a.a.a r0 = r9.q     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            org.apache.http.protocol.HttpContext r3 = r9.r     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            org.apache.http.HttpResponse r0 = r0.execute(r2, r3)     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            if (r0 == 0) goto L_0x008b
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            if (r2 == 0) goto L_0x008b
            org.apache.http.StatusLine r2 = r0.getStatusLine()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            int r2 = r2.getStatusCode()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x0092
        L_0x008b:
            r0 = 0
            if (r1 == 0) goto L_0x0091
            r1.consumeContent()     // Catch:{ IOException -> 0x01d9 }
        L_0x0091:
            return r0
        L_0x0092:
            org.apache.http.HttpEntity r8 = r0.getEntity()     // Catch:{ h -> 0x01ef, j -> 0x01eb, ClientProtocolException -> 0x01e7, Exception -> 0x01e3, all -> 0x01df }
            if (r8 != 0) goto L_0x00a1
            r0 = 0
            if (r8 == 0) goto L_0x0091
            r8.consumeContent()     // Catch:{ IOException -> 0x009f }
            goto L_0x0091
        L_0x009f:
            r1 = move-exception
            goto L_0x0091
        L_0x00a1:
            java.io.InputStream r0 = r8.getContent()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.avast.android.generic.internet.c.a.i r6 = com.avast.android.generic.internet.c.a.i.a(r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            boolean r0 = r6.h()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r0 == 0) goto L_0x00e8
            java.lang.String r0 = r6.i()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x00b3:
            boolean r1 = r6.f()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r1 == 0) goto L_0x0153
            int r1 = r6.g()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            switch(r1) {
                case 1: goto L_0x00eb;
                case 2: goto L_0x00f5;
                case 3: goto L_0x0106;
                case 4: goto L_0x0113;
                case 5: goto L_0x011b;
                case 6: goto L_0x0123;
                default: goto L_0x00c0;
            }     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x00c0:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r2.<init>()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = "Unknown error code: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            int r3 = r6.g()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = r2.toString()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r1.<init>(r2, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x00dd:
            r0 = move-exception
            r1 = r8
        L_0x00df:
            throw r0     // Catch:{ all -> 0x00e0 }
        L_0x00e0:
            r0 = move-exception
            r8 = r1
        L_0x00e2:
            if (r8 == 0) goto L_0x00e7
            r8.consumeContent()     // Catch:{ IOException -> 0x01dc }
        L_0x00e7:
            throw r0
        L_0x00e8:
            java.lang.String r0 = ""
            goto L_0x00b3
        L_0x00eb:
            com.avast.android.generic.internet.c.b r1 = new com.avast.android.generic.internet.c.b     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r1.<init>(r11, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x00f1:
            r0 = move-exception
        L_0x00f2:
            throw r0     // Catch:{ all -> 0x00f3 }
        L_0x00f3:
            r0 = move-exception
            goto L_0x00e2
        L_0x00f5:
            com.avast.android.generic.internet.c.l r1 = new com.avast.android.generic.internet.c.l     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r1.<init>(r11, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x00fb:
            r0 = move-exception
        L_0x00fc:
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00f3 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00f3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00f3 }
            throw r1     // Catch:{ all -> 0x00f3 }
        L_0x0106:
            com.avast.android.generic.internet.c.m r1 = new com.avast.android.generic.internet.c.m     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r1.<init>(r11, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x010c:
            r0 = move-exception
        L_0x010d:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ all -> 0x00f3 }
            r1.<init>(r0)     // Catch:{ all -> 0x00f3 }
            throw r1     // Catch:{ all -> 0x00f3 }
        L_0x0113:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = "Cannot connect to id.avast.com."
            r1.<init>(r2, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x011b:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = "Other error."
            r1.<init>(r2, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x0123:
            boolean r1 = r6.l()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r1 == 0) goto L_0x014b
            com.avast.android.generic.internet.c.a.w r1 = r6.m()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            boolean r2 = r1.f()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r2 == 0) goto L_0x014b
            com.avast.android.generic.internet.c.a.y r1 = r1.g()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.avast.android.generic.internet.c.a.aa r0 = com.avast.android.generic.internet.c.a.aa.GENERIC     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            boolean r2 = r1.d()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r2 == 0) goto L_0x0143
            com.avast.android.generic.internet.c.a.aa r0 = r1.e()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x0143:
            com.avast.android.generic.internet.c.j r1 = new com.avast.android.generic.internet.c.j     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = "Backend exception"
            r1.<init>(r2, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x014b:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = "Anti-Theft backend error."
            r1.<init>(r2, r0)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            throw r1     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x0153:
            com.avast.android.generic.internet.c.f r0 = new com.avast.android.generic.internet.c.f     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.google.protobuf.ByteString r1 = r6.c()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r1 = r1.toStringUtf8()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.google.protobuf.ByteString r2 = r6.e()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = r2.toStringUtf8()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.avast.android.generic.internet.c.a.w r3 = r6.m()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.google.protobuf.ByteString r3 = r3.c()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = r3.toStringUtf8()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.avast.android.generic.internet.c.a.w r4 = r6.m()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            boolean r4 = r4.d()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r4 == 0) goto L_0x01d7
            com.avast.android.generic.internet.c.a.w r4 = r6.m()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r4 = r4.e()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
        L_0x0183:
            java.lang.String r5 = r6.u()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r6 = r6.w()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r7 = 0
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r1 = "AvastAccountConnector"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            r2.<init>()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = "Connected, AUID:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = r0.f808a     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = ", enc key:"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = r0.f809b     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = ", comm password: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = r0.f810c     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = ", SMS gateway: "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r3 = r0.d     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            java.lang.String r2 = r2.toString()     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            com.avast.android.generic.util.t.b(r1, r2)     // Catch:{ h -> 0x00dd, j -> 0x00f1, ClientProtocolException -> 0x00fb, Exception -> 0x010c }
            if (r8 == 0) goto L_0x0091
            r8.consumeContent()     // Catch:{ IOException -> 0x01d4 }
            goto L_0x0091
        L_0x01d4:
            r1 = move-exception
            goto L_0x0091
        L_0x01d7:
            r4 = 0
            goto L_0x0183
        L_0x01d9:
            r1 = move-exception
            goto L_0x0091
        L_0x01dc:
            r1 = move-exception
            goto L_0x00e7
        L_0x01df:
            r0 = move-exception
            r8 = r1
            goto L_0x00e2
        L_0x01e3:
            r0 = move-exception
            r8 = r1
            goto L_0x010d
        L_0x01e7:
            r0 = move-exception
            r8 = r1
            goto L_0x00fc
        L_0x01eb:
            r0 = move-exception
            r8 = r1
            goto L_0x00f2
        L_0x01ef:
            r0 = move-exception
            goto L_0x00df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.c.c.a(com.avast.android.generic.internet.c.e, java.lang.String, java.lang.String, java.lang.String, boolean, boolean, boolean, java.lang.String, java.lang.String, long):com.avast.android.generic.internet.c.f");
    }

    private g b(e eVar, String str, String str2, String str3, boolean z2, boolean z3, boolean z4, String str4, String str5, long j2) {
        g F = f.F();
        if (str != null) {
            F.a(str);
        }
        if (str2 != null) {
            F.b(str2);
        }
        if (eVar == null) {
            F.a(e.EMAIL.a());
        } else {
            F.a(eVar.a());
        }
        F.d(Build.MODEL);
        F.f("android");
        F.e(this.y ? "tablet" : "phone");
        u p2 = t.p();
        p2.c(this.s.d());
        p2.b(this.t);
        if (str3 != null && !TextUtils.isEmpty(str3)) {
            p2.a(str3);
        }
        p2.a(z2);
        p2.c(z4);
        p2.b(z3);
        p2.d(str4);
        F.a(p2);
        if (str5 != null) {
            d f2 = com.avast.android.generic.internet.c.a.c.f();
            f2.a(str5);
            f2.a(j2);
            F.a(f2);
        }
        return F;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.a.a.a.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse
     arg types: [org.apache.http.client.methods.HttpPost, org.apache.http.protocol.HttpContext]
     candidates:
      android.a.a.a.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.client.ResponseHandler):T
      android.a.a.a.execute(org.apache.http.HttpHost, org.apache.http.HttpRequest):org.apache.http.HttpResponse
      android.a.a.a.execute(org.apache.http.client.methods.HttpUriRequest, org.apache.http.protocol.HttpContext):org.apache.http.HttpResponse */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00f3, code lost:
        r1 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0129, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x012f, code lost:
        throw new com.avast.android.generic.internet.c.h(r0);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0102 A[SYNTHETIC, Splitter:B:40:0x0102] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0129 A[ExcHandler: Exception (r0v3 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:6:0x0016] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r8, byte[] r9) {
        /*
            r7 = this;
            r1 = 0
            java.lang.String r0 = "application/x-enc-gd"
            if (r9 == 0) goto L_0x000a
            int r2 = r9.length
            r3 = 16
            if (r2 >= r3) goto L_0x0015
        L_0x000a:
            java.lang.String r0 = "AvastAccountConnector"
            java.lang.String r2 = "Using global encryption key."
            com.avast.android.generic.util.t.b(r0, r2)
            java.lang.String r0 = "application/x-enc2"
            byte[] r9 = r7.f801a
        L_0x0015:
            r2 = 0
            org.apache.http.client.methods.HttpPost r3 = new org.apache.http.client.methods.HttpPost     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r4 = r7.A     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r3.<init>(r4)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r4 = "AvastAccountConnector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r6 = "Disconnecting device with GUID: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r6 = r7.w     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            com.avast.android.generic.util.t.b(r4, r5)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r4 = "AvastAccountConnector"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r6 = "Sending request to: "
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.net.URI r6 = r3.getURI()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r6 = r6.toString()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            com.avast.android.generic.util.t.b(r4, r5)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.m r4 = com.avast.android.generic.internet.c.a.l.d()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r4.a(r8)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.l r4 = r4.build()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            byte[] r4 = r4.toByteArray()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            byte[] r4 = r7.a(r9, r4)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            org.apache.http.entity.ByteArrayEntity r5 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r5.<init>(r4)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r3.setEntity(r5)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r4 = "Content-Type"
            r3.addHeader(r4, r0)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r0 = "GUID"
            java.lang.String r4 = r7.w     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            java.lang.String r5 = "-"
            java.lang.String r6 = ""
            java.lang.String r4 = r4.replaceAll(r5, r6)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r3.addHeader(r0, r4)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            android.a.a.a r0 = r7.q     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            org.apache.http.protocol.HttpContext r4 = r7.r     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            org.apache.http.HttpResponse r0 = r0.execute(r3, r4)     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            if (r0 == 0) goto L_0x00a2
            org.apache.http.StatusLine r3 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            if (r3 == 0) goto L_0x00a2
            org.apache.http.StatusLine r3 = r0.getStatusLine()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            int r3 = r3.getStatusCode()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            r4 = 200(0xc8, float:2.8E-43)
            if (r3 == r4) goto L_0x00a9
        L_0x00a2:
            if (r2 == 0) goto L_0x00a7
            r2.consumeContent()     // Catch:{ IOException -> 0x0154 }
        L_0x00a7:
            r0 = r1
        L_0x00a8:
            return r0
        L_0x00a9:
            org.apache.http.HttpEntity r2 = r0.getEntity()     // Catch:{ ClientProtocolException -> 0x015c, Exception -> 0x0129 }
            if (r2 != 0) goto L_0x00b6
            if (r2 == 0) goto L_0x00b4
            r2.consumeContent()     // Catch:{ IOException -> 0x0157 }
        L_0x00b4:
            r0 = r1
            goto L_0x00a8
        L_0x00b6:
            java.io.InputStream r0 = r2.getContent()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.o r0 = com.avast.android.generic.internet.c.a.o.a(r0)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            boolean r1 = r0.b()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            if (r1 == 0) goto L_0x014a
            int[] r1 = com.avast.android.generic.internet.c.d.f804a     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.q r3 = r0.c()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            int r3 = r3.ordinal()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            r1 = r1[r3]     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            switch(r1) {
                case 1: goto L_0x0106;
                case 2: goto L_0x0106;
                case 3: goto L_0x0106;
                case 4: goto L_0x0106;
                case 5: goto L_0x0132;
                case 6: goto L_0x0138;
                default: goto L_0x00d3;
            }     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
        L_0x00d3:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r4 = "Unknown result: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.q r0 = r0.c()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r3 = ""
            r1.<init>(r0, r3)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            throw r1     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
        L_0x00f2:
            r0 = move-exception
            r1 = r2
        L_0x00f4:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x00fe }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00fe }
            r2.<init>(r0)     // Catch:{ all -> 0x00fe }
            throw r2     // Catch:{ all -> 0x00fe }
        L_0x00fe:
            r0 = move-exception
            r2 = r1
        L_0x0100:
            if (r2 == 0) goto L_0x0105
            r2.consumeContent()     // Catch:{ IOException -> 0x015a }
        L_0x0105:
            throw r0
        L_0x0106:
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r4 = "Other error: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            com.avast.android.generic.internet.c.a.q r0 = r0.c()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r3 = ""
            r1.<init>(r0, r3)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            throw r1     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
        L_0x0129:
            r0 = move-exception
            com.avast.android.generic.internet.c.h r1 = new com.avast.android.generic.internet.c.h     // Catch:{ all -> 0x0130 }
            r1.<init>(r0)     // Catch:{ all -> 0x0130 }
            throw r1     // Catch:{ all -> 0x0130 }
        L_0x0130:
            r0 = move-exception
            goto L_0x0100
        L_0x0132:
            com.avast.android.generic.internet.c.b r0 = new com.avast.android.generic.internet.c.b     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            r0.<init>()     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            throw r0     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
        L_0x0138:
            java.lang.String r0 = "AvastAccountConnector"
            java.lang.String r1 = "Disconnected."
            com.avast.android.generic.util.t.b(r0, r1)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            r0 = 1
            if (r2 == 0) goto L_0x00a8
            r2.consumeContent()     // Catch:{ IOException -> 0x0147 }
            goto L_0x00a8
        L_0x0147:
            r1 = move-exception
            goto L_0x00a8
        L_0x014a:
            com.avast.android.generic.internet.c.h r0 = new com.avast.android.generic.internet.c.h     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            java.lang.String r1 = "No result"
            java.lang.String r3 = ""
            r0.<init>(r1, r3)     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
            throw r0     // Catch:{ ClientProtocolException -> 0x00f2, Exception -> 0x0129 }
        L_0x0154:
            r0 = move-exception
            goto L_0x00a7
        L_0x0157:
            r0 = move-exception
            goto L_0x00b4
        L_0x015a:
            r1 = move-exception
            goto L_0x0105
        L_0x015c:
            r0 = move-exception
            r1 = r2
            goto L_0x00f4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.c.c.a(java.lang.String, byte[]):boolean");
    }

    public void a() {
        if (this.q != null) {
            this.q.a();
        }
        this.q = null;
    }

    private byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[16];
        System.arraycopy(bArr, 0, bArr3, 0, 16);
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, new SecretKeySpec(bArr3, "AES/CBC/PKCS5Padding"), new IvParameterSpec(new byte[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}));
        return instance.doFinal(bArr2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private String a(byte[] bArr, String str) {
        return new String(b.a(l.a(bArr, str.replace("-", "")))).replace('+', '-').replace('/', '_');
    }

    private void a(Context context) {
        try {
            this.q = a.a("avast! Mobile Security");
            SchemeRegistry schemeRegistry = this.q.getConnectionManager().getSchemeRegistry();
            if (Build.VERSION.SDK_INT < 8) {
                schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            } else {
                g.a(context, schemeRegistry);
            }
        } catch (Exception e2) {
            a();
        }
    }
}
