package com.avast.android.generic.app.account;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import com.avast.android.generic.internet.c.a.aa;
import com.avast.android.generic.internet.c.e;
import com.avast.android.generic.internet.c.h;
import com.avast.android.generic.internet.c.i;
import com.avast.android.generic.internet.c.j;
import com.avast.android.generic.util.t;
import com.avast.android.generic.x;
import java.lang.reflect.Method;

/* compiled from: ConnectAccountHelper */
class v extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f461a;

    /* renamed from: b  reason: collision with root package name */
    private e f462b;

    /* renamed from: c  reason: collision with root package name */
    private String f463c;
    private String d;
    private String e;
    private String f;
    private String g;
    private long h;

    public v(r rVar, e eVar, String str, String str2, String str3, String str4, String str5, long j) {
        this.f461a = rVar;
        this.f462b = eVar;
        this.f463c = str;
        this.d = str2;
        this.e = str3;
        this.f = str4;
        this.g = str5;
        this.h = j;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x01ca  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x01e2  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x01ee  */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x026e  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0273  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0127  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01b2  */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:109:0x01d2=Splitter:B:109:0x01d2, B:104:0x01c6=Splitter:B:104:0x01c6, B:99:0x01ba=Splitter:B:99:0x01ba, B:119:0x01ea=Splitter:B:119:0x01ea, B:114:0x01de=Splitter:B:114:0x01de} */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean doInBackground(java.lang.Void... r18) {
        /*
            r17 = this;
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            java.lang.String r1 = r17.b()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 != 0) goto L_0x0014
            java.lang.String r2 = "my_avast_pairing_server_address"
            r3.putString(r2, r1)
        L_0x0014:
            r2 = 0
            r15 = 0
            r14 = 0
            r13 = 0
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            boolean r1 = com.avast.android.generic.util.aa.j(r1)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 == 0) goto L_0x0048
            com.avast.android.generic.internet.c.n r1 = new com.avast.android.generic.internet.c.n     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>()     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            throw r1     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
        L_0x002c:
            r1 = move-exception
        L_0x002d:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Cannot instantiate AvastAccountConnector."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            int r1 = com.avast.android.generic.x.msg_avast_account_error_connection     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
        L_0x003d:
            if (r2 == 0) goto L_0x01fc
            r17.a()
        L_0x0042:
            r1 = 0
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r1)
        L_0x0047:
            return r1
        L_0x0048:
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            boolean r1 = com.avast.android.generic.util.aa.a(r1)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 == 0) goto L_0x0082
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r4 = 4586(0x11ea, float:6.426E-42)
            boolean r1 = com.avast.android.generic.util.aa.a(r1, r4)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 != 0) goto L_0x0082
            com.avast.android.generic.internet.c.o r1 = new com.avast.android.generic.internet.c.o     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>()     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            throw r1     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
        L_0x006c:
            r1 = move-exception
        L_0x006d:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Authentication failed."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            r0 = r17
            int r1 = r0.a(r1)     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x0082:
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            java.lang.String r1 = com.avast.android.generic.util.aa.b(r1)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 == 0) goto L_0x00b8
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r4 = 4586(0x11ea, float:6.426E-42)
            java.lang.String r1 = com.avast.android.generic.util.aa.b(r1, r4)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 != 0) goto L_0x00b8
            com.avast.android.generic.internet.c.a r1 = new com.avast.android.generic.internet.c.a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>()     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            throw r1     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
        L_0x00a6:
            r1 = move-exception
        L_0x00a7:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Account already exists. Show the password field."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            r1 = 1
            if (r2 == 0) goto L_0x0273
            r2.a()
            r2 = r1
            r3 = r14
            r1 = r15
            goto L_0x003d
        L_0x00b8:
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            boolean r1 = com.avast.android.generic.util.aa.d(r1)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 == 0) goto L_0x00f3
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r4 = 4586(0x11ea, float:6.426E-42)
            boolean r1 = com.avast.android.generic.util.aa.c(r1, r4)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 != 0) goto L_0x00f3
            com.avast.android.generic.internet.c.k r1 = new com.avast.android.generic.internet.c.k     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>()     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            throw r1     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
        L_0x00dc:
            r1 = move-exception
        L_0x00dd:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Cannot create AUID."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            r0 = r17
            int r1 = r0.a(r1)     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x00f3:
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            boolean r1 = com.avast.android.generic.util.aa.e(r1)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 == 0) goto L_0x012e
            r0 = r17
            com.avast.android.generic.app.account.r r1 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r1 = r1.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r4 = 4586(0x11ea, float:6.426E-42)
            boolean r1 = com.avast.android.generic.util.aa.d(r1, r4)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            if (r1 != 0) goto L_0x012e
            com.avast.android.generic.internet.c.p r1 = new com.avast.android.generic.internet.c.p     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>()     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            throw r1     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
        L_0x0117:
            r1 = move-exception
        L_0x0118:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Other error."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            r0 = r17
            int r1 = r0.a(r1)     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x012e:
            com.avast.android.generic.internet.c.c r1 = new com.avast.android.generic.internet.c.c     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r0 = r17
            com.avast.android.generic.app.account.r r4 = r0.f461a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            android.content.Context r4 = r4.f449a     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r1.<init>(r4, r3)     // Catch:{ InstantiationException -> 0x002c, b -> 0x006c, l -> 0x00a6, m -> 0x00dc, h -> 0x0117, j -> 0x018e, IOException -> 0x01a6, a -> 0x01b9, o -> 0x01c5, n -> 0x01d1, k -> 0x01dd, p -> 0x01e9 }
            r0 = r17
            com.avast.android.generic.internet.c.e r2 = r0.f462b     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r3 = r0.f463c     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r4 = r0.d     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r5 = r0.e     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r6 = 0
            r7 = 0
            r8 = 0
            r0 = r17
            java.lang.String r9 = r0.f     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r10 = r0.g     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            long r11 = r0.h     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            com.avast.android.generic.internet.c.f r2 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r3 = r0.f463c     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            if (r3 != 0) goto L_0x016a
            java.lang.String r3 = r2.f     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            r0.f463c = r3     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
        L_0x016a:
            r0 = r17
            java.lang.String r3 = r0.f463c     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            java.lang.String r4 = r2.f808a     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            java.lang.String r5 = r2.f809b     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            java.lang.String r6 = r2.f810c     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            java.lang.String r7 = r2.d     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r0 = r17
            java.lang.String r8 = r0.f     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            java.lang.String r9 = r2.e     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r2 = r17
            r2.a(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            r2 = 1
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ InstantiationException -> 0x0262, b -> 0x025a, l -> 0x0252, m -> 0x024a, h -> 0x0242, j -> 0x023a, IOException -> 0x0232, a -> 0x022f, o -> 0x022c, n -> 0x0229, k -> 0x0226, p -> 0x0223, all -> 0x021c }
            if (r1 == 0) goto L_0x018b
            r1.a()
        L_0x018b:
            r1 = r2
            goto L_0x0047
        L_0x018e:
            r1 = move-exception
        L_0x018f:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Backend error"
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            r0 = r17
            java.lang.String r1 = r0.a(r1)     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026e
            r2.a()
            r2 = r13
            r3 = r1
            r1 = r15
            goto L_0x003d
        L_0x01a6:
            r1 = move-exception
        L_0x01a7:
            java.lang.String r3 = "ConnectAccountHelper"
            java.lang.String r4 = "Http communication error."
            com.avast.android.generic.util.t.a(r3, r4, r1)     // Catch:{ all -> 0x01f5 }
            int r1 = com.avast.android.generic.x.msg_avast_account_error_connection     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01b9:
            r1 = move-exception
        L_0x01ba:
            int r1 = com.avast.android.generic.x.msg_anti_theft_too_old     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01c5:
            r1 = move-exception
        L_0x01c6:
            int r1 = com.avast.android.generic.x.msg_mobile_security_too_old     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01d1:
            r1 = move-exception
        L_0x01d2:
            int r1 = com.avast.android.generic.x.msg_invalid_tool_combination     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01dd:
            r1 = move-exception
        L_0x01de:
            int r1 = com.avast.android.generic.x.msg_backup_too_old     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01e9:
            r1 = move-exception
        L_0x01ea:
            int r1 = com.avast.android.generic.x.msg_secureline_too_old     // Catch:{ all -> 0x01f5 }
            if (r2 == 0) goto L_0x026a
            r2.a()
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x01f5:
            r1 = move-exception
        L_0x01f6:
            if (r2 == 0) goto L_0x01fb
            r2.a()
        L_0x01fb:
            throw r1
        L_0x01fc:
            if (r3 == 0) goto L_0x0205
            r0 = r17
            r0.a(r3)
            goto L_0x0042
        L_0x0205:
            if (r1 != 0) goto L_0x0209
            int r1 = com.avast.android.generic.x.msg_avast_account_error_internal_error
        L_0x0209:
            r0 = r17
            com.avast.android.generic.app.account.r r2 = r0.f461a
            android.content.Context r2 = r2.f449a
            java.lang.String r1 = r2.getString(r1)
            r0 = r17
            r0.a(r1)
            goto L_0x0042
        L_0x021c:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x01f6
        L_0x0223:
            r2 = move-exception
            r2 = r1
            goto L_0x01ea
        L_0x0226:
            r2 = move-exception
            r2 = r1
            goto L_0x01de
        L_0x0229:
            r2 = move-exception
            r2 = r1
            goto L_0x01d2
        L_0x022c:
            r2 = move-exception
            r2 = r1
            goto L_0x01c6
        L_0x022f:
            r2 = move-exception
            r2 = r1
            goto L_0x01ba
        L_0x0232:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x01a7
        L_0x023a:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x018f
        L_0x0242:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x0118
        L_0x024a:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x00dd
        L_0x0252:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x00a7
        L_0x025a:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x006d
        L_0x0262:
            r2 = move-exception
            r16 = r2
            r2 = r1
            r1 = r16
            goto L_0x002d
        L_0x026a:
            r2 = r13
            r3 = r14
            goto L_0x003d
        L_0x026e:
            r2 = r13
            r3 = r1
            r1 = r15
            goto L_0x003d
        L_0x0273:
            r2 = r1
            r3 = r14
            r1 = r15
            goto L_0x003d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.account.v.doInBackground(java.lang.Void[]):java.lang.Boolean");
    }

    private int a(h hVar) {
        i a2 = hVar.a();
        if (a2 == null) {
            a2 = i.UNKNOWN_ERROR;
        }
        switch (a2) {
            case INVALID_EMAIL:
                return x.msg_avast_account_error_invalid_email;
            case INVALID_CREDENTIALS:
                return x.msg_avast_account_error_invalid_credentials;
            case PASSWORD_INVALID:
                return x.msg_avast_account_error_password_invalid;
            case EMAIL_ALREADY_USED:
                return x.msg_avast_account_error_email_already_used;
            default:
                return x.msg_avast_account_error_internal_error;
        }
    }

    private String a(j jVar) {
        aa a2 = jVar.a();
        if (a2 == null) {
            a2 = aa.GENERIC;
        }
        switch (a2) {
            case DEVICE_ALREADY_EXISTING:
                return this.f461a.f449a.getString(x.msg_avast_backend_error_device_already_existing);
            case DEVICE_TOO_OLD:
            case MISSING_CLIENT_VERSION:
            case TOO_LOW_CLIENT_VERSION:
                return this.f461a.f449a.getString(x.msg_avast_backend_error_client_too_old);
            case DEVICE_HAS_NO_NUMBER:
            case INVALID_PHONE_NUMBER:
                return this.f461a.f449a.getString(x.msg_avast_backend_error_number_invalid);
            case GENERIC:
                return this.f461a.f449a.getString(x.msg_avast_backend_error_generic);
            default:
                return this.f461a.f449a.getString(x.msg_avast_backend_error_internal_error, a2.name());
        }
    }

    private void a(String str) {
        new Handler(Looper.getMainLooper()).post(new w(this, str));
    }

    private void a() {
        new Handler(Looper.getMainLooper()).post(new x(this));
    }

    private void a(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        new Handler(Looper.getMainLooper()).post(new y(this, str, str2, str3, str4, str5, str6, str7));
    }

    private String b() {
        Method method;
        String str;
        try {
            method = Class.forName("com.avast.android.mobilesecurity.app.account.ServerAddressHelper").getMethod("getPairingServerAddress", Context.class);
        } catch (Exception e2) {
            t.a("ConnectAccountHelper", "ServerAddressHelper not available.", e2);
            method = null;
        }
        if (method != null) {
            try {
                str = (String) method.invoke(null, this.f461a.f449a);
            } catch (Exception e3) {
                t.a("ConnectAccountHelper", "Invocation of ServerAddressHelper.getPairingServerAddress() failed.", e3);
                return null;
            }
        } else {
            str = null;
        }
        return str;
    }
}
