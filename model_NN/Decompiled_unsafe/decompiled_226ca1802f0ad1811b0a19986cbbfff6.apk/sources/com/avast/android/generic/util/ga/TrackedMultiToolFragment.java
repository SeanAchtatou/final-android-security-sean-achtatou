package com.avast.android.generic.util.ga;

import android.os.Bundle;
import android.view.View;

public abstract class TrackedMultiToolFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private d f1228a = null;

    public abstract String c();

    public final String b() {
        return this.f1228a.a(c());
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (isAdded()) {
            this.f1228a = new d(getActivity());
        }
    }

    public boolean j() {
        return this.f1228a.a();
    }

    public boolean k() {
        return this.f1228a.b();
    }

    public boolean l() {
        return this.f1228a.c();
    }

    public boolean m() {
        return this.f1228a.d();
    }
}
