package com.avast.android.generic.app.account;

import android.os.Bundle;
import android.view.View;
import com.avast.android.generic.app.about.FeedbackActivity;
import com.avast.android.generic.util.a;

/* compiled from: ConnectionCheckFragment */
class af implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectionCheckFragment f381a;

    af(ConnectionCheckFragment connectionCheckFragment) {
        this.f381a = connectionCheckFragment;
    }

    public void onClick(View view) {
        Bundle bundle = null;
        try {
            bundle = this.f381a.getActivity().getIntent().getExtras();
        } catch (Exception e) {
        }
        FeedbackActivity.call(this.f381a.getActivity(), bundle);
        a.a(this.f381a);
    }
}
