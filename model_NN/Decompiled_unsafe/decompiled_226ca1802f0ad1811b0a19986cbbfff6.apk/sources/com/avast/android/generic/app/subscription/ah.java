package com.avast.android.generic.app.subscription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/* compiled from: WelcomePremiumFragment */
class ah extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomePremiumFragment f535a;

    ah(WelcomePremiumFragment welcomePremiumFragment) {
        this.f535a = welcomePremiumFragment;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
            Uri data = intent.getData();
            if (data != null) {
                String substring = data.toString().substring(data.toString().indexOf(":") + 1);
                if (substring.equals("com.avast.android.mobilesecurity") || substring.equals("com.avast.android.at_play") || substring.equals("com.avast.android.antitheft") || substring.equals("com.avast.android.backup")) {
                    this.f535a.f();
                    return;
                }
                return;
            }
            return;
        }
        this.f535a.f();
    }
}
