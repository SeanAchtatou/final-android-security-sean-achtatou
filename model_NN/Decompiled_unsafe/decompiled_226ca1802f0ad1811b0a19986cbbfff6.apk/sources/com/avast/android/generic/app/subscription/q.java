package com.avast.android.generic.app.subscription;

import android.widget.Toast;
import com.avast.android.generic.licensing.c.g;
import com.avast.android.generic.licensing.c.i;
import com.avast.android.generic.licensing.c.l;
import com.avast.android.generic.licensing.c.m;
import com.avast.android.generic.licensing.database.d;
import com.avast.android.generic.util.b;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;

/* compiled from: SubscriptionFragment */
class q implements g {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f570a;

    q(SubscriptionFragment subscriptionFragment) {
        this.f570a = subscriptionFragment;
    }

    public void a(i iVar, l lVar) {
        if (iVar.c()) {
            z.a("AvastGenericLic", "Purchase was successful");
            if (lVar.e() == m.PURCHASED) {
                new d(this.f570a.r, this.f570a.e()).a(lVar.b(), lVar, null, true);
                b.a(new r(this, lVar), new Void[0]);
            } else if (lVar.e() == m.CANCELED) {
                z.a("AvastGenericLic", "Purchase failed, credit card charge was cancelled");
                Toast.makeText(this.f570a.r, x.msg_subscription_error_purchase_cancelled_message, 1).show();
                this.f570a.d();
                this.f570a.L.a(com.avast.android.generic.util.i.PURCHASE_CANCELLED);
            }
        } else if (iVar.a() == -1005) {
            z.a("AvastGenericLic", "User canceled purchase");
            Toast.makeText(this.f570a.r, x.msg_subscription_error_purchase_cancelled, 1).show();
            this.f570a.d();
            this.f570a.L.a(com.avast.android.generic.util.i.USER_CANCELLED);
        } else {
            z.a("AvastGenericLic", "Purchase failed, code: " + iVar.a() + ", " + "message: " + iVar.b());
            this.f570a.d();
            Toast.makeText(this.f570a.r, x.msg_subscription_error_purchase_failed_message, 1).show();
            this.f570a.L.a(com.avast.android.generic.util.i.PURCHASE_FAILED);
        }
    }
}
