package com.avast.android.generic.app.account;

import android.content.DialogInterface;

/* compiled from: ConnectAccountFragment */
class j implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f437a;

    j(ConnectAccountFragment connectAccountFragment) {
        this.f437a = connectAccountFragment;
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.f437a.f360c != null) {
            this.f437a.f360c.b();
        }
    }
}
