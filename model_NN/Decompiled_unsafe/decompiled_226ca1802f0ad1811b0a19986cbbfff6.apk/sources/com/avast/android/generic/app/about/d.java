package com.avast.android.generic.app.about;

import android.os.Environment;
import android.view.View;
import android.widget.Toast;
import com.avast.android.generic.util.t;
import com.avast.android.generic.x;
import java.io.File;
import java.io.IOException;

/* compiled from: AboutFragment */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutFragment f338a;

    /* renamed from: b  reason: collision with root package name */
    private int f339b = 0;

    d(AboutFragment aboutFragment) {
        this.f338a = aboutFragment;
    }

    public void onClick(View view) {
        int i = this.f339b + 1;
        this.f339b = i;
        switch (i) {
            case 3:
                Toast.makeText(this.f338a.getActivity(), this.f338a.getActivity().getString(x.msg_debug_mode_two_taps), 0).show();
                return;
            case 4:
            default:
                return;
            case 5:
                a();
                this.f339b = 0;
                return;
        }
    }

    private void a() {
        File file = new File(Environment.getExternalStorageDirectory(), "avast-debug");
        if (t.a()) {
            if ("mounted".equals(Environment.getExternalStorageState())) {
                file.delete();
            }
            t.a(false);
            Toast.makeText(this.f338a.getActivity(), this.f338a.getActivity().getString(x.msg_debug_mode_disabled), 0).show();
            return;
        }
        if ("mounted".equals(Environment.getExternalStorageState())) {
            try {
                file.createNewFile();
            } catch (IOException e) {
            }
        }
        t.a(true);
        Toast.makeText(this.f338a.getActivity(), this.f338a.getActivity().getString(x.msg_debug_mode_enabled), 0).show();
    }
}
