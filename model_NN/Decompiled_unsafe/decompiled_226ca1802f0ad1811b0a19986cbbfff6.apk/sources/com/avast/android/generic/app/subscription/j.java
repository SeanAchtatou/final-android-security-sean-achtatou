package com.avast.android.generic.app.subscription;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.avast.android.generic.licensing.c.k;
import com.avast.android.generic.util.z;

/* compiled from: SubscriptionFragment */
class j extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f552a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ k f553b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ WebView f554c;
    final /* synthetic */ ProgressBar d;
    final /* synthetic */ SubscriptionFragment e;

    j(SubscriptionFragment subscriptionFragment, AlertDialog alertDialog, k kVar, WebView webView, ProgressBar progressBar) {
        this.e = subscriptionFragment;
        this.f552a = alertDialog;
        this.f553b = kVar;
        this.f554c = webView;
        this.d = progressBar;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return false;
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        z.a("AvastGenericLic", "Advertising received web view error " + i + " (" + str + ", " + str2 + ")");
        if (this.e.isAdded()) {
            this.f552a.cancel();
            this.e.b(this.f553b);
        }
    }

    @SuppressLint({"NewApi"})
    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        if (Build.VERSION.SDK_INT >= 8) {
            z.a("AvastGenericLic", "Advertising received web view SSL error " + sslError.getPrimaryError());
        }
        if (this.e.isAdded()) {
            this.f552a.cancel();
            this.e.b(this.f553b);
        }
    }

    public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        z.a("AvastGenericLic", "Advertising web view starts loading URL " + str);
    }

    public void onPageFinished(WebView webView, String str) {
        z.a("AvastGenericLic", "Advertising web view finished loading URL " + str);
        if (this.e.isAdded()) {
            this.f554c.setVisibility(0);
            this.d.setVisibility(8);
        }
    }
}
