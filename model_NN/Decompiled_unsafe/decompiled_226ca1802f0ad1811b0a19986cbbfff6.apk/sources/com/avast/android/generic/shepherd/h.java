package com.avast.android.generic.shepherd;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.avast.android.generic.ab;
import com.avast.android.generic.licensing.PremiumHelper;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.s;
import com.avast.e.a.ad;
import com.avast.e.a.af;
import com.avast.e.a.ag;
import com.avast.e.a.ai;
import com.avast.e.a.ak;
import com.avast.e.a.al;
import com.avast.e.a.an;
import com.avast.e.a.ao;
import com.avast.e.a.aq;
import com.avast.e.a.ar;
import com.avast.e.a.az;
import com.avast.e.a.ba;
import com.avast.e.a.v;
import com.avast.e.a.w;
import com.avast.e.a.y;
import com.google.protobuf.ByteString;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.UUID;

/* compiled from: ShepherdParamsHelper */
class h {
    protected static az a(Context context, ab abVar) {
        ba o = az.o();
        o.a(b.a().a());
        HashSet<String> aa = abVar.aa();
        if (aa != null && !aa.isEmpty()) {
            Iterator<String> it = aa.iterator();
            while (it.hasNext()) {
                o.a(ByteString.copyFromUtf8(it.next()));
            }
        }
        o.a(a(context));
        if (aa.a(context)) {
            o.a(b(context));
        }
        if (!(aa.b(context) == null && aa.c(context) == null)) {
            o.a(c(context));
        }
        if (aa.d(context)) {
            o.a(d(context));
        }
        return o.build();
    }

    private static ao a(Context context) {
        ao l = an.l();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (!(telephonyManager == null || telephonyManager.getSimOperatorName() == null)) {
            l.b(ByteString.copyFromUtf8(telephonyManager.getSimOperatorName()));
        }
        if (!"".equals(Locale.getDefault().getCountry())) {
            l.a(ByteString.copyFromUtf8(Locale.getDefault().getCountry()));
        }
        al l2 = ak.l();
        l2.a(ByteString.copyFromUtf8(Build.VERSION.RELEASE));
        l2.d(ByteString.copyFromUtf8(Build.BRAND));
        l2.b(ByteString.copyFromUtf8(Build.ID));
        l2.e(ByteString.copyFromUtf8(Build.MANUFACTURER));
        l2.c(ByteString.copyFromUtf8(Build.MODEL));
        l.a(l2);
        return l;
    }

    private static ai b(Context context) {
        ai h = af.h();
        ar j = aq.j();
        if (c.MOBILE_SECURITY.equals(b.a())) {
            h.a(PremiumHelper.a(context) ? ag.PREMIUM : ag.FREE);
            j.c(ByteString.copyFromUtf8(Locale.getDefault().getLanguage()));
            String c2 = ((ab) com.avast.android.generic.aa.a(context, ab.class)).c(context);
            if (!TextUtils.isEmpty(c2)) {
                j.d(ByteString.copyFromUtf8(c2));
            }
            j.b(e(context));
        }
        try {
            j.a(ByteString.copyFromUtf8(context.getPackageManager().getPackageInfo("com.avast.android.mobilesecurity", 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
        }
        h.a(j);
        if (b.b().containsKey("intent.extra.ams.VPS_VERSION")) {
            h.a(ByteString.copyFromUtf8(b.b().getString("intent.extra.ams.VPS_VERSION")));
        }
        return h;
    }

    private static y c(Context context) {
        y f = v.f();
        ar j = aq.j();
        if (c.ANTI_THEFT.equals(b.a())) {
            boolean a2 = PremiumHelper.a(context);
            if (aa.a(context, "com.avast.android.at_play")) {
                f.a(a2 ? w.SIMPLE_PREMIUM : w.SIMPLE_FREE);
            } else {
                f.a(a2 ? w.ADVANCED_PREMIUM : w.ADVANCED_FREE);
            }
            j.c(ByteString.copyFromUtf8(Locale.getDefault().getLanguage()));
            String c2 = ((ab) com.avast.android.generic.aa.a(context, ab.class)).c(context);
            if (!TextUtils.isEmpty(c2)) {
                j.d(ByteString.copyFromUtf8(c2));
            }
            j.b(e(context));
        }
        try {
            j.a(ByteString.copyFromUtf8(context.getPackageManager().getPackageInfo("com.avast.android.at_play", 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
            try {
                j.a(ByteString.copyFromUtf8(context.getPackageManager().getPackageInfo("com.avast.android.antitheft", 0).versionName));
            } catch (PackageManager.NameNotFoundException e2) {
            }
        }
        f.a(j);
        return f;
    }

    private static ad d(Context context) {
        ad f = com.avast.e.a.aa.f();
        ar j = aq.j();
        if (c.BACKUP.equals(b.a())) {
            f.a(PremiumHelper.a(context) ? com.avast.e.a.ab.PREMIUM : com.avast.e.a.ab.FREE);
            j.c(ByteString.copyFromUtf8(Locale.getDefault().getLanguage()));
            String c2 = ((ab) com.avast.android.generic.aa.a(context, ab.class)).c(context);
            if (!TextUtils.isEmpty(c2)) {
                j.d(ByteString.copyFromUtf8(c2));
            }
            j.b(e(context));
        }
        try {
            j.a(ByteString.copyFromUtf8(context.getPackageManager().getPackageInfo("com.avast.android.backup", 0).versionName));
        } catch (PackageManager.NameNotFoundException e) {
        }
        f.a(j);
        return f;
    }

    private static ByteString e(Context context) {
        String s = ((ab) com.avast.android.generic.aa.a(context, ab.class)).s();
        if (s.a(context)) {
            s = "00000000-0000-0000-0000-000000000000";
        }
        ByteBuffer allocate = ByteBuffer.allocate(16);
        long leastSignificantBits = UUID.fromString(s).getLeastSignificantBits();
        long mostSignificantBits = UUID.fromString(s).getMostSignificantBits();
        allocate.putLong(leastSignificantBits);
        allocate.putLong(mostSignificantBits);
        return ByteString.copyFrom(allocate.array());
    }
}
