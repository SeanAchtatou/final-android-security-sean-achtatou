package com.avast.android.generic.ui;

import android.view.View;
import android.widget.TextView;

/* compiled from: ChangePasswordDialog */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ com.avast.android.generic.ui.b.a f999a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ TextView f1000b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ TextView f1001c;
    final /* synthetic */ CharSequence d;
    final /* synthetic */ CharSequence e;
    final /* synthetic */ ChangePasswordDialog f;

    a(ChangePasswordDialog changePasswordDialog, com.avast.android.generic.ui.b.a aVar, TextView textView, TextView textView2, CharSequence charSequence, CharSequence charSequence2) {
        this.f = changePasswordDialog;
        this.f999a = aVar;
        this.f1000b = textView;
        this.f1001c = textView2;
        this.d = charSequence;
        this.e = charSequence2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.ChangePasswordDialog.a(com.avast.android.generic.ui.ChangePasswordDialog, boolean):boolean
     arg types: [com.avast.android.generic.ui.ChangePasswordDialog, int]
     candidates:
      com.avast.android.generic.ui.ChangePasswordDialog.a(com.avast.android.generic.ui.ChangePasswordDialog, int):void
      com.avast.android.generic.ui.ChangePasswordDialog.a(java.lang.String, java.lang.String):boolean
      com.avast.android.generic.ui.ChangePasswordDialog.a(com.avast.android.generic.ui.ChangePasswordDialog, boolean):boolean */
    public void onClick(View view) {
        if (!this.f.f) {
            this.f999a.a(false);
            this.f1000b.startAnimation(this.f999a);
            boolean unused = this.f.f = true;
            this.f1001c.setText(this.d);
            return;
        }
        this.f999a.a(true);
        this.f1000b.startAnimation(this.f999a);
        boolean unused2 = this.f.f = false;
        this.f1001c.setText(this.e);
    }
}
