package com.avast.android.generic;

/* compiled from: R */
public final class r {
    public static final int A = 2131361938;

    /* renamed from: B */
    public static final int b_display = 2131361955;

    /* renamed from: C */
    public static final int b_dont_agree = 2131361964;

    /* renamed from: D */
    public static final int b_feedback = 2131361929;

    /* renamed from: E */
    public static final int b_ok = 2131362032;

    /* renamed from: F */
    public static final int b_privacy_policy = 2131361961;

    /* renamed from: G */
    public static final int b_revoke_ignore = 2131362039;

    /* renamed from: H */
    public static final int b_sign_in_email = 2131361997;

    /* renamed from: I */
    public static final int b_sign_in_facebook = 2131361996;

    /* renamed from: J */
    public static final int button = 2131362054;

    /* renamed from: K */
    public static final int button_refresh_licenses = 2131362007;

    /* renamed from: L */
    public static final int button_subscription_button = 2131361878;

    /* renamed from: M */
    public static final int button_subscription_progress = 2131361879;

    /* renamed from: N */
    public static final int c_next_arrow = 2131362058;

    /* renamed from: O */
    public static final int checkbox = 2131362055;

    /* renamed from: P */
    public static final int debug = 2131362016;

    /* renamed from: Q */
    public static final int dialog_custom_number_advanced = 2131361909;

    /* renamed from: R */
    public static final int dialog_custom_number_advanced_checkbox = 2131361908;

    /* renamed from: S */
    public static final int dialog_custom_number_error_message = 2131361906;

    /* renamed from: T */
    public static final int dialog_custom_number_text = 2131361907;

    /* renamed from: U */
    public static final int edit = 2131362056;

    /* renamed from: V */
    public static final int error_view = 2131362002;

    /* renamed from: W */
    public static final int feedback_button_cancel = 2131361977;

    /* renamed from: X */
    public static final int feedback_button_send = 2131361978;

    /* renamed from: Y */
    public static final int feedback_checkbox_ciq = 2131361974;

    /* renamed from: Z */
    public static final int feedback_checkbox_dumpsys = 2131361976;

    /* renamed from: a */
    public static final int about_app_name = 2131361925;

    /* renamed from: aA */
    public static final int menu_voucher = 2131362134;

    /* renamed from: aB */
    public static final int message_avast_account_conncheck_pin_check = 2131361825;

    /* renamed from: aC */
    public static final int message_avast_account_disconnected = 2131361827;

    /* renamed from: aD */
    public static final int message_filter_custom_number_entered = 2131361828;

    /* renamed from: aE */
    public static final int message_language_changed = 2131361829;

    /* renamed from: aF */
    public static final int message_passwordProtector = 2131361830;

    /* renamed from: aG */
    public static final int message_password_change_canceled = 2131361831;

    /* renamed from: aH */
    public static final int message_password_change_successful = 2131361832;

    /* renamed from: aI */
    public static final int message_recovery_number_change_pin_check_after_description = 2131361834;

    /* renamed from: aJ */
    public static final int message_voucher_canceled = 2131361837;

    /* renamed from: aK */
    public static final int message_voucher_successful = 2131361838;

    /* renamed from: aL */
    public static final int name = 2131362035;

    /* renamed from: aM */
    public static final int password1 = 2131361911;

    /* renamed from: aN */
    public static final int password2 = 2131361918;

    /* renamed from: aO */
    public static final int passwordErrorMessage = 2131361917;

    /* renamed from: aP */
    public static final int passwordImage1 = 2131361912;

    /* renamed from: aQ */
    public static final int passwordImage2 = 2131361919;

    /* renamed from: aR */
    public static final int password_desc = 2131361916;

    /* renamed from: aS */
    public static final int password_recovery_subtitle = 2131361914;

    /* renamed from: aT */
    public static final int password_recovery_title = 2131361913;

    /* renamed from: aU */
    public static final int password_show_desc = 2131361915;
    public static final int aV = 2131361958;

    /* renamed from: aW */
    public static final int purchase_web = 2131362006;

    /* renamed from: aX */
    public static final int purchase_web_container = 2131362005;

    /* renamed from: aY */
    public static final int r_communityIQ = 2131361963;

    /* renamed from: aZ */
    public static final int r_subscriptionTest = 2131362017;

    /* renamed from: aa */
    public static final int feedback_checkbox_log = 2131361975;

    /* renamed from: ab */
    public static final int feedback_description = 2131361973;

    /* renamed from: ac */
    public static final int feedback_description_error_message = 2131361972;

    /* renamed from: ad */
    public static final int feedback_description_label = 2131361971;

    /* renamed from: ae */
    public static final int feedback_email = 2131361968;

    /* renamed from: af */
    public static final int feedback_email_error_message = 2131361967;

    /* renamed from: ag */
    public static final int feedback_email_label = 2131361966;

    /* renamed from: ah */
    public static final int feedback_name = 2131361969;

    /* renamed from: ai */
    public static final int feedback_type = 2131361970;

    /* renamed from: aj */
    public static final int filebrowser_button_pick = 2131361982;

    /* renamed from: ak */
    public static final int filebrowser_empty = 2131361980;

    /* renamed from: al */
    public static final int filebrowser_path = 2131361981;

    /* renamed from: am */
    public static final int filebrowser_progress = 2131361979;

    /* renamed from: an */
    public static final int fullimage = 2131362034;

    /* renamed from: ao */
    public static final int hidden_debug_mode = 2131361924;

    /* renamed from: ap */
    public static final int icon = 2131362045;

    /* renamed from: aq */
    public static final int image = 2131362041;

    /* renamed from: ar */
    public static final int l_checker_empty = 2131362040;

    /* renamed from: as */
    public static final int l_connection_check_state = 2131361956;

    /* renamed from: at */
    public static final int l_disconnect_warning = 2131361904;

    /* renamed from: au */
    public static final int l_header = 2131361934;

    /* renamed from: av */
    public static final int list_item_notification_empty = 2131362047;

    /* renamed from: aw */
    public static final int list_item_row = 2131362048;

    /* renamed from: ax */
    public static final int list_notifications = 2131361932;

    /* renamed from: ay */
    public static final int menu_about_rate = 2131362132;

    /* renamed from: az */
    public static final int menu_select = 2131362131;

    /* renamed from: b */
    public static final int about_eula = 2131361930;

    /* renamed from: bA */
    public static final int subscription_welcome_button_install_at = 2131362022;

    /* renamed from: bB */
    public static final int subscription_welcome_button_install_backup = 2131362024;

    /* renamed from: bC */
    public static final int subscription_welcome_button_signup = 2131362026;

    /* renamed from: bD */
    public static final int subscription_welcome_group_ams_installed = 2131362021;

    /* renamed from: bE */
    public static final int subscription_welcome_group_at_installed = 2131362023;

    /* renamed from: bF */
    public static final int subscription_welcome_group_backup_installed = 2131362025;

    /* renamed from: bG */
    public static final int subscription_welcome_group_signedin = 2131362027;

    /* renamed from: bH */
    public static final int subscription_welcome_signed_in_text = 2131362028;

    /* renamed from: bI */
    public static final int text = 2131361960;

    /* renamed from: bJ */
    public static final int textAppVersion = 2131361926;

    /* renamed from: bK */
    public static final int textVpsDefinitions = 2131361928;

    /* renamed from: bL */
    public static final int textVpsVersion = 2131361927;
    public static final int bM = 2131361957;

    /* renamed from: bN */
    public static final int titlebar = 2131361933;

    /* renamed from: bO */
    public static final int v_community_iq = 2131361962;

    /* renamed from: bP */
    public static final int voucherCode = 2131361921;

    /* renamed from: bQ */
    public static final int voucherErrorMessage = 2131361920;

    /* renamed from: bR */
    public static final int voucherImage = 2131361922;

    /* renamed from: bS */
    public static final int wizard_account_info = 2131362031;

    /* renamed from: ba */
    public static final int r_web_view = 2131362018;

    /* renamed from: bb */
    public static final int root_container = 2131361876;

    /* renamed from: bc */
    public static final int row_icon = 2131362057;

    /* renamed from: bd */
    public static final int row_subscription_progress = 2131362061;

    /* renamed from: be */
    public static final int row_subscription_state = 2131362060;

    /* renamed from: bf */
    public static final int row_subtitle = 2131361839;

    /* renamed from: bg */
    public static final int row_switch = 2131362062;

    /* renamed from: bh */
    public static final int row_title = 2131361840;

    /* renamed from: bi */
    public static final int rtl_support_view_converted_tag = 2131361841;

    /* renamed from: bj */
    public static final int subscription_button_manage = 2131362015;

    /* renamed from: bk */
    public static final int subscription_button_retry = 2131362003;

    /* renamed from: bl */
    public static final int subscription_disclaimer = 2131362009;

    /* renamed from: bm */
    public static final int subscription_disclaimer_show = 2131362008;

    /* renamed from: bn */
    public static final int subscription_group_invalid = 2131361999;

    /* renamed from: bo */
    public static final int subscription_group_processing = 2131362010;

    /* renamed from: bp */
    public static final int subscription_group_valid = 2131362012;

    /* renamed from: bq */
    public static final int subscription_issues_support = 2131362029;

    /* renamed from: br */
    public static final int subscription_loading_text = 2131362011;

    /* renamed from: bs */
    public static final int subscription_manage = 2131362014;

    /* renamed from: bt */
    public static final int subscription_offers = 2131362004;

    /* renamed from: bu */
    public static final int subscription_offers_loading = 2131362000;

    /* renamed from: bv */
    public static final int subscription_progress = 2131362001;

    /* renamed from: bw */
    public static final int subscription_scrollview = 2131361998;

    /* renamed from: bx */
    public static final int subscription_valid_until = 2131362013;

    /* renamed from: by */
    public static final int subscription_welcome_button_go_back = 2131362019;

    /* renamed from: bz */
    public static final int subscription_welcome_button_install_ams = 2131362020;

    /* renamed from: c */
    public static final int about_eula_text = 2131361923;

    /* renamed from: d */
    public static final int advertising_progress = 2131361902;

    /* renamed from: e */
    public static final int advertising_webView = 2131361903;

    /* renamed from: f */
    public static final int arrow = 2131362046;

    /* renamed from: g */
    public static final int avast_account_button_connect = 2131361954;

    /* renamed from: h */
    public static final int avast_account_forgot_password = 2131361952;

    /* renamed from: i */
    public static final int avast_account_has_phone_number = 2131361940;

    /* renamed from: j */
    public static final int avast_account_has_phone_number_wrapper = 2131361939;

    /* renamed from: k */
    public static final int avast_account_password1 = 2131361951;

    /* renamed from: l */
    public static final int avast_account_password_wrapper = 2131361949;

    /* renamed from: m */
    public static final int avast_account_phone_number = 2131361947;

    /* renamed from: n */
    public static final int avast_account_phone_numberImage = 2131361948;

    /* renamed from: o */
    public static final int avast_account_phone_number_error_message = 2131361946;

    /* renamed from: p */
    public static final int avast_account_phone_number_wrapper = 2131361945;

    /* renamed from: q */
    public static final int avast_account_setup_hint_wrapper = 2131361953;

    /* renamed from: r */
    public static final int avast_account_username = 2131361943;

    /* renamed from: s */
    public static final int avast_account_usernameImage = 2131361944;

    /* renamed from: t */
    public static final int avast_account_username_error_message = 2131361942;

    /* renamed from: u */
    public static final int avast_account_username_wrapper = 2131361941;

    /* renamed from: v */
    public static final int b_agree = 2131361965;

    /* renamed from: w */
    public static final int b_buttons_checker = 2131362037;

    /* renamed from: x */
    public static final int b_buttons_revoke_ignore = 2131362038;
    public static final int y = 2131361937;

    /* renamed from: z */
    public static final int b_clear = 2131361931;
}
