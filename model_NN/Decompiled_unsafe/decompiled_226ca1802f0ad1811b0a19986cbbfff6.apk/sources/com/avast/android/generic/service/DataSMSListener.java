package com.avast.android.generic.service;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.app.passwordrecovery.a;
import com.avast.android.generic.c.d;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.z;
import java.util.HashMap;

public class DataSMSListener extends BroadcastReceiver {
    @SuppressLint({"UseSparseArrays"})

    /* renamed from: a  reason: collision with root package name */
    private static HashMap<String, b> f955a = new HashMap<>();

    /* JADX WARNING: Removed duplicated region for block: B:109:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0122 A[SYNTHETIC, Splitter:B:47:0x0122] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x017e A[SYNTHETIC, Splitter:B:77:0x017e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r13, android.content.Intent r14) {
        /*
            r12 = this;
            r11 = 10
            r0 = 1
            r1 = 0
            java.lang.String r2 = r14.getAction()
            java.lang.String r3 = "AvastComms"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Received intent in DATA SMS received listener: "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r2)
            java.lang.String r4 = r4.toString()
            com.avast.android.generic.util.z.a(r3, r13, r4)
            android.content.Context r4 = r13.getApplicationContext()
            android.os.Bundle r3 = r14.getExtras()
            java.lang.String r5 = "android.intent.action.DATA_SMS_RECEIVED"
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x0036
            android.net.Uri r2 = r14.getData()
            if (r2 != 0) goto L_0x0037
        L_0x0036:
            return
        L_0x0037:
            java.lang.String r5 = "AvastComms"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Target URI: "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r6 = r6.append(r2)
            java.lang.String r6 = r6.toString()
            com.avast.android.generic.util.z.a(r5, r4, r6)
            int r5 = r2.getPort()
            int[] r6 = r12.a()
            int r7 = r6.length
            r2 = r1
        L_0x0059:
            if (r2 >= r7) goto L_0x0192
            r8 = r6[r2]
            if (r8 != r5) goto L_0x0080
        L_0x005f:
            if (r0 != 0) goto L_0x0083
            java.lang.String r0 = "AvastComms"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Breaking because port "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r5)
            java.lang.String r2 = " was not found"
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.avast.android.generic.util.z.a(r0, r4, r1)
            goto L_0x0036
        L_0x0080:
            int r2 = r2 + 1
            goto L_0x0059
        L_0x0083:
            if (r3 == 0) goto L_0x0036
            java.lang.String r0 = "pdus"
            java.lang.Object r0 = r3.get(r0)
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            java.lang.Object[] r0 = (java.lang.Object[]) r0
            r2 = r1
        L_0x0090:
            int r1 = r0.length
            if (r2 >= r1) goto L_0x0182
            r1 = r0[r2]
            byte[] r1 = (byte[]) r1
            byte[] r1 = (byte[]) r1
            android.telephony.SmsMessage r3 = android.telephony.SmsMessage.createFromPdu(r1)
            r1 = 0
            java.lang.String r5 = r3.getOriginatingAddress()     // Catch:{ Exception -> 0x0190, all -> 0x017a }
            if (r5 != 0) goto L_0x00ac
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ IOException -> 0x00aa }
            goto L_0x0036
        L_0x00aa:
            r0 = move-exception
            goto L_0x0036
        L_0x00ac:
            byte[] r6 = r3.getUserData()     // Catch:{ Exception -> 0x0190, all -> 0x017a }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0190, all -> 0x017a }
            r3.<init>(r6)     // Catch:{ Exception -> 0x0190, all -> 0x017a }
            r1 = 4
            byte[] r1 = new byte[r1]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r3.read(r1)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r6 = r6.length     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r6 = r6 + -4
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r3.read(r6)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r7 = 0
            byte r7 = r1[r7]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r8 = 1
            byte r8 = r1[r8]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r8 = r8 << 8
            r7 = r7 | r8
            short r7 = (short) r7
            r8 = -32768(0xffffffffffff8000, float:NaN)
            if (r7 < r8) goto L_0x00d5
            r8 = 32767(0x7fff, float:4.5916E-41)
            if (r7 <= r8) goto L_0x00df
        L_0x00d5:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x00dc }
            goto L_0x0036
        L_0x00dc:
            r0 = move-exception
            goto L_0x0036
        L_0x00df:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r8.<init>()     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.StringBuilder r8 = r8.append(r5)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.String r9 = "_"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.StringBuilder r7 = r8.append(r7)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.Byte r8 = new java.lang.Byte     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r9 = 2
            byte r9 = r1[r9]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r8.<init>(r9)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.Byte r9 = new java.lang.Byte     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r10 = 3
            byte r1 = r1[r10]     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r9.<init>(r1)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r1 = r9.intValue()     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            if (r1 > r11) goto L_0x0112
            if (r1 > 0) goto L_0x011c
        L_0x0112:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0119 }
            goto L_0x0036
        L_0x0119:
            r0 = move-exception
            goto L_0x0036
        L_0x011c:
            if (r8 > r11) goto L_0x0120
            if (r8 > 0) goto L_0x012a
        L_0x0120:
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0127 }
            goto L_0x0036
        L_0x0127:
            r0 = move-exception
            goto L_0x0036
        L_0x012a:
            if (r8 <= r1) goto L_0x0136
            if (r3 == 0) goto L_0x0036
            r3.close()     // Catch:{ IOException -> 0x0133 }
            goto L_0x0036
        L_0x0133:
            r0 = move-exception
            goto L_0x0036
        L_0x0136:
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r9 = com.avast.android.generic.service.DataSMSListener.f955a     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            boolean r9 = r9.containsKey(r7)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            if (r9 == 0) goto L_0x0156
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r1 = com.avast.android.generic.service.DataSMSListener.f955a     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            com.avast.android.generic.service.b r1 = (com.avast.android.generic.service.b) r1     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            byte[][] r1 = r1.f964c     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r5 = r8 + -1
            r1[r5] = r6     // Catch:{ Exception -> 0x0167, all -> 0x018b }
        L_0x014c:
            if (r3 == 0) goto L_0x0151
            r3.close()     // Catch:{ IOException -> 0x0187 }
        L_0x0151:
            int r1 = r2 + 1
            r2 = r1
            goto L_0x0090
        L_0x0156:
            com.avast.android.generic.service.b r9 = new com.avast.android.generic.service.b     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r9.<init>(r12, r5, r1)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            byte[][] r1 = r9.f964c     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            int r5 = r8 + -1
            r1[r5] = r6     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r1 = com.avast.android.generic.service.DataSMSListener.f955a     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            r1.put(r7, r9)     // Catch:{ Exception -> 0x0167, all -> 0x018b }
            goto L_0x014c
        L_0x0167:
            r0 = move-exception
            r1 = r3
        L_0x0169:
            java.lang.String r2 = "AvastGeneric"
            java.lang.String r3 = "Error parsing DATA SMS"
            com.avast.android.generic.util.z.a(r2, r3, r0)     // Catch:{ all -> 0x018d }
            if (r1 == 0) goto L_0x0036
            r1.close()     // Catch:{ IOException -> 0x0177 }
            goto L_0x0036
        L_0x0177:
            r0 = move-exception
            goto L_0x0036
        L_0x017a:
            r0 = move-exception
            r3 = r1
        L_0x017c:
            if (r3 == 0) goto L_0x0181
            r3.close()     // Catch:{ IOException -> 0x0189 }
        L_0x0181:
            throw r0
        L_0x0182:
            r12.a(r4)
            goto L_0x0036
        L_0x0187:
            r1 = move-exception
            goto L_0x0151
        L_0x0189:
            r1 = move-exception
            goto L_0x0181
        L_0x018b:
            r0 = move-exception
            goto L_0x017c
        L_0x018d:
            r0 = move-exception
            r3 = r1
            goto L_0x017c
        L_0x0190:
            r0 = move-exception
            goto L_0x0169
        L_0x0192:
            r0 = r1
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.service.DataSMSListener.onReceive(android.content.Context, android.content.Intent):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:47:0x0094 A[Catch:{ Exception -> 0x004f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r12) {
        /*
            r11 = this;
            r3 = 0
            r6 = 1
            r5 = 0
            java.util.LinkedList r7 = new java.util.LinkedList
            r7.<init>()
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r0 = com.avast.android.generic.service.DataSMSListener.f955a
            java.util.Set r0 = r0.keySet()
            java.util.Iterator r8 = r0.iterator()
        L_0x0012:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x0098
            java.lang.Object r0 = r8.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r1 = com.avast.android.generic.service.DataSMSListener.f955a
            java.lang.Object r1 = r1.get(r0)
            com.avast.android.generic.service.b r1 = (com.avast.android.generic.service.b) r1
            r2 = r5
        L_0x0027:
            byte[][] r4 = r1.f964c     // Catch:{ Exception -> 0x004f }
            int r4 = r4.length     // Catch:{ Exception -> 0x004f }
            if (r2 >= r4) goto L_0x00b1
            byte[][] r4 = r1.f964c     // Catch:{ Exception -> 0x004f }
            r4 = r4[r2]     // Catch:{ Exception -> 0x004f }
            if (r4 != 0) goto L_0x005b
            r2 = r5
        L_0x0033:
            if (r2 == 0) goto L_0x0012
            java.lang.String r9 = r1.f963b     // Catch:{ Exception -> 0x004f }
            int r2 = r1.f962a     // Catch:{ Exception -> 0x004f }
            if (r2 != r6) goto L_0x0061
            java.lang.String r2 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x005e }
            byte[][] r1 = r1.f964c     // Catch:{ UnsupportedEncodingException -> 0x005e }
            r4 = 0
            r1 = r1[r4]     // Catch:{ UnsupportedEncodingException -> 0x005e }
            java.lang.String r4 = "UTF-8"
            r2.<init>(r1, r4)     // Catch:{ UnsupportedEncodingException -> 0x005e }
            r1 = r2
        L_0x0048:
            r11.a(r12, r9, r1)     // Catch:{ Exception -> 0x004f }
        L_0x004b:
            r7.add(r0)     // Catch:{ Exception -> 0x004f }
            goto L_0x0012
        L_0x004f:
            r1 = move-exception
            java.lang.String r2 = "AvastGeneric"
            java.lang.String r4 = "Error parsing DATA SMS"
            com.avast.android.generic.util.z.a(r2, r4, r1)
            r7.add(r0)
            goto L_0x0012
        L_0x005b:
            int r2 = r2 + 1
            goto L_0x0027
        L_0x005e:
            r1 = move-exception
            r1 = r3
            goto L_0x0048
        L_0x0061:
            int r2 = r1.f962a     // Catch:{ Exception -> 0x004f }
            if (r2 <= r6) goto L_0x004b
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0090 }
            r2.<init>()     // Catch:{ all -> 0x0090 }
            r4 = r5
        L_0x006b:
            int r10 = r1.f962a     // Catch:{ all -> 0x00af }
            if (r4 >= r10) goto L_0x0079
            byte[][] r10 = r1.f964c     // Catch:{ all -> 0x00af }
            r10 = r10[r4]     // Catch:{ all -> 0x00af }
            r2.write(r10)     // Catch:{ all -> 0x00af }
            int r4 = r4 + 1
            goto L_0x006b
        L_0x0079:
            byte[] r4 = r2.toByteArray()     // Catch:{ all -> 0x00af }
            java.lang.String r1 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x008d }
            java.lang.String r10 = "UTF-8"
            r1.<init>(r4, r10)     // Catch:{ UnsupportedEncodingException -> 0x008d }
        L_0x0084:
            r11.a(r12, r9, r1)     // Catch:{ all -> 0x00af }
            if (r2 == 0) goto L_0x004b
            r2.close()     // Catch:{ Exception -> 0x004f }
            goto L_0x004b
        L_0x008d:
            r1 = move-exception
            r1 = r3
            goto L_0x0084
        L_0x0090:
            r1 = move-exception
            r2 = r3
        L_0x0092:
            if (r2 == 0) goto L_0x0097
            r2.close()     // Catch:{ Exception -> 0x004f }
        L_0x0097:
            throw r1     // Catch:{ Exception -> 0x004f }
        L_0x0098:
            java.util.Iterator r1 = r7.iterator()
        L_0x009c:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x00ae
            java.lang.Object r0 = r1.next()
            java.lang.String r0 = (java.lang.String) r0
            java.util.HashMap<java.lang.String, com.avast.android.generic.service.b> r2 = com.avast.android.generic.service.DataSMSListener.f955a
            r2.remove(r0)
            goto L_0x009c
        L_0x00ae:
            return
        L_0x00af:
            r1 = move-exception
            goto L_0x0092
        L_0x00b1:
            r2 = r6
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.service.DataSMSListener.a(android.content.Context):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void a(Context context, String str, String str2) {
        String str3;
        boolean z;
        ab abVar = (ab) aa.a(context, ad.class);
        if (str2 != null && str != null && !str2.equals("")) {
            String trim = str2.trim();
            String trim2 = str.trim();
            z.b(context, "SMS", "DATA SMS received from " + trim2 + ": " + trim);
            int indexOf = trim.indexOf(" ");
            if (indexOf > -1) {
                str3 = trim.substring(0, indexOf);
            } else {
                str3 = trim;
            }
            boolean b2 = abVar.b(str3);
            if (b2 || !abVar.t() || !abVar.y().equals(str3)) {
                z = b2;
            } else {
                z = true;
            }
            if (!z && a.a(context, str3)) {
                z = true;
            }
            if (z) {
                aw.a(context);
                z.a(context, "SMS has correct code, will be dispatched");
                Intent intent = new Intent();
                intent.setAction("com.avast.android.generic.service.action.SMS_RECEIVED");
                intent.putExtra("number", trim2);
                intent.putExtra("text", trim);
                intent.putExtra("binary", true);
                d.a(context, intent, trim2, null, trim, true);
                z.a("AvastComms", context, "Broadcast aborting...");
                abortBroadcast();
                z.a("AvastComms", context, "Broadcast aborted");
            }
        }
    }

    /* access modifiers changed from: protected */
    public int[] a() {
        return new int[]{15874};
    }
}
