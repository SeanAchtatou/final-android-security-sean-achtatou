package com.avast.android.generic.app.wizard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import com.avast.android.generic.o;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.x;

public abstract class WizardIntroduceAccountFragment extends TrackedMultiToolFragment {
    public abstract boolean d();

    public abstract boolean e();

    public abstract int f();

    public abstract void launchNext();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_wizard_info_account, viewGroup, false);
        ((TextView) inflate.findViewById(r.wizard_account_info)).setText(f());
        if (ak.b(getActivity())) {
            ((ScrollView) inflate).setBackgroundResource(o.bg_edge_color);
        }
        ((Button) inflate.findViewById(r.b_ok)).setOnClickListener(new f(this));
        Button button = (Button) inflate.findViewById(r.y);
        if (d()) {
            button.setOnClickListener(new g(this));
        } else {
            button.setText(getString(x.l_wizard_account_skip));
            button.setOnClickListener(new h(this));
        }
        return inflate;
    }

    public void onResume() {
        super.onResume();
    }
}
