package com.avast.android.generic.app.subscription;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

/* compiled from: WelcomePremiumFragment */
class aj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomePremiumFragment f537a;

    aj(WelcomePremiumFragment welcomePremiumFragment) {
        this.f537a = welcomePremiumFragment;
    }

    public void onClick(View view) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.avast.android.mobilesecurity"));
        intent.addFlags(268468224);
        this.f537a.startActivity(intent);
    }
}
