package com.avast.android.generic.ui;

import android.text.Editable;
import android.text.TextWatcher;
import com.avast.android.generic.q;
import com.avast.android.generic.x;

/* compiled from: VoucherDialog */
class aj implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VoucherDialog f1016a;

    aj(VoucherDialog voucherDialog) {
        this.f1016a = voucherDialog;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        boolean z;
        if (this.f1016a.isAdded()) {
            if (this.f1016a.f996a.length() > 0) {
                this.f1016a.f998c.setVisibility(0);
                if (this.f1016a.f996a.length() < 10) {
                    z = true;
                } else {
                    z = false;
                }
                if (z) {
                    this.f1016a.f998c.setImageResource(q.ic_scanner_result_problem);
                } else {
                    this.f1016a.f998c.setImageResource(q.ic_scanner_result_ok);
                }
                if (z) {
                    this.f1016a.f997b.setText(this.f1016a.getString(x.pref_voucher_too_short));
                    this.f1016a.f997b.setVisibility(0);
                    return;
                }
                this.f1016a.f997b.setVisibility(8);
                return;
            }
            this.f1016a.f998c.setVisibility(4);
            this.f1016a.f997b.setVisibility(8);
        }
    }
}
