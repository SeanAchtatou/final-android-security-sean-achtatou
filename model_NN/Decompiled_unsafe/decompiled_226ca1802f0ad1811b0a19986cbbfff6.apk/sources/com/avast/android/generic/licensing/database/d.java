package com.avast.android.generic.licensing.database;

import a.a.a.a.a.a;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.h;
import com.avast.android.generic.licensing.c.b;
import com.avast.android.generic.licensing.c.l;
import com.avast.android.generic.licensing.c.m;
import com.avast.android.generic.util.z;
import com.avast.c.a.a.an;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/* compiled from: PurchaseDatabaseHelper */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f882a = false;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ContentResolver f883b;

    /* renamed from: c  reason: collision with root package name */
    private Uri f884c;
    private Context d;

    public d(Context context, Uri uri) {
        this.f883b = context.getContentResolver();
        this.f884c = uri;
        this.d = context;
    }

    public void a(String str, String str2, m mVar, long j, String str3, Long l, Long l2, Boolean bool, boolean z, String str4, String str5, String str6, Long l3, boolean z2) {
        z.a("AvastGenericLic", String.format("Writing purchase update: %s, %s, %s, %d, %s", str, str2, mVar.toString(), Long.valueOf(j), str3));
        ContentValues contentValues = new ContentValues();
        contentValues.put("order_id", str);
        contentValues.put("productId", str2);
        contentValues.put("state", Integer.valueOf(mVar.ordinal()));
        contentValues.put("purchaseTime", Long.valueOf(j));
        contentValues.put("developerPayload", str3);
        contentValues.put("expire_date", l);
        contentValues.put("next_check_date", l2);
        if (bool != null) {
            contentValues.put("validity", Integer.valueOf(bool.booleanValue() ? 1 : 0));
        } else {
            contentValues.putNull("validity");
        }
        contentValues.put("confirmed", Integer.valueOf(z ? 1 : 0));
        contentValues.put("token", str4);
        contentValues.put("signedData", str5);
        contentValues.put("signature", str6);
        contentValues.put("timestamp", l3);
        contentValues.put("subscription", Integer.valueOf(z2 ? 1 : 0));
        a(this.d, this.f883b, new e(this, contentValues, str));
    }

    public Cursor a() {
        return this.f883b.query(b.a(this.f884c), null, null, null, null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(java.lang.String r9) {
        /*
            r8 = this;
            r6 = 0
            android.content.ContentResolver r0 = r8.f883b     // Catch:{ all -> 0x0039 }
            android.net.Uri r1 = r8.f884c     // Catch:{ all -> 0x0039 }
            android.net.Uri r1 = com.avast.android.generic.licensing.database.b.a(r1)     // Catch:{ all -> 0x0039 }
            r2 = 0
            java.lang.String r3 = "confirmed = ? AND validity = ? AND productId = ?"
            r4 = 3
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0039 }
            r5 = 0
            java.lang.String r7 = "1"
            r4[r5] = r7     // Catch:{ all -> 0x0039 }
            r5 = 1
            java.lang.String r7 = "1"
            r4[r5] = r7     // Catch:{ all -> 0x0039 }
            r5 = 2
            r4[r5] = r9     // Catch:{ all -> 0x0039 }
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x0039 }
            if (r1 == 0) goto L_0x0043
            boolean r0 = r1.moveToFirst()     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x0043
            java.lang.String r0 = "order_id"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ all -> 0x0040 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ all -> 0x0040 }
        L_0x0033:
            if (r1 == 0) goto L_0x0038
            r1.close()
        L_0x0038:
            return r0
        L_0x0039:
            r0 = move-exception
        L_0x003a:
            if (r6 == 0) goto L_0x003f
            r6.close()
        L_0x003f:
            throw r0
        L_0x0040:
            r0 = move-exception
            r6 = r1
            goto L_0x003a
        L_0x0043:
            r0 = r6
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.licensing.database.d.a(java.lang.String):java.lang.String");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void} */
    public boolean a(String str, l lVar, Long l, boolean z) {
        Cursor cursor;
        boolean z2;
        try {
            Cursor query = this.f883b.query(b.a(this.f884c), null, "order_id = ?", new String[]{lVar.b()}, null);
            if (query != null) {
                try {
                    if (!query.moveToFirst() || z) {
                        z.a("AvastGenericLic", String.format("Writing purchase creation: %s at purchase state %s", str, lVar.e().name()));
                        a(lVar.b(), lVar.c(), lVar.e(), lVar.d(), lVar.f(), null, null, null, false, lVar.g(), lVar.h(), lVar.i(), l, lVar.a().equals("subs"));
                    } else {
                        m mVar = m.values()[query.getInt(query.getColumnIndex("state"))];
                        if (mVar != m.CANCELED && mVar != m.REFUNDED) {
                            String string = query.getString(query.getColumnIndex("productId"));
                            long j = query.getLong(query.getColumnIndex("purchaseTime"));
                            String string2 = query.getString(query.getColumnIndex("developerPayload"));
                            String string3 = query.getString(query.getColumnIndex("token"));
                            String string4 = query.getString(query.getColumnIndex("signedData"));
                            String string5 = query.getString(query.getColumnIndex("signature"));
                            ContentValues contentValues = new ContentValues();
                            if (!TextUtils.equals(lVar.c(), string)) {
                                contentValues.put("productId", lVar.c());
                            }
                            if (lVar.e().ordinal() != mVar.ordinal()) {
                                contentValues.put("state", Integer.valueOf(lVar.e().ordinal()));
                            }
                            if (lVar.d() > 0 && lVar.d() != j) {
                                contentValues.put("purchaseTime", Long.valueOf(lVar.d()));
                            }
                            if (!TextUtils.equals(lVar.f(), string2)) {
                                contentValues.put("developerPayload", lVar.f());
                            }
                            if (!TextUtils.equals(lVar.g(), string3)) {
                                contentValues.put("token", lVar.g());
                            }
                            if (!TextUtils.equals(lVar.h(), string4)) {
                                contentValues.put("signedData", lVar.h());
                            }
                            if (!TextUtils.equals(lVar.i(), string5)) {
                                contentValues.put("signature", lVar.i());
                            }
                            if (contentValues.size() > 0) {
                                contentValues.put("confirmed", (Boolean) false);
                                contentValues.put("timestamp", l);
                                z.a("AvastGenericLic", String.format("Writing purchase update: %s at purchase state %s", str, lVar.e().name()));
                                a(this.d, this.f883b, new f(this, contentValues, str));
                            }
                        } else if (lVar.e() == m.CANCELED || lVar.e() == m.REFUNDED) {
                            z2 = false;
                            if (query != null) {
                                query.close();
                            }
                            return z2;
                        } else {
                            z2 = true;
                            if (query != null) {
                                query.close();
                            }
                            return z2;
                        }
                    }
                } catch (Throwable th) {
                    th = th;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            z2 = false;
            if (query != null) {
                query.close();
            }
            return z2;
        } catch (Throwable th2) {
            th = th2;
            cursor = null;
        }
    }

    public void b() {
        a(this.d, this.f883b, new g(this));
    }

    public static void a(Context context) {
        Cursor cursor;
        Cursor cursor2;
        Long l;
        boolean z;
        l lVar;
        b(context);
        ContentResolver contentResolver = context.getContentResolver();
        List<String> list = null;
        an b2 = ab.b(context);
        if (b2 == an.SUITE) {
            list = h.f697a;
        } else if (b2 == an.VPN) {
            list = h.f699c;
        }
        HashMap hashMap = new HashMap();
        PackageManager packageManager = context.getPackageManager();
        LinkedList<String> linkedList = new LinkedList<>();
        for (String str : list) {
            String str2 = str + ".database.billing";
            ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(str2, 0);
            if (resolveContentProvider != null) {
                if (!resolveContentProvider.exported) {
                    if (!f882a) {
                        a.a().a("Strange, provider " + str2 + " is not exported", new Exception("Provider not exported"));
                        f882a = true;
                    }
                } else if (Build.VERSION.SDK_INT < 11 || resolveContentProvider.isEnabled()) {
                    linkedList.add(str2);
                    try {
                        Cursor query = contentResolver.query(b.a(Uri.parse("content://" + str2)), null, null, null, null);
                        if (query != null) {
                            try {
                                if (query.moveToFirst()) {
                                    int columnIndex = query.getColumnIndex("order_id");
                                    int columnIndex2 = query.getColumnIndex("timestamp");
                                    int columnIndex3 = query.getColumnIndex("productId");
                                    int columnIndex4 = query.getColumnIndex("state");
                                    int columnIndex5 = query.getColumnIndex("purchaseTime");
                                    int columnIndex6 = query.getColumnIndex("developerPayload");
                                    int columnIndex7 = query.getColumnIndex("token");
                                    int columnIndex8 = query.getColumnIndex("signedData");
                                    int columnIndex9 = query.getColumnIndex("signature");
                                    int columnIndex10 = query.getColumnIndex("next_check_date");
                                    int columnIndex11 = query.getColumnIndex("confirmed");
                                    int columnIndex12 = query.getColumnIndex("validity");
                                    int columnIndex13 = query.getColumnIndex("expire_date");
                                    int columnIndex14 = query.getColumnIndex("subscription");
                                    do {
                                        String string = query.getString(columnIndex);
                                        if (!query.isNull(columnIndex2)) {
                                            l = Long.valueOf(query.getLong(columnIndex2));
                                        } else {
                                            l = null;
                                        }
                                        if (!hashMap.containsKey(string)) {
                                            l lVar2 = new l(null);
                                            lVar2.f897b = str2;
                                            lVar2.f896a = l;
                                            hashMap.put(string, lVar2);
                                            l lVar3 = lVar2;
                                            z = true;
                                            lVar = lVar3;
                                        } else {
                                            l lVar4 = (l) hashMap.get(string);
                                            if (lVar4.f896a == null && l == null) {
                                                l lVar5 = lVar4;
                                                z = false;
                                                lVar = lVar5;
                                            } else if (lVar4.f896a != null && l == null) {
                                                lVar4.d = true;
                                                l lVar6 = lVar4;
                                                z = false;
                                                lVar = lVar6;
                                            } else if (lVar4.f896a == null && l != null) {
                                                lVar4.f896a = l;
                                                lVar4.f897b = str2;
                                                lVar4.d = true;
                                                l lVar7 = lVar4;
                                                z = true;
                                                lVar = lVar7;
                                            } else if (lVar4.f896a.longValue() > l.longValue()) {
                                                lVar4.d = true;
                                                l lVar8 = lVar4;
                                                z = false;
                                                lVar = lVar8;
                                            } else if (lVar4.f896a.longValue() < l.longValue()) {
                                                lVar4.f896a = l;
                                                lVar4.f897b = str2;
                                                lVar4.d = true;
                                                l lVar9 = lVar4;
                                                z = true;
                                                lVar = lVar9;
                                            } else {
                                                l lVar10 = lVar4;
                                                z = false;
                                                lVar = lVar10;
                                            }
                                        }
                                        if (z) {
                                            lVar.f898c = new ContentValues();
                                            lVar.f898c.put("order_id", string);
                                            lVar.f898c.put("productId", query.getString(columnIndex3));
                                            lVar.f898c.put("state", Integer.valueOf(query.getInt(columnIndex4)));
                                            lVar.f898c.put("purchaseTime", Long.valueOf(query.getLong(columnIndex5)));
                                            lVar.f898c.put("developerPayload", query.getString(columnIndex6));
                                            if (!query.isNull(columnIndex13)) {
                                                lVar.f898c.put("expire_date", Long.valueOf(query.getLong(columnIndex13)));
                                            } else {
                                                lVar.f898c.putNull("expire_date");
                                            }
                                            if (!query.isNull(columnIndex10)) {
                                                lVar.f898c.put("next_check_date", Long.valueOf(query.getLong(columnIndex10)));
                                            } else {
                                                lVar.f898c.putNull("next_check_date");
                                            }
                                            if (!query.isNull(columnIndex12)) {
                                                lVar.f898c.put("validity", Integer.valueOf(query.getInt(columnIndex12)));
                                            } else {
                                                lVar.f898c.putNull("validity");
                                            }
                                            if (!query.isNull(columnIndex14)) {
                                                lVar.f898c.put("subscription", Integer.valueOf(query.getInt(columnIndex14)));
                                            } else {
                                                lVar.f898c.putNull("subscription");
                                            }
                                            lVar.f898c.put("confirmed", Integer.valueOf(query.getInt(columnIndex11)));
                                            lVar.f898c.put("token", query.getString(columnIndex7));
                                            lVar.f898c.put("signedData", query.getString(columnIndex8));
                                            lVar.f898c.put("signature", query.getString(columnIndex9));
                                            Long l2 = null;
                                            if (!query.isNull(columnIndex2)) {
                                                l2 = Long.valueOf(query.getLong(columnIndex2));
                                            }
                                            lVar.f898c.put("timestamp", l2);
                                        }
                                    } while (query.moveToNext());
                                }
                            } catch (Throwable th) {
                                th = th;
                                cursor2 = query;
                                if (cursor2 != null) {
                                    cursor2.close();
                                }
                                throw th;
                            }
                        }
                        if (query != null) {
                            query.close();
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        cursor2 = null;
                    }
                } else if (!f882a) {
                    a.a().a("Strange, provider " + str2 + " is not enabled", new Exception("Provider not enabled"));
                    f882a = true;
                }
            }
        }
        for (String str3 : hashMap.keySet()) {
            l lVar11 = (l) hashMap.get(str3);
            for (String str4 : linkedList) {
                if (!str4.equals(lVar11.f897b)) {
                    if (!lVar11.d) {
                        try {
                            cursor = contentResolver.query(b.a(Uri.parse("content://" + str4)), null, "order_id= ?", new String[]{str3}, null);
                            if (cursor != null) {
                                try {
                                    if (!cursor.moveToFirst()) {
                                        contentResolver.insert(b.a(Uri.parse("content://" + str4)), lVar11.f898c);
                                    }
                                } catch (Throwable th3) {
                                    th = th3;
                                    if (cursor != null) {
                                        cursor.close();
                                    }
                                    throw th;
                                }
                            }
                            if (cursor != null) {
                                cursor.close();
                            }
                        } catch (Throwable th4) {
                            th = th4;
                            cursor = null;
                        }
                    } else {
                        contentResolver.update(b.a(Uri.parse("content://" + str4)), lVar11.f898c, "order_id= ?", new String[]{str3});
                    }
                }
            }
        }
    }

    private static List<String> a(Context context, ContentResolver contentResolver, k kVar) {
        List<String> list = null;
        an b2 = ab.b(context);
        if (b2 == an.SUITE) {
            list = h.f697a;
        } else if (b2 == an.VPN) {
            list = h.f699c;
        }
        LinkedList linkedList = new LinkedList();
        PackageManager packageManager = context.getPackageManager();
        for (String str : list) {
            String str2 = str + ".database.billing";
            ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(str2, 0);
            if (resolveContentProvider != null) {
                if (!resolveContentProvider.exported) {
                    if (!f882a) {
                        a.a().a("Strange, provider " + str2 + " is not exported", new Exception("Provider not exported"));
                        f882a = true;
                    }
                } else if (Build.VERSION.SDK_INT < 11 || resolveContentProvider.isEnabled()) {
                    linkedList.add(resolveContentProvider.authority);
                    kVar.a(b.a(Uri.parse("content://" + resolveContentProvider.authority)), contentResolver);
                } else if (!f882a) {
                    a.a().a("Strange, provider " + str2 + " is not enabled", new Exception("Provider not enabled"));
                    f882a = true;
                }
            }
        }
        return linkedList;
    }

    public static void a(Context context, Uri uri) {
        new d(context, uri).b();
        ((ab) aa.a(context, ab.class)).S();
        b bVar = new b(context);
        bVar.a(new h(bVar, context));
    }

    public static void b(Context context) {
        ContentResolver contentResolver = context.getContentResolver();
        a(context, contentResolver, new j(contentResolver));
    }
}
