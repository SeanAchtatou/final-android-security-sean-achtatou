package com.avast.android.generic.internet.c.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class j extends GeneratedMessageLite.Builder<i, j> implements k {

    /* renamed from: a  reason: collision with root package name */
    private int f767a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f768b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private ByteString f769c = ByteString.EMPTY;
    private int d;
    private Object e = "";
    private ByteString f = ByteString.EMPTY;
    private w g = w.a();
    private boolean h;
    private int i;
    private Object j = "";
    private Object k = "";
    private Object l = "";

    private j() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static j i() {
        return new j();
    }

    /* renamed from: a */
    public j clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public i getDefaultInstanceForType() {
        return i.a();
    }

    /* renamed from: c */
    public i build() {
        i d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    /* access modifiers changed from: private */
    public i j() {
        i d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2).asInvalidProtocolBufferException();
    }

    public i d() {
        int i2 = 1;
        i iVar = new i(this);
        int i3 = this.f767a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        ByteString unused = iVar.f766c = this.f768b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        ByteString unused2 = iVar.d = this.f769c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        int unused3 = iVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        Object unused4 = iVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        ByteString unused5 = iVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        w unused6 = iVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        boolean unused7 = iVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        int unused8 = iVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 256;
        }
        Object unused9 = iVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 512;
        }
        Object unused10 = iVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 1024;
        }
        Object unused11 = iVar.m = this.l;
        int unused12 = iVar.f765b = i2;
        return iVar;
    }

    /* renamed from: a */
    public j mergeFrom(i iVar) {
        if (iVar != i.a()) {
            if (iVar.b()) {
                a(iVar.c());
            }
            if (iVar.d()) {
                b(iVar.e());
            }
            if (iVar.f()) {
                a(iVar.g());
            }
            if (iVar.h()) {
                a(iVar.i());
            }
            if (iVar.j()) {
                c(iVar.k());
            }
            if (iVar.l()) {
                b(iVar.m());
            }
            if (iVar.n()) {
                a(iVar.o());
            }
            if (iVar.p()) {
                b(iVar.q());
            }
            if (iVar.r()) {
                b(iVar.s());
            }
            if (iVar.t()) {
                c(iVar.u());
            }
            if (iVar.v()) {
                d(iVar.w());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public j mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f767a |= 1;
                    this.f768b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f767a |= 2;
                    this.f769c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f767a |= 4;
                    this.d = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f767a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f767a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    x h2 = w.h();
                    if (e()) {
                        h2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(h2, extensionRegistryLite);
                    a(h2.d());
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f767a |= 64;
                    this.h = codedInputStream.readBool();
                    break;
                case 72:
                    this.f767a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readInt32();
                    break;
                case 82:
                    this.f767a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 90:
                    this.f767a |= 512;
                    this.k = codedInputStream.readBytes();
                    break;
                case 98:
                    this.f767a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public j a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f767a |= 1;
        this.f768b = byteString;
        return this;
    }

    public j b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f767a |= 2;
        this.f769c = byteString;
        return this;
    }

    public j a(int i2) {
        this.f767a |= 4;
        this.d = i2;
        return this;
    }

    public j a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f767a |= 8;
        this.e = str;
        return this;
    }

    public j c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f767a |= 16;
        this.f = byteString;
        return this;
    }

    public boolean e() {
        return (this.f767a & 32) == 32;
    }

    public w f() {
        return this.g;
    }

    public j a(w wVar) {
        if (wVar == null) {
            throw new NullPointerException();
        }
        this.g = wVar;
        this.f767a |= 32;
        return this;
    }

    public j b(w wVar) {
        if ((this.f767a & 32) != 32 || this.g == w.a()) {
            this.g = wVar;
        } else {
            this.g = w.a(this.g).mergeFrom(wVar).d();
        }
        this.f767a |= 32;
        return this;
    }

    public j a(boolean z) {
        this.f767a |= 64;
        this.h = z;
        return this;
    }

    public j b(int i2) {
        this.f767a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = i2;
        return this;
    }

    public j b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f767a |= 256;
        this.j = str;
        return this;
    }

    public j c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f767a |= 512;
        this.k = str;
        return this;
    }

    public j d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f767a |= 1024;
        this.l = str;
        return this;
    }
}
