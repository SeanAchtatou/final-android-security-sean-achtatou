package com.avast.android.generic.util;

import android.text.TextUtils;
import com.actionbarsherlock.R;

/* compiled from: PhoneNumbers */
public class ab {
    public static ac a(String str) {
        if (TextUtils.isEmpty(str)) {
            return ac.TOO_SHORT;
        }
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case R.styleable.SherlockTheme_textAppearanceSearchResultSubtitle:
                    if (i == 0) {
                        break;
                    } else {
                        return ac.INVALID_CHARACTER;
                    }
                case R.styleable.SherlockTheme_listPreferredItemHeightSmall:
                case R.styleable.SherlockTheme_listPreferredItemPaddingLeft:
                case R.styleable.SherlockTheme_listPreferredItemPaddingRight:
                case R.styleable.SherlockTheme_textAppearanceListItemSmall:
                default:
                    return ac.INVALID_CHARACTER;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                case R.styleable.SherlockTheme_windowMinWidthMinor:
                case '2':
                case R.styleable.SherlockTheme_actionDropDownStyle:
                case R.styleable.SherlockTheme_actionButtonStyle:
                case R.styleable.SherlockTheme_homeAsUpIndicator:
                case R.styleable.SherlockTheme_dropDownListViewStyle:
                case R.styleable.SherlockTheme_popupMenuStyle:
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                case R.styleable.SherlockTheme_actionSpinnerItemStyle:
                    break;
            }
        }
        if (str.length() < 4) {
            return ac.TOO_SHORT;
        }
        return ac.VALID;
    }

    public static boolean b(String str) {
        return a(str) == ac.VALID;
    }

    public static String c(String str) {
        if (str == null) {
            return "";
        }
        String str2 = "";
        for (int i = 0; i < str.length(); i++) {
            switch (str.charAt(i)) {
                case R.styleable.SherlockTheme_textAppearanceSearchResultSubtitle:
                    if (i != 0) {
                        break;
                    } else {
                        str2 = str2 + str.charAt(i);
                        break;
                    }
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                case R.styleable.SherlockTheme_windowMinWidthMinor:
                case '2':
                case R.styleable.SherlockTheme_actionDropDownStyle:
                case R.styleable.SherlockTheme_actionButtonStyle:
                case R.styleable.SherlockTheme_homeAsUpIndicator:
                case R.styleable.SherlockTheme_dropDownListViewStyle:
                case R.styleable.SherlockTheme_popupMenuStyle:
                case R.styleable.SherlockTheme_dropdownListPreferredItemHeight:
                case R.styleable.SherlockTheme_actionSpinnerItemStyle:
                    str2 = str2 + str.charAt(i);
                    break;
            }
        }
        return str2;
    }
}
