package com.avast.android.generic.notification;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: AvastNotificationFragment */
class b implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AvastNotificationFragment f923a;

    b(AvastNotificationFragment avastNotificationFragment) {
        this.f923a = avastNotificationFragment;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        g gVar = (g) view.getTag();
        AvastPendingIntent avastPendingIntent = gVar.g;
        if (avastPendingIntent != null) {
            this.f923a.m.a(avastPendingIntent);
        }
        if ((gVar.i & 16) > 0) {
            this.f923a.m.a(gVar.f930a, gVar.j);
            this.f923a.m.b(gVar.f930a, gVar.j);
        }
    }
}
