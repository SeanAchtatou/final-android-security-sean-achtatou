package com.avast.android.generic.shepherd;

import android.content.Context;
import android.os.Bundle;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.e.a.az;

/* compiled from: Shepherd */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static c f967a = null;

    /* renamed from: b  reason: collision with root package name */
    private static Bundle f968b = new Bundle();

    /* renamed from: c  reason: collision with root package name */
    private static boolean f969c = false;

    protected static c a() {
        return f967a;
    }

    protected static Bundle b() {
        return f968b;
    }

    public static synchronized az a(Context context) {
        az a2;
        synchronized (b.class) {
            if (!f969c) {
                throw new RuntimeException("You have to call init first");
            }
            a2 = h.a(context, (ab) aa.a(context, ab.class));
        }
        return a2;
    }
}
