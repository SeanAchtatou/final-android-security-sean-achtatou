package com.avast.android.generic.b.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: BadNewsProto */
public final class i extends GeneratedMessageLite.Builder<h, i> implements j {

    /* renamed from: a  reason: collision with root package name */
    private int f611a;

    /* renamed from: b  reason: collision with root package name */
    private List<c> f612b = Collections.emptyList();

    private i() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static i g() {
        return new i();
    }

    /* renamed from: a */
    public i clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public h getDefaultInstanceForType() {
        return h.a();
    }

    /* renamed from: c */
    public h build() {
        h d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public h d() {
        h hVar = new h(this);
        int i = this.f611a;
        if ((this.f611a & 1) == 1) {
            this.f612b = Collections.unmodifiableList(this.f612b);
            this.f611a &= -2;
        }
        List unused = hVar.f609b = this.f612b;
        return hVar;
    }

    /* renamed from: a */
    public i mergeFrom(h hVar) {
        if (hVar != h.a() && !hVar.f609b.isEmpty()) {
            if (this.f612b.isEmpty()) {
                this.f612b = hVar.f609b;
                this.f611a &= -2;
            } else {
                h();
                this.f612b.addAll(hVar.f609b);
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public i mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    f o = c.o();
                    codedInputStream.readMessage(o, extensionRegistryLite);
                    a(o.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    private void h() {
        if ((this.f611a & 1) != 1) {
            this.f612b = new ArrayList(this.f612b);
            this.f611a |= 1;
        }
    }

    public i a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        h();
        this.f612b.add(cVar);
        return this;
    }
}
