package com.avast.android.generic.g.a;

/* compiled from: SmsSender */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f685a;

    d(b bVar) {
        this.f685a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, boolean):boolean
     arg types: [com.avast.android.generic.g.a.b, int]
     candidates:
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, java.lang.Thread):java.lang.Thread
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e):void
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, boolean):boolean */
    public void run() {
        synchronized (this.f685a.m) {
            if (this.f685a.k != null && this.f685a.k.isAlive()) {
                try {
                    boolean unused = this.f685a.l = true;
                    this.f685a.k.interrupt();
                    this.f685a.k.join();
                } catch (InterruptedException e) {
                }
            }
        }
    }
}
