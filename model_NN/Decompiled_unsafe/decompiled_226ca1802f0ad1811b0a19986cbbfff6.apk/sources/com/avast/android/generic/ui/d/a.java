package com.avast.android.generic.ui.d;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.avast.android.generic.o;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ProblemChecker */
public abstract class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Fragment f1031a;

    /* renamed from: b  reason: collision with root package name */
    private List<d> f1032b = null;

    /* renamed from: c  reason: collision with root package name */
    private Context f1033c;

    /* access modifiers changed from: protected */
    public abstract void a(Context context, List<d> list, boolean z);

    public a(Context context, Fragment fragment) {
        this.f1031a = fragment;
        this.f1033c = context;
    }

    public synchronized void a(Context context, boolean z) {
        if (this.f1032b != null) {
            this.f1032b.clear();
        } else {
            this.f1032b = new LinkedList();
        }
        LinkedList linkedList = new LinkedList();
        a(context, linkedList, z);
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (!dVar.c() || z) {
                this.f1032b.add(dVar);
            }
        }
    }

    public List<d> a() {
        return this.f1032b;
    }

    public boolean a(Context context) {
        return b(context, false);
    }

    public boolean b(Context context, boolean z) {
        if (this.f1032b == null) {
            a(context, z);
        }
        if (this.f1032b != null && this.f1032b.size() > 0) {
            return true;
        }
        return false;
    }

    public int b() {
        if (this.f1032b == null) {
            return 0;
        }
        return this.f1032b.size();
    }

    public int getCount() {
        if (this.f1032b == null) {
            return 0;
        }
        return b();
    }

    public Object getItem(int i) {
        if (this.f1032b == null || i >= this.f1032b.size()) {
            return null;
        }
        return this.f1032b.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int i, View view, ViewGroup viewGroup) {
        c cVar;
        if (this.f1032b == null || i >= this.f1032b.size()) {
            return null;
        }
        if (view == null) {
            view = LayoutInflater.from(this.f1033c).inflate(t.list_item_checker, viewGroup, false);
            c cVar2 = new c(this);
            cVar2.f1034a = (TextView) view.findViewById(r.name);
            cVar2.f1035b = (TextView) view.findViewById(r.text);
            view.setTag(cVar2);
            cVar = cVar2;
        } else {
            cVar = (c) view.getTag();
        }
        a(i, cVar);
        return view;
    }

    private void a(int i, c cVar) {
        if (this.f1032b != null && i < this.f1032b.size()) {
            d dVar = this.f1032b.get(i);
            cVar.f1034a.setText(dVar.b(this.f1033c));
            cVar.f1035b.setText(dVar.c(this.f1033c));
            if (dVar.b()) {
                cVar.f1034a.setTextColor(this.f1033c.getResources().getColor(o.text_warning));
            } else {
                cVar.f1034a.setTextColor(this.f1033c.getResources().getColor(o.text_warning));
            }
        }
    }
}
