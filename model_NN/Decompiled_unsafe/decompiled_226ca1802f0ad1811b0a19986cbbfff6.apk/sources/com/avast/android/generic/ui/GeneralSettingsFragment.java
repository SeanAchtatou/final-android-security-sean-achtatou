package com.avast.android.generic.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ga.TrackedFragment;

public abstract class GeneralSettingsFragment extends TrackedFragment {

    /* renamed from: a  reason: collision with root package name */
    private int f992a;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f992a = getArguments().getInt("layoutId");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_settings_general, viewGroup, false);
        layoutInflater.inflate(this.f992a, (ViewGroup) inflate);
        return inflate;
    }
}
