package com.avast.android.generic.app.about;

import android.view.View;

/* compiled from: FeedbackFragment */
class j implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f347a;

    j(FeedbackFragment feedbackFragment) {
        this.f347a = feedbackFragment;
    }

    public void onClick(View view) {
        this.f347a.a("ms-Feedback", "send", null, 0);
        if (this.f347a.e()) {
            n unused = this.f347a.t = new n(this.f347a, null);
            this.f347a.t.execute(new Void[0]);
        }
    }
}
