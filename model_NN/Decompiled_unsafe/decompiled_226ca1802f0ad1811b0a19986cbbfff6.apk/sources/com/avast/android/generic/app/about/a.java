package com.avast.android.generic.app.about;

import android.os.Bundle;
import android.view.View;

/* compiled from: AboutFragment */
class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AboutFragment f332a;

    a(AboutFragment aboutFragment) {
        this.f332a = aboutFragment;
    }

    public void onClick(View view) {
        this.f332a.a("ms-About", "Send Feedback", "", 0);
        Bundle bundle = new Bundle();
        if (this.f332a.getArguments().containsKey("community_iq")) {
            bundle.putBundle("community_iq", this.f332a.getArguments().getBundle("community_iq"));
        }
        FeedbackActivity.call(this.f332a.getActivity(), bundle);
    }
}
