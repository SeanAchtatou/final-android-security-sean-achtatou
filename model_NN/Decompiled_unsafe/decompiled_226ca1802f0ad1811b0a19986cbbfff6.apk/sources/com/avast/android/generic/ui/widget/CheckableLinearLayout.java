package com.avast.android.generic.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1081a = {16842912};

    /* renamed from: b  reason: collision with root package name */
    private boolean f1082b = false;

    public CheckableLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @SuppressLint({"NewApi"})
    public CheckableLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setChecked(boolean z) {
        this.f1082b = z;
        refreshDrawableState();
        int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = getChildAt(i);
            if (childAt instanceof Checkable) {
                ((Checkable) childAt).setChecked(z);
            }
        }
    }

    public boolean isChecked() {
        return this.f1082b;
    }

    public void toggle() {
        setChecked(!this.f1082b);
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (this.f1082b) {
            mergeDrawableStates(onCreateDrawableState, f1081a);
        }
        return onCreateDrawableState;
    }
}
