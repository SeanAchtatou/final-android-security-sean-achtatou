package com.avast.android.generic.internet.c.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class g extends GeneratedMessageLite.Builder<f, g> implements h {

    /* renamed from: a  reason: collision with root package name */
    private int f761a;

    /* renamed from: b  reason: collision with root package name */
    private Object f762b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f763c = "";
    private Object d = "";
    private int e;
    private Object f = "";
    private Object g = "";
    private Object h = "";
    private ByteString i = ByteString.EMPTY;
    private ByteString j = ByteString.EMPTY;
    private t k = t.a();
    private Object l = "";
    private Object m = "";
    private boolean n = true;
    private boolean o;
    private c p = c.a();

    private g() {
        j();
    }

    private void j() {
    }

    /* access modifiers changed from: private */
    public static g k() {
        return new g();
    }

    /* renamed from: a */
    public g clone() {
        return k().mergeFrom(d());
    }

    /* renamed from: b */
    public f getDefaultInstanceForType() {
        return f.a();
    }

    /* renamed from: c */
    public f build() {
        f d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public f d() {
        int i2 = 1;
        f fVar = new f(this);
        int i3 = this.f761a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        Object unused = fVar.f760c = this.f762b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        Object unused2 = fVar.d = this.f763c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        Object unused3 = fVar.e = this.d;
        if ((i3 & 8) == 8) {
            i2 |= 8;
        }
        int unused4 = fVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 16;
        }
        Object unused5 = fVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 32;
        }
        Object unused6 = fVar.h = this.g;
        if ((i3 & 64) == 64) {
            i2 |= 64;
        }
        Object unused7 = fVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        ByteString unused8 = fVar.j = this.i;
        if ((i3 & 256) == 256) {
            i2 |= 256;
        }
        ByteString unused9 = fVar.k = this.j;
        if ((i3 & 512) == 512) {
            i2 |= 512;
        }
        t unused10 = fVar.l = this.k;
        if ((i3 & 1024) == 1024) {
            i2 |= 1024;
        }
        Object unused11 = fVar.m = this.l;
        if ((i3 & 2048) == 2048) {
            i2 |= 2048;
        }
        Object unused12 = fVar.n = this.m;
        if ((i3 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i2 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        boolean unused13 = fVar.o = this.n;
        if ((i3 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i2 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        boolean unused14 = fVar.p = this.o;
        if ((i3 & 16384) == 16384) {
            i2 |= 16384;
        }
        c unused15 = fVar.q = this.p;
        int unused16 = fVar.f759b = i2;
        return fVar;
    }

    /* renamed from: a */
    public g mergeFrom(f fVar) {
        if (fVar != f.a()) {
            if (fVar.b()) {
                a(fVar.c());
            }
            if (fVar.d()) {
                b(fVar.e());
            }
            if (fVar.f()) {
                c(fVar.g());
            }
            if (fVar.h()) {
                a(fVar.i());
            }
            if (fVar.j()) {
                d(fVar.k());
            }
            if (fVar.l()) {
                e(fVar.m());
            }
            if (fVar.n()) {
                f(fVar.o());
            }
            if (fVar.p()) {
                a(fVar.q());
            }
            if (fVar.r()) {
                b(fVar.s());
            }
            if (fVar.t()) {
                b(fVar.u());
            }
            if (fVar.v()) {
                g(fVar.w());
            }
            if (fVar.x()) {
                h(fVar.y());
            }
            if (fVar.z()) {
                a(fVar.A());
            }
            if (fVar.B()) {
                b(fVar.C());
            }
            if (fVar.D()) {
                b(fVar.E());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public g mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f761a |= 1;
                    this.f762b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f761a |= 2;
                    this.f763c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSmall:
                    this.f761a |= 8;
                    this.e = codedInputStream.readInt32();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f761a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f761a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f761a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f761a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f761a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f761a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case 82:
                    u p2 = t.p();
                    if (e()) {
                        p2.mergeFrom(f());
                    }
                    codedInputStream.readMessage(p2, extensionRegistryLite);
                    a(p2.d());
                    break;
                case 90:
                    this.f761a |= 1024;
                    this.l = codedInputStream.readBytes();
                    break;
                case 98:
                    this.f761a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                case 104:
                    this.f761a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBool();
                    break;
                case 112:
                    this.f761a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBool();
                    break;
                case 122:
                    d f2 = c.f();
                    if (g()) {
                        f2.mergeFrom(h());
                    }
                    codedInputStream.readMessage(f2, extensionRegistryLite);
                    a(f2.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public g a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 1;
        this.f762b = str;
        return this;
    }

    public g b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 2;
        this.f763c = str;
        return this;
    }

    public g c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 4;
        this.d = str;
        return this;
    }

    public g a(int i2) {
        this.f761a |= 8;
        this.e = i2;
        return this;
    }

    public g d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 16;
        this.f = str;
        return this;
    }

    public g e(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 32;
        this.g = str;
        return this;
    }

    public g f(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 64;
        this.h = str;
        return this;
    }

    public g a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f761a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = byteString;
        return this;
    }

    public g b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f761a |= 256;
        this.j = byteString;
        return this;
    }

    public boolean e() {
        return (this.f761a & 512) == 512;
    }

    public t f() {
        return this.k;
    }

    public g a(t tVar) {
        if (tVar == null) {
            throw new NullPointerException();
        }
        this.k = tVar;
        this.f761a |= 512;
        return this;
    }

    public g a(u uVar) {
        this.k = uVar.build();
        this.f761a |= 512;
        return this;
    }

    public g b(t tVar) {
        if ((this.f761a & 512) != 512 || this.k == t.a()) {
            this.k = tVar;
        } else {
            this.k = t.a(this.k).mergeFrom(tVar).d();
        }
        this.f761a |= 512;
        return this;
    }

    public g g(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 1024;
        this.l = str;
        return this;
    }

    public g h(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f761a |= 2048;
        this.m = str;
        return this;
    }

    public g a(boolean z) {
        this.f761a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = z;
        return this;
    }

    public g b(boolean z) {
        this.f761a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = z;
        return this;
    }

    public boolean g() {
        return (this.f761a & 16384) == 16384;
    }

    public c h() {
        return this.p;
    }

    public g a(c cVar) {
        if (cVar == null) {
            throw new NullPointerException();
        }
        this.p = cVar;
        this.f761a |= 16384;
        return this;
    }

    public g a(d dVar) {
        this.p = dVar.build();
        this.f761a |= 16384;
        return this;
    }

    public g b(c cVar) {
        if ((this.f761a & 16384) != 16384 || this.p == c.a()) {
            this.p = cVar;
        } else {
            this.p = c.a(this.p).mergeFrom(cVar).d();
        }
        this.f761a |= 16384;
        return this;
    }
}
