package com.avast.android.generic.internet.c.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class t extends GeneratedMessageLite implements v {

    /* renamed from: a  reason: collision with root package name */
    private static final t f783a = new t(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f784b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f785c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public boolean f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public Object i;
    private byte j;
    private int k;

    private t(u uVar) {
        super(uVar);
        this.j = -1;
        this.k = -1;
    }

    private t(boolean z) {
        this.j = -1;
        this.k = -1;
    }

    public static t a() {
        return f783a;
    }

    public boolean b() {
        return (this.f784b & 1) == 1;
    }

    public String c() {
        Object obj = this.f785c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f785c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString s() {
        Object obj = this.f785c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f785c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f784b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString t() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f784b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString u() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f784b & 8) == 8;
    }

    public boolean i() {
        return this.f;
    }

    public boolean j() {
        return (this.f784b & 16) == 16;
    }

    public boolean k() {
        return this.g;
    }

    public boolean l() {
        return (this.f784b & 32) == 32;
    }

    public boolean m() {
        return this.h;
    }

    public boolean n() {
        return (this.f784b & 64) == 64;
    }

    public String o() {
        Object obj = this.i;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.i = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString v() {
        Object obj = this.i;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.i = copyFromUtf8;
        return copyFromUtf8;
    }

    private void w() {
        this.f785c = "";
        this.d = "";
        this.e = "";
        this.f = false;
        this.g = false;
        this.h = false;
        this.i = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.j;
        if (b2 == -1) {
            this.j = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f784b & 1) == 1) {
            codedOutputStream.writeBytes(1, s());
        }
        if ((this.f784b & 2) == 2) {
            codedOutputStream.writeBytes(2, t());
        }
        if ((this.f784b & 4) == 4) {
            codedOutputStream.writeBytes(3, u());
        }
        if ((this.f784b & 8) == 8) {
            codedOutputStream.writeBool(4, this.f);
        }
        if ((this.f784b & 16) == 16) {
            codedOutputStream.writeBool(5, this.g);
        }
        if ((this.f784b & 32) == 32) {
            codedOutputStream.writeBool(6, this.h);
        }
        if ((this.f784b & 64) == 64) {
            codedOutputStream.writeBytes(7, v());
        }
    }

    public int getSerializedSize() {
        int i2 = this.k;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f784b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, s());
            }
            if ((this.f784b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, t());
            }
            if ((this.f784b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, u());
            }
            if ((this.f784b & 8) == 8) {
                i2 += CodedOutputStream.computeBoolSize(4, this.f);
            }
            if ((this.f784b & 16) == 16) {
                i2 += CodedOutputStream.computeBoolSize(5, this.g);
            }
            if ((this.f784b & 32) == 32) {
                i2 += CodedOutputStream.computeBoolSize(6, this.h);
            }
            if ((this.f784b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, v());
            }
            this.k = i2;
        }
        return i2;
    }

    public static u p() {
        return u.g();
    }

    /* renamed from: q */
    public u newBuilderForType() {
        return p();
    }

    public static u a(t tVar) {
        return p().mergeFrom(tVar);
    }

    /* renamed from: r */
    public u toBuilder() {
        return a(this);
    }

    static {
        f783a.w();
    }
}
