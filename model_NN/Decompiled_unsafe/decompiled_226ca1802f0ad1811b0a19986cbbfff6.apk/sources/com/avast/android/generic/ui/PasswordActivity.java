package com.avast.android.generic.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class PasswordActivity extends BaseActivity {
    public static void call(Context context, int i) {
        Intent intent = new Intent(context, PasswordActivity.class);
        intent.putExtra("callbackHandlerId", i);
        if (!(context instanceof Activity)) {
            intent.setFlags(268435456);
        }
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PasswordDialog.a(getSupportFragmentManager(), getIntent().getIntExtra("callbackHandlerId", 0));
    }
}
