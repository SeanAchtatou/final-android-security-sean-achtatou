package com.avast.android.generic.notification;

import android.content.ContentValues;
import android.os.AsyncTask;
import com.avast.android.generic.e;

/* compiled from: AvastNotificationManager */
class j extends AsyncTask<Void, Void, Void> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f937a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ h f938b;

    j(h hVar, a aVar) {
        this.f938b = hVar;
        this.f937a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Void... voidArr) {
        String str;
        ContentValues contentValues = new ContentValues();
        contentValues.put("notification_id", Long.valueOf(this.f937a.f920a));
        contentValues.put("notification_tag", this.f937a.f921b);
        contentValues.put("contentText", this.f937a.l != null ? this.f937a.l.toString() : null);
        if (this.f937a.k != null) {
            str = this.f937a.k.toString();
        } else {
            str = null;
        }
        contentValues.put("contentTitle", str);
        contentValues.put("flags", Integer.valueOf(this.f937a.f));
        contentValues.put("number", Integer.valueOf(this.f937a.e));
        contentValues.put("ongoing", Integer.valueOf((this.f937a.f & 2) > 0 ? 1 : 0));
        contentValues.put("percentage", Float.valueOf(this.f937a.f922c == -1 ? -1.0f : (((float) this.f937a.d) / ((float) this.f937a.f922c)) * 100.0f));
        contentValues.put("timestamp", Long.valueOf(System.currentTimeMillis()));
        if (this.f937a.g != null) {
            this.f937a.g.a(contentValues, "pendingIntentClass", "pendingIntentAction", "pendingIntentType", "pendingIntentData", "pendingIntentExtras", "pendingIntentFlags");
        }
        if (this.f937a.h != null) {
            this.f937a.h.a(contentValues, "deleteIntentClass", "deleteIntentAction", "deleteIntentType", "deleteIntentData", "deleteIntentExtras", "deleteIntentFlags");
        }
        contentValues.put("priority", Integer.valueOf(this.f937a.i));
        this.f938b.f933a.getContentResolver().update(e.a(this.f938b.f935c, this.f937a.f920a, this.f937a.f921b), contentValues, null, null);
        return null;
    }
}
