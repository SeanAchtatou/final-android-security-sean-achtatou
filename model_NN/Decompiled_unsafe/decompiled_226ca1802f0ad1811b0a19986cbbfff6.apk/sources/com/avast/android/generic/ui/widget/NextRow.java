package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class NextRow extends Row {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f1092a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public String f1093b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public FragmentManager f1094c;
    /* access modifiers changed from: private */
    public int m;
    private ImageView n;
    private ImageView o;
    private int p;

    public NextRow(Context context) {
        super(context);
    }

    public NextRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row_Next));
        b(context, context.obtainStyledAttributes(attributeSet, z.g, i, y.Row_Next));
    }

    public NextRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowNextStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowNextStyle, y.Row_Next));
        b(context, context.obtainStyledAttributes(attributeSet, z.g, n.rowNextStyle, y.Row_Next));
    }

    private void a(Context context, TypedArray typedArray) {
        typedArray.recycle();
    }

    private void b(Context context, TypedArray typedArray) {
        this.f1093b = typedArray.getString(1);
        this.m = typedArray.getResourceId(2, 0);
        this.p = typedArray.getResourceId(3, 0);
        this.f1092a = typedArray.getBoolean(4, false);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_next, this);
        this.n = (ImageView) findViewById(r.c_next_arrow);
        this.o = (ImageView) findViewById(r.icon);
        if (this.p != 0) {
            this.o.setVisibility(0);
            this.o.setImageResource(this.p);
            if (this.f1092a) {
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.o.getLayoutParams();
                layoutParams.gravity = 48;
                this.o.setLayoutParams(layoutParams);
            }
        }
        a(new j(this));
    }
}
