package com.avast.android.generic.licensing.c;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.RemoteException;
import android.text.TextUtils;
import com.android.vending.a.a;
import com.avast.android.generic.util.ga.TrackedFragment;
import com.avast.android.generic.util.s;
import com.avast.android.generic.util.t;
import com.avast.c.a.a.ag;
import com.avast.c.a.a.ai;
import com.avast.c.a.a.al;
import com.avast.c.a.a.q;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONException;

/* compiled from: IabHelper */
public class b {

    /* renamed from: a  reason: collision with root package name */
    String f857a = "IabHelper";

    /* renamed from: b  reason: collision with root package name */
    boolean f858b = false;

    /* renamed from: c  reason: collision with root package name */
    boolean f859c = false;
    boolean d = false;
    String e = "";
    Context f;
    a g;
    ServiceConnection h;
    int i;
    String j;
    g k;

    public b(Context context) {
        this.f = context.getApplicationContext();
        c("IAB helper created.");
    }

    public void a(h hVar) {
        if (this.f858b) {
            throw new IllegalStateException("IAB helper is already set up.");
        }
        c("Starting in-app billing setup.");
        this.h = new c(this, hVar);
        Intent intent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        if (!this.f.getPackageManager().queryIntentServices(intent, 0).isEmpty()) {
            this.f.bindService(intent, this.h, 1);
        } else if (hVar != null) {
            hVar.a(new i(3, "Billing service unavailable on device."));
        }
    }

    public void a() {
        c("Disposing.");
        this.f858b = false;
        if (this.h != null) {
            c("Unbinding from service.");
            if (this.f != null) {
                try {
                    this.f.unbindService(this.h);
                } catch (Exception e2) {
                }
            }
            this.h = null;
            this.g = null;
            this.k = null;
        }
    }

    public boolean b() {
        return this.f859c;
    }

    public void a(TrackedFragment trackedFragment, String str, int i2, g gVar, String str2) {
        a(trackedFragment, str, "inapp", i2, gVar, str2);
    }

    public void b(TrackedFragment trackedFragment, String str, int i2, g gVar, String str2) {
        a(trackedFragment, str, "subs", i2, gVar, str2);
    }

    public void a(TrackedFragment trackedFragment, String str, String str2, int i2, g gVar, String str3) {
        a("launchPurchaseFlow");
        b("launchPurchaseFlow");
        if (!str2.equals("subs") || this.f859c) {
            try {
                c("Constructing buy intent for " + str + ", item type: " + str2);
                Bundle a2 = this.g.a(3, this.f.getPackageName(), str, str2, str3);
                int a3 = a(a2);
                if (a3 != 0) {
                    d("Unable to buy item, Error response: " + a(a3));
                    i iVar = new i(a3, "Unable to buy item");
                    if (gVar != null) {
                        gVar.a(iVar, null);
                    }
                    c();
                    return;
                }
                c("Launching buy intent for " + str + ". Request code: " + i2);
                this.i = i2;
                this.k = gVar;
                this.j = str2;
                IntentSender intentSender = ((PendingIntent) a2.getParcelable("BUY_INTENT")).getIntentSender();
                Intent intent = new Intent();
                Integer num = 0;
                int intValue = num.intValue();
                Integer num2 = 0;
                int intValue2 = num2.intValue();
                Integer num3 = 0;
                trackedFragment.a(intentSender, i2, intent, intValue, intValue2, num3.intValue());
            } catch (IntentSender.SendIntentException e2) {
                d("SendIntentException while launching purchase flow for sku " + str);
                e2.printStackTrace();
                i iVar2 = new i(-1004, "Failed to send intent.");
                if (gVar != null) {
                    gVar.a(iVar2, null);
                }
                c();
            } catch (RemoteException e3) {
                d("RemoteException while launching purchase flow for sku " + str);
                e3.printStackTrace();
                i iVar3 = new i(-1001, "Remote exception while starting purchase flow");
                if (gVar != null) {
                    gVar.a(iVar3, null);
                }
                c();
            }
        } else {
            i iVar4 = new i(-1009, "Subscriptions are not available.");
            if (gVar != null) {
                gVar.a(iVar4, null);
            }
            c();
        }
    }

    public boolean a(int i2, int i3, Intent intent) {
        if (i2 != this.i) {
            return false;
        }
        a("handleActivityResult");
        c();
        if (intent == null) {
            d("Null data in IAB activity result.");
            i iVar = new i(-1002, "Null data in IAB result");
            if (this.k != null) {
                this.k.a(iVar, null);
            }
            return true;
        }
        int a2 = a(intent);
        String stringExtra = intent.getStringExtra("INAPP_PURCHASE_DATA");
        String stringExtra2 = intent.getStringExtra("INAPP_DATA_SIGNATURE");
        if (i3 == -1 && a2 == 0) {
            c("Successful resultcode from purchase activity.");
            c("Purchase data: " + stringExtra);
            c("Data signature: " + stringExtra2);
            c("Extras: " + intent.getExtras());
            c("Expected item type: " + this.j);
            if (stringExtra == null || stringExtra2 == null) {
                d("BUG: either purchaseData or dataSignature is null.");
                c("Extras: " + intent.getExtras().toString());
                i iVar2 = new i(-1008, "IAB returned null purchaseData or dataSignature");
                if (this.k != null) {
                    this.k.a(iVar2, null);
                }
                return true;
            }
            try {
                l lVar = new l(this.j, stringExtra, stringExtra2);
                if (this.k != null) {
                    this.k.a(new i(0, "Success"), lVar);
                }
            } catch (JSONException e2) {
                d("Failed to parse purchase data.");
                e2.printStackTrace();
                i iVar3 = new i(-1002, "Failed to parse purchase data.");
                if (this.k != null) {
                    this.k.a(iVar3, null);
                }
                return true;
            }
        } else if (i3 == -1) {
            c("Result code was OK but in-app billing response was not OK: " + a(a2));
            if (this.k != null) {
                this.k.a(new i(a2, "Problem purchashing item."), null);
            }
        } else if (i3 == 0) {
            c("Purchase canceled - Response: " + a(a2));
            i iVar4 = new i(-1005, "User canceled.");
            if (this.k != null) {
                this.k.a(iVar4, null);
            }
        } else {
            d("Purchase failed. Result code: " + Integer.toString(i3) + ". Response: " + a(a2));
            i iVar5 = new i(-1006, "Unknown purchase response.");
            if (this.k != null) {
                this.k.a(iVar5, null);
            }
        }
        return true;
    }

    public j a(boolean z, List<String> list, List<String> list2) {
        int a2;
        a("queryInventory");
        try {
            j jVar = new j();
            int a3 = a(jVar, "inapp");
            if (a3 != 0) {
                throw new a(a3, "Error refreshing inventory (querying owned items).");
            }
            if (z) {
                int a4 = a("inapp", jVar, list);
                if (a4 != 0) {
                    throw new a(a4, "Error refreshing inventory (querying prices of items).");
                }
            }
            if (this.f859c) {
                int a5 = a(jVar, "subs");
                if (a5 != 0) {
                    throw new a(a5, "Error refreshing inventory (querying owned subscriptions).");
                } else if (z && (a2 = a("subs", jVar, list)) != 0) {
                    throw new a(a2, "Error refreshing inventory (querying prices of subscriptions).");
                }
            }
            return jVar;
        } catch (RemoteException e2) {
            throw new a(-1001, "Remote exception while refreshing inventory.", e2);
        } catch (JSONException e3) {
            throw new a(-1002, "Error parsing JSON response while refreshing inventory.", e3);
        }
    }

    public List<k> a(List<q> list) {
        String packageName;
        int a2;
        a("queryAvailableSkus");
        try {
            boolean a3 = s.a(this.f);
            LinkedList linkedList = new LinkedList();
            if (list.isEmpty()) {
                return linkedList;
            }
            LinkedList linkedList2 = new LinkedList();
            for (q next : list) {
                if (next.b()) {
                    linkedList2.add(next.c());
                }
            }
            j jVar = new j();
            int a4 = a("inapp", jVar, linkedList2);
            if (a4 != 0) {
                throw new a(a4, "Error refreshing inventory (querying prices of items).");
            } else if (!this.f859c || (a2 = a("subs", jVar, linkedList2)) == 0) {
                for (q next2 : list) {
                    if (next2.b()) {
                        if (next2.i() > 0.0f) {
                            LinkedList linkedList3 = new LinkedList();
                            if (next2.O() > 0) {
                                linkedList3.addAll(next2.N());
                            } else {
                                linkedList3.add(ai.h().a(al.MANUFACTURER_STORE).a(ag.GOOGLE_PLAY.getNumber()).build());
                            }
                            if (jVar.b(next2.c())) {
                                boolean z = true;
                                n a5 = jVar.a(next2.c());
                                if (!a3 && a5.a() != null && a5.a().startsWith("android.test")) {
                                    z = false;
                                } else if (!(a5.b() == null || !a5.b().equals("subs") || (packageName = this.f.getPackageName()) == null)) {
                                    if (packageName.equals("com.avast.android.mobilesecurity") && !next2.c().contains(".mobilesecurity")) {
                                        z = false;
                                    } else if (packageName.equals("com.avast.android.backup") && !next2.c().contains(".backup")) {
                                        z = false;
                                    } else if (packageName.equals("com.avast.android.antitheft") && !next2.c().contains(".antitheft")) {
                                        z = false;
                                    } else if (packageName.equals("com.avast.android.at_play") && !next2.c().contains(".antitheft")) {
                                        z = false;
                                    } else if (packageName.equals("com.avast.android.vpn") && !next2.c().contains(".vpn")) {
                                        z = false;
                                    }
                                }
                                if (z) {
                                    linkedList.add(new k(this.f, next2, a5, linkedList3));
                                }
                            }
                        } else {
                            n nVar = new n("free", next2.c(), "free", null, next2.e(), next2.q());
                            LinkedList linkedList4 = new LinkedList();
                            linkedList4.add(ai.h().a(al.MANUFACTURER_STORE).a(ag.GOOGLE_PLAY.getNumber()).build());
                            linkedList.add(new k(this.f, next2, nVar, linkedList4));
                        }
                    }
                }
                return linkedList;
            } else {
                throw new a(a2, "Error refreshing inventory (querying prices of subscriptions).");
            }
        } catch (RemoteException e2) {
            throw new a(-1001, "Remote exception while refreshing inventory.", e2);
        } catch (JSONException e3) {
            throw new a(-1002, "Error parsing JSON response while refreshing inventory.", e3);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(l lVar) {
        a("consume");
        if (!lVar.f872a.equals("inapp")) {
            throw new a(-1010, "Items of type '" + lVar.f872a + "' can't be consumed.");
        }
        try {
            String g2 = lVar.g();
            String c2 = lVar.c();
            if (g2 == null || g2.equals("")) {
                d("Can't consume " + c2 + ". No token.");
                throw new a(-1007, "PurchaseInfo is missing token for sku: " + c2 + " " + lVar);
            }
            c("Consuming sku: " + c2 + ", token: " + g2);
            int b2 = this.g.b(3, this.f.getPackageName(), g2);
            if (b2 == 0) {
                c("Successfully consumed sku: " + c2);
            } else {
                c("Error consuming consuming sku " + c2 + ". " + a(b2));
                throw new a(b2, "Error consuming sku " + c2);
            }
        } catch (RemoteException e2) {
            throw new a(-1001, "Remote exception while consuming. PurchaseInfo: " + lVar, e2);
        }
    }

    public void a(List<l> list, f fVar) {
        a("consume");
        a(list, (e) null, fVar);
    }

    public static String a(int i2) {
        String[] split = "0:OK/1:User Canceled/2:Unknown/3:Billing Unavailable/4:Item unavailable/5:Developer Error/6:Error/7:Item Already Owned/8:Item not owned".split("/");
        String[] split2 = "0:OK/-1001:Remote exception during initialization/-1002:Bad response received/-1003:Purchase signature verification failed/-1004:Send intent failed/-1005:User cancelled/-1006:Unknown purchase response/-1007:Missing token/-1008:Unknown error/-1009:Subscriptions not available/-1010:Invalid consumption attempt".split("/");
        if (i2 <= -1000) {
            int i3 = -1000 - i2;
            if (i3 < 0 || i3 >= split2.length) {
                return String.valueOf(i2) + ":Unknown IAB Helper Error";
            }
            return split2[i3];
        } else if (i2 < 0 || i2 >= split.length) {
            return String.valueOf(i2) + ":Unknown";
        } else {
            return split[i2];
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (!this.f858b) {
            d("Illegal state for operation (" + str + "): IAB helper is not set up.");
            throw new IllegalStateException("IAB helper is not set up. Can't perform operation: " + str);
        }
    }

    /* access modifiers changed from: package-private */
    public int a(Bundle bundle) {
        Object obj = bundle.get("RESPONSE_CODE");
        if (obj == null) {
            c("Bundle with null response code, assuming OK (known issue)");
            return 0;
        } else if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        } else {
            if (obj instanceof Long) {
                return (int) ((Long) obj).longValue();
            }
            d("Unexpected type for bundle response code.");
            d(obj.getClass().getName());
            throw new RuntimeException("Unexpected type for bundle response code: " + obj.getClass().getName());
        }
    }

    /* access modifiers changed from: package-private */
    public int a(Intent intent) {
        Object obj = intent.getExtras().get("RESPONSE_CODE");
        if (obj == null) {
            d("Intent with no response code, assuming OK (known issue)");
            return 0;
        } else if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        } else {
            if (obj instanceof Long) {
                return (int) ((Long) obj).longValue();
            }
            d("Unexpected type for intent response code.");
            d(obj.getClass().getName());
            throw new RuntimeException("Unexpected type for intent response code: " + obj.getClass().getName());
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.d) {
            throw new IllegalStateException("Can't start async operation (" + str + ") because another async operation(" + this.e + ") is in progress" + ".");
        }
        this.e = str;
        this.d = true;
        c("Starting async operation: " + str);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        c("Ending async operation: " + this.e);
        this.e = "";
        this.d = false;
    }

    /* access modifiers changed from: package-private */
    public int a(j jVar, String str) {
        c("Querying owned items, item type: " + str);
        c("Package name: " + this.f.getPackageName());
        String str2 = null;
        do {
            c("Calling getPurchases with continuation token: " + str2);
            Bundle a2 = this.g.a(3, this.f.getPackageName(), str, str2);
            int a3 = a(a2);
            c("Owned items response: " + String.valueOf(a3));
            if (a3 != 0) {
                c("getPurchases() failed: " + a(a3));
                return a3;
            } else if (!a2.containsKey("INAPP_PURCHASE_ITEM_LIST") || !a2.containsKey("INAPP_PURCHASE_DATA_LIST") || !a2.containsKey("INAPP_DATA_SIGNATURE_LIST")) {
                d("Bundle returned from getPurchases() doesn't contain required fields.");
                return -1002;
            } else {
                ArrayList<String> stringArrayList = a2.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
                ArrayList<String> stringArrayList2 = a2.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> stringArrayList3 = a2.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
                for (int i2 = 0; i2 < stringArrayList2.size(); i2++) {
                    String str3 = stringArrayList2.get(i2);
                    c("Sku is owned: " + stringArrayList.get(i2));
                    l lVar = new l(str, str3, stringArrayList3.get(i2));
                    if (TextUtils.isEmpty(lVar.g())) {
                        e("BUG: empty/null token!");
                        c("Purchase data: " + str3);
                    }
                    jVar.a(lVar);
                }
                str2 = a2.getString("INAPP_CONTINUATION_TOKEN");
                c("Continuation token: " + str2);
            }
        } while (!TextUtils.isEmpty(str2));
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int a(String str, j jVar, List<String> list) {
        c("Querying SKU details.");
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(jVar.c(str));
        if (list != null) {
            arrayList.addAll(list);
        }
        if (arrayList.size() == 0) {
            c("queryPrices: nothing to do because there are no SKUs.");
            return 0;
        }
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("ITEM_ID_LIST", arrayList);
        Bundle a2 = this.g.a(3, this.f.getPackageName(), str, bundle);
        if (!a2.containsKey("DETAILS_LIST")) {
            int a3 = a(a2);
            if (a3 != 0) {
                c("getSkuDetails() failed: " + a(a3));
                return a3;
            }
            d("getSkuDetails() returned a bundle with neither an error nor a detail list.");
            return -1002;
        }
        Iterator<String> it = a2.getStringArrayList("DETAILS_LIST").iterator();
        while (it.hasNext()) {
            n nVar = new n(str, it.next());
            c("Got sku details: " + nVar);
            jVar.a(nVar);
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public void a(List<l> list, e eVar, f fVar) {
        b("consume");
        new Thread(new d(this, list, eVar, fVar)).start();
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        t.c(this.f857a + ": " + str);
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        t.e(this.f857a + ": In-app billing error: " + str);
    }

    /* access modifiers changed from: package-private */
    public void e(String str) {
        t.d(this.f857a + ": In-app billing warning: " + str);
    }

    public boolean d() {
        return this.f858b;
    }
}
