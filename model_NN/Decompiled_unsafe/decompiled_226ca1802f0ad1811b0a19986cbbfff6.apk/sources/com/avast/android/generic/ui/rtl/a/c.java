package com.avast.android.generic.ui.rtl.a;

import java.util.ArrayList;

/* compiled from: ArabicUtilities */
public class c {
    private static boolean a(char c2) {
        for (char[] cArr : a.h) {
            if (cArr[0] == c2) {
                return true;
            }
        }
        for (char c3 : a.g) {
            if (c3 == c2) {
                return true;
            }
        }
        return false;
    }

    private static String[] e(String str) {
        if (str != null) {
            return str.split("\\s");
        }
        return new String[0];
    }

    public static boolean a(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (a(str.charAt(i))) {
                return true;
            }
        }
        return false;
    }

    public static boolean b(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!a(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static String[] f(String str) {
        ArrayList arrayList = new ArrayList();
        String str2 = "";
        for (int i = 0; i < str.length(); i++) {
            if (a(str.charAt(i))) {
                if (str2.equals("") || b(str2)) {
                    str2 = str2 + str.charAt(i);
                } else {
                    arrayList.add(str2);
                    str2 = "" + str.charAt(i);
                }
            } else if (str2.equals("") || !b(str2)) {
                str2 = str2 + str.charAt(i);
            } else {
                arrayList.add(str2);
                str2 = "" + str.charAt(i);
            }
        }
        if (!str2.equals("")) {
            arrayList.add(str2);
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }

    public static String c(String str) {
        if (str == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        String[] split = str.split("\n");
        for (int i = 0; i < split.length; i++) {
            stringBuffer.append(d(split[i]));
            if (i < split.length - 1) {
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public static String d(String str) {
        String[] e = e(str);
        StringBuffer stringBuffer = new StringBuffer("");
        for (int i = 0; i < e.length; i++) {
            if (!a(e[i])) {
                stringBuffer.append(e[i]);
            } else if (b(e[i])) {
                stringBuffer.append(new a(e[i]).a());
            } else {
                String[] f = f(e[i]);
                for (String aVar : f) {
                    stringBuffer.append(new a(aVar).a());
                }
            }
            if (i < e.length - 1) {
                stringBuffer.append(" ");
            }
        }
        return stringBuffer.toString();
    }
}
