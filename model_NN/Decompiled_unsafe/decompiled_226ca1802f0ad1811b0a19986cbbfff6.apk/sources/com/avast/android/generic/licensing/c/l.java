package com.avast.android.generic.licensing.c;

import org.json.JSONObject;

/* compiled from: Purchase */
public class l {

    /* renamed from: a  reason: collision with root package name */
    String f872a;

    /* renamed from: b  reason: collision with root package name */
    String f873b;

    /* renamed from: c  reason: collision with root package name */
    String f874c;
    String d;
    long e;
    m f;
    String g;
    String h;
    String i;
    String j;

    public l(String str, String str2, String str3) {
        this.f872a = str;
        this.i = str2;
        JSONObject jSONObject = new JSONObject(this.i);
        this.f873b = jSONObject.optString("orderId");
        this.f874c = jSONObject.optString("packageName");
        this.d = jSONObject.optString("productId");
        this.e = jSONObject.optLong("purchaseTime");
        this.f = m.a(jSONObject.optInt("purchaseState"));
        this.g = jSONObject.optString("developerPayload");
        this.h = jSONObject.optString("token", jSONObject.optString("purchaseToken"));
        this.j = str3;
    }

    public l(String str, String str2, String str3, String str4, String str5) {
        this.f872a = str;
        this.f873b = str2;
        this.f874c = str3;
        this.d = str4;
        this.e = System.currentTimeMillis();
        this.f = m.PURCHASED;
        this.g = str5;
        this.h = "";
        this.j = "";
        this.i = "{\"packageName\":\"" + str3 + "\"," + "\"orderId\":\"" + str2 + "\"," + "\"productId\":\"" + str4 + "\"," + "\"developerPayload\":\"" + str5 + "\"," + "\"purchaseTime\":" + this.e + "," + "\"purchaseState\":" + this.f.ordinal() + "," + "\"purchaseToken\":\"" + this.h + "\"" + "}";
    }

    public String a() {
        return this.f872a;
    }

    public String b() {
        return this.f873b;
    }

    public String c() {
        return this.d;
    }

    public long d() {
        return this.e;
    }

    public m e() {
        return this.f;
    }

    public String f() {
        return this.g;
    }

    public String g() {
        return this.h;
    }

    public String h() {
        return this.i;
    }

    public String i() {
        return this.j;
    }

    public String toString() {
        return "PurchaseInfo(type:" + this.f872a + "):" + this.i;
    }
}
