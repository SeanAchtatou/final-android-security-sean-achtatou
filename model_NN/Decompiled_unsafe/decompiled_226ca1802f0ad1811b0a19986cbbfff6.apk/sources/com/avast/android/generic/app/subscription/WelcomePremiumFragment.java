package com.avast.android.generic.app.subscription;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.avast.android.generic.ab;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.x;

public class WelcomePremiumFragment extends TrackedMultiToolFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Uri f520a = Uri.parse("market://details?id=com.avast.android.backup&referrer=utm_source%3DAMS%26utm_medium%3Dafterbuy%26utm_content%3Dmobile-backup%26utm_campaign%3DMB");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public static final Uri f521b = Uri.parse("market://details?id=com.avast.android.at_play&referrer=utm_source%3DAMS%26utm_medium%3Dafterbuy%26utm_content%3Danti-theft%26utm_campaign%3DAT");

    /* renamed from: c  reason: collision with root package name */
    private Button f522c;
    private Button d;
    private Button e;
    private Button f;
    private Button g;
    private View h;
    private View i;
    private View j;
    private View k;
    private BroadcastReceiver l = new ah(this);

    public String c() {
        return "subscription/welcome";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(t.fragment_welcome_premium, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        String string;
        super.onViewCreated(view, bundle);
        this.f522c = (Button) view.findViewById(r.subscription_welcome_button_go_back);
        this.d = (Button) view.findViewById(r.subscription_welcome_button_install_ams);
        this.e = (Button) view.findViewById(r.subscription_welcome_button_install_at);
        this.f = (Button) view.findViewById(r.subscription_welcome_button_install_backup);
        this.g = (Button) view.findViewById(r.subscription_welcome_button_signup);
        this.h = view.findViewById(r.subscription_welcome_group_ams_installed);
        this.i = view.findViewById(r.subscription_welcome_group_at_installed);
        this.j = view.findViewById(r.subscription_welcome_group_backup_installed);
        this.k = view.findViewById(r.subscription_welcome_group_signedin);
        TextView textView = (TextView) view.findViewById(r.subscription_issues_support);
        if (j()) {
            string = getString(x.product_at);
        } else if (k()) {
            string = getString(x.product_backup);
        } else {
            string = getString(x.product_ams);
        }
        this.f522c.setText(getString(x.l_subscription_welcome_go_back, string));
        this.f522c.setOnClickListener(new ai(this));
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public void onResume() {
        super.onResume();
        f();
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme("package");
        getActivity().registerReceiver(this.l, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED");
        intentFilter2.addAction("com.avast.android.mobilesecurity.app.account.ACCOUNT_DISCONNECTED");
        getActivity().registerReceiver(this.l, intentFilter2);
    }

    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(this.l);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (aa.a(getActivity())) {
            this.h.setVisibility(0);
            this.d.setVisibility(8);
        } else {
            this.h.setVisibility(8);
            this.d.setVisibility(0);
            this.d.setOnClickListener(new aj(this));
        }
        if (aa.b(getActivity()) != null) {
            this.i.setVisibility(0);
            this.e.setVisibility(8);
        } else {
            this.i.setVisibility(8);
            this.e.setVisibility(0);
            this.e.setOnClickListener(new ak(this));
        }
        if (aa.d(getActivity())) {
            this.j.setVisibility(0);
            this.f.setVisibility(8);
        } else {
            this.j.setVisibility(8);
            this.f.setVisibility(0);
            this.f.setOnClickListener(new al(this));
        }
        ab abVar = (ab) com.avast.android.generic.aa.a(getActivity(), ab.class);
        if (abVar.t()) {
            this.k.setVisibility(0);
            this.g.setVisibility(8);
            ((TextView) this.k.findViewById(r.subscription_welcome_signed_in_text)).setText(getString(x.l_subscription_welcome_get_most_signedin_account, abVar.v()));
            return;
        }
        this.k.setVisibility(8);
        this.g.setVisibility(0);
        this.g.setOnClickListener(new am(this));
    }
}
