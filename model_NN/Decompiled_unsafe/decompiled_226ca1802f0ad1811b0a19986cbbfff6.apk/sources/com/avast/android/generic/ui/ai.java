package com.avast.android.generic.ui;

import android.view.View;
import com.avast.android.generic.r;
import java.util.Locale;

/* compiled from: VoucherDialog */
class ai implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f1015a;

    ai(ah ahVar) {
        this.f1015a = ahVar;
    }

    public void onClick(View view) {
        String upperCase = this.f1015a.f1014b.f996a.getText().toString().toUpperCase(Locale.getDefault());
        if (upperCase.length() >= 10) {
            this.f1015a.f1014b.a(r.message_voucher_successful, upperCase);
            this.f1015a.f1013a.dismiss();
        }
    }
}
