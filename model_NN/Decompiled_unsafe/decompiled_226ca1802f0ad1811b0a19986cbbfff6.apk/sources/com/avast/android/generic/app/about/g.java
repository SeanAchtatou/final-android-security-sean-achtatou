package com.avast.android.generic.app.about;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: FeedbackFragment */
class g implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f344a;

    g(FeedbackFragment feedbackFragment) {
        this.f344a = feedbackFragment;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (this.f344a.g()) {
            this.f344a.i.setVisibility(8);
        }
    }
}
