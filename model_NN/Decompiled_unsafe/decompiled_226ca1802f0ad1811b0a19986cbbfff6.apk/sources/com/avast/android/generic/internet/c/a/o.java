package com.avast.android.generic.internet.c.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.io.InputStream;

/* compiled from: MyAvastPairing */
public final class o extends GeneratedMessageLite implements s {

    /* renamed from: a  reason: collision with root package name */
    private static final o f775a = new o(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f776b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public q f777c;
    private byte d;
    private int e;

    private o(p pVar) {
        super(pVar);
        this.d = -1;
        this.e = -1;
    }

    private o(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static o a() {
        return f775a;
    }

    public boolean b() {
        return (this.f776b & 1) == 1;
    }

    public q c() {
        return this.f777c;
    }

    private void g() {
        this.f777c = q.OK;
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f776b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f777c.getNumber());
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f776b & 1) == 1) {
                i = 0 + CodedOutputStream.computeEnumSize(1, this.f777c.getNumber());
            }
            this.e = i;
        }
        return i;
    }

    public static o a(InputStream inputStream) {
        return ((p) d().mergeFrom(inputStream)).h();
    }

    public static p d() {
        return p.g();
    }

    /* renamed from: e */
    public p newBuilderForType() {
        return d();
    }

    public static p a(o oVar) {
        return d().mergeFrom(oVar);
    }

    /* renamed from: f */
    public p toBuilder() {
        return a(this);
    }

    static {
        f775a.g();
    }
}
