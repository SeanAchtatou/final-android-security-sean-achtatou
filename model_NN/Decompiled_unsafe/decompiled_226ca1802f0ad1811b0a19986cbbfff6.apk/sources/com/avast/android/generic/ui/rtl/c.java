package com.avast.android.generic.ui.rtl;

import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;
import com.avast.android.generic.g;
import com.avast.android.generic.r;
import java.util.Locale;
import java.util.Stack;

/* compiled from: RtlHelper */
public class c {

    /* renamed from: a  reason: collision with root package name */
    private Context f1064a;

    /* renamed from: b  reason: collision with root package name */
    private String f1065b = g.a(this.f1064a);

    public c(Context context) {
        this.f1064a = context;
    }

    public static boolean a() {
        return Locale.getDefault().getLanguage().equals(a.LANG_ARABIC);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.rtl.c.a(android.view.View, boolean):android.view.View
     arg types: [android.view.View, int]
     candidates:
      com.avast.android.generic.ui.rtl.c.a(java.util.Stack<android.view.ViewGroup>, android.view.View):boolean
      com.avast.android.generic.ui.rtl.c.a(android.view.View, boolean):android.view.View */
    public View a(View view) {
        return a(view, false);
    }

    public View a(View view, boolean z) {
        if (Build.VERSION.SDK_INT >= 17) {
            return view;
        }
        if (!this.f1065b.equals(a.LANG_ARABIC) && (!this.f1065b.equals("") || !a())) {
            return view;
        }
        this.f1065b = a.LANG_ARABIC;
        if (view.getClass().getName().equals("com.android.internal.view.menu.ListMenuItemView")) {
            return view;
        }
        View b2 = b(view, z);
        if (b2 instanceof ViewGroup) {
            Stack stack = new Stack();
            stack.add((ViewGroup) b2);
            while (!stack.isEmpty()) {
                ViewGroup viewGroup = (ViewGroup) stack.pop();
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View childAt = viewGroup.getChildAt(i);
                    a(stack, childAt);
                    b(childAt, z);
                }
            }
        }
        return b2;
    }

    public void b(View view) {
        view.setTag(r.rtl_support_view_converted_tag, false);
    }

    private boolean a(Stack<ViewGroup> stack, View view) {
        if (view instanceof AdapterView) {
            AdapterView adapterView = (AdapterView) view;
            for (int i = 0; i < adapterView.getChildCount(); i++) {
                a(stack, adapterView.getChildAt(i));
            }
            return true;
        } else if (!(view instanceof ViewGroup)) {
            return false;
        } else {
            stack.add((ViewGroup) view);
            return true;
        }
    }

    private View b(View view, boolean z) {
        Boolean bool = (Boolean) view.getTag(r.rtl_support_view_converted_tag);
        if ((bool == null || !bool.booleanValue()) && (!z || (view instanceof TextView))) {
            a converter = a.getConverter(view, this.f1065b);
            if (converter != null) {
                view = converter.ltrToRtlView(view);
            }
            view.setTag(r.rtl_support_view_converted_tag, true);
        }
        return view;
    }
}
