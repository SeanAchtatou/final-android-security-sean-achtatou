package com.avast.android.generic.ui.rtl.a;

/* compiled from: ArabicReshaper */
class b {

    /* renamed from: a  reason: collision with root package name */
    char[] f1060a;

    /* renamed from: b  reason: collision with root package name */
    int[] f1061b;

    /* renamed from: c  reason: collision with root package name */
    char[] f1062c;
    int[] d;
    final /* synthetic */ a e;

    b(a aVar, String str) {
        int i = 0;
        this.e = aVar;
        int length = str.length();
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            if (aVar.b(str.charAt(i3))) {
                i2++;
            }
        }
        this.f1061b = new int[i2];
        this.f1060a = new char[i2];
        this.d = new int[(length - i2)];
        this.f1062c = new char[(length - i2)];
        int i4 = 0;
        for (int i5 = 0; i5 < str.length(); i5++) {
            if (aVar.b(str.charAt(i5))) {
                this.f1061b[i4] = i5;
                this.f1060a[i4] = str.charAt(i5);
                i4++;
            } else {
                this.d[i] = i5;
                this.f1062c[i] = str.charAt(i5);
                i++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        char[] cArr = new char[(str.length() + this.f1060a.length)];
        for (int i = 0; i < this.d.length; i++) {
            cArr[this.d[i]] = str.charAt(i);
        }
        for (int i2 = 0; i2 < this.f1061b.length; i2++) {
            cArr[this.f1061b[i2]] = this.f1060a[i2];
        }
        return new String(cArr);
    }
}
