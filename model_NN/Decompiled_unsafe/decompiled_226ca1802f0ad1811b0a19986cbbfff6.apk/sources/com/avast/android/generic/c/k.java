package com.avast.android.generic.c;

import android.content.Context;
import android.text.TextUtils;
import com.avast.android.generic.util.aa;

/* compiled from: CommandPort */
public class k {

    /* renamed from: a  reason: collision with root package name */
    public o f639a;

    /* renamed from: b  reason: collision with root package name */
    public String f640b;

    /* renamed from: c  reason: collision with root package name */
    public String f641c;

    public k() {
        this.f639a = o.INVALID;
        this.f640b = null;
    }

    public k(o oVar, String str) {
        this.f639a = oVar;
        this.f640b = str;
    }

    public k(Context context, String str, String str2) {
        this.f639a = o.TOOL;
        this.f640b = str;
        if (TextUtils.isEmpty(str2)) {
            throw new Exception("Target tool is empty");
        }
        if (str2.equalsIgnoreCase("MS")) {
            str2 = "mobilesecurity";
        } else if (str2.equalsIgnoreCase("AT")) {
            String b2 = aa.b(context);
            if (b2 == null) {
                throw new Exception("Target tool not found");
            }
            this.f641c = b2;
            return;
        } else if (str2.equalsIgnoreCase("BU")) {
            str2 = "backup";
        } else if (str2.equalsIgnoreCase("VP")) {
            str2 = "vpn";
        }
        String str3 = "com.avast.android." + str2.toLowerCase();
        if (!aa.b(context, str3)) {
            throw new Exception("Target tool not found");
        }
        this.f641c = str3;
    }
}
