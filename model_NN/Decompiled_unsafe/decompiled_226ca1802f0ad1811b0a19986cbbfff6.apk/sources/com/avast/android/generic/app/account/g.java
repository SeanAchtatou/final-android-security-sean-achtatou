package com.avast.android.generic.app.account;

import android.view.View;
import com.avast.android.generic.util.ab;

/* compiled from: ConnectAccountFragment */
class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f433a;

    g(ConnectAccountFragment connectAccountFragment) {
        this.f433a = connectAccountFragment;
    }

    public void onClick(View view) {
        if (this.f433a.d) {
            this.f433a.a("ms-Wizard", "Avast! Account setup", "connect", 0);
        }
        String str = null;
        if ((this.f433a.y && this.f433a.e) || this.f433a.x.c()) {
            str = ab.c(this.f433a.r.getText().toString());
            if (!ab.b(str) || !this.f433a.n()) {
                return;
            }
        }
        if (this.f433a.o()) {
            this.f433a.a(this.f433a.f, this.f433a.g, str);
        } else if (this.f433a.h()) {
            this.f433a.a(this.f433a.l.getText().toString(), this.f433a.q.getText().toString(), str);
        }
    }
}
