package com.avast.android.generic.util;

import android.content.Context;
import android.os.PowerManager;

/* compiled from: WakeLock */
public class aw {

    /* renamed from: a  reason: collision with root package name */
    private static volatile PowerManager.WakeLock f1197a = null;

    /* renamed from: b  reason: collision with root package name */
    private static volatile PowerManager.WakeLock f1198b = null;

    /* renamed from: c  reason: collision with root package name */
    private static Object f1199c = new Object();

    public static void a(Context context, long j) {
        synchronized (f1199c) {
            ((PowerManager) context.getSystemService("power")).newWakeLock(1, "LW " + context.getPackageName()).acquire(j);
            z.a("AvastWL", "Launch wakelock acquired, will be released in " + j + " milliseconds automatically");
        }
    }

    public static void a(Context context) {
        a(context, 1000);
    }

    public static void a() {
        synchronized (f1199c) {
            try {
                if (f1198b != null) {
                    while (f1198b.isHeld()) {
                        f1198b.release();
                    }
                    f1198b = null;
                    z.a("AvastWL", "Partial wakelock released");
                    b();
                }
            } catch (Exception e) {
            }
        }
    }

    private static void b() {
    }
}
