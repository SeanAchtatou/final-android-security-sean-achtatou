package com.avast.android.generic.ui.widget;

import android.view.View;
import android.widget.AdapterView;

/* compiled from: ContextDialogFragment */
class e implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContextDialogFragment f1142a;

    e(ContextDialogFragment contextDialogFragment) {
        this.f1142a = contextDialogFragment;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        Long l;
        this.f1142a.dismiss();
        if (this.f1142a.f1086b != null) {
            l = Long.valueOf(this.f1142a.f1086b[i]);
        } else {
            l = null;
        }
        if (this.f1142a.getTargetFragment() != null && (this.f1142a.getTargetFragment() instanceof f)) {
            ((f) this.f1142a.getTargetFragment()).a(l, i);
        }
    }
}
