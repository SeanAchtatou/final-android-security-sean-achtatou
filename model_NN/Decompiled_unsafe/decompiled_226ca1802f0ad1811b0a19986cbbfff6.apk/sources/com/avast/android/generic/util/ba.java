package com.avast.android.generic.util;

import java.util.LinkedList;

/* compiled from: Wrappers */
public class ba {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<String> f1208a = new LinkedList<>();

    /* renamed from: b  reason: collision with root package name */
    private LinkedList<String> f1209b = new LinkedList<>();

    /* renamed from: c  reason: collision with root package name */
    private LinkedList<String> f1210c = new LinkedList<>();
    private int d = 0;
    private long e = 0;
    private byte[] f = null;

    public String a() {
        if (this.f1208a.size() == 0) {
            return null;
        }
        return this.f1208a.get(0);
    }

    public String b() {
        if (this.f1209b.size() == 0) {
            return null;
        }
        return this.f1209b.get(0);
    }

    public String c() {
        if (this.f1210c.size() == 0) {
            return null;
        }
        return this.f1210c.get(0);
    }
}
