package com.avast.android.generic.ui.c;

import b.a.a.a.a.a;
import b.a.a.a.a.c;
import b.a.a.a.a.d;
import b.a.a.a.a.i;
import com.avast.android.generic.ui.BaseActivity;
import java.util.LinkedList;
import java.util.Queue;

/* compiled from: CroutonQueue */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static Queue<d> f1025a = new LinkedList();

    public static void a(CharSequence charSequence) {
        a(charSequence, 0, 0, null);
    }

    private static void a(CharSequence charSequence, int i, int i2, i iVar) {
        f1025a.add(new d(charSequence, i, i2, iVar));
    }

    public static void a(BaseActivity baseActivity) {
        CharSequence charSequence;
        LinkedList<d> linkedList = new LinkedList<>(f1025a);
        f1025a.clear();
        for (d dVar : linkedList) {
            i a2 = dVar.f1028c != null ? dVar.f1028c : a.f1022a;
            a aVar = a.f152a;
            if (dVar.d > 0) {
                aVar = new c().a(dVar.d).a();
            }
            CharSequence c2 = dVar.f1026a;
            if (c2 == null) {
                charSequence = baseActivity.getString(dVar.f1027b);
            } else {
                charSequence = c2;
            }
            int e = baseActivity.e();
            if (e != 0) {
                d.a(baseActivity, charSequence, a2, e, aVar);
            } else {
                d.a(baseActivity, charSequence, a2).a(aVar).b();
            }
        }
    }
}
