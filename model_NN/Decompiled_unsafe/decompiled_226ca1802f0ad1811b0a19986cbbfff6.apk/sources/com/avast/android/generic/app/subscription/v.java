package com.avast.android.generic.app.subscription;

import android.content.Intent;
import android.net.Uri;
import android.view.View;
import com.avast.android.generic.a;
import com.avast.android.generic.util.w;
import com.avast.android.generic.x;

/* compiled from: SubscriptionFragment */
class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f578a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f579b;

    v(SubscriptionFragment subscriptionFragment, String str) {
        this.f579b = subscriptionFragment;
        this.f578a = str;
    }

    public void onClick(View view) {
        if (this.f579b.isAdded()) {
            try {
                this.f579b.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(this.f578a)));
            } catch (Exception e) {
                a.a(this.f579b.getActivity(), this.f579b.getString(x.msg_can_not_open_webbrowser, w.a(this.f579b.getActivity(), e)));
            }
        }
    }
}
