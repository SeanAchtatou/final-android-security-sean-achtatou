package com.avast.android.generic.i;

import android.content.Context;
import com.avast.android.generic.util.t;
import com.avast.d.a.a;
import com.avast.d.b.c;
import com.avast.d.b.y;
import com.google.protobuf.MessageLite;

/* compiled from: StreamBackWrapper */
public class d {
    public static y a(Context context, MessageLite messageLite, b bVar, c cVar, byte[] bArr) {
        a aVar = null;
        if (context == null) {
            return null;
        }
        if ((bArr == null && messageLite == null) || bVar == null || cVar == null) {
            return null;
        }
        t.c("StreamBackWrapper: going to sendData");
        com.avast.d.a aVar2 = new com.avast.d.a();
        aVar2.i = c.a();
        aVar2.f = 30000;
        aVar2.g = 30000;
        aVar2.d = a();
        aVar2.e = b();
        a a2 = a.a(context);
        com.avast.d.d dVar = new com.avast.d.d();
        dVar.a(aVar2, a2);
        if (bArr != null) {
            aVar = new a(bArr);
        }
        y a3 = dVar.a(messageLite, bVar.a(), cVar.a(), aVar);
        t.c("StreamBackWrapper: data sent");
        return a3;
    }

    private static String a() {
        return "https://auth.ff.avast.com:443";
    }

    private static String b() {
        return "http://streamback.ff.avast.com:80";
    }
}
