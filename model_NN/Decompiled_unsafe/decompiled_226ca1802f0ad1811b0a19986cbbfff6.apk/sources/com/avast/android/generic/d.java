package com.avast.android.generic;

import android.content.Context;
import java.util.Calendar;

/* compiled from: GenericContract */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f650a = {x.l_mon_short, x.l_tue_short, x.l_wed_short, x.l_thu_short, x.l_fri_short, x.l_sat_short, x.l_sun_short};

    /* renamed from: b  reason: collision with root package name */
    private static final int[] f651b = {x.l_mon, x.l_tue, x.l_wed, x.l_thu, x.l_fri, x.l_sat, x.l_sun};

    /* renamed from: c  reason: collision with root package name */
    private static final int[] f652c = {x.l_monday, x.l_tuesday, x.l_wednesday, x.l_thursday, x.l_friday, x.l_saturday, x.l_sunday};

    public static String a(Context context, int i) {
        return context.getString(f650a[i]);
    }

    public static String b(Context context, int i) {
        return context.getString(f651b[i]);
    }

    public static int a(int i) {
        int i2 = 0;
        int firstDayOfWeek = ((Calendar.getInstance().getFirstDayOfWeek() + 7) - 2) % 7;
        int i3 = 1;
        for (int i4 = 0; i4 < 7; i4++) {
            if ((i & i3) > 0) {
                i2 += a(2, (i4 + firstDayOfWeek) % 7);
            }
            i3 *= 2;
        }
        return i2;
    }

    public static int b(int i) {
        int i2 = 0;
        int firstDayOfWeek = 7 - ((Calendar.getInstance().getFirstDayOfWeek() + 7) - 2);
        int i3 = 1;
        for (int i4 = 0; i4 < 7; i4++) {
            if ((i & i3) > 0) {
                i2 += a(2, (i4 + firstDayOfWeek) % 7);
            }
            i3 *= 2;
        }
        return i2;
    }

    private static int a(int i, int i2) {
        int i3 = 1;
        if (i2 != 0) {
            int i4 = 1;
            i3 = i;
            while (i4 < i2) {
                i4++;
                i3 *= 2;
            }
        }
        return i3;
    }
}
