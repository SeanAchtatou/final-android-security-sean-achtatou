package com.avast.android.generic;

import com.avast.android.antitheft_setup.R;

/* compiled from: R */
public final class z {
    public static final int[] A = {R.attr.show_pictures, R.attr.extra_fields, R.attr.show_title_bar, R.attr.title_text, R.attr.done_button_text, R.attr.title_bar_background, R.attr.done_button_background};
    public static final int[] B = {R.attr.radius_in_meters, R.attr.results_limit, R.attr.search_text, R.attr.show_search_box};
    public static final int[] C = {R.attr.preset_size, R.attr.is_cropped};

    /* renamed from: a  reason: collision with root package name */
    public static final int[] f1263a = {R.attr.actionbarCompatButtonStyle, R.attr.actionbarCompatProgressIndicatorStyle, R.attr.actionbarCompatSeparatorStyle, R.attr.actionbarCompatLogoStyle, R.attr.actionbarCompatTextStyle, R.attr.textHeaderMaxLines, R.attr.trackAbstractMaxLines};

    /* renamed from: b  reason: collision with root package name */
    public static final int[] f1264b = {16842948, 16842964, R.attr.centered, R.attr.strokeWidth, R.attr.fillColor, R.attr.pageColor, R.attr.radius, R.attr.snap, R.attr.strokeColor};

    /* renamed from: c  reason: collision with root package name */
    public static final int[] f1265c = {16842964, R.attr.centered, R.attr.gapWidth, R.attr.unselectedDrawable, R.attr.selectedDrawable, R.attr.drawableWidth, R.attr.drawableHeight};
    public static final int[] d = {R.attr.hagDividerVertical, R.attr.hagSelectableItemBackground};
    public static final int[] e = {16842964, R.attr.centered, R.attr.gapWidth, R.attr.selectedColor, R.attr.strokeWidth, R.attr.unselectedColor, R.attr.lineWidth};
    public static final int[] f = {R.attr.mapType, R.attr.cameraBearing, R.attr.cameraTargetLat, R.attr.cameraTargetLng, R.attr.cameraTilt, R.attr.cameraZoom, R.attr.uiCompass, R.attr.uiRotateGestures, R.attr.uiScrollGestures, R.attr.uiTiltGestures, R.attr.uiZoomControls, R.attr.uiZoomGestures, R.attr.useViewLifecycle, R.attr.zOrderOnTop};
    public static final int[] g = {R.attr.rowNextStyle, R.attr.rowFragmentClass, R.attr.rowLayoutToFragment, R.attr.rowIcon, R.attr.rowIconTopAligned};
    public static final int[] h = {R.attr.rowKey, R.attr.rowTitle, R.attr.rowSubTitle, R.attr.rowDefaultValue, R.attr.rowTitleStyle, R.attr.rowSubTitleStyle, R.attr.rowBackground, R.attr.rowClickable, R.attr.rowFocusable, R.attr.rowStyle, R.attr.rowDependsOn, R.attr.rowDAO, R.attr.rowLayout};
    public static final int[] i = {R.attr.rowEntriesNames, R.attr.rowEntriesValues};
    public static final int[] j = {R.attr.background, R.attr.backgroundSplit, R.attr.divider, R.attr.height, R.attr.subtitleTextStyle, R.attr.titleTextStyle, R.attr.navigationMode, R.attr.displayOptions, R.attr.title, R.attr.subtitle, R.attr.icon, R.attr.logo, R.attr.backgroundStacked, R.attr.customNavigationLayout, R.attr.homeLayout, R.attr.progressBarStyle, R.attr.indeterminateProgressStyle, R.attr.progressBarPadding, R.attr.itemPadding};
    public static final int[] k = {16843071};
    public static final int[] l = {R.attr.background, R.attr.backgroundSplit, R.attr.height, R.attr.subtitleTextStyle, R.attr.titleTextStyle};
    public static final int[] m = {16842964, R.attr.initialActivityCount, R.attr.expandActivityOverflowButtonDrawable};
    public static final int[] n = {16842766, 16842960, 16843156, 16843230, 16843231, 16843232};
    public static final int[] o = {16842754, 16842766, 16842960, 16843014, 16843156, 16843230, 16843231, 16843233, 16843234, 16843235, 16843236, 16843237, 16843375, 16843481, 16843515, 16843516, 16843657};
    public static final int[] p = {R.attr.itemTextAppearance, R.attr.horizontalDivider, R.attr.verticalDivider, R.attr.headerBackground, R.attr.itemBackground, R.attr.windowAnimationStyle, R.attr.itemIconDisabledAlpha, R.attr.preserveIconSpacing};
    public static final int[] q = {16843039, 16843296, 16843364, R.attr.iconifiedByDefault, R.attr.queryHint};
    public static final int[] r = {16842927, 16843125, 16843126, 16843131, 16843362, 16843436, 16843437, 16843834};
    public static final int[] s = {R.attr.actionBarTabStyle, R.attr.actionBarTabBarStyle, R.attr.actionBarTabTextStyle, R.attr.actionOverflowButtonStyle, R.attr.actionBarStyle, R.attr.actionBarSplitStyle, R.attr.actionBarWidgetTheme, R.attr.actionBarSize, R.attr.actionBarDivider, R.attr.actionBarItemBackground, R.attr.actionMenuTextAppearance, R.attr.actionMenuTextColor, R.attr.actionModeStyle, R.attr.actionModeCloseButtonStyle, R.attr.actionModeBackground, R.attr.actionModeSplitBackground, R.attr.actionModeCloseDrawable, R.attr.actionModeShareDrawable, R.attr.actionModePopupWindowStyle, R.attr.buttonStyleSmall, R.attr.selectableItemBackground, R.attr.windowContentOverlay, R.attr.textAppearanceLargePopupMenu, R.attr.textAppearanceSmallPopupMenu, R.attr.textAppearanceSmall, R.attr.textColorPrimary, R.attr.textColorPrimaryDisableOnly, R.attr.textColorPrimaryInverse, R.attr.spinnerItemStyle, R.attr.spinnerDropDownItemStyle, R.attr.searchAutoCompleteTextView, R.attr.searchDropdownBackground, R.attr.searchViewCloseIcon, R.attr.searchViewGoIcon, R.attr.searchViewSearchIcon, R.attr.searchViewVoiceIcon, R.attr.searchViewEditQuery, R.attr.searchViewEditQueryBackground, R.attr.searchViewTextField, R.attr.searchViewTextFieldRight, R.attr.textColorSearchUrl, R.attr.searchResultListItemHeight, R.attr.textAppearanceSearchResultTitle, R.attr.textAppearanceSearchResultSubtitle, R.attr.listPreferredItemHeightSmall, R.attr.listPreferredItemPaddingLeft, R.attr.listPreferredItemPaddingRight, R.attr.textAppearanceListItemSmall, R.attr.windowMinWidthMajor, R.attr.windowMinWidthMinor, R.attr.dividerVertical, R.attr.actionDropDownStyle, R.attr.actionButtonStyle, R.attr.homeAsUpIndicator, R.attr.dropDownListViewStyle, R.attr.popupMenuStyle, R.attr.dropdownListPreferredItemHeight, R.attr.actionSpinnerItemStyle, R.attr.windowNoTitle, R.attr.windowActionBar, R.attr.windowActionBarOverlay, R.attr.windowActionModeOverlay, R.attr.windowSplitActionBar, R.attr.listPopupWindowStyle, R.attr.activityChooserViewStyle, R.attr.activatedBackgroundIndicator, R.attr.dropDownHintAppearance};
    public static final int[] t = {16842970};
    public static final int[] u = {16842901, 16842904, 16842964, R.attr.selectedColor, R.attr.clipPadding, R.attr.footerColor, R.attr.footerLineHeight, R.attr.footerIndicatorStyle, R.attr.footerIndicatorHeight, R.attr.footerIndicatorUnderlinePadding, R.attr.footerPadding, R.attr.linePosition, R.attr.selectedBold, R.attr.titlePadding, R.attr.topPadding};
    public static final int[] v = {16842964, R.attr.selectedColor, R.attr.fades, R.attr.fadeDelay, R.attr.fadeLength};
    public static final int[] w = {R.attr.vpiCirclePageIndicatorStyle, R.attr.vpiIconPageIndicatorStyle, R.attr.vpiLinePageIndicatorStyle, R.attr.vpiTitlePageIndicatorStyle, R.attr.vpiTabPageIndicatorStyle, R.attr.vpiUnderlinePageIndicatorStyle};
    public static final int[] x = {R.attr.rowWeekDaysStyle};
    public static final int[] y = {R.attr.multi_select};
    public static final int[] z = {R.attr.confirm_logout, R.attr.fetch_user_info, R.attr.login_text, R.attr.logout_text};
}
