package com.avast.android.generic.internet.c.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class l extends GeneratedMessageLite implements n {

    /* renamed from: a  reason: collision with root package name */
    private static final l f770a = new l(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f771b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f772c;
    private byte d;
    private int e;

    private l(m mVar) {
        super(mVar);
        this.d = -1;
        this.e = -1;
    }

    private l(boolean z) {
        this.d = -1;
        this.e = -1;
    }

    public static l a() {
        return f770a;
    }

    public boolean b() {
        return (this.f771b & 1) == 1;
    }

    public String c() {
        Object obj = this.f772c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f772c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString g() {
        Object obj = this.f772c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f772c = copyFromUtf8;
        return copyFromUtf8;
    }

    private void h() {
        this.f772c = "";
    }

    public final boolean isInitialized() {
        byte b2 = this.d;
        if (b2 == -1) {
            this.d = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f771b & 1) == 1) {
            codedOutputStream.writeBytes(1, g());
        }
    }

    public int getSerializedSize() {
        int i = this.e;
        if (i == -1) {
            i = 0;
            if ((this.f771b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, g());
            }
            this.e = i;
        }
        return i;
    }

    public static m d() {
        return m.g();
    }

    /* renamed from: e */
    public m newBuilderForType() {
        return d();
    }

    public static m a(l lVar) {
        return d().mergeFrom(lVar);
    }

    /* renamed from: f */
    public m toBuilder() {
        return a(this);
    }

    static {
        f770a.h();
    }
}
