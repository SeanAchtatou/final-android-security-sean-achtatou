package com.avast.android.generic.app.passwordrecovery;

import android.content.Intent;

/* compiled from: PasswordRecoveryDialog */
class h implements j {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ g f489a;

    h(g gVar) {
        this.f489a = gVar;
    }

    public void a(b bVar) {
        Intent intent = new Intent("com.avast.android.generic.app.passwordrecovery.ACTION_NEW_STATE");
        intent.putExtra("state_code", bVar.a());
        this.f489a.f488a.f487b.d.a(intent);
    }
}
