package com.avast.android.generic.ui;

import android.view.View;

/* compiled from: CheckerFragment */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CheckerFragment f1044a;

    h(CheckerFragment checkerFragment) {
        this.f1044a = checkerFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.CheckerFragment.a(com.avast.android.generic.ui.CheckerFragment, boolean):boolean
     arg types: [com.avast.android.generic.ui.CheckerFragment, int]
     candidates:
      com.avast.android.generic.ui.CheckerFragment.a(java.lang.Long, int):void
      com.avast.android.generic.ui.widget.f.a(java.lang.Long, int):void
      com.avast.android.generic.ui.CheckerFragment.a(com.avast.android.generic.ui.CheckerFragment, boolean):boolean */
    public void onClick(View view) {
        if (!this.f1044a.g) {
            this.f1044a.a("checker", "hide ignored", 0);
            boolean unused = this.f1044a.g = true;
        } else {
            this.f1044a.a("checker", "show ignored", 0);
            boolean unused2 = this.f1044a.g = false;
        }
        this.f1044a.i();
    }
}
