package com.avast.android.generic.ui;

import android.support.v4.app.FragmentActivity;
import com.avast.android.generic.ui.d.d;

/* compiled from: CheckerFragment */
class k implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f1047a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ CheckerFragment f1048b;

    k(CheckerFragment checkerFragment, d dVar) {
        this.f1048b = checkerFragment;
        this.f1047a = dVar;
    }

    public void run() {
        FragmentActivity activity;
        if (this.f1048b.isAdded() && (activity = this.f1048b.getActivity()) != null) {
            this.f1047a.b(activity, this.f1048b);
            if (this.f1048b.isAdded()) {
                activity.runOnUiThread(new l(this));
            }
        }
    }
}
