package com.avast.android.generic.g.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.util.z;

/* compiled from: SmsSender */
class f extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f689a;

    public void onReceive(Context context, Intent intent) {
        z.a("AvastComms", this.f689a.h, "SMS sender got SMS sending part result of " + getResultCode());
    }
}
