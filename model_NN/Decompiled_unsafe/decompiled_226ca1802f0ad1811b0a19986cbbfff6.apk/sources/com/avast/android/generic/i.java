package com.avast.android.generic;

import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import com.avast.android.generic.a.a;
import com.avast.android.generic.ui.PasswordDialog;
import com.avast.android.generic.util.y;
import java.lang.ref.WeakReference;

/* compiled from: PasswordProtector */
public class i {

    /* renamed from: a  reason: collision with root package name */
    public static Handler.Callback f730a = new j();

    /* renamed from: b  reason: collision with root package name */
    public static WeakReference<FragmentActivity> f731b;

    public static void a(FragmentActivity fragmentActivity) {
        a(fragmentActivity, false);
    }

    private static void a(FragmentActivity fragmentActivity, boolean z) {
        a aVar = (a) aa.a(fragmentActivity, a.class);
        if (aVar == null || !aVar.a()) {
            ab abVar = (ab) aa.a(fragmentActivity.getApplicationContext(), ab.class);
            if (z || (abVar != null && abVar.n())) {
                f731b = new WeakReference<>(fragmentActivity);
                ((y) aa.a(fragmentActivity, y.class)).a(r.message_passwordProtector, f730a);
                PasswordDialog.a(fragmentActivity.getSupportFragmentManager(), r.message_passwordProtector);
            }
        }
    }
}
