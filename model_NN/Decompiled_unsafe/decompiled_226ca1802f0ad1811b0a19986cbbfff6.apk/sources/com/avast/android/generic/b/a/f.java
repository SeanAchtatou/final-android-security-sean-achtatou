package com.avast.android.generic.b.a;

import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.avast.e.a.az;
import com.avast.e.a.ba;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: BadNewsProto */
public final class f extends GeneratedMessageLite.Builder<c, f> implements g {

    /* renamed from: a  reason: collision with root package name */
    private int f605a;

    /* renamed from: b  reason: collision with root package name */
    private d f606b = d.ERROR;

    /* renamed from: c  reason: collision with root package name */
    private az f607c = az.a();
    private ByteString d = ByteString.EMPTY;
    private List<k> e = Collections.emptyList();
    private int f;
    private ByteString g = ByteString.EMPTY;
    private List<ByteString> h = Collections.emptyList();
    private long i;

    private f() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static f i() {
        return new f();
    }

    /* renamed from: a */
    public f clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public c d() {
        int i2 = 1;
        c cVar = new c(this);
        int i3 = this.f605a;
        if ((i3 & 1) != 1) {
            i2 = 0;
        }
        d unused = cVar.f601c = this.f606b;
        if ((i3 & 2) == 2) {
            i2 |= 2;
        }
        az unused2 = cVar.d = this.f607c;
        if ((i3 & 4) == 4) {
            i2 |= 4;
        }
        ByteString unused3 = cVar.e = this.d;
        if ((this.f605a & 8) == 8) {
            this.e = Collections.unmodifiableList(this.e);
            this.f605a &= -9;
        }
        List unused4 = cVar.f = this.e;
        if ((i3 & 16) == 16) {
            i2 |= 8;
        }
        int unused5 = cVar.g = this.f;
        if ((i3 & 32) == 32) {
            i2 |= 16;
        }
        ByteString unused6 = cVar.h = this.g;
        if ((this.f605a & 64) == 64) {
            this.h = Collections.unmodifiableList(this.h);
            this.f605a &= -65;
        }
        List unused7 = cVar.i = this.h;
        if ((i3 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i2 |= 32;
        }
        long unused8 = cVar.j = this.i;
        int unused9 = cVar.f600b = i2;
        return cVar;
    }

    /* renamed from: a */
    public f mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                b(cVar.e());
            }
            if (cVar.f()) {
                a(cVar.g());
            }
            if (!cVar.f.isEmpty()) {
                if (this.e.isEmpty()) {
                    this.e = cVar.f;
                    this.f605a &= -9;
                } else {
                    j();
                    this.e.addAll(cVar.f);
                }
            }
            if (cVar.h()) {
                a(cVar.i());
            }
            if (cVar.j()) {
                b(cVar.k());
            }
            if (!cVar.i.isEmpty()) {
                if (this.h.isEmpty()) {
                    this.h = cVar.i;
                    this.f605a &= -65;
                } else {
                    k();
                    this.h.addAll(cVar.i);
                }
            }
            if (cVar.m()) {
                a(cVar.n());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public f mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    d a2 = d.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f605a |= 1;
                        this.f606b = a2;
                        break;
                    }
                case 18:
                    ba o = az.o();
                    if (e()) {
                        o.mergeFrom(f());
                    }
                    codedInputStream.readMessage(o, extensionRegistryLite);
                    a(o.d());
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f605a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    l f2 = k.f();
                    codedInputStream.readMessage(f2, extensionRegistryLite);
                    a(f2.d());
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f605a |= 16;
                    this.f = codedInputStream.readInt32();
                    break;
                case 50:
                    this.f605a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    k();
                    this.h.add(codedInputStream.readBytes());
                    break;
                case R.styleable.SherlockTheme_activityChooserViewStyle:
                    this.f605a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readInt64();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public f a(d dVar) {
        if (dVar == null) {
            throw new NullPointerException();
        }
        this.f605a |= 1;
        this.f606b = dVar;
        return this;
    }

    public boolean e() {
        return (this.f605a & 2) == 2;
    }

    public az f() {
        return this.f607c;
    }

    public f a(az azVar) {
        if (azVar == null) {
            throw new NullPointerException();
        }
        this.f607c = azVar;
        this.f605a |= 2;
        return this;
    }

    public f b(az azVar) {
        if ((this.f605a & 2) != 2 || this.f607c == az.a()) {
            this.f607c = azVar;
        } else {
            this.f607c = az.a(this.f607c).mergeFrom(azVar).d();
        }
        this.f605a |= 2;
        return this;
    }

    public f a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f605a |= 4;
        this.d = byteString;
        return this;
    }

    private void j() {
        if ((this.f605a & 8) != 8) {
            this.e = new ArrayList(this.e);
            this.f605a |= 8;
        }
    }

    public f a(k kVar) {
        if (kVar == null) {
            throw new NullPointerException();
        }
        j();
        this.e.add(kVar);
        return this;
    }

    public f a(l lVar) {
        j();
        this.e.add(lVar.build());
        return this;
    }

    public f a(int i2) {
        this.f605a |= 16;
        this.f = i2;
        return this;
    }

    public f b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f605a |= 32;
        this.g = byteString;
        return this;
    }

    private void k() {
        if ((this.f605a & 64) != 64) {
            this.h = new ArrayList(this.h);
            this.f605a |= 64;
        }
    }

    public f a(long j) {
        this.f605a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = j;
        return this;
    }
}
