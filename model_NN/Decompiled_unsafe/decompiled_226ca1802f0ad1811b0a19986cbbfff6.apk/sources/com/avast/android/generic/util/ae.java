package com.avast.android.generic.util;

import android.content.Context;
import android.content.Intent;

/* compiled from: ServiceStarter */
public class ae {
    public static void a(Context context, Intent intent) {
        try {
            a(intent);
            context.startService(intent);
        } catch (Exception e) {
            z.a("AvastGeneric", "Unable to start service", e);
        }
    }

    public static void a(Intent intent) {
        if (intent != null) {
            intent.addFlags(32);
        }
    }

    public static void a(Context context, Intent intent, String str) {
        if (str != null) {
            b(context, intent, str, ".service.CentralService");
            a(context, intent);
        }
    }

    public static void a(Context context, Intent intent, String str, String str2) {
        if (str != null && str2 != null) {
            c(context, intent, str, str2);
            a(context, intent);
        }
    }

    public static void b(Context context, Intent intent, String str, String str2) {
        String str3;
        if (str != null) {
            if (str.equals("com.avast.android.at_play")) {
                str3 = "com.avast.android.antitheft";
            } else {
                str3 = str;
            }
            intent.setClassName(str, str3 + str2);
        }
    }

    public static void c(Context context, Intent intent, String str, String str2) {
        if (str != null) {
            intent.setClassName(str, str + str2);
        }
    }
}
