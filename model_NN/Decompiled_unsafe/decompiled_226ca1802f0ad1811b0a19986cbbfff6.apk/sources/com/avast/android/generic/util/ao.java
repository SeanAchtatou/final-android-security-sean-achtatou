package com.avast.android.generic.util;

import android.os.Process;

/* compiled from: UserTask */
class ao extends au<Params, Result> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ am f1185a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    ao(am amVar) {
        super(null);
        this.f1185a = amVar;
    }

    public Result call() {
        Process.setThreadPriority(10);
        return this.f1185a.a(this.f1193b);
    }
}
