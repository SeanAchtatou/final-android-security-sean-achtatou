package com.avast.android.generic.app.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import com.actionbarsherlock.view.Menu;
import com.avast.android.generic.aa;
import com.avast.android.generic.q;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.CustomNumberDialog;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;
import java.util.ArrayList;
import java.util.HashMap;

public class SetRecoveryNumberDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private Handler.Callback f495a = new f(this);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ListView f496b;

    /* renamed from: c  reason: collision with root package name */
    private Intent f497c;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f497c = new Intent("android.intent.action.GET_CONTENT");
        this.f497c.setType("vnd.android.cursor.item/phone_v2");
    }

    public Dialog onCreateDialog(Bundle bundle) {
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        builder.setTitle(getString(x.pref_password_recovery_number));
        ArrayList arrayList = new ArrayList();
        boolean z = getActivity().getPackageManager().queryIntentActivities(this.f497c, Menu.CATEGORY_CONTAINER).size() > 0;
        if (z) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", getString(x.l_filter_add_from_contactlist));
            hashMap.put("image", Integer.valueOf(q.ic_menu_contact_list));
            arrayList.add(hashMap);
        }
        HashMap hashMap2 = new HashMap();
        hashMap2.put("name", getString(x.l_filter_add_custom));
        hashMap2.put("image", Integer.valueOf(q.ic_menu_contact_custom));
        arrayList.add(hashMap2);
        this.f496b = new ListView(c2);
        this.f496b.setAdapter((ListAdapter) new SimpleAdapter(c2, arrayList, t.list_item_filter_contacts_context, new String[]{"name", "image"}, new int[]{r.name, r.image}));
        this.f496b.setOnItemClickListener(new h(this, z));
        builder.setView(this.f496b);
        if (!getArguments().getBoolean("after_pin_setup", false)) {
            builder.setNeutralButton(x.F, new i(this));
            builder.setNegativeButton(x.l_password_recovery_clear, new j(this));
        }
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        return create;
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ((y) aa.a(getActivity(), y.class)).a(r.message_filter_custom_number_entered, this.f495a);
    }

    public void onDestroy() {
        super.onDestroy();
        ((y) aa.a(getActivity(), y.class)).b(r.message_filter_custom_number_entered, this.f495a);
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        switch (i) {
            case 0:
                startActivityForResult(this.f497c, 0);
                return;
            case 1:
                a();
                return;
            default:
                return;
        }
    }

    private void a() {
        CustomNumberDialog customNumberDialog = new CustomNumberDialog();
        Bundle bundle = new Bundle();
        bundle.putBoolean("disable_wildcards", true);
        customNumberDialog.setArguments(bundle);
        customNumberDialog.show(getFragmentManager(), "dialog_custom_number");
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        ((BaseActivity) getActivity()).b(true);
        if (i2 == -1 && intent != null && i == 0) {
            a(intent);
            this.f496b.post(new k(this));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Intent r8) {
        /*
            r7 = this;
            r6 = 0
            android.support.v4.app.FragmentActivity r0 = r7.getActivity()
            android.content.ContentResolver r0 = r0.getContentResolver()
            android.net.Uri r1 = r8.getData()     // Catch:{ Exception -> 0x0042, all -> 0x0068 }
            r2 = 1
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0042, all -> 0x0068 }
            r3 = 0
            java.lang.String r4 = "data1"
            r2[r3] = r4     // Catch:{ Exception -> 0x0042, all -> 0x0068 }
            r3 = 0
            r4 = 0
            r5 = 0
            android.database.Cursor r1 = r0.query(r1, r2, r3, r4, r5)     // Catch:{ Exception -> 0x0042, all -> 0x0068 }
            if (r1 == 0) goto L_0x0037
            boolean r0 = r1.moveToFirst()     // Catch:{ Exception -> 0x0072 }
            if (r0 == 0) goto L_0x0037
            java.lang.String r0 = "data1"
            int r0 = r1.getColumnIndex(r0)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r0 = r1.getString(r0)     // Catch:{ Exception -> 0x0072 }
            r7.a(r0)     // Catch:{ Exception -> 0x0072 }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return
        L_0x0037:
            if (r1 == 0) goto L_0x003c
            r1.close()
        L_0x003c:
            java.lang.String r0 = ""
            r7.a(r0)
            goto L_0x0036
        L_0x0042:
            r0 = move-exception
            r1 = r6
        L_0x0044:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0070 }
            r2.<init>()     // Catch:{ all -> 0x0070 }
            java.lang.Class r3 = r7.getClass()     // Catch:{ all -> 0x0070 }
            java.lang.String r3 = r3.getSimpleName()     // Catch:{ all -> 0x0070 }
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0070 }
            java.lang.String r3 = ": Can't pick contact phone number."
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x0070 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0070 }
            com.avast.android.generic.util.t.b(r2, r0)     // Catch:{ all -> 0x0070 }
            if (r1 == 0) goto L_0x003c
            r1.close()
            goto L_0x003c
        L_0x0068:
            r0 = move-exception
            r1 = r6
        L_0x006a:
            if (r1 == 0) goto L_0x006f
            r1.close()
        L_0x006f:
            throw r0
        L_0x0070:
            r0 = move-exception
            goto L_0x006a
        L_0x0072:
            r0 = move-exception
            goto L_0x0044
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.settings.SetRecoveryNumberDialog.a(android.content.Intent):void");
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        Intent intent = new Intent("com.avast.android.generic.app.settings.ACTION_NUMBER_ENTERED");
        intent.putExtra("phone_number", str);
        android.support.v4.content.r.a(getActivity()).a(intent);
    }

    /* access modifiers changed from: private */
    public void b() {
        android.support.v4.content.r.a(getActivity()).a(new Intent("com.avast.android.generic.app.settings.CLOSE_RECOVERY_DESCRIPTION"));
    }
}
