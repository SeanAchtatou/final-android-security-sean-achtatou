package com.avast.android.generic.internet;

/* compiled from: HttpSender */
class g implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HttpSender f820a;

    g(HttpSender httpSender) {
        this.f820a = httpSender;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, boolean):boolean
     arg types: [com.avast.android.generic.internet.HttpSender, int]
     candidates:
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, java.lang.Thread):java.lang.Thread
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, com.avast.android.generic.internet.h):void
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, boolean):boolean */
    public void run() {
        synchronized (this.f820a.h) {
            if (this.f820a.e != null && this.f820a.e.isAlive()) {
                try {
                    boolean unused = this.f820a.f = true;
                    this.f820a.e.interrupt();
                    this.f820a.e.join();
                } catch (Exception e) {
                }
            }
        }
    }
}
