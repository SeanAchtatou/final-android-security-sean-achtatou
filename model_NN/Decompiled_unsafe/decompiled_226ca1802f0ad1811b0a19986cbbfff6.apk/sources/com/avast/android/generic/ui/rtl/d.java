package com.avast.android.generic.ui.rtl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import org.xmlpull.v1.XmlPullParser;

/* compiled from: RtlLayoutInflater */
public class d extends LayoutInflater {

    /* renamed from: a  reason: collision with root package name */
    private LayoutInflater f1066a;

    /* renamed from: b  reason: collision with root package name */
    private c f1067b;

    public d(LayoutInflater layoutInflater) {
        super(layoutInflater.getContext());
        this.f1066a = layoutInflater;
        this.f1067b = new c(layoutInflater.getContext());
    }

    public Context getContext() {
        return this.f1066a.getContext();
    }

    public LayoutInflater.Filter getFilter() {
        return this.f1066a.getFilter();
    }

    public View inflate(XmlPullParser xmlPullParser, ViewGroup viewGroup) {
        return this.f1067b.a(this.f1066a.inflate(xmlPullParser, viewGroup));
    }

    public View inflate(XmlPullParser xmlPullParser, ViewGroup viewGroup, boolean z) {
        return this.f1067b.a(this.f1066a.inflate(xmlPullParser, viewGroup, z));
    }

    public View inflate(int i, ViewGroup viewGroup) {
        return this.f1067b.a(this.f1066a.inflate(i, viewGroup));
    }

    public View inflate(int i, ViewGroup viewGroup, boolean z) {
        return this.f1067b.a(this.f1066a.inflate(i, viewGroup, z));
    }

    public void setFactory(LayoutInflater.Factory factory) {
        this.f1066a.setFactory(factory);
    }

    public void setFactory2(LayoutInflater.Factory2 factory2) {
        this.f1066a.setFactory2(factory2);
    }

    public void setFilter(LayoutInflater.Filter filter) {
        this.f1066a.setFilter(filter);
    }

    public LayoutInflater cloneInContext(Context context) {
        return new d(this.f1066a.cloneInContext(context));
    }
}
