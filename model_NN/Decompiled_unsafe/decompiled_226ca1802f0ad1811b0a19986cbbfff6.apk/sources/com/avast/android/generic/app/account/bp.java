package com.avast.android.generic.app.account;

import android.content.Intent;

/* compiled from: UserAuthenticator */
public interface bp<Credentials, ErrorReason> {
    void a();

    void a(int i, int i2, Intent intent);

    void a(Object obj);

    void b();

    void b(ErrorReason errorreason);

    boolean b(int i, int i2, Intent intent);

    void c();
}
