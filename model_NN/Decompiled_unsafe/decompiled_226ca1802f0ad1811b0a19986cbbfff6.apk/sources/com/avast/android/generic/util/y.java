package com.avast.android.generic.util;

import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

/* compiled from: GlobalHandlerService */
public class y implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Handler f1260a;

    /* renamed from: b  reason: collision with root package name */
    private Map<Integer, List<k<Handler.Callback>>> f1261b;

    /* renamed from: c  reason: collision with root package name */
    private List<k<Handler.Callback>> f1262c;
    private Semaphore d;

    public final boolean a(Message message) {
        return this.f1260a.sendMessage(message);
    }

    public final boolean a(int i) {
        return this.f1260a.sendEmptyMessage(i);
    }

    public void a(int i, Handler.Callback callback) {
        List list;
        synchronized (this) {
            list = this.f1261b.get(Integer.valueOf(i));
            if (list == null) {
                list = new ArrayList();
                this.f1261b.put(Integer.valueOf(i), list);
            }
        }
        k kVar = new k(callback);
        a();
        if (!list.contains(kVar)) {
            list.add(kVar);
        }
        c();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        r0.remove(r1);
        c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        r2 = r0.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0035, code lost:
        if (r2.hasNext() == false) goto L_0x0010;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0037, code lost:
        r0 = (com.avast.android.generic.util.k) r2.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0041, code lost:
        if (r0.equals(r1) == false) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0043, code lost:
        r0.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0012, code lost:
        r1 = new com.avast.android.generic.util.k(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
        if (r0.contains(r1) == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (b() == false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(int r5, android.os.Handler.Callback r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.Map<java.lang.Integer, java.util.List<com.avast.android.generic.util.k<android.os.Handler$Callback>>> r0 = r4.f1261b     // Catch:{ all -> 0x002a }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x002a }
            java.lang.Object r0 = r0.get(r1)     // Catch:{ all -> 0x002a }
            java.util.List r0 = (java.util.List) r0     // Catch:{ all -> 0x002a }
            if (r0 != 0) goto L_0x0011
            monitor-exit(r4)     // Catch:{ all -> 0x002a }
        L_0x0010:
            return
        L_0x0011:
            monitor-exit(r4)     // Catch:{ all -> 0x002a }
            com.avast.android.generic.util.k r1 = new com.avast.android.generic.util.k
            r1.<init>(r6)
            boolean r2 = r0.contains(r1)
            if (r2 == 0) goto L_0x0010
            boolean r2 = r4.b()
            if (r2 == 0) goto L_0x002d
            r0.remove(r1)
            r4.c()
            goto L_0x0010
        L_0x002a:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x002a }
            throw r0
        L_0x002d:
            java.util.Iterator r2 = r0.iterator()
        L_0x0031:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0010
            java.lang.Object r0 = r2.next()
            com.avast.android.generic.util.k r0 = (com.avast.android.generic.util.k) r0
            boolean r3 = r0.equals(r1)
            if (r3 == 0) goto L_0x0031
            r0.clear()
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.util.y.b(int, android.os.Handler$Callback):void");
    }

    public boolean handleMessage(Message message) {
        List list;
        synchronized (this) {
            list = this.f1261b.get(Integer.valueOf(message.what));
        }
        if (list != null) {
            a(list, message);
            if (list.size() == 0) {
                this.f1261b.remove(Integer.valueOf(message.what));
            }
        }
        a(this.f1262c, message);
        return true;
    }

    private void a(List<k<Handler.Callback>> list, Message message) {
        a();
        ArrayList<k> arrayList = new ArrayList<>();
        for (k next : list) {
            if (next.get() != null) {
                arrayList.add(next);
            }
        }
        c();
        for (k kVar : arrayList) {
            ((Handler.Callback) kVar.get()).handleMessage(message);
        }
        a();
        Iterator<k<Handler.Callback>> it = list.iterator();
        while (it.hasNext()) {
            if (it.next().get() == null) {
                it.remove();
            }
        }
        c();
    }

    private void a() {
        this.d.acquireUninterruptibly();
    }

    private boolean b() {
        return this.d.tryAcquire();
    }

    private void c() {
        this.d.release();
    }
}
