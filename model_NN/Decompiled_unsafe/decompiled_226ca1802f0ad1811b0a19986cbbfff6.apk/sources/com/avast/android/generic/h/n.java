package com.avast.android.generic.h;

import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: FeedbackProto */
public final class n extends GeneratedMessageLite implements p {

    /* renamed from: a  reason: collision with root package name */
    private static final n f715a = new n(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f716b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ByteString f717c;
    /* access modifiers changed from: private */
    public q d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Object f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public c h;
    /* access modifiers changed from: private */
    public ByteString i;
    /* access modifiers changed from: private */
    public ByteString j;
    private byte k;
    private int l;

    private n(o oVar) {
        super(oVar);
        this.k = -1;
        this.l = -1;
    }

    private n(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static n a() {
        return f715a;
    }

    public boolean b() {
        return (this.f716b & 1) == 1;
    }

    public ByteString c() {
        return this.f717c;
    }

    public boolean d() {
        return (this.f716b & 2) == 2;
    }

    public q e() {
        return this.d;
    }

    public boolean f() {
        return (this.f716b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString u() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f716b & 8) == 8;
    }

    public String i() {
        Object obj = this.f;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString v() {
        Object obj = this.f;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean j() {
        return (this.f716b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString w() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f716b & 32) == 32;
    }

    public c m() {
        return this.h;
    }

    public boolean n() {
        return (this.f716b & 64) == 64;
    }

    public ByteString o() {
        return this.i;
    }

    public boolean p() {
        return (this.f716b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public ByteString q() {
        return this.j;
    }

    private void x() {
        this.f717c = ByteString.EMPTY;
        this.d = q.CUSTOM_FEEDBACK;
        this.e = "";
        this.f = "";
        this.g = "";
        this.h = c.a();
        this.i = ByteString.EMPTY;
        this.j = ByteString.EMPTY;
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 == -1) {
            this.k = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f716b & 1) == 1) {
            codedOutputStream.writeBytes(1, this.f717c);
        }
        if ((this.f716b & 2) == 2) {
            codedOutputStream.writeEnum(2, this.d.getNumber());
        }
        if ((this.f716b & 4) == 4) {
            codedOutputStream.writeBytes(3, u());
        }
        if ((this.f716b & 8) == 8) {
            codedOutputStream.writeBytes(4, v());
        }
        if ((this.f716b & 16) == 16) {
            codedOutputStream.writeBytes(5, w());
        }
        if ((this.f716b & 32) == 32) {
            codedOutputStream.writeMessage(6, this.h);
        }
        if ((this.f716b & 64) == 64) {
            codedOutputStream.writeBytes(7, this.i);
        }
        if ((this.f716b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(8, this.j);
        }
    }

    public int getSerializedSize() {
        int i2 = this.l;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f716b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, this.f717c);
            }
            if ((this.f716b & 2) == 2) {
                i2 += CodedOutputStream.computeEnumSize(2, this.d.getNumber());
            }
            if ((this.f716b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, u());
            }
            if ((this.f716b & 8) == 8) {
                i2 += CodedOutputStream.computeBytesSize(4, v());
            }
            if ((this.f716b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(5, w());
            }
            if ((this.f716b & 32) == 32) {
                i2 += CodedOutputStream.computeMessageSize(6, this.h);
            }
            if ((this.f716b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(7, this.i);
            }
            if ((this.f716b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(8, this.j);
            }
            this.l = i2;
        }
        return i2;
    }

    public static o r() {
        return o.i();
    }

    /* renamed from: s */
    public o newBuilderForType() {
        return r();
    }

    public static o a(n nVar) {
        return r().mergeFrom(nVar);
    }

    /* renamed from: t */
    public o toBuilder() {
        return a(this);
    }

    static {
        f715a.x();
    }
}
