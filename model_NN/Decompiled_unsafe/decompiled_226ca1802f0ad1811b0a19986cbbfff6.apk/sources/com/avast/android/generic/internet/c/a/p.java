package com.avast.android.generic.internet.c.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class p extends GeneratedMessageLite.Builder<o, p> implements s {

    /* renamed from: a  reason: collision with root package name */
    private int f778a;

    /* renamed from: b  reason: collision with root package name */
    private q f779b = q.OK;

    private p() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static p g() {
        return new p();
    }

    /* renamed from: a */
    public p clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public o getDefaultInstanceForType() {
        return o.a();
    }

    /* renamed from: c */
    public o build() {
        o d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    /* access modifiers changed from: private */
    public o h() {
        o d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d).asInvalidProtocolBufferException();
    }

    public o d() {
        int i = 1;
        o oVar = new o(this);
        if ((this.f778a & 1) != 1) {
            i = 0;
        }
        q unused = oVar.f777c = this.f779b;
        int unused2 = oVar.f776b = i;
        return oVar;
    }

    /* renamed from: a */
    public p mergeFrom(o oVar) {
        if (oVar != o.a() && oVar.b()) {
            a(oVar.c());
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public p mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 8:
                    q a2 = q.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f778a |= 1;
                        this.f779b = a2;
                        break;
                    }
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public p a(q qVar) {
        if (qVar == null) {
            throw new NullPointerException();
        }
        this.f778a |= 1;
        this.f779b = qVar;
        return this;
    }
}
