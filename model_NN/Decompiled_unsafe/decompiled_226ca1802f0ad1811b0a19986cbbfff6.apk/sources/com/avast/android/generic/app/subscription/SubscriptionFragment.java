package com.avast.android.generic.app.subscription;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.r;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.avast.android.generic.Application;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.licensing.LicensingBroadcastReceiver;
import com.avast.android.generic.licensing.PremiumHelper;
import com.avast.android.generic.licensing.PurchaseConfirmationService;
import com.avast.android.generic.licensing.b;
import com.avast.android.generic.licensing.c;
import com.avast.android.generic.licensing.c.g;
import com.avast.android.generic.licensing.c.i;
import com.avast.android.generic.licensing.c.k;
import com.avast.android.generic.licensing.c.l;
import com.avast.android.generic.n;
import com.avast.android.generic.o;
import com.avast.android.generic.p;
import com.avast.android.generic.t;
import com.avast.android.generic.u;
import com.avast.android.generic.ui.VoucherDialog;
import com.avast.android.generic.ui.b.a;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.ax;
import com.avast.android.generic.util.d;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.util.s;
import com.avast.android.generic.util.y;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;
import com.avast.c.a.a.ai;
import com.avast.c.a.a.al;
import com.avast.c.a.a.ap;
import com.avast.c.a.a.aq;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Semaphore;

public abstract class SubscriptionFragment extends TrackedMultiToolFragment implements Handler.Callback {
    /* access modifiers changed from: private */
    public ai A;
    private int B;
    private int C;
    private LinearLayout D;
    private Button E;
    private ProgressDialog F;
    /* access modifiers changed from: private */
    public List<ai> G;
    /* access modifiers changed from: private */
    public Semaphore H = new Semaphore(0);
    /* access modifiers changed from: private */
    public b I = new b(this.H);
    /* access modifiers changed from: private */
    public LicensingBroadcastReceiver J = null;
    private String K = null;
    /* access modifiers changed from: private */
    public d L;
    /* access modifiers changed from: private */
    public boolean M;

    /* renamed from: a  reason: collision with root package name */
    private View.OnClickListener f514a;

    /* renamed from: b  reason: collision with root package name */
    private g f515b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ab f516c;
    private r d;
    /* access modifiers changed from: private */
    public com.avast.android.generic.licensing.c.b e;
    private ViewGroup f;
    /* access modifiers changed from: private */
    public ViewGroup g;
    /* access modifiers changed from: private */
    public ViewGroup h;
    private View i;
    private ViewGroup j;
    private TextView k;
    private ViewGroup l;
    private TextView m;
    private TextView n;
    private TextView o;
    private Button p;
    private Button q;
    /* access modifiers changed from: private */
    public Activity r;
    private BroadcastReceiver s;
    /* access modifiers changed from: private */
    public c t;
    private View u;
    /* access modifiers changed from: private */
    public MenuItem v;
    /* access modifiers changed from: private */
    public ad w;
    /* access modifiers changed from: private */
    public String x;
    /* access modifiers changed from: private */
    public k y;
    private Button z;

    /* access modifiers changed from: protected */
    public abstract Uri e();

    public abstract void f();

    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        this.r = getActivity();
        this.L = d.b(this.r);
        this.f516c = (ab) aa.a(getActivity(), ab.class);
        if (!(getActivity() == null || (intent = getActivity().getIntent()) == null || !intent.hasExtra("customTag"))) {
            this.K = intent.getStringExtra("customTag");
            if (TextUtils.isEmpty(this.K)) {
                this.K = null;
            }
        }
        this.d = r.a(getActivity());
        this.e = new com.avast.android.generic.licensing.c.b(getActivity());
        this.s = new f(this);
        this.f515b = new q(this);
        this.f514a = new u(this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_subscription_suite, viewGroup, false);
        setHasOptionsMenu(true);
        return inflate;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.J = new LicensingBroadcastReceiver(getActivity(), this, this.I, true, e());
        this.i = view.findViewById(com.avast.android.generic.r.subscription_manage);
        this.C = getResources().getColor(o.text_warning);
        this.B = getResources().getColor(o.h);
        String a2 = ax.a(ab.b(getActivity()));
        LinearLayout linearLayout = (LinearLayout) view.findViewById(com.avast.android.generic.r.purchase_web_container);
        if (a2 != null) {
            linearLayout.setVisibility(0);
            this.o = (TextView) view.findViewById(com.avast.android.generic.r.purchase_web);
            this.o.setOnClickListener(new v(this, a2));
        } else {
            linearLayout.setVisibility(8);
        }
        this.u = view.findViewById(com.avast.android.generic.r.subscription_progress);
        this.n = (TextView) view.findViewById(com.avast.android.generic.r.error_view);
        this.q = (Button) view.findViewById(com.avast.android.generic.r.subscription_button_retry);
        this.q.setOnClickListener(new w(this));
        this.z = (Button) view.findViewById(com.avast.android.generic.r.button_refresh_licenses);
        this.z.setOnClickListener(new x(this));
        c(view);
        this.f = (ViewGroup) view.findViewById(com.avast.android.generic.r.subscription_group_invalid);
        a(this.f);
        this.g = (ViewGroup) this.f.findViewById(com.avast.android.generic.r.subscription_offers);
        this.h = (ViewGroup) this.f.findViewById(com.avast.android.generic.r.subscription_offers_loading);
        this.j = (ViewGroup) view.findViewById(com.avast.android.generic.r.subscription_group_processing);
        this.k = (TextView) view.findViewById(com.avast.android.generic.r.subscription_loading_text);
        this.l = (ViewGroup) view.findViewById(com.avast.android.generic.r.subscription_group_valid);
        this.m = (TextView) view.findViewById(com.avast.android.generic.r.subscription_valid_until);
        this.p = (Button) view.findViewById(com.avast.android.generic.r.subscription_button_manage);
        this.D = (LinearLayout) view.findViewById(com.avast.android.generic.r.debug);
        if (!Application.f317a || !Application.f318b || !s.a(getActivity())) {
            this.D.setVisibility(8);
        } else {
            this.E = (Button) view.findViewById(com.avast.android.generic.r.r_subscriptionTest);
            this.E.setOnClickListener(new z(this));
        }
        a(false);
        this.h.setVisibility(0);
        this.g.setVisibility(8);
        h();
        d();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(ViewGroup viewGroup) {
        TypedArray obtainStyledAttributes = getActivity().getTheme().obtainStyledAttributes(new int[]{n.subscriptionDescLayout});
        int resourceId = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        if (resourceId != 0) {
            viewGroup.addView(layoutInflater.inflate(resourceId, viewGroup, false), 0);
        }
    }

    private void c(View view) {
        View findViewById = view.findViewById(com.avast.android.generic.r.subscription_disclaimer);
        CharSequence text = getText(x.l_subscription_disclaimer_show);
        CharSequence text2 = getText(x.l_subscription_disclaimer_hide);
        TextView textView = (TextView) view.findViewById(com.avast.android.generic.r.subscription_disclaimer_show);
        textView.setOnClickListener(new aa(this, new a(findViewById), findViewById, textView, text2, (ScrollView) view.findViewById(com.avast.android.generic.r.subscription_scrollview), text));
    }

    private void a(boolean z2) {
        for (int i2 = 0; i2 < this.g.getChildCount(); i2++) {
            View childAt = this.g.getChildAt(i2);
            if (childAt != this.q) {
                childAt.setEnabled(z2);
            }
        }
    }

    public void onStart() {
        super.onStart();
        this.d.a(this.s, new IntentFilter("com.avast.android.generic.ui.licensing.ErrorDialog.DISMISSED"));
    }

    public void onResume() {
        super.onResume();
        if (isAdded()) {
            d();
        }
    }

    public void onPause() {
        super.onPause();
    }

    public void onStop() {
        super.onStop();
        try {
            this.d.a(this.s);
        } catch (Exception e2) {
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.t != null && this.t.getStatus().equals(AsyncTask.Status.RUNNING)) {
            this.t.cancel(true);
        }
        n();
        r();
        this.e.a();
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 255) {
            o();
        } else if (i2 != 6655) {
            this.e.a(i2, i3, intent);
        } else if (intent != null) {
            String stringExtra = intent.getStringExtra("resultDescription");
            if (stringExtra != null) {
                Toast.makeText(getActivity(), stringExtra, 1).show();
            }
        } else {
            String str = null;
            switch (i3) {
                case 1:
                    str = getString(x.l_error_licensing_error_short);
                    break;
                case 2:
                    PurchaseConfirmationService.b(getActivity());
                    f();
                    this.r.setResult(99);
                    this.r.finish();
                    break;
                case 3:
                    str = getString(x.l_error_wp_missing_parameters);
                    break;
                case 4:
                    str = getString(x.l_error_licensing_error_short);
                    break;
                case 5:
                    str = getString(x.l_error_wp_result_is_empty);
                    break;
                case 6:
                    z.a("AvastGenericLic", "Web purchase canceled on behalf of user");
                    Toast.makeText(this.r, x.msg_subscription_error_purchase_cancelled_web_purchase, 1).show();
                    break;
                case 7:
                    str = getString(x.l_error_wp_customer_cannot_pay);
                    break;
                case 8:
                    str = getString(x.l_error_wp_invalid_data);
                    break;
                case 9:
                    str = getString(x.l_error_wp_pin_invalid);
                    break;
                case 10:
                    str = getString(x.l_error_wp_payment_limit_reached);
                    break;
                case 11:
                    str = getString(x.l_error_wp_psp_communication_error);
                    break;
                case 12:
                    str = getString(x.l_error_wp_psp_general_error);
                    break;
                case 13:
                    str = getString(x.l_error_wp_psp_authorization_failed);
                    break;
                case 15:
                    str = getString(x.l_error_wp_psp_sms_failed);
                    break;
            }
            if (str != null) {
                a(str);
            }
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(u.menu_subscription, menu);
        this.v = menu.findItem(com.avast.android.generic.r.menu_voucher);
        this.v.setEnabled(this.w != null ? this.w.f529a : false);
        g();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != com.avast.android.generic.r.menu_voucher) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (getFragmentManager() != null) {
            this.L.b();
            VoucherDialog.a(getFragmentManager());
            y yVar = (y) aa.a(getActivity(), y.class);
            yVar.a(com.avast.android.generic.r.message_voucher_canceled, this);
            yVar.a(com.avast.android.generic.r.message_voucher_successful, this);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void g() {
        boolean z2;
        if (isAdded() && this.v != null) {
            MenuItem menuItem = this.v;
            if (!PremiumHelper.a(getActivity()) || PremiumHelper.d(getActivity())) {
                z2 = true;
            } else {
                z2 = false;
            }
            menuItem.setVisible(z2);
            if (TextUtils.isEmpty(this.x)) {
                this.v.setTitle(getString(x.l_subscription_voucher));
            } else if (this.x.length() > 8) {
                this.v.setTitle(Html.fromHtml(this.x.substring(0, 8) + "&#8230;"));
            } else {
                this.v.setTitle(this.x);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.subscription.SubscriptionFragment.a(boolean, boolean, java.lang.String):void
     arg types: [int, int, ?[OBJECT, ARRAY]]
     candidates:
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(android.content.Context, java.lang.String, com.avast.android.generic.app.subscription.ae):void
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(boolean, boolean, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.subscription.SubscriptionFragment.a(boolean, boolean, java.lang.String):void
     arg types: [int, boolean, java.lang.String]
     candidates:
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(android.content.Context, java.lang.String, com.avast.android.generic.app.subscription.ae):void
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(boolean, boolean, java.lang.String):void */
    public void d() {
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            if (PremiumHelper.a(activity)) {
                long b2 = PremiumHelper.b(activity);
                if (b2 == -2) {
                    PurchaseConfirmationService.a(activity, e(), (ab) aa.a(this.r, ab.class));
                    b2 = PremiumHelper.b(activity);
                }
                if (PremiumHelper.d(activity)) {
                    a(false, false, (String) null);
                    o();
                    return;
                }
                boolean c2 = PremiumHelper.c(activity);
                String e2 = PremiumHelper.e(activity);
                a(b2, c2);
                a(true, c2, e2);
                return;
            }
            a(false, false, (String) null);
            o();
        }
    }

    private void h() {
        this.k.setText(x.l_subscription_loading);
        this.f.setVisibility(8);
        this.l.setVisibility(8);
        this.j.setVisibility(0);
    }

    private void a(boolean z2, boolean z3, String str) {
        this.l.setVisibility(8);
        this.f.setVisibility(8);
        this.j.setVisibility(8);
        if (z2) {
            this.l.setVisibility(0);
            if (z3) {
                this.i.setVisibility(0);
                if (str != null) {
                    this.p.setEnabled(true);
                    this.p.setOnClickListener(new ac(this));
                } else {
                    this.p.setEnabled(false);
                }
            } else {
                this.i.setVisibility(8);
            }
        } else {
            this.f.setVisibility(0);
        }
        g();
    }

    private void a(long j2, boolean z2) {
        if (j2 == -1) {
            this.m.setText(getActivity().getString(x.l_lifetime_license_long));
            return;
        }
        String a2 = com.avast.android.generic.util.r.a(getActivity(), j2, 65556);
        if (z2) {
            this.m.setText(getActivity().getString(x.l_subscription_renewed_on_long, new Object[]{a2}));
            return;
        }
        this.m.setText(getActivity().getString(x.l_subscription_time_limited_long, new Object[]{a2}));
    }

    /* access modifiers changed from: private */
    public boolean a(List<k> list) {
        float f2;
        k kVar;
        TextView textView;
        boolean z2;
        Button button;
        int i2;
        TextView textView2;
        boolean z3;
        if (!isAdded()) {
            return false;
        }
        if (list == null || list.isEmpty()) {
            a(this.r, this.r.getString(x.l_offers_no_items), ae.NORMAL);
            return true;
        }
        this.g.removeAllViews();
        this.G = new LinkedList();
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        Resources resources = getResources();
        int dimensionPixelSize = resources.getDimensionPixelSize(p.content_marginVertical);
        int dimensionPixelSize2 = resources.getDimensionPixelSize(p.content_marginHorizontal);
        float f3 = 0.0f;
        Iterator<k> it = list.iterator();
        while (true) {
            f2 = f3;
            if (!it.hasNext()) {
                break;
            }
            k next = it.next();
            if (f2 == 0.0f && next.b() == com.avast.c.a.a.y.SUBSCRIPTION_MONTHLY) {
                f2 = next.i();
            }
            f3 = f2;
        }
        int size = list.size();
        for (int i3 = 0; i3 < size; i3++) {
            k kVar2 = list.get(i3);
            if (i3 + 1 < size) {
                kVar = list.get(i3 + 1);
            } else {
                kVar = null;
            }
            int i4 = 1;
            if (!TextUtils.isEmpty(kVar2.g())) {
                TextView textView3 = (TextView) layoutInflater.inflate(t.textview_offer, (ViewGroup) null);
                textView3.setText(kVar2.g());
                this.g.addView(textView3);
                if (kVar == null || !TextUtils.isEmpty(kVar.g())) {
                    textView = textView3;
                } else {
                    i4 = 2;
                    textView = textView3;
                }
            } else {
                textView = null;
            }
            TextView textView4 = null;
            if (kVar2.b() == com.avast.c.a.a.y.SUBSCRIPTION_MONTHLY) {
                Button button2 = (Button) layoutInflater.inflate(t.button_offer_green, (ViewGroup) null);
                button2.setText(getString(x.l_subscription_price_per_month, kVar2.d()));
                z2 = true;
                button = button2;
            } else if (kVar2.b() == com.avast.c.a.a.y.SUBSCRIPTION_YEARLY) {
                if (f2 <= 0.0f || kVar2.i() <= 0.0f) {
                    i2 = 0;
                } else {
                    i2 = (int) Math.floor((double) (100.0f - ((kVar2.i() / (12.0f * f2)) * 100.0f)));
                }
                Button button3 = (Button) layoutInflater.inflate(t.button_offer_green, (ViewGroup) null);
                button3.setText(getString(x.l_subscription_price_per_year, kVar2.d()));
                if (i2 > 0 && TextUtils.isEmpty(kVar2.k())) {
                    textView4 = (TextView) layoutInflater.inflate(t.offer_discount, (ViewGroup) null);
                    textView4.setText(getString(x.l_subscription_discount, Integer.valueOf(i2), "%"));
                }
                z2 = true;
                button = button3;
            } else {
                Button button4 = (Button) layoutInflater.inflate(t.button_offer, (ViewGroup) null);
                if (kVar2.i() > 0.0f) {
                    button4.setText(kVar2.c() + " " + getString(x.l_subscription_price_one_time, kVar2.d()));
                    z2 = true;
                    button = button4;
                } else {
                    button4.setText(kVar2.c());
                    z2 = false;
                    button = button4;
                }
            }
            if (!TextUtils.isEmpty(kVar2.k())) {
                TextView textView5 = (TextView) layoutInflater.inflate(t.offer_discount, (ViewGroup) null);
                textView5.setText(kVar2.k());
                textView2 = textView5;
            } else {
                textView2 = textView4;
            }
            if (z2) {
                for (ai next2 : kVar2.j()) {
                    Iterator<ai> it2 = this.G.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (it2.next().e() == next2.e()) {
                                z3 = true;
                                break;
                            }
                        } else {
                            z3 = false;
                            break;
                        }
                    }
                    if (!z3) {
                        this.G.add(next2);
                    }
                }
            }
            button.setTag(kVar2);
            button.setOnClickListener(this.f514a);
            this.g.addView(button);
            if (textView2 != null) {
                this.g.addView(textView2);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) textView2.getLayoutParams();
                layoutParams.setMargins(dimensionPixelSize2, -15, dimensionPixelSize2, dimensionPixelSize * i4);
                textView2.setLayoutParams(layoutParams);
            }
            if (textView != null) {
                LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) textView.getLayoutParams();
                layoutParams2.setMargins(dimensionPixelSize2, 0, dimensionPixelSize2, 0);
                textView.setLayoutParams(layoutParams2);
            }
            LinearLayout.LayoutParams layoutParams3 = (LinearLayout.LayoutParams) button.getLayoutParams();
            Drawable a2 = kVar2.a(getActivity(), (int) TypedValue.applyDimension(1, 20.0f, getActivity().getResources().getDisplayMetrics()));
            if (a2 != null) {
                button.setCompoundDrawablesWithIntrinsicBounds(a2, (Drawable) null, (Drawable) null, (Drawable) null);
                button.setCompoundDrawablePadding(dimensionPixelSize2);
            }
            layoutParams3.setMargins(dimensionPixelSize2, dimensionPixelSize, dimensionPixelSize2, textView2 != null ? 0 : dimensionPixelSize * i4);
            button.setLayoutParams(layoutParams3);
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(c cVar) {
        n();
        if (isAdded()) {
            if (this.n != null) {
                this.n.setVisibility(8);
            }
            if (this.q != null) {
                this.q.setVisibility(8);
            }
            if (this.u != null) {
                this.u.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        if (this.u != null && isAdded()) {
            this.u.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str, ae aeVar) {
        if (isAdded()) {
            if (this.n != null) {
                this.n.setText(str);
                if (aeVar != ae.NORMAL) {
                    if (aeVar == ae.WARNING) {
                        this.n.setTextColor(this.C);
                    } else {
                        this.n.setTextColor(this.B);
                    }
                }
                this.n.setVisibility(0);
            }
            if (this.q != null) {
                this.q.setVisibility(0);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void o() {
        if (this.t != null && this.t.getStatus().equals(AsyncTask.Status.RUNNING)) {
            this.t.cancel(true);
        }
        this.t = new c(getActivity(), e());
        this.t.a(this.x);
        this.t.a(new g(this));
        if (this.v != null) {
            this.v.setEnabled(false);
        } else {
            if (this.w == null) {
                this.w = new ad(null);
            }
            this.w.f529a = false;
        }
        this.h.setVisibility(0);
        this.g.setVisibility(8);
        com.avast.android.generic.util.b.a(this.t, new Void[0]);
    }

    /* access modifiers changed from: private */
    public void p() {
        this.x = null;
        g();
        if (this.v != null) {
            this.v.setEnabled(true);
            return;
        }
        if (this.w == null) {
            this.w = new ad(null);
        }
        this.w.f529a = true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: private */
    @SuppressLint({"SetJavaScriptEnabled", "NewApi"})
    public void a(k kVar) {
        String c2;
        if (isAdded()) {
            if (!TextUtils.isEmpty(kVar.h())) {
                if (kVar.b() == com.avast.c.a.a.y.SUBSCRIPTION_MONTHLY) {
                    c2 = getString(x.l_subscription_buy) + " " + getString(x.l_subscription_price_per_month, kVar.d());
                } else if (kVar.b() == com.avast.c.a.a.y.SUBSCRIPTION_YEARLY) {
                    c2 = getString(x.l_subscription_buy) + " " + getString(x.l_subscription_price_per_year, kVar.d());
                } else if (kVar.i() > 0.0f) {
                    c2 = kVar.c() + " " + getString(x.l_subscription_price_one_time, kVar.d());
                } else {
                    c2 = kVar.c();
                }
                Context c3 = ak.c(getActivity());
                AlertDialog.Builder builder = new AlertDialog.Builder(c3);
                View inflate = LayoutInflater.from(c3).inflate(t.dialog_advertising, (ViewGroup) null, false);
                WebView webView = (WebView) inflate.findViewById(com.avast.android.generic.r.advertising_webView);
                ProgressBar progressBar = (ProgressBar) inflate.findViewById(com.avast.android.generic.r.advertising_progress);
                progressBar.setVisibility(0);
                webView.setVisibility(8);
                webView.setVerticalScrollBarEnabled(true);
                webView.setHorizontalScrollBarEnabled(true);
                webView.getSettings().setJavaScriptEnabled(true);
                webView.getSettings().setSaveFormData(false);
                webView.getSettings().setSavePassword(false);
                webView.getSettings().setCacheMode(2);
                builder.setTitle(c2);
                builder.setView(inflate);
                builder.setPositiveButton(kVar.i() > 0.0f ? x.l_purchase : x.l_consume, new h(this, kVar));
                builder.setNegativeButton(x.F, new i(this));
                AlertDialog create = builder.create();
                create.setInverseBackgroundForced(true);
                create.setCancelable(true);
                webView.setWebViewClient(new j(this, create, kVar, webView, progressBar));
                create.setOnShowListener(new k(this, kVar, webView));
                create.show();
                this.L.b(kVar.a(), kVar.i());
                return;
            }
            b(kVar);
        }
    }

    /* access modifiers changed from: private */
    public void b(k kVar) {
        if (isAdded()) {
            if (kVar.i() <= 0.0f) {
                this.L.a(kVar.a(), 0.0f);
                if (kVar.h() == null) {
                    com.avast.android.generic.a.a(getActivity(), getString(x.l_subscription_want_consume_free), getString(x.l_consume), getString(x.F), new m(this, kVar));
                } else {
                    g(kVar);
                }
            } else if (!this.e.d()) {
                this.e.a(new l(this, kVar));
            } else if (!this.e.b()) {
                ErrorDialog.a(getFragmentManager(), 1, x.l_subscription_error_billing_connection_title, x.l_offers_subscriptions_not_supported, 0, 0, 0);
            } else {
                c(kVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void c(k kVar) {
        boolean z2;
        if (isAdded()) {
            List<ai> j2 = kVar.j();
            if (this.G.size() > 1) {
                LinkedList linkedList = new LinkedList();
                LinkedList linkedList2 = new LinkedList();
                for (ai next : this.G) {
                    Iterator<ai> it = j2.iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (it.next().e() == next.e()) {
                                z2 = true;
                                break;
                            }
                        } else {
                            z2 = false;
                            break;
                        }
                    }
                    if (z2) {
                        linkedList.add(next.g());
                        linkedList2.add(true);
                    } else {
                        linkedList.add(getString(x.l_unsupported_purchase_method, next.g()));
                        linkedList2.add(false);
                    }
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
                builder.setTitle(getString(x.l_subscription_choose_payment_provider));
                builder.setItems((CharSequence[]) linkedList.toArray(new CharSequence[linkedList.size()]), new n(this, kVar));
                AlertDialog create = builder.create();
                create.show();
                create.getListView().setAdapter((ListAdapter) new e(getActivity(), create.getListView().getAdapter(), (Boolean[]) linkedList2.toArray(new Boolean[linkedList2.size()])));
            } else if (j2.size() >= 1) {
                this.A = j2.get(0);
                d(kVar);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d(k kVar) {
        if (isAdded()) {
            if (this.A.c() == al.MANUFACTURER_STORE) {
                if (this.A.e() == 1) {
                    e(kVar);
                } else {
                    z.a("AvastGenericLic", "Payment provider not supported");
                }
            } else if (this.A.c() == al.WEB_REDIRECT) {
                f(kVar);
            }
        }
    }

    private void e(k kVar) {
        String str;
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            String a2 = kVar.a();
            this.y = kVar;
            a("subscriptionFragment", "click", a2, 0);
            this.k.setText(x.l_subscription_processing_payment);
            this.f.setVisibility(8);
            this.j.setVisibility(0);
            this.L.a(a2, kVar.i());
            String str2 = "";
            if (kVar.f() != null) {
                str2 = kVar.f();
            }
            if (!TextUtils.isEmpty(this.K)) {
                str = str2 + "|" + this.K;
            } else {
                str = str2;
            }
            try {
                if (kVar.b() == com.avast.c.a.a.y.SUBSCRIPTION_MONTHLY || kVar.b() == com.avast.c.a.a.y.SUBSCRIPTION_YEARLY) {
                    this.e.b(this, a2, 6654, this.f515b, str);
                } else {
                    this.e.a(this, a2, 6654, this.f515b, str);
                }
            } catch (Exception e2) {
                a.a.a.a.a.a.a().a("Can not launch purchase flow", e2);
                Toast.makeText(activity, getString(x.msg_subscription_error_purchase_failed_message), 1).show();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.licensing.PurchaseConfirmationService.a(android.content.Context, java.util.Set<com.avast.android.generic.licensing.a.b>, com.avast.android.generic.ab, boolean, java.lang.String, com.avast.c.a.a.an):com.avast.c.a.a.k
     arg types: [android.app.Activity, java.util.Set<com.avast.android.generic.licensing.a.b>, com.avast.android.generic.ab, int, java.lang.String, com.avast.c.a.a.an]
     candidates:
      com.avast.android.generic.licensing.PurchaseConfirmationService.a(android.content.Context, com.avast.android.generic.ab, java.lang.String, java.lang.String, java.lang.String, com.avast.c.a.a.an):com.avast.c.a.a.aa
      com.avast.android.generic.licensing.PurchaseConfirmationService.a(android.content.Context, java.util.Set<com.avast.android.generic.licensing.a.b>, com.avast.android.generic.ab, boolean, java.lang.String, com.avast.c.a.a.an):com.avast.c.a.a.k */
    private void f(k kVar) {
        if (isAdded()) {
            try {
                com.avast.c.a.a.k a2 = PurchaseConfirmationService.a((Context) this.r, this.t.f(), this.f516c, false, this.r.getPackageName(), ab.b(this.r));
                String str = "";
                if (kVar.f() != null) {
                    str = kVar.f();
                }
                if (!TextUtils.isEmpty(this.K)) {
                    str = str + "|" + this.K;
                }
                String a3 = kVar.a();
                aq l2 = ap.l();
                l2.a(a3);
                l2.a(this.A.e());
                l2.b(str);
                l2.a(a2);
                new o(this, l2).execute(new Void[0]);
            } catch (Exception e2) {
                z.a("AvastGenericLic", "Could not get redirect url for webpurchase", e2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void g(k kVar) {
        String str;
        if (isAdded()) {
            this.y = kVar;
            a("subscriptionFragment", "click", kVar.a(), 0);
            this.k.setText(x.l_subscription_processing_free);
            this.f.setVisibility(8);
            this.j.setVisibility(0);
            String str2 = "";
            if (kVar.f() != null) {
                str2 = kVar.f();
            }
            if (!TextUtils.isEmpty(this.K)) {
                str = str2 + "|" + this.K;
            } else {
                str = str2;
            }
            this.f515b.a(new i(0, null), new l("free", UUID.randomUUID().toString(), getActivity().getPackageName(), kVar.a(), str));
            this.L.c(kVar.a(), 0.0f);
        }
    }

    /* access modifiers changed from: private */
    public void q() {
        r();
        this.F = new ProgressDialog(getActivity());
        this.F.setCancelable(false);
        this.F.setMessage(getString(x.l_subscription_processing_free));
        this.F.show();
    }

    /* access modifiers changed from: private */
    public void r() {
        try {
            if (this.F != null) {
                if (isAdded()) {
                    this.F.dismiss();
                }
                this.F = null;
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        new p(this, this.f516c.Q()).execute(new Void[0]);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(str).setCancelable(false).setPositiveButton("OK", new s(this));
        builder.create().show();
    }
}
