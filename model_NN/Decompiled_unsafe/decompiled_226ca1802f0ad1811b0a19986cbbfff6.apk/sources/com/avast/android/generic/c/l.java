package com.avast.android.generic.c;

import android.os.Handler;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.z;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentLinkedQueue;

/* compiled from: CommandReceiver */
public class l {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public AvastService f642a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public ConcurrentLinkedQueue<a> f643b;

    /* renamed from: c  reason: collision with root package name */
    private LinkedList<a> f644c;
    /* access modifiers changed from: private */
    public Thread d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public Handler f;
    /* access modifiers changed from: private */
    public boolean g;

    public boolean a() {
        boolean z = true;
        z.a("AvastGeneric", this.f642a, "Checking command receiver population state");
        synchronized (this.f643b) {
            synchronized (this.f644c) {
                if (!this.f643b.isEmpty()) {
                    z.a("AvastGeneric", this.f642a, "Checked command receiver population state (command queue is not empty)");
                } else {
                    z.a("AvastGeneric", this.f642a, "Checked command receiver population state (late result list is " + (this.f644c.isEmpty() ? "" : "not") + " empty)");
                    if (this.f644c.isEmpty()) {
                        z = false;
                    }
                }
            }
        }
        return z;
    }

    public void b() {
        new Thread(new m(this)).start();
    }
}
