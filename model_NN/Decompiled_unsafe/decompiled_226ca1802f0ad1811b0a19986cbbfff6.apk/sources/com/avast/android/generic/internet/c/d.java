package com.avast.android.generic.internet.c;

import com.avast.android.generic.internet.c.a.q;

/* compiled from: AvastAccountConnector */
/* synthetic */ class d {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f804a = new int[q.values().length];

    static {
        try {
            f804a[q.AT_ERROR.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f804a[q.PAIRING_ERROR.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f804a[q.ID_ERROR.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f804a[q.OTHER_ERROR.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f804a[q.INVALID_CREDENTIALS.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f804a[q.OK.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
    }
}
