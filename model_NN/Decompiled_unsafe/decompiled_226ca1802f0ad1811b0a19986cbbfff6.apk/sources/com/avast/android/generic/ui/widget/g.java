package com.avast.android.generic.ui.widget;

import android.view.View;

/* compiled from: EditTextRow */
class g implements View.OnFocusChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EditTextRow f1143a;

    g(EditTextRow editTextRow) {
        this.f1143a = editTextRow;
    }

    public void onFocusChange(View view, boolean z) {
        if (!z) {
            this.f1143a.c();
        }
    }
}
