package com.avast.android.generic.licensing.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.util.z;
import com.avast.c.a.a.ai;
import com.avast.c.a.a.q;
import com.avast.c.a.a.y;
import java.util.List;

/* compiled from: Offer */
public class k {

    /* renamed from: a  reason: collision with root package name */
    private String f869a;

    /* renamed from: b  reason: collision with root package name */
    private y f870b;

    /* renamed from: c  reason: collision with root package name */
    private String f871c;
    private String d;
    private float e;
    private String f;
    private boolean g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private Bitmap m;
    private Integer n;
    private List<ai> o;

    public k(Context context, q qVar, n nVar, List<ai> list) {
        String str;
        String str2;
        String str3;
        String str4;
        boolean z = false;
        Integer num = null;
        this.f869a = qVar.c();
        this.f870b = qVar.g();
        this.f871c = nVar.e();
        this.d = nVar.c();
        a(qVar.i());
        this.f = nVar.d();
        if (this.f.indexOf(" (") > -1) {
            this.f = this.f.substring(0, this.f.indexOf(" ("));
        }
        if (TextUtils.isEmpty(this.f871c)) {
            this.f871c = this.f;
        }
        this.g = qVar.r() ? qVar.s() : z;
        if (this.g) {
            this.h = qVar.t() ? qVar.u() : null;
        }
        if (qVar.v()) {
            str = qVar.w();
        } else {
            str = null;
        }
        this.i = str;
        if (qVar.R()) {
            str2 = qVar.S();
        } else {
            str2 = null;
        }
        this.j = str2;
        if (qVar.z()) {
            str3 = qVar.A();
        } else {
            str3 = null;
        }
        this.l = str3;
        if (qVar.x()) {
            str4 = qVar.y();
        } else {
            str4 = null;
        }
        this.k = str4;
        this.n = qVar.H() ? Integer.valueOf(qVar.I()) : num;
        this.o = list;
        a(context);
    }

    public Drawable a(Context context, int i2) {
        if (this.m != null) {
            int height = this.m.getHeight();
            int width = this.m.getWidth();
            if (height > i2) {
                return new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(this.m, (int) (((double) width) * (((double) i2) / ((double) height))), i2, false));
            }
        }
        return null;
    }

    private void a(Context context) {
        if (this.m == null && !TextUtils.isEmpty(this.k)) {
            try {
                this.m = b.a(context, this.k);
            } catch (Exception e2) {
                z.a("AvastGenericLic", "Can not download advertising image, skipping", e2);
                this.m = null;
            }
        }
    }

    public String a() {
        return this.f869a;
    }

    public y b() {
        return this.f870b;
    }

    public String c() {
        return this.f871c;
    }

    public String d() {
        return this.d;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.h;
    }

    public String g() {
        return this.i;
    }

    public String h() {
        return this.l;
    }

    public float i() {
        return this.e;
    }

    public void a(float f2) {
        this.e = f2;
    }

    public List<ai> j() {
        return this.o;
    }

    public String k() {
        return this.j;
    }
}
