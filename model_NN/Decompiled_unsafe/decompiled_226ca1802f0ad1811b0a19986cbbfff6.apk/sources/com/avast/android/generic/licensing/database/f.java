package com.avast.android.generic.licensing.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;

/* compiled from: PurchaseDatabaseHelper */
class f implements k {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ContentValues f888a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ String f889b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ d f890c;

    f(d dVar, ContentValues contentValues, String str) {
        this.f890c = dVar;
        this.f888a = contentValues;
        this.f889b = str;
    }

    public void a(Uri uri, ContentResolver contentResolver) {
        this.f890c.f883b.update(uri, this.f888a, "order_id= ?", new String[]{this.f889b});
    }
}
