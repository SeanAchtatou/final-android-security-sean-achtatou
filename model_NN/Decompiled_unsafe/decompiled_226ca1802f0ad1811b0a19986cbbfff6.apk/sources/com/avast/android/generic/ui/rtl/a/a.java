package com.avast.android.generic.ui.rtl.a;

/* compiled from: ArabicReshaper */
public class a {

    /* renamed from: a  reason: collision with root package name */
    public static char f1057a = 1570;

    /* renamed from: b  reason: collision with root package name */
    public static char f1058b = 1571;

    /* renamed from: c  reason: collision with root package name */
    public static char f1059c = 1573;
    public static char d = 1575;
    public static char e = 1604;
    public static char[][] f = {new char[]{15270, 65270, 65269}, new char[]{15271, 65272, 65271}, new char[]{1575, 65276, 65275}, new char[]{1573, 65274, 65273}};
    public static char[] g = {1536, 1537, 1538, 1539, 1542, 1543, 1544, 1545, 1546, 1547, 1549, 1550, 1552, 1553, 1554, 1555, 1556, 1557, 1558, 1559, 1560, 1561, 1562, 1563, 1566, 1567, 1569, 1595, 1596, 1597, 1598, 1599, 1600, 1611, 1612, 1613, 1614, 1615, 1616, 1617, 1618, 1619, 1620, 1621, 1622, 1623, 1624, 1625, 1626, 1627, 1628, 1629, 1630, 1632, 1642, 1643, 1644, 1647, 1648, 1650, 1748, 1749, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1759, 1760, 1761, 1762, 1763, 1764, 1765, 1766, 1767, 1768, 1769, 1770, 1771, 1772, 1773, 1774, 1775, 1750, 1751, 1752, 1753, 1754, 1755, 1756, 1757, 1758, 1759, 1776, 1789, 65136, 65137, 65138, 65139, 65140, 65141, 65142, 65143, 65144, 65145, 65146, 65147, 65148, 65149, 65150, 65151, 64606, 64607, 64608, 64609, 64610, 64611};
    public static char[][] h = {new char[]{1570, 65153, 65153, 65154, 65154, 2}, new char[]{1571, 65155, 65155, 65156, 65156, 2}, new char[]{1572, 65157, 65157, 65158, 65158, 2}, new char[]{1573, 65159, 65159, 65160, 65160, 2}, new char[]{1574, 65161, 65163, 65164, 65162, 4}, new char[]{1575, 1575, 1575, 65166, 65166, 2}, new char[]{1576, 65167, 65169, 65170, 65168, 4}, new char[]{1577, 65171, 65171, 65172, 65172, 2}, new char[]{1578, 65173, 65175, 65176, 65174, 4}, new char[]{1579, 65177, 65179, 65180, 65178, 4}, new char[]{1580, 65181, 65183, 65184, 65182, 4}, new char[]{1581, 65185, 65187, 65188, 65186, 4}, new char[]{1582, 65189, 65191, 65192, 65190, 4}, new char[]{1583, 65193, 65193, 65194, 65194, 2}, new char[]{1584, 65195, 65195, 65196, 65196, 2}, new char[]{1585, 65197, 65197, 65198, 65198, 2}, new char[]{1586, 65199, 65199, 65200, 65200, 2}, new char[]{1587, 65201, 65203, 65204, 65202, 4}, new char[]{1588, 65205, 65207, 65208, 65206, 4}, new char[]{1589, 65209, 65211, 65212, 65210, 4}, new char[]{1590, 65213, 65215, 65216, 65214, 4}, new char[]{1591, 65217, 65219, 65220, 65218, 4}, new char[]{1592, 65221, 65223, 65224, 65222, 4}, new char[]{1593, 65225, 65227, 65228, 65226, 4}, new char[]{1594, 65229, 65231, 65232, 65230, 4}, new char[]{1601, 65233, 65235, 65236, 65234, 4}, new char[]{1602, 65237, 65239, 65240, 65238, 4}, new char[]{1603, 65241, 65243, 65244, 65242, 4}, new char[]{1604, 65245, 65247, 65248, 65246, 4}, new char[]{1605, 65249, 65251, 65252, 65250, 4}, new char[]{1606, 65253, 65255, 65256, 65254, 4}, new char[]{1607, 65257, 65259, 65260, 65258, 4}, new char[]{1608, 65261, 65261, 65262, 65262, 2}, new char[]{1609, 65263, 65263, 65264, 65264, 2}, new char[]{1649, 1649, 1649, 64337, 64337, 2}, new char[]{1610, 65265, 65267, 65268, 65266, 4}, new char[]{1646, 64484, 64488, 64489, 64485, 4}, new char[]{1649, 1649, 1649, 64337, 64337, 2}, new char[]{1706, 64398, 64400, 64401, 64399, 4}, new char[]{1729, 64422, 64424, 64425, 64423, 4}, new char[]{1764, 1764, 1764, 1764, 65262, 2}};
    private String i = "";

    public String a() {
        return this.i;
    }

    private char a(char c2, int i2) {
        for (int i3 = 0; i3 < h.length; i3++) {
            if (h[i3][0] == c2) {
                return h[i3][i2];
            }
        }
        return c2;
    }

    private int a(char c2) {
        for (int i2 = 0; i2 < h.length; i2++) {
            if (h[i2][0] == c2) {
                return h[i2][5];
            }
        }
        return 2;
    }

    /* access modifiers changed from: private */
    public boolean b(char c2) {
        for (char c3 : g) {
            if (c3 == c2) {
                return true;
            }
        }
        return false;
    }

    private String b(String str) {
        char a2;
        int length = str.length();
        char[] cArr = new char[length];
        str.getChars(0, length, cArr, 0);
        char c2 = 0;
        for (int i2 = 0; i2 < cArr.length - 1; i2++) {
            if (!b(cArr[i2]) && e != cArr[i2]) {
                c2 = cArr[i2];
            }
            if (e == cArr[i2]) {
                char c3 = cArr[i2];
                int i3 = i2 + 1;
                while (i3 < cArr.length && b(cArr[i3])) {
                    i3++;
                }
                if (i3 < cArr.length) {
                    if (i2 <= 0 || a(c2) <= 2) {
                        a2 = a(cArr[i3], c3, true);
                    } else {
                        a2 = a(cArr[i3], c3, false);
                    }
                    if (a2 != 0) {
                        cArr[i2] = a2;
                        cArr[i3] = ' ';
                    }
                }
            }
        }
        return new String(cArr).replaceAll(" ", "").trim();
    }

    private char a(char c2, char c3, boolean z) {
        char c4;
        char c5 = 0;
        if (z) {
            c4 = 2;
        } else {
            c4 = 1;
        }
        if (e != c3) {
            return 0;
        }
        if (c2 == f1057a) {
            c5 = f[0][c4];
        }
        if (c2 == f1058b) {
            c5 = f[1][c4];
        }
        if (c2 == f1059c) {
            c5 = f[3][c4];
        }
        if (c2 == d) {
            return f[2][c4];
        }
        return c5;
    }

    public a(String str) {
        b bVar = new b(this, b(str));
        if (bVar.f1062c.length > 0) {
            this.i = a(new String(bVar.f1062c));
        }
        this.i = bVar.a(this.i);
    }

    public String a(String str) {
        StringBuffer stringBuffer = new StringBuffer("");
        int length = str.length();
        char[] cArr = new char[length];
        str.getChars(0, length, cArr, 0);
        stringBuffer.append(a(cArr[0], 2));
        for (int i2 = 1; i2 < length - 1; i2++) {
            if (a(cArr[i2 - 1]) == 2) {
                stringBuffer.append(a(cArr[i2], 2));
            } else {
                stringBuffer.append(a(cArr[i2], 3));
            }
        }
        if (length >= 2) {
            if (a(cArr[length - 2]) == 2) {
                stringBuffer.append(a(cArr[length - 1], 1));
            } else {
                stringBuffer.append(a(cArr[length - 1], 4));
            }
        }
        return stringBuffer.toString();
    }
}
