package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class CheckBoxRow extends Row {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public CheckBox f1076a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1077b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public c f1078c;
    /* access modifiers changed from: private */
    public boolean m = false;
    /* access modifiers changed from: private */
    public boolean n = false;

    public CheckBoxRow(Context context) {
        super(context);
    }

    public CheckBoxRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row));
    }

    public CheckBoxRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowStyle, y.Row));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        this.f1077b = typedArray.getBoolean(3, false);
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.f1076a = (CheckBox) inflate(getContext(), t.row_check_box, this).findViewById(r.checkbox);
        this.f1076a.setId(-1);
        this.f1076a.setChecked(this.f1077b);
        a(new a(this));
        this.f1076a.setOnCheckedChangeListener(new b(this));
    }

    public void b() {
        if (e() != null) {
            this.m = true;
            try {
                this.f1076a.setChecked(e().b(this.g, this.f1077b));
            } finally {
                this.m = false;
            }
        }
    }

    public void a(c cVar) {
        this.f1078c = cVar;
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(z);
        super.setClickable(z);
        this.f1076a.setEnabled(z);
    }

    public boolean c() {
        return this.f1076a.isChecked();
    }

    public void a(boolean z) {
        if (this.f1076a.isChecked() != z) {
            if (e() != null) {
                e().a(this.g, z);
            }
            this.n = true;
            try {
                this.f1076a.setChecked(z);
            } finally {
                this.n = false;
            }
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1079a = this.f1076a.isChecked();
        savedState.f1080b = isEnabled();
        return savedState;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.m = true;
        try {
            this.f1076a.setChecked(savedState.f1079a);
            this.m = false;
            setEnabled(savedState.f1080b);
        } catch (Throwable th) {
            this.m = false;
            throw th;
        }
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new d();

        /* renamed from: a  reason: collision with root package name */
        boolean f1079a;

        /* renamed from: b  reason: collision with root package name */
        boolean f1080b;

        /* synthetic */ SavedState(Parcel parcel, a aVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            Bundle readBundle = parcel.readBundle();
            this.f1079a = readBundle.getBoolean("checked");
            this.f1080b = readBundle.getBoolean("enabled");
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            Bundle bundle = new Bundle();
            bundle.putBoolean("checked", this.f1079a);
            bundle.putBoolean("enabled", this.f1080b);
            parcel.writeBundle(bundle);
        }
    }
}
