package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.app.passwordrecovery.a;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.z;

public class SuiteMessageListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String string;
        String string2;
        String action = intent.getAction();
        Context applicationContext = context.getApplicationContext();
        Bundle extras = intent.getExtras();
        if (action.equals("com.avast.android.generic.action.MESSAGE_TO_SUITE")) {
            if (extras != null && (string2 = extras.getString("sourcePackage")) != null && !string2.equals(applicationContext.getPackageName())) {
                String string3 = extras.getString("text");
                String string4 = extras.getString("uid");
                String string5 = extras.getString("number");
                if (((string4 != null && !string4.equals("")) || string5 != null) && string3 != null && !string3.equals("")) {
                    z.b(applicationContext, string2, "Message " + string3);
                    a(applicationContext, (ab) aa.a(applicationContext, ad.class), string3, string5, string4);
                }
            }
        } else if (action.equals("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED")) {
            Intent intent2 = new Intent();
            intent2.setAction("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED");
            intent2.putExtras(new Bundle());
            ae.a(applicationContext, intent2, applicationContext.getPackageName());
        } else if (action.equals("com.avast.android.mobilesecurity.app.account.ACCOUNT_DISCONNECTED")) {
            Intent intent3 = new Intent();
            intent3.setAction("com.avast.android.mobilesecurity.app.account.ACCOUNT_DISCONNECTED");
            intent3.putExtras(new Bundle());
            ae.a(applicationContext, intent3, applicationContext.getPackageName());
        } else if (action.equals("com.avast.android.generic.action.UPDATE_CHECK_SUITE")) {
            if (extras != null && (string = extras.getString("sourcePackage")) != null && !string.equals(applicationContext.getPackageName())) {
                z.b(applicationContext, string, "Update check to be performed");
                Intent intent4 = new Intent();
                intent4.setAction("com.avast.android.generic.action.UPDATE_CHECK_SUITE");
                intent4.putExtras(extras);
                ae.a(applicationContext, intent4, applicationContext.getPackageName());
            }
        } else if (action.equals("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION")) {
            z.b(applicationContext, applicationContext.getPackageName(), "Push account task to be performed");
            Intent intent5 = new Intent();
            intent5.setAction("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION");
            ae.a(applicationContext, intent5, applicationContext.getPackageName());
        }
    }

    /* access modifiers changed from: protected */
    public void a(Context context, ab abVar, String str, String str2, String str3) {
        String str4;
        boolean z;
        int indexOf = str.indexOf(" ");
        if (indexOf > -1) {
            str4 = str.substring(0, indexOf);
        } else {
            str4 = str;
        }
        boolean b2 = abVar.b(str4);
        if (!b2 && abVar.t() && abVar.y().equals(str4)) {
            b2 = true;
        }
        if (b2 || !a.a(context, str4)) {
            z = b2;
        } else {
            z = true;
        }
        if (z) {
            aw.a(context);
            z.a(context, "Message handled, service starting");
            if (str3 == null) {
                z.a(context, "Message handled, service starting");
                Intent intent = new Intent();
                intent.setAction("com.avast.android.generic.service.action.SMS_RECEIVED");
                intent.putExtra("number", str2);
                intent.putExtra("text", str);
                ae.a(context, intent, context.getPackageName());
                return;
            }
            aw.a(context);
            Intent intent2 = new Intent();
            intent2.setAction("com.avast.android.generic.action.ACTION_C2DM_MESSAGE");
            intent2.putExtra("uid", str3);
            intent2.putExtra("message", str);
            ae.a(context, intent2, context.getPackageName());
        }
    }
}
