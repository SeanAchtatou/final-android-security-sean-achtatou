package com.avast.android.generic.licensing.c;

/* compiled from: IabResult */
public class i {

    /* renamed from: a  reason: collision with root package name */
    int f865a;

    /* renamed from: b  reason: collision with root package name */
    String f866b;

    public i(int i, String str) {
        this.f865a = i;
        if (str == null || str.trim().length() == 0) {
            this.f866b = b.a(i);
        } else {
            this.f866b = str + " (response: " + b.a(i) + ")";
        }
    }

    public int a() {
        return this.f865a;
    }

    public String b() {
        return this.f866b;
    }

    public boolean c() {
        return this.f865a == 0;
    }

    public String toString() {
        return "IabResult: " + b();
    }
}
