package com.avast.android.generic.filebrowser;

import android.support.v4.app.Fragment;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public abstract class AbstractFileBrowserActivity extends BaseSinglePaneActivity {

    /* renamed from: a  reason: collision with root package name */
    public static final String f668a = d.FILE.toString();

    /* renamed from: b  reason: collision with root package name */
    public static final String f669b = d.DIR.toString();

    /* renamed from: c  reason: collision with root package name */
    public static final String f670c = d.BOTH.toString();

    public void onBackPressed() {
        Fragment findFragmentByTag = getSupportFragmentManager().findFragmentByTag("singleFragment");
        if (findFragmentByTag == null || !(findFragmentByTag instanceof AbstractFileBrowserFragment)) {
            super.onBackPressed();
        } else if (!((AbstractFileBrowserFragment) findFragmentByTag).c()) {
            finish();
        }
    }
}
