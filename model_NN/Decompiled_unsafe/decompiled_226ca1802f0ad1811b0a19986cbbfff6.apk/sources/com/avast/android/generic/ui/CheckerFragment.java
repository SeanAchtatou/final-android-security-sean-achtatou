package com.avast.android.generic.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ae;
import com.avast.android.generic.o;
import com.avast.android.generic.q;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.d.a;
import com.avast.android.generic.ui.d.d;
import com.avast.android.generic.ui.widget.ContextDialogFragment;
import com.avast.android.generic.ui.widget.f;
import com.avast.android.generic.util.ga.TrackedMultiToolListFragment;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;
import java.util.List;

public abstract class CheckerFragment extends TrackedMultiToolListFragment implements f {

    /* renamed from: a  reason: collision with root package name */
    private View f986a;

    /* renamed from: b  reason: collision with root package name */
    private a f987b;

    /* renamed from: c  reason: collision with root package name */
    private int f988c = -1;
    private Button d;
    private Button e;
    private Button f;
    /* access modifiers changed from: private */
    public boolean g = false;
    private TextView h;
    private LinearLayout i;
    private boolean j = false;
    private boolean k = false;
    private String l = null;
    private Boolean m = null;
    private LinearLayout n = null;

    public abstract void a();

    public abstract a b();

    public abstract int c();

    public abstract int e();

    public abstract int f();

    public abstract String h();

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f987b = b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_checker, viewGroup, false);
        b(inflate).setVisibility(8);
        this.f986a = layoutInflater.inflate(t.list_item_checker_header, (ViewGroup) null);
        ((TextView) inflate.findViewById(r.l_header)).setText(e());
        this.h = (TextView) this.f986a.findViewById(r.bM);
        this.h.setText(f());
        this.d = (Button) this.f986a.findViewById(r.A);
        this.e = (Button) this.f986a.findViewById(r.y);
        this.f = (Button) this.f986a.findViewById(r.b_revoke_ignore);
        this.i = (LinearLayout) this.f986a.findViewById(r.l_checker_empty);
        if (this.j) {
            this.n = (LinearLayout) this.f986a.findViewById(r.b_buttons_checker);
            this.n.setVisibility(8);
            this.n = (LinearLayout) this.f986a.findViewById(r.b_buttons_revoke_ignore);
            this.n.setVisibility(0);
        } else {
            this.n = (LinearLayout) this.f986a.findViewById(r.b_buttons_revoke_ignore);
            this.n.setVisibility(8);
            this.n = (LinearLayout) this.f986a.findViewById(r.b_buttons_checker);
            this.n.setVisibility(0);
        }
        this.e.setOnClickListener(new g(this));
        this.f.setOnClickListener(new h(this));
        return inflate;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        getListView().addHeaderView(this.f986a);
        setListAdapter(this.f987b);
    }

    public void onResume() {
        if (isAdded()) {
            i();
            super.onResume();
        }
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        if (isAdded() && i2 == 2) {
            com.avast.android.generic.util.a.a(this);
        }
    }

    /* access modifiers changed from: protected */
    public void i() {
        boolean z;
        boolean z2;
        if (isAdded()) {
            try {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    if (this.f != null) {
                        if (this.g) {
                            this.f.setText(getString(x.l_revoke_revoke_ignore));
                        } else {
                            this.f.setText(getString(x.l_revoke_ignore));
                        }
                    }
                    this.f987b.a(activity, this.g);
                    List<d> a2 = this.f987b.a();
                    if (a2 != null) {
                        boolean z3 = false;
                        for (d c2 : a2) {
                            if (!c2.c()) {
                                z3 = true;
                            }
                        }
                        z = z3;
                    } else {
                        z = false;
                    }
                    if (this.m == null || this.m.booleanValue() != z) {
                        ab abVar = (ab) aa.a(activity, ae.class);
                        abVar.a("hasproblems" + (this.l != null ? "_" + this.l : ""), z);
                        abVar.b();
                        this.m = Boolean.valueOf(z);
                        android.support.v4.content.r.a(activity).a(new Intent("com.avast.android.generic.action.ACTION_PROBLEMS_CHANGED" + (this.l != null ? "_" + this.l : "")));
                    }
                    List<d> a3 = this.f987b.a();
                    if (!this.k) {
                        this.i.setVisibility(8);
                        if (a3 == null || a3.size() == 0) {
                            j();
                            return;
                        }
                    } else {
                        this.i.setVisibility((a3 == null || a3.size() == 0) ? 0 : 8);
                    }
                    boolean z4 = false;
                    boolean z5 = true;
                    for (d b2 : a3) {
                        if (!b2.b()) {
                            z2 = false;
                        } else {
                            z2 = z5;
                        }
                        z5 = z2;
                        z4 = true;
                    }
                    if (!z5) {
                        a("checker", "non ignorable problem present", 0);
                        this.d.setText(getString(x.cb));
                        this.d.setOnClickListener(new i(this));
                        this.e.setText(getString(c()));
                        this.d.setVisibility(0);
                        this.e.setVisibility(0);
                        this.h.setText(f());
                        this.h.setTextColor(getResources().getColor(o.text_warning));
                        this.h.setCompoundDrawablesWithIntrinsicBounds(0, 0, q.ic_scanner_result_warning, 0);
                    } else {
                        a("checker", "only ignorable problems", 0);
                        this.d.setText(getString(x.l_ignore_items_for_now));
                        this.d.setOnClickListener(new j(this));
                        this.e.setText(getString(c()));
                        this.d.setVisibility(0);
                        this.e.setVisibility(0);
                        if (z4) {
                            this.h.setText(f());
                            this.h.setTextColor(getResources().getColor(o.text_warning));
                            this.h.setCompoundDrawablesWithIntrinsicBounds(0, 0, q.ic_scanner_result_warning, 0);
                        } else {
                            this.h.setText(x.l_no_issues_text);
                            this.h.setTextColor(getResources().getColor(o.g));
                            this.h.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                        }
                    }
                    this.f987b.notifyDataSetChanged();
                }
            } catch (Exception e2) {
                z.a("AvastGeneric", "Error updating setup check rows", e2);
            }
        }
    }

    public void j() {
        if (isAdded()) {
            a("checker", "success, launch next", 0);
            a();
        }
    }

    public void onDestroyView() {
        super.onDestroyView();
        setListAdapter(null);
    }

    public void onListItemClick(ListView listView, View view, int i2, long j2) {
        FragmentActivity activity;
        if (isAdded() && (activity = getActivity()) != null) {
            int i3 = i2 - 1;
            this.f988c = i3;
            d dVar = (d) this.f987b.getItem(i3);
            a("checker", "click row " + dVar.a(), 0);
            List<String> d2 = dVar.d(activity);
            String[] strArr = new String[d2.size()];
            if (d2.size() > 0) {
                for (int i4 = 0; i4 < d2.size(); i4++) {
                    strArr[i4] = d2.get(i4);
                }
            }
            ContextDialogFragment contextDialogFragment = new ContextDialogFragment();
            contextDialogFragment.a(strArr);
            contextDialogFragment.a(getString(x.menu_dialog_select_action));
            contextDialogFragment.setTargetFragment(this, 0);
            contextDialogFragment.show(getFragmentManager(), "contextDialogFragment");
        }
    }

    public void a(Long l2, int i2) {
        if (isAdded()) {
            try {
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    d dVar = (d) this.f987b.getItem(this.f988c);
                    if (dVar == null) {
                        i();
                        return;
                    }
                    a("checker", "click context item " + dVar.a(), i2);
                    if (dVar.a(activity, this, i2)) {
                        a("checker", "problem solved " + dVar.a(), 0);
                        new Thread(new k(this, dVar)).start();
                        return;
                    }
                    a("checker", "problem solved later " + dVar.a(), 0);
                    i();
                }
            } catch (IndexOutOfBoundsException e2) {
            }
        }
    }

    public final void a(String str, String str2, String str3, long j2) {
    }

    public void a(String str, String str2, int i2) {
        if (k() != null) {
            super.a(h(), str, str2, (long) i2);
        }
    }
}
