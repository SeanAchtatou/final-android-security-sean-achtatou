package com.avast.android.generic.ui.widget;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.avast.android.generic.ui.widget.SelectorRow;

/* compiled from: SelectorRow */
class t implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelectorRow f1156a;

    t(SelectorRow selectorRow) {
        this.f1156a = selectorRow;
    }

    public void onClick(View view) {
        if (this.f1156a.getContext() instanceof FragmentActivity) {
            SelectorRow.SelectorDialog.a(((FragmentActivity) this.f1156a.getContext()).getSupportFragmentManager(), this.f1156a.getId(), this.f1156a.m, this.f1156a.n);
        }
    }
}
