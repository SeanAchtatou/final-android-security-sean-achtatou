package com.avast.android.generic.app.account;

import android.content.DialogInterface;

/* compiled from: AccountDisconnectDialog */
class b implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccountDisconnectDialog f405a;

    b(AccountDisconnectDialog accountDisconnectDialog) {
        this.f405a = accountDisconnectDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f405a.a();
    }
}
