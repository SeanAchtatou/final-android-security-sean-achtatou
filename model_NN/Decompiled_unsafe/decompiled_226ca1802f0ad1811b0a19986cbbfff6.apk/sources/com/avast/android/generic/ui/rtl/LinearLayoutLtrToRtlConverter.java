package com.avast.android.generic.ui.rtl;

import android.view.View;
import android.widget.LinearLayout;
import com.avast.android.generic.util.t;
import java.lang.reflect.Field;
import java.util.Stack;

public class LinearLayoutLtrToRtlConverter extends a {
    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.view.View.getPaddingRight():int in method: com.avast.android.generic.ui.rtl.LinearLayoutLtrToRtlConverter.swapMarginAndPadding(android.view.View):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.view.View.getPaddingRight():int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    private void swapMarginAndPadding(android.view.View r1) {
        /*
            r4 = this;
            android.view.ViewGroup$LayoutParams r0 = r5.getLayoutParams()
            boolean r0 = r0 instanceof android.widget.LinearLayout.LayoutParams
            if (r0 == 0) goto L_0x0016
            android.view.ViewGroup$LayoutParams r0 = r5.getLayoutParams()
            android.widget.LinearLayout$LayoutParams r0 = (android.widget.LinearLayout.LayoutParams) r0
            int r1 = r0.leftMargin
            int r2 = r0.rightMargin
            r0.leftMargin = r2
            r0.rightMargin = r1
        L_0x0016:
            int r0 = r5.getPaddingLeft()
            int r1 = r5.getPaddingRight()
            int r2 = r5.getPaddingTop()
            int r3 = r5.getPaddingBottom()
            r5.setPadding(r1, r2, r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.ui.rtl.LinearLayoutLtrToRtlConverter.swapMarginAndPadding(android.view.View):void");
    }

    public LinearLayoutLtrToRtlConverter(String str) {
        super(str);
    }

    public View ltrToRtlView(View view) {
        if (view instanceof LinearLayout) {
            return ltrToRtlIfLinearLayout((LinearLayout) view);
        }
        throw new IllegalArgumentException("The view must be an instance of LinearLayout and it's " + view.getClass() + ".");
    }

    private View ltrToRtlIfLinearLayout(LinearLayout linearLayout) {
        int i = 0;
        swapGravity(linearLayout);
        if (linearLayout.getOrientation() == 1) {
            while (i < linearLayout.getChildCount()) {
                swapMarginAndPadding(linearLayout.getChildAt(i));
                i++;
            }
        } else {
            Stack stack = new Stack();
            while (i < linearLayout.getChildCount()) {
                stack.add(linearLayout.getChildAt(i));
                i++;
            }
            linearLayout.removeAllViews();
            while (!stack.isEmpty()) {
                View view = (View) stack.pop();
                swapMarginAndPadding(view);
                linearLayout.addView(view);
            }
        }
        return linearLayout;
    }

    private void swapGravity(LinearLayout linearLayout) {
        try {
            Field declaredField = LinearLayout.class.getDeclaredField("mGravity");
            declaredField.setAccessible(true);
            int intValue = ((Integer) declaredField.get(linearLayout)).intValue();
            if ((intValue & 3) == 3) {
                linearLayout.setGravity((intValue ^ 3) | 5);
            } else if ((intValue & 5) == 5) {
                linearLayout.setGravity((intValue ^ 5) | 3);
            }
        } catch (IllegalAccessException e) {
            t.b("Can't set onClickListener, illegal access to field mSingleLine.", e);
        } catch (NoSuchFieldException e2) {
            t.b("Can't set onClickListener, mSingleLine field not found.", e2);
        }
    }
}
