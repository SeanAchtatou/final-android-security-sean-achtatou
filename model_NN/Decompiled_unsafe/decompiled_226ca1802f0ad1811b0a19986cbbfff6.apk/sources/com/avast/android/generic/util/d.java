package com.avast.android.generic.util;

import android.content.Context;
import com.avast.android.generic.Application;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.ga.a;
import com.avast.android.generic.util.ga.c;
import com.mixpanel.android.mpmetrics.l;
import java.util.Calendar;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: BaseTracker */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private Application f1215a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1216b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1217c;
    private l d;
    private c e;
    private j f;

    public static synchronized d b(Context context) {
        d dVar;
        synchronized (d.class) {
            dVar = (d) aa.a(context, d.class);
        }
        return dVar;
    }

    protected d(Application application, j jVar, boolean z) {
        this.f1217c = z;
        this.f1215a = application;
        this.f = jVar;
        c();
    }

    private void c() {
        a(this.f1215a);
        d(this.f1215a);
    }

    private void a(Context context) {
        a b2 = a.b();
        b2.a(context);
        b2.a(this.f1217c);
        this.e = a.a();
    }

    private void d(Context context) {
        this.d = l.a(context, "799a553ad3930da68b23eeec8a02249e");
        String s = ((ab) aa.a(context, ab.class)).s();
        b(s);
        if (this.f1216b) {
            this.d.a(s);
            this.d.b().a(s);
        }
    }

    private void b(String str) {
        this.f1216b = Math.abs(UUID.fromString(str).getLeastSignificantBits() % 1000) < 100;
        t.c("In tracking sample: " + this.f1216b);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Throwable th, boolean z) {
        if (this.e != null) {
            this.e.a(str, th, z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.util.d.a(java.lang.String, java.lang.Throwable, boolean):void
     arg types: [java.lang.String, org.json.JSONException, int]
     candidates:
      com.avast.android.generic.util.d.a(com.avast.android.generic.util.g, com.avast.android.generic.util.f, java.lang.String):void
      com.avast.android.generic.util.d.a(java.lang.String, java.lang.Throwable, boolean):void */
    /* access modifiers changed from: protected */
    public void a(String str, String str2, String str3, long j) {
        if (this.f1217c) {
            this.e.a(str, str2, str3 == null ? "" : str3, Long.valueOf(str3 == null ? -1 : j));
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("Category", str);
                if (str3 != null) {
                    jSONObject.put("Result", str3);
                    jSONObject.put("Value", j);
                }
            } catch (JSONException e2) {
                a(e2.getMessage(), (Throwable) e2, false);
            }
            if (this.f1216b) {
                this.d.a(str2, jSONObject);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (this.f1217c) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("user_id", str);
                this.d.a(jSONObject);
            } catch (JSONException e2) {
                t.b("Unable to set mixpanel superproperty for user id", e2);
            }
            this.d.b().a(jSONObject);
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str, String str2) {
        if (this.f1217c) {
            this.d.b().a(str, str2);
        }
    }

    public void a(g gVar, f fVar, String str) {
        a(str);
        a("account_connected", fVar.a());
        a(gVar.a(), "account_connected", fVar.a(), -1);
    }

    public void a(g gVar) {
        a(gVar.a(), "account_disconnected", null, -1);
    }

    public void b(g gVar) {
        a(gVar.toString(), "eula_expanded", null, -1);
    }

    public void c(g gVar) {
        a(gVar.toString(), "privacy_policy_opened", null, -1);
    }

    public void a(String str, float f2) {
        a(g.BILLING.a(), "purchase_started", str, (long) ((int) Math.floor((double) (100.0f * f2))));
    }

    public void b(String str, float f2) {
        a(g.BILLING.a(), "purchase_advertisement_shown", str, (long) ((int) Math.floor((double) (100.0f * f2))));
    }

    public void c(String str, float f2) {
        a(g.BILLING.a(), "purchase_completed", str, (long) ((int) Math.floor((double) (100.0f * f2))));
    }

    public void a(i iVar) {
        a(g.BILLING.a(), "purchase_cancelled", iVar.a(), -1);
    }

    public void a(e eVar) {
        a(g.POST_INSTALL.a(), "accept_terms", eVar.a(), -1);
    }

    public void a(h hVar) {
        a(g.AT_INSTALL.a(), "install_anti_theft_pressed", hVar.a(), -1);
    }

    public void c(Context context) {
        ab abVar = (ab) aa.a(context, ab.class);
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(abVar.ab());
        Calendar instance2 = Calendar.getInstance();
        instance2.set(10, 0);
        instance2.set(12, 0);
        instance2.set(13, 0);
        instance2.set(14, 0);
        if (instance2.after(instance)) {
            t.c("Active user tracked");
            abVar.h(instance2.getTimeInMillis());
            a(g.APPLICATION.a(), "active_user", this.f.a(), -1);
        }
    }

    public void a() {
        a(g.BILLING.a(), "already_have_license_button_pressed", null, -1);
    }

    public void b() {
        a(g.BILLING.a(), "voucher_button_pressed", null, -1);
    }
}
