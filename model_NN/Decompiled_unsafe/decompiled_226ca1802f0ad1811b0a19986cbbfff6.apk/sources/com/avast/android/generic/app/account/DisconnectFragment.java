package com.avast.android.generic.app.account;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.r;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ui.PasswordDialog;
import com.avast.android.generic.util.b;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.util.t;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;
import java.lang.reflect.Method;

public abstract class DisconnectFragment extends TrackedMultiToolFragment implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private Handler f365a;

    /* renamed from: b  reason: collision with root package name */
    private ProgressDialog f366b;

    /* renamed from: c  reason: collision with root package name */
    private Handler.Callback f367c;
    private bd d;
    /* access modifiers changed from: private */
    public ab e;
    private BroadcastReceiver f = new ba(this);

    /* access modifiers changed from: protected */
    public abstract int d();

    /* access modifiers changed from: private */
    @TargetApi(11)
    public void e() {
        if (isAdded()) {
            if (this.d != null) {
                this.d.cancel(true);
            }
            this.d = new bd(this, this.e);
            b.a(this.d, new String[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        AccountDisconnectDialog.a(getFragmentManager());
    }

    /* access modifiers changed from: private */
    public void a(bd bdVar) {
        if (isAdded()) {
            f();
            this.f366b = new ProgressDialog(getActivity());
            this.f366b.setCancelable(true);
            this.f366b.setCanceledOnTouchOutside(false);
            this.f366b.setOnCancelListener(new bb(this, bdVar));
            this.f366b.setMessage(getString(x.l_avast_account_disconnecting));
            this.f366b.show();
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        if (this.f366b != null) {
            if (isAdded()) {
                this.f366b.dismiss();
            }
            this.f366b = null;
        }
    }

    public static String a(Context context) {
        Method method;
        String str;
        try {
            method = Class.forName("com.avast.android.mobilesecurity.app.account.ServerAddressHelper").getMethod("getUnpairingServerAddress", Context.class);
        } catch (Exception e2) {
            t.a("breadcrumbs", "ServerAddressHelper not available.", e2);
            method = null;
        }
        if (method != null) {
            try {
                str = (String) method.invoke(null, context);
            } catch (Exception e3) {
                t.a("breadcrumbs", "Invocation of ServerAddressHelper.getUnpairingServerAddress() failed.", e3);
                return null;
            }
        } else {
            str = null;
        }
        return str;
    }

    private void n() {
        o();
        r.a(getActivity()).a(this.f, new IntentFilter("com.avast.android.generic.app.account.ACTION_DISCONNECT_CONFIRMED"));
    }

    private void o() {
        try {
            r.a(getActivity()).a(this.f);
        } catch (Exception e2) {
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        this.f365a = new Handler();
        n();
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        if (this.d != null && !this.d.getStatus().equals(AsyncTask.Status.FINISHED)) {
            a(this.d);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.e = (ab) aa.a(getActivity(), ab.class);
        this.f367c = PasswordDialog.a(getActivity(), d(), new bc(this));
        ((y) aa.a(getActivity(), y.class)).a(com.avast.android.generic.r.message_avast_account_disconnected, this);
    }

    /* access modifiers changed from: protected */
    public ab h() {
        return this.e;
    }

    public void onDetach() {
        super.onDetach();
        f();
    }

    public void onPause() {
        super.onPause();
        o();
    }

    public void onResume() {
        super.onResume();
        n();
    }

    public void onDestroy() {
        super.onDestroy();
        o();
        if (this.d != null && !this.d.getStatus().equals(AsyncTask.Status.FINISHED)) {
            this.d.cancel(true);
        }
        ((y) aa.a(getActivity(), y.class)).b(d(), this.f367c);
        this.f367c = null;
    }

    public boolean handleMessage(Message message) {
        if (!isAdded() || isDetached()) {
            return false;
        }
        if (message.what == com.avast.android.generic.r.message_avast_account_disconnected) {
        }
        return true;
    }
}
