package com.avast.android.generic.app.account;

import android.content.DialogInterface;

/* compiled from: DisconnectFragment */
class bb implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bd f407a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ DisconnectFragment f408b;

    bb(DisconnectFragment disconnectFragment, bd bdVar) {
        this.f408b = disconnectFragment;
        this.f407a = bdVar;
    }

    public void onCancel(DialogInterface dialogInterface) {
        this.f407a.cancel(true);
    }
}
