package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;

/* compiled from: ErrorDialog */
class b implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ErrorDialog f541a;

    b(ErrorDialog errorDialog) {
        this.f541a = errorDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f541a.f513b.putExtra("return_code", d.DISMISSED_NEGATIVE.a());
    }
}
