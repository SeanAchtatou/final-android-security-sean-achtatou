package com.avast.android.generic;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.avast.android.generic.util.ak;

/* compiled from: Alerts */
public class a {
    public static void a(Context context, String str) {
        a(context, str, null, null, null);
    }

    public static void b(Context context, String str) {
        a(context, str, null, null, null);
    }

    public static void a(Context context, String str, DialogInterface.OnClickListener onClickListener) {
        a(context, str, null, null, onClickListener);
    }

    public static void a(Context context, String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        a(context, str, str2, str3, onClickListener, null);
    }

    public static void a(Context context, String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2) {
        a(context, str, str2, str3, onClickListener, onClickListener2, true);
    }

    public static void a(Context context, String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, DialogInterface.OnClickListener onClickListener2, boolean z) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(context));
            builder.setMessage(str);
            if (str2 != null) {
                builder.setPositiveButton(str2, onClickListener);
            } else {
                builder.setPositiveButton("OK", onClickListener);
            }
            if (str3 != null) {
                builder.setNegativeButton(str3, onClickListener2);
            }
            if (!z) {
                builder.setCancelable(false);
            }
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void a(Context context, String str, CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(context));
            builder.setTitle(str);
            builder.setItems(charSequenceArr, onClickListener);
            builder.create().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
