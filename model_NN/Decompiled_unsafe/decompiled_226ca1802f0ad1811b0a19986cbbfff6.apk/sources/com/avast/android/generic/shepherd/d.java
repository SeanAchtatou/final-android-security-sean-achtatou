package com.avast.android.generic.shepherd;

import android.content.Context;
import com.avast.e.a;
import com.avast.e.a.i;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* compiled from: ShepherdConfig */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final List<WeakReference<g>> f973a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    private static d f974b = null;

    /* renamed from: c  reason: collision with root package name */
    private final i f975c = a.a();
    private i d;
    private f e = null;
    private e f = null;

    private d(Context context) {
        this.d = a.a(context);
    }

    protected static synchronized d a(Context context) {
        d dVar;
        synchronized (d.class) {
            if (f974b == null) {
                f974b = new d(context);
            }
            dVar = f974b;
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    public synchronized void a(Context context, byte[] bArr) {
        this.d = i.a(bArr);
        a.a(context, this.d);
        if (this.e != null) {
            this.e.a(this.d);
        }
        if (this.f != null) {
            this.f.a(this.d);
        }
        b(context);
    }

    private static void b(Context context) {
        Iterator<WeakReference<g>> it = f973a.iterator();
        while (it.hasNext()) {
            g gVar = (g) it.next().get();
            if (gVar == null) {
                it.remove();
            } else {
                gVar.a(a(context));
            }
        }
    }
}
