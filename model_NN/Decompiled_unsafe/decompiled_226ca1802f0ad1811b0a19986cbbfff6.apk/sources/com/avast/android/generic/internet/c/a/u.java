package com.avast.android.generic.internet.c.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class u extends GeneratedMessageLite.Builder<t, u> implements v {

    /* renamed from: a  reason: collision with root package name */
    private int f786a;

    /* renamed from: b  reason: collision with root package name */
    private Object f787b = "";

    /* renamed from: c  reason: collision with root package name */
    private Object f788c = "";
    private Object d = "";
    private boolean e;
    private boolean f;
    private boolean g;
    private Object h = "";

    private u() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static u g() {
        return new u();
    }

    /* renamed from: a */
    public u clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public t getDefaultInstanceForType() {
        return t.a();
    }

    /* renamed from: c */
    public t build() {
        t d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public t d() {
        int i = 1;
        t tVar = new t(this);
        int i2 = this.f786a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = tVar.f785c = this.f787b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = tVar.d = this.f788c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = tVar.e = this.d;
        if ((i2 & 8) == 8) {
            i |= 8;
        }
        boolean unused4 = tVar.f = this.e;
        if ((i2 & 16) == 16) {
            i |= 16;
        }
        boolean unused5 = tVar.g = this.f;
        if ((i2 & 32) == 32) {
            i |= 32;
        }
        boolean unused6 = tVar.h = this.g;
        if ((i2 & 64) == 64) {
            i |= 64;
        }
        Object unused7 = tVar.i = this.h;
        int unused8 = tVar.f784b = i;
        return tVar;
    }

    /* renamed from: a */
    public u mergeFrom(t tVar) {
        if (tVar != t.a()) {
            if (tVar.b()) {
                a(tVar.c());
            }
            if (tVar.d()) {
                b(tVar.e());
            }
            if (tVar.f()) {
                c(tVar.g());
            }
            if (tVar.h()) {
                a(tVar.i());
            }
            if (tVar.j()) {
                b(tVar.k());
            }
            if (tVar.l()) {
                c(tVar.m());
            }
            if (tVar.n()) {
                d(tVar.o());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public u mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f786a |= 1;
                    this.f787b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f786a |= 2;
                    this.f788c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f786a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewCloseIcon:
                    this.f786a |= 8;
                    this.e = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_textColorSearchUrl:
                    this.f786a |= 16;
                    this.f = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_windowMinWidthMajor:
                    this.f786a |= 32;
                    this.g = codedInputStream.readBool();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f786a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public u a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f786a |= 1;
        this.f787b = str;
        return this;
    }

    public u b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f786a |= 2;
        this.f788c = str;
        return this;
    }

    public u c(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f786a |= 4;
        this.d = str;
        return this;
    }

    public u a(boolean z) {
        this.f786a |= 8;
        this.e = z;
        return this;
    }

    public u b(boolean z) {
        this.f786a |= 16;
        this.f = z;
        return this;
    }

    public u c(boolean z) {
        this.f786a |= 32;
        this.g = z;
        return this;
    }

    public u d(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f786a |= 64;
        this.h = str;
        return this;
    }
}
