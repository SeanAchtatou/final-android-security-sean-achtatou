package com.avast.android.generic.internet;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.ae;
import com.avast.android.generic.internet.b.a;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.af;
import com.avast.android.generic.util.av;
import com.avast.android.generic.util.z;
import com.avast.b.a.a.a.m;
import com.avast.b.a.a.aj;
import com.avast.b.a.a.ak;
import com.avast.b.a.a.c;
import com.avast.b.a.a.e;
import com.avast.b.a.a.h;
import com.avast.b.a.a.s;
import com.avast.b.a.a.t;
import com.avast.b.a.a.v;
import com.avast.c.a.a.ap;
import com.avast.c.a.a.as;
import com.avast.c.a.a.d;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

/* compiled from: Http */
public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private static final IntentFilter f746a = new IntentFilter("android.intent.action.BATTERY_CHANGED");

    /* renamed from: b  reason: collision with root package name */
    private static int f747b = -1;

    /* renamed from: c  reason: collision with root package name */
    private static int f748c = -1;
    private static String d = null;
    private static String e = null;

    /* access modifiers changed from: protected */
    public abstract boolean a(AvastService avastService, ab abVar, ab abVar2, ak akVar, int i);

    /* access modifiers changed from: protected */
    public abstract boolean a(AvastService avastService, ab abVar, ab abVar2, ak akVar, int i, long j);

    public static HttpURLConnection a(URL url) {
        Exception e2;
        try {
            URLConnection openConnection = url.openConnection();
            try {
                if (openConnection instanceof HttpURLConnection) {
                    return (HttpURLConnection) openConnection;
                }
                throw new a();
            } catch (Exception e3) {
                e2 = e3;
                if (0 != 0 && (0 instanceof HttpURLConnection)) {
                    af.a(null);
                }
                throw e2;
            }
        } catch (Exception e4) {
            e2 = e4;
        }
    }

    public h a(Context context, String str, ak akVar, boolean z) {
        DataOutputStream dataOutputStream;
        HttpURLConnection httpURLConnection;
        byte[] byteArray;
        try {
            httpURLConnection = a(new URL(str));
            try {
                byteArray = akVar.build().toByteArray();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/octet-stream");
                httpURLConnection.setRequestProperty("Content-Length", Integer.toString(byteArray.length));
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setInstanceFollowRedirects(true);
                httpURLConnection.setAllowUserInteraction(false);
                httpURLConnection.setConnectTimeout(10000);
                httpURLConnection.setReadTimeout(30000);
                dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            } catch (Exception e2) {
                e = e2;
                dataOutputStream = null;
                try {
                    throw new IOException(e.getMessage());
                } catch (Throwable th) {
                    th = th;
                    af.a(dataOutputStream);
                    af.a(null);
                    af.a(httpURLConnection);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = null;
                af.a(dataOutputStream);
                af.a(null);
                af.a(httpURLConnection);
                throw th;
            }
            try {
                dataOutputStream.write(byteArray);
                dataOutputStream.flush();
                int responseCode = httpURLConnection.getResponseCode();
                if (responseCode == 200 || responseCode == 204) {
                    InputStream inputStream = httpURLConnection.getInputStream();
                    h a2 = h.a(inputStream);
                    af.a(dataOutputStream);
                    af.a(inputStream);
                    af.a(httpURLConnection);
                    return a2;
                }
                throw new IOException("HTTP status " + responseCode);
            } catch (Exception e3) {
                e = e3;
                throw new IOException(e.getMessage());
            }
        } catch (Exception e4) {
            e = e4;
            dataOutputStream = null;
            httpURLConnection = null;
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            httpURLConnection = null;
            af.a(dataOutputStream);
            af.a(null);
            af.a(httpURLConnection);
            throw th;
        }
    }

    public static Bitmap a(Context context, String str) {
        InputStream inputStream = null;
        try {
            inputStream = new URL(str).openStream();
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            af.a(inputStream);
            return decodeStream;
        } catch (Exception e2) {
            throw new IOException(e2.getMessage());
        } catch (Throwable th) {
            af.a(inputStream);
            throw th;
        }
    }

    private static byte[] a(Context context, String str, byte[] bArr) {
        return a(context, str, bArr, (a) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0087 A[Catch:{ all -> 0x0088 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x012b A[SYNTHETIC, Splitter:B:49:0x012b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] a(android.content.Context r17, java.lang.String r18, byte[] r19, com.avast.android.generic.internet.a r20) {
        /*
            java.net.URL r1 = new java.net.URL
            r0 = r18
            r1.<init>(r0)
            r3 = 0
            r4 = 0
            r2 = 0
            java.net.HttpURLConnection r5 = a(r1)     // Catch:{ Exception -> 0x013e, all -> 0x0135 }
            java.lang.String r1 = "POST"
            r5.setRequestMethod(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            java.lang.String r1 = "Content-Type"
            java.lang.String r3 = "application/octet-stream"
            r5.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            java.lang.String r1 = "Content-Length"
            r0 = r19
            int r3 = r0.length     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            java.lang.String r3 = java.lang.Integer.toString(r3)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r5.setRequestProperty(r1, r3)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 0
            r5.setUseCaches(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 1
            r5.setDoInput(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 1
            r5.setDoOutput(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 1
            r5.setInstanceFollowRedirects(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 0
            r5.setAllowUserInteraction(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 10000(0x2710, float:1.4013E-41)
            r5.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r1 = 30000(0x7530, float:4.2039E-41)
            r5.setReadTimeout(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r0 = r19
            int r7 = r0.length     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            java.io.DataOutputStream r3 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0146, all -> 0x013a }
            if (r20 != 0) goto L_0x00a6
            r0 = r19
            r3.write(r0)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r3.flush()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
        L_0x005a:
            int r1 = r5.getResponseCode()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r4 = 200(0xc8, float:2.8E-43)
            if (r1 == r4) goto L_0x0066
            r4 = 204(0xcc, float:2.86E-43)
            if (r1 != r4) goto L_0x0112
        L_0x0066:
            java.io.InputStream r2 = r5.getInputStream()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r1.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
        L_0x0073:
            r6 = 0
            int r8 = r4.length     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            int r6 = r2.read(r4, r6, r8)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r8 = -1
            if (r6 == r8) goto L_0x00e3
            r8 = 0
            r1.write(r4, r8, r6)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            goto L_0x0073
        L_0x0081:
            r1 = move-exception
            r4 = r5
        L_0x0083:
            boolean r5 = r1 instanceof com.avast.android.generic.util.ai     // Catch:{ all -> 0x0088 }
            if (r5 == 0) goto L_0x012b
            throw r1     // Catch:{ all -> 0x0088 }
        L_0x0088:
            r1 = move-exception
            r5 = r4
        L_0x008a:
            r4 = 1
            java.io.Closeable[] r4 = new java.io.Closeable[r4]
            r6 = 0
            r4[r6] = r3
            com.avast.android.generic.util.af.a(r4)
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]
            r4 = 0
            r3[r4] = r2
            com.avast.android.generic.util.af.a(r3)
            r2 = 1
            java.net.HttpURLConnection[] r2 = new java.net.HttpURLConnection[r2]
            r3 = 0
            r2[r3] = r5
            com.avast.android.generic.util.af.a(r2)
            throw r1
        L_0x00a6:
            r6 = 0
            r4 = 51200(0xc800, float:7.1746E-41)
            r1 = 0
            java.lang.String r8 = ""
            long r9 = (long) r7     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r11 = 0
            r0 = r20
            r0.a(r8, r9, r11)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
        L_0x00b4:
            if (r6 >= r7) goto L_0x00de
            if (r1 != 0) goto L_0x00de
            int r8 = r6 + r4
            if (r8 < r7) goto L_0x00bf
            int r4 = r7 - r6
            r1 = 1
        L_0x00bf:
            r0 = r19
            r3.write(r0, r6, r4)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r3.flush()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            int r6 = r6 + r4
            int r8 = r6 * 100
            int r8 = r8 / r7
            java.lang.String r9 = ""
            long r10 = (long) r7     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            double r12 = (double) r8     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r14 = 4606281698874543309(0x3feccccccccccccd, double:0.9)
            double r12 = r12 * r14
            int r8 = (int) r12     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r0 = r20
            r0.a(r9, r10, r8)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            goto L_0x00b4
        L_0x00dc:
            r1 = move-exception
            goto L_0x008a
        L_0x00de:
            r3.flush()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            goto L_0x005a
        L_0x00e3:
            r1.flush()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            if (r20 == 0) goto L_0x00f2
            java.lang.String r4 = ""
            long r6 = (long) r7     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r8 = 100
            r0 = r20
            r0.a(r4, r6, r8)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
        L_0x00f2:
            byte[] r1 = r1.toByteArray()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r4 = 1
            java.io.Closeable[] r4 = new java.io.Closeable[r4]
            r6 = 0
            r4[r6] = r3
            com.avast.android.generic.util.af.a(r4)
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]
            r4 = 0
            r3[r4] = r2
            com.avast.android.generic.util.af.a(r3)
            r2 = 1
            java.net.HttpURLConnection[] r2 = new java.net.HttpURLConnection[r2]
            r3 = 0
            r2[r3] = r5
            com.avast.android.generic.util.af.a(r2)
            return r1
        L_0x0112:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r6.<init>()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            java.lang.String r7 = "HTTP status "
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            java.lang.StringBuilder r1 = r6.append(r1)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
            throw r4     // Catch:{ Exception -> 0x0081, all -> 0x00dc }
        L_0x012b:
            java.io.IOException r5 = new java.io.IOException     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0088 }
            r5.<init>(r1)     // Catch:{ all -> 0x0088 }
            throw r5     // Catch:{ all -> 0x0088 }
        L_0x0135:
            r1 = move-exception
            r5 = r3
            r3 = r4
            goto L_0x008a
        L_0x013a:
            r1 = move-exception
            r3 = r4
            goto L_0x008a
        L_0x013e:
            r1 = move-exception
            r16 = r4
            r4 = r3
            r3 = r16
            goto L_0x0083
        L_0x0146:
            r1 = move-exception
            r3 = r4
            r4 = r5
            goto L_0x0083
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.b.a(android.content.Context, java.lang.String, byte[], com.avast.android.generic.internet.a):byte[]");
    }

    protected static byte[] a(Context context, ab abVar, String str, byte[] bArr) {
        return a(context, abVar, str, bArr, (a) null);
    }

    protected static byte[] a(Context context, ab abVar, String str, byte[] bArr, a aVar) {
        z.a("AvastComms", context, "HTTP interface starts downloading billing data");
        try {
            String str2 = "https://ff-billing.avast.com" + str + "?v=" + 2;
            z.a("AvastComms", context, "HTTP interface billing data download from " + str2 + " (input file size is " + bArr.length + " bytes)...");
            byte[] a2 = a(context, str2, bArr, aVar);
            z.a("AvastComms", context, "HTTP interface billing download of " + a2.length + " bytes of data succeeded");
            return a2;
        } catch (IOException e2) {
            z.a("AvastComms", context, "HTTP interface billing download of data IO Exception", e2);
            throw e2;
        } catch (Exception e3) {
            z.a("AvastComms", context, "HTTP interface billing download of data general exception", e3);
            throw e3;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c1  */
    @android.annotation.SuppressLint({"NewApi"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.avast.b.a.a.h a(android.content.Context r11, java.lang.String r12, com.avast.b.a.a.ak r13, com.avast.android.generic.internet.k r14, boolean r15) {
        /*
            r10 = this;
            r0 = 0
            org.apache.http.client.methods.HttpPost r3 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x0119 }
            r3.<init>(r12)     // Catch:{ Exception -> 0x0119 }
            c.a.a.a.a.h r2 = new c.a.a.a.a.h     // Catch:{ Exception -> 0x00b3 }
            c.a.a.a.a.e r1 = c.a.a.a.a.e.BROWSER_COMPATIBLE     // Catch:{ Exception -> 0x00b3 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x00b3 }
            c.a.a.a.a.a.b r4 = new c.a.a.a.a.a.b     // Catch:{ Exception -> 0x00b3 }
            com.avast.b.a.a.aj r1 = r13.build()     // Catch:{ Exception -> 0x00b3 }
            byte[] r1 = r1.toByteArray()     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r5 = "application/octet-stream"
            r4.<init>(r1, r5)     // Catch:{ Exception -> 0x00b3 }
            java.io.File r1 = r14.b()     // Catch:{ Exception -> 0x00b3 }
            if (r1 == 0) goto L_0x009c
            c.a.a.a.a.a.e r1 = new c.a.a.a.a.a.e     // Catch:{ Exception -> 0x00b3 }
            java.io.File r5 = r14.b()     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r6 = r14.a()     // Catch:{ Exception -> 0x00b3 }
            r1.<init>(r5, r6)     // Catch:{ Exception -> 0x00b3 }
        L_0x002f:
            java.lang.String r5 = "metadata"
            r2.a(r5, r4)     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r4 = "file"
            r2.a(r4, r1)     // Catch:{ Exception -> 0x00b3 }
            r3.setEntity(r2)     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r1 = "avast! Anti-Theft"
            android.a.a.a r1 = android.a.a.a.a(r1)     // Catch:{ Exception -> 0x00b3 }
            org.apache.http.conn.ClientConnectionManager r2 = r1.getConnectionManager()     // Catch:{ Exception -> 0x00d2 }
            org.apache.http.conn.scheme.SchemeRegistry r2 = r2.getSchemeRegistry()     // Catch:{ Exception -> 0x00d2 }
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00d2 }
            r5 = 8
            if (r4 >= r5) goto L_0x00ce
            java.lang.String r4 = "https"
            org.apache.http.conn.scheme.Scheme r4 = r2.get(r4)     // Catch:{ Exception -> 0x00d2 }
            if (r4 != 0) goto L_0x0068
            org.apache.http.conn.scheme.Scheme r4 = new org.apache.http.conn.scheme.Scheme     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r5 = "https"
            org.apache.http.conn.ssl.SSLSocketFactory r6 = org.apache.http.conn.ssl.SSLSocketFactory.getSocketFactory()     // Catch:{ Exception -> 0x00d2 }
            r7 = 443(0x1bb, float:6.21E-43)
            r4.<init>(r5, r6, r7)     // Catch:{ Exception -> 0x00d2 }
            r2.register(r4)     // Catch:{ Exception -> 0x00d2 }
        L_0x0068:
            org.apache.http.HttpResponse r4 = r1.execute(r3)     // Catch:{ Exception -> 0x00d2 }
            org.apache.http.StatusLine r2 = r4.getStatusLine()     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x0113 }
            r5 = 200(0xc8, float:2.8E-43)
            if (r2 == r5) goto L_0x0084
            org.apache.http.StatusLine r2 = r4.getStatusLine()     // Catch:{ Exception -> 0x0113 }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x0113 }
            r5 = 204(0xcc, float:2.86E-43)
            if (r2 != r5) goto L_0x00ee
        L_0x0084:
            org.apache.http.HttpEntity r2 = r4.getEntity()     // Catch:{ Exception -> 0x00da }
            byte[] r2 = org.apache.http.util.EntityUtils.toByteArray(r2)     // Catch:{ Exception -> 0x00da }
            com.avast.b.a.a.h r2 = com.avast.b.a.a.h.a(r2)     // Catch:{ Exception -> 0x00da }
            a(r3)     // Catch:{ Exception -> 0x00da }
            a(r4)     // Catch:{ Exception -> 0x00da }
            if (r1 == 0) goto L_0x009b
            r1.a()     // Catch:{ Exception -> 0x00da }
        L_0x009b:
            return r2
        L_0x009c:
            byte[] r1 = r14.c()     // Catch:{ Exception -> 0x00b3 }
            if (r1 == 0) goto L_0x0122
            c.a.a.a.a.a.b r1 = new c.a.a.a.a.a.b     // Catch:{ Exception -> 0x00b3 }
            byte[] r5 = r14.c()     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r6 = r14.a()     // Catch:{ Exception -> 0x00b3 }
            java.lang.String r7 = "binaryFile"
            r1.<init>(r5, r6, r7)     // Catch:{ Exception -> 0x00b3 }
            goto L_0x002f
        L_0x00b3:
            r1 = move-exception
            r2 = r0
            r8 = r3
            r3 = r0
            r0 = r1
            r1 = r8
        L_0x00b9:
            a(r1)
            a(r2)
            if (r3 == 0) goto L_0x00c4
            r3.a()
        L_0x00c4:
            java.io.IOException r1 = new java.io.IOException
            java.lang.String r0 = r0.getMessage()
            r1.<init>(r0)
            throw r1
        L_0x00ce:
            com.avast.android.generic.internet.c.a(r11, r2)     // Catch:{ Exception -> 0x00d2 }
            goto L_0x0068
        L_0x00d2:
            r2 = move-exception
            r8 = r2
            r2 = r0
            r0 = r8
            r9 = r3
            r3 = r1
            r1 = r9
            goto L_0x00b9
        L_0x00da:
            r2 = move-exception
            a(r3)     // Catch:{ Exception -> 0x0113 }
            a(r4)     // Catch:{ Exception -> 0x0113 }
            if (r1 == 0) goto L_0x0120
            r1.a()     // Catch:{ Exception -> 0x0113 }
        L_0x00e6:
            throw r2     // Catch:{ Exception -> 0x00e7 }
        L_0x00e7:
            r1 = move-exception
            r2 = r4
            r8 = r3
            r3 = r0
            r0 = r1
            r1 = r8
            goto L_0x00b9
        L_0x00ee:
            a(r3)     // Catch:{ Exception -> 0x0113 }
            a(r4)     // Catch:{ Exception -> 0x0113 }
            if (r1 == 0) goto L_0x00fa
            r1.a()     // Catch:{ Exception -> 0x0113 }
            r1 = r0
        L_0x00fa:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            r2.<init>()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r5 = "HTTP status "
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0113 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0113 }
            throw r0     // Catch:{ Exception -> 0x0113 }
        L_0x0113:
            r0 = move-exception
            r2 = r4
            r8 = r3
            r3 = r1
            r1 = r8
            goto L_0x00b9
        L_0x0119:
            r1 = move-exception
            r2 = r0
            r3 = r0
            r8 = r1
            r1 = r0
            r0 = r8
            goto L_0x00b9
        L_0x0120:
            r0 = r1
            goto L_0x00e6
        L_0x0122:
            r1 = r0
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.b.a(android.content.Context, java.lang.String, com.avast.b.a.a.ak, com.avast.android.generic.internet.k, boolean):com.avast.b.a.a.h");
    }

    protected static void a(HttpPost httpPost) {
        if (httpPost != null && httpPost.getEntity() != null) {
            a(httpPost.getEntity());
        }
    }

    protected static void a(HttpResponse httpResponse) {
        if (httpResponse != null && httpResponse.getEntity() != null) {
            a(httpResponse.getEntity());
        }
    }

    protected static void a(HttpEntity httpEntity) {
        if (httpEntity != null) {
            try {
                httpEntity.consumeContent();
            } catch (Exception e2) {
            }
        }
    }

    public static boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }

    public static ak a(ab abVar, String str, String str2, m mVar, String str3) {
        return a(abVar, str3).a(s.l().a(str).a(mVar).b(str2).build());
    }

    public static ak a(ab abVar, String str, String str2, String str3, boolean z) {
        t a2 = s.l().a(str).a(z).b(str2).a(m.NONE);
        if (str3 != null) {
            a2.c(str3);
        }
        return a(abVar, (String) null).a(a2.build());
    }

    public static ak a(ab abVar, String str) {
        int i = 100;
        ak aj = aj.aj();
        String y = abVar.y();
        if (!TextUtils.isEmpty(str)) {
            aj.c(str);
        } else if (!TextUtils.isEmpty(y)) {
            aj.c(y);
        } else {
            aj.c("");
        }
        String w = abVar.w();
        if (w == null) {
            w = "";
        }
        if (abVar.L() != null) {
            Intent registerReceiver = abVar.L().registerReceiver(null, f746a);
            if (registerReceiver != null) {
                int intExtra = registerReceiver.getIntExtra("level", -1);
                int intExtra2 = registerReceiver.getIntExtra("scale", -1);
                if (!(intExtra == -1 || intExtra2 == -1)) {
                    int i2 = (int) ((((float) intExtra) / ((float) intExtra2)) * 100.0f);
                    if (i2 <= 100) {
                        i = i2 < 0 ? 0 : i2;
                    }
                    if (i != f747b) {
                        f748c = i;
                        aj.d(i);
                    }
                }
            }
            aj.k(aa.i(abVar.L()));
        }
        String l = com.avast.android.generic.g.b.a.l(abVar.L());
        if (!TextUtils.isEmpty(l)) {
            z.a("AvastGeneric", "Current local IP address: " + l);
            if (!l.equals(d)) {
                e = l;
                aj.m(l);
            }
        }
        aj.a("ANDROID").d(Locale.getDefault().getLanguage()).e(w).g(String.valueOf(Build.VERSION.SDK_INT)).a(1);
        return aj;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    protected static boolean a(Context context, ak akVar, long j) {
        boolean z;
        ab abVar = (ab) com.avast.android.generic.aa.a(context, ae.class);
        ab abVar2 = (ab) com.avast.android.generic.aa.a(context, ad.class);
        if (abVar.c("c2dmri", j)) {
            String D = abVar.D();
            if (D == null) {
                D = "";
            }
            akVar.h(D);
            z = true;
        } else {
            z = false;
        }
        if (abVar2.c("accountSmsGateway", j)) {
            boolean z2 = !TextUtils.isEmpty(abVar2.G());
            akVar.a(z2);
            if (z2) {
                akVar.i(abVar2.G());
            }
            z = true;
        }
        if (abVar2.c("encaccesscode", j)) {
            String f = abVar2.f();
            if (TextUtils.isEmpty(f) || f.equals(ab.f321b)) {
                akVar.b(false);
            } else {
                akVar.b(true);
            }
            z = true;
        }
        String i = com.avast.android.generic.g.b.a.i(context);
        if (i != null && ((abVar.c().contains("syncImeiCache") && abVar.c("syncImeiCache", j)) || !i.equals(abVar.b("syncImeiCache", "")))) {
            akVar.f(i);
            abVar.a("syncImeiCache", i);
            abVar.a(j - 1);
            z = true;
        }
        if (abVar2.c("guid", j) || !abVar2.b("settingsLogicChange5026", false)) {
            akVar.n(abVar2.q());
        }
        int e2 = com.avast.android.generic.g.b.a.e(context);
        if ((abVar.c().contains("syncMccCache") && abVar.c("syncMccCache", j)) || (e2 > 0 && e2 != abVar.b("syncMccCache", 0))) {
            akVar.b(e2);
            abVar.a("syncMccCache", e2);
            abVar.a(j - 1);
            z = true;
        }
        int g = com.avast.android.generic.g.b.a.g(context);
        if ((!abVar.c().contains("syncMocCache") || !abVar.c("syncMocCache", j)) && (g <= 0 || g == abVar.b("syncMocCache", 0))) {
            return z;
        }
        akVar.c(g);
        abVar.a("syncMocCache", g);
        abVar.a(j - 1);
        return true;
    }

    public static c a(Context context, ab abVar, String str, String str2) {
        z.a("AvastComms", context, "Starting connection check");
        try {
            String str3 = ("com.avast.android.backup".equals(context.getPackageName()) ? "https://ff-backup.avast.com" : "https://ff-at.avast.com") + "/rest/connectionCheck?v=" + 1;
            z.a("AvastComms", context, "Connection check to " + str3 + "...");
            byte[] a2 = a(context, str3, v.h().b(str).a(str2).a(1).build().toByteArray());
            z.a("AvastComms", context, "Connection check download " + a2.length + " bytes of data succeeded");
            e a3 = e.a(a2);
            if (a3.j()) {
                abVar.i(a3.k());
                abVar.b();
            }
            if (!a3.b()) {
                return c.INVALID_RESPONSE;
            }
            if (a3.c() != c.NONE) {
                return a3.c();
            }
            if (a3.f() && a3.g()) {
                return c.DEVICE_DEPRECATED;
            }
            if (a3.h() && !a3.i()) {
                return c.DEVICE_NOT_ACTIVATED;
            }
            if (!a3.d() || a3.e() <= av.a(context).a()) {
                return c.NONE;
            }
            return c.CLIENT_TOO_OLD;
        } catch (IOException e2) {
            z.a("AvastComms", context, "Download of connection check data IO Exception", e2);
            throw e2;
        } catch (Exception e3) {
            z.a("AvastComms", context, "Download of connection check data general exception", e3);
            throw e3;
        }
    }

    public static void a() {
        f747b = f748c;
        d = e;
    }

    public static com.avast.c.a.a.t a(Context context, ab abVar, String str) {
        d r = com.avast.c.a.a.c.r();
        r.a(com.avast.android.generic.g.b.a.h(context));
        String b2 = com.avast.android.generic.g.b.a.b(context);
        if (!TextUtils.isEmpty(b2)) {
            r.a(b2);
        }
        if (!TextUtils.isEmpty(str)) {
            r.b(str);
        }
        Locale locale = Locale.getDefault();
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            r.c(locale.getLanguage());
        }
        r.b(com.avast.android.generic.g.b.a.f(context));
        r.a(abVar.W());
        r.c(av.a(context).a());
        return com.avast.c.a.a.t.a(a(context, abVar, "/rest/licensing/listOffers", r.build().toByteArray()));
    }

    public static as a(Context context, ab abVar, ap apVar) {
        return as.a(a(context, abVar, "/rest/licensing/getWebPurchaseRedirectUrl", apVar.toByteArray()));
    }

    public static com.avast.c.a.a.ad a(Context context, ab abVar, com.avast.c.a.a.aa aaVar) {
        return com.avast.c.a.a.ad.a(a(context, abVar, "/rest/licensing/manageSubscription", aaVar.toByteArray()));
    }
}
