package com.avast.android.generic.app.settings;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: RecoveryNumberDescriptionDialog */
class d implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f501a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ boolean f502b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ RecoveryNumberDescriptionDialog f503c;

    d(RecoveryNumberDescriptionDialog recoveryNumberDescriptionDialog, AlertDialog alertDialog, boolean z) {
        this.f503c = recoveryNumberDescriptionDialog;
        this.f501a = alertDialog;
        this.f502b = z;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f501a.getButton(-1).setOnClickListener(new e(this));
    }
}
