package com.avast.android.generic.app.subscription;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.avast.android.generic.ui.BaseActivity;
import com.avast.android.generic.ui.BaseSinglePaneActivity;

public class WebPurchaseActivity extends BaseSinglePaneActivity {
    public static void call(Context context) {
        ((BaseActivity) context).a(WebPurchaseActivity.class);
    }

    /* access modifiers changed from: protected */
    public Fragment a() {
        return new WebPurchaseFragment();
    }

    /* access modifiers changed from: protected */
    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        g().b();
    }
}
