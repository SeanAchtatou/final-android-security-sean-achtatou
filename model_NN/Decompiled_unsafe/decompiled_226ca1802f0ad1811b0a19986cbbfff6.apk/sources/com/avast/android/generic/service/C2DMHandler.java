package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.z;

public class C2DMHandler extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        if (intent.getAction().equals("com.avast.android.generic.action.C2DM_ENABLE_SUITE")) {
            z.b(applicationContext, intent.getStringExtra("sourcePackage"), "C2DM enable command");
            aw.a(applicationContext);
            Intent intent2 = new Intent();
            intent2.setAction(intent.getAction());
            ae.a(applicationContext, intent2, applicationContext.getPackageName());
            abortBroadcast();
        } else if (intent.getAction().equals("com.avast.android.generic.action.C2DM_DISABLE_SUITE")) {
            z.b(applicationContext, intent.getStringExtra("sourcePackage"), "C2DM disable command");
            aw.a(applicationContext);
            Intent intent3 = new Intent();
            intent3.setAction(intent.getAction());
            ae.a(applicationContext, intent3, applicationContext.getPackageName());
            abortBroadcast();
        }
    }
}
