package com.avast.android.generic.internet.c.a;

import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public enum q implements Internal.EnumLite {
    OK(0, 1),
    ID_ERROR(1, 2),
    INVALID_CREDENTIALS(2, 3),
    AT_ERROR(3, 4),
    PAIRING_ERROR(4, 5),
    OTHER_ERROR(5, 6),
    ASK_AGAIN_LATER(6, 7);
    
    private static Internal.EnumLiteMap<q> h = new r();
    private final int i;

    public final int getNumber() {
        return this.i;
    }

    public static q a(int i2) {
        switch (i2) {
            case 1:
                return OK;
            case 2:
                return ID_ERROR;
            case 3:
                return INVALID_CREDENTIALS;
            case 4:
                return AT_ERROR;
            case 5:
                return PAIRING_ERROR;
            case 6:
                return OTHER_ERROR;
            case 7:
                return ASK_AGAIN_LATER;
            default:
                return null;
        }
    }

    private q(int i2, int i3) {
        this.i = i3;
    }
}
