package com.avast.android.generic.app.subscription;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.text.TextUtils;
import com.avast.android.generic.a;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.util.w;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;
import com.avast.c.a.a.aq;
import com.avast.c.a.a.as;
import com.avast.c.a.a.i;

/* compiled from: SubscriptionFragment */
class o extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aq f564a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f565b;

    /* renamed from: c  reason: collision with root package name */
    private Context f566c;
    private i d = null;
    private Exception e = null;
    private String f = null;

    o(SubscriptionFragment subscriptionFragment, aq aqVar) {
        this.f565b = subscriptionFragment;
        this.f564a = aqVar;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        if (this.f565b.isAdded()) {
            this.f566c = this.f565b.getActivity();
            this.f565b.q();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (!this.f565b.isAdded()) {
            return false;
        }
        try {
            as a2 = b.a(this.f566c, (ab) aa.a(this.f566c, ab.class), this.f564a.build());
            if (a2.d()) {
                this.d = a2.e();
                return false;
            } else if (!a2.b() || TextUtils.isEmpty(a2.c())) {
                this.d = i.GV_GENERIC;
                return false;
            } else {
                this.f = a2.c();
                return true;
            }
        } catch (Exception e2) {
            z.a("AvastGenericLic", "Could not get redirect url", e2);
            this.d = i.GV_GENERIC;
            this.e = e2;
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        String string;
        if (this.f565b.isAdded()) {
            this.f565b.r();
            if (!bool.booleanValue()) {
                switch (t.f576c[this.d.ordinal()]) {
                    case 1:
                        a.a(this.f566c, this.f565b.getString(x.msg_home_error_restoring_transactions_no_google_account));
                        return;
                    case 2:
                        if (this.e != null) {
                            a.a(this.f566c, this.f565b.getString(x.l_cannot_query_billing_error, w.a(this.f566c, this.e)));
                            return;
                        }
                        this.f565b.t.g();
                        this.f565b.o();
                        a.a(this.f566c, this.f565b.getString(x.l_cannot_query_billing_identities_invalid));
                        return;
                    default:
                        Context context = this.f566c;
                        if (this.e != null) {
                            string = this.f565b.getString(x.l_cannot_query_billing_error, w.a(this.f566c, this.e));
                        } else {
                            string = this.f565b.getString(x.l_cannot_query_billing_error_unknown);
                        }
                        a.a(context, string);
                        return;
                }
            } else {
                try {
                    Intent intent = new Intent(this.f565b.getActivity(), WebPurchaseActivity.class);
                    intent.putExtra("paymentProviderUrl", this.f);
                    this.f565b.startActivityForResult(intent, 6655);
                } catch (Exception e2) {
                    z.a("AvastGenericLic", "Error opening WebView with redirect url: " + this.f, e2);
                }
            }
        }
    }
}
