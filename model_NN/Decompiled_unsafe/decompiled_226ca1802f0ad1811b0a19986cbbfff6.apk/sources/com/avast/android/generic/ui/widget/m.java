package com.avast.android.generic.ui.widget;

import android.text.InputFilter;
import android.text.Spanned;

/* compiled from: PasswordTextView */
class m implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    String f1151a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ PasswordTextView f1152b;

    public m(PasswordTextView passwordTextView, l lVar) {
        this.f1152b = passwordTextView;
        switch (lVar) {
            case DIGITS:
                this.f1151a = "0-9";
                return;
            case LETTERS:
                this.f1151a = "a-zA-Z";
                return;
            case DIGITS_AND_LETTERS:
                this.f1151a = "0-9a-zA-Z";
                return;
            default:
                return;
        }
    }

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        if (spanned.length() != 0 || this.f1152b.d.f1155a.length() <= 0) {
            return charSequence.toString().replaceAll("[^" + this.f1151a + "]", "");
        }
        return charSequence.toString().replaceAll("[^" + this.f1151a + "\\*]", "");
    }
}
