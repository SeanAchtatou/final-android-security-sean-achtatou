package com.avast.android.generic.util.ga;

import android.content.Context;

/* compiled from: MultiToolHelper */
public class d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1235a = false;

    /* renamed from: b  reason: collision with root package name */
    private boolean f1236b = false;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1237c = false;
    private boolean d = false;
    private boolean e = false;

    public d(Context context) {
        String packageName;
        if (context != null && (packageName = context.getPackageName()) != null) {
            if (packageName.equals("com.avast.android.mobilesecurity")) {
                this.f1235a = true;
            } else if (packageName.equals("com.avast.android.at_play") || packageName.equals("com.avast.android.antitheft")) {
                this.f1236b = true;
            } else if (packageName.equals("com.avast.android.backup")) {
                this.f1237c = true;
            } else if (packageName.equals("com.avast.android.at_client")) {
                this.d = true;
            } else if (packageName.equals("com.avast.android.antitheft_setup")) {
                this.e = true;
            }
        }
    }

    public String a(String str) {
        if (str == null) {
            return null;
        }
        if (this.f1236b) {
            return "/antiTheft/" + str;
        }
        if (this.f1237c) {
            return "/backup/" + str;
        }
        if (this.f1235a) {
            return "/ms/" + str;
        }
        if (this.d) {
            return "/antiTheftClient/" + str;
        }
        if (this.e) {
            return "/antiTheftSetup/" + str;
        }
        return "/unknown/" + str;
    }

    public boolean a() {
        return this.f1236b;
    }

    public boolean b() {
        return this.f1237c;
    }

    public boolean c() {
        return this.f1235a;
    }

    public boolean d() {
        return !c();
    }
}
