package com.avast.android.generic.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* compiled from: ByteUtils */
public class l {
    public static byte[] a(String str) {
        ByteArrayOutputStream byteArrayOutputStream;
        Throwable th;
        byte[] bArr = null;
        if (str != null) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    int length = str.length();
                    for (int i = 0; i < length; i += 2) {
                        if (i + 1 < length) {
                            byteArrayOutputStream.write((byte) ((Character.digit(str.charAt(i), 16) << 4) + Character.digit(str.charAt(i + 1), 16)));
                        } else {
                            byteArrayOutputStream.write((byte) (Character.digit(str.charAt(i), 16) << 4));
                        }
                    }
                    bArr = byteArrayOutputStream.toByteArray();
                    af.a(byteArrayOutputStream);
                } catch (Throwable th2) {
                    th = th2;
                    af.a(byteArrayOutputStream);
                    throw th;
                }
            } catch (Throwable th3) {
                Throwable th4 = th3;
                byteArrayOutputStream = null;
                th = th4;
                af.a(byteArrayOutputStream);
                throw th;
            }
        }
        return bArr;
    }

    public static byte[] a(byte[] bArr, String str) {
        ByteArrayOutputStream byteArrayOutputStream;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                byteArrayOutputStream.write(bArr);
                byteArrayOutputStream.write(a(str));
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                af.a(byteArrayOutputStream);
                return byteArray;
            } catch (IOException e) {
                e = e;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            byteArrayOutputStream = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            byteArrayOutputStream = null;
            af.a(byteArrayOutputStream);
            throw th;
        }
    }

    public static String a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        String str = "";
        for (int i = 0; i < bArr.length; i++) {
            str = str + Integer.toString((bArr[i] & 255) + 256, 16).substring(1);
        }
        return str;
    }
}
