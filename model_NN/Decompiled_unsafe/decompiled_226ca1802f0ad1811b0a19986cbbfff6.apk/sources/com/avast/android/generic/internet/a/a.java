package com.avast.android.generic.internet.a;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.h.c;
import com.avast.android.generic.h.d;
import com.avast.android.generic.h.e;
import com.avast.android.generic.h.g;
import com.avast.android.generic.h.i;
import com.avast.android.generic.licensing.PremiumHelper;
import com.avast.android.generic.util.t;
import com.google.protobuf.ByteString;
import java.util.Locale;

/* compiled from: CommunityIqManager */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f745a = {0, 7};

    public static c a(e eVar, Context context, Bundle bundle) {
        d a2 = a(context, bundle);
        if (eVar != null) {
            a2.a(eVar);
        }
        if (a(context)) {
            a(a2, context);
        }
        return a2.build();
    }

    private static d a(Context context, Bundle bundle) {
        long j;
        long j2;
        long j3;
        long j4;
        d P = c.P();
        if (context != null) {
            boolean a2 = PremiumHelper.a(context);
            boolean equals = "com.avast.android.vpn".equals(context.getPackageName());
            PackageManager packageManager = context.getPackageManager();
            if (packageManager != null) {
                long j5 = 0;
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo("com.avast.android.mobilesecurity", 0);
                    if (packageInfo != null) {
                        P.b(ByteString.copyFromUtf8(packageInfo.versionName));
                        if (equals) {
                            j4 = 4 | 0;
                        } else {
                            j4 = (a2 ? 2 : 1) | 0;
                        }
                    } else {
                        j4 = 0;
                    }
                    j5 = j4;
                } catch (PackageManager.NameNotFoundException e) {
                }
                try {
                    PackageInfo packageInfo2 = packageManager.getPackageInfo("com.avast.android.antitheft", 0);
                    if (packageInfo2 != null) {
                        P.c(ByteString.copyFromUtf8(packageInfo2.versionName));
                        if (equals) {
                            j3 = 2048 | j5;
                        } else {
                            j3 = (a2 ? 32 : 16) | j5;
                        }
                    } else {
                        j3 = j5;
                    }
                    j5 = j3;
                } catch (PackageManager.NameNotFoundException e2) {
                    try {
                        PackageInfo packageInfo3 = packageManager.getPackageInfo("com.avast.android.at_play", 0);
                        if (packageInfo3 != null) {
                            P.c(ByteString.copyFromUtf8(packageInfo3.versionName));
                            if (equals) {
                                j5 |= 32768;
                            } else {
                                j5 |= a2 ? 128 : 64;
                            }
                        }
                    } catch (PackageManager.NameNotFoundException e3) {
                    }
                }
                try {
                    PackageInfo packageInfo4 = packageManager.getPackageInfo("com.avast.android.backup", 0);
                    if (packageInfo4 != null) {
                        P.n(ByteString.copyFromUtf8(packageInfo4.versionName));
                        if (equals) {
                            j2 = 1024 | j5;
                        } else {
                            j2 = (a2 ? 512 : 256) | j5;
                        }
                    } else {
                        j2 = j5;
                    }
                    j5 = j2;
                } catch (PackageManager.NameNotFoundException e4) {
                }
                try {
                    PackageInfo packageInfo5 = packageManager.getPackageInfo("com.avast.android.vpn", 0);
                    if (packageInfo5 != null) {
                        P.o(ByteString.copyFromUtf8(packageInfo5.versionName));
                        if (equals) {
                            j = (a2 ? 8192 : 4096) | j5;
                        } else {
                            j = 16384 | j5;
                        }
                    } else {
                        j = j5;
                    }
                } catch (PackageManager.NameNotFoundException e5) {
                    j = j5;
                }
                P.a(j);
                t.c("Product bitmap: " + String.format("%#010x", Long.valueOf(j)));
            }
            P.a(ByteString.copyFromUtf8(((ab) aa.a(context, ab.class)).q()));
            P.l(ByteString.copyFromUtf8(((ab) aa.a(context, ab.class)).s()));
            P.m(ByteString.copyFromUtf8(((ab) aa.a(context, ab.class)).c(context)));
            String packageName = context.getPackageName();
            long j6 = 0;
            if ("com.avast.android.mobilesecurity".equals(packageName)) {
                j6 = a2 ? 2 : 1;
            } else if ("com.avast.android.antitheft".equals(packageName)) {
                j6 = a2 ? 32 : 16;
            } else if ("com.avast.android.at_play".equals(packageName)) {
                j6 = a2 ? 128 : 64;
            } else if ("com.avast.android.backup".equals(packageName)) {
                j6 = a2 ? 512 : 256;
            } else if (equals) {
                j6 = a2 ? 8192 : 4096;
            }
            if (j6 != 0) {
                P.b(j6);
            }
        }
        if (bundle != null) {
            String string = bundle.getString("vps_version");
            if (string != null) {
                P.d(ByteString.copyFromUtf8(string));
            }
            int i = bundle.getInt("update_result", -1);
            if (i != -1) {
                P.a(i.a(i));
            }
            int i2 = bundle.getInt("update_check_result", -1);
            if (i2 != -1) {
                P.a(g.a(i2));
            }
        }
        return P;
    }

    private static void a(d dVar, Context context) {
        TelephonyManager telephonyManager;
        dVar.e(ByteString.copyFromUtf8(Build.VERSION.RELEASE));
        dVar.f(ByteString.copyFromUtf8(Build.ID));
        dVar.i(ByteString.copyFromUtf8(Build.BRAND));
        dVar.k(ByteString.copyFromUtf8(Build.MANUFACTURER));
        dVar.j(ByteString.copyFromUtf8(Locale.getDefault().getDisplayName(Locale.US)));
        dVar.g(ByteString.copyFromUtf8(Build.MODEL));
        if (context != null && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null && telephonyManager.getSimOperatorName() != null) {
            dVar.h(ByteString.copyFromUtf8(telephonyManager.getSimOperatorName()));
        }
    }

    private static boolean a(Context context) {
        return ((ab) aa.a(context, ab.class)).o();
    }
}
