package com.avast.android.generic.app.account;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.avast.android.generic.app.wizard.WizardAccountActivity;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.x;

public class SignInPickerFragment extends TrackedMultiToolFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public bp<?, ?> f368a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public boolean f369b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public boolean f370c = true;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(t.fragment_signin_picker, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.f369b = getArguments().getBoolean("wizard", false);
        this.f370c = getArguments().getBoolean("queryPhoneNumber", true);
        ((Button) view.findViewById(r.b_sign_in_email)).setOnClickListener(new bj(this));
        ((Button) view.findViewById(r.b_sign_in_facebook)).setOnClickListener(new bk(this));
        ((Button) view.findViewById(r.y)).setOnClickListener(new bl(this));
    }

    public boolean a(int i, int i2, Intent intent) {
        if (!isAdded() || this.f368a == null || !this.f368a.b(i, i2, intent)) {
            return false;
        }
        this.f368a.a(i, i2, intent);
        return true;
    }

    /* access modifiers changed from: private */
    public bp<?, ?> a(bp<?, ?> bpVar) {
        return bpVar instanceof be ? bpVar : new bm(this, this);
    }

    /* access modifiers changed from: private */
    public r d() {
        return new bn(this, getActivity());
    }

    public int a() {
        return x.l_avast_account;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (isAdded()) {
            FragmentActivity activity = getActivity();
            if (activity instanceof WizardAccountActivity) {
                ((WizardAccountActivity) activity).a(z);
            } else {
                i();
            }
        }
    }

    public final String c() {
        return "account/signInPicker";
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        if (isAdded()) {
            super.onActivityResult(i, i2, intent);
            if (this.f368a != null) {
                this.f368a.a(i, i2, intent);
            }
        }
    }

    public void onStart() {
        super.onStart();
        if (this.f368a != null) {
            this.f368a.b();
        }
    }

    public void onStop() {
        super.onStop();
        if (this.f368a != null) {
            this.f368a.c();
        }
    }

    /* access modifiers changed from: private */
    public boolean e() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getActivity().getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* access modifiers changed from: private */
    public void f() {
        new AlertDialog.Builder(getActivity()).setTitle(getResources().getString(x.l_avast_account)).setMessage(getResources().getString(x.bu)).setCancelable(true).setPositiveButton(getResources().getString(x.bR), new bo(this)).show();
    }
}
