package com.avast.android.generic.app.passwordrecovery;

import android.app.AlertDialog;
import android.content.DialogInterface;

/* compiled from: PasswordRecoveryDialog */
class f implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AlertDialog f486a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ PasswordRecoveryDialog f487b;

    f(PasswordRecoveryDialog passwordRecoveryDialog, AlertDialog alertDialog) {
        this.f487b = passwordRecoveryDialog;
        this.f486a = alertDialog;
    }

    public void onShow(DialogInterface dialogInterface) {
        this.f486a.getButton(-1).setOnClickListener(new g(this));
    }
}
