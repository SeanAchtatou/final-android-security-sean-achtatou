package com.avast.android.generic.internet;

/* compiled from: HttpSender */
class f implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ HttpSender f819a;

    f(HttpSender httpSender) {
        this.f819a = httpSender;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, boolean):boolean
     arg types: [com.avast.android.generic.internet.HttpSender, int]
     candidates:
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, java.lang.Thread):java.lang.Thread
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, com.avast.android.generic.internet.h):void
      com.avast.android.generic.internet.HttpSender.a(com.avast.android.generic.internet.HttpSender, boolean):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00b7 A[LOOP:2: B:44:0x00b7->B:96:0x0169, LOOP_START, PHI: r0 
      PHI: (r0v38 'th' java.lang.Throwable) = (r0v37 'th' java.lang.Throwable), (r0v39 'th' java.lang.Throwable) binds: [B:107:0x00b7, B:96:0x0169] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:44:0x00b7] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r2 = 1
            r3 = 0
            r6 = 0
            r1 = r2
        L_0x0005:
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a
            boolean r0 = r0.f
            if (r0 != 0) goto L_0x00b9
            if (r1 == 0) goto L_0x00b9
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.internet.HttpSender r4 = r12.f819a
            com.avast.android.generic.service.AvastService r4 = r4.f741a
            java.lang.String r5 = "HTTP sender thread begin"
            com.avast.android.generic.util.z.a(r0, r4, r5)
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ Exception -> 0x00a9 }
            java.util.LinkedList r8 = r0.l     // Catch:{ Exception -> 0x00a9 }
            monitor-enter(r8)     // Catch:{ Exception -> 0x00a9 }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x00a6 }
            r0.<init>()     // Catch:{ all -> 0x00a6 }
            long r4 = r0.getTime()     // Catch:{ all -> 0x00a6 }
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ all -> 0x00a6 }
            java.util.LinkedList r0 = r0.l     // Catch:{ all -> 0x00a6 }
            int r0 = r0.size()     // Catch:{ all -> 0x00a6 }
            if (r0 <= 0) goto L_0x0172
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ all -> 0x00a6 }
            java.util.LinkedList r0 = r0.l     // Catch:{ all -> 0x00a6 }
            java.lang.Object r0 = r0.peek()     // Catch:{ all -> 0x00a6 }
            com.avast.android.generic.internet.h r0 = (com.avast.android.generic.internet.h) r0     // Catch:{ all -> 0x00a6 }
            long r9 = r0.h     // Catch:{ all -> 0x00a6 }
            int r0 = (r9 > r6 ? 1 : (r9 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x0172
            int r0 = (r9 > r4 ? 1 : (r9 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x0172
            long r4 = r9 - r4
        L_0x0050:
            monitor-exit(r8)     // Catch:{ all -> 0x00a6 }
            int r0 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r0 <= 0) goto L_0x007c
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.internet.HttpSender r8 = r12.f819a     // Catch:{ InterruptedException -> 0x016c }
            com.avast.android.generic.service.AvastService r8 = r8.f741a     // Catch:{ InterruptedException -> 0x016c }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x016c }
            r9.<init>()     // Catch:{ InterruptedException -> 0x016c }
            java.lang.String r10 = "HTTP sender thread sleeping for "
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ InterruptedException -> 0x016c }
            java.lang.StringBuilder r9 = r9.append(r4)     // Catch:{ InterruptedException -> 0x016c }
            java.lang.String r10 = " ms..."
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ InterruptedException -> 0x016c }
            java.lang.String r9 = r9.toString()     // Catch:{ InterruptedException -> 0x016c }
            com.avast.android.generic.util.z.a(r0, r8, r9)     // Catch:{ InterruptedException -> 0x016c }
            java.lang.Thread.sleep(r4)     // Catch:{ InterruptedException -> 0x016c }
        L_0x007c:
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ Exception -> 0x00a9 }
            r0.g()     // Catch:{ Exception -> 0x00a9 }
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ Exception -> 0x015e }
            java.util.LinkedList r4 = r0.l     // Catch:{ Exception -> 0x015e }
            monitor-enter(r4)     // Catch:{ Exception -> 0x015e }
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ all -> 0x00b5 }
            java.util.LinkedList r0 = r0.l     // Catch:{ all -> 0x00b5 }
            int r0 = r0.size()     // Catch:{ all -> 0x00b5 }
            if (r0 <= 0) goto L_0x016f
            r0 = r2
        L_0x0095:
            monitor-exit(r4)     // Catch:{ all -> 0x0163 }
        L_0x0096:
            java.lang.String r1 = "AvastComms"
            com.avast.android.generic.internet.HttpSender r4 = r12.f819a
            com.avast.android.generic.service.AvastService r4 = r4.f741a
            java.lang.String r5 = "HTTP sender thread end"
            com.avast.android.generic.util.z.a(r1, r4, r5)
            r1 = r0
            goto L_0x0005
        L_0x00a6:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00a6 }
            throw r0     // Catch:{ Exception -> 0x00a9 }
        L_0x00a9:
            r0 = move-exception
            r11 = r0
            r0 = r1
            r1 = r11
        L_0x00ad:
            java.lang.String r4 = "AvastComms"
            java.lang.String r5 = "Error in HTTP sender"
            com.avast.android.generic.util.z.a(r4, r5, r1)
            goto L_0x0096
        L_0x00b5:
            r0 = move-exception
            r1 = r3
        L_0x00b7:
            monitor-exit(r4)     // Catch:{ all -> 0x0169 }
            throw r0     // Catch:{ Exception -> 0x00a9 }
        L_0x00b9:
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a
            boolean r0 = r0.f
            if (r0 == 0) goto L_0x00df
            java.util.LinkedList r1 = new java.util.LinkedList     // Catch:{ Exception -> 0x0129 }
            r1.<init>()     // Catch:{ Exception -> 0x0129 }
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ Exception -> 0x0129 }
            com.avast.android.generic.service.AvastService r0 = r0.f741a     // Catch:{ Exception -> 0x0129 }
            boolean r2 = com.avast.android.generic.service.AvastService.b(r0)     // Catch:{ Exception -> 0x0129 }
            if (r2 != 0) goto L_0x00eb
            java.lang.String r0 = "AvastComms"
            com.avast.android.generic.internet.HttpSender r1 = r12.f819a     // Catch:{ Exception -> 0x0129 }
            com.avast.android.generic.service.AvastService r1 = r1.f741a     // Catch:{ Exception -> 0x0129 }
            java.lang.String r2 = "HTTP sender is not able to forward HTTP descriptors on stopping sender thread because SMS permission is missing"
            com.avast.android.generic.util.z.a(r0, r1, r2)     // Catch:{ Exception -> 0x0129 }
        L_0x00df:
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a
            boolean unused = r0.f = r3
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a
            r1 = 0
            java.lang.Thread unused = r0.e = r1
            return
        L_0x00eb:
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ Exception -> 0x0129 }
            java.util.LinkedList r4 = r0.l     // Catch:{ Exception -> 0x0129 }
            monitor-enter(r4)     // Catch:{ Exception -> 0x0129 }
            com.avast.android.generic.internet.HttpSender r0 = r12.f819a     // Catch:{ all -> 0x0126 }
            java.util.LinkedList r0 = r0.l     // Catch:{ all -> 0x0126 }
            java.util.Iterator r5 = r0.iterator()     // Catch:{ all -> 0x0126 }
        L_0x00fc:
            boolean r0 = r5.hasNext()     // Catch:{ all -> 0x0126 }
            if (r0 == 0) goto L_0x0132
            java.lang.Object r0 = r5.next()     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.internet.h r0 = (com.avast.android.generic.internet.h) r0     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.c.a r6 = r0.d     // Catch:{ all -> 0x0126 }
            if (r6 == 0) goto L_0x00fc
            com.avast.android.generic.c.a r6 = r0.d     // Catch:{ all -> 0x0126 }
            boolean r6 = r6.b()     // Catch:{ all -> 0x0126 }
            if (r6 != 0) goto L_0x00fc
            if (r2 == 0) goto L_0x00fc
            com.avast.android.generic.c.a r6 = r0.d     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.c.d r6 = r6.d()     // Catch:{ all -> 0x0126 }
            boolean r6 = r6.o()     // Catch:{ all -> 0x0126 }
            if (r6 == 0) goto L_0x00fc
            r1.add(r0)     // Catch:{ all -> 0x0126 }
            goto L_0x00fc
        L_0x0126:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0126 }
            throw r0     // Catch:{ Exception -> 0x0129 }
        L_0x0129:
            r0 = move-exception
            java.lang.String r1 = "AvastComms"
            java.lang.String r2 = "Error 2 in HTTP sender"
            com.avast.android.generic.util.z.a(r1, r2, r0)
            goto L_0x00df
        L_0x0132:
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0126 }
        L_0x0136:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0126 }
            if (r0 == 0) goto L_0x015c
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.internet.h r0 = (com.avast.android.generic.internet.h) r0     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.internet.HttpSender r2 = r12.f819a     // Catch:{ all -> 0x0126 }
            r2.c(r0)     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.internet.HttpSender r2 = r12.f819a     // Catch:{ all -> 0x0126 }
            java.util.LinkedList r2 = r2.l     // Catch:{ all -> 0x0126 }
            monitor-enter(r2)     // Catch:{ all -> 0x0126 }
            com.avast.android.generic.internet.HttpSender r5 = r12.f819a     // Catch:{ all -> 0x0159 }
            java.util.LinkedList r5 = r5.l     // Catch:{ all -> 0x0159 }
            r5.remove(r0)     // Catch:{ all -> 0x0159 }
            monitor-exit(r2)     // Catch:{ all -> 0x0159 }
            goto L_0x0136
        L_0x0159:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0159 }
            throw r0     // Catch:{ all -> 0x0126 }
        L_0x015c:
            monitor-exit(r4)     // Catch:{ all -> 0x0126 }
            goto L_0x00df
        L_0x015e:
            r0 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x00ad
        L_0x0163:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x00b7
        L_0x0169:
            r0 = move-exception
            goto L_0x00b7
        L_0x016c:
            r0 = move-exception
            goto L_0x007c
        L_0x016f:
            r0 = r3
            goto L_0x0095
        L_0x0172:
            r4 = r6
            goto L_0x0050
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.internet.f.run():void");
    }
}
