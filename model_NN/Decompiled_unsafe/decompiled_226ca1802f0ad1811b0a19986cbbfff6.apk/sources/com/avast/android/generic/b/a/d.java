package com.avast.android.generic.b.a;

import com.google.protobuf.Internal;

/* compiled from: BadNewsProto */
public enum d implements Internal.EnumLite {
    ERROR(0, 1),
    WARNING(1, 2),
    ASSERT(2, 3),
    UNCAUGHT_EXCEPTION(3, 4);
    
    private static Internal.EnumLiteMap<d> e = new e();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static d a(int i) {
        switch (i) {
            case 1:
                return ERROR;
            case 2:
                return WARNING;
            case 3:
                return ASSERT;
            case 4:
                return UNCAUGHT_EXCEPTION;
            default:
                return null;
        }
    }

    private d(int i, int i2) {
        this.f = i2;
    }
}
