package com.avast.android.generic.filebrowser;

import android.content.Intent;
import android.net.Uri;
import com.actionbarsherlock.view.ActionMode;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.avast.android.generic.r;
import com.avast.android.generic.u;
import com.avast.android.generic.x;
import java.util.ArrayList;
import java.util.List;

/* compiled from: AbstractFileBrowserFragment */
class b implements ActionMode.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AbstractFileBrowserFragment f675a;

    b(AbstractFileBrowserFragment abstractFileBrowserFragment) {
        this.f675a = abstractFileBrowserFragment;
    }

    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        if (this.f675a.f672b.equals("android.intent.action.GET_CONTENT")) {
            this.f675a.getSherlockActivity().getSupportMenuInflater().inflate(u.context_filebrowser_multiselect_pick, menu);
            actionMode.setTitle(x.l_filebrowser_multi_select);
            this.f675a.j.setVisibility(4);
        } else if (this.f675a.o != null) {
            return this.f675a.o.onCreateActionMode(actionMode, menu);
        }
        return true;
    }

    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        if (this.f675a.f672b.equals("android.intent.action.GET_CONTENT") || this.f675a.o == null) {
            return false;
        }
        return this.f675a.o.onPrepareActionMode(actionMode, menu);
    }

    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (this.f675a.f672b.equals("android.intent.action.GET_CONTENT")) {
            if (menuItem.getItemId() != r.menu_select) {
                return false;
            }
            a();
            return true;
        } else if (this.f675a.o != null) {
            return this.f675a.o.onActionItemClicked(actionMode, menuItem);
        } else {
            return false;
        }
    }

    public void onDestroyActionMode(ActionMode actionMode) {
        ActionMode unused = this.f675a.m = null;
        if (this.f675a.f672b.equals("android.intent.action.GET_CONTENT")) {
            this.f675a.e.clearChoices();
            this.f675a.e.requestLayout();
            this.f675a.j.setVisibility(0);
        } else if (this.f675a.o != null) {
            this.f675a.o.onDestroyActionMode(actionMode);
        }
    }

    private void a() {
        List<Integer> b2 = this.f675a.b();
        Intent intent = new Intent();
        if (b2.size() > 1) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : b2) {
                arrayList.add(this.f675a.l + this.f675a.f671a.a(intValue.intValue()).a());
            }
            intent.putStringArrayListExtra("result", arrayList);
        } else {
            intent.setData(Uri.parse(this.f675a.l + this.f675a.f671a.a(b2.get(0).intValue()).a()));
        }
        this.f675a.getActivity().setResult(-1, intent);
        this.f675a.getActivity().finish();
    }
}
