package com.avast.android.generic.licensing.c;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import com.android.vending.a.b;

/* compiled from: IabHelper */
class c implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f860a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ b f861b;

    c(b bVar, h hVar) {
        this.f861b = bVar;
        this.f860a = hVar;
    }

    public void onServiceDisconnected(ComponentName componentName) {
        this.f861b.c("Billing service disconnected.");
        this.f861b.g = null;
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f861b.c("Billing service connected.");
        this.f861b.g = b.a(iBinder);
        String packageName = this.f861b.f.getPackageName();
        try {
            this.f861b.c("Checking for in-app billing 3 support.");
            int a2 = this.f861b.g.a(3, packageName, "inapp");
            if (a2 != 0) {
                if (this.f860a != null) {
                    this.f860a.a(new i(a2, "Error checking for billing v3 support."));
                }
                this.f861b.f859c = false;
                return;
            }
            this.f861b.c("In-app billing version 3 supported for " + packageName);
            int a3 = this.f861b.g.a(3, packageName, "subs");
            if (a3 == 0) {
                this.f861b.c("Subscriptions AVAILABLE.");
                this.f861b.f859c = true;
            } else {
                this.f861b.c("Subscriptions NOT AVAILABLE. Response: " + a3);
            }
            this.f861b.f858b = true;
            if (this.f860a != null) {
                this.f860a.a(new i(0, "Setup successful."));
            }
        } catch (RemoteException e) {
            if (this.f860a != null) {
                this.f860a.a(new i(-1001, "RemoteException while setting up in-app billing."));
            }
            e.printStackTrace();
        }
    }
}
