package com.avast.android.generic.ui;

import android.view.View;
import com.avast.android.generic.app.passwordrecovery.PasswordRecoveryDialog;

/* compiled from: PasswordDialog */
class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordDialog f1005a;

    aa(PasswordDialog passwordDialog) {
        this.f1005a = passwordDialog;
    }

    public void onClick(View view) {
        new PasswordRecoveryDialog().show(this.f1005a.getFragmentManager(), "password_recovery_dialog");
    }
}
