package com.avast.android.generic.util.ga;

import android.os.Bundle;

public abstract class TrackedMultiToolDialogFragment extends TrackedDialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private d f1227a;

    public void onCreate(Bundle bundle) {
        this.f1227a = new d(getActivity());
        super.onCreate(bundle);
    }
}
