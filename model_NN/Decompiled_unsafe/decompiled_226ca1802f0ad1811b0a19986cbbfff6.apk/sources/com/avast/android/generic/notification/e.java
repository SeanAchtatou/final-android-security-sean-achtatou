package com.avast.android.generic.notification;

import android.database.ContentObserver;
import android.os.Handler;

/* compiled from: AvastNotificationFragment */
class e extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AvastNotificationFragment f926a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public e(AvastNotificationFragment avastNotificationFragment, Handler handler) {
        super(handler);
        this.f926a = avastNotificationFragment;
    }

    public boolean deliverSelfNotifications() {
        return false;
    }

    public void onChange(boolean z) {
        if (this.f926a.getActivity() != null && this.f926a.isAdded() && this.f926a.isVisible()) {
            this.f926a.j.restartLoader(10005, null, this.f926a);
        }
    }
}
