package com.avast.android.generic.licensing.c;

import org.json.JSONObject;

/* compiled from: SkuDetails */
public class n {

    /* renamed from: a  reason: collision with root package name */
    String f878a;

    /* renamed from: b  reason: collision with root package name */
    String f879b;

    /* renamed from: c  reason: collision with root package name */
    String f880c;
    String d;
    String e;
    String f;
    String g;

    public n(String str, String str2) {
        this.f878a = str;
        this.g = str2;
        JSONObject jSONObject = new JSONObject(this.g);
        this.f879b = jSONObject.optString("productId");
        this.f880c = jSONObject.optString("type");
        this.d = jSONObject.optString("price");
        this.e = jSONObject.optString("title");
        this.f = jSONObject.optString("description");
    }

    public n(String str, String str2, String str3, String str4, String str5, String str6) {
        this.f878a = str;
        this.g = null;
        this.f879b = str2;
        this.f880c = str3;
        this.d = str4;
        this.e = str5;
        this.f = str6;
    }

    public String a() {
        return this.f879b;
    }

    public String b() {
        return this.f880c;
    }

    public String c() {
        return this.d;
    }

    public String d() {
        return this.e;
    }

    public String e() {
        return this.f;
    }

    public String toString() {
        return "SkuDetails:" + this.g;
    }
}
