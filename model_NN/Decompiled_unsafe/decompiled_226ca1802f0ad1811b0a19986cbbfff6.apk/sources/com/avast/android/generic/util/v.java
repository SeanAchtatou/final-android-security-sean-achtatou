package com.avast.android.generic.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

/* compiled from: EncodingUtility */
public class v {
    public static String a(String str, boolean z, boolean z2) {
        if (str == null) {
            return null;
        }
        if (str.equals("")) {
            return "";
        }
        try {
            String replace = str.replace("\u0000", "");
            if (z) {
                replace = replace.trim();
                if (z2) {
                    while (replace.contains("  ")) {
                        replace = replace.replace("  ", " ");
                    }
                }
            }
            return URLEncoder.encode(replace, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return "NoEncoding";
        }
    }

    public static String b(String str, boolean z, boolean z2) {
        if (str == null) {
            return null;
        }
        if (str.equals("")) {
            return "";
        }
        try {
            String replace = URLDecoder.decode(str, "UTF-8").replace("\u0000", "");
            if (!z) {
                return replace;
            }
            String trim = replace.trim();
            if (!z2) {
                return trim;
            }
            while (trim.contains("  ")) {
                trim = trim.replace("  ", " ");
            }
            return trim;
        } catch (UnsupportedEncodingException e) {
            return "NoDecoding";
        }
    }
}
