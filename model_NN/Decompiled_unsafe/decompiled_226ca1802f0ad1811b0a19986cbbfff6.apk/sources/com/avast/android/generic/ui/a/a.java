package com.avast.android.generic.ui.a;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

/* compiled from: AdapterWrapper */
public class a extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private ListAdapter f1002a = null;

    /* renamed from: b  reason: collision with root package name */
    private Context f1003b;

    public a(Context context, ListAdapter listAdapter) {
        this.f1003b = context;
        this.f1002a = listAdapter;
        listAdapter.registerDataSetObserver(new b(this));
    }

    public Object getItem(int i) {
        return this.f1002a.getItem(i);
    }

    public int getCount() {
        return this.f1002a.getCount();
    }

    public int getViewTypeCount() {
        return this.f1002a.getViewTypeCount();
    }

    public int getItemViewType(int i) {
        return this.f1002a.getItemViewType(i);
    }

    public boolean areAllItemsEnabled() {
        return this.f1002a.areAllItemsEnabled();
    }

    public boolean isEnabled(int i) {
        return this.f1002a.isEnabled(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return this.f1002a.getView(i, view, viewGroup);
    }

    public long getItemId(int i) {
        return this.f1002a.getItemId(i);
    }
}
