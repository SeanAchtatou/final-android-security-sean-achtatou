package com.avast.android.generic.g.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.util.z;
import java.util.Iterator;

/* compiled from: SmsSender */
class g extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f690a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e, int, boolean):void
     arg types: [com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e, int, int]
     candidates:
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean, short):void
      com.avast.android.generic.g.a.b.a(java.lang.String, java.util.List<com.avast.android.generic.util.bc>, com.avast.android.generic.g.a.a, boolean):void
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.b, com.avast.android.generic.g.a.e, int, boolean):void */
    public void onReceive(Context context, Intent intent) {
        String stringExtra;
        e eVar;
        z.a("AvastComms", this.f690a.h, "SMS sender result on sending SMS is " + getResultCode());
        if (intent.hasExtra("smsId") && (stringExtra = intent.getStringExtra("smsId")) != null) {
            z.a("AvastComms", this.f690a.h, "SMS sender SMS intent has SMS ID " + stringExtra);
            synchronized (this.f690a.g) {
                Iterator it = this.f690a.g.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        eVar = null;
                        break;
                    }
                    eVar = (e) it.next();
                    if (eVar.f687b.compareTo(stringExtra) == 0) {
                        break;
                    }
                }
            }
            if (eVar == null) {
                z.a("AvastComms", this.f690a.h, "SMS sender did not find SMS");
                return;
            }
            switch (getResultCode()) {
                case -1:
                    this.f690a.a(eVar);
                    return;
                default:
                    this.f690a.a(eVar, getResultCode(), true);
                    return;
            }
        }
    }
}
