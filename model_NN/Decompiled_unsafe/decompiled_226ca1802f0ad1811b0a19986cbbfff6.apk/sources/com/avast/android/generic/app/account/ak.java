package com.avast.android.generic.app.account;

import android.widget.TextView;
import com.actionbarsherlock.R;
import com.avast.android.generic.ab;
import com.avast.android.generic.o;
import com.avast.android.generic.q;
import com.avast.android.generic.x;
import com.avast.b.a.a.c;

/* compiled from: ConnectionCheckFragment */
class ak implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ aj f388a;

    ak(aj ajVar) {
        this.f388a = ajVar;
    }

    public void run() {
        int color;
        if (this.f388a.s.isAdded()) {
            switch (this.f388a.r) {
                case 1:
                    if (!this.f388a.j) {
                        this.f388a.f385a = true;
                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_anti_theft_disabled);
                        this.f388a.s.f363b.setText(x.ca);
                        this.f388a.s.f363b.setOnClickListener(new al(this));
                        break;
                    }
                    break;
                case 2:
                    if (!this.f388a.l) {
                        if (!this.f388a.k) {
                            if (!this.f388a.m) {
                                if (!this.f388a.n) {
                                    if (this.f388a.o) {
                                        this.f388a.f385a = true;
                                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_secureline_too_old);
                                        this.f388a.s.f363b.setText(x.ca);
                                        this.f388a.s.f363b.setOnClickListener(new av(this));
                                        break;
                                    }
                                } else {
                                    this.f388a.f385a = true;
                                    this.f388a.d = this.f388a.s.getString(x.l_conn_check_backup_too_old);
                                    this.f388a.s.f363b.setText(x.ca);
                                    this.f388a.s.f363b.setOnClickListener(new au(this));
                                    break;
                                }
                            } else {
                                this.f388a.f385a = true;
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_at_too_old);
                                this.f388a.s.f363b.setText(x.ca);
                                this.f388a.s.f363b.setOnClickListener(new at(this));
                                break;
                            }
                        } else {
                            this.f388a.f385a = true;
                            this.f388a.d = this.f388a.s.getString(x.l_conn_check_ams_too_old);
                            this.f388a.s.f363b.setText(x.ca);
                            this.f388a.s.f363b.setOnClickListener(new as(this));
                            break;
                        }
                    } else {
                        this.f388a.f385a = true;
                        this.f388a.d = this.f388a.s.getString(x.msg_invalid_tool_combination);
                        this.f388a.s.f363b.setText(x.ca);
                        this.f388a.s.f363b.setOnClickListener(new ar(this));
                        break;
                    }
                    break;
                case 3:
                    if (this.f388a.g) {
                        if (this.f388a.i) {
                            if (!this.f388a.h) {
                                this.f388a.f385a = true;
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_no_network_active);
                                this.f388a.s.f363b.setText(x.ca);
                                this.f388a.s.f363b.setOnClickListener(new ay(this));
                                break;
                            }
                        } else {
                            this.f388a.f385a = true;
                            this.f388a.d = this.f388a.s.getString(x.l_conn_check_no_data_enabled);
                            this.f388a.s.f363b.setText(x.ca);
                            this.f388a.s.f363b.setOnClickListener(new ax(this));
                            break;
                        }
                    } else {
                        this.f388a.f385a = true;
                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_background_data_disabled);
                        this.f388a.s.f363b.setText(x.ca);
                        this.f388a.s.f363b.setOnClickListener(new aw(this));
                        break;
                    }
                    break;
                case 4:
                    if (this.f388a.e) {
                        if (this.f388a.f) {
                            if (this.f388a.p.a() != f.VALID) {
                                this.f388a.f385a = true;
                                this.f388a.f386b = true;
                                switch (az.f403a[this.f388a.p.a().ordinal()]) {
                                    case 1:
                                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_backup_invalid);
                                        break;
                                    case 2:
                                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_no_backup);
                                        break;
                                    case 3:
                                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_backup_not_up_to_date);
                                        break;
                                    default:
                                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_backup_invalid);
                                        break;
                                }
                                this.f388a.s.f363b.setText(x.l_ignore);
                                this.f388a.s.f363b.setOnClickListener(new am(this));
                                break;
                            }
                        } else {
                            this.f388a.f385a = true;
                            this.f388a.d = this.f388a.s.getString(x.l_conn_check_c2dm_incorrect);
                            this.f388a.s.f363b.setText(x.l_disconnect);
                            this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                            break;
                        }
                    } else {
                        this.f388a.f385a = true;
                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_settings_incorrect);
                        this.f388a.s.f363b.setText(x.l_disconnect);
                        this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                        break;
                    }
                    break;
                case 5:
                    if (this.f388a.q == c.NONE) {
                        ab.a(this.f388a.s.getActivity());
                        break;
                    } else {
                        this.f388a.f385a = true;
                        switch (az.f404b[this.f388a.q.ordinal()]) {
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_invalid_data);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                            case 5:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_device_not_found);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                            case 6:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_generic_c2dm_error);
                                this.f388a.s.f363b.setText(x.l_conn_check_feedback);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.k);
                                break;
                            case 7:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_generic_c2dm_error_from_google);
                                this.f388a.s.f363b.setText(x.l_conn_check_feedback);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.k);
                                break;
                            case 8:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_invalid_response);
                                this.f388a.s.f363b.setText(x.ca);
                                this.f388a.s.f363b.setOnClickListener(new an(this));
                                break;
                            case 9:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_no_c2dm_id_on_server);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                            case 10:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_not_registered_for_c2dm);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                            case 11:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_too_old);
                                this.f388a.s.f363b.setText(x.bR);
                                this.f388a.s.f363b.setOnClickListener(new ao(this));
                                break;
                            case 12:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_deprecated);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                            case 13:
                                this.f388a.d = this.f388a.s.getString(x.l_conn_check_not_activated);
                                this.f388a.s.f363b.setText(x.l_disconnect);
                                this.f388a.s.f363b.setOnClickListener(this.f388a.s.l);
                                break;
                        }
                    }
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f388a.s.f();
                    if (!this.f388a.s.d) {
                        this.f388a.f385a = true;
                        this.f388a.d = this.f388a.s.getString(x.l_conn_check_online_no_web_message_received);
                        this.f388a.s.f363b.setText(x.l_conn_check_feedback);
                        this.f388a.s.f363b.setOnClickListener(this.f388a.s.k);
                        break;
                    }
                    break;
            }
            if (this.f388a.f385a || this.f388a.r < 6 || this.f388a.r >= 26) {
                if (this.f388a.f387c) {
                    this.f388a.f386b = false;
                }
                if (this.f388a.f385a) {
                    this.f388a.s.f362a.setText(this.f388a.d);
                    TextView a2 = this.f388a.s.f362a;
                    if (this.f388a.f386b) {
                        color = this.f388a.s.getResources().getColor(o.text_warning);
                    } else {
                        color = this.f388a.s.getResources().getColor(o.h);
                    }
                    a2.setTextColor(color);
                    if (this.f388a.f387c) {
                        this.f388a.s.f363b.setText(x.ca);
                        this.f388a.s.f363b.setOnClickListener(new ap(this));
                    }
                } else {
                    this.f388a.s.f362a.setText(x.l_conn_check_no_issues);
                    this.f388a.s.f362a.setTextColor(this.f388a.s.getResources().getColor(o.g));
                    if (this.f388a.r < 6) {
                        this.f388a.s.f363b.setText(this.f388a.s.getString(x.bR));
                    } else {
                        this.f388a.s.f363b.setText(this.f388a.s.getString(x.bR));
                        this.f388a.s.f363b.setOnClickListener(new aq(this));
                    }
                }
                this.f388a.s.f363b.setEnabled(true);
                this.f388a.s.f363b.setBackgroundResource(q.j);
                this.f388a.s.f364c.setEnabled(true);
                if (this.f388a.f385a || this.f388a.r >= 6) {
                    if (this.f388a.f385a || !this.f388a.s.isAdded() || this.f388a.s.getActivity() == null) {
                    }
                    return;
                }
                this.f388a.s.a(this.f388a.r + 1);
            } else if (!this.f388a.s.d) {
                this.f388a.s.f362a.setText(this.f388a.s.getString(x.l_conn_check_receive_message_check, Integer.valueOf(26 - this.f388a.r)));
                this.f388a.s.a(this.f388a.r + 1);
            } else {
                this.f388a.s.a(26);
            }
        }
    }
}
