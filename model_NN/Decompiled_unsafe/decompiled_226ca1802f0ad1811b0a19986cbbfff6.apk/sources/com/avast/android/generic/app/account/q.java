package com.avast.android.generic.app.account;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: ConnectAccountFragment */
class q implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f448a;

    private q(ConnectAccountFragment connectAccountFragment) {
        this.f448a = connectAccountFragment;
    }

    /* synthetic */ q(ConnectAccountFragment connectAccountFragment, g gVar) {
        this(connectAccountFragment);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        boolean z = true;
        if (editable.length() < 1) {
            z = false;
        }
        if (z) {
            this.f448a.m.setVisibility(0);
            if (this.f448a.h()) {
                this.f448a.n.setVisibility(8);
                this.f448a.m.setImageResource(com.avast.android.generic.q.ic_scanner_result_ok);
                return;
            }
            this.f448a.n.setVisibility(0);
            this.f448a.m.setImageResource(com.avast.android.generic.q.ic_scanner_result_problem);
            return;
        }
        this.f448a.m.setVisibility(4);
        this.f448a.n.setVisibility(8);
    }
}
