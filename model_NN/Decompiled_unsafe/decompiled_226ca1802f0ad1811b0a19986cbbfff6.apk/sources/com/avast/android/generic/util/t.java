package com.avast.android.generic.util;

import android.util.Log;

/* compiled from: DebugLog */
public final class t {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1252a = false;

    /* renamed from: b  reason: collision with root package name */
    private static String f1253b = "app";

    public static boolean a() {
        return f1252a;
    }

    public static void a(boolean z) {
        f1252a = z;
    }

    public static void a(String str) {
        f1253b = str;
    }

    public static int b(String str) {
        return a(f1253b, str);
    }

    public static int a(String str, String str2) {
        if (f1252a) {
            return Log.v(str, str2);
        }
        return 0;
    }

    public static int c(String str) {
        return b(f1253b, str);
    }

    public static int b(String str, String str2) {
        if (f1252a) {
            return Log.d(str, str2);
        }
        return 0;
    }

    public static int a(String str, String str2, Throwable th) {
        if (f1252a) {
            return Log.d(str, str2, th);
        }
        return 0;
    }

    public static int c(String str, String str2) {
        if (f1252a) {
            return Log.i(str, str2);
        }
        return 0;
    }

    public static int b(String str, String str2, Throwable th) {
        if (f1252a) {
            return Log.i(str, str2, th);
        }
        return 0;
    }

    public static int d(String str, String str2) {
        return Log.w(str, str2);
    }

    public static int d(String str) {
        return d(f1253b, str);
    }

    public static int c(String str, String str2, Throwable th) {
        return Log.w(str, str2, th);
    }

    public static int a(String str, Throwable th) {
        return c(f1253b, str, th);
    }

    public static int e(String str) {
        return e(f1253b, str);
    }

    public static int e(String str, String str2) {
        return Log.e(str, str2);
    }

    public static int b(String str, Throwable th) {
        return d(f1253b, str, th);
    }

    public static int d(String str, String str2, Throwable th) {
        return Log.e(str, str2, th);
    }
}
