package com.avast.android.generic.app.subscription;

import com.avast.android.generic.licensing.m;
import com.avast.c.a.a.i;
import com.avast.c.a.a.v;

/* compiled from: SubscriptionFragment */
/* synthetic */ class t {

    /* renamed from: b  reason: collision with root package name */
    static final /* synthetic */ int[] f575b = new int[v.values().length];

    /* renamed from: c  reason: collision with root package name */
    static final /* synthetic */ int[] f576c = new int[i.values().length];

    static {
        try {
            f576c[i.GV_NO_IDENTITIES.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f576c[i.GV_GENERIC.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f575b[v.VOUCHER_CODE_ALREADY_CONSUMED.ordinal()] = 1;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f575b[v.VOUCHER_CODE_NOT_VALID_ANYMORE.ordinal()] = 2;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f575b[v.VOUCHER_CODE_NOT_YET_VALID.ordinal()] = 3;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f575b[v.VOUCHER_CODE_UNKNOWN.ordinal()] = 4;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f575b[v.VOUCHER_CODE_LOCKED.ordinal()] = 5;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f575b[v.VOUCHER_CODE_LICENSE_NOT_PUBLISHED.ordinal()] = 6;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f575b[v.VOUCHER_CODE_INVALID_OPERATOR.ordinal()] = 7;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f575b[v.VOUCHER_CODE_INVALID_COUNTRY.ordinal()] = 8;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f575b[v.GENERIC_ERROR.ordinal()] = 9;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f574a[m.VALID.ordinal()] = 1;
        } catch (NoSuchFieldError e12) {
        }
    }
}
