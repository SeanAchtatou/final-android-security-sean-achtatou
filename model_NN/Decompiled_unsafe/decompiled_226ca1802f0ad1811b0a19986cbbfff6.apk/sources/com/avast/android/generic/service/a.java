package com.avast.android.generic.service;

import com.avast.android.generic.util.z;

/* compiled from: AvastService */
class a implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AvastService f961a;

    a(AvastService avastService) {
        this.f961a = avastService;
    }

    public void run() {
        boolean z = true;
        long currentTimeMillis = System.currentTimeMillis();
        z.a("AvastGenericSc", "Shutdown check syncing...");
        synchronized (this.f961a.j) {
            z.a("AvastGenericSc", "Sync for shutdown check took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
            try {
                z.a("AvastGeneric", this.f961a.getApplicationContext(), "Service shutdown check");
                if (this.f961a.i > 0) {
                    z.a("AvastSDC", this.f961a.getApplicationContext(), "- " + this.f961a.i + " clients still bound");
                } else if (this.f961a.f954c != null && this.f961a.f954c.a()) {
                    z.a("AvastSDC", this.f961a.getApplicationContext(), "- SMS sender is sending");
                } else if (this.f961a.d != null && this.f961a.d.c()) {
                    z.a("AvastSDC", this.f961a.getApplicationContext(), "- HTTP sender is sending");
                } else if (this.f961a.f != null && this.f961a.f.a()) {
                    z.a("AvastSDC", this.f961a.getApplicationContext(), "- Command receiver is populated");
                } else if (this.f961a.e == null || !this.f961a.e.a()) {
                    z = false;
                } else {
                    z.a("AvastSDC", this.f961a.getApplicationContext(), "- Task handler is running");
                }
                if (this.f961a.a()) {
                    if (!z) {
                        z.a("AvastGeneric", this.f961a.getApplicationContext(), "Service stops");
                        this.f961a.h();
                    } else {
                        z.a("AvastGeneric", this.f961a.getApplicationContext(), "Service continues");
                    }
                    z.a("AvastGenericSc", "Service shutdown check took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
                }
            } catch (Exception e) {
                z.a("AvastGeneric", this.f961a.getApplicationContext(), "Service shutdown check failed", e);
            }
        }
    }
}
