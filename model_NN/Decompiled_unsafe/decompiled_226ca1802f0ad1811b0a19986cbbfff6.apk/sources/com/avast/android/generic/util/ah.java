package com.avast.android.generic.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import com.avast.android.generic.g.b.a;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/* compiled from: SystemProperties */
public class ah {
    public static boolean a(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (!a.j(context)) {
                return true;
            }
            Field declaredField = Class.forName(connectivityManager.getClass().getName()).getDeclaredField("mService");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(connectivityManager);
            Method declaredMethod = Class.forName(obj.getClass().getName()).getDeclaredMethod("getMobileDataEnabled", new Class[0]);
            declaredMethod.setAccessible(true);
            return ((Boolean) declaredMethod.invoke(obj, new Object[0])).booleanValue();
        } catch (Exception e) {
            Exception exc = e;
            try {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                Method declaredMethod2 = Class.forName(telephonyManager.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                declaredMethod2.setAccessible(true);
                Object invoke = declaredMethod2.invoke(telephonyManager, new Object[0]);
                Method declaredMethod3 = Class.forName(invoke.getClass().getName()).getDeclaredMethod("isDataConnectivityPossible", new Class[0]);
                declaredMethod3.setAccessible(true);
                return ((Boolean) declaredMethod3.invoke(invoke, new Object[0])).booleanValue();
            } catch (Exception e2) {
                z.a("AvastAntiTheft", "Two methods for getting data setting failed");
                z.a("AvastAntiTheft", "Two methods for getting data setting failed M1", exc);
                z.a("AvastAntiTheft", "Two methods for getting data setting failed M2", e2);
                return true;
            }
        }
    }
}
