package com.avast.android.generic.notification;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.f;
import android.support.v4.content.o;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.a.a.a.a;
import com.avast.android.generic.aa;
import com.avast.android.generic.e;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;

public class AvastNotificationFragment extends TrackedMultiToolFragment implements LoaderManager.LoaderCallbacks<Cursor> {

    /* renamed from: a  reason: collision with root package name */
    private View f914a;

    /* renamed from: b  reason: collision with root package name */
    private View f915b;

    /* renamed from: c  reason: collision with root package name */
    private View f916c;
    private f d;
    private f e;
    private f f;
    private a g;
    private ListView h;
    private e i;
    /* access modifiers changed from: private */
    public LoaderManager j;
    private boolean k;
    private Uri l;
    /* access modifiers changed from: private */
    public h m;
    private AdapterView.OnItemClickListener n = new b(this);

    public final String c() {
        return "notifications";
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = new e(this, new Handler());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_avast_notifications, viewGroup, false);
        this.m = (h) aa.a(getActivity(), h.class);
        this.l = this.m.h();
        this.f914a = inflate.findViewById(r.list_item_notification_empty);
        this.f914a.setVisibility(8);
        ((TextView) this.f914a.findViewById(r.bM)).setText(getText(this.m.f()));
        ((TextView) this.f914a.findViewById(r.text)).setText(getText(this.m.g()));
        this.f914a.findViewById(r.list_item_row).setOnClickListener(new c(this));
        inflate.findViewById(r.b_clear).setOnClickListener(new d(this));
        this.h = (ListView) inflate.findViewById(r.list_notifications);
        this.h.setOnItemClickListener(this.n);
        return inflate;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.widget.ListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        FragmentActivity activity = getActivity();
        this.d = new f(activity, this.m);
        this.f = new f(activity, this.m);
        this.e = new f(activity, this.m);
        this.g = new a();
        this.f915b = activity.getLayoutInflater().inflate(t.list_item_header_notifications_ongoing, (ViewGroup) this.h, false);
        this.g.a(this.f915b);
        this.g.a(this.d);
        this.f916c = activity.getLayoutInflater().inflate(t.list_item_header_notifications, (ViewGroup) this.h, false);
        this.g.a(this.f916c);
        this.g.a(this.e);
        this.g.a(this.f);
        this.h.setAdapter((ListAdapter) this.g);
        this.j = getActivity().getSupportLoaderManager();
        this.j.initLoader(10006, null, this);
        this.j.initLoader(10005, null, this);
        this.j.initLoader(10007, null, this);
        this.k = true;
    }

    public void onResume() {
        super.onResume();
        if (!this.k) {
            this.j.restartLoader(10005, null, this);
            this.j.restartLoader(10007, null, this);
        } else {
            this.k = false;
        }
        getActivity().getContentResolver().registerContentObserver(e.a(this.l), true, this.i);
    }

    public void onPause() {
        getActivity().getContentResolver().unregisterContentObserver(this.i);
        super.onPause();
    }

    public o<Cursor> onCreateLoader(int i2, Bundle bundle) {
        switch (i2) {
            case 10005:
                return new r(getActivity(), s.ONGOING);
            case 10006:
                return new f(getActivity(), e.a(this.l), null, "ongoing = ?", new String[]{"0"}, null);
            case 10007:
                return new r(getActivity(), s.TEMPORARY);
            default:
                return null;
        }
    }

    /* renamed from: a */
    public void onLoadFinished(o<Cursor> oVar, Cursor cursor) {
        boolean z;
        int i2;
        boolean z2 = true;
        int i3 = 0;
        switch (oVar.k()) {
            case 10005:
                this.d.changeCursor(cursor);
                break;
            case 10006:
                this.e.changeCursor(cursor);
                break;
            case 10007:
                this.f.changeCursor(cursor);
                break;
        }
        a aVar = this.g;
        View view = this.f915b;
        if (!this.d.isEmpty()) {
            z = true;
        } else {
            z = false;
        }
        aVar.b(view, z);
        a aVar2 = this.g;
        View view2 = this.f916c;
        if (this.e.isEmpty() && this.f.isEmpty()) {
            z2 = false;
        }
        aVar2.b(view2, z2);
        ListView listView = this.h;
        if (this.g.isEmpty()) {
            i2 = 8;
        } else {
            i2 = 0;
        }
        listView.setVisibility(i2);
        View view3 = this.f914a;
        if (!this.g.isEmpty()) {
            i3 = 8;
        }
        view3.setVisibility(i3);
    }

    public void onLoaderReset(o<Cursor> oVar) {
        switch (oVar.k()) {
            case 10005:
                this.d.changeCursor(null);
                return;
            case 10006:
                this.e.changeCursor(null);
                return;
            case 10007:
                this.f.changeCursor(null);
                return;
            default:
                return;
        }
    }
}
