package com.avast.android.generic.ui;

import android.os.Handler;
import android.os.Message;
import com.avast.android.generic.ab;

/* compiled from: PasswordDialog */
class ad extends Handler {

    /* renamed from: a  reason: collision with root package name */
    ab f1007a;

    /* renamed from: b  reason: collision with root package name */
    boolean f1008b = false;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ PasswordDialog f1009c;

    public ad(PasswordDialog passwordDialog, ab abVar) {
        this.f1009c = passwordDialog;
        this.f1007a = abVar;
    }

    public void handleMessage(Message message) {
        if (!this.f1008b) {
            a();
        }
    }

    private void a() {
        String a2 = this.f1009c.d.a();
        if (this.f1007a.b(a2) || this.f1007a.c(a2)) {
            this.f1008b = true;
            this.f1009c.c();
        }
    }
}
