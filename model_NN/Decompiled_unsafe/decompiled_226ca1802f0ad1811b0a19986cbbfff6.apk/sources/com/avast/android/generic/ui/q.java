package com.avast.android.generic.ui;

import android.widget.CompoundButton;

/* compiled from: CustomNumberDialog */
class q implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CustomNumberDialog f1055a;

    q(CustomNumberDialog customNumberDialog) {
        this.f1055a = customNumberDialog;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.f1055a.a(z);
        this.f1055a.a(this.f1055a.f991c.getText());
    }
}
