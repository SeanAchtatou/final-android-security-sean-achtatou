package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.avast.android.generic.e.a;
import com.avast.android.generic.e.b;
import com.avast.android.generic.e.c;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class Row extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final int f1100a = 1;

    /* renamed from: b  reason: collision with root package name */
    private c f1101b;
    protected String d;
    protected int e;
    protected String f;
    protected String g;
    protected View h;
    protected View.OnClickListener i;
    protected TextView j;
    protected TextView k;
    protected int l;

    public Row(Context context) {
        super(context);
    }

    public Row(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        a(context, attributeSet, i2);
    }

    public Row(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet, n.rowStyle);
    }

    /* access modifiers changed from: protected */
    public void a(Context context, AttributeSet attributeSet, int i2) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, z.h, i2, y.Row);
        this.g = obtainStyledAttributes.getString(0);
        int resourceId = obtainStyledAttributes.getResourceId(1, 0);
        if (resourceId != 0) {
            this.d = context.getString(resourceId);
        }
        this.e = obtainStyledAttributes.getResourceId(4, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(2, 0);
        if (resourceId2 != 0) {
            this.f = context.getString(resourceId2);
        }
        int resourceId3 = obtainStyledAttributes.getResourceId(10, 0);
        if (resourceId3 != 0) {
            this.h = ((View) getParent()).findViewById(resourceId3);
        }
        this.l = obtainStyledAttributes.getResourceId(12, 0);
        setBackgroundResource(obtainStyledAttributes.getResourceId(6, 0));
        setClickable(obtainStyledAttributes.getBoolean(7, true));
        setFocusable(obtainStyledAttributes.getBoolean(8, true));
        if (isInEditMode()) {
            this.f1101b = new a();
            return;
        }
        switch (obtainStyledAttributes.getInteger(11, 1)) {
            case 0:
                return;
            case 1:
                this.f1101b = new b(context);
                return;
            default:
                throw new IllegalArgumentException("this dao index is not supported");
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (e() != null) {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public final void onFinishInflate() {
        super.onFinishInflate();
        a();
        this.j = (TextView) findViewById(r.row_title);
        if (this.j != null) {
            this.j.setText(this.d);
            if (this.e != 0) {
                this.j.setTextAppearance(getContext(), this.e);
            }
        }
        this.k = (TextView) findViewById(r.row_subtitle);
        if (this.k != null) {
            if (TextUtils.isEmpty(this.f)) {
                this.k.setVisibility(8);
            } else {
                this.k.setText(this.f);
            }
        }
        if (this.h != null) {
            setEnabled(this.h.isEnabled());
        }
    }

    /* access modifiers changed from: protected */
    public void a(View.OnClickListener onClickListener) {
        this.i = onClickListener;
    }

    public boolean performClick() {
        if (this.i != null) {
            this.i.onClick(this);
        }
        return super.performClick();
    }

    public void a(c cVar) {
        this.f1101b = cVar;
        b();
    }

    public c e() {
        return this.f1101b;
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row, this);
    }

    public void b() {
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setClickable(z);
        super.setFocusable(z);
        if (this.j != null) {
            this.j.setEnabled(z);
        }
        if (this.k != null) {
            this.k.setEnabled(z);
        }
    }

    public void b(String str) {
        this.d = str;
        if (this.j != null) {
            this.j.setText(str);
        }
    }

    public void c(String str) {
        this.f = str;
        if (this.k == null) {
            return;
        }
        if (TextUtils.isEmpty(str)) {
            this.k.setVisibility(8);
            return;
        }
        this.k.setText(str);
        this.k.setVisibility(0);
    }

    public void a(Spanned spanned) {
        this.f = spanned != null ? spanned.toString() : null;
        if (this.k == null) {
            return;
        }
        if (TextUtils.isEmpty(spanned)) {
            this.k.setVisibility(8);
            return;
        }
        this.k.setText(spanned);
        this.k.setVisibility(0);
    }
}
