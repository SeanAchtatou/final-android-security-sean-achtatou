package com.avast.android.generic;

/* compiled from: GenericSettingConstants */
public class f {
    public static boolean a(String str) {
        if (str == null) {
            return false;
        }
        if (str.equals("encaccesscode") || str.equals("enctempaccesscode") || str.equals("tempaccesscoderecoverynumber") || str.equals("tempaccesscodeissuetime") || str.equals("tempaccesscodelastknowntime") || str.equals("tempaccesscodetimeouttime") || str.equals("tempaccesscodereceivertickauthtoken") || str.equals("tempaccesscodereceiversmsauthtoken") || str.equals("logcatavailable") || str.equals("c2dmri") || str.equals("c2dmowner") || str.equals("id") || str.equals("restorechecked") || str.equals("guid") || str.equals("paswordProtection") || str.equals("communityIQEnabled") || str.equals("language") || str.equals("splitcdma") || str.equals("accountEmail") || str.equals("auid") || str.equals("accountEncKey") || str.equals("accountCommPassword") || str.equals("accountReport") || str.equals("accountReportFrequency") || str.equals("accountSmsGateway") || str.equals("accountLuid") || str.equals("not1") || str.equals("not2") || str.equals("accountSmsSending") || str.startsWith("premiumAccount_") || str.startsWith("premiumExpirationDate_") || str.startsWith("premiumIsSubscription_") || str.startsWith("premiumSku_") || str.equals("welcomePremiumShown")) {
            return true;
        }
        return false;
    }
}
