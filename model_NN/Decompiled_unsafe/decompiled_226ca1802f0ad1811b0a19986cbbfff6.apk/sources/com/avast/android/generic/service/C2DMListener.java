package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.c.d;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.aw;
import com.avast.android.generic.util.z;

public class C2DMListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            a(applicationContext, intent);
        } else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
            b(applicationContext, intent);
        }
    }

    private void a(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        String stringExtra = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            String stringExtra2 = intent.getStringExtra("error");
            z.a("AvastComms", applicationContext, "C2DM listener received C2DM registration error (" + stringExtra2 + ")");
            aw.a(applicationContext);
            Intent intent2 = new Intent();
            intent2.setAction("com.avast.android.generic.action.C2DM_ERROR");
            intent2.putExtra("error", stringExtra2);
            ae.a(applicationContext, intent2, applicationContext.getPackageName());
        } else if (intent.getStringExtra("unregistered") != null) {
            z.a("AvastComms", applicationContext, "C2DM listener received C2DM unregistered event");
            aw.a(applicationContext);
            Intent intent3 = new Intent();
            intent3.setAction("com.avast.android.generic.action.ACTION_C2DM_REMOVE");
            ae.a(applicationContext, intent3, applicationContext.getPackageName());
        } else if (stringExtra != null) {
            z.a("AvastComms", applicationContext, "C2DM listener detected successful registration (ID " + stringExtra + ")");
            aw.a(applicationContext);
            Intent intent4 = new Intent();
            intent4.setAction("com.avast.android.generic.action.ACTION_C2DM_SUCCESS");
            intent4.putExtra("registration", stringExtra);
            ae.a(applicationContext, intent4, applicationContext.getPackageName());
        }
    }

    private void b(Context context, Intent intent) {
        Context applicationContext = context.getApplicationContext();
        String string = intent.getExtras().getString("uid");
        String string2 = intent.getExtras().getString("message");
        if (string != null && string2 != null && !string.equals("") && !string2.equals("")) {
            z.b(applicationContext, "C2DM", "Message received with ID " + string + ": " + string2 + ", will be dispatched");
            if (string.equals("NONE")) {
                String y = ((ab) aa.a(applicationContext, ab.class)).y();
                if (!TextUtils.isEmpty(y) && string2.equals(y + " CONNCHECK")) {
                    try {
                        Intent intent2 = new Intent("com.avast.android.generic.action.ACTION_CONNECTION_CHECK_RECEIVED");
                        ae.a(intent2);
                        applicationContext.sendBroadcast(intent2, "com.avast.android.generic.COMM_PERMISSION");
                    } catch (Exception e) {
                        z.a("AvastGeneric", "Unable to broadcast conn check intent", e);
                    }
                }
            } else {
                aw.a(applicationContext);
                Intent intent3 = new Intent();
                intent3.setAction("com.avast.android.generic.action.ACTION_C2DM_MESSAGE");
                intent3.putExtra("uid", string);
                intent3.putExtra("message", string2);
                d.a(applicationContext, intent3, null, string, string2, false);
            }
        }
    }
}
