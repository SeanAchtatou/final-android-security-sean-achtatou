package com.avast.android.generic.app.subscription;

import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.avast.android.generic.q;
import com.avast.android.generic.ui.BaseSinglePaneActivity;
import com.avast.android.generic.x;

public abstract class SubscriptionActivity extends BaseSinglePaneActivity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setBackgroundDrawable(getResources().getDrawable(q.xml_bg_actionbar_premium_wtb));
            supportActionBar.setDisplayOptions(14);
            supportActionBar.setIcon(getResources().getDrawable(q.ic_app));
            supportActionBar.setTitle(x.l_subscription_premium_plans_title);
        }
    }
}
