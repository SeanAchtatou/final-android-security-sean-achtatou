package com.avast.android.generic.b;

import android.content.Context;
import android.text.TextUtils;
import com.avast.android.generic.b.a.c;
import com.avast.android.generic.b.a.d;
import com.avast.android.generic.b.a.f;
import com.avast.android.generic.b.a.h;
import com.avast.android.generic.b.a.i;
import com.avast.android.generic.b.a.k;
import com.avast.android.generic.b.a.l;
import com.avast.android.generic.shepherd.b;
import com.google.protobuf.ByteString;

/* compiled from: BadNews */
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f597a = false;

    /* renamed from: b  reason: collision with root package name */
    private static Context f598b;

    public static void a(Throwable th) {
        a(d.UNCAUGHT_EXCEPTION, null, th);
    }

    private static void a(d dVar, String str, Throwable th) {
        if ((!TextUtils.isEmpty(str) || th != null) && f597a) {
            try {
                f o = c.o();
                o.a(dVar);
                if (th != null) {
                    do {
                        l f = k.f();
                        String message = th.getMessage();
                        if (message != null) {
                            f.a(ByteString.copyFromUtf8(message));
                        }
                        StackTraceElement[] stackTrace = th.getStackTrace();
                        String str2 = "";
                        for (int i = 0; i < stackTrace.length; i++) {
                            str2 = (str2 + stackTrace[i].toString()) + "\n";
                        }
                        f.b(ByteString.copyFromUtf8(str2));
                        o.a(f);
                        th = th.getCause();
                    } while (th != null);
                }
                if (str != null) {
                    o.a(ByteString.copyFromUtf8(str));
                }
                o.a(b.a(f598b));
                o.a(1);
                o.a(System.currentTimeMillis());
                i b2 = h.b();
                b2.a(o.build());
                com.avast.android.generic.i.d.a(f598b, null, com.avast.android.generic.i.b.BAD_NEWS, com.avast.android.generic.i.c.NOTHING, b2.build().toByteArray());
            } catch (Exception e) {
            }
        }
    }
}
