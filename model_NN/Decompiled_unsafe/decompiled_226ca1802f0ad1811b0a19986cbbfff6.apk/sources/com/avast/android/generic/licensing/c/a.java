package com.avast.android.generic.licensing.c;

/* compiled from: IabException */
public class a extends Exception {

    /* renamed from: a  reason: collision with root package name */
    i f856a;

    public a(i iVar) {
        this(iVar, (Exception) null);
    }

    public a(int i, String str) {
        this(new i(i, str));
    }

    public a(i iVar, Exception exc) {
        super(iVar.b(), exc);
        this.f856a = iVar;
    }

    public a(int i, String str, Exception exc) {
        this(new i(i, str), exc);
    }

    public i a() {
        return this.f856a;
    }
}
