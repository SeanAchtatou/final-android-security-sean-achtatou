package com.avast.android.generic.notification;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.json.JSONException;
import org.json.JSONObject;

public class AvastPendingIntent implements Parcelable {
    public static final Parcelable.Creator CREATOR = new p();

    /* renamed from: a  reason: collision with root package name */
    String f917a;

    /* renamed from: b  reason: collision with root package name */
    Uri f918b;

    /* renamed from: c  reason: collision with root package name */
    String f919c;
    int d;
    Bundle e;
    q f;

    /* synthetic */ AvastPendingIntent(Parcel parcel, p pVar) {
        this(parcel);
    }

    public AvastPendingIntent(Cursor cursor, String str, String str2, String str3, String str4, String str5, String str6) {
        if (cursor == null) {
            throw new IllegalArgumentException("cannot create AvastNotificationIntent from null cursor");
        }
        a(cursor, str, str2, str3, str4, str5, str6);
    }

    public Intent a(Context context) {
        Intent intent = new Intent();
        intent.setFlags(this.d);
        if (this.e != null && this.e.size() > 0) {
            intent.putExtras(new Bundle(this.e));
        }
        intent.setData(this.f918b);
        if (!(this.f919c == null || context == null)) {
            intent.setClassName(context, this.f919c);
        }
        if (this.f917a != null) {
            intent.setAction(this.f917a);
        }
        return intent;
    }

    public q a() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void a(ContentValues contentValues, String str, String str2, String str3, String str4, String str5, String str6) {
        contentValues.put(str, this.f919c);
        contentValues.put(str4, this.f918b == null ? null : this.f918b.toString());
        contentValues.put(str2, this.f917a);
        contentValues.put(str6, Integer.valueOf(this.d));
        contentValues.put(str3, this.f.name());
        contentValues.put(str5, b());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    /* access modifiers changed from: package-private */
    public String b() {
        if (this.e == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("___has_types", true);
            for (String next : this.e.keySet()) {
                Object obj = this.e.get(next);
                jSONObject.put(next, obj);
                jSONObject.put(next + "___type", obj.getClass().getCanonicalName());
            }
            return jSONObject.toString();
        } catch (JSONException e2) {
            throw new RuntimeException("cannot serialize intent extras to json: " + e2.getMessage(), e2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Cursor cursor, String str, String str2, String str3, String str4, String str5, String str6) {
        boolean z;
        this.f919c = cursor.getString(cursor.getColumnIndex(str));
        this.f917a = cursor.getString(cursor.getColumnIndex(str2));
        String string = cursor.getString(cursor.getColumnIndex(str3));
        this.f = string != null ? q.valueOf(string) : null;
        String string2 = cursor.getString(cursor.getColumnIndex(str4));
        if (!TextUtils.isEmpty(string2)) {
            this.f918b = Uri.parse(string2);
        } else {
            this.f918b = null;
        }
        String string3 = cursor.getString(cursor.getColumnIndex(str5));
        if (!TextUtils.isEmpty(string3)) {
            try {
                JSONObject jSONObject = new JSONObject(string3);
                if (!jSONObject.has("___has_types") || !jSONObject.getBoolean("___has_types")) {
                    z = false;
                } else {
                    z = true;
                }
                Bundle bundle = new Bundle();
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String obj = keys.next().toString();
                    if (!z || (!obj.equals("___has_types") && !obj.endsWith("___type"))) {
                        String canonicalName = String.class.getCanonicalName();
                        if (z) {
                            canonicalName = jSONObject.getString(obj + "___type");
                        }
                        Class<?> cls = Class.forName(canonicalName);
                        if (cls.equals(String.class)) {
                            bundle.putString(obj, jSONObject.getString(obj));
                        } else if (cls.equals(Integer.class)) {
                            bundle.putInt(obj, jSONObject.getInt(obj));
                        } else if (cls.equals(Long.class)) {
                            bundle.putLong(obj, jSONObject.getLong(obj));
                        } else if (cls.equals(Float.class)) {
                            bundle.putFloat(obj, (float) jSONObject.getDouble(obj));
                        } else if (cls.equals(Double.class)) {
                            bundle.putDouble(obj, jSONObject.getDouble(obj));
                        }
                    }
                }
                this.e = bundle;
            } catch (JSONException e2) {
            } catch (NoSuchElementException e3) {
            } catch (ClassNotFoundException e4) {
            }
        } else {
            this.e = null;
        }
        this.d = cursor.getInt(cursor.getColumnIndex(str6));
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f918b, 0);
        parcel.writeString(this.f919c);
        parcel.writeInt(i);
        parcel.writeParcelable(this.e, 0);
        parcel.writeString(this.f.name());
    }

    private AvastPendingIntent(Parcel parcel) {
        this.f918b = (Uri) parcel.readParcelable(getClass().getClassLoader());
        this.f919c = parcel.readString();
        this.d = parcel.readInt();
        this.e = (Bundle) parcel.readParcelable(getClass().getClassLoader());
        this.f = q.valueOf(parcel.readString());
    }
}
