package com.avast.android.generic.c;

import com.avast.android.generic.util.z;

/* compiled from: CommandReceiver */
class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f645a;

    m(l lVar) {
        this.f645a = lVar;
    }

    public void run() {
        while (!this.f645a.g) {
            z.a("AvastGeneric", this.f645a.f642a, "Waiting for child handler ...");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }
        }
        synchronized (this.f645a.e) {
            this.f645a.f.getLooper().quit();
            synchronized (this.f645a.e) {
                if (this.f645a.d != null && this.f645a.d.isAlive()) {
                    try {
                        this.f645a.d.interrupt();
                        this.f645a.d.join();
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
        synchronized (this.f645a.f643b) {
            this.f645a.f643b.clear();
        }
    }
}
