package com.avast.android.generic.internet.c;

/* compiled from: AuthenticationException */
public class b extends h {
    public b(String str, String str2) {
        super("Authentication failed with username: " + str, str2);
    }

    public b() {
        super("Authentication failed.", i.INVALID_CREDENTIALS);
    }
}
