package com.avast.android.generic.j;

import a.a.a.a.a.a;
import android.os.Bundle;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.z;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: TaskHandler */
public class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public AvastService f833a;

    /* renamed from: b  reason: collision with root package name */
    private ConcurrentHashMap<String, LinkedList<Class<? extends a>>> f834b;

    /* renamed from: c  reason: collision with root package name */
    private ConcurrentHashMap<String, LinkedList<Class<? extends a>>> f835c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public Object e;

    static /* synthetic */ int b(d dVar) {
        int i = dVar.d;
        dVar.d = i - 1;
        return i;
    }

    public boolean a() {
        boolean z;
        boolean z2 = true;
        z.a("AvastGeneric", this.f833a, "Checking task handler population state");
        synchronized (this.e) {
            AvastService avastService = this.f833a;
            StringBuilder append = new StringBuilder().append("Checked task handler population state (");
            if (this.d > 0) {
                z = true;
            } else {
                z = false;
            }
            z.a("AvastGeneric", avastService, append.append(z).append(")").toString());
            if (this.d <= 0) {
                z2 = false;
            }
        }
        return z2;
    }

    public void a(String str, Bundle bundle) {
        z.a("AvastGeneric", this.f833a, "Performing action '" + str + "'");
        synchronized (this.e) {
            this.d++;
            z.a("AvastGeneric", this.f833a, "EventsRunning at " + this.d + " (handleEvent begin)");
        }
        if (this.f835c.containsKey(str)) {
            Iterator it = this.f835c.get(str).iterator();
            while (it.hasNext()) {
                a((Class) it.next(), str, bundle);
            }
            this.f835c.remove(str);
        }
        if (this.f834b.containsKey(str)) {
            Iterator it2 = this.f834b.get(str).iterator();
            while (it2.hasNext()) {
                a((Class) it2.next(), str, bundle);
            }
        }
        synchronized (this.e) {
            this.d--;
            z.a("AvastGeneric", this.f833a, "EventsRunning at " + this.d + " (handleEvent end)");
        }
        if (!a()) {
            this.f833a.b();
        }
    }

    private void a(Class<? extends a> cls, String str, Bundle bundle) {
        synchronized (this.e) {
            this.d++;
            z.a("AvastGeneric", this.f833a, "EventsRunning at " + this.d + " (handleEvent2 begin)");
        }
        try {
            a aVar = (a) cls.newInstance();
            aVar.a(this.f833a);
            aVar.a(this.f833a, str, bundle, new e(this));
        } catch (Exception e2) {
            a.a().a("Exception in initializing event", e2);
            synchronized (this.e) {
                this.d--;
                z.a("AvastGeneric", this.f833a, "EventsRunning at " + this.d + " (handleEvent2 error)");
                if (!a()) {
                    this.f833a.b();
                }
            }
        }
    }

    public void b() {
    }
}
