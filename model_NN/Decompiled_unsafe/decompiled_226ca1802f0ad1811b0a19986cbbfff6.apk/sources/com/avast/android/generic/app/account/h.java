package com.avast.android.generic.app.account;

import android.view.View;

/* compiled from: ConnectAccountFragment */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f434a;

    h(ConnectAccountFragment connectAccountFragment) {
        this.f434a = connectAccountFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.account.ConnectAccountFragment.a(com.avast.android.generic.app.account.ConnectAccountFragment, boolean):void
     arg types: [com.avast.android.generic.app.account.ConnectAccountFragment, int]
     candidates:
      com.avast.android.generic.app.account.ConnectAccountFragment.a(com.avast.android.generic.app.account.ConnectAccountFragment, com.avast.android.generic.app.account.r):com.avast.android.generic.app.account.r
      com.avast.android.generic.app.account.ConnectAccountFragment.a(com.avast.android.generic.app.account.ConnectAccountFragment, boolean):void */
    public void onClick(View view) {
        if (this.f434a.d) {
            this.f434a.a("ms-Wizard", "Avast! Account setup", "cancel", 0);
        }
        this.f434a.a(false);
    }
}
