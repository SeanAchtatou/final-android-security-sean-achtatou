package com.avast.android.generic.internet.c;

/* compiled from: AvastAccountException */
public enum i {
    ILLEGAL_ARGUMENT_EXCEPTION,
    INVALID_REQUEST_TYPE,
    PASSWORD_INVALID,
    INVALID_EMAIL,
    INVALID_CREDENTIALS,
    INTERNAL_SERVER_ERROR,
    EMAIL_ALREADY_USED,
    UNKNOWN_ERROR;

    public static i a(String str) {
        if ("Error:illegal_argument_exception".equals(str)) {
            return ILLEGAL_ARGUMENT_EXCEPTION;
        }
        if ("Error:invalid_request_type".equals(str)) {
            return INVALID_REQUEST_TYPE;
        }
        if ("Error:password_invalid".equals(str)) {
            return PASSWORD_INVALID;
        }
        if ("Error:invalid_email".equals(str)) {
            return INVALID_EMAIL;
        }
        if ("Error:invalid_credentials".equals(str)) {
            return INVALID_CREDENTIALS;
        }
        if ("Error:internal_server_error".equals(str)) {
            return INTERNAL_SERVER_ERROR;
        }
        if ("Error:email_already_used".equals(str)) {
            return EMAIL_ALREADY_USED;
        }
        return UNKNOWN_ERROR;
    }
}
