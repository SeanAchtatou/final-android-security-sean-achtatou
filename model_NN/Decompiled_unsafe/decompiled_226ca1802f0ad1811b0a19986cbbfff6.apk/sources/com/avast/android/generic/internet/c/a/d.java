package com.avast.android.generic.internet.c.a;

import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class d extends GeneratedMessageLite.Builder<c, d> implements e {

    /* renamed from: a  reason: collision with root package name */
    private int f755a;

    /* renamed from: b  reason: collision with root package name */
    private Object f756b = "";

    /* renamed from: c  reason: collision with root package name */
    private long f757c;

    private d() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static d g() {
        return new d();
    }

    /* renamed from: a */
    public d clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d = d();
        if (d.isInitialized()) {
            return d;
        }
        throw newUninitializedMessageException(d);
    }

    public c d() {
        int i = 1;
        c cVar = new c(this);
        int i2 = this.f755a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = cVar.f754c = this.f756b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        long unused2 = cVar.d = this.f757c;
        int unused3 = cVar.f753b = i;
        return cVar;
    }

    /* renamed from: a */
    public d mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                a(cVar.e());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public d mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f755a |= 1;
                    this.f756b = codedInputStream.readBytes();
                    break;
                case 16:
                    this.f755a |= 2;
                    this.f757c = codedInputStream.readInt64();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public d a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f755a |= 1;
        this.f756b = str;
        return this;
    }

    public d a(long j) {
        this.f755a |= 2;
        this.f757c = j;
        return this;
    }
}
