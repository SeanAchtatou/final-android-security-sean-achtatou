package com.avast.android.generic.app.subscription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/* compiled from: SubscriptionFragment */
class f extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f547a;

    f(SubscriptionFragment subscriptionFragment) {
        this.f547a = subscriptionFragment;
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getIntExtra("request_code", 0) == 1) {
            this.f547a.i();
        }
    }
}
