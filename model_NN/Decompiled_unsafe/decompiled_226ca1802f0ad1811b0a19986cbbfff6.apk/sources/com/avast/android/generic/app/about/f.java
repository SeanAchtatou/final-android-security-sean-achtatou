package com.avast.android.generic.app.about;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: FeedbackFragment */
class f implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ FeedbackFragment f343a;

    f(FeedbackFragment feedbackFragment) {
        this.f343a = feedbackFragment;
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (this.f343a.f()) {
            this.f343a.e.setVisibility(8);
        }
    }
}
