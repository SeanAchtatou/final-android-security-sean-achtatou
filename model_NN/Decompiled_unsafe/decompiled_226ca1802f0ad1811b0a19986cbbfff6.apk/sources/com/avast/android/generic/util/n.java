package com.avast.android.generic.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

/* compiled from: C2DMUtils */
public class n {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1248a = false;

    public static void a(Context context) {
        try {
            context.getPackageManager().getPackageInfo("com.google.android.gsf", 0);
            z.a("AvastComms", context, "C2DM requestor is requesting new reg ID");
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
            intent.putExtra("sender", "267505377073");
            if (context.startService(intent) == null) {
                z.a("AvastComms", context, "C2DM requestor failed requesting new reg ID");
                throw new m();
            } else {
                z.a("AvastComms", context, "C2DM requestor requested new reg ID successfully");
            }
        } catch (PackageManager.NameNotFoundException e) {
            throw new m();
        }
    }
}
