package com.avast.android.generic.shepherd;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;

public class ShepherdDownloadService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private ab f966a;

    protected static void a(Context context, boolean z) {
        Intent a2 = a(context);
        a2.putExtra("intent.extra.FORCE_UPDATE", z);
        context.startService(a2);
    }

    private static Intent a(Context context) {
        Intent intent = new Intent();
        intent.setClass(context.getApplicationContext(), ShepherdDownloadService.class);
        return intent;
    }

    public ShepherdDownloadService() {
        super(ShepherdDownloadService.class.getName());
    }

    public void onCreate() {
        super.onCreate();
        this.f966a = (ab) aa.a(this, ab.class);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        boolean booleanExtra = intent.getBooleanExtra("intent.extra.FORCE_UPDATE", false);
        if ((this.f966a.Y() <= System.currentTimeMillis() || booleanExtra) && !a() && !booleanExtra) {
            this.f966a.f(System.currentTimeMillis() + 28800000);
        }
        b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a() {
        /*
            r10 = this;
            r1 = 0
            r0 = 1
            com.avast.android.generic.ab r2 = r10.f966a
            com.avast.e.a.az r2 = com.avast.android.generic.shepherd.h.a(r10, r2)
            com.avast.android.generic.ab r3 = r10.f966a     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            r3.g(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            com.avast.android.generic.i.b r3 = com.avast.android.generic.i.b.SHEPHERD     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            com.avast.android.generic.i.c r4 = com.avast.android.generic.i.c.NOTHING     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            r5 = 0
            com.avast.d.b.y r2 = com.avast.android.generic.i.d.a(r10, r2, r3, r4, r5)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            com.google.protobuf.ByteString r2 = r2.g()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            com.avast.android.generic.h.u r2 = com.avast.android.generic.h.u.a(r2)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            r3.<init>()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.Class r4 = r10.getClass()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.String r4 = r4.getName()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.String r4 = ": response hasTtl= "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            boolean r4 = r2.b()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.String r4 = " "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.String r4 = "getTtl="
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            int r4 = r2.c()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            java.lang.String r3 = r3.toString()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            com.avast.android.generic.util.t.c(r3)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            boolean r3 = r2.b()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            if (r3 == 0) goto L_0x00eb
            com.avast.android.generic.ab r3 = r10.f966a     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            int r6 = r2.c()     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            long r6 = (long) r6     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            r8 = 1000(0x3e8, double:4.94E-321)
            long r6 = r6 * r8
            long r4 = r4 + r6
            r3.f(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00c6, Exception -> 0x00d8 }
            r3 = r0
        L_0x0075:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            r4.<init>()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.Class r5 = r10.getClass()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.String r5 = r5.getName()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.String r5 = ": response hasContent= "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            boolean r5 = r2.d()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            java.lang.String r4 = r4.toString()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            com.avast.android.generic.util.t.c(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            boolean r4 = r2.d()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            if (r4 == 0) goto L_0x00b0
            com.avast.android.generic.shepherd.d r4 = com.avast.android.generic.shepherd.d.a(r10)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            com.google.protobuf.ByteString r2 = r2.e()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            byte[] r2 = r2.toByteArray()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            r4.a(r10, r2)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
        L_0x00b0:
            com.avast.android.generic.ab r2 = r10.f966a     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            r4 = 0
            r2.c(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            if (r3 != 0) goto L_0x00c5
            com.avast.android.generic.ab r2 = r10.f966a     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
            r6 = 28800000(0x1b77400, double:1.42290906E-316)
            long r4 = r4 + r6
            r2.f(r4)     // Catch:{ InvalidProtocolBufferException -> 0x00e9, Exception -> 0x00e7 }
        L_0x00c5:
            return r0
        L_0x00c6:
            r2 = move-exception
            r3 = r1
        L_0x00c8:
            com.avast.android.generic.ab r4 = r10.f966a
            r4.c(r0)
            java.lang.String r4 = r2.getMessage()
            com.avast.android.generic.util.t.b(r4, r2)
        L_0x00d4:
            if (r3 != 0) goto L_0x00c5
            r0 = r1
            goto L_0x00c5
        L_0x00d8:
            r2 = move-exception
            r3 = r1
        L_0x00da:
            com.avast.android.generic.ab r4 = r10.f966a
            r4.c(r0)
            java.lang.String r4 = r2.getMessage()
            com.avast.android.generic.util.t.b(r4, r2)
            goto L_0x00d4
        L_0x00e7:
            r2 = move-exception
            goto L_0x00da
        L_0x00e9:
            r2 = move-exception
            goto L_0x00c8
        L_0x00eb:
            r3 = r1
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.shepherd.ShepherdDownloadService.a():boolean");
    }

    private void b() {
        ((AlarmManager) getSystemService("alarm")).set(1, this.f966a.Y(), PendingIntent.getService(this, 0, a(this), 268435456));
    }
}
