package com.avast.android.generic.g.a;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import com.avast.android.generic.ab;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.bc;
import com.avast.android.generic.util.z;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/* compiled from: SmsSender */
public class b extends PhoneStateListener {

    /* renamed from: c  reason: collision with root package name */
    private static short f681c = 0;
    private static final SecureRandom d = new SecureRandom();

    /* renamed from: a  reason: collision with root package name */
    private Object f682a;

    /* renamed from: b  reason: collision with root package name */
    private ab f683b;
    private Random e;
    /* access modifiers changed from: private */
    public LinkedList<e> f;
    /* access modifiers changed from: private */
    public LinkedList<e> g;
    /* access modifiers changed from: private */
    public Context h;
    private g i;
    private f j;
    /* access modifiers changed from: private */
    public Thread k;
    /* access modifiers changed from: private */
    public boolean l;
    /* access modifiers changed from: private */
    public Object m;
    private TelephonyManager n;
    private boolean o;

    public void onServiceStateChanged(ServiceState serviceState) {
        if (serviceState.getState() == 0) {
            b();
        } else {
            c();
        }
    }

    /* access modifiers changed from: private */
    public void a(e eVar) {
        if (!(eVar == null || eVar.f688c == null)) {
            try {
                eVar.f688c.b();
            } catch (Exception e2) {
                z.a("AvastComms", this.h, "SMS sender SMS callback error", e2);
            }
        }
        g();
        synchronized (this.g) {
            this.g.remove(eVar);
        }
        if (!a() && (this.h instanceof AvastService)) {
            ((AvastService) this.h).b();
        }
    }

    /* access modifiers changed from: private */
    public void a(e eVar, int i2, boolean z) {
        if (!(eVar == null || eVar.f688c == null)) {
            try {
                eVar.f688c.a(i2);
            } catch (Exception e2) {
                z.a("AvastComms", this.h, "SMS sender SMS callback error", e2);
            }
        }
        g();
        if (z) {
            c(eVar);
        } else {
            d(eVar);
        }
        if (!a() && (this.h instanceof AvastService)) {
            ((AvastService) this.h).b();
        }
    }

    public synchronized boolean a() {
        boolean z;
        boolean z2;
        boolean z3 = false;
        synchronized (this) {
            z.a("AvastComms", this.h, "Checking SMS sender sending state...");
            synchronized (this.g) {
                z.a("AvastComms", this.h, "Got inProgress lock");
                synchronized (this.f) {
                    z.a("AvastComms", this.h, "Got queue lock");
                    z = this.g.size() > 0;
                    if (this.f.size() > 0) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                }
                z.a("AvastComms", this.h, "Released queue lock");
            }
            z.a("AvastComms", this.h, "Released inProgress lock");
            z.a("AvastComms", this.h, "Checked SMS sender sending state (" + (z || z2) + ")");
            if (z || z2) {
                z3 = true;
            }
        }
        return z3;
    }

    public void b() {
        z.a("AvastComms", this.h, "SMS sender went online");
        this.o = true;
        f();
    }

    public void c() {
        z.a("AvastComms", this.h, "SMS sender went offline");
        this.o = false;
        d();
    }

    public synchronized void a(String str, List<bc> list, a aVar, boolean z, short s) {
        b(str, list, aVar, z, s);
    }

    public synchronized void a(String str, List<bc> list, a aVar, boolean z) {
        b(str, list, aVar, z, -1);
    }

    private synchronized void b(String str, List<bc> list, a aVar, boolean z, short s) {
        if (!AvastService.b(this.h)) {
            z.a("AvastComms", this.h, "SMS permission is not available, junking SMS");
            if (aVar != null) {
                try {
                    aVar.a(1);
                } catch (Exception e2) {
                }
            }
        } else if (str == null) {
            z.a("AvastComms", this.h, "SMS sender no target number given for SMS");
            if (aVar != null) {
                try {
                    aVar.a(1);
                } catch (Exception e3) {
                }
            }
        } else {
            for (bc next : list) {
                next.e = str;
                e eVar = new e(this, null);
                eVar.f687b = String.valueOf(new Date().getTime()) + String.valueOf(this.e.nextInt(100000));
                eVar.f686a = next;
                eVar.f688c = aVar;
                eVar.d = z;
                eVar.g = s;
                b(eVar);
                z.a("AvastComms", this.h, "SMS sender added SMS ID " + eVar.f687b + " to queue: " + next.f1212b);
            }
            g();
            if (!this.o) {
                d();
                if (aVar != null) {
                    try {
                        aVar.a(4);
                    } catch (Exception e4) {
                    }
                }
            } else {
                f();
            }
        }
    }

    public synchronized void a(bc bcVar, a aVar, boolean z, short s) {
        b(bcVar, aVar, z, s);
    }

    public synchronized void a(bc bcVar, a aVar, boolean z) {
        b(bcVar, aVar, z, -1);
    }

    private synchronized void b(bc bcVar, a aVar, boolean z, short s) {
        if (!AvastService.b(this.h)) {
            z.a("AvastComms", this.h, "SMS permission is not available, junking SMS");
            if (aVar != null) {
                try {
                    aVar.a(1);
                } catch (Exception e2) {
                }
            }
        } else if (bcVar.e == null) {
            z.a("AvastComms", this.h, "SMS sender no target number given for SMS");
            if (aVar != null) {
                try {
                    aVar.a(1);
                } catch (Exception e3) {
                }
            }
        } else {
            e eVar = new e(this, null);
            eVar.f687b = String.valueOf(new Date().getTime()) + String.valueOf(this.e.nextInt(100000));
            eVar.f686a = bcVar;
            eVar.f688c = aVar;
            eVar.d = z;
            eVar.g = s;
            b(eVar);
            z.a("AvastComms", this.h, "SMS sender added SMS ID " + eVar.f687b + " to queue: " + bcVar.f1212b);
            g();
            if (!this.o) {
                d();
                if (aVar != null) {
                    try {
                        aVar.a(4);
                    } catch (Exception e4) {
                    }
                }
            } else {
                f();
            }
        }
    }

    private void b(e eVar) {
        synchronized (this.f) {
            z.a("AvastComms", this.h, "SMS sender is queueing SMS traffic...");
            this.f.add(eVar);
            Collections.sort(this.f);
            z.a("AvastComms", this.h, "SMS sender queued SMS traffic (length " + this.f.size() + ")");
        }
    }

    private void f() {
        boolean z = false;
        synchronized (this.f) {
            if (this.f.size() > 0 && this.o) {
                z = true;
            }
        }
        if (z) {
            synchronized (this.m) {
                if (this.k != null) {
                    if (this.k.isAlive()) {
                        this.k.interrupt();
                        return;
                    }
                    this.k = null;
                }
                z.a("AvastComms", this.h, "SMS sender is starting SMS thread...");
                this.l = false;
                this.k = new Thread(new c(this));
                this.k.start();
                z.a("AvastComms", this.h, "Started SMS sender thread");
            }
        }
    }

    public void d() {
        int size;
        synchronized (this.f) {
            size = this.f.size();
        }
        if (size == 0 || !this.o) {
            synchronized (this.m) {
                if (this.k != null) {
                    z.a("AvastComms", this.h, "Stopping SMS sender thread...");
                    this.l = true;
                    try {
                        if (this.k.isAlive()) {
                            this.k.interrupt();
                            this.k.join();
                        }
                    } catch (Exception e2) {
                    }
                    this.l = false;
                    this.k = null;
                    z.a("AvastComms", this.h, "Stopped SMS sender thread");
                }
            }
        }
    }

    public void e() {
        synchronized (this.f682a) {
            if (this.j != null) {
                try {
                    this.h.unregisterReceiver(this.j);
                } catch (Exception e2) {
                }
                this.j = null;
            }
            if (this.i != null) {
                try {
                    this.h.unregisterReceiver(this.i);
                } catch (Exception e3) {
                }
                this.i = null;
            }
        }
        if (this.n != null) {
            this.n.listen(this, 0);
        }
        new Thread(new d(this)).start();
    }

    private void g() {
        LinkedList linkedList = new LinkedList();
        synchronized (this.f) {
            linkedList.addAll(this.f);
        }
        int i2 = 1;
        Iterator it = linkedList.iterator();
        while (true) {
            int i3 = i2;
            if (it.hasNext()) {
                e eVar = (e) it.next();
                if (eVar.f688c != null) {
                    try {
                        eVar.f688c.b(i3);
                    } catch (Exception e2) {
                        z.a("AvastComms", this.h, "SMS sender callback error", e2);
                    }
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.e, int, boolean):void
     arg types: [com.avast.android.generic.g.a.e, int, int]
     candidates:
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.e, java.lang.String, java.lang.String):void
      com.avast.android.generic.g.a.b.a(byte[], int, int):byte[]
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean):void
      com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.e, int, boolean):void */
    /* access modifiers changed from: private */
    public void h() {
        try {
            synchronized (this.g) {
                synchronized (this.f) {
                    e poll = this.f.poll();
                    if (poll != null) {
                        bc bcVar = poll.f686a;
                        this.g.add(poll);
                        g();
                        try {
                            a(poll, bcVar.e, bcVar.f1212b);
                        } catch (Exception e2) {
                            z.a("AvastComms", this.h, "SMS sender sending error", e2);
                            a(poll, 0, false);
                        }
                    }
                }
            }
        } catch (Exception e3) {
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:65:0x01c0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.avast.android.generic.g.a.e r17, java.lang.String r18, java.lang.String r19) {
        /*
            r16 = this;
            java.lang.String r1 = "AvastComms"
            r0 = r16
            android.content.Context r2 = r0.h
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "SMS sender is sending outgoing SMS to "
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r18
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r4 = ": "
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r19
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.avast.android.generic.util.z.a(r1, r2, r3)
            if (r17 == 0) goto L_0x0039
            r0 = r17
            com.avast.android.generic.g.a.a r1 = r0.f688c
            if (r1 == 0) goto L_0x0039
            r0 = r17
            com.avast.android.generic.g.a.a r1 = r0.f688c
            r1.a()
        L_0x0039:
            r0 = r17
            java.lang.String r1 = r0.f687b
            java.lang.String r1 = r1.toString()
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "com.avast.android.antitheft.SMS_SENT"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "avsms://sent/"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r1)
            java.lang.String r4 = r4.toString()
            android.net.Uri r4 = android.net.Uri.parse(r4)
            r2.<init>(r3, r4)
            java.lang.String r3 = "smsId"
            r2.putExtra(r3, r1)
            r0 = r16
            android.content.Context r1 = r0.h
            r3 = 0
            r4 = 0
            android.app.PendingIntent r5 = android.app.PendingIntent.getBroadcast(r1, r3, r2, r4)
            r0 = r16
            android.content.Context r1 = r0.h
            r2 = 0
            android.content.Intent r3 = new android.content.Intent
            java.lang.String r4 = "com.avast.android.antitheft.PART_SMS_SENT"
            r3.<init>(r4)
            r4 = 0
            android.app.PendingIntent r3 = android.app.PendingIntent.getBroadcast(r1, r2, r3, r4)
            android.telephony.SmsManager r1 = android.telephony.SmsManager.getDefault()
            r0 = r17
            short r2 = r0.g
            if (r2 > 0) goto L_0x0102
            r0 = r19
            java.util.ArrayList r4 = r1.divideMessage(r0)
            int r2 = r4.size()
            r6 = 1
            if (r2 > r6) goto L_0x00a0
            r3 = 0
            r6 = 0
            r2 = r18
            r4 = r19
            r1.sendTextMessage(r2, r3, r4, r5, r6)
        L_0x009f:
            return
        L_0x00a0:
            r0 = r16
            com.avast.android.generic.ab r2 = r0.f683b
            boolean r2 = r2.I()
            if (r2 != 0) goto L_0x00d1
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            r2 = 0
        L_0x00b0:
            int r6 = r4.size()
            if (r2 >= r6) goto L_0x00c8
            int r6 = r4.size()
            int r6 = r6 + -1
            if (r2 >= r6) goto L_0x00c4
            r7.add(r3)
        L_0x00c1:
            int r2 = r2 + 1
            goto L_0x00b0
        L_0x00c4:
            r7.add(r5)
            goto L_0x00c1
        L_0x00c8:
            r3 = 0
            r6 = 0
            r2 = r18
            r5 = r7
            r1.sendMultipartTextMessage(r2, r3, r4, r5, r6)
            goto L_0x009f
        L_0x00d1:
            r2 = 0
        L_0x00d2:
            int r3 = r4.size()
            if (r2 >= r3) goto L_0x009f
            int r3 = r4.size()
            int r3 = r3 + -1
            if (r2 >= r3) goto L_0x00f2
            r8 = 0
            java.lang.Object r9 = r4.get(r2)
            java.lang.String r9 = (java.lang.String) r9
            r10 = 0
            r11 = 0
            r6 = r1
            r7 = r18
            r6.sendTextMessage(r7, r8, r9, r10, r11)
        L_0x00ef:
            int r2 = r2 + 1
            goto L_0x00d2
        L_0x00f2:
            r8 = 0
            java.lang.Object r9 = r4.get(r2)
            java.lang.String r9 = (java.lang.String) r9
            r11 = 0
            r6 = r1
            r7 = r18
            r10 = r5
            r6.sendTextMessage(r7, r8, r9, r10, r11)
            goto L_0x00ef
        L_0x0102:
            java.lang.String r2 = "UTF-8"
            r0 = r19
            byte[] r14 = r0.getBytes(r2)
            r3 = 0
            int r2 = r14.length
            int r2 = r2 / 120
            int r2 = r2 + 1
            java.lang.Integer r4 = java.lang.Integer.valueOf(r2)
            byte r15 = r4.byteValue()
            r4 = 10
            if (r2 > r4) goto L_0x011e
            if (r2 > 0) goto L_0x012e
        L_0x011e:
            r0 = r17
            com.avast.android.generic.g.a.a r1 = r0.f688c
            if (r1 == 0) goto L_0x009f
            r0 = r17
            com.avast.android.generic.g.a.a r1 = r0.f688c
            r2 = 1
            r1.a(r2)
            goto L_0x009f
        L_0x012e:
            short r2 = com.avast.android.generic.g.a.b.f681c
            java.security.SecureRandom r4 = com.avast.android.generic.g.a.b.d
            int r4 = r4.nextInt()
            int r2 = r2 + r4
            short r2 = (short) r2
            com.avast.android.generic.g.a.b.f681c = r2
            r2 = 0
        L_0x013b:
            int r4 = r14.length
            if (r2 >= r4) goto L_0x009f
            int r3 = r3 + 1
            int r4 = r2 + 120
            int r6 = r14.length
            if (r4 < r6) goto L_0x01a3
            int r4 = android.os.Build.VERSION.SDK_INT
            r6 = 9
            if (r4 < r6) goto L_0x019b
            int r4 = r14.length
            byte[] r4 = java.util.Arrays.copyOfRange(r14, r2, r4)
        L_0x0150:
            r6 = 1
        L_0x0151:
            r7 = 4
            byte[] r8 = new byte[r7]
            r7 = 0
            short r9 = com.avast.android.generic.g.a.b.f681c
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9
            r8[r7] = r9
            r7 = 1
            short r9 = com.avast.android.generic.g.a.b.f681c
            int r9 = r9 >> 8
            r9 = r9 & 255(0xff, float:3.57E-43)
            byte r9 = (byte) r9
            r8[r7] = r9
            r7 = 2
            java.lang.Integer r9 = java.lang.Integer.valueOf(r3)
            byte r9 = r9.byteValue()
            r8[r7] = r9
            r7 = 3
            r8[r7] = r15
            r7 = 0
            java.io.ByteArrayOutputStream r13 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x01bc }
            r13.<init>()     // Catch:{ all -> 0x01bc }
            r13.write(r8)     // Catch:{ all -> 0x01c4 }
            r13.write(r4)     // Catch:{ all -> 0x01c4 }
            r8 = 0
            r0 = r17
            short r9 = r0.g     // Catch:{ all -> 0x01c4 }
            byte[] r10 = r13.toByteArray()     // Catch:{ all -> 0x01c4 }
            if (r6 == 0) goto L_0x01ba
            r11 = r5
        L_0x018c:
            r12 = 0
            r6 = r1
            r7 = r18
            r6.sendDataMessage(r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x01c4 }
            if (r13 == 0) goto L_0x0198
            r13.close()
        L_0x0198:
            int r2 = r2 + 120
            goto L_0x013b
        L_0x019b:
            int r4 = r14.length
            r0 = r16
            byte[] r4 = r0.a(r14, r2, r4)
            goto L_0x0150
        L_0x01a3:
            int r4 = android.os.Build.VERSION.SDK_INT
            r6 = 9
            if (r4 < r6) goto L_0x01b1
            int r4 = r2 + 120
            byte[] r4 = java.util.Arrays.copyOfRange(r14, r2, r4)
        L_0x01af:
            r6 = 0
            goto L_0x0151
        L_0x01b1:
            int r4 = r2 + 120
            r0 = r16
            byte[] r4 = r0.a(r14, r2, r4)
            goto L_0x01af
        L_0x01ba:
            r11 = 0
            goto L_0x018c
        L_0x01bc:
            r1 = move-exception
            r2 = r7
        L_0x01be:
            if (r2 == 0) goto L_0x01c3
            r2.close()
        L_0x01c3:
            throw r1
        L_0x01c4:
            r1 = move-exception
            r2 = r13
            goto L_0x01be
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.g.a.b.a(com.avast.android.generic.g.a.e, java.lang.String, java.lang.String):void");
    }

    private boolean c(e eVar) {
        boolean e2;
        synchronized (this.g) {
            synchronized (this.f) {
                this.g.remove(eVar);
                e2 = e(eVar);
            }
        }
        return e2;
    }

    private boolean d(e eVar) {
        boolean e2;
        synchronized (this.f) {
            synchronized (this.g) {
                this.f.remove(eVar);
                e2 = e(eVar);
            }
        }
        return e2;
    }

    private boolean e(e eVar) {
        if (eVar.d) {
            eVar.e++;
            if (eVar.e <= 3) {
                long j2 = (long) (eVar.e * eVar.e * 2000);
                z.a("AvastComms", this.h, "SMS sender is requeueing reliable SMS descriptor (retry count " + eVar.e + ", timeout " + j2 + "ms)...");
                eVar.f = j2 + new Date().getTime();
                b(eVar);
                if (!this.o) {
                    d();
                    if (eVar.f688c != null) {
                        try {
                            eVar.f688c.a(4);
                        } catch (Exception e2) {
                        }
                    }
                } else {
                    f();
                }
                return false;
            }
            z.a("AvastComms", this.h, "SMS sender is junking reliable SMS descriptor because of too much failures");
            return true;
        }
        z.a("AvastComms", this.h, "SMS sender is junking non-reliable SMS descriptor");
        return true;
    }

    private byte[] a(byte[] bArr, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException();
        }
        int length = bArr.length;
        if (i2 < 0 || i2 > length) {
            throw new ArrayIndexOutOfBoundsException();
        }
        int i4 = i3 - i2;
        int min = Math.min(i4, length - i2);
        byte[] bArr2 = new byte[i4];
        System.arraycopy(bArr, i2, bArr2, 0, min);
        return bArr2;
    }
}
