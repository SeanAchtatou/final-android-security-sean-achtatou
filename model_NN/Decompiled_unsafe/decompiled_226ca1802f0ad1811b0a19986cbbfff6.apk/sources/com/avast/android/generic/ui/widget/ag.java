package com.avast.android.generic.ui.widget;

import android.app.TimePickerDialog;
import android.widget.TimePicker;

/* compiled from: TimeButtonRow */
class ag implements TimePickerDialog.OnTimeSetListener {

    /* renamed from: a  reason: collision with root package name */
    boolean f1138a = false;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ af f1139b;

    ag(af afVar) {
        this.f1139b = afVar;
    }

    public void onTimeSet(TimePicker timePicker, int i, int i2) {
        if (!this.f1138a) {
            this.f1139b.f1137b.a((i * 60) + i2, true, true);
            this.f1138a = true;
        }
    }
}
