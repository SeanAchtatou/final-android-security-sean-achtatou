package com.avast.android.generic.i;

import android.content.Context;
import com.avast.d.b;
import com.avast.d.c;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: InternalKeyStorage */
public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private static a f732a = null;

    /* renamed from: b  reason: collision with root package name */
    private final AtomicReference<c> f733b = new AtomicReference<>();

    /* renamed from: c  reason: collision with root package name */
    private final Context f734c;

    private a(Context context) {
        this.f734c = context;
    }

    public static synchronized a a(Context context) {
        a aVar;
        synchronized (a.class) {
            if (f732a == null) {
                f732a = new a(context);
            }
            aVar = f732a;
        }
        return aVar;
    }

    public c a() {
        c cVar = this.f733b.get();
        boolean z = false;
        if (cVar == null) {
            cVar = b();
            z = true;
        }
        if (cVar == null || cVar.d()) {
            if (cVar != null) {
            }
            return null;
        } else if (!z) {
            return cVar;
        } else {
            this.f733b.set(cVar);
            return cVar;
        }
    }

    public void a(c cVar) {
        b(cVar);
        this.f733b.set(cVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0084  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.avast.d.c b() {
        /*
            r9 = this;
            r1 = 0
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            android.content.Context r3 = r9.f734c
            java.lang.String r4 = "streamback"
            r5 = 0
            java.io.File r3 = r3.getDir(r4, r5)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = java.io.File.separator
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = "sb.key"
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.<init>(r2)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ StreamCorruptedException -> 0x0060, FileNotFoundException -> 0x007a, IOException -> 0x0089, all -> 0x008f }
            r2.<init>(r0)     // Catch:{ StreamCorruptedException -> 0x0060, FileNotFoundException -> 0x007a, IOException -> 0x0089, all -> 0x008f }
            java.io.ObjectInputStream r3 = new java.io.ObjectInputStream     // Catch:{ StreamCorruptedException -> 0x00a0, FileNotFoundException -> 0x009b, IOException -> 0x0096, all -> 0x0093 }
            r3.<init>(r2)     // Catch:{ StreamCorruptedException -> 0x00a0, FileNotFoundException -> 0x009b, IOException -> 0x0096, all -> 0x0093 }
            int r0 = r3.readInt()     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            byte[] r4 = new byte[r0]     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            r3.read(r4)     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            int r0 = r3.readInt()     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            byte[] r5 = new byte[r0]     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            r3.read(r5)     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            long r6 = r3.readLong()     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            com.avast.d.c r0 = new com.avast.d.c     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            com.google.protobuf.ByteString r4 = com.google.protobuf.ByteString.copyFrom(r4)     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            com.google.protobuf.ByteString r5 = com.google.protobuf.ByteString.copyFrom(r5)     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            r0.<init>(r4, r5, r6)     // Catch:{ StreamCorruptedException -> 0x00a5, FileNotFoundException -> 0x009e, IOException -> 0x0099 }
            if (r3 == 0) goto L_0x005a
            r3.close()
        L_0x005a:
            if (r2 == 0) goto L_0x005f
            r2.close()
        L_0x005f:
            return r0
        L_0x0060:
            r0 = move-exception
            r2 = r1
        L_0x0062:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x006c }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x006c }
            r3.<init>(r0)     // Catch:{ all -> 0x006c }
            throw r3     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
            r3 = r2
            r2 = r1
        L_0x006f:
            if (r3 == 0) goto L_0x0074
            r3.close()
        L_0x0074:
            if (r2 == 0) goto L_0x0079
            r2.close()
        L_0x0079:
            throw r0
        L_0x007a:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x007d:
            if (r3 == 0) goto L_0x0082
            r3.close()
        L_0x0082:
            if (r2 == 0) goto L_0x0087
            r2.close()
        L_0x0087:
            r0 = r1
            goto L_0x005f
        L_0x0089:
            r0 = move-exception
            r2 = r1
            r3 = r1
        L_0x008c:
            throw r0     // Catch:{ all -> 0x008d }
        L_0x008d:
            r0 = move-exception
            goto L_0x006f
        L_0x008f:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x006f
        L_0x0093:
            r0 = move-exception
            r3 = r1
            goto L_0x006f
        L_0x0096:
            r0 = move-exception
            r3 = r1
            goto L_0x008c
        L_0x0099:
            r0 = move-exception
            goto L_0x008c
        L_0x009b:
            r0 = move-exception
            r3 = r1
            goto L_0x007d
        L_0x009e:
            r0 = move-exception
            goto L_0x007d
        L_0x00a0:
            r0 = move-exception
            r8 = r2
            r2 = r1
            r1 = r8
            goto L_0x0062
        L_0x00a5:
            r0 = move-exception
            r1 = r2
            r2 = r3
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.i.a.b():com.avast.d.c");
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0072  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(com.avast.d.c r7) {
        /*
            r6 = this;
            r2 = 0
            if (r7 != 0) goto L_0x000b
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Key is null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.io.File r0 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            android.content.Context r3 = r6.f734c
            java.lang.String r4 = "streamback"
            r5 = 0
            java.io.File r3 = r3.getDir(r4, r5)
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = java.io.File.separator
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r3 = "sb.key"
            java.lang.StringBuilder r1 = r1.append(r3)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            boolean r1 = r0.exists()
            if (r1 == 0) goto L_0x003b
            r0.delete()
        L_0x003b:
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00c0, IOException -> 0x00b9, all -> 0x00b2 }
            r1.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00c0, IOException -> 0x00b9, all -> 0x00b2 }
            java.io.ObjectOutputStream r3 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00bd, all -> 0x00b6 }
            r3.<init>(r1)     // Catch:{ FileNotFoundException -> 0x00c3, IOException -> 0x00bd, all -> 0x00b6 }
            com.google.protobuf.ByteString r0 = r7.a()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            byte[] r0 = r0.toByteArray()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            if (r0 == 0) goto L_0x0052
            int r2 = r0.length     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            if (r2 != 0) goto L_0x0076
        L_0x0052:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            java.lang.String r2 = "Invalid id to write"
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            throw r0     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
        L_0x005a:
            r0 = move-exception
            r2 = r3
        L_0x005c:
            java.io.IOException r3 = new java.io.IOException     // Catch:{ all -> 0x0066 }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0066 }
            r3.<init>(r0)     // Catch:{ all -> 0x0066 }
            throw r3     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
            r3 = r2
        L_0x0068:
            if (r3 == 0) goto L_0x0070
            r3.flush()
            r3.close()
        L_0x0070:
            if (r1 == 0) goto L_0x0075
            r1.close()
        L_0x0075:
            throw r0
        L_0x0076:
            int r2 = r0.length     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            r3.writeInt(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            r3.write(r0)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            com.google.protobuf.ByteString r0 = r7.b()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            byte[] r0 = r0.toByteArray()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            if (r0 == 0) goto L_0x008a
            int r2 = r0.length     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            if (r2 != 0) goto L_0x0096
        L_0x008a:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            java.lang.String r2 = "Invalid key to write"
            r0.<init>(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            throw r0     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
        L_0x0092:
            r0 = move-exception
        L_0x0093:
            throw r0     // Catch:{ all -> 0x0094 }
        L_0x0094:
            r0 = move-exception
            goto L_0x0068
        L_0x0096:
            int r2 = r0.length     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            r3.writeInt(r2)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            r3.write(r0)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            long r4 = r7.c()     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            r3.writeLong(r4)     // Catch:{ FileNotFoundException -> 0x005a, IOException -> 0x0092 }
            if (r3 == 0) goto L_0x00ac
            r3.flush()
            r3.close()
        L_0x00ac:
            if (r1 == 0) goto L_0x00b1
            r1.close()
        L_0x00b1:
            return
        L_0x00b2:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0068
        L_0x00b6:
            r0 = move-exception
            r3 = r2
            goto L_0x0068
        L_0x00b9:
            r0 = move-exception
            r1 = r2
            r3 = r2
            goto L_0x0093
        L_0x00bd:
            r0 = move-exception
            r3 = r2
            goto L_0x0093
        L_0x00c0:
            r0 = move-exception
            r1 = r2
            goto L_0x005c
        L_0x00c3:
            r0 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.i.a.b(com.avast.d.c):void");
    }
}
