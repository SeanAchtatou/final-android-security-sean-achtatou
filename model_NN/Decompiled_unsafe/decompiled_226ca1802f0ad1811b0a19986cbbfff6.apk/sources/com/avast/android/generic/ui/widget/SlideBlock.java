package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.avast.android.generic.q;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.b.a;

public class SlideBlock extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1113a = false;

    /* renamed from: b  reason: collision with root package name */
    private CharSequence f1114b = "Title";

    /* renamed from: c  reason: collision with root package name */
    private int f1115c = q.xml_menu_arrow_up;
    private int d = q.xml_menu_arrow_down;
    private View e;
    private LinearLayout f;
    private a g;
    private ImageView h;
    private TextView i;
    private y j;

    public SlideBlock(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a(context, attributeSet);
    }

    public SlideBlock(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public SlideBlock(Context context) {
        super(context);
    }

    private void a(Context context, AttributeSet attributeSet) {
        int attributeResourceValue = attributeSet.getAttributeResourceValue("http://schemas.android.com/apk/res/android", "text", 0);
        if (attributeResourceValue > 0) {
            this.f1114b = context.getText(attributeResourceValue);
        }
    }

    /* access modifiers changed from: protected */
    public void onFinishInflate() {
        super.onFinishInflate();
        setOrientation(1);
        this.e = inflate(getContext(), t.widget_slide_block_header, null);
        this.f = new LinearLayout(getContext());
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.f.setOrientation(1);
        while (getChildCount() > 0) {
            View childAt = getChildAt(0);
            removeView(childAt);
            this.f.addView(childAt);
        }
        addView(this.e);
        addView(this.f);
        c();
        this.h = (ImageView) this.e.findViewById(r.icon);
        this.i = (TextView) this.e.findViewById(r.text);
        if (!(this.h == null || this.i == null)) {
            this.h.setImageResource(this.f1113a ? this.f1115c : this.d);
            this.i.setText(this.f1114b);
        }
        this.e.setOnClickListener(new x(this));
    }

    private void c() {
        this.g = new a(this.f);
    }

    public void a(CharSequence charSequence) {
        this.f1114b = charSequence;
        if (this.i != null) {
            this.i.setText(charSequence);
        }
    }

    public void a(View view) {
        this.f.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.f.removeAllViews();
        this.f.addView(view);
        this.f1113a = false;
        c();
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.e.setEnabled(z);
        this.i.setEnabled(z);
        this.h.setEnabled(z);
    }

    public void a(y yVar) {
        this.j = yVar;
    }

    public void a() {
        if (!this.f1113a) {
            this.g.a(false);
            this.f1113a = true;
            this.h.setImageResource(this.f1115c);
            this.f.startAnimation(this.g);
            this.f.invalidate();
            if (this.j != null) {
                this.j.a(this);
            }
        }
    }

    public void b() {
        if (this.f1113a) {
            this.g.a(true);
            this.f1113a = false;
            this.h.setImageResource(this.d);
            this.f.startAnimation(this.g);
            this.f.invalidate();
            if (this.j != null) {
                this.j.b(this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.f1113a) {
            b();
        } else {
            a();
        }
    }
}
