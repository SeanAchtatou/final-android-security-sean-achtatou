package com.avast.android.generic.d;

import android.support.v4.app.NotificationCompat;

/* compiled from: Base64 */
public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f654a = System.getProperty("line.separator");

    /* renamed from: b  reason: collision with root package name */
    private static char[] f655b = new char[64];

    /* renamed from: c  reason: collision with root package name */
    private static byte[] f656c = new byte[NotificationCompat.FLAG_HIGH_PRIORITY];

    static {
        char c2 = 'A';
        int i = 0;
        while (c2 <= 'Z') {
            f655b[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = 'a';
        while (c3 <= 'z') {
            f655b[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        char c4 = '0';
        while (c4 <= '9') {
            f655b[i] = c4;
            c4 = (char) (c4 + 1);
            i++;
        }
        int i2 = i + 1;
        f655b[i] = '+';
        int i3 = i2 + 1;
        f655b[i2] = '/';
        for (int i4 = 0; i4 < f656c.length; i4++) {
            f656c[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            f656c[f655b[i5]] = (byte) i5;
        }
    }

    public static char[] a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static char[] a(byte[] bArr, int i, int i2) {
        byte b2;
        int i3;
        byte b3;
        char c2;
        char c3;
        int i4 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i5 = i + i2;
        int i6 = 0;
        while (i < i5) {
            int i7 = i + 1;
            byte b4 = bArr[i] & 255;
            if (i7 < i5) {
                b2 = bArr[i7] & 255;
                i7++;
            } else {
                b2 = 0;
            }
            if (i7 < i5) {
                i3 = i7 + 1;
                b3 = bArr[i7] & 255;
            } else {
                i3 = i7;
                b3 = 0;
            }
            int i8 = b4 >>> 2;
            int i9 = ((b4 & 3) << 4) | (b2 >>> 4);
            int i10 = ((b2 & 15) << 2) | (b3 >>> 6);
            byte b5 = b3 & 63;
            int i11 = i6 + 1;
            cArr[i6] = f655b[i8];
            int i12 = i11 + 1;
            cArr[i11] = f655b[i9];
            if (i12 < i4) {
                c2 = f655b[i10];
            } else {
                c2 = '=';
            }
            cArr[i12] = c2;
            int i13 = i12 + 1;
            if (i13 < i4) {
                c3 = f655b[b5];
            } else {
                c3 = '=';
            }
            cArr[i13] = c3;
            i6 = i13 + 1;
            i = i3;
        }
        return cArr;
    }
}
