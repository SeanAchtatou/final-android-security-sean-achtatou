package com.avast.android.generic.b.a;

import com.avast.e.a.az;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.List;

/* compiled from: BadNewsProto */
public final class c extends GeneratedMessageLite implements g {

    /* renamed from: a  reason: collision with root package name */
    private static final c f599a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f600b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public d f601c;
    /* access modifiers changed from: private */
    public az d;
    /* access modifiers changed from: private */
    public ByteString e;
    /* access modifiers changed from: private */
    public List<k> f;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public ByteString h;
    /* access modifiers changed from: private */
    public List<ByteString> i;
    /* access modifiers changed from: private */
    public long j;
    private byte k;
    private int l;

    private c(f fVar) {
        super(fVar);
        this.k = -1;
        this.l = -1;
    }

    private c(boolean z) {
        this.k = -1;
        this.l = -1;
    }

    public static c a() {
        return f599a;
    }

    public boolean b() {
        return (this.f600b & 1) == 1;
    }

    public d c() {
        return this.f601c;
    }

    public boolean d() {
        return (this.f600b & 2) == 2;
    }

    public az e() {
        return this.d;
    }

    public boolean f() {
        return (this.f600b & 4) == 4;
    }

    public ByteString g() {
        return this.e;
    }

    public boolean h() {
        return (this.f600b & 8) == 8;
    }

    public int i() {
        return this.g;
    }

    public boolean j() {
        return (this.f600b & 16) == 16;
    }

    public ByteString k() {
        return this.h;
    }

    public List<ByteString> l() {
        return this.i;
    }

    public boolean m() {
        return (this.f600b & 32) == 32;
    }

    public long n() {
        return this.j;
    }

    private void r() {
        this.f601c = d.ERROR;
        this.d = az.a();
        this.e = ByteString.EMPTY;
        this.f = Collections.emptyList();
        this.g = 0;
        this.h = ByteString.EMPTY;
        this.i = Collections.emptyList();
        this.j = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.k;
        if (b2 == -1) {
            this.k = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f600b & 1) == 1) {
            codedOutputStream.writeEnum(1, this.f601c.getNumber());
        }
        if ((this.f600b & 2) == 2) {
            codedOutputStream.writeMessage(2, this.d);
        }
        if ((this.f600b & 4) == 4) {
            codedOutputStream.writeBytes(3, this.e);
        }
        for (int i2 = 0; i2 < this.f.size(); i2++) {
            codedOutputStream.writeMessage(4, this.f.get(i2));
        }
        if ((this.f600b & 8) == 8) {
            codedOutputStream.writeInt32(5, this.g);
        }
        if ((this.f600b & 16) == 16) {
            codedOutputStream.writeBytes(6, this.h);
        }
        for (int i3 = 0; i3 < this.i.size(); i3++) {
            codedOutputStream.writeBytes(7, this.i.get(i3));
        }
        if ((this.f600b & 32) == 32) {
            codedOutputStream.writeInt64(8, this.j);
        }
    }

    public int getSerializedSize() {
        int i2;
        int i3 = 0;
        int i4 = this.l;
        if (i4 == -1) {
            if ((this.f600b & 1) == 1) {
                i2 = CodedOutputStream.computeEnumSize(1, this.f601c.getNumber()) + 0;
            } else {
                i2 = 0;
            }
            if ((this.f600b & 2) == 2) {
                i2 += CodedOutputStream.computeMessageSize(2, this.d);
            }
            if ((this.f600b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(3, this.e);
            }
            int i5 = i2;
            for (int i6 = 0; i6 < this.f.size(); i6++) {
                i5 += CodedOutputStream.computeMessageSize(4, this.f.get(i6));
            }
            if ((this.f600b & 8) == 8) {
                i5 += CodedOutputStream.computeInt32Size(5, this.g);
            }
            if ((this.f600b & 16) == 16) {
                i5 += CodedOutputStream.computeBytesSize(6, this.h);
            }
            int i7 = 0;
            while (i3 < this.i.size()) {
                i3++;
                i7 = CodedOutputStream.computeBytesSizeNoTag(this.i.get(i3)) + i7;
            }
            i4 = i5 + i7 + (l().size() * 1);
            if ((this.f600b & 32) == 32) {
                i4 += CodedOutputStream.computeInt64Size(8, this.j);
            }
            this.l = i4;
        }
        return i4;
    }

    public static f o() {
        return f.i();
    }

    /* renamed from: p */
    public f newBuilderForType() {
        return o();
    }

    public static f a(c cVar) {
        return o().mergeFrom(cVar);
    }

    /* renamed from: q */
    public f toBuilder() {
        return a(this);
    }

    static {
        f599a.r();
    }
}
