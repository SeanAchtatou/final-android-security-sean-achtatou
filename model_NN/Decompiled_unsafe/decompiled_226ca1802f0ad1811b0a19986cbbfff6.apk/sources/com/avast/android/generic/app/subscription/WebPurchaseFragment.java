package com.avast.android.generic.app.subscription;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.util.z;
import java.util.HashMap;

public class WebPurchaseFragment extends TrackedMultiToolFragment {

    /* renamed from: a  reason: collision with root package name */
    public static String f517a;

    /* renamed from: b  reason: collision with root package name */
    private WebView f518b;

    /* renamed from: c  reason: collision with root package name */
    private String f519c;

    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        if (getActivity() != null && (intent = getActivity().getIntent()) != null && intent.hasExtra("paymentProviderUrl")) {
            this.f519c = intent.getStringExtra("paymentProviderUrl");
            if (TextUtils.isEmpty(this.f519c)) {
                this.f519c = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @SuppressLint({"SetJavaScriptEnabled"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(t.fragment_web_purchase, viewGroup, false);
        this.f518b = (WebView) inflate.findViewById(r.r_web_view);
        this.f518b.setVerticalScrollBarEnabled(true);
        this.f518b.setHorizontalScrollBarEnabled(true);
        this.f518b.getSettings().setJavaScriptEnabled(true);
        this.f518b.getSettings().setSaveFormData(false);
        this.f518b.getSettings().setSavePassword(false);
        this.f518b.getSettings().setCacheMode(2);
        this.f518b.addJavascriptInterface(new af(this), "WP_RESPONSE_CODE");
        this.f518b.setWebViewClient(new ag(this));
        z.a("AvastGenericLic", "Purchase web view pushed to start loading");
        if (Build.VERSION.SDK_INT >= 8) {
            HashMap hashMap = new HashMap(2);
            hashMap.put("Pragma", "no-cache");
            hashMap.put("Cache-Control", "no-cache");
            this.f518b.loadUrl(this.f519c, hashMap);
        } else {
            this.f518b.loadUrl(this.f519c);
        }
        return inflate;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
    }

    public void onResume() {
        super.onResume();
    }

    public void onDestroy() {
        z.a("AvastGenericLic", "Web purchase page destroyed");
        if (isAdded() && this.f518b != null) {
            this.f518b.stopLoading();
        }
        super.onDestroy();
    }

    /* access modifiers changed from: private */
    public void d() {
        getActivity().setResult(65006);
        getActivity().finish();
    }

    public String c() {
        return "subscription/webPurchase";
    }
}
