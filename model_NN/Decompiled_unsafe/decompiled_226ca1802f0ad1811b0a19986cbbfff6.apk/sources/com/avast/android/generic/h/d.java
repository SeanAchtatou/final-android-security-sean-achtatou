package com.avast.android.generic.h;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.actionbarsherlock.R;
import com.actionbarsherlock.view.Menu;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: CommunityIqProto */
public final class d extends GeneratedMessageLite.Builder<c, d> implements k {

    /* renamed from: a  reason: collision with root package name */
    private int f703a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f704b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private e f705c = e.MOBILE_SECURITY_INSTALL;
    private ByteString d = ByteString.EMPTY;
    private ByteString e = ByteString.EMPTY;
    private ByteString f = ByteString.EMPTY;
    private ByteString g = ByteString.EMPTY;
    private ByteString h = ByteString.EMPTY;
    private ByteString i = ByteString.EMPTY;
    private ByteString j = ByteString.EMPTY;
    private g k = g.UPDATE_CHECK_RESULT_UPDATE_AVAILABLE;
    private i l = i.UPDATE_RESULT_UP_TO_DATE;
    private ByteString m = ByteString.EMPTY;
    private ByteString n = ByteString.EMPTY;
    private ByteString o = ByteString.EMPTY;
    private ByteString p = ByteString.EMPTY;
    private long q;
    private ByteString r = ByteString.EMPTY;
    private long s;
    private ByteString t = ByteString.EMPTY;
    private ByteString u = ByteString.EMPTY;

    private d() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static d g() {
        return new d();
    }

    /* renamed from: a */
    public d clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public c getDefaultInstanceForType() {
        return c.a();
    }

    /* renamed from: c */
    public c build() {
        c d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public c d() {
        c cVar = new c(this);
        int i2 = this.f703a;
        int i3 = 0;
        if ((i2 & 1) == 1) {
            i3 = 1;
        }
        ByteString unused = cVar.f702c = this.f704b;
        if ((i2 & 2) == 2) {
            i3 |= 2;
        }
        e unused2 = cVar.d = this.f705c;
        if ((i2 & 4) == 4) {
            i3 |= 4;
        }
        ByteString unused3 = cVar.e = this.d;
        if ((i2 & 8) == 8) {
            i3 |= 8;
        }
        ByteString unused4 = cVar.f = this.e;
        if ((i2 & 16) == 16) {
            i3 |= 16;
        }
        ByteString unused5 = cVar.g = this.f;
        if ((i2 & 32) == 32) {
            i3 |= 32;
        }
        ByteString unused6 = cVar.h = this.g;
        if ((i2 & 64) == 64) {
            i3 |= 64;
        }
        ByteString unused7 = cVar.i = this.h;
        if ((i2 & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            i3 |= NotificationCompat.FLAG_HIGH_PRIORITY;
        }
        ByteString unused8 = cVar.j = this.i;
        if ((i2 & 256) == 256) {
            i3 |= 256;
        }
        ByteString unused9 = cVar.k = this.j;
        if ((i2 & 512) == 512) {
            i3 |= 512;
        }
        g unused10 = cVar.l = this.k;
        if ((i2 & 1024) == 1024) {
            i3 |= 1024;
        }
        i unused11 = cVar.m = this.l;
        if ((i2 & 2048) == 2048) {
            i3 |= 2048;
        }
        ByteString unused12 = cVar.n = this.m;
        if ((i2 & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            i3 |= FragmentTransaction.TRANSIT_ENTER_MASK;
        }
        ByteString unused13 = cVar.o = this.n;
        if ((i2 & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            i3 |= FragmentTransaction.TRANSIT_EXIT_MASK;
        }
        ByteString unused14 = cVar.p = this.o;
        if ((i2 & 16384) == 16384) {
            i3 |= 16384;
        }
        ByteString unused15 = cVar.q = this.p;
        if ((i2 & 32768) == 32768) {
            i3 |= 32768;
        }
        long unused16 = cVar.r = this.q;
        if ((i2 & Menu.CATEGORY_CONTAINER) == 65536) {
            i3 |= Menu.CATEGORY_CONTAINER;
        }
        ByteString unused17 = cVar.s = this.r;
        if ((i2 & Menu.CATEGORY_SYSTEM) == 131072) {
            i3 |= Menu.CATEGORY_SYSTEM;
        }
        long unused18 = cVar.t = this.s;
        if ((i2 & Menu.CATEGORY_ALTERNATIVE) == 262144) {
            i3 |= Menu.CATEGORY_ALTERNATIVE;
        }
        ByteString unused19 = cVar.u = this.t;
        if ((i2 & 524288) == 524288) {
            i3 |= 524288;
        }
        ByteString unused20 = cVar.v = this.u;
        int unused21 = cVar.f701b = i3;
        return cVar;
    }

    /* renamed from: a */
    public d mergeFrom(c cVar) {
        if (cVar != c.a()) {
            if (cVar.b()) {
                a(cVar.c());
            }
            if (cVar.d()) {
                a(cVar.e());
            }
            if (cVar.f()) {
                b(cVar.g());
            }
            if (cVar.h()) {
                c(cVar.i());
            }
            if (cVar.j()) {
                d(cVar.k());
            }
            if (cVar.l()) {
                e(cVar.m());
            }
            if (cVar.n()) {
                f(cVar.o());
            }
            if (cVar.p()) {
                g(cVar.q());
            }
            if (cVar.r()) {
                h(cVar.s());
            }
            if (cVar.t()) {
                a(cVar.u());
            }
            if (cVar.v()) {
                a(cVar.w());
            }
            if (cVar.x()) {
                i(cVar.y());
            }
            if (cVar.z()) {
                j(cVar.A());
            }
            if (cVar.B()) {
                k(cVar.C());
            }
            if (cVar.D()) {
                l(cVar.E());
            }
            if (cVar.F()) {
                a(cVar.G());
            }
            if (cVar.H()) {
                m(cVar.I());
            }
            if (cVar.J()) {
                b(cVar.K());
            }
            if (cVar.L()) {
                n(cVar.M());
            }
            if (cVar.N()) {
                o(cVar.O());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public d mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f703a |= 1;
                    this.f704b = codedInputStream.readBytes();
                    break;
                case 16:
                    e a2 = e.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f703a |= 2;
                        this.f705c = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f703a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_searchViewSearchIcon:
                    this.f703a |= 8;
                    this.e = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textAppearanceSearchResultTitle:
                    this.f703a |= 16;
                    this.f = codedInputStream.readBytes();
                    break;
                case 50:
                    this.f703a |= 32;
                    this.g = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_windowNoTitle:
                    this.f703a |= 64;
                    this.h = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_dropDownHintAppearance:
                    this.f703a |= NotificationCompat.FLAG_HIGH_PRIORITY;
                    this.i = codedInputStream.readBytes();
                    break;
                case 74:
                    this.f703a |= 256;
                    this.j = codedInputStream.readBytes();
                    break;
                case 80:
                    g a3 = g.a(codedInputStream.readEnum());
                    if (a3 == null) {
                        break;
                    } else {
                        this.f703a |= 512;
                        this.k = a3;
                        break;
                    }
                case 88:
                    i a4 = i.a(codedInputStream.readEnum());
                    if (a4 == null) {
                        break;
                    } else {
                        this.f703a |= 1024;
                        this.l = a4;
                        break;
                    }
                case 98:
                    this.f703a |= 2048;
                    this.m = codedInputStream.readBytes();
                    break;
                case 106:
                    this.f703a |= FragmentTransaction.TRANSIT_ENTER_MASK;
                    this.n = codedInputStream.readBytes();
                    break;
                case 114:
                    this.f703a |= FragmentTransaction.TRANSIT_EXIT_MASK;
                    this.o = codedInputStream.readBytes();
                    break;
                case 122:
                    this.f703a |= 16384;
                    this.p = codedInputStream.readBytes();
                    break;
                case NotificationCompat.FLAG_HIGH_PRIORITY:
                    this.f703a |= 32768;
                    this.q = codedInputStream.readInt64();
                    break;
                case 138:
                    this.f703a |= Menu.CATEGORY_CONTAINER;
                    this.r = codedInputStream.readBytes();
                    break;
                case 144:
                    this.f703a |= Menu.CATEGORY_SYSTEM;
                    this.s = codedInputStream.readInt64();
                    break;
                case 154:
                    this.f703a |= Menu.CATEGORY_ALTERNATIVE;
                    this.t = codedInputStream.readBytes();
                    break;
                case 162:
                    this.f703a |= 524288;
                    this.u = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public d a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 1;
        this.f704b = byteString;
        return this;
    }

    public d a(e eVar) {
        if (eVar == null) {
            throw new NullPointerException();
        }
        this.f703a |= 2;
        this.f705c = eVar;
        return this;
    }

    public d b(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 4;
        this.d = byteString;
        return this;
    }

    public d c(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 8;
        this.e = byteString;
        return this;
    }

    public d d(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 16;
        this.f = byteString;
        return this;
    }

    public d e(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 32;
        this.g = byteString;
        return this;
    }

    public d f(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 64;
        this.h = byteString;
        return this;
    }

    public d g(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= NotificationCompat.FLAG_HIGH_PRIORITY;
        this.i = byteString;
        return this;
    }

    public d h(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 256;
        this.j = byteString;
        return this;
    }

    public d a(g gVar) {
        if (gVar == null) {
            throw new NullPointerException();
        }
        this.f703a |= 512;
        this.k = gVar;
        return this;
    }

    public d a(i iVar) {
        if (iVar == null) {
            throw new NullPointerException();
        }
        this.f703a |= 1024;
        this.l = iVar;
        return this;
    }

    public d i(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 2048;
        this.m = byteString;
        return this;
    }

    public d j(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= FragmentTransaction.TRANSIT_ENTER_MASK;
        this.n = byteString;
        return this;
    }

    public d k(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= FragmentTransaction.TRANSIT_EXIT_MASK;
        this.o = byteString;
        return this;
    }

    public d l(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 16384;
        this.p = byteString;
        return this;
    }

    public d a(long j2) {
        this.f703a |= 32768;
        this.q = j2;
        return this;
    }

    public d m(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= Menu.CATEGORY_CONTAINER;
        this.r = byteString;
        return this;
    }

    public d b(long j2) {
        this.f703a |= Menu.CATEGORY_SYSTEM;
        this.s = j2;
        return this;
    }

    public d n(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= Menu.CATEGORY_ALTERNATIVE;
        this.t = byteString;
        return this;
    }

    public d o(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f703a |= 524288;
        this.u = byteString;
        return this;
    }
}
