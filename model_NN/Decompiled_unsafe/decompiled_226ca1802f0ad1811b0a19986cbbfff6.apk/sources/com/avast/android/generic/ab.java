package com.avast.android.generic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.avast.android.generic.util.ae;
import com.avast.android.generic.util.ga.a;
import com.avast.android.generic.util.l;
import com.avast.android.generic.util.t;
import com.avast.android.generic.util.z;
import com.avast.c.a.a.an;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Semaphore;

/* compiled from: SettingsApi */
public class ab {

    /* renamed from: a  reason: collision with root package name */
    public static final Object f320a = new Object();

    /* renamed from: b  reason: collision with root package name */
    public static String f321b = b(a("0000".getBytes()));

    /* renamed from: c  reason: collision with root package name */
    private Context f322c;
    private SharedPreferences d;
    private SharedPreferences e;
    private SharedPreferences.Editor f;
    private Object g;
    private HashMap<String, Object> h;
    private Set<String> i;
    private Semaphore j;

    public ab(Context context) {
        this(context, ac.SURVIVES_WIPE);
    }

    public ab(Context context, ac acVar) {
        this.f = null;
        this.g = new Object();
        this.h = null;
        this.i = null;
        this.f322c = context;
        if (acVar == ac.SURVIVES_WIPE) {
            this.d = context.getSharedPreferences("prefs", 0);
            this.e = context.getSharedPreferences("prefs_sync", 0);
        } else {
            this.d = context.getSharedPreferences("temporary", 0);
            this.e = context.getSharedPreferences("temporary_sync", 0);
        }
        this.j = new Semaphore(1);
    }

    public synchronized void a() {
        ad();
    }

    private void ad() {
        synchronized (this.g) {
            if (this.f == null) {
                this.f = this.d.edit();
                this.h = new HashMap<>();
                this.i = new HashSet();
            }
        }
    }

    public synchronized boolean b() {
        boolean z;
        z = true;
        synchronized (this.g) {
            try {
                if (this.f != null) {
                    z = this.f.commit();
                    a(this.f322c, this.h, false);
                    b(this.i);
                    this.f = null;
                    this.h = null;
                    this.i = null;
                }
            } catch (Exception e2) {
                z.a("AvastGeneric", "Error in committing preference store", e2);
                z = false;
            }
        }
        return z;
    }

    public synchronized boolean a(long j2) {
        boolean z;
        z = true;
        synchronized (this.g) {
            try {
                if (this.f != null) {
                    z = this.f.commit();
                    a(this.f322c, this.h, false);
                    a(this.i, j2);
                    this.f = null;
                    this.h = null;
                    this.i = null;
                }
            } catch (Exception e2) {
                z.a("AvastGeneric", "Error in committing preference store", e2);
                z = false;
            }
        }
        return z;
    }

    public SharedPreferences c() {
        return this.d;
    }

    public synchronized void a(String str, boolean z) {
        if (str != null) {
            if (!TextUtils.isEmpty(str)) {
                synchronized (this.g) {
                    ad();
                    this.f.putBoolean(str, z);
                    j(str);
                }
            }
        }
        throw new IllegalArgumentException("Key must be not null and non empty!");
    }

    public synchronized void a(String str, byte[] bArr) {
        if (str != null) {
            if (!TextUtils.isEmpty(str)) {
                synchronized (this.g) {
                    ad();
                    this.f.putString(str, l.a(bArr));
                    j(str);
                }
            }
        }
        throw new IllegalArgumentException("Key must be not null and non empty!");
    }

    public synchronized void a(String str, String str2) {
        if (str != null) {
            if (!TextUtils.isEmpty(str)) {
                synchronized (this.g) {
                    ad();
                    this.f.putString(str, str2);
                    j(str);
                }
            }
        }
        throw new IllegalArgumentException("Key must be not null and non empty!");
    }

    public synchronized void a(String str, int i2) {
        if (str != null) {
            if (!TextUtils.isEmpty(str)) {
                synchronized (this.g) {
                    ad();
                    this.f.putInt(str, i2);
                    j(str);
                }
            }
        }
        throw new IllegalArgumentException("Key must be not null and non empty!");
    }

    public synchronized void a(String str, long j2) {
        if (str != null) {
            if (!TextUtils.isEmpty(str)) {
                synchronized (this.g) {
                    ad();
                    this.f.putLong(str, j2);
                    j(str);
                }
            }
        }
        throw new IllegalArgumentException("Key must be not null and non empty!");
    }

    public synchronized void a(String str) {
        synchronized (this.g) {
            ad();
            this.f.remove(str);
            j(str);
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean b(java.lang.String r2, boolean r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            android.content.SharedPreferences r0 = r1.d     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
            boolean r3 = r0.getBoolean(r2, r3)     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
        L_0x0007:
            monitor-exit(r1)
            return r3
        L_0x0009:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x000c:
            r0 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String b(java.lang.String r2, java.lang.String r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            android.content.SharedPreferences r0 = r1.d     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
            java.lang.String r3 = r0.getString(r2, r3)     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
        L_0x0007:
            monitor-exit(r1)
            return r3
        L_0x0009:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x000c:
            r0 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int b(java.lang.String r2, int r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            android.content.SharedPreferences r0 = r1.d     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
            int r3 = r0.getInt(r2, r3)     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
        L_0x0007:
            monitor-exit(r1)
            return r3
        L_0x0009:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x000c:
            r0 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.ab.b(java.lang.String, int):int");
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long b(java.lang.String r2, long r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            android.content.SharedPreferences r0 = r1.d     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
            long r3 = r0.getLong(r2, r3)     // Catch:{ ClassCastException -> 0x000c, all -> 0x0009 }
        L_0x0007:
            monitor-exit(r1)
            return r3
        L_0x0009:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x000c:
            r0 = move-exception
            goto L_0x0007
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.ab.b(java.lang.String, long):long");
    }

    public synchronized Map<String, ?> d() {
        return this.d.getAll();
    }

    public boolean b(String str) {
        if (str == null) {
            str = "";
        }
        return f().equals(b(a(str.getBytes())));
    }

    public boolean c(String str) {
        if (str == null) {
            str = "";
        }
        return g().equals(b(a(str.getBytes())));
    }

    public boolean e() {
        return !b("0000");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void d(String str) {
        boolean z = false;
        synchronized (this) {
            try {
                Integer.parseInt(str);
            } catch (Exception e2) {
                z = true;
            }
            if (str.length() < 4 || str.length() > 6 || z) {
                a.a().a("PIN: " + str, new Exception("Invalid PIN exception"), false);
            }
            String b2 = b(a(str.getBytes()));
            a("encaccesscode", b2);
            a("encaccesscode", (Object) b2);
            b();
        }
    }

    public synchronized String f() {
        String b2;
        b2 = b("encaccesscode", (String) null);
        if (b2 == null || b2.equals("")) {
            b2 = f321b;
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void e(String str) {
        if (str != null) {
            String str2 = "";
            if (!"".equals(str)) {
                str2 = b(a(str.getBytes()));
            }
            a("enctempaccesscode", str2);
            a("enctempaccesscode", (Object) str2);
            b();
        }
    }

    public synchronized String g() {
        return b("enctempaccesscode", "");
    }

    public synchronized void b(long j2) {
        a("tempaccesscodeissuetime", j2);
        a("tempaccesscodeissuetime", Long.valueOf(j2));
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public synchronized long h() {
        return b("tempaccesscodeissuetime", -1L);
    }

    public synchronized void c(long j2) {
        a("tempaccesscodetimeouttime", j2);
        a("tempaccesscodetimeouttime", Long.valueOf(j2));
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public synchronized long i() {
        return b("tempaccesscodetimeouttime", -1L);
    }

    public synchronized void d(long j2) {
        a("tempaccesscodelastknowntime", j2);
        a("tempaccesscodelastknowntime", Long.valueOf(j2));
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public synchronized long j() {
        return b("tempaccesscodelastknowntime", -1L);
    }

    public synchronized String k() {
        return b("tempaccesscoderecoverynumber", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void f(String str) {
        a("tempaccesscodereceivertickauthtoken", str);
        a("tempaccesscodereceivertickauthtoken", (Object) str);
        b();
    }

    public synchronized String l() {
        return b("tempaccesscodereceivertickauthtoken", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void g(String str) {
        a("tempaccesscodereceiversmsauthtoken", str);
        a("tempaccesscodereceiversmsauthtoken", (Object) str);
        b();
    }

    public synchronized String m() {
        return b("tempaccesscodereceiversmsauthtoken", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean n() {
        return b("paswordProtection", false);
    }

    public synchronized void a(boolean z) {
        a("paswordProtection", z);
        a("paswordProtection", Boolean.valueOf(z));
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean o() {
        return b("communityIQEnabled", true);
    }

    public void b(boolean z) {
        a("communityIQEnabled", z);
        a("communityIQEnabled", Boolean.valueOf(z));
        b();
    }

    public String p() {
        return b("language", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public void h(String str) {
        a("language", str);
        a("language", (Object) str);
        b();
    }

    public synchronized String q() {
        String b2;
        b2 = b("guid", (String) null);
        if (TextUtils.isEmpty(b2)) {
            try {
                this.j.acquire();
            } catch (InterruptedException e2) {
                t.a("Couldn't synchronize GUID generation.", e2);
            }
            b2 = ae();
            this.j.release();
        }
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void i(String str) {
        a("guid", str);
        a("guid", (Object) str);
        b();
    }

    public synchronized String r() {
        String str = null;
        synchronized (this) {
            String b2 = b("guid", (String) null);
            if (!TextUtils.isEmpty(b2)) {
                str = b2;
            }
        }
        return str;
    }

    private String ae() {
        String b2 = b("installation_guid", (String) null);
        if (TextUtils.isEmpty(b2)) {
            b2 = UUID.randomUUID().toString();
        }
        a("guid", b2);
        b();
        return b2;
    }

    public synchronized String s() {
        String b2;
        b2 = b("installation_guid", (String) null);
        if (TextUtils.isEmpty(b2)) {
            try {
                this.j.acquire();
            } catch (InterruptedException e2) {
                t.a("Couldn't synchronize Installation GUID generation.", e2);
            }
            b2 = af();
            this.j.release();
        }
        return b2;
    }

    private String af() {
        String b2 = b("guid", (String) null);
        if (TextUtils.isEmpty(b2)) {
            b2 = UUID.randomUUID().toString();
        }
        a("installation_guid", b2);
        b();
        return b2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void a(Context context, String str, String str2, String str3, String str4, String str5, String str6) {
        a("accountEmail", str);
        a("accountEmail", (Object) str);
        a("auid", str2);
        a("auid", (Object) str2);
        a("accountEncKey", str3);
        a("accountEncKey", (Object) str3);
        a("accountCommPassword", str4);
        a("accountCommPassword", (Object) str4);
        if (!TextUtils.isEmpty(str5)) {
            a("accountSmsGateway", str5);
            a("accountSmsGateway", (Object) str5);
        } else {
            a("accountSmsGateway");
            a("accountSmsGateway", (Object) "-DEL-");
        }
        a("guid", (Object) q());
        ab abVar = (ab) aa.a(context, ae.class);
        abVar.a(context, str6);
        abVar.b();
        b();
    }

    public synchronized boolean t() {
        return !TextUtils.isEmpty(y());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public synchronized void u() {
        a("accountEmail");
        a("accountEmail", (Object) "-DEL-");
        a("auid");
        a("auid", (Object) "-DEL-");
        a("accountEncKey");
        a("accountEncKey", (Object) "-DEL-");
        a("accountCommPassword");
        a("accountCommPassword", (Object) "-DEL-");
        a("accountSmsGateway");
        a("accountSmsGateway", (Object) "-DEL-");
        b();
    }

    public synchronized String v() {
        return b("accountEmail", (String) null);
    }

    public synchronized String w() {
        return b("auid", (String) null);
    }

    public synchronized byte[] x() {
        byte[] bArr = null;
        synchronized (this) {
            try {
                bArr = l.a(b("accountEncKey", (String) null));
            } catch (IOException e2) {
                t.a("SettingsApi", "Can't convert encryption key to byte array.", e2);
            }
        }
        return bArr;
    }

    public synchronized String y() {
        return b("accountCommPassword", (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public synchronized boolean z() {
        return b("accountReport", true);
    }

    public synchronized int A() {
        return b("accountReportFrequency", 60);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public synchronized boolean B() {
        return b("accountSmsSending", true);
    }

    public synchronized boolean C() {
        return d() != null && d().containsKey("accountSmsSending");
    }

    public String D() {
        return b("c2dmri", (String) null);
    }

    public String E() {
        return b("c2dmowner", (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public void a(Context context, String str) {
        a("c2dmowner", context.getPackageName());
        a("c2dmri", str);
        a("c2dmri", (Object) str);
        a("c2dmowner", (Object) context.getPackageName());
    }

    public String F() {
        return b("accountLuid", (String) null);
    }

    public String G() {
        return b("accountSmsGateway", (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public long H() {
        return b("id", 0L);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean I() {
        return b("splitcdma", false);
    }

    public String J() {
        return b("not1", "");
    }

    public String K() {
        return b("not2", "");
    }

    public static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            byte[] bArr2 = new byte[40];
            instance.update(bArr, 0, bArr.length);
            return instance.digest();
        } catch (NoSuchAlgorithmException e2) {
            return bArr;
        }
    }

    public static String b(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = (bArr[i2] >>> 4) & 15;
            if (i3 < 0 || i3 > 9) {
                stringBuffer.append((char) ((i3 - 10) + 97));
            } else {
                stringBuffer.append((char) (i3 + 48));
            }
            byte b2 = bArr[i2] & 15;
        }
        return stringBuffer.toString();
    }

    public static void a(Context context, boolean z) {
        ab abVar = (ab) aa.a(context, ae.class);
        ab abVar2 = (ab) aa.a(context, ad.class);
        if (abVar != null && abVar2 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("c2dmowner", abVar.E());
            hashMap.put("c2dmri", abVar.D());
            hashMap.put("id", Long.valueOf(abVar2.H()));
            hashMap.put("restorechecked", true);
            hashMap.put("encaccesscode", abVar2.f());
            hashMap.put("enctempaccesscode", abVar2.g());
            hashMap.put("tempaccesscodeissuetime", Long.valueOf(abVar2.h()));
            hashMap.put("tempaccesscodelastknowntime", Long.valueOf(abVar2.j()));
            hashMap.put("tempaccesscodetimeouttime", Long.valueOf(abVar2.i()));
            hashMap.put("tempaccesscoderecoverynumber", abVar2.k());
            hashMap.put("tempaccesscodereceivertickauthtoken", abVar2.l());
            hashMap.put("tempaccesscodereceiversmsauthtoken", abVar2.m());
            hashMap.put("paswordProtection", Boolean.valueOf(abVar2.n()));
            hashMap.put("communityIQEnabled", Boolean.valueOf(abVar2.o()));
            hashMap.put("guid", abVar2.q());
            hashMap.put("language", abVar2.p());
            hashMap.put("splitcdma", Boolean.valueOf(abVar2.I()));
            hashMap.put("accountEmail", abVar2.v());
            hashMap.put("auid", abVar2.w());
            hashMap.put("accountEncKey", abVar2.x());
            hashMap.put("accountCommPassword", abVar2.y());
            hashMap.put("accountReport", Boolean.valueOf(abVar2.z()));
            hashMap.put("accountReportFrequency", Integer.valueOf(abVar2.A()));
            hashMap.put("accountLuid", abVar2.F());
            hashMap.put("accountSmsGateway", abVar2.G());
            hashMap.put("not1", abVar2.J());
            hashMap.put("not2", abVar2.K());
            if (abVar2.C()) {
                hashMap.put("accountSmsSending", Boolean.valueOf(abVar2.B()));
            }
            hashMap.put("premiumAccount_" + b(context), Boolean.valueOf(abVar2.N()));
            hashMap.put("premiumExpirationDate_" + b(context), abVar2.O());
            hashMap.put("premiumIsSubscription_" + b(context), Boolean.valueOf(abVar2.P()));
            hashMap.put("premiumSku_" + b(context), abVar2.Q());
            hashMap.put("welcomePremiumShown", Boolean.valueOf(abVar2.V()));
            a(context, hashMap, z);
        }
    }

    public static void a(Context context) {
        ab abVar = (ab) aa.a(context, ae.class);
        ab abVar2 = (ab) aa.a(context, ad.class);
        if (abVar != null && abVar2 != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("c2dmowner", abVar.E());
            hashMap.put("c2dmri", abVar.D());
            hashMap.put("restorechecked", true);
            hashMap.put("encaccesscode", abVar2.f());
            hashMap.put("paswordProtection", Boolean.valueOf(abVar2.n()));
            hashMap.put("guid", abVar2.q());
            hashMap.put("language", abVar2.p());
            hashMap.put("splitcdma", Boolean.valueOf(abVar2.I()));
            hashMap.put("accountEmail", abVar2.v());
            hashMap.put("auid", abVar2.w());
            hashMap.put("accountEncKey", abVar2.x());
            hashMap.put("accountCommPassword", abVar2.y());
            hashMap.put("accountReport", Boolean.valueOf(abVar2.z()));
            hashMap.put("accountReportFrequency", Integer.valueOf(abVar2.A()));
            hashMap.put("accountLuid", abVar2.F());
            hashMap.put("accountSmsGateway", abVar2.G());
            if (abVar2.C()) {
                hashMap.put("accountSmsSending", Boolean.valueOf(abVar2.B()));
            }
            hashMap.put("premiumAccount_" + b(context), Boolean.valueOf(abVar2.N()));
            hashMap.put("premiumExpirationDate_" + b(context), abVar2.O());
            hashMap.put("premiumIsSubscription_" + b(context), Boolean.valueOf(abVar2.P()));
            hashMap.put("premiumSku_" + b(context), abVar2.Q());
            hashMap.put("welcomePremiumShown", Boolean.valueOf(abVar2.V()));
            a(context, hashMap, false);
        }
    }

    private void a(String str, Object obj) {
        if (this.h != null) {
            this.h.put(str, obj);
        }
    }

    private void j(String str) {
        synchronized (this.g) {
            if (!(this.i == null || str == null)) {
                this.i.add(str);
            }
        }
    }

    private void b(Set<String> set) {
        if (set != null && !set.isEmpty()) {
            synchronized (this.g) {
                try {
                    long currentTimeMillis = System.currentTimeMillis() - 1;
                    SharedPreferences.Editor edit = this.e.edit();
                    for (String next : set) {
                        z.a("AvastGenericSync", "Storing change time for key " + next + " at " + currentTimeMillis);
                        edit.putLong(next, currentTimeMillis);
                    }
                    edit.commit();
                } catch (Exception e2) {
                    z.a("AvastGenericSync", "Can not store change times", e2);
                }
            }
        }
    }

    private void a(Set<String> set, long j2) {
        if (set != null && !set.isEmpty()) {
            synchronized (this.g) {
                try {
                    SharedPreferences.Editor edit = this.e.edit();
                    for (String next : set) {
                        z.a("AvastGenericSync", "Storing change time for key " + next + " at " + j2);
                        edit.putLong(next, j2);
                    }
                    edit.commit();
                } catch (Exception e2) {
                    z.a("AvastGenericSync", "Can not store change times", e2);
                }
            }
        }
    }

    public void e(long j2) {
        synchronized (this.g) {
            try {
                if (this.e.getLong("lastSync", -1) < j2) {
                    SharedPreferences.Editor edit = this.e.edit();
                    z.a("AvastGenericSync", "Notifying sync done at " + j2);
                    edit.putLong("lastSync", j2);
                    edit.commit();
                }
            } catch (Exception e2) {
                z.a("AvastGenericSync", "Can not store sync time", e2);
            }
        }
    }

    public boolean c(String str, long j2) {
        boolean z = true;
        synchronized (this.g) {
            if (TextUtils.isEmpty(str)) {
                return false;
            }
            try {
                z.a("AvastGenericSync", "Checking for sync for " + str + " at time " + j2);
                long j3 = this.e.getLong("lastSync", -1);
                if (j3 == -1) {
                    z.a("AvastGenericSync", "Sync necessary for " + str + " (no sync done yet)");
                    return true;
                }
                long j4 = this.e.getLong(str, -1);
                if (j4 == -1) {
                    z.a("AvastGenericSync", "No sync necessary for " + str + " (no change done yet)");
                    return false;
                } else if (j3 > j2 || j4 > j2) {
                    SharedPreferences.Editor edit = this.e.edit();
                    edit.putLong(str, j2);
                    edit.commit();
                    z.a("AvastGenericSync", "Sync necessary for " + str + " (time has been changed to the past)");
                    return true;
                } else {
                    if (j3 >= j4) {
                        z = false;
                    }
                    if (z) {
                        z.a("AvastGenericSync", "Sync necessary for " + str + " (" + j3 + " < " + j4 + ")");
                    } else {
                        z.a("AvastGenericSync", "No sync necessary for " + str + " (" + j3 + " >= " + j4 + ")");
                    }
                    return z;
                }
            } catch (Exception e2) {
                z.a("AvastGenericSync", "Can not store change time for key " + str, e2);
                return false;
            }
        }
    }

    @SuppressLint({"UseValueOf"})
    private static void a(Context context, HashMap<String, Object> hashMap, boolean z) {
        if (hashMap != null && !hashMap.isEmpty()) {
            Intent intent = new Intent();
            intent.setAction("com.avast.android.generic.action.PROPERTY_CHANGED");
            z.a(context, "ALL", "KEY CHANGE START");
            for (String next : hashMap.keySet()) {
                if (f.a(next)) {
                    Object obj = hashMap.get(next);
                    z.a(context, "ALL", next + " -> " + obj);
                    if (obj == null) {
                        intent.putExtra(next, "-NULL-");
                    } else if (obj instanceof String) {
                        intent.putExtra(next, (String) obj);
                    } else if (obj instanceof Boolean) {
                        intent.putExtra(next, new Boolean(((Boolean) obj).booleanValue()));
                    } else if (obj instanceof Integer) {
                        intent.putExtra(next, new Integer(((Integer) obj).intValue()));
                    } else if (obj instanceof Long) {
                        intent.putExtra(next, new Long(((Long) obj).longValue()));
                    } else if (obj instanceof byte[]) {
                        intent.putExtra(next, (byte[]) obj);
                    }
                }
            }
            z.a(context, "ALL", "KEY CHANGE END");
            intent.putExtra("sourcePackage", context.getPackageName());
            intent.putExtra("com.avast.android.generic.action.SHARE_SETTINGS", z);
            ae.a(intent);
            context.sendBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
        }
    }

    public Context L() {
        return this.f322c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean M() {
        return b("gSettingsNotificationAlwaysOn", true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean N() {
        return b("premiumAccount_" + W(), false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public Long O() {
        return Long.valueOf(b("premiumExpirationDate_" + W(), -2L));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean P() {
        return b("premiumIsSubscription_" + W(), false);
    }

    public String Q() {
        return b("premiumSku_" + W(), "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public void a(boolean z, long j2, boolean z2, String str) {
        a("premiumAccount_" + W(), z);
        if (z) {
            a("premiumExpirationDate_" + W(), j2);
            a("premiumIsSubscription_" + W(), z2);
            a("premiumSku_" + W(), str);
        } else {
            a("premiumExpirationDate_" + W(), -2L);
            a("premiumIsSubscription_" + W(), false);
            a("premiumSku_" + W(), "");
        }
        a("premiumAccount_" + W(), Boolean.valueOf(z));
        if (z) {
            a("premiumExpirationDate_" + W(), Long.valueOf(j2));
            a("premiumIsSubscription_" + W(), Boolean.valueOf(z2));
            a("premiumSku_" + W(), (Object) str);
        } else {
            a("premiumExpirationDate_" + W(), (Object) -2);
            a("premiumIsSubscription_" + W(), (Object) false);
            a("premiumSku_" + W(), (Object) "");
        }
        if (z) {
            a("guid", (Object) q());
        }
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public long R() {
        return b("lastSubRun", -1L);
    }

    public void S() {
        a("lastSubRun");
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, boolean]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public void T() {
        a("welcomePremiumShown", true);
        a("welcomePremiumShown", (Object) true);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    public void U() {
        a("welcomePremiumShown");
        a("welcomePremiumShown", (Object) "-DEL-");
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean V() {
        return b("welcomePremiumShown", false);
    }

    public an W() {
        return b(this.f322c);
    }

    public static an b(Context context) {
        if (context.getPackageName().equals("com.avast.android.vpn")) {
            return an.VPN;
        }
        return an.SUITE;
    }

    public String c(Context context) {
        String b2 = b("oemPartner", "");
        if (!TextUtils.isEmpty(b2)) {
            return b2;
        }
        String string = context.getString(x.oem_partner_name);
        k(string);
        return string;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.avast.android.generic.ab.a(android.content.Context, boolean):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void */
    private void k(String str) {
        a("oemPartner", str);
        a("oemPartner", (Object) str);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, long):long
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean */
    public boolean X() {
        return b("shepherdConnectivityChangeReceiverEnabled", false);
    }

    public void c(boolean z) {
        a("shepherdConnectivityChangeReceiverEnabled", z);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public long Y() {
        return b("shepherdNextUpdateTime", -1L);
    }

    public void f(long j2) {
        a("shepherdNextUpdateTime", j2);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public long Z() {
        return b("shepherdLastUpdateAttemptTime", -1L);
    }

    public void g(long j2) {
        a("shepherdLastUpdateAttemptTime", j2);
        b();
    }

    public HashSet<String> aa() {
        String b2 = b("shepherdTags", "");
        String[] split = b2.split(",");
        HashSet<String> hashSet = new HashSet<>();
        if (!TextUtils.isEmpty(b2)) {
            for (String add : split) {
                hashSet.add(add);
            }
        }
        return hashSet;
    }

    public void a(Set<String> set) {
        String str = "";
        if (set != null) {
            for (String str2 : set) {
                str = str + "," + str2;
            }
        }
        l(str);
    }

    public void a(String[] strArr) {
        String str = "";
        if (strArr != null) {
            for (int i2 = 0; i2 < strArr.length; i2++) {
                str = str + "," + strArr[i2];
            }
        }
        l(str);
    }

    private void l(String str) {
        if (str.length() > 0 && str.charAt(0) == ',') {
            str = str.substring(1, str.length());
        }
        t.c("SettingsApi: saving Shepherd tags=" + str);
        a("shepherdTags", str);
        b();
    }

    public void h(long j2) {
        a("lastActiveUserTrack", j2);
        b();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.b(java.lang.String, long):long
     arg types: [java.lang.String, int]
     candidates:
      com.avast.android.generic.ab.b(java.lang.String, int):int
      com.avast.android.generic.ab.b(java.lang.String, java.lang.String):java.lang.String
      com.avast.android.generic.ab.b(java.lang.String, boolean):boolean
      com.avast.android.generic.ab.b(java.lang.String, long):long */
    public long ab() {
        return b("lastActiveUserTrack", 0L);
    }

    public String ac() {
        return b("uniqueIId", (String) null);
    }
}
