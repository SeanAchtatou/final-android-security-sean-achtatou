package com.avast.android.generic.app.passwordrecovery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.util.t;

public class PasswordRecoveryReceiver extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.passwordrecovery.a.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.avast.android.generic.app.passwordrecovery.a.a(android.content.Context, com.avast.android.generic.app.passwordrecovery.b):void
      com.avast.android.generic.app.passwordrecovery.a.a(android.content.Context, java.lang.String):boolean
      com.avast.android.generic.app.passwordrecovery.a.a(android.content.Context, boolean):void */
    public void onReceive(Context context, Intent intent) {
        String stringExtra;
        t.c("PasswordRecoveryReceiver received intent with action " + intent.getAction());
        if ("android.intent.action.TIME_SET".equals(intent.getAction())) {
            a.d(context);
        } else if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.QUICKBOOT_POWERON".equals(intent.getAction())) {
            if (a.d(context)) {
                a.b(context);
            }
        } else if ("com.avast.android.generic.RECOVERY_TIME_TICK".equals(intent.getAction())) {
            String stringExtra2 = intent.getStringExtra("auth_token");
            if (stringExtra2 != null && a.b(context, stringExtra2) && a.d(context)) {
                a.c(context);
            }
        } else if ("com.avast.android.generic.RECOVERY_SMS".equals(intent.getAction()) && (stringExtra = intent.getStringExtra("auth_token")) != null && a.c(context, stringExtra) && a.d(context)) {
            switch (getResultCode()) {
                case -1:
                    a.a(context, true);
                    return;
                default:
                    a.a(context, false);
                    return;
            }
        }
    }
}
