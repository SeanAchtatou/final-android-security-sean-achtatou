package com.avast.android.generic.app.subscription;

import android.content.IntentFilter;
import android.os.AsyncTask;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.licensing.PurchaseConfirmationService;
import com.avast.android.generic.licensing.c.l;
import com.avast.android.generic.ui.widget.SubscriptionButton;
import com.avast.android.generic.util.ga.a;
import com.google.analytics.tracking.android.Transaction;

/* compiled from: SubscriptionFragment */
class r extends AsyncTask<Void, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ l f571a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ q f572b;

    r(q qVar, l lVar) {
        this.f572b = qVar;
        this.f571a = lVar;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.f572b.f570a.r.registerReceiver(this.f572b.f570a.J, new IntentFilter("com.avast.android.generic.ACTION_LICENSING_UPDATE"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Boolean doInBackground(Void... voidArr) {
        if (this.f572b.f570a.y != null && this.f572b.f570a.y.a().equals(this.f571a.c())) {
            Transaction build = new Transaction.Builder(this.f571a.b(), (long) (this.f572b.f570a.y.i() * 1000000.0f)).setAffiliation("In-App Purchase").setShippingCostInMicros(0).setTotalTaxInMicros(0).build();
            build.addItem(new Transaction.Item.Builder(this.f572b.f570a.y.a(), this.f572b.f570a.y.e(), (long) (this.f572b.f570a.y.i() * 1000000.0f), 1).setProductCategory("avast! Mobile Premium licenses").build());
            a.a().a(build);
        }
        PurchaseConfirmationService.b(this.f572b.f570a.r);
        try {
            this.f572b.f570a.H.acquire();
            return true;
        } catch (InterruptedException e) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        try {
            this.f572b.f570a.r.unregisterReceiver(this.f572b.f570a.J);
        } catch (Exception e) {
        }
        if (this.f572b.f570a.isAdded() && bool.booleanValue()) {
            switch (this.f572b.f570a.I.a()) {
                case VALID:
                    try {
                        synchronized (SubscriptionButton.f1116a) {
                            ab abVar = (ab) aa.a(this.f572b.f570a.r, ab.class);
                            if (!abVar.V()) {
                                abVar.T();
                                WelcomePremiumActivity.a(this.f572b.f570a.r);
                            }
                        }
                    } catch (Exception e2) {
                        a.a.a.a.a.a.a().a("Can not open welcome premium activity (subscription fragment)", e2);
                    }
                    this.f572b.f570a.r.setResult(-1);
                    this.f572b.f570a.r.finish();
                    this.f572b.f570a.L.c(this.f572b.f570a.y.a(), this.f572b.f570a.y.i());
                    return;
                default:
                    this.f572b.f570a.d();
                    return;
            }
        }
    }
}
