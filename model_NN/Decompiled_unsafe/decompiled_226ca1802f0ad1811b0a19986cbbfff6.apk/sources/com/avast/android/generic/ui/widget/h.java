package com.avast.android.generic.ui.widget;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import com.avast.android.generic.ui.widget.LanguageSelectorRow;

/* compiled from: LanguageSelectorRow */
class h implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LanguageSelectorRow f1144a;

    h(LanguageSelectorRow languageSelectorRow) {
        this.f1144a = languageSelectorRow;
    }

    public void onClick(View view) {
        if (this.f1144a.getContext() instanceof FragmentActivity) {
            LanguageSelectorRow.LanguageSelectDialog.a(((FragmentActivity) this.f1144a.getContext()).getSupportFragmentManager());
        }
    }
}
