package com.avast.android.generic.ui;

import android.text.InputFilter;
import android.text.Spanned;

/* compiled from: VoucherDialog */
class ae implements InputFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ VoucherDialog f1010a;

    ae(VoucherDialog voucherDialog) {
        this.f1010a = voucherDialog;
    }

    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        while (i < i2) {
            if (!Character.isLetterOrDigit(charSequence.charAt(i))) {
                return "";
            }
            i++;
        }
        return null;
    }
}
