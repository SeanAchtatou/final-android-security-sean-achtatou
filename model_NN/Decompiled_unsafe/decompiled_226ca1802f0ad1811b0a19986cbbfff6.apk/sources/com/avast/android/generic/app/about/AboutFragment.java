package com.avast.android.generic.app.about;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.u;
import com.avast.android.generic.ui.widget.SlideBlock;
import com.avast.android.generic.util.av;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.util.s;
import com.avast.android.generic.x;

public class AboutFragment extends TrackedMultiToolFragment {

    /* renamed from: a  reason: collision with root package name */
    private String f326a;

    /* renamed from: b  reason: collision with root package name */
    private String f327b;

    /* renamed from: c  reason: collision with root package name */
    private String f328c;
    private ScrollView d;

    public final String c() {
        return "about";
    }

    public int a() {
        return x.about_title;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (!arguments.containsKey("app_name")) {
            throw new NullPointerException("The AboutActivity.EXTRA_APP_NAME argument has to be set.");
        }
        this.f326a = arguments.getString("app_name");
        String string = getString(x.about_vps_version_unknown);
        this.f327b = arguments.getString("vps_version");
        if (this.f327b == null) {
            this.f327b = string;
        }
        long j = arguments.getLong("vps_definitions_count", -1);
        if (j != -1) {
            string = String.valueOf(j);
        }
        this.f328c = string;
        setHasOptionsMenu(true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @SuppressLint({"NewApi"})
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.d = (ScrollView) layoutInflater.inflate(t.fragment_about, viewGroup, false);
        if (Build.VERSION.SDK_INT >= 11) {
            this.d.setLayerType(1, null);
        }
        return this.d;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ((TextView) view.findViewById(r.about_app_name)).setText(this.f326a);
        TextView textView = (TextView) view.findViewById(r.textAppVersion);
        TextView textView2 = (TextView) view.findViewById(r.textVpsVersion);
        TextView textView3 = (TextView) view.findViewById(r.textVpsDefinitions);
        if (m()) {
            textView2.setVisibility(8);
            textView3.setVisibility(8);
        } else {
            textView2.setText(getString(x.about_vps_version, this.f327b));
            textView3.setText(getString(x.about_vps_definitions, this.f328c));
        }
        String d2 = d();
        textView.setText(getString(x.about_app_version, d2));
        ((Button) view.findViewById(r.b_feedback)).setOnClickListener(new a(this));
        float f = getResources().getDisplayMetrics().density;
        e eVar = new e(getActivity(), this.d);
        SlideBlock slideBlock = (SlideBlock) view.findViewById(r.about_eula);
        slideBlock.a(getString(x.about_eula_show));
        slideBlock.setEnabled(false);
        slideBlock.a(new b(this, slideBlock, eVar, f));
        new c(this, slideBlock).execute(new Void[0]);
        LinearLayout linearLayout = (LinearLayout) view.findViewById(r.hidden_debug_mode);
        linearLayout.setSoundEffectsEnabled(false);
        linearLayout.setOnClickListener(new d(this));
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(u.menu_about, menu);
        MenuItem findItem = menu.findItem(r.menu_about_rate);
        String packageName = getActivity().getPackageName();
        if ((!packageName.equals("com.avast.android.mobilesecurity") && !packageName.equals("com.avast.android.antitheft") && !packageName.equals("com.avast.android.at_play") && !packageName.equals("com.avast.android.backup") && !packageName.equals("com.avast.android.at_client")) || !a(e())) {
            findItem.setVisible(false);
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == r.menu_about_rate) {
            startActivity(e());
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private String d() {
        String str = "";
        if (s.a(getActivity())) {
            str = " (debug)";
        }
        if (j()) {
            av a2 = av.a(getActivity());
            return a2.b() + "." + a2.c() + "." + a2.d() + str;
        }
        String str2 = "";
        try {
            str2 = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            com.avast.android.generic.util.t.b(e.getMessage(), e);
        }
        return str2 + str;
    }

    private Intent e() {
        String packageName = getActivity().getPackageName();
        if (packageName.equals("com.avast.android.antitheft")) {
            packageName = "com.avast.android.mobilesecurity";
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + packageName));
        intent.addFlags(524288);
        return intent;
    }

    private boolean a(Intent intent) {
        return getActivity().getPackageManager().queryIntentActivities(intent, Menu.CATEGORY_CONTAINER).size() > 0;
    }
}
