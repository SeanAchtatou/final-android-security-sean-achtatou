package com.avast.android.generic.ui.widget;

import android.widget.CompoundButton;

/* compiled from: SwitchRow */
class ac implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SwitchRow f1135a;

    ac(SwitchRow switchRow) {
        this.f1135a = switchRow;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (!this.f1135a.o) {
            if (this.f1135a.e() != null && !this.f1135a.p) {
                this.f1135a.e().a(this.f1135a.g, z);
            }
            if (this.f1135a.n != null) {
                this.f1135a.n.a(this.f1135a, z);
            }
        }
    }
}
