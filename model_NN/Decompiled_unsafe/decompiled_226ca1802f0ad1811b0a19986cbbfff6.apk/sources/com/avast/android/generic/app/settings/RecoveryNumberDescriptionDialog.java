package com.avast.android.generic.app.settings;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.r;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ui.PasswordDialog;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;

public class RecoveryNumberDescriptionDialog extends DialogFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ab f492a;

    /* renamed from: b  reason: collision with root package name */
    private r f493b;

    /* renamed from: c  reason: collision with root package name */
    private BroadcastReceiver f494c;
    private Object d;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f493b = r.a(getActivity());
        this.f494c = new a(this);
        this.f492a = (ab) aa.a(getActivity(), ab.class);
        this.d = PasswordDialog.a(getActivity(), com.avast.android.generic.r.message_recovery_number_change_pin_check_after_description, new b(this));
    }

    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
        boolean z = getArguments().getBoolean("after_pin_setup", false);
        builder.setMessage(z ? x.msg_password_recovery_description_after_pin : x.msg_password_recovery_description);
        builder.setPositiveButton(x.l_password_recovery_description_button, new c(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new d(this, create, z));
        return create;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (getFragmentManager() != null) {
            SetRecoveryNumberDialog setRecoveryNumberDialog = new SetRecoveryNumberDialog();
            Bundle bundle = new Bundle();
            bundle.putBoolean("after_pin_setup", getArguments().getBoolean("after_pin_setup", false));
            setRecoveryNumberDialog.setArguments(bundle);
            setRecoveryNumberDialog.show(getFragmentManager(), "dialog_recovery_number");
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.f493b.a(this.f494c, new IntentFilter("com.avast.android.generic.app.settings.CLOSE_RECOVERY_DESCRIPTION"));
    }

    public void onDetach() {
        super.onDetach();
        try {
            this.f493b.a(this.f494c);
        } catch (Exception e) {
        }
    }
}
