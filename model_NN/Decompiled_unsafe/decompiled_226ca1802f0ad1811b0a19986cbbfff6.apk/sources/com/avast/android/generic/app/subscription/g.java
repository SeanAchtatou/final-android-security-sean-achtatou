package com.avast.android.generic.app.subscription;

import android.content.Intent;
import com.avast.android.generic.licensing.a.a;
import com.avast.android.generic.licensing.b;
import com.avast.android.generic.licensing.f;
import com.avast.android.generic.licensing.m;
import com.avast.android.generic.util.w;
import com.avast.android.generic.x;

/* compiled from: SubscriptionFragment */
class g implements f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f548a;

    g(SubscriptionFragment subscriptionFragment) {
        this.f548a = subscriptionFragment;
    }

    public void a() {
        if (this.f548a.isAdded()) {
            this.f548a.a(this.f548a.t);
        }
    }

    public void b() {
        if (this.f548a.isAdded()) {
            this.f548a.n();
            this.f548a.p();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:32:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void c() {
        /*
            r7 = this;
            r6 = 0
            r5 = 1
            r1 = 0
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            boolean r0 = r0.isAdded()
            if (r0 != 0) goto L_0x000c
        L_0x000b:
            return
        L_0x000c:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            r0.n()
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            com.avast.android.generic.licensing.c r0 = r0.t
            com.avast.c.a.a.v r0 = r0.b()
            if (r0 == 0) goto L_0x0028
            int[] r2 = com.avast.android.generic.app.subscription.t.f575b
            int r0 = r0.ordinal()
            r0 = r2[r0]
            switch(r0) {
                case 1: goto L_0x0075;
                case 2: goto L_0x0083;
                case 3: goto L_0x0091;
                case 4: goto L_0x009f;
                case 5: goto L_0x00ae;
                case 6: goto L_0x00bd;
                case 7: goto L_0x00cc;
                case 8: goto L_0x0102;
                case 9: goto L_0x0111;
                default: goto L_0x0028;
            }
        L_0x0028:
            r0 = r1
        L_0x0029:
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            com.avast.android.generic.app.subscription.SubscriptionFragment r3 = r7.f548a
            com.avast.android.generic.licensing.c r3 = r3.t
            java.util.List r3 = r3.a()
            boolean r2 = r2.a(r3)
            com.avast.android.generic.app.subscription.SubscriptionFragment r3 = r7.f548a
            r3.g()
            com.avast.android.generic.app.subscription.SubscriptionFragment r3 = r7.f548a
            com.actionbarsherlock.view.MenuItem r3 = r3.v
            if (r3 == 0) goto L_0x0120
            com.avast.android.generic.app.subscription.SubscriptionFragment r1 = r7.f548a
            com.actionbarsherlock.view.MenuItem r1 = r1.v
            r1.setEnabled(r5)
        L_0x004f:
            if (r2 != 0) goto L_0x0065
            com.avast.android.generic.app.subscription.SubscriptionFragment r1 = r7.f548a
            android.view.ViewGroup r1 = r1.h
            r2 = 8
            r1.setVisibility(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r1 = r7.f548a
            android.view.ViewGroup r1 = r1.g
            r1.setVisibility(r6)
        L_0x0065:
            if (r0 == 0) goto L_0x000b
            com.avast.android.generic.app.subscription.SubscriptionFragment r1 = r7.f548a
            android.support.v4.app.FragmentActivity r1 = r1.getActivity()
            android.widget.Toast r0 = android.widget.Toast.makeText(r1, r0, r5)
            r0.show()
            goto L_0x000b
        L_0x0075:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_code_already_consumed
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x0083:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_code_not_valid_anymore
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x0091:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_code_not_yet_valid
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x009f:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_code_unknown
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x00ae:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_code_locked
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x00bd:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_license_not_found
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x00cc:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            com.avast.android.generic.licensing.c r0 = r0.t
            java.lang.String r0 = r0.c()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00f9
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_invalid_operator
            java.lang.Object[] r3 = new java.lang.Object[r5]
            com.avast.android.generic.app.subscription.SubscriptionFragment r4 = r7.f548a
            com.avast.android.generic.licensing.c r4 = r4.t
            java.lang.String r4 = r4.c()
            r3[r6] = r4
            java.lang.String r0 = r0.getString(r2, r3)
        L_0x00f2:
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x00f9:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_invalid_operator_noop
            java.lang.String r0 = r0.getString(r2)
            goto L_0x00f2
        L_0x0102:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_invalid_country
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x0111:
            com.avast.android.generic.app.subscription.SubscriptionFragment r0 = r7.f548a
            int r2 = com.avast.android.generic.x.l_offers_generic_error
            java.lang.String r0 = r0.getString(r2)
            com.avast.android.generic.app.subscription.SubscriptionFragment r2 = r7.f548a
            java.lang.String unused = r2.x = r1
            goto L_0x0029
        L_0x0120:
            com.avast.android.generic.app.subscription.SubscriptionFragment r3 = r7.f548a
            com.avast.android.generic.app.subscription.ad r3 = r3.w
            if (r3 != 0) goto L_0x0132
            com.avast.android.generic.app.subscription.SubscriptionFragment r3 = r7.f548a
            com.avast.android.generic.app.subscription.ad r4 = new com.avast.android.generic.app.subscription.ad
            r4.<init>(r1)
            com.avast.android.generic.app.subscription.ad unused = r3.w = r4
        L_0x0132:
            com.avast.android.generic.app.subscription.SubscriptionFragment r1 = r7.f548a
            com.avast.android.generic.app.subscription.ad r1 = r1.w
            r1.f529a = r5
            goto L_0x004f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.app.subscription.g.c():void");
    }

    public void d() {
        if (this.f548a.isAdded()) {
            this.f548a.n();
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.l_offers_subscriptions_not_supported), ae.WARNING);
        }
    }

    public void a(boolean z) {
        if (this.f548a.isAdded() && !z) {
            this.f548a.n();
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.l_offers_no_internet_connectivity), ae.WARNING);
        }
    }

    public void a(int i) {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            a.a(this.f548a, i, 255);
        }
    }

    public void b(int i) {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            a.a(this.f548a, i, 255);
        }
    }

    public void a(Intent intent) {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            a.a(this.f548a, intent, 255);
        }
    }

    public void e() {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.msg_home_error_restoring_transactions_no_google_account), ae.WARNING);
        }
    }

    public void a(Exception exc) {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            if (exc != null) {
                this.f548a.a(this.f548a.r, this.f548a.r.getString(x.l_cannot_query_offers_error, new Object[]{w.a(this.f548a.r, exc)}), ae.CRITICAL);
                return;
            }
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.l_cannot_query_offers_generic), ae.CRITICAL);
        }
    }

    public void f() {
    }

    public void a(b bVar) {
        if (this.f548a.isAdded()) {
            if (bVar.a() == null || bVar.a() == m.NOT_AVAILABLE || bVar.a() == m.UNKNOWN) {
                this.f548a.r.setResult(99);
                this.f548a.r.finish();
                return;
            }
            this.f548a.r.setResult(-1);
            this.f548a.d();
        }
    }

    public void g() {
        if (this.f548a.isAdded()) {
            this.f548a.n();
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.l_offers_google_play_invalid), ae.WARNING);
        }
    }

    public void h() {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.msg_home_error_restoring_invalid_credentials), ae.WARNING);
        }
    }

    public void i() {
        if (this.f548a.isAdded()) {
            this.f548a.p();
            this.f548a.a(this.f548a.r, this.f548a.r.getString(x.msg_home_error_restoring_transactions_message_offer), ae.WARNING);
        }
    }
}
