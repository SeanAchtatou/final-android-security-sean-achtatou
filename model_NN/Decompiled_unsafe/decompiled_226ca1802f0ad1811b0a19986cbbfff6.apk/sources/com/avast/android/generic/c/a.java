package com.avast.android.generic.c;

import android.content.Context;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.internet.b;
import com.avast.android.generic.internet.e;
import com.avast.android.generic.internet.j;
import com.avast.android.generic.internet.k;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.az;
import com.avast.android.generic.util.ba;
import com.avast.android.generic.util.bc;
import com.avast.android.generic.x;
import com.avast.b.a.a.a.m;
import com.avast.b.a.a.ak;
import java.text.DateFormat;
import java.util.LinkedList;
import java.util.List;

/* compiled from: Command */
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private d f619a;

    /* renamed from: b  reason: collision with root package name */
    private boolean f620b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f621c;
    private String d;
    private String e;
    private String f;
    private boolean g;
    private int h;
    private c i;

    public String a() {
        return this.e;
    }

    public void a(boolean z) {
        this.f621c = z;
    }

    public boolean b() {
        return this.f621c;
    }

    public void b(boolean z) {
        this.f620b = z;
    }

    public String c() {
        return this.d;
    }

    public d d() {
        return this.f619a;
    }

    public String e() {
        return this.f;
    }

    public boolean f() {
        return this.f619a.c();
    }

    public final void g() {
        f d2 = this.f619a.d();
        d d3 = d();
        AvastService f2 = d3.f();
        if (!d3.a()) {
            n();
            a(f2, d3, d2, false);
        }
    }

    public final boolean h() {
        d d2 = d();
        AvastService f2 = d2.f();
        if (d2.a()) {
            return false;
        }
        m();
        return a(f2, d2);
    }

    public final void i() {
        f i2 = this.f619a.i();
        d d2 = d();
        AvastService f2 = d2.f();
        if (!d2.a()) {
            a(f2, d2, i2, true);
        }
    }

    private final void a(Context context, d dVar, f fVar, boolean z) {
        String str;
        ba baVar;
        String a2;
        String str2;
        String str3;
        String str4;
        ab abVar = (ab) aa.a(dVar.f(), ad.class);
        if (this instanceof p) {
            switch (b.f623b[fVar.ordinal()]) {
                case 1:
                    if (dVar.p()) {
                        dVar.f().b(new bc(e(), z ? dVar.j() : (String) dVar.g()), new n(dVar), j());
                        return;
                    } else {
                        dVar.f().a(new bc(e(), z ? dVar.j() : (String) dVar.g()), new n(dVar), j());
                        return;
                    }
                case 2:
                    if (dVar.p()) {
                        dVar.f().b(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.avast_ok) + " " + (z ? dVar.j() : (String) dVar.g())), new n(dVar), j());
                        return;
                    } else {
                        dVar.f().a(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.avast_ok) + " " + (z ? dVar.j() : (String) dVar.g())), new n(dVar), j());
                        return;
                    }
                case 3:
                    List k = dVar.k();
                    LinkedList linkedList = new LinkedList();
                    DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
                    DateFormat timeFormat = android.text.format.DateFormat.getTimeFormat(context);
                    for (Object next : k) {
                        if (next instanceof bc) {
                            bc bcVar = (bc) next;
                            String str5 = dateFormat.format(Long.valueOf(bcVar.d)) + " " + timeFormat.format(Long.valueOf(bcVar.d)) + ", SMS ";
                            if (bcVar.f1213c) {
                                str3 = str5 + context.getString(x.from_small);
                            } else {
                                str3 = str5 + context.getString(x.to_small);
                            }
                            String str6 = str3 + " ";
                            if (bcVar.f1211a == null || bcVar.f1211a.length() <= 0) {
                                str4 = str6 + "?";
                            } else {
                                str4 = str6 + bcVar.f1211a;
                            }
                            linkedList.add(new bc(null, (str4 + ": ") + bcVar.f1212b));
                        } else if (next instanceof az) {
                            az azVar = (az) next;
                            String str7 = (dateFormat.format(Long.valueOf(azVar.d)) + " " + timeFormat.format(Long.valueOf(azVar.d)) + ", ") + context.getString(x.qtn_call) + " ";
                            switch (b.f622a[azVar.f1202a.ordinal()]) {
                                case 1:
                                case 2:
                                    str7 = str7 + context.getString(x.from_small);
                                    break;
                                case 3:
                                    str7 = str7 + context.getString(x.to_small);
                                    break;
                            }
                            String str8 = str7 + " ";
                            if (azVar.f1203b == null || azVar.f1203b.length() <= 0) {
                                str2 = str8 + "?";
                            } else {
                                str2 = str8 + azVar.f1203b;
                            }
                            linkedList.add(new bc(null, (str2 + " " + context.getString(x.qtn_duration)) + " " + azVar.f1204c + " " + context.getString(x.qtn_duration_unit) + "."));
                        } else if ((next instanceof ba) && (a2 = (baVar = (ba) next).a()) != null) {
                            if (baVar.b() != null) {
                                a2 = a2 + ", " + baVar.b();
                            }
                            if (baVar.c() != null) {
                                a2 = a2 + ", " + baVar.c();
                            }
                            linkedList.add(new bc(null, a2));
                        }
                    }
                    if (dVar.p()) {
                        dVar.f().b(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.submission_started)), null, j());
                        if (linkedList.size() > 0) {
                            dVar.f().b(e(), linkedList, new n(dVar), j());
                        }
                        dVar.f().b(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.submission_finished)), null, j());
                        return;
                    }
                    dVar.f().a(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.submission_started)), (com.avast.android.generic.g.a.a) null, j());
                    if (linkedList.size() > 0) {
                        dVar.f().a(e(), linkedList, new n(dVar), j());
                    }
                    dVar.f().a(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.submission_finished)), (com.avast.android.generic.g.a.a) null, j());
                    return;
                default:
                    return;
            }
        } else {
            switch (b.f623b[fVar.ordinal()]) {
                case 1:
                    if (!this.f620b) {
                        dVar.f().a(new e((ak) dVar.g(), j(), dVar.n()), this, z);
                        return;
                    } else if (dVar.p()) {
                        dVar.f().b(new bc(e(), z ? dVar.j() : (String) dVar.g()), new n(dVar), j());
                        return;
                    } else {
                        dVar.f().a(new bc(e(), z ? dVar.j() : (String) dVar.g()), new n(dVar), j());
                        return;
                    }
                case 2:
                    if (!this.f620b) {
                        k n = dVar.n();
                        String c2 = c();
                        if (z) {
                            str = dVar.j();
                        } else {
                            str = (String) dVar.g();
                        }
                        dVar.f().a(new e(b.a(abVar, c2, str, (String) null, dVar.m()), j(), n), this, z);
                        return;
                    } else if (dVar.p()) {
                        dVar.f().b(new bc(e(), j.a(dVar.f(), abVar, c(), z ? dVar.j() : (String) dVar.g(), dVar.m())), new n(dVar), j());
                        return;
                    } else {
                        dVar.f().a(new bc(e(), j.a(dVar.f(), abVar, c(), z ? dVar.j() : (String) dVar.g(), dVar.m())), new n(dVar), j());
                        return;
                    }
                case 3:
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.service.AvastService.a(com.avast.android.generic.internet.e, com.avast.android.generic.c.a, boolean):void
     arg types: [com.avast.android.generic.internet.e, com.avast.android.generic.c.a, int]
     candidates:
      com.avast.android.generic.service.AvastService.a(com.avast.android.generic.util.bc, com.avast.android.generic.g.a.a, boolean):void
      com.avast.android.generic.service.AvastService.a(com.avast.android.generic.internet.e, com.avast.android.generic.c.a, boolean):void */
    private boolean a(Context context, d dVar) {
        ab abVar = (ab) aa.a(context, ad.class);
        if (this instanceof p) {
            if (dVar.p()) {
                dVar.f().b(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.avast_error) + " " + dVar.e()), new n(dVar), j());
            } else {
                dVar.f().a(new bc(e(), dVar.f().getString(dVar.f().d()) + " " + dVar.f().getString(x.avast_error) + " " + dVar.e()), new n(dVar), j());
            }
        } else if (!this.f620b) {
            dVar.f().a(new e(b.a(abVar, c(), dVar.e(), dVar.h(), a()), j()), this, false);
        } else if (dVar.p()) {
            dVar.f().b(new bc(e(), j.a(dVar.f(), abVar, a(), c(), dVar.e(), dVar.h())), new n(dVar), j());
        } else {
            dVar.f().a(new bc(e(), j.a(dVar.f(), abVar, a(), c(), dVar.e(), dVar.h())), new n(dVar), j());
        }
        return false;
    }

    public boolean j() {
        return this.g;
    }

    public void a(String str) {
        if (this.i != null) {
            this.i.a(str);
        }
    }

    public void k() {
        if (this.i != null) {
            this.i.a();
        }
    }

    public int l() {
        return this.h;
    }

    private void m() {
        if (this.f619a.h() == null || this.f619a.h() == m.NONE) {
            this.f619a.a((String) null);
        } else {
            this.f619a.a(this.f619a.h().name());
        }
    }

    private void n() {
        this.f619a.q();
    }
}
