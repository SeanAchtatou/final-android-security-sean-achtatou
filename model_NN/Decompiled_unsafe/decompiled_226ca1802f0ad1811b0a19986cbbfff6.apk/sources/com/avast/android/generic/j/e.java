package com.avast.android.generic.j;

import com.avast.android.generic.util.z;

/* compiled from: TaskHandler */
class e implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ d f836a;

    e(d dVar) {
        this.f836a = dVar;
    }

    public void a() {
        synchronized (this.f836a.e) {
            d.b(this.f836a);
            z.a("AvastGeneric", this.f836a.f833a, "EventsRunning at " + this.f836a.d + " (handleEvent2 end)");
        }
        if (!this.f836a.a()) {
            this.f836a.f833a.b();
        }
    }
}
