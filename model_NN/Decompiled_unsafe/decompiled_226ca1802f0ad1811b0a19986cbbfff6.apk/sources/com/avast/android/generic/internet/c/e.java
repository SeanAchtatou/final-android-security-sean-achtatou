package com.avast.android.generic.internet.c;

/* compiled from: AvastAccountConnector */
public enum e {
    EMAIL(3),
    FACEBOOK(5),
    GPLUS(7);
    
    private final int d;

    private e(int i) {
        this.d = i;
    }

    public int a() {
        return this.d;
    }
}
