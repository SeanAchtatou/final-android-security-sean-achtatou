package com.avast.android.generic.ui.widget;

import android.widget.CompoundButton;

/* compiled from: CheckBoxRow */
class b implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CheckBoxRow f1141a;

    b(CheckBoxRow checkBoxRow) {
        this.f1141a = checkBoxRow;
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (!this.f1141a.m) {
            if (this.f1141a.e() != null && !this.f1141a.n) {
                this.f1141a.e().a(this.f1141a.g, z);
            }
            if (this.f1141a.f1078c != null) {
                this.f1141a.f1078c.a(this.f1141a, z);
            }
        }
    }
}
