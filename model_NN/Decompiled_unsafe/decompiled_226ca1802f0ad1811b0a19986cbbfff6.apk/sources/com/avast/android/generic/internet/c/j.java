package com.avast.android.generic.internet.c;

import com.avast.android.generic.internet.c.a.aa;

/* compiled from: AvastBackendException */
public class j extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private aa f815a = null;

    protected j(String str, aa aaVar) {
        this.f815a = aaVar;
    }

    public aa a() {
        return this.f815a;
    }
}
