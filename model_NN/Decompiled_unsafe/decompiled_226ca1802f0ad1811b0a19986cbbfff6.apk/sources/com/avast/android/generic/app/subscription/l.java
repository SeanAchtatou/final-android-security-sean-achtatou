package com.avast.android.generic.app.subscription;

import com.avast.android.generic.licensing.c.h;
import com.avast.android.generic.licensing.c.i;
import com.avast.android.generic.licensing.c.k;
import com.avast.android.generic.x;

/* compiled from: SubscriptionFragment */
class l implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f558a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f559b;

    l(SubscriptionFragment subscriptionFragment, k kVar) {
        this.f559b = subscriptionFragment;
        this.f558a = kVar;
    }

    public void a(i iVar) {
        if (this.f559b.isAdded()) {
            if (!this.f559b.e.b()) {
                ErrorDialog.a(this.f559b.getFragmentManager(), 1, x.l_subscription_error_billing_connection_title, x.l_offers_subscriptions_not_supported, 0, 0, 0);
            } else {
                this.f559b.c(this.f558a);
            }
        }
    }
}
