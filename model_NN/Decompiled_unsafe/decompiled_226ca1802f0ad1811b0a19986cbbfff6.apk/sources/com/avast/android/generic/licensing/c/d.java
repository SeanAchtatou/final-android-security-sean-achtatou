package com.avast.android.generic.licensing.c;

import java.util.ArrayList;
import java.util.List;

/* compiled from: IabHelper */
class d implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ List f862a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ e f863b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ f f864c;
    final /* synthetic */ b d;

    d(b bVar, List list, e eVar, f fVar) {
        this.d = bVar;
        this.f862a = list;
        this.f863b = eVar;
        this.f864c = fVar;
    }

    public void run() {
        ArrayList arrayList = new ArrayList();
        for (l lVar : this.f862a) {
            try {
                this.d.a(lVar);
                arrayList.add(new i(0, "Successful consume of sku " + lVar.c()));
            } catch (a e) {
                arrayList.add(e.a());
            }
        }
        this.d.c();
        if (this.f863b != null) {
            this.f863b.a((l) this.f862a.get(0), (i) arrayList.get(0));
        }
        if (this.f864c != null) {
            this.f864c.a(this.f862a, arrayList);
        }
    }
}
