package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.webkit.WebView;
import java.util.Locale;

/* compiled from: SubscriptionFragment */
class k implements DialogInterface.OnShowListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ com.avast.android.generic.licensing.c.k f555a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ WebView f556b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f557c;

    k(SubscriptionFragment subscriptionFragment, com.avast.android.generic.licensing.c.k kVar, WebView webView) {
        this.f557c = subscriptionFragment;
        this.f555a = kVar;
        this.f556b = webView;
    }

    public void onShow(DialogInterface dialogInterface) {
        String str;
        Locale locale = Locale.getDefault();
        String str2 = null;
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            str2 = locale.getLanguage();
        }
        String h = this.f555a.h();
        if (str2 == null) {
            str = h;
        } else if (!h.contains("?")) {
            str = (h + "?") + "l=" + str2;
        } else {
            str = h + "&l=" + str2;
        }
        this.f556b.loadUrl(str);
    }
}
