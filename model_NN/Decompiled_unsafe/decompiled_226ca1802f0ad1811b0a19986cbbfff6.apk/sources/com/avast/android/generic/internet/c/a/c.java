package com.avast.android.generic.internet.c.a;

import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class c extends GeneratedMessageLite implements e {

    /* renamed from: a  reason: collision with root package name */
    private static final c f752a = new c(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f753b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f754c;
    /* access modifiers changed from: private */
    public long d;
    private byte e;
    private int f;

    private c(d dVar) {
        super(dVar);
        this.e = -1;
        this.f = -1;
    }

    private c(boolean z) {
        this.e = -1;
        this.f = -1;
    }

    public static c a() {
        return f752a;
    }

    public boolean b() {
        return (this.f753b & 1) == 1;
    }

    public String c() {
        Object obj = this.f754c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f754c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString i() {
        Object obj = this.f754c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f754c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f753b & 2) == 2;
    }

    public long e() {
        return this.d;
    }

    private void j() {
        this.f754c = "";
        this.d = 0;
    }

    public final boolean isInitialized() {
        byte b2 = this.e;
        if (b2 == -1) {
            this.e = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f753b & 1) == 1) {
            codedOutputStream.writeBytes(1, i());
        }
        if ((this.f753b & 2) == 2) {
            codedOutputStream.writeInt64(2, this.d);
        }
    }

    public int getSerializedSize() {
        int i = this.f;
        if (i == -1) {
            i = 0;
            if ((this.f753b & 1) == 1) {
                i = 0 + CodedOutputStream.computeBytesSize(1, i());
            }
            if ((this.f753b & 2) == 2) {
                i += CodedOutputStream.computeInt64Size(2, this.d);
            }
            this.f = i;
        }
        return i;
    }

    public static d f() {
        return d.g();
    }

    /* renamed from: g */
    public d newBuilderForType() {
        return f();
    }

    public static d a(c cVar) {
        return f().mergeFrom(cVar);
    }

    /* renamed from: h */
    public d toBuilder() {
        return a(this);
    }

    static {
        f752a.j();
    }
}
