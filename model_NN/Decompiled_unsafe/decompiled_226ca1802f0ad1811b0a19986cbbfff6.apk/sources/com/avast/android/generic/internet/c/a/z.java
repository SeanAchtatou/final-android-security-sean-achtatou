package com.avast.android.generic.internet.c.a;

import com.actionbarsherlock.R;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class z extends GeneratedMessageLite.Builder<y, z> implements ac {

    /* renamed from: a  reason: collision with root package name */
    private int f798a;

    /* renamed from: b  reason: collision with root package name */
    private Object f799b = "";

    /* renamed from: c  reason: collision with root package name */
    private aa f800c = aa.GENERIC;
    private Object d = "";

    private z() {
        f();
    }

    private void f() {
    }

    /* access modifiers changed from: private */
    public static z g() {
        return new z();
    }

    /* renamed from: a */
    public z clone() {
        return g().mergeFrom(d());
    }

    /* renamed from: b */
    public y getDefaultInstanceForType() {
        return y.a();
    }

    /* renamed from: c */
    public y build() {
        y d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public y d() {
        int i = 1;
        y yVar = new y(this);
        int i2 = this.f798a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        Object unused = yVar.f797c = this.f799b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        aa unused2 = yVar.d = this.f800c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        Object unused3 = yVar.e = this.d;
        int unused4 = yVar.f796b = i;
        return yVar;
    }

    /* renamed from: a */
    public z mergeFrom(y yVar) {
        if (yVar != y.a()) {
            if (yVar.b()) {
                a(yVar.c());
            }
            if (yVar.d()) {
                a(yVar.e());
            }
            if (yVar.f()) {
                b(yVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public z mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f798a |= 1;
                    this.f799b = codedInputStream.readBytes();
                    break;
                case 16:
                    aa a2 = aa.a(codedInputStream.readEnum());
                    if (a2 == null) {
                        break;
                    } else {
                        this.f798a |= 2;
                        this.f800c = a2;
                        break;
                    }
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    this.f798a |= 4;
                    this.d = codedInputStream.readBytes();
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public z a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f798a |= 1;
        this.f799b = str;
        return this;
    }

    public z a(aa aaVar) {
        if (aaVar == null) {
            throw new NullPointerException();
        }
        this.f798a |= 2;
        this.f800c = aaVar;
        return this;
    }

    public z b(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f798a |= 4;
        this.d = str;
        return this;
    }
}
