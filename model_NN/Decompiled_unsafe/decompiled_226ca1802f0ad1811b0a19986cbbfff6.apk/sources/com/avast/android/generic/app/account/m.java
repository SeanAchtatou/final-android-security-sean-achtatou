package com.avast.android.generic.app.account;

import android.content.Context;
import com.avast.android.generic.util.t;
import com.avast.android.generic.x;

/* compiled from: ConnectAccountFragment */
class m extends r {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Context f442a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f443b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    m(ConnectAccountFragment connectAccountFragment, Context context, Context context2) {
        super(context);
        this.f443b = connectAccountFragment;
        this.f442a = context2;
    }

    public void a(String str) {
        if (this.f443b.isAdded()) {
            this.f443b.w.post(new n(this, str));
            this.f443b.e();
            this.f443b.v.setEnabled(true);
        }
        r unused = this.f443b.f360c = (r) null;
    }

    public void a() {
        t.e("ConnectAccountFragment", "onPasswordNeeded should never happen while connecting with Facebook.");
        a(this.f442a.getString(x.msg_avast_account_error_internal_error));
    }

    public void a(z zVar, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        a(zVar, str7);
        this.f443b.a(this.f442a, str, str2, str3, str4, str5, str6);
        r unused = this.f443b.f360c = (r) null;
    }
}
