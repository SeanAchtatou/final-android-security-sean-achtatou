package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum g {
    ACCOUNT("gen-Account"),
    POST_INSTALL("gen-PostInstall"),
    MESSAGING("gen-Messaging"),
    AT_INSTALL("gen-AtInstall"),
    BILLING("gen-Billing"),
    APPLICATION("gen-App");
    
    String g;

    private g(String str) {
        this.g = str;
    }

    public String a() {
        return this.g;
    }
}
