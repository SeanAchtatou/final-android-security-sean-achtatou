package com.avast.android.generic.licensing;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.avast.android.generic.ab;
import com.avast.android.generic.g.b.a;
import com.avast.android.generic.licensing.a.b;
import com.avast.android.generic.util.ae;
import com.avast.c.a.a.aa;
import com.avast.c.a.a.an;
import com.avast.c.a.a.f;
import com.avast.c.a.a.k;
import com.avast.c.a.a.l;
import com.avast.c.a.a.m;
import java.util.Locale;
import java.util.Set;

public abstract class PurchaseConfirmationService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f840a = new Object();

    /* renamed from: b  reason: collision with root package name */
    private static final Object f841b = new Object();
    private static m j = null;

    /* renamed from: c  reason: collision with root package name */
    private i f842c = null;
    private k d = null;
    private Integer e = null;
    private Intent f = null;
    private j g = null;
    private boolean h = false;
    private boolean i = false;

    public PurchaseConfirmationService() {
        super("PurchaseConfirmationService");
    }

    public static void a(Context context) {
        ae.a(context, new Intent(), context.getPackageName(), ".licensing.PurchaseConfirmationService");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    public static void b(Context context) {
        Intent intent = new Intent();
        ae.c(context, intent, context.getPackageName(), ".licensing.PurchaseConfirmationService");
        intent.putExtra("forced", true);
        ae.a(context, intent, context.getPackageName(), ".licensing.PurchaseConfirmationService");
    }

    public static k a(Context context, Set<b> set, ab abVar, boolean z, String str, an anVar) {
        l K = k.K();
        for (b next : set) {
            K.a(f.h().a(next.a()).b(next.b()).c(next.c()).build());
        }
        K.a(abVar.q());
        if (abVar.t()) {
            K.b(abVar.w());
            K.c(abVar.y());
        }
        K.a(anVar);
        K.d(str);
        K.a(true);
        K.b(true);
        String ac = abVar.ac();
        if (!TextUtils.isEmpty(ac)) {
            K.g(ac);
            K.a(m.f().a("uniqueInstallationIdentifier").b(ac).build());
        }
        if (z) {
            K.a(System.currentTimeMillis());
            Locale locale = Locale.getDefault();
            if (!TextUtils.isEmpty(locale.getLanguage())) {
                K.e(locale.getLanguage());
            }
            K.c(a.h(context));
            String b2 = a.b(context);
            if (!TextUtils.isEmpty(b2)) {
                K.f(b2);
            }
            K.d(a.f(context));
        }
        return K.build();
    }

    public static aa a(Context context, ab abVar, String str, String str2, String str3, an anVar) {
        com.avast.c.a.a.ab z = aa.z();
        z.h(str2);
        z.i(str3);
        z.a(abVar.q());
        if (abVar.t()) {
            z.b(abVar.w());
            z.c(abVar.y());
        }
        z.a(anVar);
        z.d(str);
        String ac = abVar.ac();
        if (!TextUtils.isEmpty(ac)) {
            z.f(ac);
        }
        Locale locale = Locale.getDefault();
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            z.g(locale.getLanguage());
        }
        z.a(a.h(context));
        String b2 = a.b(context);
        if (!TextUtils.isEmpty(b2)) {
            z.e(b2);
        }
        z.b(a.f(context));
        return z.build();
    }

    /* JADX WARNING: Removed duplicated region for block: B:100:0x0166 A[LOOP:0: B:9:0x0049->B:100:0x0166, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x010c A[EDGE_INSN: B:102:0x010c->B:66:0x010c ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0151  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r29, android.net.Uri r30, com.avast.android.generic.ab r31) {
        /*
            com.avast.android.generic.licensing.database.d.a(r29)
            r3 = 0
            com.avast.android.generic.licensing.database.d r2 = new com.avast.android.generic.licensing.database.d     // Catch:{ Exception -> 0x015a, all -> 0x014d }
            r0 = r29
            r1 = r30
            r2.<init>(r0, r1)     // Catch:{ Exception -> 0x015a, all -> 0x014d }
            android.database.Cursor r8 = r2.a()     // Catch:{ Exception -> 0x015a, all -> 0x014d }
            if (r8 == 0) goto L_0x0114
            r2 = 0
            r4 = 0
            long r14 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            boolean r3 = r8.moveToFirst()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r3 == 0) goto L_0x0114
            r6 = 0
            r7 = 0
            java.lang.String r3 = "productId"
            int r16 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.String r3 = "next_check_date"
            int r17 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.String r3 = "confirmed"
            int r18 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.String r3 = "validity"
            int r19 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.String r3 = "expire_date"
            int r20 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.String r3 = "subscription"
            int r21 = r8.getColumnIndex(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            r9 = r6
            r10 = r4
            r12 = r2
        L_0x0049:
            r2 = 0
            r0 = r17
            boolean r3 = r8.isNull(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r3 != 0) goto L_0x0163
            r0 = r17
            long r2 = r8.getLong(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            r6 = r2
        L_0x005d:
            r0 = r18
            int r2 = r8.getInt(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 <= 0) goto L_0x011a
            r2 = 1
            r5 = r2
        L_0x0067:
            r2 = 0
            r0 = r19
            boolean r3 = r8.isNull(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r3 != 0) goto L_0x0160
            r0 = r19
            int r2 = r8.getInt(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 <= 0) goto L_0x011e
            r2 = 1
        L_0x0079:
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            r4 = r2
        L_0x007e:
            r0 = r16
            java.lang.String r13 = r8.getString(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r4 == 0) goto L_0x008c
            boolean r2 = r4.booleanValue()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 == 0) goto L_0x0121
        L_0x008c:
            r2 = 1
            r3 = r2
        L_0x008e:
            if (r5 == 0) goto L_0x00a4
            if (r6 == 0) goto L_0x0125
            long r22 = r6.longValue()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            r24 = -1
            int r2 = (r22 > r24 ? 1 : (r22 == r24 ? 0 : -1))
            if (r2 == 0) goto L_0x0125
            long r5 = r6.longValue()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            int r2 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r2 >= 0) goto L_0x0125
        L_0x00a4:
            r2 = 1
        L_0x00a5:
            if (r3 == 0) goto L_0x0128
            if (r2 == 0) goto L_0x0128
            r2 = 1
        L_0x00aa:
            if (r2 != 0) goto L_0x015c
            if (r4 == 0) goto L_0x015c
            boolean r2 = r4.booleanValue()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 == 0) goto L_0x015c
            r2 = 1
            java.lang.String r3 = "trial"
            boolean r3 = r13.contains(r3)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r12 == 0) goto L_0x00ca
            if (r7 == 0) goto L_0x00ca
            java.lang.String r4 = "trial"
            boolean r4 = r7.contains(r4)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r4 != 0) goto L_0x00ca
            if (r3 == 0) goto L_0x00ca
            r2 = 0
        L_0x00ca:
            if (r2 == 0) goto L_0x015c
            r12 = 1
            r0 = r20
            long r5 = r8.getLong(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            r2 = -1
            int r2 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r2 != 0) goto L_0x012a
            r2 = 1
            r4 = r2
        L_0x00db:
            r2 = -1
            int r2 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r2 == 0) goto L_0x012d
            int r2 = (r5 > r10 ? 1 : (r5 == r10 ? 0 : -1))
            if (r2 <= 0) goto L_0x012d
            r2 = 1
            r3 = r2
        L_0x00e7:
            r0 = r21
            boolean r2 = r8.isNull(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 != 0) goto L_0x0130
            r0 = r21
            int r2 = r8.getInt(r0)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r2 <= 0) goto L_0x0130
            r2 = 1
        L_0x00f8:
            if (r4 != 0) goto L_0x00fc
            if (r3 == 0) goto L_0x016b
        L_0x00fc:
            r7 = r13
            r26 = r2
            r2 = r12
            r27 = r5
            r4 = r27
            r6 = r26
        L_0x0106:
            boolean r3 = r8.moveToNext()     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            if (r3 != 0) goto L_0x0166
            if (r2 == 0) goto L_0x0132
            r3 = 1
            r2 = r31
            r2.a(r3, r4, r6, r7)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
        L_0x0114:
            if (r8 == 0) goto L_0x0119
            r8.close()
        L_0x0119:
            return
        L_0x011a:
            r2 = 0
            r5 = r2
            goto L_0x0067
        L_0x011e:
            r2 = 0
            goto L_0x0079
        L_0x0121:
            r2 = 0
            r3 = r2
            goto L_0x008e
        L_0x0125:
            r2 = 0
            goto L_0x00a5
        L_0x0128:
            r2 = 0
            goto L_0x00aa
        L_0x012a:
            r2 = 0
            r4 = r2
            goto L_0x00db
        L_0x012d:
            r2 = 0
            r3 = r2
            goto L_0x00e7
        L_0x0130:
            r2 = 0
            goto L_0x00f8
        L_0x0132:
            r3 = 0
            r4 = -2
            r6 = 0
            java.lang.String r7 = ""
            r2 = r31
            r2.a(r3, r4, r6, r7)     // Catch:{ Exception -> 0x013e, all -> 0x0155 }
            goto L_0x0114
        L_0x013e:
            r2 = move-exception
            r3 = r8
        L_0x0140:
            java.lang.String r4 = "AvastGenericLic"
            java.lang.String r5 = "Can not post initialize settings"
            com.avast.android.generic.util.z.a(r4, r5, r2)     // Catch:{ all -> 0x0157 }
            if (r3 == 0) goto L_0x0119
            r3.close()
            goto L_0x0119
        L_0x014d:
            r2 = move-exception
            r8 = r3
        L_0x014f:
            if (r8 == 0) goto L_0x0154
            r8.close()
        L_0x0154:
            throw r2
        L_0x0155:
            r2 = move-exception
            goto L_0x014f
        L_0x0157:
            r2 = move-exception
            r8 = r3
            goto L_0x014f
        L_0x015a:
            r2 = move-exception
            goto L_0x0140
        L_0x015c:
            r6 = r9
            r4 = r10
            r2 = r12
            goto L_0x0106
        L_0x0160:
            r4 = r2
            goto L_0x007e
        L_0x0163:
            r6 = r2
            goto L_0x005d
        L_0x0166:
            r9 = r6
            r10 = r4
            r12 = r2
            goto L_0x0049
        L_0x016b:
            r7 = r13
            r6 = r2
            r4 = r10
            r2 = r12
            goto L_0x0106
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.licensing.PurchaseConfirmationService.a(android.content.Context, android.net.Uri, com.avast.android.generic.ab):void");
    }
}
