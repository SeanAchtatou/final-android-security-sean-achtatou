package com.avast.android.generic.h;

import com.google.protobuf.Internal;

/* compiled from: CommunityIqProto */
public enum e implements Internal.EnumLite {
    MOBILE_SECURITY_INSTALL(0, 1),
    MOBILE_SECURITY_UPDATE(1, 2),
    ANTI_THEFT_INSTALL(2, 3),
    ANTI_THEFT_UPDATE(3, 4),
    VPS_UPDATE_CHECK(4, 5),
    VPS_UPDATE(5, 6);
    
    private static Internal.EnumLiteMap<e> g = new f();
    private final int h;

    public final int getNumber() {
        return this.h;
    }

    public static e a(int i2) {
        switch (i2) {
            case 1:
                return MOBILE_SECURITY_INSTALL;
            case 2:
                return MOBILE_SECURITY_UPDATE;
            case 3:
                return ANTI_THEFT_INSTALL;
            case 4:
                return ANTI_THEFT_UPDATE;
            case 5:
                return VPS_UPDATE_CHECK;
            case 6:
                return VPS_UPDATE;
            default:
                return null;
        }
    }

    private e(int i2, int i3) {
        this.h = i3;
    }
}
