package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.avast.android.generic.n;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class RowImage extends Row {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f1102a;

    /* renamed from: b  reason: collision with root package name */
    private int f1103b;

    /* renamed from: c  reason: collision with root package name */
    private boolean f1104c;
    private ImageView m;

    public RowImage(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, i, y.Row));
    }

    public RowImage(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
        a(context, context.obtainStyledAttributes(attributeSet, z.h, n.rowStyle, y.Row));
        b(context, context.obtainStyledAttributes(attributeSet, z.g, n.rowStyle, y.Row));
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        typedArray.recycle();
    }

    /* access modifiers changed from: protected */
    public void b(Context context, TypedArray typedArray) {
        this.f1103b = typedArray.getResourceId(3, 0);
        typedArray.recycle();
        this.f1104c = ak.b(context);
    }

    /* access modifiers changed from: protected */
    public void a() {
        View inflate = inflate(getContext(), t.row_image, this);
        this.m = (ImageView) inflate.findViewById(r.c_next_arrow);
        if (this.f1104c) {
            this.m.setVisibility(8);
        }
        this.f1102a = (ImageView) inflate.findViewById(r.row_icon);
        if (this.f1103b != 0) {
            this.f1102a.setVisibility(0);
            this.f1102a.setImageResource(this.f1103b);
        }
    }

    public void setEnabled(boolean z) {
        super.setEnabled(z);
        super.setFocusable(z);
        super.setClickable(z);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1105a = isEnabled();
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        setEnabled(savedState.f1105a);
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new s();

        /* renamed from: a  reason: collision with root package name */
        boolean f1105a;

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1105a = parcel.readBundle().getBoolean("enabled");
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            Bundle bundle = new Bundle();
            bundle.putBoolean("enabled", this.f1105a);
            parcel.writeBundle(bundle);
        }
    }
}
