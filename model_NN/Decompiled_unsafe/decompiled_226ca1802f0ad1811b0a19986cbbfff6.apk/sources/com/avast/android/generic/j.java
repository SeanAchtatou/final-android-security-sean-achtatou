package com.avast.android.generic;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import com.avast.android.generic.a.a;
import com.avast.android.generic.ui.PasswordDialog;

/* compiled from: PasswordProtector */
final class j implements Handler.Callback {
    j() {
    }

    public boolean handleMessage(Message message) {
        a aVar;
        FragmentActivity fragmentActivity = i.f731b.get();
        if (message.what != r.message_passwordProtector || fragmentActivity == null) {
            return false;
        }
        if (message.arg1 == PasswordDialog.f993a && (aVar = (a) aa.a(i.f731b.get(), a.class)) != null) {
            aVar.a(true);
        }
        if (message.arg1 == PasswordDialog.f994b) {
            fragmentActivity.moveTaskToBack(true);
        }
        return true;
    }
}
