package com.avast.android.generic.ui.rtl;

import android.view.View;
import android.widget.RelativeLayout;

public class RelativeLayoutLtrToRtlConverter extends a {
    public RelativeLayoutLtrToRtlConverter(String str) {
        super(str);
    }

    public View ltrToRtlView(View view) {
        if (view instanceof RelativeLayout) {
            return ltrToRtlIfRelativeLayout((RelativeLayout) view);
        }
        throw new IllegalArgumentException("The view must be an instance of RelativeLayout and it's " + view.getClass() + ".");
    }

    private View ltrToRtlIfRelativeLayout(RelativeLayout relativeLayout) {
        for (int i = 0; i < relativeLayout.getChildCount(); i++) {
            View childAt = relativeLayout.getChildAt(i);
            swapMarginAndPadding(childAt);
            swapAlignment(childAt);
            childAt.invalidate();
        }
        return relativeLayout;
    }

    private void swapMarginAndPadding(View view) {
        if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
            int i = layoutParams.leftMargin;
            layoutParams.leftMargin = layoutParams.rightMargin;
            layoutParams.rightMargin = i;
        }
        view.setPadding(view.getPaddingRight(), view.getPaddingTop(), view.getPaddingLeft(), view.getPaddingBottom());
    }

    private void swapAlignment(View view) {
        if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
            int[] rules = layoutParams.getRules();
            int[] iArr = new int[rules.length];
            System.arraycopy(rules, 0, iArr, 0, rules.length);
            for (int i = 0; i < iArr.length; i++) {
                layoutParams.addRule(i, 0);
            }
            if (iArr[0] != 0) {
                layoutParams.addRule(1, iArr[0]);
            }
            if (iArr[1] != 0) {
                layoutParams.addRule(0, iArr[1]);
            }
            if (iArr[5] != 0) {
                layoutParams.addRule(7, iArr[5]);
            }
            if (iArr[7] != 0) {
                layoutParams.addRule(5, iArr[7]);
            }
            if (iArr[9] != 0) {
                layoutParams.addRule(11, iArr[9]);
            }
            if (iArr[11] != 0) {
                layoutParams.addRule(9, iArr[11]);
            }
            if (iArr[9] == 0 && iArr[11] == 0 && iArr[5] == 0 && iArr[7] == 0 && iArr[0] == 0 && iArr[1] == 0) {
                layoutParams.addRule(11, -1);
            }
            for (int i2 = 0; i2 < iArr.length; i2++) {
                if (!(i2 == 0 || i2 == 1 || i2 == 5 || i2 == 7 || i2 == 9 || i2 == 11)) {
                    layoutParams.addRule(i2, iArr[i2]);
                }
            }
        }
    }
}
