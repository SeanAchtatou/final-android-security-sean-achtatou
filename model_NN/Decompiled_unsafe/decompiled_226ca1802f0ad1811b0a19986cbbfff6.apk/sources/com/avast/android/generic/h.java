package com.avast.android.generic;

import java.util.LinkedList;
import java.util.List;

/* compiled from: PackageConstants */
public class h {

    /* renamed from: a  reason: collision with root package name */
    public static final List<String> f697a = new LinkedList();

    /* renamed from: b  reason: collision with root package name */
    public static final List<String> f698b = new LinkedList();

    /* renamed from: c  reason: collision with root package name */
    public static final List<String> f699c = new LinkedList();

    static {
        f697a.add("com.avast.android.antitheft");
        f697a.add("com.avast.android.at_play");
        f697a.add("com.avast.android.mobilesecurity");
        f697a.add("com.avast.android.backup");
        f698b.add("com.avast.android.antitheft");
        f698b.add("com.avast.android.at_play");
        f699c.add("com.avast.android.vpn");
    }
}
