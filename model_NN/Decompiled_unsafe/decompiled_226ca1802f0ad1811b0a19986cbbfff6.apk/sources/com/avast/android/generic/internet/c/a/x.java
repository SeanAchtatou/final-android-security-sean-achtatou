package com.avast.android.generic.internet.c.a;

import com.actionbarsherlock.R;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedInputStream;
import com.google.protobuf.ExtensionRegistryLite;
import com.google.protobuf.GeneratedMessageLite;

/* compiled from: MyAvastPairing */
public final class x extends GeneratedMessageLite.Builder<w, x> implements ad {

    /* renamed from: a  reason: collision with root package name */
    private int f792a;

    /* renamed from: b  reason: collision with root package name */
    private ByteString f793b = ByteString.EMPTY;

    /* renamed from: c  reason: collision with root package name */
    private Object f794c = "";
    private y d = y.a();

    private x() {
        h();
    }

    private void h() {
    }

    /* access modifiers changed from: private */
    public static x i() {
        return new x();
    }

    /* renamed from: a */
    public x clone() {
        return i().mergeFrom(d());
    }

    /* renamed from: b */
    public w getDefaultInstanceForType() {
        return w.a();
    }

    /* renamed from: c */
    public w build() {
        w d2 = d();
        if (d2.isInitialized()) {
            return d2;
        }
        throw newUninitializedMessageException(d2);
    }

    public w d() {
        int i = 1;
        w wVar = new w(this);
        int i2 = this.f792a;
        if ((i2 & 1) != 1) {
            i = 0;
        }
        ByteString unused = wVar.f791c = this.f793b;
        if ((i2 & 2) == 2) {
            i |= 2;
        }
        Object unused2 = wVar.d = this.f794c;
        if ((i2 & 4) == 4) {
            i |= 4;
        }
        y unused3 = wVar.e = this.d;
        int unused4 = wVar.f790b = i;
        return wVar;
    }

    /* renamed from: a */
    public x mergeFrom(w wVar) {
        if (wVar != w.a()) {
            if (wVar.b()) {
                a(wVar.c());
            }
            if (wVar.d()) {
                a(wVar.e());
            }
            if (wVar.f()) {
                b(wVar.g());
            }
        }
        return this;
    }

    public final boolean isInitialized() {
        return true;
    }

    /* renamed from: a */
    public x mergeFrom(CodedInputStream codedInputStream, ExtensionRegistryLite extensionRegistryLite) {
        while (true) {
            int readTag = codedInputStream.readTag();
            switch (readTag) {
                case 0:
                    break;
                case 10:
                    this.f792a |= 1;
                    this.f793b = codedInputStream.readBytes();
                    break;
                case 18:
                    this.f792a |= 2;
                    this.f794c = codedInputStream.readBytes();
                    break;
                case R.styleable.SherlockTheme_textColorPrimaryDisableOnly:
                    z h = y.h();
                    if (e()) {
                        h.mergeFrom(f());
                    }
                    codedInputStream.readMessage(h, extensionRegistryLite);
                    a(h.d());
                    break;
                default:
                    if (parseUnknownField(codedInputStream, extensionRegistryLite, readTag)) {
                        break;
                    } else {
                        break;
                    }
            }
        }
        return this;
    }

    public x a(ByteString byteString) {
        if (byteString == null) {
            throw new NullPointerException();
        }
        this.f792a |= 1;
        this.f793b = byteString;
        return this;
    }

    public x a(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f792a |= 2;
        this.f794c = str;
        return this;
    }

    public boolean e() {
        return (this.f792a & 4) == 4;
    }

    public y f() {
        return this.d;
    }

    public x a(y yVar) {
        if (yVar == null) {
            throw new NullPointerException();
        }
        this.d = yVar;
        this.f792a |= 4;
        return this;
    }

    public x b(y yVar) {
        if ((this.f792a & 4) != 4 || this.d == y.a()) {
            this.d = yVar;
        } else {
            this.d = y.a(this.d).mergeFrom(yVar).d();
        }
        this.f792a |= 4;
        return this;
    }
}
