package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;
import com.avast.android.generic.licensing.c.k;

/* compiled from: SubscriptionFragment */
class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f549a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ SubscriptionFragment f550b;

    h(SubscriptionFragment subscriptionFragment, k kVar) {
        this.f550b = subscriptionFragment;
        this.f549a = kVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f550b.b(this.f549a);
    }
}
