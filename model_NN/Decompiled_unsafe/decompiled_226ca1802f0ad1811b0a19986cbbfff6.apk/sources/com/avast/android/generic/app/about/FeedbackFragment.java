package com.avast.android.generic.app.about;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import com.avast.android.generic.h.c;
import com.avast.android.generic.internet.a.a;
import com.avast.android.generic.m;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.ui.widget.SelectorRow;
import com.avast.android.generic.util.ad;
import com.avast.android.generic.util.ga.TrackedMultiToolFragment;
import com.avast.android.generic.x;
import java.util.regex.Pattern;

public class FeedbackFragment extends TrackedMultiToolFragment {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f329a = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}\\@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+");
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f330b;

    /* renamed from: c  reason: collision with root package name */
    private ScrollView f331c;
    /* access modifiers changed from: private */
    public EditText d;
    /* access modifiers changed from: private */
    public TextView e;
    private TextView f;
    /* access modifiers changed from: private */
    public EditText g;
    /* access modifiers changed from: private */
    public EditText h;
    /* access modifiers changed from: private */
    public TextView i;
    private TextView j;
    private SelectorRow k;
    /* access modifiers changed from: private */
    public CheckBoxRow l;
    /* access modifiers changed from: private */
    public CheckBoxRow m;
    /* access modifiers changed from: private */
    public CheckBoxRow n;
    private Button o;
    private Button p;
    /* access modifiers changed from: private */
    public byte[] q;
    /* access modifiers changed from: private */
    public byte[] r;
    /* access modifiers changed from: private */
    public c s;
    /* access modifiers changed from: private */
    public n t;
    private ProgressDialog u;

    public final String c() {
        return "about/feedback";
    }

    public int a() {
        return x.about_feedback;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setRetainInstance(true);
        if (bundle != null && bundle.containsKey("zipped_log") && bundle.containsKey("zipped_dumpsys")) {
            this.q = bundle.getByteArray("zipped_log");
            this.r = bundle.getByteArray("zipped_dumpsys");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f331c = (ScrollView) layoutInflater.inflate(t.fragment_feedback, viewGroup, false);
        return this.f331c;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        c(view);
        d(view);
        e(view);
        f(view);
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getActivity().getWindow().setSoftInputMode(1);
        if (this.t != null && !this.t.getStatus().equals(AsyncTask.Status.FINISHED)) {
            a(this.t);
        }
        this.s = a.a(null, getActivity(), getArguments().getBundle("community_iq"));
        if (this.q == null || this.r == null) {
            new m(this, null).execute(new Void[0]);
        }
        d();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (this.q != null && this.r != null) {
            bundle.putByteArray("zipped_log", this.q);
            bundle.putByteArray("zipped_dumpsys", this.r);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.t != null && !this.t.getStatus().equals(AsyncTask.Status.FINISHED)) {
            this.t.cancel(true);
        }
    }

    public void onDetach() {
        super.onDetach();
        h();
    }

    private void c(View view) {
        this.d = (EditText) view.findViewById(r.feedback_email);
        this.e = (TextView) view.findViewById(r.feedback_email_error_message);
        this.f = (TextView) view.findViewById(r.feedback_email_label);
        this.g = (EditText) view.findViewById(r.feedback_name);
        this.h = (EditText) view.findViewById(r.feedback_description);
        this.i = (TextView) view.findViewById(r.feedback_description_error_message);
        this.j = (TextView) view.findViewById(r.feedback_description_label);
        this.d.addTextChangedListener(new f(this));
        this.i.setText(getString(x.msg_feedback_description_missing, 20));
        this.h.addTextChangedListener(new g(this));
    }

    private void d(View view) {
        this.k = (SelectorRow) view.findViewById(r.feedback_type);
        this.k.b(getString(x.l_feedback_type));
        this.k.a(getString(x.l_feedback_type_selected));
        this.k.a(m.feedback_types);
        this.k.a(new int[]{1, 2, 3, 4});
        this.f330b = this.k.c();
        this.k.a(new h(this));
    }

    private void e(View view) {
        i iVar = new i(this);
        this.l = (CheckBoxRow) view.findViewById(r.feedback_checkbox_ciq);
        this.m = (CheckBoxRow) view.findViewById(r.feedback_checkbox_log);
        this.n = (CheckBoxRow) view.findViewById(r.feedback_checkbox_dumpsys);
        this.l.a((com.avast.android.generic.e.c) null);
        this.l.b(getString(x.l_feedback_send_community_iq));
        this.l.c(getString(x.l_feedback_send_community_iq_desc));
        this.l.a(true);
        this.l.a(iVar);
        this.m.a((com.avast.android.generic.e.c) null);
        this.m.b(getString(x.l_feedback_send_device_log));
        this.m.c(getString(x.l_feedback_send_device_log_desc));
        this.m.a(iVar);
        this.n.a(iVar);
    }

    private void f(View view) {
        this.o = (Button) view.findViewById(r.feedback_button_send);
        this.p = (Button) view.findViewById(r.feedback_button_cancel);
        this.o.setOnClickListener(new j(this));
        this.p.setOnClickListener(new k(this));
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        this.f330b = i2;
        if (this.f330b == 3) {
            a(this.l);
            a(this.m);
            a(this.n);
        } else if (this.f330b == 2) {
            a(this.l);
            a(this.n);
            this.m.setEnabled(true);
        } else {
            this.l.setEnabled(true);
            this.m.setEnabled(true);
            this.n.setEnabled(true);
        }
        if (this.f330b == 3) {
            this.i.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void d() {
        long j2;
        if (getActivity() != null) {
            ad adVar = new ad(getActivity());
            if (this.q == null || this.r == null) {
                j2 = 0;
            } else {
                if (this.m.c()) {
                    j2 = ((long) this.q.length) + 0;
                } else {
                    j2 = 0;
                }
                if (this.n.c()) {
                    j2 += (long) this.r.length;
                }
                String a2 = adVar.a((long) this.q.length);
                String a3 = adVar.a((long) this.r.length);
                this.m.c(getString(x.l_feedback_send_device_log_desc) + ", " + a2);
                this.n.c(getString(x.l_feedback_send_dumpsys_desc) + ", " + a3);
            }
            if (this.s != null) {
                if (this.l.c()) {
                    j2 += (long) this.s.getSerializedSize();
                }
                this.l.c(getString(x.l_feedback_send_community_iq_desc) + ", " + adVar.a((long) this.s.getSerializedSize()));
            }
            if (j2 > 0) {
                this.o.setText(getString(x.l_feedback_button_send) + "\n" + adVar.a(j2));
            } else {
                this.o.setText(getString(x.l_feedback_button_send));
            }
        }
    }

    private void a(CheckBoxRow checkBoxRow) {
        checkBoxRow.a(true);
        checkBoxRow.setEnabled(false);
    }

    /* access modifiers changed from: private */
    public boolean e() {
        boolean z;
        int i2;
        this.h.setText(this.h.getText().toString().trim());
        if (!g()) {
            this.i.setVisibility(0);
            i2 = ((View) this.j.getParent()).getTop() + this.j.getTop();
            z = false;
        } else {
            z = true;
            i2 = 0;
        }
        this.d.setText(this.d.getText().toString().trim());
        if (!f()) {
            this.e.setVisibility(0);
            i2 = ((View) this.f.getParent()).getTop() + this.f.getTop();
            z &= false;
        }
        if (!z) {
            this.f331c.scrollTo(0, i2);
        }
        return z;
    }

    /* access modifiers changed from: private */
    public boolean f() {
        return this.d.getText().length() == 0 || f329a.matcher(this.d.getText().toString()).matches();
    }

    /* access modifiers changed from: private */
    public boolean g() {
        return this.f330b == 3 || this.h.getText().length() > 20;
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (z) {
            Toast.makeText(getActivity(), x.msg_feedback_sent, 0).show();
            i();
            return;
        }
        Toast.makeText(getActivity(), x.msg_feedback_failed, 1).show();
    }

    /* access modifiers changed from: private */
    public void a(n nVar) {
        h();
        this.u = new ProgressDialog(getActivity());
        this.u.setCancelable(true);
        this.u.setOnCancelListener(new l(this, nVar));
        this.u.setMessage(getString(x.l_feedback_sending));
        this.u.show();
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.u != null) {
            this.u.dismiss();
            this.u = null;
        }
    }
}
