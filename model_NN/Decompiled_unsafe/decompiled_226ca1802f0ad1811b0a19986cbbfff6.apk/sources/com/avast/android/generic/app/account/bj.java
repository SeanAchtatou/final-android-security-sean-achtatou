package com.avast.android.generic.app.account;

import android.view.View;

/* compiled from: SignInPickerFragment */
class bj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SignInPickerFragment f421a;

    bj(SignInPickerFragment signInPickerFragment) {
        this.f421a = signInPickerFragment;
    }

    public void onClick(View view) {
        if (!this.f421a.e()) {
            this.f421a.f();
            return;
        }
        this.f421a.getArguments().putBoolean("wizard", this.f421a.f369b);
        this.f421a.getArguments().putBoolean("queryPhoneNumber", this.f421a.f370c);
        this.f421a.getArguments().putString("facebookToken", null);
        this.f421a.getArguments().putString("googlePlusToken", null);
        this.f421a.a(new ConnectAccountFragment());
    }
}
