package com.avast.android.generic.licensing;

import com.avast.android.generic.licensing.c.h;
import com.avast.android.generic.licensing.c.i;
import com.avast.android.generic.util.z;

/* compiled from: OfferTask */
class d implements h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f881a;

    d(c cVar) {
        this.f881a = cVar;
    }

    public void a(i iVar) {
        z.a("AvastGenericLic", "Billing setup finished.");
        Boolean unused = this.f881a.h = Boolean.valueOf(iVar.c() && this.f881a.g.b());
        this.f881a.f855c.release();
    }
}
