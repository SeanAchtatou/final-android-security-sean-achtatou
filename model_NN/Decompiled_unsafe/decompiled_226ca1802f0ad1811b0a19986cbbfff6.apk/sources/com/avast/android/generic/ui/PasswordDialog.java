package com.avast.android.generic.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.r;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.g.b.a;
import com.avast.android.generic.o;
import com.avast.android.generic.q;
import com.avast.android.generic.ui.widget.PasswordTextView;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.util.t;
import com.avast.android.generic.util.y;
import com.avast.android.generic.x;

public class PasswordDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    public static int f993a = 1;

    /* renamed from: b  reason: collision with root package name */
    public static int f994b = 2;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public ab f995c;
    /* access modifiers changed from: private */
    public PasswordTextView d;
    /* access modifiers changed from: private */
    public ImageView e;
    private int f;
    private boolean g;
    private Activity h;
    private y i;
    /* access modifiers changed from: private */
    public ad j;
    private TextView k;
    private TextView l;
    private BroadcastReceiver m;
    private r n;

    public static Handler.Callback a(Context context, int i2, ac acVar) {
        t tVar = new t(acVar);
        ((y) aa.a(context, y.class)).a(i2, tVar);
        return tVar;
    }

    public static void a(FragmentManager fragmentManager, int i2) {
        t.b(PasswordDialog.class.getSimpleName(), "Showing password dialog.");
        FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        if (((DialogFragment) fragmentManager.findFragmentByTag("passwordDialog")) == null) {
            PasswordDialog passwordDialog = new PasswordDialog();
            Bundle bundle = new Bundle();
            bundle.putInt("callbackHandlerId", i2);
            passwordDialog.setArguments(bundle);
            passwordDialog.show(beginTransaction, "passwordDialog");
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f995c = (ab) aa.a(getActivity(), ab.class);
        this.m = new u(this);
        this.n = r.a(getActivity());
        this.n.a(this.m, new IntentFilter("com.avast.android.generic.app.passwordrecovery.ACTION_RECOVERY_INITIATED"));
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.g = true;
    }

    public void onResume() {
        super.onResume();
        if (!this.g && this.d != null) {
            this.d.setText("");
        }
        this.g = false;
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        if (this.f > 0) {
            Message message = new Message();
            message.what = this.f;
            message.arg1 = f994b;
            this.i.a(message);
        }
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        Activity activity = getActivity();
        if (activity == null) {
            activity = this.h;
        }
        if (activity instanceof PasswordActivity) {
            activity.finish();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            this.n.a(this.m);
        } catch (Exception e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    @TargetApi(8)
    public Dialog onCreateDialog(Bundle bundle) {
        this.h = getActivity();
        this.f = getArguments().getInt("callbackHandlerId");
        this.i = (y) aa.a(getActivity(), y.class);
        this.j = new ad(this, this.f995c);
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        builder.setTitle(getString(x.pref_password_enter));
        View inflate = LayoutInflater.from(c2).inflate(com.avast.android.generic.t.dialog_password, (ViewGroup) null, false);
        this.d = (PasswordTextView) inflate.findViewById(com.avast.android.generic.r.password1);
        this.e = (ImageView) inflate.findViewById(com.avast.android.generic.r.passwordImage1);
        this.d.addTextChangedListener(d());
        this.e.setVisibility(8);
        this.d.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        this.k = (TextView) inflate.findViewById(com.avast.android.generic.r.password_recovery_title);
        this.l = (TextView) inflate.findViewById(com.avast.android.generic.r.password_recovery_subtitle);
        a();
        builder.setView(inflate);
        builder.setPositiveButton(x.bR, new v(this));
        builder.setNegativeButton(x.F, new w(this));
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new x(this, create));
        create.setOnKeyListener(new z(this));
        create.setCancelable(true);
        setCancelable(true);
        create.getWindow().setSoftInputMode(4);
        return create;
    }

    /* access modifiers changed from: private */
    public void a() {
        if (isAdded()) {
            if (a.j(getActivity())) {
                if (TextUtils.isEmpty(this.f995c.k())) {
                    this.k.setText(x.l_password_recovery_not_set);
                    this.k.setTextColor(getResources().getColor(o.text_context));
                    this.l.setText(x.msg_password_recovery_not_set_phone);
                    this.l.setVisibility(0);
                } else if (com.avast.android.generic.app.passwordrecovery.a.d(getActivity())) {
                    this.k.setText(x.l_password_recovery_already_active);
                    this.k.setTextColor(getResources().getColor(o.text_disabled));
                } else {
                    this.k.setOnClickListener(new aa(this));
                }
            } else if (!this.f995c.t()) {
                this.k.setText(x.l_password_recovery_not_set);
                this.k.setTextColor(getResources().getColor(o.text_context));
                this.l.setText(x.msg_password_recovery_not_set_account);
                this.l.setVisibility(0);
            } else {
                this.k.setVisibility(8);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        boolean z = false;
        this.e.setVisibility(0);
        int length = this.d.a().length();
        if (length > 0) {
            this.e.setVisibility(0);
        } else {
            this.e.setVisibility(4);
        }
        if (length < 4 || length > 6) {
            z = true;
        }
        if (z) {
            this.e.setImageResource(q.ic_scanner_result_problem);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.e.setVisibility(0);
        this.e.setImageResource(q.ic_scanner_result_ok);
        if (isAdded()) {
            try {
                dismiss();
            } catch (Exception e2) {
                t.b("Error dismissing password dialog", e2);
            }
        }
        if (this.f > 0) {
            Message message = new Message();
            message.what = this.f;
            message.arg1 = f993a;
            this.i.a(message);
        }
    }

    private TextWatcher d() {
        return new ab(this);
    }
}
