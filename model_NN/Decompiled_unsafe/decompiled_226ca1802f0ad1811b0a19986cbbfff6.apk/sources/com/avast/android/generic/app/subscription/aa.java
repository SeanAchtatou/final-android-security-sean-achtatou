package com.avast.android.generic.app.subscription;

import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;
import com.avast.android.generic.ui.b.a;

/* compiled from: SubscriptionFragment */
class aa implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f524a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ View f525b;

    /* renamed from: c  reason: collision with root package name */
    final /* synthetic */ TextView f526c;
    final /* synthetic */ CharSequence d;
    final /* synthetic */ ScrollView e;
    final /* synthetic */ CharSequence f;
    final /* synthetic */ SubscriptionFragment g;

    aa(SubscriptionFragment subscriptionFragment, a aVar, View view, TextView textView, CharSequence charSequence, ScrollView scrollView, CharSequence charSequence2) {
        this.g = subscriptionFragment;
        this.f524a = aVar;
        this.f525b = view;
        this.f526c = textView;
        this.d = charSequence;
        this.e = scrollView;
        this.f = charSequence2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, boolean):boolean
     arg types: [com.avast.android.generic.app.subscription.SubscriptionFragment, int]
     candidates:
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, com.avast.android.generic.app.subscription.ad):com.avast.android.generic.app.subscription.ad
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, com.avast.c.a.a.ai):com.avast.c.a.a.ai
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, java.lang.String):java.lang.String
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(long, boolean):void
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, com.avast.android.generic.licensing.c.k):void
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, com.avast.android.generic.licensing.c):void
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, java.util.List):boolean
      com.avast.android.generic.app.subscription.SubscriptionFragment.a(com.avast.android.generic.app.subscription.SubscriptionFragment, boolean):boolean */
    public void onClick(View view) {
        if (!this.g.M) {
            this.f524a.a(false);
            this.f525b.startAnimation(this.f524a);
            boolean unused = this.g.M = true;
            this.f526c.setText(this.d);
            this.e.postDelayed(new ab(this), this.f524a.getDuration());
            return;
        }
        this.f524a.a(true);
        this.f525b.startAnimation(this.f524a);
        boolean unused2 = this.g.M = false;
        this.f526c.setText(this.f);
    }
}
