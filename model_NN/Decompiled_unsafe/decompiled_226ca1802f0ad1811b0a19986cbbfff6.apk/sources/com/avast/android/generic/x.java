package com.avast.android.generic;

/* compiled from: R */
public final class x {

    /* renamed from: A */
    public static final int l_avast_account_connecting = 2131624068;

    /* renamed from: B */
    public static final int l_avast_account_disconnect_dialog_title = 2131624069;

    /* renamed from: C */
    public static final int l_avast_account_disconnecting = 2131624070;

    /* renamed from: D */
    public static final int l_avast_account_setup_enter_password_or_change_email = 2131624074;

    /* renamed from: E */
    public static final int l_avast_account_setup_why_is_phone_number_needed = 2131624084;
    public static final int F = 2131624087;

    /* renamed from: G */
    public static final int l_cannot_query_billing_error = 2131624088;

    /* renamed from: H */
    public static final int l_cannot_query_billing_error_unknown = 2131624089;

    /* renamed from: I */
    public static final int l_cannot_query_billing_identities_invalid = 2131624090;

    /* renamed from: J */
    public static final int l_cannot_query_manage_subscription_error = 2131624091;

    /* renamed from: K */
    public static final int l_cannot_query_manage_subscription_unknown = 2131624092;

    /* renamed from: L */
    public static final int l_cannot_query_offers_error = 2131624093;

    /* renamed from: M */
    public static final int l_cannot_query_offers_generic = 2131624094;

    /* renamed from: N */
    public static final int l_conn_check_ams_too_old = 2131624105;

    /* renamed from: O */
    public static final int l_conn_check_android_check = 2131624106;

    /* renamed from: P */
    public static final int l_conn_check_anti_theft_disabled = 2131624107;

    /* renamed from: Q */
    public static final int l_conn_check_anti_theft_enabled_check = 2131624108;

    /* renamed from: R */
    public static final int l_conn_check_at_too_old = 2131624109;

    /* renamed from: S */
    public static final int l_conn_check_background_data_disabled = 2131624110;

    /* renamed from: T */
    public static final int l_conn_check_backup_invalid = 2131624112;

    /* renamed from: U */
    public static final int l_conn_check_backup_not_up_to_date = 2131624113;

    /* renamed from: V */
    public static final int l_conn_check_backup_too_old = 2131624115;

    /* renamed from: W */
    public static final int l_conn_check_c2dm_incorrect = 2131624116;

    /* renamed from: X */
    public static final int l_conn_check_deprecated = 2131624117;

    /* renamed from: Y */
    public static final int l_conn_check_feedback = 2131624118;

    /* renamed from: Z */
    public static final int l_conn_check_no_backup = 2131624119;

    /* renamed from: a */
    public static final int Midnight = 2131623936;

    /* renamed from: aA */
    public static final int l_error_cut_down = 2131624180;

    /* renamed from: aB */
    public static final int l_error_device_or_resource_busy = 2131624181;

    /* renamed from: aC */
    public static final int l_error_http_status_400 = 2131624182;

    /* renamed from: aD */
    public static final int l_error_licensing_error_short = 2131624186;

    /* renamed from: aE */
    public static final int l_error_no_internet = 2131624187;

    /* renamed from: aF */
    public static final int l_error_reason_unknown = 2131624188;

    /* renamed from: aG */
    public static final int l_error_wp_customer_cannot_pay = 2131624189;

    /* renamed from: aH */
    public static final int l_error_wp_invalid_data = 2131624191;

    /* renamed from: aI */
    public static final int l_error_wp_missing_parameters = 2131624192;

    /* renamed from: aJ */
    public static final int l_error_wp_payment_limit_reached = 2131624194;

    /* renamed from: aK */
    public static final int l_error_wp_pin_invalid = 2131624195;

    /* renamed from: aL */
    public static final int l_error_wp_psp_authorization_failed = 2131624196;

    /* renamed from: aM */
    public static final int l_error_wp_psp_communication_error = 2131624197;

    /* renamed from: aN */
    public static final int l_error_wp_psp_general_error = 2131624198;

    /* renamed from: aO */
    public static final int l_error_wp_psp_sms_failed = 2131624199;

    /* renamed from: aP */
    public static final int l_error_wp_result_is_empty = 2131624200;

    /* renamed from: aQ */
    public static final int l_eula_hide = 2131624203;

    /* renamed from: aR */
    public static final int l_eula_show = 2131624206;

    /* renamed from: aS */
    public static final int l_faq_entry = 2131624207;

    /* renamed from: aT */
    public static final int l_feedback_button_send = 2131624209;

    /* renamed from: aU */
    public static final int l_feedback_send_community_iq = 2131624216;

    /* renamed from: aV */
    public static final int l_feedback_send_community_iq_desc = 2131624217;

    /* renamed from: aW */
    public static final int l_feedback_send_device_log = 2131624218;

    /* renamed from: aX */
    public static final int l_feedback_send_device_log_desc = 2131624219;

    /* renamed from: aY */
    public static final int l_feedback_send_dumpsys_desc = 2131624221;

    /* renamed from: aZ */
    public static final int l_feedback_sending = 2131624222;

    /* renamed from: aa */
    public static final int l_conn_check_no_data_enabled = 2131624120;

    /* renamed from: ab */
    public static final int l_conn_check_no_issues = 2131624121;

    /* renamed from: ac */
    public static final int l_conn_check_no_network_active = 2131624122;

    /* renamed from: ad */
    public static final int l_conn_check_not_activated = 2131624123;

    /* renamed from: ae */
    public static final int l_conn_check_online_device_not_found = 2131624124;

    /* renamed from: af */
    public static final int l_conn_check_online_generic_c2dm_error = 2131624125;

    /* renamed from: ag */
    public static final int l_conn_check_online_generic_c2dm_error_from_google = 2131624126;

    /* renamed from: ah */
    public static final int l_conn_check_online_invalid_data = 2131624127;

    /* renamed from: ai */
    public static final int l_conn_check_online_invalid_response = 2131624128;

    /* renamed from: aj */
    public static final int l_conn_check_online_no_c2dm_id_on_server = 2131624129;

    /* renamed from: ak */
    public static final int l_conn_check_online_no_web_message_received = 2131624130;

    /* renamed from: al */
    public static final int l_conn_check_online_not_registered_for_c2dm = 2131624131;

    /* renamed from: am */
    public static final int l_conn_check_receive_message_check = 2131624132;

    /* renamed from: an */
    public static final int l_conn_check_secureline_too_old = 2131624133;

    /* renamed from: ao */
    public static final int l_conn_check_settings_check = 2131624134;

    /* renamed from: ap */
    public static final int l_conn_check_settings_incorrect = 2131624135;

    /* renamed from: aq */
    public static final int l_conn_check_too_old = 2131624136;

    /* renamed from: ar */
    public static final int l_conn_check_version_check = 2131624137;

    /* renamed from: as */
    public static final int l_conn_check_web_connection_check = 2131624138;

    /* renamed from: at */
    public static final int l_connectioncheck = 2131624141;

    /* renamed from: au */
    public static final int l_consume = 2131624144;

    /* renamed from: av */
    public static final int l_dashboard_go_premium = 2131624146;

    /* renamed from: aw */
    public static final int l_disconnect = 2131624155;

    /* renamed from: ax */
    public static final int l_display_help_image = 2131624156;
    public static final int ay = 2131624178;

    /* renamed from: az */
    public static final int l_error_connection_refused = 2131624179;

    /* renamed from: b */
    public static final int Noon = 2131623937;

    /* renamed from: bA */
    public static final int l_monday = 2131624299;
    public static final int bB = 2131624301;

    /* renamed from: bC */
    public static final int l_no_issues_text = 2131624303;

    /* renamed from: bD */
    public static final int l_offers_code_already_consumed = 2131624305;

    /* renamed from: bE */
    public static final int l_offers_code_locked = 2131624306;

    /* renamed from: bF */
    public static final int l_offers_code_not_valid_anymore = 2131624307;

    /* renamed from: bG */
    public static final int l_offers_code_not_yet_valid = 2131624308;

    /* renamed from: bH */
    public static final int l_offers_code_unknown = 2131624309;

    /* renamed from: bI */
    public static final int l_offers_generic_error = 2131624310;

    /* renamed from: bJ */
    public static final int l_offers_google_play_invalid = 2131624311;

    /* renamed from: bK */
    public static final int l_offers_invalid_country = 2131624312;

    /* renamed from: bL */
    public static final int l_offers_invalid_operator = 2131624313;

    /* renamed from: bM */
    public static final int l_offers_invalid_operator_noop = 2131624314;

    /* renamed from: bN */
    public static final int l_offers_license_not_found = 2131624315;

    /* renamed from: bO */
    public static final int l_offers_no_internet_connectivity = 2131624317;

    /* renamed from: bP */
    public static final int l_offers_no_items = 2131624318;

    /* renamed from: bQ */
    public static final int l_offers_subscriptions_not_supported = 2131624319;
    public static final int bR = 2131624320;

    /* renamed from: bS */
    public static final int l_password_recovery_already_active = 2131624324;

    /* renamed from: bT */
    public static final int l_password_recovery_clear = 2131624325;

    /* renamed from: bU */
    public static final int l_password_recovery_description_button = 2131624326;

    /* renamed from: bV */
    public static final int l_password_recovery_dialog_title = 2131624327;

    /* renamed from: bW */
    public static final int l_password_recovery_not_set = 2131624328;

    /* renamed from: bX */
    public static final int l_password_recovery_sending = 2131624329;

    /* renamed from: bY */
    public static final int l_purchase = 2131624335;

    /* renamed from: bZ */
    public static final int l_refresh_licenses_detail = 2131624343;

    /* renamed from: ba */
    public static final int l_feedback_type = 2131624224;

    /* renamed from: bb */
    public static final int l_feedback_type_selected = 2131624225;

    /* renamed from: bc */
    public static final int l_filebrowser_multi_select = 2131624229;

    /* renamed from: bd */
    public static final int l_filter_add_custom = 2131624230;

    /* renamed from: be */
    public static final int l_filter_add_from_contactlist = 2131624231;

    /* renamed from: bf */
    public static final int l_filter_custom_number_error_emergency = 2131624233;

    /* renamed from: bg */
    public static final int l_filter_custom_number_error_format = 2131624234;

    /* renamed from: bh */
    public static final int l_filter_custom_number_error_too_short = 2131624235;

    /* renamed from: bi */
    public static final int l_fix_item = 2131624239;

    /* renamed from: bj */
    public static final int l_fri = 2131624240;

    /* renamed from: bk */
    public static final int l_fri_short = 2131624241;

    /* renamed from: bl */
    public static final int l_friday = 2131624242;

    /* renamed from: bm */
    public static final int l_google_play_error = 2131624248;

    /* renamed from: bn */
    public static final int l_google_play_error_intent = 2131624249;

    /* renamed from: bo */
    public static final int l_home_error_non_unique_guid_desc = 2131624251;

    /* renamed from: bp */
    public static final int l_home_error_too_many_devices_desc = 2131624256;

    /* renamed from: bq */
    public static final int l_home_error_too_many_google_accounts_desc = 2131624258;

    /* renamed from: br */
    public static final int l_host_cannot_be_resolved = 2131624260;

    /* renamed from: bs */
    public static final int l_ignore = 2131624263;

    /* renamed from: bt */
    public static final int l_ignore_items_for_now = 2131624266;
    public static final int bu = 2131624276;

    /* renamed from: bv */
    public static final int l_license = 2131624279;

    /* renamed from: bw */
    public static final int l_lifetime_license = 2131624293;

    /* renamed from: bx */
    public static final int l_lifetime_license_long = 2131624294;

    /* renamed from: by */
    public static final int l_mon = 2131624297;

    /* renamed from: bz */
    public static final int l_mon_short = 2131624298;

    /* renamed from: c */
    public static final int abbrev_month = 2131623938;

    /* renamed from: cA */
    public static final int l_subscription_processing_payment = 2131624412;

    /* renamed from: cB */
    public static final int l_subscription_renewed_on = 2131624414;

    /* renamed from: cC */
    public static final int l_subscription_renewed_on_long = 2131624415;

    /* renamed from: cD */
    public static final int l_subscription_subtitle_tap_to_refresh = 2131624417;

    /* renamed from: cE */
    public static final int l_subscription_time_limited_long = 2131624419;

    /* renamed from: cF */
    public static final int l_subscription_unknown = 2131624421;

    /* renamed from: cG */
    public static final int l_subscription_valid = 2131624422;

    /* renamed from: cH */
    public static final int l_subscription_voucher = 2131624425;

    /* renamed from: cI */
    public static final int l_subscription_want_consume_free = 2131624428;

    /* renamed from: cJ */
    public static final int l_subscription_welcome_get_most_signedin_account = 2131624436;

    /* renamed from: cK */
    public static final int l_subscription_welcome_go_back = 2131624438;

    /* renamed from: cL */
    public static final int l_subscription_welcome_title = 2131624443;

    /* renamed from: cM */
    public static final int l_sun = 2131624444;

    /* renamed from: cN */
    public static final int l_sun_short = 2131624445;

    /* renamed from: cO */
    public static final int l_sunday = 2131624446;

    /* renamed from: cP */
    public static final int l_thu = 2131624448;

    /* renamed from: cQ */
    public static final int l_thu_short = 2131624449;

    /* renamed from: cR */
    public static final int l_thursday = 2131624450;

    /* renamed from: cS */
    public static final int l_tue = 2131624451;

    /* renamed from: cT */
    public static final int l_tue_short = 2131624452;

    /* renamed from: cU */
    public static final int l_tuesday = 2131624453;

    /* renamed from: cV */
    public static final int l_unsupported_purchase_method = 2131624455;

    /* renamed from: cW */
    public static final int l_wed = 2131624458;

    /* renamed from: cX */
    public static final int l_wed_short = 2131624459;

    /* renamed from: cY */
    public static final int l_wednesday = 2131624460;

    /* renamed from: cZ */
    public static final int l_wizard_account_skip = 2131624462;
    public static final int ca = 2131624345;
    public static final int cb = 2131624346;

    /* renamed from: cc */
    public static final int l_revoke_ignore = 2131624347;

    /* renamed from: cd */
    public static final int l_revoke_revoke_ignore = 2131624348;

    /* renamed from: ce */
    public static final int l_sat = 2131624363;

    /* renamed from: cf */
    public static final int l_sat_short = 2131624364;

    /* renamed from: cg */
    public static final int l_saturday = 2131624365;

    /* renamed from: ch */
    public static final int l_subscription_button_na = 2131624382;

    /* renamed from: ci */
    public static final int l_subscription_button_premium = 2131624383;

    /* renamed from: cj */
    public static final int l_subscription_button_unknown = 2131624384;

    /* renamed from: ck */
    public static final int l_subscription_buy = 2131624385;

    /* renamed from: cl */
    public static final int l_subscription_choose_payment_provider = 2131624386;

    /* renamed from: cm */
    public static final int l_subscription_disclaimer_hide = 2131624387;

    /* renamed from: cn */
    public static final int l_subscription_disclaimer_show = 2131624388;

    /* renamed from: co */
    public static final int l_subscription_discount = 2131624389;

    /* renamed from: cp */
    public static final int l_subscription_error_billing_connection_title = 2131624391;

    /* renamed from: cq */
    public static final int l_subscription_error_button_ok = 2131624392;

    /* renamed from: cr */
    public static final int l_subscription_error_title = 2131624395;

    /* renamed from: cs */
    public static final int l_subscription_loading = 2131624397;

    /* renamed from: ct */
    public static final int l_subscription_none = 2131624399;

    /* renamed from: cu */
    public static final int l_subscription_not_available = 2131624400;

    /* renamed from: cv */
    public static final int l_subscription_premium_plans_title = 2131624407;

    /* renamed from: cw */
    public static final int l_subscription_price_one_time = 2131624408;

    /* renamed from: cx */
    public static final int l_subscription_price_per_month = 2131624409;

    /* renamed from: cy */
    public static final int l_subscription_price_per_year = 2131624410;

    /* renamed from: cz */
    public static final int l_subscription_processing_free = 2131624411;

    /* renamed from: d */
    public static final int abbrev_month_day = 2131623939;

    /* renamed from: dA */
    public static final int msg_can_not_open_webbrowser = 2131624503;

    /* renamed from: dB */
    public static final int msg_debug_mode_disabled = 2131624504;

    /* renamed from: dC */
    public static final int msg_debug_mode_enabled = 2131624505;

    /* renamed from: dD */
    public static final int msg_debug_mode_two_taps = 2131624506;

    /* renamed from: dE */
    public static final int msg_feedback_description_missing = 2131624509;

    /* renamed from: dF */
    public static final int msg_feedback_failed = 2131624511;

    /* renamed from: dG */
    public static final int msg_feedback_sent = 2131624512;

    /* renamed from: dH */
    public static final int msg_home_error_license_already_expired = 2131624516;

    /* renamed from: dI */
    public static final int msg_home_error_license_invalid = 2131624517;

    /* renamed from: dJ */
    public static final int msg_home_error_restoring_invalid_credentials = 2131624518;

    /* renamed from: dK */
    public static final int msg_home_error_restoring_too_often_message = 2131624519;

    /* renamed from: dL */
    public static final int msg_home_error_restoring_transactions_message = 2131624520;

    /* renamed from: dM */
    public static final int msg_home_error_restoring_transactions_message_generic = 2131624521;

    /* renamed from: dN */
    public static final int msg_home_error_restoring_transactions_message_offer = 2131624522;

    /* renamed from: dO */
    public static final int msg_home_error_restoring_transactions_no_google_account = 2131624523;

    /* renamed from: dP */
    public static final int msg_invalid_tool_combination = 2131624525;

    /* renamed from: dQ */
    public static final int msg_mobile_security_too_old = 2131624527;

    /* renamed from: dR */
    public static final int msg_no_internet_connectivity = 2131624528;

    /* renamed from: dS */
    public static final int msg_password_recovery_confirmation = 2131624530;

    /* renamed from: dT */
    public static final int msg_password_recovery_description = 2131624531;

    /* renamed from: dU */
    public static final int msg_password_recovery_description_after_pin = 2131624532;

    /* renamed from: dV */
    public static final int msg_password_recovery_message_not_sent = 2131624533;

    /* renamed from: dW */
    public static final int msg_password_recovery_message_sent = 2131624534;

    /* renamed from: dX */
    public static final int msg_password_recovery_not_set_account = 2131624535;

    /* renamed from: dY */
    public static final int msg_password_recovery_not_set_phone = 2131624536;

    /* renamed from: dZ */
    public static final int msg_recovery_sms_text = 2131624537;
    public static final int da = 2131624464;

    /* renamed from: db */
    public static final int menu_dialog_select_action = 2131624468;

    /* renamed from: dc */
    public static final int midnight = 2131624470;

    /* renamed from: dd */
    public static final int month = 2131624471;

    /* renamed from: de */
    public static final int month_day = 2131624472;

    /* renamed from: df */
    public static final int month_day_year = 2131624473;

    /* renamed from: dg */
    public static final int month_year = 2131624474;

    /* renamed from: dh */
    public static final int msg_anti_theft_too_old = 2131624475;

    /* renamed from: di */
    public static final int msg_avast_account_c2dm_error = 2131624476;

    /* renamed from: dj */
    public static final int msg_avast_account_c2dm_needs_api_level_8 = 2131624478;

    /* renamed from: dk */
    public static final int msg_avast_account_disconnected = 2131624482;

    /* renamed from: dl */
    public static final int msg_avast_account_error_connection = 2131624483;

    /* renamed from: dm */
    public static final int msg_avast_account_error_email_already_used = 2131624484;

    /* renamed from: dn */
    public static final int msg_avast_account_error_internal_error = 2131624485;

    /* renamed from: do */
    public static final int msg_avast_account_error_invalid_credentials = 2131624486;

    /* renamed from: dp */
    public static final int msg_avast_account_error_invalid_email = 2131624487;

    /* renamed from: dq */
    public static final int msg_avast_account_error_password_invalid = 2131624489;

    /* renamed from: dr */
    public static final int msg_avast_account_phone_number_not_international = 2131624492;

    /* renamed from: ds */
    public static final int msg_avast_account_phone_number_not_valid = 2131624493;

    /* renamed from: dt */
    public static final int msg_avast_account_phone_number_too_short = 2131624494;

    /* renamed from: du */
    public static final int msg_avast_backend_error_client_too_old = 2131624497;

    /* renamed from: dv */
    public static final int msg_avast_backend_error_device_already_existing = 2131624498;

    /* renamed from: dw */
    public static final int msg_avast_backend_error_generic = 2131624499;

    /* renamed from: dx */
    public static final int msg_avast_backend_error_internal_error = 2131624500;

    /* renamed from: dy */
    public static final int msg_avast_backend_error_number_invalid = 2131624501;

    /* renamed from: dz */
    public static final int msg_backup_too_old = 2131624502;

    /* renamed from: e */
    public static final int abbrev_month_day_year = 2131623940;

    /* renamed from: eA */
    public static final int product_at = 2131624598;

    /* renamed from: eB */
    public static final int product_backup = 2131624599;

    /* renamed from: eC */
    public static final int qtn_call = 2131624600;

    /* renamed from: eD */
    public static final int qtn_duration = 2131624601;

    /* renamed from: eE */
    public static final int qtn_duration_unit = 2131624602;

    /* renamed from: eF */
    public static final int short_format_month = 2131624627;

    /* renamed from: eG */
    public static final int submission_finished = 2131624628;

    /* renamed from: eH */
    public static final int submission_started = 2131624629;

    /* renamed from: eI */
    public static final int time1_time2 = 2131624631;

    /* renamed from: eJ */
    public static final int time_date = 2131624632;

    /* renamed from: eK */
    public static final int time_wday = 2131624634;

    /* renamed from: eL */
    public static final int time_wday_date = 2131624635;

    /* renamed from: eM */
    public static final int to_small = 2131624636;

    /* renamed from: eN */
    public static final int unit_b = 2131624639;

    /* renamed from: eO */
    public static final int unit_gb = 2131624640;

    /* renamed from: eP */
    public static final int unit_kb = 2131624641;

    /* renamed from: eQ */
    public static final int unit_mb = 2131624642;

    /* renamed from: eR */
    public static final int unit_tb = 2131624643;

    /* renamed from: eS */
    public static final int wday1_date1_time1_wday2_date2_time2 = 2131624644;

    /* renamed from: eT */
    public static final int wday1_date1_wday2_date2 = 2131624645;

    /* renamed from: eU */
    public static final int wday_date = 2131624646;

    /* renamed from: ea */
    public static final int msg_secureline_too_old = 2131624538;

    /* renamed from: eb */
    public static final int msg_subscription_error_message = 2131624541;

    /* renamed from: ec */
    public static final int msg_subscription_error_purchase_cancelled = 2131624543;

    /* renamed from: ed */
    public static final int msg_subscription_error_purchase_cancelled_message = 2131624544;

    /* renamed from: ee */
    public static final int msg_subscription_error_purchase_cancelled_web_purchase = 2131624545;

    /* renamed from: ef */
    public static final int msg_subscription_error_purchase_failed_message = 2131624547;

    /* renamed from: eg */
    public static final int noon = 2131624553;

    /* renamed from: eh */
    public static final int numeric_date = 2131624555;

    /* renamed from: ei */
    public static final int oem_partner_name = 2131624566;

    /* renamed from: ej */
    public static final int pref_account_connected = 2131624569;

    /* renamed from: ek */
    public static final int pref_account_not_connected = 2131624570;

    /* renamed from: el */
    public static final int pref_language_current = 2131624574;

    /* renamed from: em */
    public static final int pref_language_default = 2131624575;

    /* renamed from: en */
    public static final int pref_password_change_not_match = 2131624578;

    /* renamed from: eo */
    public static final int pref_password_change_title = 2131624579;

    /* renamed from: ep */
    public static final int pref_password_default = 2131624580;

    /* renamed from: eq */
    public static final int pref_password_enter = 2131624582;

    /* renamed from: er */
    public static final int pref_password_hide_desc = 2131624585;

    /* renamed from: es */
    public static final int pref_password_non_digits = 2131624587;

    /* renamed from: et */
    public static final int pref_password_recovery_number = 2131624589;

    /* renamed from: eu */
    public static final int pref_password_show_desc = 2131624592;

    /* renamed from: ev */
    public static final int pref_password_too_long = 2131624593;

    /* renamed from: ew */
    public static final int pref_password_too_short = 2131624594;

    /* renamed from: ex */
    public static final int pref_voucher_title = 2131624595;

    /* renamed from: ey */
    public static final int pref_voucher_too_short = 2131624596;

    /* renamed from: ez */
    public static final int product_ams = 2131624597;

    /* renamed from: f */
    public static final int abbrev_month_year = 2131623941;

    /* renamed from: g */
    public static final int about_app_version = 2131623944;

    /* renamed from: h */
    public static final int about_eula_hide = 2131623947;

    /* renamed from: i */
    public static final int about_eula_show = 2131623948;

    /* renamed from: j */
    public static final int about_feedback = 2131623949;

    /* renamed from: k */
    public static final int about_title = 2131623950;

    /* renamed from: l */
    public static final int about_vps_definitions = 2131623951;

    /* renamed from: m */
    public static final int about_vps_version = 2131623952;

    /* renamed from: n */
    public static final int about_vps_version_unknown = 2131623953;

    /* renamed from: o */
    public static final int applicationId = 2131623970;

    /* renamed from: p */
    public static final int avast_error = 2131623977;

    /* renamed from: q */
    public static final int avast_ok = 2131623978;

    /* renamed from: r */
    public static final int date1_date2 = 2131624036;

    /* renamed from: s */
    public static final int date1_time1_date2_time2 = 2131624037;

    /* renamed from: t */
    public static final int from_small = 2131624045;

    /* renamed from: u */
    public static final int hour_ampm = 2131624048;

    /* renamed from: v */
    public static final int hour_cap_ampm = 2131624049;

    /* renamed from: w */
    public static final int hour_minute_24 = 2131624050;

    /* renamed from: x */
    public static final int hour_minute_ampm = 2131624051;

    /* renamed from: y */
    public static final int hour_minute_cap_ampm = 2131624052;

    /* renamed from: z */
    public static final int l_avast_account = 2131624063;
}
