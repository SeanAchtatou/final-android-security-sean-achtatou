package com.avast.android.generic.ui.widget;

import android.content.DialogInterface;
import android.os.Message;
import com.avast.android.generic.aa;
import com.avast.android.generic.ui.widget.SelectorRow;
import com.avast.android.generic.util.y;

/* compiled from: SelectorRow */
class w implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ SelectorRow.SelectorDialog f1157a;

    w(SelectorRow.SelectorDialog selectorDialog) {
        this.f1157a = selectorDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Message obtain = Message.obtain();
        obtain.what = this.f1157a.f1111b;
        obtain.arg1 = i;
        ((y) aa.a(this.f1157a.getActivity(), y.class)).a(obtain);
        dialogInterface.dismiss();
    }
}
