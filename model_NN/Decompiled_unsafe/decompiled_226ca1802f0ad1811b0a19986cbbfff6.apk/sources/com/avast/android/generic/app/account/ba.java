package com.avast.android.generic.app.account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.ui.PasswordDialog;

/* compiled from: DisconnectFragment */
class ba extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ DisconnectFragment f406a;

    ba(DisconnectFragment disconnectFragment) {
        this.f406a = disconnectFragment;
    }

    public void onReceive(Context context, Intent intent) {
        if (this.f406a.e.e()) {
            PasswordDialog.a(this.f406a.getFragmentManager(), this.f406a.d());
        } else {
            this.f406a.e();
        }
    }
}
