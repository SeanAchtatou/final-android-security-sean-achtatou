package com.avast.android.generic.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import com.avast.android.generic.r;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.x;
import java.util.regex.Pattern;

@TargetApi(8)
public class CustomNumberDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f989a = Pattern.compile("[0-9+()\\-#]*");

    /* renamed from: b  reason: collision with root package name */
    private static final Pattern f990b = Pattern.compile("[0-9+()\\-#.*]*");
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public EditText f991c;
    private TextView d;
    /* access modifiers changed from: private */
    public CheckBox e;
    private View f;
    private boolean g;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getArguments() != null) {
            this.g = getArguments().getBoolean("disable_wildcards", false);
        } else {
            this.g = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public Dialog onCreateDialog(Bundle bundle) {
        Context c2 = ak.c(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(c2);
        builder.setTitle(getString(x.l_filter_add_custom));
        builder.setPositiveButton(x.bR, new m(this));
        builder.setNegativeButton(x.F, new n(this));
        View inflate = LayoutInflater.from(c2).inflate(t.dialog_custom_number, (ViewGroup) null, false);
        a(inflate);
        builder.setView(inflate);
        AlertDialog create = builder.create();
        create.setInverseBackgroundForced(true);
        create.setOnShowListener(new o(this, create));
        return create;
    }

    private void a(View view) {
        this.f991c = (EditText) view.findViewById(r.dialog_custom_number_text);
        this.d = (TextView) view.findViewById(r.dialog_custom_number_error_message);
        this.e = (CheckBox) view.findViewById(r.dialog_custom_number_advanced_checkbox);
        this.f = view.findViewById(r.dialog_custom_number_advanced);
        if (this.g) {
            this.e.setVisibility(8);
            a(false);
        } else {
            a(this.e.isChecked());
            this.e.setOnCheckedChangeListener(new q(this));
        }
        this.f991c.addTextChangedListener(new r(this));
    }

    /* access modifiers changed from: private */
    public void a(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            this.d.setVisibility(8);
        } else if (!b(charSequence.toString(), this.e.isChecked())) {
            this.d.setText(x.l_filter_custom_number_error_format);
            this.d.setVisibility(0);
        } else if (!a(charSequence.toString())) {
            this.d.setText(x.l_filter_custom_number_error_too_short);
            this.d.setVisibility(0);
        } else if (PhoneNumberUtils.isEmergencyNumber(charSequence.toString())) {
            this.d.setText(x.l_filter_custom_number_error_emergency);
            this.d.setVisibility(0);
        } else {
            this.d.setVisibility(8);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.f.setVisibility(z ? 0 : 8);
    }

    public static boolean a(String str, boolean z) {
        if (!TextUtils.isEmpty(str) && b(str, z) && a(str)) {
            return true;
        }
        return false;
    }

    private static boolean b(String str, boolean z) {
        if ((!z || f990b.matcher(str).matches()) && (z || f989a.matcher(str).matches())) {
            return true;
        }
        return false;
    }

    private static boolean a(String str) {
        if (str.contains("*")) {
            if (str.replaceAll("[^0-9]", "").length() >= 4) {
                return true;
            }
            return false;
        } else if (str.length() < 6) {
            return false;
        } else {
            return true;
        }
    }
}
