package com.avast.android.generic.d;

import android.support.v4.app.FragmentTransaction;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.security.DigestOutputStream;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PrivateKey;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Enumeration;
import java.util.TreeMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/* compiled from: JarSigner */
public class d {
    private static X509Certificate a(InputStream inputStream) {
        try {
            return (X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(inputStream);
        } finally {
            inputStream.close();
        }
    }

    private static PrivateKey b(InputStream inputStream) {
        PKCS8EncodedKeySpec pKCS8EncodedKeySpec;
        PrivateKey generatePrivate;
        int i = 0;
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            byte[] bArr = new byte[10000];
            while (true) {
                int read = dataInputStream.read(bArr, i, 10000 - i);
                if (read == -1) {
                    break;
                }
                i += read;
            }
            byte[] bArr2 = new byte[i];
            System.arraycopy(bArr, 0, bArr2, 0, i);
            pKCS8EncodedKeySpec = new PKCS8EncodedKeySpec(bArr2);
            generatePrivate = KeyFactory.getInstance("RSA").generatePrivate(pKCS8EncodedKeySpec);
        } catch (InvalidKeySpecException e) {
            generatePrivate = KeyFactory.getInstance("DSA").generatePrivate(pKCS8EncodedKeySpec);
        } finally {
            dataInputStream.close();
        }
        return generatePrivate;
    }

    private static byte[] c(InputStream inputStream) {
        int i = 0;
        DataInputStream dataInputStream = new DataInputStream(inputStream);
        try {
            byte[] bArr = new byte[10000];
            while (true) {
                int read = dataInputStream.read(bArr, i, 10000 - i);
                if (read != -1) {
                    i += read;
                } else {
                    byte[] bArr2 = new byte[i];
                    System.arraycopy(bArr, 0, bArr2, 0, i);
                    return bArr2;
                }
            }
        } finally {
            dataInputStream.close();
        }
    }

    private static String a(JarFile jarFile) {
        String str = ("" + "Manifest-Version: 1.0\r\n") + "Created-By: 1.0 (Android SignApk)\r\n\r\n";
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        byte[] bArr = new byte[FragmentTransaction.TRANSIT_ENTER_MASK];
        TreeMap treeMap = new TreeMap();
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry nextElement = entries.nextElement();
            treeMap.put(nextElement.getName(), nextElement);
        }
        for (JarEntry jarEntry : treeMap.values()) {
            String name = jarEntry.getName();
            if (!jarEntry.isDirectory() && !name.equals("META-INF/MANIFEST.MF") && !name.equals("META-INF/CERT.SF") && !name.equals("META-INF/CERT.RSA")) {
                InputStream inputStream = jarFile.getInputStream(jarEntry);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    instance.update(bArr, 0, read);
                }
                str = (str + "Name: " + name + "\r\n") + "SHA1-Digest: " + String.valueOf(b.a(instance.digest())) + "\r\n\r\n";
            }
            str = str;
        }
        return str;
    }

    private static String a(JarFile jarFile, String str) {
        MessageDigest instance = MessageDigest.getInstance("SHA1");
        byte[] bArr = new byte[FragmentTransaction.TRANSIT_ENTER_MASK];
        MessageDigest instance2 = MessageDigest.getInstance("SHA1");
        PrintStream printStream = new PrintStream(new DigestOutputStream(new ByteArrayOutputStream(), instance2), true, "UTF-8");
        printStream.write(str.getBytes());
        printStream.flush();
        String str2 = ("Signature-Version: 1.0\r\n" + "Created-By: 1.0 (Android SignApk)\r\n") + "SHA1-Digest-Manifest: " + String.valueOf(b.a(instance2.digest())) + "\r\n\r\n";
        TreeMap treeMap = new TreeMap();
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry nextElement = entries.nextElement();
            treeMap.put(nextElement.getName(), nextElement);
        }
        for (JarEntry jarEntry : treeMap.values()) {
            String name = jarEntry.getName();
            if (!jarEntry.isDirectory() && !name.equals("META-INF/MANIFEST.MF") && !name.equals("META-INF/CERT.SF") && !name.equals("META-INF/CERT.RSA")) {
                printStream.print("Name: " + name + "\r\n");
                InputStream inputStream = jarFile.getInputStream(jarEntry);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    instance.update(bArr, 0, read);
                }
                printStream.print("SHA1-Digest: " + String.valueOf(b.a(instance.digest())) + "\r\n\r\n");
                str2 = (str2 + "Name: " + name + "\r\n") + "SHA1-Digest: " + String.valueOf(b.a(instance2.digest())) + "\r\n\r\n";
            }
            str2 = str2;
        }
        return str2;
    }

    private static byte[] a(String str, e eVar, PrivateKey privateKey, X509Certificate x509Certificate) {
        return eVar.a(str);
    }

    private static void a(JarFile jarFile, JarOutputStream jarOutputStream) {
        byte[] bArr = new byte[FragmentTransaction.TRANSIT_ENTER_MASK];
        TreeMap treeMap = new TreeMap();
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry nextElement = entries.nextElement();
            treeMap.put(nextElement.getName(), nextElement);
        }
        for (JarEntry jarEntry : treeMap.values()) {
            String name = jarEntry.getName();
            if (!jarEntry.isDirectory() && !name.equals("META-INF/MANIFEST.MF") && !name.equals("META-INF/CERT.SF") && !name.equals("META-INF/CERT.RSA")) {
                JarEntry jarEntry2 = jarFile.getJarEntry(name);
                if (jarEntry2.getMethod() == 0) {
                    jarOutputStream.putNextEntry(new JarEntry(jarEntry2));
                } else {
                    JarEntry jarEntry3 = new JarEntry(name);
                    jarEntry3.setTime(jarEntry2.getTime());
                    jarOutputStream.putNextEntry(jarEntry3);
                }
                InputStream inputStream = jarFile.getInputStream(jarEntry2);
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    jarOutputStream.write(bArr, 0, read);
                }
                jarOutputStream.flush();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.jar.JarFile.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.util.jar.JarFile.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.util.jar.JarFile.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x01fd, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x01fe, code lost:
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x0205, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x0206, code lost:
        r1.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0209, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:140:0x0222, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0223, code lost:
        r4 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:146:0x022f, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0230, code lost:
        r2 = null;
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x0233, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:149:0x0234, code lost:
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x0236, code lost:
        r1 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:?, code lost:
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0194, code lost:
        r5.close();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x0222 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:51:0x014d] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x018f A[SYNTHETIC, Splitter:B:76:0x018f] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0194 A[Catch:{ IOException -> 0x0205 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r17, int r18, int r19, java.lang.String r20, java.lang.String r21) {
        /*
            r4 = 0
            r5 = 0
            r3 = 0
            java.io.File r7 = new java.io.File     // Catch:{ Exception -> 0x018a }
            r0 = r20
            r7.<init>(r0)     // Catch:{ Exception -> 0x018a }
            if (r21 != 0) goto L_0x0181
            java.lang.String r1 = "apk"
            r2 = 0
            java.io.File r1 = java.io.File.createTempFile(r1, r2)     // Catch:{ Exception -> 0x018a }
        L_0x0013:
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ all -> 0x0198 }
            android.content.res.Resources r6 = r17.getResources()     // Catch:{ all -> 0x0198 }
            r0 = r18
            java.io.InputStream r6 = r6.openRawResource(r0)     // Catch:{ all -> 0x0198 }
            r2.<init>(r6)     // Catch:{ all -> 0x0198 }
            java.security.cert.X509Certificate r8 = a(r2)     // Catch:{ all -> 0x0245 }
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x018a }
            r6 = 0
            r3[r6] = r2     // Catch:{ Exception -> 0x018a }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x018a }
            r3 = 0
            java.util.Date r2 = r8.getNotBefore()     // Catch:{ Exception -> 0x018a }
            long r9 = r2.getTime()     // Catch:{ Exception -> 0x018a }
            r11 = 3600000(0x36ee80, double:1.7786363E-317)
            long r9 = r9 + r11
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ all -> 0x01a4 }
            android.content.res.Resources r6 = r17.getResources()     // Catch:{ all -> 0x01a4 }
            r0 = r19
            java.io.InputStream r6 = r6.openRawResource(r0)     // Catch:{ all -> 0x01a4 }
            r2.<init>(r6)     // Catch:{ all -> 0x01a4 }
            java.security.PrivateKey r11 = b(r2)     // Catch:{ all -> 0x0242 }
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x018a }
            r6 = 0
            r3[r6] = r2     // Catch:{ Exception -> 0x018a }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x018a }
            r3 = 0
            java.util.jar.JarFile r6 = new java.util.jar.JarFile     // Catch:{ all -> 0x0238 }
            r2 = 0
            r6.<init>(r7, r2)     // Catch:{ all -> 0x0238 }
            java.util.jar.JarOutputStream r4 = new java.util.jar.JarOutputStream     // Catch:{ all -> 0x023c }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x023c }
            r2.<init>(r1)     // Catch:{ all -> 0x023c }
            r4.<init>(r2)     // Catch:{ all -> 0x023c }
            r2 = 9
            r4.setLevel(r2)     // Catch:{ all -> 0x01d7 }
            java.lang.String r12 = a(r6)     // Catch:{ all -> 0x01d7 }
            java.util.jar.JarEntry r2 = new java.util.jar.JarEntry     // Catch:{ all -> 0x01d7 }
            java.lang.String r5 = "META-INF/MANIFEST.MF"
            r2.<init>(r5)     // Catch:{ all -> 0x01d7 }
            r2.setTime(r9)     // Catch:{ all -> 0x01d7 }
            r4.putNextEntry(r2)     // Catch:{ all -> 0x01d7 }
            byte[] r2 = r12.getBytes()     // Catch:{ all -> 0x01d7 }
            r4.write(r2)     // Catch:{ all -> 0x01d7 }
            r5 = 0
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x01d7 }
            r13 = 17
            if (r2 >= r13) goto L_0x0248
            com.avast.android.generic.d.f r2 = new com.avast.android.generic.d.f     // Catch:{ Throwable -> 0x01b0 }
            r13 = 1
            java.security.cert.X509Certificate[] r13 = new java.security.cert.X509Certificate[r13]     // Catch:{ Throwable -> 0x01b0 }
            r14 = 0
            r13[r14] = r8     // Catch:{ Throwable -> 0x01b0 }
            java.lang.String r14 = "SHA1"
            r2.<init>(r11, r13, r14)     // Catch:{ Throwable -> 0x01b0 }
        L_0x009a:
            java.lang.String r13 = a(r6, r12)     // Catch:{ Throwable -> 0x01c7 }
            byte[] r2 = a(r13, r2, r11, r8)     // Catch:{ Throwable -> 0x01c7 }
            java.util.jar.JarEntry r8 = new java.util.jar.JarEntry     // Catch:{ Throwable -> 0x01c7 }
            java.lang.String r14 = "META-INF/CERT.SF"
            r8.<init>(r14)     // Catch:{ Throwable -> 0x01c7 }
            r8.setTime(r9)     // Catch:{ Throwable -> 0x01c7 }
            r4.putNextEntry(r8)     // Catch:{ Throwable -> 0x01c7 }
            java.io.PrintStream r8 = new java.io.PrintStream     // Catch:{ Throwable -> 0x01c7 }
            r14 = 1
            java.lang.String r15 = "UTF-8"
            r8.<init>(r4, r14, r15)     // Catch:{ Throwable -> 0x01c7 }
            r8.print(r13)     // Catch:{ Throwable -> 0x01c7 }
            r8.flush()     // Catch:{ Throwable -> 0x01c7 }
            java.util.jar.JarEntry r8 = new java.util.jar.JarEntry     // Catch:{ Throwable -> 0x01c7 }
            java.lang.String r13 = "META-INF/CERT.RSA"
            r8.<init>(r13)     // Catch:{ Throwable -> 0x01c7 }
            r8.setTime(r9)     // Catch:{ Throwable -> 0x01c7 }
            r4.putNextEntry(r8)     // Catch:{ Throwable -> 0x01c7 }
            r4.write(r2)     // Catch:{ Throwable -> 0x01c7 }
            r4.flush()     // Catch:{ Throwable -> 0x01c7 }
            r2 = 1
        L_0x00d1:
            if (r2 != 0) goto L_0x0139
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ all -> 0x01cb }
            android.content.res.Resources r5 = r17.getResources()     // Catch:{ all -> 0x01cb }
            int r8 = com.avast.android.generic.w.key     // Catch:{ all -> 0x01cb }
            java.io.InputStream r5 = r5.openRawResource(r8)     // Catch:{ all -> 0x01cb }
            r2.<init>(r5)     // Catch:{ all -> 0x01cb }
            byte[] r3 = c(r2)     // Catch:{ all -> 0x0240 }
            r5 = 1
            java.io.Closeable[] r5 = new java.io.Closeable[r5]     // Catch:{ all -> 0x01d7 }
            r8 = 0
            r5[r8] = r2     // Catch:{ all -> 0x01d7 }
            com.avast.android.generic.util.af.a(r5)     // Catch:{ all -> 0x01d7 }
            com.avast.android.generic.d.h r2 = new com.avast.android.generic.d.h     // Catch:{ all -> 0x01d7 }
            r2.<init>()     // Catch:{ all -> 0x01d7 }
            r2.a(r11)     // Catch:{ all -> 0x01d7 }
            java.lang.String r5 = a(r6, r12)     // Catch:{ all -> 0x01d7 }
            java.lang.String r8 = "UTF-8"
            byte[] r8 = r5.getBytes(r8)     // Catch:{ all -> 0x01d7 }
            r2.a(r8)     // Catch:{ all -> 0x01d7 }
            byte[] r2 = r2.a()     // Catch:{ all -> 0x01d7 }
            java.util.jar.JarEntry r8 = new java.util.jar.JarEntry     // Catch:{ all -> 0x01d7 }
            java.lang.String r11 = "META-INF/CERT.SF"
            r8.<init>(r11)     // Catch:{ all -> 0x01d7 }
            r8.setTime(r9)     // Catch:{ all -> 0x01d7 }
            r4.putNextEntry(r8)     // Catch:{ all -> 0x01d7 }
            java.io.PrintStream r8 = new java.io.PrintStream     // Catch:{ all -> 0x01d7 }
            r11 = 1
            java.lang.String r12 = "UTF-8"
            r8.<init>(r4, r11, r12)     // Catch:{ all -> 0x01d7 }
            r8.print(r5)     // Catch:{ all -> 0x01d7 }
            r8.flush()     // Catch:{ all -> 0x01d7 }
            java.util.jar.JarEntry r5 = new java.util.jar.JarEntry     // Catch:{ all -> 0x01d7 }
            java.lang.String r8 = "META-INF/CERT.RSA"
            r5.<init>(r8)     // Catch:{ all -> 0x01d7 }
            r5.setTime(r9)     // Catch:{ all -> 0x01d7 }
            r4.putNextEntry(r5)     // Catch:{ all -> 0x01d7 }
            r4.write(r3)     // Catch:{ all -> 0x01d7 }
            r4.write(r2)     // Catch:{ all -> 0x01d7 }
            r4.flush()     // Catch:{ all -> 0x01d7 }
        L_0x0139:
            a(r6, r4)     // Catch:{ all -> 0x01d7 }
            r6.close()     // Catch:{ Exception -> 0x020a, all -> 0x020f }
        L_0x013f:
            r2 = 0
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x0226, all -> 0x0214 }
            r5 = 0
            r3[r5] = r4     // Catch:{ Exception -> 0x0226, all -> 0x0214 }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x0226, all -> 0x0214 }
            r6 = 0
            r5 = 0
            if (r21 != 0) goto L_0x0176
            r7.delete()     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            boolean r2 = r1.renameTo(r7)     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            if (r2 != 0) goto L_0x0176
            r2 = 0
            r3 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01ea, all -> 0x0222 }
            r4.<init>(r7)     // Catch:{ Exception -> 0x01ea, all -> 0x0222 }
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x022f, all -> 0x0222 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x022f, all -> 0x0222 }
            com.avast.android.generic.util.ag.a(r2, r4)     // Catch:{ Exception -> 0x0233, all -> 0x0222 }
            r3 = 2
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x0233, all -> 0x0222 }
            r7 = 0
            r3[r7] = r2     // Catch:{ Exception -> 0x0233, all -> 0x0222 }
            r7 = 1
            r3[r7] = r4     // Catch:{ Exception -> 0x0233, all -> 0x0222 }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x0233, all -> 0x0222 }
            r2 = 0
            r3 = 0
            r1.delete()     // Catch:{ Exception -> 0x0236, all -> 0x0222 }
        L_0x0176:
            if (r6 == 0) goto L_0x017b
            r6.close()     // Catch:{ IOException -> 0x0200 }
        L_0x017b:
            if (r5 == 0) goto L_0x0180
            r5.close()     // Catch:{ IOException -> 0x0200 }
        L_0x0180:
            return
        L_0x0181:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x018a }
            r0 = r21
            r1.<init>(r0)     // Catch:{ Exception -> 0x018a }
            goto L_0x0013
        L_0x018a:
            r1 = move-exception
        L_0x018b:
            throw r1     // Catch:{ all -> 0x018c }
        L_0x018c:
            r1 = move-exception
        L_0x018d:
            if (r4 == 0) goto L_0x0192
            r4.close()     // Catch:{ IOException -> 0x0205 }
        L_0x0192:
            if (r5 == 0) goto L_0x0197
            r5.close()     // Catch:{ IOException -> 0x0205 }
        L_0x0197:
            throw r1
        L_0x0198:
            r1 = move-exception
            r2 = r3
        L_0x019a:
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x018a }
            r6 = 0
            r3[r6] = r2     // Catch:{ Exception -> 0x018a }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x018a }
            throw r1     // Catch:{ Exception -> 0x018a }
        L_0x01a4:
            r1 = move-exception
            r2 = r3
        L_0x01a6:
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x018a }
            r6 = 0
            r3[r6] = r2     // Catch:{ Exception -> 0x018a }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x018a }
            throw r1     // Catch:{ Exception -> 0x018a }
        L_0x01b0:
            r2 = move-exception
            com.avast.android.generic.d.g r2 = new com.avast.android.generic.d.g     // Catch:{ Throwable -> 0x01c0 }
            r13 = 1
            java.security.cert.X509Certificate[] r13 = new java.security.cert.X509Certificate[r13]     // Catch:{ Throwable -> 0x01c0 }
            r14 = 0
            r13[r14] = r8     // Catch:{ Throwable -> 0x01c0 }
            java.lang.String r14 = "SHA1"
            r2.<init>(r11, r13, r14)     // Catch:{ Throwable -> 0x01c0 }
            goto L_0x009a
        L_0x01c0:
            r2 = move-exception
            java.lang.Exception r8 = new java.lang.Exception     // Catch:{ Throwable -> 0x01c7 }
            r8.<init>(r2)     // Catch:{ Throwable -> 0x01c7 }
            throw r8     // Catch:{ Throwable -> 0x01c7 }
        L_0x01c7:
            r2 = move-exception
            r2 = r5
            goto L_0x00d1
        L_0x01cb:
            r1 = move-exception
            r2 = r3
        L_0x01cd:
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ all -> 0x01d7 }
            r5 = 0
            r3[r5] = r2     // Catch:{ all -> 0x01d7 }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ all -> 0x01d7 }
            throw r1     // Catch:{ all -> 0x01d7 }
        L_0x01d7:
            r1 = move-exception
            r2 = r4
            r3 = r6
        L_0x01da:
            r3.close()     // Catch:{ Exception -> 0x020d, all -> 0x0219 }
        L_0x01dd:
            r4 = 0
            r3 = 1
            java.io.Closeable[] r3 = new java.io.Closeable[r3]     // Catch:{ Exception -> 0x022b, all -> 0x021e }
            r5 = 0
            r3[r5] = r2     // Catch:{ Exception -> 0x022b, all -> 0x021e }
            com.avast.android.generic.util.af.a(r3)     // Catch:{ Exception -> 0x022b, all -> 0x021e }
            r4 = 0
            r5 = 0
            throw r1     // Catch:{ Exception -> 0x018a }
        L_0x01ea:
            r1 = move-exception
            r16 = r3
            r3 = r2
            r2 = r16
        L_0x01f0:
            r4 = 2
            java.io.Closeable[] r4 = new java.io.Closeable[r4]     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            r7 = 0
            r4[r7] = r2     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            r2 = 1
            r4[r2] = r3     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            com.avast.android.generic.util.af.a(r4)     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
            throw r1     // Catch:{ Exception -> 0x01fd, all -> 0x0222 }
        L_0x01fd:
            r1 = move-exception
            r4 = r6
            goto L_0x018b
        L_0x0200:
            r1 = move-exception
            r1.printStackTrace()
            throw r1
        L_0x0205:
            r1 = move-exception
            r1.printStackTrace()
            throw r1
        L_0x020a:
            r2 = move-exception
            goto L_0x013f
        L_0x020d:
            r3 = move-exception
            goto L_0x01dd
        L_0x020f:
            r1 = move-exception
            r5 = r4
            r4 = r6
            goto L_0x018d
        L_0x0214:
            r1 = move-exception
            r5 = r4
            r4 = r2
            goto L_0x018d
        L_0x0219:
            r1 = move-exception
            r5 = r2
            r4 = r3
            goto L_0x018d
        L_0x021e:
            r1 = move-exception
            r5 = r2
            goto L_0x018d
        L_0x0222:
            r1 = move-exception
            r4 = r6
            goto L_0x018d
        L_0x0226:
            r1 = move-exception
            r5 = r4
            r4 = r2
            goto L_0x018b
        L_0x022b:
            r1 = move-exception
            r5 = r2
            goto L_0x018b
        L_0x022f:
            r1 = move-exception
            r2 = r3
            r3 = r4
            goto L_0x01f0
        L_0x0233:
            r1 = move-exception
            r3 = r4
            goto L_0x01f0
        L_0x0236:
            r1 = move-exception
            goto L_0x01f0
        L_0x0238:
            r1 = move-exception
            r2 = r5
            r3 = r4
            goto L_0x01da
        L_0x023c:
            r1 = move-exception
            r2 = r5
            r3 = r6
            goto L_0x01da
        L_0x0240:
            r1 = move-exception
            goto L_0x01cd
        L_0x0242:
            r1 = move-exception
            goto L_0x01a6
        L_0x0245:
            r1 = move-exception
            goto L_0x019a
        L_0x0248:
            r2 = r5
            goto L_0x00d1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.avast.android.generic.d.d.a(android.content.Context, int, int, java.lang.String, java.lang.String):void");
    }
}
