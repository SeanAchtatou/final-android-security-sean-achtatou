package com.avast.android.generic.app.subscription;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import com.avast.android.generic.ui.a.a;

/* compiled from: PurchaseProviderArrayAdapter */
public class e extends a {

    /* renamed from: a  reason: collision with root package name */
    private Boolean[] f546a;

    public e(Context context, ListAdapter listAdapter, Boolean[] boolArr) {
        super(context, listAdapter);
        this.f546a = boolArr;
    }

    public boolean isEnabled(int i) {
        return this.f546a[i].booleanValue();
    }

    public boolean areAllItemsEnabled() {
        for (Boolean booleanValue : this.f546a) {
            if (!booleanValue.booleanValue()) {
                return false;
            }
        }
        return true;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2 = super.getView(i, view, viewGroup);
        if (view2 != null) {
            if (!isEnabled(i)) {
                view2.setEnabled(false);
            } else {
                view2.setEnabled(true);
            }
        }
        return view2;
    }
}
