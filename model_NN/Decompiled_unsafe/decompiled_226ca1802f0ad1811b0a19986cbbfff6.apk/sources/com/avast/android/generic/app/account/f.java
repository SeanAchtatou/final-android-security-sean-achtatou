package com.avast.android.generic.app.account;

/* compiled from: BackupValidIndicator */
public enum f {
    VALID,
    NO_BACKUP,
    INVALID_BACKUP_FORMAT,
    NOT_UP_TO_DATE_BACKUP,
    GENERIC_ISSUE
}
