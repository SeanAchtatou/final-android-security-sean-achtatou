package com.avast.android.generic.notification;

/* compiled from: AvastNotificationManager */
class o {

    /* renamed from: a  reason: collision with root package name */
    long f944a;

    /* renamed from: b  reason: collision with root package name */
    String f945b;

    /* synthetic */ o(long j, String str, i iVar) {
        this(j, str);
    }

    private o(long j, String str) {
        this.f944a = j;
        this.f945b = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        o oVar = (o) obj;
        if (this.f944a != oVar.f944a) {
            return false;
        }
        if (this.f945b != null) {
            if (this.f945b.equals(oVar.f945b)) {
                return true;
            }
        } else if (oVar.f945b == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.f945b != null ? this.f945b.hashCode() : 0) + (((int) (this.f944a ^ (this.f944a >>> 32))) * 31);
    }
}
