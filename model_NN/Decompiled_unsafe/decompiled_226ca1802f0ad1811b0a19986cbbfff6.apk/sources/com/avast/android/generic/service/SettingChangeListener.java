package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.avast.android.generic.a.a;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.ad;
import com.avast.android.generic.ae;
import com.avast.android.generic.util.z;
import java.util.Set;

public class SettingChangeListener extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String stringExtra;
        Context applicationContext = context.getApplicationContext();
        z.a(applicationContext, "Received key change event from other component");
        if (intent.getAction().equals("com.avast.android.generic.action.PROPERTY_CHANGED") && (stringExtra = intent.getStringExtra("sourcePackage")) != null && !stringExtra.equals(applicationContext.getPackageName())) {
            Bundle extras = intent.getExtras();
            Set<String> keySet = extras.keySet();
            boolean z = extras.getBoolean("com.avast.android.generic.action.SHARE_SETTINGS", false);
            keySet.remove("com.avast.android.generic.action.SHARE_SETTINGS");
            keySet.remove("sourcePackage");
            if (z) {
                ((a) aa.a(applicationContext, a.class)).a(false);
            }
            if (keySet.size() != 0) {
                ab abVar = (ab) aa.a(applicationContext, ad.class);
                ab abVar2 = (ab) aa.a(applicationContext, ae.class);
                boolean t = abVar.t();
                z.b(applicationContext, stringExtra, "KEY CHANGE START");
                for (String next : keySet) {
                    if (next.equals("c2dmowner") || next.equals("c2dmri")) {
                        a(abVar2, next, extras.get(next), stringExtra);
                    } else {
                        a(abVar, next, extras.get(next), stringExtra);
                    }
                }
                z.b(applicationContext, stringExtra, "KEY CHANGE END");
                abVar2.b();
                abVar.b();
                boolean t2 = abVar.t();
                if (!t && t2) {
                    Intent intent2 = new Intent();
                    intent2.setAction("com.avast.android.mobilesecurity.app.account.ACCOUNT_CONNECTED");
                    com.avast.android.generic.util.ae.a(applicationContext, intent2, applicationContext.getPackageName());
                }
                a(applicationContext, abVar, abVar2, extras, keySet, stringExtra);
            }
        }
    }

    private void a(ab abVar, String str, Object obj, String str2) {
        if (obj.equals("-DEL-")) {
            z.b(abVar.L(), str2, str + " -> deleted");
            abVar.a(str);
        } else if (obj.equals("-NULL-")) {
            z.b(abVar.L(), str2, str + " -> NULL");
            abVar.a(str, (String) null);
        } else {
            z.b(abVar.L(), str2, str + " -> " + obj);
            if (obj instanceof String) {
                abVar.a(str, (String) obj);
            } else if (obj instanceof Boolean) {
                abVar.a(str, ((Boolean) obj).booleanValue());
            } else if (obj instanceof Integer) {
                abVar.a(str, ((Integer) obj).intValue());
            } else if (obj instanceof Long) {
                abVar.a(str, ((Long) obj).longValue());
            } else if (obj instanceof byte[]) {
                abVar.a(str, (byte[]) obj);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void a(Context context, ab abVar, ab abVar2, Bundle bundle, Set<String> set, String str) {
        Intent intent = new Intent();
        intent.setAction("com.avast.android.generic.action.ACTION_SUITE_SETTINGS_COMMIT");
        for (String putExtra : set) {
            intent.putExtra(putExtra, true);
        }
        com.avast.android.generic.util.aa.a(context, context.getPackageName(), intent);
    }
}
