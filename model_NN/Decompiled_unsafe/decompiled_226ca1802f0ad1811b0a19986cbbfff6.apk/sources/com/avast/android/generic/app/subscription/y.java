package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;
import com.avast.android.generic.licensing.PurchaseConfirmationService;

/* compiled from: SubscriptionFragment */
class y implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f582a;

    y(x xVar) {
        this.f582a = xVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        PurchaseConfirmationService.b(this.f582a.f581a.getActivity());
        this.f582a.f581a.f();
        this.f582a.f581a.r.setResult(99);
        this.f582a.f581a.r.finish();
    }
}
