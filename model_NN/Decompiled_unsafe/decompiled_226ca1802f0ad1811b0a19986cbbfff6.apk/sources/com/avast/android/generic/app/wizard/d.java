package com.avast.android.generic.app.wizard;

import android.os.SystemClock;
import android.view.View;
import com.avast.android.generic.util.a;
import com.avast.android.generic.util.e;

/* compiled from: EulaFragment */
class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EulaFragment f591a;

    d(EulaFragment eulaFragment) {
        this.f591a = eulaFragment;
    }

    public void onClick(View view) {
        this.f591a.f.a(e.DISAGREE);
        this.f591a.a("ms-Wizard", "accept", "no", (long) ((int) (SystemClock.uptimeMillis() - this.f591a.d)));
        a.a(this.f591a);
    }
}
