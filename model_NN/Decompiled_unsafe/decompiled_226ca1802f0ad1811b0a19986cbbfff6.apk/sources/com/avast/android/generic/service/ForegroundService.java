package com.avast.android.generic.service;

import android.app.Notification;
import android.app.Service;

public abstract class ForegroundService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private static final Class<?>[] f956a = {Boolean.TYPE};

    /* renamed from: b  reason: collision with root package name */
    private static final Class<?>[] f957b = {Integer.TYPE, Notification.class};

    /* renamed from: c  reason: collision with root package name */
    private static final Class<?>[] f958c = {Boolean.TYPE};
    private Object[] d = new Object[1];
    private Object[] e = new Object[2];
    private Object[] f = new Object[1];
    private int g = 0;
}
