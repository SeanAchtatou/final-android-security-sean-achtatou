package com.avast.android.generic.ui.widget;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.util.AttributeSet;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.n;
import com.avast.android.generic.t;
import com.avast.android.generic.util.ak;
import com.avast.android.generic.y;
import com.avast.android.generic.z;

public class SelectorRow extends Row implements Handler.Callback {

    /* renamed from: a  reason: collision with root package name */
    private int f1106a;

    /* renamed from: b  reason: collision with root package name */
    private int f1107b;

    /* renamed from: c  reason: collision with root package name */
    private int[] f1108c;
    /* access modifiers changed from: private */
    public String[] m;
    /* access modifiers changed from: private */
    public int n;
    private String o;
    private int p;
    private u q;

    public SelectorRow(Context context) {
        this(context, null);
    }

    public SelectorRow(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, n.rowNextStyle);
    }

    public SelectorRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a(Context context, AttributeSet attributeSet, int i) {
        super.a(context, attributeSet, i);
        this.o = this.f;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, z.i, i, y.Row_Next);
        this.f1106a = obtainStyledAttributes.getResourceId(1, 0);
        this.f1107b = obtainStyledAttributes.getResourceId(0, 0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, z.h, 0, 0);
        this.p = obtainStyledAttributes2.getInt(3, 0);
        obtainStyledAttributes2.recycle();
    }

    /* access modifiers changed from: protected */
    public void a() {
        inflate(getContext(), t.row_next, this);
        if (getId() == -1) {
            throw new NullPointerException("You have to specify android:id, otherwise the SelectorRow won't work.");
        }
        if (this.f1107b != 0) {
            this.m = getResources().getStringArray(this.f1107b);
        }
        if (this.f1106a != 0) {
            this.f1108c = getResources().getIntArray(this.f1106a);
        }
        a(new t(this));
        this.n = 0;
        f();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onDetachedFromWindow();
        ((com.avast.android.generic.util.y) aa.a(getContext(), com.avast.android.generic.util.y.class)).a(getId(), this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ((com.avast.android.generic.util.y) aa.a(getContext(), com.avast.android.generic.util.y.class)).b(getId(), this);
    }

    public void a(int i) {
        this.f1107b = i;
        this.m = getResources().getStringArray(this.f1107b);
        f();
    }

    public void a(int[] iArr) {
        this.f1106a = 0;
        this.f1108c = iArr;
    }

    public void a(u uVar) {
        this.q = uVar;
    }

    public void a(String str) {
        this.o = str;
    }

    public int c() {
        if (this.f1108c != null) {
            return this.f1108c[this.n];
        }
        return this.n;
    }

    private void a(int i, boolean z) {
        int i2 = 0;
        this.n = 0;
        if (this.f1108c != null) {
            while (true) {
                if (i2 >= this.f1108c.length) {
                    break;
                } else if (i == this.f1108c[i2]) {
                    this.n = i2;
                    break;
                } else {
                    i2++;
                }
            }
        }
        f();
        if (z && this.q != null) {
            this.q.a(this, this.n, c());
        }
    }

    public boolean handleMessage(Message message) {
        if (message.what != getId()) {
            return false;
        }
        this.n = message.arg1;
        f();
        d();
        if (this.q != null) {
            this.q.a(this, this.n, c());
        }
        return true;
    }

    private void d() {
        if (e() != null && this.g != null) {
            e().a(this.g, c());
        }
    }

    public void b() {
        if (e() != null) {
            a(e().b(this.g, this.p), false);
        }
    }

    private void f() {
        if (this.m != null) {
            String str = this.m[this.n].toString();
            if (this.o != null) {
                c(String.format(this.o, str));
                return;
            }
            c(str);
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f1109a = this.n;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.n = savedState.f1109a;
        f();
    }

    class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new v();

        /* renamed from: a  reason: collision with root package name */
        int f1109a;

        /* synthetic */ SavedState(Parcel parcel, t tVar) {
            this(parcel);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f1109a = parcel.readBundle().getInt("selected");
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            Bundle bundle = new Bundle();
            bundle.putInt("selected", this.f1109a);
            parcel.writeBundle(bundle);
        }
    }

    public class SelectorDialog extends DialogFragment {

        /* renamed from: a  reason: collision with root package name */
        private String[] f1110a;
        /* access modifiers changed from: private */

        /* renamed from: b  reason: collision with root package name */
        public int f1111b;

        /* renamed from: c  reason: collision with root package name */
        private int f1112c;

        public static void a(FragmentManager fragmentManager, int i, String[] strArr, int i2) {
            if (fragmentManager.findFragmentByTag("selector_row_dialog") == null) {
                new SelectorDialog(i, strArr, i2).show(fragmentManager, "selector_row_dialog");
            }
        }

        private SelectorDialog(int i, String[] strArr, int i2) {
            this.f1111b = i;
            this.f1112c = i2;
            this.f1110a = strArr;
        }

        public SelectorDialog() {
        }

        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            if (bundle != null) {
                this.f1110a = bundle.getStringArray("mEntriesNames");
                this.f1111b = bundle.getInt("mMessageId");
                this.f1112c = bundle.getInt("mSelected");
            }
        }

        public Dialog onCreateDialog(Bundle bundle) {
            AlertDialog.Builder builder = new AlertDialog.Builder(ak.c(getActivity()));
            builder.setSingleChoiceItems(this.f1110a, this.f1112c, new w(this));
            AlertDialog create = builder.create();
            create.setInverseBackgroundForced(true);
            return create;
        }

        public void onSaveInstanceState(Bundle bundle) {
            super.onSaveInstanceState(bundle);
            bundle.putStringArray("mEntriesNames", this.f1110a);
            bundle.putInt("mMessageId", this.f1111b);
            bundle.putInt("mSelected", this.f1112c);
        }
    }
}
