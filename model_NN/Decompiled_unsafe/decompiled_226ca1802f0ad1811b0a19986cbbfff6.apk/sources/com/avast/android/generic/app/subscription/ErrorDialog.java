package com.avast.android.generic.app.subscription;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.r;
import com.avast.android.generic.x;

public class ErrorDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private r f512a;
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public Intent f513b;

    public static void a(FragmentManager fragmentManager, int i, int i2, int i3, int i4, int i5, int i6) {
        ErrorDialog errorDialog = new ErrorDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("request_code", i);
        if (i2 != 0) {
            bundle.putInt("title", i2);
        }
        if (i3 != 0) {
            bundle.putInt("message", i3);
        }
        if (i4 != 0) {
            bundle.putInt("button_positive", i4);
        }
        if (i5 != 0) {
            bundle.putInt("button_negative", i5);
        }
        if (i6 != 0) {
            bundle.putInt("button_neutral", i6);
        }
        errorDialog.setArguments(bundle);
        errorDialog.show(fragmentManager, ErrorDialog.class.getCanonicalName());
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f512a = r.a(getActivity());
        this.f513b = new Intent("com.avast.android.generic.ui.licensing.ErrorDialog.DISMISSED");
        this.f513b.putExtra("request_code", getArguments().getInt("request_code"));
    }

    public Dialog onCreateDialog(Bundle bundle) {
        boolean z;
        boolean z2 = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        Bundle arguments = getArguments();
        builder.setTitle(arguments.getInt("title", x.l_subscription_error_title)).setMessage(arguments.getInt("message", x.msg_subscription_error_message));
        if (arguments.containsKey("button_positive")) {
            builder.setPositiveButton(arguments.getInt("button_positive"), new a(this));
            z = true;
        } else {
            z = false;
        }
        if (arguments.containsKey("button_negative")) {
            builder.setNegativeButton(arguments.getInt("button_negative"), new b(this));
            z = true;
        }
        if (arguments.containsKey("button_neutral")) {
            builder.setNeutralButton(arguments.getInt("button_neutral"), new c(this));
        } else {
            z2 = z;
        }
        if (!z2) {
            builder.setPositiveButton(x.l_subscription_error_button_ok, (DialogInterface.OnClickListener) null);
        }
        AlertDialog create = builder.create();
        create.setCanceledOnTouchOutside(false);
        return create;
    }

    public void onCancel(DialogInterface dialogInterface) {
        super.onCancel(dialogInterface);
        this.f513b.putExtra("return_code", d.CANCELED.a());
    }

    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        this.f512a.a(this.f513b);
    }
}
