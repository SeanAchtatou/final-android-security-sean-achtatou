package com.avast.android.generic.app.subscription;

import android.os.Bundle;
import android.view.View;
import com.avast.android.generic.app.account.AccountActivity;

/* compiled from: WelcomePremiumFragment */
class am implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomePremiumFragment f540a;

    am(WelcomePremiumFragment welcomePremiumFragment) {
        this.f540a = welcomePremiumFragment;
    }

    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("queryPhoneNumber", this.f540a.j());
        AccountActivity.call(this.f540a.getActivity(), bundle);
    }
}
