package com.avast.android.generic.i;

import android.util.SparseArray;
import java.util.EnumSet;
import java.util.Iterator;

/* compiled from: StreamBackType */
public enum c {
    NOTHING(0);
    

    /* renamed from: b  reason: collision with root package name */
    private static final SparseArray<c> f739b = new SparseArray<>();

    /* renamed from: c  reason: collision with root package name */
    private final int f740c;

    static {
        Iterator it = EnumSet.allOf(c.class).iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            f739b.put(cVar.a(), cVar);
        }
    }

    private c(int i) {
        this.f740c = i;
    }

    public final int a() {
        return this.f740c;
    }
}
