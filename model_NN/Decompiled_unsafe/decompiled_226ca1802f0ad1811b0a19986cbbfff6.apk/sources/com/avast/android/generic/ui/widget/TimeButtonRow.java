package com.avast.android.generic.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import com.avast.android.generic.e.c;
import com.avast.android.generic.n;
import com.avast.android.generic.util.aj;

public class TimeButtonRow extends BlackButtonRow {
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f1127b;

    /* renamed from: c  reason: collision with root package name */
    private int f1128c;
    private ah m;

    public TimeButtonRow(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public TimeButtonRow(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, n.rowStyle);
    }

    /* access modifiers changed from: protected */
    public void a(Context context, TypedArray typedArray) {
        try {
            this.f1128c = Integer.parseInt(typedArray.getString(3));
        } catch (NumberFormatException e) {
            this.f1128c = 0;
        }
        typedArray.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.widget.TimeButtonRow.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.avast.android.generic.ui.widget.Row.a(android.content.Context, android.util.AttributeSet, int):void
      com.avast.android.generic.ui.widget.TimeButtonRow.a(int, boolean, boolean):void */
    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        if (!isInEditMode()) {
            a(new af(this, DateFormat.is24HourFormat(getContext())));
        }
        a(this.f1128c, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ui.widget.TimeButtonRow.a(int, boolean, boolean):void
     arg types: [int, int, int]
     candidates:
      com.avast.android.generic.ui.widget.Row.a(android.content.Context, android.util.AttributeSet, int):void
      com.avast.android.generic.ui.widget.TimeButtonRow.a(int, boolean, boolean):void */
    public void b() {
        a(e().b(this.g, this.f1128c), false, false);
    }

    /* access modifiers changed from: private */
    public void a(int i, boolean z, boolean z2) {
        this.f1074a.setText(aj.a(getContext(), i));
        c e = e();
        if (!(!z || i == this.f1127b || e == null)) {
            e.a(this.g, i);
        }
        int i2 = this.f1127b;
        this.f1127b = i;
        if (z2 && this.m != null && i2 != i) {
            this.m.a(this, i);
        }
    }
}
