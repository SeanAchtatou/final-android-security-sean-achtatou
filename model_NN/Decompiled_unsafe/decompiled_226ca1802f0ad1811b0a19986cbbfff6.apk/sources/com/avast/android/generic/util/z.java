package com.avast.android.generic.util;

import android.content.Context;

/* compiled from: Log */
public class z {
    public static void a(String str, String str2) {
        t.b(str, str2);
    }

    public static void a(String str, String str2, Throwable th) {
        b(str, str2, th);
    }

    private static void b(String str, String str2, Throwable th) {
        if (th != null) {
            t.d(str, str2, th);
        } else {
            t.b(str, str2);
        }
    }

    public static void a(Context context, String str, String str2) {
        t.b("AvastIPC", context.getPackageName() + " to " + str + ": " + str2);
    }

    public static void b(Context context, String str, String str2) {
        t.b("AvastIPC", context.getPackageName() + " from " + str + ": " + str2);
    }

    public static void a(Context context, String str) {
        t.b("AvastIPC", context.getPackageName() + ": " + str);
    }

    public static void a(String str, Context context, String str2) {
        t.b(str, context.getPackageName() + ": " + str2);
    }

    public static void a(String str, Context context, String str2, Throwable th) {
        t.d(str, context.getPackageName() + ": " + str2, th);
    }
}
