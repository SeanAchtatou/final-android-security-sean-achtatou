package com.avast.android.generic.j;

import android.content.Context;
import android.os.Bundle;
import com.avast.android.generic.service.AvastService;
import com.avast.android.generic.util.ga.e;

/* compiled from: Task */
public abstract class a extends e {

    /* renamed from: a  reason: collision with root package name */
    private AvastService f829a;

    public abstract void a(Context context, String str, Bundle bundle);

    public void a(Context context, String str, Bundle bundle, c cVar) {
        try {
            new Thread(new b(this, context, str, bundle, cVar)).start();
        } catch (Exception e) {
            a.a.a.a.a.a.a().a("Exception in executing event", e);
            cVar.a();
        }
    }

    public void a(AvastService avastService) {
        super.a((Context) avastService);
        this.f829a = avastService;
    }

    public String e_() {
        return null;
    }
}
