package com.avast.android.generic.app.subscription;

import android.content.Intent;
import android.view.View;

/* compiled from: WelcomePremiumFragment */
class ak implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WelcomePremiumFragment f538a;

    ak(WelcomePremiumFragment welcomePremiumFragment) {
        this.f538a = welcomePremiumFragment;
    }

    public void onClick(View view) {
        Intent intent = new Intent("android.intent.action.VIEW", WelcomePremiumFragment.f521b);
        intent.addFlags(268468224);
        this.f538a.startActivity(intent);
    }
}
