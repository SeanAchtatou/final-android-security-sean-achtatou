package com.avast.android.generic.h;

import com.google.protobuf.Internal;

/* compiled from: FeedbackProto */
public enum q implements Internal.EnumLite {
    CUSTOM_FEEDBACK(0, 1),
    FEATURE_REQUEST(1, 2),
    BUG_REPORT(2, 3),
    CRASH_REPORT(3, 4);
    
    private static Internal.EnumLiteMap<q> e = new r();
    private final int f;

    public final int getNumber() {
        return this.f;
    }

    public static q a(int i) {
        switch (i) {
            case 1:
                return CUSTOM_FEEDBACK;
            case 2:
                return FEATURE_REQUEST;
            case 3:
                return BUG_REPORT;
            case 4:
                return CRASH_REPORT;
            default:
                return null;
        }
    }

    private q(int i, int i2) {
        this.f = i2;
    }
}
