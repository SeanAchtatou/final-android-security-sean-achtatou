package com.avast.android.generic.util;

import android.support.v4.app.FragmentTransaction;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.util.Timer;

/* compiled from: CommandExecutor */
public class o {
    public static int a(String str, StringBuilder sb) {
        DataInputStream dataInputStream;
        z.a("AvastGeneric", "Executing " + str + " as normal user");
        Process exec = Runtime.getRuntime().exec(str);
        try {
            dataInputStream = new DataInputStream(exec.getInputStream());
            try {
                exec.exitValue();
            } catch (IllegalThreadStateException e) {
                exec.waitFor();
            } catch (Throwable th) {
                th = th;
            }
            int available = dataInputStream.available();
            if (available > 0) {
                byte[] bArr = new byte[available];
                dataInputStream.read(bArr, 0, available);
                sb.append(new String(bArr));
            }
            af.a(dataInputStream);
            return exec.exitValue();
        } catch (Throwable th2) {
            th = th2;
            dataInputStream = null;
            af.a(dataInputStream);
            throw th;
        }
    }

    public static String a(String str) {
        return a(str, 30);
    }

    public static String a(String str, int i) {
        String message;
        DataInputStream dataInputStream;
        DataInputStream dataInputStream2;
        try {
            Process exec = Runtime.getRuntime().exec("su");
            StringBuilder sb = new StringBuilder();
            StringBuilder sb2 = new StringBuilder();
            Integer num = null;
            try {
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new DataOutputStream(exec.getOutputStream()), "UTF-8");
                DataInputStream dataInputStream3 = new DataInputStream(exec.getInputStream());
                try {
                    dataInputStream2 = new DataInputStream(exec.getErrorStream());
                    try {
                        z.a("AvastGeneric", "Executing " + str + " as superuser");
                        String[] split = ((str + "\n") + "exit\n").split("\n");
                        for (int i2 = 0; i2 < split.length; i2++) {
                            z.a("AvastGeneric", "Executing " + split[i2] + " as superuser");
                            outputStreamWriter.write(split[i2] + "\n");
                        }
                        outputStreamWriter.flush();
                        a(dataInputStream3, dataInputStream2, sb, sb2);
                        Thread currentThread = Thread.currentThread();
                        q qVar = new q();
                        Timer timer = null;
                        if (i > 0) {
                            timer = new Timer();
                            timer.schedule(new p(qVar, currentThread), (long) (i * 1000));
                        }
                        z.a("AvastGeneric", "Waiting for process...");
                        try {
                            byte[] bArr = new byte[FragmentTransaction.TRANSIT_EXIT_MASK];
                            while (exec != null) {
                                try {
                                    num = Integer.valueOf(exec.exitValue());
                                } catch (IllegalThreadStateException e) {
                                }
                                if (dataInputStream3.available() > 0) {
                                    int read = dataInputStream3.read(bArr);
                                    if (sb != null) {
                                        String str2 = new String(bArr, 0, read);
                                        if (str2.contains("denied")) {
                                            throw new Exception("Permission denied");
                                        }
                                        sb.append(str2);
                                    }
                                    if (read > 0) {
                                        continue;
                                    }
                                }
                                if (dataInputStream2.available() > 0) {
                                    int read2 = dataInputStream2.read(bArr);
                                    if (sb2 != null) {
                                        sb2.append(new String(bArr, 0, read2).replace("\r", "").replace("\n", "").trim());
                                        if (sb2.length() > 0) {
                                            z.a("AvastGeneric", "Errors: " + sb2.toString());
                                            throw new Exception(sb2.toString());
                                        }
                                    }
                                    if (read2 > 0) {
                                        continue;
                                    }
                                }
                                if (num != null) {
                                    break;
                                } else if (qVar.f1251a) {
                                    throw new InterruptedException();
                                } else {
                                    Thread.sleep(50);
                                }
                            }
                            if (timer != null) {
                                timer.cancel();
                            }
                        } catch (InterruptedException e2) {
                            if (qVar.f1251a) {
                                exec.destroy();
                                throw new Exception("Command did not return in " + i + " seconds");
                            } else if (timer != null) {
                                timer.cancel();
                            }
                        } catch (Throwable th) {
                            if (timer != null) {
                                timer.cancel();
                            }
                            throw th;
                        }
                        if (num == null) {
                            num = -1;
                        }
                        z.a("AvastGeneric", "Process finished (return code " + num + ").");
                        af.a(dataInputStream3, dataInputStream2);
                        if (num.intValue() != 0) {
                            throw new Exception("Return code is " + num);
                        } else if (sb2.length() > 0) {
                            z.a("AvastGeneric", "Errors: " + sb2.toString());
                            throw new Exception(sb2.toString());
                        } else {
                            String sb3 = sb.toString();
                            z.a("AvastGeneric", "Result: " + sb3);
                            return sb3;
                        }
                    } catch (Exception e3) {
                        exec.destroy();
                        throw e3;
                    } catch (Throwable th2) {
                        th = th2;
                        dataInputStream = dataInputStream3;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    dataInputStream2 = null;
                    dataInputStream = dataInputStream3;
                    af.a(dataInputStream, dataInputStream2);
                    throw th;
                }
            } catch (Throwable th4) {
                th = th4;
                dataInputStream = null;
                dataInputStream2 = null;
                af.a(dataInputStream, dataInputStream2);
                throw th;
            }
        } catch (Exception e4) {
            Throwable cause = e4.getCause();
            if (cause == null || (message = cause.getMessage()) == null || !message.contains("denied")) {
                throw e4;
            }
            throw new Exception("Permission denied");
        }
    }

    private static int a(DataInputStream dataInputStream, DataInputStream dataInputStream2, StringBuilder sb, StringBuilder sb2) {
        int i = -1;
        int available = dataInputStream.available();
        if (available > 0) {
            byte[] bArr = new byte[available];
            i = dataInputStream.read(bArr, 0, available);
            String str = new String(bArr);
            if (str.contains("denied")) {
                throw new Exception("Permission denied");
            }
            sb.append(str);
        }
        z.a("AvastGeneric", "Read exactly " + i + " bytes from result stream");
        int available2 = dataInputStream2.available();
        if (available2 > 0) {
            byte[] bArr2 = new byte[available2];
            dataInputStream2.read(bArr2, 0, available2);
            sb2.append(new String(bArr2).replace("\r", "").replace("\n", "").trim());
        }
        z.a("AvastGeneric", "Read approx. " + available2 + " bytes from error stream");
        if (sb2.length() <= 0) {
            return i;
        }
        z.a("AvastGeneric", "Errors: " + sb2.toString());
        throw new Exception(sb2.toString());
    }
}
