package com.avast.android.generic.g.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.z;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;

/* compiled from: StateProvider */
public abstract class a extends PhoneStateListener {
    private static String f = null;
    private static boolean g = false;

    /* renamed from: a  reason: collision with root package name */
    private TelephonyManager f691a;

    /* renamed from: b  reason: collision with root package name */
    private CellLocation f692b;

    /* renamed from: c  reason: collision with root package name */
    private int f693c;
    private ServiceState d;
    private Context e;

    public abstract void a(int i, String str);

    public abstract void a(b bVar);

    public static String a(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return null;
        }
        String networkOperatorName = telephonyManager.getNetworkOperatorName();
        if (networkOperatorName == null || networkOperatorName.equals("")) {
            return b(context);
        }
        return networkOperatorName;
    }

    public static String b(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return null;
        }
        String simOperatorName = telephonyManager.getSimOperatorName();
        if (simOperatorName == null || simOperatorName.equals("")) {
            return null;
        }
        return simOperatorName;
    }

    public static String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return null;
        } else if (telephonyManager.getSimState() == 1) {
            return null;
        } else {
            try {
                String subscriberId = telephonyManager.getSubscriberId();
                if (subscriberId == null || subscriberId.equals("")) {
                    subscriberId = null;
                }
                return subscriberId;
            } catch (Exception e2) {
                z.a("AvastGeneric", "Can not read IMSI", e2);
                return null;
            }
        }
    }

    public static int d(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            return telephonyManager.getSimState();
        }
        z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
        return 0;
    }

    public static int e(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return 0;
        }
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator != null) {
            try {
                int parseInt = Integer.parseInt(networkOperator.substring(0, 3));
                if (parseInt > 0) {
                    return parseInt;
                }
            } catch (Exception e2) {
            }
        }
        return f(context);
    }

    public static int f(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return 0;
        }
        String simOperator = telephonyManager.getSimOperator();
        if (simOperator != null) {
            try {
                int parseInt = Integer.parseInt(simOperator.substring(0, 3));
                if (parseInt > 0) {
                    return parseInt;
                }
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    public static int g(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return 0;
        }
        String networkOperator = telephonyManager.getNetworkOperator();
        if (networkOperator != null) {
            try {
                int parseInt = Integer.parseInt(networkOperator.substring(3));
                if (parseInt > 0) {
                    return parseInt;
                }
            } catch (Exception e2) {
            }
        }
        return h(context);
    }

    public static int h(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return 0;
        }
        String simOperator = telephonyManager.getSimOperator();
        if (simOperator != null) {
            try {
                int parseInt = Integer.parseInt(simOperator.substring(3));
                if (parseInt > 0) {
                    return parseInt;
                }
            } catch (Exception e2) {
            }
        }
        return 0;
    }

    public static String i(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            z.a("AvastGeneric", context, "StateProvider: could not get telephony manager");
            return null;
        }
        try {
            str = telephonyManager.getDeviceId();
            if (str != null && str.equals("")) {
                str = null;
            }
        } catch (Exception e2) {
            z.a("AvastGeneric", "Can not read device ID", e2);
            str = null;
        }
        if (str == null) {
            if (Build.VERSION.SDK_INT >= 9) {
                try {
                    str = com.avast.android.generic.f.a.a().b();
                } catch (Exception e3) {
                }
                if (str != null && str.equals("")) {
                    str = null;
                }
                if (str == null && (str = ((ab) aa.a(context, ab.class)).q()) != null && str.equals("")) {
                    return null;
                }
            } else {
                str = ((ab) aa.a(context, ab.class)).q();
                if (str != null && str.equals("")) {
                    return null;
                }
            }
        }
        return str;
    }

    public void onCallStateChanged(int i, String str) {
        a(i, str);
    }

    public void onServiceStateChanged(ServiceState serviceState) {
        this.d = serviceState;
        a();
    }

    public void onSignalStrengthChanged(int i) {
        this.f693c = i;
        a();
    }

    public void onCellLocationChanged(CellLocation cellLocation) {
        this.f692b = cellLocation;
        a();
    }

    private void a() {
        try {
            b bVar = new b(this.e, this.f691a, this.f692b, this.f693c, this.d);
            if (bVar.a() == 0) {
                a(bVar);
            }
        } catch (Exception e2) {
            z.a("AvastGeneric", this.e, "RadioData could not be instantiated", e2);
        }
    }

    @SuppressLint({"NewApi"})
    public static boolean j(Context context) {
        if (context == null) {
            return false;
        }
        try {
            return context.getPackageManager().hasSystemFeature("android.hardware.telephony");
        } catch (Exception e2) {
            z.a("AvastGeneric", "Error while checking for call feature", e2);
            return false;
        }
    }

    public static boolean k(Context context) {
        if (c(context) == null) {
            return false;
        }
        return true;
    }

    public static void a(ConnectivityManager connectivityManager, boolean z) {
        String str;
        String str2;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        if (!z) {
            f = null;
        }
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || activeNetworkInfo.getState() == null || activeNetworkInfo.getState() == NetworkInfo.State.DISCONNECTED || activeNetworkInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                f = null;
                g = true;
                return;
            }
            int type = activeNetworkInfo.getType();
            if (type == 7 || type == 8) {
                f = null;
                g = true;
                return;
            }
            String str3 = null;
            String str4 = null;
            boolean z5 = false;
            for (NetworkInterface inetAddresses : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                for (InetAddress inetAddress : Collections.list(inetAddresses.getInetAddresses())) {
                    if (inetAddress.isLoopbackAddress() || TextUtils.isEmpty(inetAddress.getHostAddress())) {
                        str = str3;
                        str2 = str4;
                        z2 = z4;
                        z3 = z5;
                    } else if (inetAddress instanceof Inet6Address) {
                        str = inetAddress.getHostAddress();
                        str2 = str4;
                        z3 = z5;
                        z2 = true;
                    } else {
                        z2 = z4;
                        z3 = true;
                        String str5 = str3;
                        str2 = inetAddress.getHostAddress();
                        str = str5;
                    }
                    z5 = z3;
                    z4 = z2;
                    str4 = str2;
                    str3 = str;
                }
            }
            if (z4 && z5) {
                f = str4;
            } else if (z4) {
                f = str3;
            } else if (z5) {
                f = str4;
            } else {
                f = null;
            }
            g = true;
        } catch (Throwable th) {
            f = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.g.b.a.a(android.net.ConnectivityManager, boolean):void
     arg types: [android.net.ConnectivityManager, int]
     candidates:
      com.avast.android.generic.g.b.a.a(int, java.lang.String):void
      com.avast.android.generic.g.b.a.a(android.net.ConnectivityManager, boolean):void */
    public static String l(Context context) {
        if (g || context == null) {
            return f;
        }
        a((ConnectivityManager) context.getSystemService("connectivity"), true);
        return f;
    }
}
