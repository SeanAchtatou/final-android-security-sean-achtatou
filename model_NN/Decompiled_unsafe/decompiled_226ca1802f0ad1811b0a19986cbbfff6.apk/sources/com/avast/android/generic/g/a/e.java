package com.avast.android.generic.g.a;

import com.avast.android.generic.util.bc;
import java.util.Date;

/* compiled from: SmsSender */
class e implements Comparable<e> {

    /* renamed from: a  reason: collision with root package name */
    public bc f686a;

    /* renamed from: b  reason: collision with root package name */
    public String f687b;

    /* renamed from: c  reason: collision with root package name */
    public a f688c;
    public boolean d;
    public int e;
    public long f;
    public short g;
    final /* synthetic */ b h;

    private e(b bVar) {
        this.h = bVar;
        this.e = 0;
        this.f = new Date().getTime();
        this.g = -1;
    }

    /* synthetic */ e(b bVar, c cVar) {
        this(bVar);
    }

    /* renamed from: a */
    public int compareTo(e eVar) {
        if (this.f > eVar.f) {
            return 1;
        }
        if (this.f < eVar.f) {
            return -1;
        }
        return 0;
    }
}
