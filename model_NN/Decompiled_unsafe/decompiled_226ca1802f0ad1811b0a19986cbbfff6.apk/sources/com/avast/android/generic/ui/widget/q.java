package com.avast.android.generic.ui.widget;

import android.os.Parcel;
import android.os.Parcelable;
import com.avast.android.generic.ui.widget.PasswordTextView;

/* compiled from: PasswordTextView */
final class q implements Parcelable.Creator<PasswordTextView.SavedState> {
    q() {
    }

    /* renamed from: a */
    public PasswordTextView.SavedState createFromParcel(Parcel parcel) {
        return new PasswordTextView.SavedState(parcel);
    }

    /* renamed from: a */
    public PasswordTextView.SavedState[] newArray(int i) {
        return new PasswordTextView.SavedState[i];
    }
}
