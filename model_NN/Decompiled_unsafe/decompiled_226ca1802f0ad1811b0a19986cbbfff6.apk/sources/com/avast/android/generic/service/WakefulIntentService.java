package com.avast.android.generic.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.os.Handler;

public abstract class WakefulIntentService extends IntentService {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f959a = WakefulIntentService.class;
    @SuppressLint({"HandlerLeak"})

    /* renamed from: b  reason: collision with root package name */
    private Handler f960b = new c(this);

    protected WakefulIntentService() {
        super("WakefulIntentService");
    }
}
