package com.avast.android.generic.licensing.database;

import android.content.Context;
import com.avast.android.generic.a;
import com.avast.android.generic.licensing.c.b;
import com.avast.android.generic.licensing.c.i;
import com.avast.android.generic.util.z;
import java.util.List;

/* compiled from: PurchaseDatabaseHelper */
final class h implements com.avast.android.generic.licensing.c.h {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f892a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ Context f893b;

    h(b bVar, Context context) {
        this.f892a = bVar;
        this.f893b = context;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.licensing.c.b.a(boolean, java.util.List<java.lang.String>, java.util.List<java.lang.String>):com.avast.android.generic.licensing.c.j
     arg types: [int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      com.avast.android.generic.licensing.c.b.a(java.lang.String, com.avast.android.generic.licensing.c.j, java.util.List<java.lang.String>):int
      com.avast.android.generic.licensing.c.b.a(java.util.List<com.avast.android.generic.licensing.c.l>, com.avast.android.generic.licensing.c.e, com.avast.android.generic.licensing.c.f):void
      com.avast.android.generic.licensing.c.b.a(int, int, android.content.Intent):boolean
      com.avast.android.generic.licensing.c.b.a(boolean, java.util.List<java.lang.String>, java.util.List<java.lang.String>):com.avast.android.generic.licensing.c.j */
    public void a(i iVar) {
        try {
            this.f892a.a(this.f892a.a(false, (List<String>) null, (List<String>) null).a(), new i(this));
        } catch (Exception e) {
            z.a("AvastGenericLic", "Error test mode", e);
            a.a(this.f893b, "Error: " + e.getMessage());
        }
    }
}
