package com.avast.android.generic.licensing.database;

import android.net.Uri;

/* compiled from: BillingContract */
public class b implements c {
    public static Uri a(Uri uri) {
        Uri.Builder buildUpon = uri.buildUpon();
        buildUpon.appendPath("purchaseHistory");
        return buildUpon.build();
    }
}
