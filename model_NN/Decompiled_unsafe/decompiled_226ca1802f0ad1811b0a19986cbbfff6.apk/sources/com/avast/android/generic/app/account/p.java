package com.avast.android.generic.app.account;

import android.text.Editable;
import android.text.TextWatcher;
import com.avast.android.generic.q;
import com.avast.android.generic.util.ab;
import com.avast.android.generic.util.ac;
import com.avast.android.generic.x;

/* compiled from: ConnectAccountFragment */
class p implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f447a;

    private p(ConnectAccountFragment connectAccountFragment) {
        this.f447a = connectAccountFragment;
    }

    /* synthetic */ p(ConnectAccountFragment connectAccountFragment, g gVar) {
        this(connectAccountFragment);
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        boolean z = true;
        if (editable.length() < 1) {
            z = false;
        }
        if ((!this.f447a.y || !this.f447a.e) && !this.f447a.x.c()) {
            this.f447a.t.setVisibility(4);
            this.f447a.u.setVisibility(8);
        } else if (z) {
            this.f447a.t.setVisibility(0);
            ac a2 = ab.a(ab.c(this.f447a.r.getText().toString()));
            if (a2 != ac.VALID) {
                if (a2 == ac.TOO_SHORT) {
                    this.f447a.u.setText(x.msg_avast_account_phone_number_too_short);
                } else {
                    this.f447a.u.setText(x.msg_avast_account_phone_number_not_valid);
                }
                this.f447a.u.setVisibility(0);
                this.f447a.t.setImageResource(q.ic_scanner_result_problem);
            } else if (this.f447a.n()) {
                this.f447a.u.setVisibility(8);
                this.f447a.t.setImageResource(q.ic_scanner_result_ok);
            } else {
                this.f447a.u.setText(x.msg_avast_account_phone_number_not_international);
                this.f447a.u.setVisibility(0);
                this.f447a.t.setImageResource(q.ic_scanner_result_problem);
            }
        } else {
            this.f447a.t.setVisibility(4);
            this.f447a.u.setVisibility(8);
        }
    }
}
