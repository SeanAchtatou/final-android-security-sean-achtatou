package com.avast.android.generic.internet.c.a;

import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import com.google.protobuf.Internal;

/* compiled from: MyAvastPairing */
public final class f extends GeneratedMessageLite implements h {

    /* renamed from: a  reason: collision with root package name */
    private static final f f758a = new f(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public int f759b;
    /* access modifiers changed from: private */

    /* renamed from: c  reason: collision with root package name */
    public Object f760c;
    /* access modifiers changed from: private */
    public Object d;
    /* access modifiers changed from: private */
    public Object e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public Object g;
    /* access modifiers changed from: private */
    public Object h;
    /* access modifiers changed from: private */
    public Object i;
    /* access modifiers changed from: private */
    public ByteString j;
    /* access modifiers changed from: private */
    public ByteString k;
    /* access modifiers changed from: private */
    public t l;
    /* access modifiers changed from: private */
    public Object m;
    /* access modifiers changed from: private */
    public Object n;
    /* access modifiers changed from: private */
    public boolean o;
    /* access modifiers changed from: private */
    public boolean p;
    /* access modifiers changed from: private */
    public c q;
    private byte r;
    private int s;

    private f(g gVar) {
        super(gVar);
        this.r = -1;
        this.s = -1;
    }

    private f(boolean z) {
        this.r = -1;
        this.s = -1;
    }

    public static f a() {
        return f758a;
    }

    public boolean b() {
        return (this.f759b & 1) == 1;
    }

    public String c() {
        Object obj = this.f760c;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.f760c = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString I() {
        Object obj = this.f760c;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.f760c = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean d() {
        return (this.f759b & 2) == 2;
    }

    public String e() {
        Object obj = this.d;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.d = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString J() {
        Object obj = this.d;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.d = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean f() {
        return (this.f759b & 4) == 4;
    }

    public String g() {
        Object obj = this.e;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.e = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString K() {
        Object obj = this.e;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.e = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean h() {
        return (this.f759b & 8) == 8;
    }

    public int i() {
        return this.f;
    }

    public boolean j() {
        return (this.f759b & 16) == 16;
    }

    public String k() {
        Object obj = this.g;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.g = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString L() {
        Object obj = this.g;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.g = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean l() {
        return (this.f759b & 32) == 32;
    }

    public String m() {
        Object obj = this.h;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.h = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString M() {
        Object obj = this.h;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.h = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean n() {
        return (this.f759b & 64) == 64;
    }

    public String o() {
        Object obj = this.i;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.i = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString N() {
        Object obj = this.i;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.i = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean p() {
        return (this.f759b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128;
    }

    public ByteString q() {
        return this.j;
    }

    public boolean r() {
        return (this.f759b & 256) == 256;
    }

    public ByteString s() {
        return this.k;
    }

    public boolean t() {
        return (this.f759b & 512) == 512;
    }

    public t u() {
        return this.l;
    }

    public boolean v() {
        return (this.f759b & 1024) == 1024;
    }

    public String w() {
        Object obj = this.m;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.m = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString O() {
        Object obj = this.m;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.m = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean x() {
        return (this.f759b & 2048) == 2048;
    }

    public String y() {
        Object obj = this.n;
        if (obj instanceof String) {
            return (String) obj;
        }
        ByteString byteString = (ByteString) obj;
        String stringUtf8 = byteString.toStringUtf8();
        if (Internal.isValidUtf8(byteString)) {
            this.n = stringUtf8;
        }
        return stringUtf8;
    }

    private ByteString P() {
        Object obj = this.n;
        if (!(obj instanceof String)) {
            return (ByteString) obj;
        }
        ByteString copyFromUtf8 = ByteString.copyFromUtf8((String) obj);
        this.n = copyFromUtf8;
        return copyFromUtf8;
    }

    public boolean z() {
        return (this.f759b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096;
    }

    public boolean A() {
        return this.o;
    }

    public boolean B() {
        return (this.f759b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192;
    }

    public boolean C() {
        return this.p;
    }

    public boolean D() {
        return (this.f759b & 16384) == 16384;
    }

    public c E() {
        return this.q;
    }

    private void Q() {
        this.f760c = "";
        this.d = "";
        this.e = "";
        this.f = 0;
        this.g = "";
        this.h = "";
        this.i = "";
        this.j = ByteString.EMPTY;
        this.k = ByteString.EMPTY;
        this.l = t.a();
        this.m = "";
        this.n = "";
        this.o = true;
        this.p = false;
        this.q = c.a();
    }

    public final boolean isInitialized() {
        byte b2 = this.r;
        if (b2 == -1) {
            this.r = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        if ((this.f759b & 1) == 1) {
            codedOutputStream.writeBytes(1, I());
        }
        if ((this.f759b & 2) == 2) {
            codedOutputStream.writeBytes(2, J());
        }
        if ((this.f759b & 8) == 8) {
            codedOutputStream.writeInt32(3, this.f);
        }
        if ((this.f759b & 16) == 16) {
            codedOutputStream.writeBytes(4, L());
        }
        if ((this.f759b & 32) == 32) {
            codedOutputStream.writeBytes(5, M());
        }
        if ((this.f759b & 64) == 64) {
            codedOutputStream.writeBytes(6, N());
        }
        if ((this.f759b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
            codedOutputStream.writeBytes(7, this.j);
        }
        if ((this.f759b & 256) == 256) {
            codedOutputStream.writeBytes(8, this.k);
        }
        if ((this.f759b & 4) == 4) {
            codedOutputStream.writeBytes(9, K());
        }
        if ((this.f759b & 512) == 512) {
            codedOutputStream.writeMessage(10, this.l);
        }
        if ((this.f759b & 1024) == 1024) {
            codedOutputStream.writeBytes(11, O());
        }
        if ((this.f759b & 2048) == 2048) {
            codedOutputStream.writeBytes(12, P());
        }
        if ((this.f759b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
            codedOutputStream.writeBool(13, this.o);
        }
        if ((this.f759b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
            codedOutputStream.writeBool(14, this.p);
        }
        if ((this.f759b & 16384) == 16384) {
            codedOutputStream.writeMessage(15, this.q);
        }
    }

    public int getSerializedSize() {
        int i2 = this.s;
        if (i2 == -1) {
            i2 = 0;
            if ((this.f759b & 1) == 1) {
                i2 = 0 + CodedOutputStream.computeBytesSize(1, I());
            }
            if ((this.f759b & 2) == 2) {
                i2 += CodedOutputStream.computeBytesSize(2, J());
            }
            if ((this.f759b & 8) == 8) {
                i2 += CodedOutputStream.computeInt32Size(3, this.f);
            }
            if ((this.f759b & 16) == 16) {
                i2 += CodedOutputStream.computeBytesSize(4, L());
            }
            if ((this.f759b & 32) == 32) {
                i2 += CodedOutputStream.computeBytesSize(5, M());
            }
            if ((this.f759b & 64) == 64) {
                i2 += CodedOutputStream.computeBytesSize(6, N());
            }
            if ((this.f759b & NotificationCompat.FLAG_HIGH_PRIORITY) == 128) {
                i2 += CodedOutputStream.computeBytesSize(7, this.j);
            }
            if ((this.f759b & 256) == 256) {
                i2 += CodedOutputStream.computeBytesSize(8, this.k);
            }
            if ((this.f759b & 4) == 4) {
                i2 += CodedOutputStream.computeBytesSize(9, K());
            }
            if ((this.f759b & 512) == 512) {
                i2 += CodedOutputStream.computeMessageSize(10, this.l);
            }
            if ((this.f759b & 1024) == 1024) {
                i2 += CodedOutputStream.computeBytesSize(11, O());
            }
            if ((this.f759b & 2048) == 2048) {
                i2 += CodedOutputStream.computeBytesSize(12, P());
            }
            if ((this.f759b & FragmentTransaction.TRANSIT_ENTER_MASK) == 4096) {
                i2 += CodedOutputStream.computeBoolSize(13, this.o);
            }
            if ((this.f759b & FragmentTransaction.TRANSIT_EXIT_MASK) == 8192) {
                i2 += CodedOutputStream.computeBoolSize(14, this.p);
            }
            if ((this.f759b & 16384) == 16384) {
                i2 += CodedOutputStream.computeMessageSize(15, this.q);
            }
            this.s = i2;
        }
        return i2;
    }

    public static g F() {
        return g.k();
    }

    /* renamed from: G */
    public g newBuilderForType() {
        return F();
    }

    public static g a(f fVar) {
        return F().mergeFrom(fVar);
    }

    /* renamed from: H */
    public g toBuilder() {
        return a(this);
    }

    static {
        f758a.Q();
    }
}
