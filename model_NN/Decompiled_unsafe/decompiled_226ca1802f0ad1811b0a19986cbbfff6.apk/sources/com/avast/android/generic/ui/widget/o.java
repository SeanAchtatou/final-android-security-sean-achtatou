package com.avast.android.generic.ui.widget;

import android.text.Editable;
import android.text.TextWatcher;

/* compiled from: PasswordTextView */
class o implements TextWatcher {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ PasswordTextView f1154a;

    private o(PasswordTextView passwordTextView) {
        this.f1154a = passwordTextView;
    }

    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (i3 > i2 && charSequence.length() > 0 && PasswordTextView.f1095a.matcher(charSequence.toString()).matches()) {
            this.f1154a.e.removeMessages(0);
            this.f1154a.e.obtainMessage(0, i, i + i3).sendToTarget();
        }
    }

    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    public void afterTextChanged(Editable editable) {
        if (PasswordTextView.f1095a.matcher(editable.toString()).matches()) {
            this.f1154a.e.sendMessageDelayed(this.f1154a.e.obtainMessage(0, 0, 0), 1000);
        }
    }
}
