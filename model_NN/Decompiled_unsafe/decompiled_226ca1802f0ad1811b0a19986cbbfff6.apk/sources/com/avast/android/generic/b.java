package com.avast.android.generic;

import android.content.Context;
import android.support.v4.content.a;

/* compiled from: AsyncLoader */
public abstract class b<D> extends a<D> {
    private D f;

    public b(Context context) {
        super(context);
    }

    public void b(D d) {
        if (!n()) {
            this.f = d;
            super.b(d);
        }
    }

    /* access modifiers changed from: protected */
    public void g() {
        if (this.f != null) {
            b(this.f);
        }
        if (u() || this.f == null) {
            p();
        }
    }

    /* access modifiers changed from: protected */
    public void h() {
        b();
    }

    /* access modifiers changed from: protected */
    public void i() {
        super.i();
        h();
        this.f = null;
    }
}
