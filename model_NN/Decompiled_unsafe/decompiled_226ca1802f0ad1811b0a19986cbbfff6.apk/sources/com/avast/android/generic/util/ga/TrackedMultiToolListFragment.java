package com.avast.android.generic.util.ga;

import android.os.Bundle;
import android.view.View;

public abstract class TrackedMultiToolListFragment extends TrackedListFragment {

    /* renamed from: a  reason: collision with root package name */
    private d f1229a = null;

    public abstract String g();

    public final String k() {
        return this.f1229a.a(g());
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (isAdded()) {
            this.f1229a = new d(getActivity());
        }
    }
}
