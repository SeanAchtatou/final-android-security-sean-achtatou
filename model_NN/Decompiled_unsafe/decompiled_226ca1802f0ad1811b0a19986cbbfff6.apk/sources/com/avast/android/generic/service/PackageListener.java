package com.avast.android.generic.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.avast.android.generic.ab;
import com.avast.android.generic.ae;
import com.avast.android.generic.util.aa;
import com.avast.android.generic.util.z;

public class PackageListener extends BroadcastReceiver {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(android.content.Context, boolean):void */
    public void onReceive(Context context, Intent intent) {
        String stringExtra;
        z.a(context, "Package listener, " + intent.getAction() + " intent received, context " + context);
        if (context != null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            aa.a();
            boolean equals = intent.getAction().equals("android.intent.action.PACKAGE_ADDED");
            boolean equals2 = intent.getAction().equals("android.intent.action.PACKAGE_REMOVED");
            boolean booleanExtra = intent.getBooleanExtra("android.intent.extra.REPLACING", false);
            boolean equals3 = intent.getAction().equals("com.avast.android.generic.action.SHARE_SETTINGS");
            if ((equals || equals2 || equals3) && !booleanExtra) {
                if (!equals3) {
                    stringExtra = aa.a(context, intent);
                    if (stringExtra == null) {
                        return;
                    }
                } else {
                    stringExtra = intent.getStringExtra("sourcePackage");
                    if (stringExtra == null) {
                        return;
                    }
                }
                String str = stringExtra;
                z.a(context, intent.getAction() + " intent received");
                String a2 = aa.a(context, str, "com.avast.android.generic.action.SHARE_SETTINGS");
                if (a2 != null) {
                    z.a(context, "Skipping ADD/REMOVE/SHARESETTINS intent because component " + a2 + " has for sure already handled it");
                } else if (equals3 && str != null && str.equals(context.getPackageName())) {
                    z.a(context, "Skipping SHARESETTINGS intent because it just is myself");
                } else if (equals3) {
                    ab.a(context, true);
                    Intent intent2 = new Intent();
                    intent2.setAction("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION");
                    aa.a(context, str, intent2);
                } else if (equals) {
                    a(context, str, true);
                } else if (equals2) {
                    ab abVar = (ab) com.avast.android.generic.aa.a(context, ae.class);
                    String E = abVar.E();
                    if (E != null && str.equals(E)) {
                        z.a(context, "C2DM owner " + E + " uninstalled, another suite member needs to take over");
                        a(context);
                    }
                    abVar.b();
                    Intent intent3 = new Intent();
                    intent3.setAction("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION");
                    aa.a(context, context.getPackageName(), intent3);
                }
            }
        }
    }

    private void a(Context context) {
        Intent intent = new Intent();
        intent.setAction("com.avast.android.generic.action.C2DM_ENABLE_SUITE");
        intent.putExtra("sourcePackage", context.getPackageName());
        z.a(context, "ALL", "Sending enable C2DM command");
        com.avast.android.generic.util.ae.a(intent);
        context.sendOrderedBroadcast(intent, "com.avast.android.generic.COMM_PERMISSION");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.ab.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.avast.android.generic.ab.a(java.lang.String, java.lang.Object):void
      com.avast.android.generic.ab.a(java.util.Set<java.lang.String>, long):void
      com.avast.android.generic.ab.a(android.content.Context, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, int):void
      com.avast.android.generic.ab.a(java.lang.String, long):void
      com.avast.android.generic.ab.a(java.lang.String, java.lang.String):void
      com.avast.android.generic.ab.a(java.lang.String, boolean):void
      com.avast.android.generic.ab.a(java.lang.String, byte[]):void
      com.avast.android.generic.ab.a(android.content.Context, boolean):void */
    public static void a(Context context, String str, boolean z) {
        if ((!aa.a(context, "com.avast.android.antitheft") || !str.equals("com.avast.android.at_play")) && (!aa.a(context, "com.avast.android.at_play") || !str.equals("com.avast.android.antitheft"))) {
            if (z) {
                ab.a(context, false);
            } else {
                ab.a(context);
            }
            Intent intent = new Intent();
            intent.setAction("com.avast.android.mobilesecurity.app.account.PUSH_ACCOUNT_COMMUNICATION");
            aa.a(context, str, intent);
            return;
        }
        Intent intent2 = new Intent();
        intent2.setAction("com.avast.android.antitheft.action.DISABLE_YOUR_COMPONENTS");
        aa.a(context, "com.avast.android.at_play", intent2);
    }
}
