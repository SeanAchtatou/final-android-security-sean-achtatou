package com.avast.android.generic.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: UserTask */
final class an implements ThreadFactory {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f1184a = new AtomicInteger(1);

    an() {
    }

    public Thread newThread(Runnable runnable) {
        return new Thread(runnable, "UserTask #" + this.f1184a.getAndIncrement());
    }
}
