package com.avast.android.generic.app.wizard;

import android.content.Context;
import android.view.View;
import com.avast.android.generic.aa;
import com.avast.android.generic.ab;
import com.avast.android.generic.util.a;

/* compiled from: WizardIntroduceAccountFragment */
class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ WizardIntroduceAccountFragment f594a;

    f(WizardIntroduceAccountFragment wizardIntroduceAccountFragment) {
        this.f594a = wizardIntroduceAccountFragment;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.avast.android.generic.app.wizard.WizardAccountActivity.a(android.content.Context, boolean):void
     arg types: [android.support.v4.app.FragmentActivity, int]
     candidates:
      com.avast.android.generic.ui.BaseActivity.a(java.lang.Class<? extends android.support.v4.app.FragmentActivity>, android.os.Bundle):void
      com.avast.android.generic.app.wizard.WizardAccountActivity.a(android.content.Context, boolean):void */
    public void onClick(View view) {
        if (((ab) aa.a(this.f594a.getActivity(), ab.class)).t()) {
            this.f594a.launchNext();
        } else if (this.f594a.e()) {
            WizardAccountActivity.call(this.f594a.getActivity(), true);
        } else {
            WizardAccountActivity.a((Context) this.f594a.getActivity(), true);
        }
        a.a(this.f594a);
    }
}
