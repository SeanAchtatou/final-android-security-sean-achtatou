package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;

/* compiled from: ErrorDialog */
class c implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ErrorDialog f542a;

    c(ErrorDialog errorDialog) {
        this.f542a = errorDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f542a.f513b.putExtra("return_code", d.DISMISSED_NEUTRAL.a());
    }
}
