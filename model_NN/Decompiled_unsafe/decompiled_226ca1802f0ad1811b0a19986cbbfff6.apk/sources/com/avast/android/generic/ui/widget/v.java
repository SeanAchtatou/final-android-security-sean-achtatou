package com.avast.android.generic.ui.widget;

import android.os.Parcel;
import android.os.Parcelable;
import com.avast.android.generic.ui.widget.SelectorRow;

/* compiled from: SelectorRow */
final class v implements Parcelable.Creator<SelectorRow.SavedState> {
    v() {
    }

    /* renamed from: a */
    public SelectorRow.SavedState createFromParcel(Parcel parcel) {
        return new SelectorRow.SavedState(parcel, null);
    }

    /* renamed from: a */
    public SelectorRow.SavedState[] newArray(int i) {
        return new SelectorRow.SavedState[i];
    }
}
