package com.avast.android.generic.i;

import android.util.SparseArray;
import java.util.EnumSet;
import java.util.Iterator;

/* compiled from: StreamBackPlugin */
public enum b {
    BLACKHOLE(0),
    ANDROID_STATS(4),
    SHEPHERD(6),
    BAD_NEWS(9);
    
    private static final SparseArray<b> e = new SparseArray<>();
    private final int f;

    static {
        Iterator it = EnumSet.allOf(b.class).iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            e.put(bVar.a(), bVar);
        }
    }

    private b(int i) {
        this.f = i;
    }

    public final int a() {
        return this.f;
    }
}
