package com.avast.android.generic.internet;

import android.os.Bundle;
import com.avast.android.generic.c.a;
import com.avast.b.a.a.ak;
import java.util.Date;

/* compiled from: HttpSender */
public class h implements Comparable<h> {

    /* renamed from: a  reason: collision with root package name */
    public ak f821a = null;

    /* renamed from: b  reason: collision with root package name */
    public k f822b = null;

    /* renamed from: c  reason: collision with root package name */
    public d f823c;
    public a d = null;
    public boolean e = false;
    public boolean f = false;
    public int g = 0;
    public long h = new Date().getTime();
    public int i = 0;
    public Bundle j = null;
    final /* synthetic */ HttpSender k;

    public h(HttpSender httpSender) {
        this.k = httpSender;
    }

    /* renamed from: a */
    public int compareTo(h hVar) {
        if (this.h > hVar.h) {
            return 1;
        }
        if (this.h < hVar.h) {
            return -1;
        }
        return 0;
    }

    public String a() {
        if (this.f821a == null) {
            return "(empty)";
        }
        return this.f821a.toString();
    }
}
