package com.avast.android.generic.app.account;

import com.avast.android.generic.ui.widget.CheckBoxRow;
import com.avast.android.generic.ui.widget.c;
import com.avast.android.generic.x;

/* compiled from: ConnectAccountFragment */
class i implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f435a;

    /* renamed from: b  reason: collision with root package name */
    final /* synthetic */ ConnectAccountFragment f436b;

    i(ConnectAccountFragment connectAccountFragment, p pVar) {
        this.f436b = connectAccountFragment;
        this.f435a = pVar;
    }

    public void a(CheckBoxRow checkBoxRow, boolean z) {
        if (z) {
            this.f436b.z.setVisibility(0);
            this.f436b.z.setText(this.f436b.getString(x.l_avast_account_setup_why_is_phone_number_needed));
            this.f436b.s.setVisibility(0);
        } else {
            this.f436b.s.setVisibility(8);
            this.f436b.z.setVisibility(8);
        }
        this.f435a.afterTextChanged(this.f436b.r.getText());
    }
}
