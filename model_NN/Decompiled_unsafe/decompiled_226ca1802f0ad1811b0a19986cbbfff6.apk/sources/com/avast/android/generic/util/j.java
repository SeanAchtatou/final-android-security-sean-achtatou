package com.avast.android.generic.util;

/* compiled from: BaseTracker */
public enum j {
    AMS("ams"),
    AAT("aat"),
    AB("ab"),
    ASL("asl"),
    AAT_CLIENT("aatClient"),
    AAT_SETUP("aatSetup");
    
    String g;

    private j(String str) {
        this.g = str;
    }

    public String a() {
        return this.g;
    }
}
