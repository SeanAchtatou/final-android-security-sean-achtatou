package com.avast.android.generic.b.a;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.List;

/* compiled from: BadNewsProto */
public final class h extends GeneratedMessageLite implements j {

    /* renamed from: a  reason: collision with root package name */
    private static final h f608a = new h(true);
    /* access modifiers changed from: private */

    /* renamed from: b  reason: collision with root package name */
    public List<c> f609b;

    /* renamed from: c  reason: collision with root package name */
    private byte f610c;
    private int d;

    private h(i iVar) {
        super(iVar);
        this.f610c = -1;
        this.d = -1;
    }

    private h(boolean z) {
        this.f610c = -1;
        this.d = -1;
    }

    public static h a() {
        return f608a;
    }

    private void e() {
        this.f609b = Collections.emptyList();
    }

    public final boolean isInitialized() {
        byte b2 = this.f610c;
        if (b2 == -1) {
            this.f610c = 1;
            return true;
        } else if (b2 == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void writeTo(CodedOutputStream codedOutputStream) {
        getSerializedSize();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f609b.size()) {
                codedOutputStream.writeMessage(1, this.f609b.get(i2));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public int getSerializedSize() {
        int i = this.d;
        if (i == -1) {
            i = 0;
            for (int i2 = 0; i2 < this.f609b.size(); i2++) {
                i += CodedOutputStream.computeMessageSize(1, this.f609b.get(i2));
            }
            this.d = i;
        }
        return i;
    }

    public static i b() {
        return i.g();
    }

    /* renamed from: c */
    public i newBuilderForType() {
        return b();
    }

    public static i a(h hVar) {
        return b().mergeFrom(hVar);
    }

    /* renamed from: d */
    public i toBuilder() {
        return a(this);
    }

    static {
        f608a.e();
    }
}
