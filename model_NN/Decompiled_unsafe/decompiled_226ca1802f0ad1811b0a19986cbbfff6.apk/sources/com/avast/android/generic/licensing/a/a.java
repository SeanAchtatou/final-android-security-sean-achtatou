package com.avast.android.generic.licensing.a;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.avast.android.generic.util.z;
import com.avast.android.generic.x;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.util.HashSet;
import java.util.Set;

/* compiled from: GoogleAccountHelper */
public class a {
    public static Set<b> a(Context context) {
        HashSet hashSet = new HashSet();
        for (Account account : ((AccountManager) context.getSystemService("account")).getAccounts()) {
            if (account.type.equals("com.google")) {
                hashSet.add(a(context, account.name));
            }
        }
        return hashSet;
    }

    private static b a(Context context, String str) {
        return new b(str, "audience:server:client_id:267505377073.apps.googleusercontent.com", GoogleAuthUtil.getToken(context, str, "audience:server:client_id:267505377073.apps.googleusercontent.com"));
    }

    public static void a(Fragment fragment, int i, int i2, String str) {
        if (fragment.isAdded()) {
            try {
                Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(i, fragment.getActivity(), i2);
                if (errorDialog != null) {
                    errorDialog.show();
                } else {
                    com.avast.android.generic.a.a(fragment.getActivity(), str);
                }
            } catch (Exception e) {
                z.a("AvastGeneric", "Can not show google play error dialog", e);
                com.avast.android.generic.a.a(fragment.getActivity(), str);
            }
        }
    }

    public static void a(Fragment fragment, int i, int i2) {
        if (fragment.isAdded()) {
            a(fragment, i, i2, fragment.getString(x.l_google_play_error, Integer.valueOf(i)));
        }
    }

    public static void a(Fragment fragment, Intent intent, int i) {
        if (fragment.isAdded()) {
            try {
                fragment.startActivityForResult(intent, i);
            } catch (Exception e) {
                com.avast.android.generic.a.a(fragment.getActivity(), fragment.getString(x.l_google_play_error_intent));
            }
        }
    }

    public static void a(Context context, Set<b> set) {
        for (b c2 : set) {
            String c3 = c2.c();
            if (!TextUtils.isEmpty(c3)) {
                GoogleAuthUtil.invalidateToken(context, c3);
            }
        }
    }
}
