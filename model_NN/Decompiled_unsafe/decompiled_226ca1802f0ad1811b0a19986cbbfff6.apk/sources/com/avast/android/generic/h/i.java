package com.avast.android.generic.h;

import com.google.protobuf.Internal;

/* compiled from: CommunityIqProto */
public enum i implements Internal.EnumLite {
    UPDATE_RESULT_UP_TO_DATE(0, 1),
    UPDATE_RESULT_UPDATED(1, 2),
    UPDATE_RESULT_OLD_APPLICATION_VERSION(2, 3),
    UPDATE_RESULT_CONNECTION_PROBLEMS(3, 4),
    UPDATE_RESULT_NOT_ENOUGH_INTERNAL_SPACE_TO_UPDATE(4, 5),
    UPDATE_RESULT_INVALID_VPS(5, 6),
    UPDATE_RESULT_UNKNOWN_ERROR(6, 7);
    
    private static Internal.EnumLiteMap<i> h = new j();
    private final int i;

    public final int getNumber() {
        return this.i;
    }

    public static i a(int i2) {
        switch (i2) {
            case 1:
                return UPDATE_RESULT_UP_TO_DATE;
            case 2:
                return UPDATE_RESULT_UPDATED;
            case 3:
                return UPDATE_RESULT_OLD_APPLICATION_VERSION;
            case 4:
                return UPDATE_RESULT_CONNECTION_PROBLEMS;
            case 5:
                return UPDATE_RESULT_NOT_ENOUGH_INTERNAL_SPACE_TO_UPDATE;
            case 6:
                return UPDATE_RESULT_INVALID_VPS;
            case 7:
                return UPDATE_RESULT_UNKNOWN_ERROR;
            default:
                return null;
        }
    }

    private i(int i2, int i3) {
        this.i = i3;
    }
}
