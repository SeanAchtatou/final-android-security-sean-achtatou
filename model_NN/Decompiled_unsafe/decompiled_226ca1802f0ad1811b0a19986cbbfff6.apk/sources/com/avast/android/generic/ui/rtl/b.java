package com.avast.android.generic.ui.rtl;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.avast.android.generic.ui.rtl.a.c;
import com.avast.android.generic.util.t;
import java.lang.reflect.Field;
import java.text.Bidi;

/* compiled from: RtlButton */
public class b extends Button {

    /* renamed from: a  reason: collision with root package name */
    private String f1063a;

    public b(TextView textView, String str) {
        super(textView.getContext());
        this.f1063a = str;
        setId(textView.getId());
        if (textView.getLayoutParams() != null) {
            setLayoutParams(textView.getLayoutParams());
        }
        setPadding(textView.getPaddingLeft(), textView.getPaddingTop(), textView.getPaddingRight(), textView.getPaddingBottom());
        setEnabled(textView.isEnabled());
        setVisibility(textView.getVisibility());
        a(textView);
        setClickable(textView.isClickable());
        setFocusable(textView.isFocusable());
        setAutoLinkMask(textView.getAutoLinkMask());
        setLinksClickable(textView.getLinksClickable());
        setTextSize(0, textView.getTextSize());
        setTextColor(textView.getTextColors());
        setTextScaleX(textView.getTextScaleX());
        setTypeface(textView.getTypeface());
        setLinkTextColor(textView.getLinkTextColors());
        setHintTextColor(textView.getHintTextColors());
        setBackgroundDrawable(textView.getBackground());
        setCompoundDrawablePadding(textView.getCompoundDrawablePadding());
        Drawable[] compoundDrawables = textView.getCompoundDrawables();
        setCompoundDrawables(compoundDrawables[2], compoundDrawables[1], compoundDrawables[0], compoundDrawables[3]);
        setText(textView.getText());
        setHint(textView.getHint());
        setGravity(textView.getGravity());
        b(textView);
        c(textView);
        d(textView);
    }

    private void a(TextView textView) {
        try {
            Field declaredField = TextView.class.getDeclaredField("mSingleLine");
            declaredField.setAccessible(true);
            setInputType(textView.getInputType());
            setSingleLine(((Boolean) declaredField.get(textView)).booleanValue());
        } catch (IllegalAccessException e) {
            t.b("Can't set onClickListener, illegal access to field mSingleLine.", e);
        } catch (NoSuchFieldException e2) {
            t.b("Can't set onClickListener, mSingleLine field not found.", e2);
        }
    }

    private void b(TextView textView) {
        try {
            Field declaredField = View.class.getDeclaredField("mOnClickListener");
            declaredField.setAccessible(true);
            View.OnClickListener onClickListener = (View.OnClickListener) declaredField.get(textView);
            if (onClickListener != null) {
                setOnClickListener(onClickListener);
            }
        } catch (IllegalAccessException e) {
            t.b("Can't set onClickListener, illegal access to field mOnClickListener.", e);
        } catch (NoSuchFieldException e2) {
            t.b("Can't set onClickListener, mOnClickListener field not found.", e2);
        }
    }

    private void c(TextView textView) {
        try {
            Field declaredField = View.class.getDeclaredField("mOnLongClickListener");
            declaredField.setAccessible(true);
            View.OnLongClickListener onLongClickListener = (View.OnLongClickListener) declaredField.get(textView);
            if (onLongClickListener != null) {
                setOnLongClickListener(onLongClickListener);
            }
        } catch (IllegalAccessException e) {
            t.b("Can't set onClickListener, illegal access to field mOnLongClickListener.", e);
        } catch (NoSuchFieldException e2) {
            t.b("Can't set onClickListener, mOnLongClickListener field not found.", e2);
        }
    }

    private void d(TextView textView) {
        try {
            Field declaredField = View.class.getDeclaredField("mOnCreateContextMenuListener");
            declaredField.setAccessible(true);
            View.OnCreateContextMenuListener onCreateContextMenuListener = (View.OnCreateContextMenuListener) declaredField.get(textView);
            if (onCreateContextMenuListener != null) {
                setOnCreateContextMenuListener(onCreateContextMenuListener);
            }
        } catch (IllegalAccessException e) {
            t.b("Can't set onClickListener, illegal access to field mOnCreateContextMenuListenerField.", e);
        } catch (NoSuchFieldException e2) {
            t.b("Can't set onClickListener, mOnCreateContextMenuListenerField field not found.", e2);
        }
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (charSequence == null || this.f1063a == null || !this.f1063a.equals(a.LANG_ARABIC)) {
            super.setText(charSequence, bufferType);
        } else {
            super.setText(c.c(charSequence.toString()), bufferType);
        }
    }

    public void setGravity(int i) {
        Bidi bidi = new Bidi(getText().toString(), -1);
        if ((i & 3) == 3 && (bidi.isRightToLeft() || bidi.isMixed())) {
            super.setGravity((i ^ 3) | 5);
        } else if ((i & 5) != 5 || !bidi.isLeftToRight()) {
            super.setGravity(i);
        } else {
            super.setGravity((i ^ 5) | 3);
        }
    }
}
