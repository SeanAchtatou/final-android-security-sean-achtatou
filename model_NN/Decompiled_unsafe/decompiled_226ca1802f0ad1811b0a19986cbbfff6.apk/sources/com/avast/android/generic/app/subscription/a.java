package com.avast.android.generic.app.subscription;

import android.content.DialogInterface;

/* compiled from: ErrorDialog */
class a implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ErrorDialog f523a;

    a(ErrorDialog errorDialog) {
        this.f523a = errorDialog;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        this.f523a.f513b.putExtra("return_code", d.DISMISSED_POSITIVE.a());
    }
}
